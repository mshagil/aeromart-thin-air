/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.bl;

import com.isa.thinair.airtravelagents.api.dto.SearchInvoicesDTO;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestTransferSendInvoicesBL extends PlatformTestCase {

	public TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) AirTravelagentsModuleUtils.getInstance().getServiceBD(AirtravelagentsConstants.FullBDKeys.AIRTRAVELAGENTSFINANCE_SERVICE_REMOTE);
		
	}

	/*public void testGenerateInvoices() throws Exception{
		
		int param_Month = 6;
		int param_Year = 2006;
		int param_InvoicePeriod=2;
		
		Collection agentCodes = new ArrayList();
		agentCodes.add("JAI028");
		agentCodes.add("KBL002");
		agentCodes.add("KBL003");
		agentCodes.add("KBL005");
		agentCodes.add("KBL006");
		
		
		SearchInvoicesDTO searchInvoicesDTO = new SearchInvoicesDTO();
		
		searchInvoicesDTO.setMonth(param_Month);
		searchInvoicesDTO.setYear(param_Year);
		searchInvoicesDTO.setInvoicePeriod(param_InvoicePeriod);
		//searchInvoicesDTO.setAgentCodes(agentCodes);
	
		try {
		ServiceResponce responce =new InvoiceBL().generateInvoices(searchInvoicesDTO, GenerateInvoiceOption.IF_EXISTS_DO_NOT_PROCEED);
		//ServiceResponce responce =getTravelAgentFinanceBD().generateInvoices(searchInvoicesDTO, GenerateInvoiceOption.IF_EXISTS_DO_NOT_PROCEED);
	
		Collection generatedFailedInvoices = (Collection) responce.getResponseParam(ResponseCodes.EXISTING_INVOICES);
		if(generatedFailedInvoices!=null) {
			System.out.println("already have : "  + generatedFailedInvoices.size());	
		}
		
		}
		catch(ModuleException  e) {
			
			System.out.println(e.getExceptionCode());
		}
	
	}*/
	
	public void testGetGeneratedInvoices() throws Exception{
		
		int param_Month = 6;
		int param_Year = 2006;
		int param_InvoicePeriod=2;
		boolean param_allAgents=true;
		    
	
	/*	Collection agentCodes = new ArrayList();
		agentCodes.add("JAI028");
		agentCodes.add("KBL002");
		agentCodes.add("KBL003");
		agentCodes.add("KBL005");
		agentCodes.add("KBL006");
		*/
		
		SearchInvoicesDTO searchInvoicesDTO = new SearchInvoicesDTO();
		
		searchInvoicesDTO.setMonth(param_Month);
		searchInvoicesDTO.setYear(param_Year);
		searchInvoicesDTO.setInvoicePeriod(param_InvoicePeriod);
		//searchInvoicesDTO.setAgentCodes(agentCodes);
		searchInvoicesDTO.setFilterZeroInvoices(false);
		//TravelAgentFinanceBD agentFinanceBD =   (TravelAgentFinanceBD) AirTravelagentsModuleUtils.getInstance().getServiceBD("airtravelagentsfinance.service.remote");	
		
		Page page = AirTravelagentsModuleUtils.getInvoiceJdbcDAO().getGeneratedInvoices(searchInvoicesDTO, 0, 50);
		//Page page = agentFinanceBD.getGeneratedInvoices(searchInvoicesDTO, 0, 10);
				
		
		System.out.println(page.toString());
	
	}
	

	
/*	public void testTransferInvoicesForAgents() throws Exception{
		
	
		
		int param_Month = 6;
		int param_Year = 2006;
		int param_InvoicePeriod=2;
		
		Collection agentCodes = new ArrayList();
		//agentCodes.add("JAI028");
		//agentCodes.add("KBL002");
		//agentCodes.add("KBL003");
		//agentCodes.add("KBL005");
		//agentCodes.add("KBL006");
	
		SearchInvoicesDTO searchInvoicesDTO = new SearchInvoicesDTO();
		
		searchInvoicesDTO.setMonth(param_Month);
		searchInvoicesDTO.setYear(param_Year);
		searchInvoicesDTO.setInvoicePeriod(param_InvoicePeriod);
		searchInvoicesDTO.setAgentCodes(agentCodes);
		
		try {
			getTravelAgentFinanceBD().transferToInterfaceTables(searchInvoicesDTO);
		} catch (ModuleException e) {
			System.out.println("code : - " + e.getExceptionCode());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//new TravelAgentFinanceBL().generateInvoices()
		
	}*/
	
	
	


}