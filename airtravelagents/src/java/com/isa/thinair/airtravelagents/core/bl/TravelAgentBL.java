/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTicketStockTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentTicketSequnce;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * @author Chamindap Last Modified on 19-Jan-2009
 */
public class TravelAgentBL {

	private static final Log log = LogFactory.getLog(TravelAgentBL.class);
	private static final String INIT_REMARKS = "Initial Creation";
	private static final String PAY_IN_REMARK = "Pay in currency change";

	private static final String SUB_AGENT_CREATION = "Agent Creation";
	private static final String SUB_AGENT_CREDIT_ADJUSTMENT = "Credit Limit Adj.";

	private static AgentDAO agentDAO = AirTravelagentsModuleUtils.getAgentDAO();

	/**
	 * Method to adjust the credit limit.
	 * 
	 * @param agentCode
	 *            the agentCode
	 * @param currentCreditLimit
	 *            the currentCreditLimit
	 * @param newCreditLimit
	 *            the newCreditLimit
	 * @param remarks
	 *            the remarks
	 * @param forceCancel
	 *            the forceCancel
	 * @param loginId
	 *            the loginId
	 * @throws ModuleException
	 */
	public static void adjustCreditLimit(String agentCode, BigDecimal currentCreditLimit, BigDecimal newCreditLimit,
			String remarks, boolean forceCancel, String loginId, String currencySpecifiedIn) throws ModuleException {
		Agent agent = null;
		AgentSummary agentSummary = null;

		agent = agentDAO.getAgent(agentCode);
		agentSummary = agent.getAgentSummary();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		BigDecimal availableCreditBase = agentSummary.getAvailableCredit();
		if (Agent.CURRENCY_LOCAL.equals(currencySpecifiedIn)) {
			availableCreditBase = BLUtil.getAmountInLocal(availableCreditBase, agent.getCurrencyCode(), exchangeRateProxy);
		}

		// credit limits were specified in local the availbae credit base is actually local as converted from the above
		// line.
		if (newCreditLimit.compareTo(AccelAeroCalculator.subtract(currentCreditLimit, availableCreditBase)) == -1) {
			throw new ModuleException("airtravelagent.logic.credit.error");
		}

		BigDecimal newCreditLimitBase = null;
		BigDecimal newCreditLimitLocal = null;
		BigDecimal adjustmentAmountBase = null;
		BigDecimal adjustmentAmountLocal = null;
		BigDecimal bdCurrentLimit = currentCreditLimit;

		String payInCurrency = null;
		if (currencySpecifiedIn == null || Agent.CURRENCY_BASE.equals(currencySpecifiedIn)) {
			newCreditLimitBase = newCreditLimit;
			newCreditLimitLocal = newCreditLimitBase;
			adjustmentAmountBase = AccelAeroCalculator.subtract(newCreditLimitBase, bdCurrentLimit);
			adjustmentAmountLocal = adjustmentAmountBase;
			// newCreditLimitLocal = BLUtil.getAmountInLocal(newCreditLimitBase,agent.getCurrencyCode());
			payInCurrency = AppSysParamsUtil.getBaseCurrency();
		} else {
			newCreditLimitLocal = newCreditLimit;
			adjustmentAmountLocal = AccelAeroCalculator.subtract(newCreditLimitLocal, bdCurrentLimit);
			newCreditLimitBase = BLUtil.getAmountInBase(newCreditLimitLocal, agent.getCurrencyCode(), exchangeRateProxy);
			bdCurrentLimit = BLUtil.getAmountInBase(bdCurrentLimit, agent.getCurrencyCode(), exchangeRateProxy);
			adjustmentAmountBase = AccelAeroCalculator.subtract(newCreditLimitBase, bdCurrentLimit);
			payInCurrency = agent.getCurrencyCode();
		}

		if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
			if (agent.getGsaCode() != null) {
				Agent creditSharedAgent = getCreditSharedAgent(agent.getGsaCode());
				if (creditSharedAgent != null) {
					AgentSummary creditSharedAgentSummary = creditSharedAgent.getAgentSummary();

					if (adjustmentAmountBase.compareTo(creditSharedAgentSummary.getAvailableCredit()) > 0) {
						throw new ModuleException("cc.reporting.agent.no.enough.available.credits");
					} else if (adjustmentAmountBase.compareTo(creditSharedAgent.getCreditLimit()) > 0) {
						throw new ModuleException("cc.reporting.agent.no.enough.credit.limit");
					}

					BigDecimal newCSACreditLimitBase = AccelAeroCalculator.subtract(creditSharedAgent.getCreditLimit(),
							adjustmentAmountBase);

					agentDAO.updateAgentAvailableCredit(creditSharedAgent.getAgentCode(), creditSharedAgent.getCreditLimit(),
							newCSACreditLimitBase);
					if (AccelAeroCalculator.isGreaterThan(adjustmentAmountBase, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						AirTravelagentsModuleUtils.getTravelAgentFinanceBD().recordSubAgentOperationDebit(
								creditSharedAgent.getAgentCode(), adjustmentAmountBase, adjustmentAmountLocal, payInCurrency,
								loginId, SalesChannel.TRAVEL_AGENT, SUB_AGENT_CREDIT_ADJUSTMENT + " - " + agent.getAgentCode());
					} else {
						AirTravelagentsModuleUtils.getTravelAgentFinanceBD().recordSubAgentOperationCredit(
								creditSharedAgent.getAgentCode(), adjustmentAmountBase.negate(), adjustmentAmountLocal.negate(),
								payInCurrency, loginId, SalesChannel.TRAVEL_AGENT,
								SUB_AGENT_CREDIT_ADJUSTMENT + " - " + agent.getAgentCode());
					}
				}
			}
		}

		agentDAO.addAgentCreditHistory(agentCode, newCreditLimitBase, newCreditLimitLocal, payInCurrency, remarks, loginId);

		agentDAO.updateAgentCreditLimit(agentCode, newCreditLimitBase, newCreditLimitLocal);

		agentDAO.updateAgentAvailableCredit(agentCode, bdCurrentLimit, newCreditLimitBase);
	}

	/**
	 * Method to save agent after checking for active GSA.
	 * 
	 * @param agent
	 * @param userId
	 * @return  agent code
	 * @throws ModuleException
	 * 
	 *             FIXME this need to be done only if corresponding app parameter is enabled! FIXME this need to be done
	 *             only if current applicable exchange rate of the currency changes! FIXME situations where exchange
	 *             rate get effective in a future date need to be handled!
	 */
	public static String saveAgent(Agent agent, String userId) throws ModuleException {
		String territoryCode = null;
		String payInCurrency = null;
		Agent gSA = null;

		String agentTypeCode = agent.getAgentTypeCode();
		BigDecimal bankGuranteeSpecified = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditLimitSpecified = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal bankGuranteeConverted = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditLimitConverted = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal bspGuranteeSpecified = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal bspGuranteeConverted = AccelAeroCalculator.getDefaultBigDecimalZero();
		AgentSummary agentSummary = agent.getAgentSummary();

		Agent curAgent = agentDAO.getAgent(agent.getAgentCode());
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
	
		if(curAgent != null) {
			agentDAO.deleteAgentApplicableOND(curAgent.getApplicableOndList());
			agentDAO.deleteAgentHandlingFeeModList(curAgent.getModificationHandlingFees());
		}	
		
		if (Agent.CURRENCY_BASE.equals(agent.getCurrencySpecifiedIn())) {
			bankGuranteeSpecified = agent.getBankGuaranteeCashAdvance();
			creditLimitSpecified = agent.getCreditLimit();
			bspGuranteeSpecified = agent.getBspGuarantee();
			// bankGuranteeConverted = BLUtil.getAmountInLocal(bankGuranteeSpecified, agent.getCurrencyCode());
			// creditLimitConverted = BLUtil.getAmountInLocal(creditLimitSpecified, agent.getCurrencyCode());
			agent.setBankGuaranteeCashAdvanceLocal(bankGuranteeSpecified);
			agent.setCreditLimitLocal(creditLimitSpecified);
			agent.setBspGuaranteeLocal(bspGuranteeSpecified);
			payInCurrency = AppSysParamsUtil.getBaseCurrency();
		} else {
			if (agent.getBankGuaranteeCashAdvanceLocal() != null) {
				bankGuranteeSpecified = agent.getBankGuaranteeCashAdvanceLocal();
			}
			if (agent.getCreditLimitLocal() != null) {
				creditLimitSpecified = agent.getCreditLimitLocal();
			} else {
				agent.setCreditLimitLocal(creditLimitSpecified);
			}

			if (agent.getBspGuaranteeLocal() != null) {
				bspGuranteeSpecified = agent.getBspGuaranteeLocal();
			}

			/*
			 * skip the exchange rate based calculation if bank guarantee has not changed - fix for invalid/no exch rate
			 * currency edit
			 */
			bspGuranteeConverted = BLUtil.getAmountInBase(bspGuranteeSpecified, agent.getCurrencyCode(), exchangeRateProxy);

			if (curAgent != null && curAgent.getBankGuaranteeCashAdvanceLocal() != null
					&& bankGuranteeSpecified.doubleValue() == curAgent.getBankGuaranteeCashAdvanceLocal().doubleValue()) {
				agent.setBankGuaranteeCashAdvance(curAgent.getBankGuaranteeCashAdvance());
				agent.setCreditLimit(curAgent.getCreditLimit());
			} else {
				bankGuranteeConverted = BLUtil.getAmountInBase(bankGuranteeSpecified, agent.getCurrencyCode(), exchangeRateProxy);
				creditLimitConverted = BLUtil.getAmountInBase(creditLimitSpecified, agent.getCurrencyCode(), exchangeRateProxy);

				agent.setBankGuaranteeCashAdvance(bankGuranteeConverted);
				agent.setCreditLimit(creditLimitConverted);
			}
			agent.setBspGuarantee(bspGuranteeConverted);

			if (agentSummary != null) {
				if (agentSummary.getAvailableCredit() == null) {
					agentSummary.setAvailableCredit(agent.getCreditLimit());
				}
				agent.setAgentSummary(agentSummary);
			}
			payInCurrency = agent.getCurrencyCode();
		}

		if (AppSysParamsUtil.isMaintainAgentBspCreditLimit()) {
			if (agent.getVersion() == -1 && agentSummary != null && agent.getBspGuarantee() != null) {
				agentSummary.setAvailableBSPCredit(agent.getBspGuarantee());
				agent.setAgentSummary(agentSummary);
			} else if (agent.getVersion() != -1 && curAgent != null && curAgent.getAgentSummary() != null
					&& curAgent.getAgentSummary().getAvailableBSPCredit() != null) {

				if (agentSummary == null) {
					agentSummary = curAgent.getAgentSummary();
				}

				BigDecimal agentSummBSPCredit = curAgent.getAgentSummary().getAvailableBSPCredit();
				BigDecimal creditAdjstDiffer = AccelAeroCalculator.subtract(curAgent.getBspGuarantee(), agent.getBspGuarantee());
				BigDecimal agentSummUpdatedBSPCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

				agentSummUpdatedBSPCredit = AccelAeroCalculator.subtract(agentSummBSPCredit, creditAdjstDiffer);

				if (AccelAeroCalculator.isLessThan(agentSummUpdatedBSPCredit, AccelAeroCalculator.getDefaultBigDecimalZero())) {
					agentSummUpdatedBSPCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				agentSummary.setAvailableBSPCredit(agentSummUpdatedBSPCredit);
				agent.setAgentSummary(agentSummary);
			}
		} else {
			if (agent.getVersion() != -1 && curAgent != null && curAgent.getBspGuarantee() != null) {
				agent.setBspGuarantee(curAgent.getBspGuarantee());
				if (curAgent.getBspGuarantee().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
					agent.setBspGuaranteeLocal(BLUtil.getAmountInLocal(curAgent.getBspGuarantee(), agent.getCurrencyCode(),
							exchangeRateProxy));
				} else {
					agent.setBspGuaranteeLocal(AccelAeroCalculator.getDefaultBigDecimalZero());
				}

				if (curAgent.getAgentSummary() != null && curAgent.getAgentSummary().getAvailableBSPCredit() != null) {

					if (agentSummary == null) {
						agentSummary = curAgent.getAgentSummary();
					}

					agentSummary.setAvailableBSPCredit(curAgent.getAgentSummary().getAvailableBSPCredit());
					agent.setAgentSummary(agentSummary);
				}

			} else {
				agent.setBspGuarantee(AccelAeroCalculator.getDefaultBigDecimalZero());
				agent.setBspGuaranteeLocal(AccelAeroCalculator.getDefaultBigDecimalZero());
			}
		}
		// AARESAA-8122
		if (curAgent != null) {
			agent.setEnableWSMultiCurrency(curAgent.getEnableWSMultiCurrency());
		}

		if ((agentTypeCode != null) && (agentTypeCode.equals("GSA")) && (agent.getStatus().equals("ACT"))) {
			territoryCode = agent.getTerritoryCode();
			gSA = agentDAO.getGSAForTerritory(territoryCode, agent.getServiceChannel());

			if (gSA != null) {
				if (agent.getAgentCode() == null && agent.getTerritoryCode().equals(gSA.getTerritoryCode())
						&& !AppSysParamsUtil.enableMultipleGSAsForAStation()) {
					throw new ModuleException("airtravelagent.logic.activeGSA" + ".exist.in.terrritory");
				} else if (agent.getTerritoryCode().equals(gSA.getTerritoryCode())
						&& (agent.getAgentCode() != null && !gSA.getAgentCode().equals(agent.getAgentCode()))
						&& !AppSysParamsUtil.enableMultipleGSAsForAStation()) {
					throw new ModuleException("airtravelagent.logic.activeGSA" + ".exist.in.terrritory");
				} else {
					agentDAO.saveAgent(agent, userId);
				}
			} else {
				agentDAO.saveAgent(agent, userId);
			}
		} else {
			agentDAO.saveAgent(agent, userId);
		}

		if (AppSysParamsUtil.isGSAStructureVersion2Enabled() && Agent.HAS_CREDIT_LIMIT_YES.equals(agent.getHasCreditLimit())
				&& agent.getGsaCode() != null && agent.getVersion() <= 0) {

			Agent reportingFixedAgent = getCreditSharedAgent(agent.getGsaCode());
			AgentSummary fixedAgentSummary = reportingFixedAgent.getAgentSummary();

			if (agent != null && reportingFixedAgent != null) {

				fixedAgentSummary.setAvailableCredit(
						AccelAeroCalculator.subtract(fixedAgentSummary.getAvailableCredit(), agent.getCreditLimit()));
				agentDAO.saveAgent(reportingFixedAgent, userId);

				BigDecimal agentCreditLimitLocal = null;
				String agentCurrencyCode = null;
				if (Agent.CURRENCY_BASE.equals(agent.getCurrencyCode())) {
					agentCreditLimitLocal = agent.getCreditLimit();
					agentCurrencyCode = AppSysParamsUtil.getBaseCurrency();
				} else {
					agentCreditLimitLocal = agent.getCreditLimitLocal();
					agentCurrencyCode = agent.getCurrencyCode();
				}

				AirTravelagentsModuleUtils.getTravelAgentFinanceBD().recordSubAgentOperationDebit(
						reportingFixedAgent.getAgentCode(), agent.getCreditLimit(), agentCreditLimitLocal, agentCurrencyCode,
						userId, SalesChannel.TRAVEL_AGENT, SUB_AGENT_CREATION + " - " + agent.getAgentCode());
			}
		}

		/* Insert an entry in the credit history if paying currency changed */
		if (curAgent != null) {
			boolean payInCurChanged = !agent.getCurrencySpecifiedIn().equalsIgnoreCase(curAgent.getCurrencySpecifiedIn());
			boolean curChanged = !agent.getCurrencyCode().equalsIgnoreCase(curAgent.getCurrencyCode());
			if (payInCurChanged
					|| (!payInCurChanged && agent.getCurrencySpecifiedIn().equals(Agent.CURRENCY_LOCAL) && curChanged)) {
				agentDAO.addAgentCreditHistory(agent.getAgentCode(), agent.getCreditLimit(), agent.getCreditLimitLocal(),
						payInCurrency, PAY_IN_REMARK, userId);
			}
		} else { // new agent scenario
			if (agent.getVersion() <= 0) {
				agentDAO.addAgentCreditHistory(agent.getAgentCode(), agent.getCreditLimit(), agent.getCreditLimitLocal(),
						payInCurrency, INIT_REMARKS, userId);
			}
		}
		return agent.getAgentCode();
	}

	/**
	 * @param agentTicketStockTo
	 * @throws ModuleException
	 */
	public static void saveAgenTicketStock(AgentTicketStockTO agentTicketStockTo) throws ModuleException {
		AgentTicketSequnce agentTktSeqnce;
		// Add TKT series
		if (agentTicketStockTo.getVersion() == -1) {

			boolean validSeries = agentDAO.isValidAgentTicketSequnceRange(agentTicketStockTo.getAgentCode(),
					agentTicketStockTo.getSequenceLowerBound(), agentTicketStockTo.getSequenceUpperBound(), -1);
			if (!validSeries) {
				throw new ModuleException("airtravelagent.tkt.seq.overlap", AirTravelAgentConstants.MODULE_NAME);
			}

			agentTktSeqnce = TravelAgentBL.createAgentTicketSequnce(agentTicketStockTo);

			agentDAO.createAgentTicketSequnceRange(agentTktSeqnce.getSequnceName(), agentTicketStockTo.getSequenceLowerBound(),
					agentTicketStockTo.getSequenceUpperBound(), agentTicketStockTo.getCycleSequence().equalsIgnoreCase("Y"));

		} else {
			// Edit TKT series
			boolean validSeries = agentDAO.isValidAgentTicketSequnceRange(agentTicketStockTo.getAgentCode(),
					agentTicketStockTo.getSequenceLowerBound(), agentTicketStockTo.getSequenceUpperBound(),
					agentTicketStockTo.getAgentTicketSeqId());

			if (!validSeries) {
				throw new ModuleException("airtravelagent.tkt.seq.overlap", AirTravelAgentConstants.MODULE_NAME);
			}

			agentDAO.updateAgentTicketStockUpperBound(agentTicketStockTo.getSequenceName(),
					agentTicketStockTo.getSequenceUpperBound());
			agentTktSeqnce = agentDAO.getAgentTicketStock(agentTicketStockTo.getAgentTicketSeqId());
			agentTktSeqnce.setStatus(agentTicketStockTo.getStatus());
			agentTktSeqnce.setSequnceUpperBound(agentTicketStockTo.getSequenceUpperBound());

		}
		agentDAO.saveAgentTicketStock(agentTktSeqnce);
	}

	/**
	 * @param agentTicketStockTo
	 * @return
	 * @throws ModuleException
	 */
	private static AgentTicketSequnce createAgentTicketSequnce(AgentTicketStockTO agentTicketStockTo) throws ModuleException {

		AgentTicketSequnce agentTktSeqnce = new AgentTicketSequnce();

		agentTktSeqnce.setAgentCode(agentTicketStockTo.getAgentCode());
		agentTktSeqnce.setSequnceLowerBound(agentTicketStockTo.getSequenceLowerBound());
		agentTktSeqnce.setSequnceUpperBound(agentTicketStockTo.getSequenceUpperBound());
		agentTktSeqnce.setCycle(agentTicketStockTo.getCycleSequence());
		agentTktSeqnce.setStatus(agentTicketStockTo.getStatus());

		Collection<AgentTicketSequnce> agentTktStocks = agentDAO.getAgentTicketStocks(agentTicketStockTo.getAgentCode());

		agentTktSeqnce.setSequnceName(AgentTicketSequnce.TICKET_SEQ_SUFFIX + "_" + agentTicketStockTo.getAgentCode() + "_"
				+ (agentTktStocks.size() + 1));

		return agentTktSeqnce;

	}

	/**
	 * Update currecny exRate CREDIT_LIMIT_UPDATE_STATUS to Success \ Fail
	 * 
	 * @param colCurrency
	 * @param status
	 * @throws ModuleException
	 */
	public static void updateCreditLimitUpdateStatus(Collection<CurrencyExchangeRate> colCurrExchangeRates, String status)
			throws ModuleException {
		if (colCurrExchangeRates != null && status != null && !colCurrExchangeRates.isEmpty()) {
			Iterator<CurrencyExchangeRate> itrCurrencyList = colCurrExchangeRates.iterator();
			CurrencyExchangeRate currExRate;
			while (itrCurrencyList.hasNext()) {
				currExRate = (CurrencyExchangeRate) itrCurrencyList.next();
				currExRate.setCreditLimitUpdateStatus(status);
			}

			BLUtil.saveOrUpdateCurrencyExRate(colCurrExchangeRates);
		}
	}

	/**
	 * get all the agents for a dry user.
	 * 
	 * @return
	 */
	public static List<AgentInfoDTO> getAllAgenntsForDryOperation(boolean isOnAccount) {
		return AirTravelagentsModuleUtils.getAgentJdbcDAO().getAllAgentsForRemoteRefund(isOnAccount);
	}

	/**
	 * get all the reporting agents for dry user refund
	 * 
	 * @param agentCode
	 * @return
	 */
	public static List<AgentInfoDTO> getAllReportingAgenntsForDryOperation(String agentCode, boolean isOnAccount) {
		return AirTravelagentsModuleUtils.getAgentJdbcDAO().getReportingAgentsForRemoteRefund(agentCode, isOnAccount);
	}

	public static void blockNonPayingAgents(String userId) throws ModuleException {

		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();

		Calendar curDateCal = Calendar.getInstance();
		curDateCal.setTime(currentDate);

		if (curDateCal.get(Calendar.DAY_OF_MONTH) == 1 || curDateCal.get(Calendar.DAY_OF_MONTH) == 16) {
			int noOfPastInvoices = AppSysParamsUtil.getNoOfInvoicesForBlockAgentOperation();

			Date[] dates = getPastInvoicePeriods(noOfPastInvoices, curDateCal);

			log.info("Executing blockNonPayingAgents  " + "[Day Of Execution = " + curDateCal.get(Calendar.DAY_OF_MONTH) + ", "
					+ "pastInvoices = " + noOfPastInvoices + "," + "invoiceFromDate= " + dates[0] + "" + "invoicesToDate= "
					+ dates[1] + "]");

			// Agents who has invoices not settled
			Collection<String> agentCodes = AirTravelagentsModuleUtils.getAgentJdbcDAO().getAgentsWithUnsettledInvoices(dates[0],
					dates[1]);

			if (agentCodes != null && agentCodes.size() > 0) {
				Collection<Agent> agentsCol = AirTravelagentsModuleUtils.getAgentDAO().getAgentsCol(agentCodes);
				if (agentsCol != null && agentsCol.size() > 0) {
					Collection<Agent> agentCollection = new ArrayList<Agent>();
					Iterator<Agent> agentsItr = agentsCol.iterator();
					while (agentsItr.hasNext()) {
						Agent agent = (Agent) agentsItr.next();
						agent.setStatus(Agent.STATUS_BLOCKED);
						agent.setModifiedBy(userId);
						agent.setModifiedDate(new Date());
						agentCollection.add(agent);
					}

					if (!agentCollection.isEmpty()) {
						AirTravelagentsModuleUtils.getAgentDAO().saveOrUpdate(agentCollection);
					}
				}
			} else {
				log.info("No direct paying agents found with unsettled invoices");
			}
		} else {
			log.warn("Skipping blockNonPayingAgents as current day is not allowed [Day Of Execution = "
					+ curDateCal.get(Calendar.DAY_OF_MONTH) + "]");
		}
	}

	public static boolean isDryAgent(String agentCode) {
		Agent agent = agentDAO.getAgent(agentCode);
		String ownAirlineCode = AppSysParamsUtil.getDefaultAirlineIdentifierCode();
		if (agent != null) {
			if (!agent.getAirlineCode().equals(ownAirlineCode)) {
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	/**
	 * Calculates invoice period for blocking agents.
	 * 
	 * Assumption - Invoices are generated bi-weekly on 1st and 16th of each month.
	 * 
	 * @param numberOfPastInvoices
	 * @param curDateCal
	 * @return
	 */
	private static Date[] getPastInvoicePeriods(int numberOfPastInvoices, Calendar curDateCal) {

		Calendar toDateCal = Calendar.getInstance();
		toDateCal.setTime(curDateCal.getTime());
		toDateCal.add(Calendar.DATE, -1);
		toDateCal.set(Calendar.HOUR_OF_DAY, 23);
		toDateCal.set(Calendar.MINUTE, 59);
		toDateCal.set(Calendar.SECOND, 59);
		toDateCal.set(Calendar.MILLISECOND, 999);

		Calendar fromDateCal = Calendar.getInstance();
		fromDateCal.setTime(toDateCal.getTime());

		if (toDateCal.get(Calendar.DAY_OF_MONTH) == 15) {
			fromDateCal.set(Calendar.DAY_OF_MONTH, 1);
		} else {
			fromDateCal.set(Calendar.DAY_OF_MONTH, 16);
		}

		fromDateCal.set(Calendar.HOUR_OF_DAY, 0);
		fromDateCal.set(Calendar.MINUTE, 0);
		fromDateCal.set(Calendar.SECOND, 0);
		fromDateCal.set(Calendar.MILLISECOND, 0);

		for (int i = 0; i < numberOfPastInvoices; ++i) {
			if (fromDateCal.get(Calendar.DAY_OF_MONTH) == 16) {
				fromDateCal.set(Calendar.DAY_OF_MONTH, 1);
			} else {
				fromDateCal.set(Calendar.DAY_OF_MONTH, 16);
				fromDateCal.add(Calendar.MONTH, -1);
			}
		}

		return new Date[] { fromDateCal.getTime(), toDateCal.getTime() };
	}

	public static Agent getCreditSharedAgent(String agentCode) {
		String requestingAgent = agentCode;
		Agent creditSharedAgent = null;
		boolean retrieveCreditSharedAgent = false;
		do {
			Agent agent = agentDAO.getAgent(requestingAgent);
			if (agent != null) {
				if (Agent.HAS_CREDIT_LIMIT_YES.equals(agent.getHasCreditLimit())) {
					creditSharedAgent = agent;
					retrieveCreditSharedAgent = true;
				} else {
					requestingAgent = agent.getGsaCode();
				}
			} else {
				creditSharedAgent = agentDAO.getAgent(agentCode);
				retrieveCreditSharedAgent = true;
			}
		} while (!retrieveCreditSharedAgent);

		return creditSharedAgent;
	}

	public static Collection<Agent> getReportingAgentsForGSA(String gsaId, Collection<String> selectedAgentTypes, boolean includeGsa) {
		Collection<Agent> subAgents = new ArrayList<>();

		if (includeGsa) {
			subAgents.add(agentDAO.getAgent(gsaId));
		}

		if (gsaId != null) {
			Collection<Agent> reportingAgents = agentDAO.getReportingAgentsForGSA(gsaId);
			if (selectedAgentTypes.contains("All") && reportingAgents != null) {
				subAgents.addAll(reportingAgents);
				for (Agent reportingAgent : reportingAgents) {
					subAgents.addAll(getReportingAgentsForGSA(reportingAgent.getAgentCode(), selectedAgentTypes, false));
				}
			} else if (reportingAgents != null) {
				for (Agent reportingAgent : reportingAgents) {
					if (selectedAgentTypes.contains(reportingAgent.getAgentTypeCode())) {
						subAgents.add(reportingAgent);
					}
					subAgents.addAll(getReportingAgentsForGSA(reportingAgent.getAgentCode(), selectedAgentTypes, false));
				}
			}
		}

		return subAgents;
	}
}