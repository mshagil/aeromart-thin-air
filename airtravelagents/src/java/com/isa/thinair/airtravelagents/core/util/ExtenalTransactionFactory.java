/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airtravelagents.core.util;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import com.isa.thinair.airtravelagents.api.model.PaxExtTransaction;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * factory to get credits and debits
 * 
 * @author Thushara
 */
public abstract class ExtenalTransactionFactory {

	/**
	 * 
	 * @param tnxId
	 * @param amount
	 * @param nominalCode
	 * @param invoiced
	 * @param notes
	 * @param agentCode
	 * @param userId
	 * @return
	 * @throws ModuleException
	 */
	public static PaxExtTransaction getCreditInstance(BigDecimal amount, int nominalCode, String agentCode, String userId,
			Integer salesChannelCode) throws ModuleException {

		PaxExtTransaction extTransaction = new PaxExtTransaction();

		if (amount != null && amount.compareTo(BigDecimal.ZERO) != -1) {
			throw new ModuleException("airtravelagent.param.null");
		}

		extTransaction.setAmount(amount.negate());
		extTransaction.setDrOrcr(AirTravelAgentConstants.TnxTypes.CREDIT);
		extTransaction.setNominalCode(nominalCode);
		extTransaction.setTransctionDate(new GregorianCalendar().getTime());
		extTransaction.setUserId(userId);
		extTransaction.setSalesChannelCode(salesChannelCode);

		return extTransaction;
	}

	/**
	 * 
	 * @param tnxId
	 * @param amount
	 * @param nominalCode
	 * @param invoiced
	 * @param notes
	 * @param agentCode
	 * @param userId
	 * @return
	 * @throws ModuleException
	 */
	public static PaxExtTransaction getDebitInstance(BigDecimal amount, int nominalCode, String agentCode, String userId,
			Integer salesChannelCode, String pnr) throws ModuleException {

		PaxExtTransaction extTransaction = new PaxExtTransaction();

		if (amount != null && amount.compareTo(BigDecimal.ZERO) != -1) {
			throw new ModuleException("airtravelagent.param.null");
		}

		extTransaction.setAmount(amount);
		extTransaction.setDrOrcr(AirTravelAgentConstants.TnxTypes.DEBIT);
		extTransaction.setNominalCode(nominalCode);
		extTransaction.setTransctionDate(new GregorianCalendar().getTime());
		extTransaction.setUserId(userId);
		extTransaction.setSalesChannelCode(salesChannelCode);

		return extTransaction;
	}
}
