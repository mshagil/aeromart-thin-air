/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import com.isa.thinair.airtravelagents.api.model.AgentCollection;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentCollectionDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Chamindap
 * @isa.module.dao-impl dao-name="agentCollectionDAO"
 */
public class AgentCollectionDAOImpl extends PlatformHibernateDaoSupport implements AgentCollectionDAO {

	/**
	 * The constructor.
	 */
	public AgentCollectionDAOImpl() {
		super();
	}

	/**
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentCollectionDAO#getAgentCollections(String)
	 */
	public Collection<AgentCollection> getAgentCollections(String agentCode) {
		Collection<AgentCollection> agentCollection = null;

		agentCollection = find("select ac from AgentCollection as ac where ac.agentCode=?", agentCode, AgentCollection.class);

		return agentCollection;
	}

	/**
	 * @param agentcollection
	 *            .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 *      .AgentCollectionDAO#addAgentCollection(com.isa.thinair.airtravelagents .api.model.AgentCollection)
	 */
	public void addAgentCollection(AgentCollection agentcollection) {
		try {
			hibernateSave(agentcollection);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentCollectionDAO#removeAgentCollection(String)
	 */
	public void removeAgentCollection(int receiptNumber) {
		Object agentCollection = load(AgentCollection.class, new Integer(receiptNumber));
		delete(agentCollection);
	}

	/**
	 * @param receiptNumber
	 *            .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 *      .AgentCollectionDAO#updateAgentCollection(java.lang.String)
	 */
	public void updateAgentCollection(int receiptNumber) {
		List<AgentCollection> list = null;
		AgentCollection agentCollection = null;

		list = find(
				"select agentCollection " + "from AgentCollection as agentCollection where" + " agentCollection.receiptNumber=?",
				Integer.toString(receiptNumber), AgentCollection.class);

		if (list.size() != 0) {
			agentCollection = (AgentCollection) list.get(0);
		}

		agentCollection.setStatus("R"); /* REVERSED changed to R */
		agentCollection.setCancelationDate(new GregorianCalendar().getTime());

		update(agentCollection);

	}

	/**
	 * @param agentCode
	 * @param amount
	 * @param cp
	 * @param isPost
	 *            .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 *      .AgentCollectionDAO#updateAgentSummary(java.lang.String, double, String, boolean)
	 */
	public void updateAgentSummarySettle(String agentCode, BigDecimal amount) {
		AgentSummary agentSummary = null;
		BigDecimal availableFundsForInv;

		agentSummary = getAgentSummary(agentCode);
		availableFundsForInv = agentSummary.getAvailableFundsForInv();
		availableFundsForInv = AccelAeroCalculator.subtract(availableFundsForInv,amount);

		agentSummary.setAvailableFundsForInv(availableFundsForInv);
		update(agentSummary);
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param cp
	 * @param isPost
	 */
	public void updateAgentSummaryPost(String agentCode, BigDecimal amount) {
		AgentSummary agentSummary = null;
		BigDecimal availableCredit;
		BigDecimal availableFundsForInv;

		agentSummary = getAgentSummary(agentCode);
		availableCredit = agentSummary.getAvailableCredit();
		availableFundsForInv = agentSummary.getAvailableFundsForInv();
		availableCredit = AccelAeroCalculator.add(availableCredit,amount);
		availableFundsForInv =  AccelAeroCalculator.add(availableFundsForInv,amount);

		agentSummary.setAvailableCredit(availableCredit);
		agentSummary.setAvailableFundsForInv(availableFundsForInv);

		update(agentSummary);
	}

	// /**
	// *
	// * @param agentCode
	// * @param amount
	// * @param cp
	// * @param isPost
	// */
	// public void updateAgentSummary(String agentCode, double amount, String cp,
	// boolean isPost) {
	// AgentSummary agentSummary = null;
	// double availableCredit;
	// double availableFundsForInv;
	// double availableAdvance;
	//
	// agentSummary = getAgentSummary(agentCode);
	// availableCredit = agentSummary.getAvailableCredit();
	// availableFundsForInv = agentSummary.getAvailableFundsForInv();
	// availableAdvance = agentSummary.getAvailableAdvance();
	// if (cp.equals("INV")) {
	// if (isPost) {
	// /* For invoice post payment*/
	// availableCredit = availableCredit + amount;
	// availableFundsForInv = availableFundsForInv + amount;
	// } else {
	// /* For invoice settlement*/
	// availableFundsForInv = availableFundsForInv - amount;
	// }
	// } else if (cp.equals("ADV")){
	// if (isPost) {
	// /* For advance post payment*/
	// availableAdvance = availableAdvance + amount;
	// } else {
	// /* For advance invoice settlement*/
	// // availableAdvance = availableAdvance - amount;
	// // availableCredit = availableCredit + amount;
	// }
	// }
	//
	// agentSummary.setAvailableCredit(availableCredit);
	// agentSummary.setAvailableAdvance(availableAdvance);
	// agentSummary.setAvailableFundsForInv(availableFundsForInv);
	//
	// update(agentSummary);
	// }

	/**
	 * Method to reverse handling.
	 * 
	 * @param agentCode
	 * @param amount
	 *            .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 *      .AgentCollectionDAO#updateAgentSummary(java.lang.String, BigDecimal)
	 */
	public void updateAgentSummary(String agentCode, BigDecimal amount) {
		BigDecimal availableCredit;
		AgentSummary agentSummary = null;

		agentSummary = getAgentSummary(agentCode);
		availableCredit = agentSummary.getAvailableCredit();
		availableCredit = AccelAeroCalculator.subtract(availableCredit, availableCredit);
		agentSummary.setAvailableCredit(availableCredit);

		update(agentSummary);

	}

	/**
	 * Update AgentSummary
	 * 
	 * @param agentSummary
	 */
	public void updateAgentSummary(AgentSummary agentSummary) {
		update(agentSummary);
	}

	/**
	 * @param agentCode
	 * @param amount
	 *            .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 *      .AgentCollectionDAO#updateAgentSummary(java.lang.String, BigDecimal)
	 */
	public void updateAgentSummary(String agentCode, BigDecimal amount, String cpCode) {
		BigDecimal availableFundsForInv;
		// double availableAdvance;
		AgentSummary agentSummary = null;

		agentSummary = getAgentSummary(agentCode);
		availableFundsForInv = agentSummary.getAvailableFundsForInv();
		// availableAdvance = agentSummary.getAvailableAdvance();

		availableFundsForInv = AccelAeroCalculator.subtract(availableFundsForInv, availableFundsForInv);

		// if (cpCode.equals("INV")) {
		// availableFundsForInv = availableFundsForInv - amount;
		// } else if (cpCode.equals("ADV")) {
		// availableAdvance = availableAdvance - amount;
		// }

		agentSummary.setAvailableFundsForInv(availableFundsForInv);
		// agentSummary.setAvailableAdvance(availableAdvance);
		update(agentSummary);
	}

	/**
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentCollectionDAO#getCollectionDetails(int)
	 */
	public AgentCollection getCollectionDetails(int receiptNumber) {
		AgentCollection agentCollection = null;
		List<AgentCollection> list = null;

		list = find("select ac from AgentCollection " + "as ac where ac.receiptNumber=?",
				Integer.toString(receiptNumber), AgentCollection.class);

		if (list.size() != 0) {
			agentCollection = (AgentCollection) list.get(0);
		}

		return agentCollection;
	}

	/**
	 * Private method to get AgentSummary.
	 * 
	 * @param agentCode
	 *            the agent code.
	 * @return the AgentSummary.
	 */
	public AgentSummary getAgentSummary(String agentCode) {
		List<AgentSummary> list = null;
		AgentSummary agentSummary = null;

		list = find(
				"select agentSummary from " + "AgentSummary as agentSummary where agentSummary.agentCode=?", agentCode, AgentSummary.class);

		if (list.size() != 0) {
			agentSummary = (AgentSummary) list.get(0);
		}

		return agentSummary;
	}

	/**
	 * Method to record sales on agent summary.
	 * 
	 * @param agentCode
	 *            the agent code.
	 * @param amount
	 *            record sales amount.
	 * @param force
	 */
	public void recordDebit(String agentCode, BigDecimal amount, boolean force) throws CommonsDataAccessException {
		AgentSummary agentSummary = null;
		BigDecimal availableCredit;
		BigDecimal amountUpdate = null;
		agentSummary = getAgentSummary(agentCode);

		if (agentSummary != null) {
			availableCredit = agentSummary.getAvailableCredit();
		} else {
			throw new CommonsDataAccessException("airtravelagent.logic.agent" + ".summary.null");
		}

		if ((availableCredit.compareTo(amount) == 1) || (force && (availableCredit.compareTo(amount) == -1))) {
			amountUpdate = AccelAeroCalculator.subtract(availableCredit, amount);
		} else if (!force && (availableCredit.compareTo(amount) == -1)) {
			throw new CommonsDataAccessException("airtravelagent.logic.amount" + ".exceeds");
		}

		agentSummary.setAvailableCredit(amountUpdate);
		update(agentSummary);
	}

	/**
	 * Method to record refund sales on agent summary.
	 * 
	 * @param agentCode
	 *            the agent code.
	 * @param amount
	 *            refund record sales amount.
	 */
	public void recordCredit(String agentCode, BigDecimal amount) throws CommonsDataAccessException {
		AgentSummary agentSummary = null;
		BigDecimal availableCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountUpdate = null;

		agentSummary = getAgentSummary(agentCode);

		if (agentSummary != null) {
			availableCredit = agentSummary.getAvailableCredit();
		} else {
			throw new CommonsDataAccessException("airtravelagent.logic.agent" + ".summary.null");
		}

		amountUpdate = AccelAeroCalculator.add(availableCredit,amount);
		agentSummary.setAvailableCredit(amountUpdate);
		update(agentSummary);
	}
}