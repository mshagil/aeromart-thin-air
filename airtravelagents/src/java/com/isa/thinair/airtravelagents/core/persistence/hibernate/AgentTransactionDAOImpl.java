/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.isa.thinair.airtravelagents.api.dto.AgentTransactionsDTO;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentTransactionDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.ProductType;

/**
 * @author Chamindap / Byorn
 * @isa.module.dao-impl dao-name="agentTransactionDAO"
 */
public class AgentTransactionDAOImpl extends PlatformHibernateDaoSupport implements AgentTransactionDAO {

	private static Log log = LogFactory.getLog(AgentTransactionDAOImpl.class);

	/**
	 * The constructor.
	 */
	public AgentTransactionDAOImpl() {
		super();
	}

	/**
	 * Save or update agent transaction data.
	 * 
	 * @param agentTransaction
	 *            the agentTransaction.
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 *      .AgentTransactionDAO#saveOrUpdate(com.isa.thinair.airtravelagents.api .model.AgentTransaction)
	 */
	public void saveOrUpdate(AgentTransaction agentTransaction) {
		if (log.isDebugEnabled()) {
			log.debug("before saving agent transaction" + agentTransaction.getTnxId());
		}

		super.hibernateSaveOrUpdate(agentTransaction);

		if (log.isDebugEnabled()) {
			log.debug("after saving agent transaction" + agentTransaction.getTnxId());
		}
	}

	/**
	 * Remove given AgentTransaction.
	 * 
	 * @param agentTransactionId
	 *            .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentTransactionDAO#removeAgentTransaction(int)
	 */
	public void removeAgentTransaction(int agentTransactionId) {
		Integer tnsId = new Integer(agentTransactionId);

		Object agentTransaction = load(AgentTransaction.class, tnsId);
		delete(agentTransaction);
	}

	/**
	 * 
	 * Get a given AgentTransaction.
	 * 
	 * @param agentTransactionId
	 *            agentTransactionId.
	 * @return the agentTransactionId.
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentTransactionDAO#getAgentTransaction(int)
	 */
	public AgentTransaction getAgentTransaction(int agentTransactionId) {
		Integer tnsId = new Integer(agentTransactionId);

		return (AgentTransaction) get(AgentTransaction.class, tnsId);
	}

	/**
	 * 
	 * Get a given AgentTransaction.
	 * 
	 * @param receiptNumber
	 *            receiptNumber.
	 * @return the agentTransaction.
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentTransactionDAO#getAgentTransaction(String)
	 */
	public AgentTransaction getAgentTransaction(String receiptNumber) {
		AgentTransaction agentTransaction = null;
		List<AgentTransaction> agentTransactionList = null;

		agentTransactionList = find(
				"select agentTransaction from AgentTransaction as agentTransaction " + "where agentTransaction.receiptNumber =?",
				receiptNumber, AgentTransaction.class);

		if (agentTransactionList.size() != 0) {
			agentTransaction = (AgentTransaction) agentTransactionList.get(0);
		}

		return agentTransaction;
	}

	/**
	 * 
	 */
	public Collection<AgentTransaction> getAgentTransactions(AgentTransactionsDTO transactionsDTO) {

		if (transactionsDTO == null) {
			throw new CommonsDataAccessException("airtravelagent.param.null");
		}

		Collection<Integer> codes = new ArrayList<Integer>();
		Iterator<AgentTransactionNominalCode> tnxNominalCodesIterator;
		String agentCode = transactionsDTO.getAgentCode();
		Date getTnxFromDate = transactionsDTO.getTnxFromDate();
		Date getTnxEndDate = transactionsDTO.getTnxToDate();
		Collection<AgentTransactionNominalCode> nominalCodesLIst = transactionsDTO.getAgentTransactionNominalCodes();
		List<String> adjustmentCarriers = transactionsDTO.getAdjustmentCarriers();

		// business rule : -all the parameters are sent and wont be blank)
		if (agentCode == null || getTnxFromDate == null || getTnxEndDate == null || nominalCodesLIst == null) {
			return null;
		}

		// iterate through the nominal codes sent by the front end.
		tnxNominalCodesIterator = transactionsDTO.getAgentTransactionNominalCodes().iterator();
		while (tnxNominalCodesIterator.hasNext()) {
			AgentTransactionNominalCode agentTransactionNominalCode = (AgentTransactionNominalCode) tnxNominalCodesIterator
					.next();
			codes.add(new Integer(agentTransactionNominalCode.getCode()));
		}

		// prepare the in stirng part for hql using the nominal codes int values.
		String inStr = PlatformUtiltiies.constructINStringForInts(codes);

		StringBuilder sb = new StringBuilder();
		sb.append(" FROM AgentTransaction a ");
		sb.append(" WHERE a.agentCode = ? AND a.transctionDate between ? AND ? AND a.nominalCode in (" + inStr + ") ");
		if (!adjustmentCarriers.isEmpty()) { // filter out by carrier
			sb.append(" AND a.adjustmentCarrierCode in (" + PlatformUtiltiies.constructINStringForCharactors(adjustmentCarriers)
					+ ")");
		}
		sb.append(" order by a.tnxId asc");

		// String hql = " FROM AgentTransaction a "
		// + " WHERE a.agentCode = ? AND a.transctionDate between ? and ? and a.nominalCode in (" + inStr + ") " +
		// "order by a.tnxId asc";

		return find(sb.toString(), new Object[] { agentCode, getTnxFromDate, getTnxEndDate }, AgentTransaction.class);

	}

	@Override
	public List<AgentTransaction> isDuplicateRecordExistForAgent(String agentCode, BigDecimal amount, String currency,
			int nominalCode) {
		return find(
				"select agentTransaction from AgentTransaction as agentTransaction " + "where agentTransaction.agentCode='"
						+ agentCode + "' and ABS(agentTransaction.amountLocal)='" + amount
						+ "' and agentTransaction.currencyCode='" + currency + "' and agentTransaction.nominalCode="
						+ nominalCode, AgentTransaction.class);
	}

	@Override
	public AgentTransaction getAgentTransactionByExtRef(String externalRef) {
		AgentTransaction agentTransaction = null;
		List<AgentTransaction> agentTransactionList = null;
		Object[] params = new Object[] { externalRef, ProductType.GOQUO };
		agentTransactionList = find(
				"select agentTransaction from AgentTransaction as agentTransaction where agentTransaction.externalRef =? AND agentTransaction.productType= ?",
				params, AgentTransaction.class);

		if (agentTransactionList.size() != 0) {
			agentTransaction = (AgentTransaction) agentTransactionList.get(0);
		}

		return agentTransaction;
	}
	
	@Override
	public void updateAgentCreditReveresed(int agentTransactionId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "UPDATE T_AGENT_TRANSACTION A SET A.CREDIT_REVERSED='Y' WHERE A.TXN_ID=" + agentTransactionId;
		jdbcTemplate.update(sql);
	}
	
	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}
}