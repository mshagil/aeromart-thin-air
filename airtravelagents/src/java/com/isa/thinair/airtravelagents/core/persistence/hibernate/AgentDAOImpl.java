/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.criteria.AdminFeeAgentSearchCriteria;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.airtravelagents.api.dto.AgentTokenTo;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTicketStockTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentAlertConfig;
import com.isa.thinair.airtravelagents.api.model.AgentApplicableOND;
import com.isa.thinair.airtravelagents.api.model.AgentCreditBlock;
import com.isa.thinair.airtravelagents.api.model.AgentCreditHistory;
import com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentTicketSequnce;
import com.isa.thinair.airtravelagents.api.model.AgentTicketStock;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CriteriaParserForHibernate;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Chamindap / Byorn
 * @isa.module.dao-impl dao-name="agentDAO"
 * 
 */
public class AgentDAOImpl extends PlatformHibernateDaoSupport implements AgentDAO {

	private final int ORACLE_MAX_IN_PARMS = 1000;

	private final int LOC_TYPE_STATION = 100;
	private final int LOC_TYPE_TERRITORY = 101;
	private final int LOC_TYPE_COUNTRY = 102;
	private final String GSA_CODE = "GSA";

	private final String channelAA = Constants.INTERNAL_CHANNEL;
	private final String channelALL = Constants.BOTH_CHANNELS;
	private final String channelEX = Constants.EXTERNAL_CHANNEL;

	private static Log log = LogFactory.getLog(AgentDAOImpl.class);

	/**
	 * The default constructure.
	 */
	public AgentDAOImpl() {
		super();
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getAgent(String)
	 */
	public Agent getAgent(String agentId) {
		Agent agent = null;
		List<Agent> agentList = null;

		agentList = find("select agent from Agent as agent where agent.agentCode = ? ", agentId, Agent.class);

		if (agentList.size() != 0) {
			agent = agentList.get(0);
		}
		return agent;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getShowPriceInSelectedCur(String)
	 */
	@SuppressWarnings("rawtypes")
	public boolean getShowPriceInSelectedCur(String agentId) {
		String agentWSMultiCurrencySupport = null;
		List<String> featureEnableList = null;

		featureEnableList = find("select agent.enableWSMultiCurrency from Agent as agent" + " where agent.agentCode = ? ",
				agentId, String.class);

		if (featureEnableList.size() != 0) {
			if (featureEnableList.get(0) != null) {
				agentWSMultiCurrencySupport = featureEnableList.get(0);
				if (agentWSMultiCurrencySupport != null && "Y".equals(agentWSMultiCurrencySupport)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getAgent(String, String)
	 */
	@SuppressWarnings("rawtypes")
	public Agent getAgent(String agentId, String[] channel) {
		List<String> channelList = Arrays.asList(channel);
		String channelSql = Util.buildStringInClauseContent(channelList);

		List agentList = find(" select agent from Agent as agent" + " where agent.agentCode = '" + agentId
				+ "' and agent.serviceChannel in (" + channelSql + ")", Agent.class);

		Agent agent = null;

		if (agentList.size() != 0) {
			agent = (Agent) agentList.get(0);
		}

		return agent;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getAgentByIATANumber(String)
	 */
	public Agent getAgentByIATANumber(String agentIATANumber) {
		Agent agent = null;
		List<Agent> agentList = null;

		agentList = find("select agent from Agent as agent" + " where agent.status='" + Agent.STATUS_ACTIVE + "' "
				+ " and agent.agentIATANumber =? and agent.serviceChannel in ('" + channelAA + "','" + channelALL
				+ "') and agent.airlineCode = '" + AppSysParamsUtil.getDefaultAirlineIdentifierCode() + "'", agentIATANumber,
				Agent.class);

		if (agentList.size() != 0) {
			agent = (Agent) agentList.get(0);
		}

		return agent;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getAgentUsers()
	 */
	@SuppressWarnings("rawtypes")
	public Page getAgentUsers(String agentCode, int startIndex, int pageSize) {
		Collection agentUsersCollection = null;
		Page page = null;
		int recSize;
		Query query = null;

		query = getSession().createQuery(
				"select ag.userIds from" + " Agent as ag, AgentUser as au where user.userId = au.userId " + "and au.agentCode='"
						+ agentCode + "' and ag.serviceChannel in ('" + channelAA + "','" + channelALL + "')");

		recSize = query.list().size();

		agentUsersCollection = query.setFirstResult(startIndex).setMaxResults(pageSize).list();

		page = new Page(recSize, startIndex, startIndex + pageSize, agentUsersCollection);

		return page;
	}

	/**
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void saveAgent(Agent agent, String userId) {
		boolean isDebug = log.isDebugEnabled();
		if (isDebug) {
			log.debug("before saveAgent" + agent.getAgentCode());
		}

		AgentSummary agentSummary = agent.getAgentSummary();
		String stationCode = null;
		String agentTypeCode = null;
		int lastAgent = 0;
		int currentAgent = 0;
		String currentAgentCode = null;
		String airlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();

		if (agent.getAgentCode() == null) {
			stationCode = agent.getStationCode();
			agentTypeCode = agent.getAgentTypeCode();

			if (agentTypeCode.equals("GSA")) {
				lastAgent = getAgentIntCode(airlineCode, GSA_CODE);
				currentAgent = lastAgent + 1;
				currentAgentCode = airlineCode.concat(GSA_CODE).concat(Integer.toString(currentAgent));
			} else {
				lastAgent = getAgentIntCode(airlineCode, stationCode);
				currentAgent = lastAgent + 1;
				if (stationCode != null) {
					currentAgentCode = airlineCode.concat(stationCode).concat(Integer.toString(currentAgent));
				}
			}
			agent.setAgentCode(currentAgentCode);
			if (agentSummary != null) {
				agentSummary.setAgentCode(currentAgentCode);
			} else {
				agentSummary = new AgentSummary();
				agentSummary.setAgentCode(currentAgentCode);
			}
			agent.setCreatedDate(new Date());
			agent.setCreatedBy(userId);
		} else {

			// FIX for the created date saving as null issue- Noshani.

			List agentCodeCol = new ArrayList();

			agentCodeCol.add(agent.getAgentCode());
			List agentsCol = (ArrayList) getAllAgents(agentCodeCol);
			if (agentsCol != null && agentsCol.size() > 0) {
				Agent preAgent = (Agent) agentsCol.get(0);
				agent.setCreatedDate(preAgent.getCreatedDate());
				agent.setCreatedBy(preAgent.getCreatedBy());
			}

			agent.setModifiedDate(new Date());
			agent.setModifiedBy(userId);
		}

		if (agent.getEnableWSMultiCurrency() == null) {
			agent.setEnableWSMultiCurrency("N");
		}

		// if(version<0){
		// agent.setCreatedDate(new Date());
		// agent.setCreatedBy(userId);
		// }else {
		// agent.setModifiedDate(new Date());
		// agent.setModifiedBy(userId);
		// }

		saveOrUpdate(agent);

		if (isDebug) {
			log.debug("saved agent" + agent.getAgentCode());
		}

		if (agentSummary != null) {
			saveOrUpdate(agentSummary);
			if (isDebug) {
				log.debug("saved agentSummary" + agentSummary.getAgentCode());
			}
		} else {
			log.warn("################### agent summary was NULL ###############");
		}

		if (log.isDebugEnabled()) {
			log.debug("saved Agent" + agent.getAgentCode());
		}
	}

	@SuppressWarnings("unchecked")
	public Collection<Agent> getAllAgents(Collection<String> agentCodes) {

		if (agentCodes == null || agentCodes.isEmpty()) {
			return new ArrayList<Agent>();
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM T_AGENT WHERE ");
		sql.append(agentCodeSqlGenarate(agentCodes));

		Collection<Agent> agents = (Collection<Agent>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Agent> agents = new ArrayList<Agent>();
				Agent agent;

				if (rs != null) {
					while (rs.next()) {
						agent = new Agent();

						agent.setAgentCode(PlatformUtiltiies.nullHandler(rs.getString("AGENT_CODE")));
						agent.setAgentTypeCode(PlatformUtiltiies.nullHandler(rs.getString("AGENT_TYPE_CODE")));
						agent.setAgentName(PlatformUtiltiies.nullHandler(rs.getString("AGENT_NAME")));
						agent.setAddressLine1(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_LINE_1")));
						agent.setAddressLine2(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_LINE_2")));
						agent.setAddressCity(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_CITY")));
						agent.setAddressStateProvince(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_STATE_PROVINCE")));
						agent.setPostalCode(PlatformUtiltiies.nullHandler(rs.getString("POSTAL_CODE")));
						agent.setAccountCode(PlatformUtiltiies.nullHandler(rs.getString("ACCOUNT_CODE")));
						agent.setStationCode(PlatformUtiltiies.nullHandler(rs.getString("STATION_CODE")));
						agent.setTerritoryCode(PlatformUtiltiies.nullHandler(rs.getString("TERRITORY_CODE")));
						agent.setGsaCode(PlatformUtiltiies.nullHandler(rs.getString("GSA_CODE")));
						agent.setTelephone(PlatformUtiltiies.nullHandler(rs.getString("TELEPHONE")));

						agent.setFax(PlatformUtiltiies.nullHandler(rs.getString("FAX")));
						agent.setEmailId(PlatformUtiltiies.nullHandler(rs.getString("EMAIL_ID")));
						agent.setAgentIATANumber(PlatformUtiltiies.nullHandler(rs.getString("AGENT_IATA_NUMBER")));
						agent.setCreditLimit(rs.getBigDecimal("CREDIT_LIMIT"));
						agent.setTotalCreditLimit(rs.getBigDecimal("CREDIT_LIMIT"));
						agent.setBankGuaranteeCashAdvance(rs.getBigDecimal("BANK_GUARANTEE_CASH_ADVANCE"));
						agent.setStatus(PlatformUtiltiies.nullHandler(rs.getString("STATUS")));
						agent.setCreatedBy(PlatformUtiltiies.nullHandler(rs.getString("CREATED_BY")));
						agent.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
						agent.setModifiedBy(PlatformUtiltiies.nullHandler(rs.getString("MODIFIED_BY")));
						agent.setModifiedDate(rs.getTimestamp("MODIFIED_DATE"));
						agent.setVersion(rs.getLong("VERSION"));
						agent.setReportingToGSA(PlatformUtiltiies.nullHandler(rs.getString("REPORT_TO_GSA")));
						agent.setBillingEmail(PlatformUtiltiies.nullHandler(rs.getString("BILLING_EMAIL")));
						agent.setHandlingChargAmount(rs.getBigDecimal("HANDLING_CHARGE_AMOUNT"));
						agent.setCurrencyCode(PlatformUtiltiies.nullHandler(rs.getString("CURRENCY_CODE")));
						agent.setNotes(PlatformUtiltiies.nullHandler(rs.getString("NOTES")));
						agent.setServiceChannel(PlatformUtiltiies.nullHandler(rs.getString("SERVICE_CHANNEL_CODE")));
						agent.setEnableWSMultiCurrency(PlatformUtiltiies.nullHandler(rs.getString("SHOW_PRICE_IN_SELECTED_CUR")));
						agent.setEmailsToNotify(PlatformUtiltiies.nullHandler(rs.getString("EMAILS_TO_NOTIFY")));

						agents.add(agent);
					}
				}

				return agents;
			}
		});
		return agents;
	}

	/**
	 * Remove the Agent
	 */
	public void removeAgent(String agentCode, long version) throws CommonsDataAccessException {
		Agent agentO = (Agent) load(Agent.class, agentCode);

		long versionO = agentO.getVersion();
		// String channel = agentO.getServiceChannel();

		if (version != versionO) {
			throw new CommonsDataAccessException("module.update.delete.failed");
		} else {
			try {
				if (agentO != null) {

					if (agentO.getAgentSummary() != null) {
						Object agentsummary = load(AgentSummary.class, agentCode);
						if (agentsummary != null) {
							delete(agentsummary);
						}
					}

					deleteAll(getCreditHistoryForAgent(agentCode));
					delete(agentO);
				}
			} catch (ObjectNotFoundException onf) {
				log.error(onf);
			}

		}

	}

	/**
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getAgents(int, int)
	 */
	@SuppressWarnings("unchecked")
	public Page getAgents(int startIndex, int pageSize) {
		Collection<Agent> agentUsersCollection = null;
		Page page = null;
		int recSize;
		Query query = getSession()
				.createSQLQuery(
						"select agent from Agent as agent " + " where agent.serviceChannel in ('" + channelALL + "','"
								+ channelAA + "')");

		recSize = query.list().size();
		agentUsersCollection = query.setFirstResult(startIndex).setMaxResults(pageSize).list();

		page = new Page(recSize, startIndex, startIndex + pageSize, agentUsersCollection);

		return page;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getAgentsForGSA(String)
	 */

	public Collection<Agent> getAgentsForGSA(String gsaId) {
		Collection<Agent> agentsCollection = null;

		agentsCollection = (Collection<Agent>) find("from Agent as agent where agent.gsaCode='" + gsaId + "'", Agent.class);
		return agentsCollection;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getReportingAgentsForGSA(String)
	 */
	public Collection<Agent> getReportingAgentsForGSA(String gsaId) {
		Collection<Agent> agents = null;

		agents = find("select agent from Agent as agent where agent.gsaCode = ? " + "and agent.reportingToGSA = 'Y'", gsaId,
				Agent.class);

		return agents;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getReportingAgentsForGSA(String)
	 */
	public Collection<Agent> getReportingAgentsForTerritory(String territoryId) {
		Collection<Agent> agents = null;

		agents = find("select agent from Agent as agent where agent.territoryCode = ? " + "and agent.reportingToGSA = 'Y'",
				territoryId, Agent.class);

		return agents;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getAgentForStation(String)
	 */
	public Collection<Agent> getAgentForStation(String stationCode) {
		Collection<Agent> agentsCollection = null;
		String channelAA = Constants.INTERNAL_CHANNEL;

		agentsCollection = find("from Agent as agent where agent.stationCode='" + stationCode
				+ "' and agent.serviceChannel in ('" + channelAA + "','" + channelALL + "')", Agent.class);
		return agentsCollection;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#getAgentForTerritory(String)
	 */
	public Collection<Agent> getAgentForTerritory(String territoryId) {
		Collection<Agent> agentsCollection = null;

		agentsCollection = (Collection<Agent>) find("from Agent as agent where agent.territoryCode='" + territoryId
				+ "' and agent.status='ACT' and agent.serviceChannel in ('" + channelAA + "','" + channelALL + "')", Agent.class);
		return agentsCollection;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #getAgentsForLocation(java.lang.String, int)
	 */
	public Collection<Agent> getAgentsForStation(String code, int locType) {
		Collection<Agent> agentsLocationCollection = null;

		if (this.LOC_TYPE_STATION == locType) {
			agentsLocationCollection = this.getAgentForStation(code);
		} else if (this.LOC_TYPE_TERRITORY == locType) {
			agentsLocationCollection = this.getAgentForTerritory(code);
		} else if (this.LOC_TYPE_COUNTRY == locType) {
			agentsLocationCollection = this.getAgentsForCountry(code);
		}
		return agentsLocationCollection;
	}

	public Collection<Agent> getAgentsForCountry(String countryId) {
		Collection<Agent> agentsCollection = null;

		/*
		 * agentsCollection = getHibernateTemplate() .find("select agent from Agent as agent where agent.territoryCode "
		 * + "= (select te.territoryCode from Territory as te, Country as " + "c where c.countryCode = te.countryCode "
		 * + "and c.countryCode = '"+countryId+"')");
		 */
		agentsCollection = find("select agent from Agent as agent where agent.serviceChannel in ('" + channelAA + "','"
				+ channelALL + "') and agent.airlineCode = '" + AppSysParamsUtil.getDefaultAirlineIdentifierCode()
				+ "' and agent.stationCode " + " in (select st.stationCode from Station as st, Country as c "
				+ " where c.countryCode = st.countryCode " + " and c.countryCode = '" + countryId + "')", Agent.class);
		return agentsCollection;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .TravelAgentDAO#searchAgents(Set)
	 */
	@SuppressWarnings("rawtypes")
	public Page searchAgents(List<ModuleCriterion> criteria, int startIndex, int pageSize, List<String> orderByFieldList) {
		Collection searchAgents = null;
		List list = null;
		Criteria cri = null;
		Page page = null;
		int recSize;

		// performance improvement success
		Criteria crirowCount = CriteriaParserForHibernate.parse(criteria, getSession().createCriteria(Agent.class),
				orderByFieldList);
		crirowCount.setProjection(Projections.rowCount());
		list = crirowCount.list();
		recSize = (int) (long) (Long) list.get(0);

		cri = CriteriaParserForHibernate.parse(criteria, getSession().createCriteria(Agent.class), orderByFieldList);

		searchAgents = cri.setFirstResult(startIndex).setMaxResults(pageSize).list();
		page = new Page(recSize, startIndex, startIndex + pageSize, searchAgents);

		return page;
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #assignAgentUser(User, java.lang.String,
	 *      boolean)
	 */
	public void assignAgentUser(String userId, String agentCode, boolean assignFlag) {

		/**
		 * TODO temporary fix : -- One User cant belong to many agents...table structure and model is wrong The temporay
		 * fix will solve this though.
		 */
		// JdbcTemplate jdbcTemplate = new JdbcTemplate(AirtravelagentUtil.getDatasource());
		// jdbcTemplate.execute("delete from T_AGENT_USER  where USER_ID = '" + userId + "'");
		/*****************************************/

		Agent agent = null;
		Set<String> agentUserIds = null;

		agent = getAgent(agentCode);

		if (agent != null) {
			// agentUserIds = agent.getUserIds();

			try {
				agentUserIds = (Set<String>) getSecurityBD().getAgentUserIDs(agentCode);
			} catch (ModuleException e) {
				log.error(e);
			}

			if (assignFlag) {
				agentUserIds.add(userId);
			} else {
				agentUserIds.remove(userId);
			}
			// agent.setUserIds(agentUserIds);
			// saveAgent(agent);
		}
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #getCreditHistoryForAgent(java.lang.String)
	 */
	public Collection<AgentCreditHistory> getCreditHistoryForAgent(String agentCode) {
		Collection<AgentCreditHistory> agentCreditHistoryCollection = null;

		agentCreditHistoryCollection = find("select acr from AgentCreditHistory as acr where "
		// + "acr.agentCode=? order by acr.updateDate, acr.agnCrHisId desc",
				+ " acr.agentCode=? order by acr.agnCrHisId desc", agentCode, AgentCreditHistory.class);

		return agentCreditHistoryCollection;
	}

	/**
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentDAO#getGSAs()
	 */
	public Collection<Agent> getGSAs() {
		Collection<Agent> allGSAS = null;

		allGSAS = find("select agent from Agent as" + " agent where agent.agentTypeCode='GSA' and agent.serviceChannel in ('"
				+ channelAA + "','" + channelALL + "')", Agent.class);

		return allGSAS;
	}

	/**
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentDAO#getAgentTypes()
	 */
	@SuppressWarnings("unchecked")
	public Collection<AgentType> getAgentTypes(List<ModuleCriterion> criteria, List<String> orderByFieldList) {
		return CriteriaParserForHibernate.parse(criteria, getSession().createCriteria(AgentType.class), orderByFieldList).list();
	}

	/**
	 * 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #updateAgentAvailableCredit(java.lang.String,
	 *      BigDecimal, BigDecimal)
	 */
	@SuppressWarnings("unchecked")
	public void updateAgentAvailableCredit(String agentCode, BigDecimal currentCreditLimit, BigDecimal newCreditLimit) {
		AgentSummary agentSummary = null;
		BigDecimal availableCredit;
		BigDecimal newAvailableCredit;
		List<AgentSummary> list = null;

		list = find("select agentSummary from " + "AgentSummary as agentSummary " + "where agentSummary.agentCode=?", agentCode,
				AgentSummary.class);

		if (list.size() != 0) {
			agentSummary = (AgentSummary) list.get(0);
		}

		availableCredit = agentSummary.getAvailableCredit();

		newAvailableCredit = AccelAeroCalculator.add(availableCredit, newCreditLimit, currentCreditLimit.negate());

		if (newAvailableCredit.compareTo(BigDecimal.ZERO) != -1) {
			agentSummary.setAvailableCredit(newAvailableCredit);
		} else {
			agentSummary = null;
		}
		update(agentSummary);

	}

	/**
	 * 
	 * @param agentCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<AgentSummary> getAgentSummary(String agentCode) {
		return find("select agentSummary from " + "AgentSummary as agentSummary " + "where agentSummary.agentCode=?", agentCode,
				AgentSummary.class);
	}

	/**
	 * Save or Update
	 * 
	 * @param agentSummary
	 */
	public void saveOrUpdate(AgentSummary agentSummary) {
		super.hibernateSaveOrUpdate(agentSummary);
	}

	/**
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #updateAgentCreditLimit(java.lang.String,
	 *      double)
	 */
	public void updateAgentCreditLimit(String agentCode, BigDecimal newCreditLimitBase, BigDecimal newCreditLimitLocal) {
		boolean isDebugEn = log.isDebugEnabled();
		if (isDebugEn) {
			log.debug("inside updateAgentCreditLimit " + agentCode + "| new credit limit base :" + newCreditLimitBase
					+ " new credit limit local " + newCreditLimitLocal);
		}
		Agent agent = null;

		agent = getAgent(agentCode);
		agent.setCreditLimit(newCreditLimitBase);
		agent.setCreditLimitLocal(newCreditLimitLocal);
		agent.setTotalCreditLimit(newCreditLimitBase);
		agent.setTotalCreditLimitLocal(newCreditLimitLocal);
		update(agent);
		if (isDebugEn) {
			log.debug("finished updateAgentCreditLimit " + agentCode);
		}
	}

	/**
	 * Method will return all names of the agent with agent code
	 * 
	 * @return return all names of the agent with agent code.
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentDAO#getAllAgentNames()
	 */
	@SuppressWarnings("unchecked")
	public Hashtable<String, String> getAllAgentNames() {
		Hashtable<String, String> hashtable = null;
		List<Agent> agents = null;
		Iterator<Agent> iter = null;

		agents = find("from Agent", Agent.class);
		hashtable = new Hashtable<String, String>();
		iter = agents.iterator();

		while (iter.hasNext()) {
			Agent agent = (Agent) iter.next();
			hashtable.put(agent.getAgentCode(), agent.getAgentDisplayName());
		}

		return hashtable;
	}

	/**
	 * Method will return all GSA agents with agent code, name and territory id.
	 * 
	 * @param territories
	 * @return agents.
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentDAO#getGSAsForTerritories(java.util.Collection)
	 */
	public Collection<Agent> getGSAsForTerritories() {
		Collection<Agent> gSAsForTerritories = null;
		LocationBD locationBD = null;
		Collection<Territory> territories = null;
		String territoryCode = null;
		Collection<Agent> allGSAS = null;
		Agent agent = null;

		locationBD = getLocationBD();
		gSAsForTerritories = new ArrayList<Agent>();
		allGSAS = getGSAs();

		try {
			territories = locationBD.getTerritories();
			Iterator<Territory> iter = territories.iterator();

			while (iter.hasNext()) {
				Territory territory = (Territory) iter.next();
				territoryCode = territory.getTerritoryCode();

				Iterator<Agent> iterator = allGSAS.iterator();
				while (iterator.hasNext()) {
					Agent element = (Agent) iterator.next();

					if (element.getTerritoryCode() != null && element.getTerritoryCode().equals(territoryCode)) {
						agent = new Agent();
						agent.setTerritoryCode(territoryCode);
						agent.setAgentName(element.getAgentDisplayName());
						agent.setAgentCode(element.getAgentCode());
						agent.setStatus(element.getStatus());
						gSAsForTerritories.add(agent);
					}
				}
			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return gSAsForTerritories;
	}

	/**
	 * @return .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentDAO#getUnassignUsers()
	 */
	@SuppressWarnings("unchecked")
	public Collection<User> getUnassignUsers() {
		Collection<User> users = null;

		users = find("select user from User as user " + "where user.userId not in (select distinct au.userId "
				+ "from AgentUser as au)", User.class);
		return users;
	}

	/**
	 * Private method to get last agent code.
	 * 
	 * @return agent code int value.
	 */
	@SuppressWarnings("rawtypes")
	private int getAgentIntCode(String airlineCode, String agentcode) {
		Collection<String> agents = null;
		int max = 0;
		int temp = 0;

		agents = find("select agent.agentCode from " + "Agent as agent where agent.agentCode like '" + airlineCode + agentcode
				+ "%'", String.class);

		Iterator<String> iter = agents.iterator();

		while (iter.hasNext()) {
			String element = iter.next();
			if ("".equals(airlineCode)) {
				temp = Integer.parseInt(element.substring(3).trim());
			} else {
				temp = Integer.parseInt(element.substring(6).trim());// trimed to get it working on mig-data : Nasly
																		// 02Apr
			}

			if (temp > max) {
				max = temp;
			}
		}
		return max;
	}

	/**
	 * private String getCountryCurrencCode(Agent agent) { String queryString = null; /* if ((agent.getTerritoryCode()
	 * == null) || (agent.getTerritoryCode().trim().equals(""))) { queryString =
	 * "select distinct cou.currency_code from T_AGENT ag, " + " T_COUNTRY cou, T_TERRITORY tr, T_STATION st where " +
	 * " ag.station_code = st.station_code " + " and st.territory_code = tr.territory_code " +
	 * " and tr.country_code = cou.country_code " + " and ag.station_code = '" + agent.getStationCode() + "'"; } else {
	 * queryString = "select  distinct cou.currency_code from T_AGENT ag," + " T_COUNTRY cou, T_TERRITORY tr " +
	 * " where ag.territory_code = tr.territory_code " + " and tr.country_code = cou.country_code " +
	 * " and ag.territory_code = '" + agent.getTerritoryCode() + "'"; }
	 * 
	 * 
	 * queryString = "select distinct cou.currency_code from T_STATION st, " +
	 * "T_COUNTRY cou where st.country_code = cou.country_code " + "and st.station_code = '" + agent.getStationCode() +
	 * "'";
	 * 
	 * DataSource ds = getDatasource(); JdbcTemplate templete = new JdbcTemplate(ds);
	 * 
	 * return (String) templete.query(queryString, new ResultSetExtractor() {
	 * 
	 * public Object extractData(ResultSet rs) throws SQLException, DataAccessException { String currency = null; if (rs
	 * != null) { while (rs.next()) { currency = rs.getString("currency_code"); } } return currency; } }); }
	 **/

	/**
	 * Method to get agent for given territory.
	 * 
	 * @param territoryCode
	 * @return the agent
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #getGSAForTerritory(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public Agent getGSAForTerritory(String territoryCode) {
		Agent agent = null;
		List<Agent> list = null;

		list = find("select agent from Agent" + " as agent where agent.status='ACT' "
				+ " and agent.agentTypeCode='GSA' and agent.territoryCode =?", territoryCode, Agent.class);

		if (list.size() != 0) {
			agent = (Agent) list.get(0);
		}

		return agent;
	}

	/**
	 * Method to get agent for given territory.
	 * 
	 * @param territoryCode
	 * @return the agent
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #getGSAForTerritory(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public Agent getGSAForTerritory(String territoryCode, String channel) {
		Agent agent = null;
		List<Agent> list = null;

		if (channel.equals(channelALL)) {
			list = find("select agent from Agent" + " as agent where agent.status='ACT' "
					+ " and agent.agentTypeCode='GSA' and agent.territoryCode =? and agent.serviceChannel in ('" + channel
					+ "','" + channelAA + "')", territoryCode, Agent.class);
		} else {
			list = find("select agent from Agent" + " as agent where agent.status='ACT' "
					+ " and agent.agentTypeCode='GSA' and agent.territoryCode =? and agent.serviceChannel in ('" + channel
					+ "','" + channelALL + "')", territoryCode, Agent.class);
		}

		if (list.size() != 0) {
			agent = (Agent) list.get(0);
		}

		return agent;
	}

	/**
	 * Method to get all active agent for given GSA Code.
	 * 
	 * @param gSACode
	 * @return the agent
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #getGSAForTerritory(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public Collection<Agent> getActiveAgentsForGSA(String gSACode) {
		Collection<Agent> agents = null;

		agents = find("select agent from Agent as agent " + "where agent.gsaCode = ?"
				+ "and agent.status='ACT' and agent.agentTypeCode != 'GSA' and agent.serviceChannel in ('" + channelAA + "','"
				+ channelALL + "')", gSACode, Agent.class);

		return agents;
	}

	/**
	 * Method to get all agent for given set of Agent codes.
	 * 
	 * @param agentCodes
	 * @return the agent
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO #getAgents(java.util.Collection)
	 */

	@SuppressWarnings("unchecked")
	public Collection<Agent> getAgents(Collection<String> agentCodes) {

		if (agentCodes == null || agentCodes.isEmpty()) {
			return new ArrayList<Agent>();
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM T_AGENT WHERE ");
		sql.append(agentCodeSqlGenarate(agentCodes));
		sql.append(" AND SERVICE_CHANNEL_CODE IN ('" + channelAA + "','" + channelALL + "')");

		Collection<Agent> agents = (Collection<Agent>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Agent> agents = new ArrayList<Agent>();
				Agent agent;

				if (rs != null) {
					while (rs.next()) {
						agent = new Agent();

						agent.setAgentCode(PlatformUtiltiies.nullHandler(rs.getString("AGENT_CODE")));
						agent.setAgentTypeCode(PlatformUtiltiies.nullHandler(rs.getString("AGENT_TYPE_CODE")));
						agent.setAgentName(PlatformUtiltiies.nullHandler(rs.getString("AGENT_NAME")));
						agent.setAirlineCode(PlatformUtiltiies.nullHandler(rs.getString("AIRLINE_CODE")));
						agent.setAddressLine1(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_LINE_1")));
						agent.setAddressLine2(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_LINE_2")));
						agent.setAddressCity(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_CITY")));
						agent.setAddressStateProvince(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_STATE_PROVINCE")));
						agent.setPostalCode(PlatformUtiltiies.nullHandler(rs.getString("POSTAL_CODE")));
						agent.setAccountCode(PlatformUtiltiies.nullHandler(rs.getString("ACCOUNT_CODE")));
						agent.setStationCode(PlatformUtiltiies.nullHandler(rs.getString("STATION_CODE")));
						agent.setTerritoryCode(PlatformUtiltiies.nullHandler(rs.getString("TERRITORY_CODE")));
						agent.setGsaCode(PlatformUtiltiies.nullHandler(rs.getString("GSA_CODE")));
						agent.setTelephone(PlatformUtiltiies.nullHandler(rs.getString("TELEPHONE")));

						agent.setFax(PlatformUtiltiies.nullHandler(rs.getString("FAX")));
						agent.setEmailId(PlatformUtiltiies.nullHandler(rs.getString("EMAIL_ID")));
						agent.setAgentIATANumber(PlatformUtiltiies.nullHandler(rs.getString("AGENT_IATA_NUMBER")));
						agent.setCreditLimit(rs.getBigDecimal("CREDIT_LIMIT"));
						agent.setTotalCreditLimit(rs.getBigDecimal("TOTAL_CREDIT_LIMIT"));
						agent.setBankGuaranteeCashAdvance(rs.getBigDecimal("BANK_GUARANTEE_CASH_ADVANCE"));
						agent.setStatus(PlatformUtiltiies.nullHandler(rs.getString("STATUS")));
						agent.setCreatedBy(PlatformUtiltiies.nullHandler(rs.getString("CREATED_BY")));
						agent.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
						agent.setModifiedBy(PlatformUtiltiies.nullHandler(rs.getString("MODIFIED_BY")));
						agent.setModifiedDate(rs.getTimestamp("MODIFIED_DATE"));
						agent.setVersion(rs.getLong("VERSION"));
						agent.setReportingToGSA(PlatformUtiltiies.nullHandler(rs.getString("REPORT_TO_GSA")));
						agent.setBillingEmail(PlatformUtiltiies.nullHandler(rs.getString("BILLING_EMAIL")));
						agent.setHandlingChargAmount(rs.getBigDecimal("HANDLING_CHARGE_AMOUNT"));
						agent.setCurrencyCode(PlatformUtiltiies.nullHandler(rs.getString("CURRENCY_CODE")));
						agent.setNotes(PlatformUtiltiies.nullHandler(rs.getString("NOTES")));
						agent.setServiceChannel(PlatformUtiltiies.nullHandler(rs.getString("SERVICE_CHANNEL_CODE")));
						agent.setEnableWSMultiCurrency(PlatformUtiltiies.nullHandler(rs.getString("SHOW_PRICE_IN_SELECTED_CUR")));
						agent.setEmailsToNotify(PlatformUtiltiies.nullHandler(rs.getString("EMAILS_TO_NOTIFY")));

						agent.setHandlingFeeAppliesTo(Integer.parseInt(rs.getString("HANDLING_CHG_APPLIES_TO")));
						agent.setHandlingFeeChargeBasisCode(rs.getString("HANDLING_CHG_BASIS_CODE"));
						agent.setHandlingFeeOnewayAmount(rs.getBigDecimal("HANDLING_CHG_OW_AMOUNT") != null ? rs
								.getBigDecimal("HANDLING_CHG_OW_AMOUNT") : null);
						agent.setHandlingFeeReturnAmount(rs.getBigDecimal("HANDLING_CHG_RT_AMOUNT") != null ? rs
								.getBigDecimal("HANDLING_CHG_RT_AMOUNT") : null);
						agent.setHandlingFeeMinAmount(rs.getString("HANDLING_CHG_MIN_AMOUNT") != null ? rs
								.getBigDecimal("HANDLING_CHG_MIN_AMOUNT") : null);
						agent.setHandlingFeeMaxAmount(rs.getString("HANDLING_CHG_MAX_AMOUNT") != null ? rs
								.getBigDecimal("HANDLING_CHG_MAX_AMOUNT") : null);
						agent.setHandlingFeeEnable(rs.getString("HANDLING_CHG_ENABLED"));
						agent.setHandlingFeeModificationEnabled(rs.getString("MOD_HANDLING_CHG_ENABLED"));
						agent.setHandlingFeeModificationApllieTo(Integer.parseInt(rs.getString("MOD_HANDLING_CHG_APPLIES_TO")));
						agent.setMaxAdultAllowed(rs.getString("MAX_ADULT_COUNT") != null ? rs.getInt("MAX_ADULT_COUNT") : null);
						
						agents.add(agent);
					}
				}

				return agents;
			}
		});
		return agents;
	}

	private String agentCodeSqlGenarate(Collection<String> agentCodes) {
		StringBuilder sqlBuilder = new StringBuilder();
		List<String> agentCList = new ArrayList<String>(agentCodes);

		while (!agentCList.isEmpty()) {
			if (agentCList.size() > ORACLE_MAX_IN_PARMS) {
				List<String> subList = agentCList.subList(0, ORACLE_MAX_IN_PARMS);
				sqlBuilder.append(" AGENT_CODE IN(" + Util.buildStringInClauseContent(subList) + ") OR ");
				agentCList = agentCList.subList(ORACLE_MAX_IN_PARMS, agentCList.size());
			} else {
				sqlBuilder.append(" AGENT_CODE IN(" + Util.buildStringInClauseContent(agentCList) + ") ");
				break;
			}
		}
		return sqlBuilder.toString();
	}

	public Map<String, Agent> getAgentsMap(Collection<String> agentCodes) {
		Collection<Agent> agents = null;

		if (agentCodes != null && !agentCodes.isEmpty()) {
			agents = getAgents(agentCodes);
		} else {
			agents = new ArrayList<Agent>();
		}

		Map<String, Agent> agentMap = new HashMap<String, Agent>();
		for (Iterator<Agent> iter = agents.iterator(); iter.hasNext();) {
			Agent agent = (Agent) iter.next();
			agentMap.put(agent.getAgentCode(), agent);
		}
		return agentMap;

	}

	@SuppressWarnings("unchecked")
	public Collection<Agent> getAgents(String agentTypeCode) {
		Collection<Agent> agents = null;

		agents = find("select agent from Agent as agent " + "where agent.agentTypeCode =? order by agent.agentTypeCode",
				agentTypeCode, Agent.class);

		return agents;
	}

	public BigDecimal getAgentAvailableCredit(String agentCode) throws ModuleException {
		String hql = "select agentSummary.availableCredit " + "from AgentSummary as agentSummary "
				+ "where agentSummary.agentCode = ?";
		Object availableCreditObj = getSession().createQuery(hql).setString(0, agentCode).uniqueResult();
		if (availableCreditObj == null) {
			throw new ModuleException("airtravelagent.logic.agent.notexists");
		}
		return (BigDecimal) availableCreditObj;
	}

	/**
	 * Returns the agent type details of a particular agent type code
	 * 
	 * @param agentTypeCode
	 * @return
	 */
	public AgentType getAgentType(String agentTypeCode) {

		List<AgentType> agenTypeList = null;
		AgentType agentType = null;

		agenTypeList = find("select agentType from AgentType as agentType where agentType.agentTypeCode = ?",
				new Object[] { agentTypeCode }, AgentType.class);

		if (agenTypeList.size() != 0) {
			agentType = agenTypeList.get(0);
		}
		return agentType;
	}

	/**
	 * Returns the agent type of a perticular agent by agent id
	 * 
	 * @param agentId
	 * @return
	 */
	public String getAgentTypeByAgentId(String agentId) {

		List<String> agenTypeList = null;
		String agentType = null;

		agenTypeList = find("select agent.agentTypeCode from Agent as agent" + " where agent.agentCode = ? ", agentId,
				String.class);

		if (agenTypeList.size() != 0) {
			agentType = agenTypeList.get(0);
		}
		return agentType;
	}

	/**
	 * Return All Agents
	 */
	public Collection<String> getAllAgents() {

		return find("select agent.agentCode from Agent as agent " + " where agent.serviceChannel in ('" + channelALL + "','"
				+ channelAA + "')", String.class);

	}

	/**
	 * Return All Own Airline Agents
	 */
	public Collection<String> getAllOwnAirlineAgents() {

		return find("select agent.agentCode from Agent as agent " + " where agent.serviceChannel in ('" + channelALL + "','"
				+ channelAA + "') and agent.airlineCode = '" + AppSysParamsUtil.getDefaultAirlineIdentifierCode() + "'",
				String.class);

	}

	/**
	 * Return All Agents
	 */
	/*
	 * public Map getAllAgentsMap() { Collection agents = getAllAgents(); Map agentMap = new HashMap(); for (Iterator
	 * iter = agents.iterator(); iter.hasNext();) { Agent agent = (Agent) iter.next();
	 * agentMap.put(agent.getAgentCode(), agent); } return agentMap; }
	 */

	/**
	 * Private method to get the Locatin BD.
	 * 
	 * @return LocationBD.
	 */
	private LocationBD getLocationBD() {
		return AirTravelagentsModuleUtils.getLocationBD();
	}

	/**
	 * Private method to get the Security BD.
	 * 
	 * @return SecurityBD.
	 */
	private SecurityBD getSecurityBD() {
		return AirTravelagentsModuleUtils.getSecurityBD();
	}

	/**
	 * Returns air reservation module data source
	 * 
	 * @return
	 */
	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	@SuppressWarnings("unchecked")
	public Collection<AgentTO> getAllAgentTOs() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String sql = " SELECT AGENT_CODE, AGENT_TYPE_CODE, AGENT_NAME, ADDRESS_LINE_1, "
				+ " ADDRESS_LINE_2, ADDRESS_CITY, ADDRESS_STATE_PROVINCE, "
				+ " POSTAL_CODE, ACCOUNT_CODE, STATION_CODE, TERRITORY_CODE, " + " TELEPHONE, FAX, EMAIL_ID, AGENT_IATA_NUMBER, "
				+ " CREDIT_LIMIT, BANK_GUARANTEE_CASH_ADVANCE, STATUS, "
				+ " CREATED_BY, CREATED_DATE, MODIFIED_BY, MODIFIED_DATE, " + " VERSION, REPORT_TO_GSA, BILLING_EMAIL, "
				+ " HANDLING_CHARGE_AMOUNT, CURRENCY_CODE, NOTES " + " FROM T_AGENT" + " WHERE SERVICE_CHANNEL_CODE IN ('"
				+ channelEX + "','" + channelALL + "') ORDER BY AGENT_CODE ";

		Collection<AgentTO> colAgentTO = (Collection<AgentTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<AgentTO> colAgentTO = new ArrayList<AgentTO>();
				AgentTO agentTO;

				if (rs != null) {
					while (rs.next()) {
						agentTO = new AgentTO();

						agentTO.setAgentCode(PlatformUtiltiies.nullHandler(rs.getString("AGENT_CODE")));
						agentTO.setAgentTypeCode(PlatformUtiltiies.nullHandler(rs.getString("AGENT_TYPE_CODE")));
						agentTO.setAgentName(PlatformUtiltiies.nullHandler(rs.getString("AGENT_NAME")));
						agentTO.setAddressLine1(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_LINE_1")));
						agentTO.setAddressLine2(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_LINE_2")));
						agentTO.setAddressCity(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_CITY")));
						agentTO.setAddressStateProvince(PlatformUtiltiies.nullHandler(rs.getString("ADDRESS_STATE_PROVINCE")));
						agentTO.setPostalCode(PlatformUtiltiies.nullHandler(rs.getString("POSTAL_CODE")));
						agentTO.setAccountCode(PlatformUtiltiies.nullHandler(rs.getString("ACCOUNT_CODE")));
						agentTO.setStationCode(PlatformUtiltiies.nullHandler(rs.getString("STATION_CODE")));
						agentTO.setTerritoryCode(PlatformUtiltiies.nullHandler(rs.getString("TERRITORY_CODE")));
						agentTO.setTelephone(PlatformUtiltiies.nullHandler(rs.getString("TELEPHONE")));

						agentTO.setFax(PlatformUtiltiies.nullHandler(rs.getString("FAX")));
						agentTO.setEmailId(PlatformUtiltiies.nullHandler(rs.getString("EMAIL_ID")));
						agentTO.setAgentIATANumber(PlatformUtiltiies.nullHandler(rs.getString("AGENT_IATA_NUMBER")));
						agentTO.setCreditLimit(rs.getBigDecimal("CREDIT_LIMIT"));
						agentTO.setBankGuaranteeCashAdvance(rs.getBigDecimal("BANK_GUARANTEE_CASH_ADVANCE"));
						agentTO.setStatus(PlatformUtiltiies.nullHandler(rs.getString("STATUS")));
						agentTO.setCreatedBy(PlatformUtiltiies.nullHandler(rs.getString("CREATED_BY")));
						agentTO.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
						agentTO.setModifiedBy(PlatformUtiltiies.nullHandler(rs.getString("MODIFIED_BY")));
						agentTO.setModifiedDate(rs.getTimestamp("MODIFIED_DATE"));
						agentTO.setVersion(rs.getLong("VERSION"));
						agentTO.setReportingToGSA(PlatformUtiltiies.nullHandler(rs.getString("REPORT_TO_GSA")));
						agentTO.setBillingEmail(PlatformUtiltiies.nullHandler(rs.getString("BILLING_EMAIL")));
						agentTO.setHandlingChargAmount(rs.getBigDecimal("HANDLING_CHARGE_AMOUNT"));
						agentTO.setCurrencyCode(PlatformUtiltiies.nullHandler(rs.getString("CURRENCY_CODE")));
						agentTO.setNotes(PlatformUtiltiies.nullHandler(rs.getString("NOTES")));

						colAgentTO.add(agentTO);
					}
				}

				return colAgentTO;
			}
		});

		return colAgentTO;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getAgentCountryCodes(Collection<String> stationCodes) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String sql = " SELECT COUNTRY_CODE, STATION_CODE FROM T_STATION WHERE STATION_CODE IN ("
				+ Util.buildStringInClauseContent(stationCodes) + " )";

		Map<String, String> mapStationCodeAndCountryCode = (Map<String, String>) jdbcTemplate.query(sql,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, String> mapStationCodeAndCountryCode = new HashMap<String, String>();
						String countryCode;
						String stationCode;

						if (rs != null) {
							while (rs.next()) {
								stationCode = PlatformUtiltiies.nullHandler(rs.getString("STATION_CODE"));
								countryCode = PlatformUtiltiies.nullHandler(rs.getString("COUNTRY_CODE"));

								mapStationCodeAndCountryCode.put(stationCode, countryCode);
							}
						}

						return mapStationCodeAndCountryCode;
					}
				});

		return mapStationCodeAndCountryCode;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Collection<String>> getAgentIds(Collection<String> countryCodes) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String sql = " SELECT A.AGENT_CODE, S.COUNTRY_CODE FROM T_AGENT A, T_STATION S "
				+ " WHERE A.SERVICE_CHANNEL_CODE IN = ('" + channelALL + "','" + channelEX
				+ "') AND A.STATION_CODE = S.STATION_CODE AND S.COUNTRY_CODE IN ("
				+ Util.buildStringInClauseContent(countryCodes) + ") ";

		Map<String, Collection<String>> mapCountryCodeAgentCodes = (Map<String, Collection<String>>) jdbcTemplate.query(sql,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, Collection<String>> mapCountryCodeAgentCodes = new HashMap<String, Collection<String>>();
						Collection<String> colAgentCodes;
						String countryCode;
						String agentCode;

						if (rs != null) {
							while (rs.next()) {
								countryCode = PlatformUtiltiies.nullHandler(rs.getString("COUNTRY_CODE"));
								agentCode = PlatformUtiltiies.nullHandler(rs.getString("AGENT_CODE"));

								if (mapCountryCodeAgentCodes.containsKey(countryCode)) {
									colAgentCodes = (Collection<String>) mapCountryCodeAgentCodes.get(countryCode);
									colAgentCodes.add(agentCode);
								} else {
									colAgentCodes = new HashSet<String>();
									colAgentCodes.add(agentCode);

									mapCountryCodeAgentCodes.put(countryCode, colAgentCodes);
								}
							}
						}

						return mapCountryCodeAgentCodes;
					}
				});

		return mapCountryCodeAgentCodes;
	}

	public void addAgentCreditHistory(String agentCode, BigDecimal newCreditLimit, BigDecimal newCreditLimitLocal,
			String specifiedCur, String remarks, String loginId) {
		Calendar cal = Calendar.getInstance();

		AgentCreditHistory agentCreditHistory = new AgentCreditHistory();

		agentCreditHistory.setAgentCode(agentCode);
		agentCreditHistory.setCreditLimitamount(newCreditLimit);
		agentCreditHistory.setCreditLimitamountLocal(newCreditLimitLocal);
		agentCreditHistory.setRemarks(remarks);
		agentCreditHistory.setSpecifiedCurrency(specifiedCur);
		agentCreditHistory.setCreatedBy(loginId);
		agentCreditHistory.setUpdateDate(cal.getTime());

		super.hibernateSaveOrUpdate(agentCreditHistory);
	}

	public void addAgentCreditHistory(Collection<AgentCreditHistory> colAgentHistory) {
		super.hibernateSaveOrUpdateAll(colAgentHistory);
	}

	public void saveOrUpdate(Agent agent) {
		super.hibernateSaveOrUpdate(agent);
	}

	public void deleteAgentApplicableOND(Set<AgentApplicableOND> applicableOND) {
		super.deleteAll(applicableOND);
	}

	public void deleteAgentHandlingFeeModList(Set<AgentHandlingFeeModification> agentHandlingFeeModList){
		super.deleteAll(agentHandlingFeeModList);
	}
	
	public void setAgentToken(AgentToken agentToken) {
		super.hibernateSaveOrUpdate(agentToken);
	}

	public AgentToken getAgentToken(String token) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "SELECT A.AGENT_TOKEN_ID, A.TOKEN, A.AGENT_CODE, A.USER_NAME, A.TOKEN_ISSUE_TIMESTAMP, A.TOKEN_EXPIRY_TIMESTAMP, A.IS_EXPIRED FROM T_AGENT_TOKEN A WHERE A.TOKEN = '"
				+ token + "'";

		@SuppressWarnings("unchecked")
		AgentToken agentToken = (AgentToken) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				AgentToken agentToken = new AgentToken();
				while (rs.next()) {
					int tokenID = -1;
					String token = "";
					String agentCode = "";
					String userName = "";
					Date tokenIssueTimeStamp = null;
					Date tokenExpiryTimeStamp = null;
					String isExpired = "";

					if (rs != null) {
						tokenID = rs.getInt("AGENT_TOKEN_ID");
						token = PlatformUtiltiies.nullHandler(rs.getString("TOKEN"));
						agentCode = PlatformUtiltiies.nullHandler(rs.getString("AGENT_CODE"));
						userName = PlatformUtiltiies.nullHandler(rs.getString("USER_NAME"));
						rs.getString("TOKEN_EXPIRY_TIMESTAMP");
						tokenIssueTimeStamp = rs.getTimestamp("TOKEN_ISSUE_TIMESTAMP");
						tokenExpiryTimeStamp = rs.getTimestamp("TOKEN_EXPIRY_TIMESTAMP");
						isExpired = PlatformUtiltiies.nullHandler(rs.getString("IS_EXPIRED"));
					}

					agentToken.setTokenId(tokenID);
					agentToken.setToken(token);
					agentToken.setAgentCode(agentCode);
					agentToken.setUserName(userName);
					agentToken.setTokenIssueTimeStamp(tokenIssueTimeStamp);
					agentToken.setTokenExpiryTimeStamp(tokenExpiryTimeStamp);
					agentToken.setExpired(isExpired);

				}
				return agentToken;
			}
		});

		return agentToken;
	}

	public void expireAgentToken(int tokenID) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "UPDATE T_AGENT_TOKEN A SET A.IS_EXPIRED='Y' WHERE A.AGENT_TOKEN_ID='" + tokenID + "'";
		jdbcTemplate.update(sql);
	}

	public List<AgentTokenTo> getExpiredAgentTokens() throws ModuleException {
		List<AgentTokenTo> expiredAgentTokens;

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "SELECT A.AGENT_TOKEN_ID, A.TOKEN FROM T_AGENT_TOKEN A WHERE A.TOKEN_EXPIRY_TIMESTAMP < SYSDATE OR IS_EXPIRED = 'Y'";

		expiredAgentTokens = (List<AgentTokenTo>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AgentTokenTo> expiredAgentTokens = new ArrayList<AgentTokenTo>();
				AgentTokenTo agentTokenTo;
				while (rs.next()) {
					agentTokenTo = new AgentTokenTo();
					agentTokenTo.setTokenId(rs.getInt("AGENT_TOKEN_ID"));
					agentTokenTo.setToken(rs.getString("TOKEN"));
					expiredAgentTokens.add(agentTokenTo);
				}
				return expiredAgentTokens;
			}
		});

		return expiredAgentTokens;
	}

	public void removeAgentTokens(List<Integer> agentTokenIds) throws ModuleException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		List<Integer> agentTokenIdsPaged;
		String sql;

		int pageSize = 999;
		int endIndex;
		for (int a = 0; a <= agentTokenIds.size() / pageSize; a++) {

			endIndex = agentTokenIds.size() <= (a + 1) * pageSize ? agentTokenIds.size() : ((a + 1) * pageSize);
			agentTokenIdsPaged = agentTokenIds.subList(a * pageSize, endIndex);

			if (!agentTokenIdsPaged.isEmpty()) {
				sql = "DELETE T_AGENT_TOKEN WHERE AGENT_TOKEN_ID IN (" + Util.buildIntegerInClauseContent(agentTokenIdsPaged)
						+ ")";
				jdbcTemplate.execute(sql);
			}
		}

	}

	public void removeAgentToken(int agentTokenId) throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "DELETE T_AGENT_TOKEN WHERE AGENT_TOKEN_ID = " + agentTokenId + "";
		jdbcTemplate.execute(sql);
	}

	public void saveOrUpdate(Collection<Agent> agents) {
		super.hibernateSaveOrUpdateAll(agents);
		if (log.isDebugEnabled()) {
			log.debug("Finished saveOrUpdate");
		}
	}

	public void saveOrUpdateAgentSummary(Collection<AgentSummary> agentSummary) {
		super.hibernateSaveOrUpdateAll(agentSummary);
		if (log.isDebugEnabled()) {
			log.debug("Finished saveOrUpdateAgentSummary");
		}
	}

	public Collection<Agent> getAgentswithLocalCurrency(String currencyCode) {
		Collection<Agent> agentsCollection = find("from Agent as agent where agent.currencyCode = '" + currencyCode + "' ",
				Agent.class);
		return agentsCollection;
	}

	@SuppressWarnings("unchecked")
	public Collection<Agent> getAgentsCol(Collection<String> colAgentCodes) {
		StringBuilder sql = new StringBuilder();
		List<String> agentCodeList = new ArrayList<String>(colAgentCodes);

		while (!agentCodeList.isEmpty()) {
			if (agentCodeList.size() > ORACLE_MAX_IN_PARMS) {
				List<String> subList = agentCodeList.subList(0, ORACLE_MAX_IN_PARMS);
				sql.append(" agent.agentCode IN(" + Util.buildStringInClauseContent(subList) + ") OR ");
				agentCodeList = agentCodeList.subList(ORACLE_MAX_IN_PARMS, agentCodeList.size());
			} else {
				sql.append(" agent.agentCode IN(" + Util.buildStringInClauseContent(agentCodeList) + ") ");
				break;
			}
		}

		String sqlStatment = sql.toString();

		if (sqlStatment.length() > 0) {
			return find("select agent from Agent as agent where " + sqlStatment + " order by agent.agentCode", Agent.class);
		} else {
			return new ArrayList<Agent>();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public Collection<AgentInfoDTO> getAgentsForLocationAndType(final String country, final String city, String type) {
		ArrayList<AgentInfoDTO> agentsCollection = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuffer sql = new StringBuffer();

		sql.append("SELECT DISTINCT(A.AGENT_CODE), A.AGENT_TYPE_CODE, A.AGENT_NAME, A.ADDRESS_LINE_1, "
				+ "A.ADDRESS_LINE_2, A.ADDRESS_CITY, A.ADDRESS_STATE_PROVINCE, "
				+ "A.POSTAL_CODE, A.STATION_CODE,A.TELEPHONE, A.FAX, A.EMAIL_ID, "
				+ "A.STATUS,A.VERSION, A.BILLING_EMAIL,A.CURRENCY_CODE, T.AGENT_TYPE_DESCRIPTION, " + " case A.AGENT_TYPE_CODE "
				+ "when 'SO' then  A.LOCATION_URL " + "when 'GSA' then A.LOCATION_URL " + "end  as LOCATION_URL "
				+ "FROM  T_AGENT A, T_STATION S, T_AGENT_TYPE T WHERE  A.STATUS='ACT' AND A.IS_LCC_INTEGRATION_AGENT<>'Y' "
				+ "AND A.WEBSITE_VISIBILITY='Y' ");

		if (type != null && !"".equals(type)) {
			if (type.equalsIgnoreCase("TA")) {
				sql.append("AND A.AGENT_TYPE_CODE='" + type + "' ");
			} else if (type.equalsIgnoreCase("GSASO")) {
				String strtype = "('GSA','SO')";
				sql.append("AND A.AGENT_TYPE_CODE IN " + strtype);
			} else {
				sql.append(" AND A.AGENT_TYPE_CODE<>'CO' ");
			}
		}

		boolean setCity = false;

		sql.append(" AND A.STATION_CODE = S.STATION_CODE ");
		if (city != null && !"".equals(city) && !"ALL".equals(city)) {
			sql.append("AND A.STATION_CODE= ? ");
			setCity = true;
		}
		sql.append("AND S.COUNTRY_CODE = ? AND A.AGENT_TYPE_CODE = T.AGENT_TYPE_CODE ORDER BY A.AGENT_NAME");

		final boolean queryWithCity = setCity;

		PreparedStatementSetter preparedStatementSetter = new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement arg0) throws SQLException {
				int index = 0;
				if (queryWithCity) {
					arg0.setString(++index, city);
				}
				arg0.setString(++index, country);

			}
		};

		agentsCollection = (ArrayList<AgentInfoDTO>) jdbcTemplate.query(sql.toString(), preparedStatementSetter,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						ArrayList<AgentInfoDTO> agentsList = new ArrayList<AgentInfoDTO>();
						if (rs != null) {
							while (rs.next()) {
								AgentInfoDTO agent = new AgentInfoDTO();
								agent.setAgentCode(rs.getString("AGENT_CODE"));
								agent.setAddressCity(rs.getString("ADDRESS_CITY"));
								agent.setAddressLine1(rs.getString("ADDRESS_LINE_1"));
								agent.setAddressLine2(rs.getString("ADDRESS_LINE_2"));
								agent.setAddressStateProvince(rs.getString("ADDRESS_STATE_PROVINCE"));
								agent.setPostalCode(rs.getString("POSTAL_CODE"));
								agent.setAgentName(rs.getString("AGENT_NAME"));
								agent.setAgentTypeCode(rs.getString("AGENT_TYPE_CODE"));
								agent.setTelephone(rs.getString("TELEPHONE"));
								agent.setBillingEmail(rs.getString("BILLING_EMAIL"));
								agent.setStationCode(rs.getString("STATION_CODE"));
								agent.setLocationUrl(rs.getString("LOCATION_URL"));
								agent.setEmailId(rs.getString("EMAIL_ID"));
								agent.setFax(rs.getString("FAX"));
								agent.setCurrencyCode(rs.getString("CURRENCY_CODE"));
								agent.setStatus(rs.getString("STATUS"));
								agent.setVersion(rs.getLong("VERSION"));
								agent.setAgentTypeDesc(rs.getString("AGENT_TYPE_DESCRIPTION"));
								agentsList.add(agent);

							}
							return agentsList;
						} else
							return null;

					}
				});

		return agentsCollection;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, List<AgentInfoDTO>> getAgentsForCountries(List<String> countryCodes) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT(A.AGENT_CODE), A.AGENT_TYPE_CODE, A.AGENT_NAME, A.ADDRESS_LINE_1, ");
		sql.append("A.ADDRESS_LINE_2, A.ADDRESS_CITY, A.ADDRESS_STATE_PROVINCE, ");
		sql.append("A.POSTAL_CODE, A.STATION_CODE,A.TELEPHONE, A.FAX, A.EMAIL_ID, ");
		sql.append("A.STATUS, A.VERSION, A.BILLING_EMAIL, A.CURRENCY_CODE, A.LOCATION_URL, ");
		sql.append("S.COUNTRY_CODE, S.STATION_NAME, A.REPORT_TO_GSA ");
		sql.append("FROM  T_AGENT A, T_STATION S ");
		sql.append("WHERE A.AGENT_TYPE_CODE IN ('GSA','TA', 'SO') ");
		sql.append("AND IS_LCC_INTEGRATION_AGENT = 'N' ");
		sql.append("AND A.STATION_CODE = S.STATION_CODE ");
		sql.append("AND S.COUNTRY_CODE IN (" + Util.buildStringInClauseContent(countryCodes) + ") ");
		sql.append("ORDER BY A.AGENT_NAME");

		HashMap<String, List<AgentInfoDTO>> agentInfoMap = (HashMap<String, List<AgentInfoDTO>>) jdbcTemplate.query(
				sql.toString(), new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						HashMap<String, List<AgentInfoDTO>> agentInfoMap = new HashMap<String, List<AgentInfoDTO>>();
						if (rs != null) {
							while (rs.next()) {

								String countryCode = rs.getString("COUNTRY_CODE");
								List<AgentInfoDTO> agentInfoList = agentInfoMap.get(countryCode);
								if (agentInfoList == null) {
									agentInfoList = new ArrayList<AgentInfoDTO>();
									agentInfoMap.put(countryCode, agentInfoList);
								}

								AgentInfoDTO agent = new AgentInfoDTO();
								agent.setAgentCode(rs.getString("AGENT_CODE"));
								agent.setAddressCity(rs.getString("ADDRESS_CITY"));
								agent.setAddressLine1(rs.getString("ADDRESS_LINE_1"));
								agent.setAddressLine2(rs.getString("ADDRESS_LINE_2"));
								agent.setAddressStateProvince(rs.getString("ADDRESS_STATE_PROVINCE"));
								agent.setPostalCode(rs.getString("POSTAL_CODE"));
								agent.setAgentName(rs.getString("AGENT_NAME"));
								agent.setAgentTypeCode(rs.getString("AGENT_TYPE_CODE"));
								agent.setTelephone(rs.getString("TELEPHONE"));
								agent.setBillingEmail(rs.getString("BILLING_EMAIL"));
								agent.setStationCode(rs.getString("STATION_CODE"));
								agent.setStationName(rs.getString("STATION_NAME"));
								agent.setLocationUrl(rs.getString("LOCATION_URL"));
								agent.setEmailId(rs.getString("EMAIL_ID"));
								agent.setFax(rs.getString("FAX"));
								agent.setCurrencyCode(rs.getString("CURRENCY_CODE"));
								agent.setStatus(rs.getString("STATUS"));
								agent.setVersion(rs.getLong("VERSION"));
								agent.setReportToGSA(rs.getString("REPORT_TO_GSA"));
								agentInfoList.add(agent);

							}
						}
						return agentInfoMap;
					}
				});

		return agentInfoMap;
	}

	public BigDecimal getUninvocedTotal(String agentCode, Date startDate, Date endDate) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("select sum(tx.amount) as uia from t_agent_transaction tx where tx.nominal_code in (1, 7, 8) ");
		sql.append("and tx.tnx_date > ? and tx.tnx_date < ? and tx.agent_code = ? ");
		sql.append("and tx.tnx_date >  (select NVL(max(invoice_period_to),'01-JAN-2006') from t_invoice where agent_code = ?)");
		Object[] params = { startDate, endDate, agentCode, agentCode };

		BigDecimal totalUninviced = (BigDecimal) jdbcTemplate.query(sql.toString(), params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BigDecimal uia = null;
				if (rs != null) {
					while (rs.next()) {
						uia = rs.getBigDecimal("uia");
					}
				}
				if (uia == null)
					uia = AccelAeroCalculator.getDefaultBigDecimalZero();
				return uia;
			}
		});

		return totalUninviced;
	}

	/**
	 * 
	 * @param agentCode
	 * @return
	 */
	public List<AgentTicketStock> getAgentSTock(String agentCode) {
		return find("select agentTicketStock from " + "AgentTicketStock as agentTicketStock "
				+ "where agentTicketStock.agentCode=? order by agentTicketStock.addedDate", agentCode, AgentTicketStock.class);
	}

	/**
	 * Save or Update
	 * 
	 * @param agentSummary
	 */
	public void saveOrUpdateAgentStock(AgentTicketStock agentTicketStock) {
		super.hibernateSaveOrUpdate(agentTicketStock);
	}

	@Override
	public boolean isValidAgentTicketSequnceRange(String agentCode, long lowerBound, long upperBound, long tktSequenceId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT agent_code ");
		sql.append("  FROM t_agent_ticket_sequence ");
		sql.append("  WHERE ((sequence_min < " + lowerBound + " ");
		sql.append(" AND sequence_max      > " + upperBound + " ) ");
		sql.append(" OR (sequence_min     >=" + lowerBound + "");
		sql.append(" AND sequence_min     <=" + upperBound + " )");
		sql.append(" OR (sequence_max     >=" + lowerBound + "");
		sql.append(" AND sequence_max     <=" + upperBound + " ) )");
		if (tktSequenceId > -1) {
			sql.append(" AND AGENT_TICKET_SEQUENCE_ID     <> " + tktSequenceId);
		}

		Boolean valid = (Boolean) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				boolean valid = true;
				if (rs != null) {
					while (rs.next()) {
						return false;
					}
				}
				return valid;
			}
		});

		return valid;
	}

	@Override
	public void createAgentTicketSequnceRange(String sequenceName, long lowerBound, long upperBound, boolean cycle) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("CREATE SEQUENCE " + sequenceName + " INCREMENT BY 1 MAXVALUE " + upperBound + " MINVALUE " + (lowerBound - 1));
		if (cycle) {
			sql.append(" CYCLE ");
		}
		sql.append(" NOCACHE ORDER");
		jdbcTemplate.execute(sql.toString());

	}

	@Override
	public void dropAgentTicketSequnceRange(String sequenceName) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("DROP SEQUENCE " + sequenceName);

		jdbcTemplate.execute(sql.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page getAgentTicketStocks(String agentCode, int startIndex, int pageSize) throws ModuleException {

		Collection<AgentTicketSequnce> agentTicketStockCollection = null;
		Collection<AgentTicketStockTO> agentTicketStockToCollection = new ArrayList<AgentTicketStockTO>();

		Page page = null;
		int recSize;
		Query query = null;

		query = getSession().createQuery(
				"select ats from AgentTicketSequnce ats where ats.agentCode='" + agentCode
						+ "' order by ats.agentTicketSequnceId ");

		recSize = query.list().size();

		agentTicketStockCollection = query.setFirstResult(startIndex).setMaxResults(pageSize).list();
		Iterator<AgentTicketSequnce> agentSeqIterator = agentTicketStockCollection.iterator();
		while (agentSeqIterator.hasNext()) {
			AgentTicketSequnce agentTicketSequnce = agentSeqIterator.next();

			long stockSize = agentTicketSequnce.getSequnceUpperBound() - agentTicketSequnce.getSequnceLowerBound();

			AgentTicketStockTO agentTktStock = new AgentTicketStockTO();

			agentTktStock.setAgentTicketSeqId(agentTicketSequnce.getAgentTicketSequnceId());
			agentTktStock.setAgentCode(agentTicketSequnce.getAgentCode());
			agentTktStock.setSequenceLowerBound(agentTicketSequnce.getSequnceLowerBound());
			agentTktStock.setSequenceUpperBound(agentTicketSequnce.getSequnceUpperBound());
			agentTktStock.setSequenceName(agentTicketSequnce.getSequnceName());
			agentTktStock.setCycleSequence(agentTicketSequnce.getCycle());
			agentTktStock.setStatus(agentTicketSequnce.getStatus());
			agentTktStock.setStockSize(stockSize);
			agentTktStock.setVersion(agentTicketSequnce.getVersion());

			agentTicketStockToCollection.add(agentTktStock);

		}

		page = new Page(recSize, startIndex, startIndex + pageSize, agentTicketStockToCollection);

		return page;
	}

	@Override
	public Long saveAgentTicketStock(AgentTicketSequnce agentTicketSequnce) throws ModuleException {

		if (agentTicketSequnce.getAgentTicketSequnceId() == null) {
			return (Long) hibernateSave(agentTicketSequnce);
		} else {
			hibernateSaveOrUpdate(agentTicketSequnce);
			return agentTicketSequnce.getAgentTicketSequnceId();
		}
	}

	@Override
	public Collection<AgentTicketSequnce> getAgentTicketStocks(String agentCode) throws ModuleException {
		return find("select ats from AgentTicketSequnce ats where ats.agentCode=? order by ats.agentTicketSequnceId ", agentCode,
				AgentTicketSequnce.class);
	}

	@Override
	public AgentTicketSequnce getAgentTicketStock(long agentTicketSequnceId) throws ModuleException {
		List<AgentTicketSequnce> agentTktStokcs = find(
				"select ats from AgentTicketSequnce ats where ats.agentTicketSequnceId=? ", agentTicketSequnceId,
				AgentTicketSequnce.class);
		if (!agentTktStokcs.isEmpty()) {
			return agentTktStokcs.get(0);
		}
		return null;
	}

	@Override
	public void updateAgentTicketStockUpperBound(String sequenceName, long upperBound) throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("ALTER SEQUENCE " + sequenceName + " MAXVALUE " + upperBound);

		jdbcTemplate.execute(sql.toString());

	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getAgentTransationNominalCodes() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "SELECT nominal_code, description FROM t_agent_transaction_nc";

		Map<String, String> mapAgentTransationNominalCodes = (Map<String, String>) jdbcTemplate.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, String> mapAgentTransationNominalCodes = new HashMap<String, String>();

						if (rs != null) {
							while (rs.next()) {
								mapAgentTransationNominalCodes.put(rs.getString("nominal_code"), rs.getString("description"));
							}
						}

						return mapAgentTransationNominalCodes;
					}
				});
		return mapAgentTransationNominalCodes;
	}

	public List<String> getGSAAirports(String territoryCode) throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "select airport_code from t_airport where station_code in (select station_code from t_station where territory_code='"
				+ territoryCode + "' union select station_code from t_agent where territory_code='" + territoryCode + "')";

		@SuppressWarnings("unchecked")
		List<String> airports = (List<String>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> airports = new ArrayList<String>();

				if (rs != null) {
					while (rs.next()) {
						airports.add(rs.getString("airport_code"));
					}
				}

				return airports;
			}
		});
		return airports;
	}

	public void deactivateTravelAgents() {
		String sqlUpdate = "update t_agent set status = 'INA', inactive_from_date = null where inactive_from_date < to_date("
				+ CalendarUtil.formatForSQL_toDate(CalendarUtil.getCurrentZuluDateTime()) + ")";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(InvoicingModuleUtils.getDatasource());
		jdbcTemplate.execute(sqlUpdate);
	}

	public List<Agent> getLCCUnpublishedAgents() {
		String airlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
		return find("from Agent agent where agent.lccPublishStatus in ('" + Util.LCC_TOBE_PUBLISHED + "','"
				+ Util.LCC_TOBE_REPUBLISHED + "') and agent.agentCode like '" + airlineCode + "%'", Agent.class);
	}

	@Override
	public void updateAgentVersion(String agentCode, long version) {
		String hql = "UPDATE Agent SET version=:version WHERE agentCode=:agentCode";
		Query qry = getSession().createQuery(hql).setParameter("version", version).setParameter("agentCode", agentCode);
		qry.executeUpdate();
	}

	@Override
	public void updateAgentPublishStatus(String agentCode, String lccPublishStatus) {
		String hql = "UPDATE Agent SET lccPublishStatus=:lccPublishStatus WHERE agentCode=:agentCode";
		Query qry = getSession().createQuery(hql).setParameter("lccPublishStatus", lccPublishStatus)
				.setParameter("agentCode", agentCode);
		qry.executeUpdate();
	}

	@Override
	@SuppressWarnings("unchecked")
	public AgentAlertConfig getAgentAlertConfig(String paymentMethod) throws ModuleException {
		String hql = "FROM AgentAlertConfig alertConfig WHERE alertConfig.paymentMethod =:paymentMethod";
		Query qry = getSession().createQuery(hql).setParameter("paymentMethod", paymentMethod);
		List<AgentAlertConfig> agentAlertConfigs = (List<AgentAlertConfig>) qry.list();

		return (agentAlertConfigs != null) ? agentAlertConfigs.get(0) : null;
	}

	@Override
	public AgentToken getAgentTokenById(String token) throws ModuleException {
		List<AgentToken> agentTokenList = null;

		agentTokenList = find("select agentToken from AgentToken as agentToken"
				+ " where agentToken.token = ? and agentToken.expired = '" + AgentToken.FALSE + "'", token, AgentToken.class);
		AgentToken agentToken = null;
		if (agentTokenList.size() > 0) {
			agentToken = agentTokenList.get(0);
			agentToken.getAgent().toString();
		}
		return agentToken;
	}

	@Override
	public String getCurrencyCodeByStationCode(String agentStation) throws ModuleException {
		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT DISTINCT TCR.CURRENCY_CODE ");
		sql.append(" FROM T_AIRPORT TA,T_STATION TS,T_COUNTRY TC, T_CURRENCY TCR ");
		sql.append(" WHERE  TS.STATION_CODE = '");
		sql.append(agentStation);
		sql.append("' AND TS.COUNTRY_CODE = TC.COUNTRY_CODE ");
		sql.append(" AND TC.CURRENCY_CODE = TCR.CURRENCY_CODE ");
		sql.append(" AND TCR.STATUS = 'ACT' ");

		String res = null;
		try {
			res = (String) jt.queryForObject(sql.toString(), String.class);
		} catch (IncorrectResultSizeDataAccessException e) {
			// Currency code might be an inactive one so we return null in that
			// case
		}
		return res;
	}

	public Serializable saveAgentCreditBlock(AgentCreditBlock agentCreditBlock) throws ModuleException {
		return super.hibernateSave(agentCreditBlock);
	}

	public AgentCreditBlock getAgentCreditBlock(Integer blockId, String status) throws ModuleException {
		List<AgentCreditBlock> agentCreditBlockList = null;

		if (status != null) {
			agentCreditBlockList = find(
					"select agentCreditBlock from AgentCreditBlock as agentCreditBlock where agentCreditBlock.agentCreditBlockId = ? and agentCreditBlock.status = ? ",
					new Object[] { blockId, status }, AgentCreditBlock.class);
		} else {
			agentCreditBlockList = find("select agentCreditBlock from AgentCreditBlock as agentCreditBlock"
					+ " where agentCreditBlock.agentCreditBlockId = ? ", blockId, AgentCreditBlock.class);
		}

		AgentCreditBlock agentCreditBlock = null;
		if (agentCreditBlockList.size() > 0) {
			agentCreditBlock = agentCreditBlockList.get(0);
		}
		return agentCreditBlock;

	}

	public void updateAgentCreditBlock(AgentCreditBlock agentCreditBlock) throws ModuleException {
		super.hibernateSaveOrUpdate(agentCreditBlock);
	}

	public List<String> getSubAgentTypesByAgentCode(String agentCode) throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT SA.SUB_AGENT_TYPE_CODE AS SUB_AGENT_TYPE_CODE FROM T_SUB_AGENT_TYPE SA, T_AGENT AG ");
		sql.append("WHERE AG.AGENT_TYPE_CODE=SA.AGENT_TYPE_CODE AND AG.AGENT_CODE ='" + agentCode + "'");

		@SuppressWarnings("unchecked")
		List<String> subAgentTypes = (List<String>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> subAgentTypes = new ArrayList<String>();

				if (rs != null) {
					while (rs.next()) {
						subAgentTypes.add(rs.getString("SUB_AGENT_TYPE_CODE"));
					}
				}

				return subAgentTypes;
			}
		});
		return subAgentTypes;
	}

	@Override
	public List<String> getAgentWiseApplicableRoutes(String agentCode) throws ModuleException {
		List<String> result = find("select ondCOde from AgentApplicableOND agentApplicableOND"
				+ " where agentApplicableOND.agent.agentCode = ? ", agentCode, String.class);

		return result;
	}

	@SuppressWarnings("unchecked")
	public Page getActiveAgents(AdminFeeAgentSearchCriteria criteria, int rownum, int pageSize) {
		if (log.isDebugEnabled()) {
			log.debug("getActiveAgents(AdminFeeAgentSearchCriteria criteria()");
		}
		String[] s = getActiveAgentHql(criteria);
		String hql = s[0];
		String hqlCount = s[1];

		if (log.isDebugEnabled()) {
			log.debug("GET ACTIVE AGENT: HQL : " + hql);
			log.debug("GET ACTIVE AGENT: HQLCOUNT : " + hqlCount);
		}

		Query mainQuery = getSession().createQuery(hql).setFirstResult(rownum).setMaxResults(pageSize);
		Query countQuery = getSession().createQuery(hqlCount);

		if (criteria.getAgentCode() != null) {
			mainQuery.setParameter("agentcode", "%" + criteria.getAgentCode().toLowerCase().trim() + "%");
			countQuery.setParameter("agentcode", "%" + criteria.getAgentCode().toLowerCase().trim() + "%");
		}

		List l = mainQuery.list();
		int count = ((Long) countQuery.uniqueResult()).intValue();

		return new Page(count, rownum, rownum + pageSize, l);
	}

	@Override
	public void saveAdminFeeAgent(List agentCodesAddList, List agentCodesDeleteList) throws ModuleException {
		if (log.isDebugEnabled()) {
			// log.debug("adminfeeagent :" + );
		}

		if (agentCodesAddList != null && !agentCodesAddList.isEmpty()) {
			addAdminFeeAgents(agentCodesAddList);
		}

		if (agentCodesDeleteList != null && !agentCodesDeleteList.isEmpty()) {
			removeAdminFeeAgents(agentCodesDeleteList);
		}

		// hibernateSaveOrUpdate(adminFeeAgent);
	}

	private void removeAdminFeeAgents(List<String> adminFeeAgents) throws ModuleException {

		if (adminFeeAgents != null && !adminFeeAgents.isEmpty()) {

			adminFeeAgents = getEscapeSqlForAgent(adminFeeAgents);

			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

			List<String> adminFeeAgentsPaged;
			String sql;

			int pageSize = 999;
			int endIndex;
			for (int a = 0; a <= adminFeeAgents.size() / pageSize; a++) {

				endIndex = adminFeeAgents.size() <= (a + 1) * pageSize ? adminFeeAgents.size() : ((a + 1) * pageSize);
				adminFeeAgentsPaged = adminFeeAgents.subList(a * pageSize, endIndex);

				if (!adminFeeAgentsPaged.isEmpty()) {
					sql = "DELETE T_ADMIN_FEE_AGENT WHERE AGENT_CODE IN (" + Util.buildStringInClauseContent(adminFeeAgentsPaged)
							+ ")";
					jdbcTemplate.execute(sql);
				}
			}
		}

	}

	private void addAdminFeeAgents(List<String> adminFeeAgents) throws ModuleException {

		if (adminFeeAgents != null && !adminFeeAgents.isEmpty()) {
			String strTableName = "T_ADMIN_FEE_AGENT";
			adminFeeAgents = getEscapeSqlForAgent(adminFeeAgents);

			List<String> columns = new ArrayList<>();
			columns.add("AGENT_CODE");

			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

			List<String> adminFeeAgentsPaged;
			String sql;

			int pageSize = 999;
			int endIndex;
			for (int a = 0; a <= adminFeeAgents.size() / pageSize; a++) {

				endIndex = adminFeeAgents.size() <= (a + 1) * pageSize ? adminFeeAgents.size() : ((a + 1) * pageSize);
				adminFeeAgentsPaged = adminFeeAgents.subList(a * pageSize, endIndex);

				if (!adminFeeAgentsPaged.isEmpty()) {
					sql = "INSERT ALL " + Util.buildINTOClauseForMultipleInsert(strTableName, columns, adminFeeAgentsPaged)
							+ " SELECT * FROM dual";
					jdbcTemplate.execute(sql);
				}
			}
		}

	}

	private String[] getActiveAgentHql(AdminFeeAgentSearchCriteria criteria) {

		String hql = " select agent from Agent agent";
		String hqlCount = " select count(*) from Agent agent";
		String hqlWhere = "";
		String hqlOrderby = " order by upper(agent.agentCode), agent.agentCode asc ";

		if (criteria.getAgentCode() != null) {

			if (hqlWhere.equals("")) {
				hqlWhere = hqlWhere + " where lower(agent.agentCode) like :agentcode ";
			} else {
				hqlWhere = hqlWhere + " and lower(agent.agentCode) like :agentcode ";
			}

		}

		hql = hql + " " + hqlWhere + " " + hqlOrderby;
		hqlCount = hqlCount + " " + hqlWhere + " " + hqlOrderby;

		return new String[] { hql, hqlCount };
	}

	private static List<String> getEscapeSqlForAgent(List<String> strArray) {
		List<String> escapeSqlList = new ArrayList<String>();
		if (strArray != null && !strArray.isEmpty()) {
			for (String str : strArray) {
				escapeSqlList.add(escapeSqlInjectionForAgents(str));
			}
		}
		return escapeSqlList;
	}

	/*
	 * This is to replace characters that can be lead to sql injection, since agentCode doesnt contain any special
	 * characters
	 */
	private static String escapeSqlInjectionForAgents(String strAgents) {
		String data = null;
		if (strAgents != null && strAgents.length() > 0) {
			strAgents = strAgents.replace("\\", "\\\\");
			strAgents = strAgents.replace("'", "\\'");
			strAgents = strAgents.replace("\0", "\\0");
			strAgents = strAgents.replace("\n", "\\n");
			strAgents = strAgents.replace("\r", "\\r");
			strAgents = strAgents.replace("\"", "\\\"");
			strAgents = strAgents.replace("\\x1a", "\\Z");
			data = strAgents;
		}
		return data;
	}

	
	public Collection<AgentHandlingFeeModification> getAgentActiveHandlingFeeDefinedForMOD(String agentCode) {
		List<AgentHandlingFeeModification> result = find(
				"from AgentHandlingFeeModification amhf" + " where amhf.status='ACT' AND amhf.agent.agentCode = ? ", agentCode,
				AgentHandlingFeeModification.class);

		return result;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents() {
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		Date currentZuluTime = CalendarUtil.getCurrentSystemTimeInZulu();
		String currentZuluTimeInString = CalendarUtil.formatForSQL_toDate(currentZuluTime);
		
		StringBuffer query =  new StringBuffer();
		
		query.append("SELECT AGENT_CODE  ");
		query.append("FROM t_agent  ");
		query.append("WHERE SHOW_PRICE_IN_SELECTED_CUR = 'Y'  ");
		query.append("AND status                       = 'ACT'  ");
		query.append("UNION  ");
		query.append("SELECT UNIQUE agent.agent_code  ");
		query.append("FROM T_OND_FARE ondFare  ");
		query.append("JOIN T_FARE_RULE fareRule  ");
		query.append("ON fareRule.FARE_RULE_ID = ondFare.FARE_RULE_ID  ");
		query.append("JOIN T_FARE_AGENT fareAgent  ");
		query.append("ON fareAgent.FARE_RULE_ID = fareRule.FARE_RULE_ID  ");
		query.append("JOIN t_agent agent  ");
		query.append("ON agent.agent_code = fareAgent.AGENT_CODE  ");
		query.append("JOIN t_user user_  ");
		query.append("ON user_.agent_code = agent.agent_code  ");
		query.append("JOIN T_USER_ACL userAcl  ");
		query.append("ON userAcl.USER_ID            = user_.USER_ID  ");
		query.append("WHERE ondFare.STATUS          = 'ACT'  ");
		query.append("AND ondFare.EFFECTIVE_TO_DATE > to_date(").append(currentZuluTimeInString).append(") ");
		query.append("AND ondFare.SALES_VALID_TO    > to_date(").append(currentZuluTimeInString).append(") ");
		query.append("AND fareRule.STATUS           = 'ACT'  ");
		query.append("AND agent.STATUS              = 'ACT'  ");
		query.append("AND user_.STATUS              = 'ACT'");				
		
		List<String> agentsCodes = (List<String>)jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public List<String> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> agentsCodes = new ArrayList<>();

				if (rs != null) {
					while (rs.next()) {
						agentsCodes.add(rs.getString("AGENT_CODE"));
					}
				}

				return agentsCodes;
			}
		});	
		
		return agentsCodes;
		
	}
	
}