/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.airtravelagents.api.dto.external.EXTPaxPayTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.airtravelagents.api.model.PaxExtTransaction;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.airtravelagents.api.util.CollectionTypeEnum;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentCollectionDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentTransactionDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.ExternalTransactionDAO;
import com.isa.thinair.airtravelagents.core.util.AgentTransactionFactory;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.ProductType;

/**
 * @author Byorn
 * 
 */
public class TravelAgentTransactionBL {

	private final Log log = LogFactory.getLog(TravelAgentTransactionBL.class);

	private static String EXTERNAL_TRANSDAO = "externalTransactionDAO";

	public static enum SETTLEMENT_PRIORITY {
		DISTRIBUTED_CREDIT,
		SHARED_CREDIT
	}

	/**
	 * The constructor. -
	 */
	public TravelAgentTransactionBL() {
	}

	private AgentTransactionDAO getAgentTransactionDAO() {
		return AirTravelagentsModuleUtils.getAgentTransactionDAO();
	}

	private AgentDAO getAgentDAO() {
		return AirTravelagentsModuleUtils.getAgentDAO();
	}

	private boolean isDryAgent(String agentCode) {
		return TravelAgentBL.isDryAgent(agentCode);
	}

	private void validateAgentCredit(String agentCode, BigDecimal payableAmount, PaymentReferenceTO paymentReferenceTO)
			throws ModuleException {
		AgentSummary agentSummary = getAgentCollectionDAO().getAgentSummary(agentCode);
		BigDecimal avlCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null
				&& paymentReferenceTO.getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
			avlCredit = agentSummary.getAvailableBSPCredit();
		} else {
			avlCredit = agentSummary.getAvailableCredit();
		}

		if (payableAmount != null && payableAmount.compareTo(avlCredit) == 1) {
			throw new ModuleException("airtravelagent.logic.creditbalance.exceeds");
		}
	}

	@Deprecated
	private void adjustOwnDueAmount(String agentCode, BigDecimal amount) throws ModuleException {
		if (!isDryAgent(agentCode)) {
			AgentDAO agentDAO = getAgentDAO();
			List<AgentSummary> lstAgentSummary = agentDAO.getAgentSummary(agentCode);
			AgentSummary agentSummary = lstAgentSummary.get(0);
			agentSummary.setOwnDueAmount(AccelAeroCalculator.add(agentSummary.getOwnDueAmount(), amount));
			agentDAO.saveOrUpdateAgentSummary(lstAgentSummary);
		}
	}

	@Deprecated
	private void adjustAgentSummaryForPayments(String agentCode, BigDecimal amount, boolean isAdjustInvoicePool,
			String invoiceAdjAgentCode, boolean isAdjustAvailableCredit, boolean isFixedCreditAgent) throws ModuleException {

		// skipping the credit adjustment if the agent is a dry agent
		if (!isDryAgent(agentCode)) {
			List<AgentSummary> updatingAgentSummaryList = new ArrayList<AgentSummary>();

			AgentDAO agentDAO = getAgentDAO();
			List<AgentSummary> lstAgentSummary = agentDAO.getAgentSummary(agentCode);
			AgentSummary agentSummary = lstAgentSummary.get(0);
			updatingAgentSummaryList.add(agentSummary);

			BigDecimal effectiveAmountForAvailableCredit = amount;

			if (isFixedCreditAgent) {
				List<String> paymentSettlementPriority = AppSysParamsUtil.getPaymentSettlementPriority();
				BigDecimal remainingAmount = amount;
				for (int i = 0; i < paymentSettlementPriority.size(); i++) {
					String paymentSettlementOption = paymentSettlementPriority.get(i);
					boolean isLastOption = (paymentSettlementPriority.size() == i + 1);
					if (AccelAeroCalculator.isGreaterThan(remainingAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						if (SETTLEMENT_PRIORITY.DISTRIBUTED_CREDIT.toString().equals(paymentSettlementOption)) {
							BigDecimal distributedCollection = agentSummary.getDistributedCreditCollection();
							if (AccelAeroCalculator.isGreaterThan(distributedCollection,
									AccelAeroCalculator.getDefaultBigDecimalZero())) {

								List<BigDecimal> calculatedResults = handleSettlementAmount(distributedCollection,
										remainingAmount, isLastOption);
								distributedCollection = AccelAeroCalculator.subtract(distributedCollection,
										calculatedResults.get(0));
								agentSummary.setDistributedCreditCollection(distributedCollection);
								remainingAmount = calculatedResults.get(1);

							}
						} else if (SETTLEMENT_PRIORITY.SHARED_CREDIT.toString().equals(paymentSettlementOption)) {
							BigDecimal ownDueAmount = agentSummary.getOwnDueAmount();
							if (AccelAeroCalculator.isGreaterThan(ownDueAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {

								List<BigDecimal> calculatedResults = handleSettlementAmount(ownDueAmount, remainingAmount,
										isLastOption);
								effectiveAmountForAvailableCredit = calculatedResults.get(0);
								ownDueAmount = AccelAeroCalculator.subtract(ownDueAmount, calculatedResults.get(0));
								agentSummary.setOwnDueAmount(ownDueAmount);
								remainingAmount = calculatedResults.get(1);

							}
						}
					}
				}
			} else {
				agentSummary.setOwnDueAmount(AccelAeroCalculator.subtract(agentSummary.getOwnDueAmount(), amount));
			}

			if (isAdjustAvailableCredit) {
				agentSummary.setAvailableCredit(
						AccelAeroCalculator.add(agentSummary.getAvailableCredit(), effectiveAmountForAvailableCredit));
			}

			if (isAdjustInvoicePool) {
				if (agentCode.equals(invoiceAdjAgentCode)) {
					agentSummary.setAvailableFundsForInv(AccelAeroCalculator.add(agentSummary.getAvailableFundsForInv(), amount));
				} else {
					AgentSummary invoiceAdjAgentSummary = agentDAO.getAgentSummary(invoiceAdjAgentCode).get(0);
					invoiceAdjAgentSummary.setAvailableFundsForInv(
							AccelAeroCalculator.add(invoiceAdjAgentSummary.getAvailableFundsForInv(), amount));
					updatingAgentSummaryList.add(invoiceAdjAgentSummary);
				}
			}

			agentDAO.saveOrUpdateAgentSummary(updatingAgentSummaryList);
		}
	}

	private List<BigDecimal> handleSettlementAmount(BigDecimal dueAmount, BigDecimal paymentAmount, boolean isLastOption) {
		List<BigDecimal> list = new ArrayList<BigDecimal>();
		BigDecimal wipeOffAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal remainingAmountForPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (isLastOption) {
			wipeOffAmount = paymentAmount;
			if (AccelAeroCalculator.isLessThan(paymentAmount, dueAmount)) {
				remainingAmountForPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
			} else {
				remainingAmountForPayment = AccelAeroCalculator.subtract(dueAmount, paymentAmount);
			}
		} else {
			if (AccelAeroCalculator.isLessThan(paymentAmount, dueAmount)) {
				wipeOffAmount = paymentAmount;
				remainingAmountForPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
			} else {
				wipeOffAmount = dueAmount;
				remainingAmountForPayment = AccelAeroCalculator.subtract(paymentAmount, wipeOffAmount);
			}

		}

		list.add(wipeOffAmount);
		list.add(remainingAmountForPayment);

		return list;
	}

	private void adjustAgentSummary(String agentCode, BigDecimal amount, boolean isAdjustInvoicePool, String invoiceAdjAgentCode)
			throws ModuleException {

		// skipping the credit adjustment if the agent is a dry agent
		if (!isDryAgent(agentCode)) {
			// Nili 10:05 AM 2/18/2009
			// We just have to adjust the credit limit. We don't need to update the whole Agent object itself.
			// Under Web Services there will be a high hit of reservations via the same agency.
			// Due to this errors like "Row was updated or deleted by another transaction" could happen.

			List<AgentSummary> updatingAgentSummaryList = new ArrayList<AgentSummary>();

			AgentDAO agentDAO = getAgentDAO();
			List<AgentSummary> lstAgentSummary = agentDAO.getAgentSummary(agentCode);
			AgentSummary agentSummary = lstAgentSummary.get(0);
			updatingAgentSummaryList.add(agentSummary);
			agentSummary.setAvailableCredit(AccelAeroCalculator.add(agentSummary.getAvailableCredit(), amount));

			if (isAdjustInvoicePool) {
				if (agentCode.equals(invoiceAdjAgentCode)) {
					agentSummary.setAvailableFundsForInv(AccelAeroCalculator.add(agentSummary.getAvailableFundsForInv(), amount));
				} else {
					AgentSummary invoiceAdjAgentSummary = agentDAO.getAgentSummary(invoiceAdjAgentCode).get(0);
					invoiceAdjAgentSummary.setAvailableFundsForInv(
							AccelAeroCalculator.add(invoiceAdjAgentSummary.getAvailableFundsForInv(), amount));
					updatingAgentSummaryList.add(invoiceAdjAgentSummary);
				}
			}

			agentDAO.saveOrUpdateAgentSummary(updatingAgentSummaryList);
		}
	}

	/**
	 * @param agentCode
	 * @param amount
	 * @param agentPaymentMethod
	 * @throws ModuleException
	 */
	private void adjustAgentSummary(String agentCode, BigDecimal amount, PAYMENT_REF_TYPE paymentRefType) throws ModuleException {

		// skipping the credit adjustment if the agent is a dry agent
		if (!isDryAgent(agentCode)) {
			AgentDAO agentDAO = getAgentDAO();
			List<AgentSummary> lstAgentSummary = agentDAO.getAgentSummary(agentCode);
			AgentSummary agentSummary = lstAgentSummary.get(0);
			if (PAYMENT_REF_TYPE.BSP.equals(paymentRefType)) {
				agentSummary.setAvailableBSPCredit(AccelAeroCalculator.add(agentSummary.getAvailableBSPCredit(), amount));
			} else {
				agentSummary.setAvailableCredit(AccelAeroCalculator.add(agentSummary.getAvailableCredit(), amount));
			}
			agentDAO.saveOrUpdateAgentSummary(lstAgentSummary);
		}
	}

	/**
	 * ta makes a sale
	 * 
	 * @param userPrincipal
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param pnr
	 * @param payCurrencyDTO
	 * @param paymentReferenceTO
	 * @param productType
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public AgentTransaction recordSale(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks,
			String pnr, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType)
			throws ModuleException {

		if (paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null) {
			if (paymentReferenceTO.getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
				return this.recordBSPSale(userPrincipal, agentCode, amount, remarks, pnr, payCurrencyDTO, paymentReferenceTO,
						productType);
			} else {
				return this.recordOnAccSale(userPrincipal, agentCode, amount, remarks, pnr, payCurrencyDTO, paymentReferenceTO,
						productType);
			}
		} else {
			return this.recordOnAccSale(userPrincipal, agentCode, amount, remarks, pnr, payCurrencyDTO, paymentReferenceTO,
					productType);
		}
	}

	public AgentTransaction recordExternalProductSale(UserPrincipal userPrincipal, String agentCode, BigDecimal amount,
			String remarks, String pnr, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO,
			Character productType) throws ModuleException {

		if (paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null) {
			if (paymentReferenceTO.getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
				return this.recordExternalProductBSPSale(userPrincipal, agentCode, amount, remarks, pnr, payCurrencyDTO,
						paymentReferenceTO, productType);
			} else {
				return this.recordExternalProductOnAccSale(userPrincipal, agentCode, amount, remarks, pnr, payCurrencyDTO,
						paymentReferenceTO, productType);
			}
		} else {
			return this.recordExternalProductOnAccSale(userPrincipal, agentCode, amount, remarks, pnr, payCurrencyDTO,
					paymentReferenceTO, productType);
		}
	}

	public AgentTransaction recordBSPSale(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks,
			String pnr, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType)
			throws ModuleException {

		String userId = userPrincipal.getUserId();
		int nominalCode = AgentTransactionNominalCode.BSP_ACC_SALE.getCode();
		if (productType != null && ProductType.GOQUO.equals(productType)) {
			nominalCode = AgentTransactionNominalCode.EXT_PROD_BSP_ACC_SALE.getCode();
		}
		int salesChannel = userPrincipal.getSalesChannel();

		if (agentCode == null) {
			throw new ModuleException("airtravelagent.param.null");
		}

		// We are skipping the agent account balance for dry agents as their account balance is defaulted to zero
		if (!isDryAgent(agentCode) && AppSysParamsUtil.isMaintainAgentBspCreditLimit()) {
			this.validateAgentCredit(agentCode, amount, paymentReferenceTO);
		}

		AgentTransaction agentTransaction = null;

		if (AppSysParamsUtil.isAeroMartPayEnabled()) {
			agentTransaction = getAgentTransactionDAO().getAgentTransactionByExtRef(paymentReferenceTO.getPaymentRef());
			if (agentTransaction != null) {
				agentTransaction.setPnr(pnr);
			}
		}

		if (agentTransaction == null) {
			agentTransaction = AgentTransactionFactory.getDebitInstance(amount, nominalCode, 0, remarks, agentCode, userId,
					salesChannel, pnr, payCurrencyDTO, productType, paymentReferenceTO.getPaymentRef(), null);
		}

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		// TODO- decide on app parameter whether BSP balance is maintained.
		if (AppSysParamsUtil.isMaintainAgentBspCreditLimit()) {
			adjustAgentSummary(agentCode, amount.negate(), PAYMENT_REF_TYPE.BSP);
		}

		return agentTransaction;
	}

	public AgentTransaction recordExternalProductBSPSale(UserPrincipal userPrincipal, String agentCode, BigDecimal amount,
			String remarks, String pnr, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO,
			Character productType) throws ModuleException {

		String userId = userPrincipal.getUserId();
		int nominalCode = AgentTransactionNominalCode.EXT_PROD_BSP_ACC_SALE.getCode();
		if (productType == null) {
			throw new ModuleException("airtravelagent.product.type.null");
		}
		int salesChannel = userPrincipal.getSalesChannel();

		if (agentCode == null) {
			throw new ModuleException("airtravelagent.param.null");
		}

		// We are skipping the agent account balance for dry agents as their account balance is defaulted to zero
		if (!isDryAgent(agentCode) && AppSysParamsUtil.isMaintainAgentBspCreditLimit()) {
			this.validateAgentCredit(agentCode, amount, paymentReferenceTO);
		}

		AgentTransaction agentTransaction = AgentTransactionFactory.getDebitInstance(amount, nominalCode, 0, remarks, agentCode,
				userId, salesChannel, pnr, payCurrencyDTO, productType, paymentReferenceTO.getPaymentRef(), null);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		// TODO- decide on app parameter whether BSP balance is maintained.
		if (AppSysParamsUtil.isMaintainAgentBspCreditLimit()) {
			adjustAgentSummary(agentCode, amount.negate(), PAYMENT_REF_TYPE.BSP);
		}

		return agentTransaction;
	}

	public AgentTransaction recordOnAccSale(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks,
			String pnr, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType)
			throws ModuleException {

		String userId = userPrincipal.getUserId();
		int nominalCode = AgentTransactionNominalCode.ACC_SALE.getCode();
		if (productType != null) {
			nominalCode = AgentTransactionNominalCode.EXT_PROD_ACC_SALE.getCode();
		}
		int salesChannel = userPrincipal.getSalesChannel();

		if (agentCode == null) {
			throw new ModuleException("airtravelagent.param.null");
		}

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		// We are skipping the agent account balance for dry agents as their account balance is defaulted to zero
		if (!isDryAgent(creditSharedAgentCode)) {
			this.validateAgentCredit(creditSharedAgent.getAgentCode(), amount, paymentReferenceTO);
		}

		if (paymentReferenceTO != null) {
			if (paymentReferenceTO.getPaymentRefType() == PAYMENT_REF_TYPE.LOYALTY) {
				userId = AppSysParamsUtil.getLoyaltyUserForWeb();
			}
		}

		AgentTransaction agentTransactionGoQuo = null;
		AgentTransaction agentTransaction = null;
		if (AppSysParamsUtil.isAeroMartPayEnabled()) {
			agentTransactionGoQuo = getAgentTransactionDAO().getAgentTransactionByExtRef(paymentReferenceTO.getPaymentRef());
			if (agentTransactionGoQuo != null) {
				agentTransactionGoQuo.setPnr(pnr);
				getAgentTransactionDAO().saveOrUpdate(agentTransactionGoQuo);
			}
		}
		if (agentTransaction == null) {
			agentTransaction = AgentTransactionFactory.getDebitInstance(amount, nominalCode, 0, remarks, agentCode, userId,
					salesChannel, pnr, payCurrencyDTO, productType, paymentReferenceTO.getPaymentRef(), creditSharedAgentCode);
		}

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		adjustAgentSummary(creditSharedAgent.getAgentCode(), amount.negate(), false, agentCode);

		adjustTotalDueAmount(agentCode, amount);
		return agentTransaction;
	}

	public AgentTransaction recordExternalProductOnAccSale(UserPrincipal userPrincipal, String agentCode, BigDecimal amount,
			String remarks, String pnr, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO,
			Character productType) throws ModuleException {

		String userId = userPrincipal.getUserId();
		int nominalCode = AgentTransactionNominalCode.EXT_PROD_ACC_SALE.getCode();
		if (productType == null) {
			throw new ModuleException("airtravelagent.product.type.null");
		}
		int salesChannel = userPrincipal.getSalesChannel();

		if (agentCode == null) {
			throw new ModuleException("airtravelagent.param.null");
		}
		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}
		// We are skipping the agent account balance for dry agents as their account balance is defaulted to zero
		if (!isDryAgent(agentCode)) {
			this.validateAgentCredit(agentCode, amount, paymentReferenceTO);
		}

		if (paymentReferenceTO != null) {
			if (paymentReferenceTO.getPaymentRefType() == PAYMENT_REF_TYPE.LOYALTY) {
				userId = AppSysParamsUtil.getLoyaltyUserForWeb();
			}
		}

		AgentTransaction agentTransactionGoQuo = null;
		AgentTransaction agentTransaction = null;
		if (AppSysParamsUtil.isAeroMartPayEnabled()) {
			agentTransactionGoQuo = getAgentTransactionDAO().getAgentTransactionByExtRef(paymentReferenceTO.getPaymentRef());
			if (agentTransactionGoQuo != null) {
				agentTransactionGoQuo.setPnr(pnr);
				getAgentTransactionDAO().saveOrUpdate(agentTransactionGoQuo);
			}
		}

		if (agentTransaction == null) {
			agentTransaction = AgentTransactionFactory.getDebitInstance(amount, nominalCode, 0, remarks, agentCode, userId,
					salesChannel, pnr, payCurrencyDTO, productType, paymentReferenceTO.getPaymentRef(), creditSharedAgentCode);
		}
		// agentTransaction.setProductType(ProductType.GOQUO);
		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		adjustAgentSummary(creditSharedAgent.getAgentCode(), amount.negate(), false, agentCode);
		// adjustOwnDueAmount(agentCode, amount);
		adjustTotalDueAmount(agentCode, amount);

		return agentTransaction;
	}

	public AgentTransaction recordAgentCommission(UserPrincipal userPrincipal, String agentCode, BigDecimal amount,
			String remarks, String pnr, String amountSpecifiedIn) throws ModuleException {

		if (agentCode == null) {
			throw new ModuleException("airtravelagent.param.null");
		}

		String currencyCode = null;
		Agent agent = getAgentDAO().getAgent(agentCode);
		if (agent != null) {
			currencyCode = agent.getCurrencyCode();
		}

		int nominalCode = AgentTransactionNominalCode.ACC_COMMISSION_GRANT.getCode();
		String userId = userPrincipal.getUserId();
		Integer salesChannelCode = userPrincipal.getSalesChannel();
		BigDecimal amountInBase = currencyConverterForAgents(amount, currencyCode, amountSpecifiedIn).getLeft();
		BigDecimal amountInLocal = currencyConverterForAgents(amountInBase, currencyCode, amountSpecifiedIn).getRight();

		// TODO:RW review agent collection change
		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		AgentTransaction agentTransaction = AgentTransactionFactory.getCreditInstance(amountInBase, nominalCode, 0, remarks,
				agentCode, userId, salesChannelCode, pnr, currencyCode, amountInLocal, creditSharedAgentCode);
		getAgentTransactionDAO().saveOrUpdate(agentTransaction);
		adjustAgentSummary(creditSharedAgent.getAccountCode(), amountInBase, false, agentCode);
		// adjustOwnDueAmount(agentCode, amountInBase.negate());

		return agentTransaction;

	}

	public AgentTransaction removeAgentCommission(String agentCode, String userId, Integer salesChannelCode, BigDecimal amount,
			String remarks, String pnr, String amountSpecifiedIn) throws ModuleException {

		if (agentCode == null) {
			throw new ModuleException("airtravelagent.param.null");
		}

		String currencyCode = null;
		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		if (creditSharedAgent != null) {
			currencyCode = creditSharedAgent.getCurrencyCode();
		}

		int nominalCode = AgentTransactionNominalCode.ACC_COMMISSION_REMOVE.getCode();
		BigDecimal amountInBase = currencyConverterForAgents(amount, currencyCode, amountSpecifiedIn).getLeft();
		BigDecimal amountInLocal = currencyConverterForAgents(amountInBase, currencyCode, amountSpecifiedIn).getRight();

		AgentTransaction agentTransaction = AgentTransactionFactory.getDebitInstance(amountInBase, amountInLocal, nominalCode, 0,
				remarks, agentCode, userId, salesChannelCode, pnr, currencyCode, null, null, null, creditSharedAgentCode);
		getAgentTransactionDAO().saveOrUpdate(agentTransaction);
		adjustAgentSummary(creditSharedAgent.getAgentCode(), amount.negate(), false, agentCode);
		// adjustOwnDueAmount(agentCode, amount);

		return agentTransaction;

	}

	// TODO: move to : AgentTransactionFactory
	private Pair<BigDecimal, BigDecimal> currencyConverterForAgents(BigDecimal amount, String currencyCode,
			String amountSpecifiedIn) throws ModuleException {

		BigDecimal amountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountInLocal = AccelAeroCalculator.getDefaultBigDecimalZero();

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		if (amountSpecifiedIn == null || Agent.CURRENCY_BASE.equals(amountSpecifiedIn)) {
			amountInBase = amount;
			amountInLocal = BLUtil.getAmountInLocal(amountInBase, currencyCode, exchangeRateProxy);
		} else {
			amountInLocal = amount;
			amountInBase = BLUtil.getAmountInBase(amountInLocal, currencyCode, exchangeRateProxy);
		}

		return Pair.of(amountInBase, amountInLocal);
	}

	/**
	 * ta makes a refund on behalf of us.
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param productType
	 *            TODO
	 * 
	 * @throws ModuleException
	 */
	public AgentTotalSaledDTO recordRefund(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks,
			String pnr, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType)
			throws ModuleException {
		AgentTransaction agentTransaction = null;

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		if (paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null
				&& paymentReferenceTO.getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
			agentTransaction = AgentTransactionFactory.getCreditInstance(amount, AgentTransactionNominalCode.BSP_REFUND.getCode(),
					0, remarks, agentCode, userPrincipal.getUserId(), userPrincipal.getSalesChannel(), pnr, payCurrencyDTO,
					productType, creditSharedAgentCode);
		} else {
			agentTransaction = AgentTransactionFactory.getCreditInstance(amount, AgentTransactionNominalCode.ACC_REFUND.getCode(),
					0, remarks, agentCode, userPrincipal.getUserId(), userPrincipal.getSalesChannel(), pnr, payCurrencyDTO, null,
					creditSharedAgentCode);
		}

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		PAYMENT_REF_TYPE paymentRefType = paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null
				? paymentReferenceTO.getPaymentRefType()
				: null;

		adjustAgentSummary(creditSharedAgent.getAgentCode(), amount, paymentRefType);

		if (PAYMENT_REF_TYPE.BSP != paymentRefType) {
			// adjustOwnDueAmount(agentCode, amount.negate());
			adjustTotalDueAmount(agentCode, amount.negate());
		}

		return composeAgentSalesDTO(agentTransaction);
	}

	public AgentTotalSaledDTO recordExternalProductRefund(UserPrincipal userPrincipal, String agentCode, BigDecimal amount,
			String remarks, String pnr, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO,
			Character productType) throws ModuleException {
		AgentTransaction agentTransaction = null;

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		if (paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null
				&& paymentReferenceTO.getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
			agentTransaction = AgentTransactionFactory.getCreditInstance(amount,
					AgentTransactionNominalCode.EXT_PROD_BSP_ACC_REFUND.getCode(), 0, remarks, agentCode,
					userPrincipal.getUserId(), userPrincipal.getSalesChannel(), pnr, payCurrencyDTO, productType,
					creditSharedAgentCode);
		} else {
			agentTransaction = AgentTransactionFactory.getCreditInstance(amount,
					AgentTransactionNominalCode.EXT_PROD_ACC_REFUND.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
					userPrincipal.getSalesChannel(), pnr, payCurrencyDTO, productType, creditSharedAgentCode);
		}

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		adjustAgentSummary(agentCode, amount, paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null
				? paymentReferenceTO.getPaymentRefType()
				: null);
		return composeAgentSalesDTO(agentTransaction);
	}

	/*
	 * @JIRA AARESAA-2363 MEthod Compose Agent Transaction Session values to generate Refund invoices.
	 */
	private AgentTotalSaledDTO composeAgentSalesDTO(AgentTransaction agTxn) throws ModuleException {

		AgentTotalSaledDTO agentTotalSaledDTO = new AgentTotalSaledDTO();
		agentTotalSaledDTO.setTotalSales(agTxn.getAmount());
		agentTotalSaledDTO.setAgentCode(agTxn.getAgentCode());
		agentTotalSaledDTO.setCurrencyCode(agTxn.getCurrencyCode());
		agentTotalSaledDTO.setPnr(agTxn.getPnr());

		if (agTxn.getAmountLocal() != null) {
			agentTotalSaledDTO.setTotalSalesLocal(agTxn.getAmountLocal());
		}

		return agentTotalSaledDTO;
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param carrierCode
	 *            TODO
	 * @param paymentType
	 * @return transaction ID
	 * @throws ModuleException
	 */
	public int recordPayment(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks,
			CollectionTypeEnum collectionTypeEnum, String amountSpecifiedIn, String carrierCode, String receiptNumber)
			throws ModuleException {

		BigDecimal amountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountInLocal = AccelAeroCalculator.getDefaultBigDecimalZero();
		String currencyCode = null;
		boolean isGSA = false;

		Agent agent = getAgentDAO().getAgent(agentCode);

		Agent gsAgent = getAgentDAO().getAgent(userPrincipal.getAgentCode());
		boolean isGSAV2Enabled = AppSysParamsUtil.isGSAStructureVersion2Enabled();

		isGSA = isGSAV2Enabled ? isValidReportingAgent(gsAgent, agent) : isGsaUser(userPrincipal);

		if (agent != null) {
			currencyCode = agent.getCurrencyCode();
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		if (amountSpecifiedIn == null || Agent.CURRENCY_BASE.equals(amountSpecifiedIn)) {
			amountInBase = amount;
			amountInLocal = BLUtil.getAmountInLocal(amountInBase, currencyCode, exchangeRateProxy);
		} else {
			amountInLocal = amount;
			amountInBase = BLUtil.getAmountInBase(amountInLocal, currencyCode, exchangeRateProxy);
		}

		if (isGSA) {
			String gsaCode = userPrincipal.getAgentCode();
			// skipping the credit adjustment if the agent is a dry agent
			if (!isDryAgent(gsaCode)) {
				if (isGSAV2Enabled) {
					checkReportingAgentCreditForAdjustments(amountInBase, agent);
				} else {
					AgentSummary gsaSum = getAgentCollectionDAO().getAgentSummary(gsaCode);
					BigDecimal gsaAvlCredit = gsaSum.getAvailableCredit();
					if ((amountInBase.compareTo(gsaAvlCredit) == 1)) {
						throw new ModuleException("cc.airtravelagent.logic.creditbalance.gsa.exceeds");
					}
				}
			}
		}

		String collectionType = collectionTypeEnum.getCode();// .==CollectionTypeEnum.CASH.getCode()?
		AgentTransaction agentTransaction = null;
		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		if (collectionType.equals(CollectionTypeEnum.CASH.getCode())) {

			agentTransaction = AgentTransactionFactory.getCreditInstance(amountInBase, amountInLocal,
					AgentTransactionNominalCode.PAY_INV_CASH.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
					userPrincipal.getSalesChannel(), "", currencyCode, carrierCode, receiptNumber, creditSharedAgentCode, null);

		} else if (collectionType.equals(CollectionTypeEnum.CREDITCARD.getCode())) {
			agentTransaction = AgentTransactionFactory.getCreditInstance(amountInBase, amountInLocal,
					AgentTransactionNominalCode.PAY_INV_CC.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
					userPrincipal.getSalesChannel(), "", currencyCode, carrierCode, receiptNumber, creditSharedAgentCode, 'T');

		} else {

			agentTransaction = AgentTransactionFactory.getCreditInstance(amountInBase, amountInLocal,
					AgentTransactionNominalCode.PAY_INV_CHQ.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
					userPrincipal.getSalesChannel(), "", currencyCode, carrierCode, receiptNumber, creditSharedAgentCode, null);
		}

		if (agentTransaction == null) {
			throw new ModuleException("airtravelagent.param.null");
		}
		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		if (isGSAV2Enabled) {
			if (Agent.HAS_CREDIT_LIMIT_YES.equals(agent.getHasCreditLimit())) {
				adjustCreditInfoOfCreditSharedAgent(userPrincipal, amountInBase, amountInLocal, agent, receiptNumber,
						AgentTransactionNominalCode.ACC_DEBIT);
				adjustFixedAgentCreditAndSettlementPool(agentCode, amountInBase);
				adjustReportingAgentCreditAndSettlementPool(agent.getGsaCode(), amountInBase);
			} else {
				adjustCreditInfoOfCreditSharedAgent(userPrincipal, amountInBase, amountInLocal, agent, receiptNumber,
						AgentTransactionNominalCode.ACC_DEBIT);
				adjustSharedAgentCreditAndSettlementPool(agentCode, amountInBase);
				adjustReportingAgentCreditAndSettlementPool(agent.getGsaCode(), amountInBase);
			}
		} else {
			adjustAgentSummary(creditSharedAgent.getAgentCode(), amountInBase, true, agentCode);
			adjustOwnDueAmount(agentCode, amountInBase.negate());
			recordDebitPaymentIfGSA(userPrincipal, amountInBase, amountInLocal, agentCode, agent.getAgentDisplayName(),
					currencyCode, receiptNumber);
		}

		return agentTransaction.getTnxId();
	}

	private boolean isGsaUser(UserPrincipal userPrincipal) {

		boolean gsaFlag = false;

		String gsaCode = userPrincipal.getAgentCode();
		Agent gsaAagent = getAgentDAO().getAgent(gsaCode);
		if (gsaAagent != null && AgentType.GSA.equals(gsaAagent.getAgentTypeCode())) {
			gsaFlag = true;
		}
		return gsaFlag;

	}

	private boolean isValidReportingAgent(Agent gsAgent, Agent agent) throws ModuleException {
		boolean hasReportingAgents = false;
		if (agent.getGsaCode() != null) {
			Collection<Agent> reportingAgents = getAgentDAO().getReportingAgentsForGSA(gsAgent.getAgentCode());
			for (Agent reportingAgent : reportingAgents) {
				if (agent.getAgentCode().equals(reportingAgent.getAgentCode())) {
					hasReportingAgents = true;
					break;
				}
			}
			if (!hasReportingAgents) {
				throw new ModuleException("cc.airtravelagent.logic.invalid.reporting.agent");
			}
		} else {
			if (!AppSysParamsUtil.getCarrierAgent().equals(gsAgent.getAgentTypeCode())) {
				throw new ModuleException("cc.airtravelagent.logic.invalid.reporting.agent");
			}
		}
		return hasReportingAgents;
	}

	@Deprecated
	private void adjustCreditInfoOfParentAgent(UserPrincipal userPrincipal, BigDecimal amountBase, BigDecimal amountLocal,
			Agent invoicingAgent, String receiptNumber) throws ModuleException {

		String loggedAgentCode = userPrincipal.getAgentCode();
		String parentAgentCode = invoicingAgent.getGsaCode();
		boolean isInvoicingAgentHasCreditLimit = Agent.HAS_CREDIT_LIMIT_YES.equals(invoicingAgent.getHasCreditLimit());

		if (!isDryAgent(parentAgentCode)) {
			List<AgentSummary> lstAgentSummary = getAgentDAO().getAgentSummary(parentAgentCode);
			AgentSummary parentAgentSummary = lstAgentSummary.get(0);
			List<AgentSummary> updatingAgentSummaryList = new ArrayList<AgentSummary>();
			if (loggedAgentCode.equals(parentAgentCode)) {

				AgentTransaction agentTransaction = AgentTransactionFactory.getDebitInstance(amountBase, amountLocal,
						AgentTransactionNominalCode.ACC_DEBIT.getCode(), 0,
						"Recieved Payment from :" + invoicingAgent.getAgentCode() + " " + invoicingAgent.getAgentName(),
						parentAgentCode, userPrincipal.getUserId(), userPrincipal.getSalesChannel(), "",
						invoicingAgent.getCurrencyCode(), null, receiptNumber, null, parentAgentCode);
				getAgentTransactionDAO().saveOrUpdate(agentTransaction);

				if (isInvoicingAgentHasCreditLimit) {
					parentAgentSummary.setDistributedCreditCollection(
							AccelAeroCalculator.add(parentAgentSummary.getDistributedCreditCollection(), amountBase));
				} else {
					parentAgentSummary.setOwnDueAmount(AccelAeroCalculator.add(parentAgentSummary.getOwnDueAmount(), amountBase));
				}

				updatingAgentSummaryList.add(parentAgentSummary);
				getAgentDAO().saveOrUpdateAgentSummary(updatingAgentSummaryList);

			} else if (!isInvoicingAgentHasCreditLimit) {
				// Increase available credit of parent agent if shared credit is used by invoicing agent
				parentAgentSummary
						.setAvailableCredit(AccelAeroCalculator.add(parentAgentSummary.getAvailableCredit(), amountBase));

				updatingAgentSummaryList.add(parentAgentSummary);
				getAgentDAO().saveOrUpdateAgentSummary(updatingAgentSummaryList);
			}
		}

	}

	private void recordDebitPaymentIfGSA(UserPrincipal userPrincipal, BigDecimal amountBase, BigDecimal amountLocal,
			String agentCode, String agentName, String agentCurrencyCode, String receiptNumber) throws ModuleException {

		String gsaCode = userPrincipal.getAgentCode();
		Agent agent = getAgentDAO().getAgent(gsaCode);
		if (AgentType.GSA.equals(agent.getAgentTypeCode())) {

			Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
			String creditSharedAgentCode = null;
			if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
				creditSharedAgentCode = creditSharedAgent.getAgentCode();
			}

			AgentTransaction agentTransaction = AgentTransactionFactory.getDebitInstance(amountBase, amountLocal,
					AgentTransactionNominalCode.ACC_DEBIT.getCode(), 0, "Recieved Payment from :" + agentCode + " " + agentName,
					gsaCode, userPrincipal.getUserId(), userPrincipal.getSalesChannel(), "", agentCurrencyCode, null,
					receiptNumber, null, creditSharedAgentCode);

			getAgentTransactionDAO().saveOrUpdate(agentTransaction);

			checkLimitOfAvailableCreditOfGSA(agent, amountBase);

			adjustAgentSummary(creditSharedAgent.getAgentCode(), amountBase.negate(), true, agentCode);
			// adjustOwnDueAmount(agentCode, amountBase);

		}
	}

	private void checkLimitOfAvailableCreditOfGSA(Agent agent, BigDecimal debitAmount) throws ModuleException {
		// skipping the credit adjustment if the agent is a dry agent
		if (!isDryAgent(agent.getAgentCode())) {
			AgentSummary ags = agent.getAgentSummary();
			if (ags != null) {
				BigDecimal availableCredit = ags.getAvailableCredit();
				availableCredit = AccelAeroCalculator.subtract(availableCredit, debitAmount);
				if (availableCredit.compareTo(BigDecimal.ZERO) == -1) {
					throw new ModuleException("airtravelagent.gsa.available.credit.zero");
				}
			}
		}
	}

	/**
	 * This method is used to adjust the pool and credit balacne of an agent, without affecting the invoicing same
	 * nominal code will be used for both debit and credit transaction. A minus value assume a reduction to the pool
	 * thus treated as a debit (vise versa)
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param carrierCode
	 *            TODO
	 * @param paymentType
	 * @return transaction ID
	 * 
	 * @throws ModuleException
	 */
	public int recordPaymentReverse(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks,
			String amountSpecifiedIn, String carrierCode, String receiptNumber) throws ModuleException {

		BigDecimal amountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountInLocal = AccelAeroCalculator.getDefaultBigDecimalZero();
		String currencyCode = null;
		boolean isGSAV2Enabled = AppSysParamsUtil.isGSAStructureVersion2Enabled();

		Agent agent = getAgentDAO().getAgent(agentCode);
		if (agent != null) {
			currencyCode = agent.getCurrencyCode();
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		if (amountSpecifiedIn == null || Agent.CURRENCY_BASE.equals(amountSpecifiedIn)) {
			amountInBase = amount;
			amountInLocal = BLUtil.getAmountInLocal(amountInBase, currencyCode, exchangeRateProxy);
		} else {
			amountInLocal = amount;
			amountInBase = BLUtil.getAmountInBase(amountInLocal, currencyCode, exchangeRateProxy);
		}

		AgentTransaction agentTransaction = null;

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}
		boolean isCreditTnx = false;
		if (amount.compareTo(BigDecimal.ZERO) == 1) {
			isCreditTnx = true;
			agentTransaction = AgentTransactionFactory.getCreditInstance(amountInBase, amountInLocal,
					AgentTransactionNominalCode.ACC_PAY_REVERSE.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
					userPrincipal.getSalesChannel(), "", currencyCode, carrierCode, receiptNumber, creditSharedAgentCode, null);
		} else {
			agentTransaction = AgentTransactionFactory.getDebitInstance(amountInBase.negate(), amountInLocal,
					AgentTransactionNominalCode.ACC_PAY_REVERSE.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
					userPrincipal.getSalesChannel(), "", currencyCode, carrierCode, receiptNumber, null, creditSharedAgentCode);
		}

		if (agentTransaction == null) {
			throw new ModuleException("airtravelagent.param.null");
		}

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);
		if (isGSAV2Enabled) {
			AgentTransactionNominalCode reportingAgentTnxInstance = AgentTransactionNominalCode.ACC_DEBIT;
			if (!isCreditTnx) {
				// aquire credit from credit shared agent
				reportingAgentTnxInstance = AgentTransactionNominalCode.ACC_CREDIT;
			}
			if (Agent.HAS_CREDIT_LIMIT_YES.equals(agent.getHasCreditLimit())) {
				if (!isCreditTnx) {
					validateDebitTransaction(amountInBase, creditSharedAgent.getAgentSummary().getAvailableCredit());
				}
				adjustCreditInfoOfCreditSharedAgent(userPrincipal, amountInBase, amountInLocal, agent, receiptNumber,
						reportingAgentTnxInstance);
				adjustFixedAgentCreditAndSettlementPool(agentCode, amountInBase);
				adjustReportingAgentCreditAndSettlementPool(agent.getGsaCode(), amountInBase);
			} else {
				adjustCreditInfoOfCreditSharedAgent(userPrincipal, amountInBase, amountInLocal, agent, receiptNumber,
						reportingAgentTnxInstance);
				adjustSharedAgentCreditAndSettlementPool(agentCode, amountInBase);
				adjustReportingAgentCreditAndSettlementPool(agent.getGsaCode(), amountInBase);
			}
		} else {
			adjustAgentSummary(creditSharedAgent.getAgentCode(), amountInBase, true, agentCode);
		}
		// adjustOwnDueAmount(agentCode, amountInBase.negate());

		return agentTransaction.getTnxId();
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param carrierCode
	 *            TODO
	 * @return transaction ID
	 * @throws ModuleException
	 */
	public int recordDebit(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks, String pnr,
			String amountSpecifiedIn, String carrierCode, String receiptNumber) throws ModuleException {

		BigDecimal amountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountInLocal = AccelAeroCalculator.getDefaultBigDecimalZero();
		String currencyCode = null;
		boolean isGSAV2Enabled = AppSysParamsUtil.isGSAStructureVersion2Enabled();

		Agent agent = getAgentDAO().getAgent(agentCode);
		if (agent != null) {
			currencyCode = agent.getCurrencyCode();
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		if (amountSpecifiedIn == null || Agent.CURRENCY_BASE.equals(amountSpecifiedIn)) {
			amountInBase = amount;
			amountInLocal = BLUtil.getAmountInLocal(amountInBase, currencyCode, exchangeRateProxy);
		} else {
			amountInLocal = amount;
			amountInBase = BLUtil.getAmountInBase(amountInLocal, currencyCode, exchangeRateProxy);
		}

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		if (isGSAV2Enabled) {
			if (Agent.HAS_CREDIT_LIMIT_YES.equals(agent.getHasCreditLimit())) {
				validateDebitTransaction(amountInBase, creditSharedAgent.getAgentSummary().getAvailableCredit());
			}
		}

		AgentTransaction agentTransaction = AgentTransactionFactory.getDebitInstance(amountInBase, amountInLocal,
				AgentTransactionNominalCode.ACC_DEBIT.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
				userPrincipal.getSalesChannel(), pnr, currencyCode, carrierCode, receiptNumber, null, creditSharedAgentCode);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		if (isGSAV2Enabled) {
			if (Agent.HAS_CREDIT_LIMIT_YES.equals(agent.getHasCreditLimit())) {
				adjustCreditInfoOfCreditSharedAgent(userPrincipal, amountInBase, amountInLocal, agent, receiptNumber,
						AgentTransactionNominalCode.ACC_CREDIT);
				adjustFixedAgentCreditAndSettlementPool(agentCode, amountInBase.negate());
				adjustReportingAgentCreditAndSettlementPool(agent.getGsaCode(), amountInBase.negate());
			} else {
				adjustCreditInfoOfCreditSharedAgent(userPrincipal, amountInBase, amountInLocal, agent, receiptNumber,
						AgentTransactionNominalCode.ACC_CREDIT);
				adjustSharedAgentCreditAndSettlementPool(agentCode, amountInBase.negate());
				adjustReportingAgentCreditAndSettlementPool(agent.getGsaCode(), amountInBase.negate());
			}
		} else {
			adjustAgentSummary(creditSharedAgent.getAgentCode(), amountInBase.negate(), true, agentCode);
		}
		// adjustOwnDueAmount(agentCode, amountInBase);

		return agentTransaction.getTnxId();
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param carrierCode
	 *            TODO
	 * @return transaction ID
	 * @throws ModuleException
	 */
	public int recordCredit(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks, String pnr,
			String amountSpecifiedIn, String carrierCode, String receiptNumber) throws ModuleException {

		BigDecimal amountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountInLocal = AccelAeroCalculator.getDefaultBigDecimalZero();
		boolean isGSAV2Enabled = AppSysParamsUtil.isGSAStructureVersion2Enabled();

		String currencyCode = null;
		Agent agent = getAgentDAO().getAgent(agentCode);
		if (agent != null) {
			currencyCode = agent.getCurrencyCode();
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		if (amountSpecifiedIn == null || Agent.CURRENCY_BASE.equals(amountSpecifiedIn)) {
			amountInBase = amount;
			amountInLocal = BLUtil.getAmountInLocal(amountInBase, currencyCode, exchangeRateProxy);
		} else {
			amountInLocal = amount;
			amountInBase = BLUtil.getAmountInBase(amountInLocal, currencyCode, exchangeRateProxy);
		}

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		AgentTransaction agentTransaction = AgentTransactionFactory.getCreditInstance(amountInBase, amountInLocal,
				AgentTransactionNominalCode.ACC_CREDIT.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
				userPrincipal.getSalesChannel(), pnr, currencyCode, carrierCode, receiptNumber, creditSharedAgentCode, null);
		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		if (isGSAV2Enabled) {
			if (Agent.HAS_CREDIT_LIMIT_YES.equals(agent.getHasCreditLimit())) {
				adjustCreditInfoOfCreditSharedAgent(userPrincipal, amountInBase, amountInLocal, agent, receiptNumber,
						AgentTransactionNominalCode.ACC_DEBIT);
				adjustFixedAgentCreditAndSettlementPool(agentCode, amountInBase);
				adjustReportingAgentCreditAndSettlementPool(agent.getGsaCode(), amountInBase);
			} else {
				adjustCreditInfoOfCreditSharedAgent(userPrincipal, amountInBase, amountInLocal, agent, receiptNumber,
						AgentTransactionNominalCode.ACC_DEBIT);
				adjustSharedAgentCreditAndSettlementPool(agentCode, amountInBase);
				adjustReportingAgentCreditAndSettlementPool(agent.getGsaCode(), amountInBase);
			}
		} else {
			adjustAgentSummary(creditSharedAgent.getAgentCode(), amountInBase, true, agentCode);
		}
		// adjustOwnDueAmount(agentCode, amountInBase.negate());

		return agentTransaction.getTnxId();
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @throws ModuleException
	 */
	public void recordExternalAccountPay(UserPrincipal userPrincipal, Collection<EXTPaxPayTO> colPaxPays, String agentCode,
			BigDecimal amount, String remarks, String pnr, String userId) throws ModuleException {

		Integer agentSalesTxId = null;
		if (agentCode == null) {
			throw new ModuleException("airtravelagent.param.null");
		}

		Agent agent = getAgentDAO().getAgent(agentCode);

		AgentSummary sum = getAgentCollectionDAO().getAgentSummary(agentCode);
		BigDecimal avlCredit = sum.getAvailableCredit();

		// skipping the credit adjustment if the agent is a dry agent
		if (!isDryAgent(agent.getAgentCode())) {
			if (amount.compareTo(avlCredit) == 1) {
				throw new ModuleException("airtravelagent.logic.creditbalance.exceeds");
			}
		}
		String agentCurrency = agent.getCurrencyCode();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate exRate = exchangeRateProxy.getCurrencyExchangeRate(agentCurrency);
		PayCurrencyDTO payCurrDto = new PayCurrencyDTO(agentCurrency, exRate.getMultiplyingExchangeRate());

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		AgentTransaction agentTransaction = AgentTransactionFactory.getDebitInstance(amount,
				AgentTransactionNominalCode.EXT_ACC_SALE.getCode(), 0, remarks, agentCode, userId,
				userPrincipal.getSalesChannel(), pnr, payCurrDto, null, null, creditSharedAgentCode);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		agentSalesTxId = new Integer(agentTransaction.getTnxId());
		adjustAgentSummary(creditSharedAgent.getAgentCode(), amount.negate(), false, agentCode);
		// adjustOwnDueAmount(agentCode, amount);

		recordExternalTransactionRecord(colPaxPays, agentSalesTxId, userPrincipal.getUserId(), userPrincipal.getSalesChannel(),
				AirTravelAgentConstants.TnxTypes.DEBIT, false, agentCurrency, creditSharedAgentCode);
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @throws ModuleException
	 */
	public void recordExternalAccountRefund(UserPrincipal userPrincipal, Collection<EXTPaxPayTO> colPaxPays, String agentCode,
			BigDecimal amount, String remarks, String pnr, String userId) throws ModuleException {

		Integer agentSalesTxId = null;
		Agent agent = getAgentDAO().getAgent(agentCode);
		String agentCurrency = agent.getCurrencyCode();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate exRate = exchangeRateProxy.getCurrencyExchangeRate(agentCurrency);
		PayCurrencyDTO payCurrDto = new PayCurrencyDTO(agentCurrency, exRate.getMultiplyingExchangeRate());

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		AgentTransaction agentTransaction = AgentTransactionFactory.getCreditInstance(amount,
				AgentTransactionNominalCode.EXT_ACC_REFUND.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
				userPrincipal.getSalesChannel(), pnr, payCurrDto, null, creditSharedAgentCode);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);
		adjustAgentSummary(creditSharedAgent.getAgentCode(), amount, true, agentCode);
		// adjustOwnDueAmount(agentCode, amount.negate());

		agentSalesTxId = new Integer(agentTransaction.getTnxId());
		recordExternalTransactionRecord(colPaxPays, agentSalesTxId, userId, userPrincipal.getSalesChannel(),
				AirTravelAgentConstants.TnxTypes.CREDIT, true, agent.getCurrencyCode(), creditSharedAgentCode);

	}

	private void recordExternalTransactionRecord(Collection<EXTPaxPayTO> colPaxPays, Integer agentTnxId, String userId,
			int SalesChanellCode, String strType, boolean isRefund, String currencyCode, String creditSharedAgent)
			throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("Saving the Pax external Transaction");
		}
		Collection<PaxExtTransaction> colExtTransactions = new HashSet<PaxExtTransaction>();
		PaxExtTransaction paxExtTransaction = null;
		Iterator<EXTPaxPayTO> iter = null;
		EXTPaxPayTO extPaxPayTO = null;
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate exRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);
		if (colPaxPays != null) {
			iter = colPaxPays.iterator();
			while (iter.hasNext()) {
				extPaxPayTO = iter.next();

				paxExtTransaction = new PaxExtTransaction();

				paxExtTransaction.setAgentTnxId(agentTnxId);

				paxExtTransaction.setPnrPaxId(extPaxPayTO.getPnrPaxId());

				if (isRefund) {
					paxExtTransaction.setNominalCode(AgentTransactionNominalCode.EXT_ACC_REFUND.getCode());
					paxExtTransaction.setAmount(extPaxPayTO.getAmount().negate());
					paxExtTransaction.setAmountLocal(
							AccelAeroCalculator.multiply(extPaxPayTO.getAmount().negate(), exRate.getMultiplyingExchangeRate()));
				} else {
					paxExtTransaction.setNominalCode(AgentTransactionNominalCode.EXT_ACC_SALE.getCode());
					paxExtTransaction.setAmount(extPaxPayTO.getAmount());
					paxExtTransaction.setAmountLocal(
							AccelAeroCalculator.multiply(extPaxPayTO.getAmount(), exRate.getMultiplyingExchangeRate()));
				}
				paxExtTransaction.setCurrencyCode(currencyCode);
				paxExtTransaction.setSalesChannelCode(SalesChanellCode);
				paxExtTransaction.setUserId(userId);
				paxExtTransaction.setDrOrcr(strType);
				paxExtTransaction.setTransctionDate(new GregorianCalendar().getTime());
				paxExtTransaction.setCreditSharedAgent(creditSharedAgent);

				colExtTransactions.add(paxExtTransaction);
			}
			getEXTTransactionDAO().saveExtTransactions(colExtTransactions);
		}
	}

	/**
	 * 
	 * @return
	 */
	private AgentCollectionDAO getAgentCollectionDAO() {
		return (AgentCollectionDAO) AirTravelagentsModuleUtils.getDAO(AirTravelagentsModuleUtils.AGENT_COLLECTIONDAO);
	}

	/**
	 * 
	 * @return
	 */
	private ExternalTransactionDAO getEXTTransactionDAO() {
		return (ExternalTransactionDAO) AirTravelagentsModuleUtils.getDAO(EXTERNAL_TRANSDAO);
	}

	public int recordBSPCredit(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks, String pnr,
			String amountSpecifiedIn, String carrierCode, String receiptNumber) throws ModuleException {

		BigDecimal amountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountInLocal = AccelAeroCalculator.getDefaultBigDecimalZero();
		String currencyCode = null;
		Agent agent = getAgentDAO().getAgent(agentCode);
		if (agent != null) {
			currencyCode = agent.getCurrencyCode();
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		if (amountSpecifiedIn == null || Agent.CURRENCY_BASE.equals(amountSpecifiedIn)) {
			amountInBase = amount;
			amountInLocal = BLUtil.getAmountInLocal(amountInBase, currencyCode, exchangeRateProxy);
		} else {
			amountInLocal = amount;
			amountInBase = BLUtil.getAmountInBase(amountInLocal, currencyCode, exchangeRateProxy);
		}

		// TODO:RW handle for BSP
		// Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		// if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
		// agentCode = creditSharedAgent.getAgentCode();
		// }

		AgentTransaction agentTransaction = AgentTransactionFactory.getCreditInstance(amountInBase, amountInLocal,
				AgentTransactionNominalCode.BSP_SETTLEMENT.getCode(), 0, remarks, agentCode, userPrincipal.getUserId(),
				userPrincipal.getSalesChannel(), pnr, currencyCode, carrierCode, receiptNumber, null, null);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);

		adjustAgentSummary(agentCode, amountInBase, PAYMENT_REF_TYPE.BSP);

		return agentTransaction.getTnxId();
	}

	public List<AgentTransaction> isDuplicateRecordExistForAgent(String agentCode, BigDecimal amount, int nominalCode)
			throws ModuleException {
		Agent agent = getAgentDAO().getAgent(agentCode);
		if (agent != null) {
			String currency = agent.getCurrencyCode();
			return getAgentTransactionDAO().isDuplicateRecordExistForAgent(agentCode, amount, currency, nominalCode);
		}
		return null;
	}

	private void adjustCreditInfoOfCreditSharedAgent(UserPrincipal userPrincipal, BigDecimal amountBase, BigDecimal amountLocal,
			Agent invoicingAgent, String receiptNumber, AgentTransactionNominalCode tnxInstance) throws ModuleException {

		if (!StringUtil.isNullOrEmpty(invoicingAgent.getGsaCode())) {
			boolean isInvoicingAgentHasCreditLimit = Agent.HAS_CREDIT_LIMIT_YES.equals(invoicingAgent.getHasCreditLimit());
			Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(invoicingAgent.getGsaCode());
			String parentAgentCode = creditSharedAgent.getAgentCode();
			BigDecimal adjustAmount = amountBase;

			if (!isDryAgent(parentAgentCode)) {
				List<AgentSummary> lstAgentSummary = getAgentDAO().getAgentSummary(parentAgentCode);
				AgentTransaction agentTransaction = null;
				if (tnxInstance.equals(AgentTransactionNominalCode.ACC_DEBIT)) {
					agentTransaction = AgentTransactionFactory.getDebitInstance(amountBase, amountLocal, tnxInstance.getCode(), 0,
							"Recieved Payment from :" + invoicingAgent.getAgentCode() + " " + invoicingAgent.getAgentName(),
							parentAgentCode, userPrincipal.getUserId(), userPrincipal.getSalesChannel(), "",
							invoicingAgent.getCurrencyCode(), null, receiptNumber, null, null);
				} else {
					agentTransaction = AgentTransactionFactory.getCreditInstance(amountBase, amountLocal, tnxInstance.getCode(),
							0, "Account Debit for :" + invoicingAgent.getAgentCode() + " " + invoicingAgent.getAgentName(),
							parentAgentCode, userPrincipal.getUserId(), userPrincipal.getSalesChannel(), "",
							invoicingAgent.getCurrencyCode(), null, receiptNumber, null, null);
					adjustAmount = amountBase.negate();
				}
				getAgentTransactionDAO().saveOrUpdate(agentTransaction);

				if (isInvoicingAgentHasCreditLimit) {

					AgentSummary parentAgentSummary = lstAgentSummary.get(0);
					List<AgentSummary> updatingAgentSummaryList = new ArrayList<AgentSummary>();
					if (parentAgentSummary.getAvailableCredit().doubleValue() >= adjustAmount.doubleValue()) {
						parentAgentSummary.setAvailableCredit(
								AccelAeroCalculator.subtract(parentAgentSummary.getAvailableCredit(), adjustAmount));

						updatingAgentSummaryList.add(parentAgentSummary);
						getAgentDAO().saveOrUpdateAgentSummary(updatingAgentSummaryList);
					} else {
						throw new ModuleException("agent.invoice.settlement.reporting.agent.credit.insufficient");
					}

				}

			}
		}

	}

	private void adjustTotalDueAmount(String agentCode, BigDecimal amount) throws ModuleException {
		if (!StringUtil.isNullOrEmpty(agentCode) && !isDryAgent(agentCode)) {
			AgentDAO agentDAO = getAgentDAO();
			List<AgentSummary> lstAgentSummary = agentDAO.getAgentSummary(agentCode);
			AgentSummary agentSummary = lstAgentSummary.get(0);
			agentSummary.setOwnDueAmount(AccelAeroCalculator.add(agentSummary.getOwnDueAmount(), amount));
			agentDAO.saveOrUpdateAgentSummary(lstAgentSummary);
		}
	}

	private void adjustAgentAvailableFundsForInv(String agentCode, BigDecimal amount, String invoiceAdjAgentCode)
			throws ModuleException {

		// skipping the credit adjustment if the agent is a dry agent
		if (!StringUtil.isNullOrEmpty(agentCode) && !isDryAgent(agentCode)) {

			List<AgentSummary> updatingAgentSummaryList = new ArrayList<AgentSummary>();

			AgentDAO agentDAO = getAgentDAO();
			List<AgentSummary> lstAgentSummary = agentDAO.getAgentSummary(agentCode);
			AgentSummary agentSummary = lstAgentSummary.get(0);
			updatingAgentSummaryList.add(agentSummary);

			if (agentCode.equals(invoiceAdjAgentCode)) {
				agentSummary.setAvailableFundsForInv(AccelAeroCalculator.add(agentSummary.getAvailableFundsForInv(), amount));
			} else {
				AgentSummary invoiceAdjAgentSummary = agentDAO.getAgentSummary(invoiceAdjAgentCode).get(0);
				invoiceAdjAgentSummary.setAvailableFundsForInv(
						AccelAeroCalculator.add(invoiceAdjAgentSummary.getAvailableFundsForInv(), amount));
				updatingAgentSummaryList.add(invoiceAdjAgentSummary);
			}

			agentDAO.saveOrUpdateAgentSummary(updatingAgentSummaryList);
		}
	}

	private void adjustFixedAgentCreditAndSettlementPool(String agentCode, BigDecimal amountInBase) throws ModuleException {
		adjustTotalDueAmount(agentCode, amountInBase.negate());
		adjustAgentSummary(agentCode, amountInBase, true, agentCode);
	}

	private void adjustSharedAgentCreditAndSettlementPool(String agentCode, BigDecimal amountInBase) throws ModuleException {
		adjustTotalDueAmount(agentCode, amountInBase.negate());
		adjustAgentAvailableFundsForInv(agentCode, amountInBase, agentCode);
	}

	private void adjustReportingAgentCreditAndSettlementPool(String agentCode, BigDecimal amountInBase) throws ModuleException {
		adjustTotalDueAmount(agentCode, amountInBase);
		adjustAgentAvailableFundsForInv(agentCode, amountInBase.negate(), agentCode);
	}

	private void checkReportingAgentCreditForAdjustments(BigDecimal amountBase, Agent invoicingAgent) throws ModuleException {

		if (!StringUtil.isNullOrEmpty(invoicingAgent.getGsaCode())) {
			boolean isInvoicingAgentHasCreditLimit = Agent.HAS_CREDIT_LIMIT_YES.equals(invoicingAgent.getHasCreditLimit());
			Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(invoicingAgent.getGsaCode());
			String parentAgentCode = creditSharedAgent.getAgentCode();

			if (!isDryAgent(parentAgentCode)) {
				List<AgentSummary> lstAgentSummary = getAgentDAO().getAgentSummary(parentAgentCode);

				if (isInvoicingAgentHasCreditLimit) {
					AgentSummary parentAgentSummary = lstAgentSummary.get(0);
					if (parentAgentSummary.getAvailableCredit().doubleValue() < amountBase.doubleValue()) {
						throw new ModuleException("agent.invoice.settlement.reporting.agent.credit.insufficient");
					}

				}

			}
		}

	}

	private void validateDebitTransaction(BigDecimal debitAmount, BigDecimal availableCredit) throws ModuleException {
		if (debitAmount != null && availableCredit != null && AccelAeroCalculator.isGreaterThan(debitAmount, availableCredit)) {
			throw new ModuleException("agent.settlement.agent.credit.insufficient.for.debit");
		}
	}

	public int reverseCredit(UserPrincipal userPrincipal, String agentCode, BigDecimal amount, String remarks, String pnr,
			String amountSpecifiedIn, String carrierCode, String receiptNumber, String originalTnxId) throws ModuleException {

		validateReverseCreditTransactionRequest(originalTnxId, amount);

		int tnxId = recordDebit(userPrincipal, agentCode, amount, remarks, pnr, amountSpecifiedIn, carrierCode, receiptNumber);
		getAgentTransactionDAO().updateAgentCreditReveresed(Integer.parseInt(originalTnxId));

		return tnxId;

	}

	private void validateReverseCreditTransactionRequest(String originalTnxId, BigDecimal amount) throws ModuleException {

		if (StringUtil.isNullOrEmpty(originalTnxId)) {
			throw new ModuleException("cc.user.form.agent.credit.reverse.transaction.id.required");
		}

		AgentTransaction agentTransaction = getAgentTransactionDAO().getAgentTransaction(Integer.parseInt(originalTnxId));
		if (agentTransaction == null || "DR".equals(agentTransaction.getDrOrcr())) {
			throw new ModuleException("cc.user.form.agent.credit.reverse.transaction.invalid");
		}

		if (AccelAeroCalculator.subtract(agentTransaction.getAmount().abs(), amount.abs()).abs().doubleValue() > 0.01) {
			throw new ModuleException("cc.user.form.agent.credit.reverse.amount.invalid");
		}

		if (agentTransaction.toAgentTransactionTO().isCreditReversed()) {
			throw new ModuleException("cc.user.form.agent.credit.reverse.already.reversed");
		}

		if (14 == agentTransaction.getNominalCode()) {
			throw new ModuleException("cc.user.form.agent.credit.reverse.transaction.invalid");
		}
	}
}
