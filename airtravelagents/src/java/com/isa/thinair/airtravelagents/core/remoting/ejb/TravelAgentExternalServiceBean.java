/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 * @version $Id$
 */
package com.isa.thinair.airtravelagents.core.remoting.ejb;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airtravelagents.api.dto.external.AgentTO;
import com.isa.thinair.airtravelagents.api.dto.external.EXTAgentPayTO;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.core.service.bd.TravelAgentServiceExternalDelegateImpl;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

/**
 * @author Nilindra Fernando
 */
@Stateless
@RemoteBinding(jndiBinding = "TravelAgentExternalService.remote")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class TravelAgentExternalServiceBean extends PlatformBaseSessionBean implements TravelAgentServiceExternalDelegateImpl {

	private final Log log = LogFactory.getLog(getClass());

	@Override
	public AgentTO getAgentTO(String agentId) throws ModuleException {
		return AirTravelagentsModuleUtils.getTravelAgentBD().getAgentTO(agentId);
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Page searchAgentTOs(List<ModuleCriterion> criterion, int startIndex, int pageSize, List<String> orderByFieldList)
			throws ModuleException {
		return AirTravelagentsModuleUtils.getTravelAgentBD().searchAgentTOs(criterion, startIndex, pageSize, orderByFieldList);
	}

	@Override
	public void recordExternalAccountPay(EXTAgentPayTO extAgentTo) throws ModuleException {
		AirTravelagentsModuleUtils.getTravelAgentFinanceBD().recordExternalAccountPay(extAgentTo);
	}

	@Override
	public void recordExternalAccountRefund(EXTAgentPayTO extAgentTo) throws ModuleException {
		AirTravelagentsModuleUtils.getTravelAgentFinanceBD().recordExternalAccountRefund(extAgentTo);
	}

}
