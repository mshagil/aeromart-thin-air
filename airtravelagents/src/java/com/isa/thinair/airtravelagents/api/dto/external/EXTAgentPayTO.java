/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author : Thushara
 * @since 2.0
 */
public class EXTAgentPayTO implements Serializable {

	private static final long serialVersionUID = 5082734167718965072L;

	private String agentCode;

	private BigDecimal amount;

	private String remarks;

	private String pnr;

	private String userId;

	private Collection<EXTPaxPayTO> colPaxPayTos;

	/**
	 * Gets the AgentCode
	 * 
	 * @return agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * Sets the agentCode
	 * 
	 * @param agentCode
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * Gets the Remarks
	 * 
	 * @return remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Sets the Remarks
	 * 
	 * @param remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Gets the PNR
	 * 
	 * @return pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * Sets the PNR
	 * 
	 * @param pnr
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * Gets the UserID
	 * 
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the User Id
	 * 
	 * @param userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * returns the amount
	 * 
	 * @return Returns the amount.
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * sets the amount
	 * 
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * Gets the Collection of PaxPayTos
	 * 
	 * @return colPaxPayTos
	 */
	public Collection<EXTPaxPayTO> getColPaxPayTos() {
		return colPaxPayTos;
	}

	/**
	 * Sets the Collection of PaxPayTOs
	 * 
	 * @param colPaxPayTos
	 */
	public void setColPaxPayTos(Collection<EXTPaxPayTO> colPaxPayTos) {
		this.colPaxPayTos = colPaxPayTos;
	}
}
