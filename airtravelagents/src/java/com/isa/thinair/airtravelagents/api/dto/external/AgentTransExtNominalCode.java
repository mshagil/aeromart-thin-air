/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;

/**
 * @author Nilindra Fernando
 */
public class AgentTransExtNominalCode implements Serializable {

	private static final long serialVersionUID = -7916642472578263228L;

	private int code;

	private AgentTransExtNominalCode(int code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public int getCode() {
		return code;
	}

	public static final AgentTransExtNominalCode EXT_ACC_SALE = new AgentTransExtNominalCode(10);
	public static final AgentTransExtNominalCode EXT_ACC_REFUND = new AgentTransExtNominalCode(11);

}
