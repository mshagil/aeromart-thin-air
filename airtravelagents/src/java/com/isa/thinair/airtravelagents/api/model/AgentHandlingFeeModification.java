package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author rajitha
 * @hibernate.class table = "T_AGENT_MOD_HANDLING_FEE"
 */
public class AgentHandlingFeeModification implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer agentModificationHandlingFeeId;

	private Agent agent;

	private BigDecimal amount;

	private Integer operation;

	private String status;

	private String chargeBasisCode;

	/**
	 * @return
	 * @hibernate.id column = "AGENT_MOD_HANDLING_FEE_ID" generator-class ="native"
	 * @hibernate.generator-param name = "sequence" value ="S_AGENT_MOD_HANDLING_FEE"
	 * 
	 */
	public Integer getAgentModificationHandlingFeeId() {
		return agentModificationHandlingFeeId;
	}

	public void setAgentModificationHandlingFeeId(Integer agentModificationHandlingFeeId) {
		this.agentModificationHandlingFeeId = agentModificationHandlingFeeId;
	}

	/**
	 * @return the meal
	 * @hibernate.many-to-one column="AGENT_CODE"
	 *                        class="com.isa.thinair.airtravelagents.api.model.Agent"
	 * 
	 */
	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	/**
	 * @hibernate.property column = "AMOUNT"
	 * @return
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @hibernate.property column = "APPLICABLE_OPERATION_TYPE"
	 * @return
	 */
	public Integer getOperation() {
		return operation;
	}

	/**
	 * @param operation
	 */
	public void setOperation(Integer operation) {
		this.operation = operation;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "CHARGE_BASIS"
	 * @return
	 */
	public String getChargeBasisCode() {
		return chargeBasisCode;
	}

	/**
	 * @param chargeBasisCode
	 */
	public void setChargeBasisCode(String chargeBasisCode) {
		this.chargeBasisCode = chargeBasisCode;
	}

}