/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author : Chamindap
 * @hibernate.class table = "T_COLLECTION_METHOD" CollectionMethod is the entity class to repesent a CollectionMethod
 *                  model
 * 
 */
public class CollectionPurpose extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -675313883306680914L;

	private int cpId;

	private String description;

	/**
	 * 
	 */
	public CollectionPurpose() {
		super();
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the cpId.
	 * @hibernate.id column = "CM_CODE" generator-class = "assigned"
	 */
	public int getCpId() {
		return cpId;
	}

	/**
	 * @param cpId
	 *            The cpId to set.
	 */
	public void setCpId(int cpId) {
		this.cpId = cpId;
	}
}