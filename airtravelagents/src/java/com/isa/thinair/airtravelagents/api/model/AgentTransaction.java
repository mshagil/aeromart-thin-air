/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airtravelagents.api.dto.external.AgentTransactionTO;
import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Chamindap
 * @hibernate.class table = "T_AGENT_TRANSACTION" Agent is the entity class to repesent a Agent model
 */
public class AgentTransaction extends Persistent implements Serializable {

	private static final long serialVersionUID = -6760711853103967322L;

	private int tnxId;

	private String agentCode;

	private BigDecimal amount;

	private String drOrcr;

	private int invoiced;

	private Date transctionDate;

	private int nominalCode;

	private String userId;

	private String notes;

	private Integer salesChannelCode;

	private String pnr;

	private String currencyCode;

	private BigDecimal amountLocal;

	private String adjustmentCarrierCode;

	private String receiptNumber;

	private Character productType = 'P';

	private String creditSharedAgent;
	
	private String externalRef;

	private Character creditReversed = 'N';
	/**
	 * The constructor.
	 */
	public AgentTransaction() {
		super();
	}

	public AgentTransactionTO toAgentTransactionTO() {
		AgentTransactionTO agentTransactionTO = new AgentTransactionTO();
		agentTransactionTO.setTnxId(this.getTnxId());
		agentTransactionTO.setAgentCode(this.getAgentCode());
		agentTransactionTO.setAmount(this.getAmount());
		agentTransactionTO.setDrOrcr(this.getDrOrcr());
		agentTransactionTO.setInvoiced(this.getInvoiced());
		agentTransactionTO.setTransctionDate(this.getTransctionDate());
		agentTransactionTO.setNominalCode(this.getNominalCode());
		agentTransactionTO.setUserId(this.getUserId());
		agentTransactionTO.setNotes(this.getNotes());
		agentTransactionTO.setSalesChannelCode(this.getSalesChannelCode());
		agentTransactionTO.setPnr(this.getPnr());
		agentTransactionTO.setCurrencyCode(this.getCurrencyCode());
		agentTransactionTO.setAmountLocal(this.getAmountLocal());
		agentTransactionTO.setAdjustmentCarrierCode(this.getAdjustmentCarrierCode());
		agentTransactionTO.setReceiptNumber(this.getReceiptNumber());
		agentTransactionTO.setReceiptNumber(this.getReceiptNumber());
		
		if('Y'==this.getCreditReversed()) {
			agentTransactionTO.setCreditReversed(true);	
		}		

		return agentTransactionTO;
	}

	/**
	 * returns the customer questionnair id.
	 * 
	 * @return Returns the customerQuestionnairId.
	 * @hibernate.id column = "TXN_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_TRANSACTION"
	 */
	public int getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            The tnxId to set.
	 */
	public void setTnxId(int tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * @return Returns the agentCode.
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the drOrcr.
	 * @hibernate.property column = "DR_CR"
	 */
	public String getDrOrcr() {
		return drOrcr;
	}

	/**
	 * @param drOrcr
	 *            The drOrcr to set.
	 */
	public void setDrOrcr(String drOrcr) {
		this.drOrcr = drOrcr;
	}

	/**
	 * @return Returns the invoiced.
	 * @hibernate.property column = "INVOICED"
	 */
	public int getInvoiced() {
		return invoiced;
	}

	/**
	 * @param invoiced
	 *            The invoiced to set.
	 */
	public void setInvoiced(int invoiced) {
		this.invoiced = invoiced;
	}

	/**
	 * @return Returns the transctionDate.
	 * @hibernate.property column = "TNX_DATE"
	 */
	public Date getTransctionDate() {
		return transctionDate;
	}

	/**
	 * @param transctionDate
	 *            The transctionDate to set.
	 */
	public void setTransctionDate(Date transctionDate) {
		this.transctionDate = transctionDate;
	}

	/**
	 * @return Returns the nominalCode.
	 * @hibernate.property column = "NOMINAL_CODE"
	 */
	public int getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            The nominalCode to set.
	 */
	public void setNominalCode(int nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the notes.
	 * @hibernate.property column = "NOTES"
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            The notes to set.
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return Returns the salesChannelCode.
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            The salesChannelCode to set.
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "currency_code"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "amount_local"
	 */
	public BigDecimal getAmountLocal() {
		return amountLocal;
	}

	public void setAmountLocal(BigDecimal amountLocal) {
		this.amountLocal = amountLocal;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "ADJUSTMENT_CARRIER_CODE"
	 */
	public String getAdjustmentCarrierCode() {
		return adjustmentCarrierCode;
	}

	/**
	 * set the adjustment carrier code
	 * 
	 * @param adjustmentCarrierCode
	 */
	public void setAdjustmentCarrierCode(String adjustmentCarrierCode) {
		this.adjustmentCarrierCode = adjustmentCarrierCode;
	}

	/**
	 * @return Returns the .
	 * @hibernate.property column = "RECEIPT_NUMBER"
	 */
	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	/**
	 * @return the productType
	 * @hibernate.property column = "PRODUCT_TYPE"
	 */
	public Character getProductType() {
		return productType;
	}

	/**
	 * @param productType
	 *            the productType to set
	 */
	public void setProductType(Character productType) {
		this.productType = productType;
	}

	/**
	 * @return the productType
	 * @hibernate.property column = "EXT_REFERENCE"
	 */
	public String getExternalRef() {
		return externalRef;
	}

	/**
	 * @param externalRef
	 *            the externalRef to set
	 */
	public void setExternalRef(String externalRef) {
		this.externalRef = externalRef;
	}
	/**
	 * @return the creditSharedAgent
	 * @hibernate.property column = "DEBIT_CREDIT_AGENT"
	 */
	public String getCreditSharedAgent() {
		return creditSharedAgent;
	}

	/**
	 * @param creditSharedAgent
	 *            the creditSharedAgent to set
	 */
	public void setCreditSharedAgent(String creditSharedAgent) {
		this.creditSharedAgent = creditSharedAgent;
	}

	/**
	 * @return the creditReversed
	 * @hibernate.property column = "CREDIT_REVERSED"
	 */
	public Character getCreditReversed() {
		return creditReversed;
	}

	public void setCreditReversed(Character creditReversed) {
		this.creditReversed = creditReversed;
	}

}