/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Chamindap
 * @hibernate.class table = "T_COLLECTION_METHOD" CollectionMethod is the entity class to repesent a CollectionMethod
 *                  model
 * 
 */
public class CollectionMethod extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3380479627452046229L;

	private String cmCode;

	private String description;

	/**
	 * 
	 */
	public CollectionMethod() {
		super();
	}

	/**
	 * returns the cmCode
	 * 
	 * @return Returns the cmCode.
	 * @hibernate.id column = "CM_CODE" generator-class = "assigned"
	 */
	public String getCmCode() {
		return cmCode;
	}

	/**
	 * sets the cmCode
	 * 
	 * @param cmCode
	 *            The cmCode to set.
	 */
	public void setCmCode(String cmCode) {
		this.cmCode = cmCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}