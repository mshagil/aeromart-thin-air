/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airtravelagents.api.util;

import java.io.Serializable;

/**
 * @author Lasantha
 * 
 */
public class CollectionTypeEnum implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5348719929828628041L;
	private String code;

	private CollectionTypeEnum(String code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public boolean equals(CollectionTypeEnum allocateEnum) {
		return (allocateEnum.getCode() == this.getCode());
	}

	public String getCode() {
		return code;
	}

	public static final CollectionTypeEnum CASH = new CollectionTypeEnum("CSH");
	public static final CollectionTypeEnum CHEQUE = new CollectionTypeEnum("CHQ");
	public static final CollectionTypeEnum CREDIT = new CollectionTypeEnum("CRE");
	public static final CollectionTypeEnum DEBIT = new CollectionTypeEnum("DBT");
	public static final CollectionTypeEnum REVERSE = new CollectionTypeEnum("ADJ");
	public static final CollectionTypeEnum BSP = new CollectionTypeEnum("BSP");
	public static final CollectionTypeEnum REVERSE_CREDIT = new CollectionTypeEnum("RVCRE");
	public static final CollectionTypeEnum CREDITCARD = new CollectionTypeEnum("CCD");
}
