package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;

/**
 * Entity store agent notification configuration for payment methods
 * 
 * @author rumesh
 * @hibernate.class table = "T_AGENT_NOTIFICATION_CONFIG"
 */
public class AgentAlertConfig implements Serializable {

	public static final String ENTRY_SEPARATOR = ",";

	private static final long serialVersionUID = 1L;

	private Integer agentAlertConfigId;

	private String paymentMethod;

	private String notifyingLimits;

	private String notifyingEmails;

	private Integer notifyIntervalInDays;

	/**
	 * @return the agentAlertConfigId
	 * @hibernate.id column = "AGENT_NOTIFICATION_CONFIG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_NOTIFICATION_CONFIG"
	 */
	public Integer getAgentAlertConfigId() {
		return agentAlertConfigId;
	}

	/**
	 * @param agentAlertConfigId
	 *            the agentAlertConfigId to set
	 */
	public void setAgentAlertConfigId(Integer agentAlertConfigId) {
		this.agentAlertConfigId = agentAlertConfigId;
	}

	/**
	 * @return the paymentMethod
	 * @hibernate.property column = "PAYMENT_CODE"
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod
	 *            the paymentMethod to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the notifyingLimits
	 * @hibernate.property column = "NOTIFYING_LIMITS"
	 */
	public String getNotifyingLimits() {
		return notifyingLimits;
	}

	/**
	 * @param notifyingLimits
	 *            the notifyingLimits to set
	 */
	public void setNotifyingLimits(String notifyingLimits) {
		this.notifyingLimits = notifyingLimits;
	}

	/**
	 * @return the notifyingEmails
	 * @hibernate.property column = "NOTIFYING_EMAIL_ADDRESSES"
	 */
	public String getNotifyingEmails() {
		return notifyingEmails;
	}

	/**
	 * @param notifyingEmails
	 *            the notifyingEmails to set
	 */
	public void setNotifyingEmails(String notifyingEmails) {
		this.notifyingEmails = notifyingEmails;
	}

	/**
	 * @return the notifyIntervalInDays
	 * @hibernate.property column = "NOTIFY_INTERVAL_IN_DAYS"
	 */
	public Integer getNotifyIntervalInDays() {
		return notifyIntervalInDays;
	}

	/**
	 * @param notifyIntervalInDays
	 *            the notifyIntervalInDays to set
	 */
	public void setNotifyIntervalInDays(Integer notifyIntervalInDays) {
		this.notifyIntervalInDays = notifyIntervalInDays;
	}

}
