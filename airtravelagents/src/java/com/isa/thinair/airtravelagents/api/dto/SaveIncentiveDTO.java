package com.isa.thinair.airtravelagents.api.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author harshaa
 * 
 */
public class SaveIncentiveDTO implements Serializable {

	private static final long serialVersionUID = -1209683331392334440L;
	private String agentIncSchemeID;
	private String schemeName;
	private String schemePrdFrom;
	private String schemePrdTo;
	private String comments;
	private String status;
	private String version;
	private String ticketPriceComponent;
	private String incPromoBookings;
	private String incOwnTRFBookings;
	private String incRepAgentSales;
	private String incentiveBasis;
	private String rateStartValue;
	private String rateEndValue;
	private String incToalFare;
	private String incentiveBasisA;
	private String presentage;
	private String incentiveBasisF;
	private String agents;

	List<SaveIncentiveSlabDTO> slabLst;

	public List<SaveIncentiveSlabDTO> getSlabLst() {
		return slabLst;
	}

	public void setSlabLst(List<SaveIncentiveSlabDTO> slabLst) {
		this.slabLst = slabLst;
	}

	public String getAgentIncSchemeID() {
		return agentIncSchemeID;
	}

	public void setAgentIncSchemeID(String agentIncSchemeID) {
		this.agentIncSchemeID = agentIncSchemeID;
	}

	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	public String getSchemePrdFrom() {
		return schemePrdFrom;
	}

	public void setSchemePrdFrom(String schemePrdFrom) {
		this.schemePrdFrom = schemePrdFrom;
	}

	public String getSchemePrdTo() {
		return schemePrdTo;
	}

	public void setSchemePrdTo(String schemePrdTo) {
		this.schemePrdTo = schemePrdTo;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTicketPriceComponent() {
		return ticketPriceComponent;
	}

	public void setTicketPriceComponent(String ticketPriceComponent) {
		this.ticketPriceComponent = ticketPriceComponent;
	}

	public String getIncPromoBookings() {
		return incPromoBookings;
	}

	public void setIncPromoBookings(String incPromoBookings) {
		this.incPromoBookings = incPromoBookings;
	}

	public String getIncOwnTRFBookings() {
		return incOwnTRFBookings;
	}

	public void setIncOwnTRFBookings(String incOwnTRFBookings) {
		this.incOwnTRFBookings = incOwnTRFBookings;
	}

	public String getIncRepAgentSales() {
		return incRepAgentSales;
	}

	public void setIncRepAgentSales(String incRepAgentSales) {
		this.incRepAgentSales = incRepAgentSales;
	}

	public String getIncentiveBasis() {
		return incentiveBasis;
	}

	public void setIncentiveBasis(String incentiveBasis) {
		this.incentiveBasis = incentiveBasis;
	}

	public String getRateStartValue() {
		return rateStartValue;
	}

	public void setRateStartValue(String rateStartValue) {
		this.rateStartValue = rateStartValue;
	}

	public String getRateEndValue() {
		return rateEndValue;
	}

	public void setRateEndValue(String rateEndValue) {
		this.rateEndValue = rateEndValue;
	}

	public String getIncToalFare() {
		return incToalFare;
	}

	public void setIncToalFare(String incToalFare) {
		this.incToalFare = incToalFare;
	}

	public String getIncentiveBasisA() {
		return incentiveBasisA;
	}

	public void setIncentiveBasisA(String incentiveBasisA) {
		this.incentiveBasisA = incentiveBasisA;
	}

	public String getPresentage() {
		return presentage;
	}

	public void setPresentage(String presentage) {
		this.presentage = presentage;
	}

	public String getIncentiveBasisF() {
		return incentiveBasisF;
	}

	public void setIncentiveBasisF(String incentiveBasisF) {
		this.incentiveBasisF = incentiveBasisF;
	}

	public String getAgents() {
		return agents;
	}

	public void setAgents(String agents) {
		this.agents = agents;
	}

}
