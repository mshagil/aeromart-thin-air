/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Chamindap
 * @hibernate.class table = "T_AGENT_USER" * AgentUser is the entity class to repesent a AgentUser model
 */
public class AgentUser extends Tracking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 174009318107565409L;

	private String agentCode;

	private String userId;

	/**
	 * 
	 */
	public AgentUser() {
		super();
	}

	/**
	 * returns the agentCode
	 * 
	 * @return Returns the agentCode.
	 * @hibernate.id column = "AGENT_CODE" generator-class = "assigned"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * sets the agentCode
	 * 
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * returns the user id.
	 * 
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the user id.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
}