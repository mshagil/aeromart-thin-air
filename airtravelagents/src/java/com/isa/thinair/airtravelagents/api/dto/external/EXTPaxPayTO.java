/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author : Thushara
 * @since 2.0
 */
public class EXTPaxPayTO implements Serializable {

	private static final long serialVersionUID = 5082734167718965072L;

	private int pnrPaxId;

	private BigDecimal amount;

	/**
	 * returns the pnrPaxId
	 * 
	 * @return Returns the pnrPaxId.
	 */
	public int getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * sets the pnrPaxId
	 */
	public void setPnrPaxId(int pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * returns the amount
	 * 
	 * @return Returns the amount.
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * sets the amount
	 * 
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
