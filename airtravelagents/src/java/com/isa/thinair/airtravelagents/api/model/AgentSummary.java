/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airtravelagents.api.dto.external.AgentSummaryTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author : Chamindap
 * @hibernate.class table = "T_AGENT_SUMMARY" AgentSummary is the entity class to repesent a AgentSummary model
 */
public class AgentSummary extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1441967412045370436L;

	private String agentCode;

	private BigDecimal availableCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal availableBSPCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal availableFundsForInv = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal availableAdvance = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds due amount of a travel agent who uses shared credit */
	private BigDecimal ownDueAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds total amount collected from credit distributed for sub agents */
	private BigDecimal distributedCreditCollection = AccelAeroCalculator.getDefaultBigDecimalZero();

	/**
	 * The constructor.
	 */
	public AgentSummary() {
		super();
	}

	/**
	 * Creates Agent Summary Object from AgentSummaryTO object
	 * 
	 * @param agentSummaryTO
	 * @throws ModuleException
	 */
	public AgentSummary(AgentSummaryTO agentSummaryTO) throws ModuleException {
		this();
		Util.copyProperties(this, agentSummaryTO);
	}

	/**
	 * Returns to Agent Summary TO information
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public AgentSummaryTO toAgentSummaryTO() throws ModuleException {
		AgentSummaryTO agentSummaryTO = new AgentSummaryTO();
		Util.copyProperties(agentSummaryTO, this);
		return agentSummaryTO;
	}

	/**
	 * @return Returns the agentCode.
	 * @hibernate.id column = "AGENT_CODE" generator-class = "assigned"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the availableAdvance.
	 * @hibernate.property column = "AVILABLE_ADVANCE"
	 */
	public BigDecimal getAvailableAdvance() {
		return availableAdvance;
	}

	/**
	 * @param availableAdvance
	 *            The availableAdvance to set.
	 */
	public void setAvailableAdvance(BigDecimal availableAdvance) {
		this.availableAdvance = availableAdvance;
	}

	/**
	 * @return Returns the availableCredit.
	 * @hibernate.property column = "AVILABLE_CREDIT"
	 */
	public BigDecimal getAvailableCredit() {
		return availableCredit;
	}

	/**
	 * @param availableCredit
	 *            The availableCredit to set.
	 */
	public void setAvailableCredit(BigDecimal availableCredit) {
		this.availableCredit = availableCredit;
	}

	/**
	 * @return Returns the availableFundsForInv.
	 * @hibernate.property column = "AVILABLE_FUNDS_FOR_INV"
	 */
	public BigDecimal getAvailableFundsForInv() {
		return availableFundsForInv;
	}

	/**
	 * @param availableFundsForInv
	 *            The availableFundsForInv to set.
	 */
	public void setAvailableFundsForInv(BigDecimal availableFundsForInv) {
		this.availableFundsForInv = availableFundsForInv;
	}

	/**
	 * @hibernate.property column = "AVILABLE_BSP_CREDIT"
	 * @return
	 */
	public BigDecimal getAvailableBSPCredit() {
		return availableBSPCredit;
	}

	/**
	 * @param availableBSPCredit
	 */
	public void setAvailableBSPCredit(BigDecimal availableBSPCredit) {
		this.availableBSPCredit = availableBSPCredit;
	}

	/**
	 * @return the ownDueAmount
	 * @hibernate.property column = "SHARED_CREDIT_DUE_AMOUNT"
	 */
	public BigDecimal getOwnDueAmount() {
		return ownDueAmount;
	}

	/**
	 * @param ownDueAmount
	 *            the ownDueAmount to set
	 */
	public void setOwnDueAmount(BigDecimal ownDueAmount) {
		this.ownDueAmount = ownDueAmount;
	}

	/**
	 * @return the distributedCreditCollection
	 * @hibernate.property column = "DISTRIBUTED_CREDIT_COLLECTION"
	 */
	public BigDecimal getDistributedCreditCollection() {
		return distributedCreditCollection;
	}

	/**
	 * @param distributedCreditCollection
	 *            the distributedCreditCollection to set
	 */
	public void setDistributedCreditCollection(BigDecimal distributedCreditCollection) {
		this.distributedCreditCollection = distributedCreditCollection;
	}

}
