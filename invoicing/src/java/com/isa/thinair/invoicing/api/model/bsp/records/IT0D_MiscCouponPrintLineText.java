package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT0D_MiscCouponPrintLineText extends BasicRecord  {
	public IT0D_MiscCouponPrintLineText(DISH_FILE_TYPE fileType) throws BSPException{
		super(fileType, DISH_RECORD_TYPE.IT0D);
	}
}
