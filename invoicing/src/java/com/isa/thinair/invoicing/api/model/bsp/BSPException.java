package com.isa.thinair.invoicing.api.model.bsp;

public class BSPException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public BSPException(String message){
		super(message);
	}
	
	public BSPException(String message, Throwable cause){
		super(message, cause);
	}
}
