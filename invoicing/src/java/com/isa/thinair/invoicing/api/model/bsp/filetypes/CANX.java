package com.isa.thinair.invoicing.api.model.bsp.filetypes;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0S_StockControlNumbers;

public class CANX extends BasicFileType {
	public CANX() {
		super(DISH_FILE_TYPE.CANX);
	}

	public void addStockNumber(IT0S_StockControlNumbers stockNumbers) throws BSPException {
		addElement(stockNumbers);
	}
}
