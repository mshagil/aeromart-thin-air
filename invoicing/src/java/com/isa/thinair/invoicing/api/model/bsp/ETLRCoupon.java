package com.isa.thinair.invoicing.api.model.bsp;

import java.util.Date;

public class ETLRCoupon {

	private String couponNumber;

	private Date modifiedDate;

	private Date depatureDate;

	private String couponStatus;

	private String segmentCode;

	private String depatureCityCode;

	private String arrivalCityCode;

	private String returnFlag;

	private String flightNumber;

	private String classofService;

	private String bagWeight;

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getDepatureDate() {
		return depatureDate;
	}

	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	public String getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
		this.setDepatureCityCode(segmentCode.substring(0, 3));
		this.setArrivalCityCode(segmentCode.substring(segmentCode.length() - 2, segmentCode.length()));
	}

	public String getDepatureCityCode() {
		return depatureCityCode;
	}

	public void setDepatureCityCode(String depatureCityCode) {
		this.depatureCityCode = depatureCityCode;
	}

	public String getArrivalCityCode() {
		return arrivalCityCode;
	}

	public void setArrivalCityCode(String arrivalCityCode) {
		this.arrivalCityCode = arrivalCityCode;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getClassofService() {
		return classofService;
	}

	public void setClassofService(String classofService) {
		this.classofService = classofService;
	}

	public String getBagWeight() {
		return bagWeight;
	}

	public void setBagWeight(String bagWeight) {
		this.bagWeight = bagWeight;
	}

}