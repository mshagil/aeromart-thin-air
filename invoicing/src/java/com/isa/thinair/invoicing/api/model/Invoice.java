/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;


import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.invoicing.api.model.Entity;

/**
 * 
 * @author : Byorn
 * @hibernate.class table = "T_INVOICE" * Invoice is the entity class to repesent a Invoice model
 */

public class Invoice extends Persistent implements Serializable {

	private static final long serialVersionUID = 8187598551267517276L;

	public static final int BILLING_PERIOD_1 = 1;

	public static final int BILLING_PERIOD_2 = 2;

	private String invoiceNumber;

	private Date invoiceDate;

	private Date inviocePeriodFrom;

	private Date invoicePeriodTo;

	private BigDecimal invoiceAmount;

	private BigDecimal settledAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String status;

	private int billingPeriod;

	private String transferStatus;

	private String agentCode;

	private String accountCode;

	private Timestamp transferTimestamp;

	private Integer numOfTimesMailSent = new Integer(0);

	private String mailStatus;

	private BigDecimal commissionAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal invoiceAmountLocal;

	private String pnr; // AARESAA-2363
	
	private String entityCode;
	
	private Entity entity;

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * 
	 */
	public Invoice() {
		super();
	}

	/**
	 * returns the invoiceNumber
	 * 
	 * @return Returns the invoiceNumber.
	 * @hibernate.id column = "INVOICE_NUMBER" generator-class = "assigned"
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * sets the invoiceNumber
	 * 
	 * @param invoiceNumber
	 *            The invoiceNumber to set.
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * returns the invoiceDate
	 * 
	 * @return Returns the invoiceDate.
	 * @hibernate.property column = "INVOICE_DATE"
	 */
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	/**
	 * sets the invoiceDate
	 * 
	 * @param invoiceDate
	 *            The invoiceDate to set.
	 */
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	/**
	 * returns the inviocePeriodFrom
	 * 
	 * @return Returns the inviocePeriodFrom.
	 * @hibernate.property column = "INVIOCE_PERIOD_FROM"
	 */
	public Date getInviocePeriodFrom() {
		return inviocePeriodFrom;
	}

	/**
	 * sets the inviocePeriodFrom
	 * 
	 * @param inviocePeriodFrom
	 *            The inviocePeriodFrom to set.
	 */
	public void setInviocePeriodFrom(Date inviocePeriodFrom) {
		this.inviocePeriodFrom = inviocePeriodFrom;
	}

	/**
	 * returns the invoicePeriodTo
	 * 
	 * @return Returns the invoicePeriodTo.
	 * @hibernate.property column = "INVOICE_PERIOD_TO"
	 */
	public Date getInvoicePeriodTo() {
		return invoicePeriodTo;
	}

	/**
	 * sets the invoicePeriodTo
	 * 
	 * @param invoicePeriodTo
	 *            The invoicePeriodTo to set.
	 */
	public void setInvoicePeriodTo(Date invoicePeriodTo) {
		this.invoicePeriodTo = invoicePeriodTo;
	}

	/**
	 * returns the invoiceAmount
	 * 
	 * @return Returns the invoiceAmount.
	 * @hibernate.property column = "INVOICE_AMOUNT"
	 */
	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}

	/**
	 * sets the invoiceAmount
	 * 
	 * @param invoiceAmount
	 *            The invoiceAmount to set.
	 */
	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	/**
	 * returns the settledAmount
	 * 
	 * @return Returns the settledAmount.
	 * @hibernate.property column = "SETTLED_AMOUNT"
	 */
	public BigDecimal getSettledAmount() {
		return settledAmount;
	}

	/**
	 * sets the settledAmount
	 * 
	 * @param settledAmount
	 *            The settledAmount to set.
	 */
	public void setSettledAmount(BigDecimal settledAmount) {
		this.settledAmount = settledAmount;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the billingPeriod
	 * 
	 * @return Returns the billingPeriod.
	 * @hibernate.property column = "BILLING_PERIOD"
	 */
	public int getBillingPeriod() {
		return billingPeriod;
	}

	/**
	 * sets the billingPeriod
	 * 
	 * @param billingPeriod
	 *            The billingPeriod to set.
	 */
	public void setBillingPeriod(int billingPeriod) {
		this.billingPeriod = billingPeriod;
	}

	/**
	 * @return Returns the agentCode.
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the transferstatus.
	 * @hibernate.property column = "TRANSFER_STATUS"
	 */
	public String getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	/**
	 * @return Returns the transfertimestamp.
	 * @hibernate.property column = "TRANSFER_TIMESTAMP"
	 */
	public Timestamp getTransferTimestamp() {
		return transferTimestamp;
	}

	public void setTransferTimestamp(Timestamp transferTimestamp) {
		this.transferTimestamp = transferTimestamp;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 * @return Returns the numOfTimesMailSent.
	 * @hibernate.property column = "EMAILS_SENT"
	 */
	public Integer getNumOfTimesMailSent() {
		return numOfTimesMailSent;
	}

	/**
	 * @param numOfTimesMailSent
	 *            The numOfTimesMailSent to set.
	 */
	public void setNumOfTimesMailSent(Integer numOfTimesMailSent) {
		this.numOfTimesMailSent = numOfTimesMailSent;
	}

	/**
	 * @return Returns the mailStatus.
	 * @hibernate.property column = "EMAIL_STATUS"
	 */
	public String getMailStatus() {
		return mailStatus;
	}

	/**
	 * @param mailStatus
	 *            The mailStatus to set.
	 */
	public void setMailStatus(String mailStatus) {
		this.mailStatus = mailStatus;
	}

	/**
	 * @return Returns the mailStatus.
	 * @hibernate.property column = "commission_amount"
	 */
	public BigDecimal getCommissionAmount() {
		return commissionAmount;
	}

	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}

	/**
	 * @return Returns the mailStatus.
	 * @hibernate.property column = "total_fare_amount"
	 */
	public BigDecimal getTotalFareAmount() {
		return totalFareAmount;
	}

	public void setTotalFareAmount(BigDecimal totalFareAmount) {
		this.totalFareAmount = totalFareAmount;
	}

	/**
	 * @return Returns the mailStatus.
	 * @hibernate.property column = "INVOICE_AMOUNT_LOCAL"
	 */
	public BigDecimal getInvoiceAmountLocal() {
		return invoiceAmountLocal;
	}

	public void setInvoiceAmountLocal(BigDecimal invoiceAmountLocal) {
		this.invoiceAmountLocal = invoiceAmountLocal;
	}

	/**
	 * @return Returns the mailStatus.
	 * @hibernate.property column = "ENTITY_CODE"
	 */
	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	/**
	 * @return returns the entity
	 * @hibernate.many-to-one class="com.isa.thinair.invoicing.api.model.Entity"
	 * column="ENTITY_CODE" insert="false" update="false" lazy="false"
	 */
	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}
}