/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * DTO can be use to transfer row invoice-search data date.
 * 
 * @author SanjeewaF JIRA : AARESAA-2363 <br>
 *         Last modified by dumindaG
 * 
 */
public class SearchInvoicesReceiptsDTO implements Serializable {

	private static final long serialVersionUID = 2064232083583201615L;

	private Date txnDate;

	private Integer tnxId;

	private String agentCode;

	private String pnr;

	private int invoicePeriod;

	private Collection<Integer> colTnxIds;

	public Collection getColTnxIds() {
		return colTnxIds;
	}

	public void setColTnxIds(Collection colTnxIds) {
		this.colTnxIds = colTnxIds;
	}

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
		setInvoicePeriod(getInvoicePeriod(txnDate));
	}

	public int getInvoicePeriod() {
		return invoicePeriod;
	}

	public void setInvoicePeriod(int invoicePeriod) {
		this.invoicePeriod = invoicePeriod;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	private static int getInvoicePeriod(Date bookedTime) {

		int date = CalendarUtil.getCalendar(bookedTime).get(Calendar.DATE);
		if (1 <= date && date <= 15) {
			return 1;
		}

		return 2;

	}

	/**
	 * @return Returns the tnxId.
	 */
	public Integer getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            The tnxId to set.
	 */
	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}

}