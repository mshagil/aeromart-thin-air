package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BKP84_FormOfPayment extends BasicBKRecord implements DishTransactional {

	public BKP84_FormOfPayment(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BKP84, "BKP", 84l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);

	}

	public void setStandardNumericQulaifier(String numQualifier) throws BSPException {
		dataContainer.setValue(3, numQualifier);
	}

	public void setIssueDate(Date endingDate) throws BSPException {
		dataContainer.setValue(4, endingDate);
	}

	public void setTrasactionNo(long transactionNo) throws BSPException {
		dataContainer.setValue(5, transactionNo);
	}

	public void setSettlementAuthoriseCode(String code) throws BSPException {
		dataContainer.setValue(6, code);
	}

	public void setReservedSpace(String data) throws BSPException {
		dataContainer.setValue(7, data);

	}

	public void setPaymentAmount(BigDecimal value) throws BSPException {
		dataContainer.setValue(7, value);
	}

	public void setPaymentAccoutNumber(String accNum) throws BSPException {
		dataContainer.setValue(8, accNum);
	}

	public void setExpiryDate(Date expiryDate) throws BSPException {
		dataContainer.setValue(9, expiryDate);
	}

	public void setApprovalCode(String code) throws BSPException {
		dataContainer.setValue(11, code);
	}

	public void setInvoiceNumber(Long number) throws BSPException {
		dataContainer.setValue(12, number);
	}

	public void setInvoiceDate(Date date) throws BSPException {
		dataContainer.setValue(13, date);
	}

	public void setSignedAmount(BigDecimal amount) throws BSPException {
		dataContainer.setValue(14, amount);
	}

	public void setRemitanceAmount(BigDecimal amount) throws BSPException {
		dataContainer.setValue(15, amount);
	}

	public void setCurrencyType(String type) throws BSPException {
		dataContainer.setValue(16, type);
		dataContainer.setValue(19, type);
	}

	public void setVerificationValue(String value) throws BSPException {
		dataContainer.setValue(17, value);

	}

}