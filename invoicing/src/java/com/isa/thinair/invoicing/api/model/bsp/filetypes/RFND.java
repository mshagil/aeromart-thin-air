package com.isa.thinair.invoicing.api.model.bsp.filetypes;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.records.IT03_RelatedTicketInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT05_MonetaryAmounts;
import com.isa.thinair.invoicing.api.model.bsp.records.IT08_FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0Y_AgencyData;

public class RFND extends BasicFileType{

	public RFND() {
		super(DISH_FILE_TYPE.RFND);
	}
	

	public void addRelatedTicketInfo(IT03_RelatedTicketInformation relatedTicketInfo) throws BSPException{
		setTransactionId(relatedTicketInfo);
		addElement(relatedTicketInfo);
	}
	
	public void addMonetaryAmounts(IT05_MonetaryAmounts monetaryAmts) throws BSPException{
		setTransactionId(monetaryAmts);
		addElement(monetaryAmts);
	}
	
	public void addFormOfPayment(IT08_FormOfPayment formOfPayment) throws BSPException{
		setTransactionId(formOfPayment);
		addElement(formOfPayment);
	}
	
	public void addAgencyData(IT0Y_AgencyData agencyData) throws BSPException{
		setTransactionId(agencyData);
		addElement(agencyData);
	}
	

}
