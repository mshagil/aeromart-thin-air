package com.isa.thinair.invoicing.api.model.bsp;


public class ElementFactory {
	/** Format Identifier/Record length **/
	public enum FMT_ID {
		S("S"), /* short */
		L("L"), /* long */
		X("X"), /* extra long */
		NA("");
		/* not applicable */
		
		String str;
		FMT_ID(String str){
			this.str = str;
		}
		public String toString(){
			return str;
		}
	}

	/** data type */
	private enum DT {
		ALPHA, /* alpha */
		ALPHA_NUMERIC, /* alpha numeric */
		NUMERIC
		/* numeric */
	}

	public enum DE_STATUS {
		M, /* mandatory */
		C, /* conditional */
		NA
		/* not applicable */
	}

	public enum DISH_ELEMENT {

		RESD("Reserved space", FMT_ID.NA, 1, DT.ALPHA_NUMERIC), /* RESD element size will vary */

		RCID("Record identifier", FMT_ID.S, 1, DT.ALPHA), 
		SPED("System provider reporting period ending date", FMT_ID.S, 6, DT.NUMERIC), 
		RPSI("Reporting system identifier", FMT_ID.S, 4, DT.ALPHA_NUMERIC), 
		REVN("Handbook revision number", FMT_ID.S, 3, DT.NUMERIC), 
		TPST("Test/Production status", FMT_ID.S, 4, DT.ALPHA_NUMERIC), 
		PRDA("Processing date", FMT_ID.S, 6, DT.NUMERIC),
		TIME("Processing time", FMT_ID.S, 4, DT.NUMERIC),
		ISOC("ISO country code", FMT_ID.S, 2, DT.ALPHA),
		FTYP("File type", FMT_ID.S, 1, DT.ALPHA_NUMERIC),
		FTSN("File type sequence number", FMT_ID.S, 2, DT.ALPHA_NUMERIC),
		TRNN("Transaction number", FMT_ID.S, 6, DT.NUMERIC),		
		AGTN("Agent numeric code", FMT_ID.S, 8, DT.NUMERIC),
		RMED("Remittence end date", FMT_ID.S, 6, DT.NUMERIC),
		GROS("Gross Value Amount", FMT_ID.S, 15, DT.NUMERIC),
		TREM("Gross Value Amount", FMT_ID.S, 15, DT.NUMERIC),
		TCOM("Total Commission Value Amount", FMT_ID.S, 15, DT.NUMERIC),
		TTMF("Total Tax/Miscellaneous Fee Amount", FMT_ID.S, 15, DT.NUMERIC),
		TLRP("Total Late Reporting Penalty", FMT_ID.S, 15, DT.NUMERIC),
		TTCA("Total Tax on Commission Amount", FMT_ID.S, 15, DT.NUMERIC),
		CJCP("Conjunction ticket indicator", FMT_ID.S, 3, DT.ALPHA_NUMERIC),
		CPUI("Coupon use indicator", FMT_ID.S, 4, DT.ALPHA_NUMERIC),
		DAIS("Date of issue", FMT_ID.S, 6, DT.NUMERIC),
		STAT("Statistical code", FMT_ID.S, 3, DT.ALPHA_NUMERIC),
		TDNR("Ticket/Document number", FMT_ID.S, 15, DT.ALPHA_NUMERIC),
		CDGT("Check digit", FMT_ID.S, 1, DT.NUMERIC),
		COBL("Commition amount", FMT_ID.S, 11, DT.NUMERIC),
		NTFA("Net Fare Amount", FMT_ID.S, 11, DT.NUMERIC),
		TRNC("Transaction code", FMT_ID.S, 4, DT.ALPHA_NUMERIC),
		TCNR("Transmission control number", FMT_ID.X, 15, DT.ALPHA_NUMERIC),
		TCND("Transmission control number check digit", FMT_ID.X, 1, DT.NUMERIC),
		TACN("Ticketing airline code number", FMT_ID.S, 5, DT.ALPHA_NUMERIC), 
		FORM("Format identifier", FMT_ID.S, 1, DT.ALPHA_NUMERIC), 
		PXNM("Passenger name", FMT_ID.S, 49, DT.ALPHA_NUMERIC), 
		ALTP("Approved location type", FMT_ID.S, 1, DT.ALPHA_NUMERIC), 
		SCNR("Stock control number", FMT_ID.S, 16, DT.ALPHA_NUMERIC), 
		SCNF("Stock control number from", FMT_ID.S, 16, DT.ALPHA_NUMERIC), 
		SCNT("Stock control number to", FMT_ID.S, 4, DT.NUMERIC), 
		ALNC("Approved location numeric code", FMT_ID.S, 8, DT.NUMERIC), 
		ESAC("Settlement authorization code", FMT_ID.S, 14, DT.ALPHA_NUMERIC), 
		DISI("Data input status indicator", FMT_ID.S, 1, DT.ALPHA_NUMERIC), 
		VISO("Vendor ISO country code", FMT_ID.S, 2, DT.ALPHA), 
		VNID("Vendor identification", FMT_ID.S, 4, DT.NUMERIC), 
		RCPN("Related ticket/document coupon number", FMT_ID.S, 4, DT.NUMERIC), 
		RTDN("Related ticket/document number", FMT_ID.S, 15, DT.ALPHA_NUMERIC), 
		DIRD("Date of issue related document", FMT_ID.S, 6, DT.NUMERIC), 
		TOUR("Tour code", FMT_ID.S, 15, DT.ALPHA_NUMERIC),
		WAVR("Waiver code", FMT_ID.S, 14, DT.ALPHA_NUMERIC),
		RMIC("Reason for memo issuance code", FMT_ID.S, 5, DT.ALPHA_NUMERIC),
		PNRR("PNR Reference and/or Airline reference data", FMT_ID.X, 13, DT.ALPHA_NUMERIC),
		TODC("True Origin/Destionation city codes", FMT_ID.X, 14, DT.ALPHA_NUMERIC),
		TKMI("Ticketing mode indicator", FMT_ID.X, 1, DT.ALPHA_NUMERIC),
		ORIN("Original issue information", FMT_ID.X, 32, DT.ALPHA_NUMERIC),
		FARE("Fare", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		EQFR("Equivalent fare paid", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		TAXA("Tax", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		TOTL("Total", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		NTSI("Neutral ticketing system identifier", FMT_ID.X, 4, DT.ALPHA_NUMERIC),
		SASI("Servicing airline/system provider identifier", FMT_ID.X, 4, DT.ALPHA_NUMERIC),
		CLID("Client identification", FMT_ID.X, 8, DT.ALPHA_NUMERIC),
		BAID("Booking agent identification", FMT_ID.X, 6, DT.ALPHA_NUMERIC),
		PXDA("Passenger specific data", FMT_ID.X, 49, DT.ALPHA_NUMERIC),
		VLNC("Validating location numeric code", FMT_ID.X, 8, DT.NUMERIC),
		BOON("Booking agency/location number", FMT_ID.X, 10, DT.ALPHA_NUMERIC),
		BEOT("Booking entity outlet type", FMT_ID.X, 1, DT.ALPHA_NUMERIC),
		AEBA("Amount entered by agent", FMT_ID.S, 11, DT.NUMERIC),
		TMFT("Tax/Miscellaneous fee type", FMT_ID.S, 8, DT.ALPHA_NUMERIC),
		TMFA("Tax/Miscellaneous fee amount", FMT_ID.S, 11, DT.NUMERIC),
		TDAM("Ticket/document amount", FMT_ID.S, 11, DT.NUMERIC),
		LREP("Late Reporting Penalty", FMT_ID.S, 11, DT.NUMERIC),
		CUTP("Currency type", FMT_ID.S, 4, DT.ALPHA_NUMERIC),
		MLOC("Multi-Location Identifier", FMT_ID.S, 3, DT.ALPHA_NUMERIC),
		TOCA("Tax on commission amount", FMT_ID.S, 11, DT.NUMERIC),
		COTP("Commission type", FMT_ID.S, 6, DT.ALPHA_NUMERIC),
		CORT("Commission rate", FMT_ID.S, 5, DT.NUMERIC),
		COAM("Commission amount", FMT_ID.S, 11, DT.NUMERIC),
		SPTP("Supplementary Type", FMT_ID.S, 6, DT.NUMERIC),
		SPTR("Supplementary Rate", FMT_ID.S, 5, DT.NUMERIC),
		SPAM("Supplementary Amount", FMT_ID.S, 11, DT.NUMERIC),
		EFRT("Effective commition rate", FMT_ID.S,5, DT.NUMERIC),
		EFCO("Effective commition amount", FMT_ID.S,11, DT.NUMERIC),
	
		NRID("Net-reporting indicator", FMT_ID.S, 2, DT.ALPHA_NUMERIC),
		APBC("Amount paid by the customer", FMT_ID.S, 11, DT.NUMERIC),
		TCTP("Tax on commission type", FMT_ID.S, 6, DT.ALPHA_NUMERIC),
		ORAC("Origin airport/city code", FMT_ID.L, 5, DT.ALPHA),
		DSTC("Destination", FMT_ID.L, 5, DT.ALPHA),
		FFRF("Frequent flyer reference", FMT_ID.X, 16, DT.ALPHA_NUMERIC),
		CARR("Carrier", FMT_ID.L, 4, DT.ALPHA_NUMERIC),
		RBKD("Reservation booking designator", FMT_ID.L, 2, DT.ALPHA_NUMERIC),
		FTDA("Flight date", FMT_ID.L, 5, DT.ALPHA_NUMERIC),
		NBDA("Not valid before date", FMT_ID.X, 5, DT.ALPHA_NUMERIC),
		NADA("Not valid after date", FMT_ID.X, 5, DT.ALPHA_NUMERIC),
		FBTD("Fare basis ticket designator", FMT_ID.L, 15, DT.ALPHA_NUMERIC),
		FTNR("Flight number", FMT_ID.L, 5, DT.ALPHA_NUMERIC),
		FTDT("Flight departure time", FMT_ID.X, 5, DT.ALPHA_NUMERIC),
		FBAL("Free baggage allowance", FMT_ID.X, 3, DT.ALPHA_NUMERIC),
		FBST("Flight booking status", FMT_ID.X, 2, DT.ALPHA),
		SEGI("Segment identifier", FMT_ID.L, 1, DT.NUMERIC),
		STPO("Stopover code", FMT_ID.L, 1, DT.ALPHA),
		EMCP("EMD coupon number", FMT_ID.X, 1, DT.NUMERIC),
		EMCV("EMD coupon value", FMT_ID.X, 11, DT.NUMERIC),
		EMRT("EMD related ticket number", FMT_ID.X, 15, DT.ALPHA_NUMERIC),
		EMRC("EMD related coupon number", FMT_ID.X, 1, DT.NUMERIC),
		EMSC("EMD reason for issuance of subcode", FMT_ID.X, 3, DT.ALPHA_NUMERIC),
		EMOC("EMD free owner airline designator", FMT_ID.X, 2, DT.ALPHA_NUMERIC),
		XBOA("EMD excess baggage over allowance qualifier", FMT_ID.X, 1, DT.ALPHA_NUMERIC),
		XBRU("EMD excess baggage rate per unit", FMT_ID.X, 12, DT.ALPHA_NUMERIC),
		XBNE("EMD excess baggage total number in excess", FMT_ID.X, 12, DT.ALPHA_NUMERIC),
		EMRM("EMD remarks", FMT_ID.X, 70, DT.ALPHA_NUMERIC),
		EMCI("EMD consumed at the issuance indicator", FMT_ID.X, 1, DT.ALPHA_NUMERIC),	
		XBCT("EMD excess baggage currency type", FMT_ID.X, 3, DT.ALPHA),
		FRCA("Fare calculation area", FMT_ID.X, 87, DT.ALPHA_NUMERIC),
		FRCS("Fare calculation sequence number", FMT_ID.X, 1, DT.NUMERIC),
		FCMI("Fare calculation mode indicator", FMT_ID.X, 1, DT.ALPHA_NUMERIC),
		FCPI("Fare calculation pricing indicator", FMT_ID.X, 1, DT.ALPHA_NUMERIC),
		FPAC("Form of payment account number", FMT_ID.S, 19, DT.ALPHA_NUMERIC),
		FPAM("Form of payment amount", FMT_ID.S, 11, DT.NUMERIC),
		APLC("Approval code", FMT_ID.S, 6, DT.ALPHA_NUMERIC),
		EXPC("Extended payment code", FMT_ID.S, 2, DT.ALPHA_NUMERIC),
		FPTP("Form of payment type", FMT_ID.S, 10, DT.ALPHA_NUMERIC),
		EXDA("Expiry date", FMT_ID.S, 4, DT.ALPHA_NUMERIC),
		CSTF("Customer file reference", FMT_ID.S, 27, DT.ALPHA_NUMERIC),
		CRCC("Credit card corporate contact", FMT_ID.S, 1, DT.ALPHA_NUMERIC),
		AVCD("Address verification code", FMT_ID.S, 2, DT.ALPHA_NUMERIC),
		SAPP("Source of approval code", FMT_ID.S, 1, DT.ALPHA_NUMERIC),
		FPTI("Form of payment transaction information", FMT_ID.S, 25, DT.ALPHA_NUMERIC),
		AUTA("Authorized amount", FMT_ID.S, 11, DT.ALPHA_NUMERIC),
		CVVR("Card verification value result", FMT_ID.S, 1, DT.ALPHA_NUMERIC),
		AGTA("Agency data/agent use", FMT_ID.X, 20, DT.ALPHA_NUMERIC),
		AGTO("Agency data/receiving order code", FMT_ID.X, 12, DT.ALPHA_NUMERIC),
		AGTS("Agency data/section code", FMT_ID.X, 6, DT.ALPHA_NUMERIC),
		AGTU("Agency data/sales staff code", FMT_ID.X, 6, DT.ALPHA_NUMERIC),
		AGTR("Agency data/routing code", FMT_ID.X, 3, DT.ALPHA_NUMERIC),
		AGTC("Agency data/customer code", FMT_ID.X, 10, DT.ALPHA_NUMERIC),
		AGTP("Agency data/paying in code", FMT_ID.X, 1, DT.ALPHA_NUMERIC),
		AGTF("Agency data/free code", FMT_ID.X, 10, DT.ALPHA_NUMERIC),
		AGTT("Agency data/package tour code", FMT_ID.X, 15, DT.ALPHA_NUMERIC),
		AGTK("Agency data/rebate code 1", FMT_ID.X, 7, DT.ALPHA_NUMERIC),
		AGTB("Agency data/rebate code 2", FMT_ID.X, 7, DT.ALPHA_NUMERIC),
		AGTE("Agency data/pricing code", FMT_ID.X, 7, DT.ALPHA_NUMERIC),
		CCOC("Credit card company data/Organisation code", FMT_ID.X, 2, DT.ALPHA_NUMERIC),
		CCEN("Credit card company data/Other expense number", FMT_ID.X, 14, DT.ALPHA_NUMERIC),
		CCRM("Credit card company data/remarks", FMT_ID.X, 10, DT.ALPHA_NUMERIC),
		CCHN("Credit card company data/card holder's name", FMT_ID.X, 26, DT.ALPHA_NUMERIC),
		SFAM("Signed for amount", FMT_ID.S, 11, DT.NUMERIC),
		ENRS("Endorsements/Restrictions", FMT_ID.X, 147, DT.ALPHA_NUMERIC),
		FPIN("Form of payment information", FMT_ID.X, 50, DT.ALPHA_NUMERIC),
		RFID("Reason for issuance description", FMT_ID.X, 86, DT.ALPHA_NUMERIC),
		BERA("Bank exchange rate", FMT_ID.X, 13, DT.ALPHA_NUMERIC),
		ICDN("In connection with document number", FMT_ID.X, 15, DT.ALPHA_NUMERIC),
		AMIL("Amount in letters", FMT_ID.X, 72, DT.ALPHA_NUMERIC),
		RFIC("Reason for issuance code", FMT_ID.X, 1, DT.ALPHA_NUMERIC),
		MPOC("Multiple Purpose Document- Other charges", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		MPEQ("Multiple Purpose Document- Equivalent", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		MPEV("Multiple Purpose Document- Exchange value", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		MPSC("Multiple Purpose Document- Service charge", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		MPTX("Multiple Purpose Document- Ticket total", FMT_ID.X, 11, DT.ALPHA_NUMERIC),
		OAAI("Optional Agency/Airline Information", FMT_ID.X, 88, DT.ALPHA_NUMERIC),
		PLID("Print line identifier", FMT_ID.X, 1, DT.ALPHA_NUMERIC),
		PLTX("Print line text", FMT_ID.X, 86, DT.ALPHA_NUMERIC),
		CPNR("Coupon number", FMT_ID.X, 1, DT.NUMERIC),
		SPIN("Sponsor information", FMT_ID.X, 34, DT.ALPHA_NUMERIC),
		RRDC("Report record counter", FMT_ID.S, 11, DT.NUMERIC),
		SMSG("Standard Message Identifier",FMT_ID.X,3,DT.ALPHA_NUMERIC),
		SQNR("Sequence Number",FMT_ID.X,8,DT.NUMERIC),
		STNQ("Standard Numeric Qualifier",FMT_ID.X,2,DT.NUMERIC),
		BSPI("BSP Identifier",FMT_ID.X,3,DT.ALPHA_NUMERIC),
		RSQN("Reserved Space",FMT_ID.X,90,DT.ALPHA_NUMERIC),
		TREF("Transaction Reference Number",FMT_ID.X,18,DT.ALPHA_NUMERIC),
		TREC("Transaction Record Counter",FMT_ID.X,3,DT.NUMERIC),
		AUDI("Audit Status Indicator",FMT_ID.X,1,DT.ALPHA_NUMERIC),
		CARF("Commercial Agreement Reference",FMT_ID.X,10,DT.ALPHA_NUMERIC),
		FSQN("File Sequence Number",FMT_ID.X,6,DT.NUMERIC),
		FPSN("Form of Payment Sequence Number",FMT_ID.X,1,DT.NUMERIC),
		//ETLR relevent DISH ELEMENTS
		CHR01("ETLR Char Records",FMT_ID.X,1,DT.ALPHA_NUMERIC),
		CHR02("ETLR Char Records",FMT_ID.X,2,DT.ALPHA_NUMERIC),
		CHR03("ETLR Char Records",FMT_ID.X,3,DT.ALPHA_NUMERIC),
		CHR04("ETLR Char Records",FMT_ID.X,4,DT.ALPHA_NUMERIC),
		CHR05("ETLR Char Records",FMT_ID.X,5,DT.ALPHA_NUMERIC),
		CHR06("ETLR Char Records",FMT_ID.X,6,DT.ALPHA_NUMERIC),
		CHR07("ETLR Char Records",FMT_ID.X,7,DT.ALPHA_NUMERIC),
		CHR08("ETLR Char Records",FMT_ID.X,8,DT.ALPHA_NUMERIC),
		CHR09("ETLR Char Records",FMT_ID.X,9,DT.ALPHA_NUMERIC),
		CHR10("ETLR Char Records",FMT_ID.X,10,DT.ALPHA_NUMERIC),
		CHR11("ETLR Char Records",FMT_ID.X,11,DT.ALPHA_NUMERIC),
		CHR12("ETLR Char Records",FMT_ID.X,12,DT.ALPHA_NUMERIC),
		CHR13("ETLR Char Records",FMT_ID.X,13,DT.ALPHA_NUMERIC),
		CHR14("ETLR Char Records",FMT_ID.X,14,DT.ALPHA_NUMERIC),
		CHR15("ETLR Char Records",FMT_ID.X,15,DT.ALPHA_NUMERIC),
		CHR16("ETLR Char Records",FMT_ID.X,16,DT.ALPHA_NUMERIC),
		CHR17("ETLR Char Records",FMT_ID.X,17,DT.ALPHA_NUMERIC),
		CHR18("ETLR Char Records",FMT_ID.X,18,DT.ALPHA_NUMERIC),
		CHR19("ETLR Char Records",FMT_ID.X,19,DT.ALPHA_NUMERIC),
		CHR20("ETLR Char Records",FMT_ID.X,20,DT.ALPHA_NUMERIC),
		CHR21("ETLR Char Records",FMT_ID.X,21,DT.ALPHA_NUMERIC),
		CHR25("ETLR Char Records",FMT_ID.X,25,DT.ALPHA_NUMERIC),
		CHR35("ETLR Char Records",FMT_ID.X,35,DT.ALPHA_NUMERIC),
		CHR40("ETLR Char Records",FMT_ID.X,40,DT.ALPHA_NUMERIC),
		CHR90("ETLR Char Records",FMT_ID.X,90,DT.ALPHA_NUMERIC),
		CHR95("ETLR Char Records",FMT_ID.X,95,DT.ALPHA_NUMERIC),
		CHR720("ETLR Char Records",FMT_ID.X,720,DT.ALPHA_NUMERIC),
		NUMARIC04("ETLR decimal Records",FMT_ID.X,4,DT.NUMERIC),
		NUMARIC05("ETLR decimal Records",FMT_ID.X,5,DT.NUMERIC),
		NUMARIC07("ETLR decimal Records",FMT_ID.X,7,DT.NUMERIC),
		NUMARIC09("ETLR decimal Records",FMT_ID.X,9,DT.NUMERIC),
		NUMARIC11("ETLR decimal Records",FMT_ID.X,11,DT.NUMERIC),
		NUMARIC17("ETLR decimal Records",FMT_ID.X,17,DT.NUMERIC);
		
		private String description;
		private FMT_ID recordLength;
		private DT dataType;
		private int dtLength;

		DISH_ELEMENT(String desc, FMT_ID recLen, int dtLength, DT dataType) {
			this.description = desc;
			this.recordLength = recLen;
			this.dataType = dataType;
			this.dtLength = dtLength;
		}

		public String getDescription() {
			return this + ": " + this.description;
		}

		public FMT_ID getRecordLength() {
			return recordLength;
		}

		public DT getDataType() {
			return dataType;
		}

		public int getDtLength() {
			return dtLength;
		}
	}

	public static BasicDataType getElement(DISH_ELEMENT element, int length) {
		BasicDataType dataType = null;
		switch (element.dataType) {
		case ALPHA_NUMERIC:
			dataType = new AlphaNumeric(length);
			break;
		case ALPHA:
			dataType = new Alpha(length);
			break;
		case NUMERIC:
			dataType = new Numeric(length);
			break;
		}
		return dataType;
	}

	public static BasicDataType getElement(DISH_ELEMENT element) {
		return getElement(element, element.dtLength);
	}

}
