/**
 * 
 */
package com.isa.thinair.invoicing.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * BSPHOTFileLog is the entity class to represent BSP Hot file log
 * 
 * @hibernate.class table = "T_BSP_HOT_FILE_LOG"
 */
public class BSPHOTFileLog extends Persistent implements Serializable {
	
	private Integer logID;
	
	private String fileName;
	
	private String fileStatus;
	
	private String uploadedBy;
	
	private Date uploadedOn;
	
	private Date dpcProcessedDate;
	
	private String processedBy;
	
	private Date processedOn;
	
	public enum HOT_FILE_STATUS {
		UPLOADED, PROCESSED
	};

	
	/**
	 * @return Returns the logID.
	 * 
	 * @hibernate.id column="BSP_HOT_FILE_LOG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BSP_HOT_FILE_LOG"
	 */
	public Integer getLogID() {
		return logID;
	}

	public void setLogID(Integer logID) {
		this.logID = logID;
	}

	/**
	 * @return Returns the fileName.
	 * @hibernate.property column = "FILE_NAME"
	 */
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return Returns the fileStatus.
	 * @hibernate.property column = "FILE_STATUS"
	 */
	public String getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	/**
	 * @return Returns the uploadedBy.
	 * @hibernate.property column = "UPLOADED_BY"
	 */
	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	/**
	 * @return Returns the uploadedOn.
	 * @hibernate.property column = "UPLOADED_ON"
	 */
	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	/**
	 * @return Returns the dpcProcessedDate.
	 * @hibernate.property column = "DPC_PROCESSED_DATE"
	 */
	public Date getDpcProcessedDate() {
		return dpcProcessedDate;
	}

	public void setDpcProcessedDate(Date dpcProcessedDate) {
		this.dpcProcessedDate = dpcProcessedDate;
	}

	/**
	 * @return Returns the processedBy.
	 * @hibernate.property column = "PROCESSED_BY"
	 */
	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	/**
	 * @return Returns the processedOn.
	 * @hibernate.property column = "PROCESSED_ON"
	 */
	public Date getProcessedOn() {
		return processedOn;
	}

	public void setProcessedOn(Date processedOn) {
		this.processedOn = processedOn;
	}
}
