/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X40_Commission_Data extends BasicRecord {

	public X40_Commission_Data(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X40);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(String sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator);
	}

	public void setCOMMISS_SEQ_NUM(String commissionSeqNumber) throws BSPException {
		dataContainer.setValue(5, commissionSeqNumber);
	}

	public void setCOMMISS_RATE(String commissionRate) throws BSPException {
		dataContainer.setValue(6, commissionRate);
	}

	public void setCOMMISS_AMT(String commissionAmount) throws BSPException {
		dataContainer.setValue(7, commissionAmount);
	}

	public void setCOMMISS_CURR_CODE(String commissionCurrencyCode) throws BSPException {
		dataContainer.setValue(8, commissionCurrencyCode);
	}

	public void setCOMMISS_TAX_INDIC(String commissionTaxIndicator) throws BSPException {
		dataContainer.setValue(9, commissionTaxIndicator);
	}

	public void setCOMMISS_TYPE(String commissionType) throws BSPException {
		dataContainer.setValue(10, commissionType);
	}

}
