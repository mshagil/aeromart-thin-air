package com.isa.thinair.invoicing.api.model.bsp;

public class BSPAgentDataDTO {
	private String agentCode;

	private String iataCode;

	private String station_code;

	private Long accountCode;

	private String countaryCode;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getIataCode() {
		return iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getStation_code() {
		return station_code;
	}

	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}

	public Long getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCodeStr) {
		if (accountCodeStr.matches("\\d*")) {
			this.accountCode = Long.parseLong(accountCodeStr);
		} else {
			this.accountCode = 0l;
		}
	}

	public String getCountaryCode() {
		return countaryCode;
	}

	public void setCountaryCode(String countaryCode) {
		this.countaryCode = countaryCode;
	}
}
