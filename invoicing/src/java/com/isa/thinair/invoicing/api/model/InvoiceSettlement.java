/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Chamindap
 * @hibernate.class table = "T_INVOICE_SETTLEMENT" InvoiceSettlement is the entity class to repesent a InvoiceSettlement
 *                  model
 * 
 */
public class InvoiceSettlement extends Persistent implements Serializable {

	private static final long serialVersionUID = 4853926627826235451L;

	public static final String ENTRY_TYPE_CR = "CR";

	public static final String ENTRY_TYPE_DR = "DR";

	private int settllementId;

	private Date settlementDate;

	private BigDecimal amountPaid;

	private String entryType;

	private String invoiceNo;

	private String cpCode;

	private String reason;

	private String userId;

	/**
	 * The constructor.
	 */
	public InvoiceSettlement() {
		super();
	}

	/**
	 * returns the settllementId
	 * 
	 * @return Returns the settllementId.
	 * @hibernate.id column = "SETTLLEMENT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_INVOICE_SETTLEMENT_STLMNT_ID"
	 */
	public int getSettllementId() {
		return settllementId;
	}

	/**
	 * sets the settllementId
	 * 
	 * @param settllementId
	 *            The settllementId to set.
	 */
	public void setSettllementId(int settllementId) {
		this.settllementId = settllementId;
	}

	/**
	 * returns the settlementDate
	 * 
	 * @return Returns the settlementDate.
	 * @hibernate.property column = "SETTLEMENT_DATE"
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}

	/**
	 * sets the settlementDate
	 * 
	 * @param settlementDate
	 *            The settlementDate to set.
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	/**
	 * returns the amountPaid
	 * 
	 * @return Returns the amountPaid.
	 * @hibernate.property column = "AMOUNT_PAID"
	 */
	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	/**
	 * sets the amountPaid
	 * 
	 * @param amountPaid
	 *            The amountPaid to set.
	 */
	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	/**
	 * returns the entryType
	 * 
	 * @return Returns the entryType.
	 * @hibernate.property column = "ENTRY_TYPE"
	 */
	public String getEntryType() {
		return entryType;
	}

	/**
	 * sets the entryType
	 * 
	 * @param entryType
	 *            The entryType to set.
	 */
	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}

	/**
	 * @return Returns the cpId.
	 * @hibernate.property column = "CP_CODE"
	 */
	public String getCpCode() {
		return cpCode;
	}

	/**
	 * @param cpId
	 *            The cpId to set.
	 */
	public void setCpCode(String cpCode) {
		this.cpCode = cpCode;
	}

	/**
	 * @return Returns the invoiceNo.
	 * @hibernate.property column = "INVOICE_NUMBER"
	 */
	public String getInvoiceNo() {
		return invoiceNo;
	}

	/**
	 * @param invoiceNo
	 *            The invoiceNo to set.
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 * @return Returns the reason.
	 * @hibernate.property column = "REASON"
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            The reason to set.
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

}