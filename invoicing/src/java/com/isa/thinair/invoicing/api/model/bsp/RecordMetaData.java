package com.isa.thinair.invoicing.api.model.bsp;

import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class RecordMetaData {
	private int min;
	private int max;
	private ElementBuilder elementBuilder;
	private DISH_RECORD_TYPE recordType;
	public final static int UNLIMITED = -1;

	public RecordMetaData(DISH_RECORD_TYPE recordType, ElementBuilder elementBuilder, int min, int max) {
		this.min = min;
		this.max = max;
		this.elementBuilder = elementBuilder;
		this.recordType = recordType;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	public ElementBuilder getElementBuilder() {
		return elementBuilder;
	}

	public DISH_RECORD_TYPE getRecordType() {
		return recordType;
	}
}