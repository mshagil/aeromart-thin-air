package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.FMT_ID;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.ApprovedLocationType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.ConjuctionTktIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.CouponUseIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.DataInputStatusIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.StatisticalCodeType;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.utils.BSPApiUtils;

public class IT02_BasicTransaction extends BasicRecord implements DishTransactional {

	private final static int MAX_STOCK_CONTROL_NUMBER_SETS = 4;

	private int currentStockControlCount = 0;

	public IT02_BasicTransaction(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT02);
		setTransactionCode(fileType);
		setFormatIdentifier(fileType.getFormatId());
	}

	public IT02_BasicTransaction(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType) throws BSPException {
		super(fileType, recordType);
		setTransactionCode(fileType);
		setFormatIdentifier(fileType.getFormatId());
	}

	public void setTransactionNo(Long id) throws BSPException {
		dataContainer.setValue(2, id);
	}

	public void setAgentCode(Long id) throws BSPException {
		dataContainer.setValue(3, id);
	}

	public void setConjunctionTicketIndicator(ConjuctionTktIndicator conjunctionTicketIndicator) throws BSPException {
		dataContainer.setValue(4, conjunctionTicketIndicator.toString());
	}

	/**
	 * SHOULD NOT BE SET OF RFND - Refer DISH 20.1
	 * 
	 * @param couponUseIndicator
	 * @throws BSPException
	 */
	public void setCouponUseIndicator(CouponUseIndicator[] couponUseIndicator) throws BSPException {
		StringBuilder couponStr = new StringBuilder();
		if (couponUseIndicator != null && couponUseIndicator.length == 4) {
			for (CouponUseIndicator cui : couponUseIndicator) {
				couponStr.append(cui);
			}
		} else {
			throw new BSPException("Invalid no of coupons indicators ");
		}
		dataContainer.setValue(5, couponStr.toString());
	}

	public void setDateOfIssue(Date dateOfIssue) throws BSPException {
		dataContainer.setValue(6, dateOfIssue);
	}

	public void setStatisticalCode(StatisticalCodeType statisticalCodeType) throws BSPException {
		dataContainer.setValue(7, statisticalCodeType.getType());
	}

	/**
	 * This is the e-ticket no <br />
	 * e.g. 9862400000000 (986-airline code, 24 - iata defined range , rest sequence)
	 * 
	 * @param ticketDocumentNo
	 * @throws BSPException
	 */
	public void setTicketDocumentNo(String ticketDocumentNo) throws BSPException {
		dataContainer.setValue(8, ticketDocumentNo);
		dataContainer.setValue(9, BSPApiUtils.calculateMod7CheckBit(ticketDocumentNo));
	}

	private void setTransactionCode(DISH_FILE_TYPE transactionCode) throws BSPException {
		dataContainer.setValue(10, transactionCode.toString());
	}

	public void setTransmissionCtrlNo(String transmissionCtrlNo) throws BSPException {
		dataContainer.setValue(11, transmissionCtrlNo);
		dataContainer.setValue(12, BSPApiUtils.calculateMod7CheckBit(transmissionCtrlNo));
	}

	public void setTicketingAirlineCodeNo(String ticketingAirlineCodeNo) throws BSPException {
		dataContainer.setValue(13, ticketingAirlineCodeNo);
		dataContainer.setValue(14, BSPApiUtils.calculateMod7CheckBit(ticketingAirlineCodeNo));
	}

	private void setFormatIdentifier(FMT_ID formatIdentifier) throws BSPException {
		dataContainer.setValue(15, formatIdentifier.toString());
	}

	public void setPassengerName(String passengerName) throws BSPException {
		dataContainer.setValue(16, passengerName, true);
	}

	public void setApprovedLocationType(ApprovedLocationType approvedLocationType) throws BSPException {
		dataContainer.setValue(17, approvedLocationType.toString());
	}

	public void setStockNumber(String stockNumber) throws BSPException {
		dataContainer.setValue(18, stockNumber);
	}

	public void addStockControlNumber(String stockCtrlNoFrom, Long stockCtrlNoTo) throws BSPException {
		dataContainer.setValue(18, stockCtrlNoFrom);
		dataContainer.setValue(19, stockCtrlNoTo);
		currentStockControlCount++;
	}

	public void addStockControlNumberSet(Long approvedLocationCode, String approvedLocationType, String stockCtrlNoFrom,
			Long stockCtrlNoTo) throws BSPException {
		if (currentStockControlCount < 1) {
			throw new BSPException("First set of stock numbers is not added!!");
		}
		if (MAX_STOCK_CONTROL_NUMBER_SETS <= currentStockControlCount) {
			throw new BSPException("Max number of stock allowed exceeded");
		}
		int offset = (currentStockControlCount - 1) * 4;
		dataContainer.setValue(20 + offset, approvedLocationCode);
		dataContainer.setValue(21 + offset, approvedLocationType);
		dataContainer.setValue(22 + offset, stockCtrlNoFrom);
		dataContainer.setValue(23 + offset, stockCtrlNoTo);
		currentStockControlCount++;
	}

	public void setSettlementAuthorizationCode(String settlementAuthorizationCode) throws BSPException {
		dataContainer.setValue(32, settlementAuthorizationCode);
	}

	public void setDataInputStatusIndicator(DataInputStatusIndicator dataInputStatusIndicator) throws BSPException {
		dataContainer.setValue(33, dataInputStatusIndicator.toString());
	}

	public void setIsoCountryCode(String isoCountryCode) throws BSPException {
		dataContainer.setValue(34, isoCountryCode);
	}

	public void setVendorIdentification(String vendorIdentification) throws BSPException {
		dataContainer.setValue(35, vendorIdentification);
	}
}
