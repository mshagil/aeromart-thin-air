package com.isa.thinair.invoicing.api.model.bsp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DE_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DISH_ELEMENT;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.FMT_ID;

public class RecordFactory {
	private static RecordFactory recordFactory;

	private Map<DISH_FILE_TYPE, RecordBuilder> recordFileMap = new HashMap<RecordFactory.DISH_FILE_TYPE, RecordBuilder>();

	private RecordFactory() throws BSPException {

		/* TKTT/TKTA/TKTB element section */
		/* will be used as the base for creating others (by cloning) */
		ElementBuilder it01 = new ElementBuilder();
		it01.addElement(DISH_ELEMENT.RCID);
		it01.addElement(DISH_ELEMENT.SPED);
		it01.addElement(DISH_ELEMENT.RPSI);
		it01.addElement(DISH_ELEMENT.REVN);
		it01.addElement(DISH_ELEMENT.TPST);
		it01.addElement(DISH_ELEMENT.PRDA);
		it01.addElement(DISH_ELEMENT.TIME);
		it01.addElement(DISH_ELEMENT.ISOC);
		it01.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 6);
		it01.addElement(DISH_ELEMENT.FTYP, DE_STATUS.C);
		it01.addElement(DISH_ELEMENT.FTSN, DE_STATUS.C);
		it01.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 216);

		ElementBuilder it02 = new ElementBuilder();
		it02.addElement(DISH_ELEMENT.RCID);
		it02.addElement(DISH_ELEMENT.TRNN);
		it02.addElement(DISH_ELEMENT.AGTN);
		it02.addElement(DISH_ELEMENT.CJCP, DE_STATUS.C);
		it02.addElement(DISH_ELEMENT.CPUI);
		it02.addElement(DISH_ELEMENT.DAIS);
		it02.addElement(DISH_ELEMENT.STAT);
		it02.addElement(DISH_ELEMENT.TDNR);
		it02.addElement(DISH_ELEMENT.CDGT);
		it02.addElement(DISH_ELEMENT.TRNC);
		it02.addElement(DISH_ELEMENT.TCNR, DE_STATUS.C);
		it02.addElement(DISH_ELEMENT.TCND, DE_STATUS.C);
		it02.addElement(DISH_ELEMENT.TACN);
		it02.addElement(DISH_ELEMENT.CDGT);
		it02.addElement(DISH_ELEMENT.FORM);
		it02.addElement(DISH_ELEMENT.PXNM);
		// setting default status to Conditional
		it02.setDefaultStatus(DE_STATUS.C);
		it02.addElement(DISH_ELEMENT.ALTP);
		it02.addElements(DISH_ELEMENT.SCNR, DISH_ELEMENT.SCNF);
		it02.addElement(DISH_ELEMENT.SCNT);
		// second stock control number set
		it02.addElement(DISH_ELEMENT.ALNC);
		it02.addElement(DISH_ELEMENT.ALTP);
		it02.addElement(DISH_ELEMENT.SCNF);
		it02.addElement(DISH_ELEMENT.SCNT);
		// third stock control number set
		it02.addElement(DISH_ELEMENT.ALNC);
		it02.addElement(DISH_ELEMENT.ALTP);
		it02.addElement(DISH_ELEMENT.SCNF);
		it02.addElement(DISH_ELEMENT.SCNT);
		// fourth stock control number set
		it02.addElement(DISH_ELEMENT.ALNC);
		it02.addElement(DISH_ELEMENT.ALTP);
		it02.addElement(DISH_ELEMENT.SCNF);
		it02.addElement(DISH_ELEMENT.SCNT);

		it02.addElement(DISH_ELEMENT.ESAC, DE_STATUS.NA);
		it02.addElement(DISH_ELEMENT.DISI, DE_STATUS.NA);
		it02.addElement(DISH_ELEMENT.ISOC, DE_STATUS.M);
		it02.addElement(DISH_ELEMENT.VISO);
		it02.addElement(DISH_ELEMENT.VNID);
		it02.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 1);

		ElementBuilder it0S = new ElementBuilder();
		it0S.addElement(DISH_ELEMENT.RCID);
		it0S.addElement(DISH_ELEMENT.TRNN);
		// 1st set of additional stock numbers
		it0S.addElement(DISH_ELEMENT.ALNC);
		it0S.addElement(DISH_ELEMENT.ALTP);
		it0S.addElement(DISH_ELEMENT.SCNF);
		it0S.addElement(DISH_ELEMENT.SCNT);
		// 2nd set of additional stock numbers
		it0S.setDefaultStatus(DE_STATUS.C);
		it0S.addElement(DISH_ELEMENT.ALNC);
		it0S.addElement(DISH_ELEMENT.ALTP);
		it0S.addElement(DISH_ELEMENT.SCNF);
		it0S.addElement(DISH_ELEMENT.SCNT);
		// 3rd set of additional stock numbers
		it0S.addElement(DISH_ELEMENT.ALNC);
		it0S.addElement(DISH_ELEMENT.ALTP);
		it0S.addElement(DISH_ELEMENT.SCNF);
		it0S.addElement(DISH_ELEMENT.SCNT);
		// 4th set of additional stock numbers
		it0S.addElement(DISH_ELEMENT.ALNC);
		it0S.addElement(DISH_ELEMENT.ALTP);
		it0S.addElement(DISH_ELEMENT.SCNF);
		it0S.addElement(DISH_ELEMENT.SCNT);
		// 5th set of additional stock numbers
		it0S.addElement(DISH_ELEMENT.ALNC);
		it0S.addElement(DISH_ELEMENT.ALTP);
		it0S.addElement(DISH_ELEMENT.SCNF);
		it0S.addElement(DISH_ELEMENT.SCNT);
		// 6th set of additional stock numbers
		it0S.addElement(DISH_ELEMENT.ALNC);
		it0S.addElement(DISH_ELEMENT.ALTP);
		it0S.addElement(DISH_ELEMENT.SCNF);
		it0S.addElement(DISH_ELEMENT.SCNT);
		// 7th set of additional stock numbers
		it0S.addElement(DISH_ELEMENT.ALNC);
		it0S.addElement(DISH_ELEMENT.ALTP);
		it0S.addElement(DISH_ELEMENT.SCNF);
		it0S.addElement(DISH_ELEMENT.SCNT);
		// 8th set of additional stock numbers
		it0S.addElement(DISH_ELEMENT.ALNC);
		it0S.addElement(DISH_ELEMENT.ALTP);
		it0S.addElement(DISH_ELEMENT.SCNF);
		it0S.addElement(DISH_ELEMENT.SCNT);
		it0S.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 16);

		ElementBuilder it03 = new ElementBuilder();
		it03.addElement(DISH_ELEMENT.RCID);
		it03.addElement(DISH_ELEMENT.TRNN);
		// first related ticket document
		it03.addElement(DISH_ELEMENT.RCPN, DE_STATUS.C);
		it03.addElement(DISH_ELEMENT.RTDN);
		it03.addElement(DISH_ELEMENT.CDGT);
		// change the default status to C
		it03.setDefaultStatus(DE_STATUS.C);
		// second related ticket document
		it03.addElement(DISH_ELEMENT.RCPN);
		it03.addElement(DISH_ELEMENT.RTDN);
		it03.addElement(DISH_ELEMENT.CDGT);
		// third related ticket document
		it03.addElement(DISH_ELEMENT.RCPN);
		it03.addElement(DISH_ELEMENT.RTDN);
		it03.addElement(DISH_ELEMENT.CDGT);
		// fourth related ticket document
		it03.addElement(DISH_ELEMENT.RCPN);
		it03.addElement(DISH_ELEMENT.RTDN);
		it03.addElement(DISH_ELEMENT.CDGT);
		// fifth related ticket document
		it03.addElement(DISH_ELEMENT.RCPN);
		it03.addElement(DISH_ELEMENT.RTDN);
		it03.addElement(DISH_ELEMENT.CDGT);
		// sixth related ticket document
		it03.addElement(DISH_ELEMENT.RCPN);
		it03.addElement(DISH_ELEMENT.RTDN);
		it03.addElement(DISH_ELEMENT.CDGT);
		// seventh related ticket document
		it03.addElement(DISH_ELEMENT.RCPN);
		it03.addElement(DISH_ELEMENT.RTDN);
		it03.addElement(DISH_ELEMENT.CDGT);
		it03.addElement(DISH_ELEMENT.DIRD);
		it03.addElement(DISH_ELEMENT.TOUR);
		it03.addElement(DISH_ELEMENT.WAVR);
		it03.addElement(DISH_ELEMENT.RESD, DE_STATUS.NA, 73);

		ElementBuilder it04 = new ElementBuilder(FMT_ID.X, DE_STATUS.C);
		it04.addElement(DISH_ELEMENT.RCID, FMT_ID.S, DE_STATUS.M);
		it04.addElement(DISH_ELEMENT.TRNN, FMT_ID.S, DE_STATUS.M);
		it04.addElement(DISH_ELEMENT.PNRR);
		it04.addElement(DISH_ELEMENT.TODC);
		it04.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 4);
		it04.addElement(DISH_ELEMENT.TKMI, DE_STATUS.M);
		it04.addElement(DISH_ELEMENT.ORIN);
		it04.addElement(DISH_ELEMENT.TOUR, FMT_ID.S);
		it04.addElement(DISH_ELEMENT.FARE, DE_STATUS.M);
		it04.addElement(DISH_ELEMENT.EQFR);
		it04.addElement(DISH_ELEMENT.TAXA);
		it04.addElement(DISH_ELEMENT.TAXA);
		it04.addElement(DISH_ELEMENT.TAXA);
		it04.addElement(DISH_ELEMENT.TOTL, DE_STATUS.M);
		it04.addElement(DISH_ELEMENT.NTSI);
		it04.addElement(DISH_ELEMENT.SASI);
		it04.addElement(DISH_ELEMENT.CLID);
		it04.addElement(DISH_ELEMENT.BAID);
		it04.addElement(DISH_ELEMENT.PXDA);
		it04.addElement(DISH_ELEMENT.VLNC);
		it04.addElement(DISH_ELEMENT.BOON);
		it04.addElement(DISH_ELEMENT.BEOT);
		it04.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 13);

		ElementBuilder it05 = new ElementBuilder(DE_STATUS.C);
		it05.addElement(DISH_ELEMENT.RCID, DE_STATUS.M);
		it05.addElement(DISH_ELEMENT.TRNN, DE_STATUS.M);
		it05.addElement(DISH_ELEMENT.AEBA);
		it05.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 11);
		it05.addElement(DISH_ELEMENT.TMFT);
		it05.addElement(DISH_ELEMENT.TMFA);
		it05.addElement(DISH_ELEMENT.TMFT);
		it05.addElement(DISH_ELEMENT.TMFA);
		it05.addElement(DISH_ELEMENT.TMFT);
		it05.addElement(DISH_ELEMENT.TMFA);
		it05.addElement(DISH_ELEMENT.TDAM, DE_STATUS.M);
		it05.addElement(DISH_ELEMENT.CUTP, DE_STATUS.M);
		it05.addElement(DISH_ELEMENT.TOCA, DE_STATUS.NA);
		it05.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 1);
		it05.addElement(DISH_ELEMENT.TMFT);
		it05.addElement(DISH_ELEMENT.TMFA);
		it05.addElement(DISH_ELEMENT.TMFT);
		it05.addElement(DISH_ELEMENT.TMFA);
		it05.addElement(DISH_ELEMENT.TMFT);
		it05.addElement(DISH_ELEMENT.TMFA);
		it05.addElement(DISH_ELEMENT.COTP);
		it05.addElement(DISH_ELEMENT.CORT);
		it05.addElement(DISH_ELEMENT.COAM);
		it05.addElement(DISH_ELEMENT.COTP);
		it05.addElement(DISH_ELEMENT.CORT);
		it05.addElement(DISH_ELEMENT.COAM);
		it05.addElement(DISH_ELEMENT.COTP);
		it05.addElement(DISH_ELEMENT.CORT);
		it05.addElement(DISH_ELEMENT.COAM);
		it05.addElement(DISH_ELEMENT.NRID);
		it05.addElement(DISH_ELEMENT.APBC);
		it05.addElement(DISH_ELEMENT.TCTP, DE_STATUS.NA);

		ElementBuilder it06 = new ElementBuilder(FMT_ID.L);
		it06.addElement(DISH_ELEMENT.RCID);
		it06.addElement(DISH_ELEMENT.TRNN);
		it06.addElement(DISH_ELEMENT.ORAC);
		it06.addElement(DISH_ELEMENT.DSTC);
		it06.addElement(DISH_ELEMENT.FFRF, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.CARR, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 1);
		it06.addElement(DISH_ELEMENT.RBKD, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FTDA, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.NBDA, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.NADA, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBTD);
		it06.addElement(DISH_ELEMENT.FTNR, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FTDT, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBAL, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBST, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.SEGI);
		it06.addElement(DISH_ELEMENT.STPO, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.ORAC);
		it06.addElement(DISH_ELEMENT.DSTC);
		it06.addElement(DISH_ELEMENT.FFRF, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.CARR, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 1);
		it06.addElement(DISH_ELEMENT.RBKD, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FTDA, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.NBDA, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.NADA, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBTD);
		it06.addElement(DISH_ELEMENT.FTNR, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FTDT, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBAL, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBST, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.SEGI);
		it06.addElement(DISH_ELEMENT.STPO, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.ORAC);
		it06.addElement(DISH_ELEMENT.DSTC);
		it06.addElement(DISH_ELEMENT.FFRF, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.CARR, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 1);
		it06.addElement(DISH_ELEMENT.RBKD, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FTDA, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.NBDA, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.NADA, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBTD);
		it06.addElement(DISH_ELEMENT.FTNR, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FTDT, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBAL, FMT_ID.X, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.FBST, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.SEGI);
		it06.addElement(DISH_ELEMENT.STPO, DE_STATUS.C);
		it06.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 8);

		ElementBuilder it02CNJ = new ElementBuilder();
		it02CNJ.addElement(DISH_ELEMENT.RCID);
		it02CNJ.addElement(DISH_ELEMENT.TRNN);
		it02CNJ.addElement(DISH_ELEMENT.AGTN);
		it02CNJ.addElement(DISH_ELEMENT.CJCP, DE_STATUS.C);
		it02CNJ.addElement(DISH_ELEMENT.CPUI);
		it02CNJ.addElement(DISH_ELEMENT.DAIS);
		it02CNJ.addElement(DISH_ELEMENT.STAT);
		it02CNJ.addElement(DISH_ELEMENT.TDNR);
		it02CNJ.addElement(DISH_ELEMENT.CDGT);
		it02CNJ.addElement(DISH_ELEMENT.TRNC);
		it02CNJ.addElement(DISH_ELEMENT.TCNR, DE_STATUS.C);
		it02CNJ.addElement(DISH_ELEMENT.TCND, DE_STATUS.C);
		it02CNJ.addElement(DISH_ELEMENT.TACN);
		it02CNJ.addElement(DISH_ELEMENT.CDGT);
		it02CNJ.addElement(DISH_ELEMENT.FORM);
		it02CNJ.addElement(DISH_ELEMENT.PXNM);
		// setting default status to Conditional
		it02CNJ.setDefaultStatus(DE_STATUS.C);
		it02CNJ.addElement(DISH_ELEMENT.ALTP);
		it02CNJ.addElements(DISH_ELEMENT.SCNR, DISH_ELEMENT.SCNF);
		it02CNJ.addElement(DISH_ELEMENT.SCNT);
		// second stock control number setString
		it02CNJ.addElement(DISH_ELEMENT.ALNC);
		it02CNJ.addElement(DISH_ELEMENT.ALTP);
		it02CNJ.addElement(DISH_ELEMENT.SCNF);
		it02CNJ.addElement(DISH_ELEMENT.SCNT);
		// third stock control number set
		it02CNJ.addElement(DISH_ELEMENT.ALNC);
		it02CNJ.addElement(DISH_ELEMENT.ALTP);
		it02CNJ.addElement(DISH_ELEMENT.SCNF);
		it02CNJ.addElement(DISH_ELEMENT.SCNT);
		// fourth stock control number set
		it02CNJ.addElement(DISH_ELEMENT.ALNC);
		it02CNJ.addElement(DISH_ELEMENT.ALTP);
		it02CNJ.addElement(DISH_ELEMENT.SCNF);
		it02CNJ.addElement(DISH_ELEMENT.SCNT);

		it02CNJ.addElement(DISH_ELEMENT.ESAC, DE_STATUS.NA);
		it02CNJ.addElement(DISH_ELEMENT.DISI, DE_STATUS.NA);
		it02CNJ.addElement(DISH_ELEMENT.ISOC, DE_STATUS.M);
		it02CNJ.addElement(DISH_ELEMENT.VISO);
		it02CNJ.addElement(DISH_ELEMENT.VNID);
		it02CNJ.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 1);

		ElementBuilder it06CNJ = new ElementBuilder(FMT_ID.L);
		it06CNJ.addElement(DISH_ELEMENT.RCID);
		it06CNJ.addElement(DISH_ELEMENT.TRNN);
		it06CNJ.addElement(DISH_ELEMENT.ORAC);
		it06CNJ.addElement(DISH_ELEMENT.DSTC);
		it06CNJ.addElement(DISH_ELEMENT.FFRF, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.CARR, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 1);
		it06CNJ.addElement(DISH_ELEMENT.RBKD, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FTDA, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.NBDA, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.NADA, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBTD);
		it06CNJ.addElement(DISH_ELEMENT.FTNR, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FTDT, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBAL, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBST, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.SEGI);
		it06CNJ.addElement(DISH_ELEMENT.STPO, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.ORAC);
		it06CNJ.addElement(DISH_ELEMENT.DSTC);
		it06CNJ.addElement(DISH_ELEMENT.FFRF, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.CARR, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 1);
		it06CNJ.addElement(DISH_ELEMENT.RBKD, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FTDA, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.NBDA, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.NADA, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBTD);
		it06CNJ.addElement(DISH_ELEMENT.FTNR, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FTDT, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBAL, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBST, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.SEGI);
		it06CNJ.addElement(DISH_ELEMENT.STPO, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.ORAC);
		it06CNJ.addElement(DISH_ELEMENT.DSTC);
		it06CNJ.addElement(DISH_ELEMENT.FFRF, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.CARR, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 1);
		it06CNJ.addElement(DISH_ELEMENT.RBKD, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FTDA, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.NBDA, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.NADA, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBTD);
		it06CNJ.addElement(DISH_ELEMENT.FTNR, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FTDT, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBAL, FMT_ID.X, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.FBST, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.SEGI);
		it06CNJ.addElement(DISH_ELEMENT.STPO, DE_STATUS.C);
		it06CNJ.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 8);

		ElementBuilder it07 = new ElementBuilder(FMT_ID.X);
		it07.addElement(DISH_ELEMENT.RCID);
		it07.addElement(DISH_ELEMENT.TRNN);
		it07.addElement(DISH_ELEMENT.FRCA, DE_STATUS.C);
		it07.addElement(DISH_ELEMENT.FRCS, DE_STATUS.C);
		it07.addElement(DISH_ELEMENT.FRCA, DE_STATUS.C);
		it07.addElement(DISH_ELEMENT.FRCS, DE_STATUS.C);
		it07.addElement(DISH_ELEMENT.FCMI);
		//Change from 20.1 to 20.3 Mandatory
		it07.addElement(DISH_ELEMENT.FCPI);
		it07.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 70);

		ElementBuilder it08 = new ElementBuilder(DE_STATUS.C);
		it08.addElement(DISH_ELEMENT.RCID, DE_STATUS.M);
		it08.addElement(DISH_ELEMENT.TRNN, DE_STATUS.M);
		// first form of payment
		it08.addElement(DISH_ELEMENT.FPAC);
		it08.addElement(DISH_ELEMENT.FPAM, DE_STATUS.M);
		it08.addElement(DISH_ELEMENT.APLC);
		it08.addElement(DISH_ELEMENT.CUTP, DE_STATUS.M);
		it08.addElement(DISH_ELEMENT.EXPC);
		it08.addElement(DISH_ELEMENT.FPTP, DE_STATUS.M);
		it08.addElement(DISH_ELEMENT.EXDA);
		it08.addElement(DISH_ELEMENT.CSTF);
		it08.addElement(DISH_ELEMENT.CRCC);
		it08.addElement(DISH_ELEMENT.AVCD);
		it08.addElement(DISH_ELEMENT.SAPP);
		it08.addElement(DISH_ELEMENT.FPTI);
		it08.addElement(DISH_ELEMENT.AUTA);
		// second form of payment
		it08.addElement(DISH_ELEMENT.FPAC);
		it08.addElement(DISH_ELEMENT.FPAM, DE_STATUS.M);
		it08.addElement(DISH_ELEMENT.APLC);
		it08.addElement(DISH_ELEMENT.CUTP, DE_STATUS.M);
		it08.addElement(DISH_ELEMENT.EXPC);
		it08.addElement(DISH_ELEMENT.FPTP, DE_STATUS.M);
		it08.addElement(DISH_ELEMENT.EXDA);
		it08.addElement(DISH_ELEMENT.CSTF);
		it08.addElement(DISH_ELEMENT.CRCC);
		it08.addElement(DISH_ELEMENT.AVCD);
		it08.addElement(DISH_ELEMENT.SAPP);
		it08.addElement(DISH_ELEMENT.FPTI);
		it08.addElement(DISH_ELEMENT.AUTA);
		it08.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 2);

		ElementBuilder it0Y = new ElementBuilder(FMT_ID.X, DE_STATUS.C);
		it0Y.addElement(DISH_ELEMENT.RCID, DE_STATUS.M);
		it0Y.addElement(DISH_ELEMENT.TRNN, DE_STATUS.M);
		it0Y.addElement(DISH_ELEMENT.AGTA);
		it0Y.addElement(DISH_ELEMENT.AGTO);
		it0Y.addElement(DISH_ELEMENT.AGTS);
		it0Y.addElement(DISH_ELEMENT.AGTU);
		it0Y.addElement(DISH_ELEMENT.AGTR);
		it0Y.addElement(DISH_ELEMENT.AGTC);
		it0Y.addElement(DISH_ELEMENT.AGTP);
		it0Y.addElement(DISH_ELEMENT.AGTF);
		it0Y.addElement(DISH_ELEMENT.AGTT);
		it0Y.addElement(DISH_ELEMENT.AGTK);
		it0Y.addElement(DISH_ELEMENT.AGTB);
		it0Y.addElement(DISH_ELEMENT.AGTE);
		it0Y.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 7);
		it0Y.addElement(DISH_ELEMENT.CCOC);
		it0Y.addElement(DISH_ELEMENT.CCEN);
		it0Y.addElement(DISH_ELEMENT.CCRM);
		it0Y.addElement(DISH_ELEMENT.CCRM);
		it0Y.addElement(DISH_ELEMENT.CCRM);
		it0Y.addElement(DISH_ELEMENT.CCHN);
		it0Y.addElement(DISH_ELEMENT.SFAM, FMT_ID.S);
		it0Y.addElement(DISH_ELEMENT.CUTP, FMT_ID.S);
		it0Y.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 50);

		ElementBuilder it09 = new ElementBuilder();
		it09.setRecordLength(FMT_ID.X);
		it09.addElement(DISH_ELEMENT.RCID);
		it09.addElement(DISH_ELEMENT.TRNN);
		it09.addElement(DISH_ELEMENT.ENRS, DE_STATUS.C);
		it09.addElement(DISH_ELEMENT.FPIN);
		it09.addElement(DISH_ELEMENT.FPIN);
		it09.addElement(DISH_ELEMENT.RESD, FMT_ID.NA);

		ElementBuilder it0B = new ElementBuilder(FMT_ID.X);
		it0B.addElement(DISH_ELEMENT.RCID);
		it0B.addElement(DISH_ELEMENT.TRNN);
		it0B.addElement(DISH_ELEMENT.OAAI, DE_STATUS.C);
		it0B.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 160);

		ElementBuilder it0Z = new ElementBuilder();
		it0Z.addElement(DISH_ELEMENT.RCID);
		it0Z.addElement(DISH_ELEMENT.RRDC, DE_STATUS.M);
		it0Z.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, 243);

		// /////////////////////////////////////////////////////////

		ElementBuilder bkt06 = new ElementBuilder();
		bkt06.addElement(DISH_ELEMENT.RCID);
		bkt06.addElement(DISH_ELEMENT.TRNN);
		bkt06.addElement(DISH_ELEMENT.AGTN);
		bkt06.addElement(DISH_ELEMENT.CJCP, DE_STATUS.C);
		bkt06.addElement(DISH_ELEMENT.CPUI);
		bkt06.addElement(DISH_ELEMENT.DAIS);
		bkt06.addElement(DISH_ELEMENT.STAT);
		bkt06.addElement(DISH_ELEMENT.TDNR);
		bkt06.addElement(DISH_ELEMENT.CDGT);
		bkt06.addElement(DISH_ELEMENT.TRNC);
		bkt06.addElement(DISH_ELEMENT.TCNR, DE_STATUS.C);
		bkt06.addElement(DISH_ELEMENT.TCND, DE_STATUS.C);
		bkt06.addElement(DISH_ELEMENT.TACN);
		bkt06.addElement(DISH_ELEMENT.CDGT);
		bkt06.addElement(DISH_ELEMENT.FORM);
		bkt06.addElement(DISH_ELEMENT.PXNM);
		// setting default status to Conditional
		bkt06.setDefaultStatus(DE_STATUS.C);
		bkt06.addElement(DISH_ELEMENT.ALTP);
		bkt06.addElements(DISH_ELEMENT.SCNR, DISH_ELEMENT.SCNF);
		bkt06.addElement(DISH_ELEMENT.SCNT);
		// second stock control number set
		bkt06.addElement(DISH_ELEMENT.ALNC);
		bkt06.addElement(DISH_ELEMENT.ALTP);
		bkt06.addElement(DISH_ELEMENT.SCNF);
		bkt06.addElement(DISH_ELEMENT.SCNT);
		// third stock control number set
		bkt06.addElement(DISH_ELEMENT.ALNC);
		bkt06.addElement(DISH_ELEMENT.ALTP);
		bkt06.addElement(DISH_ELEMENT.SCNF);
		bkt06.addElement(DISH_ELEMENT.SCNT);
		// fourth stock control number set
		bkt06.addElement(DISH_ELEMENT.ALNC);
		bkt06.addElement(DISH_ELEMENT.ALTP);
		bkt06.addElement(DISH_ELEMENT.SCNF);
		bkt06.addElement(DISH_ELEMENT.SCNT);

		bkt06.addElement(DISH_ELEMENT.ESAC, DE_STATUS.NA);
		bkt06.addElement(DISH_ELEMENT.DISI, DE_STATUS.NA);
		bkt06.addElement(DISH_ELEMENT.ISOC, DE_STATUS.M);
		bkt06.addElement(DISH_ELEMENT.VISO);
		bkt06.addElement(DISH_ELEMENT.VNID);
		bkt06.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 1);

		ElementBuilder bks07 = new ElementBuilder();
		bks07.addElement(DISH_ELEMENT.RCID);
		bks07.addElement(DISH_ELEMENT.TRNN);
		bks07.addElement(DISH_ELEMENT.AGTN);
		bks07.addElement(DISH_ELEMENT.CJCP, DE_STATUS.C);
		bks07.addElement(DISH_ELEMENT.CPUI);
		bks07.addElement(DISH_ELEMENT.DAIS);
		bks07.addElement(DISH_ELEMENT.STAT);
		bks07.addElement(DISH_ELEMENT.TDNR);
		bks07.addElement(DISH_ELEMENT.CDGT);
		bks07.addElement(DISH_ELEMENT.TRNC);
		bks07.addElement(DISH_ELEMENT.TCNR, DE_STATUS.C);
		bks07.addElement(DISH_ELEMENT.TCND, DE_STATUS.C);
		bks07.addElement(DISH_ELEMENT.TACN);
		bks07.addElement(DISH_ELEMENT.CDGT);
		bks07.addElement(DISH_ELEMENT.FORM);
		bks07.addElement(DISH_ELEMENT.PXNM);
		// setting default status to Conditional
		bks07.setDefaultStatus(DE_STATUS.C);
		bks07.addElement(DISH_ELEMENT.ALTP);
		bks07.addElements(DISH_ELEMENT.SCNR, DISH_ELEMENT.SCNF);
		bks07.addElement(DISH_ELEMENT.SCNT);

		// ////////////////////////////////////////////////////

		RecordBuilder tkttRbuilder = new RecordBuilder();
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT01, it01, 1, 1);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT02, it02, 1, 1);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0S, it0S, 0, RecordMetaData.UNLIMITED);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT04, it04, 1, 1);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT05, it05, 1, RecordMetaData.UNLIMITED);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT06, it06, 1, 2);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT02CNJ, it02CNJ, 0, 1);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT06CNJ, it06CNJ, 0, 2);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT07, it07, 1, 2);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT08, it08, 1, RecordMetaData.UNLIMITED);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0Y, it0Y, 0, RecordMetaData.UNLIMITED);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT09, it09, 1, 1);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0B, it0B, 0, 1);
		tkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0Z, it0Z, 0, 1);

		recordFileMap.put(DISH_FILE_TYPE.TKTT, tkttRbuilder);
		recordFileMap.put(DISH_FILE_TYPE.EMDA, tkttRbuilder);
		recordFileMap.put(DISH_FILE_TYPE.TKTA, tkttRbuilder);// clone check?
		recordFileMap.put(DISH_FILE_TYPE.TKTB, tkttRbuilder);// clone check?
		
		
		ElementBuilder it0G = new ElementBuilder(FMT_ID.X);
		it0G.addElement(DISH_ELEMENT.RCID);
		it0G.addElement(DISH_ELEMENT.TRNN);
		it0G.addElement(DISH_ELEMENT.EMCP);
		it0G.addElement(DISH_ELEMENT.EMCV, DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.CUTP);
		it0G.addElement(DISH_ELEMENT.EMRT,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.EMRC,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.EMSC,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.EMOC,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.XBOA,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.XBRU,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.XBNE,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.EMRM,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.EMCI, DE_STATUS.M);
		it0G.addElement(DISH_ELEMENT.XBCT,DE_STATUS.C);
		it0G.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 112);
		
		ElementBuilder it0A = new ElementBuilder(FMT_ID.X);
		it0A.addElement(DISH_ELEMENT.RCID);
		it0A.addElement(DISH_ELEMENT.TRNN);
		it0A.addElement(DISH_ELEMENT.RFID,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.BERA,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.ICDN,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.CDGT,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.AMIL,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.RFIC,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.MPOC,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.MPEQ,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.MPEV,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.MPSC,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.MPTX,DE_STATUS.C);
		it0A.addElement(DISH_ELEMENT.RESD, FMT_ID.NA, DE_STATUS.M, 5);
		
		RecordBuilder emdsRbuilder = new RecordBuilder();
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT01, it01, 1, 1);
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT02, it02, 1, 1);
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT04, it04, 1, RecordMetaData.UNLIMITED);
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT05, it05, 1, RecordMetaData.UNLIMITED);
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0G, it0G, 1, RecordMetaData.UNLIMITED);
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT08, it08, 1, RecordMetaData.UNLIMITED);
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT09, it09, 1, 1);
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0A, it0A, 1, 1);
		emdsRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0Z, it0Z, 0, 1);
		
		recordFileMap.put(DISH_FILE_TYPE.EMDS, emdsRbuilder);
		
		/* CANX/CANN/CANR element section */
		it01 = it01.clone();
		it02 = it02.clone(DE_STATUS.M);
		it02.changeStatus(4, DE_STATUS.NA);
		it02.changeStatus(7, DE_STATUS.NA);
		it02.changeStatus(11, DE_STATUS.C);
		it02.changeStatus(12, DE_STATUS.C);
		it02.changeStatus(16, DE_STATUS.NA);
		for (int i = 17; i <= 32; i++) {
			it02.changeStatus(i, DE_STATUS.C);
		}
		it02.changeStatus(33, DE_STATUS.NA);
		it02.changeStatus(35, DE_STATUS.C);
		it02.changeStatus(36, DE_STATUS.C);

		it0S = it0S.clone(DE_STATUS.C);
		for (int i = 1; i <= 6; i++) {
			it0S.changeStatus(i, DE_STATUS.M);
		}
		it0S.changeStatus(35, DE_STATUS.C);
		it0Z = it0Z.clone();

		RecordBuilder cnxRbuilder = new RecordBuilder();
		cnxRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT01, it01, 1, 1);
		cnxRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT02, it02, 1, 1);
		cnxRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0S, it0S, 0, RecordMetaData.UNLIMITED);
		cnxRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0Z, it0Z, 0, 1);

		recordFileMap.put(DISH_FILE_TYPE.CANX, cnxRbuilder);
		recordFileMap.put(DISH_FILE_TYPE.CANN, cnxRbuilder);
		recordFileMap.put(DISH_FILE_TYPE.CANR, cnxRbuilder);

		/* RFND element section */
		it01 = it01.clone();
		it02 = it02.clone(DE_STATUS.M);
		it02.changeStatus(4, DE_STATUS.NA);
		it02.changeStatus(5, DE_STATUS.NA);
		it02.changeStatus(11, DE_STATUS.C);
		it02.changeStatus(12, DE_STATUS.C);
		it02.changeStatus(15, DE_STATUS.NA);
		it02.changeStatus(16, DE_STATUS.C);
		it02.changeStatus(17, DE_STATUS.NA);
		for (int i = 17; i <= 31; i++) {
			it02.changeStatus(i, DE_STATUS.NA);
		}
		it02.changeStatus(32, DE_STATUS.C);
		it02.changeStatus(35, DE_STATUS.C);
		it02.changeStatus(36, DE_STATUS.C);

		it03 = it03.clone(DE_STATUS.C);
		for (int i = 1; i <= 5; i++) {
			it03.changeStatus(i, DE_STATUS.M);
		}
		it03.changeStatus(24, DE_STATUS.C);
		it03.changeStatus(27, DE_STATUS.C);

		it05 = it05.clone(DE_STATUS.C);
		it05.changeStatus(1, DE_STATUS.M);
		it05.changeStatus(2, DE_STATUS.M);
		it05.changeStatus(4, DE_STATUS.M);
		it05.changeStatus(11, DE_STATUS.M);
		it05.changeStatus(12, DE_STATUS.M);
		it05.changeStatus(13, DE_STATUS.NA);
		it05.changeStatus(14, DE_STATUS.M);
		it05.changeStatus(32, DE_STATUS.NA);

		it08 = it08.clone(DE_STATUS.C);
		it08.changeStatus(1, DE_STATUS.M);
		it08.changeStatus(2, DE_STATUS.M);
		it08.changeStatus(4, DE_STATUS.M);
		it08.changeStatus(5, DE_STATUS.NA);
		it08.changeStatus(6, DE_STATUS.M);
		it08.changeStatus(7, DE_STATUS.NA);
		it08.changeStatus(8, DE_STATUS.M);
		it08.changeStatus(9, DE_STATUS.NA);
		it08.changeStatus(29, DE_STATUS.M);

		it0Y = it0Y.clone(DE_STATUS.C);
		it0Y.changeStatus(1, DE_STATUS.M);
		it0Y.changeStatus(2, DE_STATUS.M);
		it0Y.changeStatus(15, DE_STATUS.M);
		it0Y.changeStatus(24, DE_STATUS.M);

		it0Z = it0Z.clone();

		RecordBuilder refundRbuilder = new RecordBuilder();
		refundRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT01, it01, 1, 1);
		refundRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT02, it02, 1, 1);
		refundRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT03, it03, 0, RecordMetaData.UNLIMITED);
		refundRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT05, it05, 1, RecordMetaData.UNLIMITED);
		refundRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT08, it08, 1, RecordMetaData.UNLIMITED);
		refundRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0Y, it0Y, 0, RecordMetaData.UNLIMITED);
		refundRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0Z, it0Z, 0, 1);

		recordFileMap.put(DISH_FILE_TYPE.RFND, refundRbuilder);

		/* EMTY element section */
		RecordBuilder emptyRBuilder = new RecordBuilder();
		it01 = it01.clone();
		it0Z = it0Z.clone();
		emptyRBuilder.addElementBuilder(DISH_RECORD_TYPE.IT01, it01, 1, 1);
		emptyRBuilder.addElementBuilder(DISH_RECORD_TYPE.IT0Z, it0Z, 0, 1);
		recordFileMap.put(DISH_FILE_TYPE.EMTY, emptyRBuilder);

		/* Hot file related report section */

		ElementBuilder dfh01 = new ElementBuilder();
		dfh01.addElement(DISH_ELEMENT.SMSG);
		dfh01.addElement(DISH_ELEMENT.SQNR);
		dfh01.addElement(DISH_ELEMENT.STNQ);
		dfh01.addElement(DISH_ELEMENT.BSPI);
		dfh01.addElement(DISH_ELEMENT.TACN);
		dfh01.addElement(DISH_ELEMENT.REVN);
		dfh01.addElement(DISH_ELEMENT.TPST);
		dfh01.addElement(DISH_ELEMENT.PRDA);
		dfh01.addElement(DISH_ELEMENT.TIME);
		dfh01.addElement(DISH_ELEMENT.ISOC);
		dfh01.addElement(DISH_ELEMENT.FSQN);
		dfh01.addElement(DISH_ELEMENT.RESD, 90);

		ElementBuilder dfh02 = new ElementBuilder();
		dfh02.addElement(DISH_ELEMENT.SMSG);
		dfh02.addElement(DISH_ELEMENT.SQNR);
		dfh02.addElement(DISH_ELEMENT.STNQ);
		dfh02.addElement(DISH_ELEMENT.BSPI);
		dfh02.addElement(DISH_ELEMENT.TACN);
		dfh02.addElement(DISH_ELEMENT.REVN);
		dfh02.addElement(DISH_ELEMENT.TPST);
		dfh02.addElement(DISH_ELEMENT.PRDA);
		dfh02.addElement(DISH_ELEMENT.TIME);
		dfh02.addElement(DISH_ELEMENT.ISOC);
		dfh02.addElement(DISH_ELEMENT.FSQN);
		dfh02.addElement(DISH_ELEMENT.RESD, 106);

		ElementBuilder dfh03 = new ElementBuilder();
		dfh03.addElement(DISH_ELEMENT.SMSG);
		dfh03.addElement(DISH_ELEMENT.SQNR);
		dfh03.addElement(DISH_ELEMENT.STNQ);
		dfh03.addElement(DISH_ELEMENT.BSPI);
		dfh03.addElement(DISH_ELEMENT.TACN);
		dfh03.addElement(DISH_ELEMENT.REVN);
		dfh03.addElement(DISH_ELEMENT.TPST);
		dfh03.addElement(DISH_ELEMENT.PRDA);
		dfh03.addElement(DISH_ELEMENT.TIME);
		dfh03.addElement(DISH_ELEMENT.ISOC);
		dfh03.addElement(DISH_ELEMENT.FSQN);
		dfh03.addElement(DISH_ELEMENT.RESD);
		dfh03.addElement(DISH_ELEMENT.CUTP);
		dfh03.addElement(DISH_ELEMENT.MLOC);
		dfh03.addElement(DISH_ELEMENT.RESD, 79);

		ElementBuilder bks24 = new ElementBuilder();
		createBKCommonElement(bks24);
		bks24.addElement(DISH_ELEMENT.TCNR, DE_STATUS.C);
		bks24.addElement(DISH_ELEMENT.TCND, DE_STATUS.C);
		bks24.addElement(DISH_ELEMENT.CPUI, DE_STATUS.C);
		bks24.addElement(DISH_ELEMENT.CJCP, DE_STATUS.C);
		bks24.addElement(DISH_ELEMENT.AGTN);
		bks24.addElement(DISH_ELEMENT.RESD, 5);
		bks24.addElement(DISH_ELEMENT.TOUR, DE_STATUS.C);
		bks24.addElement(DISH_ELEMENT.TRNC);
		bks24.addElement(DISH_ELEMENT.TODC, DE_STATUS.C);
		bks24.addElement(DISH_ELEMENT.RESD, 4);
		bks24.addElement(DISH_ELEMENT.PNRR, DE_STATUS.C);
		bks24.addElement(DISH_ELEMENT.RESD, 9);

		ElementBuilder bks30 = new ElementBuilder();
		createBKCommonElement(bks30);
		bks30.addElement(DISH_ELEMENT.COBL);
		bks30.addElement(DISH_ELEMENT.NTFA);
		bks30.addElement(DISH_ELEMENT.RESD, 1);
		bks30.addElement(DISH_ELEMENT.TMFT, DE_STATUS.C);
		bks30.addElement(DISH_ELEMENT.TMFA, DE_STATUS.C);
		bks30.addElement(DISH_ELEMENT.TMFT, DE_STATUS.C);
		bks30.addElement(DISH_ELEMENT.TMFA, DE_STATUS.C);
		bks30.addElement(DISH_ELEMENT.TDAM);
		bks30.addElement(DISH_ELEMENT.LREP, DE_STATUS.C);
		bks30.addElement(DISH_ELEMENT.RESD, 5);
		bks30.addElement(DISH_ELEMENT.CUTP);
		bks30.addElement(DISH_ELEMENT.RESD, 3);

		ElementBuilder bks39 = new ElementBuilder();
		createBKCommonElement(bks39);
		bks39.addElement(DISH_ELEMENT.STAT, DE_STATUS.C);
		bks39.addElement(DISH_ELEMENT.COTP, DE_STATUS.C);
		bks39.addElement(DISH_ELEMENT.CORT);
		bks39.addElement(DISH_ELEMENT.COAM);
		bks39.addElement(DISH_ELEMENT.SPTP, DE_STATUS.C);
		bks39.addElement(DISH_ELEMENT.SPTR, DE_STATUS.C);
		bks39.addElement(DISH_ELEMENT.SPAM, DE_STATUS.C);
		bks39.addElement(DISH_ELEMENT.EFRT, DE_STATUS.C);
		bks39.addElement(DISH_ELEMENT.EFCO, DE_STATUS.C);
		bks39.addElement(DISH_ELEMENT.APBC);
		bks39.addElement(DISH_ELEMENT.RESD, 14);
		bks39.addElement(DISH_ELEMENT.CUTP);
		bks39.addElement(DISH_ELEMENT.RESD, 3);

		ElementBuilder bks45 = new ElementBuilder();
		createBKCommonElement(bks45);

		ElementBuilder bks46 = new ElementBuilder();
		createBKCommonElement(bks46);
		bks46.addElement(DISH_ELEMENT.ORIN, DE_STATUS.C);
		bks46.addElement(DISH_ELEMENT.ENRS, DE_STATUS.C);
		bks46.addElement(DISH_ELEMENT.RESD, 5);

		ElementBuilder bki63 = new ElementBuilder();
		createBKCommonElement(bki63);
		bki63.addElement(DISH_ELEMENT.SEGI);
		bki63.addElement(DISH_ELEMENT.STPO);
		bki63.addElement(DISH_ELEMENT.NBDA, DE_STATUS.C);
		bki63.addElement(DISH_ELEMENT.NADA, DE_STATUS.C);
		bki63.addElement(DISH_ELEMENT.RESD, 7);
		bki63.addElement(DISH_ELEMENT.ORAC);
		bki63.addElement(DISH_ELEMENT.DSTC);
		bki63.addElement(DISH_ELEMENT.CARR, DE_STATUS.C);
		bki63.addElement(DISH_ELEMENT.RESD, 1);
		bki63.addElement(DISH_ELEMENT.FTNR, DE_STATUS.C);
		bki63.addElement(DISH_ELEMENT.RBKD);
		bki63.addElement(DISH_ELEMENT.FTDA);
		bki63.addElement(DISH_ELEMENT.RESD, 1);
		bki63.addElement(DISH_ELEMENT.FTDT, DE_STATUS.C);
		bki63.addElement(DISH_ELEMENT.RESD, 3);
		bki63.addElement(DISH_ELEMENT.FBST, DE_STATUS.C);
		bki63.addElement(DISH_ELEMENT.FBAL, DE_STATUS.C);
		bki63.addElement(DISH_ELEMENT.RESD, 1);
		bki63.addElement(DISH_ELEMENT.FBTD);
		bki63.addElement(DISH_ELEMENT.FFRF, DE_STATUS.C);
		bki63.addElement(DISH_ELEMENT.RESD, 3);

		ElementBuilder bar64 = new ElementBuilder();
		createBKCommonElement(bar64);
		bar64.addElement(DISH_ELEMENT.FARE);
		bar64.addElement(DISH_ELEMENT.TKMI);
		bar64.addElement(DISH_ELEMENT.EQFR, DE_STATUS.C);
		bar64.addElement(DISH_ELEMENT.TAXA, DE_STATUS.C);
		bar64.addElement(DISH_ELEMENT.TAXA, DE_STATUS.C);
		bar64.addElement(DISH_ELEMENT.TAXA, DE_STATUS.C);
		bar64.addElement(DISH_ELEMENT.TOTL);
		bar64.addElement(DISH_ELEMENT.NTSI, DE_STATUS.C);
		bar64.addElement(DISH_ELEMENT.SASI, DE_STATUS.C);
		bar64.addElement(DISH_ELEMENT.TOTL);
		bar64.addElement(DISH_ELEMENT.NTSI);
		bar64.addElement(DISH_ELEMENT.SASI);
		bar64.addElement(DISH_ELEMENT.FCMI);
		bar64.addElement(DISH_ELEMENT.BAID);
		bar64.addElement(DISH_ELEMENT.BOON);
		bar64.addElement(DISH_ELEMENT.BEOT);
		bar64.addElement(DISH_ELEMENT.FCPI);
		bar64.addElement(DISH_ELEMENT.RESD, 1);

		ElementBuilder bar65 = new ElementBuilder();
		createBKCommonElement(bar65);
		bar65.addElement(DISH_ELEMENT.PXNM);
		bar65.addElement(DISH_ELEMENT.PXDA);

		ElementBuilder bar66 = new ElementBuilder();
		createBKCommonElement(bar66);
		bar66.addElement(DISH_ELEMENT.FPSN);
		bar66.addElement(DISH_ELEMENT.FPIN);
		bar66.addElement(DISH_ELEMENT.RESD, 44);

		ElementBuilder bmp70 = new ElementBuilder();
		createBKCommonElement(bmp70);
		bar66.addElement(DISH_ELEMENT.RFID);
		bar66.addElement(DISH_ELEMENT.RFIC);
		bar66.addElement(DISH_ELEMENT.RESD, 8);

		ElementBuilder bmp71 = new ElementBuilder();
		createBKCommonElement(bmp71);
		bmp71.addElement(DISH_ELEMENT.ICDN);
		bmp71.addElement(DISH_ELEMENT.CDGT);
		bmp71.addElement(DISH_ELEMENT.RESD, 16);

		ElementBuilder bmp72 = new ElementBuilder();
		createBKCommonElement(bmp72);
		bmp72.addElement(DISH_ELEMENT.AMIL);
		bmp72.addElement(DISH_ELEMENT.RESD, 23);

		ElementBuilder bmp73 = new ElementBuilder();
		createBKCommonElement(bmp73);
		bmp73.addElement(DISH_ELEMENT.OAAI);
		bmp73.addElement(DISH_ELEMENT.RESD, 7);

		ElementBuilder bmp74 = new ElementBuilder();
		createBKCommonElement(bmp74);
		bmp74.addElement(DISH_ELEMENT.PLID);
		bmp74.addElement(DISH_ELEMENT.PLTX);
		bmp74.addElement(DISH_ELEMENT.RESD, 8);

		ElementBuilder bmd75 = new ElementBuilder();
		createBKCommonElement(bmd75);

		ElementBuilder bmd76 = new ElementBuilder();
		createBKCommonElement(bmd76);
		bmp74.addElement(DISH_ELEMENT.EMCP);
		bmp74.addElement(DISH_ELEMENT.EMRM);
		bmp74.addElement(DISH_ELEMENT.RESD, 24);

		ElementBuilder bmp77 = new ElementBuilder();
		createBKCommonElement(bmp77);
		bmp77.addElement(DISH_ELEMENT.RESD, 1);
		bmp77.addElement(DISH_ELEMENT.CPNR);
		bmp77.addElement(DISH_ELEMENT.PLID);
		bmp77.addElement(DISH_ELEMENT.PLTX);
		bmp77.addElement(DISH_ELEMENT.RESD, 6);

		ElementBuilder bmp78 = new ElementBuilder();
		createBKCommonElement(bmp78);
		bmp78.addElement(DISH_ELEMENT.PLID);
		bmp78.addElement(DISH_ELEMENT.SPIN, DE_STATUS.C);
		bmp78.addElement(DISH_ELEMENT.PLID, DE_STATUS.C);
		bmp78.addElement(DISH_ELEMENT.SPIN, DE_STATUS.C);
		bmp78.addElement(DISH_ELEMENT.RESD, 25);

		ElementBuilder bkf81 = new ElementBuilder();
		createBKCommonElement(bkf81);
		bkf81.addElement(DISH_ELEMENT.RESD, 2);
		bkf81.addElement(DISH_ELEMENT.FRCS, DE_STATUS.C);
		bkf81.addElement(DISH_ELEMENT.FRCA, DE_STATUS.C);
		bkf81.addElement(DISH_ELEMENT.RESD, 5);

		ElementBuilder bkp83 = new ElementBuilder();
		createBKCommonElement(bkp83);

		ElementBuilder bkp84 = new ElementBuilder();
		createBKCommonElement(bkp84);

		ElementBuilder bot93 = new ElementBuilder();
		bot93.addElement(DISH_ELEMENT.SMSG);
		bot93.addElement(DISH_ELEMENT.SQNR);
		bot93.addElement(DISH_ELEMENT.STNQ);
		bot93.addElement(DISH_ELEMENT.AGTN);
		bot93.addElement(DISH_ELEMENT.RMED);
		bot93.addElement(DISH_ELEMENT.RESD, 7);
		bot93.addElement(DISH_ELEMENT.GROS);
		bot93.addElement(DISH_ELEMENT.TREM);
		bot93.addElement(DISH_ELEMENT.TCOM);
		bot93.addElement(DISH_ELEMENT.TTMF);
		bot93.addElement(DISH_ELEMENT.TLRP);
		bot93.addElement(DISH_ELEMENT.TRNC);
		bot93.addElement(DISH_ELEMENT.RESD, 1);
		bot93.addElement(DISH_ELEMENT.TTCA);
		bot93.addElement(DISH_ELEMENT.CUTP);
		bot93.addElement(DISH_ELEMENT.RESD, 3);

		ElementBuilder bot94 = new ElementBuilder();
		bot94.addElement(DISH_ELEMENT.SMSG);
		bkf81.addElement(DISH_ELEMENT.SPIN, DE_STATUS.C);
		bot94.addElement(DISH_ELEMENT.SQNR);
		bot94.addElement(DISH_ELEMENT.STNQ);
		bot94.addElement(DISH_ELEMENT.AGTN);
		bot94.addElement(DISH_ELEMENT.RMED);
		bot94.addElement(DISH_ELEMENT.RESD, 7);
		bot94.addElement(DISH_ELEMENT.GROS);
		bot94.addElement(DISH_ELEMENT.TREM);
		bot94.addElement(DISH_ELEMENT.TCOM);
		bot94.addElement(DISH_ELEMENT.TTMF);
		bot94.addElement(DISH_ELEMENT.TLRP);
		bot94.addElement(DISH_ELEMENT.RESD, 5);
		bot94.addElement(DISH_ELEMENT.TTCA);
		bot94.addElement(DISH_ELEMENT.CUTP);
		bot94.addElement(DISH_ELEMENT.RESD, 3);

		ElementBuilder bct95 = new ElementBuilder();
		bct95.addElement(DISH_ELEMENT.SMSG);
		bct95.addElement(DISH_ELEMENT.SQNR);
		bct95.addElement(DISH_ELEMENT.STNQ);
		bct95.addElement(DISH_ELEMENT.AGTN);
		bct95.addElement(DISH_ELEMENT.RMED);
		bct95.addElement(DISH_ELEMENT.RESD, 7);
		bct95.addElement(DISH_ELEMENT.GROS);
		bct95.addElement(DISH_ELEMENT.TREM);
		bct95.addElement(DISH_ELEMENT.TCOM);
		bct95.addElement(DISH_ELEMENT.TTMF);
		bct95.addElement(DISH_ELEMENT.TLRP);
		bct95.addElement(DISH_ELEMENT.RESD, 5);
		bct95.addElement(DISH_ELEMENT.TTCA);
		bct95.addElement(DISH_ELEMENT.CUTP);
		bct95.addElement(DISH_ELEMENT.RESD, 3);

		ElementBuilder bft99 = new ElementBuilder();
		bft99.addElement(DISH_ELEMENT.SMSG);
		bft99.addElement(DISH_ELEMENT.SQNR);
		bft99.addElement(DISH_ELEMENT.STNQ);
		bft99.addElement(DISH_ELEMENT.AGTN);
		bft99.addElement(DISH_ELEMENT.RMED);
		bft99.addElement(DISH_ELEMENT.RESD, 7);
		bft99.addElement(DISH_ELEMENT.GROS);
		bft99.addElement(DISH_ELEMENT.TREM);
		bft99.addElement(DISH_ELEMENT.TCOM);
		bft99.addElement(DISH_ELEMENT.TTMF);
		bft99.addElement(DISH_ELEMENT.TLRP);
		bft99.addElement(DISH_ELEMENT.RESD, 5);
		bft99.addElement(DISH_ELEMENT.TTCA);
		bft99.addElement(DISH_ELEMENT.CUTP);
		bft99.addElement(DISH_ELEMENT.RESD, 3);
		// ////////////////////////////////////////////////////

		RecordBuilder bkttRbuilder = new RecordBuilder();
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.DFH01, dfh01, 1, 1);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.DFH02, dfh02, 1, 1);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.DFH03, dfh03, 1, 1);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKT06, bkt06, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKS24, bks24, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKS30, bks30, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKS39, bks39, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKS45, bks45, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKS46, bks46, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKI63, bki63, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BAR64, bar64, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BAR65, bar65, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BAR66, bar66, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMP70, bmp70, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMP71, bmp71, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMP72, bmp72, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMP73, bmp73, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMP74, bmp74, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMD75, bmd75, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMD76, bmd76, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMP77, bmp77, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BMP78, bmp78, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKF81, bkf81, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKP83, bkp83, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BKP84, bkp84, 1, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BOT93, bot93, 0, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BOT94, bot94, 0, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BCT95, bct95, 0, RecordMetaData.UNLIMITED);
		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.BFT99, bft99, 0, RecordMetaData.UNLIMITED);

		bkttRbuilder.addElementBuilder(DISH_RECORD_TYPE.IT0Z, it0Z, 0, 1);
		recordFileMap.put(DISH_FILE_TYPE.BKTT, bkttRbuilder);

		RecordBuilder emptyRHOTBuilder = new RecordBuilder();

		emptyRHOTBuilder.addElementBuilder(DISH_RECORD_TYPE.DFH01, dfh01, 1, 1);
		emptyRHOTBuilder.addElementBuilder(DISH_RECORD_TYPE.DFH02, dfh02, 1, 1);
		emptyRHOTBuilder.addElementBuilder(DISH_RECORD_TYPE.DFH03, dfh03, 1, 1);
		emptyRHOTBuilder.addElementBuilder(DISH_RECORD_TYPE.BOT93, bot93, 0, 1);
		emptyRHOTBuilder.addElementBuilder(DISH_RECORD_TYPE.BOT94, bot94, 0, 1);
		emptyRHOTBuilder.addElementBuilder(DISH_RECORD_TYPE.BCT95, bct95, 0, 1);
		recordFileMap.put(DISH_FILE_TYPE.EMTHOT, emptyRHOTBuilder);

		// //ETLR BUILDING
		ElementBuilder x00_ticket_header = new ElementBuilder();
		x00_ticket_header.addElement(DISH_ELEMENT.CHR13);
		x00_ticket_header.addElement(DISH_ELEMENT.CHR08);
		x00_ticket_header.addElement(DISH_ELEMENT.CHR02);

		ElementBuilder x00_mutation_ticket_header = new ElementBuilder();
		x00_mutation_ticket_header.addElement(DISH_ELEMENT.CHR01);
		x00_mutation_ticket_header.addElement(DISH_ELEMENT.CHR01);
		x00_mutation_ticket_header.addElement(DISH_ELEMENT.CHR01);

		ElementBuilder x00_25 = new ElementBuilder();
		x00_25.addElement(DISH_ELEMENT.CHR03);
		x00_25.addElement(DISH_ELEMENT.CHR40);
		x00_25.addElement(DISH_ELEMENT.CHR40);

		ElementBuilder x00_30 = new ElementBuilder();
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR08);
		x00_30.addElement(DISH_ELEMENT.CHR15);
		x00_30.addElement(DISH_ELEMENT.CHR15);
		x00_30.addElement(DISH_ELEMENT.CHR02);
		x00_30.addElement(DISH_ELEMENT.CHR20);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR04);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR08);
		x00_30.addElement(DISH_ELEMENT.CHR06);
		x00_30.addElement(DISH_ELEMENT.CHR06);
		x00_30.addElement(DISH_ELEMENT.CHR04);
		x00_30.addElement(DISH_ELEMENT.CHR20);
		x00_30.addElement(DISH_ELEMENT.CHR04);
		x00_30.addElement(DISH_ELEMENT.CHR20);
		x00_30.addElement(DISH_ELEMENT.CHR08);
		x00_30.addElement(DISH_ELEMENT.CHR14);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.NUMARIC17);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.NUMARIC17);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.NUMARIC17);
		x00_30.addElement(DISH_ELEMENT.NUMARIC07);
		x00_30.addElement(DISH_ELEMENT.CHR04);
		x00_30.addElement(DISH_ELEMENT.CHR04);
		x00_30.addElement(DISH_ELEMENT.CHR06);
		x00_30.addElement(DISH_ELEMENT.CHR07);
		x00_30.addElement(DISH_ELEMENT.CHR09);
		x00_30.addElement(DISH_ELEMENT.CHR06);
		x00_30.addElement(DISH_ELEMENT.CHR04);
		x00_30.addElement(DISH_ELEMENT.CHR01);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR11);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.NUMARIC17);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR03);
		x00_30.addElement(DISH_ELEMENT.CHR04);
		x00_30.addElement(DISH_ELEMENT.CHR20);
		x00_30.addElement(DISH_ELEMENT.CHR20);
		x00_30.addElement(DISH_ELEMENT.CHR90);

		ElementBuilder x00_50 = new ElementBuilder();
		x00_50.addElement(DISH_ELEMENT.CHR03);
		x00_50.addElement(DISH_ELEMENT.NUMARIC17);

		ElementBuilder x00_90 = new ElementBuilder();
		x00_90.addElement(DISH_ELEMENT.CHR03);
		x00_90.addElement(DISH_ELEMENT.CHR14);

		ElementBuilder x34 = new ElementBuilder();
		x34.addElement(DISH_ELEMENT.CHR13);
		x34.addElement(DISH_ELEMENT.CHR08);
		x34.addElement(DISH_ELEMENT.CHR02);
		x34.addElement(DISH_ELEMENT.NUMARIC04);
		x34.addElement(DISH_ELEMENT.CHR720);

		ElementBuilder x36 = new ElementBuilder();
		x36.addElement(DISH_ELEMENT.CHR13);
		x36.addElement(DISH_ELEMENT.CHR08);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);
		x36.addElement(DISH_ELEMENT.NUMARIC04);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.CHR02);
		x36.addElement(DISH_ELEMENT.CHR03);
		x36.addElement(DISH_ELEMENT.NUMARIC09);

		ElementBuilder x40 = new ElementBuilder();
		x40.addElement(DISH_ELEMENT.CHR13);
		x40.addElement(DISH_ELEMENT.CHR08);
		x40.addElement(DISH_ELEMENT.CHR02);
		x40.addElement(DISH_ELEMENT.NUMARIC04);
		x40.addElement(DISH_ELEMENT.NUMARIC04);
		x40.addElement(DISH_ELEMENT.NUMARIC11);
		x40.addElement(DISH_ELEMENT.NUMARIC11);
		x40.addElement(DISH_ELEMENT.CHR03);
		x40.addElement(DISH_ELEMENT.CHR03);
		x40.addElement(DISH_ELEMENT.CHR06);

		ElementBuilder x45 = new ElementBuilder();
		x45.addElement(DISH_ELEMENT.CHR13);
		x45.addElement(DISH_ELEMENT.CHR08);
		x45.addElement(DISH_ELEMENT.CHR02);
		x45.addElement(DISH_ELEMENT.NUMARIC04);
		x45.addElement(DISH_ELEMENT.CHR10);
		x45.addElement(DISH_ELEMENT.NUMARIC17);
		x45.addElement(DISH_ELEMENT.CHR03);
		x45.addElement(DISH_ELEMENT.CHR35);
		x45.addElement(DISH_ELEMENT.CHR07);
		x45.addElement(DISH_ELEMENT.CHR17);
		x45.addElement(DISH_ELEMENT.CHR03);
		x45.addElement(DISH_ELEMENT.NUMARIC17);
		x45.addElement(DISH_ELEMENT.CHR03);
		x45.addElement(DISH_ELEMENT.CHR03);
		x45.addElement(DISH_ELEMENT.CHR03);

		ElementBuilder x60 = new ElementBuilder();
		x60.addElement(DISH_ELEMENT.CHR13);
		x60.addElement(DISH_ELEMENT.CHR08);
		x60.addElement(DISH_ELEMENT.CHR02);
		x60.addElement(DISH_ELEMENT.NUMARIC04);
		x60.addElement(DISH_ELEMENT.NUMARIC04);
		x60.addElement(DISH_ELEMENT.CHR01);
		x60.addElement(DISH_ELEMENT.CHR08);
		x60.addElement(DISH_ELEMENT.NUMARIC04);
		x60.addElement(DISH_ELEMENT.CHR03);
		x60.addElement(DISH_ELEMENT.CHR15);
		x60.addElement(DISH_ELEMENT.NUMARIC17);
		x60.addElement(DISH_ELEMENT.CHR08);
		x60.addElement(DISH_ELEMENT.CHR08);
		x60.addElement(DISH_ELEMENT.CHR03);
		x60.addElement(DISH_ELEMENT.CHR03);
		x60.addElement(DISH_ELEMENT.CHR14);
		x60.addElement(DISH_ELEMENT.CHR02);
		x60.addElement(DISH_ELEMENT.CHR06);
		x60.addElement(DISH_ELEMENT.CHR02);
		x60.addElement(DISH_ELEMENT.CHR06);
		x60.addElement(DISH_ELEMENT.CHR06);
		x60.addElement(DISH_ELEMENT.CHR06);
		x60.addElement(DISH_ELEMENT.CHR04);
		x60.addElement(DISH_ELEMENT.CHR04);
		x60.addElement(DISH_ELEMENT.CHR05);
		x60.addElement(DISH_ELEMENT.CHR03);
		x60.addElement(DISH_ELEMENT.CHR04);
		x60.addElement(DISH_ELEMENT.CHR25);
		x60.addElement(DISH_ELEMENT.CHR08);
		x60.addElement(DISH_ELEMENT.CHR20);
		x60.addElement(DISH_ELEMENT.CHR03);
		x60.addElement(DISH_ELEMENT.CHR02);
		x60.addElement(DISH_ELEMENT.CHR03);

		ElementBuilder x70 = new ElementBuilder();
		x70.addElement(DISH_ELEMENT.CHR13);
		x70.addElement(DISH_ELEMENT.CHR08);
		x70.addElement(DISH_ELEMENT.CHR02);
		x70.addElement(DISH_ELEMENT.NUMARIC04);
		x70.addElement(DISH_ELEMENT.CHR13);
		x70.addElement(DISH_ELEMENT.CHR02);
		x70.addElement(DISH_ELEMENT.CHR10);
		x70.addElement(DISH_ELEMENT.CHR03);
		x70.addElement(DISH_ELEMENT.NUMARIC17);
		x70.addElement(DISH_ELEMENT.CHR03);
		x70.addElement(DISH_ELEMENT.NUMARIC17);
		x70.addElement(DISH_ELEMENT.NUMARIC17);
		x70.addElement(DISH_ELEMENT.CHR01);
		x70.addElement(DISH_ELEMENT.CHR95);

		ElementBuilder x74 = new ElementBuilder();
		x74.addElement(DISH_ELEMENT.CHR13);
		x74.addElement(DISH_ELEMENT.CHR08);
		x74.addElement(DISH_ELEMENT.CHR02);
		x74.addElement(DISH_ELEMENT.NUMARIC04);
		x74.addElement(DISH_ELEMENT.CHR13);

		ElementBuilder x80 = new ElementBuilder();
		x80.addElement(DISH_ELEMENT.CHR13);
		x80.addElement(DISH_ELEMENT.CHR08);
		x80.addElement(DISH_ELEMENT.CHR02);
		x80.addElement(DISH_ELEMENT.NUMARIC04);
		x80.addElement(DISH_ELEMENT.CHR04);
		x80.addElement(DISH_ELEMENT.CHR03);
		x80.addElement(DISH_ELEMENT.CHR04);
		x80.addElement(DISH_ELEMENT.CHR08);
		x80.addElement(DISH_ELEMENT.CHR06);
		x80.addElement(DISH_ELEMENT.NUMARIC05);
		x80.addElement(DISH_ELEMENT.CHR03);
		x80.addElement(DISH_ELEMENT.CHR03);
		x80.addElement(DISH_ELEMENT.CHR20);

		ElementBuilder x92 = new ElementBuilder();
		x92.addElement(DISH_ELEMENT.CHR13);
		x92.addElement(DISH_ELEMENT.CHR08);
		x92.addElement(DISH_ELEMENT.CHR02);
		x92.addElement(DISH_ELEMENT.NUMARIC04);
		x92.addElement(DISH_ELEMENT.NUMARIC04);
		x92.addElement(DISH_ELEMENT.CHR03);
		x92.addElement(DISH_ELEMENT.CHR14);
		x92.addElement(DISH_ELEMENT.CHR06);
		x92.addElement(DISH_ELEMENT.CHR03);
		x92.addElement(DISH_ELEMENT.CHR04);
		x92.addElement(DISH_ELEMENT.CHR08);
		x92.addElement(DISH_ELEMENT.CHR07);
		x92.addElement(DISH_ELEMENT.CHR02);
		x92.addElement(DISH_ELEMENT.CHR01);
		x92.addElement(DISH_ELEMENT.CHR01);
		x92.addElement(DISH_ELEMENT.CHR03);
		x92.addElement(DISH_ELEMENT.CHR20);
		x92.addElement(DISH_ELEMENT.CHR15);
		x92.addElement(DISH_ELEMENT.CHR04);
		x92.addElement(DISH_ELEMENT.CHR01);

		ElementBuilder x94 = new ElementBuilder();
		x94.addElement(DISH_ELEMENT.CHR13);
		x94.addElement(DISH_ELEMENT.CHR08);
		x94.addElement(DISH_ELEMENT.CHR02);
		x94.addElement(DISH_ELEMENT.NUMARIC04);
		x94.addElement(DISH_ELEMENT.NUMARIC04);
		x94.addElement(DISH_ELEMENT.CHR01);
		x94.addElement(DISH_ELEMENT.CHR03);
		x94.addElement(DISH_ELEMENT.CHR08);
		x94.addElement(DISH_ELEMENT.CHR08);
		x94.addElement(DISH_ELEMENT.CHR05);
		x94.addElement(DISH_ELEMENT.CHR03);
		x94.addElement(DISH_ELEMENT.CHR03);
		x94.addElement(DISH_ELEMENT.CHR08);
		x94.addElement(DISH_ELEMENT.CHR08);
		x94.addElement(DISH_ELEMENT.CHR05);
		x94.addElement(DISH_ELEMENT.CHR03);
		x94.addElement(DISH_ELEMENT.CHR03);
		x94.addElement(DISH_ELEMENT.CHR05);
		x94.addElement(DISH_ELEMENT.CHR03);
		x94.addElement(DISH_ELEMENT.CHR03);
		x94.addElement(DISH_ELEMENT.CHR08);
		x94.addElement(DISH_ELEMENT.CHR05);
		x94.addElement(DISH_ELEMENT.CHR03);
		x94.addElement(DISH_ELEMENT.CHR03);

		ElementBuilder xA0 = new ElementBuilder();
		xA0.addElement(DISH_ELEMENT.CHR13);
		xA0.addElement(DISH_ELEMENT.CHR08);
		xA0.addElement(DISH_ELEMENT.CHR02);
		xA0.addElement(DISH_ELEMENT.NUMARIC04);
		xA0.addElement(DISH_ELEMENT.CHR04);
		xA0.addElement(DISH_ELEMENT.CHR04);
		xA0.addElement(DISH_ELEMENT.CHR03);
		xA0.addElement(DISH_ELEMENT.CHR15);
		xA0.addElement(DISH_ELEMENT.CHR18);
		xA0.addElement(DISH_ELEMENT.CHR15);
		xA0.addElement(DISH_ELEMENT.CHR13);
		xA0.addElement(DISH_ELEMENT.CHR01);
		xA0.addElement(DISH_ELEMENT.CHR21);
		xA0.addElement(DISH_ELEMENT.CHR03);
		xA0.addElement(DISH_ELEMENT.CHR03);
		xA0.addElement(DISH_ELEMENT.CHR12);
		xA0.addElement(DISH_ELEMENT.CHR03);

		RecordBuilder etlrRec = new RecordBuilder();
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X00, x00_ticket_header, 1, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X00M, x00_mutation_ticket_header, 1, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X00_25, x00_25, 1, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X00_30, x00_30, 1, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X00_50, x00_50, 1, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X00_90, x00_90, 1, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X34, x34, 1, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X36, x36, 0, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X40, x40, 0, 1);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X45, x45, 1, 2);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X60, x60, 1, 16);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X70, x70, 0, 16);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X74, x74, 0, RecordMetaData.UNLIMITED);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X80, x80, 0, RecordMetaData.UNLIMITED);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X92, x92, 1, RecordMetaData.UNLIMITED);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.X94, x94, 0, RecordMetaData.UNLIMITED);
		etlrRec.addElementBuilder(DISH_RECORD_TYPE.XA0, xA0, 1, RecordMetaData.UNLIMITED);
		recordFileMap.put(DISH_FILE_TYPE.ETLR, etlrRec);
	}

	private void createBKCommonElement(ElementBuilder bks24) {
		bks24.addElement(DISH_ELEMENT.SMSG);
		bks24.addElement(DISH_ELEMENT.SQNR);
		bks24.addElement(DISH_ELEMENT.STNQ);
		bks24.addElement(DISH_ELEMENT.DAIS);
		bks24.addElement(DISH_ELEMENT.TRNN);
		bks24.addElement(DISH_ELEMENT.TDNR);
		bks24.addElement(DISH_ELEMENT.CDGT);
	}

	public enum DISH_FILE_TYPE {
		TKTT(FMT_ID.X), EMDA(FMT_ID.X), CANX(FMT_ID.S), CANR(FMT_ID.S), RFND(FMT_ID.NA), TKTA(FMT_ID.X), TKTB(FMT_ID.X), CANN(
				FMT_ID.S), EMTY(FMT_ID.S), EMTHOT(FMT_ID.S), BKTT(FMT_ID.X), EMDS(FMT_ID.X), ETLR(FMT_ID.X);

		private FMT_ID formatId;

		DISH_FILE_TYPE(FMT_ID formatId) {
			this.formatId = formatId;
		}

		public FMT_ID getFormatId() {
			return formatId;
		}
	}

	public enum DISH_RECORD_TYPE {
		IT01(1, FMT_ID.S), IT02(2, FMT_ID.S, FMT_ID.L, FMT_ID.X), IT0S("S", FMT_ID.S, FMT_ID.X), IT03(3, FMT_ID.S, FMT_ID.L,
				FMT_ID.X), IT04(4, FMT_ID.S, FMT_ID.L, FMT_ID.X), IT05(5, FMT_ID.S, FMT_ID.L, FMT_ID.X), IT06(6, FMT_ID.L,
				FMT_ID.X), IT02CNJ(2, FMT_ID.S, FMT_ID.L, FMT_ID.X), IT06CNJ(6, FMT_ID.L, FMT_ID.X), IT0G("G", FMT_ID.X), IT07(7,
				FMT_ID.X), IT08(8, FMT_ID.S, FMT_ID.L, FMT_ID.X), IT0Y("Y", FMT_ID.S, FMT_ID.X), IT09(9, FMT_ID.X), IT0A("A",
				FMT_ID.X), IT0B("B", FMT_ID.X), IT0C("C", FMT_ID.X), IT0D("D", FMT_ID.X), IT0E("E", FMT_ID.X), IT0Z("Z", FMT_ID.S), DFH01(
				1, FMT_ID.S), DFH02(2, FMT_ID.S), DFH03(3, FMT_ID.S), BKT06(4, FMT_ID.S, FMT_ID.L, FMT_ID.X), BKS24(5, FMT_ID.S,
				FMT_ID.L, FMT_ID.X), BKS30(6, FMT_ID.S, FMT_ID.L, FMT_ID.X), BKS39(7, FMT_ID.S, FMT_ID.L, FMT_ID.X), BKS42(8,
				FMT_ID.S, FMT_ID.L, FMT_ID.X), BKS45(9, FMT_ID.S, FMT_ID.L, FMT_ID.X), BKS46(10, FMT_ID.S, FMT_ID.L, FMT_ID.X), BKI63(
				11, FMT_ID.S, FMT_ID.L, FMT_ID.X), BAR64(12, FMT_ID.S, FMT_ID.L, FMT_ID.X), BAR65(13, FMT_ID.S, FMT_ID.L,
				FMT_ID.X), BAR66(14, FMT_ID.S, FMT_ID.L, FMT_ID.X), BMP70(15, FMT_ID.S, FMT_ID.L, FMT_ID.X), BMP71(16, FMT_ID.S,
				FMT_ID.L, FMT_ID.X), BMP72(17, FMT_ID.S, FMT_ID.L, FMT_ID.X), BMP73(18, FMT_ID.S, FMT_ID.L, FMT_ID.X), BMP74(19,
				FMT_ID.S, FMT_ID.L, FMT_ID.X), BMD75(20, FMT_ID.S, FMT_ID.L, FMT_ID.X), BMD76(21, FMT_ID.S, FMT_ID.L, FMT_ID.X), BMP77(
				22, FMT_ID.S, FMT_ID.L, FMT_ID.X), BMP78(23, FMT_ID.S, FMT_ID.L, FMT_ID.X), BKF81(24, FMT_ID.S, FMT_ID.L,
				FMT_ID.X), BKP83(25, FMT_ID.S, FMT_ID.L, FMT_ID.X), BKP84(26, FMT_ID.S, FMT_ID.L, FMT_ID.X), BOT93(27, FMT_ID.S,
				FMT_ID.L, FMT_ID.X), BOT94(28, FMT_ID.S, FMT_ID.L, FMT_ID.X), BCT95(29, FMT_ID.S, FMT_ID.L, FMT_ID.X), BFT99(30,
				FMT_ID.S, FMT_ID.L, FMT_ID.X), X00(1, FMT_ID.X), X00M(2, FMT_ID.S, FMT_ID.L, FMT_ID.X), X00_25(3, FMT_ID.S,
				FMT_ID.L, FMT_ID.X), X00_30(4, FMT_ID.S, FMT_ID.L, FMT_ID.X), X00_50(5, FMT_ID.S, FMT_ID.L, FMT_ID.X), X00_90(6,
				FMT_ID.S, FMT_ID.L, FMT_ID.X), X34(7, FMT_ID.S, FMT_ID.L, FMT_ID.X), X36(8, FMT_ID.S, FMT_ID.L, FMT_ID.X), X40(9,
				FMT_ID.S, FMT_ID.L, FMT_ID.X), X45(10, FMT_ID.S, FMT_ID.L, FMT_ID.X), X60(11, FMT_ID.S, FMT_ID.L, FMT_ID.X), X70(
				12, FMT_ID.S, FMT_ID.L, FMT_ID.X), X74(12, FMT_ID.S, FMT_ID.L, FMT_ID.X), X80(12, FMT_ID.S, FMT_ID.L, FMT_ID.X), X92(
				12, FMT_ID.S, FMT_ID.L, FMT_ID.X), X94(12, FMT_ID.S, FMT_ID.L, FMT_ID.X), XA0(12, FMT_ID.S, FMT_ID.L, FMT_ID.X);

		private String recordId;
		private FMT_ID[] supportedFmts;

		DISH_RECORD_TYPE(String recordId, FMT_ID... fmtIds) {
			this.recordId = recordId;
			supportedFmts = fmtIds;
		}

		DISH_RECORD_TYPE(int recordId, FMT_ID... fmtIds) {
			this.recordId = recordId + "";
			supportedFmts = fmtIds;
		}

		public String recordId() {
			return this.recordId;
		}

		public boolean isSupportedFmt(FMT_ID fmtId) {
			if (supportedFmts != null) {
				Set<FMT_ID> compatSet = new HashSet<ElementFactory.FMT_ID>();
				switch (fmtId) {
				case X:
					compatSet.add(FMT_ID.X);
				case L:
					compatSet.add(FMT_ID.L);
				case S:
					compatSet.add(FMT_ID.S);
					break;
				}
				for (FMT_ID tmp : supportedFmts) {
					if (compatSet.contains(tmp)) {
						return true;
					}
				}
			}
			return false;
		}
	}

	public static DISH_RECORD_TYPE getPrecedingRecordType(DISH_RECORD_TYPE type) {
		DISH_RECORD_TYPE prevRecord = null;
		for (DISH_RECORD_TYPE itrRecord : DISH_RECORD_TYPE.values()) {
			if (type == itrRecord)
				break;
			prevRecord = itrRecord;
		}
		return prevRecord;
	}

	public RecordBuilder getRecordBuilder(DISH_FILE_TYPE fileType) {
		return recordFileMap.get(fileType);
	}

	public RecordMetaData getRecordMetaData(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType) {
		return recordFileMap.get(fileType).getRecordMetaData(recordType);
	}

	public OrderedElementContainer getOrderedElementContainer(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType) {
		OrderedElementContainer container = new OrderedElementContainer();

		for (ElementMetaData metaData : recordFileMap.get(fileType).getElementBuilder(recordType).getMetaDataList()) {
			container.addElement(metaData);
		}

		return container;
	}

	public static RecordFactory getInstance() throws BSPException {
		if (recordFactory == null) {
			recordFactory = new RecordFactory();
		}
		return recordFactory;
	}
}
