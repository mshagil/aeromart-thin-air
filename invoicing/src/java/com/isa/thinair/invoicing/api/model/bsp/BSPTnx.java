package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;
import java.util.Date;

public class BSPTnx {
	private Integer pnrPaxID;

	private int paxSequence;

	private BigDecimal amount;

	private Date tnxDate;

	private String tnxType;

	private BigDecimal payCurrencyAmount;

	private String payCurrencyCode;

	private int nominalCode;

	private String nomialDesc;

	private String lccUniqueTnxID;

	private Integer tnxID;

	private Integer originalPaymentID;

	private String agentCode;

	private String iataAgentCode;

	private String pnr;

	private String paxName;
	
	private String country;

	private boolean isFreshReservationPayment;

	private String externalReference;
	
	private String productType;

	public Integer getPnrPaxID() {
		return pnrPaxID;
	}

	public void setPnrPaxID(Integer pnrPaxID) {
		this.pnrPaxID = pnrPaxID;
	}

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getTnxDate() {
		return tnxDate;
	}

	public void setTnxDate(Date tnxDate) {
		this.tnxDate = tnxDate;
	}

	public String getTnxType() {
		return tnxType;
	}

	public void setTnxType(String tnxType) {
		this.tnxType = tnxType;
	}

	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;
	}

	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	public int getNominalCode() {
		return nominalCode;
	}

	public void setNominalCode(int nominalCode) {
		this.nominalCode = nominalCode;
	}

	public String getLccUniqueTnxID() {
		return lccUniqueTnxID;
	}

	public void setLccUniqueTnxID(String lccUniqueTnxID) {
		this.lccUniqueTnxID = lccUniqueTnxID;
	}

	public Integer getTnxID() {
		return tnxID;
	}

	public void setTnxID(Integer tnxID) {
		this.tnxID = tnxID;
	}

	public Integer getOriginalPaymentID() {
		return originalPaymentID;
	}

	public void setOriginalPaymentID(Integer originalPaymentID) {
		this.originalPaymentID = originalPaymentID;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getIataAgentCode() {
		return iataAgentCode;
	}

	public void setIataAgentCode(String iataAgentCode) {
		this.iataAgentCode = iataAgentCode;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}
	
	public boolean isFreshReservationPayment() {
		return isFreshReservationPayment;
	}

	public void setFreshReservationPayment(boolean isFreshReservationPayment) {
		this.isFreshReservationPayment = isFreshReservationPayment;
	}

	public boolean equals(Object other) {
		if (other instanceof BSPTnx) {
			BSPTnx tnx = (BSPTnx) other;
			if (this.getLccUniqueTnxID() != null && tnx.getLccUniqueTnxID() != null) {
				if (this.lccUniqueTnxID.equals(tnx.getLccUniqueTnxID()) && this.getPaxSequence() == tnx.getPaxSequence()) {
					return true;
				} else {
					return false;
				}
			} else if ((this.getTnxID() != null && this.getTnxID().intValue() != 0)
					&& (tnx.getTnxID() != null && tnx.getTnxID().intValue() != 0)
					&& (this.getPaxSequence() == tnx.getPaxSequence())) {
				if (this.getTnxID().equals(tnx.getTnxID())) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		if (this.getTnxID() != null && this.getTnxID() != 0) {
			return this.getTnxID().hashCode() + this.getPaxSequence();
		} else if (this.getLccUniqueTnxID() != null) {
			return this.getLccUniqueTnxID().hashCode() + this.getPaxSequence();
		} else {
			return (int) Math.round(Math.random());
		}
	}

	public String getNomialDesc() {
		return nomialDesc;
	}

	public void setNomialDesc(String nomialDesc) {
		this.nomialDesc = nomialDesc;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
}
