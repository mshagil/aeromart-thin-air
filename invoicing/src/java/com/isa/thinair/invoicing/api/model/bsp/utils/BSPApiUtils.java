package com.isa.thinair.invoicing.api.model.bsp.utils;


public class BSPApiUtils {

	public static String calculateMod7CheckBit(String numberStr) {
		if (numberStr == null || numberStr.trim().equals("")) {
			return "";
		}
		long number = Long.parseLong(numberStr);
		return calculateMod7CheckBit(number);
	}

	public static String calculateMod7CheckBit(long number) {
		long chkBit = number % 7;
		return chkBit + "";
	}
	
	public static char getSignedFieldValueReplacement(char character) {

		char replacementChar = ' ';
		switch (character) {
		case '{':
			replacementChar = '0';
			break;
		case 'A':
			replacementChar = '1';
			break;
		case 'B':
			replacementChar = '2';
			break;
		case 'C':
			replacementChar = '3';
			break;
		case 'D':
			replacementChar = '4';
			break;
		case 'E':
			replacementChar = '5';
			break;
		case 'F':
			replacementChar = '6';
			break;
		case 'G':
			replacementChar = '7';
			break;
		case 'H':
			replacementChar = '8';
			break;
		case 'I':
			replacementChar = '9';
			break;
		case '}':
			replacementChar = '0';
			break;
		case 'J':
			replacementChar = '1';
			break;
		case 'K':
			replacementChar = '2';
			break;
		case 'L':
			replacementChar = '3';
			break;
		case 'M':
			replacementChar = '4';
			break;
		case 'N':
			replacementChar = '5';
			break;
		case 'O':
			replacementChar = '6';
			break;
		case 'P':
			replacementChar = '7';
			break;
		case 'Q':
			replacementChar = '8';
			break;
		case 'R':
			replacementChar = '9';
			break;

		}
		return replacementChar;
	}
}
