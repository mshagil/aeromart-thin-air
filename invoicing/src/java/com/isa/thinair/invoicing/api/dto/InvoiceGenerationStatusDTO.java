/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.isa.thinair.commons.api.dto.Page;

/**
 * @author: Byorn
 * 
 */
public class InvoiceGenerationStatusDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6485851774437828193L;

	/** Holds the total amount generated for the period **/
	private BigDecimal totalAmountForPeriod;

	private BigDecimal totalAmountTransfered;

	private boolean hasEXDB = false;

	/** Holds the Collection of GeneratedInvoicesDTO in the Page object **/
	private Page invoices;

	/**
	 * @return Returns the invoices.
	 */
	public Page getInvoices() {
		return invoices;
	}

	/**
	 * @param invoices
	 *            The invoices to set.
	 */
	public void setInvoices(Page invoices) {
		this.invoices = invoices;
	}

	/**
	 * @return Returns the totalAmountForPeriod.
	 */
	public BigDecimal getTotalAmountForPeriod() {
		return totalAmountForPeriod;
	}

	/**
	 * @param totalAmountForPeriod
	 *            The totalAmountForPeriod to set.
	 */
	public void setTotalAmountForPeriod(BigDecimal totalAmountForPeriod) {
		this.totalAmountForPeriod = totalAmountForPeriod;
	}

	/**
	 * @return Returns the totalAmountTransfered.
	 */
	public BigDecimal getTotalAmountTransfered() {
		return totalAmountTransfered;
	}

	/**
	 * @param totalAmountTransfered
	 *            The totalAmountTransfered to set.
	 */
	public void setTotalAmountTransfered(BigDecimal totalAmountTransfered) {
		this.totalAmountTransfered = totalAmountTransfered;
	}

	/**
	 * @return Returns the amountStatus.
	 */
	public String getAmountStatus() {

		NumberFormat formatter = new DecimalFormat("#,###,###.00");
		if (this.totalAmountForPeriod != null) {

			if (this.totalAmountTransfered != null) {

				if (Math.round(this.totalAmountForPeriod.doubleValue()) == Math.round(this.totalAmountTransfered.doubleValue())) {
					return "For above period " + formatter.format(this.totalAmountForPeriod.doubleValue())
							+ " is Generated and Transfered";
				} else {
					if (Math.round(this.totalAmountTransfered.doubleValue()) == 0
							|| Math.round(this.totalAmountTransfered.doubleValue()) == 0.0) {
						return "For above period " + formatter.format(this.totalAmountForPeriod.doubleValue())
								+ " is Generated but NOT Transfered";
					} else {
						if (this.getTotalAmountTransfered().intValue() == -1) {
							return "Transferring ... .. .. .. .....";
						}

						return formatter.format(this.totalAmountForPeriod.doubleValue()) + " is Generated BUT Transfered is : "
								+ formatter.format(this.getTotalAmountTransfered().doubleValue());
					}

				}
			} else {
				if (hasEXDB) {
					return "For above period " + formatter.format(this.totalAmountForPeriod.doubleValue())
							+ " is Generated but NOT Transfered";
				} else {
					return "For above period " + formatter.format(this.totalAmountForPeriod.doubleValue()) + " is Generated ";

				}
			}

		}

		return "No Invoices For the selected Period";
	}

	/**
	 * @return Returns the hasEXDB.
	 */
	public boolean isHasEXDB() {
		return hasEXDB;
	}

	/**
	 * @param hasEXDB
	 *            The hasEXDB to set.
	 */
	public void setHasEXDB(boolean hasEXDB) {
		this.hasEXDB = hasEXDB;
	}

}