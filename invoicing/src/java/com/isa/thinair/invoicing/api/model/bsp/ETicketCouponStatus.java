package com.isa.thinair.invoicing.api.model.bsp;

import java.util.Date;

public class ETicketCouponStatus {

	private String eTicketNumber;

	private Date modifiedDate;

	private String couponNumber;

	private String couponStatus;

	private String pnr;

	private String psudoCity;

	private String agentID;

	private String agentCode;

	public String geteTicketNumber() {
		return eTicketNumber;
	}

	public void seteTicketNumber(String eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPsudoCity() {
		return psudoCity;
	}

	public void setPsudoCity(String psudoCity) {
		this.psudoCity = psudoCity;
	}

	public String getAgentID() {
		return agentID;
	}

	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

}