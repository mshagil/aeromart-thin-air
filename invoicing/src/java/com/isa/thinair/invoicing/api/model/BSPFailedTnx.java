package com.isa.thinair.invoicing.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * BSPFailedTnxDTO is the entity class to represent BSP Failed transactions
 * 
 * @hibernate.class table = "T_BSP_TRANSACTION_LOG"
 */
public class BSPFailedTnx extends Persistent implements Serializable {

	private Integer bspTxnLogID;

	private String agentCode;

	private Integer tnxID;

	private String lccUniqueID;

	private String status;

	private String description;

	private String reportingUserID;

	private Integer paxSequence;

	private Date creatingDate;

	private String modifiedBy;

	private Date modifiedDate;
	
	private String countryCode;

	/**
	 * @return Returns the bspTxnLogID.
	 * 
	 * @hibernate.id column="BSP_TXN_LOG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BSP_TRANSACTION_LOG"
	 */
	public Integer getBspTxnLogID() {
		return bspTxnLogID;
	}

	public void setBspTxnLogID(Integer bspTxnLogID) {
		this.bspTxnLogID = bspTxnLogID;
	}

	/**
	 * @return Returns the agentCode.
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the tnxID.
	 * @hibernate.property column = "TXN_ID"
	 */
	public Integer getTnxID() {
		return tnxID;
	}

	/**
	 * @param tnxID
	 *            The tnxID to set.
	 */
	public void setTnxID(Integer tnxID) {
		this.tnxID = tnxID;
	}

	/**
	 * @return Returns the lccUniqueID.
	 * @hibernate.property column = "LCC_UNIQUE_TXN_ID"
	 */
	public String getLccUniqueID() {
		return lccUniqueID;
	}

	/**
	 * @param lccUniqueID
	 *            The lccUniqueID to set.
	 */
	public void setLccUniqueID(String lccUniqueID) {
		this.lccUniqueID = lccUniqueID;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the description.
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the reportingUserID.
	 * @hibernate.property column = "CREATED_BY"
	 */
	public String getReportingUserID() {
		return reportingUserID;
	}

	/**
	 * @param reportingUserID
	 *            The reportingUserID to set.
	 */
	public void setReportingUserID(String reportingUserID) {
		this.reportingUserID = reportingUserID;
	}

	/**
	 * @return Returns the paxSequence.
	 * @hibernate.property column = "PAX_SEQUENCE"
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            The paxSequence to set.
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return Returns the creatingDate.
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatingDate() {
		return creatingDate;
	}

	/**
	 * @param creatingDate
	 *            The creatingDate to set.
	 */
	public void setCreatingDate(Date creatingDate) {
		this.creatingDate = creatingDate;
	}

	/**
	 * @return Returns the modifiedBy.
	 * @hibernate.property column = "MODIFIED_BY"
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            The modifiedBy to set.
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return Returns the modifiedDate.
	 * @hibernate.property column = "MODIFIED_DATE"
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            The modifiedDate to set.
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return Returns the countryCode.
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            The countryCode to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
