package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.FMT_ID;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BKI63_ItineraryDataSegment extends BasicBKRecord implements DishTransactional {

	public BKI63_ItineraryDataSegment(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BKI63, "BKI", 63l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);

	}

	public void setStandardNumericQulaifier(String numQualifier) throws BSPException {
		dataContainer.setValue(3, numQualifier);
	}

	public void setRemitanceEndingDate(Date endingDate) throws BSPException {
		dataContainer.setValue(4, endingDate);
	}

	public void setTrasactionNo(long transactionNo) throws BSPException {
		dataContainer.setValue(5, transactionNo);
	}

	public void setDocumentNumber(String documentNo) throws BSPException {
		dataContainer.setValue(6, documentNo);
	}

	public void setCheckDigit(Long digit) throws BSPException {
		dataContainer.setValue(7, digit);
	}

	public void setSegmentIdentifier(String segmentIdentifier) throws BSPException {
		dataContainer.setValue(8, segmentIdentifier);
	}

	public void setStopoverCode(String stepOverCode) throws BSPException {
		dataContainer.setValue(9, stepOverCode);
	}

	public void setNotValidBeforeDate(Date beforeDate) throws BSPException {
		dataContainer.setValue(10, beforeDate);
	}

	public void setNotValidAfterDate(Date afterDate) throws BSPException {
		dataContainer.setValue(11, afterDate);
	}

	public void setReservedSpace(String resSpace) throws BSPException {
		dataContainer.setValue(12, resSpace);
		dataContainer.setValue(16, resSpace);
		dataContainer.setValue(20, resSpace);
		dataContainer.setValue(22, resSpace);
		dataContainer.setValue(25, resSpace);
		dataContainer.setValue(28, resSpace);
	}

	public void setOriginAirport(String airportCode) throws BSPException {
		dataContainer.setValue(13, airportCode);
	}

	public void setDestinationAirport(String airportCode) throws BSPException {
		dataContainer.setValue(14, airportCode);
	}

	public void setCarrie(String carrier) throws BSPException {
		dataContainer.setValue(15, carrier);
	}

	public void setFlightNumber(String flightNo) throws BSPException {
		dataContainer.setValue(17, flightNo);
	}

	public void setReservationBookingDesignator(String resDesignator) throws BSPException {
		dataContainer.setValue(18, resDesignator);
	}

	public void setFlightDate(Date flightDate) throws BSPException {
		dataContainer.setValue(19, flightDate);
	}

	public void setDepartureTime(Date flightDate) throws BSPException {
		dataContainer.setValue(21, flightDate);
	}

	public void setFlightBookingStatus(String bookingStatus) throws BSPException {
		dataContainer.setValue(23, bookingStatus);
	}

	public void setFlightBaggageAllowance(String baggageAllowance) throws BSPException {
		dataContainer.setValue(24, baggageAllowance);
	}

	public void setFareBasisDesignator(String fareBasisDesignator) throws BSPException {
		dataContainer.setValue(26, fareBasisDesignator);
	}

	public void setFrequentFlyer(String freqFlyerRef) throws BSPException {
		dataContainer.setValue(27, freqFlyerRef);
	}

	public void setTransactionCode(DISH_FILE_TYPE transactionCode) throws BSPException {
		dataContainer.setValue(10, transactionCode.toString());
	}

	public void setFormatIdentifier(FMT_ID formatIdentifier) throws BSPException {
		dataContainer.setValue(15, formatIdentifier.toString());
	}

}
