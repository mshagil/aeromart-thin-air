package com.isa.thinair.invoicing.api.service;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.invoicing.api.dto.GenerateInvoiceOption;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceGenerationStatusDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesReceiptsDTO;
import com.isa.thinair.invoicing.api.model.BSPFailedTnx;
import com.isa.thinair.invoicing.api.model.BSPHOTFileLog;
import com.isa.thinair.invoicing.api.model.bsp.BSPReconciliationRecordDTO;
import com.isa.thinair.invoicing.api.model.bsp.ReportFileTypes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public interface InvoicingBD {

	public static final String SERVICE_NAME = "InvoicingService";

	public void sendInvoiceMail(GeneratedInvoiceDTO generatedInvoicesDTO) throws ModuleException;

	/**
	 * Email generted Invoices to the respective agents Billing Email Address
	 * 
	 * @param searchInvoicesDTO
	 * @throws ModuleException
	 */
	public void sendInvoiceMail(SearchInvoicesDTO searchInvoicesDTO) throws ModuleException;

	/**
	 * Transfer the Generated Invoices to the External Interface Tables.
	 * 
	 * @param searchInvoicesDTO
	 * @throws ModuleException
	 */
	public void transferToInterfaceTables(SearchInvoicesDTO searchInvoicesDTO) throws ModuleException;

	/**
	 * Auto Invoicing called by scheduler job
	 * 
	 * @throws ModuleException
	 */
	public void autoEmailInvoices() throws ModuleException;

	public void transferInvoices(Collection invoices) throws ModuleException;

	/**
	 * Get invoice details
	 * 
	 * @param searchInvoicesDTO
	 * @param startIndex
	 * @param maxRecords
	 * @return InvoiceGenerationStatusDTO collection
	 * @throws ModuleException
	 */
	public InvoiceGenerationStatusDTO getGeneratedInvoicesDetails(SearchInvoicesDTO searchInvoicesDTO, int startIndex,
			int maxRecords) throws ModuleException;

	/**
	 * Generate invoices for selected or all agents
	 * 
	 * @param searchInvoicesDTO
	 * @throws ModuleException
	 */
	public ServiceResponce generateInvoices(SearchInvoicesDTO searchInvoicesDTO, GenerateInvoiceOption generateInvoiceOption)
			throws ModuleException;

	/**
	 * 
	 * @param agentCode
	 * @return
	 */
	public Collection getUnsettledInvoicesForAgent(String agentCode) throws ModuleException;

	/**
	 * 
	 * @param critiria
	 * @return
	 * @throws ModuleException
	 */
	public InvoiceSummaryDTO searchInvoiceForAgent(String agentGSACode, String year, String month, int billingPeriod,
			boolean payCurr, String invoiceNo, String pnr,String entity) throws ModuleException;

	/**
	 * 
	 * @param critiria
	 * @return
	 * @throws ModuleException
	 */
	public Collection<GeneratedInvoiceDTO> searchGeneratedInvoicesForAgent(String agentGSACode, String dateFrom, String dateTo)
			throws ModuleException;

	/**
	 * Method to settle invoices.
	 * 
	 * @param agentCode
	 * @param settleCollection
	 * @throws ModuleException
	 */
	public void settleInvoice(String agentCode, Collection settleCollection) throws ModuleException;

	public ServiceResponce generateInvoicesReciptsForRefunds(Collection<AgentTotalSaledDTO> agenTotalSaledDTOs,
			SearchInvoicesReceiptsDTO searchInvoicesReceiptsDTO) throws ModuleException;

	public ServiceResponce generateInvoicesRecipts(SearchInvoicesReceiptsDTO searchInvoicesReceiptsDTO) throws ModuleException;

	public ResultSet getInvoiceReportDataSet(Collection<String> invoiceIDs) throws ModuleException;

	public void publishDailySalesToBSP(Date rptPeriodStartDate, Date rptPeriodEndDate, boolean processErrorsOnly,
			String countryCode, String agentCode) throws ModuleException;

	public void uploadPendingRETFiles() throws ModuleException;

	public Page getBSPReports(int startrec, int numofrecs) throws ModuleException;

	public Page getBSPReports(int startrec, int numofrecs, List criteria) throws ModuleException;

	public BSPFailedTnx getChangingBSPFailedTnx(String bspTxnLogID) throws ModuleException;

	public void saveOrUpdateBSPFailedTransaction(BSPFailedTnx savingBspFailedTnx) throws ModuleException;

	public Page getBSPHOTFileRecords(int startrec, int numofrecs, List criteria) throws ModuleException;

	public void saveOrUpdateBSPHOTFile(BSPHOTFileLog hotFile) throws ModuleException;

	public void deleteBSPHOTFile(Integer hotFileLogId) throws ModuleException;

	public void processBSPHOTFile(String hotFileContent, String hotFileName) throws ModuleException;

	public List<BSPReconciliationRecordDTO> getBSPReconciliationReportData(ReportsSearchCriteria criteria) throws ModuleException;

	public void generateReportFile(Date rptPeriodStartDate, Date rptPeriodEndDate, boolean processErrorsOnly,
			ReportFileTypes.FileTypes fileType) throws ModuleException;
	
	/**
	 * X3 to Acceleaero : Transfer BSP sales summary
	 */
	
	/**
	 * Transfer the Generated BSPS to the JMS Queue.
	 * 
	 * @param searchDTO
	 * @throws ModuleException
	 */
	public void transferBSPsToQueue(SearchInvoicesDTO searchDTO) throws ModuleException;
	
	/**
	 * Transfer the BSPs to External DBs.
	 * 
	 * @param collection
	 * @throws ModuleException
	 */
	public void transferBSPsalesToExternalInterface(Collection collection) throws ModuleException;
}