/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicHOTFileRecordParser;

/**
 * @author Janaka Padukka
 * 
 */
public class BKS24 extends BasicHOTFileRecordParser {

	private Date DAIS;

	private String TRNN;

	private Long TDNR;

	private String CDGT;

	private String TCNR;

	private String TCND;

	private String CPUI;

	private String CJCP;

	private String AGTN;

	private String TOUR;

	private String TRNC;

	private String TODC;

	private String PNRR;

	public BKS24(String recordString, String recordType) throws BSPException {
		super(recordString, recordType);
		parseRecord();

	}

	public void parseRecord() throws BSPException {
		// Only required fields are parsed
		DateFormat df = new SimpleDateFormat("yyMMdd");
		try {
			this.DAIS = df.parse(this.recordString.substring(13, 19));
		} catch (ParseException e) {
			throw new BSPException("Invalid date format", e);
		}
		this.TDNR = new Long(this.recordString.substring(25, 39).trim());
		this.TRNC = this.recordString.substring(71, 75);
		this.PNRR = this.recordString.substring(85, 98).trim();
	}

	public Date getDAIS() {
		return DAIS;
	}

	public String getTRNN() {
		return TRNN;
	}

	public Long getTDNR() {
		return TDNR;
	}

	public String getCDGT() {
		return CDGT;
	}

	public String getTCNR() {
		return TCNR;
	}

	public String getTCND() {
		return TCND;
	}

	public String getCPUI() {
		return CPUI;
	}

	public String getCJCP() {
		return CJCP;
	}

	public String getAGTN() {
		return AGTN;
	}

	public String getTOUR() {
		return TOUR;
	}

	public String getTRNC() {
		return TRNC;
	}

	public String getTODC() {
		return TODC;
	}

	public String getPNRR() {
		return PNRR;
	}
}
