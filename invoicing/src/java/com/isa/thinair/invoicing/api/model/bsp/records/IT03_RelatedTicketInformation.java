package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.utils.BSPApiUtils;

public class IT03_RelatedTicketInformation extends BasicRecord implements DishTransactional {
	private final static int MAX_RELATED_TICKET_SETS = 7;

	private int currentRelatedTicketCount = 0;

	public IT03_RelatedTicketInformation(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT03);
	}

	public void setTransactionNo(Long id) throws BSPException {
		dataContainer.setValue(2, id);
	}

	public void addTicketCoupon(String relatedTicketCouponId, String relatedTicketNumber) throws BSPException {
		if (MAX_RELATED_TICKET_SETS <= currentRelatedTicketCount) {
			throw new RuntimeException("Max number of related tickets allowed exceeded");
		}
		int offset = currentRelatedTicketCount * 4;
		dataContainer.setValue(3 + offset, relatedTicketCouponId);
		dataContainer.setValue(4 + offset, relatedTicketNumber);
		dataContainer.setValue(5 + offset, BSPApiUtils.calculateMod7CheckBit(relatedTicketNumber));
		currentRelatedTicketCount++;
	}

	public void setDateOfIssueRelatedDoc(Date dateOfIssueRelatedDoc) throws BSPException {
		dataContainer.setValue(24, dateOfIssueRelatedDoc);
	}

	public void setTourCode(String tourCode) throws BSPException {
		dataContainer.setValue(25, tourCode);
	}

	public void setWaiverCode(String waiverCode) throws BSPException {
		dataContainer.setValue(26, waiverCode);
	}

	public void setReasonForMemoIssuance(String reasonForMemoIssuance) throws BSPException {
		dataContainer.setValue(27, reasonForMemoIssuance);
	}

}
