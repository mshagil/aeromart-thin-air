/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp.filetypes;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.records.IT04_AdditionalSaleInfo;
import com.isa.thinair.invoicing.api.model.bsp.records.IT05_MonetaryAmounts;
import com.isa.thinair.invoicing.api.model.bsp.records.IT08_FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.records.IT09_AdditionalInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0A_MiscDocInfo;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0G_ElectronicMiscDocCouponDetailsNRemarks;

/**
 * @author Janaka Padukka
 *
 */
public class EMDS extends BasicFileType {

	public EMDS() {
		super(DISH_FILE_TYPE.EMDS);
	}
	
	public void setAdditionalSalesInfo(IT04_AdditionalSaleInfo additionalSalesInfo) throws BSPException {
		setTransactionId(additionalSalesInfo);
		addElement(additionalSalesInfo);
	}
	
	public void addMonetaryAmts(IT05_MonetaryAmounts monetaryAmts) throws BSPException {
		setTransactionId(monetaryAmts);
		addElement(monetaryAmts);
	}
	
	public void addElectronicMiscDocument(IT0G_ElectronicMiscDocCouponDetailsNRemarks emd) throws BSPException{
		setTransactionId(emd);
		addElement(emd);
	}
	
	public void addFormOfPayment(IT08_FormOfPayment formOfPayment) throws BSPException {
		setTransactionId(formOfPayment);
		addElement(formOfPayment);
	}
		
	public void setAdditionalInfo(IT09_AdditionalInformation additionalInfo) throws BSPException {
		setTransactionId(additionalInfo);
		addElement(additionalInfo);
	}
	
	public void addMiscDocumentInfo(IT0A_MiscDocInfo miscDocInfo) throws BSPException {
		setTransactionId(miscDocInfo);
		addElement(miscDocInfo);
	}
}
