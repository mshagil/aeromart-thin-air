package com.isa.thinair.invoicing.api.model.bsp;

import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.records.BCT95_BillingAnalysisCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BFT99_FileTotalCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BOT93_TransactionCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BOT94_CurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH01_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH02_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH03_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.IT01_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0Z_FileTrailer;

public interface DISHStructured extends DISHGeneratable {
	public void setFileHeader(IT01_FileHeader fileHeader) throws BSPException;

	public void setFileTrailer(IT0Z_FileTrailer fileTrailer) throws BSPException;

	public DISH_FILE_TYPE getFileType();

	public void setTrnxId(Long tnxId);

	public void setDFHFileHeader(DFH01_FileHeader fileHeader) throws BSPException;

	public void setDFH2FileHeader(DFH02_FileHeader fileHeader) throws BSPException;

	public void setDFH3FileHeader(DFH03_FileHeader fileHeader) throws BSPException;

	public void setBOT93FileHeader(BOT93_TransactionCurrencyType bot93Trailer) throws BSPException;

	public void setBOT94FileHeader(BOT94_CurrencyType bct95Trailer) throws BSPException;

	public void setBCT95FileHeader(BCT95_BillingAnalysisCurrencyType bft99Trailer) throws BSPException;

	public void setBFT99FileHeader(BFT99_FileTotalCurrencyType bot94Trailer) throws BSPException;
}
