package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DE_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DISH_ELEMENT;

public class OrderedElementContainer implements DISHGeneratable {

	private static Log log = LogFactory.getLog(OrderedElementContainer.class);

	private List<BasicDataType> elementList = new ArrayList<BasicDataType>();
	private List<ElementMetaData> metaDataList = new ArrayList<ElementMetaData>();

	public void addElement(ElementMetaData metaData) {
		BasicDataType element = null;
		if (metaData.getElement() == DISH_ELEMENT.RESD) {
			element = ElementFactory.getElement(metaData.getElement(), metaData.getLength());
		} else {
			element = ElementFactory.getElement(metaData.getElement());
		}
		elementList.add(element);
		metaDataList.add(metaData);
	}

	public void setValue(int elementId, String value) throws BSPException {
		setValue(elementId, value, false);
	}

	public void setValue(int elementId, String value, boolean trim) throws BSPException {
		if (value == null)
			return;

		if (elementList.get(elementId - 1) instanceof Alpha) {
			((Alpha) elementList.get(elementId - 1)).setData(value, trim);
		} else if (elementList.get(elementId - 1) instanceof AlphaNumeric) {
			((AlphaNumeric) elementList.get(elementId - 1)).setData(value, trim);
		} else if (elementList.get(elementId - 1) instanceof Numeric) {
			((Numeric) elementList.get(elementId - 1)).setData(value, trim);
		} else {
			throw new BSPException("Invalid data type");
		}
	}

	public void setValue(int elementId, Long value) throws BSPException {
		if (value == null)
			return;

		if (elementList.get(elementId - 1) instanceof Numeric) {
			((Numeric) elementList.get(elementId - 1)).setData(value);
		} else {
			throw new BSPException("Invalid data type");
		}
	}

	public void setValue(int elementId, BigDecimal value) throws BSPException {
		if (value == null)
			return;

		if (elementList.get(elementId - 1) instanceof Numeric) {
			((Numeric) elementList.get(elementId - 1)).setData(value);
		} else {
			throw new BSPException("Invalid data type");
		}
	}

	public void setValue(int elementId, BigDecimal value, int decimalPlaces) throws BSPException {
		if (value == null)
			return;

		if (elementList.get(elementId - 1) instanceof Numeric) {
			((Numeric) elementList.get(elementId - 1)).setData(value, decimalPlaces);
		} else {
			throw new BSPException("Invalid data type");
		}
	}

	public void setTime(int elementId, Date date) throws BSPException {
		if (date == null)
			return;

		String value = null;
		SimpleDateFormat sdf = null;
		if (elementList.get(elementId - 1).getLength() == 4 || elementList.get(elementId - 1).getLength() == 5) {
			sdf = new SimpleDateFormat("HHmm");
			value = sdf.format(date);
		}

		if (elementList.get(elementId - 1) instanceof Numeric) {
			((Numeric) elementList.get(elementId - 1)).setData(value);
		} else if (elementList.get(elementId - 1) instanceof AlphaNumeric) {
			((AlphaNumeric) elementList.get(elementId - 1)).setData(value);
		} else {
			throw new BSPException("Invalid data type " + (elementList.get(elementId - 1).toString()));
		}
	}

	public void setValue(int elementId, Date date) throws BSPException {
		if (date == null)
			return;

		String value = null;
		SimpleDateFormat sdf = null;
		if (elementList.get(elementId - 1).getLength() == 4) {
			if (elementList.get(elementId - 1) instanceof Numeric) {
				sdf = new SimpleDateFormat("HHmm");
			} else {
				sdf = new SimpleDateFormat("MMyy");
			}
			value = sdf.format(date);
		} else if (elementList.get(elementId - 1).getLength() == 5) {
			sdf = new SimpleDateFormat("ddMMM");
			value = sdf.format(date);
		} else if (elementList.get(elementId - 1).getLength() == 6) {
			sdf = new SimpleDateFormat("yyMMdd");
			value = sdf.format(date);
		} else if (elementList.get(elementId - 1).getLength() == 8) {
			sdf = new SimpleDateFormat("yyyyMMdd");
			value = sdf.format(date);
		}

		if (elementList.get(elementId - 1) instanceof Numeric) {
			((Numeric) elementList.get(elementId - 1)).setData(value);
		} else if (elementList.get(elementId - 1) instanceof AlphaNumeric) {
			((AlphaNumeric) elementList.get(elementId - 1)).setData(value);
		} else {
			throw new BSPException("Invalid data type " + (elementList.get(elementId - 1).toString()));
		}
	}

	@Override
	public String getDISHFmtData() {
		StringBuilder sb = new StringBuilder();
		// int i = 1;
		for (BasicDataType basicDType : elementList) {
			sb.append(basicDType.getDISHFmtData());
			// String type = "";
			// if(basicDType instanceof Numeric){
			// type = "N";
			// } else if(basicDType instanceof Alpha){
			// type = "A";
			// } else if(basicDType instanceof AlphaNumeric){
			// type = "L";
			// }
			// if(sb.length()>0){
			// sb.append(",");
			// }
			// for(int j=0; j< basicDType.length ; j++){
			// sb.append(type);
			// }
			// i++;
		}
		return sb.toString();
	}

	@Override
	public boolean isValidDISHFormat() {
		boolean isValidDishFormat = true;
		int i = 0;
		for (BasicDataType basicDataType : elementList) {
			ElementMetaData metaData = metaDataList.get(i++);

			if (metaData.getStatus() == DE_STATUS.M && metaData.getElement() != DISH_ELEMENT.RESD) {
				if (basicDataType.getData() == null) {
					log.error("mandatory rules violated " + "BasicDataType - " + basicDataType + ", MetaData - " + metaData);
					isValidDishFormat = false;
					break;
				}
			}
		}

		return isValidDishFormat;
	}

	public List<BasicDataType> getElementList() {
		return elementList;
	}

	public List<ElementMetaData> getMetaDataList() {
		return metaDataList;
	}
}
