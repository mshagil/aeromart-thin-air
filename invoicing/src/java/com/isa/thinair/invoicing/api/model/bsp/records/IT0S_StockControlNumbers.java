package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT0S_StockControlNumbers extends BasicRecord implements DishTransactional {
	private final static int MAX_STOCK_CONTROL_NUMBER_SETS = 8;

	private int currentStockControlCount = 0;

	public IT0S_StockControlNumbers(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT0S);
	}

	public void setTransactionNo(Long id) throws BSPException {
		dataContainer.setValue(2, id);
	}

	public void addStockControlNumberSet(Long approvedLocationCode, String approvedLocationType, String stockCtrlNoFrom,
			Long stockCtrlNoTo) throws BSPException {
		if (MAX_STOCK_CONTROL_NUMBER_SETS <= currentStockControlCount) {
			throw new RuntimeException("Max number of stock allowed exceeded");
		}
		int offset = currentStockControlCount * 4;
		dataContainer.setValue(3 + offset, approvedLocationCode);
		dataContainer.setValue(4 + offset, approvedLocationType);
		dataContainer.setValue(5 + offset, stockCtrlNoFrom);
		dataContainer.setValue(6 + offset, stockCtrlNoTo);
		currentStockControlCount++;
	}

}
