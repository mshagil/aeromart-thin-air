package com.isa.thinair.invoicing.core.config;

import java.util.List;
import java.util.Map;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @isa.module.config-bean
 */
public class InvoicingConfig extends DefaultModuleConfig {

	public InvoicingConfig() {
		super();
	}

	private String transferToSqlServer;

	private String userName;

	private String password;

	private boolean jmsEar;

	private String dishntsi;

	private boolean fareLadderInRetFmt;

	private Map<String, String> externalProducts;

	private List<Integer> externalProductNominalCodes;

	private List<String> invoiceTransferAccountingSystemList;

	public boolean isJmsEar() {
		return jmsEar;
	}

	public void setJmsEar(boolean jmsEar) {
		this.jmsEar = jmsEar;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return Returns the transferToSqlServer.
	 */
	public String getTransferToSqlServer() {
		return transferToSqlServer;
	}

	/**
	 * @param transferToSqlServer
	 *            The transferToSqlServer to set.
	 */
	public void setTransferToSqlServer(String transferToSqlServer) {
		this.transferToSqlServer = transferToSqlServer;
	}

	/*
	 * DISH RET NTSI(Neutral Ticketing System Identifier.)
	 */
	public String getDishntsi() {
		return dishntsi;
	}

	public void setDishntsi(String dishntsi) {
		this.dishntsi = dishntsi;
	}

	/*
	 * format of DISH FRCA in DISH spec's format
	 */
	public boolean isFareLadderInRetFmt() {
		return fareLadderInRetFmt;
	}

	public void setFareLadderInRetFmt(boolean fareLadderInRetFmt) {
		this.fareLadderInRetFmt = fareLadderInRetFmt;
	}

	public List<Integer> getExternalProductNominalCodes() {
		return externalProductNominalCodes;
	}

	public void setExternalProductNominalCodes(List<Integer> externalProductNominalCodes) {
		this.externalProductNominalCodes = externalProductNominalCodes;
	}

	public Map<String, String> getExternalProducts() {
		return externalProducts;
	}

	public void setExternalProducts(Map<String, String> externalProducts) {
		this.externalProducts = externalProducts;
	}

	public List<String> getInvoiceTransferAccountingSystemList() {
		return invoiceTransferAccountingSystemList;
	}

	public void setInvoiceTransferAccountingSystemList(List<String> invoiceTransferAccountingSystemList) {
		this.invoiceTransferAccountingSystemList = invoiceTransferAccountingSystemList;
	}

}