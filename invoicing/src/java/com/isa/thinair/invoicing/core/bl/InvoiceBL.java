/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.model.Invoice;
import com.isa.thinair.invoicing.api.model.InvoiceSettlement;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceDAO;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceJdbcDAO;

/**
 * @author Chamindap,ISuru
 * 
 */
public class InvoiceBL {

	public static String AGENT_COLLECTIONDAO = "agentCollectionDAO";

	public static String INVOICEDAO = "invoiceDAO";

	public static String RESERVATION_TNX_DAO = "ReservationTnxDAO";

	public static String AGENT_TNX_DAO = "agentTransactionDAO";

	private InvoiceDAO invoiceDAO = null;

	private Log log = LogFactory.getLog(InvoiceBL.class);

	/**
	 * The constructor. -
	 */
	public InvoiceBL() {
		super();
	}

	/**
	 * settleCollection is a collection of String key, and a Double value Objects // loop thru the hash table // for
	 * each // check pool for sufficient funds (otherwise throw exception) // create InvoiceSettlement object // reduce
	 * the pool / summary // update the invoice settle status.
	 * 
	 * @param agentCode
	 * @param settleCollection
	 * @param validateSettleAmount
	 *            TODO
	 * @param userId
	 * @throws ModuleException
	 */

	public void settleInvoice(String agentCode, Collection<InvoiceSettlement> settleCollection, boolean validateSettleAmount)
			throws ModuleException {
		TravelAgentBD agentBD = InvoicingModuleUtils.lookupTravelAgentBD();
		BigDecimal amount;
		String invoiceNumber = null;
		boolean invoiceCheck = false;

		if (invoiceDAO == null) {
			invoiceDAO = getInvoiceDAO();
		}

		if (validateSettleAmount) {
			checkTotalSettlementAmountWithInvoicePool(agentCode, settleCollection);
		}

		Iterator<InvoiceSettlement> iter = settleCollection.iterator();

		while (iter.hasNext()) {
			InvoiceSettlement settlement = (InvoiceSettlement) iter.next();
			invoiceNumber = settlement.getInvoiceNo();
			amount = settlement.getAmountPaid();

			Invoice invoice = getInvoiceDAO().getInvoice(invoiceNumber);

			invoiceCheck = checkInvoiceAmount(invoice, amount); // checks the
			// invoice amount with settling total amount () i.e settled + amount

			if (invoiceCheck) {
				if (invoice.getInvoiceAmount().compareTo(BigDecimal.ZERO) == -1) {
					// negative invoice
					// settlement.setEntryType(InvoiceSettlement.ENTRY_TYPE_DR);
					amount = amount.negate();
				} else {
					// settlement.setEntryType(InvoiceSettlement.ENTRY_TYPE_CR);
				}
				invoiceDAO.addInvoiceSettlement(settlement); // adds invoice
				// settlement with CR
				invoiceDAO.updateInvoiceSettlement(invoiceNumber, amount);
				// update invoice table status(settled{if equal} or unsettled)
				// and settle amount in Agent summary
				InvoicingModuleUtils.lookupTravelAgentFinanceBD().updateAgentSummarySettle(agentCode, amount);
			}
		}
	}

	/**
	 * Gets The Invoice Summary Dto filled with invoice details
	 * 
	 * @param agentGSACode
	 *            the Agent code
	 * @param year
	 *            the year
	 * @param month
	 *            the month
	 * @param billingPeriod
	 *            the billing period
	 * @param payCurr
	 *            the payment currency
	 * @return InvoiceSummaryDTO the summary Dto
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public InvoiceSummaryDTO searchInvoiceForAgent(String agentGSACode, String year, String month, int billingPeriod,
			boolean payCurr, String invoiceNo, String pnr, String entity) throws ModuleException {
		InvoiceSummaryDTO invoiceSummaryDTO = null;
		Map<String, BigDecimal> externalProductInvoiceAmounts = new HashMap<String, BigDecimal>();
		Map<String, BigDecimal> externalProductInvoiceLocalAmounts = new HashMap<String, BigDecimal>();
		TravelAgentBD agentBD = InvoicingModuleUtils.lookupTravelAgentBD();

		try {
			if (pnr != null && !"".equals(pnr)) {
				invoiceSummaryDTO = getInvoiceDAO().getInvoiceForAgent(invoiceNo, pnr);// get the invoice from inv no
			} else {
				invoiceSummaryDTO = getInvoiceDAO().getInvoiceForAgent(agentGSACode, year, month, billingPeriod, entity);
			}
			Agent agent = agentBD.getAgent(agentGSACode);
			invoiceSummaryDTO.setAgentAddress(complieAgentAddress(agent));
			invoiceSummaryDTO.setAccountName(agent.getAgentDisplayName());
			invoiceSummaryDTO.setAgentEmail(agent.getBillingEmail());
			invoiceSummaryDTO.setReportToGSA(agent.getReportingToGSA());
			invoiceSummaryDTO.setCurrencyCode(agent.getCurrencyCode());

			if (invoiceSummaryDTO.getInvoice() != null && payCurr) {
				if (pnr != null && !"".equals(pnr)) {
					invoiceSummaryDTO = getInvoiceJDBCDAO().getInvoiceBreakDownForAgent(agentGSACode, pnr, invoiceSummaryDTO);
				} else {
					invoiceSummaryDTO = getInvoiceJDBCDAO().getInvoiceBreakDownForAgent(agentGSACode, year, month, billingPeriod,
							invoiceSummaryDTO);
				}
			}

			List<InvoiceDTO> invoiceDTOs = new ArrayList<InvoiceDTO>();
			if (pnr != null && !"".equals(pnr)) {
				invoiceDTOs = getInvoiceDAO().getExternalInvoicePerAgent(agentGSACode, pnr);
			} else {
				invoiceDTOs = getInvoiceDAO().getExternalInvoicePerAgent(agentGSACode, year, month, billingPeriod, entity);
			}

			for (InvoiceDTO invoiceDTO : invoiceDTOs) {
				externalProductInvoiceAmounts.put(invoiceDTO.getEntityName(), invoiceDTO.getInvoiceAmount());
				externalProductInvoiceLocalAmounts.put(invoiceDTO.getEntityName(), invoiceDTO.getInvoiceAmountLocal());
			}

			if (externalProductInvoiceAmounts != null && externalProductInvoiceAmounts.size() > 0) {
				invoiceSummaryDTO.setInvoiceExternalProductAmounts(externalProductInvoiceAmounts);
			}

			if (externalProductInvoiceLocalAmounts != null && externalProductInvoiceLocalAmounts.size() > 0) {
				invoiceSummaryDTO.setInvoiceExternalProductAmountsLocal(externalProductInvoiceLocalAmounts);
			}

		} catch (CommonsDataAccessException cdaex) {
			log.error("searchInvoiceForAgent failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			log.error("searchInvoiceForAgent failed. ", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}
		return invoiceSummaryDTO;
	}

	private String complieAgentAddress(Agent agent) {
		String address = null;
		if (agent != null) {
			address = agent.getAddressLine1() != null ? agent.getAddressLine1() : " " + ",";
			address += agent.getAddressLine2() != null ? agent.getAddressLine2() : " " + ",";
			address += agent.getAddressCity() != null ? agent.getAddressCity() : " " + ",";
			address += agent.getAddressStateProvince() != null ? agent.getAddressStateProvince() : " ";
		} else {
			address = " ";
		}
		return address;
	}

	/**
	 * Gets The Invoice Summary Dto filled with invoice details
	 * 
	 * @param agentGSACode
	 *            the Agent code
	 * @param dateFrom
	 * @param dateTo
	 * @return GeneratedInvoiceDTO
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public Collection<GeneratedInvoiceDTO> searchGeneratedInvoicesForAgent(String agentGSACode, String dateFrom, String dateTo)
			throws ModuleException {
		Collection<GeneratedInvoiceDTO> colGeneratedInvoiceDTO = new ArrayList();
		Invoice resInvoice = null;
		Invoice ExtInvoice = null;
		TravelAgentBD agentBD = InvoicingModuleUtils.lookupTravelAgentBD();

		try {
			colGeneratedInvoiceDTO = getInvoiceDAO().getGenaratedInvoicesForAgent(agentGSACode, dateFrom, dateTo);
		} catch (CommonsDataAccessException cdaex) {
			log.error("searchGeneratedInvoicesForAgent failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			log.error("searchGeneratedInvoicesForAgent failed. ", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}
		return colGeneratedInvoiceDTO;
	}

	/**
	 * Private to get InvoiceDAO.
	 * 
	 * @return InvoiceDAO.
	 */
	private InvoiceDAO getInvoiceDAO() {
		return (InvoiceDAO) InvoicingModuleUtils.getDAO(INVOICEDAO);
	}

	/**
	 * Private to get InvoiceDAO.
	 * 
	 * @return InvoiceDAO.
	 */
	private InvoiceJdbcDAO getInvoiceJDBCDAO() {
		return (InvoiceJdbcDAO) InvoicingModuleUtils.getInvoiceJdbcDAO();
	}

	/**
	 * 
	 * @param amount
	 * @return
	 * @throws ModuleException
	 */
	private boolean checkInvoiceAmount(Invoice invoice, BigDecimal amount) throws ModuleException {
		boolean check = false;
		BigDecimal invoiceAmount;
		BigDecimal settledAmount;

		// to handle both plus and minus invoices // by kasun
		invoiceAmount = invoice.getInvoiceAmount().abs();
		settledAmount = invoice.getSettledAmount().abs();

		settledAmount = AccelAeroCalculator.add(settledAmount, amount.abs());

		if (invoiceAmount.compareTo(settledAmount) == -1) {
			throw new ModuleException("airtravelagent.logic.invoiceamount.exceeds");
		} else {
			check = true;
		}
		return check;
	}

	private void checkTotalSettlementAmountWithInvoicePool(String agentCode, Collection<InvoiceSettlement> settleCollection)
			throws ModuleException {
		if (settleCollection != null && !settleCollection.isEmpty()) {
			Iterator<InvoiceSettlement> iter = settleCollection.iterator();
			BigDecimal totalSettlementAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			while (iter.hasNext()) {
				InvoiceSettlement settlement = (InvoiceSettlement) iter.next();
				totalSettlementAmount = AccelAeroCalculator.add(totalSettlementAmount, settlement.getAmountPaid());
			}

			if (!StringUtil.isNullOrEmpty(agentCode) && totalSettlementAmount.doubleValue() > 0) {
				List<AgentSummary> lstAgentSummary = InvoicingModuleUtils.lookupTravelAgentBD().getAgentSummary(agentCode);
				if (lstAgentSummary != null && !lstAgentSummary.isEmpty()) {
					AgentSummary agentSummary = lstAgentSummary.get(0);
					if (AccelAeroCalculator.isLessThan(agentSummary.getAvailableFundsForInv(), totalSettlementAmount)) {
						throw new ModuleException("agent.invoice.settlement.amount.exceeds");
					}
				}
			}
		}

	}

}