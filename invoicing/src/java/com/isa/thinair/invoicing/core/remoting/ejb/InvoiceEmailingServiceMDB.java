/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.invoicing.core.remoting.ejb;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.annotation.security.RunAs;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.core.util.CredentialInvokerUtil;

/**
 * @author Byorn
 */
@MessageDriven(name = "InvoiceEmailingServiceMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/invoiceQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = "3600") })
@RunAs("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
public class InvoiceEmailingServiceMDB implements MessageListener {
	private static final long serialVersionUID = -2343245398965963779L;
	private final Log log = LogFactory.getLog(getClass());

	@Resource
	private MessageDrivenContext mdc;

	public void onMessage(Message message) {
		try {
			if (log.isDebugEnabled()) {
				log.debug("InvoiceEmailingServiceMDB onMessage Called");
			}
			CredentialInvokerUtil.invokeCredentials();
			
			ObjectMessage objMessage = (ObjectMessage) message;
			if (objMessage.getObject() instanceof GeneratedInvoiceDTO) {
				GeneratedInvoiceDTO generatedInvoiceDTO = (GeneratedInvoiceDTO) objMessage.getObject();

				InvoicingModuleUtils.lookupInvoicingBD().sendInvoiceMail(generatedInvoiceDTO);
			} else if (objMessage.getObject() instanceof ArrayList) {
				ArrayList list = (ArrayList) objMessage.getObject();
				if (log.isDebugEnabled()) {
					log.debug("List for Transferring" + list.size());
				}
				InvoicingModuleUtils.lookupInvoicingBD().transferInvoices(list);
			}

		} catch (JMSException jmsException) {
			log.error("Exception in MessagingServiceMDB:onMessage() " + jmsException.getMessage(), jmsException);
			this.mdc.setRollbackOnly();
		} catch (Exception exception) {
			log.error("Exception in MessagingServiceMDB:onMessage() " + exception.getMessage(), exception);
			this.mdc.setRollbackOnly();
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
