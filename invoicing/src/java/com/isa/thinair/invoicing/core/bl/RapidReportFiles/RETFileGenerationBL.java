package com.isa.thinair.invoicing.core.bl.RapidReportFiles;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.CurrencyProxy;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.invoicing.api.model.bsp.BSPAgentDataDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BSPSegmentInfoDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TransactionType;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionTaxData;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.ETLRCoupon;
import com.isa.thinair.invoicing.api.model.bsp.ETLRCouponRec;
import com.isa.thinair.invoicing.api.model.bsp.ETLRFileBuilder;
import com.isa.thinair.invoicing.api.model.bsp.ETLRRec;
import com.isa.thinair.invoicing.api.model.bsp.ETicketCoupon;
import com.isa.thinair.invoicing.api.model.bsp.ETicketCouponStatus;
import com.isa.thinair.invoicing.api.model.bsp.ETicketFOP;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.ApprovedLocationType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.ConjuctionTktIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.CouponUseIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.DataInputStatusIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.FormOfPaymentType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.NetReportingIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.StatisticalCodeType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.TicketingMode;
import com.isa.thinair.invoicing.api.model.bsp.FlightCoupon;
import com.isa.thinair.invoicing.api.model.bsp.FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.HotFileBuilder;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.ReportFileTypes;
import com.isa.thinair.invoicing.api.model.bsp.RetFileBuilder;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.BKTT;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.BasicFileType;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.ETLR;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.RFND;
import com.isa.thinair.invoicing.api.model.bsp.records.BAR64_DocumentAmounts;
import com.isa.thinair.invoicing.api.model.bsp.records.BAR65_AdditionalPassengerInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BAR66_AdditionalPaymentRecordInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BKF81_FareCalculationRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BKI63_ItineraryDataSegment;
import com.isa.thinair.invoicing.api.model.bsp.records.BKP83_ElectronicTransaction;
import com.isa.thinair.invoicing.api.model.bsp.records.BKP84_FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS24_TicketIdentification;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS30_DocumentAmount;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS39_CommissionRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS42_TaxCommissionRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS45_TicketInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS46_SalesTransactions;
import com.isa.thinair.invoicing.api.model.bsp.records.BKT06_TransactionHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.BMD75_EDocumentCouponDetails;
import com.isa.thinair.invoicing.api.model.bsp.records.BMD76_EDocumentCouponRemarks;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP70_MiscellaneousDocumentInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP71_MiscellaneousAdditionalInformationRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP72_MiscellaneousDocumnetAmountInLetters;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP73_OptionalAirlineInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP74_AdditionalPrintLinesRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP77_CouponAdditionPrintLines;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP78_TicketAdviceSponsorInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT02_BasicTransaction;
import com.isa.thinair.invoicing.api.model.bsp.records.IT02_BasicTransaction_CNJ;
import com.isa.thinair.invoicing.api.model.bsp.records.IT03_RelatedTicketInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT04_AdditionalSaleInfo;
import com.isa.thinair.invoicing.api.model.bsp.records.IT05_MonetaryAmounts;
import com.isa.thinair.invoicing.api.model.bsp.records.IT06_Itinerary;
import com.isa.thinair.invoicing.api.model.bsp.records.IT06_Itinerary_CNJ;
import com.isa.thinair.invoicing.api.model.bsp.records.IT07_FareCalculation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT08_FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.records.IT09_AdditionalInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_25_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_30_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_50_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_90_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_Mutation_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X34_Mutation_Fare_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X36_Ticket_Tax;
import com.isa.thinair.invoicing.api.model.bsp.records.X45_FOP;
import com.isa.thinair.invoicing.api.model.bsp.records.X60_Coupon;
import com.isa.thinair.invoicing.api.model.bsp.records.X70_Exchange_Records;
import com.isa.thinair.invoicing.api.model.bsp.records.X92_Coupon_Status_History_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X94_Revalidation_Record;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.core.util.BSPCoreUtils;
import com.isa.thinair.platform.api.constants.PlatformConstants;

public class RETFileGenerationBL {

	public void generateReportFile(Date rptPeriodStartDate, Date rptPeriodEndDate, UserPrincipal userPrincipal,
			boolean processErrorsOnly, ReportFileTypes.FileTypes fileType) throws ModuleException {
		try {
			if (fileType.equals(ReportFileTypes.FileTypes.RET)) {
				SimpleDateFormat formatter5 = new SimpleDateFormat("yyyyMMdd");
				String date = formatter5.format(rptPeriodStartDate);
				String fileName = "FINDATA_" + date + ".FIN";
				String content = generateRPTContent(rptPeriodStartDate, rptPeriodEndDate, userPrincipal);
				writeTextFile(fileName, content);

			} else if (fileType.equals(ReportFileTypes.FileTypes.HOT)) {
				SimpleDateFormat formatter5 = new SimpleDateFormat("yyyyMMdd");
				String date = formatter5.format(rptPeriodStartDate);
				String fileName = "BSPDATA_" + date + ".BSP";
				String content = generateHOTContent(rptPeriodStartDate, rptPeriodEndDate, userPrincipal);
				writeTextFile(fileName, content);
			} else if (fileType.equals(ReportFileTypes.FileTypes.ETLR)) {
				SimpleDateFormat formatter5 = new SimpleDateFormat("yyyyMMdd");
				String date = formatter5.format(rptPeriodStartDate);
				String fileName = "TRANS_RBP_" + date + ".ETLR";
				String content = generateETLRContent(rptPeriodStartDate, rptPeriodEndDate, userPrincipal);
				writeTextFile(fileName, content);
			}
		} catch (Exception e) {
			throw new ModuleException("Ret file generation business logic exception");
		}
	}

	private String generateHOTContent(Date rptPeriodStartDate, Date rptPeriodEndDate, UserPrincipal userPrincipal)
			throws BSPException {
		StringBuffer buf = new StringBuffer();
		HotFileBuilder hfb = new HotFileBuilder("SY", new Date(), rptPeriodEndDate, 203L);
		Map<HotFileBuilder, List<BSPTransactionDataDTO>> loggingDataMap = new HashMap<HotFileBuilder, List<BSPTransactionDataDTO>>();
		try {
			String agentLlist = AppSysParamsUtil.getHotFileAgentTypes();
			List<String> agentTypeList = Arrays.asList(agentLlist.split("\\s*,\\s*"));

			String agentTypes = Util.buildStringInClauseContent(agentTypeList);
			Collection<BSPAgentDataDTO> bspEnabledAgents = InvoicingModuleUtils.getBSPLinkJdbcDao()
					.getCTOandATOAgents(agentTypes);
			long sequenceno = 00000004l;
			for (BSPAgentDataDTO agent : bspEnabledAgents) {
				List<BSPTransactionDataDTO> bspTnxList = BSPCoreUtils.composeHotTransactionDataDTOs("", agent.getAgentCode(),
						rptPeriodStartDate, rptPeriodEndDate, userPrincipal);

				if (loggingDataMap.get(hfb) == null) {
					loggingDataMap.put(hfb, new ArrayList<BSPTransactionDataDTO>());
				}
				for (BSPTransactionDataDTO bspTnx : bspTnxList) {
					if (bspTnx.getTnxType() == TransactionType.TKTT) {
						BKTT bktt = hfb.getNewBKTTInstance();

						Long ticketDocumentNo = bspTnx.getFirstSetOfSegments().getLeft();
						List<BSPSegmentInfoDTO> firstSegment = bspTnx.getFirstSetOfSegments().getRight();

						BKT06_TransactionHeader bkt06 = (BKT06_TransactionHeader) bktt
								.createNewRecord(BKT06_TransactionHeader.class);
						bkt06.setSequesnceNo(sequenceno);
						bkt06.setAirlineCodeNumber(bspTnx.getTicketingAirlineCode());
						String txnNo = bspTnx.getAaTnxID().toString();
						if (txnNo.length() > 6) {
							bkt06.setTransactionNo(Long.parseLong(txnNo.substring(txnNo.length() - 6)));
						} else {
							bkt06.setTransactionNo(Long.parseLong(txnNo));
						}
						bkt06.setTransactionRecordCounter(Long.getLong(((Integer) bspTnxList.size()).toString()));
						bkt06.setReportingSysyIdenfifier("AGTD");
						bkt06.setApproveLocationNumericCode(00000000l);

						// bkt06.setVendorISOCountryCode("SY");
						// bkt06.ticketDocumentNo
						sequenceno++;
						BKS24_TicketIdentification bks24 = (BKS24_TicketIdentification) bktt
								.createNewRecord(BKS24_TicketIdentification.class);
						createBKRecord(sequenceno, bspTnx, bks24);
						bks24.setTransactionCode(DISH_FILE_TYPE.TKTT);
						if (bspTnx.getIataAgentCode() != null) {
							bks24.setAgentNumericCode(bspTnx.getIataAgentCode());
						}

						sequenceno++;
						BKS30_DocumentAmount bks30 = (BKS30_DocumentAmount) bktt.createNewRecord(BKS30_DocumentAmount.class);
						createBKRecord(sequenceno, bspTnx, bks30);
						bks30.setCurrencyType(bspTnx.getPayCurrencyCode());

						sequenceno++;
						BKS39_CommissionRecord bks39 = (BKS39_CommissionRecord) bktt
								.createNewRecord(BKS39_CommissionRecord.class);
						createBKRecord(sequenceno, bspTnx, bks39);
						bks39.setCurrencyType(bspTnx.getPayCurrencyCode());
						if (bspTnx.getAgentCommission() != null) {
							bks39.setCommitionAmount(Long.getLong(bspTnx.getAgentCommission().toString()));
						}
						sequenceno++;

						BKS45_TicketInformation bsk45 = (BKS45_TicketInformation) bktt
								.createNewRecord(BKS45_TicketInformation.class);
						createBKRecord(sequenceno, bspTnx, bsk45);
						// bsk45.setRemitanceEndingDate(new Date());

						sequenceno++;

						BKS46_SalesTransactions bks46 = (BKS46_SalesTransactions) bktt
								.createNewRecord(BKS46_SalesTransactions.class);
						createBKRecord(sequenceno, bspTnx, bks46);
						bks46.setEndorsementsRestrictions("NON ENDO BR");

						sequenceno++;
						BKI63_ItineraryDataSegment bki63 = (BKI63_ItineraryDataSegment) bktt
								.createNewRecord(BKI63_ItineraryDataSegment.class);
						createBKRecord(sequenceno, bspTnx, bki63);
						for (BSPSegmentInfoDTO bspSeg : firstSegment) {
							bki63.setDestinationAirport(bspSeg.getDestination());
							bki63.setOriginAirport(bspSeg.getOrigin());
							bki63.setDepartureTime(bspSeg.getDepartureDate());
							bki63.setDocumentNumber(bspSeg.getPnr());

						}

						sequenceno++;
						BAR64_DocumentAmounts bar64 = (BAR64_DocumentAmounts) bktt.createNewRecord(BAR64_DocumentAmounts.class);
						createBKRecord(sequenceno, bspTnx, bar64);
						bar64.setFare(bspTnx.getTotalFare().toString());

						sequenceno++;
						BAR65_AdditionalPassengerInformation bar65 = (BAR65_AdditionalPassengerInformation) bktt
								.createNewRecord(BAR65_AdditionalPassengerInformation.class);
						createBKRecord(sequenceno, bspTnx, bar65);
						bar65.setPassengerName(bspTnx.getPaxName().replace(" ", "/"));

						sequenceno++;
						BAR66_AdditionalPaymentRecordInformation bar66 = (BAR66_AdditionalPaymentRecordInformation) bktt
								.createNewRecord(BAR66_AdditionalPaymentRecordInformation.class);
						createBKRecord(sequenceno, bspTnx, bar66);

						sequenceno++;
						BMP73_OptionalAirlineInformation bmp73 = (BMP73_OptionalAirlineInformation) bktt
								.createNewRecord(BMP73_OptionalAirlineInformation.class);
						createBKRecord(sequenceno, bspTnx, bmp73);

						sequenceno++;

						sequenceno++;
						BKF81_FareCalculationRecord bkf81 = (BKF81_FareCalculationRecord) bktt
								.createNewRecord(BKF81_FareCalculationRecord.class);
						createBKRecord(sequenceno, bspTnx, bkf81);

						sequenceno++;
						BKP83_ElectronicTransaction bkp83 = (BKP83_ElectronicTransaction) bktt
								.createNewRecord(BKP83_ElectronicTransaction.class);
						createBKRecord(sequenceno, bspTnx, bkp83);

						sequenceno++;
						BKP84_FormOfPayment bkp84 = (BKP84_FormOfPayment) bktt.createNewRecord(BKP84_FormOfPayment.class);
						createBKRecord(sequenceno, bspTnx, bkp84);

						loggingDataMap.get(hfb).add(bspTnx);
					} else {
						BKTT bktt = hfb.getNewBKTTInstance();
						BKT06_TransactionHeader bkt06 = (BKT06_TransactionHeader) bktt
								.createNewRecord(BKT06_TransactionHeader.class);
						bkt06.setSequesnceNo(sequenceno);
						bkt06.setAirlineCodeNumber("SY");
						bkt06.setTransactionNo(Long.parseLong(bspTnx.getAaTnxID().toString()));
						bkt06.setTransactionRecordCounter(Long.getLong(((Integer) bspTnxList.size()).toString()));
						bkt06.setReportingSysyIdenfifier("AGTD");
						bkt06.setApproveLocationNumericCode(00000000l);

						// bkt06.setVendorISOCountryCode("SY");
						// bkt06.
						sequenceno++;
						BKS24_TicketIdentification bks24 = (BKS24_TicketIdentification) bktt
								.createNewRecord(BKS24_TicketIdentification.class);
						createBKRecord(sequenceno, bspTnx, bks24);
						bks24.setTransactionCode(DISH_FILE_TYPE.TKTT);
						if (bspTnx.getIataAgentCode() != null) {
							bks24.setAgentNumericCode(bspTnx.getIataAgentCode());
						}

						sequenceno++;
						BKS30_DocumentAmount bks30 = (BKS30_DocumentAmount) bktt.createNewRecord(BKS30_DocumentAmount.class);
						createBKRecord(sequenceno, bspTnx, bks30);
						bks30.setCurrencyType(bspTnx.getPayCurrencyCode());

						sequenceno++;
						BKS39_CommissionRecord bks39 = (BKS39_CommissionRecord) bktt
								.createNewRecord(BKS39_CommissionRecord.class);
						createBKRecord(sequenceno, bspTnx, bks39);
						sequenceno++;
						BKS42_TaxCommissionRecord bks42 = (BKS42_TaxCommissionRecord) bktt
								.createNewRecord(BKS42_TaxCommissionRecord.class);
						createBKRecord(sequenceno, bspTnx, bks42);
						sequenceno++;
						BKS45_TicketInformation bsk45 = (BKS45_TicketInformation) bktt
								.createNewRecord(BKS45_TicketInformation.class);
						createBKRecord(sequenceno, bspTnx, bsk45);
						// bsk45.setRemitanceEndingDate(new Date());

						sequenceno++;

						BKS46_SalesTransactions bks46 = (BKS46_SalesTransactions) bktt
								.createNewRecord(BKS46_SalesTransactions.class);
						createBKRecord(sequenceno, bspTnx, bks46);

						sequenceno++;
						BKI63_ItineraryDataSegment bki63 = (BKI63_ItineraryDataSegment) bktt
								.createNewRecord(BKI63_ItineraryDataSegment.class);
						createBKRecord(sequenceno, bspTnx, bki63);

						sequenceno++;
						BAR64_DocumentAmounts bar64 = (BAR64_DocumentAmounts) bktt.createNewRecord(BAR64_DocumentAmounts.class);
						createBKRecord(sequenceno, bspTnx, bar64);

						sequenceno++;
						BAR65_AdditionalPassengerInformation bar65 = (BAR65_AdditionalPassengerInformation) bktt
								.createNewRecord(BAR65_AdditionalPassengerInformation.class);
						createBKRecord(sequenceno, bspTnx, bar65);

						sequenceno++;
						BAR66_AdditionalPaymentRecordInformation bar66 = (BAR66_AdditionalPaymentRecordInformation) bktt
								.createNewRecord(BAR66_AdditionalPaymentRecordInformation.class);
						createBKRecord(sequenceno, bspTnx, bar66);

						sequenceno++;
						BMP70_MiscellaneousDocumentInformation bmp70 = (BMP70_MiscellaneousDocumentInformation) bktt
								.createNewRecord(BMP70_MiscellaneousDocumentInformation.class);
						createBKRecord(sequenceno, bspTnx, bmp70);

						sequenceno++;
						BMP71_MiscellaneousAdditionalInformationRecord bmp71 = (BMP71_MiscellaneousAdditionalInformationRecord) bktt
								.createNewRecord(BMP71_MiscellaneousAdditionalInformationRecord.class);
						//Please check this and uncomment this
						//createBKRecord(sequenceno, bspTnx, bmp71);

						sequenceno++;
						BMP72_MiscellaneousDocumnetAmountInLetters bmp72 = (BMP72_MiscellaneousDocumnetAmountInLetters) bktt
								.createNewRecord(BMP72_MiscellaneousDocumnetAmountInLetters.class);
						createBKRecord(sequenceno, bspTnx, bmp72);

						sequenceno++;
						BMP73_OptionalAirlineInformation bmp73 = (BMP73_OptionalAirlineInformation) bktt
								.createNewRecord(BMP73_OptionalAirlineInformation.class);
						createBKRecord(sequenceno, bspTnx, bmp73);

						sequenceno++;
						BMP74_AdditionalPrintLinesRecord bmp74 = (BMP74_AdditionalPrintLinesRecord) bktt
								.createNewRecord(BMP74_AdditionalPrintLinesRecord.class);
						createBKRecord(sequenceno, bspTnx, bmp74);

						sequenceno++;
						BMD75_EDocumentCouponDetails bmd75 = (BMD75_EDocumentCouponDetails) bktt
								.createNewRecord(BMD75_EDocumentCouponDetails.class);
						createBKRecord(sequenceno, bspTnx, bmd75);

						sequenceno++;
						BMD76_EDocumentCouponRemarks bmd76 = (BMD76_EDocumentCouponRemarks) bktt
								.createNewRecord(BMD76_EDocumentCouponRemarks.class);
						createBKRecord(sequenceno, bspTnx, bmd76);

						sequenceno++;
						BMP77_CouponAdditionPrintLines bmp77 = (BMP77_CouponAdditionPrintLines) bktt
								.createNewRecord(BMP77_CouponAdditionPrintLines.class);
						createBKRecord(sequenceno, bspTnx, bmp77);

						sequenceno++;
						BMP78_TicketAdviceSponsorInformation bmp78 = (BMP78_TicketAdviceSponsorInformation) bktt
								.createNewRecord(BMP78_TicketAdviceSponsorInformation.class);
						createBKRecord(sequenceno, bspTnx, bmp78);

						sequenceno++;
						BKF81_FareCalculationRecord bkf81 = (BKF81_FareCalculationRecord) bktt
								.createNewRecord(BKF81_FareCalculationRecord.class);
						createBKRecord(sequenceno, bspTnx, bkf81);

						sequenceno++;
						BKP83_ElectronicTransaction bkp83 = (BKP83_ElectronicTransaction) bktt
								.createNewRecord(BKP83_ElectronicTransaction.class);
						createBKRecord(sequenceno, bspTnx, bkp83);

						sequenceno++;
						BKP84_FormOfPayment bkp84 = (BKP84_FormOfPayment) bktt.createNewRecord(BKP84_FormOfPayment.class);
						createBKRecord(sequenceno, bspTnx, bkp84);

						loggingDataMap.get(hfb).add(bspTnx);

					}
					sequenceno++;
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		buf.append(hfb.getDISHFmtData());

		return buf.toString();
	}

	private void createBKRecord(long sequenceno, BSPTransactionDataDTO bspTnx, BasicBKRecord bks24) throws BSPException {
		bks24.setSequenceNo(sequenceno);
		bks24.setIsssueDate(bspTnx.getDateOfIssue());
		String txnNo = bspTnx.getAaTnxID().toString();
		if (txnNo.length() > 6) {
			bks24.setTransactionNo(Long.parseLong(txnNo.substring(txnNo.length() - 6)));
		} else {
			bks24.setTransactionNo(Long.parseLong(txnNo));
		}
		// bks24.setTransactionNo(Long.parseLong(bspTnx.getAaTnxID().toString()));
		bks24.setDocumentNumber(bspTnx.getPnr());
		bks24.setCheckDigit(6l);
	}

	private String generateRPTContent(Date rptPeriodStartDate, Date rptPeriodEndDate, UserPrincipal userPrincipal)
			throws BSPException {

		StringBuffer buf = new StringBuffer();
		RetFileBuilder rfb = new RetFileBuilder("", new Date(), rptPeriodEndDate, 203L);
		CurrencyProxy currencyProxy = new CurrencyProxy(InvoicingModuleUtils.getAirmasterBD());
		String baseCurrency = AppSysParamsUtil.getBaseCurrency();

		Map<RetFileBuilder, List<BSPTransactionDataDTO>> loggingDataMap = new HashMap<RetFileBuilder, List<BSPTransactionDataDTO>>();
		try {
			int baseCurencyDecimalPlaces = currencyProxy.getCurrency(baseCurrency).getDecimalPlaces();
			String agentLlist = AppSysParamsUtil.getRetFileAgentTypes();

			List<String> agentTypeList = Arrays.asList(agentLlist.split("\\s*,\\s*"));

			String agentTypes = Util.buildStringInClauseContent(agentTypeList);

			Collection<BSPAgentDataDTO> bspEnabledAgents = InvoicingModuleUtils.getBSPLinkJdbcDao()
					.getCTOandATOAgents(agentTypes);
			ExchangeRateProxy exRateProxy = new ExchangeRateProxy(rptPeriodEndDate);
			for (BSPAgentDataDTO agent : bspEnabledAgents) {
				/*
				 * ----------------------------------------------
				 */
				List<BSPTransactionDataDTO> bspTnxList = BSPCoreUtils.composeRetTransactionDataDTOs("", agent.getAgentCode(),
						rptPeriodStartDate, rptPeriodEndDate, userPrincipal, false, true, exRateProxy);
				if (loggingDataMap.get(rfb) == null) {
					loggingDataMap.put(rfb, new ArrayList<BSPTransactionDataDTO>());
				}
				for (BSPTransactionDataDTO bspTnx : bspTnxList) {
					Currency currency = currencyProxy.getCurrency(bspTnx.getPayCurrencyCode());
					int payCurrDecimalPlaces = currency.getDecimalPlaces();

					if (bspTnx.getTnxType() == TransactionType.TKTT || bspTnx.getTnxType() == TransactionType.EMDA) {
						BasicFileType tktt = null;
						if (bspTnx.getTnxType() == TransactionType.TKTT) {
							tktt = rfb.getNewTKTTInstance();
						} else if (bspTnx.getTnxType() == TransactionType.EMDA) {
							tktt = rfb.getNewEMDAInstance();
						}

						IT02_BasicTransaction it02 = (IT02_BasicTransaction) tktt.createNewRecord(IT02_BasicTransaction.class);

						// it02.setAgentCode(new Long(bspTnx.getIataCode()));
						it02.setDateOfIssue(bspTnx.getDateOfIssue());
						it02.setPassengerName(bspTnx.getLongPaxName());
						it02.setTicketingAirlineCodeNo(bspTnx.getTicketingAirlineCode());
						// hot fix for RB eticket number missing starting 0
						// TODO: convert eticket number long to String
						it02.setTicketDocumentNo(correctedEticketNumber(bspTnx.getFirstSetOfSegments().getLeft() + "",
								bspTnx.getTicketingAirlineCode()));
						it02.setApprovedLocationType(ApprovedLocationType.RETAIL_AGENCY);
						it02.setIsoCountryCode(agent.getCountaryCode());
						it02.setStatisticalCode(StatisticalCodeType.International);
						it02.setAgentCode(Long.parseLong(agent.getIataCode()));

						IT04_AdditionalSaleInfo it04 = (IT04_AdditionalSaleInfo) tktt
								.createNewRecord(IT04_AdditionalSaleInfo.class);

						it04.setFare(adjustDecimalPlaces(bspTnx.getTotalFare(), baseCurencyDecimalPlaces, baseCurrency, 11));
						// it04.setFare(baseCurrency
						// + adjustDecimalPlaces(
						// AccelAeroCalculator.multiply(bspTnx.getTotalFare(), bspTnx.getCurrencyExchangeRate()),
						// payCurrDecimalPlaces));
						it04.setTicketingModeIndicator(TicketingMode.DIRECT);
						it04.setPnrReference(bspTnx.getPnr());
						it04.setTotal(baseCurrency,
								adjustDecimalPlaces(bspTnx.getBaseCurrencyAmount(), baseCurencyDecimalPlaces, baseCurrency, 11));
						// it04.setTotal(baseCurrency, adjustDecimalPlaces(bspTnx.getPayCurrencyAmount(),
						// payCurrDecimalPlaces));
						String IT07XTData = "";
						for (Entry<Integer, String> entry : getIT04TaxData(bspTnx.getTnxOndTaxInfoCollection()).entrySet()) {
							if (entry.getKey() <= 3) {
								it04.addTax(entry.getValue());
							} else {
								IT07XTData = entry.getValue();
							}
						}

						String originalIssueInformation = correctedEticketNumber(bspTnx.getFirstSetOfSegments().getLeft() + "",
								bspTnx.getTicketingAirlineCode())
								+ agent.getStation_code()
								+ bspTnx.getDateOfIssue().toString().replace("-", "") + agent.getIataCode();
						it04.setOriginalIssueInformation(originalIssueInformation);
						if (InvoicingModuleUtils.getInvoicingConfig().getDishntsi() != null) {
							it04.setNeutralTktSystemId(InvoicingModuleUtils.getInvoicingConfig().getDishntsi());
						}
						it04.setPassengerSpecificData(bspTnx.getPaxSpecificData());

						IT05_MonetaryAmounts it05 = (IT05_MonetaryAmounts) tktt.createNewRecord(IT05_MonetaryAmounts.class);
						// it05.setAmountEnteredByAgent(bspTnx.getPayCurrencyAmount());
						// it05.setAmountPaidByCustomer(bspTnx.getPayCurrencyAmount());
						it05.setAmountEnteredByAgent(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
						it05.setAmountPaidByCustomer(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
						// it05.setCurrencyType(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
						it05.setCurrencyType(baseCurrency, baseCurencyDecimalPlaces);
						it05.setNetReportingIndicator(NetReportingIndicator.NA);
						it05.setTicketAmount(bspTnx.getPayCurrencyAmount().divide(bspTnx.getCurrencyExchangeRate(), 3),
								baseCurencyDecimalPlaces);
						for (BSPTransactionTaxData entry : getIT05TaxData(bspTnx.getTnxOndTaxInfoCollection())) {
							CurrencyExchangeRate currencyExchangeRate = exRateProxy.getCurrencyExchangeRate(entry
									.getDepatureAirportLocalCurrency());
							BigDecimal taxAmount = new BigDecimal(entry.getAmount()).multiply(currencyExchangeRate
									.getMultiplyingExchangeRate());
							it05.addTaxFee(entry.getTaxCode(), taxAmount, 2);
						}
						// if (bspTnx.getTaxCollection().get(TaxCodes.TX1.toString()) != null)
						// it05.addTaxFee("TX1",
						// AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.multiply(
						// bspTnx.getTaxCollection().get(TaxCodes.TX1.toString()), bspTnx.getCurrencyExchangeRate())),
						// payCurrDecimalPlaces);
						// if (bspTnx.getTaxCollection().get(TaxCodes.TX2.toString()) != null)
						// it05.addTaxFee("TX2",
						// AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.multiply(
						// bspTnx.getTaxCollection().get(TaxCodes.TX2.toString()), bspTnx.getCurrencyExchangeRate())),
						// payCurrDecimalPlaces);
						// if (bspTnx.getTaxCollection().get(TaxCodes.TX3.toString()) != null
						// && !bspTnx.getTaxCollection().get(TaxCodes.TX3.toString())
						// .equals(AccelAeroCalculator.getDefaultBigDecimalZero()))
						// it05.addTaxFee("TX3",
						// AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.multiply(
						// bspTnx.getTaxCollection().get(TaxCodes.TX3.toString()), bspTnx.getCurrencyExchangeRate())),
						// payCurrDecimalPlaces);
						// if (bspTnx.getAgentCommission() != null) {
						// it05.addCommission(CommissionType.STANDARD, new BigDecimal(0L), new BigDecimal(
						// AccelAeroCalculator.formatAsDecimal(bspTnx.getAgentCommission())));
						// }

						IT06_Itinerary it06 = (IT06_Itinerary) tktt.createNewRecord(IT06_Itinerary.class);
						int couponsUsed = 1;
						CouponUseIndicator[] coupons = { CouponUseIndicator.V, CouponUseIndicator.V, CouponUseIndicator.V,
								CouponUseIndicator.V };

						for (BSPSegmentInfoDTO resSeg : bspTnx.getFirstSetOfSegments().getRight()) {

							FlightCoupon flightCoupon = new FlightCoupon();
							coupons[couponsUsed - 1] = CouponUseIndicator.F;
							flightCoupon.setOriginAirportCode(resSeg.getOrigin());
							flightCoupon.setDestinationAirportCode(resSeg.getDestination());
							flightCoupon.setCarrier(resSeg.getCarrierCode());
							flightCoupon.setFareBasis(AppSysParamsUtil.getBSPDefaultFareBasisCode());
							String flightNo = resSeg.getFlightNumber().substring(2);
							flightCoupon.setFlightNumber(flightNo);
							flightCoupon.setFlightDate(resSeg.getDepartureDate());
							flightCoupon.setFlightDepartureTime(resSeg.getDepartureDate());
							flightCoupon.setReservationBookingDesignatior("Y");
							flightCoupon.setSegmentIdentifier(new Long(resSeg.getSegmentIdentifier()));
							flightCoupon.setFreeBaggageAllowance(resSeg.getFreeBaggageAllowance() + "k");
							flightCoupon.setFlightBookingStatus("OK");
							if (couponsUsed == 4) {
								IT06_Itinerary it06Second = (IT06_Itinerary) tktt.createNewRecord(IT06_Itinerary.class);
								it06Second.addFlightCoupon(flightCoupon);
							} else {
								it06.addFlightCoupon(flightCoupon);
							}

							couponsUsed++;
						}

						it02.setCouponUseIndicator(coupons);

						if (bspTnx.getSecondSetOfSegments() != null) {
							IT02_BasicTransaction_CNJ it02CNJ = (IT02_BasicTransaction_CNJ) tktt
									.createNewRecord(IT02_BasicTransaction_CNJ.class);

							// it02CNJ.setAgentCode(new Long(agent.getIataCode()));
							it02CNJ.setDateOfIssue(bspTnx.getDateOfIssue());
							it02CNJ.setPassengerName(bspTnx.getPaxName());
							it02CNJ.setTicketingAirlineCodeNo(bspTnx.getTicketingAirlineCode());
							it02CNJ.setTicketDocumentNo(bspTnx.getSecondSetOfSegments().getLeft() + "");
							it02CNJ.setApprovedLocationType(ApprovedLocationType.RETAIL_AGENCY);
							it02CNJ.setIsoCountryCode(agent.getStation_code());
							it02CNJ.setStatisticalCode(StatisticalCodeType.International);

							it02CNJ.setConjunctionTicketIndicator(ConjuctionTktIndicator.CNJ);

							IT06_Itinerary_CNJ it06CNJ = (IT06_Itinerary_CNJ) tktt.createNewRecord(IT06_Itinerary_CNJ.class);
							int couponsUsedCNJ = 1;
							CouponUseIndicator[] couponsCNJ = { CouponUseIndicator.V, CouponUseIndicator.V, CouponUseIndicator.V,
									CouponUseIndicator.V };

							for (BSPSegmentInfoDTO resSeg : bspTnx.getSecondSetOfSegments().getRight()) {

								FlightCoupon flightCoupon = new FlightCoupon();
								couponsCNJ[couponsUsedCNJ - 1] = CouponUseIndicator.F;
								flightCoupon.setOriginAirportCode(resSeg.getOrigin());
								flightCoupon.setDestinationAirportCode(resSeg.getDestination());
								flightCoupon.setCarrier(resSeg.getCarrierCode());
								flightCoupon.setFareBasis(AppSysParamsUtil.getBSPDefaultFareBasisCode());
								String flightNo = resSeg.getFlightNumber().substring(2);
								flightCoupon.setFlightNumber(flightNo);
								flightCoupon.setFlightDate(resSeg.getDepartureDate());
								flightCoupon.setFlightDepartureTime(resSeg.getDepartureDate());
								flightCoupon.setReservationBookingDesignatior("Y");
								flightCoupon.setSegmentIdentifier(new Long(resSeg.getSegmentIdentifier()));
								flightCoupon.setFreeBaggageAllowance(resSeg.getFreeBaggageAllowance() + "k");
								flightCoupon.setFlightBookingStatus("OK");
								if (couponsUsedCNJ == 4) {
									IT06_Itinerary_CNJ it06SecondCNJ = (IT06_Itinerary_CNJ) tktt
											.createNewRecord(IT06_Itinerary_CNJ.class);
									it06SecondCNJ.addFlightCoupon(flightCoupon);
								} else {
									it06CNJ.addFlightCoupon(flightCoupon);
								}
								couponsUsedCNJ++;

							}
							it02CNJ.setCouponUseIndicator(couponsCNJ);
						}

						IT07_FareCalculation it07 = (IT07_FareCalculation) tktt.createNewRecord(IT07_FareCalculation.class);
						it07.setFareCalculationModeIndicator("0");
						String[] fareCalcAreas = bspTnx.getFareCalculationAreas();
						if (fareCalcAreas == null || fareCalcAreas[0] == null) {
							throw new BSPException("Fare Calculation Area is missing");
						} else {
							if (fareCalcAreas[1] == null) {
								it07.addFareCalculationArea(fareCalcAreas[0] + IT07XTData, 1);
							} else {
								it07.addFareCalculationArea(fareCalcAreas[0], 1);
								if (fareCalcAreas[2] == null) {
									it07.addFareCalculationArea(fareCalcAreas[1] + IT07XTData, 2);
								} else {
									it07.addFareCalculationArea(fareCalcAreas[1], 2);
									if (fareCalcAreas[3] == null) {
										IT07_FareCalculation it07Sec = (IT07_FareCalculation) tktt
												.createNewRecord(IT07_FareCalculation.class);
										it07Sec.setFareCalculationModeIndicator("0");
										it07Sec.addFareCalculationArea(fareCalcAreas[2] + IT07XTData, 3);
									} else {
										IT07_FareCalculation it07Sec = (IT07_FareCalculation) tktt
												.createNewRecord(IT07_FareCalculation.class);
										it07Sec.setFareCalculationModeIndicator("0");

										it07Sec.addFareCalculationArea(fareCalcAreas[2], 3);
										it07Sec.addFareCalculationArea(fareCalcAreas[3] + IT07XTData, 4);
									}
								}
							}
						}

						IT08_FormOfPayment it08 = (IT08_FormOfPayment) tktt.createNewRecord(IT08_FormOfPayment.class);
						FormOfPayment fop = new FormOfPayment(baseCurrency, baseCurencyDecimalPlaces);
						fop.setPaymentAmount(bspTnx.getPayCurrencyAmount().divide(bspTnx.getCurrencyExchangeRate(), 3));

						fop.setFormOfPaymentType(FormOfPaymentType.CA);
						fop.setCustomerFileReference(bspTnx.getPnr());
						it08.addFormOfPayment(fop);

						IT09_AdditionalInformation it09 = (IT09_AdditionalInformation) tktt
								.createNewRecord(IT09_AdditionalInformation.class);

						String endosement = "NON INTERLINEABLE/NON REF//NON ENDO"; // todo fill with the endorcements
																					// for transaction
						it09.setEndorsements(endosement);
						// CASH or CREDIT CARD or for exchange DE*CA/SYP250000
						it09.addFormOfPayment("CASH");
					} else if (bspTnx.getTnxType() == TransactionType.RFND) {
						RFND rfnd = rfb.getNewRFNDInstance();
						IT02_BasicTransaction it02 = (IT02_BasicTransaction) rfnd.createNewRecord(IT02_BasicTransaction.class);
						// it02.setAgentCode(new Long(agent.getIataCode()));
						it02.setDateOfIssue(bspTnx.getDateOfIssue());
						it02.setPassengerName(bspTnx.getPaxName());
						it02.setTicketingAirlineCodeNo(bspTnx.getTicketingAirlineCode());
						it02.setTicketDocumentNo(correctedEticketNumber(bspTnx.getFirstSetOfSegments().getLeft() + "",
								bspTnx.getTicketingAirlineCode()));
						// it02.setApprovedLocationType(ApprovedLocationType.RETAIL_AGENCY);
						it02.setIsoCountryCode("SL");
						it02.setStatisticalCode(StatisticalCodeType.International);
						it02.setDataInputStatusIndicator(DataInputStatusIndicator.M);
						// TODO set the correct indicator

						IT03_RelatedTicketInformation it03 = (IT03_RelatedTicketInformation) rfnd
								.createNewRecord(IT03_RelatedTicketInformation.class);
						int[] couponNoIdentifiers = new int[4];
						for (int i = 0; i < bspTnx.getFirstSetOfSegments().getRight().size(); ++i) {
							couponNoIdentifiers[i] = i + 1;
						}
						StringBuffer sbCouponNoID = new StringBuffer();
						for (int j : couponNoIdentifiers) {
							sbCouponNoID.append("" + j + "");
						}
						it03.addTicketCoupon(
								sbCouponNoID.toString(),
								correctedEticketNumber(bspTnx.getFirstSetOfSegments().getLeft() + "",
										bspTnx.getTicketingAirlineCode()));
						if (bspTnx.getSecondSetOfSegments() != null) {
							couponNoIdentifiers = new int[4];
							for (int i = 0; i < bspTnx.getSecondSetOfSegments().getRight().size(); ++i) {
								couponNoIdentifiers[i] = i + 1;
							}
							sbCouponNoID = new StringBuffer();
							for (int j : couponNoIdentifiers) {
								sbCouponNoID.append("" + j + "");
							}
							it03.addTicketCoupon(sbCouponNoID.toString(), bspTnx.getSecondSetOfSegments().getLeft().toString());
						}
						it03.setDateOfIssueRelatedDoc(bspTnx.getOriginalPaymentTnxDate());

						IT05_MonetaryAmounts it05 = (IT05_MonetaryAmounts) rfnd.createNewRecord(IT05_MonetaryAmounts.class);
						// it05.setAmountEnteredByAgent(bspTnx.getPayCurrencyAmount().negate());
						// it05.setAmountPaidByCustomer(bspTnx.getPayCurrencyAmount().negate());
						it05.setAmountEnteredByAgent(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
						it05.setAmountPaidByCustomer(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
						it05.setCurrencyType(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
						it05.setNetReportingIndicator(NetReportingIndicator.NA);
						it05.setTicketAmount(bspTnx.getPayCurrencyAmount().negate(), payCurrDecimalPlaces);

						IT08_FormOfPayment it08 = (IT08_FormOfPayment) rfnd.createNewRecord(IT08_FormOfPayment.class);
						FormOfPayment fop = new FormOfPayment(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
						fop.setPaymentAmount(bspTnx.getPayCurrencyAmount().negate());
						fop.setFormOfPaymentType(FormOfPaymentType.CA);
						fop.setCustomerFileReference(bspTnx.getPnr());
						it08.addFormOfPayment(fop);
					}
					loggingDataMap.get(rfb).add(bspTnx);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * IT02_BasicTransaction it02 = (IT02_BasicTransaction) tktt.createNewRecord(IT02_BasicTransaction.class);
		 * it02.setAgentCode((long) 1); IT04_AdditionalSaleInfo it04 = (IT04_AdditionalSaleInfo)
		 * tktt.createNewRecord(IT04_AdditionalSaleInfo.class);
		 * 
		 * it04.setTicketingModeIndicator(TicketingMode.DIRECT); RFND rfnd = rfb.getNewRFNDInstance();
		 */
		buf.append(rfb.getDISHFmtData());

		return buf.toString();
	}

	private String generateETLRContent(Date etlrPeriodStartDate, Date etlrPeriodEndDate, UserPrincipal userPrincipal)
			throws BSPException {
		StringBuffer buf = new StringBuffer();
		ETLRFileBuilder hfb = new ETLRFileBuilder("", new Date(), etlrPeriodEndDate, 10L);
		Map<ETLRFileBuilder, List<BSPTransactionDataDTO>> loggingDataMap = new HashMap<ETLRFileBuilder, List<BSPTransactionDataDTO>>();

		Collection<ETLRCouponRec> etlrCoupons = InvoicingModuleUtils.getBSPLinkJdbcDao().getETLRCouponRec(etlrPeriodStartDate,
				etlrPeriodEndDate);
		HashMap<String, ETLRRec> etlrRecords = new HashMap<String, ETLRRec>();
		Iterator<ETLRCouponRec> itETLRCoupons = etlrCoupons.iterator();
		while (itETLRCoupons.hasNext()) {
			ETLRCouponRec eticketCoupon = (ETLRCouponRec) itETLRCoupons.next();
			String eticketNumber = eticketCoupon.geteTicketNumber();
			// if eticket number is exist, adding new coupon for the eticket
			if (etlrRecords.containsKey(eticketNumber)) {
				ETicketCoupon coupon = new ETicketCoupon();
				coupon.setCouponNumber(eticketCoupon.getCouponNumber());
				String couponStatus = eticketCoupon.getCouponStatus();
				coupon.setCouponStatus(couponStatus);
				coupon.setSegmentCode(eticketCoupon.getSegmentCode());
				coupon.setDepatureDate(eticketCoupon.getDepatureDate());
				coupon.setFlightNumber(eticketCoupon.getFlightNumber());
				coupon.setClassofService(eticketCoupon.getClassofService());
				if (etlrRecords.get(eticketNumber).getIsInFinalEticketStatus()) {
					if (!(couponStatus.equalsIgnoreCase(EticketStatus.FLOWN.code())
							|| couponStatus.equalsIgnoreCase(EticketStatus.FLOWN.code())
							|| couponStatus.equalsIgnoreCase(EticketStatus.EXCHANGED.code())
							|| couponStatus.equalsIgnoreCase(EticketStatus.VOID.code())
							|| couponStatus.equalsIgnoreCase(EticketStatus.CLOSED.code())
							|| couponStatus.equalsIgnoreCase(EticketStatus.PRINT_EXCHANGED.code()) || couponStatus
								.equalsIgnoreCase(EticketStatus.BOARDED.code()))) {

						etlrRecords.get(eticketNumber).setIsInFinalEticketStatus(false);
					}
				}
				if (etlrRecords.get(eticketNumber).isAllVoid()) {
					if (!couponStatus.equalsIgnoreCase(EticketStatus.VOID.code())) {
						etlrRecords.get(eticketNumber).setAllVoid(false);
					}
				}
				ETLRRec etlrRec = etlrRecords.get(eticketNumber);
				// Adding segmentwise(coupon by coupon) charges to calculate E-ticket total
				etlrRecords.get(eticketNumber).setTotalSurchargeAmount(
						etlrRec.getTotalSurchargeAmount().add(eticketCoupon.getTotalSurchargeAmount()));
				etlrRecords.get(eticketNumber).setTaxAmount(etlrRec.getTaxAmount().add(eticketCoupon.getTaxAmount()));
				etlrRecords.get(eticketNumber).setFareAmount(etlrRec.getFareAmount().add(eticketCoupon.getFareAmount()));

				etlrRecords.get(eticketNumber).geteTicketCoupon().add(coupon);
				etlrRecords.get(eticketNumber).getLastUpdatedCouponData().put(eticketCoupon.getCouponNumber(), coupon);

			} else { // if new eticket number
				ETLRRec etlrRec = new ETLRRec();
				etlrRec.setIsInFinalEticketStatus(true);
				etlrRec.setAllVoid(true);
				etlrRec.seteTicketNumber(eticketNumber);
				etlrRec.setModifiedDate(eticketCoupon.getModifiedDate());
				etlrRec.setCreatedDate(eticketCoupon.getCreatedDate());
				etlrRec.setPaxTypeCode(eticketCoupon.getPaxTypeCode());
				etlrRec.setFirstName(eticketCoupon.getFirstName());
				etlrRec.setLastName(eticketCoupon.getLastName());
				etlrRec.setPaxID(eticketCoupon.getPaxID());
				etlrRec.setReturnFlag(eticketCoupon.getReturnFlag());
				etlrRec.setPnr(eticketCoupon.getPnr());
				etlrRec.setPaymentCurrencyCode(AppSysParamsUtil.getBaseCurrency());
				etlrRec.setTotalSurchargeAmount(eticketCoupon.getTotalSurchargeAmount());
				etlrRec.setTaxAmount(eticketCoupon.getTaxAmount());
				etlrRec.setFareAmount(eticketCoupon.getFareAmount());
				etlrRec.setAgentTypeCode(eticketCoupon.getAgentTypeCode());
				etlrRec.setAgentCountaryCode(eticketCoupon.getAgentCountaryCode());
				etlrRec.setAgentID(eticketCoupon.getAgentID());
				etlrRec.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				etlrRec.seteTicketVersion(eticketCoupon.geteTicketVersion());

				Collection<ETicketCouponStatus> etlrCouponStatus = InvoicingModuleUtils.getBSPLinkJdbcDao()
						.getETicketCouponStatus(eticketNumber);
				etlrRec.seteTicketCouponStatus(etlrCouponStatus);
				String pofDate = new SimpleDateFormat("dd-MMM-yy").format(eticketCoupon.getModifiedDate());
				List<ETicketFOP> etlrFOPs = InvoicingModuleUtils.getBSPLinkJdbcDao().getETicketFOP(eticketNumber, pofDate,
						eticketCoupon.getPaxID());
				etlrRec.seteTicketFop(etlrFOPs);

				ETicketCoupon coupon = new ETicketCoupon();
				coupon.setCouponNumber(eticketCoupon.getCouponNumber());
				String couponStatus = eticketCoupon.getCouponStatus();
				coupon.setCouponStatus(couponStatus);
				if (!(couponStatus.equalsIgnoreCase(EticketStatus.FLOWN.code())
						|| couponStatus.equalsIgnoreCase(EticketStatus.FLOWN.code())
						|| couponStatus.equalsIgnoreCase(EticketStatus.EXCHANGED.code())
						|| couponStatus.equalsIgnoreCase(EticketStatus.VOID.code())
						|| couponStatus.equalsIgnoreCase(EticketStatus.CLOSED.code())
						|| couponStatus.equalsIgnoreCase(EticketStatus.PRINT_EXCHANGED.code()) || couponStatus
							.equalsIgnoreCase(EticketStatus.BOARDED.code()))) {
					etlrRec.setIsInFinalEticketStatus(false);
				}
				if (!couponStatus.equalsIgnoreCase(EticketStatus.VOID.code())) {
					etlrRec.setAllVoid(false);
				}
				coupon.setSegmentCode(eticketCoupon.getSegmentCode());
				coupon.setDepatureDate(eticketCoupon.getDepatureDate());
				coupon.setFlightNumber(eticketCoupon.getFlightNumber());
				coupon.setClassofService(eticketCoupon.getClassofService());
				List<ETicketCoupon> listCoupon = new ArrayList<ETicketCoupon>();
				listCoupon.add(coupon);
				etlrRec.seteTicketCoupon(listCoupon);
				HashMap<String, ETicketCoupon> lastUpdatedCouponData = new HashMap<String, ETicketCoupon>();
				lastUpdatedCouponData.put(eticketCoupon.getCouponNumber(), coupon);
				etlrRec.setLastUpdatedCouponData(lastUpdatedCouponData);

				etlrRecords.put(eticketNumber, etlrRec);

			}
		}
		return genarateETLRContent(etlrRecords, etlrPeriodStartDate);
	}

	private String genarateETLRContent(HashMap<String, ETLRRec> etlrRecords, Date etlrPeriodStartDate) throws BSPException {
		RETFileGenerationBL retFileBl = new RETFileGenerationBL();

		System.out.println("ETLR gen started......");
		StringBuilder sbETLR = new StringBuilder("ETH SST ETLR                 000000000"
				+ new SimpleDateFormat("ddMMyy").format(etlrPeriodStartDate) + "\n");

		int numofrec = 0;
		for (String eticketNumber : etlrRecords.keySet()) {
			System.out.println(numofrec++ + "ETicket Number:" + eticketNumber);
			ETLRFileBuilder retBuilder = new ETLRFileBuilder("WW", new Date(), new Date(), 201L);
			ETLR tktt = retBuilder.getNewETLRInstance();

			ETLRRec etlrRec = etlrRecords.get(eticketNumber);
			List<ETicketCoupon> listCoupon = etlrRec.geteTicketCoupon();

			String origin = listCoupon.get(0).getSegmentCode().substring(0, 3);
			String destination = listCoupon.get(listCoupon.size() - 1).getSegmentCode().substring(4, 7);
			String deliveringCity = destination;
			if ("Y".equalsIgnoreCase(etlrRec.getReturnFlag())) {
				deliveringCity = listCoupon.get(listCoupon.size() - 1).getSegmentCode().substring(0, 3);
			}

			X00_Ticket_Header_Record x00 = (X00_Ticket_Header_Record) tktt.createNewRecord(X00_Ticket_Header_Record.class);
			x00.setTicketNumber(eticketNumber);
			x00.setRTATimeStamp(etlrRec.getModifiedDate());
			x00.setRecordType("OO");

			X00_Mutation_Ticket_Header_Record x00MT = (X00_Mutation_Ticket_Header_Record) tktt
					.createNewRecord(X00_Mutation_Ticket_Header_Record.class);

			x00MT.setIsNewTicketIndication("Y");
			if (etlrRec.geteTicketVersion() > 0) {
				x00MT.setIsNewTicketIndication("N");
			}

			x00MT.setPurgeIndication("");
			if (etlrRec.getIsInFinalEticketStatus()) {
				x00MT.setPurgeIndication("P");
			}

			x00MT.setVoidIndication("N");
			if (etlrRec.isAllVoid()) {
				x00MT.setVoidIndication("Y");

			}

			X00_25_Ticket_Header_Record x00_25 = (X00_25_Ticket_Header_Record) tktt
					.createNewRecord(X00_25_Ticket_Header_Record.class);
			x00_25.setPaxType(etlrRec.getPaxTypeCode());
			x00_25.setPaxFirstName(etlrRec.getFirstName());
			x00_25.setPaxSurName(etlrRec.getLastName());

			X00_30_Ticket_Header_Record X00_30 = (X00_30_Ticket_Header_Record) tktt
					.createNewRecord(X00_30_Ticket_Header_Record.class);
			X00_30.setNUM_FLT_SEGS(etlrRec.geteTicketCoupon().size());
			X00_30.setDOCUMENT_TYPE("T");
			X00_30.setTKT_CREATE_DATE(etlrRec.getCreatedDate());
			X00_30.set1ST_TKT_NUMBER(eticketNumber);
			X00_30.setLAST_TKT_NUMBER(eticketNumber);
			X00_30.setNUM_OF_BOOKLETS(1);
			X00_30.setTOUR_CODE("");
			X00_30.setTKTNG_MODE_INDIC("");
			X00_30.setINTL_SALES_INDIC("");
			X00_30.setINTL_DOM_IND("I");// TODO:
			X00_30.setSELF_SALE_INDIC("");
			X00_30.setNET_RPTG_INDIC("");
			X00_30.setTKTING_DATE(etlrRec.getCreatedDate());
			X00_30.setORIG_AIRPORT(origin);
			X00_30.setDEST_AIRPORT(destination);
			X00_30.setRES_SYSTEM(etlrRec.getCarrierCode());
			X00_30.setSYMB_PNR_ADDR(etlrRec.getPnr());
			X00_30.setRES_SYSTM_OAL("");
			X00_30.setSYMB_PNR_ADDR_OAL("");
			X00_30.setINVOICE_NUM("");
			X00_30.setCLIENT_ACCT_CODE("");
			X00_30.setPRICING_INDIC("");// TODO:
			X00_30.setCURRENCY_CODE(etlrRec.getPaymentCurrencyCode());
			X00_30.setTOTAL_FARE(etlrRec.getFareAmount().add(etlrRec.getTaxAmount()).add(etlrRec.getTotalSurchargeAmount())
					.toString());
			X00_30.setBF_CURR_CD(etlrRec.getPaymentCurrencyCode());
			X00_30.setBF_FARE(etlrRec.getFareAmount().add(etlrRec.getTotalSurchargeAmount()).toString());
			X00_30.setEF_CURR_CD("");
			X00_30.setEF_FARE("0.0");
			X00_30.setBOOKING_IATA("");
			X00_30.setSYSTEM_PROVIDER(eticketNumber.substring(0, 3));
			X00_30.setDELIV_SYSTEM(etlrRec.getCarrierCode());
			X00_30.setDELIV_CITY(deliveringCity);
			X00_30.setAGY_LOCALE("");
			X00_30.setRES_SYS_ID("");
			X00_30.setPSEUDO_CITY("");
			X00_30.setORIG_SYS("");
			X00_30.setAGENT_TYPE("T");
			X00_30.setTKTD_COUNTRY(etlrRec.getAgentCountaryCode());
			X00_30.setTKTD_AGENT(etlrRec.getAgentID());
			X00_30.setNET_CURR(etlrRec.getPaymentCurrencyCode());
			X00_30.setNET_AMNT(etlrRec.getFareAmount().add(etlrRec.getTaxAmount()).add(etlrRec.getTotalSurchargeAmount())
					.toString());
			X00_30.setBBR_CURREXCH_CODE("");
			X00_30.setTL3("");
			X00_30.setCURRENCY_EXCH_AMT("");
			X00_30.setCERTIFICATE_NUMBER("");
			X00_30.setENDORSE_RESTRICT("");

			X00_50_Ticket_Header_Record X00_50 = (X00_50_Ticket_Header_Record) tktt
					.createNewRecord(X00_50_Ticket_Header_Record.class);
			X00_50.setDOC_CURR_CODE(etlrRec.getPaymentCurrencyCode());
			X00_50.setDOC_AMT(etlrRec.getFareAmount().add(etlrRec.getTaxAmount()).add(etlrRec.getTotalSurchargeAmount())
					.toString());

			X00_90_Ticket_Header_Record X00_90 = (X00_90_Ticket_Header_Record) tktt
					.createNewRecord(X00_90_Ticket_Header_Record.class);
			X00_90.setPLATING_CARRIER(etlrRec.getCarrierCode());
			X00_90.setDATE_TIME(etlrRec.getCreatedDate());

			X34_Mutation_Fare_Record X34 = (X34_Mutation_Fare_Record) tktt.createNewRecord(X34_Mutation_Fare_Record.class);
			X34.setTICKET_NUMBER(eticketNumber);
			X34.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
			X34.setREC_TYPE("34");
			X34.setSEQENCE_INDIC("0001");// TODO:
			X34.setFARE_LADDER("");
			ArrayList<BSPTransactionTaxData> paxTaxData = InvoicingModuleUtils.getBSPLinkJdbcDao().getBSPPaxTnxTaxData(
					etlrRec.getPaxID().toString());

			// TODO: prepare the tax breakdown
			X36_Ticket_Tax X36 = (X36_Ticket_Tax) tktt.createNewRecord(X36_Ticket_Tax.class);
			X36.setTICKET_NUMBER(eticketNumber);
			X36.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
			X36.setREC_TYPE("36");
			int taxIndex = 0;
			for (BSPTransactionTaxData taxData : paxTaxData) {
				if (!taxData.getAmount().equalsIgnoreCase("0")) {
					X36.addTAXData(taxIndex, taxData.getCurrancyCode(), taxData.getCountryCode(), taxData.getTaxCode(),
							taxData.getAmount());
					taxIndex++;
				}
			}

			// TODO: prepare Commission
			// X40_Commission_Data X40 = (X40_Commission_Data) tktt.createNewRecord(X40_Commission_Data.class);
			// X40.setTICKET_NUMBER(eticketNumber);
			// X40.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
			// X40.setREC_TYPE("40");
			// X40.setSEQUENCE_INDIC("0001");
			// X40.setCOMMISS_SEQ_NUM("0096");
			// X40.setCOMMISS_RATE("23.2");
			// X40.setCOMMISS_AMT("23245.34");
			// X40.setCOMMISS_CURR_CODE("AED");
			// X40.setCOMMISS_TAX_INDIC("");
			// X40.setCOMMISS_TYPE("");

			// TODO: Form OF Payment join t_res_pcd & T_TEMP_PAYMENT_TNX
			List<ETicketFOP> listFOP = etlrRec.geteTicketFop();
			int index = 1;
			for (ETicketFOP eTicketFOP : listFOP) {
				X45_FOP X45 = (X45_FOP) tktt.createNewRecord(X45_FOP.class);
				X45.setTICKET_NUMBER(eticketNumber);
				X45.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
				X45.setREC_TYPE("45");
				X45.setSEQUENCE_INDIC(Integer.toString(index));
				X45.setFOP_TYPE(eTicketFOP.getPaymentMethod());
				X45.setFOP_AMT(eTicketFOP.getTnxAmount());
				X45.setCC_VENDOR(eTicketFOP.getCcVendor());
				X45.setCC_NUM("");
				X45.setCC_EXP_DATE("");
				X45.setAUTH("");
				X45.setAPPROVAL_SOURCE("");
				X45.setAUTH_AMT("");
				X45.setADDR_VERIF_CODE("");
				X45.set1ST_CC_NUM_EXT_PAY("");
				X45.setCC_CORP_CONTRACT("");
				index++;
			}

			int couponSequqnce = 1;
			int exCouponSequqnce = 1;
			int ticketNumber = 100;

			ArrayList<ETLRCoupon> coupns = InvoicingModuleUtils.getBSPLinkJdbcDao().getETKTCoupon(eticketNumber);
			for (ETLRCoupon eTicketCoupon : coupns) {
				if (!"E".equalsIgnoreCase(eTicketCoupon.getCouponStatus())) {
					X60_Coupon X60 = (X60_Coupon) tktt.createNewRecord(X60_Coupon.class);
					X60.setTICKET_NUMBER(eticketNumber);
					X60.setRTA_TIMESTAMP(eTicketCoupon.getModifiedDate());
					X60.setREC_TYPE("60");
					X60.setSEQUENCE_INDIC(new BigDecimal(couponSequqnce));
					X60.setTICKET_NUM(new BigDecimal(ticketNumber + couponSequqnce));
					X60.setFLIGHT_INDICS(" ");
					X60.setSTAT_CHG_DATE(eTicketCoupon.getModifiedDate());
					X60.setEDIFACT_COUPON_NUM(new BigDecimal(couponSequqnce));
					X60.setCOUPON_STATUS(eTicketCoupon.getCouponStatus());
					X60.setEXCH_TKT_NUM("");
					X60.setCOUPON_VALUE(BigDecimal.ZERO);
					X60.setDEP_DATE(eTicketCoupon.getDepatureDate());
					X60.setDEP_TIMEIn24hrs(eTicketCoupon.getDepatureDate());
					X60.setSTOPOVER_CODE("");
					X60.setSEGMENT_STATUS("OK");
					X60.setSAC_CODE("");
					X60.setNVB_CENT("");
					X60.setNVB_DATE(null);
					X60.setNVA_CENT("");
					X60.setNVA_DATE(null);
					X60.setORIG_AIRPORT(eTicketCoupon.getDepatureCityCode());
					X60.setDEST_AIRPORT(eTicketCoupon.getArrivalCityCode());
					X60.setMKTING_AIRLINE(etlrRec.getCarrierCode());
					X60.setCARRIER(etlrRec.getCarrierCode());
					X60.setFLIGHT_NUM(eTicketCoupon.getFlightNumber());
					X60.setCLASS_OF_SERVICE("");
					X60.setFQTV_AIRLINE("");
					X60.setFQTV_NUMBER("");
					// X60.setBBB_DATE(new SimpleDateFormat("yyyyMMdd").format(new Date()));
					X60.setBBB_DATE("00000000");
					X60.setFARE_BASIS_CODE("");
					X60.setVOL_INVOL_INDIC("");
					X60.setBAGGAGE_RESTRICT_CD(eTicketCoupon.getBagWeight());
					X60.setBAGGAGE_UNIT("K");

					couponSequqnce++;
				} else {
					X70_Exchange_Records X70 = (X70_Exchange_Records) tktt.createNewRecord(X70_Exchange_Records.class);
					X70.setTICKET_NUMBER(eticketNumber);
					X70.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
					X70.setREC_TYPE("70");
					X70.setSEQUENCE_INDIC(Integer.toString(exCouponSequqnce));
					X70.setETK("");
					X70.setEXCH_TYPE("");
					X70.setORIG_FOP("");
					X70.setADDL_COLL_CURR("");
					X70.setADDL_COLL_AMT("");
					X70.setADDL_COLL_TOTCURR(etlrRec.getPaymentCurrencyCode());
					X70.setADDL_COLL_TOTAMT("");
					X70.setADMIN_FEE_AMT("");
					X70.setTTX("");
					X70.setEXCHANGE_INFO("");
					exCouponSequqnce++;
				}

			}
			// TODO: recent Exchange Records
			// X74_Recent_Exchange_Record X74 = (X74_Recent_Exchange_Record)
			// tktt.createNewRecord(X74_Recent_Exchange_Record.class);
			// X74.setTICKET_NUMBER(eticketNumber);
			// X74.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
			// X74.setREC_TYPE("74");
			// X74.setSEQUENCE_INDIC("0001");
			// X74.setMOST_REC_EXCH_TKT("");

			// TODO Alternative Flight History Records
			// X80_Alternative_Flight_History_Record X80 = (X80_Alternative_Flight_History_Record) tktt
			// .createNewRecord(X80_Alternative_Flight_History_Record.class);
			// X80.setTICKET_NUMBER(eticketNumber);
			// X80.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
			// X80.setREC_TYPE("80");
			// X80.setSEQUENCE_INDIC("0001");
			// X80.setATC("");
			// X80.setCT2("");
			// X80.setHCC("");
			// X80.setHDT("");
			// X80.setHDC("");
			// X80.setFLT("");
			// X80.setCLA("");
			// X80.setAVI("");
			// X80.setFBN("");

			Iterator<ETicketCouponStatus> itETLRCoupons = etlrRec.geteTicketCouponStatus().iterator();
			int couponSeq = 1;
			while (itETLRCoupons.hasNext()) {
				ETicketCouponStatus historicalCoupon = itETLRCoupons.next();
				X92_Coupon_Status_History_Record X92 = (X92_Coupon_Status_History_Record) tktt
						.createNewRecord(X92_Coupon_Status_History_Record.class);
				X92.setTICKET_NUMBER(eticketNumber);
				X92.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
				X92.setREC_TYPE("92");
				X92.setSEQUENCE_INDIC(new BigDecimal(couponSeq));
				X92.setCSH(new BigDecimal(100 + couponSeq));
				X92.setTPS(historicalCoupon.getCouponStatus());
				X92.setDATE_TIME(historicalCoupon.getModifiedDate());
				X92.setTPC(historicalCoupon.getAgentCode());
				X92.setTHL(historicalCoupon.getPsudoCity());
				X92.setTHA("");
				X92.setTAG(historicalCoupon.getAgentCode());
				X92.setTAN("0000000");
				// if (historicalCoupon.getAgentID() != null) {
				// X92.setTAG(historicalCoupon.getAgentID());
				// X92.setTAN(historicalCoupon.getAgentCode());
				// }
				X92.setTPT(etlrRec.getCarrierCode());
				X92.setSSQ("");
				X92.setTTH("");
				X92.setAP4("");
				X92.setOPN(historicalCoupon.getPnr());
				X92.setTCJ("");
				X92.setCNJ("");
				String ticketModiDate = new SimpleDateFormat("ddMMyyyy").format(etlrRec.getModifiedDate());
				String couponModiDate = new SimpleDateFormat("ddMMyyyy").format(historicalCoupon.getModifiedDate());
				X92.setCHG_STATUS("N");
				if (ticketModiDate.equalsIgnoreCase(couponModiDate)) {
					X92.setCHG_STATUS("Y");
				}

				couponSeq++;
			}
			int couponSeq94 = 1;
			for (String eticketcouponNumber : etlrRec.getLastUpdatedCouponData().keySet()) {
				ETicketCoupon lastUpdatedCoupon = etlrRec.getLastUpdatedCouponData().get(eticketcouponNumber);
				X94_Revalidation_Record X94 = (X94_Revalidation_Record) tktt.createNewRecord(X94_Revalidation_Record.class);
				X94.setTICKET_NUMBER(eticketNumber);
				X94.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
				X94.setREC_TYPE("94");
				X94.setSEQUENCE_INDIC(Integer.toString(couponSeq94));
				X94.setRSN(Integer.toString(100 + couponSeq94));
				X94.setRSS("0");
				X94.setORIG_MK_CRR_CD(etlrRec.getCarrierCode());
				X94.setORIG_MK_DEP_TIME(etlrRec.getCreatedDate());
				X94.setORIG_MK_DEP_DATE(etlrRec.getCreatedDate());
				X94.setORIG_MK_FLT_NBR("");
				X94.setORIG_MK_COS("");
				X94.setNEW_MK_CRR_CD(etlrRec.getCarrierCode());
				X94.setNEW_MK_DEP_TIME(lastUpdatedCoupon.getDepatureDate());
				X94.setNEW_MK_DEP_DATE(lastUpdatedCoupon.getDepatureDate());
				// RB152A- more than 5 characters remove RB- carrier code
				String flightNumber = lastUpdatedCoupon.getFlightNumber().substring(2);
				X94.setNEW_MK_FLT_NBR(flightNumber);
				X94.setNEW_OP_COS(lastUpdatedCoupon.getClassofService());
				X94.setORIG_OP_CRR_CD(etlrRec.getCarrierCode());
				X94.setORIG_OP_FLT_NBR("");
				X94.setORIG_OP_COS("");
				X94.setORIG_OP_CTL_CRR("");
				X94.setNEW_OP_CRR_CD(etlrRec.getCarrierCode());
				X94.setNEW_OP_FLT_NBR("");
				X94.setNEW_OP_COS("");
				X94.setNEW_OP_CTL_CRR("");
				couponSeq94++;
			}

			// TODO: IN OUR DB WE RECORD ONLY THE ALLOW BAGGAGE WEIGHT, AND ADDITIONA REQUESTED WEIGHT WHEN THE TIME OF
			// CREATING RESEVATION: "MD"
			// XA0_Extend_Alternate_Coupon_Flight_History XA0 = (XA0_Extend_Alternate_Coupon_Flight_History) tktt
			// .createNewRecord(XA0_Extend_Alternate_Coupon_Flight_History.class);
			// XA0.setTICKET_NUMBER(eticketNumber);
			// XA0.setRTA_TIMESTAMP(etlrRec.getModifiedDate());
			// XA0.setREC_TYPE("60");
			// XA0.setSEQUENCE_INDIC("0001");
			// XA0.setSEN("");
			// XA0.setSECOND_SEQ_NBR("");
			// XA0.setUNIT_OF_MEASURE("");
			// XA0.setNBR_OF_BAGS("");
			// XA0.setCHK_BAG_WEIGHT("");
			// XA0.setUNCHK_BAG_WEIGHT("");
			// XA0.setHEAD_OF_POOL("");
			// XA0.setNBR_OF_BAG_TAGS("");
			// XA0.setBAG_TAG_INFO("");
			// XA0.setDEST("");
			// XA0.setBAG_TAG_CRR("");
			// XA0.setSTART_BAG_TAG_NBR("");
			// XA0.setNBR_CONSEC_NBRS("");
			// System.out.println(retBuilder.getDISHFmtData());

			sbETLR.append(retBuilder.getDISHFmtData());
		}
		System.out.println("------------------------------------------------------------------------------");
		// System.out.println(sbETLR.toString());
		System.out.println("------------------------------------------------------------------------------");
		System.out.println("ETLR gen end......");
		return sbETLR.toString();

	}

	private void writeTextFile(String fileName, String content) throws IOException {

		File folderCheck = new File(PlatformConstants.getAbsRETFileUploadPath());

		if (folderCheck.canWrite()) {
			StringBuilder localFilePath = new StringBuilder();
			localFilePath.append(PlatformConstants.getAbsRETFileUploadPath()).append(File.separatorChar);
			localFilePath.append(fileName);

			File file = new File(localFilePath.toString());
			FileUtils.writeStringToFile(file, content);
		} else {
			throw new IOException(fileName + ":RET file writing failed due to limited permissions");
		}
	}

	private String adjustDecimalPlaces(BigDecimal decimal, int decimalPlaces, String currancyCode, int maxLength) {
		String str = decimal.toString();
		String arr[] = str.split("\\.");
		if (arr.length > 1) {
			String dec = arr[1];
			if (dec.length() < decimalPlaces) {
				dec = dec + getZeros(decimalPlaces - dec.length());
			} else if (dec.length() > decimalPlaces) {
				dec = dec.substring(0, decimalPlaces);
			}
			str = arr[0];
			if (arr[0].length() + decimalPlaces < maxLength) {
				str = arr[0] + "." + dec;
			}
			if (str.length() + currancyCode.length() <= maxLength) {
				str = currancyCode + str;
			}

		}
		return str;
	}

	private String getZeros(int i) {
		StringBuffer bf = new StringBuffer();
		while (i > 0) {
			bf.append("0");
			i--;
		}
		return bf.toString();
	}

	// Fix for eticket number that carrier code starting with '0' ie: for RB - 070
	// TODO: eticketnumber need to convert in to String instead of long
	private String correctedEticketNumber(String eticketNumber, String carrierCode) {
		String correctedEticketNumber = eticketNumber;
		if (eticketNumber.length() < 13 && eticketNumber.length() > 10 && !eticketNumber.startsWith(carrierCode)) {
			correctedEticketNumber = carrierCode.concat(eticketNumber.substring(eticketNumber.length() - 10,
					eticketNumber.length()));
		}
		return correctedEticketNumber;
	}

	private Map<Integer, String> getIT04TaxData(List<BSPTransactionTaxData> taxCollection) {
		Map<Integer, String> it04Tax = new HashMap<Integer, String>(4);

		int position = 1;
		BigDecimal totalExtraTax = BigDecimal.ZERO;
		StringBuilder sbXTDataforITO7 = new StringBuilder("XT");
		List<BSPTransactionTaxData> xtTaxs = new ArrayList<BSPTransactionTaxData>();
		for (BSPTransactionTaxData bspTnxTax : taxCollection) {
			if (position <= 3) {
				totalExtraTax = new BigDecimal(bspTnxTax.getAmount());
				String taxRec = bspTnxTax.getTaxCode() + " " + totalExtraTax.toString();
				if (taxRec.length() <= 11) {
					it04Tax.put(position, bspTnxTax.getTaxCode() + " " + totalExtraTax.toString());
				} else {
					xtTaxs.add(bspTnxTax);
				}
				if (position == 3) {
					sbXTDataforITO7.append(" " + bspTnxTax.getCurrancyCode() + bspTnxTax.getAmount() + bspTnxTax.getTaxCode());
				}
				position++;
			} else {
				totalExtraTax = totalExtraTax.add(new BigDecimal(bspTnxTax.getAmount()));
				it04Tax.put(3, "XT" + totalExtraTax);
				if ((new BigDecimal(bspTnxTax.getAmount()).compareTo(BigDecimal.ZERO) != 0)) {
					sbXTDataforITO7.append(" " + bspTnxTax.getCurrancyCode() + "" + bspTnxTax.getAmount()
							+ bspTnxTax.getTaxCode());
					it04Tax.put(4, sbXTDataforITO7.toString());
				}
			}

		}
		// if length exceed 11 it goes to XT
		for (BSPTransactionTaxData xtTax : xtTaxs) {
			totalExtraTax = totalExtraTax.add(new BigDecimal(xtTax.getAmount()));
			it04Tax.put(3, "XT" + totalExtraTax);
			if ((new BigDecimal(xtTax.getAmount()).compareTo(BigDecimal.ZERO) != 0)) {
				sbXTDataforITO7.append(" " + xtTax.getCurrancyCode() + "" + xtTax.getAmount() + xtTax.getTaxCode());
				it04Tax.put(4, sbXTDataforITO7.toString());
			}
		}

		return it04Tax;
	}

	private ArrayList<BSPTransactionTaxData> getIT05TaxData(ArrayList<BSPTransactionTaxData> taxCollection) {
		Map<Integer, String> it04Tax = new HashMap<Integer, String>(3);
		ArrayList<BSPTransactionTaxData> IT05TaxData = new ArrayList<BSPTransactionTaxData>(6);
		int position = 0;
		BigDecimal totalExtraTax = BigDecimal.ZERO;
		for (BSPTransactionTaxData bspTnxTax : taxCollection) {
			if (position < 6) {
				BSPTransactionTaxData taxBox = bspTnxTax;
				IT05TaxData.add(position, taxBox);
				position++;
			} else {
				totalExtraTax = totalExtraTax.add(new BigDecimal(bspTnxTax.getAmount()));
				BSPTransactionTaxData sixthTaxBox = IT05TaxData.get(5);
				sixthTaxBox.setAmount(totalExtraTax.toString());
				sixthTaxBox.setTaxCode("XT");
				IT05TaxData.set(5, sixthTaxBox);
			}

		}
		return IT05TaxData;
	}
}
