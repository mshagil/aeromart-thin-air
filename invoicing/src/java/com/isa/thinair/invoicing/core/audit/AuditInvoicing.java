package com.isa.thinair.invoicing.core.audit;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.invoicing.api.model.InvoiceSettlement;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;

public class AuditInvoicing {

	public static final String SETTLE_INVOICE = "SETTLEINVOICE";

	public static final String VIEW_INVOICE = "VIEWINVOICE";

	public static final String RECORD_PAYMENT = "RECORDPAYMENT";

	public static final String CODE = "code";

	public static final String STATUS = "status";

	public static void doAudit(String code, String type, String userID, LinkedHashMap<String, String> contents)
			throws ModuleException {

		Audit audit = new Audit();

		if (userID != null) {
			audit.setUserId(userID);
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		if (contents == null) {
			contents = new LinkedHashMap<String, String>();
		}

		if (code != null && !code.isEmpty()) {
			contents.put(CODE, code);
		}

		if (type.equals(VIEW_INVOICE)) {
			audit.setTaskCode(String.valueOf(TasksUtil.TA_VIEW_TRAVEL_AGENT_RECIPTS));
		}

		if (type.equals(SETTLE_INVOICE)) {
			audit.setTaskCode(String.valueOf(TasksUtil.TA_SETTLE_TRAVEL_AGENT_INVOICES));
		}

		if (type.equals(RECORD_PAYMENT)) {
			audit.setTaskCode(String.valueOf(TasksUtil.TA_RECORD_TRAVEL_AGENT_PAYMENTS));
		}

		InvoicingModuleUtils.getAuditorBD().audit(audit, contents);

	}

	public static void doInvoicingAudit(String code, String type, String userID, Collection data) throws ModuleException {
		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
		for (Iterator iter = data.iterator(); iter.hasNext();) {
			InvoiceSettlement settlement = (InvoiceSettlement) iter.next();
			contents.put("InvoiceNo", settlement.getInvoiceNo());
			contents.put("AmountPaid", String.valueOf(settlement.getAmountPaid()));
			doAudit(code, type, userID, contents);
		}
	}

}
