/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.invoicing.core.service.bd;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;

/**
 * The class used to Transfer BSP sales summary to queue ("queue/bspQueue")
 * 
 * @author
 */

public class TransferBSPsToQueue extends PlatformBaseServiceDelegate {
	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME = "queue/bspQueue";
	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	/**
	 * The data is transferred to queue and processed from there to external system(X3)
	 * 
	 * @param bspSales
	 */
	public void transferToQueue(Collection bspSales) {
		try {
			sendMessage((ArrayList) bspSales, InvoicingModuleUtils.getInstance().getModuleConfig().getJndiProperties(),
					DESTINATION_JNDI_NAME, CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}
}
