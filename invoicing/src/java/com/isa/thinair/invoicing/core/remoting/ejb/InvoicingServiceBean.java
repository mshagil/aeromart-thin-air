/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.remoting.ejb;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.bl.ExternalAccountingSystem;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.BSPResultsDTO;
import com.isa.thinair.invoicing.api.dto.GenerateInvoiceOption;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceGenerationStatusDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesReceiptsDTO;
import com.isa.thinair.invoicing.api.model.BSPFailedTnx;
import com.isa.thinair.invoicing.api.model.BSPHOTFileLog;
import com.isa.thinair.invoicing.api.model.bsp.BSPReconciliationRecordDTO;
import com.isa.thinair.invoicing.api.model.bsp.ReportFileTypes;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.invoicing.core.audit.AuditInvoicing;
import com.isa.thinair.invoicing.core.bl.InvoiceBL;
import com.isa.thinair.invoicing.core.bl.InvoiceGeneratorBL;
import com.isa.thinair.invoicing.core.bl.InvoiceReceiptsGeneratorBL;
import com.isa.thinair.invoicing.core.bl.TransferInvoiceBL;
import com.isa.thinair.invoicing.core.bl.RapidReportFiles.RETFileGenerationBL;
import com.isa.thinair.invoicing.core.bl.bsp.BSPPublishBL;
import com.isa.thinair.invoicing.core.bl.bsp.BSPReconciliationBL;
import com.isa.thinair.invoicing.core.persistence.dao.BSPLinkJdbcDAO;
import com.isa.thinair.invoicing.core.persistence.dao.ExternalInvoiceTransferDAO;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceDAO;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceJdbcDAO;
import com.isa.thinair.invoicing.core.persistence.dao.TransferInvoiceDAO;
import com.isa.thinair.invoicing.core.persistence.dao.TransferInvoiceJdbcDAO;
import com.isa.thinair.invoicing.core.service.bd.InvoicingServiceDelegateImpl;
import com.isa.thinair.invoicing.core.service.bd.InvoicingServiceLocalDelegateImpl;
import com.isa.thinair.invoicing.core.service.bd.SendInvoicesViaMDB;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

/**
 * @author Chamindap / Byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "InvoicingService.remote")
@LocalBinding(jndiBinding = "InvoicingService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class InvoicingServiceBean extends PlatformBaseSessionBean
		implements InvoicingServiceDelegateImpl, InvoicingServiceLocalDelegateImpl {

	private static final String EMPTY_STRING = "";
	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Called By the MDB. NOTE: One at a time to avoid resending if exception occurs
	 * 
	 * @param generatedInvoicesDTOS
	 * @throws ModuleException
	 */
	@TransactionTimeout(1900)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendInvoiceMail(GeneratedInvoiceDTO generatedInvoicesDTO) throws ModuleException {

		try {
			TransferInvoiceDAO invoiceDAO = InvoicingModuleUtils.getTransferInvoiceDAO();

			boolean emailSent = new TransferInvoiceBL().sendInvoiceMail(generatedInvoicesDTO);
			Collection<String> invoiceNumbers = new ArrayList<String>();
			String invoiceNumber = generatedInvoicesDTO.getInvoiceNumber();
			invoiceNumbers.add(invoiceNumber);
			if (emailSent) {
				invoiceDAO.updateEmailsStatus(InvoicingInternalConstants.EmailStatus.SENT, invoiceNumbers);
			} else {
				invoiceDAO.updateEmailsStatus(InvoicingInternalConstants.EmailStatus.NOT_SENT, invoiceNumbers);
			}
		} catch (CommonsDataAccessException e) {
			log.error("sendInvoiceMail", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (ModuleException e) {
			log.error("sendInvoiceMail", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());
		} catch (Exception e) {
			log.error("sendInvoiceMail", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}

	}

	/**
	 * @NOTE - 1: SENDS THE USER SELECTED (PAGE) INVOICES MDB
	 * @NOTE - 2: Will update the Status to 'PRO'
	 * @param fromDate
	 * @param toDate
	 * @throws ModuleException
	 */
	@TransactionTimeout(1900)
	public void sendInvoiceMail(SearchInvoicesDTO invoicesDTO) throws ModuleException {

		try {

			if (invoicesDTO == null) {
				throw new ModuleException("airtravelagent.param.nullempty", InvoicingConstants.MODULE_NAME);
			}

			if (log.isDebugEnabled()) {
				log.debug("Selected No. Of Invoices :" + invoicesDTO.getAgentCodes() == null
						? "null - > ALL"
						: invoicesDTO.getAgentCodes().size() + " Going to Send to MDB");
			}
			TransferInvoiceBL transferInvoiceBL = new TransferInvoiceBL();
			GeneratedInvoiceDTO genDt = null;
			Collection<String> invoiceNumbers = new ArrayList<String>();
			Collection<GeneratedInvoiceDTO> invoices = transferInvoiceBL.getGeneratedInvoices(invoicesDTO);
			// just updating to process state
			if (invoices != null) {
				Iterator<GeneratedInvoiceDTO> inIter = invoices.iterator();
				while (inIter.hasNext()) {
					genDt = (GeneratedInvoiceDTO) inIter.next();
					invoiceNumbers.add(genDt.getInvoiceNumber());
				}
			}
			updateEmailTransferStatus(invoiceNumbers);
			// send to the MDB to queue
			transferInvoiceBL.sendInvoiceMail(invoices);

		} catch (CommonsDataAccessException e) {
			log.error("sendInvoiceMail", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (ModuleException e) {
			log.error("sendInvoiceMail", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (Exception e) {
			log.error("sendInvoiceMail", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}

	}

	/**
	 * CALLED BY MDB.The method used to insert invoices to external financial system
	 * and updating transfer status in invoice core table
	 * @param invoices
	 * @throws ModuleException
	 */
	public void transferInvoices(Collection invoices) throws ModuleException {
		try {
			if (log.isDebugEnabled()) {
				log.debug("Going to Transfer Invoices");
			}
			String system = EMPTY_STRING;
			Iterator it = invoices.iterator();
			while (it.hasNext()) {
				InvoiceDTO invoice = (InvoiceDTO) it.next();
				system = invoice.getAccountingSystemName();
				if (!EMPTY_STRING.equals(system)) {
					break;
				}
			}
			if (StringUtil.isNullOrEmpty(system)) {
				log.error("Accounting system couldn't find. Please configure it");
				throw new ModuleException("module.dependencymap.invalid");
			} else {
				ExternalAccountingSystem accountingSystem = (ExternalAccountingSystem) BLUtil
						.getExternalAccoutingSystemsImplConfig()
						.getAccountingSystemMap().get(system);
				ExternalInvoiceTransferDAO extinvoiceTransferDAO = (ExternalInvoiceTransferDAO) InvoicingModuleUtils
						.getExternalAccoutingSystemsDAOImplConfig()
						.getAccountingSystemDAOMap().get(system);
				extinvoiceTransferDAO.insertInvoicesToInterfaceTable(invoices,
						(XDBConnectionUtil) accountingSystem);
			}
			Collection<InvoiceDTO> invoicesToUpdate = new ArrayList<InvoiceDTO>();
			Iterator<InvoiceDTO> iterator = invoices.iterator();
			int i = 0;
			while (iterator.hasNext()) {
				i++;
				invoicesToUpdate.add(iterator.next());
				if (i == 10) {
					updateInvoiceTransferStatus(invoicesToUpdate);
					i = 0;
					invoicesToUpdate.clear();
				}
			}
			if (invoicesToUpdate.size() > 0) {
				updateInvoiceTransferStatus(invoicesToUpdate);
			}
			if (log.isDebugEnabled()) {
				log.debug("Successfully Transferred and Updated Statuses of all Invoices");
			}
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferInvoices", e);
			throw new ModuleException(e, e.getExceptionCode(),
					e.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferInvoices", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}
	}

	/**
	 * @param fromDate
	 * @param toDate
	 * @throws ModuleException
	 */
	@TransactionTimeout(1900)
	public void transferToInterfaceTables(SearchInvoicesDTO searchInvoicesDTO) throws ModuleException {

		try {
			new TransferInvoiceBL().transferToInterfaceTables(searchInvoicesDTO);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferInvoiceDetails ", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferInvoiceDetails", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferInvoiceDetails", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}
	}

	/**
	 * Update transfer status of the invoices afer the bulk has been transfered.
	 * 
	 * @param invoices
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateInvoiceTransferStatus(Collection<InvoiceDTO> invoices) throws ModuleException {
		if (invoices == null || invoices.size() <= 0) {
			return;
		}
		Collection<String> invoiceNumbers = new ArrayList<String>();
		Iterator<InvoiceDTO> iterator = invoices.iterator();
		while (iterator.hasNext()) {
			InvoiceDTO invoiceDTO = (InvoiceDTO) iterator.next();
			invoiceNumbers.add(invoiceDTO.getInvoiceNumber());
		}
		TransferInvoiceDAO invoiceDAO = InvoicingModuleUtils.getTransferInvoiceDAO();
		invoiceDAO.updateInvoiceTransferStatus(InvoicingInternalConstants.TransferStatus.TRANSFERED_SUCCESSFULY, invoiceNumbers);
	}

	/**
	 * Update transfer status of the invoices afer the bulk has been transfered.
	 * 
	 * @param invoices
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateEmailTransferStatus(Collection<String> invoiceNumbers) throws ModuleException {
		if (invoiceNumbers == null || invoiceNumbers.size() <= 0) {
			return;
		}
		TransferInvoiceDAO invoiceDAO = InvoicingModuleUtils.getTransferInvoiceDAO();
		invoiceDAO.updateEmailsStatus(InvoicingInternalConstants.EmailStatus.PROCESSING, invoiceNumbers);
	}

	/**
	 * Scheduler Autoinvoice execute This method
	 */
	public void autoEmailInvoices() throws ModuleException {

		TransferInvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getTransferInvoiceJdbcDAO();

		// get auto invoices for current period
		Object[] objs = invoiceJdbcDAO.getAutoInvoicesForPeriod(CalendarUtil.getLastMomentOfPreviousDaY());

		Collection<String> invoiceNumbers = (Collection<String>) objs[0];
		Collection<GeneratedInvoiceDTO> generatedInvoiceDTOs = (Collection) objs[1];

		if (invoiceNumbers == null || invoiceNumbers.size() <= 0 || generatedInvoiceDTOs == null
				|| generatedInvoiceDTOs.size() <= 0) {
			log.error("Invoices Not Found");
			throw new ModuleException("invoicing.auto.failed");
		}
		// This couold cause an stale data exception
		updateEmailTransferStatus(invoiceNumbers);

		if (log.isDebugEnabled()) {
			log.debug("Updated the invoice Statuses to 'Sending'");
		}

		if (generatedInvoiceDTOs != null) {
			for (GeneratedInvoiceDTO generatedInvoiceDTO : generatedInvoiceDTOs) {
				new SendInvoicesViaMDB().sendEmailsViaMDB(generatedInvoiceDTO);
			}
			log.info("############## Completed Auto Invoicing for Agents  ##############");
		}
	}

	/**
	 * Method to retrive all the generated invoices for search criteria.
	 * 
	 * @param SearchInvoicesDTO
	 * @param startIndex
	 * @param maxRecords
	 * @return
	 * @throws ModuleException
	 */
	public InvoiceGenerationStatusDTO getGeneratedInvoicesDetails(SearchInvoicesDTO searchInvoicesDTO, int startIndex,
			int maxRecords) throws ModuleException {

		try {

			InvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getInvoiceJdbcDAO();
			InvoiceGenerationStatusDTO invoiceGenerationStatusDTO = new InvoiceGenerationStatusDTO();

			invoiceGenerationStatusDTO
					.setInvoices(invoiceJdbcDAO.getGeneratedInvoices(searchInvoicesDTO, startIndex, maxRecords));

			invoiceGenerationStatusDTO.setTotalAmountForPeriod(invoiceJdbcDAO.getInvoiceTotal(searchInvoicesDTO));

			if (ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer().equalsIgnoreCase("true")) {

				invoiceGenerationStatusDTO.setHasEXDB(true);
				if (invoiceJdbcDAO.invoiceTransferInProgress(searchInvoicesDTO)) {
					invoiceGenerationStatusDTO.setTotalAmountTransfered(new BigDecimal(-1));
				} else {
					List<String> accountingSystemName = InvoicingModuleUtils.getInvoicingConfig()
							.getInvoiceTransferAccountingSystemList();
					try {
						for (String system : accountingSystemName) {
							if (StringUtil.isNullOrEmpty(system)) {
								log.error("Accounting system couldn't find. Please configure it");
								throw new ModuleException("module.dependencymap.invalid");
							} else {
								ExternalAccountingSystem accountingSystem = (ExternalAccountingSystem) BLUtil
										.getExternalAccoutingSystemsImplConfig().getAccountingSystemMap().get(system);

								// passing accounting system (holding connection data) to below method
								invoiceGenerationStatusDTO.setTotalAmountTransfered(invoiceJdbcDAO.getInvoiceTotalinExternalDB(
										searchInvoicesDTO, (XDBConnectionUtil) accountingSystem, system));
							}
						}
					} catch (Exception ex) {
						log.error("Retrieval of invoice from external system failed", ex);
					}

					// invoiceGenerationStatusDTO.setTotalAmountTransfered(invoiceJdbcDAO
					// .getInvoiceTotalinExternalDB(searchInvoicesDTO));
				}
			} else {
				invoiceGenerationStatusDTO.setHasEXDB(false);
			}
			return invoiceGenerationStatusDTO;

		} catch (CommonsDataAccessException e) {
			log.error("getGeneratedInvoicesDetails", e);
			throw new ModuleException(e, e.getExceptionCode(), InvoicingConstants.MODULE_NAME);
		} catch (Exception e) {
			log.error("getGeneratedInvoicesDetails", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}

	}

	/**
	 * @param critiria
	 */
	public InvoiceSummaryDTO searchInvoiceForAgent(String agentGSACode, String year, String month, int billingPeriod,
			boolean payCurr, String invoiceNo, String pnr,String entity) throws ModuleException {

		return new InvoiceBL().searchInvoiceForAgent(agentGSACode, year, month, billingPeriod, payCurr, invoiceNo, pnr,entity);
	}

	/**
	 * @param critiria
	 */
	public Collection<GeneratedInvoiceDTO> searchGeneratedInvoicesForAgent(String agentGSACode, String dateFrom, String dateTo)
			throws ModuleException {

		return new InvoiceBL().searchGeneratedInvoicesForAgent(agentGSACode, dateFrom, dateTo);
	}

	/**
	 * @param agentCode
	 * @return
	 */
	public Collection getUnsettledInvoicesForAgent(String agentCode) throws ModuleException {
		Collection agentCollections = null;

		try {
			agentCollections = getInvoiceDAO().getUnsettledInvoicesForAgent(agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getUnsettledInvoicesForAgent  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			log.error("getUnsettledInvoicesForAgent failed.", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}

		return agentCollections;
	}

	/**
	 * Is Called By Scheduler Job / And Front End
	 * 
	 * @param fromDate
	 * @param toDate
	 * @throws ModuleException
	 */
	@TransactionTimeout(2800)
	public ServiceResponce generateInvoices(SearchInvoicesDTO searchInvoicesDTO, GenerateInvoiceOption generateInvoiceOption)
			throws ModuleException {

		try {
			return new InvoiceGeneratorBL().generateInvoices(searchInvoicesDTO, generateInvoiceOption);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			log.error("generateInvoices", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (ModuleException e) {
			log.error("generateInvoices", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());
		} catch (Exception e) {
			log.error("generateInvoices", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airtravelagent.agtotsales.notfound");
		}
	}

	/**
	 * Gets InvoiceDAO.
	 * 
	 * @return InvoiceDAO.
	 */
	private InvoiceDAO getInvoiceDAO() {
		return (InvoiceDAO) InvoicingModuleUtils.getDAO(InvoicingModuleUtils.INVOICEDAO);
	}

	/**
	 * @param agentCode
	 * @param settleCollection
	 * @throws ModuleException
	 */
	public void settleInvoice(String agentCode, Collection settleCollection) throws ModuleException {

		InvoiceBL travelAgentFinanceBL = new InvoiceBL();

		try {
			AuditInvoicing.doInvoicingAudit(agentCode, AuditInvoicing.SETTLE_INVOICE, getUserPrincipal().getUserId(),
					settleCollection);
			travelAgentFinanceBL.settleInvoice(agentCode, settleCollection, true);

		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("settleInvoice airtravelagents finance module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			log.error("settleInvoice airtravelagents finance module failed", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("settleInvoice  failed.", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}
	}

	/**
	 * Generate Invoices
	 * 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce generateInvoicesRecipts(SearchInvoicesReceiptsDTO searchInvoicesReceiptsDTO) throws ModuleException {
		new InvoiceReceiptsGeneratorBL().generateInvoicesForPayments(searchInvoicesReceiptsDTO);
		return new DefaultServiceResponse(true);
	}

	/**
	 * @JIRA AARESAA-2363 Method use to generate ON-ACCOUNT Refunds. Support the Ongoing Transactions.(saving Values
	 *       Will be in session scope).
	 */
	public ServiceResponce generateInvoicesReciptsForRefunds(Collection<AgentTotalSaledDTO> agenTotalSaledDTOs,
			SearchInvoicesReceiptsDTO searchInvoicesReceiptsDTO) throws ModuleException {

		try {
			ServiceResponce serviceResponce = new InvoiceReceiptsGeneratorBL().generateRefundInvoices(agenTotalSaledDTOs,
					searchInvoicesReceiptsDTO);

			if (log.isDebugEnabled()) {
				log.debug("===================Generating Transaction Refund Invoice Completed===================");
			}

			return serviceResponce;
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			log.error("generateInvoices", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (ModuleException e) {
			log.error("generateInvoices", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());
		} catch (Exception e) {
			log.error("generateInvoices", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airtravelagent.agtotsales.notfound");
		}
	}

	public ResultSet getInvoiceReportDataSet(Collection<String> invoiceIDs) throws ModuleException {
		return getInvoiceDAO().getInvoiceExportData(invoiceIDs);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(1900)
	public void publishDailySalesToBSP(Date rptPeriodStartDate, Date rptPeriodEndDate, boolean processErrorsOnly,
			String countryCode, String agentCode) throws ModuleException {
		BSPPublishBL bspBl = new BSPPublishBL();
		bspBl.publishDailySales(rptPeriodStartDate, rptPeriodEndDate, this.getUserPrincipal(), processErrorsOnly, countryCode,
				agentCode);
	}

	public Page getBSPReports(int startrec, int numofrecs) throws ModuleException {
		try {
			return InvoicingModuleUtils.getBSPLinkDao().getBSPReports(startrec, numofrecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), InvoicingConstants.MODULE_NAME);
		}
	}

	public BSPFailedTnx getChangingBSPFailedTnx(String bspTxnLogID) throws ModuleException {
		try {
			return InvoicingModuleUtils.getBSPLinkDao().getChangingBSPFailedTnx(bspTxnLogID);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), InvoicingConstants.MODULE_NAME);
		}
	}

	public void saveOrUpdateBSPFailedTransaction(BSPFailedTnx savingBspFailedTnx) throws ModuleException {
		try {
			Collection<BSPFailedTnx> colBSPFailedTnx = new ArrayList<BSPFailedTnx>();
			colBSPFailedTnx.add(savingBspFailedTnx);
			InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateBSPFailedTransactions(colBSPFailedTnx);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), InvoicingConstants.MODULE_NAME);
		}
	}

	public Page getBSPReports(int startrec, int numofrecs, List criteria) throws ModuleException {
		try {
			return InvoicingModuleUtils.getBSPLinkDao().getBSPReports(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), InvoicingConstants.MODULE_NAME);
		}
	}

	@Override
	public Page getBSPHOTFileRecords(int startrec, int numofrecs, List criteria) throws ModuleException {
		try {
			return InvoicingModuleUtils.getBSPLinkDao().getBSPHOTFileRecords(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), InvoicingConstants.MODULE_NAME);
		}
	}

	@Override
	public void saveOrUpdateBSPHOTFile(BSPHOTFileLog hotFile) throws ModuleException {
		try {
			InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateBSPHOTFile(hotFile);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), InvoicingConstants.MODULE_NAME);
		}
	}

	@Override
	public void deleteBSPHOTFile(Integer hotFileLogId) throws ModuleException {
		try {
			InvoicingModuleUtils.getBSPLinkDao().deleteBSPHOTFile(hotFileLogId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), InvoicingConstants.MODULE_NAME);
		}
	}

	@TransactionTimeout(2900)
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void processBSPHOTFile(String hotFileContent, String hotFileName) throws ModuleException {
		new BSPReconciliationBL().parseHOTFile(hotFileContent, this.getUserPrincipal().getUserId(), hotFileName);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<BSPReconciliationRecordDTO> getBSPReconciliationReportData(ReportsSearchCriteria criteria)
			throws ModuleException {
		return new BSPReconciliationBL().getBSPReconciliationReportData(criteria, this.getUserPrincipal());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void uploadPendingRETFiles() throws ModuleException {
		BSPPublishBL bspBl = new BSPPublishBL();
		bspBl.uploadRetFiles();
	}

	@Override
	@TransactionTimeout(2900)
	public void generateReportFile(Date rptPeriodStartDate, Date rptPeriodEndDate, boolean processErrorsOnly,
			ReportFileTypes.FileTypes fileType) throws ModuleException {
		// TODO Auto-generated method stub
		RETFileGenerationBL retFileBl = new RETFileGenerationBL();
		retFileBl.generateReportFile(rptPeriodStartDate, rptPeriodEndDate, this.getUserPrincipal(), processErrorsOnly, fileType);
	}
	
	/**
	 * Method used to transfer data into queue for inserting data into external system(X3).
	 * This method calls transferBSPsToQueue() method in BSPPublishBL class
	 * @param searchBSPsDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(1900)
	public void transferBSPsToQueue(SearchInvoicesDTO searchBSPsDTO) throws ModuleException {

		try {
			BSPPublishBL bspBL = new BSPPublishBL();
			bspBL.transferBSPsToQueue(searchBSPsDTO);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferBSPDetails ", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferBSPDetails", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferInvoiceDetails", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}
		
	}
	
	/**
	 * Update transfer status of the BSP after the bulk has been transfered.
	 * @param bspRecords
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void updateBSPsTransferStatus(Collection<BSPResultsDTO> bspRecords) throws ModuleException {
		if (bspRecords == null || bspRecords.size() <= 0) {
			return;
		}
		Collection<Integer> bspSalesIds = new ArrayList<Integer>();
		Iterator<BSPResultsDTO> iterator = bspRecords.iterator();
		while (iterator.hasNext()) {
			BSPResultsDTO bspResultsDTO = (BSPResultsDTO) iterator.next();
			bspSalesIds.add(bspResultsDTO.getSalesID());
		}
		BSPLinkJdbcDAO bspJdbcDAO = InvoicingModuleUtils.getBSPLinkJdbcDao();
		bspJdbcDAO.updateBSPStatus(InvoicingInternalConstants.TransferStatus.TRANSFERED_SUCCESSFULY, bspSalesIds);
	}
	
	/**
	 * CALLED BY MDB
	 * Transfer the queued data to external table and update the status of internal summary table.
	 * @param bspsToTransfer
	 * 	
	 * @throws ModuleException
	 */
	public void transferBSPsalesToExternalInterface(Collection bspsToTransfer) throws ModuleException {

		try {
			if (log.isDebugEnabled()) {
				log.debug("Going to Transfer BSP sales");
			}
			BSPLinkJdbcDAO bspJdbcDAO = InvoicingModuleUtils.getBSPLinkJdbcDao();
			bspJdbcDAO.insertBSPsToInterfaceTable(bspsToTransfer);
			Collection<BSPResultsDTO> bspsToUpdate = new ArrayList<BSPResultsDTO>();
			Iterator<BSPResultsDTO> iterator = bspsToTransfer.iterator();
			int i = 0;
			
			while (iterator.hasNext()) {
				i++;
				bspsToUpdate.add(iterator.next());
				if (i == 10) {
					updateBSPsTransferStatus(bspsToUpdate);
					i = 0;
					bspsToUpdate.clear();
				}
			}

			if (bspsToUpdate.size() > 0) {
				updateBSPsTransferStatus(bspsToUpdate);
			}

			if (log.isDebugEnabled()) {
				log.debug("Successfully Transferred and Updated Statuses of all BSPs");
			}

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferBSPs", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());

		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("transferBSPs", e);
			throw new ModuleException(e, InvoicingConstants.MODULE_NAME);
		}
	}

}