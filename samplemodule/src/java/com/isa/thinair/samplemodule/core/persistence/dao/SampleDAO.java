/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.samplemodule.core.persistence.dao;

import java.util.List;

import com.isa.thinair.samplemodule.api.model.SampleOjbect;

/**
 * @author Nasly
 * 
 */
public interface SampleDAO {
	public void saveOrUpdate(SampleOjbect obj);

	public void delete(String primaryKey);

	public List getRecords();

	public SampleOjbect getRecord(String id);

	public List search(List criteria);
}
