/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.samplemodule.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.samplemodule.api.service.SampleServiceBD;

/**
 * @author Nasly
 */
@Remote
public interface SampleServiceDelegateImpl extends SampleServiceBD {

}
