package com.isa.thinair.samplemodule.api.service;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class SampleModuleUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("samplemodule");
	}
}
