package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightScheduleDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightScheduleInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightScheduleLegInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightScheduleSegmentInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FrequencyInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.RoutesResultInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ScheduleOriginDestinationInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCScheduleAvailableRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCScheduleAvailableRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRS;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCScheduleAvailabilityBL {

	private static final Log log = LogFactory.getLog(LCCAvailabilitySearchBL.class);

	private LCCScheduleAvailableRQ lccScheduleAvailableRQ = null;

	private LCCScheduleAvailableRS lccScheduleAvailableRS = null;

	private ScheduleAvailRQ scheduleAvailRQ = null;

	private ScheduleAvailRS scheduleAvailRS = null;

	private UserPrincipal userPrincipal;

	private int indexSeq = 1;

	public void setScheduleAvailRQ(ScheduleAvailRQ scheduleAvailRQ) {
		this.scheduleAvailRQ = scheduleAvailRQ;
	}

	public ScheduleAvailRS execute(BasicTrackInfo trackInfo) throws ModuleException {

		try {
			scheduleAvailRS = new ScheduleAvailRS();

			if (log.isDebugEnabled()) {
				log.debug("[" + AppSysParamsUtil.getDefaultCarrierCode() + "-AAServices] has been called by LCC]");
			}

			lccScheduleAvailableRQ = new LCCScheduleAvailableRQ();
			createLCCScheduleAvailRQ(trackInfo);
			lccScheduleAvailableRS = LCConnectorUtils.getMaxicoExposedWS().getAvailableSchedule(lccScheduleAvailableRQ);

			LCCResponseAttributes responseAttributes = lccScheduleAvailableRS.getResponseAttributes();
			if (responseAttributes.getSuccess() == null || responseAttributes.getErrors().size() > 0) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccScheduleAvailableRS.getResponseAttributes()
						.getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}
			indexSeq = 1;
			return createAllFlightsCombinations(lccScheduleAvailableRS);

		} catch (Exception ex) {
			log.error("Error in availability search", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.schedule.invocationError", ex);
			}
		}
	}

	private ScheduleAvailRS createAllFlightsCombinations(LCCScheduleAvailableRS lccScheduleAvailableRS) {
		Collection<Collection<FlightScheduleInfoDTO>> colFlightsInfo = new ArrayList<Collection<FlightScheduleInfoDTO>>();
		Collection<Collection<FlightScheduleInfoDTO>> colRetFlightsInfo = new ArrayList<Collection<FlightScheduleInfoDTO>>();
		List<RoutesResultInfo> rouList = lccScheduleAvailableRS.getRoutesResultInfo();
		List<RoutesResultInfo> rouReturnList = lccScheduleAvailableRS.getRoutesReturnResultInfo();

		populateFlightScheduleInfo(rouList, colFlightsInfo, false);

		if (lccScheduleAvailableRQ.isReturnFlag()) {
			populateFlightScheduleInfo(rouReturnList, colRetFlightsInfo, true);
		}

		scheduleAvailRS.setFlightSchedules(colFlightsInfo);
		scheduleAvailRS.setFlightSchedulesReturns(colRetFlightsInfo);

		return scheduleAvailRS;
	}

	private void populateFlightScheduleInfo(List<RoutesResultInfo> rList,
			Collection<Collection<FlightScheduleInfoDTO>> colFlights, final boolean reverseOrder) {
		Collection<FlightScheduleInfoDTO> colFlightScheduleInfoDTOs = null;
		List<RoutesResultInfo> routeInfoList = null;
		Map<String, List> routesMap = new HashMap<String, List>();

		for (RoutesResultInfo info : rList) {
			List routeList = routesMap.get(info.getRouteId());
			if (routeList == null) {
				routeList = new ArrayList<RoutesResultInfo>();
				routesMap.put(info.getRouteId(), routeList);
			}
			routeList.add(info);
		}

		List<String> removedRouteIDs = checkFlightAvailability(routesMap);
		if (removedRouteIDs != null && !removedRouteIDs.isEmpty()) {
			for (Iterator iterator = removedRouteIDs.iterator(); iterator.hasNext();) {
				String key = (String) iterator.next();
				routesMap.remove(key);
			}

		}

		Set keys = routesMap.keySet();

		if (keys != null && !keys.isEmpty()) {

			for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
				String key = (String) iterator.next();

				routeInfoList = routesMap.get(key);

				// Sorting End
				for (RoutesResultInfo info : routeInfoList) {

					for (FlightScheduleDetails flightScheduleDetails : info.getFlightScheduleDetails()) {

						List<FlightScheduleInfo> fliList = flightScheduleDetails.getFlightScheduleInfo();
						colFlightScheduleInfoDTOs = new ArrayList<FlightScheduleInfoDTO>();

						for (FlightScheduleInfo schedule : fliList) {
							FlightScheduleInfoDTO flightScheduleInfoDTO = new FlightScheduleInfoDTO();
							flightScheduleInfoDTO.setScheduleId(schedule.getScheduleId());
							flightScheduleInfoDTO.setFlightNumber(schedule.getFlightNumber());
							flightScheduleInfoDTO.setStartDate(schedule.getStartDate());
							flightScheduleInfoDTO.setStopDate(schedule.getStopDate());
							flightScheduleInfoDTO.setStartDateLocal(schedule.getStartDateLocal());
							flightScheduleInfoDTO.setStopDateLocal(schedule.getStopDateLocal());

							flightScheduleInfoDTO.setOverlapingScheduleId(schedule.getOverlapingScheduleId());
							flightScheduleInfoDTO.setAvailableSeatKilometers(schedule.getAvailableSeatKilometers());
							flightScheduleInfoDTO.setNumberOfDepartures(schedule.getNumberOfDepartures());

							flightScheduleInfoDTO.setDepartureStnCode(schedule.getDepartureStnCode());
							flightScheduleInfoDTO.setArrivalStnCode(schedule.getArrivalStnCode());

							flightScheduleInfoDTO.setDepartureAptName(schedule.getDepartureAptName());
							flightScheduleInfoDTO.setArrivalAptName(schedule.getArrivalAptName());

							flightScheduleInfoDTO.setModelNumber(schedule.getModelNumber());
							flightScheduleInfoDTO.setOperationTypeId(schedule.getOperationTypeId());
							flightScheduleInfoDTO.setBuildStatusCode(schedule.getBuildStatusCode());
							flightScheduleInfoDTO.setStatusCode(schedule.getStatusCode());
							flightScheduleInfoDTO.setSequenceId(indexSeq);
							/*
							 * flightScheduleInfoDTO.setManuallyChanged(schedule.getManuallyChanged());
							 * 
							 * flightScheduleInfoDTO.setLocalChange(schedule.getLocalChange());
							 * flightScheduleInfoDTO.setGdsIds(schedule.getGdsIds());
							 * flightScheduleInfoDTO.setNumberOfOpenFlights(schedule.getNumberOfFlights());
							 */

							FrequencyInfo frequencyInfo = schedule.getFrequency();
							Frequency frequency = new Frequency();
							frequency.setDay0(frequencyInfo.isDay0());
							frequency.setDay1(frequencyInfo.isDay1());
							frequency.setDay2(frequencyInfo.isDay2());
							frequency.setDay3(frequencyInfo.isDay3());
							frequency.setDay4(frequencyInfo.isDay4());
							frequency.setDay5(frequencyInfo.isDay5());
							frequency.setDay6(frequencyInfo.isDay6());

							flightScheduleInfoDTO.setFrequency(frequency);

							FrequencyInfo frequencyInfoLocal = schedule.getFrequencyLocal();
							Frequency frequencyLocal = new Frequency();
							frequencyLocal.setDay0(frequencyInfoLocal.isDay0());
							frequencyLocal.setDay1(frequencyInfoLocal.isDay1());
							frequencyLocal.setDay2(frequencyInfoLocal.isDay2());
							frequencyLocal.setDay3(frequencyInfoLocal.isDay3());
							frequencyLocal.setDay4(frequencyInfoLocal.isDay4());
							frequencyLocal.setDay5(frequencyInfoLocal.isDay5());
							frequencyLocal.setDay6(frequencyInfoLocal.isDay6());

							flightScheduleInfoDTO.setFrequencyLocal(frequencyLocal);

							Collection<FlightScheduleLegInfo> flightScheduleLegInfos = schedule.getFlightScheduleLegs();
							Set<FlightScheduleLegInfoDTO> flightSet = new HashSet<FlightScheduleLegInfoDTO>();

							for (FlightScheduleLegInfo legs : flightScheduleLegInfos) {
								FlightScheduleLegInfoDTO dto = new FlightScheduleLegInfoDTO();
								dto.setId(legs.getId());
								dto.setScheduleId(legs.getScheduleId());
								dto.setLegNumber(legs.getLegNumber());
								dto.setEstDepartureTimeZulu(legs.getEstDepartureTimeZulu());
								dto.setEstArrivalTimeZulu(legs.getEstArrivalTimeZulu());

								dto.setEstDepartureTimeLocal(legs.getEstDepartureTimeLocal());
								dto.setEstArrivalTimeLocal(legs.getEstArrivalTimeLocal());
								dto.setDuration(legs.getDuration());

								dto.setEstDepartureDayOffset(legs.getEstDepartureDayOffset());
								dto.setEstArrivalDayOffset(legs.getEstArrivalDayOffset());

								dto.setEstDepartureDayOffsetLocal(legs.getEstDepartureDayOffsetLocal());
								dto.setEstArrivalTimeLocal(legs.getEstArrivalTimeLocal());

								dto.setOrigin(legs.getOrigin());
								dto.setDestination(legs.getDestination());
								dto.setModelRouteId(legs.getModelRouteId());

								flightSet.add(dto);
							}

							flightScheduleInfoDTO.setFlightScheduleLegs(flightSet);

							Collection<FlightScheduleSegmentInfo> flightScheduleSegmentInfos = schedule
									.getFlightScheduleSegments();
							Set<FlightScheduleSegmentInfoDTO> segmentDTOs = new HashSet<FlightScheduleSegmentInfoDTO>();

							for (FlightScheduleSegmentInfo segment : flightScheduleSegmentInfos) {
								FlightScheduleSegmentInfoDTO dto = new FlightScheduleSegmentInfoDTO();
								dto.setFlSchSegId(segment.getFlSchSegId());
								dto.setScheduleId(segment.getScheduleId());
								dto.setOverlapSegmentId(segment.getOverlapSegmentId());
								dto.setSegmentCode(segment.getSegmentCode());
								dto.setValidFlag(segment.isValidFlag());

								segmentDTOs.add(dto);
							}

							flightScheduleInfoDTO.setFlightScheduleSegments(segmentDTOs);

							colFlightScheduleInfoDTOs.add(flightScheduleInfoDTO);
							// count++;
						}

						if (colFlightScheduleInfoDTOs != null && colFlightScheduleInfoDTOs.size() > 0)
							colFlights.add(colFlightScheduleInfoDTOs);
					}

				}
				indexSeq++;
			}
		}
	}

	private void createLCCScheduleAvailRQ(BasicTrackInfo trackInfo) throws ModuleException {

		// populate pos detail
		lccScheduleAvailableRQ.setLccPos(LCCClientCommonUtils.createPOS(this.userPrincipal, trackInfo));

		// populate header info
		lccScheduleAvailableRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(scheduleAvailRQ.getTransactionIdentifier()));

		createOriginDestinationInfomationList();
	}

	private void createOriginDestinationInfomationList() {

		lccScheduleAvailableRQ.setReturnFlag(scheduleAvailRQ.isRoundTrip());
		lccScheduleAvailableRQ.setStartDateTime(scheduleAvailRQ.getStartDateTime());
		lccScheduleAvailableRQ.setStopDateTime(scheduleAvailRQ.getStopDateTime());
		lccScheduleAvailableRQ.setAirlineCode(scheduleAvailRQ.getDefaultCarrierCode());

		ScheduleOriginDestinationInfo scheduleOriginDestinationInfo = new ScheduleOriginDestinationInfo();
		// scheduleOriginDestinationInfo.setAirlineCode(scheduleAvailRQ.getDefaultCarrierCode());
		scheduleOriginDestinationInfo.setOriginLocation(scheduleAvailRQ.getOriginLocation());
		scheduleOriginDestinationInfo.setDestinationLocation(scheduleAvailRQ.getDestLocation());
		lccScheduleAvailableRQ.getScheduleOriginDestinationInfo().add(scheduleOriginDestinationInfo);

	}

	private List<String> checkFlightAvailability(Map<String, List> routesMap) {
		List<RoutesResultInfo> routeInfoList = null;
		List<String> removeList = new ArrayList<String>();
		List<String> allRouteIDList = new ArrayList<String>();
		List<String> filledRouteIDList = new ArrayList<String>();
		Set keys = routesMap.keySet();

		if (keys != null && !keys.isEmpty()) {

			for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
				String key = (String) iterator.next();
				routeInfoList = routesMap.get(key);
				for (RoutesResultInfo info : routeInfoList) {
					allRouteIDList.add(info.getRouteId());

					for (FlightScheduleDetails flightScheduleDetails : info.getFlightScheduleDetails()) {
						List<FlightScheduleInfo> fliList = flightScheduleDetails.getFlightScheduleInfo();
						if (fliList == null || fliList.isEmpty())
							removeList.add(info.getRouteId());
						if (fliList != null || !fliList.isEmpty())
							filledRouteIDList.add(info.getRouteId());
					}
				}
			}
			
			for(String routeID:allRouteIDList){
				boolean isfilledID = false;
				for (String filledRouteID : filledRouteIDList) {
					if (routeID.equals(filledRouteID)) {
						isfilledID = true;
					}
				}
								
				if (!isfilledID) {
					removeList.add(routeID);
				}
			}
				 

		}
		return removeList;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}
}
