package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertActionCode;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookedFlightAlertInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCClearAlertRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCClearAlertRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientClearAlertDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientClearAlertBL {

	private static final Log log = LogFactory.getLog(LCCClientClearAlertBL.class);

	public static void clearSegmentAlerts(LCCClientClearAlertDTO lccClientClearAlertDTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("LCCClient clearSegmentAlerts called.");
		}
		try {

			LCCClearAlertRQ lccClearAlertRQ = process(lccClientClearAlertDTO, userPrincipal, trackInfo);

			if (log.isDebugEnabled()) {
				log.debug("Calling LCC raiseOrClearAlert.");
			}

			// Call web service method
			LCCClearAlertRS lccClearAlertRS = LCConnectorUtils.getMaxicoExposedWS().clearSegmentAlerts(lccClearAlertRQ);

			if (log.isDebugEnabled()) {
				log.debug("LCCClient raiseOrClearAlert called.");
			}

			if (lccClearAlertRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccClearAlertRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

		} catch (Exception e) {
			log.error("ERROR ", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
	}

	private static LCCClearAlertRQ process(LCCClientClearAlertDTO lccClientClearAlertDTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) {

		LCCClearAlertRQ lccClearAlertRQ = new LCCClearAlertRQ();

		if (lccClientClearAlertDTO != null) {
			lccClearAlertRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccClearAlertRQ.setAlertAction(getAlertActionCode(lccClientClearAlertDTO.getActionTaken()));
			lccClearAlertRQ.setAlertText(lccClientClearAlertDTO.getActionText());
			lccClearAlertRQ.setPnr(lccClientClearAlertDTO.getPNR());
			Collection<BookedFlightAlertInfo> bookedFlightAlertInfos = new ArrayList<BookedFlightAlertInfo>();
			Map<Integer, String> pnrSegIds = lccClientClearAlertDTO.getPnrSegIds();
			for (Integer pnrSegId : pnrSegIds.keySet()) {
				BookedFlightAlertInfo bookedFlightAlertInfo = new BookedFlightAlertInfo();
				bookedFlightAlertInfo.setFlightRefNumber(pnrSegId.toString());
				bookedFlightAlertInfo.setCarrierCode(pnrSegIds.get(pnrSegId));
				AlertInfo alertInfo = new AlertInfo();
				alertInfo.setContent(lccClientClearAlertDTO.getAlertContent());
				bookedFlightAlertInfo.getAlert().add(alertInfo);
				bookedFlightAlertInfos.add(bookedFlightAlertInfo);
			}
			lccClearAlertRQ.getAlertInfo().addAll(bookedFlightAlertInfos);
		}
		return lccClearAlertRQ;
	}

	private static AlertActionCode getAlertActionCode(String actionTaken) {
		if (actionTaken.equals("1")) {
			return AlertActionCode.ALERT_1_OTHER;
		} else if (actionTaken.equals("2")) {
			return AlertActionCode.ALERT_2_EMAIL;
		} else if (actionTaken.equals("3")) {
			return AlertActionCode.ALERT_3_FAX;
		} else if (actionTaken.equals("4")) {
			return AlertActionCode.ALERT_4_TELEPHONE;
		}
		return null;
	}

}
