package com.isa.thinair.lccclient.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.lccclient.api.service.LCCAlertingBD;

@Remote
public interface LCCAlertingBDRemote extends LCCAlertingBD {

}
