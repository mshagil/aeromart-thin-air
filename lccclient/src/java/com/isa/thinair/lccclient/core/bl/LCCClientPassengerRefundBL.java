/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerRefund;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCErrorCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPassengerRefundRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPassengerRefundRS;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.ServiceResponce;

public class LCCClientPassengerRefundBL {

	private static final Logger LOGGER = Logger.getLogger(LCCClientPassengerRefundBL.class);

	public static boolean refundPassengers(CommonReservationAssembler reservationAssembler, String userNotes,
			UserPrincipal userPrincipal, SessionContext sessionContext, TrackInfoDTO trackInfoDTO,
			Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds) throws ModuleException {

		boolean status = true;
		Collection<LCCClientPaymentInfo> manualReversablePayments = new ArrayList<LCCClientPaymentInfo>();
		LCCClientReservation reservation = reservationAssembler.getLccreservation();
		String groupPnr = reservationAssembler.getLccreservation().getPNR();

		PaymentAssembler paymentAssembler = new PaymentAssembler();
		Collection<Integer> tempTnxIds = new ArrayList<Integer>();

		ReservationContactInfo transformContactInfo = AirProxyReservationUtil.transformContactInfo(reservation.getContactInfo());
		CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
		boolean isReversePayment = false;

		try {

			Set<LCCClientReservationPax> reservationPaxes = reservation.getPassengers();
			for (LCCClientReservationPax reservationPax : reservationPaxes) {
				LCCClientPaymentWorkFlow.setLccUniqueTnxIdsForGroupRefund(reservationPax.getLccClientPaymentAssembler());
				LCCClientPaymentWorkFlow.transform(paymentAssembler, reservationPax.getLccClientPaymentAssembler());
			}

			if (paymentAssembler.getPayments().size() > 0) {

				ReservationBO.getPaymentInfo(paymentAssembler, reservation.getPNR());

				tempTnxIds = (Collection<Integer>) ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(null,
						transformContactInfo, paymentAssembler.getPayments(), credentialsDTO, true, false);

				ServiceResponce response = null;
				try {
					response = LCCClientModuleUtil.getReservationBD().refundPayment(trackInfoDTO, paymentAssembler,
							reservation.getPNR(), transformContactInfo, tempTnxIds);
				} catch (ModuleException mx) {
					isReversePayment = true;
					throw new ModuleException(mx, mx.getExceptionCode(), mx.getModuleCode());
				}

				Collection<PaymentInfo> paymentInfos = (Collection<PaymentInfo>) response
						.getResponseParam(CommandParamNames.PAYMENT_INFO);
				Map<String, String> paymentAuthIdMap = (Map<String, String>) response
						.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
				Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) response
						.getResponseParam(CommandParamNames.PAYMENT_BROKER_REF_MAP);

				// Since we return form other module we need updated list.
				paymentAssembler.getPayments().clear();
				paymentAssembler.getPayments().addAll(paymentInfos);

				// Setting the additional CC information
				for (LCCClientReservationPax reservationPax : reservationPaxes) {
					for (LCCClientPaymentInfo lccClientPaymentInfo : reservationPax.getLccClientPaymentAssembler().getPayments()) {
						if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
							CommonCreditCardPaymentInfo cardPaymentInfo = (CommonCreditCardPaymentInfo) lccClientPaymentInfo;
							String key = LCCReservationUtil.getPaymentKey(cardPaymentInfo);
							cardPaymentInfo.setAuthorizationId(paymentAuthIdMap.get(key));
							cardPaymentInfo.setPaymentBrokerId(paymentBrokerRefMap.get(key));
							manualReversablePayments.add(cardPaymentInfo);
						}
					}
				}
			}

			LCCPassengerRefundRQ refundRQ = new LCCPassengerRefundRQ();
			refundRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
			refundRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(reservationAssembler.getLccreservation().getVersion()));

			refundRQ.setGroupPnr(groupPnr);
			for (LCCClientReservationPax reservationPax : reservationPaxes) {
				PassengerRefund refund = new PassengerRefund();
				refund.setTravelerRefNumber(reservationPax.getTravelerRefNumber());
				refund.getPaymentDetails().addAll(
						getPaymentDetails(reservationPax.getLccClientPaymentAssembler(), userNotes, reservation.getPNR()));
				if (preferredRefundOrder != null) {
					refund.getPreferredRefundOrder().addAll(preferredRefundOrder);
				}
				if (pnrPaxOndChgIds != null) {
					refund.getPnrPaxOndChgIds().addAll(pnrPaxOndChgIds);
				}

				refundRQ.getRefund().add(refund);
			}

			LCCPassengerRefundRS refundRS = LCConnectorUtils.getMaxicoExposedWS().refundPassenger(refundRQ);

			if (refundRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = refundRS.getResponseAttributes().getErrors().get(0);
				if (lccError.getErrorCode().compareTo(LCCErrorCode.ERR_68_MAXICO_EXPOSED_NO_REFUNDABLE_AMOUNTS) == 0) {
					throw new ModuleException("lccclient.refundable.payments.not.found");
				}

				if (lccError.getErrorCode().compareTo(LCCErrorCode.ERR_69_MAXICO_EXPOSED_CAN_NOT_REFUND_MORE_THAN_PAID) == 0) {
					throw new ModuleException("lccclient.refund.more.than.paid");
				}

				status = false;
			} else {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Refund data captured successfully.");
				}
				LCCClientModuleUtil.getReservationBD().updateTempPaymentEntry(tempTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, null, null, null, null);
			}
		} catch (Exception ex) {

			LOGGER.error("Error in book", ex);

			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, "Refund fail for reservation", null, null, null);

			sessionContext.setRollbackOnly();
			status = false;

			if (!isReversePayment) {
				LCCClientPaymentWorkFlow.reversePayments(groupPnr, manualReversablePayments);
			}
			LOGGER.error("Reversable payments reversed.");
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}

		return status;
	}

	private static List<PaymentDetails> getPaymentDetails(LCCClientPaymentAssembler lccClientPaymentAssembler, String userNotes,
			String pnr) throws ModuleException {
		List<PaymentDetails> paymentDetailsList = new ArrayList<PaymentDetails>();
		for (LCCClientPaymentInfo clientPaymentInfo : lccClientPaymentAssembler.getPayments()) {
			PaymentDetails paymentDetails = LCCClientPaymentUtil.transform(clientPaymentInfo, pnr);
			paymentDetails.setRemarks(userNotes);
			paymentDetailsList.add(paymentDetails);
		}

		return paymentDetailsList;
	}

}
