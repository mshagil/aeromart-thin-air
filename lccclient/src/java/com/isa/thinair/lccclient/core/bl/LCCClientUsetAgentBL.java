package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCInvalidateAASessionsForUserRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCInvalidateAASessionsForUserRS;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientUsetAgentBL {

	private static Log log = LogFactory.getLog(LCCClientUsetAgentBL.class);

	public static void invalidateAAServiceSessionAcrossCarriers(String userId, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		try {
			LCCInvalidateAASessionsForUserRQ invalidateAASessionsForUserRQ = new LCCInvalidateAASessionsForUserRQ();
			invalidateAASessionsForUserRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			invalidateAASessionsForUserRQ.setUserId(userId);

			LCCInvalidateAASessionsForUserRS invalidateAASessionsForUserRS = LCConnectorUtils.getMaxicoExposedWS()
					.invalidateAASessionsForUser(invalidateAASessionsForUserRQ);

			if (invalidateAASessionsForUserRS.getResponseAttributes().getSuccess() == null) {
				log.error("Error occurred when removing the AASession for the user in other carriers calling invalidateAASessionsForUser WS method.");
				throw new ModuleException("lccclient.reservation.invocationError");
			}

		} catch (Exception ex) {
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				log.error("lccclient.user.aasession.invalidation.error", ex);
				throw new ModuleException("lccclient.user.aasession.invalidation.error", ex);
			}
		}
	}
}
