package com.isa.thinair.lccclient.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.lccclient.api.service.LCCPaxCreditBD;

@Remote
public interface LCCPaxCreditBDRemote extends LCCPaxCreditBD {

}
