package com.isa.thinair.lccclient.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.lccclient.api.service.LCCAncillaryBD;

@Local
public interface LCCAncillaryBDLocal extends LCCAncillaryBD {

}
