package com.isa.thinair.lccclient.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.lccclient.api.service.LCCUserAgentSeviceBD;
import com.isa.thinair.lccclient.core.bl.LCCClientUsetAgentBL;
import com.isa.thinair.lccclient.core.service.bd.LCCUserAgentSeviceBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCUserAgentSeviceBDRemote;

@Stateless
@RemoteBinding(jndiBinding = LCCUserAgentSeviceBD.SERVICE_NAME + ".remote")
@LocalBinding(jndiBinding = LCCUserAgentSeviceBD.SERVICE_NAME + ".local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCUserAgentSeviceBean extends PlatformBaseSessionBean implements LCCUserAgentSeviceBDLocal,
		LCCUserAgentSeviceBDRemote {

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void invalidateAAServiceSessionAcrossCarriers(String userId, BasicTrackInfo trackInfo) throws ModuleException {
		LCCClientUsetAgentBL.invalidateAAServiceSessionAcrossCarriers(userId, this.getUserPrincipal(), trackInfo);
	}

}
