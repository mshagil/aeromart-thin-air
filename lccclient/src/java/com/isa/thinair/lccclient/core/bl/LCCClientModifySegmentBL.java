package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AdminInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservationSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredJourneyDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxExternalCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialReqDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingMethod;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.MODIFY_MODES;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.api.util.ETicketUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Nilindra Fernando
 * @since 3:57 PM 11/6/2009
 */
public class LCCClientModifySegmentBL {

	private static Log log = LogFactory.getLog(LCCClientModifySegmentBL.class);

	private static void checkModifySegmentConstraints(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO)
			throws ModuleException {
		MODIFY_MODES modifyModes = lccClientResAlterQueryModesTO.getModifyModes();

		if (modifyModes != MODIFY_MODES.MODIFY_OND) {
			throw new ModuleException("lccclient.invalid.modify.segment.request");
		}

		String groupPNR = PlatformUtiltiies.nullHandler(lccClientResAlterQueryModesTO.getGroupPNR());

		if (groupPNR.length() == 0) {
			throw new ModuleException("lccclient.reservation.empty.group.pnr");
		}

		if (lccClientResAlterQueryModesTO.getOldPnrSegs() == null || lccClientResAlterQueryModesTO.getOldPnrSegs().size() == 0) {
			throw new ModuleException("lccclient.segment.references.empty");
		}

		if (lccClientResAlterQueryModesTO.getLccClientSegmentAssembler() == null
				|| lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getSegmentsMap().size() == 0) {
			throw new ModuleException("lccclient.segment.references.empty");
		}
	}

	public static LCCClientReservation execute(LCCClientResAlterModesTO lccClientResAlterModesTO, UserPrincipal userPrincipal,
			SessionContext sessionContext, TrackInfoDTO trackInfoDTO) throws ModuleException {

		String groupPNR = lccClientResAlterModesTO.getGroupPNR();
		PaymentAssembler paymentAssembler = new PaymentAssembler();
		LCCClientSegmentAssembler lccClientSegmentAssembler = lccClientResAlterModesTO.getLccClientSegmentAssembler();
		ReservationContactInfo transformContactInfo = AirProxyReservationUtil.transformContactInfo(lccClientSegmentAssembler
				.getContactInfo());
		Collection<Integer> tempTnxIds = new ArrayList<Integer>();
		CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
		// If payment failed in the capture payment itself, we have to skip the reversal in again.
		// following boolean will be used for that. Until we capture the payment successfully and return back we don't
		// need to perform reversal
		boolean performReversal = false;
		boolean payFullPayment = false;

		try {
			checkModifySegmentConstraints(lccClientResAlterModesTO);

			LCCClientPaymentWorkFlow.setLccUniqueTnxId(lccClientSegmentAssembler.getPassengerPaymentsMap().values());
			for (LCCClientPaymentAssembler clientPaymentAsm : lccClientSegmentAssembler.getPassengerPaymentsMap().values()) {
				LCCClientPaymentWorkFlow.transform(paymentAssembler, clientPaymentAsm);
			}

			boolean bspPayment = LCCReservationUtil.isBSPpayment(paymentAssembler.getPayments());

			Map<Integer, CardPaymentInfo> tempPaymentMap = LCCClientPaymentWorkFlow
					.populateTemPaymentEntries(lccClientSegmentAssembler.getTemporyPaymentMap());

			if (paymentAssembler.getPayments().size() > 0) {
				ReservationBO.getPaymentInfo(paymentAssembler, groupPNR);
				// Make the temporary payment entry
				tempTnxIds = (Collection<Integer>) ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
						tempPaymentMap, transformContactInfo, paymentAssembler.getPayments(), credentialsDTO, true, false);

				ServiceResponce response = LCCClientModuleUtil.getReservationBD().makePayment(trackInfoDTO, paymentAssembler,
						groupPNR, transformContactInfo, tempTnxIds);
				performReversal = true;

				Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) response
						.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
				Collection<PaymentInfo> paymentInfos = (Collection<PaymentInfo>) response
						.getResponseParam(CommandParamNames.PAYMENT_INFO);
				Map<String, String> paymentAuthIdMap = (Map<String, String>) response
						.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
				Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) response
						.getResponseParam(CommandParamNames.PAYMENT_BROKER_REF_MAP);

				// Since we return form other module we need updated list.
				paymentAssembler.getPayments().clear();
				paymentAssembler.getPayments().addAll(paymentInfos);
				LCCReservationUtil.combineActualCreditPayments(lccClientSegmentAssembler.getPassengerPaymentsMap(),
						paxCreditPayments, paymentAuthIdMap, paymentBrokerRefMap);
			}

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterModesTO,
					LCCModificationTypeCode.MODTYPE_6_MODIFY_OND, userPrincipal, trackInfoDTO);
			lccAirBookModifyRQ.setAppIndicator(lccClientResAlterModesTO.getAppIndicator().toString());
			lccAirBookModifyRQ.setUserNote(lccClientResAlterModesTO.getUserNotes());

			// Setting the external charges
			lccAirBookModifyRQ.getLccReservation().getAirReservation()
					.setPriceInfo(LCCReservationUtil.parsePriceInfo(lccClientSegmentAssembler.getPassengerPaymentsMap()));

			addSpecialRequestDetails(lccAirBookModifyRQ.getLccReservation(), lccClientSegmentAssembler);

			boolean isFirstPayment = false;

			if (lccClientResAlterModesTO.getPaymentTypes() != null
					&& lccClientResAlterModesTO.getPaymentTypes().intValue() == ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT
							.intValue()) {
				isFirstPayment = lccClientResAlterModesTO.getReservationStatus().equals(
						com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus.ON_HOLD);
				composeFulFillments(lccAirBookModifyRQ, lccClientResAlterModesTO, userPrincipal, isFirstPayment);
				payFullPayment = true;
			}

			// add passenger e ticket details to the modify request
			if (lccClientResAlterModesTO.getReservationStatus().equals(
					com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus.CONFIRMED)
					|| payFullPayment) {
				// Generate E tickets
				if (lccClientResAlterModesTO.getReservationStatus().equals(
						com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus.ON_HOLD)
						|| isOndChange(lccClientResAlterModesTO)) {
					lccAirBookModifyRQ.setTicketingMethod(TicketingMethod.CREATE);
					List<LCCClientReservationSegment> allLccSegments = new ArrayList<LCCClientReservationSegment>();
					List<LCCClientReservationSegment> unflownLccSegments = new ArrayList<LCCClientReservationSegment>();
					// add all existing segments
					allLccSegments.addAll(lccClientResAlterModesTO.getExistingAllSegs());
					// add new segments
					allLccSegments.addAll(lccClientSegmentAssembler.getSegmentsMap().values());

					// get unflown segments . new segments , remove old segment and Cancel segments
					unflownLccSegments.addAll(allLccSegments);
					unflownLccSegments.removeAll(lccClientResAlterModesTO.getOldPnrSegs());

					unflownLccSegments = ETicketUtil.getUnflownSegments(unflownLccSegments);

					CreateTicketInfoDTO ticketInfoDto = ETicketUtil.createTicketingInfoDto(trackInfoDTO);
					ticketInfoDto.setBSPPayment(bspPayment);

					Map<Integer, Map<Integer, EticketDTO>> passengerEtickets = ETicketUtil.generateIATAETicketNumbers(
							lccClientResAlterModesTO.getPassengers(), unflownLccSegments, ticketInfoDto);
					Iterator<LCCClientReservationPax> passngerIterator = lccClientResAlterModesTO.getPassengers().iterator();
					while (passngerIterator.hasNext()) {
						LCCClientReservationPax lcclientReservationPax = passngerIterator.next();
						AirTraveler airTraveler = Transformer.transform(lcclientReservationPax, lccAirBookModifyRQ
								.getLccReservation().getAirReservation().getTravelerInfo());
						if (passengerEtickets.keySet().contains(lcclientReservationPax.getPaxSequence())) {
							airTraveler.getETickets().addAll(
									ETicketUtil.createPaxEticket(allLccSegments, lcclientReservationPax.getPaxSequence(),
											passengerEtickets.get(lcclientReservationPax.getPaxSequence())));
						}
					}
				} else {
					lccAirBookModifyRQ.setTicketingMethod(TicketingMethod.EXCHANGE);
				}
			}

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			LCCClientReservation lccRes = Transformer.transform(lccAirBookRS, tempTnxIds);
			LCCClientModuleUtil.getReservationBD().updateTempPaymentEntry(tempTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, null, null, null, null);
			return lccRes;
		} catch (Exception ex) {
			log.error("Error in modify segment", ex);
			if (performReversal) {
				sessionContext.setRollbackOnly();
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE,
						"Payment failed for modify segment", null, null, null);
				LCCClientPaymentWorkFlow.extractCreditCardPayments(paymentAssembler);
				if (!paymentAssembler.getPayments().isEmpty()) {
					LCCClientModuleUtil.getReservationBD().refundPayment(trackInfoDTO, paymentAssembler, groupPNR,
							transformContactInfo, tempTnxIds);
					ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS,
							"Undo success for modify segment", null, null, null);
				}
			}
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	/**
	 * @param lccClientResAlterModesTO
	 * @param newSegments
	 * @return
	 */
	private static boolean isOndChange(LCCClientResAlterModesTO lccClientResAlterModesTO) throws ModuleException {

		List<LCCClientReservationSegment> oldSegmentCol = new ArrayList<LCCClientReservationSegment>(
				lccClientResAlterModesTO.getOldPnrSegs());
		List<LCCClientReservationSegment> newSegmentCol = new ArrayList<LCCClientReservationSegment>(lccClientResAlterModesTO
				.getLccClientSegmentAssembler().getSegmentsMap().values());

		Collections.sort(oldSegmentCol);
		Collections.sort(newSegmentCol);

		String oldOND = buildOnd(oldSegmentCol);
		String newOND = buildOnd(newSegmentCol);

		return !oldOND.equals(newOND);
	}

	/**
	 * @param lccSegments
	 * @return
	 */
	private static String buildOnd(List<LCCClientReservationSegment> lccSegments) {

		String ond = "";
		for (LCCClientReservationSegment segment : lccSegments) {
			if (!segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				ond += segment.getSegmentCode();
				ond += segment.getCarrierCode();
			}
		}
		return ond;
	}

	private static void addSpecialRequestDetails(LCCReservation lccReservation,
			LCCClientSegmentAssembler lccClientSegmentAssembler) {
		TravelerInfo travelerInfo = null;

		if (lccReservation.getAirReservation().getTravelerInfo() == null) {
			travelerInfo = new TravelerInfo();
		} else {
			travelerInfo = lccReservation.getAirReservation().getTravelerInfo();
		}

		SpecialReqDetails specialReqDetails = new SpecialReqDetails();
		List<InsuranceRequest> insuranceRequests = transform(lccClientSegmentAssembler.getResInsurances());
		if (insuranceRequests != null && !insuranceRequests.isEmpty()) {
			specialReqDetails.getInsuranceRequests().addAll(insuranceRequests);
		}
		populateSSRFromExternalCharges(lccClientSegmentAssembler.getPassengerExtChargeMap(), specialReqDetails);

		travelerInfo.setSpecialReqDetails(specialReqDetails);
		lccReservation.getAirReservation().setTravelerInfo(travelerInfo);
	}

	private static void populateSSRFromExternalCharges(Map<Integer, List<LCCClientExternalChgDTO>> passengerExtChargeMap,
			SpecialReqDetails specialReqDetails) {
		for (Entry<Integer, List<LCCClientExternalChgDTO>> entry : passengerExtChargeMap.entrySet()) {
			Integer travelerRef = entry.getKey();
			List<LCCClientExternalChgDTO> extChargeList = entry.getValue();

			for (LCCClientExternalChgDTO extChgDTO : extChargeList) {
				if (EXTERNAL_CHARGES.AIRPORT_SERVICE.equals(extChgDTO.getExternalCharges())) {
					AirportServiceRequest airportServiceRequest = new AirportServiceRequest();
					airportServiceRequest.setSsrCode(extChgDTO.getCode());
					airportServiceRequest.setServiceCharge(extChgDTO.getAmount());
					airportServiceRequest.setAirportCode(extChgDTO.getAirportCode());
					airportServiceRequest.setCarrierCode(extChgDTO.getCarrierCode());
					airportServiceRequest.setFlightRefNumber(extChgDTO.getFlightRefNumber());
					airportServiceRequest.setTravelerRefNumber(travelerRef.toString() + "$");
					airportServiceRequest.setApplyOn(extChgDTO.getApplyOn());

					specialReqDetails.getAirportServiceRequests().add(airportServiceRequest);

				} else if (EXTERNAL_CHARGES.INFLIGHT_SERVICES.equals(extChgDTO.getExternalCharges())) {
					SpecialServiceRequest ssr = new SpecialServiceRequest();
					ssr.setCarrierCode(extChgDTO.getCarrierCode());
					ssr.setCharge(extChgDTO.getAmount());
					ssr.setFlightRefNumber(extChgDTO.getFlightRefNumber());
					ssr.setSsrCode(extChgDTO.getCode());
					ssr.setTravelerRefNumber(travelerRef.toString() + "$");

					specialReqDetails.getSpecialServiceRequests().add(ssr);
				}
			}

		}
	}

	private static List<InsuranceRequest> transform(List<LCCClientReservationInsurance> resInsurances) {

		List<InsuranceRequest> insuranceRQs = new ArrayList<InsuranceRequest>();
		InsuranceRequest insuranceRQ = null;
		if (resInsurances != null && !resInsurances.isEmpty()) {
			for (LCCClientReservationInsurance resInsurance : resInsurances) {
				insuranceRQ = new InsuranceRequest();
				insuranceRQ.setAmount(resInsurance.getQuotedTotalPremium());
				insuranceRQ.setDestination(resInsurance.getDestination());
				insuranceRQ.setOrigin(resInsurance.getOrigin());
				insuranceRQ.setOperatingCarrierCode(resInsurance.getOperatingAirline());
				insuranceRQ.setDateOfTravel(resInsurance.getDateOfTravel());
				insuranceRQ.setDateOfReturn(resInsurance.getDateOfReturn());
				insuranceRQ.setInsuranceRefNumber(Integer.parseInt(resInsurance.getInsuranceQuoteRefNumber()));
				insuranceRQ.setApplicablePaxCount(resInsurance.getApplicablePaxCount());
				insuranceRQ.setSsrFeeCode(resInsurance.getSsrFeeCode());
				insuranceRQ.setPlanCode(resInsurance.getPlanCode());

				InsuredJourneyDetails insuredJourney = new InsuredJourneyDetails();
				insuredJourney.setJourneyStartAirportCode(resInsurance.getInsuredJourneyDTO().getJourneyStartAirportCode());
				insuredJourney.setJourneyEndAirportCode(resInsurance.getInsuredJourneyDTO().getJourneyEndAirportCode());
				insuredJourney.setJourneyStartDate(resInsurance.getInsuredJourneyDTO().getJourneyStartDate());
				insuredJourney.setJourneyEndDate(resInsurance.getInsuredJourneyDTO().getJourneyEndDate());
				insuredJourney.setJourneyStartDateOffset(resInsurance.getInsuredJourneyDTO().getJourneyStartDateOffset());
				insuredJourney.setJourneyEndDateOffset(resInsurance.getInsuredJourneyDTO().getJourneyEndDateOffset());
				insuredJourney.setRoundTrip(resInsurance.getInsuredJourneyDTO().isRoundTrip());

				insuranceRQ.setInsuredJourneyDetails(insuredJourney);
				insuranceRQs.add(insuranceRQ);
			}

		}
		return insuranceRQs;
	}

	private static void composeFulFillments(LCCAirBookModifyRQ lccAirBookModifyRQ,
			LCCClientResAlterModesTO lccClientResAlterModesTO, UserPrincipal userPrincipal, boolean isFirstPayment)
			throws ModuleException {
		LCCClientSegmentAssembler lccClientSegmentAssembler = lccClientResAlterModesTO.getLccClientSegmentAssembler();
		if (lccClientSegmentAssembler.getPassengerPaymentsMap().size() > 0) {
			// Fulfillment
			Fulfillment fulfillment = new Fulfillment();
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setFulfillment(fulfillment);
			fulfillment.getCustomerFulfillments().addAll(
					LCCReservationUtil.parseTravelerFulfillments(lccClientSegmentAssembler.getPassengerPaymentsMap(),
							userPrincipal, lccClientResAlterModesTO.getGroupPNR(), false, isFirstPayment));
		}
	}

	public static ReservationBalanceTO query(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {

		try {
			checkModifySegmentConstraints(lccClientResAlterQueryModesTO);

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterQueryModesTO,
					LCCModificationTypeCode.MODTYPE_BALANCES_2_FOR_MODIFY_OND, userPrincipal, trackInfo);

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyResQuery(lccAirBookModifyRQ);
			return Transformer.transformBalances(lccAirBookRS);
		} catch (Exception ex) {
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	private static LCCAirBookModifyRQ process(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			LCCModificationTypeCode lccModificationTypeCode, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {

		LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
		lccAirBookModifyRQ.setLoadDataOptions(LCCClientCommonUtils.getDefaultLoadOptions());
		lccAirBookModifyRQ.setModificationTypeCode(lccModificationTypeCode);
		lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(lccClientResAlterQueryModesTO
				.getLccClientSegmentAssembler().getLccTransactionIdentifier()));

		LCCReservation lccReservation = composeAirReservation(lccClientResAlterQueryModesTO.getGroupPNR(),
				lccClientResAlterQueryModesTO.getVersion(), lccClientResAlterQueryModesTO.getReservationStatus(),
				lccClientResAlterQueryModesTO.getOldPnrSegs(), lccClientResAlterQueryModesTO.getExistingAllSegs(),
				lccClientResAlterQueryModesTO.getAnciOfferTemplates());

		if (lccClientResAlterQueryModesTO.getLccClientSegmentAssembler() != null) {
			setAirTravelRefType(lccReservation, lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
					.getPassengerPaymentsMap());
		}

		AirReservationSummary airs = new AirReservationSummary();
		airs.setAdminInfo(new AdminInfo());
		airs.getAdminInfo().setOwnerAgentCode(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getOwnerAgent());
		lccReservation.getAirReservation().setAirReservationSummary(airs);
		lccReservation.getAirReservation().setContactInfo(
				LCCClientApiUtils.transform(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getContactInfo()));
		lccAirBookModifyRQ.setLccReservation(lccReservation);
		lccAirBookModifyRQ.getPerPaxExtChargeMap().addAll(
				composePassengerExternalChargeMap(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
						.getPassengerExtChargeMap(), new Date()));

		LCCReservation newLccReservation = composeAirReservation(lccClientResAlterQueryModesTO.getGroupPNR(),
				lccClientResAlterQueryModesTO.getVersion(), lccClientResAlterQueryModesTO.getReservationStatus(),
				lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getSegmentsMap().values(), null,
				lccClientResAlterQueryModesTO.getAnciOfferTemplates());
		lccAirBookModifyRQ.setNewLCCReservation(newLccReservation);

		// passing the charge override params to the lcc
		ChargerOverride chargerOverride = LCCReservationUtil.createChargeOverride(lccClientResAlterQueryModesTO);
		lccAirBookModifyRQ.setChargeOverride(chargerOverride);

		lccAirBookModifyRQ.setAutoCancellationEnabled(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
				.isAutoCancellationEnabled());

		lccAirBookModifyRQ.setServiceTaxRatio(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getServiceTaxRatio());
		lccAirBookModifyRQ.setAutoRefundEnabled(AppSysParamsUtil.isAgentCreditRefundEnabled());
		lccAirBookModifyRQ.getAutoRefundEnabledAgentTypes().addAll(AppSysParamsUtil.getAgentTypesWhichAgentCreditRefundEnabled());

		return lccAirBookModifyRQ;
	}

	/*
	 * This method set traveler seq & traveler type to AirTraveler Object. This is used in identifying actual pax type
	 * for airport service charge calculation
	 */
	private static void setAirTravelRefType(LCCReservation lccReservation,
			Map<Integer, LCCClientPaymentAssembler> paymentAssemblerMap) {
		if (paymentAssemblerMap != null) {
			for (Entry<Integer, LCCClientPaymentAssembler> entry : paymentAssemblerMap.entrySet()) {
				Integer PaxSeq = entry.getKey();

				AirTraveler airTraveler = new AirTraveler();
				airTraveler.setTravelerRefNumber(PaxSeq.toString());
				airTraveler.setTravelerSequence(PaxSeq);
				if (entry.getValue() != null) {
					airTraveler.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(entry.getValue().getPaxType()));
				}

				if (lccReservation.getAirReservation().getTravelerInfo() == null) {
					lccReservation.getAirReservation().setTravelerInfo(new TravelerInfo());
				}

				lccReservation.getAirReservation().getTravelerInfo().getAirTraveler().add(airTraveler);

			}
		}
	}

	private static List<PerPaxExternalCharges> composePassengerExternalChargeMap(
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap, Date date) {
		List<PerPaxExternalCharges> ppExtChgList = new ArrayList<PerPaxExternalCharges>();
		for (Integer seqNo : paxExtChgMap.keySet()) {
			List<ExternalCharge> externalChargeList = new ArrayList<ExternalCharge>();
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : paxExtChgMap.get(seqNo)) {
				ExternalCharge externalCharge = new ExternalCharge();
				externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
				externalCharge.setApplicableDateTime(date);
				externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

				if (lccClientExternalChgDTO.getCarrierCode() == null) {
					externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				} else {
					externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
				}
				externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
				externalCharge.setCode(lccClientExternalChgDTO.getCode());
				externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
				externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
				externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
				externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
				externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());

				externalChargeList.add(externalCharge);
			}
			PerPaxExternalCharges ppExtCharge = new PerPaxExternalCharges();
			ppExtCharge.setPaxSequence(seqNo);
			ppExtCharge.getExternalCharges().addAll(externalChargeList);
			ppExtChgList.add(ppExtCharge);
		}
		return ppExtChgList;
	}

	private static LCCReservation composeAirReservation(String groupPNR, String version, String status,
			Collection<LCCClientReservationSegment> colLCCReservationSegments,
			Collection<LCCClientReservationSegment> colExistingAllSegs,
			Map<String, Map<EXTERNAL_CHARGES, Integer>> selectedAnciOfferTemplateIDs) {
		LCCReservation lccReservation = new LCCReservation();
		lccReservation.setGroupPnr(groupPNR);

		AirReservation airReservation = new AirReservation();
		lccReservation.setAirReservation(airReservation);

		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();
		OriginDestinationOption originDestinationOption = new OriginDestinationOption();

		if (colLCCReservationSegments != null) {
			for (LCCClientReservationSegment lccClientReservationSegment : colLCCReservationSegments) {
				originDestinationOption.getFlightSegment().add(Transformer.transform(lccClientReservationSegment));

				Map<EXTERNAL_CHARGES, Integer> selecteOfferTemplates = selectedAnciOfferTemplateIDs
						.get(lccClientReservationSegment.getAnciOfferFlightSegmentRefNumber());

				if (selecteOfferTemplates != null && !selecteOfferTemplates.isEmpty()) {

					StringStringIntegerMap selTemplateIDs = new StringStringIntegerMap();
					selTemplateIDs.setKey(lccClientReservationSegment.getAnciOfferFlightSegmentRefNumber());

					for (Entry<EXTERNAL_CHARGES, Integer> selectedTemplateIDEntry : selecteOfferTemplates.entrySet()) {
						StringIntegerMap offerTemplateIDEntry = new StringIntegerMap();
						offerTemplateIDEntry.setKey(selectedTemplateIDEntry.getKey().toString());
						offerTemplateIDEntry.setValue(selectedTemplateIDEntry.getValue());

						selTemplateIDs.getValue().add(offerTemplateIDEntry);
					}
					originDestinationOption.getSegWiseSelectedAnciOfferTemplateIDs().add(selTemplateIDs);
				}
			}
		}
		originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);
		airReservation.setAirItinerary(originDestinationOptions);

		OriginDestinationOptions originDestinationOptionsAll = new OriginDestinationOptions();
		OriginDestinationOption originDestinationOptionAll = new OriginDestinationOption();
		if (colExistingAllSegs != null) {
			for (LCCClientReservationSegment lccClientReservationSegment : colExistingAllSegs) {
				originDestinationOptionAll.getFlightSegment().add(Transformer.transform(lccClientReservationSegment));
			}
		}
		originDestinationOptionsAll.getOriginDestinationOption().add(originDestinationOptionAll);
		airReservation.setExistingAirItinerary(originDestinationOptionsAll);

		airReservation.setVersion(version);
		airReservation.setStatus(status);
		return lccReservation;
	}

}
