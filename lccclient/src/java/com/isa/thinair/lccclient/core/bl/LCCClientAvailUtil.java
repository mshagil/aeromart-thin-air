package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AdditionalFareInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTravelerAvail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AvailPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ClassOfServicePreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.DoubleIntegerListMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerBooleanMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCServiceTaxContainer;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LogicalCabinSeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ModifyPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OldFareDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationInformation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerListMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SystemType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfoSummary;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ClassOfServicePreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.lccclient.core.util.LCCAncillaryUtil;

public class LCCClientAvailUtil {

	public static List<OriginDestinationInformation>
			createOriginDestinationInfomationList(Collection<OriginDestinationInformationTO> ondTOList, boolean isFareQuote) {

		ArrayList<OriginDestinationInformation> ondInfoList = new ArrayList<OriginDestinationInformation>();
		int ondSequence = 0;

		// populate origin destination list
		for (OriginDestinationInformationTO ondTO : ondTOList) {
			OriginDestinationInformation ondInfo = new OriginDestinationInformation();

			ondInfo.setDepartureDateTimeStart(ondTO.getDepartureDateTimeStart());
			ondInfo.setDepartureDateTimeEnd(ondTO.getDepartureDateTimeEnd());
			ondInfo.setPreferredDate(ondTO.getPreferredDate());
			ondInfo.setOriginLocation(ondTO.getOrigin());
			ondInfo.setDestinationLocation(ondTO.getDestination());
			ondInfo.setReturnFlag(ondTO.isReturnFlag());
			ondInfo.setArrivalDateTimeStart(ondTO.getArrivalDateTimeStart());
			ondInfo.setArrivalDateTimeEnd(ondTO.getArrivalDateTimeEnd());
			ondInfo.setPreferredCabinClass(ondTO.getPreferredClassOfService());
			ondInfo.setPreferredLogicalCabin(ondTO.getPreferredLogicalCabin());
			ondInfo.setPreferredBookingType(ondTO.getPreferredBookingType());
			ondInfo.setPreferredBookingClass(ondTO.getPreferredBookingClass());
			ondInfo.getOndSegmentBookingClasses()
					.addAll(Transformer.convertToLccStringMap(ondTO.getSegmentBookingClassSelection()));
			ondInfo.getExistingFlightSegIds().addAll(ondTO.getExistingFlightSegIds());
			ondInfo.getExistingPnrSegIds().addAll(ondTO.getExistingPnrSegRPHs());
			ondInfo.setFlownOnd(ondTO.isFlownOnd());
			ondInfo.setUnTouchedOnd(ondTO.isUnTouchedOnd());
			if (ondTO.getDateChangedResSegList() != null && ondTO.getDateChangedResSegList().size() > 0) {
				ondInfo.getDateChangedResSegList().addAll(ondTO.getDateChangedResSegList());
			}

			ondInfo.setOndSequence(ondSequence);
			if (ondTO.getUnTouchedResSegList() != null) {
				ondInfo.getUnTouchedResSegList().addAll(ondTO.getUnTouchedResSegList());
			}

			if (ondTO.getOldPerPaxFareTOList() != null) {
				List<FareTO> oldPerPaxFareTOList = ondTO.getOldPerPaxFareTOList();
				List<OldFareDetails> oldFareDetailsList = new ArrayList<OldFareDetails>();
				for (FareTO fareTO : oldPerPaxFareTOList) {
					if (fareTO != null) {
						OldFareDetails oldFareDetails = new OldFareDetails();
						if (fareTO.getFareType() != null) {
							oldFareDetails.setOldFareType(Integer.parseInt(fareTO.getFareType()));
						}
						oldFareDetails.setCarrierCode(fareTO.getCarrierCode());
						oldFareDetails.setOldFareId(fareTO.getFareId());
						oldFareDetails.setOldFareAmount(fareTO.getAmount());
						oldFareDetails.setFlightSegId(fareTO.getAppliedFlightSegId());
						oldFareDetails.setLogicalCCCode(fareTO.getLogicalCCCode());
						oldFareDetails.setOndCode(fareTO.getSegmentCode());
						oldFareDetails.setBulkTicketFareRule(fareTO.isBulkTicketFareRule());
						oldFareDetailsList.add(oldFareDetails);
					}
				}
				ondInfo.getOldFareDetails().addAll(oldFareDetailsList);
			}

			ondInfo.setPreferredBundledFarePeriodId(ondTO.getPreferredBundleFarePeriodId());

			if (ondTO.getHubTimeDetailMap() != null) {
				List<StringIntegerMap> hubTimeDetails = new ArrayList<StringIntegerMap>();

				for (Entry<String, Integer> hubTimeEntry : ondTO.getHubTimeDetailMap().entrySet()) {
					StringIntegerMap hubTimeResult = new StringIntegerMap();
					hubTimeResult.setKey(hubTimeEntry.getKey());
					hubTimeResult.setValue(hubTimeEntry.getValue());
					hubTimeDetails.add(hubTimeResult);
				}
				ondInfo.getHubTimeDetailMap().addAll(hubTimeDetails);
			}

			// if fare quote request add flight segments.
			if (isFareQuote || ondTO.isSpecificFlightsAvailability()) {

				OriginDestinationOption ondOption = new OriginDestinationOption();
				for (FlightSegmentTO flightSegmentTO : ondTO.getOrignDestinationOptions().get(0).getFlightSegmentList()) {
					FlightSegment flightSegment = new FlightSegment();
					flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
					flightSegment.setRouteRefNumber(flightSegmentTO.getRouteRefNumber());
					flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
					flightSegment.setOperatingAirline(flightSegmentTO.getOperatingAirline());
					flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
					flightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
					flightSegment.setDepatureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
					flightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
					flightSegment.setSubJourneyGroup(flightSegmentTO.getSubJourneyGroup());
					ondOption.getFlightSegment().add(flightSegment);
				}
				OriginDestinationOptions ondOptions = new OriginDestinationOptions();
				ondOptions.getOriginDestinationOption().add(ondOption);
				ondInfo.setOriginDestinationOptions(ondOptions);
			}
			ondInfo.setSpecificFlightsAvailability(ondTO.isSpecificFlightsAvailability());
			ondInfo.setDepartureCitySearch(ondTO.isDepartureCitySearch());
			ondInfo.setArrivalCitySearch(ondTO.isArrivalCitySearch());
			ondInfoList.add(ondInfo);
			ondSequence++;
		}
		return ondInfoList;
	}

	public static TravelerInfoSummary createTravelerInfoSummary(TravelerInfoSummaryTO travelerInfoSummaryTO) {
		TravelerInfoSummary infoSummary = new TravelerInfoSummary();
		AirTravelerAvail tarvlerAvail = new AirTravelerAvail();
		infoSummary.setAirTravelerAvail(tarvlerAvail);

		for (PassengerTypeQuantityTO paxQuantityTO : travelerInfoSummaryTO.getPassengerTypeQuantityList()) {
			PassengerTypeQuantity paxQuantity = new PassengerTypeQuantity();
			paxQuantity.setPassengerType(LCCClientCommonUtils.getLCCPaxTypeCode(paxQuantityTO.getPassengerType()));
			paxQuantity.setQuantity(paxQuantityTO.getQuantity());
			tarvlerAvail.getPassengerTypeQuantity().add(paxQuantity);
		}
		return infoSummary;
	}

	public static TravelPreferences createTravelPreferences(TravelPreferencesTO travelPreferencesTO) {
		TravelPreferences travelPreferences = new TravelPreferences();
		travelPreferences.setBookingType(getBookingType(travelPreferencesTO.getBookingType()));
		travelPreferences.setOpenReturn(travelPreferencesTO.isOpenReturn());
		return travelPreferences;
	}

	public static BookingType getBookingType(String bType) {
		BookingType lccBType = BookingType.NORMAL;
		if (BookingClass.BookingClassType.STANDBY.equals(bType)) {
			lccBType = BookingType.STANDBY;
		} else if (BookingClass.BookingClassType.NORMAL.equals(bType)) {
			lccBType = BookingType.NORMAL;
		} else if (BookingClass.BookingClassType.OPEN_RETURN.equals(bType)) {
			lccBType = BookingType.OPEN_RETURN;
		}
		return lccBType;
	}

	public static AvailPreferences createAvailreferences(BaseAvailRQ flightAvailRQ) {
		AvailPreferencesTO availPreferencesTO = flightAvailRQ.getAvailPreferences();

		AvailPreferences availPreferences = new AvailPreferences();
		availPreferences.setRestrictionLevel(availPreferencesTO.getRestrictionLevel());

		if (availPreferencesTO.getParticipatingCarriers() != null && availPreferencesTO.getParticipatingCarriers().size() > 0) {
			availPreferences.getParticipatingCarriers().addAll(availPreferencesTO.getParticipatingCarriers());
		}
		availPreferences.setIncludeHalfReturn(availPreferencesTO.isHalfReturnFareQuote());
		// availPreferences.setInboundOnd(availPreferencesTO.isInboundFareModified());
		availPreferences.setStayOverTimeInMillis(availPreferencesTO.getStayOverTimeInMillis());
		availPreferences.setAppIndicator(availPreferencesTO.getAppIndicator().toString());
		availPreferences.setModifyBooking(availPreferencesTO.isModifyBooking());
		availPreferences.setAddSegment(availPreferencesTO.isAddSegment());
		availPreferences.setQuoteFares(availPreferencesTO.isQuoteFares());
		availPreferences.setRequoteFlightSearch(availPreferencesTO.isRequoteFlightSearch());
		availPreferences.setMulticitySearch(availPreferencesTO.isMultiCitySearch());
		availPreferences.setPnr(availPreferencesTO.getPnr());
		availPreferences.setFromNameChange(availPreferencesTO.isFromNameChange());
		availPreferences.setAllowOverrideSameOrHigherFareMod(availPreferencesTO.isAllowOverrideSameOrHigherFareMod());
		availPreferences.setPreserveOndOrder(availPreferencesTO.isPreserveOndOrder());
		availPreferences.setIncludeHRTFares(availPreferencesTO.isIncludeHRTFares());
		availPreferences.setFareCalendarSearch(availPreferencesTO.isFareCalendarSearch());

		boolean flownOndExist = false;
		List<String> pnrSegRPHs = new ArrayList<String>();
		List<Date> modOndDepDateList = new ArrayList<Date>();

		for (OriginDestinationInformationTO ondInfoTO : flightAvailRQ.getOriginDestinationInformationList()) {
			pnrSegRPHs.addAll(ondInfoTO.getExistingPnrSegRPHs());
			flownOndExist = flownOndExist || ondInfoTO.isFlownOnd();

			if (!ondInfoTO.isFlownOnd()
					&& (ondInfoTO.getExistingFlightSegIds() == null || ondInfoTO.getExistingFlightSegIds().size() == 0)
					&& ondInfoTO.getPreferredDate() != null) {
				modOndDepDateList.add(ondInfoTO.getPreferredDate());
			}
		}

		if (!availPreferencesTO.isSkipValidityFQ() && AppSysParamsUtil.isModifyFQOnInitialSegmentFQDate()) {

			if (flightAvailRQ.getTravelPreferences().isOpenReturnConfirm()) {
				if (availPreferencesTO.getModifiedSegmentsList() != null
						&& availPreferencesTO.getModifiedSegmentsList().size() > 0) {
					Date maxValidDate = null;
					Date lastFQDate = null;
					for (FlightSegmentDTO segment : availPreferencesTO.getModifiedSegmentsList()) {
						lastFQDate = segment.getLastFareQuotedDate();
						maxValidDate = segment.getTicketValidTill();
						break;
					}
					if (maxValidDate == null) {
						maxValidDate = getMaxValidDate(availPreferencesTO);
					}
					if (maxValidDate != null
					// && maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())
					) {
						availPreferences.setLastFareQuotedDate(lastFQDate);
						availPreferences.setFQOnLastFQDate(true);
						if (maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())) {
							availPreferences.setFQWithinValidity(true);
						}
					}
				}
			} else if (flownOndExist) {
				Date maxValidDate = availPreferencesTO.getTicketValidTill();
				Date lastFQDate = availPreferencesTO.getLastFareQuotedDate();
				if (maxValidDate != null && lastFQDate != null
				// && maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())
				) {
					availPreferences.setLastFareQuotedDate(lastFQDate);
					availPreferences.setFQOnLastFQDate(true);
					if (maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())) {
						availPreferences.setFQWithinValidity(true);
					}

					// Calculate Penalty when extend ticket validity
					if (modOndDepDateList != null && modOndDepDateList.size() > 0 && availPreferences.isFQWithinValidity()) {

						Collections.sort(modOndDepDateList);

						Date modFirstDepDate = modOndDepDateList.get(0);

						if (modFirstDepDate.after(maxValidDate)) {
							availPreferences.setFQWithinValidity(false);
						}

					}

				}
			}

		}

		if (availPreferencesTO.getQuoteOndFlexi() != null && !availPreferencesTO.getQuoteOndFlexi().isEmpty()) {
			for (Entry<Integer, Boolean> entry : availPreferencesTO.getQuoteOndFlexi().entrySet()) {
				IntegerBooleanMap mapObj = new IntegerBooleanMap();
				mapObj.setKey(entry.getKey());
				mapObj.setValue(entry.getValue());
				availPreferences.getQuoteOndFlexi().add(mapObj);
			}
		}

		if (availPreferencesTO.getOndFlexiSelected() != null && !availPreferencesTO.getOndFlexiSelected().isEmpty()) {
			for (Entry<Integer, Boolean> entry : availPreferencesTO.getOndFlexiSelected().entrySet()) {
				IntegerBooleanMap mapObj = new IntegerBooleanMap();
				mapObj.setKey(entry.getKey());
				mapObj.setValue(entry.getValue());
				availPreferences.getOndFlexiSelected().add(mapObj);
			}
		}

		availPreferences.setPromoCode(availPreferencesTO.getPromoCode());
		availPreferences.setBankIdNumber(availPreferencesTO.getBankIdentificationNumber());
		availPreferences.setPrefLanguage(availPreferencesTO.getPreferredLanguage());
		availPreferences.setPointOfSale(availPreferencesTO.getPointOfSale());

		ModifyPreferences modifyPref = new ModifyPreferences();
		modifyPref.setFlightsPerOndRestriction(availPreferencesTO.getFlightsPerOndRestriction());
		modifyPref.setOldFareAmount(availPreferencesTO.getOldFareAmount());
		modifyPref.setOldFareId(availPreferencesTO.getOldFareID());
		modifyPref.setOldFareType(availPreferencesTO.getOldFareType());
		modifyPref.setCarrierCode(availPreferencesTO.getOldFareCarrierCode());
		modifyPref.setInboundModified(availPreferencesTO.isInboundFareModified());

		List<FlightSegment> existingSegList = new ArrayList<FlightSegment>();

		if (availPreferencesTO.getExistingSegmentsList() != null) {
			for (FlightSegmentDTO fsDTO : availPreferencesTO.getExistingSegmentsList()) {
				existingSegList.add(populateFlightSegment(fsDTO));
			}
		}

		List<FlightSegment> modifiedSegList = new ArrayList<FlightSegment>();

		if (availPreferencesTO.getModifiedSegmentsList() != null) {
			for (FlightSegmentDTO fsDTO : availPreferencesTO.getModifiedSegmentsList()) {
				modifiedSegList.add(populateFlightSegment(fsDTO));
			}
		}

		List<FlightSegment> inverseSegList = new ArrayList<FlightSegment>();

		if (availPreferencesTO.getInverseSegmentsList() != null) {
			for (FlightSegmentDTO fsDTO : availPreferencesTO.getInverseSegmentsList()) {
				inverseSegList.add(populateFlightSegment(fsDTO));
			}
		}

		modifyPref.getExistingSegments().addAll(existingSegList);
		modifyPref.getModifiedSegmentsList().addAll(modifiedSegList);
		modifyPref.getInverseSegmentsList().addAll(inverseSegList);
		if (!modifiedSegList.isEmpty() && isDryModification(modifiedSegList)) {
			modifyPref.setInverseFareId(availPreferencesTO.getInverseFareID());
		}
		availPreferences.setModifyPref(modifyPref);

		List<DoubleIntegerListMap> flightDateFareList = new ArrayList<DoubleIntegerListMap>();
		for (Entry<Double, List<Integer>> entry : availPreferencesTO.getFlightDateFareMap().entrySet()) {
			DoubleIntegerListMap flightDateFareMap = new DoubleIntegerListMap();
			flightDateFareMap.setKey(entry.getKey());
			flightDateFareMap.getValue().addAll(entry.getValue());
			flightDateFareList.add(flightDateFareMap);
		}
		availPreferences.getFlightDateFareMap().addAll(flightDateFareList);

		return availPreferences;
	}

	public static Date getMaxValidDate(AvailPreferencesTO availPreferences) {
		int validMonths = AppSysParamsUtil.getDefaultTicketValidityPeriodInMonths();
		Date firstDepDate = null;
		for (FlightSegmentDTO fltSeg : availPreferences.getModifiedSegmentsList()) {
			if (firstDepDate == null || firstDepDate.after(fltSeg.getDepartureDateTimeZulu())) {
				firstDepDate = fltSeg.getDepartureDateTimeZulu();
			}
		}
		if (firstDepDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(firstDepDate.getTime());
			cal.add(Calendar.MONTH, validMonths);
			return cal.getTime();
		}
		return null;
	}

	public static boolean isDryModification(List<FlightSegment> modifiedSegList) {

		boolean dryModification = true;
		String operatingCarrier = null;
		for (FlightSegment fs : modifiedSegList) {
			if (operatingCarrier == null) {
				operatingCarrier = fs.getOperatingAirline();
				continue;
			}
			if (!fs.getOperatingAirline().equals(operatingCarrier)) {
				dryModification = false;
				break;
			}
		}
		return dryModification;
	}

	public static ClassOfServicePreferences createClassOfServicePreferences(ClassOfServicePreferencesTO cosPreferencesTO) {
		ClassOfServicePreferences cosPreferences = null;
		if (cosPreferencesTO != null) {
			cosPreferences = new ClassOfServicePreferences();

			Map<String, List<Integer>> cabinClassSelectionMap = cosPreferencesTO.getCabinClassSelection();
			if (cabinClassSelectionMap != null) {
				for (Entry<String, List<Integer>> entry : cabinClassSelectionMap.entrySet()) {
					StringIntegerListMap strIntListMap = new StringIntegerListMap();
					strIntListMap.setKey(entry.getKey());
					if (entry.getValue() != null) {
						strIntListMap.getValue().addAll(entry.getValue());
					}
					cosPreferences.getCabinClassSelection().add(strIntListMap);
				}
			}

			Map<String, List<Integer>> logicalCabinClassSelectionMap = cosPreferencesTO.getLogicalCabinClassSelection();
			if (logicalCabinClassSelectionMap != null) {
				for (Entry<String, List<Integer>> entry : logicalCabinClassSelectionMap.entrySet()) {
					StringIntegerListMap strIntListMap = new StringIntegerListMap();
					strIntListMap.setKey(entry.getKey());
					if (entry.getValue() != null) {
						strIntListMap.getValue().addAll(entry.getValue());
					}
					cosPreferences.getLogicalCabinClassSelection().add(strIntListMap);
				}
			}

		}
		return cosPreferences;
	}

	public static FlightSegment populateFlightSegment(FlightSegmentTO fsTO) {

		FlightSegment fSeg = new FlightSegment();
		fSeg.setFlightId(fsTO.getFlightId() == null ? 0 : fsTO.getFlightId());
		fSeg.setSegmentCode(fsTO.getSegmentCode());
		fSeg.setDepatureDateTime(fsTO.getDepartureDateTime());
		fSeg.setArrivalDateTime(fsTO.getArrivalDateTime());
		fSeg.setDepatureDateTimeZulu(fsTO.getDepartureDateTimeZulu());
		fSeg.setArrivalDateTimeZulu(fsTO.getArrivalDateTimeZulu());
		fSeg.setFlightNumber(fsTO.getFlightNumber());
		fSeg.setFlightRefNumber(fsTO.getFlightRefNumber());

		return fSeg;
	}

	// Convert FlightSegmentDTO to FlightSegment
	public static FlightSegment populateFlightSegment(FlightSegmentDTO fsDTO) {
		FlightSegment fSeg = new FlightSegment();
		fSeg.setFlightId(fsDTO.getFlightId() == null ? 0 : fsDTO.getFlightId());
		fSeg.setSegmentCode(fsDTO.getSegmentCode());
		fSeg.setDepartureAirportCode(fsDTO.getFromAirport());
		fSeg.setArrivalAirportCode(fsDTO.getToAirport());
		fSeg.setDepatureDateTime(fsDTO.getDepartureDateTime());
		fSeg.setArrivalDateTime(fsDTO.getArrivalDateTime());
		fSeg.setDepatureDateTimeZulu(fsDTO.getDepartureDateTimeZulu());
		fSeg.setArrivalDateTimeZulu(fsDTO.getArrivalDateTimeZulu());
		fSeg.setFlightNumber(fsDTO.getFlightNumber());
		fSeg.setSegmentId(fsDTO.getSegmentId());
		fSeg.setOperationType(fsDTO.getOperationTypeID() == null ? 0 : fsDTO.getOperationTypeID());
		fSeg.setFlightStatus(fsDTO.getFlightStatus());
		fSeg.setOperatingAirline(fsDTO.getOperatingCarrier());
		return fSeg;
	}

	/**
	 * @param transformSegFareOptions
	 *            boolean denoting whether to transform segment fare options or not.
	 */
	public static Object[] createOriginDestinationInfomation(List<OriginDestinationInformation> ondInfoList, int channelCode,
			boolean transformSegFareOptions) {

		List<OriginDestinationInformationTO> ondOptionResultList = new ArrayList<OriginDestinationInformationTO>();
		Boolean selectedFltReset = false;
		// setting the ond information list
		for (OriginDestinationInformation ondInfo : ondInfoList) {

			OriginDestinationInformationTO ondInfoTO = new OriginDestinationInformationTO();
			ondInfoTO.setOrigin(ondInfo.getOriginLocation());
			ondInfoTO.setDestination(ondInfo.getDestinationLocation());
			ondInfoTO.setReturnFlag(ondInfo.isReturnFlag());
			ondInfoTO.setPreferredDate(ondInfo.getPreferredDate());
			ondInfoTO.setPreferredClassOfService(ondInfo.getPreferredCabinClass());
			ondInfoTO.setPreferredLogicalCabin(ondInfo.getPreferredLogicalCabin());
			ondInfoTO.setPreferredBookingType(ondInfo.getPreferredBookingType());
			ondInfoTO.setPreferredBookingClass(ondInfo.getPreferredBookingClass());
			ondInfoTO.setArrivalDateTimeStart(ondInfo.getArrivalDateTimeStart());
			ondInfoTO.setArrivalDateTimeEnd(ondInfo.getArrivalDateTimeEnd());
			ondInfoTO.setDepartureDateTimeStart(ondInfo.getDepartureDateTimeStart());
			ondInfoTO.setDepartureDateTimeEnd(ondInfo.getDepartureDateTimeEnd());
			ondInfoTO.getExistingFlightSegIds().addAll(ondInfo.getExistingFlightSegIds());
			ondInfoTO.getExistingPnrSegRPHs().addAll(ondInfo.getExistingPnrSegIds());
			ondInfoTO.setSpecificFlightsAvailability(ondInfo.isSpecificFlightsAvailability());
			ondInfoTO.setPreferredBundleFarePeriodId(ondInfo.getPreferredBundledFarePeriodId());
			ondOptionResultList.add(ondInfoTO);

			if (ondInfo.getHubTimeDetailMap() != null) {
				Map<String, Integer> hubTimeDetailMap = new HashMap<String, Integer>();
				for (StringIntegerMap hubTimeEntry : ondInfo.getHubTimeDetailMap()) {
					hubTimeDetailMap.put(hubTimeEntry.getKey(), hubTimeEntry.getValue());
				}
				ondInfoTO.setHubTimeDetailMap(hubTimeDetailMap);
			}

			if (ondInfo.getOriginDestinationOptions() != null && (!transformSegFareOptions
					|| (transformSegFareOptions && ondInfo.getOriginDestinationSegFareOptions() != null))) {
				List<OriginDestinationOption> ondOptionList = transformSegFareOptions
						? ondInfo.getOriginDestinationSegFareOptions().getOriginDestinationOption()
						: ondInfo.getOriginDestinationOptions().getOriginDestinationOption();

				ondOp: for (OriginDestinationOption ondOption : ondOptionList) {
					OriginDestinationOptionTO ondOptionTO = new OriginDestinationOptionTO();
					boolean isFownOrUntouched = (ondInfo.isUnTouchedOnd() || ondInfo.isFlownOnd()) ? true : false;
					for (FlightSegment flightSegment : ondOption.getFlightSegment()) {
						if (!isFownOrUntouched && !withingMktCarrierFltBufferTime(flightSegment.getDepatureDateTimeZulu(),
								channelCode, flightSegment.getBookingType(), flightSegment.getFlightType())) {
							if (ondOption.isSelectedOption()) {
								selectedFltReset = true;
							}
							continue ondOp;
						}
						FlightSegmentTO segmentTO = new FlightSegmentTO();
						segmentTO.setDepartureDateTime(flightSegment.getDepatureDateTime());
						segmentTO.setArrivalDateTime(flightSegment.getArrivalDateTime());
						segmentTO.setFlightId(flightSegment.getFlightId());
						segmentTO.setFlightNumber(flightSegment.getFlightNumber());
						segmentTO.setFlightRefNumber(flightSegment.getFlightRefNumber());
						segmentTO.setRouteRefNumber(flightSegment.getRouteRefNumber());
						segmentTO.setFlightSegId(
								FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber()));
						segmentTO.setSegmentCode(flightSegment.getSegmentCode());
						segmentTO.setOperationType(flightSegment.getOperationType());
						segmentTO.setOperatingAirline(flightSegment.getOperatingAirline());
						segmentTO.setCabinClassCode(flightSegment.getCabinClassCode());
						segmentTO.setReturnFlag(ondInfo.isReturnFlag());
						segmentTO.setDepartureDateTimeZulu(flightSegment.getDepatureDateTimeZulu());
						segmentTO.setArrivalDateTimeZulu(flightSegment.getArrivalDateTimeZulu());
						segmentTO.setSeatAvailCount(flightSegment.getSeatAvailCount());
						segmentTO.setSubJourneyGroup(flightSegment.getSubJourneyGroup());
						segmentTO.setFlightModelDescription(flightSegment.getFltModelDesc());
						segmentTO.setFlightDuration(flightSegment.getFlightDuration());

						if (ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(flightSegment.getReturnFlag())) {
							segmentTO.setReturnFlag(true);
						}

						if (flightSegment.getAirTravelerAvail() != null) {
							for (PassengerTypeQuantity paxQuantity : flightSegment.getAirTravelerAvail()
									.getPassengerTypeQuantity()) {

								// PassengerTypeQuantityTO paxQuantityTO = new PassengerTypeQuantityTO();
								// paxQuantityTO.setPassengerType();
								// paxQuantityTO.setQuantity(paxQuantity.getQuantity());
								// segmentTO.getPassengerTypeQuantityList().add(paxQuantityTO);
								String paxType = LCCClientCommonUtils
										.convertLCCPaxCodeToAAPaxCodes(paxQuantity.getPassengerType());
								if (PaxTypeTO.INFANT.equals(paxType)) {
									segmentTO.setInfantCount(paxQuantity.getQuantity());
								} else if (PaxTypeTO.ADULT.equals(paxType)) {
									segmentTO.setAdultCount(paxQuantity.getQuantity());
								} else if (PaxTypeTO.CHILD.equals(paxType)) {
									segmentTO.setChildCount(paxQuantity.getQuantity());
								}
							}
						}

						if (flightSegment.getAvailableAdultCountMap() != null
								&& flightSegment.getAvailableAdultCountMap().size() > 0) {
							for (StringIntegerMap hubTimeEntry : flightSegment.getAvailableAdultCountMap()) {
								segmentTO.getAvailableAdultCountMap().put(hubTimeEntry.getKey(), hubTimeEntry.getValue());
							}
						} else {
							updatePaxCountLogicalCCWise(flightSegment, segmentTO, PaxTypeTO.ADULT);
						}

						if (flightSegment.getAvailableInfantCountMap() != null
								&& flightSegment.getAvailableInfantCountMap().size() > 0) {
							for (StringIntegerMap hubTimeEntry : flightSegment.getAvailableInfantCountMap()) {
								segmentTO.getAvailableInfantCountMap().put(hubTimeEntry.getKey(), hubTimeEntry.getValue());
							}
						} else {
							updatePaxCountLogicalCCWise(flightSegment, segmentTO, PaxTypeTO.INFANT);
						}

						ondOptionTO.getFlightSegmentList().add(segmentTO);
					}
					Collections.sort(ondOptionTO.getFlightSegmentList());
					ondOptionTO.setSystem(transformSystem(ondOption.getSystemType()));
					if (transformSegFareOptions) {
						ondOptionTO.setSelected(true);
					} else {
						ondOptionTO.setSelected(ondOption.isSelectedOption());
					}
					ondOptionTO.setSeatAvailable(ondOption.isSeatAvailable());

					List<FlightFareSummaryTO> fareSummaryList = new ArrayList<FlightFareSummaryTO>();
					for (AdditionalFareInfo fareInfo : ondOption.getAdditionalFareInfo()) {
						FlightFareSummaryTO fareSummaryTO = new FlightFareSummaryTO();
						BigDecimal totalBaseFare = fareInfo.getAdultFare();
						if (fareInfo.isFlexiAvailable() && fareInfo.getFlexiCharge() != null) {
							totalBaseFare = AccelAeroCalculator.add(totalBaseFare, fareInfo.getFlexiCharge());
						}
						if (fareInfo.getBundledFarePeriodId()!= null && fareInfo.getBundledFareFee() != null) {
							totalBaseFare = AccelAeroCalculator.add(totalBaseFare, fareInfo.getBundledFareFee());
						}
						fareSummaryTO.setBaseFareAmount(totalBaseFare);
						fareSummaryTO.setTotalPrice(AccelAeroCalculator.add(totalBaseFare, fareInfo.getAdultCharges(), fareInfo.getServiceTaxAmountForAdult()));

						fareSummaryTO.setSeatAvailable(fareInfo.isSeatAvailable());
						fareSummaryTO.setLogicalCCCode(fareInfo.getLogicalCabinClass());
						fareSummaryTO.setLogicalCCDesc(fareInfo.getLogicalCabinClassDescription());
						fareSummaryTO.setSelected(fareInfo.isSelected());
						fareSummaryTO.setWithFlexi(fareInfo.isWithFlexi());
						fareSummaryTO.setServiceTaxes(fareInfo.getServiceTaxAmountForAdult());
						fareSummaryTO.setServiceTaxPercentageForFares(fareInfo.getServiceTaxPercentageForFares());
						if (fareInfo.isWithFlexi()) {
							fareSummaryTO.setFlexiRuleId(fareInfo.getFlexiRuleID());
						}
						fareSummaryTO.setFreeFlexi(fareInfo.isIsFreeFlexi());
						fareSummaryTO.setComment(fareInfo.getComment());
						fareSummaryTO.setVisibleChannelName(fareInfo.getVisibleChannelName());
						fareSummaryTO.setRank(fareInfo.getNestRank());
						int noOfAvailableSeats = fareInfo.getAvailableNoOfSeats();
						if (noOfAvailableSeats <= AppSysParamsUtil.getMinimumSeatsCutOverToShowInIbe()) {
							fareSummaryTO.setNoOfAvailableSeats(fareInfo.getAvailableNoOfSeats());
						}
						fareSummaryTO.setBundledFarePeriodId(fareInfo.getBundledFarePeriodId());
						fareSummaryTO.setBundleFareFee(fareInfo.getBundledFareFee());
						fareSummaryTO.setBookingCodes(new HashSet<String>(fareInfo.getBookingCodes()));
						fareSummaryTO
								.setSegmentBookingClasses(Transformer.convertLCCMapToAAMap(fareInfo.getSegmentBookingClasses()));
						fareSummaryTO.setBundledFareFreeServiceNames(
								LCCAncillaryUtil.convertToBundledFareFreeServiceNames(fareInfo.getBundledFareFreeServices()));
						fareSummaryTO.setImageUrl(fareInfo.getImageUrl());
						fareSummaryTO.setBundleFareDescriptionTemplateDTO(Transformer.transform(fareInfo
								.getBundleFareDescriptionInfo()));
						
						fareSummaryTO.setMinimumFareByPriceOnd(
								Transformer.transformStringDecimalToFarePriceOndMap(fareInfo.getMinimumFareByPriceOndType()));
						fareSummaryList.add(fareSummaryTO);
					}
					ondOptionTO.setFlightFareSummaryList(fareSummaryList);
					List<OriginDestinationOptionTO> ondFareOptions = transformSegFareOptions
							? ondInfoTO.getOriginDestinationSegFareOptions()
							: ondInfoTO.getOrignDestinationOptions();
					ondFareOptions.add(ondOptionTO);

				}
			}
		}

		return new Object[] { ondOptionResultList, selectedFltReset };

	}

	/**
	 * Merges segment fare options to List<OriginDestinationInformationTO> in selected results if segment fare options
	 * are available.
	 * 
	 * @param res
	 *            Selected results
	 * @param segRes
	 *            Segment fare options.
	 */
	public static void mergeSegFareOndOptions(Object res, Object segRes) {
		List<OriginDestinationInformationTO> selected = (List<OriginDestinationInformationTO>) res;
		List<OriginDestinationInformationTO> segFare = (List<OriginDestinationInformationTO>) segRes;
		for (int i = 0; i < selected.size(); i++) {
			OriginDestinationInformationTO selInfo = selected.get(i);
			OriginDestinationInformationTO segInfo = segFare.get(i);

			if (!segInfo.getOriginDestinationSegFareOptions().isEmpty()) {
				selInfo.setOriginDestinationSegFareOptions(segInfo.getOriginDestinationSegFareOptions());
			}
		}
	}

	public static Set<ServiceTaxContainer> populateServiceTaxContainer(List<LCCServiceTaxContainer> lccServiceTaxContainers) {
		Set<ServiceTaxContainer> serviceTaxContainers = null;

		if (!lccServiceTaxContainers.isEmpty()) {
			serviceTaxContainers = new HashSet<ServiceTaxContainer>();
			for (LCCServiceTaxContainer lccServiceTaxContainer : lccServiceTaxContainers) {
				ServiceTaxContainer serviceTaxContainer = new ServiceTaxContainer();
				serviceTaxContainer
						.setExternalCharge(LCCClientPriceUtil.getExternalChargeType(lccServiceTaxContainer.getTaxChargeCode()));
				serviceTaxContainer.setTaxApplicable(lccServiceTaxContainer.isTaxApplicable());
				serviceTaxContainer.setTaxApplyingFlightRefNumber(lccServiceTaxContainer.getTaxApplyingFlightRefNumber());
				serviceTaxContainer.setTaxRatio(lccServiceTaxContainer.getTaxRatio());

				for (ExternalChargeType externalChargeType : lccServiceTaxContainer.getTaxableExternalCharges()) {
					serviceTaxContainer.getTaxableExternalCharges()
							.add(LCCClientPriceUtil.getExternalChargeType(externalChargeType));
				}
				serviceTaxContainers.add(serviceTaxContainer);
			}
		}

		return serviceTaxContainers;
	}

	private static SYSTEM transformSystem(SystemType systemType) {
		SYSTEM system = SYSTEM.NONE;
		if (SystemType.AA == systemType) {
			system = SYSTEM.AA;
		} else if (SystemType.INT == systemType) {
			system = SYSTEM.INT;
		}
		return system;
	}

	private static boolean withingMktCarrierFltBufferTime(Date departureDate, int channelCode, String bookingType,
			String flightType) {
		Date currentSysTimeInZulu = CalendarUtil.getCurrentSystemTimeInZulu();
		long cutoverTime = 0;

		if (channelCode == SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY)) {
			if (flightType != null) {
				if (flightType.equals("INT")) {
					cutoverTime = AppSysParamsUtil.getPublicReservationCutoverInMillis();
				} else if (flightType.equals("DOM")) {
					cutoverTime = AppSysParamsUtil.getPublicReservationCutoverInMillisForDomestic();
				}
			} else {
				cutoverTime = AppSysParamsUtil.getPublicReservationCutoverInMillis();
			}
		} else if (channelCode == SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY)) {
			if (BookingClass.BookingClassType.STANDBY.equals(bookingType)) {
				cutoverTime = AppSysParamsUtil.getStandByAgentReservationCutoverInMillis();
			} else {
				cutoverTime = AppSysParamsUtil.getAgentReservationCutoverInMillis();
			}

		}

		Calendar d = Calendar.getInstance();
		d.setTimeInMillis(currentSysTimeInZulu.getTime() + cutoverTime);
		if (d.getTime().before(departureDate)) {
			return true;
		} else {
			return false;
		}
	}

	private static void updatePaxCountLogicalCCWise(FlightSegment flightSegment, FlightSegmentTO segmentTO,
			String sourcePaxType) {
		if (flightSegment.getLogicalCabinWiseSeatAvailability() != null) {
			for (LogicalCabinSeatAvailability logicalCCWiseAvail : flightSegment.getLogicalCabinWiseSeatAvailability()) {
				String logicalCC = logicalCCWiseAvail.getLogicalCabinClass();
				for (PassengerTypeQuantity paxQuantity : flightSegment.getAirTravelerAvail().getPassengerTypeQuantity()) {
					String paxType = LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(paxQuantity.getPassengerType());
					if (PaxTypeTO.INFANT.equals(paxType) && PaxTypeTO.INFANT.equals(sourcePaxType)) {
						segmentTO.getAvailableInfantCountMap().put(logicalCC, paxQuantity.getQuantity());
					} else if (PaxTypeTO.ADULT.equals(paxType) && PaxTypeTO.ADULT.equals(sourcePaxType)) {
						segmentTO.getAvailableAdultCountMap().put(logicalCC, paxQuantity.getQuantity());
					}
				}
			}

		}
	}
}
