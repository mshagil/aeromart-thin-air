package com.isa.thinair.lccclient.core.bl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightAvailRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCAvailabilitySearchBL {

	private static final Log log = LogFactory.getLog(LCCAvailabilitySearchBL.class);

	private FlightAvailRQ flightAvailRQ;

	private FlightAvailRS flightAvailRS = new FlightAvailRS();

	private UserPrincipal userPrincipal;

	public void setFlightAvailRQ(FlightAvailRQ flightAvailRQ) {
		this.flightAvailRQ = flightAvailRQ;
	}

	public FlightAvailRS execute(BasicTrackInfo trackInfo) throws ModuleException {

		try {
			LCCFlightAvailRQ lccFlightAvailRQ = createLCCFlightAvailRQ(trackInfo);
			// set flag to identify price quote done by availability search or from fare quote
			lccFlightAvailRQ.getAvailPreferences().setFromFareQuote(false);

			LCCFlightAvailRS lccFlightAvailRS = LCConnectorUtils.getMaxicoExposedWS().searchAvailableFlights(lccFlightAvailRQ);

			if (lccFlightAvailRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccFlightAvailRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			createFlightAvailRS(lccFlightAvailRS, flightAvailRQ.getAvailPreferences().getOndFlexiSelected());
		} catch (Exception ex) {
			log.error("Error in availability search", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}

		return flightAvailRS;
	}

	private LCCFlightAvailRQ createLCCFlightAvailRQ(BasicTrackInfo trackInfo) throws ModuleException {

		LCCFlightAvailRQ lccFlightAvailRQ = new LCCFlightAvailRQ();

		// populate origin destination list
		lccFlightAvailRQ.getOriginDestinationInformation().addAll(
				LCCClientAvailUtil.createOriginDestinationInfomationList(flightAvailRQ.getOriginDestinationInformationList(),
						false));

		// populate traveler information
		lccFlightAvailRQ.setTravelerInfoSummary(LCCClientAvailUtil.createTravelerInfoSummary(flightAvailRQ
				.getTravelerInfoSummary()));

		// populate travel preferences
		lccFlightAvailRQ.setTravelPreferences(LCCClientAvailUtil.createTravelPreferences(flightAvailRQ.getTravelPreferences()));

		// populate avail preferences
		lccFlightAvailRQ.setAvailPreferences(LCCClientAvailUtil.createAvailreferences(flightAvailRQ));

		// populate pos detail
		lccFlightAvailRQ.setLccPos(LCCClientCommonUtils.createPOS(this.userPrincipal, trackInfo));

		// populate header info
		lccFlightAvailRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(flightAvailRQ.getTransactionIdentifier()));

		return lccFlightAvailRQ;
	}

	@SuppressWarnings("unchecked")
	private void createFlightAvailRS(LCCFlightAvailRS lccFlightAvailRS, Map<Integer, Boolean> ondwiseFlexiSelected) {

		// setting the transaction identifier
		flightAvailRS.setTransactionIdentifier(lccFlightAvailRS.getHeaderInfo().getTransactionIdentifier());

		// setting the ond information list

		Object[] res = LCCClientAvailUtil.createOriginDestinationInfomation(lccFlightAvailRS.getOriginDestinationInformation(),
				this.userPrincipal.getSalesChannel(), false);

		// Segment fare results.
		Object[] segRes = LCCClientAvailUtil.createOriginDestinationInfomation(
				lccFlightAvailRS.getOriginDestinationInformation(), this.userPrincipal.getSalesChannel(), true);

		// Merge selected results and segment fare results
		LCCClientAvailUtil.mergeSegFareOndOptions(res[0], segRes[0]);

		flightAvailRS.getOriginDestinationInformationList().addAll((Collection<? extends OriginDestinationInformationTO>) res[0]);

		Boolean resetSelectedFare = (Boolean) res[1];
		// setting the selected flight price info
		if (lccFlightAvailRS.getSelectedFlightPriceInfo() != null && !resetSelectedFare) {
			flightAvailRS.setSelectedPriceFlightInfo(LCCClientPriceUtil.createPriceInfoTO(lccFlightAvailRS
					.getSelectedFlightPriceInfo().getPriceInfo(), ondwiseFlexiSelected));

			PriceInfo selectedSegPriceInfo = lccFlightAvailRS.getSelectedSegmentFlightPriceInfo().getPriceInfo();

			if (selectedSegPriceInfo != null) {
				flightAvailRS.setSelectedSegmentFlightPriceInfo(LCCClientPriceUtil.createPriceInfoTO(selectedSegPriceInfo,
						ondwiseFlexiSelected));
			}
		}

		if (lccFlightAvailRS != null && lccFlightAvailRS.getReservationParams() != null) {
			flightAvailRS.setReservationParms(lccFlightAvailRS.getReservationParams());
		}
		// flightAvailRS.setOnHoldEnabled(lccFlightAvailRS.isIbeOnholdEnabled());

		flightAvailRS
				.setApplicableServiceTaxes(LCCClientAvailUtil.populateServiceTaxContainer(lccFlightAvailRS.getServiceTaxes()));
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}
}
