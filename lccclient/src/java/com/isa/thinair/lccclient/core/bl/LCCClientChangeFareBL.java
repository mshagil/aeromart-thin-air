package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FareRuleFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerStringMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InvFareAlloc;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InvFareType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OpenReturnOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatDistribution;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentInvFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCChangeFareRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCChangeFareRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFareAvailRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFareAvailRS;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentsFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareAllocTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OpenReturnOptionsTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SegmentInvFareTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientChangeFareBL {
	private static final Log log = LogFactory.getLog(LCCClientChangeFareBL.class);

	public FareAvailRS searchFares(FareAvailRQ fareAvailRQ, UserPrincipal userPrinciple, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCFareAvailRQ lccFareAvailRQ = createLCCFareAvailRQ(fareAvailRQ, userPrinciple, trackInfo);

		LCCFareAvailRS lccFareAvailRS = LCConnectorUtils.getMaxicoExposedWS().searchAvailableFares(lccFareAvailRQ);
		if (lccFareAvailRS.getResponseAttributes().getSuccess() == null) {
			LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccFareAvailRS.getResponseAttributes().getErrors());
			throw new ModuleException(lccError.getErrorCode().value());
		}

		return populateFareAvailRS(lccFareAvailRS);
	}

	private FareAvailRS populateFareAvailRS(LCCFareAvailRS lccFareAvailRS) {
		FareAvailRS fareAvailRS = new FareAvailRS();
		for (SegmentInvFare invFare : lccFareAvailRS.getSegmentInvFares()) {
			SegmentInvFareTO segmentInvFareTO = new SegmentInvFareTO();
			segmentInvFareTO.setFlightRefNumber(invFare.getFlightRefNumber());
			List<InvFareTypeTO> invetoryFareTypes = new ArrayList<InvFareTypeTO>();
			segmentInvFareTO.setInvetoryFareTypes(invetoryFareTypes);
			for (InvFareType invFareType : invFare.getInventoryFareTypes()) {
				InvFareTypeTO invFareTypeTO = new InvFareTypeTO();
				invFareTypeTO.setFareOndCode(invFareType.getFareOndCode());
				invFareTypeTO.setFareType(invFareType.getFareType());
				List<InvFareAllocTO> invFareAllocations = new ArrayList<InvFareAllocTO>();
				invFareTypeTO.setInvFareAllocations(invFareAllocations);

				for (InvFareAlloc inv : invFareType.getInvFareAllocs()) {
					InvFareAllocTO invFareAllocTO = new InvFareAllocTO();
					invFareAllocTO.setAvailableMaxSeats(inv.getAvailableMaxSeats());
					invFareAllocTO.setAvailableSeats(inv.getAvailableSeats());
					invFareAllocTO.setAllocatedSeats(inv.getAllocatedSeats());
					invFareAllocTO.setOnholdSeats(inv.getOnholdSeats());
					invFareAllocTO.setSoldSeats(inv.getSoldSeats());
					invFareAllocTO.setBcInvStatus(inv.getBcInvStatus().toString()); // TODO validate
					invFareAllocTO.setBcType(com.isa.thinair.airproxy.api.model.reservation.commons.InvFareAllocTO.BCType
							.valueOf(inv.getBcType().toString()));
					invFareAllocTO.setBookingCode(inv.getBookingClass());
					invFareAllocTO.setCcCode(inv.getCabinClass());
					invFareAllocTO.setLogicalCabinClass(inv.getLogicalCabinClass());
					invFareAllocTO.setNestRank(inv.getNestRank() == null ? null : inv.getNestRank().intValue());
					invFareAllocTO.setOnholdRestricted(inv.isOnHoldRestricted() ? "Y" : "N");
					invFareAllocTO.setActualSeatsOnHold(inv.getActualSeatsOnhold());
					invFareAllocTO.setActualSeatsSold(inv.getActualsSeatsSold());
					invFareAllocTO.setAvailableNestedSeats(inv.getAvailableNestedSeats());
					invFareAllocTO.setCommonAllSegments(inv.isCommonToAllSegments());

					List<FareRuleFareDTO> fareRuleFares = new ArrayList<FareRuleFareDTO>();
					invFareAllocTO.setFareRuleFares(fareRuleFares);

					for (FareRuleFare fareRuleFare : inv.getFareRuleFares()) {

						FareRuleFareDTO fareRuleFareTO = new FareRuleFareDTO();
						fareRuleFareTO.setAdultFareAmount(fareRuleFare.getAdultFareAmount());
						fareRuleFareTO.setAdultFareApplicable(fareRuleFare.isAdultFareApplicable());
						fareRuleFareTO.setBookingClassCode(fareRuleFare.getBookingClass());
						fareRuleFareTO.setChildFareAmount(fareRuleFare.getChildFareAmount());
						fareRuleFareTO.setChildFareApplicable(fareRuleFare.isChildFareApplicable());
						fareRuleFareTO.setFareBasisCode(fareRuleFare.getFareBasisCode());
						fareRuleFareTO.setFareId(fareRuleFare.getFareId());
						fareRuleFareTO.setFareRuleCode(fareRuleFare.getFareRuleCode());
						fareRuleFareTO.setInfantFareAmount(fareRuleFare.getInfantFareAmount());
						fareRuleFareTO.setInfantFareApplicable(fareRuleFare.isInfantFareApplicable());
						fareRuleFareTO.setVisibleAgents(fareRuleFare.getVisibleAgents());
						fareRuleFareTO.setVisibleChannelNames(fareRuleFare.getVisibleChannelNames());
						fareRuleFares.add(fareRuleFareTO);
					}
					invFareAllocations.add(invFareAllocTO);
				}

				invetoryFareTypes.add(invFareTypeTO);
			}
			fareAvailRS.addSegmentInvFare(segmentInvFareTO);
		}
		return fareAvailRS;
	}

	private LCCFareAvailRQ createLCCFareAvailRQ(FareAvailRQ fareAvailRQ, UserPrincipal userPrinciple, BasicTrackInfo trackInfo)
			throws ModuleException {

		LCCFareAvailRQ lccFareAvailRQ = new LCCFareAvailRQ();

		// populate origin destination list
		lccFareAvailRQ.getOriginDestinationInformation().addAll(LCCClientAvailUtil
				.createOriginDestinationInfomationList(fareAvailRQ.getOriginDestinationInformationList(), true));

		// populate traveler information
		lccFareAvailRQ.setTravelerInfoSummary(LCCClientAvailUtil.createTravelerInfoSummary(fareAvailRQ.getTravelerInfoSummary()));

		// populate travel preferences
		lccFareAvailRQ.setTravelPreferences(LCCClientAvailUtil.createTravelPreferences(fareAvailRQ.getTravelPreferences()));

		// populate avail preferences
		lccFareAvailRQ.setAvailPreferences(LCCClientAvailUtil.createAvailreferences(fareAvailRQ));

		// populate pos detail
		lccFareAvailRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrinciple, trackInfo));

		// populate header info
		lccFareAvailRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(fareAvailRQ.getTransactionIdentifier()));

		// Change fare specifics
		lccFareAvailRQ.setSalesChannelCode(fareAvailRQ.getSalesChannelCode());
		lccFareAvailRQ.setTravelAgentCode(fareAvailRQ.getTravelAgentCode());
		lccFareAvailRQ.setSelectedOBFlightRefNumber(fareAvailRQ.getSelectedOBFltRefNo());
		lccFareAvailRQ.setSelectedIBFlightRefNumber(fareAvailRQ.getSelectedIBFltRefNo());
		lccFareAvailRQ.setExcludeNonLowestPublicFares(fareAvailRQ.isExcludeNonLowestPublicFares());
		lccFareAvailRQ.setExcludeOnewayPublicFaresFromReturnJourneys(fareAvailRQ.isExcludeOnewayPublicFaresFromReturnJourneys());

		if (fareAvailRQ.getOutboundFlightSegments() != null) {
			for (FlightSegmentTO fsTO : fareAvailRQ.getOutboundFlightSegments()) {
				lccFareAvailRQ.getOutboundFlightSegments().add(LCCClientAvailUtil.populateFlightSegment(fsTO));
			}
		}

		if (fareAvailRQ.getInboundFlightSegments() != null) {
			for (FlightSegmentTO fsTO : fareAvailRQ.getInboundFlightSegments()) {
				lccFareAvailRQ.getInboundFlightSegments().add(LCCClientAvailUtil.populateFlightSegment(fsTO));
			}
		}

		return lccFareAvailRQ;
	}

	public FlightPriceRS changeFare(ChangeFareRQ changeFareRQ, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCChangeFareRQ lccChangeFareRQ = populateLCCChangeFareRQ(changeFareRQ, userPrincipal, trackInfo);
		LCCChangeFareRS lccChangeFareRS = LCConnectorUtils.getMaxicoExposedWS().changeFares(lccChangeFareRQ);
		if (lccChangeFareRS.getResponseAttributes().getSuccess() == null) {
			LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccChangeFareRS.getResponseAttributes().getErrors());
			throw new ModuleException(lccError.getErrorCode().value());
		}
		FlightPriceRS flightPriceRS = populateFlightPriceRS(lccChangeFareRS, userPrincipal,
				changeFareRQ.getAvailPreferences().getOndFlexiSelected());
		// PriceInfoTO priceInfoTo = flightPriceRS.getSelectedPriceFlightInfo();
		PriceInfoTO priceInfoTo = flightPriceRS.getSelectedPriceFlightInfo();
		if (priceInfoTo != null && priceInfoTo.getFareTypeTO() != null) {

			Map<Integer, Boolean> changeFareFlexiSelection = changeFareRQ.getAvailPreferences().getOndFlexiSelected();
			if (changeFareFlexiSelection != null && !changeFareFlexiSelection.isEmpty()) {
				Map<Integer, Boolean> flexiSelectionMap = priceInfoTo.getFareTypeTO().getOndFlexiSelection();
				if (flexiSelectionMap != null && !flexiSelectionMap.isEmpty()) {
					for (Entry<Integer, Boolean> entry : flexiSelectionMap.entrySet()) {
						if (entry.getValue() != null && entry.getValue().booleanValue() != false
								&& changeFareFlexiSelection.containsKey(entry.getKey())
								&& changeFareFlexiSelection.get(entry.getKey()) != null) {
							entry.setValue(changeFareFlexiSelection.get(entry.getKey()));
						} else {
							entry.setValue(Boolean.FALSE);
						}
					}

					priceInfoTo.getFareTypeTO().setOndFlexiSelection(flexiSelectionMap);

					// Set flexi selection flage for LogicalCabin Class
					Iterator<OndClassOfServiceSummeryTO> iteratorOndclassOfServiceSummaryTo = priceInfoTo
							.getAvailableLogicalCCList().iterator();
					while (iteratorOndclassOfServiceSummaryTo.hasNext()) {
						OndClassOfServiceSummeryTO ondClassOfServiceSummeryTO = (OndClassOfServiceSummeryTO) iteratorOndclassOfServiceSummaryTo
								.next();
						ondClassOfServiceSummeryTO.getAvailableLogicalCCList().get(0)
								.setWithFlexi(flexiSelectionMap.get(ondClassOfServiceSummeryTO.getSequence()));
					}

				}
			}

		}
		return flightPriceRS;
	}

	private FlightPriceRS populateFlightPriceRS(LCCChangeFareRS lccFareAvailRS, UserPrincipal userPrincipal,
			Map<Integer, Boolean> ondwiseFlexiSelected) {
		FlightPriceRS flightPriceRS = new FlightPriceRS();

		Object[] res = LCCClientAvailUtil.createOriginDestinationInfomation(lccFareAvailRS.getOriginDestinationInformation(),
				userPrincipal.getSalesChannel(), false);
		// setting the ond information list
		flightPriceRS.getOriginDestinationInformationList().addAll((Collection<? extends OriginDestinationInformationTO>) res[0]);
		flightPriceRS.setSelectedPriceFlightInfo(
				LCCClientPriceUtil.createPriceInfoTO(lccFareAvailRS.getPriceInfo(), ondwiseFlexiSelected));
		flightPriceRS.setOpenReturnOptionsTO(populateOpenReturnOptionsTO(lccFareAvailRS.getOpenReturnOptionsTO()));
		return flightPriceRS;
	}

	private OpenReturnOptionsTO populateOpenReturnOptionsTO(OpenReturnOptions openReturnOptions) {
		OpenReturnOptionsTO openReturnOptionsTO = null;
		if (openReturnOptions != null) {
			openReturnOptionsTO = new OpenReturnOptionsTO();
			openReturnOptionsTO.setConfirmExpiryDate(openReturnOptions.getConfirmExpiryDate());
			openReturnOptionsTO.setTravelExpiryDate(openReturnOptions.getTravelExpiryDate());
			openReturnOptionsTO.setAirportCode(openReturnOptions.getAirportCode());
		}
		return openReturnOptionsTO;
	}

	private LCCChangeFareRQ populateLCCChangeFareRQ(ChangeFareRQ changeFareRQ, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) {
		LCCChangeFareRQ lccChangeFareRQ = new LCCChangeFareRQ();
		// populate origin destination list
		lccChangeFareRQ.getOriginDestinationInformation().addAll(LCCClientAvailUtil
				.createOriginDestinationInfomationList(changeFareRQ.getOriginDestinationInformationList(), true));

		// populate traveler information
		lccChangeFareRQ
				.setTravelerInfoSummary(LCCClientAvailUtil.createTravelerInfoSummary(changeFareRQ.getTravelerInfoSummary()));

		// populate travel preferences
		lccChangeFareRQ.setTravelPreferences(LCCClientAvailUtil.createTravelPreferences(changeFareRQ.getTravelPreferences()));

		// populate avail preferences
		lccChangeFareRQ.setAvailPreferences(LCCClientAvailUtil.createAvailreferences(changeFareRQ));

		// populate pos detail
		lccChangeFareRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

		// populate header info
		lccChangeFareRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(changeFareRQ.getTransactionIdentifier()));

		lccChangeFareRQ.setSelectedFareType(changeFareRQ.getSelectedFareType().toString());
		for (SegmentsFareDTO segmentFareDTO : changeFareRQ.getChangedFareSegments()) {
			FareSegment changedFlightSegment = new FareSegment();
			changedFlightSegment.setCabinClass(segmentFareDTO.getCabinClassCode());
			changedFlightSegment.setLogicalCabinClass(segmentFareDTO.getLogicalCabinClassCode());
			changedFlightSegment.setInbound(segmentFareDTO.isInbound());
			changedFlightSegment.setFixedQuotaSeats(segmentFareDTO.isFixedQuotaSeats());
			changedFlightSegment.setOndCode(segmentFareDTO.getOndCode());
			changedFlightSegment.setOndSequence(segmentFareDTO.getOndSequence());

			if (segmentFareDTO.getSegmentRPHMap() != null) {
				for (Iterator segIt = segmentFareDTO.getSegmentRPHMap().entrySet().iterator(); segIt.hasNext();) {
					Map.Entry segEntry = (Map.Entry) segIt.next();
					changedFlightSegment.getSegmentsMap().add(populateIntegerStringMap(segEntry));
				}
			}

			changedFlightSegment.getSeatDistributions()
					.addAll(populateSeatDistributionList(segmentFareDTO.getSeatDistributions()));
			FareSummary fareSummary = populateFareSummary(segmentFareDTO.getFareSummaryDTO());
			changedFlightSegment.setFareSummary(fareSummary);
			changedFlightSegment.setFareType(segmentFareDTO.getFareType());
			lccChangeFareRQ.getChangedFareSegments().add(changedFlightSegment);
		}
		lccChangeFareRQ.setPosAirport(changeFareRQ.getPosAirport());
		changeFareRQ.getChangedFareSegments();

		lccChangeFareRQ.getCarrierWiseOnds().addAll(populateCarrierWiseOnd(changeFareRQ.getCarrierWiseOnd()));

		return lccChangeFareRQ;
	}

	private IntegerStringMap populateIntegerStringMap(Map.Entry mapEntry) {

		IntegerStringMap intStrMapObj = new IntegerStringMap();
		intStrMapObj.setKey((Integer) mapEntry.getKey());
		intStrMapObj.setValue((String) mapEntry.getValue());

		return intStrMapObj;
	}

	private static List<StringStringListMap> populateCarrierWiseOnd(Map<String, List<String>> carrierWiseOndMap) {
		List<StringStringListMap> carrierWiseOndList = new ArrayList<StringStringListMap>();
		for (Entry<String, List<String>> entry : carrierWiseOndMap.entrySet()) {
			carrierWiseOndList.add(populateStringStringListMap(entry));
		}

		return carrierWiseOndList;
	}

	private static StringStringListMap populateStringStringListMap(Map.Entry<String, List<String>> mapEntry) {

		StringStringListMap strStrMapObj = new StringStringListMap();
		strStrMapObj.setKey(mapEntry.getKey());
		strStrMapObj.getValue().addAll(mapEntry.getValue());

		return strStrMapObj;
	}

	private FareSummary populateFareSummary(FareSummaryDTO fareSummaryDTO) {
		FareSummary fareSummary = new FareSummary();
		fareSummary.setFareId(fareSummaryDTO.getFareId());

		if (fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT) != null) {
			fareSummary.setFareAmount(new BigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT)));
		}

		fareSummary.setFarePercentage(fareSummaryDTO.getFarePercentage());
		fareSummary.setBookingClassCode(fareSummaryDTO.getBookingClassCode());
		fareSummary.setFareRuleID(fareSummaryDTO.getFareRuleID());
		fareSummary.setFareCategoryCode(fareSummaryDTO.getFareCategoryCode());
		fareSummary.setFareCommonAllSegments(fareSummaryDTO.isFareCommonAllSegments());
		fareSummary.setFareRuleCode(fareSummaryDTO.getFareRuleCode());
		fareSummary.setFareBasisCode(fareSummaryDTO.getFareBasisCode());
		fareSummary.setFareRuleComment(fareSummaryDTO.getFareRuleComment());
		fareSummary.setChildFareType(fareSummaryDTO.getChildFareType());
		fareSummary.setChildFare(new BigDecimal(fareSummaryDTO.getChildFare()));
		fareSummary.setInfantFareType(fareSummaryDTO.getInfantFareType());
		fareSummary.setInfantFare(new BigDecimal(fareSummaryDTO.getInfantFare()));

		if (fareSummaryDTO.getVisibleChannels() != null) {
			for (Integer channel : (List<Integer>) fareSummaryDTO.getVisibleChannels()) {
				fareSummary.getVisibleChannels().add(channel);
			}
		}

		if (fareSummaryDTO.getVisibleChannelNames() != null) {
			for (String chName : (List<String>) fareSummaryDTO.getVisibleChannelNames()) {
				fareSummary.getVisibleChannelNames().add(chName);
			}
		}

		if (fareSummaryDTO.getVisibleAgents() != null) {
			for (String agent : (List<String>) fareSummaryDTO.getVisibleAgents()) {
				fareSummary.getVisibleAgents().add(agent);
			}
		}

		fareSummary.setAdultApplicability(fareSummaryDTO.isAdultApplicability());
		fareSummary.setChildApplicability(fareSummaryDTO.isChildApplicability());
		fareSummary.setInfantApplicability(fareSummaryDTO.isInfantApplicability());
		fareSummary.setAdultFareRefundable(fareSummaryDTO.isAdultFareRefundable());
		fareSummary.setChildFareRefundable(fareSummaryDTO.isChildFareRefundable());
		fareSummary.setInfantFareRefundable(fareSummaryDTO.isInfantFareRefundable());
		fareSummary.setOndCode(fareSummaryDTO.getOndCode());

		return fareSummary;
	}

	private List<SeatDistribution> populateSeatDistributionList(
			Collection<com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution> seatDistDTOList) {
		List<SeatDistribution> seatDistList = new ArrayList<SeatDistribution>();
		if (seatDistDTOList != null) {
			for (com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution seatDistDTO : seatDistDTOList) {
				SeatDistribution seatDist = new SeatDistribution();
				seatDist.setBookingClassCode(seatDistDTO.getBookingClassCode());
				seatDist.setNoOfSeats(seatDistDTO.getNoOfSeats());
				seatDist.setNestedSeats(seatDistDTO.isNestedSeats());
				seatDist.setBookingClassType(seatDistDTO.getBookingClassType());
				seatDist.setOnholdRestricted(seatDistDTO.isOnholdRestricted());
				seatDistList.add(seatDist);
			}
		}

		return seatDistList;
	}
}
