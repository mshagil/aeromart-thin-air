package com.isa.thinair.lccclient.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

public class LCCClientConfig extends DefaultModuleConfig {

	private String lccWsUrl;

	private String dumpMessages;

	public String getLccWsUrl() {
		return lccWsUrl;
	}

	public void setLccWsUrl(String lccWsUrl) {
		this.lccWsUrl = lccWsUrl;
	}

	public String getDumpMessages() {
		return dumpMessages;
	}

	public void setDumpMessages(String dumpMessages) {
		this.dumpMessages = dumpMessages;
	}
}
