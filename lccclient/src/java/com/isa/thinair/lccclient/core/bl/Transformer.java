package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.*;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAgreementMasterDataRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAgreementMasterDataRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRequoteRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAnciModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCDiscountRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCErrorCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPOS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxCreditSearchRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReadRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReprotectSegmentsRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSearchReservationRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSearchReservationRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelfReprotectFlightsRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTransferSegmentsRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCUserNoteRQ;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FarePriceOnd;
import com.isa.thinair.airmaster.api.dto.MasterDataCriteriaDTO;
import com.isa.thinair.airmaster.api.dto.MasterDataDTO;
import com.isa.thinair.airproxy.api.dto.ChargeAdjustmentPrivilege;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationRequest;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAdminInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationPreferrenceInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeReverse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentHolder;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationOwnerAgentContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO.SegmentDetails;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCOfflinePaymentDetails;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PNRModificationAuthorizationUtils;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.RefundableChargeDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.StationContactDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTranslationTemplateDTO;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.XMLDataTypeUtil;
import com.isa.thinair.lccclient.api.util.ETicketUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.LCCUtils;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.lccclient.core.util.LCCAncillaryUtil;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareFreeServiceTO;

//import com.isa.connectivity.profiles.maxico.v1.common.dto.PromoSelectionCriteria;

/**
 * @author Nilindra Fernando
 */
public class Transformer {
	
	private static final Log log = LogFactory.getLog(Transformer.class);

	public static LCCReadRQ transform(LCCClientPnrModesDTO lcclientPnrModesDTO, UserPrincipal userPrincipal,
			ModificationParamRQInfo paramRQInfo, BasicTrackInfo trackInfo) {
		/* LCCReadRQ */
		LCCReadRQ lccReadRQ = new LCCReadRQ();
		lccReadRQ.setGroupPnr(lcclientPnrModesDTO.getGroupPNR());
		lccReadRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccReadRQ.setLoadReservationOptions(LCCReservationUtil.createLoadReservationOptions(lcclientPnrModesDTO));
		lccReadRQ.setSelectedLanguage(lcclientPnrModesDTO.getPreferredLanguage());
		lccReadRQ.setModificationParamsRQ(LCCReservationUtil.createModificationParamRQ(paramRQInfo							));

		// Related to dry booking loading
		if (StringUtils.isNotEmpty(lcclientPnrModesDTO.getAirlineCode())
				&& StringUtils.isNotEmpty(lcclientPnrModesDTO.getMarketingAirlineCode())) {
			LoadDryReservationOptions dryReservationOptions = new LoadDryReservationOptions();
			dryReservationOptions.setMarketingAirlineCode(lcclientPnrModesDTO.getMarketingAirlineCode());
			if (lcclientPnrModesDTO.getInterlineAgreementId() != null) {
				dryReservationOptions.setInterlineAgreementId(lcclientPnrModesDTO.getInterlineAgreementId());
			}

			LCCGroupReservationCarrier groupReservationCarrier = new LCCGroupReservationCarrier();
			groupReservationCarrier.setAirlineCode(lcclientPnrModesDTO.getAirlineCode());
			if (StringUtils.isNotEmpty(lcclientPnrModesDTO.getGroupPNR())) {
				groupReservationCarrier.setCarrierPNR(lcclientPnrModesDTO.getGroupPNR());
			} else if (StringUtils.isNotEmpty(lcclientPnrModesDTO.getPnr())) {
				groupReservationCarrier.setCarrierPNR(lcclientPnrModesDTO.getPnr());
			}

			dryReservationOptions.getGroupReservationCarrier().add(groupReservationCarrier);
			lccReadRQ.setLoadDryReservationOptions(dryReservationOptions);
		}
		return lccReadRQ;
	}

	public static LCCSearchReservationRQ transform(ReservationSearchDTO reservationSearchDTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) {

		/* SearchReservation options */
		SearchReservatonOptions searchReservatonOptions = new SearchReservatonOptions();
		searchReservatonOptions.setBookingClassType(reservationSearchDTO.getBookingClassType());
		searchReservatonOptions.setDepartureDate(reservationSearchDTO.getDepartureDate());
		searchReservatonOptions.setETicket(reservationSearchDTO.getETicket());
		searchReservatonOptions.setInvoiceNumber(reservationSearchDTO.getInvoiceNumber());
		searchReservatonOptions.setExactMatch(reservationSearchDTO.isExactMatch());
		searchReservatonOptions.setFirstName(reservationSearchDTO.getFirstName());
		searchReservatonOptions.setFlightNo(reservationSearchDTO.getFlightNo());
		searchReservatonOptions.setFromAirport(reservationSearchDTO.getFlightNo());
		searchReservatonOptions.setLastName(reservationSearchDTO.getLastName());
		searchReservatonOptions.setPageNumber(reservationSearchDTO.getPageNo());
		searchReservatonOptions.setPassport(reservationSearchDTO.getPassport());
		searchReservatonOptions.setPnr(reservationSearchDTO.getPnr());
		searchReservatonOptions.setReturnDate(reservationSearchDTO.getReturnDate());
		// searchReservatonOptions.setSkipExternalSegments(reservationSearchDTO.get)
		// TODO set this value in the search dto
		searchReservatonOptions.setTelephoneNo(reservationSearchDTO.getTelephoneNo());
		searchReservatonOptions.setTelephoneNoForSQL(reservationSearchDTO.getTelephoneNoForSQL());
		searchReservatonOptions.setToAirport(reservationSearchDTO.getToAirport());

		if (reservationSearchDTO.getOriginChannelIds() != null && reservationSearchDTO.getOriginChannelIds().size() > 0) {
			searchReservatonOptions.getOriginChannelIds().addAll(reservationSearchDTO.getOriginChannelIds());
		}
		searchReservatonOptions.setEarlyDepartureDate(reservationSearchDTO.getEarlyDepartureDate());
		searchReservatonOptions.setEmail(reservationSearchDTO.getEmail());
		searchReservatonOptions.setOwnerChannelId(reservationSearchDTO.getOwnerChannelId());
		searchReservatonOptions.setOwnerAgentCode(reservationSearchDTO.getOwnerAgentCode());
		searchReservatonOptions.setOriginAgentCode(reservationSearchDTO.getOriginAgentCode());
		searchReservatonOptions.setOriginUserId(reservationSearchDTO.getOriginUserId());
		searchReservatonOptions.setFilterZeroCredits(reservationSearchDTO.isFilterZeroCredits());
		searchReservatonOptions.setIncludePaxInfo(reservationSearchDTO.isIncludePaxInfo());
		searchReservatonOptions.setSearchIBEBookings(reservationSearchDTO.isSearchIBEBookings());

		if (reservationSearchDTO.getSearchableCarriers() != null && !reservationSearchDTO.getSearchableCarriers().isEmpty()) {
			for (String carrierCode : reservationSearchDTO.getSearchableCarriers()) {
				searchReservatonOptions.getAccessibleCarriers().add(carrierCode);
			}
		}

		if (reservationSearchDTO.getReservationStates() != null && reservationSearchDTO.getReservationStates().size() > 0) {
			searchReservatonOptions.getReservationStates().addAll(reservationSearchDTO.getReservationStates());
		}

		if (reservationSearchDTO instanceof ReservationPaymentDTO) {
			ReservationPaymentDTO reservationPaymentDTO = (ReservationPaymentDTO) reservationSearchDTO;
			searchReservatonOptions.setCcAuthorCode(reservationPaymentDTO.getCcAuthorCode());
			searchReservatonOptions.setCreditCardNo(reservationPaymentDTO.getCreditCardNo());
			searchReservatonOptions.setEDate(reservationPaymentDTO.getEDate());
		}

		if (reservationSearchDTO.getBookingCategories() != null && reservationSearchDTO.getBookingCategories().size() > 0) {
			searchReservatonOptions.getBookingCategories().addAll(reservationSearchDTO.getBookingCategories());
		}

		/* LCCSearchReservationsRQ */
		LCCSearchReservationRQ lccSearchReservationRQ = new LCCSearchReservationRQ();
		lccSearchReservationRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccSearchReservationRQ.setSearchReservatonOptions(searchReservatonOptions);
		return lccSearchReservationRQ;
	}

	public static LCCAgreementMasterDataRQ transform(MasterDataCriteriaDTO masterDataCriteriaDTO, UserPrincipal userPrincipal) {
		LCCAgreementMasterDataRQ rq = new LCCAgreementMasterDataRQ();
		rq.setLoadAgents(masterDataCriteriaDTO.isLoadAgents());
		rq.setLoadBookingClasses(masterDataCriteriaDTO.isLoadBookingClasses());
		rq.setLoadFlights(masterDataCriteriaDTO.isLoadFlightNumbers());
		rq.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal));

		return rq;
	}

	public static MasterDataDTO transform(LCCAgreementMasterDataRS rs) {

		Map<String, String> flights = new TreeMap<String, String>();
		Map<String, String> agents = new TreeMap<String, String>();
		Map<String, String> bookingClasses = new TreeMap<String, String>();

		for (StringStringStringMap agreement : rs.getAgents()) {
			for (StringStringMap agent : agreement.getValue()) {
				agents.put(agent.getKey(), agent.getValue());
			}
		}

		for (StringStringStringMap agreement : rs.getFlightNumbers()) {
			for (StringStringMap flight : agreement.getValue()) {
				flights.put(flight.getKey(), flight.getValue());
			}
		}

		for (StringStringStringMap agreement : rs.getBookingClasses()) {
			for (StringStringMap bookingClass : agreement.getValue()) {
				bookingClasses.put(bookingClass.getKey(), bookingClass.getValue());
			}
		}

		MasterDataDTO masterDataDTO = new MasterDataDTO();
		masterDataDTO.setAgents(agents);
		masterDataDTO.setBookingClasses(bookingClasses);
		masterDataDTO.setFlightNumbers(flights);
		return masterDataDTO;
	}

	public static Map<String, String> convertLCCMapToAAMap(List<StringStringMap> lccMap) {
		Map<String, String> aaMap = null;
		if (!lccMap.isEmpty()) {
			aaMap = new HashMap<String, String>();
			for (StringStringMap lccMapObj : lccMap) {
				aaMap.put(lccMapObj.getKey(), lccMapObj.getValue());
			}
		}

		return aaMap;
	}

	
	public static UserNote transform(String userNote, String userNoteType) throws ModuleException {
		UserNote lastUserNote = new UserNote();

		lastUserNote.setAction("");
		lastUserNote.getCarrierCode().add(AppSysParamsUtil.getCarrierCode());
		lastUserNote.setNoteText(BeanUtils.nullHandler(userNote));
		lastUserNote.setSystemText("");
		lastUserNote.setTimestamp(new Date());
		lastUserNote.setUsername("");
        lastUserNote.setUserNoteType(userNoteType);
		return lastUserNote;
	}

	public static FormOfIdentification transform(String foidNumber, Date psptExpiry, String psptIssuedCntry, Integer foidType,
			String placeOfBirth, String travelDocumentType, String visaDocNumber, String visaDocPlaceOfIssue,
			Date visaDocIssueDate, String visaApplicableCountry) throws ModuleException {
		FormOfIdentification formOfIdentification = new FormOfIdentification();
		formOfIdentification.setFoidNumber(foidNumber);
		formOfIdentification.setPsptExpiry((psptExpiry != null) ? XMLDataTypeUtil.getXMLGregorianCalendar(psptExpiry) : null);
		formOfIdentification.setPsptIssuedCntry(psptIssuedCntry);
		formOfIdentification.setPaxCategoryFOIDId(foidType == null ? null : foidType.toString());
		formOfIdentification.setPlaceOfBirth(placeOfBirth);
		formOfIdentification.setVisaDocIssueDate((visaDocIssueDate != null) ? XMLDataTypeUtil
				.getXMLGregorianCalendar(visaDocIssueDate) : null);
		formOfIdentification.setTravelDocumentType(travelDocumentType);
		formOfIdentification.setVisaDocNumber(visaDocNumber);
		formOfIdentification.setVisaDocPlaceOfIssue(visaDocPlaceOfIssue);
		formOfIdentification.setVisaApplicableCountry(visaApplicableCountry);
		return formOfIdentification;
	}

	public static TravelerInfo transform(Collection<LCCClientReservationPax> passengers) throws ModuleException {
		TravelerInfo travelerInfo = new TravelerInfo();

		for (LCCClientReservationPax lcclientReservationPax : passengers) {
			travelerInfo.getAirTraveler().add(transform(lcclientReservationPax));
		}

		SpecialReqDetails specialReqDetails = new SpecialReqDetails();
		for (LCCClientReservationPax lcclientReservationPax : passengers) {
			if (lcclientReservationPax.getSelectedAncillaries() != null) {

				specialReqDetails.getSeatRequests().addAll(transformSeatRequest(lcclientReservationPax.getSelectedAncillaries()));
				specialReqDetails.getSpecialServiceRequests().addAll(
						transformSSR(lcclientReservationPax.getSelectedAncillaries()));
				specialReqDetails.getMealsRequests().addAll(transformMeals(lcclientReservationPax.getSelectedAncillaries()));
				specialReqDetails.getBaggagesRequests()
						.addAll(transformBaggages(lcclientReservationPax.getSelectedAncillaries()));
				specialReqDetails.getAirportServiceRequests().addAll(
						transformAirportServiceRequest(lcclientReservationPax.getSelectedAncillaries()));
			}
		}

		travelerInfo.setSpecialReqDetails(specialReqDetails);

		return travelerInfo;
	}

	public static TravelerInfo transform(Collection<LCCClientReservationPax> passengers,
			CommonReservationPreferrenceInfo preferenceInfo, Map<Integer, Map<Integer, EticketDTO>> eTicketNumbers,
			Set<LCCClientReservationSegment> reservationSegments) throws ModuleException {
		TravelerInfo travelerInfo = new TravelerInfo();

		SpecialReqDetails specialReqDetails = new SpecialReqDetails();

		for (LCCClientReservationPax lcclientReservationPax : passengers) {
			AirTraveler airTraveler = transform(lcclientReservationPax);
			travelerInfo.getAirTraveler().add(airTraveler);

			if (lcclientReservationPax.getSelectedAncillaries() != null) {

				specialReqDetails.getSeatRequests().addAll(transformSeatRequest(lcclientReservationPax.getSelectedAncillaries()));
				specialReqDetails.getSpecialServiceRequests().addAll(
						transformSSR(lcclientReservationPax.getSelectedAncillaries()));
				specialReqDetails.getMealsRequests().addAll(transformMeals(lcclientReservationPax.getSelectedAncillaries()));
				specialReqDetails.getBaggagesRequests()
						.addAll(transformBaggages(lcclientReservationPax.getSelectedAncillaries()));
				specialReqDetails.getAirportServiceRequests().addAll(
						transformAirportServiceRequest(lcclientReservationPax.getSelectedAncillaries()));
			}

			// add passenger e ticket details to the book request
			if (eTicketNumbers.keySet().contains(lcclientReservationPax.getPaxSequence())) {
				airTraveler.getETickets().addAll(
						ETicketUtil.createPaxEticket(reservationSegments, lcclientReservationPax.getPaxSequence(),
								eTicketNumbers.get(lcclientReservationPax.getPaxSequence())));
			}
		}

		travelerInfo.setSpecialReqDetails(specialReqDetails);
		travelerInfo.setPreferredLanguage(preferenceInfo.getPreferredLanguage());

		return travelerInfo;
	}

	private static List<BookMealRequest> transformMeals(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		List<BookMealRequest> bookMealRequests = new ArrayList<BookMealRequest>();
		for (LCCSelectedSegmentAncillaryDTO segmentAncillaryDTO : selectedAncillaries) {
			if (segmentAncillaryDTO.getMealDTOs() != null) {
				for (LCCMealDTO mealDTO : segmentAncillaryDTO.getMealDTOs()) {
					BookMealRequest bookMealRequest = new BookMealRequest();
					bookMealRequest.setMealCode(mealDTO.getMealCode());
					bookMealRequest.setMealName(mealDTO.getMealName());
					bookMealRequest.setMealQuantity(mealDTO.getSoldMeals());
					bookMealRequest.setCabinClass(segmentAncillaryDTO.getFlightSegmentTO().getCabinClassCode());
					bookMealRequest.setFlightRefNumber(segmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber());
					bookMealRequest.setTravelerRefNumber(segmentAncillaryDTO.getTravelerRefNumber());
					bookMealRequests.add(bookMealRequest);
				}
			}
		}

		return bookMealRequests;
	}

	private static List<BookBaggageRequest> transformBaggages(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		List<BookBaggageRequest> bookBaggageRequests = new ArrayList<BookBaggageRequest>();
		for (LCCSelectedSegmentAncillaryDTO segmentAncillaryDTO : selectedAncillaries) {
			if (segmentAncillaryDTO.getBaggageDTOs() != null) {
				for (LCCBaggageDTO baggageDTO : segmentAncillaryDTO.getBaggageDTOs()) {
					BookBaggageRequest bookBaggageRequest = new BookBaggageRequest();
					// bookBaggageRequest.setBaggageCode(baggageDTO.getBaggageCode());
					bookBaggageRequest.setBaggageName(baggageDTO.getBaggageName());
					bookBaggageRequest.setBaggageDescription(baggageDTO.getBaggageDescription());
					// bookBaggageRequest.setBaggageQuantity(baggageDTO.getSoldMeals());
					bookBaggageRequest.setCabinClass(segmentAncillaryDTO.getFlightSegmentTO().getCabinClassCode());
					bookBaggageRequest.setFlightRefNumber(segmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber());
					bookBaggageRequest.setTravelerRefNumber(segmentAncillaryDTO.getTravelerRefNumber());
					bookBaggageRequests.add(bookBaggageRequest);
				}
			}
		}
		return bookBaggageRequests;
	}

	private static List<SpecialServiceRequest> transformSSR(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		List<SpecialServiceRequest> specialServiceRequests = new ArrayList<SpecialServiceRequest>();
		if (selectedAncillaries != null) {
			for (LCCSelectedSegmentAncillaryDTO segmentAncillaryDTO : selectedAncillaries) {
				if (segmentAncillaryDTO.getSpecialServiceRequestDTOs() != null) {
					for (LCCSpecialServiceRequestDTO serviceRequestDTO : segmentAncillaryDTO.getSpecialServiceRequestDTOs()) {

						SpecialServiceRequest specialServiceRequest = new SpecialServiceRequest();
						specialServiceRequest.setSsrCode(serviceRequestDTO.getSsrCode());
						specialServiceRequest.setCarrierCode(serviceRequestDTO.getCarrierCode());
						specialServiceRequest.setCharge(serviceRequestDTO.getCharge());
						specialServiceRequest.setText(serviceRequestDTO.getText());
						specialServiceRequest.setFlightRefNumber(segmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber());
						specialServiceRequest.setTravelerRefNumber(segmentAncillaryDTO.getTravelerRefNumber());

						specialServiceRequests.add(specialServiceRequest);
					}
				}
			}
		}
		return specialServiceRequests;
	}

	private static List<AirportServiceRequest> transformAirportServiceRequest(
			List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		List<AirportServiceRequest> airportServiceRequests = new ArrayList<AirportServiceRequest>();
		if (selectedAncillaries != null) {
			for (LCCSelectedSegmentAncillaryDTO segmentAncillaryDTO : selectedAncillaries) {
				if (segmentAncillaryDTO.getAirportServiceDTOs() != null) {
					for (LCCAirportServiceDTO airportServiceDTO : segmentAncillaryDTO.getAirportServiceDTOs()) {
						AirportServiceRequest airportServiceRequest = new AirportServiceRequest();
						airportServiceRequest.setSsrCode(airportServiceDTO.getSsrCode());
						airportServiceRequest.setDescription(airportServiceDTO.getSsrDescription());
						airportServiceRequest.setCarrierCode(airportServiceDTO.getCarrierCode());
						airportServiceRequest.setAirportCode(airportServiceDTO.getAirportCode());
						airportServiceRequest.setServiceCharge(airportServiceDTO.getServiceCharge());
						airportServiceRequest.setApplicabilityType(airportServiceDTO.getApplicabilityType());
						airportServiceRequest.setApplyOn(airportServiceDTO.getApplyOn());
						airportServiceRequest.setFlightRefNumber(segmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber());
						airportServiceRequest.setTravelerRefNumber(segmentAncillaryDTO.getTravelerRefNumber());
						airportServiceRequests.add(airportServiceRequest);
					}
				}
			}
		}
		return airportServiceRequests;
	}

	private static List<SeatRequest> transformSeatRequest(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		List<SeatRequest> seatRequests = new ArrayList<SeatRequest>();

		if (selectedAncillaries != null) {
			for (LCCSelectedSegmentAncillaryDTO segmentAncillaryDTO : selectedAncillaries) {

				LCCAirSeatDTO airSeatDTO = segmentAncillaryDTO.getAirSeatDTO();
				if (airSeatDTO != null) {
					SeatRequest seatRequest = new SeatRequest();
					seatRequest.setSeatNumber(airSeatDTO.getSeatNumber());
					seatRequest.setFlightRefNumber(segmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber());
					seatRequest.setTravelerRefNumber(segmentAncillaryDTO.getTravelerRefNumber());

					seatRequests.add(seatRequest);
				}
			}
		}
		return seatRequests;
	}

	public static AirTraveler transform(LCCClientReservationPax lcclientReservationPax) throws ModuleException {
		AirTraveler airTraveler = new AirTraveler();

		if ((lcclientReservationPax.getInfants() != null && lcclientReservationPax.getInfants().size() > 0)
				|| (PaxTypeTO.ADULT.equals(lcclientReservationPax.getPaxType()) && lcclientReservationPax.getAttachedPaxId() != null)) {
			airTraveler.setAccompaniedByInfant(true);
		}
		/*
		 * boolean accompaniedByInfant = (lcclientReservationPax.getInfants() != null &&
		 * lcclientReservationPax.getInfants().size() > 0); airTraveler.setAccompaniedByInfant(accompaniedByInfant);
		 */

		String travelerRefNumber = lcclientReservationPax.getTravelerRefNumber();
		if (BeanUtils.nullHandler(travelerRefNumber).isEmpty()) {
			travelerRefNumber = PaxTypeUtils.travelerReference(lcclientReservationPax);
		}

		airTraveler.setTravelerRefNumber(travelerRefNumber);
		airTraveler.setTravelerSequence(lcclientReservationPax.getPaxSequence());
		airTraveler.setPassengerType(LCCClientCommonUtils.getLCCPaxTypeCode(lcclientReservationPax.getPaxType()));
		airTraveler.setPersonName(LCCClientApiUtils.transform(lcclientReservationPax.getTitle(),
				lcclientReservationPax.getFirstName(), lcclientReservationPax.getLastName()));
		airTraveler.setNationalityCode(LCCUtils.getNationalityIsoCode(lcclientReservationPax.getNationalityCode()));
		airTraveler.setBirthDate(XMLDataTypeUtil.getXMLGregorianCalendar(lcclientReservationPax.getDateOfBirth()));
		LCCClientReservationAdditionalPax addnInfo = lcclientReservationPax.getLccClientAdditionPax();

		String psptNo = "";
		Date psptExpiry = null;
		String psptIssuedCntry = "";

		if (addnInfo != null) {
			psptNo = addnInfo.getPassportNo();
			psptExpiry = addnInfo.getPassportExpiry();
			psptIssuedCntry = addnInfo.getPassportIssuedCntry();
			airTraveler.setFormOfIdentification(transform(psptNo, psptExpiry, psptIssuedCntry, 0, addnInfo.getPlaceOfBirth(),
					addnInfo.getTravelDocumentType(), addnInfo.getVisaDocNumber(), addnInfo.getVisaDocPlaceOfIssue(),
					addnInfo.getVisaDocIssueDate(), addnInfo.getVisaApplicableCountry()));
			airTraveler.setFfid(addnInfo.getFfid());
		}

		airTraveler.setNationalityName(LCCUtils.getNationalityName(lcclientReservationPax.getNationalityCode()));
		airTraveler.setPassengerCategory(lcclientReservationPax.getPaxCategory());

		return airTraveler;
	}

	/**
	 * @param lcclientReservationPax
	 * @param travelerInfo
	 * @return
	 * @throws ModuleException
	 */
	public static AirTraveler transform(LCCClientReservationPax lcclientReservationPax, TravelerInfo travelerInfo)
			throws ModuleException {
		AirTraveler airTraveler = null;
		for (AirTraveler airTrvl : travelerInfo.getAirTraveler()) {
			if (airTrvl.getTravelerSequence().intValue() == lcclientReservationPax.getPaxSequence()) {
				return airTrvl;
			}
		}
		airTraveler = new AirTraveler();
		travelerInfo.getAirTraveler().add(airTraveler);

		String travelerRefNumber = lcclientReservationPax.getTravelerRefNumber();
		if (BeanUtils.nullHandler(travelerRefNumber).isEmpty()) {
			travelerRefNumber = PaxTypeUtils.travelerReference(lcclientReservationPax);
		}

		airTraveler.setTravelerRefNumber(travelerRefNumber);
		airTraveler.setTravelerSequence(lcclientReservationPax.getPaxSequence());
		airTraveler.setPassengerType(LCCClientCommonUtils.getLCCPaxTypeCode(lcclientReservationPax.getPaxType()));
		airTraveler.setPersonName(LCCClientApiUtils.transform(lcclientReservationPax.getTitle(),
				lcclientReservationPax.getFirstName(), lcclientReservationPax.getLastName()));
		airTraveler.setNationalityCode(LCCUtils.getNationalityIsoCode(lcclientReservationPax.getNationalityCode()));
		airTraveler.setBirthDate(XMLDataTypeUtil.getXMLGregorianCalendar(lcclientReservationPax.getDateOfBirth()));

		return airTraveler;
	}

	private static List<LCCClientReservationInsurance> transformInsurance(LCCAirBookRS lccAirBookRS) {
		List<LCCClientReservationInsurance> lccInsurances = new ArrayList<LCCClientReservationInsurance>();

		List<InsuranceRequest> insurances = lccAirBookRS.getLccReservation().getAirReservation().getTravelerInfo()
				.getSpecialReqDetails().getInsuranceRequests();

		LCCClientReservationInsurance lccInsurance = null;

		if (insurances != null && !insurances.isEmpty()) {
			for (InsuranceRequest insurance : insurances) {
				lccInsurance = new LCCClientReservationInsurance();
				lccInsurance.setInsuranceQuoteRefNumber(insurance.getInsuranceRefNumber() + "");
				lccInsurance.setPolicyCode(insurance.getPoliceCode());
				lccInsurance.setDateOfTravel(insurance.getDateOfTravel());
				lccInsurance.setDateOfReturn(insurance.getDateOfReturn());
				lccInsurance.setOrigin(insurance.getOrigin());
				lccInsurance.setDestination(insurance.getDestination());
				lccInsurance.setQuotedTotalPremium(insurance.getAmount());
				lccInsurance.setState(insurance.getState());
				lccInsurance.setAlertAutoCancellation(insurance.isAlertAutoCancellation());

				lccInsurances.add(lccInsurance);
			}
		}
		return lccInsurances;
	}

	public static LCCClientReservation transform(LCCAirBookRS lccAirBookRS, Collection<Integer> tempTnxIds)
			throws ModuleException {
		return transform(lccAirBookRS, null, tempTnxIds);
	}

	public static LCCClientReservation transform(LCCAirBookRS lccAirBookRS, Collection<Integer> tempTnxIds,
			LCCOfflinePaymentDetails offlinePaymentDetails) throws ModuleException {
		LCCClientReservation lccClientReservation = transform(lccAirBookRS, null, tempTnxIds);
		if (offlinePaymentDetails != null) {
			lccClientReservation.setLccOfflinePaymentDetails(offlinePaymentDetails);
		}
		return lccClientReservation;
	}

	public static LCCClientReservation transform(LCCAirBookRS lccAirBookRS, LCCClientPnrModesDTO pnrModesDTO,
			Collection<Integer> tempTnxIds) throws ModuleException {
		if (tempTnxIds == null) {
			tempTnxIds = new ArrayList<Integer>();
		}

		if (lccAirBookRS.getResponseAttributes().getSuccess() == null) {
			LCCError lccError = BeanUtils.getFirstElement(lccAirBookRS.getResponseAttributes().getErrors());
			if (LCCErrorCode.ERR_23_MAXICO_EXPOSED_RESERVATION_NOT_ELIGIBLE_FOR_ONHOLD.equals(lccError.getErrorCode())) {
				throw new ModuleException("airreservations.createonhold.noholdallowed");
			} else {
				throw new ModuleException(lccError.getErrorCode().value());
			}
		}

		LCCClientReservation lccClientReservation = new LCCClientReservation();
		
		lccClientReservation.setReservationInsurances(new ArrayList<LCCClientReservationInsurance>());
		List<LCCClientReservationInsurance> lccInsurances = transformInsurance(lccAirBookRS);
		lccClientReservation.getReservationInsurances().addAll(lccInsurances);

		// Carrier Name
		lccClientReservation.setCarrierName(new HashMap<String, String>());
		for (CarrierCodeName carrierCodeName : lccAirBookRS.getLccReservation().getCarrierCodeName()) {
			lccClientReservation.getCarrierName().put(carrierCodeName.getCarrierCode(), carrierCodeName.getCarrierName());
		}

		// CarrierPNR
		lccClientReservation.setCarrierPNR(new HashMap<String, String>());
		for (CarrierCodePnr carrierCodePnr : lccAirBookRS.getLccReservation().getCarrierCodePnr()) {
			lccClientReservation.getCarrierPNR().put(carrierCodePnr.getCarrierCode(), carrierCodePnr.getPnr());
		}

		// Marketing Airline code
		lccClientReservation.setMarketingCarrier(lccAirBookRS.getLccReservation().getMarketingCarrierCode());

		AirReservation lccAirReservation = lccAirBookRS.getLccReservation().getAirReservation();

		// LCCClientReservationAdminInfo
		CommonReservationAdminInfo adminInfo = transform(lccAirReservation.getAirReservationSummary().getAdminInfo());
		lccClientReservation.setAdminInfo(adminInfo);

		// LCCClientReservationPreferenceInfo
		CommonReservationPreferrenceInfo preferenceInfo = new CommonReservationPreferrenceInfo();
		if (lccAirReservation.getAirReservationSummary().getAirReservationPreferences() != null) {
			preferenceInfo = transform(lccAirReservation.getAirReservationSummary().getAirReservationPreferences());
		}
		lccClientReservation.setPreferrenceInfo(preferenceInfo);

		// LCCClient ReservationContactInfotransform
		CommonReservationContactInfo contactInfo = transform(lccAirReservation.getContactInfo());
		lccClientReservation.setContactInfo(contactInfo);

		// LCCClient GroupPNR
		String groupPNR = lccAirBookRS.getLccReservation().getGroupPnr();
		lccClientReservation.setPNR(groupPNR);
		lccClientReservation.setGroupPNR(true);

		// LCCClient Segments
		Set<LCCClientReservationSegment> lccClientReservationSegmentSet = transform(lccAirReservation.getAirItinerary(),
				lccAirReservation.getTicketing(), pnrModesDTO);
		transformSegementModifications(lccClientReservationSegmentSet);
		for (LCCClientReservationSegment lccClientReservationSegment : lccClientReservationSegmentSet) {
			lccClientReservationSegment.setLccReservation(lccClientReservation);
			lccClientReservation.addSegment(lccClientReservationSegment);
		}

		// LCCClient Passengers
		Set<LCCClientReservationPax> lccClientReservationPaxSet = transformIntoLCCClientReservationPaxSet(lccAirReservation,
				lccClientReservationSegmentSet);
		for (LCCClientReservationPax lccClientReservationPax : lccClientReservationPaxSet) {
			lccClientReservationPax.setLccReservation(lccClientReservation);
			lccClientReservation.addPassenger(lccClientReservationPax);
		}

		// LCCClient Status
		TicketingStatus ticketingStatus = lccAirReservation.getTicketing().getTicketingStatus();
		if (ticketingStatus == TicketingStatus.TICKETTYPE_TICKETED
				|| ticketingStatus == TicketingStatus.TICKETTYPE_PARTIAL_TICKETED) {
			lccClientReservation.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);
			lccClientReservation.setDisplayStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);
		} else if (ticketingStatus == TicketingStatus.TICKETTYPE_CANCELLED) {
			lccClientReservation.setStatus(ReservationInternalConstants.ReservationStatus.CANCEL);
			lccClientReservation.setDisplayStatus(ReservationInternalConstants.ReservationStatus.CANCEL);
		} else if (ticketingStatus == TicketingStatus.TICKETTYPE_ONHOLD_TICKETED) {
			lccClientReservation.setStatus(ReservationInternalConstants.ReservationStatus.ON_HOLD);
			lccClientReservation.setDisplayStatus(ReservationInternalConstants.ReservationStatus.ON_HOLD);
		}

		lccClientReservation.setBookingCategory(lccAirReservation.getBookingCategory());
		lccClientReservation.setOriginCountryOfCall(lccAirReservation.getOriginCountryOfCall());
		lccClientReservation.setNameChangeCount(lccAirReservation.getNameChangeCount());

		// Get Total Pax count by type (AdultCount[0], ChildCount[1], InfantCount[2])
		int totalPaxCountByType[] = getPaxCountByType(lccAirReservation.getAirReservationSummary().getPassengerTypeQuantity());
		lccClientReservation.setTotalPaxAdultCount(totalPaxCountByType[0]);
		lccClientReservation.setTotalPaxChildCount(totalPaxCountByType[1]);
		lccClientReservation.setTotalPaxInfantCount(totalPaxCountByType[2]);

		// LCCClient TotalTicketFare
		BigDecimal totalTicketFare = lccAirReservation.getPriceInfo().getFareType().getTotalFare();
		lccClientReservation.setTotalTicketFare(totalTicketFare);

		// LCCClient Fees
		List<FeeTO> feesList = transformFees(lccAirReservation.getPriceInfo().getFareType().getFees());
		lccClientReservation.setFees(feesList);

		// LCCClient Taxes
		List<TaxTO> taxesList = transformTaxes(lccAirReservation.getPriceInfo().getFareType().getTaxes(), null);
		lccClientReservation.setTaxes(taxesList);

		// LCCClient Surcharges
		List<SurchargeTO> surcharges = transformSurcharges(lccAirReservation.getPriceInfo().getFareType().getSurcharges(), null);
		lccClientReservation.setSurcharges(surcharges);

		// LCCClient Reservation Surcharges Summary
		lccClientReservation.setExternalChargersSummary(createExternalChargesSummary(surcharges));

		// LCCClient TotalAdjustmentCharge, TotalCancelCharge, TotalModificationCharge
		BigDecimal[] totalAdjCnxModCharges = transformIntoTotalAdjCnxModCharges(lccAirReservation.getPriceInfo().getFareType()
				.getFees());
		lccClientReservation.setTotalTicketAdjustmentCharge(totalAdjCnxModCharges[0]);
		lccClientReservation.setTotalTicketCancelCharge(totalAdjCnxModCharges[1]);
		lccClientReservation.setTotalTicketModificationCharge(totalAdjCnxModCharges[2]);

		// LCCClient TotalTicketSurCharge
		BigDecimal totalTicketSurCharge = lccAirReservation.getPriceInfo().getFareType().getTotalSurcharges();
		lccClientReservation.setTotalTicketSurCharge(totalTicketSurCharge);

		// LCCClient TotalTicketTaxCharge
		BigDecimal totalTicketTaxCharge = lccAirReservation.getPriceInfo().getFareType().getTotalTaxes();
		lccClientReservation.setTotalTicketTaxCharge(totalTicketTaxCharge);

		// LCCClient TotalAvailableBalance
		lccClientReservation.setTotalAvailableBalance(lccAirReservation.getPriceInfo().getFareType().getTotalBalance());

		// LCCClient TotalTicketPrice
		lccClientReservation.setTotalTicketPrice(lccAirReservation.getPriceInfo().getFareType().getTotalPrice());

		// LCCClient TotalPaidAmount
		lccClientReservation.setTotalPaidAmount(lccAirReservation.getPriceInfo().getFareType().getTotalPaymentAmount());

		// LCCClient Total Discount Amount
		lccClientReservation.setTotalDiscount(lccAirReservation.getPriceInfo().getFareType().getTotalDiscount());

		// LCCClient Actual Paid amounts (In paid currency)
		lccClientReservation.setLccClientPaymentHolder(transform(lccAirReservation.getFulfillment().getCustomerFulfillments()));

		// LCCClient UserNote
		String userNote = lccAirReservation.getLastUserNote().getNoteText();
		lccClientReservation.setLastUserNote(userNote);

		// LCCClient ZuluBookingTimestamp
		if (lccAirReservation.getTicketing().getTicketBookingDate() != null) {
			Date zuluBookingTimestamp = lccAirReservation.getTicketing().getTicketBookingDate();
			lccClientReservation.setZuluBookingTimestamp(zuluBookingTimestamp);
		}

		// LCCClient Release ZuluBookingTimestamp
		Date ticketTimeLimit = lccAirReservation.getTicketing().getTicketTimeLimit();
		Date zuluReleaseTimeStamp = ticketTimeLimit == null ? null : ticketTimeLimit;
		lccClientReservation.setZuluReleaseTimeStamp(zuluReleaseTimeStamp);

		// Version
		lccClientReservation.setVersion(lccAirBookRS.getLccReservation().getAirReservation().getVersion());

		// Set OND carrier
		List<LCCClientCarrierOndGroup> lccClientCarrierOndGrouping = transformOndGrp(lccAirBookRS.getLccReservation()
				.getAirReservation().getCarrierOndGrouping());
		lccClientReservation.setCarrierOndGrouping(lccClientCarrierOndGrouping);

		// Set Flight Segment Alert Information
		lccClientReservation.setAlertInfoTOs(transformFlightAlert(lccAirBookRS.getLccReservation().getAirReservation()
				.getAlertInfo()));

		// Set flexi alerts list
		lccClientReservation.setFlexiAlertInfoTOs(transformFlexiAlertInfo(lccAirBookRS.getLccReservation().getAirReservation()
				.getReservationPaxOndFlexibility(), lccClientReservation.getSegments(), lccClientReservation.getPassengers()));

		// Set bus segment alerts
		// lccClientReservation.setSegmentInfoTOs(transformSegmentAlertInfo(lccClientReservation.getSegments()));

		// Set Interline Modification Params
		lccClientReservation.setInterlineModificationParams(lccAirBookRS.getModificationParams());

		lccClientReservation.setInterlineChargeAdjustmentPrivileges(transformChargeAdjustmentPrivileges(lccAirBookRS
				.getChargeAdjustmentPrivileges()));

		lccClientReservation.setModifiableReservation(lccAirBookRS.getLccReservation().getAirReservation()
				.isModifiableReservation());

		// Sets refundable charge details
		lccClientReservation.setRefundableChargeDetails(transformRefundableChargeDetails(lccAirReservation
				.getRefundableChargeDetails()));

		ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
				ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, "payment success", null, null, null);

		// Set Auto Cancellation info
		lccClientReservation.setAutoCancellationInfo(transformAutoCancellationInfo(lccAirBookRS.getLccReservation()
				.getAirReservation().getLccAutoCancellationInfo()));

		// set Station Contact DTOs
		lccClientReservation.setStationContacts(transformStationContactDTOs(lccAirBookRS.getLccReservation().getAirReservation()
				.getLccStationContactDTOs()));

		// Set Applied Promotion info
		lccClientReservation.setLccPromotionInfoTO(transformLCCPromoInfo(lccAirBookRS.getLccReservation().getAirReservation()
				.getLccPromoInfo()));

		lccClientReservation.setNameChangeCutoffTime(lccAirBookRS.getLccReservation().getAirReservation()
				.getNameChangeCutoffTime() == null ? 0 : lccAirBookRS.getLccReservation().getAirReservation()
				.getNameChangeCutoffTime());
		lccClientReservation.setNameChangeThresholdTimePerStation(lccAirBookRS.getLccReservation().getAirReservation()
				.getNameChangeThresholdTimePerStation());

		if (!lccAirReservation.getServiceTaxes().isEmpty()) {
			Set<ServiceTaxContainer> serviceTaxContainers = LCCClientAvailUtil.populateServiceTaxContainer(lccAirReservation
					.getServiceTaxes());
			lccClientReservation.setApplicableServiceTaxes(serviceTaxContainers);
		}

		lccClientReservation.setBundledFareDTOs(transformToBundledFare(lccAirReservation.getLccBundledFares()));

		for (LCCClientReservationSegment lccResvSeg : lccClientReservation.getSegments()) {
			if (lccResvSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				lccResvSeg.setModifible(false);
			} else if (pnrModesDTO != null) {
				lccResvSeg.setModifible(PNRModificationAuthorizationUtils.isInterlineSegmentModifible(
						lccResvSeg.getModifyTillBufferDateTime(), lccResvSeg.getModifyTillFlightClosureDateTime(),
						pnrModesDTO.getAppIndicator(), pnrModesDTO.getMapPrivilegeIds(), lccResvSeg.isFlownSegment()));
			}
		}

		lccClientReservation.setTotalGOQUOAmount(lccAirReservation.getTotalGoquoAmount());

		lccClientReservation.setGdsId(lccAirReservation.getGdsId());
		lccClientReservation.setExternalPos(lccAirReservation.getExternalPos());
		lccClientReservation.setExternalRecordLocator(lccAirReservation.getExternalRecordLocator());
		
		lccClientReservation.setActionedByIBE(lccAirReservation.isActionedByIBE());
		lccClientReservation.setInfantPaymentSeparated(lccAirReservation.isInfantPaymentSeparated());
		populateNoShowRefundableInfo(lccClientReservation, lccAirBookRS);
		populateTaxInvoices(lccClientReservation, lccAirBookRS);

		return lccClientReservation;
	}

	public static Collection<ReservationListTO> transform(LCCSearchReservationRS lccSearchReservationRS) {
		Collection<ReservationListTO> reservationListTOs = new ArrayList<ReservationListTO>();
		for (ReservationSummary reservationSummary : lccSearchReservationRS.getReservationSummary()) {
			ReservationListTO reservationListTO = new ReservationListTO();
			reservationListTO.setPnrNo(reservationSummary.getPnrNo());
			reservationListTO.setOriginatorPnr(reservationSummary.getOriginatorPnr());
			reservationListTO.setPaxName(reservationSummary.getPaxName());
			reservationListTO.setPnrStatus(reservationSummary.getPnrStatus());
			reservationListTO.setReleaseTimeStamp(reservationSummary.getReleaseTimeStamp());
			reservationListTO.setRowNo(reservationSummary.getRowNo());
			reservationListTO.setMarketingAirlineCode(reservationSummary.getDryReservationSummary().getMarketingAirlineCode());
			reservationListTO.setAirlineCode(reservationSummary.getDryReservationSummary().getGroupReservationCarrier()
					.getAirlineCode());
			reservationListTO.setInterlineAgreementId(reservationSummary.getDryReservationSummary().getInterlineAgreementId());
			Collection<FlightInfoTO> flightInfoTOs = new ArrayList<FlightInfoTO>();
			for (FlightInfo flightInfo : reservationSummary.getFlightInfo()) {
				if (flightInfo.getSubStatus() != null
						&& flightInfo.getSubStatus().equals(ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED)) {
					continue;
				}
				FlightInfoTO flightInfoTO = new FlightInfoTO();
				flightInfoTO.setAirLine(flightInfo.getAirLine());
				flightInfoTO.setFlightNo(flightInfo.getFlightNo());
				flightInfoTO.setDepartureDate(flightInfo.getDepartureDate());
				flightInfoTO.setDepartureTime(flightInfo.getDepartureTime());
				flightInfoTO.setDepartureDateLong(flightInfo.getDepartureDateLong());
				flightInfoTO.setArrivalDate(flightInfo.getArrivalDate());
				flightInfoTO.setArrivalTime(flightInfo.getArrivalTime());
				flightInfoTO.setOrignNDest(flightInfo.getOrignNDest());
				flightInfoTO.setPaxCount(flightInfo.getPaxCount());
				flightInfoTO.setStatus(flightInfo.getStatus());
				flightInfoTO.setDisplayStatus(flightInfo.getStatus());
				flightInfoTO.setCosDes(flightInfo.getCosDescription());
				flightInfoTO.setOpenReturnSegment(flightInfo.isIsOpenReturnSegment());
				flightInfoTO.setArrivalTerminal(StringUtil.getNotEmptyString(null));
				flightInfoTO.setDepartureTerminal(StringUtil.getNotEmptyString(null));
				flightInfoTO.setFlightModelDescription(StringUtil.getNotEmptyString(null));
				flightInfoTO.setNoOfStops(StringUtil.getNotEmptyString(null));
				flightInfoTO.setFlightStopOverDuration(StringUtil.getNotEmptyString(null));
				flightInfoTO.setFlightDuration(StringUtil.getNotEmptyString(null));
				flightInfoTO.setRemarks(StringUtil.getNotEmptyString(null));
				flightInfoTOs.add(flightInfoTO);
			}
			reservationListTO.setFlightInfo(flightInfoTOs);
			reservationListTOs.add(reservationListTO);
		}
		return reservationListTOs;
	}

	public static List<LCCClientAlertInfoTO> transformFlexiAlertInfo(List<PaxOndFlexibility> paxOndFlexibilityList,
			Set<LCCClientReservationSegment> reservationSegments, Set<LCCClientReservationPax> reservationPaxs) {
		Map<Integer, List<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap = new HashMap<Integer, List<ReservationPaxOndFlexibilityDTO>>();
		List<ReservationPaxOndFlexibilityDTO> reservationPaxOndFlexibilityDTOs = null;

		Set<Integer> ppfIdList = null;
		if (paxOndFlexibilityList != null) {
			ppfIdList = new HashSet<Integer>();
			for (PaxOndFlexibility reservationPaxOndFlexibility : paxOndFlexibilityList) {
				ppfIdList.add(reservationPaxOndFlexibility.getPpfId());
			}
		}

		if (ppfIdList != null) {
			for (int ppfId : ppfIdList) {
				reservationPaxOndFlexibilityDTOs = new ArrayList<ReservationPaxOndFlexibilityDTO>();
				for (PaxOndFlexibility reservationPaxOndFlexibility : paxOndFlexibilityList) {
					if (ppfId == reservationPaxOndFlexibility.getPpfId()) {
						if (reservationPaxOndFlexibility.getStatus().equals("ACT")
								&& reservationPaxOndFlexibility.getAvailableCount() > 0) {
							ReservationPaxOndFlexibilityDTO paxOndFlexibilityDTO = new ReservationPaxOndFlexibilityDTO();
							paxOndFlexibilityDTO.setAvailableCount(reservationPaxOndFlexibility.getAvailableCount());
							paxOndFlexibilityDTO.setCutOverBufferInMins(reservationPaxOndFlexibility.getCutOverBufferInMins());
							paxOndFlexibilityDTO.setDescription(reservationPaxOndFlexibility.getDescription());
							paxOndFlexibilityDTO.setFlexibilityTypeId(reservationPaxOndFlexibility.getFlexibilityTypeId());
							paxOndFlexibilityDTO.setFlexiRateId(reservationPaxOndFlexibility.getFlexibilityTypeId());
							paxOndFlexibilityDTO.setPpfId(reservationPaxOndFlexibility.getPpfId());
							paxOndFlexibilityDTO.setPpOndFlxId(reservationPaxOndFlexibility.getPpOndFlxId());
							paxOndFlexibilityDTO.setStatus(reservationPaxOndFlexibility.getStatus());
							paxOndFlexibilityDTO.setUtilizedCount(reservationPaxOndFlexibility.getUtilizedCount());
							paxOndFlexibilityDTO.setFlexiRuleID(reservationPaxOndFlexibility.getFlexiRuleID());
							reservationPaxOndFlexibilityDTOs.add(paxOndFlexibilityDTO);
						}
					}
				}
				ondFlexibilitiesMap.put(ppfId, reservationPaxOndFlexibilityDTOs);
			}
		}

		// Fill flexi info list
		List<LCCClientAlertInfoTO> flexiAlertInfoList = new ArrayList<LCCClientAlertInfoTO>();
		for (LCCClientReservationSegment reservationSegment : reservationSegments) {
			if (!reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				List<ReservationPaxOndFlexibilityDTO> segmentFlexiList = segmentFlexiList(ondFlexibilitiesMap,
						reservationSegment.getBookingFlightSegmentRefNumber(), reservationPaxs);
				if (segmentFlexiList != null && segmentFlexiList.size() > 0) {
					List<LCCClientAlertTO> flexiAlertList = transformAlertTO(sortOndFlexi(segmentFlexiList));
					LCCClientAlertInfoTO alertInfo = new LCCClientAlertInfoTO();
					alertInfo.setAlertTO(flexiAlertList);
					alertInfo.setFlightSegmantRefNumber(reservationSegment.getBookingFlightSegmentRefNumber());
					reservationSegment.setFlexiRuleID(segmentFlexiList.iterator().next().getFlexiRuleID());
					flexiAlertInfoList.add(alertInfo);
				}
			}
		}

		return flexiAlertInfoList;
	}

	public static List<ReservationPaxOndFlexibilityDTO> segmentFlexiList(
			Map<Integer, List<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap, String pnrSegId,
			Set<LCCClientReservationPax> reservationPaxs) {
		Integer ppfId = retrivePnrPaxFareId(pnrSegId, reservationPaxs);
		return ondFlexibilitiesMap.get(ppfId);
	}

	private static int retrivePnrPaxFareId(String pnrSegId, Set<LCCClientReservationPax> reservationPaxs) {
		Integer ppfId = -1;
		if (reservationPaxs != null) {
			for (LCCClientReservationPax pax : reservationPaxs) {
				if (!ReservationInternalConstants.PassengerType.INFANT.equals(pax.getPaxType()) && pax.getFares() != null) {
					for (FareTO fare : pax.getFares()) {
						for (String segRPH : fare.getPnrSegIds()) {
							if (segRPH.equals(pnrSegId)) {
								ppfId = fare.getPnrPaxFareId();
								return ppfId;
							}
						}
					}
				}
			}
		}
		return ppfId;
	}

	public static List<ReservationPaxOndFlexibilityDTO> sortOndFlexi(List<ReservationPaxOndFlexibilityDTO> segmentFlexiList) {
		// Modification and Cancellation are saved against type ids 1 & 2
		Collections.sort(segmentFlexiList, new Comparator<ReservationPaxOndFlexibilityDTO>() {
			@Override
			public int compare(ReservationPaxOndFlexibilityDTO flexiDTO1, ReservationPaxOndFlexibilityDTO flexiDTO2) {
				return (new Integer(flexiDTO1.getFlexibilityTypeId())).compareTo(new Integer(flexiDTO2.getFlexibilityTypeId()));
			}
		});
		return segmentFlexiList;
	}

	public static List<LCCClientAlertTO> transformAlertTO(List<ReservationPaxOndFlexibilityDTO> segmentFlexiList) {
		List<LCCClientAlertTO> clientAlertList = new ArrayList<LCCClientAlertTO>();
		for (ReservationPaxOndFlexibilityDTO paxOndFlexi : segmentFlexiList) {
			LCCClientAlertTO alertTO = new LCCClientAlertTO();
			alertTO.setContent(String.valueOf(paxOndFlexi.getAvailableCount()));
			alertTO.setAlertId(paxOndFlexi.getFlexibilityTypeId());
			clientAlertList.add(alertTO);
		}
		LCCClientAlertTO alertTO = new LCCClientAlertTO();// Used for additional details
		alertTO.setContent(getAdditionalFlexiDetails(segmentFlexiList));
		alertTO.setAlertId(new Integer(0));// Additional details against 0
		clientAlertList.add(alertTO);

		return clientAlertList;
	}

	public static String getAdditionalFlexiDetails(List<ReservationPaxOndFlexibilityDTO> segmentFlexiList) {
		for (ReservationPaxOndFlexibilityDTO paxOndFlexi : segmentFlexiList) {
			return String.valueOf(paxOndFlexi.getCutOverBufferInMins() / 60);
		}
		return null;
	}

	public static CommonReservationAdminInfo transform(AdminInfo adminInfo) {

		CommonReservationAdminInfo lccClientReservationAdminInfo = new CommonReservationAdminInfo();
		lccClientReservationAdminInfo.setOriginAgentCode(adminInfo.getOriginAgentCode());
		lccClientReservationAdminInfo.setOriginIpAddress(adminInfo.getOriginIpAddress());
		lccClientReservationAdminInfo.setOriginCountryCode(adminInfo.getOriginCountryCode());
		lccClientReservationAdminInfo.setOriginChannelId(adminInfo.getOriginChannelID());
		lccClientReservationAdminInfo.setOriginSalesTerminal(adminInfo.getOriginSalesTerminal());
		lccClientReservationAdminInfo.setOwnerAgentCode(adminInfo.getOwnerAgentCode());
		lccClientReservationAdminInfo.setOwnerChannelId(adminInfo.getOwnerChannelID());
		lccClientReservationAdminInfo.setLastSalesTerminal(adminInfo.getLastSalesTerminal());

		AgentContactInfo ownerAgentContactInfo = adminInfo.getOwnerAgentContactInfo();
		LCCClientReservationOwnerAgentContactInfo lccOwnerAgentContactInfo = new LCCClientReservationOwnerAgentContactInfo();
		lccOwnerAgentContactInfo.setAgentCarrier(ownerAgentContactInfo.getAgentCarrier());
		lccOwnerAgentContactInfo.setAgentEmail(ownerAgentContactInfo.getAgentEmail());
		lccOwnerAgentContactInfo.setAgentName(ownerAgentContactInfo.getAgentName());
		lccOwnerAgentContactInfo.setAgentTelephone(ownerAgentContactInfo.getAgentTelephone());
		lccClientReservationAdminInfo.setOwnerAgentContactInfo(lccOwnerAgentContactInfo);

		return lccClientReservationAdminInfo;
	}

	public static CommonReservationPreferrenceInfo transform(AirReservationPreferences preferenceInfo) {

		CommonReservationPreferrenceInfo lccClientReservationPreferenceInfo = new CommonReservationPreferrenceInfo();
		if (preferenceInfo != null) {
			lccClientReservationPreferenceInfo.setPreferredLanguage(preferenceInfo.getPreferredLanguage());
		}

		return lccClientReservationPreferenceInfo;
	}

	public static List<LCCClientCarrierOndGroup> transformOndGrp(List<CarrierOndGroup> carrierOndGrouping) {
		List<LCCClientCarrierOndGroup> lccClientCarrierOndGrouping = new ArrayList<LCCClientCarrierOndGroup>();
		if (carrierOndGrouping != null) {
			for (CarrierOndGroup carrierOndGroup : carrierOndGrouping) {
				LCCClientCarrierOndGroup lccCarrierOndGroup = new LCCClientCarrierOndGroup();
				lccCarrierOndGroup.setCarrierCode(carrierOndGroup.getCarrierCode());
				lccCarrierOndGroup.setSegmentCode(carrierOndGroup.getSegmentCode());
				lccCarrierOndGroup.setCarrierPnrSegIds(carrierOndGroup.getCarrierPnrSegIds());
				lccCarrierOndGroup.setCarrierOndGroupRPH(carrierOndGroup.getCarrierOndGroupRPH());
				lccCarrierOndGroup.setDepartureDateTime(carrierOndGroup.getDepartureDateTime());
				lccCarrierOndGroup.setStatus(carrierOndGroup.getStatus());
				lccCarrierOndGroup.setTotalCharges(carrierOndGroup.getTotalChargesForDisplay());
				lccClientCarrierOndGrouping.add(lccCarrierOndGroup);
			}
		}
		return lccClientCarrierOndGrouping;
	}

	public static CommonReservationContactInfo transform(ContactInfo contactInfo) throws ModuleException {

		CommonReservationContactInfo commonReservationContactInfo = new CommonReservationContactInfo();

		List<String> addressLineList = contactInfo.getAddress().getAddressLine();
		int addressSize = addressLineList.size();

		String city = contactInfo.getAddress().getCityName();
		String countryCode = contactInfo.getAddress().getCountry().getCountryCode();
		String email = contactInfo.getEmail();

		Telephone telephone = contactInfo.getFax();
		String fax = telephone == null ? "" : telephone.getPhoneNumber();
		telephone = contactInfo.getMobile();
		String mobileNo = telephone == null ? "" : telephone.getPhoneNumber();
		telephone = contactInfo.getTelephone();
		String phoneNo = telephone == null ? "" : telephone.getPhoneNumber();

		String firstName = (contactInfo.getPersonName() != null) ? contactInfo.getPersonName().getFirstName() : "";
		String lastName = (contactInfo.getPersonName() != null) ? contactInfo.getPersonName().getSurName() : "";
		String nationality = contactInfo.getNationalityCode();

		String streetAddress1 = addressSize > 0 ? addressLineList.get(0) : "";
		String streetAddress2 = addressSize > 1 ? addressLineList.get(1) : "";
		String title = (contactInfo.getPersonName() != null) ? contactInfo.getPersonName().getTitle() : "";
		String state = contactInfo.getAddress().getState();
		String zipCode = contactInfo.getZipCode();
		String prefLang = (contactInfo.getPreferredLanguage() == null) ? Locale.ENGLISH.toString() : contactInfo
				.getPreferredLanguage();

		commonReservationContactInfo.setCity(city);
		commonReservationContactInfo.setCountryCode(countryCode);
		commonReservationContactInfo.setEmail(email);
		commonReservationContactInfo.setFax(fax);
		commonReservationContactInfo.setFirstName(firstName);
		commonReservationContactInfo.setLastName(lastName);
		commonReservationContactInfo.setMobileNo(mobileNo);
		commonReservationContactInfo.setPhoneNo(phoneNo);
		commonReservationContactInfo.setStreetAddress1(streetAddress1);
		commonReservationContactInfo.setStreetAddress2(streetAddress2);
		commonReservationContactInfo.setTitle(title);
		commonReservationContactInfo.setNationalityCode(LCCUtils.getNationalityCode(nationality));
		commonReservationContactInfo.setState(state);
		commonReservationContactInfo.setZipCode(zipCode);
		commonReservationContactInfo.setPreferredLanguage(prefLang);

		// Emergency Contact
		if (contactInfo.getEmgnPersonName() != null) {
			commonReservationContactInfo.setEmgnTitle(contactInfo.getEmgnPersonName().getTitle());
			commonReservationContactInfo.setEmgnFirstName(contactInfo.getEmgnPersonName().getFirstName());
			commonReservationContactInfo.setEmgnLastName(contactInfo.getEmgnPersonName().getSurName());
		}

		String emgnPhone = (contactInfo.getEmgnTelephone() != null) ? contactInfo.getEmgnTelephone().getPhoneNumber() : "";
		commonReservationContactInfo.setEmgnPhoneNo(emgnPhone);
		commonReservationContactInfo.setEmgnEmail(contactInfo.getEmgnEmail());
		
		commonReservationContactInfo.setTaxRegNo(contactInfo.getTaxRegNo());
		commonReservationContactInfo.setSendPromoEmail(contactInfo.isSendPromoEmail());

		return commonReservationContactInfo;
	}

	public static ReservationContactInfo transformToReservationContact(
			CommonReservationContactInfo lccClientReservationContactInfo) {

		ReservationContactInfo reservationContactInfo = new ReservationContactInfo();

		String city = lccClientReservationContactInfo.getCity();
		String countryCode = lccClientReservationContactInfo.getCountryCode();
		String email = lccClientReservationContactInfo.getEmail();

		String fax = lccClientReservationContactInfo.getFax();
		String mobileNo = lccClientReservationContactInfo.getMobileNo();
		String phoneNo = lccClientReservationContactInfo.getPhoneNo();

		String firstName = lccClientReservationContactInfo.getFirstName();
		String lastName = lccClientReservationContactInfo.getLastName();

		String streetAddress1 = lccClientReservationContactInfo.getStreetAddress1();
		String streetAddress2 = lccClientReservationContactInfo.getStreetAddress2();
		String title = lccClientReservationContactInfo.getTitle();
		String prefLang = (lccClientReservationContactInfo.getPreferredLanguage() == null)
				? Locale.ENGLISH.toString()
				: lccClientReservationContactInfo.getPreferredLanguage();

		reservationContactInfo.setCity(city);
		reservationContactInfo.setCountryCode(countryCode);
		reservationContactInfo.setEmail(email);
		reservationContactInfo.setFax(fax);
		reservationContactInfo.setFirstName(firstName);
		reservationContactInfo.setLastName(lastName);
		reservationContactInfo.setMobileNo(mobileNo);
		reservationContactInfo.setPhoneNo(phoneNo);
		reservationContactInfo.setStreetAddress1(streetAddress1);
		reservationContactInfo.setStreetAddress2(streetAddress2);
		reservationContactInfo.setTitle(title);
		reservationContactInfo.setZipCode(lccClientReservationContactInfo.getZipCode());
		reservationContactInfo.setPreferredLanguage(prefLang);

		// Emergency Contact
		reservationContactInfo.setEmgnTitle(lccClientReservationContactInfo.getEmgnTitle());
		reservationContactInfo.setEmgnFirstName(lccClientReservationContactInfo.getEmgnFirstName());
		reservationContactInfo.setEmgnLastName(lccClientReservationContactInfo.getEmgnLastName());
		reservationContactInfo.setEmgnPhoneNo(lccClientReservationContactInfo.getEmgnPhoneNo());
		reservationContactInfo.setEmgnEmail(lccClientReservationContactInfo.getEmgnEmail());

		reservationContactInfo.setTaxRegNo(lccClientReservationContactInfo.getTaxRegNo());
		
		return reservationContactInfo;
	}

	private static Set<LCCClientReservationPax> transformIntoLCCClientReservationPaxSet(AirReservation lccAirReservation,
			Set<LCCClientReservationSegment> lccClientReservationSegmentSet) throws ModuleException {

		Map<String, LCCClientReservationSegment> mapSegments = new HashMap<String, LCCClientReservationSegment>();
		Iterator<LCCClientReservationSegment> segItr = lccClientReservationSegmentSet.iterator();
		Map<Integer, String> pnrSegmentSubStatus = new HashMap<Integer, String>();

		while (segItr.hasNext()) {
			LCCClientReservationSegment val = segItr.next();
			mapSegments.put(val.getFlightSegmentRefNumber(), val);
			pnrSegmentSubStatus.put(val.getPnrSegID(), val.getSubStatus());
		}

		// pnr segment map
		Map<String, LCCClientReservationSegment> mapPnrSegments = new HashMap<String, LCCClientReservationSegment>();
		Iterator<LCCClientReservationSegment> pnrSegItr = lccClientReservationSegmentSet.iterator();
		while (pnrSegItr.hasNext()) {
			LCCClientReservationSegment val = pnrSegItr.next();
			mapPnrSegments.put(val.getBookingFlightSegmentRefNumber(), val);
		}

		// AirTraveler
		Map<Integer, AirTraveler> mapTraveler = new HashMap<Integer, AirTraveler>();
		for (AirTraveler airTraveler : lccAirReservation.getTravelerInfo().getAirTraveler()) {
			mapTraveler.put(PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber()), airTraveler);
		}

		// Ancillaries
		SpecialReqDetails lccSpecialReq = lccAirReservation.getTravelerInfo().getSpecialReqDetails();
		Map<Integer, List<BookMealRequest>> mapMeals = new HashMap<Integer, List<BookMealRequest>>();
		if (lccSpecialReq.getMealsRequests() != null) {
			for (BookMealRequest meal : lccSpecialReq.getMealsRequests()) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(meal.getTravelerRefNumber());
				if (!mapMeals.containsKey(paxSeq)) {
					mapMeals.put(paxSeq, new ArrayList<BookMealRequest>());
				}
				mapMeals.get(paxSeq).add(meal);
			}
		}
		Map<Integer, List<BookBaggageRequest>> mapBaggages = new HashMap<Integer, List<BookBaggageRequest>>();
		if (lccSpecialReq.getBaggagesRequests() != null) {
			for (BookBaggageRequest baggage : lccSpecialReq.getBaggagesRequests()) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(baggage.getTravelerRefNumber());
				if (!mapBaggages.containsKey(paxSeq)) {
					mapBaggages.put(paxSeq, new ArrayList<BookBaggageRequest>());
				}
				mapBaggages.get(paxSeq).add(baggage);
			}
		}

		Map<Integer, List<SpecialServiceRequest>> mapSSRs = new HashMap<Integer, List<SpecialServiceRequest>>();
		if (lccSpecialReq.getSpecialServiceRequests() != null) {
			for (SpecialServiceRequest ssr : lccSpecialReq.getSpecialServiceRequests()) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(ssr.getTravelerRefNumber());
				if (!mapSSRs.containsKey(paxSeq)) {
					mapSSRs.put(paxSeq, new ArrayList<SpecialServiceRequest>());
				}
				mapSSRs.get(paxSeq).add(ssr);
			}
		}

		Map<Integer, List<SeatRequest>> mapSeats = new HashMap<Integer, List<SeatRequest>>();
		if (lccSpecialReq.getSeatRequests() != null) {
			for (SeatRequest seat : lccSpecialReq.getSeatRequests()) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(seat.getTravelerRefNumber());
				if (!mapSeats.containsKey(paxSeq)) {
					mapSeats.put(paxSeq, new ArrayList<SeatRequest>());
				}
				mapSeats.get(paxSeq).add(seat);
			}
		}

		Map<Integer, List<AirportServiceRequest>> mapAirportServices = new HashMap<Integer, List<AirportServiceRequest>>();
		if (lccSpecialReq.getAirportServiceRequests() != null) {
			for (AirportServiceRequest airportServiceRequest : lccSpecialReq.getAirportServiceRequests()) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(airportServiceRequest.getTravelerRefNumber());
				if (!mapAirportServices.containsKey(paxSeq)) {
					mapAirportServices.put(paxSeq, new ArrayList<AirportServiceRequest>());
				}
				mapAirportServices.get(paxSeq).add(airportServiceRequest);
			}
		}

		// PerPaxPriceInfo
		Map<Integer, PerPaxPriceInfo> mapPerPaxPrice = new HashMap<Integer, PerPaxPriceInfo>();
		for (PerPaxPriceInfo perPaxPriceInfo : lccAirReservation.getPriceInfo().getPerPaxPriceInfo()) {
			mapPerPaxPrice.put(PaxTypeUtils.getPaxSeq(perPaxPriceInfo.getTravelerRefNumber()), perPaxPriceInfo);
		}

		// Note : we don't need CarrierWiseFulfillment
		// CustomerFulfillment
		Map<Integer, TravelerFulfillment> mapCustomerFulfillment = new HashMap<Integer, TravelerFulfillment>();
		for (TravelerFulfillment travelerFulfillment : lccAirReservation.getFulfillment().getCustomerFulfillments()) {
			mapCustomerFulfillment.put(PaxTypeUtils.getPaxSeq(travelerFulfillment.getTravelerRefNumber()), travelerFulfillment);
		}

		// Set of all Pax Reservations
		Set<LCCClientReservationPax> lccClientReservationPaxSet = new HashSet<LCCClientReservationPax>();

		for (Entry<Integer, AirTraveler> entry : mapTraveler.entrySet()) {

			Integer paxSeq = entry.getKey();

			AirTraveler airTraveler = entry.getValue();
			PerPaxPriceInfo perPaxPriceInfo = mapPerPaxPrice.get(paxSeq);
			TravelerFulfillment customerTravelerFulfillment = mapCustomerFulfillment.get(paxSeq);

			LCCClientReservationPax lccClientReservationPax = new LCCClientReservationPax();

			// LCCPax TravelerRef
			lccClientReservationPax.setTravelerRefNumber(airTraveler.getTravelerRefNumber());

			// LCCPax PaxSequence
			lccClientReservationPax.setPaxSequence(paxSeq);

			// LCCPax DOB
			XMLGregorianCalendar dateOfBirthXML = airTraveler.getBirthDate();
			Date dateOfBirth = dateOfBirthXML == null ? null : dateOfBirthXML.toGregorianCalendar().getTime();
			lccClientReservationPax.setDateOfBirth(dateOfBirth);

			// LCCPax Name
			lccClientReservationPax.setTitle(airTraveler.getPersonName().getTitle());
			lccClientReservationPax.setFirstName(airTraveler.getPersonName().getFirstName());
			lccClientReservationPax.setLastName(airTraveler.getPersonName().getSurName());

			// LCCPax Nationality
			lccClientReservationPax.setNationalityCode(LCCUtils.getNationalityCode(airTraveler.getNationalityCode()));

			// LCCPax Type
			lccClientReservationPax
					.setPaxType(LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(airTraveler.getPassengerType()));

			// Set Auto Cancellation Alerts
			lccClientReservationPax.setAlertAutoCancellation(airTraveler.isAlertAutoCancellation());

			//Lcc Client alrady blacklisted in reservation
			lccClientReservationPax.setBlacklisted(airTraveler.isBlacklisted());
			
			// LCC Additional Pax Info
			LCCClientReservationAdditionalPax addnInfo = new LCCClientReservationAdditionalPax();

			if (airTraveler.getFormOfIdentification() != null) {
				addnInfo.setPassportNo(airTraveler.getFormOfIdentification().getFoidNumber());
				if (airTraveler.getFormOfIdentification().getPsptExpiry() != null) {
					addnInfo.setPassportExpiry(airTraveler.getFormOfIdentification().getPsptExpiry().toGregorianCalendar()
							.getTime());
				}
				addnInfo.setPassportIssuedCntry(airTraveler.getFormOfIdentification().getPsptIssuedCntry());
				addnInfo.setPlaceOfBirth(airTraveler.getFormOfIdentification().getPlaceOfBirth());
				addnInfo.setTravelDocumentType(airTraveler.getFormOfIdentification().getTravelDocumentType());
				addnInfo.setVisaDocNumber(airTraveler.getFormOfIdentification().getVisaDocNumber());
				addnInfo.setVisaDocPlaceOfIssue(airTraveler.getFormOfIdentification().getVisaDocPlaceOfIssue());
				if (airTraveler.getFormOfIdentification().getVisaDocIssueDate() != null) {
					addnInfo.setVisaDocIssueDate(airTraveler.getFormOfIdentification().getVisaDocIssueDate()
							.toGregorianCalendar().getTime());
				}
				addnInfo.setVisaApplicableCountry(airTraveler.getFormOfIdentification().getVisaApplicableCountry());
			}

			addnInfo.setFfid(airTraveler.getFfid());
			lccClientReservationPax.setLccClientAdditionPax(addnInfo);

			List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
			Set<String> segFltRefSet = new HashSet<String>();

			Map<String, SeatRequest> mapPaxSegSeats = new HashMap<String, SeatRequest>();
			if (mapSeats.get(paxSeq) != null) {
				for (SeatRequest sReq : mapSeats.get(paxSeq)) {
					String fltRef = sReq.getFlightRefNumber();
					segFltRefSet.add(fltRef);
					mapPaxSegSeats.put(fltRef, sReq);
				}
			}
			Map<String, List<BookMealRequest>> mapPaxSegMeals = new HashMap<String, List<BookMealRequest>>();
			if (mapMeals.get(paxSeq) != null) {
				for (BookMealRequest mReq : mapMeals.get(paxSeq)) {
					String fltRef = mReq.getFlightRefNumber();
					if (!mapPaxSegMeals.containsKey(fltRef)) {
						segFltRefSet.add(fltRef);
						mapPaxSegMeals.put(fltRef, new ArrayList<BookMealRequest>());
					}
					mapPaxSegMeals.get(fltRef).add(mReq);
				}
			}
			Map<String, List<BookBaggageRequest>> mapPaxSegBaggages = new HashMap<String, List<BookBaggageRequest>>();
			if (mapBaggages.get(paxSeq) != null) {
				for (BookBaggageRequest bReq : mapBaggages.get(paxSeq)) {
					String fltRef = bReq.getFlightRefNumber();
					if (!mapPaxSegBaggages.containsKey(fltRef)) {
						segFltRefSet.add(fltRef);
						mapPaxSegBaggages.put(fltRef, new ArrayList<BookBaggageRequest>());
					}
					mapPaxSegBaggages.get(fltRef).add(bReq);
				}
			}
			Map<String, List<SpecialServiceRequest>> mapPaxSegSSRs = new HashMap<String, List<SpecialServiceRequest>>();
			if (mapSSRs.get(paxSeq) != null) {
				for (SpecialServiceRequest ssrReq : mapSSRs.get(paxSeq)) {
					String fltRef = ssrReq.getFlightRefNumber();
					if (!mapPaxSegSSRs.containsKey(fltRef)) {
						segFltRefSet.add(fltRef);
						mapPaxSegSSRs.put(fltRef, new ArrayList<SpecialServiceRequest>());
					}
					mapPaxSegSSRs.get(fltRef).add(ssrReq);
				}
			}

			Map<String, List<AirportServiceRequest>> mapPaxSegAPS = new HashMap<String, List<AirportServiceRequest>>();
			if (mapAirportServices.get(paxSeq) != null) {
				for (AirportServiceRequest apsReq : mapAirportServices.get(paxSeq)) {
					String fltRef = apsReq.getFlightRefNumber();
					if (!mapPaxSegAPS.containsKey(fltRef)) {
						segFltRefSet.add(fltRef);
						mapPaxSegAPS.put(fltRef, new ArrayList<AirportServiceRequest>());
					}
					mapPaxSegAPS.get(fltRef).add(apsReq);
				}
			}

			for (String fltRef : segFltRefSet) {
				LCCSelectedSegmentAncillaryDTO anciSeg = new LCCSelectedSegmentAncillaryDTO();
				LCCAirSeatDTO lccSeat = new LCCAirSeatDTO();
				SeatRequest seat = mapPaxSegSeats.get(fltRef);
				if (seat != null) {
					lccSeat.setSeatNumber(seat.getSeatNumber());
					lccSeat.setSeatCharge(seat.getSeatCharge());
					lccSeat.setAlertAutoCancellation(seat.isAlertAutoCancellation());
				}

				LCCClientReservationSegment segObj = mapSegments.get(fltRef);
				FlightSegmentTO flightSegment = new FlightSegmentTO();
				flightSegment.setSegmentCode(segObj.getSegmentCode());
				flightSegment.setDepartureDateTime(segObj.getDepartureDate());
				flightSegment.setArrivalDateTime(segObj.getArrivalDate());
				flightSegment.setFlightNumber(segObj.getFlightNo());
				flightSegment.setCabinClassCode(segObj.getCabinClassCode());
				flightSegment.setFlightRefNumber(segObj.getFlightSegmentRefNumber());
				flightSegment.setPnrSegId(segObj.getPnrSegID().toString());
				flightSegment.setSegmentSequence(segObj.getSegmentSeq());
				flightSegment.setFlownSegment(segObj.isFlownSegment());

				List<LCCMealDTO> lccMealList = new ArrayList<LCCMealDTO>();
				if (mapPaxSegMeals.get(fltRef) != null) {
					for (BookMealRequest mealReq : mapPaxSegMeals.get(fltRef)) {
						LCCMealDTO meal = new LCCMealDTO();
						meal.setMealCode(mealReq.getMealCode());
						meal.setMealCategoryID(mealReq.getMealCategoryID());
						meal.setMealName(mealReq.getMealName());
						meal.setMealCharge(mealReq.getMealCharge());
						meal.setSoldMeals(1);
						meal.setAlertAutoCancellation(mealReq.isAlertAutoCancellation());
						lccMealList.add(meal);
					}
				}

				transformMealsWithQuantity(lccMealList);// to apply the quantities with multimeals

				List<LCCBaggageDTO> lccBaggageList = new ArrayList<LCCBaggageDTO>();
				if (mapPaxSegBaggages.get(fltRef) != null) {
					for (BookBaggageRequest baggageReq : mapPaxSegBaggages.get(fltRef)) {
						LCCBaggageDTO baggage = new LCCBaggageDTO();
						baggage.setBaggageName(baggageReq.getBaggageName());
						baggage.setBaggageDescription(baggageReq.getBaggageDescription());
						baggage.setBaggageCharge(baggageReq.getBaggageCharge());
						baggage.setAlertAutoCancellation(baggageReq.isAlertAutoCancellation());
						baggage.setOndBaggageChargeId(String.valueOf(baggageReq.getBaggageTemplateChargeId()));
						lccBaggageList.add(baggage);
					}
				}

				List<LCCSpecialServiceRequestDTO> lccSSRList = new ArrayList<LCCSpecialServiceRequestDTO>();
				if (mapPaxSegSSRs.get(fltRef) != null) {
					for (SpecialServiceRequest ssrReq : mapPaxSegSSRs.get(fltRef)) {
						LCCSpecialServiceRequestDTO ssr = new LCCSpecialServiceRequestDTO();
						ssr.setDescription(ssrReq.getDescription());
						ssr.setSsrCode(ssrReq.getSsrCode());
						ssr.setCharge(ssrReq.getCharge());
						ssr.setText(ssrReq.getText());
						ssr.setAlertAutoCancellation(ssrReq.isAlertAutoCancellation());
						lccSSRList.add(ssr);
					}
				}

				List<LCCAirportServiceDTO> lccAPSList = new ArrayList<LCCAirportServiceDTO>();
				if (mapPaxSegAPS.get(fltRef) != null) {
					for (AirportServiceRequest apsReq : mapPaxSegAPS.get(fltRef)) {
						LCCAirportServiceDTO aps = new LCCAirportServiceDTO();
						aps.setSsrCode(apsReq.getSsrCode());
						aps.setSsrName(apsReq.getSsrName());
						aps.setSsrDescription(apsReq.getDescription());
						aps.setAirportCode(apsReq.getAirportCode());
						aps.setServiceCharge(apsReq.getServiceCharge());
						aps.setAlertAutoCancellation(apsReq.isAlertAutoCancellation());
						lccAPSList.add(aps);
					}
				}

				// TODO airport transfer functionality should be implement in lcc flow
				List<LCCAirportServiceDTO> paxAPTList = new ArrayList<LCCAirportServiceDTO>();

				lccSeat.setLogicalCabinClassCode(segObj.getLogicalCabinClass());
				anciSeg.setAirSeatDTO(lccSeat);
				anciSeg.setFlightSegmentTO(flightSegment);
				anciSeg.setMealDTOs(lccMealList);
				anciSeg.setBaggageDTOs(lccBaggageList);
				anciSeg.setSpecialServiceRequestDTOs(lccSSRList);
				anciSeg.setAirportServiceDTOs(lccAPSList);
				anciSeg.setTravelerRefNumber(airTraveler.getTravelerRefNumber());
				anciSeg.setAirportTransferDTOs(paxAPTList);
				selectedAncillaries.add(anciSeg);
			}
			// end of for loop
			lccClientReservationPax.setSelectedAncillaries(selectedAncillaries);

			// LCCPax Status
			lccClientReservationPax.setStatus(airTraveler.getStatus());

			// LCCPax TotalAdjustmentCharge, TotalCancelCharge, TotalModificationCharge
			BigDecimal[] totalAdjCnxModCharges = transformIntoTotalAdjCnxModCharges(perPaxPriceInfo.getPassengerPrice().getFees());
			lccClientReservationPax.setTotalAdjustmentCharge(totalAdjCnxModCharges[0]);
			lccClientReservationPax.setTotalCancelCharge(totalAdjCnxModCharges[1]);
			lccClientReservationPax.setTotalModificationCharge(totalAdjCnxModCharges[2]);

			// LCCPax TotalFare
			lccClientReservationPax.setTotalFare(perPaxPriceInfo.getPassengerPrice().getTotalFare());

			// LCCPax TotalSurcharge
			lccClientReservationPax.setTotalSurCharge(perPaxPriceInfo.getPassengerPrice().getTotalSurcharges());

			// LCCPax TotalCharge
			lccClientReservationPax.setTotalTaxCharge(perPaxPriceInfo.getPassengerPrice().getTotalTaxes());

			// LCCPax TotalAvailableBalance
			lccClientReservationPax.setTotalAvailableBalance(perPaxPriceInfo.getPassengerPrice().getTotalBalance());

			lccClientReservationPax.setTotalDiscount(perPaxPriceInfo.getPassengerPrice().getTotalDiscount());

			lccClientReservationPax.setTotalPaidAmount(perPaxPriceInfo.getPassengerPrice().getTotalPaymentAmount());

			// LCCClient TotalPaxPrice
			lccClientReservationPax.setTotalPrice(perPaxPriceInfo.getPassengerPrice().getTotalPriceWithInfant());

			// LCCPax Actual Paid amounts (In paid currency)
			LCCClientPaymentHolder lccClientPaymentHolder = new LCCClientPaymentHolder();
			if (customerTravelerFulfillment != null) {
				transform(lccClientPaymentHolder, customerTravelerFulfillment.getPaymentDetails());
				transformPaxCreits(lccClientPaymentHolder, customerTravelerFulfillment.getCreditDetails());

				BigDecimal totalBalanceDue = getPaxTotalAmountDue(perPaxPriceInfo.getPassengerPrice().getBalance());

				// updating actual refundable credit details
				BigDecimal actualCredit = getRefundableCreditAmount(customerTravelerFulfillment.getCreditDetails(),
						totalBalanceDue);
				if (lccClientReservationPax.getTotalAvailableBalance().doubleValue() < 0 && actualCredit.doubleValue() > 0) {
					lccClientReservationPax.setTotalActualCredit(actualCredit.negate());
				}
			}
			lccClientReservationPax.setLccClientPaymentHolder(lccClientPaymentHolder);

			// LCCPax Release ZuluBookingTimestamp
			Date zuluReleaseTimeStamp = null; // FIXME : What to set here ?
			lccClientReservationPax.setZuluReleaseTimeStamp(zuluReleaseTimeStamp);

			// LCCPax ZuluBookingTimestamp
			Date zuluStartTimeStamp = null;// FIXME : What to set here ?
			lccClientReservationPax.setZuluStartTimeStamp(zuluStartTimeStamp);

			/** Sets passenger fares,taxes and surcharges */
			List<FeeTO> paxFees = new ArrayList<FeeTO>();
			paxFees.addAll(transformFees(perPaxPriceInfo.getPassengerPrice().getFees()));
			paxFees.addAll(transformDiscounts(perPaxPriceInfo.getPassengerPrice().getDiscount()));
			lccClientReservationPax.setFees(paxFees);
			lccClientReservationPax.setTaxes(transformTaxes(perPaxPriceInfo.getPassengerPrice().getTaxes(), pnrSegmentSubStatus));
			lccClientReservationPax.setSurcharges(transformSurcharges(perPaxPriceInfo.getPassengerPrice().getSurcharges(),
					pnrSegmentSubStatus));
			lccClientReservationPax.setFares(transformFares(perPaxPriceInfo.getPassengerPrice().getBaseFare(),
					pnrSegmentSubStatus));

			// transform e tickets
			lccClientReservationPax.seteTickets(transformEtickets(airTraveler.getETickets(), mapPnrSegments,
					airTraveler.getTravelerRefNumber()));

			// Add to Set
			lccClientReservationPaxSet.add(lccClientReservationPax);
		}

		// Map of <Parent's Sequence, Infant's object reference>
		Map<Integer, LCCClientReservationPax> infants = new HashMap<Integer, LCCClientReservationPax>();

		// Extract all infants
		for (LCCClientReservationPax lccClientReservationPax : lccClientReservationPaxSet) {
			String travelerRefNumber = lccClientReservationPax.getTravelerRefNumber();
			if (PaxTypeUtils.isInfant(travelerRefNumber)) {
				infants.put(PaxTypeUtils.getParentSeq(travelerRefNumber), lccClientReservationPax);
			}
		}

		// Connect to Parent
		for (LCCClientReservationPax parentReservationPax : lccClientReservationPaxSet) {

			Integer paxSeq = PaxTypeUtils.getPaxSeq(parentReservationPax.getTravelerRefNumber());

			LCCClientReservationPax infantReservationPax = infants.get(paxSeq);

			if (infantReservationPax != null) {
				// Set infant's parent
				infantReservationPax.setParent(parentReservationPax);

				// Add infant to parent
				parentReservationPax.getInfants().add(infantReservationPax);
			}
		}

		return lccClientReservationPaxSet;
	}

	private static void transformMealsWithQuantity(List<LCCMealDTO> lccMealList) {
		Map<String, LCCMealDTO> mealQuantityMap = new HashMap<String, LCCMealDTO>();
		LCCMealDTO tempMeal = null;
		if (lccMealList != null && lccMealList.size() > 0) {
			for (LCCMealDTO meal : lccMealList) {
				if (mealQuantityMap.containsKey(meal.getMealCode())) {
					tempMeal = mealQuantityMap.get(meal.getMealCode());
					tempMeal.setSoldMeals(tempMeal.getSoldMeals() + 1);
					tempMeal.setTotalPrice(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(meal.getMealCharge(),
							tempMeal.getSoldMeals())));
					mealQuantityMap.put(meal.getMealCode(), tempMeal);
				} else {
					meal.setSoldMeals(1);
					meal.setTotalPrice(AccelAeroCalculator.formatAsDecimal(meal.getMealCharge()));
					mealQuantityMap.put(meal.getMealCode(), meal);
				}
			}

			lccMealList.clear();
			lccMealList.addAll(mealQuantityMap.values());
		}

	}

	public static LCCClientPaymentHolder transform(List<TravelerFulfillment> travelerFulfillments) {
		LCCClientPaymentHolder lccClientPaymentHolder = new LCCClientPaymentHolder();
		for (TravelerFulfillment travelerFulfillment : travelerFulfillments) {
			transform(lccClientPaymentHolder, travelerFulfillment.getPaymentDetails());
			transformPaxCreits(lccClientPaymentHolder, travelerFulfillment.getCreditDetails());
		}
		return lccClientPaymentHolder;
	}

	public static LCCClientPaymentHolder transformPaxCreits(LCCClientPaymentHolder lccClientPaymentHolder,
			List<CreditDetails> creditDetailsList) {
		for (CreditDetails creditDetails : creditDetailsList) {
			lccClientPaymentHolder.addCredit(creditDetails.getAmount(), creditDetails.getMcAmount(), creditDetails.getCreditId(),
					LCCClientCommonUtils.convertLCCCreditStatusToAACreditStatus(creditDetails.getStatus()),
					creditDetails.getExpireDate(), creditDetails.getTxnId(), creditDetails.getUserNote(),
					creditDetails.getCarrierCode(), creditDetails.getCurrencyCode(), creditDetails.getLccUniqueTnx(), creditDetails.isNonRefundableCredit());
		}
		return lccClientPaymentHolder;
	}

	/**
	 * 
	 * This method will convert only Customer Fulfillments as it captures actual customer payment details including
	 * actual payment made by the given currency.
	 */
	public static LCCClientPaymentHolder transform(LCCClientPaymentHolder lccClientPaymentHolder,
			List<PaymentDetails> paymentDetailsList) {

		// FIXME : In PaymentDetails we have [LoyaltyRedemption] which is not being mapped !
		for (PaymentDetails paymentDetails : paymentDetailsList) {
			String paymentCarrierCode = paymentDetails.getPaymentSummary().getPaymentCarrier();
			PaymentCurrencyAmount paymentCurrencyAmount = paymentDetails.getPaymentCurrencyAmount();

			BigDecimal payCurrencyAmount = paymentCurrencyAmount != null
					? paymentCurrencyAmount.getPaymentCurrencyAmount()
					: paymentDetails.getPaymentSummary().getPaymentAmount();
			String payCurrencyCode = paymentCurrencyAmount != null ? paymentCurrencyAmount.getCurrencyCodeGroup()
					.getCurrencyCode() : paymentDetails.getPaymentSummary().getCurrencyCodeGroup().getCurrencyCode();
			Date applicableDateTime = paymentDetails.getPaymentSummary().getApplicableDateTime();
			BigDecimal paymentAmount = paymentDetails.getPaymentSummary().getPaymentAmount();
			String paidAirlineCurrencyCode = paymentDetails.getPaymentSummary().getCurrencyCodeGroup().getCurrencyCode();
			String payRef = paymentDetails.getPaymentRefNumber();
			String carrierVisePayments = paymentDetails.getCarrierVisePayments();
			String lccUniqueTnxId = paymentDetails.getUniqueTnxId();
			String remarks = paymentDetails.getRemarks();

			String originalPayRef = paymentDetails.getOriginalPaymentRefNumber();
			String originalPaymentTnxRef = paymentDetails.getOriginalPaymentTnxRefNumber();

			// TODO: If agent need to be shown in interline/dry for cash payments in account tab, agent code should be
			// propagated through lcc - AARESAA-9374
			if (paymentDetails.isCash()) {
				String dummyPayment = paymentDetails.getDummyPayment() != null ? paymentDetails.getDummyPayment() : "N";
				lccClientPaymentHolder.addCashPayment(paymentCarrierCode, null, paymentAmount, paidAirlineCurrencyCode,
						applicableDateTime, null, payCurrencyAmount, payCurrencyCode, payRef, originalPayRef, null, null,
						carrierVisePayments, lccUniqueTnxId, Agent.PAYMENT_MODE_CASH, remarks, dummyPayment,
						originalPaymentTnxRef);

			} else if (paymentDetails.getLoyaltyFFID() != null && !"".equals(paymentDetails.getLoyaltyFFID())) {
				lccClientPaymentHolder.addLMSPayment(paymentCarrierCode, paymentDetails.getLoyaltyFFID(), null, paymentAmount,
						paidAirlineCurrencyCode, applicableDateTime, payCurrencyAmount, payCurrencyCode, payRef, originalPayRef,
						null, carrierVisePayments, lccUniqueTnxId, remarks, originalPaymentTnxRef,
						PaymentType.LOYALTY_PAYMENT.toString(), paymentDetails.isNonRefundable());
			} else if (paymentDetails.getDirectBill() != null && paymentDetails.getVoucherPayment() == null) {
				String agentCode = paymentDetails.getDirectBill().getAgentCode();
				if (ReservationTnxNominalCode.getBSPAccountTypeNominalCodes().contains(paymentDetails.getNorminalCode())) {
					lccClientPaymentHolder.addAgentCreditPayment(paymentCarrierCode, agentCode, paymentAmount,
							paidAirlineCurrencyCode, applicableDateTime, paymentDetails.getDirectBill().getAgentName(),
							payCurrencyAmount, payCurrencyCode, payRef, originalPayRef, null, null, carrierVisePayments,
							lccUniqueTnxId, Agent.PAYMENT_MODE_BSP, remarks, originalPaymentTnxRef);
				} else {
					lccClientPaymentHolder.addAgentCreditPayment(paymentCarrierCode, agentCode, paymentAmount,
							paidAirlineCurrencyCode, applicableDateTime, paymentDetails.getDirectBill().getAgentName(),
							payCurrencyAmount, payCurrencyCode, payRef, originalPayRef, null, null, carrierVisePayments,
							lccUniqueTnxId, Agent.PAYMENT_MODE_ONACCOUNT, remarks, originalPaymentTnxRef);
				}

			} else if (paymentDetails.getPaymentCard() != null) {
				CommonCreditCardPayment paymentCard = paymentDetails.getPaymentCard();
				int paymentCardType = LCCClientCommonUtils.getPaymentCardType(paymentCard.getCardType()).getTypeValue();
				// String cardNumber = paymentCard.getLast4Digits();
				String cardNumber = paymentCard.getNo();
				String authorizationId = paymentCard.getAuthorizationId();

				lccClientPaymentHolder.addCardPayment(paymentCarrierCode, paymentCardType, null, cardNumber, null, null, null,
						paymentAmount, paidAirlineCurrencyCode, applicableDateTime, authorizationId, payCurrencyAmount,
						paymentCard.getCardHolderName(), payCurrencyCode, payRef, originalPayRef, null, carrierVisePayments,
						lccUniqueTnxId, remarks, paymentCard.getPaymentBrokerId(), originalPaymentTnxRef);

			} else if (paymentDetails.getVoucherPayment() != null) {
				VoucherDTO voucherDTO = new VoucherDTO();
				voucherDTO.setVoucherId(paymentDetails.getVoucherPayment().getVoucherID());
				//voucherDTO.setAmountInBase(paymentDetails.getVoucherPayment().getVoucherAmount().toString());
				voucherDTO.setRedeemdAmount(paymentDetails.getVoucherPayment().getVoucherRedeemAmount().toString());
				lccClientPaymentHolder.addVoucherPayment(paymentCarrierCode, voucherDTO, null, paymentAmount,
						paidAirlineCurrencyCode, applicableDateTime, payCurrencyAmount, payCurrencyCode, payRef, originalPayRef,
						null, carrierVisePayments, lccUniqueTnxId, remarks, originalPaymentTnxRef);
			} else if (paymentDetails.getTravelerCreditPayment() != null) {
				// we are not allowing to refund pax credit payments. hence passing null as the originalPayRef
				lccClientPaymentHolder.addPaxCreditPayment(paymentCarrierCode, null, paymentDetails.getTravelerCreditPayment()
						.getDescription(), paymentAmount, paidAirlineCurrencyCode, applicableDateTime, payCurrencyAmount,
						payCurrencyCode, payRef, null, null, carrierVisePayments, lccUniqueTnxId, remarks, originalPaymentTnxRef);
			}
		}

		return lccClientPaymentHolder;
	}

	public static List<ChargeAdjustment> transformAdjustmentList(List<LCCClientChargeAdustment> lccClientChargeAdjustmentList) {

		List<ChargeAdjustment> chargeAdjustmentList = new ArrayList<ChargeAdjustment>();

		if (lccClientChargeAdjustmentList != null) {
			for (LCCClientChargeAdustment lccClientChargeAdjustment : lccClientChargeAdjustmentList) {

				ChargeAdjustment chargeAdjustment = new ChargeAdjustment();
				chargeAdjustment.setAmount(lccClientChargeAdjustment.getAmount());
				chargeAdjustment.setCarrierCode(lccClientChargeAdjustment.getCarrierCode());
				chargeAdjustment.setCarrierOndGroupRPH(lccClientChargeAdjustment.getCarrierOndGroupRPH());

				if (lccClientChargeAdjustment.isRefundable()) {
					chargeAdjustment.setChargeType(ChargeType.REFUNDABLE);
				} else {
					chargeAdjustment.setChargeType(ChargeType.NON_REFUNDABLE);
				}

				chargeAdjustment.setSegmentCode(lccClientChargeAdjustment.getSegmentCode());
				chargeAdjustment.setTravelerRefNumber(lccClientChargeAdjustment.getTravelerRefNumber());
				chargeAdjustment.setUserNote(lccClientChargeAdjustment.getUserNote());
				chargeAdjustment.setChargeAdjustmentTypeId(lccClientChargeAdjustment.getAdjustmentTypeId());

				chargeAdjustmentList.add(chargeAdjustment);
			}
		}
		return chargeAdjustmentList;
	}

	public static Set<LCCClientReservationSegment> transform(OriginDestinationOptions originDestinationOptions,
			TicketingInfo ticketingInfo, LCCClientPnrModesDTO pnrModesDTO) {

		Set<LCCClientReservationSegment> lcclccResvSegSet = new HashSet<LCCClientReservationSegment>();
		Collection<TicketModifiableStates> colTicketModifiableStates = ticketingInfo.getTicketModifiableStates();
		TicketModifiableStates ticketModifiableStates;

		for (OriginDestinationOption originDestinationOption : originDestinationOptions.getOriginDestinationOption()) {
			for (FlightSegment flightSegment : originDestinationOption.getFlightSegment()) {

				LCCClientReservationSegment lccResvSeg = new LCCClientReservationSegment();

				Date arrivalDate = flightSegment.getArrivalDateTime();
				lccResvSeg.setArrivalDate(arrivalDate);
				lccResvSeg.setArrivalDateZulu(flightSegment.getArrivalDateTimeZulu());
				lccResvSeg.setArrivalAirportName(flightSegment.getArrivalAirportName());

				Date departureDate = flightSegment.getDepatureDateTime();
				lccResvSeg.setDepartureDate(departureDate);
				lccResvSeg.setDepartureDateZulu(flightSegment.getDepatureDateTimeZulu());
				lccResvSeg.setDepartureAirportName(flightSegment.getDepartureAirportName());
				lccResvSeg.setCabinClassCode(flightSegment.getCabinClassCode());
				lccResvSeg.setLogicalCabinClass(flightSegment.getLogicalCabinClassCode());

				lccResvSeg.setFlightNo(flightSegment.getFlightNumber());
				// lccResvSeg.setFlightSegmentRefNumber(SegmentUtil.composeFlightRPH(flightSegment));
				lccResvSeg.setFlightSegmentRefNumber(flightSegment.getFlightRefNumber());
				lccResvSeg.setRouteRefNumber(flightSegment.getRouteRefNumber());
				if (!AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
					lccResvSeg.setSegmentModifiableAsPerETicketStatus(true);
				}
				lccResvSeg.setBookingFlightSegmentRefNumber(flightSegment.getBookingFlightRefNumber());
				lccResvSeg.setPnrSegID(Integer.parseInt(flightSegment.getBookingFlightRefNumber()));

				lccResvSeg.setFlightBaggageAllowance(flightSegment.getBaggageAllowance());

				// FIXME : Must figure out a way to know whether a segment is return or not.
				// This issue is complicated by the fact that LCC supports open JAW bookings.

				// lccResvSeg.setReturnFlag(null);
				lccResvSeg.setReturnFlag(flightSegment.getReturnFlag());

				lccResvSeg.setSegmentCode(flightSegment.getSegmentCode());

				lccResvSeg.setSegmentSeq(flightSegment.getSegmentSequence());

				lccResvSeg.setStatus(flightSegment.getStatus().toString());

				lccResvSeg.setSubStatus(flightSegment.getSubStatus().toString());

				lccResvSeg.setDisplayStatus(flightSegment.getDisplayStatus());

				lccResvSeg.setUnSegment(ReservationInternalConstants.ReservationSegmentStatus.FLIGHT_CANCEL.equals(flightSegment.getDisplayStatus()));

				lccResvSeg.setBookingType(flightSegment.getBookingType());

				lccResvSeg.setInterlineGroupKey(flightSegment.getInterlineGroupKey());

				lccResvSeg.setInterlineReturnGroupKey(flightSegment.getReturnOndGroupId() == null ? "" : (flightSegment
						.getReturnOndGroupId() + ""));

				lccResvSeg.setSystem(SYSTEM.INT);

				lccResvSeg.setCarrierCode(flightSegment.getOperatingAirline());

				ticketModifiableStates = getTicketModifiableStates(colTicketModifiableStates,
						flightSegment.getBookingFlightRefNumber());

				/* segment modification */
				lccResvSeg.setModifyByDate(flightSegment.isModifyByDate());

				lccResvSeg.setModifyByRoute(flightSegment.isModifyByRoute());

				lccResvSeg.setCheckInTimeGap(flightSegment.getCheckInTimeGap());

				lccResvSeg.setSubStationShortName(flightSegment.getSubStationShortName());

				lccResvSeg.setGroundStationPnrSegmentID(flightSegment.getGroundStationPnrSegmentID());

				lccResvSeg.setBaggageONDGroupId(flightSegment.getBaggageONDGroupId());

				lccResvSeg.setJourneySequence(flightSegment.getJourneySeq());

				lccResvSeg.setLastFareQuoteDate(flightSegment.getLastFareQuoteDateZulu());

				if (flightSegment.getSegmentConnTimeInfo() != null) {
					SegmentConnTimeInfo segmentConnTimeInfo = flightSegment.getSegmentConnTimeInfo();

					if (StringUtils.isNotEmpty(segmentConnTimeInfo.getDepAirportMinConTime())) {
						lccResvSeg.setDepAirportMinConTime(segmentConnTimeInfo.getDepAirportMinConTime());
					}

					if (StringUtils.isNotEmpty(segmentConnTimeInfo.getDepAirportMaxConTime())) {
						lccResvSeg.setDepAirportMaxConTime(segmentConnTimeInfo.getDepAirportMaxConTime());
					}

					if (StringUtils.isNotEmpty(segmentConnTimeInfo.getArriAirportMinConTime())) {
						lccResvSeg.setArriAirportMinConTime(segmentConnTimeInfo.getArriAirportMinConTime());
					}

					if (StringUtils.isNotEmpty(segmentConnTimeInfo.getArriAirportMaxConTime())) {
						lccResvSeg.setArriAirportMaxConTime(segmentConnTimeInfo.getArriAirportMaxConTime());
					}
				}

				if (ticketModifiableStates != null) {
					lccResvSeg.setModifyTillBufferDateTime(ticketModifiableStates.getTicketModifyTillBufferDateTime());
					lccResvSeg.setCancelTillBufferDateTime(ticketModifiableStates.getTicketCancelTillBufferDateTime());
					lccResvSeg.setModifyTillFlightClosureDateTime(ticketModifiableStates
							.getTicketModifyTillFlightClosureDateTime());
				}
				if (flightSegment.getOldFareDetails() != null) {
					FareTO fareTO = new FareTO();
					fareTO.setAmount(flightSegment.getOldFareDetails().getOldFareAmount());
					fareTO.setFareId(flightSegment.getOldFareDetails().getOldFareId());
					fareTO.setFareType(Integer.toString(flightSegment.getOldFareDetails().getOldFareType()));
					fareTO.setCarrierCode(flightSegment.getOldFareDetails().getCarrierCode());
					fareTO.setFareBasisCode(flightSegment.getOldFareDetails().getFareBasisCode());
					fareTO.setBookingClassCode(flightSegment.getOldFareDetails().getBookingClass());
					fareTO.setSegmentCode(flightSegment.getOldFareDetails().getOndCode());
					fareTO.setBulkTicketFareRule(flightSegment.getOldFareDetails().isBulkTicketFareRule() != null
							? flightSegment.getOldFareDetails().isBulkTicketFareRule()
							: false);
					fareTO.setAppliedFlightSegId(flightSegment.getOldFareDetails().getFlightSegId());
					fareTO.setLogicalCCCode(flightSegment.getLogicalCabinClassCode());
					fareTO.setOndSequence(flightSegment.getOndGroupId());
					lccResvSeg.setFareTO(fareTO);
				}

				lccResvSeg.setAlertAutoCancellation(flightSegment.isAlertAutoCancellation());

				lccResvSeg.setBundledServicePeriodId(flightSegment.getBundledServiceId());

				lcclccResvSegSet.add(lccResvSeg);
			}
		}

		return lcclccResvSegSet;
	}

	private static TicketModifiableStates getTicketModifiableStates(Collection<TicketModifiableStates> colTicketModifiableStates,
			String flightRefNumber) {

		if (colTicketModifiableStates != null && colTicketModifiableStates.size() > 0) {
			for (TicketModifiableStates ticketModifiableStates : colTicketModifiableStates) {
				if (BeanUtils.nullHandler(ticketModifiableStates.getFlightRefNumber()).equals(
						BeanUtils.nullHandler(flightRefNumber))) {
					return ticketModifiableStates;
				}
			}
		}

		return null;
	}

	private static BigDecimal[] transformIntoTotalAdjCnxModCharges(List<Fee> fees) {
		BigDecimal totalAdjustmentCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalCancelCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (Fee fee : fees) {
			FeeType feeType = fee.getFeeType();
			BigDecimal feeAmount = fee.getAmount();

			if (FeeType.ADJ == feeType) {
				totalAdjustmentCharge = totalAdjustmentCharge.add(feeAmount);
			} else if (FeeType.CNX == feeType) {
				totalCancelCharge = totalCancelCharge.add(feeAmount);
			} else if (FeeType.MOD == feeType) {
				totalModificationCharge = totalModificationCharge.add(feeAmount);
			} else {
				// FIXME : What do we do with ISU and ESU charges ??
			}
		}

		return new BigDecimal[] { totalAdjustmentCharge, totalCancelCharge, totalModificationCharge };
	}

	/**
	 * Pax Count Totals by Type
	 * 
	 * @param lccAirReservation
	 * @return
	 */
	private static int[] getPaxCountByType(List<PassengerTypeQuantity> passengerTypeQuantitys) {

		// [0] = Adult Pax Type Count
		// [1] = Child Pax Type Count
		// [2] = Infant Pax Type Count
		int totalPaxCountByType[] = new int[3];

		for (PassengerTypeQuantity passengerTypeQuantity : passengerTypeQuantitys) {
			if (passengerTypeQuantity.getPassengerType() == PassengerType.ADT) {
				totalPaxCountByType[0] = passengerTypeQuantity.getQuantity();
			} else if (passengerTypeQuantity.getPassengerType() == PassengerType.CHD) {
				totalPaxCountByType[1] = passengerTypeQuantity.getQuantity();
			} else if (passengerTypeQuantity.getPassengerType() == PassengerType.INF) {
				totalPaxCountByType[2] = passengerTypeQuantity.getQuantity();
			}
		}

		return totalPaxCountByType;
	}

	public static FlightSegment transform(LCCClientReservationSegment lccClientReservationSegment) {
		FlightSegment flightSegment = new FlightSegment();
		flightSegment.setSegmentCode(lccClientReservationSegment.getSegmentCode());
		flightSegment.setDepatureDateTime(lccClientReservationSegment.getDepartureDate());
		flightSegment.setDepatureDateTimeZulu(lccClientReservationSegment.getDepartureDateZulu());
		flightSegment.setArrivalDateTime(lccClientReservationSegment.getArrivalDate());
		flightSegment.setArrivalDateTimeZulu(lccClientReservationSegment.getArrivalDateZulu());
		flightSegment.setFlightNumber(lccClientReservationSegment.getFlightNo());
		flightSegment.setOperatingAirline(AppSysParamsUtil.extractCarrierCode(lccClientReservationSegment.getFlightNo()));
		flightSegment.setStatus(SegmentStatus.fromValue(lccClientReservationSegment.getStatus()));
		flightSegment.setFlightRefNumber(lccClientReservationSegment.getFlightSegmentRefNumber());
		flightSegment.setBookingFlightRefNumber(lccClientReservationSegment.getBookingFlightSegmentRefNumber());
		flightSegment.setSegmentSequence(lccClientReservationSegment.getSegmentSeq());
		flightSegment.setReturnFlag(lccClientReservationSegment.getReturnFlag());
		flightSegment.setRouteRefNumber(lccClientReservationSegment.getRouteRefNumber());

		String segmentCode = BeanUtils.nullHandler(lccClientReservationSegment.getSegmentCode());
		if (segmentCode.length() > 0) {
			String[] segments = segmentCode.split("/");

			flightSegment.setDepartureAirportCode(segments[0]);
			flightSegment.setArrivalAirportCode(segments[segments.length - 1]);
		}

		flightSegment.setCabinClassCode(lccClientReservationSegment.getCabinClassCode());
		flightSegment.setLogicalCabinClassCode(lccClientReservationSegment.getLogicalCabinClass());
		flightSegment.setBaggageONDGroupId(lccClientReservationSegment.getBaggageONDGroupId());

		return flightSegment;
	}

	public static ReservationBalanceTO transformBalances(LCCAirBookRS lccAirBookRS) throws ModuleException {
		if (lccAirBookRS.getResponseAttributes().getSuccess() == null) {
			LCCError lccError = BeanUtils.getFirstElement(lccAirBookRS.getResponseAttributes().getErrors());
			throw new ModuleException(lccError.getErrorCode().value());
		}

		ResCnxModAddResBalances resCnxModAddResBalances = lccAirBookRS.getLccReservation().getAirReservation()
				.getResCnxModAddResBalances();
		AlterationBalances alterationBalances = resCnxModAddResBalances.getAlterationBalances();

		ReservationBalanceTO lccClientReservationBalanceTO = new ReservationBalanceTO();
		lccClientReservationBalanceTO.setInfantCharge(resCnxModAddResBalances.getInfantCharge());
		lccClientReservationBalanceTO.setSegmentSummary(transform(alterationBalances));

		lccClientReservationBalanceTO.setTotalAmountDue(alterationBalances.getTotalAmountDue());
		lccClientReservationBalanceTO.setTotalCreditAmount(alterationBalances.getTotalCreditAmount());
		lccClientReservationBalanceTO.setTotalCnxCharge(alterationBalances.getTotalCnxChargeForCurrentOperation());
		lccClientReservationBalanceTO.setTotalModCharge(alterationBalances.getTotalModChargeForCurrentOperation());
		lccClientReservationBalanceTO.setTotalPrice(alterationBalances.getTotalPrice());
		if (alterationBalances.getCustomerPaymentDetails() != null && !alterationBalances.getCustomerPaymentDetails().isEmpty()) {
			lccClientReservationBalanceTO.setTotalPaidAmount(getTotalPaidAmount(alterationBalances.getCustomerPaymentDetails()));
		} else {
			lccClientReservationBalanceTO.setTotalPaidAmount(calculateTotalPaidAmount(resCnxModAddResBalances
					.getTravelerCnxModAddResBalances()));
		}

		Collection<LCCClientPassengerSummaryTO> colLCCClientPassengerSummaryTO = new ArrayList<LCCClientPassengerSummaryTO>();
		lccClientReservationBalanceTO.setPassengerSummaryList(colLCCClientPassengerSummaryTO);

		for (TravelerCnxModAddResBalances travelerCnxModAddResBalances : resCnxModAddResBalances
				.getTravelerCnxModAddResBalances()) {
			LCCClientPassengerSummaryTO passengerSummaryTO = new LCCClientPassengerSummaryTO();

			passengerSummaryTO.setTravelerRefNumber(travelerCnxModAddResBalances.getTravelerRefNumber());
			passengerSummaryTO.setPaxName(travelerCnxModAddResBalances.getTravelerName());
			passengerSummaryTO.setInfantName(travelerCnxModAddResBalances.getTravelerInfantName());
			passengerSummaryTO.setPaxType(LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(travelerCnxModAddResBalances
					.getPassengerType()));

			passengerSummaryTO.setSegmentSummaryTO(transform(travelerCnxModAddResBalances.getAlterationBalances()));

			passengerSummaryTO.setTotalAmountDue(travelerCnxModAddResBalances.getAlterationBalances().getTotalAmountDue());
			passengerSummaryTO.setTotalCreditAmount(travelerCnxModAddResBalances.getAlterationBalances().getTotalCreditAmount());
			passengerSummaryTO.setTotalCnxCharge(travelerCnxModAddResBalances.getAlterationBalances()
					.getTotalCnxChargeForCurrentOperation());
			passengerSummaryTO.setTotalModCharge(travelerCnxModAddResBalances.getAlterationBalances()
					.getTotalModChargeForCurrentOperation());
			passengerSummaryTO.setTotalPrice(travelerCnxModAddResBalances.getAlterationBalances().getTotalPrice());
			if (travelerCnxModAddResBalances.getAlterationBalances().getCustomerPaymentDetails() != null
					&& !travelerCnxModAddResBalances.getAlterationBalances().getCustomerPaymentDetails().isEmpty()) {
				passengerSummaryTO.setTotalPaidAmount(getTotalPaidAmount(travelerCnxModAddResBalances.getAlterationBalances()
						.getCustomerPaymentDetails()));
			} else {
				passengerSummaryTO.setTotalPaidAmount(getTotalPaidAmount(travelerCnxModAddResBalances.getAlterationBalances()
						.getPaymentDetails()));
			}

			colLCCClientPassengerSummaryTO.add(passengerSummaryTO);
		}
		lccClientReservationBalanceTO.setVersion(lccAirBookRS.getLccReservation().getAirReservation().getVersion());
		return lccClientReservationBalanceTO;
	}

	private static BigDecimal calculateTotalPaidAmount(List<TravelerCnxModAddResBalances> travelerCnxModAddResBalances) {
		BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (travelerCnxModAddResBalances != null && !travelerCnxModAddResBalances.isEmpty()) {
			for (TravelerCnxModAddResBalances travelerCnxModAddResBalance : travelerCnxModAddResBalances) {
				totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, getTotalPaidAmount(travelerCnxModAddResBalance
						.getAlterationBalances().getPaymentDetails()));
			}
		}
		return AccelAeroCalculator.scaleValueDefault(totalPaidAmount).negate();
	}

	private static BigDecimal getTotalPaidAmount(List<PaymentDetails> lstPaymentDetails) {
		BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (lstPaymentDetails != null && lstPaymentDetails.size() > 0) {
			for (PaymentDetails paymentDetails : lstPaymentDetails) {
				totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, paymentDetails.getPaymentSummary().getPaymentAmount());
			}
		}

		return AccelAeroCalculator.scaleValueDefault(totalPaidAmount).negate();
	}

	private static LCCClientSegmentSummaryTO transform(AlterationBalances alterationBalances) {
		LCCClientSegmentSummaryTO lccClientSegmentSummaryTO = new LCCClientSegmentSummaryTO();
		CnxModAddONDBalances cancellations = alterationBalances.getCnxONDBalances();
		if (cancellations != null) {
			lccClientSegmentSummaryTO.setCurrentFareAmount(cancellations.getCurrentAmounts().getTotalFare());
			lccClientSegmentSummaryTO.setCurrentTaxAmount(cancellations.getCurrentAmounts().getTotalTaxes());
			lccClientSegmentSummaryTO.setCurrentSurchargeAmount(cancellations.getCurrentAmounts().getTotalSurcharges());
			lccClientSegmentSummaryTO.setCurrentDiscount(cancellations.getCurrentAmounts().getTotalDiscount() == null
					? BigDecimal.ZERO
					: cancellations.getCurrentAmounts().getTotalDiscount());

			lccClientSegmentSummaryTO
					.setCurrentCnxAmount(computeAmount(cancellations.getCurrentAmounts().getFees(), FeeType.CNX));
			lccClientSegmentSummaryTO
					.setCurrentModAmount(computeAmount(cancellations.getCurrentAmounts().getFees(), FeeType.MOD));
			lccClientSegmentSummaryTO
					.setCurrentAdjAmount(computeAmount(cancellations.getCurrentAmounts().getFees(), FeeType.ADJ));

			lccClientSegmentSummaryTO.setCurrentTotalPrice(AccelAeroCalculator.scaleValueDefault(cancellations
					.getCurrentAmounts().getTotalPrice()));

			lccClientSegmentSummaryTO.setCurrentRefunds(cancellations.getRefundableAmounts().getTotalPrice());
			lccClientSegmentSummaryTO.setCurrentNonRefunds(cancellations.getNonRefundableAmounts().getTotalPrice());

			lccClientSegmentSummaryTO.setNewCnxAmount(alterationBalances.getTotalCnxChargeForCurrentOperation());
			lccClientSegmentSummaryTO.setNewModAmount(alterationBalances.getTotalModChargeForCurrentOperation());
		}

		// New
		CnxModAddONDBalances addons = alterationBalances.getAddONDBalances();
		if (addons != null) {
			lccClientSegmentSummaryTO.setNewFareAmount(addons.getCurrentAmounts().getTotalFare());
			lccClientSegmentSummaryTO.setNewTaxAmount(addons.getCurrentAmounts().getTotalTaxes());
			lccClientSegmentSummaryTO.setNewSurchargeAmount(addons.getCurrentAmounts().getTotalSurcharges());
			lccClientSegmentSummaryTO.setNewTotalPrice(AccelAeroCalculator.scaleValueDefault(addons.getCurrentAmounts()
					.getTotalPrice()));
		}

		return lccClientSegmentSummaryTO;
	}

	private static BigDecimal computeAmount(List<Fee> fees, FeeType feeType) {
		BigDecimal totalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (Fee fee : fees) {
			if (feeType.value().equals(fee.getFeeType().value())) {
				totalCharge = AccelAeroCalculator.add(totalCharge, fee.getAmount());
			}
		}

		return totalCharge;
	}

	/**
	 * Transforms Fees to LCClientReservation.
	 * 
	 * @param priceInfo
	 * @return List<FeeTO>
	 * @author lalanthi - Date : 10/12/2009
	 */
	private static List<FeeTO> transformFees(List<Fee> lccFeeList) {
		List<FeeTO> feesList = new ArrayList<FeeTO>();
		if (lccFeeList != null) {
			for (Fee lccFee : lccFeeList) {
				FeeTO feeTO = new FeeTO();
				feeTO.setAmount(lccFee.getAmount());
				feeTO.setCarrierCode(lccFee.getCarrierCode());
				feeTO.setFeeCode(lccFee.getFeeCode());
				feeTO.setFeeName(lccFee.getFeeName());
				feeTO.setSegmentCode(lccFee.getSegmentCode());
				feeTO.setDepartureDate(lccFee.getDepartureDateTime());
				if (lccFee.getApplicableDateTime() != null) {
					Date gc = lccFee.getApplicableDateTime();
					feeTO.setApplicableTime(gc);
				}
				feesList.add(feeTO);
			}
		}
		return feesList;
	}

	private static List<FeeTO> transformDiscounts(List<Discount> discounts) {
		List<FeeTO> feesList = new ArrayList<FeeTO>();
		if (discounts != null) {
			for (Discount discount : discounts) {
				FeeTO feeTO = new FeeTO();
				feeTO.setAmount(discount.getAmount());
				feeTO.setCarrierCode(discount.getCarrierCode());
				feeTO.setFeeCode(discount.getDiscountCode());
				feeTO.setFeeName(discount.getDiscountName());
				feeTO.setSegmentCode(discount.getSegmentCode());
				if (discount.getApplicableDateTime() != null) {
					Date gc = discount.getApplicableDateTime();
					feeTO.setApplicableTime(gc);
				}
				feeTO.setDepartureDate(discount.getDepartureDateTime());
				feesList.add(feeTO);
			}
		}
		return feesList;
	}

	/**
	 * Transforms Taxes to LCClientReservation.
	 * 
	 * @param priceInfo
	 * @return List<TaxTO>
	 * @author lalanthi - Date : 10/12/2009
	 */
	private static List<TaxTO> transformTaxes(List<Tax> lccTaxList, Map<Integer, String> pnrSegmentSubStatus) {
		List<TaxTO> taxesList = new ArrayList<TaxTO>();
		if (lccTaxList != null) {
			for (Tax lccTax : lccTaxList) {

				if (pnrSegmentSubStatus != null && isHideExchangeCharge(lccTax.getPnrSegmentID(), pnrSegmentSubStatus)) {
					continue;
				}
				TaxTO taxTO = new TaxTO();
				taxTO.setAmount(lccTax.getAmount());
				taxTO.setCarrierCode(lccTax.getCarrierCode());
				taxTO.setTaxCode(lccTax.getTaxCode());
				taxTO.setTaxName(lccTax.getTaxName());
				taxTO.setSegmentCode(lccTax.getSegmentCode());
				taxTO.setDepartureDate(lccTax.getDepartureDateTime());
				if (lccTax.getApplicableDateTime() != null) {
					Date gc = lccTax.getApplicableDateTime();
					taxTO.setApplicableTime(gc);
				}
				taxesList.add(taxTO);
			}
		}
		return taxesList;
	}

	/**
	 * Transforms e ticket details to LCClientReservation.
	 * 
	 * @param airTraveler
	 * @param mapPnrSegments
	 * @param travellerRefNo
	 * @return
	 */
	private static Collection<LccClientPassengerEticketInfoTO> transformEtickets(Collection<Eticket> lccEticketList,
			Map<String, LCCClientReservationSegment> mapPnrSegments, String travellerRefNo) {
		Collection<LccClientPassengerEticketInfoTO> eTickets = new ArrayList<LccClientPassengerEticketInfoTO>();
		Date currentDate = new Date();
		if (lccEticketList != null) {
			for (Eticket lccTicket : lccEticketList) {
				LCCClientReservationSegment lccClientReservationSegment = mapPnrSegments.get(lccTicket.getPnrSegmentRefNumber());

				String paxStatus = lccTicket.getPaxStatus();

				LccClientPassengerEticketInfoTO lccClientTicketInfo = new LccClientPassengerEticketInfoTO();
				lccClientTicketInfo.setCarrierCode(lccTicket.getAirlineCode());
				lccClientTicketInfo.setPaxETicketNo(String.valueOf(lccTicket.getEticketNumber()));
				lccClientTicketInfo.setDepartureDate(lccClientReservationSegment.getDepartureDate());
				lccClientTicketInfo.setFlightNo(lccClientReservationSegment.getFlightNo());
				lccClientTicketInfo.setPaxETicketStatus(lccTicket.getTicketStatus());
				lccClientTicketInfo.setSegmentCode(lccClientReservationSegment.getSegmentCode());
				lccClientTicketInfo.setSegmentStatus(lccClientReservationSegment.getStatus());
				lccClientTicketInfo.setEticketId(lccTicket.getTicketId());
				lccClientTicketInfo.setCouponNo(lccTicket.getCouponNo());
				lccClientTicketInfo.setPnrSegId(lccTicket.getPnrSegmentRefNumber());
				lccClientTicketInfo.setTravelerRefNumber(travellerRefNo);
				lccClientTicketInfo.setPaxStatus(paxStatus);
				lccClientTicketInfo.setPpfsId(lccTicket.getPnrPaxFareSegId());
				lccClientTicketInfo.setCodeShareCarrierCode(lccClientReservationSegment.getCsOcCarrierCode());
				lccClientTicketInfo.setCodeShareFlightNumber(lccClientReservationSegment.getCsOcFlightNumber());

				if (lccClientReservationSegment.getStatus().equals(
						ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.DATE
							&& currentDate.compareTo(lccClientReservationSegment.getDepartureDateZulu()) > 0) {
						lccClientReservationSegment.setFlownSegment(true);
					} else if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.PFS
							&& (ReservationInternalConstants.PaxFareSegmentTypes.FLOWN.equals(paxStatus) || ReservationInternalConstants.PaxFareSegmentTypes.NO_REC
									.equals(paxStatus))) {
						lccClientReservationSegment.setFlownSegment(true);
					} else if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET
							&& EticketStatus.FLOWN.code().equals(lccTicket.getTicketStatus())) {
						lccClientReservationSegment.setFlownSegment(true);
					} else if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()
							&&	(EticketStatus.CHECKEDIN.code().equals(lccTicket.getTicketStatus())
									|| EticketStatus.BOARDED.code().equals(lccTicket.getTicketStatus()))){
						lccClientReservationSegment.setFlownSegment(true);
					}
				}

				eTickets.add(lccClientTicketInfo);
			}
		}
		return eTickets;
	}

	/**
	 * Transforms Surcharges to LCClientReservation.
	 * 
	 * @param priceInfo
	 * @return List<SurchargeTO>
	 * @author lalanthi - Date : 10/12/2009
	 */
	private static List<SurchargeTO> transformSurcharges(List<Surcharge> lccSurchargeList,
			Map<Integer, String> pnrSegmentSubStatus) {
		List<SurchargeTO> surchargesList = new ArrayList<SurchargeTO>();
		if (lccSurchargeList != null) {
			for (Surcharge lccSurcharge : lccSurchargeList) {
				if (pnrSegmentSubStatus != null && isHideExchangeCharge(lccSurcharge.getPnrSegmentID(), pnrSegmentSubStatus)) {
					continue;
				}
				SurchargeTO surchargeTO = new SurchargeTO();
				surchargeTO.setAmount(lccSurcharge.getAmount());
				surchargeTO.setCarrierCode(lccSurcharge.getCarrierCode());
				surchargeTO.setSurchargeCode(lccSurcharge.getSurchargeCode());
				surchargeTO.setSurchargeName(lccSurcharge.getSurchargeName());
				surchargeTO.setSegmentCode(lccSurcharge.getSegmentCode());
				surchargeTO.setDepartureDate(lccSurcharge.getDepartureDateTime());
				if (lccSurcharge.getApplicableDateTime() != null) {
					Date gc = lccSurcharge.getApplicableDateTime();
					surchargeTO.setApplicableTime(gc);
				}
				surchargesList.add(surchargeTO);
			}
		}
		return surchargesList;
	}

	/**
	 * Converts the LCCBaseFares to AccleAero TO's
	 * 
	 * @param lccFaresList
	 * @return
	 */
	private static List<FareTO> transformFares(List<BaseFare> lccFaresList, Map<Integer, String> pnrSegmentSubStatus) {
		List<FareTO> faresList = new ArrayList<FareTO>();
		if (lccFaresList != null) {
			for (BaseFare lccFare : lccFaresList) {
				if (pnrSegmentSubStatus != null && isHideExchangeCharge(lccFare.getPnrSegmentID(), pnrSegmentSubStatus)) {
					continue;
				}
				FareTO fareTO = new FareTO();
				if (lccFare.getApplicableDateTime() != null) {
					fareTO.setApplicableDate(lccFare.getApplicableDateTime());
				}
				fareTO.setDescription(lccFare.getDescription());
				fareTO.setAmount(lccFare.getAmount());
				// Fix me : segment modification to be refactored according to the fare rules in LCC
				// fareTO.setIsAllowModifyDate(true);
				// fareTO.setIsAllowModifyRoute(true);
				fareTO.setSegmentCode(lccFare.getSegmentCode());
				fareTO.setCarrierCode(lccFare.getCarrierCode().get(0));
				fareTO.setPnrPaxFareId(lccFare.getPnrPaxFareId());
				fareTO.setPnrSegIds(lccFare.getSegmentRPH());
				fareTO.setDepartureDate(lccFare.getDepartureDateTime());
				fareTO.setBookingClassCode(lccFare.getBookingClass());

				faresList.add(fareTO);
			}
		}
		return faresList;
	}

	public static LCCTransferSegmentsRQ transform(LCCClientTransferSegmentTO transferSegmentTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo, String salesChannelKey) {
		LCCTransferSegmentsRQ transferSegmentsRQ = new LCCTransferSegmentsRQ();
		transferSegmentsRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

		List<TransferSegment> transferSegments = new ArrayList<TransferSegment>();
		Map<String, SegmentDetails> newSegmentDetails = transferSegmentTO.getNewSegmentDetails();
		Map<String, SegmentDetails> oldSegmentDetails = transferSegmentTO.getOldSegmentDetails();
		List<String> oldSegmentList = new ArrayList<String>(oldSegmentDetails.keySet());
		List<String> newSegmentList = new ArrayList<String>(newSegmentDetails.keySet());
		int i = 0;

		// Size of the newSegmentDetails and oldSegmentDetails should be same
		for (String segmentCode : newSegmentDetails.keySet()) {
			// Set new external segment details
			SegmentDetails newSegmentDetail = newSegmentDetails.get(newSegmentList.get(i));
			ExternalSegment newExternalSegment = new ExternalSegment();
			newExternalSegment.setArrivalDate(newSegmentDetail.getArrivalDate());
			newExternalSegment.setArrivalDateZulu(newSegmentDetail.getArrivalDateZulu());
			newExternalSegment.setDepartureDate(newSegmentDetail.getDepartureDate());
			newExternalSegment.setDepartureDateZulu(newSegmentDetail.getDepartureDateZulu());
			// adding the flight segment Id for new transfer segment.
			newExternalSegment.setFlightSegId(newSegmentDetail.getFlightSegId());
			newExternalSegment.setSegmentSeq(newSegmentDetail.getSegmentSeq());
			newExternalSegment.setStatus(SegmentStatus.fromValue(newSegmentDetail.getStatus()));

			newExternalSegment.setCabinClass(newSegmentDetail.getCabinClass());
			newExternalSegment.setLogicalCabinClass(newSegmentDetail.getLogicalCabinClass());
			newExternalSegment.setFlightNumber(newSegmentDetail.getFlightNumber());
			newExternalSegment.setSegmentCode(newSegmentDetail.getSegmentCode());

			// Set old external segment details
			SegmentDetails oldSegmentDetail = oldSegmentDetails.get(oldSegmentList.get(i));
			ExternalSegment oldExternalSegment = new ExternalSegment();
			oldExternalSegment.setArrivalDate(oldSegmentDetail.getArrivalDate());
			oldExternalSegment.setArrivalDateZulu(oldExternalSegment.getArrivalDateZulu());
			oldExternalSegment.setDepartureDate(oldSegmentDetail.getDepartureDate());
			oldExternalSegment.setDepartureDateZulu(oldExternalSegment.getDepartureDateZulu());

			oldExternalSegment.setCabinClass(oldSegmentDetail.getCabinClass());
			oldExternalSegment.setLogicalCabinClass(oldSegmentDetail.getLogicalCabinClass());
			oldExternalSegment.setFlightNumber(oldSegmentDetail.getFlightNumber());
			oldExternalSegment.setSegmentCode(oldSegmentDetail.getSegmentCode());
			oldExternalSegment.setSegmentSeq(oldSegmentDetail.getSegmentSeq());
			oldExternalSegment.setStatus(SegmentStatus.fromValue(oldSegmentDetail.getStatus()));

			TransferSegment transferSegment = new TransferSegment();
			transferSegment.setCarrierCode(newSegmentDetail.getCarrier());
			transferSegment.setPnrSegmentId(oldSegmentDetail.getPnrSegId().toString());
			transferSegment.setTargetFlightSegmentId(newSegmentDetail.getFlightSegId().toString());
			transferSegment.setNewExternalSegment(newExternalSegment);
			transferSegment.setOldExternalSegment(oldExternalSegment);
			transferSegments.add(transferSegment);
			i++;
		}

		transferSegmentsRQ.getTransferSegments().addAll(transferSegments);
		transferSegmentsRQ.setPnr(transferSegmentTO.getPnr());
		transferSegmentsRQ.setReservationVersion(transferSegmentTO.getReservationVersion());
		transferSegmentsRQ.setSalesChannelKey(salesChannelKey);
		
		return transferSegmentsRQ;
	}

	public static LCCReprotectSegmentsRQ transform(LCCClientTransferSegmentTO transferSegmentTO,
			Map<Integer, String> fltSegToPnrSegMap, UserPrincipal userPrincipal) {
		LCCReprotectSegmentsRQ lccReprotectSegmentsRQ = new LCCReprotectSegmentsRQ();

		ReprotectSegment reprotectSegment = new ReprotectSegment();

		Map<String, SegmentDetails> newSegmentDetails = transferSegmentTO.getNewSegmentDetails();
		Map<String, SegmentDetails> oldSegmentDetails = transferSegmentTO.getOldSegmentDetails();

		// new transfer segments
		for (String segmentCode : newSegmentDetails.keySet()) {
			SegmentDetails newSegmentDetail = newSegmentDetails.get(segmentCode);

			ExternalSegment newExternalSegment = new ExternalSegment();
			newExternalSegment.setArrivalDate(newSegmentDetail.getArrivalDate());
			newExternalSegment.setArrivalDateZulu(newSegmentDetail.getArrivalDateZulu());
			newExternalSegment.setDepartureDate(newSegmentDetail.getDepartureDate());
			newExternalSegment.setDepartureDateZulu(newSegmentDetail.getDepartureDateZulu());
			// adding the flight segment Id for new transfer segment.
			newExternalSegment.setFlightSegId(newSegmentDetail.getFlightSegId());
			newExternalSegment.setCabinClass(newSegmentDetail.getCabinClass());
			newExternalSegment.setLogicalCabinClass(newSegmentDetail.getLogicalCabinClass());
			newExternalSegment.setFlightNumber(newSegmentDetail.getFlightNumber());
			newExternalSegment.setSegmentCode(newSegmentDetail.getSegmentCode());
			newExternalSegment.setStatus(SegmentStatus.fromValue(newSegmentDetail.getStatus()));

			reprotectSegment.getNewExternalSegment().add(newExternalSegment);
		}

		// old transfer segments
		for (String segmentCode : oldSegmentDetails.keySet()) {
			SegmentDetails oldSegmentDetail = oldSegmentDetails.get(segmentCode);

			ExternalSegment oldExternalSegment = new ExternalSegment();
			oldExternalSegment.setArrivalDate(oldSegmentDetail.getArrivalDate());
			oldExternalSegment.setArrivalDateZulu(oldExternalSegment.getArrivalDateZulu());
			oldExternalSegment.setDepartureDate(oldSegmentDetail.getDepartureDate());
			oldExternalSegment.setDepartureDateZulu(oldExternalSegment.getDepartureDateZulu());

			oldExternalSegment.setFlightSegId(oldSegmentDetail.getFlightSegId());
			oldExternalSegment.setCabinClass(oldSegmentDetail.getCabinClass());
			oldExternalSegment.setLogicalCabinClass(oldSegmentDetail.getLogicalCabinClass());
			oldExternalSegment.setFlightNumber(oldSegmentDetail.getFlightNumber());
			oldExternalSegment.setSegmentCode(oldSegmentDetail.getSegmentCode());
			oldExternalSegment.setStatus(SegmentStatus.fromValue(oldSegmentDetail.getStatus()));

			reprotectSegment.getOldExternalSegment().add(oldExternalSegment);
		}

		// pnr information
		Map<Integer, PerFlightSegTransferRes> mapPerFlightSegTransferRes = new HashMap<Integer, PerFlightSegTransferRes>();

		for (Integer fltSegId : fltSegToPnrSegMap.keySet()) {

			PerFlightSegTransferRes flightSegTransferRes = mapPerFlightSegTransferRes.get(fltSegId);
			if (flightSegTransferRes == null) {
				flightSegTransferRes = new PerFlightSegTransferRes();
				flightSegTransferRes.setFlightSegId(fltSegId);
				mapPerFlightSegTransferRes.put(fltSegId, flightSegTransferRes);
			}

			TransferReservationInfo transferReservationInfo = new TransferReservationInfo();
			String[] pnrInformation = fltSegToPnrSegMap.get(fltSegId).split("-");
			String pnr = pnrInformation[0];
			String version = pnrInformation[1];
			Integer pnrSegmentId = Integer.valueOf(pnrInformation[2]);
			String cabinClass = pnrInformation[3];

			transferReservationInfo.setPnr(pnr);
			transferReservationInfo.setVersion(version);
			transferReservationInfo.setPnrSegmentId(pnrSegmentId);
			transferReservationInfo.setCabinClass(cabinClass);
			transferReservationInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			flightSegTransferRes.getTransferReservationInfo().add(transferReservationInfo);
		}

		reprotectSegment.getPerFlightSegTransferRes().addAll(mapPerFlightSegTransferRes.values());
		if (transferSegmentTO.getReprotectFlightSegIds() != null && transferSegmentTO.getReprotectFlightSegIds().size() > 0) {
			reprotectSegment.getReProtectFltSegIds().addAll(transferSegmentTO.getReprotectFlightSegIds());
		}
		if (transferSegmentTO.getIBEVisibleFlag() != null) {
			reprotectSegment.setIbeVisibleOnly(transferSegmentTO.getIBEVisibleFlag());
		}

		lccReprotectSegmentsRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal));
		lccReprotectSegmentsRQ.setReprotectSegments(reprotectSegment);

		return lccReprotectSegmentsRQ;
	}

	/**
	 * Converts the BookedFlightAlertInfo to AccleAero TO's
	 * 
	 * @param BookedFlightAlertInfo
	 * @return
	 */
	private static List<LCCClientAlertInfoTO> transformFlightAlert(List<BookedFlightAlertInfo> bookedFlightAlertInfos) {
		List<LCCClientAlertInfoTO> alertInfoTOs = new ArrayList<LCCClientAlertInfoTO>();
		if (bookedFlightAlertInfos != null) {
			for (BookedFlightAlertInfo bookedFlightAlertInfo : bookedFlightAlertInfos) {
				LCCClientAlertInfoTO alertInfoTO = new LCCClientAlertInfoTO();

				List<AlertInfo> alertInfos = bookedFlightAlertInfo.getAlert();
				List<LCCClientAlertTO> clientAlertTOs = new ArrayList<LCCClientAlertTO>();
				if (alertInfos != null && alertInfos.size() != 0) {
					alertInfoTO.setFlightSegmantRefNumber(bookedFlightAlertInfo.getFlightRefNumber());
					for (AlertInfo alertInfo : alertInfos) {
						LCCClientAlertTO clientAlertTO = new LCCClientAlertTO();
						clientAlertTO.setContent(alertInfo.getContent());
						clientAlertTO.setAlertId(alertInfo.getAlertId());
						clientAlertTO.setIbeVisibleOnly(alertInfo.getVisibleIBEonly());
						clientAlertTO.setIbeActioned(alertInfo.getActionedByIBE());
						clientAlertTO.setHasFlightSegments(alertInfo.isHasFlightSegments());
						clientAlertTOs.add(clientAlertTO);
					}
					alertInfoTO.setAlertTO(clientAlertTOs);
					alertInfoTOs.add(alertInfoTO);
				}

			}

		}
		return alertInfoTOs;
	}

	private static String[] getTravelRoute(Set<String> segmentCodes) {
		LinkedList<String> linkedSegments = new LinkedList<String>();
		for (String segmentCode : segmentCodes) {
			if (linkedSegments.size() == 0) {
				linkedSegments.add(segmentCode.split("/")[0]);
				linkedSegments.add(segmentCode.split("/")[1]);
			} else {
				String[] stations = segmentCode.split("/");
				if (linkedSegments.indexOf(stations[1]) != -1 && linkedSegments.indexOf(stations[0]) != -1) {
					continue;
				} else if (linkedSegments.indexOf(stations[0]) != -1) {
					if (linkedSegments.indexOf(stations[1]) == -1) {
						linkedSegments.add(linkedSegments.indexOf(stations[0]) + 1, stations[1]);
					}
				} else if (linkedSegments.indexOf(stations[1]) != -1) {
					if (linkedSegments.indexOf(stations[0]) == -1) {
						linkedSegments.add(linkedSegments.indexOf(stations[1]), stations[0]);
					}
				} else {
					linkedSegments.add(segmentCode.split("/")[0]);
					linkedSegments.add(segmentCode.split("/")[1]);
				}
			}
		}
		return linkedSegments.toArray(new String[linkedSegments.size()]);
	}

	public static Collection<ReservationPaxDTO> transform(LCCPaxCreditSearchRS lccPaxCreditSearchRS) throws ModuleException {
		Collection<ReservationPaxDTO> results = new ArrayList<ReservationPaxDTO>();
		if (lccPaxCreditSearchRS.getResponseAttributes().getSuccess() == null) {
			LCCError lccError = BeanUtils.getFirstElement(lccPaxCreditSearchRS.getResponseAttributes().getErrors());
			throw new ModuleException(lccError.getErrorCode().value());
		}

		List<ReservationPassengerDetailInfo> resPaxDetails = lccPaxCreditSearchRS.getPaxCretidDetailInfo();
		if (!resPaxDetails.isEmpty()) {
			for (ReservationPassengerDetailInfo resPaxDetail : resPaxDetails) {
				results.add(transformReservationPassengerDetailInfo(resPaxDetail));
			}
		}
		return results;
	}

	private static ReservationPaxDTO transformReservationPassengerDetailInfo(ReservationPassengerDetailInfo resPaxDetail)
			throws ModuleException {
		ReservationPaxDTO dto = new ReservationPaxDTO();
		dto.setCarrierCode(resPaxDetail.getCarrierCode());
		dto.setPnr(resPaxDetail.getPnr());
		dto.setPnrDate(resPaxDetail.getPnrDate());
		dto.setStatus(resPaxDetail.getStatus());

		int[] paxCounts = getPaxCountByType(resPaxDetail.getPassengerTypeQuantity());
		dto.setAdultCount(paxCounts[0]);
		dto.setChildCount(paxCounts[1]);
		dto.setInfantCount(paxCounts[2]);

		// adding contact info
		ContactInfo contactInfo = resPaxDetail.getContactInfo();
		if (contactInfo != null) {
			ReservationContactInfo reservationContactInfo = new ReservationContactInfo();
			reservationContactInfo.setFirstName(contactInfo.getPersonName().getFirstName());
			reservationContactInfo.setLastName(contactInfo.getPersonName().getSurName());
			dto.setReservationContactInfo(reservationContactInfo);
		}

		// adding pax detail
		List<AirTraveler> airTravelrList = resPaxDetail.getPassengers();
		if (airTravelrList != null) {
			for (AirTraveler airTraveler : airTravelrList) {
				ReservationPaxDetailsDTO paxDetailsDTO = new ReservationPaxDetailsDTO();
				paxDetailsDTO.setPnrPaxId(airTraveler.getPnrPaxId());
				paxDetailsDTO.setPaxType(LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(airTraveler.getPassengerType()));
				paxDetailsDTO.setCredit(airTraveler.getCredit());
				paxDetailsDTO.setResidingCarrierAmount(airTraveler.getResidingCarrierCredit());
				paxDetailsDTO.setFirstName(airTraveler.getPersonName().getFirstName());
				paxDetailsDTO.setLastName(airTraveler.getPersonName().getSurName());
				paxDetailsDTO.setTitle(airTraveler.getPersonName().getTitle());
				dto.addPassenger(paxDetailsDTO);
			}
		}

		List<BookingFlightSegment> flightSerments = resPaxDetail.getFlightSegments();
		if (flightSerments != null) {
			for (BookingFlightSegment flightSegment : flightSerments) {
				ReservationSegmentDTO reservationSegmentDTO = new ReservationSegmentDTO();
				reservationSegmentDTO.setFlightNo(flightSegment.getFlightNumber());
				reservationSegmentDTO.setSegmentCode(flightSegment.getSegmentCode());
				reservationSegmentDTO.setDepartureDate(flightSegment.getDepatureDateTime());
				reservationSegmentDTO.setZuluDepartureDate(flightSegment.getDepatureDateTimeZulu());
				reservationSegmentDTO.setArrivalDate(flightSegment.getArrivalDateTime());
				reservationSegmentDTO.setZuluArrivalDate(flightSegment.getArrivalDateTimeZulu());
				dto.addSegment(reservationSegmentDTO);
			}
		}

		List<BookingFlightSegment> exFlightSerments = resPaxDetail.getExternalflightSegments();
		if (exFlightSerments != null) {
			for (BookingFlightSegment exFlightSegment : exFlightSerments) {
				ReservationExternalSegmentTO externalSegmentTO = new ReservationExternalSegmentTO();
				externalSegmentTO.setFlightNo(exFlightSegment.getFlightNumber());
				externalSegmentTO.setStatus(exFlightSegment.getStatus().toString());
				externalSegmentTO.setSegmentCode(exFlightSegment.getSegmentCode());
				externalSegmentTO.setDepartureDate(exFlightSegment.getDepatureDateTime());
				externalSegmentTO.setArrivalDate(exFlightSegment.getArrivalDateTime());
				dto.addExternalSegment(externalSegmentTO);
			}

		}

		return dto;
	}

	public static Map<EXTERNAL_CHARGES, BigDecimal> createExternalChargesSummary(List<SurchargeTO> surcharges) {
		Map<EXTERNAL_CHARGES, BigDecimal> chargesSummary = new HashMap<EXTERNAL_CHARGES, BigDecimal>();
		Map externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();

		BigDecimal insuranceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal seatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal mealCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal baggageCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal flixiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditCardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal inflightServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal airportServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal promotionRewardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal additionalSeatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal anciPenaltyCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (surcharges != null) {
			for (SurchargeTO surchargeTO : surcharges) {
				if (surchargeTO != null) {

					if (surchargeTO.getSurchargeCode() != null) {
						if (surchargeTO.getSurchargeCode().equals(externalChargesMap.get(EXTERNAL_CHARGES.INSURANCE.toString()))) {
							insuranceCharge = AccelAeroCalculator.add(insuranceCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode()
								.equals(externalChargesMap.get(EXTERNAL_CHARGES.MEAL.toString()))) {
							mealCharge = AccelAeroCalculator.add(mealCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.BAGGAGE.toString()))) {
							baggageCharge = AccelAeroCalculator.add(baggageCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.SEAT_MAP.toString()))) {
							seatCharge = AccelAeroCalculator.add(seatCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString()))) {
							inflightServiceCharge = AccelAeroCalculator.add(inflightServiceCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.FLEXI_CHARGES.toString()))) {
							flixiCharge = AccelAeroCalculator.add(flixiCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.CREDIT_CARD.toString()))) {
							creditCardCharge = AccelAeroCalculator.add(creditCardCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.AIRPORT_SERVICE.toString()))) {
							airportServiceCharge = AccelAeroCalculator.add(airportServiceCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.PROMOTION_REWARD.toString()))) {
							promotionRewardCharge = AccelAeroCalculator.add(promotionRewardCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE.toString()))) {
							additionalSeatCharge = AccelAeroCalculator.add(additionalSeatCharge, surchargeTO.getAmount());
						} else if (surchargeTO.getSurchargeCode().equals(
								externalChargesMap.get(EXTERNAL_CHARGES.ANCI_PENALTY.toString()))) {
							anciPenaltyCharge = AccelAeroCalculator.add(anciPenaltyCharge, surchargeTO.getAmount());
						}
					}
				}
			}

		}

		chargesSummary.put(EXTERNAL_CHARGES.INSURANCE, insuranceCharge);
		chargesSummary.put(EXTERNAL_CHARGES.MEAL, mealCharge);
		chargesSummary.put(EXTERNAL_CHARGES.BAGGAGE, baggageCharge);
		chargesSummary.put(EXTERNAL_CHARGES.SEAT_MAP, seatCharge);
		chargesSummary.put(EXTERNAL_CHARGES.INFLIGHT_SERVICES, inflightServiceCharge);
		chargesSummary.put(EXTERNAL_CHARGES.FLEXI_CHARGES, flixiCharge);
		chargesSummary.put(EXTERNAL_CHARGES.CREDIT_CARD, creditCardCharge);
		chargesSummary.put(EXTERNAL_CHARGES.AIRPORT_SERVICE, airportServiceCharge);
		chargesSummary.put(EXTERNAL_CHARGES.PROMOTION_REWARD, promotionRewardCharge);
		chargesSummary.put(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE, additionalSeatCharge);
		chargesSummary.put(EXTERNAL_CHARGES.ANCI_PENALTY, anciPenaltyCharge);

		return chargesSummary;
	}

	public static void transformSegementModifications(Set<LCCClientReservationSegment> reservationSegments) {
		Map<String, List<LCCClientReservationSegment>> groupings = new HashMap<String, List<LCCClientReservationSegment>>();
		Map<String, Boolean[]> groupValues = new HashMap<String, Boolean[]>();

		// extract the group keys
		for (LCCClientReservationSegment reservationSegment : reservationSegments) {
			String groupKey = reservationSegment.getInterlineGroupKey();
			if (!groupings.keySet().contains(groupKey)) {
				groupings.put(groupKey, new ArrayList<LCCClientReservationSegment>());
				groupValues.put(groupKey, new Boolean[] { true, true });
			}
			groupings.get(groupKey).add(reservationSegment);

			Boolean arr[] = groupValues.get(groupKey);
			if (!reservationSegment.isModifyByDate()) {
				arr[0] = false;
			}
			if (!reservationSegment.isModifyByRoute()) {
				arr[1] = false;
			}
		}

		// traversing segments with groupings and segment modifications are setting according to the segment group
		for (String groupKey : groupings.keySet()) {
			List<LCCClientReservationSegment> group = groupings.get(groupKey);
			Boolean arr[] = groupValues.get(groupKey);
			for (LCCClientReservationSegment segment : group) {
				segment.setModifyByDate(arr[0]);
				segment.setModifyByRoute(arr[1]);
			}
		}

	}

	public static LCCAnciModifyRQ transformAnciModification(LCCAnciModificationRequest anciModificationRequest) {
		CommonAncillaryModifyAssembler ancillaryModifyAssembler = anciModificationRequest.getCommonAncillaryModifyAssembler();
		TrackInfoDTO trackInfoDTO = anciModificationRequest.getTrackInfoDTO();
		UserPrincipal userPrincipal = anciModificationRequest.getUserPrincipal();
		boolean isEnableFraudCheck = anciModificationRequest.isEnableFraudCheck();

		LCCAnciModifyRQ lccAnciModifyRQ = new LCCAnciModifyRQ();
		lccAnciModifyRQ.setCommonAnciModifyAssembler(transformAnciModifyAssembler(ancillaryModifyAssembler));
		lccAnciModifyRQ.setTrackingInfo(transformTrackingInfo(trackInfoDTO));
		lccAnciModifyRQ.setFraudCheckEnbaled(isEnableFraudCheck);

		List<StringStringListMap> flightRefWiseList = LCCAncillaryUtil
				.populateFlightRefWiseSelectedAnciMap(anciModificationRequest.getFlightRefWiseSelectedMeals());
		if (flightRefWiseList != null) {
			lccAnciModifyRQ.getCommonAnciModifyAssembler().getFlightRefWiseSelectedMeals().addAll(flightRefWiseList);
		}

		HeaderInfo headerInfo = LCCClientCommonUtils.createHeaderInfo(null);
		LCCPOS lccpos = LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO);

		lccAnciModifyRQ.setHeaderInfo(headerInfo);
		lccAnciModifyRQ.setLccPos(lccpos);

		return lccAnciModifyRQ;
	}

	private static CommonAnciModifyAssembler
			transformAnciModifyAssembler(CommonAncillaryModifyAssembler ancillaryModifyAssembler) {

		Map<Integer, LCCClientPaymentAssembler> passengerPaymentMap = ancillaryModifyAssembler.getPassengerPaymentMap();
		Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxAddAnciMap = ancillaryModifyAssembler.getPaxAddAncillaryMap();
		Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxRemoveAnciMap = ancillaryModifyAssembler.getPaxRemoveAncillaryMap();
		Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxUpdateAnciMap = ancillaryModifyAssembler.getPaxUpdateAncillaryMap();
		String pnr = ancillaryModifyAssembler.getPnr();
		SYSTEM system = ancillaryModifyAssembler.getTargetSystem();
		Map<Integer, CommonCreditCardPaymentInfo> tempPaymentMap = ancillaryModifyAssembler.getTemporyPaymentMap();
		String transactionId = ancillaryModifyAssembler.getTransactionIdentifier();
		String version = ancillaryModifyAssembler.getVersion();

		CommonAnciModifyAssembler commonAnciModifyAssembler = new CommonAnciModifyAssembler();
		if (passengerPaymentMap != null) {
			transformPassengerPaymentMap(commonAnciModifyAssembler, passengerPaymentMap);
		}
		if (paxAddAnciMap != null) {
			transformAnciMap(paxAddAnciMap, commonAnciModifyAssembler.getPaxAddAncillaryMap());
		}
		if (paxRemoveAnciMap != null) {
			transformAnciMap(paxRemoveAnciMap, commonAnciModifyAssembler.getPaxRemoveAncillaryMap());
		}
		if (paxUpdateAnciMap != null) {
			transformAnciMap(paxUpdateAnciMap, commonAnciModifyAssembler.getPaxUpdateAncillaryMap());
		}
		commonAnciModifyAssembler.setPnr(pnr);
		commonAnciModifyAssembler.setSystem(system.toString());
		if (tempPaymentMap != null) {
			transformTempPaymentMap(tempPaymentMap, commonAnciModifyAssembler.getTempPaymentMap());
		}
		commonAnciModifyAssembler.setTransactionIdentifier(transactionId);
		commonAnciModifyAssembler.setServiceTaxRatio(ancillaryModifyAssembler.getServiceTaxRatio());
		commonAnciModifyAssembler.setVersion(version);
		commonAnciModifyAssembler
				.setApplyPenaltyForAnciModification(ancillaryModifyAssembler.isApplyPenaltyForAnciModification());
		commonAnciModifyAssembler.setForceConfirm(ancillaryModifyAssembler.isForceConfirm());
		
		return commonAnciModifyAssembler;
	}

	private static void transformPassengerPaymentMap(CommonAnciModifyAssembler commonAnciModifyAssembler,
			Map<Integer, LCCClientPaymentAssembler> passengerPaymentMap) {

		for (Integer i : passengerPaymentMap.keySet()) {

			LCCClientPaymentAssembler lccClientPaymentAssembler = passengerPaymentMap.get(i);
			Collection<LCCClientExternalChgDTO> externalCharges = lccClientPaymentAssembler.getPerPaxExternalCharges();
			BigDecimal totalPayAmount = lccClientPaymentAssembler.getTotalPayAmount();
			boolean isPaymentExist = lccClientPaymentAssembler.isPaymentExist();

			ClientPaymentAssembler clientPaymentAssembler = new ClientPaymentAssembler();
			clientPaymentAssembler.setPaxType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(lccClientPaymentAssembler.getPaxType()));
			clientPaymentAssembler.setPaymentExist(isPaymentExist);
			clientPaymentAssembler.setTotalAmount(totalPayAmount);
			// transformPaymentInfo(payments, clientPaymentAssembler);

			for (LCCClientExternalChgDTO externalChgDTO : (ArrayList<LCCClientExternalChgDTO>) externalCharges) {
				ClientExternalChg clientExternalChg = new ClientExternalChg();
				clientExternalChg.setAmount(externalChgDTO.getAmount());
				clientExternalChg.setCarrierCode(externalChgDTO.getCarrierCode());
				clientExternalChg.setCode(externalChgDTO.getCode());
				clientExternalChg.setExternalCharges(externalChgDTO.getExternalCharges().toString());
				clientExternalChg.setFlightRefNumber(externalChgDTO.getFlightRefNumber());
				clientExternalChg.setIsAmountConsumedForPayment(externalChgDTO.isAmountConsumedForPayment());
				clientExternalChg.setAirportCode(externalChgDTO.getAirportCode());
				clientPaymentAssembler.getPerPaxExternalCharges().add(clientExternalChg);
			}

			PassengerPaymentMap paymentMap = new PassengerPaymentMap();
			paymentMap.setKey(i);
			paymentMap.setValue(clientPaymentAssembler);

			commonAnciModifyAssembler.getPassengerPaymentMap().add(paymentMap);
		}
	}

	private static void transformAnciMap(Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> anciMap,
			List<AncillaryMap> ancillaryMapList) {

		Map<String, Collection<FlightSegmentInfoTO>> ondBaggageGrpIdWiseFlightSegments = getOndBaggageGrpIdWiseFlightSegments(anciMap);

		for (Integer i : anciMap.keySet()) {
			AncillaryMap ancillaryMap = new AncillaryMap();
			ancillaryMap.setKey(i);

			List<LCCSelectedSegmentAncillaryDTO> ancillaryDTOList = anciMap.get(i);
			for (LCCSelectedSegmentAncillaryDTO ancillaryDTO : ancillaryDTOList) {

				LCCAirSeatDTO lccAirSeatDTO = ancillaryDTO.getAirSeatDTO();
				String carrierCode = ancillaryDTO.getCarrierCode();
				FlightSegmentTO flightSegmentTO = ancillaryDTO.getFlightSegmentTO();
				List<LCCInsuranceQuotationDTO> insuranceQuotationDTOs = ancillaryDTO.getInsuranceQuotations();
				List<LCCMealDTO> mealDTOList = ancillaryDTO.getMealDTOs();
				List<LCCBaggageDTO> baggageDTOList = ancillaryDTO.getBaggageDTOs();
				List<LCCSpecialServiceRequestDTO> ssrList = ancillaryDTO.getSpecialServiceRequestDTOs();
				List<LCCAirportServiceDTO> airportServiceList = ancillaryDTO.getAirportServiceDTOs();
				String travelRefNumber = ancillaryDTO.getTravelerRefNumber();
				String paxType = ancillaryDTO.getPaxType();

				SelectedSegmentAncillary selectedSegmentAncillary = new SelectedSegmentAncillary();

				Seat seat = new Seat();
				if (lccAirSeatDTO != null) {
					seat.setBookedPassengerType(LCCClientCommonUtils.getLCCPaxTypeCode(lccAirSeatDTO.getBookedPassengerType()));
					seat.setQuotedCharge(lccAirSeatDTO.getSeatCharge());
					seat.setSeatAvailability(lccAirSeatDTO.getSeatAvailability());
					seat.setSeatCharge(lccAirSeatDTO.getSeatCharge());
					seat.setSeatMessage(lccAirSeatDTO.getSeatMessage());
					seat.setSeatNumber(lccAirSeatDTO.getSeatNumber());
					seat.setSeatType(getSeatType(lccAirSeatDTO.getSeatType()));

					for (String notAllowedType : lccAirSeatDTO.getNotAllowedPassengerType()) {
						if (notAllowedType.equals(PaxTypeTO.ADULT)) {
							seat.getNotAllowedPassengerType().add(PassengerType.ADT);
						} else if (notAllowedType.equals(PaxTypeTO.CHILD)) {
							seat.getNotAllowedPassengerType().add(PassengerType.CHD);
						} else if (notAllowedType.equals(PaxTypeTO.INFANT)) {
							seat.getNotAllowedPassengerType().add(PassengerType.INF);
						} else if (notAllowedType.equals(PaxTypeTO.PARENT)) {
							seat.getNotAllowedPassengerType().add(PassengerType.PRT);
						}
					}

					selectedSegmentAncillary.setAirSeatDTO(seat);
				}

				FlightSegmentInfoTO flightSegmentInfoTO = transformFlightSegmentTO(flightSegmentTO);
				selectedSegmentAncillary.setFlightSegmentTO(flightSegmentInfoTO);

				if (insuranceQuotationDTOs != null && !insuranceQuotationDTOs.isEmpty()) {
					List<InsuranceQuotation> insuranceQuotations = new ArrayList<InsuranceQuotation>();

					for (LCCInsuranceQuotationDTO insuranceQuotationDTO : insuranceQuotationDTOs) {
						InsuranceQuotation insuranceQuotation = new InsuranceQuotation();
						insuranceQuotation.setInsuranceRefNumber(Integer.parseInt(insuranceQuotationDTO.getInsuranceRefNumber()));
						insuranceQuotation.setOperatingAirline(insuranceQuotationDTO.getOperatingAirline());
						insuranceQuotation.setPolicyCode(insuranceQuotationDTO.getPolicyCode());
						insuranceQuotation.setQuotedCurrencyCode(insuranceQuotationDTO.getQuotedCurrencyCode());
						insuranceQuotation.setQuotedTotalPremiumAmount(insuranceQuotationDTO.getQuotedTotalPremiumAmount());
						insuranceQuotation.setTotalPremiumAmount(insuranceQuotationDTO.getQuotedTotalPremiumAmount());
						insuranceQuotation.setSsrFeeCode(insuranceQuotationDTO.getSsrFeeCode());
						insuranceQuotation.setPlanCode(insuranceQuotationDTO.getPlanCode());
						insuranceQuotations.add(insuranceQuotation);

					}
					selectedSegmentAncillary.getInsuranceQuotationDTOs().addAll(insuranceQuotations);
				}

				if (mealDTOList != null) {
					for (LCCMealDTO mealDTO : mealDTOList) {
						Meal meal = new Meal();
						meal.setAllocatedMeals(mealDTO.getAllocatedMeals());
						meal.setAvailableMeals(mealDTO.getAvailableMeals());
						meal.setMealCharge(mealDTO.getMealCharge());
						meal.setMealCode(mealDTO.getMealCode());
						meal.setMealDescription(mealDTO.getMealDescription());
						meal.setMealImageLink(mealDTO.getMealImagePath());
						meal.setMealName(mealDTO.getMealName());
						meal.setQuotedCharge(mealDTO.getMealCharge());
						meal.setSoldMeals(mealDTO.getSoldMeals());
						meal.setTranslatedMealDescription(mealDTO.getTranslatedMealDescription());
						meal.setTranslatedMealTitle(mealDTO.getTranslatedMealTitle());
						meal.setMealCategoryID(mealDTO.getMealCategoryID());
						meal.setMealCategoryCode(mealDTO.getMealCategoryCode());
						meal.setIsAnciOffer(mealDTO.isAnciOffer());
						meal.setOfferedAnciTemplateID(mealDTO.getOfferedTemplateID());
						selectedSegmentAncillary.getMealDTOs().add(meal);
					}
				}

				if (baggageDTOList != null && baggageDTOList.size() > 0) {
					selectedSegmentAncillary.getExternalFlightSegmentTOs().addAll(
							getExternalFlightSegments(flightSegmentInfoTO, ondBaggageGrpIdWiseFlightSegments));

					for (LCCBaggageDTO baggageDTO : baggageDTOList) {
						Baggage baggage = new Baggage();
						baggage.setBaggageCharge(baggageDTO.getBaggageCharge());
						baggage.setBaggageDescription(baggageDTO.getBaggageDescription());
						baggage.setBaggageName(baggageDTO.getBaggageName());
						baggage.setQuotedCharge(baggageDTO.getBaggageCharge());
						baggage.setOndChargeId(baggageDTO.getOndBaggageChargeId());

						selectedSegmentAncillary.getBaggageDTOs().add(baggage);
					}
				}

				if (ssrList != null) {
					for (LCCSpecialServiceRequestDTO ssrDto : ssrList) {
						SpecialServiceRequest ssrRequest = new SpecialServiceRequest();
						ssrRequest.setAvailableQty(ssrDto.getAvailableQty());
						ssrRequest.setCarrierCode(ssrDto.getCarrierCode());
						ssrRequest.setCharge(ssrDto.getCharge());
						ssrRequest.setDescription(ssrDto.getDescription());
						ssrRequest.setServiceQuantity(ssrDto.getServiceQuantity());
						ssrRequest.setSsrCode(ssrDto.getSsrCode());
						ssrRequest.setStatus(ssrDto.getStatus());
						ssrRequest.setText(ssrDto.getText());
						ssrRequest.setTravelerRefNumber(travelRefNumber);

						selectedSegmentAncillary.getSpecialServiceRequestDTOs().add(ssrRequest);
					}
				}

				if (airportServiceList != null) {
					for (LCCAirportServiceDTO airportServiceDTO : airportServiceList) {
						AirportServiceRequest airportServiceRequest = new AirportServiceRequest();
						airportServiceRequest.setAirportCode(airportServiceDTO.getAirportCode());
						airportServiceRequest.setApplicabilityType(airportServiceDTO.getApplicabilityType());
						airportServiceRequest.setCarrierCode(airportServiceDTO.getCarrierCode());
						airportServiceRequest.setDescription(airportServiceDTO.getSsrDescription());
						airportServiceRequest.setServiceCharge(airportServiceDTO.getServiceCharge());
						airportServiceRequest.setSsrCode(airportServiceDTO.getSsrCode());
						airportServiceRequest.setApplyOn(airportServiceDTO.getApplyOn());
						airportServiceRequest.setTravelerRefNumber(travelRefNumber);

						selectedSegmentAncillary.getAirportServiceRequestDTOs().add(airportServiceRequest);
					}
				}

				selectedSegmentAncillary.setCarrierCode(carrierCode);
				selectedSegmentAncillary.setTravelerRefNumber(travelRefNumber);
				if (paxType != null) {
					selectedSegmentAncillary.setPaxType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxType));
				}
				ancillaryMap.getValue().add(selectedSegmentAncillary);
			}
			ancillaryMapList.add(ancillaryMap);
		}

	}

	private static Collection<FlightSegmentInfoTO> getExternalFlightSegments(FlightSegmentInfoTO flightSegmentInfoTO,
			Map<String, Collection<FlightSegmentInfoTO>> ondBaggageGrpIdWiseFlightSegments) {
		Collection<FlightSegmentInfoTO> externalFlightSegments = new ArrayList<FlightSegmentInfoTO>();

		String baggageONDGrpId = BeanUtils.nullHandler(flightSegmentInfoTO.getBaggageONDGroupId());
		if (baggageONDGrpId.length() > 0) {
			Collection<FlightSegmentInfoTO> colFlightSegmentInfoTO = ondBaggageGrpIdWiseFlightSegments.get(baggageONDGrpId);
			if (colFlightSegmentInfoTO != null && colFlightSegmentInfoTO.size() > 0) {
				for (FlightSegmentInfoTO externalFlightSegmentInfoTO : colFlightSegmentInfoTO) {
					if (!isSameObject(flightSegmentInfoTO, externalFlightSegmentInfoTO)) {
						externalFlightSegments.add(externalFlightSegmentInfoTO);
					}
				}
			}
		}
		return externalFlightSegments;
	}

	private static boolean isSameObject(FlightSegmentInfoTO source, FlightSegmentInfoTO target) {

		String srcFltRefNo = BeanUtils.nullHandler(source.getFlightRefNumber());
		String tgtFltRefNo = BeanUtils.nullHandler(target.getFlightRefNumber());

		if (srcFltRefNo.equals(tgtFltRefNo)) {
			return true;
		} else {
			return false;
		}
	}

	private static Map<String, Collection<FlightSegmentInfoTO>> getOndBaggageGrpIdWiseFlightSegments(
			Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> anciMap) {

		Map<String, Map<String, FlightSegmentInfoTO>> ondGrpIdWiseUniqueSegments = new HashMap<String, Map<String, FlightSegmentInfoTO>>();

		for (List<LCCSelectedSegmentAncillaryDTO> elements : anciMap.values()) {
			for (LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO : elements) {
				String baggageOndGroupId = BeanUtils.nullHandler(lccSelectedSegmentAncillaryDTO.getFlightSegmentTO()
						.getBaggageONDGroupId());

				if (baggageOndGroupId.length() > 0) {
					if (ondGrpIdWiseUniqueSegments.containsKey(baggageOndGroupId)) {
						Map<String, FlightSegmentInfoTO> map = ondGrpIdWiseUniqueSegments.get(baggageOndGroupId);

						FlightSegmentInfoTO flightSegmentInfoTO = transformFlightSegmentTO(lccSelectedSegmentAncillaryDTO
								.getFlightSegmentTO());
						String fltRefNo = BeanUtils.nullHandler(flightSegmentInfoTO.getFlightRefNumber());

						if (!map.containsKey(fltRefNo)) {
							map.put(fltRefNo, flightSegmentInfoTO);
						}
					} else {
						FlightSegmentInfoTO flightSegmentInfoTO = transformFlightSegmentTO(lccSelectedSegmentAncillaryDTO
								.getFlightSegmentTO());
						Map<String, FlightSegmentInfoTO> map = new HashMap<String, FlightSegmentInfoTO>();
						map.put(BeanUtils.nullHandler(flightSegmentInfoTO.getFlightRefNumber()), flightSegmentInfoTO);
						ondGrpIdWiseUniqueSegments.put(baggageOndGroupId, map);
					}

				}
			}
		}

		Map<String, Collection<FlightSegmentInfoTO>> ondBaggageGrpIdWiseFlightSegments = new HashMap<String, Collection<FlightSegmentInfoTO>>();
		for (Entry<String, Map<String, FlightSegmentInfoTO>> entry : ondGrpIdWiseUniqueSegments.entrySet()) {
			ondBaggageGrpIdWiseFlightSegments.put(entry.getKey(), entry.getValue().values());
		}

		return ondBaggageGrpIdWiseFlightSegments;
	}

	private static FlightSegmentInfoTO transformFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		FlightSegmentInfoTO flightSegmentInfoTO = new FlightSegmentInfoTO();
		flightSegmentInfoTO.setAdultCount(flightSegmentTO.getAdultCount());
		flightSegmentInfoTO.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
		flightSegmentInfoTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
		flightSegmentInfoTO.setCabinClassCode(flightSegmentTO.getCabinClassCode());
		flightSegmentInfoTO.setChildCount(flightSegmentTO.getChildCount());
		flightSegmentInfoTO.setDepartureDateTime(flightSegmentTO.getDepartureDateTime());
		flightSegmentInfoTO.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
		if (flightSegmentTO.getFlightId() != null) {
			flightSegmentInfoTO.setFlightId(flightSegmentTO.getFlightId().toString());
		}
		flightSegmentInfoTO.setFlightNumber(flightSegmentTO.getFlightNumber());
		flightSegmentInfoTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
		if (flightSegmentTO.getFlightSegId() != null) {
			flightSegmentInfoTO.setFlightSegId(flightSegmentTO.getFlightSegId().toString());
		}
		flightSegmentInfoTO.setInfantCount(flightSegmentTO.getInfantCount());
		flightSegmentInfoTO.setOperatingAirline(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
		flightSegmentInfoTO.setOperationType(flightSegmentTO.getOperationType());
		flightSegmentInfoTO.setReturnFlag(flightSegmentTO.isReturnFlag());
		flightSegmentInfoTO.setSegmentCode(flightSegmentTO.getSegmentCode());
		flightSegmentInfoTO.setSubStationShortName(flightSegmentTO.getSubStationShortName());
		flightSegmentInfoTO.setBaggageONDGroupId(flightSegmentTO.getBaggageONDGroupId());
		flightSegmentInfoTO.setBookingFlightRefNumber(flightSegmentTO.getPnrSegId());

		return flightSegmentInfoTO;
	}

	private static void transformTempPaymentMap(Map<Integer, CommonCreditCardPaymentInfo> tempPaymentMap,
			List<PaymentMap> lccTempMap) {
		for (Integer i : tempPaymentMap.keySet()) {
			PaymentMap paymentMap = new PaymentMap();
			paymentMap.setKey(i);

			CommonCreditCardPaymentInfo creditCardPaymentInfo = tempPaymentMap.get(i);

			/*
			 * ClientPaymentInfo commonInfo = new ClientPaymentInfo();
			 * commonInfo.setActualPaymentMethod(creditCardPaymentInfo.getActualPaymentMethod());
			 * commonInfo.setCarrierCode(creditCardPaymentInfo.getCarrierCode());
			 * commonInfo.setIncludeExternalCharges(creditCardPaymentInfo.includeExternalCharges());
			 * commonInfo.setLccPaxTnxId(creditCardPaymentInfo.getLccPaxTnxId());
			 * commonInfo.setLccUniqueTnxId(creditCardPaymentInfo.getLccUniqueTnxId());
			 * commonInfo.setOriginalPayReference(creditCardPaymentInfo.getOriginalPayReference());
			 * commonInfo.setPayCurrencyAmount(creditCardPaymentInfo.getPayCurrencyAmount());
			 * 
			 * PayCurrencyDTO payCurrencyDTO = creditCardPaymentInfo.getPayCurrencyDTO();
			 * 
			 * PayCurrency payCurrency = new PayCurrency();
			 * payCurrency.setBoundaryValue(payCurrencyDTO.getBoundaryValue());
			 * payCurrency.setBreakPointValue(payCurrencyDTO.getBreakPointValue());
			 * payCurrency.setPayCurrencyCode(payCurrencyDTO.getPayCurrencyCode());
			 * payCurrency.setPayCurrencyExchangeRateId(payCurrency.getPayCurrencyExchangeRateId());
			 * payCurrency.setPayCurrMultiplyingExchangeRate(payCurrencyDTO.getPayCurrMultiplyingExchangeRate());
			 * payCurrency.setTotalPayCurrencyAmount(payCurrencyDTO.getTotalPayCurrencyAmount());
			 * 
			 * commonInfo.setPayCurrencyDTO(payCurrency);
			 * 
			 * commonInfo.setPayReference(creditCardPaymentInfo.getPayReference());
			 * commonInfo.setRecieptNumber(creditCardPaymentInfo.getRecieptNumber());
			 * commonInfo.setRemarks(creditCardPaymentInfo.getRemarks());
			 * commonInfo.setTotalAmount(creditCardPaymentInfo.getTotalAmount());
			 * commonInfo.setTotalAmountCurrencyCode(creditCardPaymentInfo.getTotalAmountCurrencyCode());
			 * commonInfo.setTxnDateTime(creditCardPaymentInfo.getTxnDateTime());
			 */

			CommonCreditCardPayment lccComCardPayment = new CommonCreditCardPayment();

			// lccComCardPayment.setClientPaymentInfo(commonInfo);
			lccComCardPayment.setAddress(creditCardPaymentInfo.getAddress());
			lccComCardPayment.setAppIndicator(creditCardPaymentInfo.getAppIndicator().toString());
			lccComCardPayment.setAuthorizationId(creditCardPaymentInfo.getAuthorizationId());
			lccComCardPayment.setCardHolderName(creditCardPaymentInfo.getCardHoldersName());
			lccComCardPayment.setEDate(creditCardPaymentInfo.geteDate());
			lccComCardPayment.setGroupPNR(creditCardPaymentInfo.getGroupPNR());
			lccComCardPayment.setIsPaymentSuccess(creditCardPaymentInfo.isPaymentSuccess());
			lccComCardPayment.setName(creditCardPaymentInfo.getName());
			lccComCardPayment.setNo(creditCardPaymentInfo.getNo());
			lccComCardPayment.setFirst4Digits(creditCardPaymentInfo.getNoFirstDigits());
			lccComCardPayment.setLast4Digits(creditCardPaymentInfo.getNoLastDigits());
			lccComCardPayment.setOldCCRecordId(creditCardPaymentInfo.getOldCCRecordId());
			lccComCardPayment.setPaymentBrokerId(creditCardPaymentInfo.getPaymentBrokerId());
			lccComCardPayment.setSecurityCode(creditCardPaymentInfo.getSecurityCode());
			lccComCardPayment.setTemporyPaymentId(creditCardPaymentInfo.getTemporyPaymentId());
			lccComCardPayment.setTnxMode(creditCardPaymentInfo.getTnxMode().getCode());
			// lccComCardPayment.setCardType()(creditCardPaymentInfo.getType());

			/*
			 * IPGIdentificationParamsDTO ipgIdentificationParamsDTO =
			 * creditCardPaymentInfo.getIpgIdentificationParamsDTO();
			 * 
			 * IPGIdentificationParams lccIdentificationParams = new IPGIdentificationParams();
			 * lccIdentificationParams.setFullyQualifiedIPGConfigName
			 * (ipgIdentificationParamsDTO.getFQIPGConfigurationName());
			 * lccIdentificationParams.setIPGConfigurationExists
			 * (ipgIdentificationParamsDTO.is_isIPGConfigurationExists());
			 * lccIdentificationParams.setIpgId(ipgIdentificationParamsDTO.getIpgId());
			 * lccIdentificationParams.setPaymentCurrencyCode(ipgIdentificationParamsDTO.getPaymentCurrencyCode());
			 */
			paymentMap.setValue(lccComCardPayment);

			lccTempMap.add(paymentMap);
		}
	}

	private static TrackInfo transformTrackingInfo(TrackInfoDTO trackInfoDTO) {
		TrackInfo trackInfo = new TrackInfo();
		trackInfo.setCarrierCode(trackInfoDTO.getCarrierCode());
		trackInfo.setIpAddress(trackInfoDTO.getIpAddress());
		trackInfo.setMarketingAgentCode(trackInfoDTO.getMarketingAgentCode());
		trackInfo.setMarketingAgentStationCode(trackInfoDTO.getMarketingAgentStationCode());
		trackInfo.setMarketingUserId(trackInfoDTO.getMarketingUserId());
		trackInfo.setOriginIPNumber(trackInfoDTO.getOriginIPNumber());
		trackInfo.setSessionId(trackInfoDTO.getSessionId());
		if (trackInfoDTO.getAppIndicator() != null) {
			trackInfo.setAppIndicator(AppIndicator.fromValue(trackInfoDTO.getAppIndicator().toString()));
		}
		return trackInfo;
	}

	private static SeatType getSeatType(String seatType) {
		if (seatType == null) {
			return null;
		}
		if (seatType.equalsIgnoreCase("EXIT")) {
			return SeatType.EXIT;
		} else {
			return null;
		}
	}

	public static void transform(List<CustomerCredit> customerCreditList, Collection<PaxCreditDTO> paxCreditDTOList) {
		if (customerCreditList != null && !customerCreditList.isEmpty()) {
			for (CustomerCredit customerCredit : customerCreditList) {
				PaxCreditDTO paxCreditDTO = new PaxCreditDTO();
				paxCreditDTO.setPnr(customerCredit.getPnr());
				paxCreditDTO.setTitle(customerCredit.getTitle());
				paxCreditDTO.setFirstName(customerCredit.getFirstName());
				paxCreditDTO.setLastName(customerCredit.getLastName());
				paxCreditDTO.setPaxSequence(customerCredit.getPaxSequence());
				paxCreditDTO.setBalance(customerCredit.getAmount() != null ? customerCredit.getAmount() : AccelAeroCalculator
						.getDefaultBigDecimalZero());
				paxCreditDTO.setDateOfExpiry(customerCredit.getExpireDate());
				paxCreditDTOList.add(paxCreditDTO);
			}
		}
	}

	/**
	 * Transforms a {@link ChargeAdjustmentType} object to a {@link ChargeAdjustmentTypeDTO} object.
	 * 
	 * @param adjType
	 *            : The {@link ChargeAdjustmentType} object to be transformed.
	 * 
	 * @return : The transformed {@link ChargeAdjustmentTypeDTO} object.
	 */
	public static ChargeAdjustmentTypeDTO transform(ChargeAdjustmentType adjType) {
		ChargeAdjustmentTypeDTO chargeAdjustmentTypeDTO = new ChargeAdjustmentTypeDTO();

		chargeAdjustmentTypeDTO.setChargeAdjustmentTypeId(adjType.getChargeAdjustmentTypeId());
		chargeAdjustmentTypeDTO.setChargeAdjustmentTypeName(adjType.getChargeAdjustmentTypeName());
		chargeAdjustmentTypeDTO.setDescription(adjType.getDescription());
		chargeAdjustmentTypeDTO.setRefundableChargeCode(adjType.getRefundableChargeCode());
		chargeAdjustmentTypeDTO.setNonRefundableChargeCode(adjType.getNonRefundableChargeCode());
		chargeAdjustmentTypeDTO.setRefundablePrivilegeId(adjType.getRefundablePrivilegeId());
		chargeAdjustmentTypeDTO.setNonRefundablePrivilegeId(adjType.getNonRefundablePrivilegeId());
		chargeAdjustmentTypeDTO.setDisplayOrder(adjType.getDisplayOrder());
		chargeAdjustmentTypeDTO.setDefaultAdjustment(adjType.isDefaultAdustment());
		chargeAdjustmentTypeDTO.setStatus(adjType.getStatus());

		return chargeAdjustmentTypeDTO;
	}

	private static Map<String, List<ChargeAdjustmentPrivilege>> transformChargeAdjustmentPrivileges(
			List<ChargeAdjustmentTypePrivileges> chargeAdjustmentPrivileges) {
		Map<String, List<ChargeAdjustmentPrivilege>> chargeAdjustmentPrivilegesMap = new HashMap<String, List<ChargeAdjustmentPrivilege>>();

		if (chargeAdjustmentPrivileges != null && chargeAdjustmentPrivileges.size() > 0) {

			for (ChargeAdjustmentTypePrivileges chargeAdjustmentTypePrivileges : chargeAdjustmentPrivileges) {
				chargeAdjustmentPrivilegesMap
						.put(chargeAdjustmentTypePrivileges.getCarrierCode(),
								trasnformChargeAdjustmentPrivilegeList(chargeAdjustmentTypePrivileges
										.getChargeAdjustmentPrivilegeList()));
			}
		}

		return chargeAdjustmentPrivilegesMap;
	}

	private static List<ChargeAdjustmentPrivilege> trasnformChargeAdjustmentPrivilegeList(
			List<ChargeAdjustmentTypePrivilege> chargeAdjustmentPrivilegeList) {
		List<ChargeAdjustmentPrivilege> chargeAdjustmentPrivileges = new ArrayList<ChargeAdjustmentPrivilege>();
		if (chargeAdjustmentPrivilegeList != null && chargeAdjustmentPrivilegeList.size() > 0) {
			for (ChargeAdjustmentTypePrivilege lccChargeAdjustmentTypePrivilege : chargeAdjustmentPrivilegeList) {
				ChargeAdjustmentPrivilege chargeAdjustmentPriv = new ChargeAdjustmentPrivilege();
				chargeAdjustmentPriv.setChargeAdjustmentTypeId(lccChargeAdjustmentTypePrivilege.getChargeAdjustmentTypeId());
				chargeAdjustmentPriv.setRefundable(lccChargeAdjustmentTypePrivilege.isRefundAllowed());
				chargeAdjustmentPriv.setNonRefundable(lccChargeAdjustmentTypePrivilege.isNonRefundAllowed());
				chargeAdjustmentPrivileges.add(chargeAdjustmentPriv);
			}
		}
		return chargeAdjustmentPrivileges;
	}

	/**
	 * Transforms LCC results into ThinAir objects. In this method LCC refundable charge details are converted into
	 * ThinAir DTOs
	 * 
	 * @param lccRefundableCharges
	 *            LCC Objects holding data
	 * @return ThinAit objects holding the same data. Formatted into a Map<String,
	 *         Collection<RefundableChargeDetailDTO>> with Key -> pnrPaxID, Value -> collection of
	 *         RefundableChargeDetailDTO containing the refundable charge amount data
	 */
	private static Map<String, Collection<RefundableChargeDetailDTO>> transformRefundableChargeDetails(
			List<StringRefundableDetailsMap> lccRefundableCharges) {

		Map<String, Collection<RefundableChargeDetailDTO>> results = new HashMap<String, Collection<RefundableChargeDetailDTO>>();

		for (StringRefundableDetailsMap resultEntry : lccRefundableCharges) {

			List<RefundableChargeDetailDTO> refundableCharges = new ArrayList<RefundableChargeDetailDTO>();

			for (RefundableChargeDetail refundableChargeDetail : resultEntry.getValue()) {
				RefundableChargeDetailDTO refundableChargeDTO = new RefundableChargeDetailDTO();
				refundableChargeDTO.setChargeType(refundableChargeDetail.getChargeType());
				refundableChargeDTO.setPnrPaxOndChgIDs(refundableChargeDetail.getPnrPaxOndChgIDs());
				refundableChargeDTO.setPnrSegID(refundableChargeDetail.getPnrSegID());
				refundableChargeDTO.setPpfIDs(refundableChargeDetail.getPpfIDs());

				Map<String, BigDecimal> chargeAmountMap = new HashMap<String, BigDecimal>();
				for (StringDecimalMap stringDecimalEntry : refundableChargeDetail.getChargeAmountMap()) {

					chargeAmountMap.put(stringDecimalEntry.getKey(), stringDecimalEntry.getValue());
				}
				refundableChargeDTO.setChargeAmountMap(chargeAmountMap);
				refundableCharges.add(refundableChargeDTO);
			}
			results.put(resultEntry.getKey(), refundableCharges);
		}

		return results;
	}

	private static AutoCancellationInfo transformAutoCancellationInfo(LCCAutoCancellationInfo lccAutoCancellationInfo) {
		if (lccAutoCancellationInfo != null) {
			AutoCancellationInfo autoCancellationInfo = new AutoCancellationInfo();
			autoCancellationInfo.setAutoCancellationId(lccAutoCancellationInfo.getCancellationId());
			autoCancellationInfo.setCancellationType(lccAutoCancellationInfo.getCancellationType());
			autoCancellationInfo.setExpireOn(lccAutoCancellationInfo.getExpireOn());
			return autoCancellationInfo;
		}
		return null;
	}

	private static Set<StationContactDTO> transformStationContactDTOs(List<LCCStationContactDTO> lccStationContactDTOList) {
		Set<StationContactDTO> stationContactDTOSet = null;
		if (lccStationContactDTOList != null && lccStationContactDTOList.size() > 0) {
			stationContactDTOSet = new HashSet<StationContactDTO>();
			for (LCCStationContactDTO lccStationContactDTO : lccStationContactDTOList) {
				StationContactDTO stationContactDTO = new StationContactDTO();
				stationContactDTO.setStationCode(lccStationContactDTO.getStationCode());
				stationContactDTO.setStationName(lccStationContactDTO.getStationName());
				stationContactDTO.setStationContact(lccStationContactDTO.getStationContact());
				stationContactDTO.setStationTelephone(lccStationContactDTO.getStationTelephone());
				stationContactDTOSet.add(stationContactDTO);
			}
		}
		return stationContactDTOSet;
	}

	private static LCCPromotionInfoTO transformLCCPromoInfo(LCCPromotionInfo lccPromotionInfo) {
		LCCPromotionInfoTO lccPromotionInfoTO = null;
		if (lccPromotionInfo != null) {
			lccPromotionInfoTO = new LCCPromotionInfoTO();
			lccPromotionInfoTO.setPromoCodeId(lccPromotionInfo.getPromoCodeId());
			lccPromotionInfoTO.setPromoType(lccPromotionInfo.getPromoType());
			lccPromotionInfoTO.setDiscountType(lccPromotionInfo.getDiscountType());
			lccPromotionInfoTO.setDiscountApplyTo(lccPromotionInfo.getDiscountApplyTo());
			lccPromotionInfoTO.setDiscountValue(lccPromotionInfo.getDiscountValue());
			lccPromotionInfoTO.setDiscountAs(lccPromotionInfo.getDiscountAs());
			lccPromotionInfoTO.setModificationAllowed(lccPromotionInfo.isModificationAllowed());
			lccPromotionInfoTO.setCancellationAllowed(lccPromotionInfo.isCancellationAllowed());
			lccPromotionInfoTO.setSplitAllowed(lccPromotionInfo.isSplitAllowed());
			lccPromotionInfo.setRemovePaxAllowed(lccPromotionInfo.isRemovePaxAllowed());
			lccPromotionInfoTO.setAppliedAdults(lccPromotionInfo.getAppliedAdults());
			lccPromotionInfoTO.setAppliedChilds(lccPromotionInfo.getAppliedChilds());
			lccPromotionInfoTO.setAppliedInfants(lccPromotionInfo.getAppliedInfants());
			lccPromotionInfoTO.setAppliedAncis(new HashSet<String>(lccPromotionInfo.getAppliedAncis()));
			lccPromotionInfoTO.setApplicableBINs(new HashSet<Integer>(lccPromotionInfo.getApplicableBINs()));
			lccPromotionInfoTO.setSystemGenerated(lccPromotionInfo.isSystemGenerated());
			lccPromotionInfoTO.setCreditDiscountAmount(lccPromotionInfo.getCreditDiscountAmount());
		}
		return lccPromotionInfoTO;
	}

	public static ReservationBalanceTO transformBalances(LCCAirBookRequoteRS lccAirBookRequoteRS) throws ModuleException {
		if (lccAirBookRequoteRS.getResponseAttributes().getSuccess() == null) {
			LCCError lccError = BeanUtils.getFirstElement(lccAirBookRequoteRS.getResponseAttributes().getErrors());
			throw new ModuleException(lccError.getErrorCode().value());
		}

		RequoteReservationBalances requoteReservationBalances = lccAirBookRequoteRS.getRequoteReservationBalances();
		RequoteBalances requoteBalances = requoteReservationBalances.getRequoteBalances();

		ReservationBalanceTO lccClientReservationBalanceTO = new ReservationBalanceTO();

		lccClientReservationBalanceTO.setInfantCharge(requoteReservationBalances.getInfantCharge());
		lccClientReservationBalanceTO.setSegmentSummary(transform(requoteBalances));
		lccClientReservationBalanceTO.setShowCnxCharge(requoteReservationBalances.isApplyCnxCharge());
		lccClientReservationBalanceTO.setShowModCharge(requoteReservationBalances.isApplyModCharge());
		lccClientReservationBalanceTO.setTotalAmountDue(requoteBalances.getTotalAmountDue());
		lccClientReservationBalanceTO.setTotalCreditAmount(requoteBalances.getTotalCreditAmount());
		lccClientReservationBalanceTO.setTotalCnxCharge(requoteBalances.getTotalCnxCharge());
		lccClientReservationBalanceTO.setTotalModCharge(requoteBalances.getTotalModCharge());
		lccClientReservationBalanceTO.setTotalPrice(requoteBalances.getTotalPrice());
		Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap = null;
		if (requoteBalances.getPaxWiseAdjustmentAmount() != null && requoteBalances.getPaxWiseAdjustmentAmount().size() > 0) {
			paxWiseAdjustmentAmountMap = new HashMap<Integer, BigDecimal>();
			for (IntegerDecimalMap adjAmountMap : requoteBalances.getPaxWiseAdjustmentAmount()) {
				paxWiseAdjustmentAmountMap.put(adjAmountMap.getKey(), adjAmountMap.getValue());
			}
		}
		lccClientReservationBalanceTO.setPaxWiseAdjustmentAmountMap(paxWiseAdjustmentAmountMap);
		if (requoteBalances.getCustomerPaymentDetails() != null && !requoteBalances.getCustomerPaymentDetails().isEmpty()) {
			lccClientReservationBalanceTO.setTotalPaidAmount(getTotalPaidAmount(requoteBalances.getCustomerPaymentDetails()));
		} else {
			boolean isPaymentDetailsFilled = false;
			if (requoteReservationBalances.getPaxRequoteReservationBalances() != null
					&& !requoteReservationBalances.getPaxRequoteReservationBalances().isEmpty()) {
				for (PaxRequoteReservationBalances paxRequoteReservationBalance : requoteReservationBalances
						.getPaxRequoteReservationBalances()) {
					if (paxRequoteReservationBalance.getRequoteBalances().getPaymentDetails() != null
							&& paxRequoteReservationBalance.getRequoteBalances().getPaymentDetails().size() > 0) {
						isPaymentDetailsFilled = true;
						break;
					}
				}
			}
			if (isPaymentDetailsFilled) {
				lccClientReservationBalanceTO.setTotalPaidAmount(calculateRequoteTotalPaidAmount(requoteReservationBalances
						.getPaxRequoteReservationBalances()));
			} else {
				if (requoteBalances.getTotalPaidAmount() != null) {
					lccClientReservationBalanceTO.setTotalPaidAmount(requoteBalances.getTotalPaidAmount());
				} else {
					BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					lccClientReservationBalanceTO.setTotalPaidAmount(AccelAeroCalculator.scaleValueDefault(totalPaidAmount));
				}

			}

		}

		Collection<LCCClientPassengerSummaryTO> colLCCClientPassengerSummaryTO = new ArrayList<LCCClientPassengerSummaryTO>();
		lccClientReservationBalanceTO.setPassengerSummaryList(colLCCClientPassengerSummaryTO);

		for (PaxRequoteReservationBalances paxRequoteReservationBalances : requoteReservationBalances
				.getPaxRequoteReservationBalances()) {
			LCCClientPassengerSummaryTO passengerSummaryTO = new LCCClientPassengerSummaryTO();

			passengerSummaryTO.setTravelerRefNumber(paxRequoteReservationBalances.getPaxRefNumber());
			passengerSummaryTO.setPaxName(paxRequoteReservationBalances.getPaxName());
			passengerSummaryTO.setInfantName(paxRequoteReservationBalances.getPaxInfantName());
			passengerSummaryTO.setPaxType(LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(paxRequoteReservationBalances
					.getPassengerType()));
			passengerSummaryTO.setAccompaniedTravellerRef(paxRequoteReservationBalances.getInfantRefNumber());

			passengerSummaryTO.setSegmentSummaryTO(transform(paxRequoteReservationBalances.getRequoteBalances()));

			passengerSummaryTO.setTotalAmountDue(paxRequoteReservationBalances.getRequoteBalances().getTotalAmountDue());
			passengerSummaryTO.setTotalCreditAmount(paxRequoteReservationBalances.getRequoteBalances().getTotalCreditAmount());
			passengerSummaryTO.setTotalCnxCharge(paxRequoteReservationBalances.getRequoteBalances().getTotalCnxCharge());
			passengerSummaryTO.setTotalModCharge(paxRequoteReservationBalances.getRequoteBalances().getTotalModCharge());
			passengerSummaryTO.setTotalPrice(paxRequoteReservationBalances.getRequoteBalances().getTotalPrice());
			if (paxRequoteReservationBalances.getRequoteBalances().getCustomerPaymentDetails() != null
					&& !paxRequoteReservationBalances.getRequoteBalances().getCustomerPaymentDetails().isEmpty()) {
				passengerSummaryTO.setTotalPaidAmount(getTotalPaidAmount(paxRequoteReservationBalances.getRequoteBalances()
						.getCustomerPaymentDetails()));
			} else if (paxRequoteReservationBalances.getRequoteBalances().getPaymentDetails() != null
					&& paxRequoteReservationBalances.getRequoteBalances().getPaymentDetails().size() > 0) {
				passengerSummaryTO.setTotalPaidAmount(getTotalPaidAmount(paxRequoteReservationBalances.getRequoteBalances()
						.getPaymentDetails()));
			} else {
				if (paxRequoteReservationBalances.getRequoteBalances().getTotalPaidAmount() != null) {
					passengerSummaryTO
							.setTotalPaidAmount(paxRequoteReservationBalances.getRequoteBalances().getTotalPaidAmount());
				} else {
					BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					passengerSummaryTO.setTotalPaidAmount(AccelAeroCalculator.scaleValueDefault(totalPaidAmount));
				}
			}

			if (paxRequoteReservationBalances.getRequoteBalances().getCarrierProductCodeWiseAmountDue() != null
					&& paxRequoteReservationBalances.getRequoteBalances().getCarrierProductCodeWiseAmountDue().size() > 0) {
				Map<String, Map<String, BigDecimal>> carrierProductCodeWiseAmountDueMap = new HashMap<String, Map<String, BigDecimal>>();
				for (CarrierProductCodeWiseAmountDue carrierProductCodeWiseAmountDue : paxRequoteReservationBalances
						.getRequoteBalances().getCarrierProductCodeWiseAmountDue()) {
					String carrierCode = carrierProductCodeWiseAmountDue.getKey();
					Map<String, BigDecimal> productCodeWiseAmountDue = transformStringDecimalMap(carrierProductCodeWiseAmountDue
							.getValue());
					carrierProductCodeWiseAmountDueMap.put(carrierCode, productCodeWiseAmountDue);
				}
				passengerSummaryTO.setCarrierProductCodeWiseAmountDue(carrierProductCodeWiseAmountDueMap);
			}

			colLCCClientPassengerSummaryTO.add(passengerSummaryTO);
		}
		lccClientReservationBalanceTO.setVersion(lccAirBookRequoteRS.getVersion());
		lccClientReservationBalanceTO.setPaxEffectiveTax(transforPaxEffectiveTax(requoteBalances.getPaxEffectiveTaxes()));
		return lccClientReservationBalanceTO;
	}

	private static Map<Integer, List<ExternalChgDTO>> transforPaxEffectiveTax(List<IntegerExtChgMap> lccPaxEffectiveTaxes) {
		Map<Integer, List<ExternalChgDTO>> aaPaxEffectiveTax = new HashMap<Integer, List<ExternalChgDTO>>();
		for (IntegerExtChgMap lccObj : lccPaxEffectiveTaxes) {
			List<ClientExternalChg> lccExtCharges = lccObj.getValue();

			if (!aaPaxEffectiveTax.containsKey(lccObj.getKey())) {
				aaPaxEffectiveTax.put(lccObj.getKey(), new ArrayList<ExternalChgDTO>());
			}

			List<ExternalChgDTO> extChargeTOs = aaPaxEffectiveTax.get(lccObj.getKey());

			for (ClientExternalChg clientExternalChg : lccExtCharges) {
				ServiceTaxExtChgDTO externalChgDTO = new ServiceTaxExtChgDTO();
				externalChgDTO.setAmount(clientExternalChg.getAmount());
				externalChgDTO.setChargeCode(clientExternalChg.getCode());
				externalChgDTO.setExternalChargesEnum(getExternalChargeEnum(clientExternalChg.getExternalCharges()));
				externalChgDTO.setFlightRefNumber(clientExternalChg.getFlightRefNumber());

				extChargeTOs.add(externalChgDTO);
			}

		}
		return aaPaxEffectiveTax;
	}

	private static BigDecimal calculateRequoteTotalPaidAmount(List<PaxRequoteReservationBalances> paxRequoteReservationBalances) {
		BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paxRequoteReservationBalances != null && !paxRequoteReservationBalances.isEmpty()) {
			for (PaxRequoteReservationBalances paxRequoteReservationBalance : paxRequoteReservationBalances) {
				totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, getTotalPaidAmount(paxRequoteReservationBalance
						.getRequoteBalances().getPaymentDetails()));
			}
		}
		return AccelAeroCalculator.scaleValueDefault(totalPaidAmount).negate();
	}

	private static LCCClientSegmentSummaryTO transform(RequoteBalances requoteBalances) {

		LCCClientSegmentSummaryTO lccClientSegmentSummaryTO = new LCCClientSegmentSummaryTO();

		BaseRequoteBalances existingOndBalances = requoteBalances.getExistingOndBalances();
		BaseRequoteBalances requoteOndBalances = requoteBalances.getRequoteOndBalances();

		lccClientSegmentSummaryTO.setCurrentFareAmount(existingOndBalances.getFareAmount());
		lccClientSegmentSummaryTO.setCurrentTaxAmount(existingOndBalances.getTaxAmount());
		lccClientSegmentSummaryTO.setCurrentSurchargeAmount(existingOndBalances.getSurAmount());
		lccClientSegmentSummaryTO.setCurrentAdjAmount(existingOndBalances.getAdjAmount());
		lccClientSegmentSummaryTO.setCurrentModAmount(existingOndBalances.getModAmount());
		lccClientSegmentSummaryTO.setCurrentCnxAmount(existingOndBalances.getCnxAmount());
		lccClientSegmentSummaryTO.setCurrentDiscount(existingOndBalances.getDiscountAmount());
		lccClientSegmentSummaryTO.setCurrentSeatAmount(existingOndBalances.getSeatAmount());
		lccClientSegmentSummaryTO.setCurrentMealAmount(existingOndBalances.getMealAmount());
		lccClientSegmentSummaryTO.setCurrentBaggageAmount(existingOndBalances.getBaggageAmount());
		lccClientSegmentSummaryTO.setCurrentInsuranceAmount(existingOndBalances.getInsuranceAmount());
		lccClientSegmentSummaryTO.setCurrentSSRAmount(existingOndBalances.getSsrAmount());
		lccClientSegmentSummaryTO.setCurrentModificationPenatly(existingOndBalances.getPenaltyAmount());
		lccClientSegmentSummaryTO.setCurrentTotalPrice(existingOndBalances.getTotalPrice());

		lccClientSegmentSummaryTO.setCurrentRefunds(requoteBalances.getTotalRefundableAmount());
		lccClientSegmentSummaryTO.setCurrentNonRefunds(requoteBalances.getTotalNonRefundableAmount());

		lccClientSegmentSummaryTO.setNewFareAmount(requoteOndBalances.getFareAmount());
		lccClientSegmentSummaryTO.setNewTaxAmount(requoteOndBalances.getTaxAmount());
		lccClientSegmentSummaryTO.setNewSurchargeAmount(requoteOndBalances.getSurAmount());
		lccClientSegmentSummaryTO.setNewAdjAmount(requoteOndBalances.getAdjAmount());
		lccClientSegmentSummaryTO.setNewModAmount(requoteOndBalances.getModAmount());
		lccClientSegmentSummaryTO.setNewCnxAmount(requoteOndBalances.getCnxAmount());
		lccClientSegmentSummaryTO.setNewDiscount(requoteOndBalances.getDiscountAmount());
		lccClientSegmentSummaryTO.setNewSeatAmount(requoteOndBalances.getSeatAmount());
		lccClientSegmentSummaryTO.setNewMealAmount(requoteOndBalances.getMealAmount());
		lccClientSegmentSummaryTO.setNewBaggageAmount(requoteOndBalances.getBaggageAmount());
		lccClientSegmentSummaryTO.setNewInsuranceAmount(requoteOndBalances.getInsuranceAmount());
		lccClientSegmentSummaryTO.setNewSSRAmount(requoteOndBalances.getSsrAmount());
		lccClientSegmentSummaryTO.setNewExtraFeeAmount(requoteOndBalances.getExtraFeeAmount());
		lccClientSegmentSummaryTO.setModificationPenalty(requoteOndBalances.getPenaltyAmount());
		lccClientSegmentSummaryTO.setNewTotalPrice(requoteOndBalances.getTotalPrice());

		return lccClientSegmentSummaryTO;
	}

	private static boolean isHideExchangeCharge(Integer pnrSegmentID, Map<Integer, String> pnrSegmentSubStatus) {

		String subStatus = "";
		if (pnrSegmentID != null && pnrSegmentSubStatus.containsKey(pnrSegmentID)) {
			subStatus = pnrSegmentSubStatus.get(pnrSegmentID);
			if (AppSysParamsUtil.hideExchangesCharges()
					&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equalsIgnoreCase(subStatus)) {
				return true;
			}
		}

		return false;
	}

	private static BigDecimal getDiscountAmount(List<Fee> feeList) {
		BigDecimal promotionDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (Fee fee : feeList) {
			if (fee.getFeeCode().equals(ChargeGroupCode.DIS.value())) {
				promotionDiscount = AccelAeroCalculator.add(promotionDiscount, fee.getAmount());
			}
		}
		return promotionDiscount;
	}

	public static PromotionSelectionCriteria transformFromPromoSelectionCriteria(PromoSelectionCriteria promoSelCri) {
		PromotionSelectionCriteria lccPromoSelectionCriteria = null;
		if (promoSelCri != null) {
			lccPromoSelectionCriteria = new PromotionSelectionCriteria();
			lccPromoSelectionCriteria.setPromoCode(promoSelCri.getPromoCode());
			lccPromoSelectionCriteria.setPromoType(promoSelCri.getPromoType());
			lccPromoSelectionCriteria.getOndList().addAll(promoSelCri.getOndList());
			lccPromoSelectionCriteria.getFlights().addAll(promoSelCri.getFlights());
			lccPromoSelectionCriteria.getFlightIds().addAll(promoSelCri.getFlightIds());
			lccPromoSelectionCriteria.getFlightSegIds().addAll(promoSelCri.getFlightSegIds());
			lccPromoSelectionCriteria.getFlightSegWiseLogicalCabinClass().addAll(
					LCCUtils.composeIntegerStringMap(promoSelCri.getFlightSegWiseLogicalCabinClass()));
			lccPromoSelectionCriteria.setSalesChannel(promoSelCri.getSalesChannel());
			lccPromoSelectionCriteria.setReservationDate(promoSelCri.getReservationDate());
			lccPromoSelectionCriteria.getOndFlightDates().addAll(populateIntegerDateMap(promoSelCri.getOndFlightDates()));
			lccPromoSelectionCriteria.setAgent(promoSelCri.getAgent());
			lccPromoSelectionCriteria.getCabinClasses().addAll(promoSelCri.getCabinClasses());
			lccPromoSelectionCriteria.getLogicalCabinClasses().addAll(promoSelCri.getLogicalCabinClasses());
			lccPromoSelectionCriteria.getBookingClasses().addAll(promoSelCri.getBookingClasses());
			lccPromoSelectionCriteria.setJourneyType(promoSelCri.getJourneyType().name());
			lccPromoSelectionCriteria.setPreferedLanguage(promoSelCri.getPreferredLanguage());
			lccPromoSelectionCriteria.setAdultCount(promoSelCri.getAdultCount());
			lccPromoSelectionCriteria.setChildCount(promoSelCri.getChildCount());
			lccPromoSelectionCriteria.setInfantCount(promoSelCri.getInfantCount());
			lccPromoSelectionCriteria.setBankIdNo(promoSelCri.getBankIdNo());
		}
		return lccPromoSelectionCriteria;
	}

	public static List<StringStringMap> convertToLccStringMap(Map<String, String> aaStringMap) {
		List<StringStringMap> lccStringMap = new ArrayList<StringStringMap>();
		if (aaStringMap != null && !aaStringMap.isEmpty()) {
			for (Entry<String, String> aaEntry : aaStringMap.entrySet()) {
				StringStringMap lccMapObj = new StringStringMap();
				lccMapObj.setKey(aaEntry.getKey());
				lccMapObj.setValue(aaEntry.getValue());
				lccStringMap.add(lccMapObj);
			}
		}
		return lccStringMap;
	}

	public static EXTERNAL_CHARGES getExternalChargeEnum(String externalChgStr) {
		EXTERNAL_CHARGES extChg = null;
		if (EXTERNAL_CHARGES.CREDIT_CARD.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.CREDIT_CARD;
		} else if (EXTERNAL_CHARGES.FLEXI_CHARGES.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.FLEXI_CHARGES;
		} else if (EXTERNAL_CHARGES.AIRPORT_SERVICE.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.AIRPORT_SERVICE;
		} else if (EXTERNAL_CHARGES.HANDLING_CHARGE.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.HANDLING_CHARGE;
		} else if (EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.INFLIGHT_SERVICES;
		} else if (EXTERNAL_CHARGES.INSURANCE.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.INSURANCE;
		} else if (EXTERNAL_CHARGES.MEAL.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.MEAL;
		} else if (EXTERNAL_CHARGES.BAGGAGE.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.BAGGAGE;
		} else if (EXTERNAL_CHARGES.NO_SHORE.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.NO_SHORE;
		} else if (EXTERNAL_CHARGES.NON_REF_ADJ.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.NON_REF_ADJ;
		} else if (EXTERNAL_CHARGES.REF_ADJ.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.REF_ADJ;
		} else if (EXTERNAL_CHARGES.REFUND.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.REFUND;
		} else if (EXTERNAL_CHARGES.SEAT_MAP.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.SEAT_MAP;
		} else if (EXTERNAL_CHARGES.PROMOTION_REWARD.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.PROMOTION_REWARD;
		} else if (EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE;
		} else if (EXTERNAL_CHARGES.JN_ANCI.toString().equals(externalChgStr)) {
			extChg = EXTERNAL_CHARGES.JN_ANCI;
		}

		return extChg;
	}

	private static List<IntegerDateMap> populateIntegerDateMap(Map<Integer, Date> ondFlightDates) {
		List<IntegerDateMap> lccOndFlights = new ArrayList<IntegerDateMap>();

		if (ondFlightDates != null && !ondFlightDates.isEmpty()) {
			for (Entry<Integer, Date> flightEntry : ondFlightDates.entrySet()) {
				IntegerDateMap lccOndFlightObj = new IntegerDateMap();
				lccOndFlightObj.setKey(flightEntry.getKey());
				lccOndFlightObj.setValue(flightEntry.getValue());
				lccOndFlights.add(lccOndFlightObj);
			}
		}

		return lccOndFlights;
	}

	public static List<BundledFareDTO> transformToBundledFare(List<LCCBundledFare> lccBundledFares) {

		List<BundledFareDTO> bunldedFareList = new ArrayList<BundledFareDTO>();
		if (lccBundledFares != null && !lccBundledFares.isEmpty()) {
			for (LCCBundledFare lccBundledFare : lccBundledFares) {
				if (lccBundledFare != null && lccBundledFare.getBundledFarePeriodId() != null) {
					BundledFareDTO bundledFareDTO = new BundledFareDTO();
					bundledFareDTO.setBundledFarePeriodId(lccBundledFare.getBundledFarePeriodId());
					bundledFareDTO.setBundledFareName(lccBundledFare.getBundledFareName());
					bundledFareDTO.setPerPaxBundledFee(lccBundledFare.getPerPaxBundledFee());
					bundledFareDTO.setBookingClasses(new HashSet<String>(lccBundledFare.getBookingClasses()));

					Set<BundledFareFreeServiceTO> bundledFreeServices = new HashSet<BundledFareFreeServiceTO>();
					List<LCCBundledFareFreeService> lccBundledFreeServices = lccBundledFare.getFreeServices();

					for (LCCBundledFareFreeService lccBundledFreeService : lccBundledFreeServices) {
						BundledFareFreeServiceTO freeServiceTo = new BundledFareFreeServiceTO();
						freeServiceTo.setId(lccBundledFreeService.getFreeServiceId());
						freeServiceTo.setServiceName(lccBundledFreeService.getServiceName());
						freeServiceTo.setTemplateId(lccBundledFreeService.getTemplateId());

						bundledFreeServices.add(freeServiceTo);
					}
					bundledFareDTO.setApplicableServices(bundledFreeServices);
					bunldedFareList.add(bundledFareDTO);
				} else {
					bunldedFareList.add(null);
				}

			}
		}
		return bunldedFareList;
	}
	
	
	private static List<DiscountChargeTO> paxDiscountChargesList(List<DiscountCharge> lccPaxDiscountCharges) {
		List<DiscountChargeTO> paxDiscountCharges = null;
		if (lccPaxDiscountCharges != null && !lccPaxDiscountCharges.isEmpty()) {
			paxDiscountCharges = new ArrayList<DiscountChargeTO>();
			for (DiscountCharge lccDiscountCharge : lccPaxDiscountCharges) {
				DiscountChargeTO discountChargeTO = new DiscountChargeTO();
				discountChargeTO.setChargeCode(lccDiscountCharge.getChargeCode());
				discountChargeTO.setChargeGroupCode(lccDiscountCharge.getChargeGroupCode());
				discountChargeTO.setDiscountAmount(lccDiscountCharge.getDiscountAmount());
				discountChargeTO.setRateId(lccDiscountCharge.getRateId());
				discountChargeTO.setSegmentCode(lccDiscountCharge.getSegmentCode());
				discountChargeTO.setFlightSegmentIds(lccDiscountCharge.getFlightSegIds());				
				discountChargeTO.setExternalChargeCode(lccDiscountCharge.getExternalChargeCode());
				paxDiscountCharges.add(discountChargeTO);

			}
		}
		return paxDiscountCharges;
	}
	
	public static ReservationDiscountDTO transformReservationDiscount(LCCDiscountRS lccDiscountRS) {
		ReservationDiscountDTO reservationDiscountDTO = null;
		if (lccDiscountRS != null) {
			reservationDiscountDTO = new ReservationDiscountDTO();
			reservationDiscountDTO.setDiscountCode(lccDiscountRS.getDiscountCode());

			if (lccDiscountRS.getDiscountPercentage() != null)
				reservationDiscountDTO.setDiscountPercentage(lccDiscountRS.getDiscountPercentage().doubleValue());

			if (lccDiscountRS.getPromoPaxCount() != null)
				reservationDiscountDTO.setPromoPaxCount(lccDiscountRS.getPromoPaxCount());

			reservationDiscountDTO.setPromotionId(lccDiscountRS.getPromotionId());
			reservationDiscountDTO.setTotalDiscount(lccDiscountRS.getTotalDiscount());

			Map<Integer, PaxDiscountDetailTO> paxDiscountDetails = null;
			if (lccDiscountRS.getPaxDiscountDetails() != null && !lccDiscountRS.getPaxDiscountDetails().isEmpty()) {
				paxDiscountDetails = new HashMap<Integer, PaxDiscountDetailTO>();
				BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (PaxDiscountDetails lccPaxDiscountDetails : lccDiscountRS.getPaxDiscountDetails()) {
					PaxDiscountDetailTO paxDiscountDetailTO = new PaxDiscountDetailTO();
					paxDiscountDetailTO.setPaxType(lccPaxDiscountDetails.getPaxType());
					paxDiscountDetailTO.setPaxSequence(lccPaxDiscountDetails.getPaxSequence());

					List<DiscountChargeTO> paxDiscountCharges = paxDiscountChargesList(lccPaxDiscountDetails
							.getPaxDiscountCharges());

					if (paxDiscountCharges != null && !paxDiscountCharges.isEmpty()) {
						paxDiscountDetailTO.getPaxDiscountChargeTOs().addAll(paxDiscountCharges);
						totalDiscount = AccelAeroCalculator.add(totalDiscount, getDiscountChargesTotal(paxDiscountCharges));
					}

					List<DiscountChargeTO> infantDiscountCharges = paxDiscountChargesList(lccPaxDiscountDetails
							.getInfantDiscountCharges());

					if (infantDiscountCharges != null && !infantDiscountCharges.isEmpty()) {
						paxDiscountDetailTO.getInfantDiscountChargeTOs().addAll(infantDiscountCharges);
						totalDiscount = AccelAeroCalculator.add(totalDiscount, getDiscountChargesTotal(infantDiscountCharges));
					}

					paxDiscountDetails.put(lccPaxDiscountDetails.getPaxSequence(), paxDiscountDetailTO);

				}
				if (totalDiscount.doubleValue() > 0) {
					reservationDiscountDTO.setTotalDiscount(totalDiscount);
				}
				reservationDiscountDTO.setPaxDiscountDetails(paxDiscountDetails);
			}

		}

		return reservationDiscountDTO;
	}

	private static BigDecimal getDiscountChargesTotal(List<DiscountChargeTO> discountCharges) {
		BigDecimal sumOfDiscountCharges = new BigDecimal(0);
		if (discountCharges != null && !discountCharges.isEmpty()) {
			for (DiscountChargeTO discountChargeTO : discountCharges) {
				if (discountChargeTO != null) {
					BigDecimal chargeDiscount = discountChargeTO.getDiscountAmount();
					if (chargeDiscount != null) {
						sumOfDiscountCharges = AccelAeroCalculator.add(sumOfDiscountCharges, chargeDiscount);
					}
				}

			}
		}
		return sumOfDiscountCharges;
	}

	public static Map<String, BigDecimal> transformStringDecimalMap(List<StringDecimalMap> stringDecimalList) {
		Map<String, BigDecimal> stringDecimalMap = new HashMap<String, BigDecimal>();
		for (StringDecimalMap stringDecimalObj : stringDecimalList) {
			stringDecimalMap.put(stringDecimalObj.getKey(), stringDecimalObj.getValue());
		}
		return stringDecimalMap;
	}

	public static Map<String, Double> transformStringDoubleMap(List<StringDoubleMap> stringDoubleList) {
		Map<String, Double> stringDoubleMap = new HashMap<String, Double>();
		for (StringDoubleMap stringDoubleObj : stringDoubleList) {
			stringDoubleMap.put(stringDoubleObj.getKey(), stringDoubleObj.getValue());
		}
		return stringDoubleMap;
	}

	public static List<StringDecimalMap> populateStringDecimalMap(Map<String, BigDecimal> stringDecimalMap) {
		List<StringDecimalMap> stringDecimalMaps = new ArrayList<StringDecimalMap>();

		if (stringDecimalMap != null && !stringDecimalMap.isEmpty()) {
			for (Entry<String, BigDecimal> stringDecimal : stringDecimalMap.entrySet()) {
				StringDecimalMap stringDecimalObj = new StringDecimalMap();
				stringDecimalObj.setKey(stringDecimal.getKey());
				stringDecimalObj.setValue(stringDecimal.getValue());
				stringDecimalMaps.add(stringDecimalObj);
			}
		}

		return stringDecimalMaps;
	}
	
	private static BigDecimal getRefundableCreditAmount(List<CreditDetails> creditDetailsList, BigDecimal totalBalanceDue) {
		BigDecimal refundableCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (creditDetailsList != null && !creditDetailsList.isEmpty()) {
			for (CreditDetails creditDetails : creditDetailsList) {
				if (!creditDetails.isNonRefundableCredit()
						&& CreditInfoDTO.status.AVAILABLE.equals(LCCClientCommonUtils
								.convertLCCCreditStatusToAACreditStatus(creditDetails.getStatus()))) {
					refundableCreditAmount = AccelAeroCalculator.add(refundableCreditAmount, creditDetails.getMcAmount());
				}
			}
		}

		return AccelAeroCalculator.subtract(refundableCreditAmount, totalBalanceDue);
	}

	private static BigDecimal getPaxTotalAmountDue(Collection<Balance> colBalance) {
		BigDecimal totalBalanceDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (colBalance != null && !colBalance.isEmpty()) {
			for (Balance balance : colBalance) {
				if (balance.getAmount().doubleValue() > 0) {
					totalBalanceDue = AccelAeroCalculator.add(totalBalanceDue, balance.getAmount());
				}
			}
		}

		return totalBalanceDue;
	}

	public static Map<FarePriceOnd, BigDecimal> transformStringDecimalToFarePriceOndMap(List<StringDecimalMap> stringDecimalList) {
		Map<FarePriceOnd, BigDecimal> farePriceOndDecimalMap = new HashMap<FarePriceOnd, BigDecimal>();
		if (stringDecimalList != null && !stringDecimalList.isEmpty()) {
			for (StringDecimalMap stringDecimalObj : stringDecimalList) {
				farePriceOndDecimalMap.put(FarePriceOnd.valueOf(stringDecimalObj.getKey()), stringDecimalObj.getValue());
			}
		}
		return farePriceOndDecimalMap;
	}

		public static LCCUserNoteRQ transform(UserNoteTO userNoteTO, TrackInfoDTO trackInfo) {
		LCCUserNoteRQ lccUserNoteRQ = new LCCUserNoteRQ();
		lccUserNoteRQ.setTrackInfo(transformTrackingInfo(trackInfo));
		lccUserNoteRQ.setUserNote(transform(userNoteTO));
		return lccUserNoteRQ;
	}

	private static UserNote transform(UserNoteTO userNoteTO) {

		UserNote userNote = new UserNote();
		userNote.setNoteText(userNoteTO.getUserNotes().get(0));
		userNote.setUserNoteType(userNoteTO.getUserNoteType());
		userNote.setPnr(userNoteTO.getPnr());
		userNote.setAction(userNoteTO.getAction());
		userNote.setSystemText(userNoteTO.getSystemNote());
		userNote.setUsername(userNoteTO.getUserName());
		return userNote;
	}
	
	public static List<ChargeReverse> transformChargeReverseList(List<LCCClientChargeReverse> lccClientChargeReverseList) {

		List<ChargeReverse> chargeReverseList = new ArrayList<ChargeReverse>();

		if (lccClientChargeReverseList != null) {
			for (LCCClientChargeReverse lccClientChargeReverse : lccClientChargeReverseList) {

				ChargeReverse chargeReverse = new ChargeReverse();

				chargeReverse.setCarrierCode(lccClientChargeReverse.getCarrierCode());
				chargeReverse.setCarrierOndGroupRPH(lccClientChargeReverse.getCarrierOndGroupRPH());
				chargeReverse.setSegmentCode(lccClientChargeReverse.getSegmentCode());
				chargeReverse.setTravelerRefNumber(lccClientChargeReverse.getTravelerRefNumber());
				chargeReverse.getTravelerRefNumberList().addAll(lccClientChargeReverse.getTravelerRefNumbersList());
				chargeReverse.setUserNote(lccClientChargeReverse.getUserNote());

				chargeReverseList.add(chargeReverse);
			}
		}
		return chargeReverseList;
	}
	
	private static void populateNoShowRefundableInfo(LCCClientReservation lccClientReservation, LCCAirBookRS lccAirBookRS) {
		for (LCCClientReservationPax lccClientReservationPax : lccClientReservation.getPassengers()) {
			for (NoShowRefundableInfo noShowRefundableInfo : lccAirBookRS.getLccReservation().getAirReservation()
					.getNoShowRefundableInfo()) {
				if (lccClientReservationPax.getTravelerRefNumber().contains(noShowRefundableInfo.getTravellerRefNo())) {
					lccClientReservationPax.setNoShowRefundableAmount(AccelAeroCalculator.add(lccClientReservationPax
							.getNoShowRefundableAmount() == null ? AccelAeroCalculator.getDefaultBigDecimalZero()
							: lccClientReservationPax.getNoShowRefundableAmount(),
							noShowRefundableInfo.getAmount() == null ? AccelAeroCalculator.getDefaultBigDecimalZero()
									: noShowRefundableInfo.getAmount()));
					if(!lccClientReservationPax.isNoShowRefundable() && noShowRefundableInfo.isIsRefundable()){
						lccClientReservationPax.setNoShowRefundable(true);
					}
				}
			}
		}
	}

	private static void populateTaxInvoices(LCCClientReservation lccClientReservation, LCCAirBookRS lccAirBookRS) {
		AirReservation reservation = lccAirBookRS.getLccReservation().getAirReservation();
		if (reservation.getTaxInvoicesList() != null) {
			List<com.isa.thinair.airreservation.api.model.TaxInvoice> taxInvoicesList = new ArrayList<com.isa.thinair.airreservation.api.model.TaxInvoice>();
			if (reservation.getTaxInvoicesList() != null && !reservation.getTaxInvoicesList().isEmpty()) {
				for (int i = 0; i < reservation.getTaxInvoicesList().size(); i++) {
					com.isa.thinair.airreservation.api.model.TaxInvoice taxInvoice = new com.isa.thinair.airreservation.api.model.TaxInvoice();

					if (reservation.getTaxInvoicesList().get(i).getDateOfIssue() != null){
						taxInvoice.setDateOfIssue(reservation.getTaxInvoicesList().get(i).getDateOfIssue());
					} if (reservation.getTaxInvoicesList().get(i).getNonTaxableAmount() != null){
						taxInvoice.setNonTaxableAmount(reservation.getTaxInvoicesList().get(i).getNonTaxableAmount());
					} if (true){
						taxInvoice.setOriginalTaxInvoiceId(reservation.getTaxInvoicesList().get(i).getOriginalTaxInvoiceId());
					} if (reservation.getTaxInvoicesList().get(i).getTaxableAmount() != null){
						taxInvoice.setTaxableAmount(reservation.getTaxInvoicesList().get(i).getTaxableAmount());
					} if (true){
						taxInvoice.setTaxInvoiceId(reservation.getTaxInvoicesList().get(i).getTaxInvoiceId());
					} if (reservation.getTaxInvoicesList().get(i).getTaxRate1Amount() != null){
						taxInvoice.setTaxRate1Amount(reservation.getTaxInvoicesList().get(i).getTaxRate1Amount());
					} if (true){
						taxInvoice.setTaxRate1Id(reservation.getTaxInvoicesList().get(i).getTaxRate1Id());
					} if (reservation.getTaxInvoicesList().get(i).getTaxRate2Amount() != null){
						taxInvoice.setTaxRate2Amount(reservation.getTaxInvoicesList().get(i).getTaxRate2Amount());
					} if (true){
						taxInvoice.setTaxRate2Id(reservation.getTaxInvoicesList().get(i).getTaxRate2Id());
					} if (reservation.getTaxInvoicesList().get(i).getTaxRate3Amount() != null){
						taxInvoice.setTaxRate3Amount(reservation.getTaxInvoicesList().get(i).getTaxRate3Amount());
					} if (true){
						taxInvoice.setTaxRate3Id(reservation.getTaxInvoicesList().get(i).getTaxRate3Id());
					} if (true){
						taxInvoice.setTnxSeq(reservation.getTaxInvoicesList().get(i).getTnxSeq());
					} if (reservation.getTaxInvoicesList().get(i).getTotalDiscount() != null){
						taxInvoice.setTotalDiscount(reservation.getTaxInvoicesList().get(i).getTotalDiscount());
					} if (reservation.getTaxInvoicesList().get(i).getInvoiceType() != null){
						taxInvoice.setInvoiceType(reservation.getTaxInvoicesList().get(i).getInvoiceType());
					} if (reservation.getTaxInvoicesList().get(i).getCurrencyCode() != null){
						taxInvoice.setCurrencyCode(reservation.getTaxInvoicesList().get(i).getCurrencyCode());
					} if (reservation.getTaxInvoicesList().get(i).getStateCode() != null){
						taxInvoice.setStateCode(reservation.getTaxInvoicesList().get(i).getStateCode());
					} if (reservation.getTaxInvoicesList().get(i).getTaxRegistrationNumber() != null){
						taxInvoice.setTaxRegistrationNumber(reservation.getTaxInvoicesList().get(i).getTaxRegistrationNumber());
					} if (reservation.getTaxInvoicesList().get(i).getOriginalPnr() != null){
						taxInvoice.setOriginalPnr(reservation.getTaxInvoicesList().get(i).getOriginalPnr());
					} if (reservation.getTaxInvoicesList().get(i).getNameOfTheAirline() != null){
						taxInvoice.setNameOfTheAirline(reservation.getTaxInvoicesList().get(i).getNameOfTheAirline());
					} if (reservation.getTaxInvoicesList().get(i).getAirlineOfficeSTAddress1() != null){
						taxInvoice.setAirlineOfficeSTAddress1(reservation.getTaxInvoicesList().get(i).getAirlineOfficeSTAddress1());
					} if (reservation.getTaxInvoicesList().get(i).getAirlineOfficeSTAddress2() != null){
						taxInvoice.setAirlineOfficeSTAddress2(reservation.getTaxInvoicesList().get(i).getAirlineOfficeSTAddress2()); 
					} if (reservation.getTaxInvoicesList().get(i).getAirlineOfficeCity() != null){
						taxInvoice.setAirlineOfficeCity(reservation.getTaxInvoicesList().get(i).getAirlineOfficeCity());
					} if (reservation.getTaxInvoicesList().get(i).getAirlineOfficeCountryCode() != null){
						taxInvoice.setAirlineOfficeCountryCode(reservation.getTaxInvoicesList().get(i).getAirlineOfficeCountryCode());
					} if (reservation.getTaxInvoicesList().get(i).getGstinForInvoiceState() != null){
						taxInvoice.setGstinForInvoiceState(reservation.getTaxInvoicesList().get(i).getGstinForInvoiceState());
					} if (reservation.getTaxInvoicesList().get(i).getNameOfTheRecipient() != null){
						taxInvoice.setNameOfTheRecipient(reservation.getTaxInvoicesList().get(i).getNameOfTheRecipient()); 
					} if (reservation.getTaxInvoicesList().get(i).getRecipientStreetAddress1() != null){
						taxInvoice.setRecipientStreetAddress1(reservation.getTaxInvoicesList().get(i).getRecipientStreetAddress1());
					} if (reservation.getTaxInvoicesList().get(i).getRecipientStreetAddress2() != null){
						taxInvoice.setRecipientStreetAddress2(reservation.getTaxInvoicesList().get(i).getRecipientStreetAddress2()); 
					} if (reservation.getTaxInvoicesList().get(i).getRecipientCity() != null){
						taxInvoice.setRecipientCity(reservation.getTaxInvoicesList().get(i).getRecipientCity());
					} if (reservation.getTaxInvoicesList().get(i).getRecipientCountryCode() != null){
						taxInvoice.setRecipientCountryCode(reservation.getTaxInvoicesList().get(i).getRecipientCountryCode());
					} if (reservation.getTaxInvoicesList().get(i).getGstinOfTheRecipient() != null){
						taxInvoice.setGstinOfTheRecipient(reservation.getTaxInvoicesList().get(i).getGstinOfTheRecipient()); 
					} if (reservation.getTaxInvoicesList().get(i).getStateOfTheRecipient() != null){
						taxInvoice.setStateOfTheRecipient(reservation.getTaxInvoicesList().get(i).getStateOfTheRecipient()); 
					} if (reservation.getTaxInvoicesList().get(i).getStateCodeOfTheRecipient() != null){
						taxInvoice.setStateCodeOfTheRecipient(reservation.getTaxInvoicesList().get(i).getStateCodeOfTheRecipient());
					} if (reservation.getTaxInvoicesList().get(i).getAccountCodeOfService() != null){
						taxInvoice.setAccountCodeOfService(reservation.getTaxInvoicesList().get(i).getAccountCodeOfService());
					} if (reservation.getTaxInvoicesList().get(i).getDescriptionOfService() != null){
						taxInvoice.setDescriptionOfService(reservation.getTaxInvoicesList().get(i).getDescriptionOfService());
					} if (reservation.getTaxInvoicesList().get(i).getPlaceOfSupply() != null){
						taxInvoice.setPlaceOfSupply(reservation.getTaxInvoicesList().get(i).getPlaceOfSupply());
					} if (reservation.getTaxInvoicesList().get(i).getPlaceOfSupplyState() != null){
						taxInvoice.setPlaceOfSupplyState(reservation.getTaxInvoicesList().get(i).getPlaceOfSupplyState());
					} if (reservation.getTaxInvoicesList().get(i).getDigitalSignForInvoiceState() != null){
						taxInvoice.setDigitalSignForInvoiceState(reservation.getTaxInvoicesList().get(i).getDigitalSignForInvoiceState());
					} if (reservation.getTaxInvoicesList().get(i).getTotalValueOfService() != null){
						taxInvoice.setTotalValueOfService(reservation.getTaxInvoicesList().get(i).getTotalValueOfService()); 
					} if (reservation.getTaxInvoicesList().get(i).getTaxRate1Percentage() != null){
						taxInvoice.setTaxRate1Percentage(reservation.getTaxInvoicesList().get(i).getTaxRate1Percentage().doubleValue()); 
					} if (reservation.getTaxInvoicesList().get(i).getTaxRate2Percentage() != null){
						taxInvoice.setTaxRate2Percentage(reservation.getTaxInvoicesList().get(i).getTaxRate2Percentage().doubleValue()); 
					} if (reservation.getTaxInvoicesList().get(i).getTaxRate3Percentage() != null){
						taxInvoice.setTaxRate3Percentage(reservation.getTaxInvoicesList().get(i).getTaxRate3Percentage().doubleValue());
					} if (reservation.getTaxInvoicesList().get(i).getTotalTaxAmount() != null){
						taxInvoice.setTotalTaxAmount(reservation.getTaxInvoicesList().get(i).getTotalTaxAmount());
					} if (reservation.getTaxInvoicesList().get(i).getDateOfIssueOriginalTaxInvoice() != null){
						taxInvoice.setDateOfIssueOriginalTaxInvoice(reservation.getTaxInvoicesList().get(i).getDateOfIssueOriginalTaxInvoice());
					}
					if (reservation.getTaxInvoicesList().get(i).getTaxInvoiceNumber() != null){
						taxInvoice.setTaxInvoiceNumber(reservation.getTaxInvoicesList().get(i).getTaxInvoiceNumber());
					}
					if (reservation.getTaxInvoicesList().get(i).getOriginalTaxInvoiceNumber() != null){
						taxInvoice.setOriginalTaxInvoiceNumber(reservation.getTaxInvoicesList().get(i).getOriginalTaxInvoiceNumber());
					}
					if (reservation.getTaxInvoicesList().get(i).getJourneyDetails() != null) {
						taxInvoice.setJourneyOND(reservation.getTaxInvoicesList().get(i).getJourneyDetails());
					}

					taxInvoicesList.add(taxInvoice);
				}
			}
			lccClientReservation.setTaxInvoicesList(taxInvoicesList);
		}
	}

	public static CarrierWiseServiceTax transformServiceTaxRS(ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS){
		CarrierWiseServiceTax carrierWiseServiceTax = new CarrierWiseServiceTax();
		
		carrierWiseServiceTax.setServiceTaxDepositeCountryCode(serviceTaxRS.getServiceTaxDepositeCountryCode());
		carrierWiseServiceTax.setServiceTaxDepositeStateCode(serviceTaxRS.getServiceTaxDepositeStateCode());
		
		for(Map.Entry<Integer, List<ServiceTaxTO>> entry : serviceTaxRS.getPaxWiseServiceTaxes().entrySet()){
			
			PaxWiseServiceTaxesMap paxWiseServiceTaxesMap = new PaxWiseServiceTaxesMap();
			
			paxWiseServiceTaxesMap.setKey(entry.getKey());
			paxWiseServiceTaxesMap.getValue().addAll(transformSericeTaxTO(entry.getValue()));
			
			carrierWiseServiceTax.getPaxWiseServiceTaxes().add(paxWiseServiceTaxesMap);
			
		}
		
		return carrierWiseServiceTax;
	}
	
	private static List<ServiceTax> transformSericeTaxTO(List<ServiceTaxTO> serviceTaxTOs){
		List<ServiceTax> serviceTaxs = new ArrayList<>();
		for(ServiceTaxTO serviceTaxTO : serviceTaxTOs){
			
			ServiceTax serviceTax = new ServiceTax();
			
			serviceTax.setAmount(serviceTaxTO.getAmount());
			serviceTax.setCarrierCode(serviceTaxTO.getCarrierCode());
			serviceTax.setChargeCode(serviceTaxTO.getChargeCode());
			serviceTax.setChargeRateId(serviceTaxTO.getChargeRateId());
			serviceTax.setChargeGroupCode(serviceTaxTO.getChargeGroupCode());
			serviceTax.setFlightRefNumber(serviceTaxTO.getFlightRefNumber());
			serviceTax.setNonTaxableAmount(serviceTaxTO.getNonTaxableAmount());
			serviceTax.setTaxableAmount(serviceTaxTO.getTaxableAmount());
			
			serviceTaxs.add(serviceTax);
		}
		
		return serviceTaxs;
	}
		
	public static LCCSelfReprotectFlightsRQ transform(TrackInfoDTO trackInfo, String alertId){
		
		LCCSelfReprotectFlightsRQ lccSelfReprotectFlightsRQ = new LCCSelfReprotectFlightsRQ();
		lccSelfReprotectFlightsRQ.setAlertId(alertId);
		lccSelfReprotectFlightsRQ.setTrackInfo(transformTrackingInfo(trackInfo));
		
		return lccSelfReprotectFlightsRQ;
	}

	public static BundleFareDescriptionTemplateDTO transform(BundleFareDescriptionInfo sourceDescriptionInfo) {

		BundleFareDescriptionTemplateDTO targetTemplateDTO = new BundleFareDescriptionTemplateDTO();

		if (sourceDescriptionInfo != null) {
			targetTemplateDTO.setTemplateID(sourceDescriptionInfo.getDescriptionTemplateId());
			targetTemplateDTO.setTemplateName(sourceDescriptionInfo.getTemplateName());
			targetTemplateDTO.setDefaultFreeServicesContent(sourceDescriptionInfo.getDefaultFreeServicesContent());
			targetTemplateDTO.setDefaultPaidServicesContent(sourceDescriptionInfo.getDefaultPaidServicesContent());

			Map<String, BundleFareDescriptionTranslationTemplateDTO> translationTemplates = new HashMap<String, BundleFareDescriptionTranslationTemplateDTO>();
			List<StringBundledFareTranslationMap> sourceTranslationTemplates = sourceDescriptionInfo.getTranslationTemplates();

			if (sourceTranslationTemplates != null && !sourceTranslationTemplates.isEmpty()) {
				for (StringBundledFareTranslationMap sourceTranslationTemplate : sourceTranslationTemplates) {
					String language = sourceTranslationTemplate.getKey();
					BundleFareTranslationInfo sourceTranslationInfo = sourceTranslationTemplate.getValue();

					BundleFareDescriptionTranslationTemplateDTO targetTranslation = new BundleFareDescriptionTranslationTemplateDTO();
					targetTranslation.setTemplateID(sourceTranslationInfo.getDescriptionTemplateId());
					targetTranslation.setLanguageCode(sourceTranslationInfo.getLanguageCode());
					targetTranslation.setFreeServicesContent(sourceTranslationInfo.getFreeServicesContent());
					targetTranslation.setPaidServicesContent(sourceTranslationInfo.getPaidServicesContent());

					translationTemplates.put(language, targetTranslation);

				}
				targetTemplateDTO.setTranslationTemplates(translationTemplates);
			}
		}

		return targetTemplateDTO;
	}

}
