package com.isa.thinair.lccclient.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @isa.module.service-interface module-name="lccclient" description="LC Connecct Client"
 */
public class LCCClientConfigModuleService extends DefaultModule {
}
