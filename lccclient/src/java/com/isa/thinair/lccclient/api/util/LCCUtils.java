package com.isa.thinair.lccclient.api.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerStringMap;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * @author Nilindra Fernando
 */
public class LCCUtils {

	/**
	 * Method to get the 2 letter iso code from the nationality code
	 * 
	 * @param nationalityCode
	 *            Integer representing the nationality in the DB
	 * @return 2 letter iso code
	 * @throws ModuleException
	 */
	public static String getNationalityIsoCode(Integer nationalityCode) throws ModuleException {
		String strNationality = null;

		if (nationalityCode != null) {
			Nationality nationality = LCCClientModuleUtil.getCommonMasterBD().getNationality(nationalityCode);

			if (nationality != null) {
				strNationality = nationality.getIsoCode();
			}
		}

		return strNationality;
	}

	/**
	 * Method to get the nationality code from the 2 letter iso code
	 * 
	 * @param nationalityIsoCode
	 *            2 letter iso code
	 * @return Integer representing the nationality in the DB
	 * @throws ModuleException
	 */
	public static Integer getNationalityCode(String nationalityIsoCode) throws ModuleException {
		Integer intNationality = null;

		if (nationalityIsoCode != null) {
			Nationality nationality = LCCClientModuleUtil.getCommonMasterBD().getNationality(nationalityIsoCode);
			if (nationality != null) {
				intNationality = nationality.getNationalityCode();
			}
		}

		return intNationality;
	}

	public static String getCountryName(String countryCode) throws ModuleException {
		String strCountryName = null;

		if (countryCode != null) {
			Country country = LCCClientModuleUtil.getCommonMasterBD().getCountryByName(countryCode);

			if (country != null) {
				strCountryName = country.getCountryName();
			}
		}

		return strCountryName;
	}

	public static String getNationalityName(Integer nationalityCode) throws ModuleException {
		String strNationality = null;

		if (nationalityCode != null) {
			Nationality nationality = LCCClientModuleUtil.getCommonMasterBD().getNationality(nationalityCode);

			if (nationality != null) {
				strNationality = nationality.getDescription();
			}
		}

		return strNationality;
	}

	public static TrackInfoDTO getTrackInfoDto(UserPrincipal userPrincipal) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		if (userPrincipal != null) {
			trackInfoDTO.setCarrierCode(userPrincipal.getDefaultCarrierCode());
			trackInfoDTO.setIpAddress(userPrincipal.getIpAddress());
		}
		return trackInfoDTO;
	}
	
	public static List<IntegerStringMap> composeIntegerStringMap(Map<Integer, String> inputMap) {
		List<IntegerStringMap> outputList = new ArrayList<IntegerStringMap>();
		for (Integer key : inputMap.keySet()) {
			IntegerStringMap outputdMapElement = new IntegerStringMap();
			outputdMapElement.setKey(key);
			outputdMapElement.setValue(inputMap.get(key));
			outputList.add(outputdMapElement);
		}
		return outputList;
	}

}