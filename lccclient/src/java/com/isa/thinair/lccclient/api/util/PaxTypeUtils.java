package com.isa.thinair.lccclient.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

/**
 * @author Nilindra Fernando
 */
public class PaxTypeUtils {

	private static final String $ = "$";
	private static final String CARRIER_SEPERATOR = "|";
	private static final String TRAVELLER_SEPERATOR = ",";
	private static final String VERSION_SEPERATOR = "-";

	public static String travelerReference(LCCClientReservationPax lcclientReservationPax) {
		String travelerReference = "";
		if (ReservationInternalConstants.PassengerType.ADULT.equals(lcclientReservationPax.getPaxType())) {
			travelerReference = "A" + lcclientReservationPax.getPaxSequence();
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(lcclientReservationPax.getPaxType())) {
			travelerReference = "C" + lcclientReservationPax.getPaxSequence();
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(lcclientReservationPax.getPaxType())) {
			// FIXME getParent is null in reservation flow change the method

			if (lcclientReservationPax.getParent() != null)
				travelerReference = "I" + lcclientReservationPax.getPaxSequence() + "/A"
						+ lcclientReservationPax.getParent().getPaxSequence();
			else
				travelerReference = "I" + lcclientReservationPax.getPaxSequence() + "/A"
						+ lcclientReservationPax.getAttachedPaxId();
		}
		return travelerReference;
	}

	/**
	 * Returns whether or not the passed traveler ref number is that of an infant
	 * 
	 * @param infantTravelerRefNumber
	 * @return
	 */
	// TODO : This method should be factored out for both AA & LCC (Until then keeping method public)
	public static boolean isInfant(String travelerRefNumber) {
		return travelerRefNumber.indexOf('/') == -1 ? false : true;
	}

	/**
	 * Extract the Pax Sequence from TravelerRefNumber (or return as is if it is only composed of ints)
	 * 
	 * @param travelerRefNumber
	 * @return extracted Sequence
	 */
	// This method should be factored out for both AA & LCC
	public static Integer getPaxSeq(String travelerRefNumber) {

		// Check if only numbers
		boolean isOnlyNumbers = true;
		for (int i = 0; i < travelerRefNumber.length(); i++) {
			if (!Character.isDigit(travelerRefNumber.charAt(i))) {
				isOnlyNumbers = false;
				break;
			}
		}

		if (isOnlyNumbers) {
			return Integer.valueOf(travelerRefNumber.trim());
		}

		// Extract Pax Sequence
		StringBuilder sequence = new StringBuilder();
		String numbers = "0123456789";

		int indexOf$ = travelerRefNumber.indexOf('$');

		for (int i = indexOf$ - 1; i >= 0; i--) {
			char charAt = travelerRefNumber.charAt(i);
			if (numbers.indexOf(charAt) == -1) {
				if (i > 2) {
					if (travelerRefNumber.charAt(i - 1) == '/') {
						sequence = new StringBuilder();
						i--;
						continue;
					}
				}
				break;
			} else {
				sequence.append(charAt);
			}
		}

		String sequenceStr = sequence.reverse().toString();
		if (sequenceStr.length() != 0) {
			return Integer.valueOf(sequenceStr);
		} else {
			throw new RuntimeException("Could not extract pax sequence from travelerRefNumber = " + travelerRefNumber);
		}
	}

	/**
	 * If the passed traveler ref number is not that of an infant then this method will return a (-1)
	 * 
	 * @param infantTravelerRefNumber
	 * @return
	 */
	// TODO : This method should be factored out for both AA & LCC (Until then keeping method public)
	public static Integer getParentSeq(String infantTravelerRefNumber) {

		StringBuilder sequence = new StringBuilder();
		String numbers = "0123456789";

		int indexOfFS = infantTravelerRefNumber.indexOf('/');
		if (indexOfFS == -1) {
			return indexOfFS;
		}

		for (int i = indexOfFS + 2; i <= infantTravelerRefNumber.length(); i++) {
			char charAt = infantTravelerRefNumber.charAt(i);
			if (numbers.indexOf(charAt) == -1) {
				break;
			} else {
				sequence.append(charAt);
			}
		}

		return Integer.valueOf(sequence.toString());
	}

	/**
	 * Returns corrected encoded traveler reference information for the passed LccClientPax infant
	 * 
	 * @param reservationPax
	 * @return travelerReference
	 */
	public static String fixLccInfantTravelerReference(LCCClientReservationPax infantLccPax) {
		StringBuilder travelerReference = new StringBuilder();
		for (String perCarrierTravelerRef : infantLccPax.getTravelerRefNumber().split(PaxTypeUtils.TRAVELLER_SEPERATOR)) {
			Integer infantPnrPaxId = PaxTypeUtils.getPnrPaxId(perCarrierTravelerRef);
			String carrier = perCarrierTravelerRef.split("\\" + PaxTypeUtils.CARRIER_SEPERATOR)[0];
			travelerReference.append(carrier + PaxTypeUtils.CARRIER_SEPERATOR + "I" + infantLccPax.getPaxSequence() + "/A"
					+ infantLccPax.getParent().getPaxSequence() + PaxTypeUtils.$ + infantPnrPaxId
					+ PaxTypeUtils.TRAVELLER_SEPERATOR);
		}

		return travelerReference.substring(0, travelerReference.length() - 1);
	}

	public static Integer getPnrPaxId(String lccTravelerReferenceNumber) {
		int pnrPaxIdStartIndex = lccTravelerReferenceNumber.lastIndexOf(PaxTypeUtils.$);
		pnrPaxIdStartIndex = pnrPaxIdStartIndex + 1;
		if (pnrPaxIdStartIndex > 0) {
			String pnrPaxId = lccTravelerReferenceNumber.substring(pnrPaxIdStartIndex);
			return new Integer(pnrPaxId);
		}
		return null;
	}

	/**
	 * method to extract carrier (AA services) traveler ref number from LCC traveler ref number
	 */
	public static String getCarrierTravelerRefFromLCCTravelerRef(String airlineCode, String lccTravelerRef) {
		String carrierTravelerRef = null;
		if (lccTravelerRef != null) {
			String[] travelRefNumbers = lccTravelerRef.split(TRAVELLER_SEPERATOR);
			for (String carrerRefProtion : travelRefNumbers) {
				if (carrerRefProtion.indexOf(airlineCode, 0) >= 0) {
					int indexOfAirlineCodeSeparator = carrerRefProtion.lastIndexOf(CARRIER_SEPERATOR);
					carrierTravelerRef = carrerRefProtion.substring(indexOfAirlineCodeSeparator + 1);
					carrierTravelerRef = carrierTravelerRef.trim();
					break;
				}
			}
		}
		return carrierTravelerRef;
	}

	public static String getMarketingOwnerAgentCode(String marketingCarrier, String ownerAgentCode) {
		String[] mktingCarrierData = null;
		String mktingOwnerAgCode = null;
		if (ownerAgentCode != null) {
			for (String agentCode : ownerAgentCode.split(TRAVELLER_SEPERATOR)) {
				if (agentCode.contains(marketingCarrier)) {
					mktingCarrierData = agentCode.split("\\" + CARRIER_SEPERATOR);
					if (mktingCarrierData != null && mktingCarrierData.length > 1) {
						mktingOwnerAgCode = mktingCarrierData[1];
						break;
					}
				}
			}
		}
		return mktingOwnerAgCode;
	}

	public static String convertLCCPaxCodeToAAPaxCodes(PassengerType paxType) {
		switch (paxType) {
		case ADT:
			return PaxTypeTO.ADULT;
		case CHD:
			return PaxTypeTO.CHILD;
		case INF:
			return PaxTypeTO.INFANT;
		case PRT:
			return PaxTypeTO.PARENT;
		default:
			return null; // undefined pax type found, return null}
		}
	}

	public static PassengerType convertAAPaxCodeToLCCPaxCodes(String applicablePaxType) {
		if (applicablePaxType.equals(PaxTypeTO.ADULT)) {
			return PassengerType.ADT;
		} else if (applicablePaxType.equals(PaxTypeTO.CHILD)) {
			return PassengerType.CHD;
		} else if (applicablePaxType.equals(PaxTypeTO.INFANT)) {
			return PassengerType.INF;
		} else if (applicablePaxType.equals(PaxTypeTO.PARENT)) {
			return PassengerType.PRT;
		}

		return null;
	}

	public static List<String> getTravelRefList(String travelRef) {
		List<String> travelRefList = null;
		if (travelRef != null) {
			String[] travelRefArr = travelRef.split(PaxTypeUtils.TRAVELLER_SEPERATOR);

			if (travelRefArr != null && travelRefArr.length != 0) {
				travelRefList = new ArrayList<String>();
				for (String paxRef : travelRefArr) {
					travelRefList.add(paxRef.trim());
				}
			}
		}

		return travelRefList;
	}

	/**
	 * to get infant TravelerRefNumber for a given adult TravelerRefNumber from Collection<LCCClientPassengerSummaryTO>
	 * when an infant is associated with an Adult
	 * 
	 * */
	public static String getAssociatedInfTravRef(Collection<LCCClientPassengerSummaryTO> colPassengerSummary,
			String adultTravelRef) {

		if (adultTravelRef != null && colPassengerSummary != null) {
			String adultRefStr = adultTravelRef.split("\\" + CARRIER_SEPERATOR)[1];
			String adultRef = adultRefStr.split("\\" + $)[0];

			for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
				if (paxSummaryTo != null) {
					if (paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {

						String infantTravRef = paxSummaryTo.getTravelerRefNumber();
						String infantRefStr = infantTravRef.split("\\" + CARRIER_SEPERATOR)[1];
						String infantRef = infantRefStr.split("\\" + $)[0];
						String infAdultRef = infantRef.split("/")[1];

						if (infAdultRef.equals(adultRef)) {
							return infantTravRef;
						}

					}
				}

			}
		}
		return null;
	}

}
