package com.isa.thinair.lccclient.api.service;

import com.isa.thinair.airmaster.api.dto.MasterDataCriteriaDTO;
import com.isa.thinair.airmaster.api.dto.MasterDataDTO;
import com.isa.thinair.commons.api.exception.ModuleException;


public interface LCCMasterDataBD {
	public static final String SERVICE_NAME = "MasterDataService";

	public MasterDataDTO getLccAgreementMasterData(MasterDataCriteriaDTO masterDataCriteriaDTO) throws ModuleException;

}

