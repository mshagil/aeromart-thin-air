package com.isa.thinair.lccclient.api.service;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * lccclient interface to call LCC alerting apis
 * 
 * @author sanjaya
 */
public interface LCCAlertingBD {

	public static final String SERVICE_NAME = "LCCAlertingService";

	/**
	 * Searches the LCC for alerts for a given search criteria.
	 * 
	 * @param searchCriteria
	 *            The alert search criteria.
	 * @param trackInfo
	 *            TODO
	 * @return A {@link Page} of alert results.
	 * @throws ModuleException
	 */
	public Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria, BasicTrackInfo trackInfo) throws ModuleException;

	/**
	 * Clears the provided set of alerts in the provided airline.
	 * 
	 * @param airline
	 * @param collection
	 * @param trackInfo
	 *            TODO
	 */
	public void clearAlerts(String airline, Collection<Integer> collection, BasicTrackInfo trackInfo) throws ModuleException;

	public void addAlerts(List<AlertDetailDTO> alertDTOList, BasicTrackInfo trackInfo, String carrierCode) throws ModuleException;

}
