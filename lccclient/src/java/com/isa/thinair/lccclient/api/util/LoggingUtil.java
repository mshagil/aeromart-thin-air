package com.isa.thinair.lccclient.api.util;

import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerCreditPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;

public class LoggingUtil {

	public static String getPaymentDetails(List<TravelerFulfillment> travelerFulfillments) {
		StringBuffer paymentInfo = new StringBuffer();
		if (travelerFulfillments != null) {
			for (TravelerFulfillment travelerFulfillment : travelerFulfillments) {
				paymentInfo.append(" pax sequence " + travelerFulfillment.getPaxSequence() + " requesting for payments [ ");
				paymentInfo.append(getPaymentDetailsLog(travelerFulfillment.getPaymentDetails()));
				paymentInfo.append(" ] \n");
			}
		}
		return paymentInfo.toString();
	}

	public static String getPaymentDetailsLog(List<PaymentDetails> paymentDetailsList) {
		StringBuffer paymentInfo = new StringBuffer();
		for (PaymentDetails paymentDetails : paymentDetailsList) {
			paymentInfo.append(paymentDetails.getPaymentSummary().getPaymentCarrier() + " - "
					+ paymentDetails.getPaymentSummary().getPaymentAmount());
			if (paymentDetails.getTravelerCreditPayment() != null) {
				TravelerCreditPayment travelerCreditPayment = paymentDetails.getTravelerCreditPayment();
				paymentInfo.append(" utilizing credits from " + travelerCreditPayment.getBasePnr() + " max up to "
						+ travelerCreditPayment.getCarrierBaseAmount());
				if (travelerCreditPayment.getEDate() != null)
					paymentInfo.append(" for the pax credit record " + travelerCreditPayment.getPaxCreditId() + " expires on "
							+ travelerCreditPayment.getEDate());
			}
		}
		return paymentInfo.toString();
	}

}
