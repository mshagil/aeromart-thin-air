package com.isa.thinair.paymentbroker.core.bl.nmcrypt;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;

import com.isa.thinair.paymentbroker.api.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerConfig;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.impl.config.PaymentBrokerImplConfig;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestPaymentBrokerNMCryptgateImpl extends PlatformTestCase {

    private final Log log = LogFactory.getLog(getClass());

    private static PaymentBrokerImplConfig configImpl = PaymentBrokerModuleUtil.getImplConfig();

    PaymentBroker paymentBroker = null;

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private PaymentBroker getPaymentBroker() {
        System.out.println("Inside the getPaymentBroker()");

        paymentBroker = configImpl.getPaymentBroker();
        System.out.println("Payment broker Lookup successful...");

        return paymentBroker;

    }

    public void testCardPayment() {
        CreditCardPayment creditCardPayment = new CreditCardPayment();
        creditCardPayment.setCardholderName("Hans Mustermann");
        creditCardPayment.setCardNumber("4111111111111111");
        creditCardPayment.setExpiryDate("0312");
        creditCardPayment.setAmount("21.06");
        creditCardPayment.setCurrency("EUR");
        creditCardPayment.setCvvField("003");

        String pnr = "100000000";
        AppIndicatorEnum appInd = AppIndicatorEnum.APP_XBE;

        TnxModeEnum tnxMode = null;
        Map mapInfo = new HashMap();

        try {
            ServiceResponce serviceResponce = getPaymentBroker().charge(creditCardPayment, pnr, appInd, tnxMode);

            System.out.println("==================================================");
            System.out.println("Transaction Success Status :" + serviceResponce.isSuccess());
            System.out.println("Transaction Response Code :" + serviceResponce.getResponseCode());
            String transMessage = (String) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
            System.out.println("Transaction Status Result :" + transMessage);
            System.out.println("==================================================");

            assertNotNull("testCardPayment is null", serviceResponce);

        } catch (ModuleException moduleException) {
            System.out.println("inside the moduleException...");
            moduleException.printStackTrace();
            fail("ModuleException at testCardPayment = " + moduleException.getMessageString());
            log.error("ModuleException at testCardPayment = " + moduleException.getMessageString());
        } catch (Exception exception) {
            System.out.println("inside the Exception...");
            exception.printStackTrace();
            fail("Exception at testCardPayment = " + exception.getMessage());
            log.error("Exception at testCardPayment = " + exception.getMessage());
        }
    }

}
