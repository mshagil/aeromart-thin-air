package com.isa.thinair.paymentbroker.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.paymentbroker.api.util.PaymentConstants;


public class TestMe {

    private static final int ASCII_A = 64;

    public static void main(String[] args) {
        // System.out.println(composeReferenceDate());
        //showChars();
    }


    private static void showChars() {
        int len = 60;
        for (int i = 1; i < len; i++) {
            System.out.println(i + ": " + getChars(i));
        }
    }

    private static String composeReferenceDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd/HH/mm/ss");
        String date[] = StringUtils.split(sdf.format(new Date()), '/');
        int year = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1]);
        int day = Integer.parseInt(date[2]);
        int hours = Integer.parseInt(date[3]);
        int minutes = Integer.parseInt(date[4]);
        int seconds = Integer.parseInt(date[5]);

        // if (day > 9) {
        // day = (day - 9) + PaymentConstants.ASCII_A;
        // return (String.valueOf((char) year) + String.valueOf((char) month) +
        // String.valueOf((char) day));
        // }
        return (getChars(year) + getChars(month) + getChars(day) + getChars(hours) + getChars(minutes) + getChars(seconds));

    }

    private static String getChars(int no) {
        String chars = null;
        no += PaymentConstants.ASCII_AT;

        if (no > 116) {
            no -= 52;
            chars = String.valueOf((char) PaymentConstants.ASCII_C) + String.valueOf((char) no);
        } else if (no > 90) {
            no -= 26;
            chars = String.valueOf((char) PaymentConstants.ASCII_B) + String.valueOf((char) no);
        } else {
            chars = String.valueOf((char) PaymentConstants.ASCII_A) + String.valueOf((char) no);
        }

        return chars;
    }
}
