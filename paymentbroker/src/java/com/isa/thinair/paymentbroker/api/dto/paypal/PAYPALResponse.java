package com.isa.thinair.paymentbroker.api.dto.paypal;

import java.io.Serializable;

import com.paypal.soap.api.DoDirectPaymentResponseType;
import com.paypal.soap.api.DoExpressCheckoutPaymentResponseType;
import com.paypal.soap.api.GetExpressCheckoutDetailsResponseType;
import com.paypal.soap.api.SetExpressCheckoutResponseType;

public class PAYPALResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private int paymentBrokerRefNo;

	private String currentState;

	private DoDirectPaymentResponseType doDirectPaymentResponseType;

	private SetExpressCheckoutResponseType setExpressCheckoutResponseType;

	private GetExpressCheckoutDetailsResponseType getExpressCheckoutDetailsResponseType;

	private DoExpressCheckoutPaymentResponseType doExpressCheckoutPaymentResponseType;

	private String creditCardType;

	private String CVV2;

	public void setSetExpressCheckoutResponseType(SetExpressCheckoutResponseType setExpressCheckoutResponseType) {
		this.setExpressCheckoutResponseType = setExpressCheckoutResponseType;
	}

	public SetExpressCheckoutResponseType getSetExpressCheckoutResponseType() {
		return setExpressCheckoutResponseType;
	}

	public void setDoDirectPaymentResponseType(DoDirectPaymentResponseType doDirectPaymentResponseType) {
		this.doDirectPaymentResponseType = doDirectPaymentResponseType;
	}

	public DoDirectPaymentResponseType getDoDirectPaymentResponseType() {
		return doDirectPaymentResponseType;
	}

	public void
			setDoExpressCheckoutPaymentResponseType(DoExpressCheckoutPaymentResponseType doExpressCheckoutPaymentResponseType) {
		this.doExpressCheckoutPaymentResponseType = doExpressCheckoutPaymentResponseType;
	}

	public DoExpressCheckoutPaymentResponseType getDoExpressCheckoutPaymentResponseType() {
		return doExpressCheckoutPaymentResponseType;
	}

	public void setGetExpressCheckoutDetailsResponseType(
			GetExpressCheckoutDetailsResponseType getExpressCheckoutDetailsResponseType) {
		this.getExpressCheckoutDetailsResponseType = getExpressCheckoutDetailsResponseType;
	}

	public GetExpressCheckoutDetailsResponseType getGetExpressCheckoutDetailsResponseType() {
		return getExpressCheckoutDetailsResponseType;
	}

	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public String getCreditCardType() {
		return creditCardType;
	}

	public void setCVV2(String cVV2) {
		CVV2 = cVV2;
	}

	public String getCVV2() {
		return CVV2;
	}

}
