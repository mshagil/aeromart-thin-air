package com.isa.thinair.paymentbroker.api.dto.tap;

import java.io.Serializable;

public class TapPaymentRequestCallRs extends TapCommonResponse implements Serializable{

	private static final long serialVersionUID = -190909078944591L;
	
	private String referenceID;
	private String paymentURL;
	private String tapPaymentURL;
	
	public String getReferenceID() {
		return referenceID;
	}
	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}
	public String getPaymentURL() {
		return paymentURL;
	}
	public void setPaymentURL(String paymentURL) {
		this.paymentURL = paymentURL;
	}
	
	public String getTapPaymentURL() {
		return tapPaymentURL;
	}
	public void setTapPaymentURL(String tapPaymentURL) {
		this.tapPaymentURL = tapPaymentURL;
	}
	
}
