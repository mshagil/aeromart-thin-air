package com.isa.thinair.paymentbroker.api.dto.Parsian;

import java.io.Serializable;

public class ParsianRefundResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String message;
	
	private boolean hasPermission;
	
	private boolean success;
	
	private short refundStatusId;
	
	private short transactionStatus;
	
	private int paymentBrokerRefNo;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isHasPermission() {
		return hasPermission;
	}

	public void setHasPermission(boolean hasPermission) {
		this.hasPermission = hasPermission;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public short getRefundStatusId() {
		return refundStatusId;
	}

	public void setRefundStatusId(short refundStatusId) {
		this.refundStatusId = refundStatusId;
	}

	public short getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(short transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}	
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("message:" + message).append(",hasPermission:" + hasPermission)
				.append(",success:" + success).append(",refundStatusId:" + refundStatusId)
				.append(",transactionStatus:" + transactionStatus).append(",paymentBrokerRefNo:" + paymentBrokerRefNo);;

		return builder.toString();
	}
}
