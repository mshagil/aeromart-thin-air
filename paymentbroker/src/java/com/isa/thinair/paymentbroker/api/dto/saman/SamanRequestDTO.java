package com.isa.thinair.paymentbroker.api.dto.saman;

import java.io.Serializable;

public class SamanRequestDTO implements Serializable {

	private static final long serialVersionUID = -1909090984622344L;

	private String referenceNo;

	private String merchantId;

	private String merchantPassword;

	private Double refundAmount;

	// additional parameters for SRTM implementation
	private String refundDateTimeStr;

	private String refundUser;

	private String refundPassword;
	
	private String transResNo;

	private String refundResNo;

	private String refundReferenceNo;

	private String refundErrorCode;

	private String refundErrorDesc;

	private String cusEmail;

	private String cusCell;
	
	private int exeTime;
	
	private String transReqReferance;
	
	private boolean immediateRefund;
	

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantPassword() {
		return merchantPassword;
	}

	public void setMerchantPassword(String merchantPassword) {
		this.merchantPassword = merchantPassword;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("referenceNo:" + referenceNo).append(",merchantId:" + merchantId)
				.append(",merchantPassword:" + merchantPassword).append(",refundAmount:" + refundAmount);

		if (refundReferenceNo != null) {
			builder.append(",refundDateTimeStr:" + refundDateTimeStr).append(",transResNo:" + transResNo)
					.append(",refundResNo:" + refundResNo).append(",refundReferenceNo:" + refundReferenceNo)
					.append(",refundErrorCode:" + refundErrorCode).append(",refundErrorDesc:" + refundErrorDesc)
					.append(",cusEmail:" + cusEmail).append(",cusCell:" + cusCell);
		}

		return builder.toString();
	}
	
	public String getRefund_RegString() {
		StringBuilder builder = new StringBuilder();
//		(userName, password, refNum, resNum, transactionTermId, refundTermId,
				//amount, requestId, exeTime, email, cellNumber});
		builder.append("userName:" + refundUser).append(",password:" + refundPassword)
		.append(",refNum:" + referenceNo).append(",resNum:" + transReqReferance)
		.append(",transactionTermId:" + merchantId).append(",refundTermId:" + merchantId)
		.append(",amount:" + refundAmount).append(",requestId:" + "TPT_ID")
		.append(",exeTime:" + exeTime).append(",email:" + cusEmail)
		.append(",cellNumber:" + cusCell);
		return builder.toString();
	}
	
	public String getRefund_ExcString(Long refundRegID) {
		StringBuilder builder = new StringBuilder();
//		(userName, password, refundRegID, TypeRefundAction.Approve, merchentID)
		builder.append("userName:" + refundUser).append(",password:" + refundPassword)
		.append(",partialRefundId:" + refundRegID).append(",typRefundAction:" + "Approve")
		.append(",termId:" + merchantId);
		
		return builder.toString();
	}

	public String getRefundDateTimeStr() {
		return refundDateTimeStr;
	}

	public void setRefundDateTimeStr(String refundDateTimeStr) {
		this.refundDateTimeStr = refundDateTimeStr;
	}

	public String getRefundUser() {
		return refundUser;
	}

	public void setRefundUser(String refundUser) {
		this.refundUser = refundUser;
	}

	public String getRefundPassword() {
		return refundPassword;
	}

	public void setRefundPassword(String refundPassword) {
		this.refundPassword = refundPassword;
	}

	public String getTransResNo() {
		return transResNo;
	}

	public void setTransResNo(String transResNo) {
		this.transResNo = transResNo;
	}

	public String getRefundResNo() {
		return refundResNo;
	}

	public void setRefundResNo(String refundResNo) {
		this.refundResNo = refundResNo;
	}

	public String getRefundReferenceNo() {
		return refundReferenceNo;
	}

	public void setRefundReferenceNo(String refundReferenceNo) {
		this.refundReferenceNo = refundReferenceNo;
	}

	public String getRefundErrorCode() {
		return refundErrorCode;
	}

	public void setRefundErrorCode(String refundErrorCode) {
		this.refundErrorCode = refundErrorCode;
	}

	public String getRefundErrorDesc() {
		return refundErrorDesc;
	}

	public void setRefundErrorDesc(String refundErrorDesc) {
		this.refundErrorDesc = refundErrorDesc;
	}

	public String getCusEmail() {
		return cusEmail;
	}

	public void setCusEmail(String cusEmail) {
		this.cusEmail = cusEmail;
	}

	public String getCusCell() {
		return cusCell;
	}

	public void setCusCell(String cusCell) {
		this.cusCell = cusCell;
	}

	public int getExeTime() {
		return exeTime;
	}

	public void setExeTime(int exeTime) {
		this.exeTime = exeTime;
	}

	public String getTransReqReferance() {
		return transReqReferance;
	}

	public void setTransReqReferance(String transReqReferance) {
		this.transReqReferance = transReqReferance;
	}
	
	public boolean isImmediateRefund() {
		return immediateRefund;
	}

	public void setImmediateRefund(boolean immediateRefund) {
		this.immediateRefund = immediateRefund;
	}

}