package com.isa.thinair.paymentbroker.api.dto.Parsian;

import java.io.Serializable;

public class ParsianPaymentRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;	
	
	private String pin;
	
	private int amount;
	
	private int orderId;
	
	private String callbackUrl;
	
	private long authority;
	
	private short status;
	
	private int orderToVoid;
	
	private int orderToReversal;
	
	private int orderToRefund;

	private String AdditionalData;

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public long getAuthority() {
		return authority;
	}

	public void setAuthority(long authority) {
		this.authority = authority;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}	
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("pin:" + pin).append(",amount:" + amount).append(",orderId:" + orderId)
				.append(",callbackUrl:" + callbackUrl);

		return builder.toString();
	}

	public int getOrderToVoid() {
		return orderToVoid;
	}

	public void setOrderToVoid(int orderToVoid) {
		this.orderToVoid = orderToVoid;
	}

	public int getOrderToReversal() {
		return orderToReversal;
	}

	public void setOrderToReversal(int orderToReversal) {
		this.orderToReversal = orderToReversal;
	}

	public int getOrderToRefund() {
		return orderToRefund;
	}

	public void setOrderToRefund(int orderToRefund) {
		this.orderToRefund = orderToRefund;
	}
	
	public String getRefundString() {
		StringBuilder builder = new StringBuilder();
		builder.append("pin:" + pin).append(",refund_amount:" + amount).append(",orderId:" + orderId)
				.append(",order_id_to_refund:" + orderToRefund);

		return builder.toString();
	}

	public String getAdditionalData() {
		return AdditionalData;
	}

	public void setAdditionalData(String AdditionalData) {
		this.AdditionalData = AdditionalData;
	}
}
