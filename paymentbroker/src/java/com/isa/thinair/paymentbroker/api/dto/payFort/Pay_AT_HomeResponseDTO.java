package com.isa.thinair.paymentbroker.api.dto.payFort;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

public class Pay_AT_HomeResponseDTO implements Serializable {

private static final long serialVersionUID = -8308509598531544L;
	
	public static final String STATUS_CODE = "statusCode";
	public static final String ORDER_ID = "orderID";
	public static final String ORDER_REF = "orderReference";
	public static final String TRACK_NUMBER = "trackingNumber";
	public static final String CHECK_SUM = "checkSum";
	public static final String SERVICE_NAME = "serviceName";
	public static final String LANGUAGE = "language";
	
	private String statusCode;
	
	private String orderID;
	
	private String orderReference;
	
	private String trackingNumber;
	
	private String signature;
	
	private String 	Status;
	
	private String additionalNote;
	
	private String errorCode;
	
	private String errorDesc;
	
	private XMLGregorianCalendar msgDate;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getAdditionalNote() {
		return additionalNote;
	}

	public void setAdditionalNote(String additionalNote) {
		this.additionalNote = additionalNote;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public XMLGregorianCalendar getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(XMLGregorianCalendar msgDate) {
		this.msgDate = msgDate;
	}

	public String getOrderReference() {
		return orderReference;
	}

	public void setOrderReference(String orderReference) {
		this.orderReference = orderReference;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

}
