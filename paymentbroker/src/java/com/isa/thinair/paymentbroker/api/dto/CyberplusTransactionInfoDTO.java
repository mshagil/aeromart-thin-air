package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

public class CyberplusTransactionInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -45427471529097319L;

	private int errorCode;

	private int transactionStatus;

	private String authNb;

	private int authResult;

	private long amount;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public int getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(int transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getAuthNb() {
		return authNb;
	}

	public void setAuthNb(String authNb) {
		this.authNb = authNb;
	}

	public int getAuthResult() {
		return authResult;
	}

	public void setAuthResult(int authResult) {
		this.authResult = authResult;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String toString() {
		StringBuilder transactionInfoDTO = new StringBuilder();
		transactionInfoDTO.append("errorCode").append(":").append(errorCode).append(", ").append("transactionStatus").append(":")
				.append(transactionStatus).append(", ").append("authNb").append(":").append(authNb).append(", ");
		return transactionInfoDTO.toString();
	}
}
