package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.paymentbroker.api.util.CVVOptions;

public class IPGPaymentOptionDTO implements Serializable {

	private static final long serialVersionUID = 3190271753568488500L;

	public IPGPaymentOptionDTO(){}

	private String firstSegmentONDCode;
	private String selCurrency;
	private String baseCurrency;
	private String providerName;
	private String providerCode;
	private String description;
	private int paymentGateway;
	private String moduleCode;
	private boolean saveCard;
	private String entityCode;
	

	private int cvvOption = CVVOptions.CARD_CVV_OPTION_MANDATORY;
	private int onholdReleaseTime;
	/** A flag to indicate whether this is the default payment gateway for the given module */
	private String isDefault;
	private String isModificationAllow;
	private boolean offlinePayment;
	
	private BigDecimal aliasBasePaymentAmount;

	public String getFirstSegmentONDCode() {
		return firstSegmentONDCode;
	}

	public String getSelCurrency() {
		return selCurrency;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public String getProviderName() {
		return providerName;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public String getDescription() {
		return description;
	}

	public int getPaymentGateway() {
		return paymentGateway;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public int getCvvOption() {
		return cvvOption;
	}

	public int getOnholdReleaseTime() {
		return onholdReleaseTime;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public String getIsModificationAllow() {
		return isModificationAllow;
	}

	public boolean isOfflinePayment() {
		return offlinePayment;
	}


	public IPGPaymentOptionDTO setSelCurrency(String selCurrency) {
		this.selCurrency = selCurrency;
		return this;
	}

	public IPGPaymentOptionDTO setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
		return this;
	}

	public IPGPaymentOptionDTO setProviderName(String providerName) {
		this.providerName = providerName;
		return this;
	}

	public IPGPaymentOptionDTO setProviderCode(String providerCode) {
		this.providerCode = providerCode;
		return this;
	}

	public IPGPaymentOptionDTO setDescription(String description) {
		this.description = description;
		return this;
	}

	public IPGPaymentOptionDTO setPaymentGateway(int paymentGateway) {
		this.paymentGateway = paymentGateway;
		return this;
	}

	public IPGPaymentOptionDTO setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
		return this;
	}

	public IPGPaymentOptionDTO setCvvOption(int cvvOption) {
		this.cvvOption = cvvOption;
		return this;
	}

	public IPGPaymentOptionDTO setOnholdReleaseTime(int onholdReleaseTime) {
		this.onholdReleaseTime = onholdReleaseTime;
		return this;
	}

	public IPGPaymentOptionDTO setIsDefault(String isDefault) {
		this.isDefault = isDefault;
		return this;
	}

	public IPGPaymentOptionDTO setIsModificationAllow(String isModificationAllow) {
		this.isModificationAllow = isModificationAllow;
		return this;
	}

	public IPGPaymentOptionDTO setOfflinePayment(boolean offlinePayment) {
		this.offlinePayment = offlinePayment;
		return this;
	}

	public IPGPaymentOptionDTO setFirstSegmentONDCode(String firstSegmentONDCode) {
		this.firstSegmentONDCode = firstSegmentONDCode;
		return this;
	}

	public boolean isSaveCard() {
		return saveCard;
	}

	public void setSaveCard(boolean saveCard) {
		this.saveCard = saveCard;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public BigDecimal getAliasBasePaymentAmount() {
		return aliasBasePaymentAmount;
	}

	public void setAliasBasePaymentAmount(BigDecimal aliasBasePaymentAmount) {
		this.aliasBasePaymentAmount = aliasBasePaymentAmount;
	}


}
