package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
public class CountryPaymentCardBehaviourDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int paymentGateWayID;

	private int cardType;

	private String behaviour;

	private String cardDisplayName;

	private String cssClassName;

	private String i18nMsgKey;

	private String status;

	private boolean isModicifationAllowed;

	private boolean isWithinOnholdPeriod;

	public int getPaymentGateWayID() {
		return paymentGateWayID;
	}

	public void setPaymentGateWayID(int paymentGateWayID) {
		this.paymentGateWayID = paymentGateWayID;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public String getBehaviour() {
		return behaviour;
	}

	public void setBehaviour(String behaviour) {
		this.behaviour = behaviour;
	}

	public String getCardDisplayName() {
		return cardDisplayName;
	}

	public void setCardDisplayName(String cardDisplayName) {
		this.cardDisplayName = cardDisplayName;
	}

	public String getCssClassName() {
		return cssClassName;
	}

	public void setCssClassName(String cssClassName) {
		this.cssClassName = cssClassName;
	}

	public String getI18nMsgKey() {
		return i18nMsgKey;
	}

	public void setI18nMsgKey(String i18nMsgKey) {
		this.i18nMsgKey = i18nMsgKey;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isModicifationAllowed() {
		return isModicifationAllowed;
	}

	public void setModicifationAllowed(boolean isModicifationAllowed) {
		this.isModicifationAllowed = isModicifationAllowed;
	}

	public boolean isWithinOnholdPeriod() {
		return isWithinOnholdPeriod;
	}

	public void setWithinOnholdPeriod(boolean isWithinOnholdPeriod) {
		this.isWithinOnholdPeriod = isWithinOnholdPeriod;
	}
}
