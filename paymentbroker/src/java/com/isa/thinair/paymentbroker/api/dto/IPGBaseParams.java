package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

public abstract class IPGBaseParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1431668385064907251L;
	private IPGIdentificationParamsDTO identificationParams;

	public IPGBaseParams(IPGIdentificationParamsDTO identificationParams) {
		this.identificationParams = identificationParams;
	}

	public IPGIdentificationParamsDTO getIdentificationParams() {
		return identificationParams;
	}

	public void setIdentificationParams(IPGIdentificationParamsDTO identificationParams) {
		this.identificationParams = identificationParams;
	}
}
