package com.isa.thinair.paymentbroker.api.dto.ameriabank;

import java.io.Serializable;

public class AmeriaBankCommonResponse extends AmeriaBankBaseResponse implements Serializable {

	private static final long serialVersionUID = -190909098464589L;

	private String paymentId;
	private String opaque;

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getOpaque() {
		return opaque;
	}

	public void setOpaque(String opaque) {
		this.opaque = opaque;
	}

	@Override
	public String toString() {
		StringBuilder resBuff = new StringBuilder();
		resBuff.append(super.toString());
		resBuff.append(", paymentId: " + this.paymentId);
		resBuff.append(", opaque: " + this.opaque);

		return resBuff.toString();
	}

}
