package com.isa.thinair.paymentbroker.api.dto.tap;

import java.io.Serializable;

public class TapCommonResponse implements Serializable {

	private static final long serialVersionUID = -525439478944591L;
	
	private String responseCode;
	private String responseMessage;
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
}
