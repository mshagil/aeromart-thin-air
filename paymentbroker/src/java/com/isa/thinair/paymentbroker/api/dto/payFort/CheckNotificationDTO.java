package com.isa.thinair.paymentbroker.api.dto.payFort;

import java.io.Serializable;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

public class CheckNotificationDTO implements Serializable  {
	
	// later implments with a common class for Requests
	private static final long serialVersionUID = -8393939923121544L;
	
	public static final String NO_VALUE = "";
	public static final String VALID_HASH = "VALID HASH.";
	public static final String INVALID_HASH = "INVALID HASH.";
	public static final String INVALID_RESPONSE = "INVALID RESPONSE";
	
	private String merchantID;
	
	private String orderID;
	
	private String payment;
	
	private String currency;
	
	private String status;
	
	private String ticketNumber;
	
	private String serviceName;
	
	private String signature;
	
	private XMLGregorianCalendar msgDate;
	
	private Map<String , String> request;

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public XMLGregorianCalendar getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(XMLGregorianCalendar msgDate) {
		this.msgDate = msgDate;
	}
	
	public void setRequest(Map request) {
		
		this.request = request;
		this.merchantID = null2unknown((String) request.get("merchantID"));
		this.orderID = null2unknown((String) request.get("orderID"));
		this.payment = null2unknown((String) request.get("payment"));
		this.currency = null2unknown((String) request.get("currency"));
		this.status = null2unknown((String) request.get("status"));
		this.ticketNumber = null2unknown((String) request.get("ticketNumber"));
		this.serviceName = null2unknown((String) request.get("serviceName"));

	}
	
	public void setReassignRequest(Map request) {

		this.request = request;
		this.merchantID = null2unknown((String) request.get("merchantID"));
		this.orderID = null2unknown((String) request.get("orderID"));
		this.payment = "PAYatHOME";
		this.currency = null2unknown((String) request.get("currency"));
		this.status = null2unknown((String) request.get("status"));
		this.serviceName = null2unknown((String) request.get("serviceName"));

	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

}
