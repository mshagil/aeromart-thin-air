/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.api.util;

import java.io.Serializable;

/**
 * @author kasun
 * 
 */
public class TnxModeEnum implements Serializable {

	private static final long serialVersionUID = -924086917773647187L;

	private int code;

	private TnxModeEnum(int code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public boolean equals(TnxModeEnum allocateEnum) {
		return (allocateEnum.getCode() == this.getCode());
	}

	public int getCode() {
		return code;
	}

	public static final TnxModeEnum MAIL_TP_ORDER = new TnxModeEnum(1);

	public static final TnxModeEnum SECURE_3D = new TnxModeEnum(2);

}
