package com.isa.thinair.paymentbroker.api.dto.saman;

import java.io.Serializable;

public class SamanResponseDTO implements Serializable {

	private static final long serialVersionUID = -8303009098978344L;

	private String referenceNo;

	private Double verifiactionResult;

	private Integer refundResult;

	private String refundResultDesc;

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public Double getVerifiactionResult() {
		return verifiactionResult;
	}

	public void setVerifiactionResult(Double verifiactionResult) {
		this.verifiactionResult = verifiactionResult;
	}

	public Integer getRefundResult() {
		return refundResult;
	}

	public void setRefundResult(Integer refundResult) {
		this.refundResult = refundResult;
	}

	public String getRefundResultDesc() {
		return refundResultDesc;
	}

	public void setRefundResultDesc(String refundResultDesc) {
		this.refundResultDesc = refundResultDesc;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("referenceNo:" + referenceNo).append(",verificationResult:" + verifiactionResult)
				.append(",refundResult:" + refundResult).append(",refundResultDesc:" + refundResultDesc);

		return builder.toString();
	}

}
