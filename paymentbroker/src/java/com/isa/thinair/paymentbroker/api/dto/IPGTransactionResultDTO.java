package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;
/**
 * Track the transaction details. 
 * 
 * @author Pradeep Karunanayake
 *
 */
public class IPGTransactionResultDTO  implements Serializable {
	
	private static final long serialVersionUID = 6485203184774713108L;
	
	private boolean showTransactionDetail;
	
	private String transactionTime;
	
	private String transactionStatus;
	
	private String amount;
	
	private String currency;
	
	private String referenceID;
	
	private String merchantTrackID;
	

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}

	public String getMerchantTrackID() {
		return merchantTrackID;
	}

	public void setMerchantTrackID(String merchantTrackID) {
		this.merchantTrackID = merchantTrackID;
	}

	public boolean isShowTransactionDetail() {
		return showTransactionDetail;
	}

	public void setShowTransactionDetail(boolean showTransactionDetail) {
		this.showTransactionDetail = showTransactionDetail;
	}	
	
}
