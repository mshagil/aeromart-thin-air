package com.isa.thinair.paymentbroker.api.dto.ameriabank;

import java.io.Serializable;

public class AmeriaBankPaymentIdRequest extends AmeriaBankCommonRequest implements Serializable {

	private static final long serialVersionUID = -190909098464586L;

	private String backURL;
	private String description;
	private String currency;
	private String userID; // for user binding cases
	private String opaque;

	public String getBackURL() {
		return backURL;
	}

	public void setBackURL(String backURL) {
		this.backURL = backURL;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getOpaque() {
		return opaque;
	}

	public void setOpaque(String opaque) {
		this.opaque = opaque;
	}

	@Override
	public String toString() {
		StringBuilder resBuff = new StringBuilder();
		resBuff.append(super.toString());
		resBuff.append("orderId: " + getOrderId());
		resBuff.append(", backURL: " + this.backURL);
		resBuff.append("description: " + this.description);
		resBuff.append(", currency: " + this.currency);
		resBuff.append("userID: " + this.userID);
		resBuff.append(", opaque: " + this.opaque);

		return resBuff.toString();
	}

}
