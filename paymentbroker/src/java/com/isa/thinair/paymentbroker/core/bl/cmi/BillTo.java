package com.isa.thinair.paymentbroker.core.bl.cmi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class BillTo {

	@XmlElement
	private String Name;
	
	@XmlElement
	private String Street1;
	
	@XmlElement
	private String Street2;
	
	@XmlElement
	private String City;
	
	@XmlElement
	private String StateProv;
	
	@XmlElement
	private String Country;
	
	@XmlElement
	private String TelVoice;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getStreet1() {
		return Street1;
	}

	public void setStreet1(String street1) {
		Street1 = street1;
	}

	public String getStreet2() {
		return Street2;
	}

	public void setStreet2(String street2) {
		Street2 = street2;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getStateProv() {
		return StateProv;
	}

	public void setStateProv(String stateProv) {
		StateProv = stateProv;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getTelVoice() {
		return TelVoice;
	}

	public void setTelVoice(String telVoice) {
		TelVoice = telVoice;
	}




}
