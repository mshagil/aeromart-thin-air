package com.isa.thinair.paymentbroker.core.bl.unionpay;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Currency;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.unionpay.acp.sdk.SDKConfig;
import com.unionpay.acp.sdk.SDKUtil;

public class PaymentBrokerUnionPayECommerceImpl extends PaymentBrokerUnionPayTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerUnionPayECommerceImpl.class);

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		String updatedMerchantTxnId = merchantTxnId + PaymentConstants.MODE_OF_SERVICE.PAYMENT;

		Map<String, String> postDataMap = getPurchaseDataMap(ipgRequestDTO, updatedMerchantTxnId);

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();		

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), updatedMerchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_CAPTURE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		String ipgURL = getIpgURL() + getFrontEndTNXURLSuffix();

		String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, ipgURL);
		
		if (log.isDebugEnabled()) {
			log.debug("Union-Pay Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}
		
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(updatedMerchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}

	private Map<String, String> getPurchaseDataMap(IPGRequestDTO ipgRequestDTO, String merchantTxnId) {
		String finalAmount = PaymentBrokerUtils.getFormattedAmount(ipgRequestDTO.getAmount(), ipgRequestDTO.getNoOfDecimals());

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(UnionPayConstants.VERSION, getVersion());
		postDataMap.put(UnionPayConstants.ENCODING, "UTF-8");

		postDataMap.put(UnionPayConstants.SIGN_METHOD, UnionPayConstants.SignatureMethod.RSA);
		postDataMap.put(UnionPayConstants.TXN_TYPE, UnionPayConstants.TxnType.PURCHASE);
		postDataMap.put(UnionPayConstants.TXN_SUB_TYPE, UnionPayConstants.TxnSubType.PURCHASE);
		postDataMap.put(UnionPayConstants.PRODUCT_TYPE, UnionPayConstants.ProductType.SECURE_PAY);
		postDataMap.put(UnionPayConstants.ACCESS_TYPE, UnionPayConstants.AccessType.MERCAHNT_DIRECT);
		postDataMap.put(UnionPayConstants.CHANNEL_TYPE, UnionPayConstants.ChannelType.INTERNET);
		postDataMap.put(UnionPayConstants.FRONT_END_URL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(UnionPayConstants.BACK_END_URL, ipgRequestDTO.getReturnUrl());/// prefix "ZX|" before
																						/// notification address
		postDataMap.put(UnionPayConstants.ACQ_INST_CODE, getAquirerId());
		postDataMap.put(UnionPayConstants.MERCHANT_ID, getMerchantId());
		postDataMap.put(UnionPayConstants.MERCHANT_TYPE, getMerchantCategory());
		postDataMap.put(UnionPayConstants.MERCHANT_NAME, getMerchantName());
		postDataMap.put(UnionPayConstants.MERCHANT_ABBR, getMerchantAbbriviation());
		postDataMap.put(UnionPayConstants.ORDER_ID, merchantTxnId);
		postDataMap.put(UnionPayConstants.TXN_AMOUNT, finalAmount);

		Currency currency = Currency.getInstance("USD");
		postDataMap.put(UnionPayConstants.TXN_CURRENCY, String.valueOf(currency.getNumericCode()));
		postDataMap.put(UnionPayConstants.ERROR_REDIRECT_URL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(UnionPayConstants.ORDER_TIMEOUT, getTimeout());

		postDataMap.put(UnionPayConstants.TXN_TIME, getUnionPayDefaultDateFormat().format(new Date()));

		// Sign data map
		SDKUtil.signByCertInfo(postDataMap, "UTF-8", getSecureKeyLocation(), getKeyStorePassword());

		return postDataMap;
	}

	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerUnionPayECommerceImpl::getReponseData()] Begin ");

		String errorCode = "";
		int tnxResultCode = 0;
		String errorSpecification = "";
		String transactionId = null;

		int cardType = 5; // default card type
		String responseString = PaymentBrokerUtils.getRequestDataAsString(fields);
		SDKConfig.getConfig().loadPropertiesFromPath(getConfigPropertiesLocation());
		fields.remove(PaymentConstants.IPG_SESSION_ID);
		fields.put(UnionPayConstants.SIGNATURE, ((String) fields.get(UnionPayConstants.SIGNATURE)).replace(" ", "+"));
		boolean validSignature = SDKUtil.validate(fields, "UTF-8");

		UnionPayResponse unionPayResponse = new UnionPayResponse();
		unionPayResponse.setReponse(fields);

		CreditCardTransaction unionpayCreditCardTransaction = updateCCTransaction(ipgResponseDTO, unionPayResponse);

		log.debug("[PaymentBrokerUnionPayECommerceImpl::getReponseData()] Mid Response -" + unionPayResponse);

		ServiceResponce response;
		String status;
		boolean errorExists = false;
		if (unionpayCreditCardTransaction.getTransactionId().equalsIgnoreCase(unionPayResponse.getOrderId()) && validSignature) {
			if (unionPayResponse.getResponseCode().equals(UnionPayConstants.ResponseCode.SUCCESS)) {
				errorExists = false;
			} else {
				setPaymentQueryDR(new PaymentBrokerUnionPayQueryDR());
				PaymentQueryDR paymentQueryDR = getPaymentQueryDR();
				response = paymentQueryDR.query(unionpayCreditCardTransaction, null,
						PaymentBrokerInternalConstants.QUERY_CALLER.VERIFY);
				if (response.isSuccess()) {
					errorExists = false;
				} else {
					errorSpecification = "" + response.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
					errorExists = true;
				}
			}
		} else {
			log.error("[PaymentBrokerUnionPayECommerceImpl::getReponseData()] Error");
			errorExists = true;
		}

		if (!errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			transactionId = unionPayResponse.getQueryId();
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(unionPayResponse.getQueryId());
		ipgResponseDTO.setCardType(cardType);
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), responseString, errorSpecification,
				transactionId, transactionId, tnxResultCode);

		log.debug("[PaymentBrokerUnionPayECommerceImpl::getReponseData()] End -");

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REFUND_OPERATION_NOT_SUPPORTED);
			return defaultServiceResponse;
		} else {
			int tnxResultCode;
			String errorCode;
			String transactionMsg;
			String status;
			String errorSpecification;
			String merchTnxRef;

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO()
					.loadTransaction(creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			String amount = PaymentBrokerUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());

			PaymentQueryDR queryDR = getPaymentQueryDR();
			ServiceResponce serviceResponce = queryDR.query(ccTransaction, null,
					PaymentBrokerInternalConstants.QUERY_CALLER.REFUND);

			if (creditCardPayment.getPreviousCCPayment() != null && serviceResponce.isSuccess()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

				ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
				if (!serviceResponse.isSuccess()) {
					return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
							ccTransaction.getPaymentGatewayConfigurationName());
				}
				merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
						PaymentConstants.MODE_OF_SERVICE.PAYMENT);
				
				String refundMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;
				
				Map<String, String> postDataMap = getRefundPostMap(refundMerchTnxRef, amount,
						(String) serviceResponce.getResponseParam("queryId"));

				String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

				CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), refundMerchTnxRef,
						creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), strRequestParams, "",
						getPaymentGatewayName(), null, false);


				Integer paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

				UnionPayResponse response = UnionPayPaymentUtils.getResponse(postDataMap, getConfigPropertiesLocation());

				log.debug("Refund Response : " + response.toString());

				DefaultServiceResponse sr = new DefaultServiceResponse(false);

				if (UnionPayConstants.ResponseCode.SUCCESS.equals(response.getResponseCode())) {
					errorCode = "";
					transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
					errorSpecification = "";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
				} else {
					errorCode = "";
					// errorCode = UnionpayPaymentUtils.getErrorInfo(unionPayRefundResponse);
					transactionMsg = response.getResponseMsg();
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				}

				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(),
						PaymentBrokerUtils.getRequestDataAsString(response.getResponse()), errorSpecification,
						response.getResponseCode(), response.getOrigQueryId() + ":" + response.getQueryId(), tnxResultCode);

				sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getQueryId());
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
				return sr;
			} else {
				DefaultServiceResponse sr = new DefaultServiceResponse(false);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

				return sr;
			}
		}
	}


	private Map<String, String> getRefundPostMap(String merchTnxRef, String amount, String origQueryId) {
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(UnionPayConstants.VERSION, getVersion());
		postDataMap.put(UnionPayConstants.ENCODING, "UTF-8");

		postDataMap.put(UnionPayConstants.SIGN_METHOD, UnionPayConstants.SignatureMethod.RSA);
		postDataMap.put(UnionPayConstants.TXN_TYPE, UnionPayConstants.TxnType.PURCHASE_REFUND);
		postDataMap.put(UnionPayConstants.TXN_SUB_TYPE, UnionPayConstants.TxnSubType.DEFAULT);
		postDataMap.put(UnionPayConstants.PRODUCT_TYPE, UnionPayConstants.ProductType.SECURE_PAY);
		postDataMap.put(UnionPayConstants.ACCESS_TYPE, UnionPayConstants.AccessType.MERCAHNT_DIRECT);
		postDataMap.put(UnionPayConstants.CHANNEL_TYPE, UnionPayConstants.ChannelType.INTERNET);

		String responseUrl = "ZX|" + AppSysParamsUtil.getSecureServiceAppIBEUrl();// 10.20.11.28:8180/service-app

		postDataMap.put(UnionPayConstants.BACK_END_URL, responseUrl);/// prefix "ZX|"
		postDataMap.put(UnionPayConstants.ACQ_INST_CODE, getAquirerId());
		postDataMap.put(UnionPayConstants.MERCHANT_ID, getMerchantId());
		postDataMap.put(UnionPayConstants.MERCHANT_TYPE, getMerchantCategory());
		postDataMap.put(UnionPayConstants.MERCHANT_NAME, getMerchantName());
		postDataMap.put(UnionPayConstants.MERCHANT_ABBR, getMerchantAbbriviation());
		postDataMap.put(UnionPayConstants.ORDER_ID, merchTnxRef);
		postDataMap.put(UnionPayConstants.TXN_AMOUNT, amount);
		postDataMap.put(UnionPayConstants.ORIG_TNX_QUERY_ID, origQueryId);

		postDataMap.put(UnionPayConstants.TXN_TIME, getUnionPayDefaultDateFormat().format(new Date()));

		// Sign data map
		SDKUtil.signByCertInfo(postDataMap, "UTF-8", getSecureKeyLocation(), getKeyStorePassword());

		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());

		String ipgURL = getIpgURL() + getBackEndTNXURLSuffix();
		postDataMap.put("URL", ipgURL);
		return postDataMap;
	}

	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerUnionPayECommerceImpl::reverse()] Begin ");
		int tnxResultCode = -20;
		String errorCode = "";
		String transactionMsg;
		String merchTnxRef;
		String errorSpecification = "";
		Integer paymentBrokerRefNo;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO()
				.loadTransaction(creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		// TODO: when refund is a partial refund then refund amount should be round-down all the other cases
		// should be round-up
		String refundAmountStr = PaymentBrokerUtils.getFormattedAmount(creditCardPayment.getAmount(),
				AccelAeroCalculator.getNoOfDecimals(new BigDecimal(creditCardPayment.getAmount())));

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		PaymentQueryDR oNewQuery = getPaymentQueryDR();

		ServiceResponce serviceResponce = oNewQuery.query(ccTransaction, null,
				PaymentBrokerInternalConstants.QUERY_CALLER.CANCEL);

		if (serviceResponce.isSuccess()) {
			String responseStr = (String) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);
			if ("".equals(responseStr)) {
				log.info("UnionPay Inquery Request Fail For Reverse  ");
				transactionMsg = "UnionPay Inquery Request No Error code Returned";
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

			} else if (UnionPayConstants.ResponseCode.SUCCESS.equals(responseStr)) {
				merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
						PaymentConstants.MODE_OF_SERVICE.PAYMENT);

				String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.CANCEL;
				String updatedResponseStr = "";

				Map<String, String> cancelPostMap = getReversePaymentMap(updatedMerchTnxRef, refundAmountStr,
						ccTransaction.getTransactionId());

				CreditCardTransaction ccNewTransaction = auditTransaction(ccTransaction.getTransactionReference(),
						getMerchantId(), updatedMerchTnxRef, creditCardPayment.getTemporyPaymentId(),
						bundle.getString("SERVICETYPE_REVERSAL"), PaymentBrokerUtils.getRequestDataAsString(cancelPostMap), "",
						getPaymentGatewayName(), null, false);

				UnionPayResponse reverseResponse = UnionPayPaymentUtils.getResponse(cancelPostMap, getConfigPropertiesLocation());

				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

				if (reverseResponse != null && reverseResponse.getResponseCode() != null) {
					if (UnionPayConstants.ResponseCode.SUCCESS.equals(reverseResponse.getResponseCode())) {
						errorCode = "";
						tnxResultCode = 1;
						// errorSpecification = UnionPayPaymentUtils.getErrorDescription(refundResponse.getResponse());
						sr.setSuccess(true);
						sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
								PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
					} else {
						transactionMsg = "error";
						tnxResultCode = 0;
						// errorSpecification = UnionPayPaymentUtils.getErrorDescription(refundResponse.getResponse());
						sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
								PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
					}
				} else {
					transactionMsg = "error";
					tnxResultCode = 0;
					errorSpecification = "UnionPay Reverse Request Failed";
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
				}

				updatedResponseStr = PaymentBrokerUtils.getRequestDataAsString(reverseResponse.getResponse()) + ","
						+ errorSpecification;
				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), updatedResponseStr, errorSpecification,
						cancelPostMap.get(UnionPayConstants.ORDER_ID), cancelPostMap.get(UnionPayConstants.ORDER_ID),
						tnxResultCode);

			}

			// else if (UnionPayPaymentUtils.TRANSACTION_SETTLED.equals(responseStr)) {
			// sr = (DefaultServiceResponse) this.refund(creditCardPayment, pnr, appIndicator, tnxMode);
			// }
		}

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			sr.setResponseCode(String.valueOf(ccTransaction.getTransactionReference()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}

		log.debug("[PaymentBrokerUnionPayECommerceImpl::reverse()] End");
		return sr;

	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		log.info("[PaymentBrokerUnionPayECommerceImpl::resolvePartialPayments()] Begin");

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;
		PaymentQueryDR query;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();
			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());

			try {

				ServiceResponce serviceResponce = getPaymentQueryDR().query(oCreditCardTransaction,
						UnionPayPaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					String responseStr = (String) serviceResponce
							.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);

					BigDecimal paymentAmount = ReservationModuleUtils.getReservationBD()
							.getTempPaymentAmount(oCreditCardTransaction.getTemporyPaymentId());

					if (UnionPayConstants.ResponseCode.SUCCESS.equals(responseStr)) {

						BigDecimal receivedPayAmount = new BigDecimal(
								(String) serviceResponce.getResponseParam(PaymentConstants.AMOUNT));

						receivedPayAmount = AccelAeroCalculator.divide(receivedPayAmount, 100);

						log.debug("[PaymentBrokerUnionPayECommerceImpl::getReponseData()] - Payment Amount-" + paymentAmount
								+ " receivedPayAmount-" + receivedPayAmount);

						ipgQueryDTO.setBaseAmount(receivedPayAmount);
						boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);

						if (isResConfirmationSuccess) {
							if (log.isDebugEnabled()) {
								log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
							}

						} else {
							if (refundFlag) {
								// Do refund
								String txnNo = PlatformUtiltiies
										.nullHandler(serviceResponce.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

								oPrevCCPayment = new PreviousCreditCardPayment();
								oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
								oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

								oCCPayment = new CreditCardPayment();
								oCCPayment.setPreviousCCPayment(oPrevCCPayment);
								oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
								oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
								oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
								oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
								oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
								oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

								ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
										oCCPayment.getTnxMode());

								if (srRev.isSuccess()) {
									// Change State to 'IS'
									oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
									log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
								} else {
									// Change State to 'IF'
									oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
									log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							} else {
								// Change State to 'IP'
								oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
								log.info("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
						
					} else {
						// No Payments exist
						// Update status to 'II'
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				} else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}

			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerUnionPayECommerceImpl::resolvePartialPayments()] End");
		return sr;
	}

	private Map<String, String> getReversePaymentMap(String merchTnxRef, String amount, String origQueryId) {

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(UnionPayConstants.VERSION, getVersion());
		postDataMap.put(UnionPayConstants.ENCODING, "UTF-8");

		postDataMap.put(UnionPayConstants.SIGN_METHOD, UnionPayConstants.SignatureMethod.RSA);
		postDataMap.put(UnionPayConstants.TXN_TYPE, UnionPayConstants.TxnType.PURCHASE_CANCEL);
		postDataMap.put(UnionPayConstants.TXN_SUB_TYPE, UnionPayConstants.TxnSubType.DEFAULT);
		postDataMap.put(UnionPayConstants.PRODUCT_TYPE, UnionPayConstants.ProductType.SECURE_PAY);
		postDataMap.put(UnionPayConstants.ACCESS_TYPE, UnionPayConstants.AccessType.MERCAHNT_DIRECT);
		postDataMap.put(UnionPayConstants.CHANNEL_TYPE, UnionPayConstants.ChannelType.INTERNET);

		String responseUrl = "ZX|" + AppSysParamsUtil.getSecureServiceAppIBEUrl();// 10.20.11.28:8180/service-app

		postDataMap.put(UnionPayConstants.BACK_END_URL, responseUrl);/// prefix "ZX|"
		postDataMap.put(UnionPayConstants.ACQ_INST_CODE, getAquirerId());
		postDataMap.put(UnionPayConstants.MERCHANT_ID, getMerchantId());
		postDataMap.put(UnionPayConstants.MERCHANT_TYPE, getMerchantCategory());
		postDataMap.put(UnionPayConstants.MERCHANT_NAME, getMerchantName());
		postDataMap.put(UnionPayConstants.MERCHANT_ABBR, getMerchantAbbriviation());
		postDataMap.put(UnionPayConstants.ORDER_ID, merchTnxRef);
		postDataMap.put(UnionPayConstants.ORIG_TNX_QUERY_ID, origQueryId);

		postDataMap.put(UnionPayConstants.TXN_AMOUNT, amount);

		postDataMap.put(UnionPayConstants.TXN_TIME, getUnionPayDefaultDateFormat().format(new Date()));

		// Sign data map
		SDKUtil.signByCertInfo(postDataMap, "UTF-8", getSecureKeyLocation(), getKeyStorePassword());

		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());

		String ipgURL = getIpgURL() + getBackEndTNXURLSuffix();
		postDataMap.put("URL", ipgURL);
		return postDataMap;
	}

	private CreditCardTransaction updateCCTransaction(IPGResponseDTO ipgResponseDTO, UnionPayResponse unionPayResponse) {
		CreditCardTransaction unionpayCreditCardTransaction = loadAuditTransactionByTnxRefNo(
				ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		unionpayCreditCardTransaction.setComments(strResponseTime);
		unionpayCreditCardTransaction.setResponseTime(timeDiffInMillis);
		unionpayCreditCardTransaction.setTransactionId(unionPayResponse.getOrderId());
		unionpayCreditCardTransaction.setAidCccompnay(unionPayResponse.getQueryId());
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(unionpayCreditCardTransaction);
		return unionpayCreditCardTransaction;
	}
	
}
