package com.isa.thinair.paymentbroker.core.bl.test;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerTestPG extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerTestPG.class);
	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		if (!AppSysParamsUtil.isTestStstem()) {
			throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
		}

		String finalAmount = PaymentBrokerUtils.getFormattedAmount(ipgRequestDTO.getAmount(), ipgRequestDTO.getNoOfDecimals());

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		merchantTxnId += "_testPG";

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		String requestDataForm = "";

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				"dummystringvalues" + "," + sessionID, "", getPaymentGatewayName(), null, false);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put("AMOUNT", finalAmount);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerOgoneImpl::getReponseData()] Begin ");

		if (!AppSysParamsUtil.isTestStstem()) {
			throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
		}

		int tnxResultCode = 1;
		String errorSpecification = "";
		String merchantTxnReference;
		String authorizeID = "accepted";
		int cardType = 1; // default card type

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());
		merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		ipgResponseDTO.setStatus("ACCEPTED");
		ipgResponseDTO.setErrorCode("");
		ipgResponseDTO.setMessage("");
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authorizeID);
		ipgResponseDTO.setCardType(cardType);

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), "TEST PG Response", errorSpecification, "1",
				"32410", tnxResultCode);

		return ipgResponseDTO;
	}

	@Override
	public Properties getProperties() {

		Properties props = super.getProperties();
		props.put("xmlResponse", "true");
		return props;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {

		if (!AppSysParamsUtil.isTestStstem()) {
			throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
		}

		XMLResponseDTO response = new XMLResponseDTO();
		response.setStatus("9");
		return response;

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (!AppSysParamsUtil.isTestStstem()) {
			throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
		}

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
