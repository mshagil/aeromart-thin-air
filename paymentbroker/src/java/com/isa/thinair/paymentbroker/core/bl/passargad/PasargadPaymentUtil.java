package com.isa.thinair.paymentbroker.core.bl.passargad;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.platform.api.constants.PlatformConstants;

public class PasargadPaymentUtil {

	private static Log log = LogFactory.getLog(PasargadPaymentUtil.class);

	public static String byteArrayToHexString(byte[] bytes) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			if (((int) bytes[i] & 0xff) < 0x10)
				buffer.append("0");
			buffer.append(Long.toString((int) bytes[i] & 0xff, 16));
		}
		return buffer.toString();
	}

	public static String getEncodedSign(Map<String, String> postDataMap) {
		String sign = generateSign(postDataMap);
		log.debug("Sign is : " + sign);

		String encodedSign = null;
		PrivateKey privKey = generatePrivateKey(postDataMap.get(PasargadRequest.KEYSTORE_LOCATION));
		try {
			encodedSign = encryptData(sign, privKey);
		} catch (IOException e) {
			log.error("Data Encryption Faild : ", e);
		}
		return encodedSign;
	}

	public static String generateFormRequest(Map<String, String> postDataMap) {
		String encodedSign = getEncodedSign(postDataMap);

		StringBuilder sb = new StringBuilder();
		sb.append("<form action='" + postDataMap.get(PasargadRequest.IPG_URL)
				+ "' method='post' id='pgwForm' enctype='application/x-www-form-urlencoded'> ");
		sb.append("<input type='hidden' name='merchantCode' value='" + postDataMap.get(PasargadRequest.MERCHANT_CODE) + "' /> ");
		sb.append("<input type='hidden' name='terminalCode' value='" + postDataMap.get(PasargadRequest.TERMINAL_CODE) + "' /> ");
		sb.append("<input type='hidden' name='amount' value='" + postDataMap.get(PasargadRequest.AMOUNT) + "' /> ");
		sb.append("<input type='hidden' name='redirectAddress' value='" + postDataMap.get(PasargadRequest.REDIRECT_URL) + "' /> ");
		sb.append("<input type='hidden' name='timeStamp' value='" + postDataMap.get(PasargadRequest.TIMESTAMP) + "' /> ");
		sb.append("<input type='hidden' name='invoiceNumber' value='" + postDataMap.get(PasargadRequest.INVOICE_NUMBER) + "' /> ");
		sb.append("<input type='hidden' name='invoiceDate' value='" + postDataMap.get(PasargadRequest.INVOICE_DATE) + "' /> ");
		sb.append("<input type='hidden' name='action' value='" + postDataMap.get(PasargadRequest.ACTION) + "' /> ");
		sb.append("<input type='hidden' name='sign' value='" + encodedSign + "' /> ");
		sb.append("<script type='text/javascript'> document.getElementById('pgwForm').submit();" + "</script>");
		sb.append("</form>");

		log.debug("Payment Gateway Request - Auto submit form : " + sb.toString());
		return sb.toString();
	}

	private static String generateSign(Map<String, String> postDataMap) {
		String sign = "#";
		if (postDataMap.get(PasargadRequest.MERCHANT_CODE) != null) {
			sign += postDataMap.get(PasargadRequest.MERCHANT_CODE) + "#";
		}
		if (postDataMap.get(PasargadRequest.TERMINAL_CODE) != null) {
			sign += postDataMap.get(PasargadRequest.TERMINAL_CODE) + "#";
		}
		if (postDataMap.get(PasargadRequest.INVOICE_NUMBER) != null) {
			sign += postDataMap.get(PasargadRequest.INVOICE_NUMBER) + "#";
		}
		if (postDataMap.get(PasargadRequest.INVOICE_DATE) != null) {
			sign += postDataMap.get(PasargadRequest.INVOICE_DATE) + "#";
		}
		if (postDataMap.get(PasargadRequest.AMOUNT) != null) {
			sign += postDataMap.get(PasargadRequest.AMOUNT) + "#";
		}
		if (postDataMap.get(PasargadRequest.REDIRECT_URL) != null) {
			sign += postDataMap.get(PasargadRequest.REDIRECT_URL) + "#";
		}
		if (postDataMap.get(PasargadRequest.ACTION) != null) {
			sign += postDataMap.get(PasargadRequest.ACTION) + "#";
		}
		if (postDataMap.get(PasargadRequest.TIMESTAMP) != null) {
			sign += postDataMap.get(PasargadRequest.TIMESTAMP) + "#";
		}
		return sign;
	}

	private static PrivateKey generatePrivateKey(String keyStoreName) {
		RSAKeyValue keyVal = null;
		try {
			String keystoreLocation = PlatformConstants.getConfigRootAbsPath() + "/repository/modules/paymentbroker/"
					+ keyStoreName;
			File file = new File(keystoreLocation);
			JAXBContext jaxbContext = JAXBContext.newInstance(RSAKeyValue.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			keyVal = (RSAKeyValue) unmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			log.error("RSAKey Identification Faild : ", e);
		}

		BigInteger modulus = new BigInteger(1, Base64.decodeBase64(keyVal.getModulus()));
		BigInteger pubExp = new BigInteger(1, Base64.decodeBase64(keyVal.getExponent()));
		BigInteger priExp = new BigInteger(1, Base64.decodeBase64(keyVal.getD()));
		BigInteger pP = new BigInteger(1, Base64.decodeBase64(keyVal.getP()));
		BigInteger pQ = new BigInteger(1, Base64.decodeBase64(keyVal.getQ()));
		BigInteger pEP = new BigInteger(1, Base64.decodeBase64(keyVal.getDp()));
		BigInteger pEQ = new BigInteger(1, Base64.decodeBase64(keyVal.getDq()));
		BigInteger cC = new BigInteger(1, Base64.decodeBase64(keyVal.getInverseQ()));

		KeyFactory keyFactory;
		PrivateKey privKey = null;
		try {
			keyFactory = KeyFactory.getInstance("RSA");
			RSAPrivateCrtKeySpec rsaPrivateCrtKeyspc = new RSAPrivateCrtKeySpec(modulus, pubExp, priExp, pP, pQ, pEP, pEQ, cC);
			privKey = keyFactory.generatePrivate(rsaPrivateCrtKeyspc);

		} catch (NoSuchAlgorithmException e) {
			log.error("Private Key Generation Faild - NosuchAlgorithm : ", e);
		} catch (InvalidKeySpecException e) {
			log.error("Private Key Generation Faild - InvalidKey : ", e);
		}

		return privKey;
	}

	private static String encryptData(String data, PrivateKey privKey) throws IOException {
		log.debug("\n----------------ENCRYPTION STARTED------------");

		log.debug("Data Before Encryption :" + data);
		byte[] encryptedData = null;
		String encodedBytes = null;
		try {
			Signature sig = Signature.getInstance("SHA1withRSA", "SunRsaSign");
			sig.initSign(privKey);
			sig.update(data.getBytes("UTF-8"));
			encryptedData = sig.sign();

			encodedBytes = Base64.encodeBase64String(encryptedData);
			log.debug("Encryted Data: " + encodedBytes.toString());
		} catch (Exception e) {
			log.error("Data Encryption Failed : ", e);
		}

		log.debug("----------------ENCRYPTION COMPLETED------------");
		return encodedBytes;
	}

//	public static Object getPasargadXMLResponse(Map<String, String> postDataMap, Class clazz) {
//		
//		System.setProperty("javax.net.ssl.keyStore", postDataMap.get(PasargadRequest.KEYSTORE_ALIAS));			
//		System.setProperty("javax.net.ssl.trustStore", postDataMap.get(PasargadRequest.KEYSTORE_ALIAS));
//		HostConfiguration hcon = new HostConfiguration();
//		if (postDataMap.get("USE_PROXY").equals("Y")) {
//			hcon.setProxy(postDataMap.get("PROXY_HOST"), Integer.valueOf(postDataMap.get("PROXY_PORT")));
//		}
//
//		HttpClient client = new HttpClient();
//		client.setHostConfiguration(hcon);
//
//		String xmlResponseString = null;
//
//		PostMethod method = new PostMethod(postDataMap.get("URL"));
//
//		for (String key : postDataMap.keySet()) {
//
//			if (log.isDebugEnabled() && key.equalsIgnoreCase("PROXY_HOST") || key.equalsIgnoreCase("PROXY_PORT")
//					|| key.equalsIgnoreCase("URL") || key.equalsIgnoreCase("USE_PROXY")) {
//				log.debug("PASARGAD Proxy Details : " + key + " : " + postDataMap.get(key));
//			}
//
//			if (!key.equals("PROXY_HOST") && !key.equals("PROXY_PORT") && !key.equals("URL") && !key.equals("USE_PROXY")
//					&& !key.equals(PasargadRequest.KEYSTORE_ALIAS)) {
//				method.addParameter(key, postDataMap.get(key));
//			}
//		}
//		try {
//			log.debug("Executing the PASARGAD Request for URL : " + postDataMap.get("URL"));
//			int returnCode = client.executeMethod(method);
//
//			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
//				log.debug("The Post method is not implemented by this URI");
//				return null;
//			} else {
//				xmlResponseString = method.getResponseBodyAsString();
//				log.debug("XML Response for CheckTransaction : " + xmlResponseString);
//
//				return parseXML(xmlResponseString, clazz);
//			}
//		} catch (Exception e) {
//			log.debug("Exception while parsing the XML response " + e.getMessage(), e);
//		} finally {
//			method.releaseConnection();
//		}
//		return null;
//	}

	public static Object parseXML(String xmlResponseString, Class clazz) {

		JAXBContext jaxbContext;
		Object pasargadResponse = null;
		try {
			jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(xmlResponseString);

			pasargadResponse = clazz.cast(unmarshaller.unmarshal(reader));
		} catch (JAXBException e) {
			log.debug("ParseXML method fails for CheckTransactionResponse : " + e);
		}

		return pasargadResponse;
	}

	public static IPGConfigsDTO getIPGConfigs(String merchantID) {
		IPGConfigsDTO ipgConfigsDTO = new IPGConfigsDTO();
		ipgConfigsDTO.setMerchantID(merchantID);
		return ipgConfigsDTO;
	}

	public static Object getPasargadXMLResponse(Map<String, String> postDataMap, Class clazz) {
		String xmlResponseString = null;
		xmlResponseString = getPOSTMethodResponse(postDataMap);
		return parseXML(xmlResponseString, clazz);
	}
	
	private static String getPOSTMethodResponse(Map<String, String> postDataMap) {
		String urlParameters = postDataMap.get("REQUEST_DATA");
		byte[] postData = null;
		try {
			postData = urlParameters.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			log.debug("UTF Encodeing Not Supported: " + e1);
		}
		int postDataLength = postData.length;
		String response = "";
		URL url;
		try {
			url = new URL(postDataMap.get("URL"));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			if (postDataMap.get("USE_PROXY").equals("Y")) {
				System.setProperty("https.proxyHost", postDataMap.get("PROXY_HOST") );
				System.setProperty("https.proxyPort", postDataMap.get("PROXY_PORT"));
			}
			System.setProperty("javax.net.ssl.keyStore", postDataMap.get(PasargadRequest.KEYSTORE_ALIAS));			
			System.setProperty("javax.net.ssl.trustStore", postDataMap.get(PasargadRequest.KEYSTORE_ALIAS));
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			conn.setUseCaches(false);
			try {
				DataOutputStream wr = new DataOutputStream(conn.getOutputStream()) ;			
				wr.write(postData);
				Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
		        for (int c; (c = in.read()) >= 0;)
		            sb.append((char)c);
		        response = sb.toString();
			} catch (Exception e) {
				log.debug("HTTP POST Method error for Pasargad PGW: " + e);
			}			
		} catch (IOException e) {
			log.debug("HTTP POST Method error for Pasargad PGW: " + e);
		}	
		return response;
		
	}
}
