/*
 * Decompiled with CFR 0_102.
 */
package com.isa.thinair.paymentbroker.core.bl.benefit;

public class NotEnoughDataException
extends Exception {
    public NotEnoughDataException() {
    }

    public NotEnoughDataException(String msg) {
        super(msg);
    }
}
