package com.isa.thinair.paymentbroker.core.bl.cyberplus;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CyberPlusRequest {

	private static Log log = LogFactory.getLog(CyberPlusRequest.class);

	public static final String VADS_VERSION = "vads_version";

	public static final String VADS_PAGE_ACTION = "vads_page_action";

	public static final String VADS_SITE_ID = "vads_site_id";

	public static final String VADS_TRANS_ID = "vads_trans_id";

	public static final String VADS_AMOUNT = "vads_amount";

	public static final String VADS_URL_RETURN = "vads_url_return";

	public static final String VADS_ACTION_MODE = "vads_action_mode";

	public static final String VADS_CURRENCY = "vads_currency";

	public static final String VADS_CTX_MODE = "vads_ctx_mode";

	public static final String VADS_PAYMENT_CARDS = "vads_payment_cards";

	public static final String VADS_PAYMENT_CONFIG = "vads_payment_config";

	public static final String VADS_RETURN_MODE = "vads_return_mode";

	public static final String VADS_TRANS_DATE = "vads_trans_date";

	public static final String VADS_THREEDS_MPI = "vads_threeds_mpi";

	public static final String VADS_THREEDS_STATUS = "vads_threeds_status";

	public static final String VADS_USER_INFO = "vads_user_info";

	public static final String SIGNATURE = "signature";

	public static final String VADS_CARD_NUMBER = "vads_card_number";

	public static final String VADS_EXPIRY_MONTH = "vads_expiry_month";

	public static final String VADS_EXPIRY_YEAR = "vads_expiry_year";

	public static final String VADS_CVV = "vads_cvv";

	public static final String VADS_URL_CANCEL = "vads_url_cancel";

	public static final String VADS_URL_ERROR = "vads_url_error";

	public static final String VADS_URL_REFERRAL = "vads_url_referral";

	public static final String VADS_URL_REFUSED = "vads_url_refused";

	public static final String VADS_URL_SUCCESS = "vads_url_success";

	public static final String VADS_LANGUAGE = "vads_language";

	public static final String VADS_CUST_EMAIL = "vads_cust_email";

	public static final String VADS_ORDER_ID = "vads_order_id";

	public CyberPlusRequest() {
	}

}
