package com.isa.thinair.paymentbroker.core.config;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.paymentbroker.core.impl.config.PaymentBrokerImplConfig;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class PaymentBrokerModuleUtil {

	private static Log log = LogFactory.getLog(PaymentBrokerModuleUtil.class);

	private static final String PAYMENTBROKER_CONSTANTS_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/PaymentBrokerPro";

	private static final String MTC_PAYMENTBROKER_CONSTANTS_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/MTCPaymentBrokerPro";

	private static ResourceBundle bundle = null;

	private static ResourceBundle mtcBundle = null;

	public static PaymentBrokerConfig getConfig() {
		PaymentBrokerConfig config = (PaymentBrokerConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/paymentbroker?id=paymentbrokerModuleConfig");
		return config;
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	/**
	 * Load the payment broker related resource bundles
	 */
	private static synchronized void loadResourceBundle() {
		if (bundle == null) {
			bundle = ResourceBundle.getBundle(PAYMENTBROKER_CONSTANTS_RESOURCE_BUNDLE_NAME, Locale.US);
		}
	}

	private static synchronized void loadMTCResourceBundle() {
		if (mtcBundle == null) {
			mtcBundle = ResourceBundle.getBundle(MTC_PAYMENTBROKER_CONSTANTS_RESOURCE_BUNDLE_NAME, Locale.US);
		}
	}

	/**
	 * Load the payment broker related resource bundles
	 */
	public static ResourceBundle getResourceBundle() {
		if (bundle == null) {
			loadResourceBundle();
		}

		return bundle;

	}

	public static ResourceBundle getMTCResourceBundle() {
		if (mtcBundle == null) {
			loadMTCResourceBundle();
		}

		return mtcBundle;

	}

	/**
	 * Returns paymentbroker configurations for core implementations.
	 * 
	 * @return IConfig
	 */
	public static PaymentBrokerImplConfig getImplConfig() {
		return (PaymentBrokerImplConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/paymentbroker?id=paymentbrokerImplConfig");
	}
	
	/**
	 * Return reservation service delegate
	 * 
	 * @return
	 */
	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	
	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getConfig(), "paymentbroker",
				"webservices.config.dependencymap.invalid");
	}
	
}
