package com.isa.thinair.paymentbroker.core.bl.cyberplus;

import java.math.BigDecimal;
import java.util.ResourceBundle;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.CyberplusRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

/**
 * This class contain all the CyberPlus common methods. Used in MOTO implementation and Redirect implementation.
 */

public class PaymentBrokerCyberPlusTemplate extends PaymentBrokerTemplate {

	static Log log = LogFactory.getLog(PaymentBrokerCyberPlusTemplate.class);

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected static int STARTIGN_SEQ_NO = 1;

	protected static final int ERROR_CODE_RESULT_SUCCESS = 0;

	protected static final int CANCELLED = 9;

	protected static final int SUBMITTED = 6;

	protected static final int WAITING_FOR_SUBMISSION = 4;

	protected static final String DEFAULT_LANGUAGE = "en";

	private String actionMode;

	private String ctxMode;

	private String pageAction;

	private String paymentConfig;

	private String returnMode;

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	public ServiceResponce cancel(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
			log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
			log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			String[] merchTnxRefBreakDown = CyberPlusPaymentUtils.getBreakDown(merchTnxRef);
			AppIndicatorEnum appIndicatorEnum = SalesChannelsUtil.isAppIndicatorWebOrMobile(merchTnxRefBreakDown[0])
					? AppIndicatorEnum.APP_IBE
					: AppIndicatorEnum.APP_XBE;

			String updatedMerchTnxRef = composeAccelAeroTransactionRef(appIndicatorEnum, merchTnxRefBreakDown[1],
					merchTnxRefBreakDown[2], merchTnxRefBreakDown[3], PaymentConstants.MODE_OF_SERVICE.CANCEL);
			XMLGregorianCalendar transDate = CyberPlusPaymentUtils.getXMLGregorianCalFromDate(merchTnxRefBreakDown[2]);

			CyberplusRequestDTO requestDTO = new CyberplusRequestDTO();
			requestDTO.setShopId(getMerchantId());
			requestDTO.setTransmissionDate(transDate);
			requestDTO.setTransactionId(merchTnxRefBreakDown[3]);
			requestDTO.setSequenceNb(STARTIGN_SEQ_NO);
			requestDTO.setCtxMode(getCtxMode());
			requestDTO.setComment("");
			requestDTO.setProxyHost(getProxyHost());
			requestDTO.setProxyPort(getProxyPort());

			String signature = CyberPlusPaymentUtils.createSignature(getSecureSecret(), requestDTO.getShopId(),
					CyberPlusPaymentUtils.formatDate(merchTnxRefBreakDown[2], CyberPlusPaymentUtils.DATE_FORMAT),
					requestDTO.getTransactionId(), requestDTO.getSequenceNb(), requestDTO.getCtxMode(), requestDTO.getComment());

			requestDTO.setWsSignature(signature);

			CyberplusResponseDTO response = null;
			Integer paymentBrokerRefNo;

			CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef, new Integer(
					ccTransaction.getTemporyPaymentId()), bundle.getString("SERVICETYPE_REVERSAL"), requestDTO.toString(), "",
					getPaymentGatewayName(), null, false);

			// Sends the cancel request and gets the response

			try {
				// Cancel request.
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				response = ebiWebervices.cancel(requestDTO);

				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
			} catch (ModuleException e) {
				log.error(e);
				throw new ModuleException("paymentbroker.reverse.genericReverseError");
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Cancel Response : " + response.toString());

			DefaultServiceResponse sr = new DefaultServiceResponse(false);

			if (ERROR_CODE_RESULT_SUCCESS == response.getErrorCode() && CANCELLED == response.getTransactionStatus()) {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_CANCEL);
			} else {
				// TODO
				errorCode = CyberPlusPaymentUtils.API_ERROR_CODE_PREFIX + response.getErrorCode();
				transactionMsg = CyberPlusPaymentUtils.getWSAPIErrorDescription(response.getErrorCode(),
						response.getTransactionStatus());
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorSpecification,
					ccTransaction.getAidCccompnay(), ccTransaction.getTransactionId(), tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, ccTransaction.getAidCccompnay());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.setResponseCode("Failed");
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);

			return sr;
		}
	}

	public ServiceResponce modify(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
			log.debug("CC adjust Amount : " + creditCardPayment.getAmount());
			log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			merchTnxRef = ccTransaction.getTempReferenceNum();
			BigDecimal ajustedFinalAmount = creditCardPayment.getPreviousCCPayment().getTotalAmount()
					.subtract(new BigDecimal(creditCardPayment.getAmount()));
			String amount = CyberPlusPaymentUtils.getFormattedAmount(String.valueOf(ajustedFinalAmount.doubleValue()),
					creditCardPayment.getNoOfDecimalPoints());
			String payCurrencyNumericCode = getStandardCurrencyCode(creditCardPayment.getCurrency());
			String[] merchTnxRefBreakDown = CyberPlusPaymentUtils.getBreakDown(merchTnxRef);
			AppIndicatorEnum appIndicatorEnum = SalesChannelsUtil.isAppIndicatorWebOrMobile(merchTnxRefBreakDown[0])
					? AppIndicatorEnum.APP_IBE
					: AppIndicatorEnum.APP_XBE;

			String updatedMerchTnxRef = composeAccelAeroTransactionRef(appIndicatorEnum, merchTnxRefBreakDown[1],
					merchTnxRefBreakDown[2], merchTnxRefBreakDown[3], PaymentConstants.MODE_OF_SERVICE.REFUND);
			XMLGregorianCalendar transDate = CyberPlusPaymentUtils.getXMLGregorianCalFromDate(merchTnxRefBreakDown[2]);

			CyberplusRequestDTO requestDTO = new CyberplusRequestDTO();
			requestDTO.setShopId(getMerchantId());
			requestDTO.setTransmissionDate(transDate);
			requestDTO.setTransactionId(merchTnxRefBreakDown[3]);
			requestDTO.setSequenceNb(STARTIGN_SEQ_NO);
			requestDTO.setCtxMode(getCtxMode());
			requestDTO.setAmount(Long.valueOf(amount));
			requestDTO.setCurrency(Integer.valueOf(payCurrencyNumericCode));
			requestDTO.setPresentationDate(transDate);
			requestDTO.setComment("");
			requestDTO.setProxyHost(getProxyHost());
			requestDTO.setProxyPort(getProxyPort());

			String signature = CyberPlusPaymentUtils.createSignature(getSecureSecret(), requestDTO.getShopId(),
					CyberPlusPaymentUtils.formatDate(merchTnxRefBreakDown[2], CyberPlusPaymentUtils.DATE_FORMAT),
					requestDTO.getTransactionId(), requestDTO.getSequenceNb(), requestDTO.getCtxMode(), requestDTO.getAmount(),
					requestDTO.getCurrency(),
					CyberPlusPaymentUtils.formatDate(merchTnxRefBreakDown[2], CyberPlusPaymentUtils.DATE_FORMAT),
					requestDTO.getComment());

			requestDTO.setWsSignature(signature);

			CyberplusResponseDTO response = null;
			Integer paymentBrokerRefNo;

			CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef, new Integer(
					ccTransaction.getTemporyPaymentId()), bundle.getString("SERVICETYPE_REFUND"), requestDTO.toString(), "",
					getPaymentGatewayName(), null, false);

			// Sends the modify request and modify the initial transaction amount

			try {
				// Cancel request.
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				response = ebiWebervices.modify(requestDTO);

				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Modify Response : " + response.toString());

			DefaultServiceResponse sr = new DefaultServiceResponse(false);

			if (ERROR_CODE_RESULT_SUCCESS == response.getErrorCode() && WAITING_FOR_SUBMISSION == response.getTransactionStatus()) {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_CANCEL);
			} else {
				// TODO
				errorCode = CyberPlusPaymentUtils.API_ERROR_CODE_PREFIX + response.getErrorCode();
				transactionMsg = CyberPlusPaymentUtils.getWSAPIErrorDescription(response.getErrorCode(),
						response.getTransactionStatus());
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorSpecification,
					ccTransaction.getAidCccompnay(), ccTransaction.getTransactionId(), tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, ccTransaction.getAidCccompnay());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.setResponseCode("Failed");
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);

			return sr;
		}
	}

	/**
	 * This method use to get 4217 ISO standard numeric code for currency. AED = 784; EUR = 978;
	 * 
	 * @param currencyCode
	 *            String containing the currency code
	 * @return relevant currency numeric code
	 */
	protected String getStandardCurrencyCode(String currency) throws ModuleException {

		if (currency != null && !currency.equals("")) {
			String currencyCode = PaymentBrokerUtils.getPaymentBrokerDAO().getCurrencyNumericCode(currency,
					ipgIdentificationParamsDTO.getIpgId());
			if (currencyCode != null && !currencyCode.isEmpty()) {
				return currencyCode;
			} else {
				throw new ModuleException("pay.gateway.error.cyberPlus.currencyNotSupported");
			}
		} else {
			throw new ModuleException("pay.gateway.error.cyberPlus.currencyNotSupported");
		}
	}

	/**
	 * Returns the card type as integer
	 * 
	 * @param card
	 *            the card
	 * @return card type
	 */
	protected String getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 5-CB]
		String mapType = "VISA"; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			mapType = (String) mapCardType.get(card);
		}
		return mapType;
	}

	public String getActionMode() {
		return actionMode;
	}

	public void setActionMode(String actionMode) {
		this.actionMode = actionMode;
	}

	public String getCtxMode() {
		return ctxMode;
	}

	public void setCtxMode(String ctxMode) {
		this.ctxMode = ctxMode;
	}

	public String getPageAction() {
		return pageAction;
	}

	public void setPageAction(String pageAction) {
		this.pageAction = pageAction;
	}

	public String getPaymentConfig() {
		return paymentConfig;
	}

	public void setPaymentConfig(String paymentConfig) {
		this.paymentConfig = paymentConfig;
	}

	public String getReturnMode() {
		return returnMode;
	}

	public void setReturnMode(String returnMode) {
		this.returnMode = returnMode;
	}

}
