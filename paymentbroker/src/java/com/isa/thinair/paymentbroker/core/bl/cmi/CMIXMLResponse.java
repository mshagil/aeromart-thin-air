package com.isa.thinair.paymentbroker.core.bl.cmi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Class file for holding API response of CMI
 * @author dev
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "CC5Response")
public class CMIXMLResponse {

	@XmlElement
	private String ProcReturnCode;
	@XmlElement
	private Extra Extra;

	public Extra getExtra() {
		return Extra;
	}

	public void setExtra(Extra extra) {
		Extra = extra;
	}

	@XmlElement
	private String AuthCode;
	@XmlElement
	private String OrderId;
	@XmlElement
	private String TransId;
	@XmlElement
	private String ErrMsg;
	@XmlElement
	private String Response;
	@XmlElement
	private String HostRefNum;
	@XmlElement
	private String GroupId;

	public String getProcReturnCode() {
		return ProcReturnCode;
	}

	public void setProcReturnCode(String procReturnCode) {
		ProcReturnCode = procReturnCode;
	}

	public String getAuthCode() {
		return AuthCode;
	}

	public void setAuthCode(String authCode) {
		AuthCode = authCode;
	}

	public String getOrderId() {
		return OrderId;
	}

	public void setOrderId(String orderId) {
		OrderId = orderId;
	}

	public String getTransId() {
		return TransId;
	}

	public void setTransId(String transId) {
		TransId = transId;
	}

	public String getErrMsg() {
		return ErrMsg;
	}

	public void setErrMsg(String errMsg) {
		ErrMsg = errMsg;
	}

	public String getResponse() {
		return Response;
	}

	public void setResponse(String response) {
		Response = response;
	}

	public String getHostRefNum() {
		return HostRefNum;
	}

	public void setHostRefNum(String hostRefNum) {
		HostRefNum = hostRefNum;
	}

	public String getGroupId() {
		return GroupId;
	}

	public void setGroupId(String groupId) {
		GroupId = groupId;
	}

	@Override
	public String toString() {
		return "CMIXMLResponse [ProcReturnCode=" + ProcReturnCode + ", Extra="
				+ Extra + ", AuthCode=" + AuthCode + ", OrderId=" + OrderId
				+ ", TransId=" + TransId + ", ErrMsg=" + ErrMsg + ", Response="
				+ Response + ", HostRefNum=" + HostRefNum + ", GroupId="
				+ GroupId + "]";
	}

	
}
