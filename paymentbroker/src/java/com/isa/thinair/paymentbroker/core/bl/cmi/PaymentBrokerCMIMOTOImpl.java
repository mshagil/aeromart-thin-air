package com.isa.thinair.paymentbroker.core.bl.cmi;

import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.APPROVED;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.POST_AUTH;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.PRE_AUTH;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.SERVICETYPE_AUTHORIZE;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerCMIMOTOImpl extends PaymentBrokerCMITemplate {
	static Log log = LogFactory.getLog(PaymentBrokerCMIMOTOImpl.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil
			.getResourceBundle();

	/**
	 * API for preAuth is implemented
	 */
	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode,
			List<CardDetailConfigDTO> cardDetailConfigData)
			throws ModuleException {
		String merchantTxnId =  composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		CMIXMLRequest request = new CMIXMLRequest();
		request.setClientId(getMerchantId());
		request.setCurrency(Integer.parseInt(getCurrency()));
		request.setCvv2Val(creditCardPayment.getCvvField());
		request.setExpires(CMIPaymentUtils.getExpiryDate(creditCardPayment
				.getExpiryDate()));
		request.setName(getUserName());
		request.setNumber(creditCardPayment.getCardNumber());
		request.setPassword(getPassword());
		request.setTotal(Double.parseDouble(creditCardPayment.getAmount()));
		request.setType(PRE_AUTH);
		request.setOrderId(merchantTxnId);
		if (creditCardPayment.getTravelDTO() != null) {
			BillTo buyerDet = new BillTo();
			request.setEmail(creditCardPayment.getTravelDTO().getEmailAddress());
			buyerDet.setName(creditCardPayment.getTravelDTO().getContactName());
			buyerDet.setTelVoice(creditCardPayment.getTravelDTO()
					.getContactNumber());
			buyerDet.setCountry(creditCardPayment.getTravelDTO()
					.getResidenceCountryCode());
			request.setBillTo(buyerDet);
		}
		String requestString = null;
		try {
			requestString = CMIPaymentUtils.generateXMLRequest(request);

		} catch (ModuleException e) {
			throw e;
		} catch (Exception err) {
			throw new ModuleException(
					"paymentbroker.cmi.online.request.API.error");
		}
			
		
		CreditCardTransaction ccTransaction = auditTransaction(pnr,
				getMerchantId(), merchantTxnId,
				creditCardPayment.getTemporyPaymentId(),
				bundle.getString(SERVICETYPE_AUTHORIZE), request.toString(),
				"", getPaymentGatewayName(), null, false);

		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(
				false);
		Integer procReturnCode = 0;
		CMIXMLResponse response;
		CMIXMLResponse postResponse = null;
		// calling the payment gateway API

		response = CMIPaymentUtils.executeCMIApiRequest(requestString,
				getIpgURL());

		String orderNumber = null;
		Integer paymentBrokerRefNo = ccTransaction.getTransactionRefNo();
		if (response.getResponse().equals(APPROVED)) {
			orderNumber = response.getOrderId();
			CMIXMLRequest postRequest = new CMIXMLRequest();
			postRequest.setClientId(getMerchantId());
			postRequest.setOrderId(orderNumber);
			postRequest.setName(getUserName());
			postRequest.setPassword(getPassword());
			postRequest.setType(POST_AUTH);
			if (creditCardPayment.getTravelDTO() != null) {
				BillTo buyerDet = new BillTo();
				postRequest.setEmail(creditCardPayment.getTravelDTO().getEmailAddress());
				buyerDet.setName(creditCardPayment.getTravelDTO().getContactName());
				buyerDet.setTelVoice(creditCardPayment.getTravelDTO()
						.getContactNumber());
				buyerDet.setCountry(creditCardPayment.getTravelDTO()
						.getResidenceCountryCode());
				postRequest.setBillTo(buyerDet);
			}
			String postRequestString = CMIPaymentUtils
					.generateXMLRequest(postRequest);
			log.info("Postauthorization request" + postRequestString);

			postResponse = CMIPaymentUtils.executeCMIApiRequest(
					postRequestString, getIpgURL());
			log.info("Postauthorization API response" + postResponse);

			if (postResponse.getResponse().equals(APPROVED)) {
				serviceResponse.setSuccess(true);
				procReturnCode = Integer.parseInt(postResponse
						.getProcReturnCode());
				orderNumber = postResponse.getOrderId();

			} else {
				CMIPaymentUtils.prepareErrorStatus(serviceResponse,
						postResponse);

			}

		} else {
			CMIPaymentUtils.prepareErrorStatus(serviceResponse, response);
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(),
				postResponse.toString(), postResponse.getErrMsg(),
				postResponse.getAuthCode(), orderNumber, procReturnCode);
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		serviceResponse.addResponceParam(
				PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID,
				postResponse.getAuthCode());
		return serviceResponse;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO,
			List<CardDetailConfigDTO> configDataList) throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap,
			IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(
			IPGPrepPaymentDTO params) throws ModuleException {
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap,
			IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
	}

}
