/**
 * 
 */
package com.isa.thinair.paymentbroker.core.bl.hsbc;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CustomClassLoader;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;

/**
 * @author Asiri Liyanage
 */
public class HsbcIPGUtil {

	private static Log log = LogFactory.getLog(HsbcIPGUtil.class);

	private static final String DEFAULT_TRANSACTION_CODE = "-1";

	private static final String ERROR_CODE_PREFIX = "hsbc.";

	private static final String RETURN_URL_APPENDER = "?DR=${DR}";

	private static final String CARD_NUMBER = "CardNum";

	private static final String CARD_EXPIRY = "CardExp";

	private static final String SECURE_CODE = "CardSecurityCode";

	private static final String MERCHANT_TXN_REF = "MerchTxnRef";

	private static final String IPG_BASE_CURRENCY = "USD";

	private static CustomClassLoader customClassLoader;

	private int i;

	/**
	 * This method needs to be called in each public method, so that it will initialize the environment related to
	 * payment client.
	 */
	private synchronized void initialize(String paymentClientPath) {
		if (customClassLoader == null) {
			initializeTheProvider();
			loadCustomLoader(paymentClientPath);
		}
	}

	/**
	 * Loads the custom loader
	 */
	private void loadCustomLoader(String paymentClientPath) {
		if (log.isDebugEnabled()) {
			log.debug("######################################## LOADING #############################################");
		}
		//String path1 = paymentClientPath + "/classes/bcprov-jdk13-119-QSI-1.0.jar";
		String path2 = paymentClientPath + "/classes/log4j.jar";
		String path3 = paymentClientPath + "/classes/PaymentClient.jar";

		Collection<String> colJarFiles = new ArrayList<String>();
		//colJarFiles.add(path1);
		colJarFiles.add(path2);
		colJarFiles.add(path3);

		try {
			customClassLoader = new CustomClassLoader(colJarFiles, new ArrayList(), true);
		} catch (Exception e) {
			log.error(e);
		}

		if (log.isDebugEnabled()) {
			log.debug("######################################## LOADED #############################################");
		}
	}

	private Provider getProvider(String strProvider) {
		Provider[] providers = Security.getProviders();
		i = 0;
		for (Provider provider : providers) {
			i++;
			if (provider.getName().equals(strProvider)) {
				return provider;
			}
		}
		return null;
	}

	/**
	 * Initialize the security provider
	 */
	private void initializeTheProvider() {

		int intSunJCEIndex = 1;
		Provider jceProvider = null;
		Provider BCProvider = null;

		jceProvider = getProvider("SunJCE");
		if (jceProvider != null && i != intSunJCEIndex) {
			Security.removeProvider(jceProvider.getName());
			Security.insertProviderAt(jceProvider, intSunJCEIndex);
		}

		BCProvider = getProvider("BC");
		if (BCProvider == null) {
			Security.insertProviderAt(new BouncyCastleProvider(), 2);
		}
	}

	private Object getPaymentClient() {
		Object clazz = null;
		try {
			Class aClass = customClassLoader.loadClass("PaymentClient.PaymentClientImpl");
			clazz = (Object) aClass.newInstance();

		} catch (Exception e) {
			log.error(e);
		}
		return clazz;
	}

	private String invokeOperation(Object clazz, String operationName, Object[] params, Class[] clazzType) {
		Object response = null;
		try {
			Method m = clazz.getClass().getMethod(operationName, clazzType);
			response = m.invoke(clazz, params);
		} catch (Exception e) {
			log.error(e);
		}

		if (response != null) {
			return response.toString();
		}

		return null;

	}

	private boolean invokeOperationBoolean(Object clazz, String operationName, Object[] params, Class[] clazzType) {
		Object response = null;
		try {
			Method m = clazz.getClass().getMethod(operationName, clazzType);
			response = m.invoke(clazz, params);
		} catch (Exception e) {
			log.error(e);
		}

		if (response != null) {
			return new Boolean(response.toString());
		}

		return false;

	}

	/**
	 * This function uses the returned status code retrieved from the Digital Response and returns an appropriate
	 * description for the code
	 * 
	 * @param vResponseCode
	 *            String containing the vpc_TxnResponseCode
	 * @return description String containing the appropriate description
	 */
	private String getResponseDescription(String vResponseCode) {

		String result = "";

		// check if a single digit response code
		if (vResponseCode.length() == 1) {

			// Java cannot switch on a string so turn everything to a char
			char input = vResponseCode.charAt(0);

			switch (input) {
			case '0':
				result = "Transaction Successful.";
				break;
			case '1':
				result = "Unknown Error.";
				break;
			case '2':
				result = "Bank Declined Transaction.";
				break;
			case '3':
				result = "No Reply from Bank.";
				break;
			case '4':
				result = "Expired Card.";
				break;
			case '5':
				result = "Insufficient Funds.";
				break;
			case '6':
				result = "Error Communicating with Bank.";
				break;
			case '7':
				result = "Payment Server System Error.";
				break;
			case '8':
				result = "Transaction Type Not Supported.";
				break;
			case '9':
				result = "Bank declined transaction (Do not contact Bank).";
				break;
			default:
				result = "Unable to determine.";
			}
			return result;
		} else {
			return "No Value Returned.";
		}
	}

	/**
	 * Returns the application specific error code for the transaction response
	 * 
	 * @param vResponseCode
	 *            the transaction reponse code
	 * @return the matching error code
	 */
	private String getMatchingTxnErrorCode(String vResponseCode) {
		String result = "";

		log.debug("Response Code : " + vResponseCode);

		// check if a single digit response code
		if (vResponseCode.length() == 1) {

			// Java cannot switch on a string so turn everything to a char
			char input = vResponseCode.charAt(0);

			switch (input) {
			case '0':
				result = "0";
				break;
			case '1':
				result = "-401";
				break;
			case '2':
				result = ERROR_CODE_PREFIX + "1";
				break;
			case '3':
				result = ERROR_CODE_PREFIX + "2";
				break;
			case '4':
				result = "33";
				break;
			case '5':
				result = "51";
				break;
			case '6':
				result = ERROR_CODE_PREFIX + "3";
				break;
			case '7':
				result = "230";
				break;
			case '8':
				result = ERROR_CODE_PREFIX + "12";
				break;
			case '9':
				result = ERROR_CODE_PREFIX + "4";
				break;
			default:
				result = "-401";
			}
			return result;
		} else {
			return "";
		}
	}

	/**
	 * @param objPayClient
	 * @param status
	 * @param message
	 * @param errorSpecification
	 * @return
	 * @throws ModuleException
	 */
	private HsbcIPGResponse evaluateResponse(Object objPayClient) throws ModuleException {

		String qsiResponseCode;
		String result;
		String exception;
		String errorCode = DEFAULT_TRANSACTION_CODE;
		String status = IPGResponseDTO.STATUS_REJECTED;
		String message = "";
		String errorSpecification = "";

		int txnResultCode;
		String merchTxnRef;
		String orderinfo;
		String merchantId;
		String purchaseAmount;
		String receiptNo;
		String acqResponseCode;
		String authorizeID;
		String batchNo;
		String ipgTxnId;
		String cardType;
		HsbcIPGResponse hsbcIPGResponse;

		// Step 5(6) - Get the result data from the Digital Receipt
		// *****************************************************
		// Extract the available Digital Receipt fields
		// Get the QSI Response Code for the transaction
		qsiResponseCode = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.QSIResponseCode" },
				new Class[] { String.class });

		if (qsiResponseCode == null || qsiResponseCode.length() == 0) {
			// Display an error as QSIResponseCode could not be retrieved
			// perform error handling for this command
			log.error("Did not receive a successful response. " + qsiResponseCode);
			throw new ModuleException("Did not receive a successful response. " + qsiResponseCode);
		}

		// Check if the QSI Response Code is an error message
		// an error message has a response code of '7'
		if (qsiResponseCode.equals("7")) {

			// Get the error returned from the Payment Client.
			result = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.ERROR" },
					new Class[] { String.class });
			exception = "Received an error response. ";
			if (result != null && result.length() != 0) {
				exception = exception + result;
			}
			// The response is an error message so generate an Error Page
			// perform error handling for this command
			log.error(exception + ", response : " + qsiResponseCode);
			// throw new ModuleException(exception + ", response : " + qsiResponseCode);
		}

		// Extract the available Digital Receipt fields
		// The example retrieves the MerchantID and CapturedAmount values,
		// which should be the same as the values sent. In a production
		// system the sent values and the receive values could be checked to
		// make sure they are the same.
		merchTxnRef = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.MerchTxnRef" },
				new Class[] { String.class });
		orderinfo = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.OrderInfo" },
				new Class[] { String.class });
		merchantId = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.MerchantId" },
				new Class[] { String.class });
		purchaseAmount = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.PurchaseAmountInteger" },
				new Class[] { String.class });
		receiptNo = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.ReceiptNo" },
				new Class[] { String.class });
		acqResponseCode = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.AcqResponseCode" },
				new Class[] { String.class });
		authorizeID = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.AuthorizeId" },
				new Class[] { String.class });
		batchNo = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.BatchNo" },
				new Class[] { String.class });
		ipgTxnId = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.TransactionNo" },
				new Class[] { String.class });
		cardType = invokeOperation(objPayClient, "getResultField", new Object[] { "DigitalReceipt.CardType" },
				new Class[] { String.class });

		if (qsiResponseCode.equals("0")) {
			errorCode = "";
			status = IPGResponseDTO.STATUS_ACCEPTED;
			txnResultCode = 1;
		} else {
			message = getResponseDescription(qsiResponseCode);
			errorCode = getMatchingTxnErrorCode(qsiResponseCode);
			txnResultCode = 0;
			errorSpecification = status + " " + message;
			authorizeID = null;
		}

		hsbcIPGResponse = new HsbcIPGResponse();
		hsbcIPGResponse.setQsiResponseCode(qsiResponseCode);
		hsbcIPGResponse.setAuthorizeID(authorizeID);
		hsbcIPGResponse.setBatchNo(batchNo);
		hsbcIPGResponse.setCardType(cardType);
		hsbcIPGResponse.setIpgTxnId(ipgTxnId);
		hsbcIPGResponse.setMerchantId(merchantId);
		hsbcIPGResponse.setMerchTxnRef(merchTxnRef);
		hsbcIPGResponse.setOrderinfo(orderinfo);
		hsbcIPGResponse.setPurchaseAmount(purchaseAmount);
		hsbcIPGResponse.setReceiptNo(receiptNo);
		hsbcIPGResponse.setErrorCode(errorCode);
		hsbcIPGResponse.setErrorSpecification(errorSpecification);
		hsbcIPGResponse.setTxnResultCode(txnResultCode);
		hsbcIPGResponse.setStatus(status);
		hsbcIPGResponse.setMessage(message);

		if (log.isDebugEnabled()) {
			log.debug("Result of transaction is --> merchTxnRef : " + merchTxnRef + ", orderinfo : " + orderinfo
					+ ", merchantId : " + merchantId + ", purchaseAmount : " + purchaseAmount + ", receiptNo : " + receiptNo
					+ ", qsiResponseCode : " + qsiResponseCode + ", acqResponseCode : " + acqResponseCode + ", authorizeID : "
					+ authorizeID + ", batchNo : " + batchNo + ", transactionNo : " + ipgTxnId + ", cardType : " + cardType);
			log.debug("**************************");
		}
		return hsbcIPGResponse;
	}

	/**
	 * Creates the encrypted request to be sent to the payment gateway.
	 * 
	 * @param hsbcIPGRequest
	 * @return
	 * @throws ModuleException
	 */
	public String getEncryptedRequest(HsbcIPGRequest hsbcIPGRequest) {

		Object objPayClient = null;
		String digitalOrder = null;
		String result;
		String errMsg;
		String strAmount = null;
		int amount = 0;
		String returnURL;
		BigDecimal hundred;
		BigDecimal amountInLowCurDom;
		String merchantTxnRef;
		IPGRequestDTO ipgRequestDTO;
		String merchantId;
		String paymentClientPath;
		String locale;

		ipgRequestDTO = hsbcIPGRequest.getIpgRequestDTO();
		merchantTxnRef = hsbcIPGRequest.getMerchantTxnRef();
		merchantId = hsbcIPGRequest.getMerchantId();
		locale = hsbcIPGRequest.getLocale();
		returnURL = hsbcIPGRequest.getReturnUrl();
		paymentClientPath = hsbcIPGRequest.getPaymentClientPath();

		initialize(paymentClientPath);

		// Step 1 - Connect to the Payment Client
		// ***************************************
		// create a Payment Client object
		objPayClient = getPaymentClient();

		// Step 2 - Test the Payment Client object was created
		// successfully
		// ****************************************************************
		// Test the communication to the Payment Client using an "echo"
		result = invokeOperation(objPayClient, "echo", new Object[] { "Test" }, new Class[] { String.class });

		if (!result.equals("echo:Test")) {
			// Display error as communication to the Payment Client failed
			// perform error handling for this command
			log.error("Error occurred in echo. " + result);
			throw new ModuleRuntimeException("Error occurred in echo. " + result);
		}

		// Step 3 - Prepare to Generate and Send Digital Order
		// ***************************************************
		// The extra details are not accepted by the
		// getDigitalOrder() method and so we must add them
		// explicitly here for inclusion in the DO creation.
		// Adding the supplementary data to the DO is done using the
		// "addDigitalOrderField" command
		// add MerchTxnRef number

		invokeOperation(objPayClient, "addDigitalOrderField", new Object[] { MERCHANT_TXN_REF, merchantTxnRef }, new Class[] {
				String.class, String.class });

		// objPayClient.addDigitalOrderField("MerchTxnRef", new Integer(merchantTxnId).toString());

		// Step 4 - Generate and Send Digital Order
		// ****************************************

		strAmount = ipgRequestDTO.getAmount();

		amount = getIntAmount(strAmount);

		// Create and send the Digital Order
		returnURL = ipgRequestDTO.getReturnUrl() + RETURN_URL_APPENDER; // Added the reference for the DR to be used
		// by the payment gateway.

		digitalOrder = invokeOperation(objPayClient, "getDigitalOrder", new Object[] { ipgRequestDTO.getPnr(), merchantId,
				amount, locale, returnURL }, new Class[] { String.class, String.class, int.class, String.class, String.class });

		if (digitalOrder != null && digitalOrder.length() == 0) {
			// Retrieve the Payment Client Error (There may be none to get)
			result = invokeOperation(objPayClient, "getResultField", new Object[] { "PaymentClient.Error" },
					new Class[] { String.class });
			errMsg = "Digital Order is invalid. " + digitalOrder;

			if (result != null && result.length() != 0) {
				errMsg = errMsg + result;
			}

			// Display an Error Page as the Digital Order was not processed
			// perform error handling for this command
			log.error(errMsg);
			throw new ModuleRuntimeException(errMsg);
		}

		return digitalOrder;
	}

	private int getIntAmount(String strAmount) {
		int amount = 0;
		BigDecimal hundred;
		BigDecimal amountInLowCurDom;
		if (strAmount != null || !strAmount.equals("")) {
			GlobalConfig globalConfig = PaymentBrokerModuleUtil.getGlobalConfig();
			String strVisible = globalConfig.getBizParam(SystemParamKeys.CURRENCY_ROUNDUP);

			/*
			 * FIXME TODO - exchange rate effective date should be passed to this method Multiple Exchange Rate CR -
			 * 29-Dec-2008 12:45
			 */
			Currency currency;
			CurrencyExchangeRate currencyExchangeRate = null;
			try {
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(IPG_BASE_CURRENCY);
			} catch (ModuleException e) {
				throw new ModuleRuntimeException(e.getExceptionCode());
			} finally {
				if (currencyExchangeRate == null) {
					throw new ModuleRuntimeException("Exchange Rate Undefined");
				}
			}

			currency = currencyExchangeRate.getCurrency();

			// BigDecimal dblExRate = currencyExchangeRate.getExchangeRate();
			BigDecimal dblExRate = currencyExchangeRate.getExrateBaseToCurNumber();// JIRA : AARESAA-3087
			BigDecimal dblBoundary = currency.getBoundryValue();
			BigDecimal dblBreakPoint = currency.getBreakPoint();

			BigDecimal ipgAmount = new BigDecimal(strAmount).divide(dblExRate, 2, RoundingMode.UP);

			if (strVisible != null && strVisible.equals("Y") && dblBoundary != null && dblBoundary.doubleValue() != 0) {
				ipgAmount = AccelAeroRounderPolicy.getRoundedValue(ipgAmount, dblBoundary, dblBreakPoint);
			}

			// Amount is multiplied by 100, because the payment component is
			// accepting only integers and it devides the integer by 100.
			hundred = new BigDecimal(100);
			amountInLowCurDom = ipgAmount.multiply(hundred);
			amount = amountInLowCurDom.intValue();
		}
		return amount;
	}

	/**
	 * Extract the encrypted Digital Receipt from the request. Note that the default redirection from the Payment Server
	 * contains a parameter "DR" which was sent as part of the DO in the ReturnURL field.
	 * 
	 * @param encryptedDR
	 * @param paymentClientPath
	 * @return
	 * @throws ModuleException
	 */
	public HsbcIPGResponse getDecryptedResponse(String encryptedDR, String paymentClientPath) throws ModuleException {

		Object objPayClient = null;
		boolean decrypted;
		boolean hasNext;

		String result;
		String exception;

		initialize(paymentClientPath);

		// Step 1 - Connect to the Payment Client
		// ***************************************
		// create a Payment Client object
		objPayClient = getPaymentClient();

		// Step 2 - Test the Payment Client object was created successfully
		// ****************************************************************
		// Test the communication to the Payment Client using an "echo"
		result = invokeOperation(objPayClient, "echo", new Object[] { "Test" }, new Class[] { String.class });
		if (!result.equals("echo:Test")) {
			// perform error handling for this command
			log.error("Error occurred in echo. " + result);
			throw new ModuleException("Error occurred in echo. " + result);

		}

		// Step 3 - Decrypt Digital Receipt
		// ********************************
		decrypted = invokeOperationBoolean(objPayClient, "decryptDigitalReceipt", new Object[] { encryptedDR },
				new Class[] { String.class });
		if (!decrypted) {
			// Retrieve the Payment Client Error (There may be none to get)
			result = invokeOperation(objPayClient, "getResultField", new Object[] { "PaymentClient.Error" },
					new Class[] { String.class });
			exception = "Digital Receipt is invalid. ";
			if (result != null && result.length() != 0) {
				exception = exception + result;
			}
			// perform error handling for this command
			log.error(exception + ", DR : " + encryptedDR);
			throw new ModuleException(exception + ", DR : " + encryptedDR);
		}

		// Step 4 - Check the DR to see if there is a valid result
		// *******************************************************
		// Use the "nextResult" command to check if the DR contains a result
		hasNext = invokeOperationBoolean(objPayClient, "nextResult", new Object[] {}, new Class[] {});
		if (!hasNext) {
			// Retrieve the Payment Client Error (There may be none to get)
			result = invokeOperation(objPayClient, "getResultField", new Object[] { "PaymentClient.Error" },
					new Class[] { String.class });
			exception = "Digital Receipt does not contain a result. ";
			if (result != null && result.length() != 0) {
				exception = exception + result;
			}
			// Display an error as the DR doesn't contain a result
			// perform error handling for this command
			log.error(exception + ", DR : " + encryptedDR);
			throw new ModuleException(exception + ", DR : " + encryptedDR);
		}

		return evaluateResponse(objPayClient);
	}

	/**
	 * Performs charging for the MOTO version of payment gateway.
	 * 
	 * @param hsbcIPGRequest
	 * @return
	 * @throws ModuleException
	 */
	public HsbcIPGResponse charge(HsbcIPGRequest hsbcIPGRequest) throws ModuleException {

		log.debug("Entered charge()");

		String exception;
		String strAmount;
		int amount = 0;
		String returnURL = "";
		BigDecimal hundred;
		BigDecimal amountInLowCurDom;
		String merchantTxnRef;
		CreditCardPayment creditCardPayment;
		String merchantId;
		String paymentClientPath;
		boolean isMotoSent;

		Object objPayClient = null;
		boolean hasNext;

		String result;
		creditCardPayment = hsbcIPGRequest.getCreditCardPayment();
		merchantTxnRef = hsbcIPGRequest.getMerchantTxnRef();
		merchantId = hsbcIPGRequest.getMerchantId();
		returnURL = hsbcIPGRequest.getReturnUrl();
		paymentClientPath = hsbcIPGRequest.getPaymentClientPath();

		initialize(paymentClientPath);

		// Step 1 - Connect to the Payment Client
		// **************************************
		// create a Payment Client object
		objPayClient = getPaymentClient();

		// Step 2 - Test the Payment Client object was created successfully
		// ****************************************************************
		// Test the communication to the Payment Client using an "echo"
		result = invokeOperation(objPayClient, "echo", new Object[] { "Test" }, new Class[] { String.class });
		if (!result.equals("echo:Test")) {
			// Display error as communication to the Payment Client failed
			// perform error handling for this command
			log.error("Error occurred in echo. " + result);
			throw new ModuleException("Error occurred in echo. " + result);
		}

		// Step 3 - Prepare to Generate and Send Digital Order
		// ***************************************************
		// The extra details are not accepted by the
		// sendMOTODigitalOrder() method and so we must add them
		// explicitly here for inclusion in the DO creation.
		// Adding the supplementary data to the DO is done using the
		// "addDigitalOrderField" command

		invokeOperation(objPayClient, "addDigitalOrderField", new Object[] { CARD_NUMBER, creditCardPayment.getCardNumber() },
				new Class[] { String.class, String.class });
		invokeOperation(objPayClient, "addDigitalOrderField", new Object[] { CARD_EXPIRY, creditCardPayment.getExpiryDate() },
				new Class[] { String.class, String.class });
		invokeOperation(objPayClient, "addDigitalOrderField", new Object[] { SECURE_CODE, creditCardPayment.getCvvField() },
				new Class[] { String.class, String.class });

		// add MerchTxnRef number
		invokeOperation(objPayClient, "addDigitalOrderField", new Object[] { MERCHANT_TXN_REF, merchantTxnRef }, new Class[] {
				String.class, String.class });

		strAmount = creditCardPayment.getAmount();

		/*
		 * if (strAmount != null || !strAmount.equals("")) { GlobalConfig globalConfig =
		 * PaymentBrokerModuleUtil.getGlobalConfig(); String strVisible =
		 * globalConfig.getBizParam(SystemParamKeys.CURRENCY_ROUNDUP);
		 * 
		 * Currency currency = commonMasterBD.getCurrency(IPG_BASE_CURRENCY); BigDecimal dblExRate =
		 * currency.getBaseExchangeRate(); BigDecimal dblBoundary = currency.getBoundryValue(); BigDecimal dblBreakPoint
		 * = currency.getBreakPoint();
		 * 
		 * BigDecimal ipgAmount = new BigDecimal(strAmount).divide(dblExRate, 3, RoundingMode.UP);
		 * 
		 * if (strVisible != null && strVisible.equals("Y") && dblBoundary != null && dblBoundary.doubleValue() != 0) {
		 * ipgAmount = NumberUtil.getRoundedValue(ipgAmount, dblBoundary, dblBreakPoint); }
		 * 
		 * // Amount is multiplied by 100, because the payment component is // accepting only integers and it devides
		 * the integer by 100. hundred = new BigDecimal(100); amountInLowCurDom = ipgAmount.multiply(hundred); amount =
		 * amountInLowCurDom.intValue(); }
		 */
		amount = getIntAmount(strAmount);

		// Step 4 - Generate and Send Digital Order (& receive DR)
		// *******************************************************

		// Create and send the Digital Order. Setting values for locale and return URL does not have any effect here.
		// Therefore default values are used.
		// (This primary command also receives the Digital Receipt)
		isMotoSent = invokeOperationBoolean(objPayClient, "sendMOTODigitalOrder", new Object[] { creditCardPayment.getPnr(),
				merchantId, amount, "en", returnURL }, new Class[] { String.class, String.class, int.class, String.class,
				String.class });
		if (!isMotoSent) {
			// Retrieve the Payment Client Error (There may be none to get)
			result = invokeOperation(objPayClient, "getResultField", new Object[] { "PaymentClient.Error" },
					new Class[] { String.class });

			exception = "Digital Order is invalid. ";
			if (result != null && result.length() != 0) {

				exception = exception + result;
			}
			// perform error handling for this command
			log.error(exception);
			throw new ModuleException(exception);

		}

		// Step 5 - Check the DR to see if there is a valid result
		// *******************************************************
		// Use the "nextResult" command to check if the DR contains a result
		hasNext = invokeOperationBoolean(objPayClient, "nextResult", new Object[] {}, new Class[] {});
		if (!hasNext) {
			// Retrieve the Payment Client Error (There may be none to get)
			result = invokeOperation(objPayClient, "getResultField", new Object[] { "PaymentClient.Error" },
					new Class[] { String.class });
			exception = "Digital Receipt does not contain a result. ";
			if (result != null && result.length() != 0) {
				exception = exception + result;
			}
			// Display an error as the DR doesn't contain a result
			log.error(exception);
			throw new ModuleException(exception);
		}

		return evaluateResponse(objPayClient);

	}
}