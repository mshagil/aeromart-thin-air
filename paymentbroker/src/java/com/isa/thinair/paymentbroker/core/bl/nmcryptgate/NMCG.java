package com.isa.thinair.paymentbroker.core.bl.nmcryptgate;

/**
 * A bridge between Nmcryptgate and Packaged Nmcryptgate
 * 
 * @author Nilindra Fernando
 */
public class NMCG extends Nmcryptgate {
	static {
		System.out.println("##############################");
		System.out.println("# Loaded NMCRYPTGATE PACKAGE #");
		System.out.println("##############################");
	}
}
