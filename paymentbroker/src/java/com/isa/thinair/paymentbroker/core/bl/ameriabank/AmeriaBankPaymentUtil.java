package com.isa.thinair.paymentbroker.core.bl.ameriabank;

import java.math.BigDecimal;
import java.util.Map;

import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonRequest;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentFieldsResponse;

public class AmeriaBankPaymentUtil {
	public static final String PROVIDER_NAME = "AMERIA";

	public static final String RESPONSE_CODE_1_0 = "Processing center response timeout";
	public static final String RESPONSE_CODE_0_100 = "No payment attempts";
	public static final String RESPONSE_CODE_0_2000 = "Transaction declined since the card is in the blacklist";
	public static final String RESPONSE_CODE_0_2001 = "Transaction declined since Clients IP address is in the blacklist";
	public static final String RESPONSE_CODE_0_20010 = "Transaction declined since payment amount exceeded the limits set by the issuer bank";
	public static final String RESPONSE_CODE_0_2002 = "Transaction declined since payment amount exceeded the limits(daily turnover limits of the merchant set by the acquiring bank or one card turnover limit of the merchant or one transaction limit ofthe merchant)";
	public static final String RESPONSE_CODE_0_2004 = "Payment through SSL without entering SVС data is forbidden";
	public static final String RESPONSE_CODE_0_2005 = "We failed to check the issuers signature, i.e. PARes was readable but was not signed correctly";
	public static final String RESPONSE_CODE_0_2006 = "Issuer declined authentication";
	public static final String RESPONSE_CODE_0_2007 = "Card data registration timeout starting from the moment ofpayment registration (timeout will be in 20 minutes)";

	public static final String RESPONSE_CODE_0_2011 = "Card is included in 3d secure but the issuer bank is not ready (at that moment) to execute 3ds of transaction";
	public static final String RESPONSE_CODE_0_2012 = "This operation is not supported";
	public static final String RESPONSE_CODE_0_2013 = "Payment attempts expired";
	public static final String RESPONSE_CODE_0_2015 = "VERes of DS contain iReq, due to which payment was declined";

	public static final String RESPONSE_CODE_0_2016 = "Issuer bank is not ready (at that moment) to execute 3ds oftransaction (for instance, the bank’s ACS does not function). Wecannot define whether the card is included in 3d secure or not.";
	public static final String RESPONSE_CODE_0_2018 = "Directory server Visa or MasterCard cannot be reached or - connection failed- error is received in response to request VeReq. This error is the result interaction of payment gateway and MPSservers due to their technical failures.";
	public static final String RESPONSE_CODE_0_2019 = "Issuers PARes contains iReq, due to which payment was declined.";
	public static final String RESPONSE_CODE_0_9000 = "Transactions start";

	public static final String RESPONSE_CODE_00 = "Payment successfully completed";
	public static final String RESPONSE_CODE_01 = "Order with the given number is already registered in the system";
	public static final String RESPONSE_CODE_0100 = "Issuer bank has forbidden online card transactions";
	public static final String RESPONSE_CODE_01001 = "At the moment of registering the transaction, i.e. when the card details are not entered yet";
	public static final String RESPONSE_CODE_0101 = "Card’s validity period has expired";

	public static final String RESPONSE_CODE_0103 = "No connection with the issuer bank, the merchant must call the issuer bank";
	public static final String RESPONSE_CODE_0104 = "Attempt to make a transaction via blocked account";
	public static final String RESPONSE_CODE_0107 = "It is necessary to call the issuer bank";

	public static final String RESPONSE_CODE_0109 = "Merchants/*terminal ID is invalid (for completion and preauthorization with different IDs)";
	public static final String RESPONSE_CODE_0110 = "Transaction amount is invalid";
	public static final String RESPONSE_CODE_0111 = "Card number is invalid";
	public static final String RESPONSE_CODE_0116 = "Transaction amount exceeds the available account balance";
	public static final String RESPONSE_CODE_0120 = "Transaction declined since it is not allowed by the issuerPayment network code: 57Reason for declining the transaction should be clarified from theissuer.";
	public static final String RESPONSE_CODE_0121 = "Attempt to make a transaction exceeding the daily limit set by the issuer bank";
	public static final String RESPONSE_CODE_0123 = "Limit on the number of transactions is exceeded. Client has mademaximum number of transactions within the limit and attempts tomake one more transaction";
	public static final String RESPONSE_CODE_0125 = "Invalid Card numberAttempt to refund an amount exceeding the hold, attempt torefund zero amount";

	public static final String RESPONSE_CODE_0151017 = "3-D Secure connection error";
	public static final String RESPONSE_CODE_0151018 = "Processing timeout, failed to send";
	public static final String RESPONSE_CODE_0151019 = "Processing timeout, sent but no response received from the bank";

	public static final String RESPONSE_CODE_02001 = "Fraudulent transaction ( according to processing or payment network)";
	public static final String RESPONSE_CODE_02003 = "Merchant is not allowed to perform SSL (Not 3d- Secure/SecureCode) transactions";
	public static final String RESPONSE_CODE_02005 = "Payment does not comply with the 3ds verification terms";
	public static final String RESPONSE_CODE_0208 = "Card is lost";
	public static final String RESPONSE_CODE_0209 = "Card limitations are exceeded";
	public static final String RESPONSE_CODE_0341014 = "RBS decline error";
	public static final String RESPONSE_CODE_0341041 = "Refund error";

	public static final String RESPONSE_CODE_05 = "Error in request parameters";
	public static final String RESPONSE_CODE_071015 = "Incorrect card parameters input";
	public static final String RESPONSE_CODE_08204 = "Such order has already been registered (check by order number)";
	public static final String RESPONSE_CODE_09001 = "Internal code of RBS rejection";

	public static final String RESPONSE_CODE_09002 = "Cardholder attempts to make a forbidden transaction";
	public static final String RESPONSE_CODE_09003 = "Attempt to make a transaction in the amount exceeding the limit set by the issuer bank";
	public static final String RESPONSE_CODE_09004 = "Error message format according to issuer bank";
	public static final String RESPONSE_CODE_09007 = "No connection with the issuer bank.Stand-in authorization is not allowed for the given card number(this mode means that the issuer cannot connect to the paymentnetwork, so the transaction is either in offline mode and is thengoing to be sent to the back office or will be declined)";
	public static final String RESPONSE_CODE_09009 = "General system failure fixed by the payment network or the issuer bank";
	public static final String RESPONSE_CODE_09010 = "Issuer bank is not available";
	public static final String RESPONSE_CODE_09013 = "Invalid transaction format according to the network";
	public static final String RESPONSE_CODE_09014 = "Transaction is not found (while sending completion, reversal or refund request)";
	public static final String RESPONSE_CODE_0999 = "Transaction authorization did not start. Declined due to fraud or 3dsec error.";

	public static final String RESPONSE_CODE_02 = "Order declined due to errors in payment details";
	public static final String RESPONSE_CODE_03 = "Unknown (forbidden) currency";
	public static final String RESPONSE_CODE_04 = "Required parameter of the request is missing";
	public static final String RESPONSE_CODE_06 = "Unregistered OrderId";
	public static final String RESPONSE_CODE_07 = "System error";
	public static final String RESPONSE_CODE_20 = "Incorrect Username and Password";
	public static final String RESPONSE_CODE_30 = "Incorrect Value of Opque field of the initial request";
	public static final String RESPONSE_CODE_550 = "System Error";
	public static final String RESPONSE_CODE_514 = "Do not haveReverse operationpermission";
	public static final String RESPONSE_CODE_513 = "Do not haveRefund operationpermission";
	public static final String RESPONSE_CODE_560 = "Operation failed";
	public static final String RESPONSE_CODE_520 = "Overtime error";
	public static final String RESPONSE_CODE_50 = "Payment sum error ";
	public static final String RESPONSE_CODE_510 = "Incorrect parameters";
	public static final String RESPONSE_CODE_500 = "Unknown error";

	public static final String PAYMENT_ID_RESP_SUCCESS_CODE = "1";
	public static final String PAYMENT_ID_RESP_SUCCESS_MESSAGE = "OK";

	public static final String PAYMENT_REDIRECTION_RESP_SUCCESS_MESSAGE = "Operation Approved";

	public static final String COMMON_RESP_SUCCESS_CODE = "00";
	public static final String UNIDENTIFIED_ERROR = "unidentified error (possibly at AccelAero end)";
	public static final String UNIDENTIFIED_ERROR_CODE = "unidentified error code (immediate inspection required)";

	public static final String AMERIA_PAYMENT_FIELDS_AUTH_CODE = "paymentFieldsResponseAuthCode";
	public static final String AMERIA_PAYMENT_FIELDS_OPAQUE = "paymentFieldsResponseOpaque";
	public static final String AMERIA_PAYMENT_FIELDS_ORDER_ID = "paymentFieldsResponseOrderId";
	public static final String AMERIA_PAYMENT_FIELDS_STATUS = "paymentFieldsResponseStatus";
	public static final String AMERIA_PAYMENT_FIELDS_TNX_RESULTS_CODE = "paymentFieldsResponseTnxResultsCode";
	public static final String AMERIA_PAYMENT_FIELDS_ERROR_CODE = "paymentFieldsResponseErrorCode";
	public static final String AMERIA_PAYMENT_FIELDS_ERROR_DESC = "paymentFieldsResponseErrorDescription";

	public enum ameriaPaymentState {
		PAYMENT_STARTED("payment_started"), PAYMENT_APPROVED("payment_approved"), PAYMENT_DECLINED("payment_declined"),

		PAYMENT_DEPOSITED("payment_deposited"), PAYMENT_REFUNDED("payment_refunded"), PAYMENT_AUTO_AUTHORIZED(
				"payment_autoauthorized"),

		PAYMENT_VOID("payment_void");

		private final String code;

		ameriaPaymentState(String code) {
			this.code = code;
		}

		public String getCode() {
			return this.code;
		}

	}

	public static final String AMD = "AMD";
	public static final String USD = "USD";
	public static final String RUB = "RUB";
	public static final String EUR = "EUR";

	private static final String AMD_ISO_CODE = "051";
	private static final String USD_ISO_CODE = "840";
	private static final String RUB_ISO_CODE = "643";
	private static final String EUR_ISO_CODE = "978";

	public static String getErrorDescription(String code) {

		String errorDescription;
		switch (code) {
		case "0-1":
			errorDescription = RESPONSE_CODE_1_0;
			break;
		case "0-100":
			errorDescription = RESPONSE_CODE_0_100;
			break;
		case "0-2000":
			errorDescription = RESPONSE_CODE_0_2000;
			break;
		case "0-2001":
			errorDescription = RESPONSE_CODE_0_2001;
			break;
		case "0-20010":
			errorDescription = RESPONSE_CODE_0_20010;
			break;
		case "0-2002":
			errorDescription = RESPONSE_CODE_0_2002;
			break;
		case "0-2004":
			errorDescription = RESPONSE_CODE_0_2004;
			break;
		case "0-2005":
			errorDescription = RESPONSE_CODE_0_2005;
			break;
		case "0-2006":
			errorDescription = RESPONSE_CODE_0_2006;
			break;
		case "0-2007":
			errorDescription = RESPONSE_CODE_0_2007;
			break;
		case "0-2011":
			errorDescription = RESPONSE_CODE_0_2011;
			break;
		case "0-2012":
			errorDescription = RESPONSE_CODE_0_2012;
			break;
		case "0-2013":
			errorDescription = RESPONSE_CODE_0_2013;
			break;
		case "0-2015":
			errorDescription = RESPONSE_CODE_0_2015;
			break;
		case "0-2016":
			errorDescription = RESPONSE_CODE_0_2016;
			break;
		case "0-2018":
			errorDescription = RESPONSE_CODE_0_2018;
			break;
		case "0-2019":
			errorDescription = RESPONSE_CODE_0_2019;
			break;
		case "0-9000":
			errorDescription = RESPONSE_CODE_0_9000;
			break;
		case "00":
			errorDescription = RESPONSE_CODE_00;
			break;
		case "01":
			errorDescription = RESPONSE_CODE_01;
			break;
		case "0100":
			errorDescription = RESPONSE_CODE_0100;
			break;
		case "01001":
			errorDescription = RESPONSE_CODE_01001;
			break;
		case "0101":
			errorDescription = RESPONSE_CODE_0101;
			break;
		case "0109":
			errorDescription = RESPONSE_CODE_0109;
			break;
		case "0110":
			errorDescription = RESPONSE_CODE_0110;
			break;
		case "0111":
			errorDescription = RESPONSE_CODE_0111;
			break;
		case "0116":
			errorDescription = RESPONSE_CODE_0116;
			break;
		case "0120":
			errorDescription = RESPONSE_CODE_0120;
			break;
		case "0121":
			errorDescription = RESPONSE_CODE_0121;
			break;
		case "0123":
			errorDescription = RESPONSE_CODE_0123;
			break;
		case "0125":
			errorDescription = RESPONSE_CODE_0125;
			break;
		case "0151017":
			errorDescription = RESPONSE_CODE_0151017;
			break;
		case "0151018":
			errorDescription = RESPONSE_CODE_0151018;
			break;
		case "0151019":
			errorDescription = RESPONSE_CODE_0151019;
			break;
		case "02001":
			errorDescription = RESPONSE_CODE_02001;
			break;
		case "02003":
			errorDescription = RESPONSE_CODE_02003;
			break;
		case "02005":
			errorDescription = RESPONSE_CODE_02005;
			break;
		case "0208":
			errorDescription = RESPONSE_CODE_0208;
			break;
		case "0209;":
			errorDescription = RESPONSE_CODE_0209;
			break;
		case "0341014":
			errorDescription = RESPONSE_CODE_0341014;
			break;
		case "0341041":
			errorDescription = RESPONSE_CODE_0341041;
			break;
		case "05":
			errorDescription = RESPONSE_CODE_05;
			break;
		case "071015":
			errorDescription = RESPONSE_CODE_071015;
			break;
		case "08204":
			errorDescription = RESPONSE_CODE_08204;
			break;
		case "09002":
			errorDescription = RESPONSE_CODE_09002;
			break;
		case "09003":
			errorDescription = RESPONSE_CODE_09003;
			break;
		case "09004":
			errorDescription = RESPONSE_CODE_09004;
			break;
		case "09007":
			errorDescription = RESPONSE_CODE_09007;
			break;

		case "09009":
			errorDescription = RESPONSE_CODE_09009;
			break;
		case "09010":
			errorDescription = RESPONSE_CODE_09010;
			break;
		case "09013":
			errorDescription = RESPONSE_CODE_09013;
			break;
		case "09014":
			errorDescription = RESPONSE_CODE_09014;
			break;
		case "0999":
			errorDescription = RESPONSE_CODE_0999;
			break;
		case "02":
			errorDescription = RESPONSE_CODE_02;
			break;
		case "03":
			errorDescription = RESPONSE_CODE_03;
			break;
		case "04":
			errorDescription = RESPONSE_CODE_04;
			break;
		case "06":
			errorDescription = RESPONSE_CODE_06;
			break;
		case "07":
			errorDescription = RESPONSE_CODE_07;
			break;
		case "20":
			errorDescription = RESPONSE_CODE_20;
			break;
		case "30":
			errorDescription = RESPONSE_CODE_30;
			break;
		case "550":
			errorDescription = RESPONSE_CODE_550;
			break;
		case "514":
			errorDescription = RESPONSE_CODE_514;
			break;
		case "513":
			errorDescription = RESPONSE_CODE_513;
			break;
		case "560":
			errorDescription = RESPONSE_CODE_560;
			break;
		case "520":
			errorDescription = RESPONSE_CODE_520;
			break;
		case "50":
			errorDescription = RESPONSE_CODE_50;
			break;
		case "510":
			errorDescription = RESPONSE_CODE_510;
			break;
		case "500":
			errorDescription = RESPONSE_CODE_500;
			break;
		case "EMPTY_CODE":
			errorDescription = UNIDENTIFIED_ERROR_CODE;
			break;
		default:
			errorDescription = UNIDENTIFIED_ERROR;
			break;
		}

		return errorDescription;
	}

	public static IPGConfigsDTO getIPGConfigs(String merchantID, String userName, String password) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);
		oIPGConfigsDTO.setUsername(userName);
		oIPGConfigsDTO.setPassword(password);

		return oIPGConfigsDTO;
	}

	public static String getCurrencyCode(String currency) throws ModuleException {
		if (AMD.contentEquals(currency)) {
			return AMD_ISO_CODE;
		}

		if (USD.contentEquals(currency)) {
			return USD_ISO_CODE;
		}

		if (RUB.contentEquals(currency)) {
			return RUB_ISO_CODE;
		}

		if (EUR.contentEquals(currency)) {
			return EUR_ISO_CODE;
		}

		throw new ModuleException("paymentbroker.ameria.currency.code.invalid");
	}

	public static boolean validateReceiptMap(Map receiptyMap, String paymentId, int orderId) throws ModuleException {

		if (receiptyMap != null && !StringUtil.isNullOrEmpty(paymentId)
				&& AmeriaBankPaymentUtil.COMMON_RESP_SUCCESS_CODE.contentEquals((String) receiptyMap.get("respcode"))
				&& paymentId.equalsIgnoreCase((String) receiptyMap.get("paymentid"))
				&& orderId == Integer.valueOf((String) receiptyMap.get("orderID"))) {
			return true;

		}

		return false;
		// throw new ModuleException("paymentbroker.ameria.payment.receipt.invalid");
	}

	public static void validatePaymentFieldsResponse(AmeriaBankPaymentFieldsResponse ameriaBankPaymentFieldsResponse,
			BigDecimal paymentAmount, String opaque, String currencyCode, boolean isPaymentFlow) throws ModuleException {

		String errorCode = null;

		// deposit amount won't match if refund is done, not used for the check

		if (paymentAmount.compareTo(new BigDecimal(ameriaBankPaymentFieldsResponse.getPaymentAmount())) != 0) {
			errorCode = "paymentbroker.ameria.payment.amount.mismatch";
		}

		if (paymentAmount.compareTo(ameriaBankPaymentFieldsResponse.getApprovedAmount()) != 0) {
			errorCode = "paymentbroker.ameria.payment.approvedamount.mismatch";
		}

		if (opaque != null && !opaque.contentEquals(ameriaBankPaymentFieldsResponse.getOpaque())) {
			errorCode = "paymentbroker.ameria.payment.opaque.mismatch";
		}

		if (currencyCode != null && !currencyCode.contentEquals(ameriaBankPaymentFieldsResponse.getCurrency())) {
			errorCode = "paymentbroker.ameria.payment.currency.mismatch ";
		}

		// only 3,5 are allowed (Virtual Arca, Binding transaction not applicable)
		if (!(ameriaBankPaymentFieldsResponse.getPaymentType() == 3 || ameriaBankPaymentFieldsResponse.getPaymentType() == 5)) {
			errorCode = "paymentbroker.ameria.payment.type.invalid";
		}

		if (isPaymentFlow) {

			if (!ameriaPaymentState.PAYMENT_DEPOSITED.getCode().contentEquals(ameriaBankPaymentFieldsResponse.getPaymentState())) {
				errorCode = "paymentbroker.ameria.payment.not.deposited";
			}
		} // refund case
		else if (!(ameriaPaymentState.PAYMENT_DEPOSITED.getCode()
				.contentEquals(ameriaBankPaymentFieldsResponse.getPaymentState()) || ameriaPaymentState.PAYMENT_REFUNDED
				.getCode().contentEquals(ameriaBankPaymentFieldsResponse.getPaymentState()))) {
			errorCode = "paymentbroker.ameria.refund.prestate.invalid";

		}

		if (errorCode != null) {
			ModuleException me = new ModuleException(errorCode);
			me.setErrorType(ErrorType.PAYMENT_ERROR);
			throw me;
		}

	}

	public static void getErrorDescription(String responseCode, String responseMessage) {

		if (StringUtil.isNullOrEmpty(responseCode)) {
			responseCode = "EMPTY_CODE";
		}
		if (StringUtil.isNullOrEmpty(responseMessage)) {
			responseMessage = getErrorDescription(responseCode);
		}

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("error-code: ");
		stringBuilder.append(responseCode);
		stringBuilder.append("error-message: ");
		stringBuilder.append(responseMessage);

		responseMessage = stringBuilder.toString();
	}

	public static AmeriaBankCommonRequest buildPaymentFieldsRequest(String merchantId, String user, String password,
			BigDecimal paymentAmount, int orderId) {

		AmeriaBankCommonRequest ameriaBankPaymentFieldsRequest = new AmeriaBankCommonRequest();

		ameriaBankPaymentFieldsRequest.setClientID(merchantId);
		ameriaBankPaymentFieldsRequest.setUserName(user);
		ameriaBankPaymentFieldsRequest.setUserPassword(password);
		ameriaBankPaymentFieldsRequest.setPaymentAmount(paymentAmount);
		ameriaBankPaymentFieldsRequest.setOrderId(orderId);
		return ameriaBankPaymentFieldsRequest;
	}
}
