package com.isa.thinair.paymentbroker.core.bl.cmi.fatourati.util;

import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.Random;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReq;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenRes;

public class FatouratiUtil {

	private static final String HASH_ALG = "MD5";

	private FatouratiUtil() {

	}

	public static byte[] getHash(PayByRefFatGenReq request, String macControl) throws ModuleException {
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		StringBuilder builder = new StringBuilder();
		builder.append(request.getCreancierId());
		builder.append(request.getDateServeurCreancier());
		builder.append(request.getAction().getValue());
		builder.append(request.getNbrTotalArticles());
		String formatedTotalArticles = decimalFormat.format(request.getMontantTotalArticles().floatValue());
		builder.append(formatedTotalArticles);

		for (PanierClientType clientType : request.getPanierClient()) {
			builder.append(clientType.getIdArticle());
		}

		builder.append(macControl);
		String plainText = builder.toString();
		MessageDigest mdAlgorithm;
		try {
			mdAlgorithm = MessageDigest.getInstance(HASH_ALG);
			mdAlgorithm.update(plainText.getBytes());

			byte[] digest = mdAlgorithm.digest();
			return digest;
		} catch (Exception e) {
			throw new ModuleException("Failed to generate MAC value");
		}

	}

	// temp method
	public static PayByRefFatGenRes getASampleResult() {
		PayByRefFatGenRes result = new PayByRefFatGenRes();
		result.setCodeRetour("000");
		result.setMsgRetour("Success");
		PanierClientType clientType = new PanierClientType();
		long fat = 1030064;
		Random rand = new Random();
		fat = fat + rand.nextInt();
		clientType.setRefFatourati(String.valueOf(fat));
		PanierClientType[] arr = { clientType };
		result.setPanierClientRes(arr);
		return result;

	}

}
