package com.isa.thinair.paymentbroker.core.bl.eDirham;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.core.bl.ogone.OgoneRequest;

public class EDirhamPaymentUtils {

	private static Log log = LogFactory.getLog(EDirhamPaymentUtils.class);

	public static final String TRANSACTION_SUCCESSFULL = "0000";

	public static final String TRANSACTION_ALREADY_CONFIRMED = "6514";

	public static final String DATA_SEPARATOR = "&";

	public static final String VALUE_SEPARATOR = "=";

	public static final String PAY_DESCRIPTION = "AirArabiaPayment";

	public static enum EDIRHAM_OPERATION {
		SERVICE_INQ(28), PAY_WEB(0), PRE_AUTH(2), COMPLETE(33), AUTO_UPDATE(13), REFUND(37), VOID(30);

		int operation;

		EDIRHAM_OPERATION(int operation) {
			this.operation = operation;
		}

		public String getOperation() {
			return Integer.toString(operation);
		}
	};

	public static enum EDIRHAM_EXTRACT_STATUS {
		CONFIRM(0), TO_EXTRACT(1), NOTTO_EXTRACT(3), ACK(4), CUTOFF(5), UNCONF(6);

		int extractStatus;

		EDIRHAM_EXTRACT_STATUS(int extractStatus) {
			this.extractStatus = extractStatus;
		}

		public int getExtractStatus() {
			return extractStatus;
		}
	};

	public static String getTransactionReqTime() {
		Date currentDate = new Date();
		SimpleDateFormat formater = new SimpleDateFormat("ddMMyyyyHHmmss");

		return formater.format(currentDate);

	}

	public static String computeSecureHash(Map<String, String> postDataMap, String secureKey, EDIRHAM_OPERATION operation)
			throws Exception {

		StringBuilder stringToHash = new StringBuilder();
		stringToHash.append(secureKey);
		stringToHash.append(postDataMap.get(EDirhamRequest.ACTION));
		if (operation.equals(EDIRHAM_OPERATION.SERVICE_INQ)) {
			stringToHash.append(postDataMap.get(EDirhamRequest.APPLICATIONNUMBER));
			stringToHash.append(postDataMap.get(EDirhamRequest.BANKID));
			stringToHash.append(postDataMap.get(EDirhamRequest.CURRENCY));
			stringToHash.append(postDataMap.get(EDirhamRequest.ESERVICEMAINCODESUBCODE_1));
			stringToHash.append(postDataMap.get(EDirhamRequest.ESERVICEQUANTITY_1));
			stringToHash.append(postDataMap.get(EDirhamRequest.ESERVICEPRICE_1));
			stringToHash.append(postDataMap.get(EDirhamRequest.EXTRAFIELDS_INTENDEDEDIRHAMSERVICE));
			stringToHash.append(postDataMap.get(EDirhamRequest.MERCHANTID));
			stringToHash.append(postDataMap.get(EDirhamRequest.PAYMENTMETHODTYPE));
			stringToHash.append(postDataMap.get(EDirhamRequest.TERMINALID));
			stringToHash.append(postDataMap.get(EDirhamRequest.TRANSACTIONREQUESTDATE));
			stringToHash.append(postDataMap.get(EDirhamRequest.UNIQUETRANSACTIONID));
			stringToHash.append(postDataMap.get(EDirhamRequest.VERSION));
		} else if (operation.equals(EDIRHAM_OPERATION.PAY_WEB)) {
			// stringToHash.append(postDataMap.get(EDirhamRequest.AMOUNT));
			stringToHash.append(postDataMap.get(EDirhamRequest.APPLICATIONNUMBER));
			stringToHash.append(postDataMap.get(EDirhamRequest.BANKID));
			stringToHash.append(postDataMap.get(EDirhamRequest.CURRENCY));
			stringToHash.append(postDataMap.get(EDirhamRequest.ESERVICEMAINCODESUBCODE_1));
			stringToHash.append(postDataMap.get(EDirhamRequest.ESERVICEPRICE_1));
			stringToHash.append(postDataMap.get(EDirhamRequest.ESERVICEQUANTITY_1));
			stringToHash.append(postDataMap.get(EDirhamRequest.EXTRAFIELDS_F14));
			stringToHash.append(postDataMap.get(EDirhamRequest.EXTRAFIELDS_F16));
			stringToHash.append(postDataMap.get(EDirhamRequest.EXTRAFIELDS_F18));
			stringToHash.append(postDataMap.get(EDirhamRequest.EXTRAFIELDS_INTENDEDEDIRHAMSERVICE));
			stringToHash.append(postDataMap.get(EDirhamRequest.LANGUAGE));
			stringToHash.append(postDataMap.get(EDirhamRequest.MERCHANTID));
			stringToHash.append(postDataMap.get(EDirhamRequest.MERCHANTMODULESESSIONID));
			// stringToHash.append(postDataMap.get(EDirhamRequest.NATIONALID));
			stringToHash.append(postDataMap.get(EDirhamRequest.PUN));
			stringToHash.append(postDataMap.get(EDirhamRequest.PAYMENTDESCRIPTION));
			stringToHash.append(postDataMap.get(EDirhamRequest.TRANSACTIONREQUESTDATE));
			// stringToHash.append(postDataMap.get(EDirhamRequest.VERSION));
		} else if (operation.equals(EDIRHAM_OPERATION.AUTO_UPDATE)) {
			stringToHash.append(postDataMap.get(EDirhamRequest.BANKID));
			stringToHash.append(postDataMap.get(EDirhamRequest.MERCHANTID));
			stringToHash.append(postDataMap.get(EDirhamRequest.PUN));
			stringToHash.append(postDataMap.get(EDirhamRequest.TRANSACTIONREQUESTDATE));
		} else if (operation.equals(EDIRHAM_OPERATION.REFUND)) {
			stringToHash.append(postDataMap.get(EDirhamRequest.BANKID));
			stringToHash.append(postDataMap.get(EDirhamRequest.CURRENCY));
			stringToHash.append(postDataMap.get(EDirhamRequest.FIELD61_SERVICECODEQUANTITY));
			stringToHash.append(postDataMap.get(EDirhamRequest.FIELD61_SERVICEMAINCODESUBCODE));
			stringToHash.append(postDataMap.get(EDirhamRequest.FIELD63_ORIGINALTRANSACTIONUNIQUEID));
			stringToHash.append(postDataMap.get(EDirhamRequest.MERCHANTID));
			stringToHash.append(postDataMap.get(EDirhamRequest.TERMINALID));
			stringToHash.append(postDataMap.get(EDirhamRequest.TRANSACTIONAMOUNT));
			stringToHash.append(postDataMap.get(EDirhamRequest.TRANSACTIONREQUESTDATE));
			stringToHash.append(postDataMap.get(EDirhamRequest.UNIQUETRANSACTIONID));
		}

		return computeSHA2(secureKey, stringToHash.toString());
	}

	public static String computeSHA2(String key, String text) throws NoSuchAlgorithmException, InvalidKeyException,
			IllegalStateException, UnsupportedEncodingException {
		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
		sha256_HMAC.init(secret_key);

		String hash = new String(Hex.encodeHex(sha256_HMAC.doFinal(text.getBytes("UTF-8"))));
		hash = new String(Base64.encodeBase64(hash.getBytes()));
		return hash;
	}

	private static void prepareCommonRequest(HttpClient client, PostMethod method, Map<String, String> postDataMap) {
		HostConfiguration hcon = new HostConfiguration();

		if (postDataMap.get("USE_PROXY").equals("Y")) {
			hcon.setProxy(postDataMap.get("PROXY_HOST"), Integer.valueOf(postDataMap.get("PROXY_PORT")));
		}

		client.setHostConfiguration(hcon);
		method.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");

		for (String key : postDataMap.keySet()) {

			if (log.isDebugEnabled() && key.equalsIgnoreCase("PROXY_HOST") || key.equalsIgnoreCase("PROXY_PORT")
					|| key.equalsIgnoreCase("URL") || key.equalsIgnoreCase("USE_PROXY")) {
				log.debug("E-Dirham Proxy Details : " + key + " : " + postDataMap.get(key));
			}

			if (!key.equals("PROXY_HOST") && !key.equals("PROXY_PORT") && !key.equals("URL") && !key.equals("USE_PROXY")) {
				method.addParameter(key, postDataMap.get(key));
				if (log.isDebugEnabled()) {
					if (!key.equalsIgnoreCase(OgoneRequest.CARDNO) && !key.equalsIgnoreCase(OgoneRequest.CVC)
							&& !key.equalsIgnoreCase(OgoneRequest.ED) && !key.equalsIgnoreCase(OgoneRequest.PSWD)) {
						log.debug("E-Dirham Request : " + key + " : " + postDataMap.get(key));
					}
				}
			}
		}
	}

	public static EDirhamResponse getEDirhamResponse(Map<String, String> postDataMap, String secretKey,
			EDIRHAM_OPERATION operation) {

		HttpClient client = new HttpClient();
		EDirhamResponse eDirhamResponse = new EDirhamResponse();
		PostMethod method = new PostMethod(postDataMap.get("URL"));
		prepareCommonRequest(client, method, postDataMap);

		String responseString = null;
		try {
			log.debug("Executing the E-Dirham Request for URL : " + postDataMap.get("URL"));
			int returnCode = client.executeMethod(method);

			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
				log.debug("The Post method is not implemented by this URI");
				return null;
			} else {
				responseString = method.getResponseBodyAsString();
				log.debug("XML Response : " + responseString);

				eDirhamResponse = parseResponse(responseString, operation);
				String genSecureHash = generateSecureHashFromResponse(eDirhamResponse, secretKey, operation);
				if (genSecureHash.equals(eDirhamResponse.getSecureHash())) {
					return eDirhamResponse;
				} else {
					log.warn("Wrong secure hash in response genSecureHash:" + genSecureHash + ", received SecureHash: "
							+ eDirhamResponse.getSecureHash());
					return new EDirhamResponse();
				}
			}
		} catch (Exception e) {
			log.debug("Exception while parsing the XML response " + e.getMessage(), e);
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	public static String generateSecureHashFromResponse(EDirhamResponse eDirhamResponse, String secretKey,
			EDIRHAM_OPERATION operation)
 throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException,
			IllegalStateException {
		StringBuilder stringToHash = new StringBuilder();
		stringToHash.append(secretKey);
		if (operation.equals(EDIRHAM_OPERATION.SERVICE_INQ)) {
			stringToHash.append(eDirhamResponse.getBankId());
			stringToHash.append(eDirhamResponse.getCollectionCenterFees());
			stringToHash.append(eDirhamResponse.getCurrency());
			stringToHash.append(eDirhamResponse.geteDirhamFees());
			stringToHash.append(eDirhamResponse.getAmount());
			stringToHash.append(eDirhamResponse.getTotalAmount());
			stringToHash.append(eDirhamResponse.getFees());
			stringToHash.append(eDirhamResponse.getMainCodeSubCode());
			stringToHash.append(eDirhamResponse.getEnglishDescription());
			stringToHash.append(eDirhamResponse.getMinistryEnglishName());
			stringToHash.append(eDirhamResponse.getMerchantId());
			stringToHash.append(eDirhamResponse.getPaymentMethodType());
			stringToHash.append(eDirhamResponse.getRetrievalRefNumber());
			stringToHash.append(eDirhamResponse.getStatus());
			stringToHash.append(eDirhamResponse.getStatusMessage());
			stringToHash.append(eDirhamResponse.getTerminalId());
			stringToHash.append(eDirhamResponse.getTransactionAmount());
			stringToHash.append(eDirhamResponse.getTransactionResponseDate());
			stringToHash.append(eDirhamResponse.getPun());
		} else if (operation.equals(EDIRHAM_OPERATION.PAY_WEB)) {
			stringToHash.append(eDirhamResponse.getAmount());
			stringToHash.append(eDirhamResponse.getBankId());
			stringToHash.append(eDirhamResponse.getCollectionCenterFees());
			stringToHash.append(eDirhamResponse.getConfirmationID());
			stringToHash.append(eDirhamResponse.getCurrency());
			stringToHash.append(eDirhamResponse.geteDirhamFees());
			stringToHash.append(eDirhamResponse.geteServiceData());
			stringToHash.append(eDirhamResponse.getMerchantId());
			stringToHash.append(eDirhamResponse.getMerchantModuleSessionId());
			stringToHash.append(eDirhamResponse.getPun());
			stringToHash.append(eDirhamResponse.getPaymentMethodType());
			stringToHash.append(eDirhamResponse.getStatus());
			stringToHash.append(eDirhamResponse.getStatusMessage());
			stringToHash.append(eDirhamResponse.getTerminalId());
			stringToHash.append(eDirhamResponse.getTransactionAmount());
			stringToHash.append(eDirhamResponse.getTransactionResponseDate());
		} else if (operation.equals(EDIRHAM_OPERATION.AUTO_UPDATE)) {
			stringToHash.append(eDirhamResponse.getAction());
			stringToHash.append(eDirhamResponse.getBankId());
			stringToHash.append(eDirhamResponse.getCollectionCenterFees());
			stringToHash.append(eDirhamResponse.getConfirmationID());
			stringToHash.append(eDirhamResponse.getCurrency());
			stringToHash.append(eDirhamResponse.geteDirhamFees());
			stringToHash.append(eDirhamResponse.geteServiceData());
			stringToHash.append(eDirhamResponse.getExtractStatus());
			stringToHash.append(eDirhamResponse.getMerchantId());
			stringToHash.append(eDirhamResponse.getMerchantModuleSessionId());
			stringToHash.append(eDirhamResponse.getOriginalTransactionStatus());
			stringToHash.append(eDirhamResponse.getOriginalTransactionStatusMessage());
			stringToHash.append(eDirhamResponse.getPun());
			stringToHash.append(eDirhamResponse.getStatus());
			stringToHash.append(eDirhamResponse.getStatusMessage());
			stringToHash.append(eDirhamResponse.getTransactionAmount());
			stringToHash.append(eDirhamResponse.getTransactionResponseDate());
		} else if (operation.equals(EDIRHAM_OPERATION.REFUND)) {
			stringToHash.append(eDirhamResponse.getMerchantId());
			stringToHash.append(eDirhamResponse.getRetrievalRefNumber());
			stringToHash.append(eDirhamResponse.getStatus());
			stringToHash.append(eDirhamResponse.getStatusMessage());
			stringToHash.append(eDirhamResponse.getTransactionAmount());
			stringToHash.append(eDirhamResponse.getTransactionResponseDate());
			stringToHash.append(eDirhamResponse.getPun());
		}

		return computeSHA2(secretKey, stringToHash.toString());
	}

	public static EDirhamResponse parseResponse(String responseString, EDIRHAM_OPERATION operation) {

		Map<String, String> responseParamMap = new HashMap<String, String>();
		if (responseString != null) {
			String parameters[] = responseString.split(DATA_SEPARATOR);
			for (int i = 0; i < parameters.length; i++) {
				String paramValue[] = parameters[i].split(VALUE_SEPARATOR, 2);
				responseParamMap.put(paramValue[0], paramValue[1]);
			}
		}
		EDirhamResponse eDirhamResp = new EDirhamResponse();
		eDirhamResp.setResponse(responseParamMap, EDirhamPaymentUtils.EDIRHAM_OPERATION.AUTO_UPDATE.getOperation());
		return eDirhamResp;
	}

	public static String getFormattedAmount(String amount, int noOfDecimals) {
		if (SystemPropertyUtil.checkRemoveCardPaymentDecimals()) {
			amount = Long.toString(Math.round(Double.parseDouble(amount)));
		}
				
		return amount.toString();		
	}

	public static String getErrorInfo(EDirhamResponse response) {
		// TODO Auto-generated method stub
		return null;
	}

	public static IPGConfigsDTO getIPGConfigs(String merchantId, String user, String password) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantId);
		oIPGConfigsDTO.setUsername(user);
		oIPGConfigsDTO.setPassword(password);

		return oIPGConfigsDTO;
	}

	public static String[] getMappedUnifiedError(Object error) {

		String[] errorDesc = new String[2];

		if (error instanceof String) {

			String errorString = error.toString();

			if (errorString.equalsIgnoreCase("0001")) {
				errorDesc[0] = "0001";
				errorDesc[1] = "Transaction is completed successfully but early auto update failed";
			} else if (errorString.equalsIgnoreCase("2093") || errorString.equalsIgnoreCase("2198") || errorString.equalsIgnoreCase("2199")) {
				errorDesc[0] = "2093";
				errorDesc[1] = "Canceled before submitting card details";
			} else if (errorString.equalsIgnoreCase("2011")) {
				errorDesc[0] = "2011";
				errorDesc[1] = "PUN for payment already exists on e-Dirham System";
			} else { // Default error from PGW
				errorDesc[0] = "DEF_ERR";
				errorDesc[1] = "Error occurred at payment payment gateway page";
			}
		}

		return errorDesc;
	}

}
