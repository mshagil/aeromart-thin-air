package com.isa.thinair.paymentbroker.core.bl.ogone;

public class OgoneRequest {

	public static final String PSPID = "PSPID";

	public static final String ORDERID = "ORDERID";

	public static final String USERID = "USERID";

	public static final String PSWD = "PSWD";

	public static final String AMOUNT = "AMOUNT";

	public static final String CURRENCY = "CURRENCY";

	public static final String CARDNO = "CARDNO";

	public static final String ED = "ED";

	public static final String COM = "COM";

	public static final String SHASIGN = "SHASIGN";

	public static final String CVC = "CVC";

	public static final String OPERATION = "OPERATION";

	public static final String WITHROOT = "WITHROOT";

	public static final String RTIMEOUT = "RTIMEOUT";

	public static final String ECI = "ECI";

	public static final String FLAG3D = "FLAG3D";

	public static final String WIN3DS = "WIN3DS";

	public static final String ACCEPTURL = "ACCEPTURL";

	public static final String DECLINEURL = "DECLINEURL";

	public static final String EXCEPTIONURL = "EXCEPTIONURL";

	public static final String LANGUAGE = "LANGUAGE";

	public static final String HTTP_USER_AGENT = "HTTP_USER_AGENT";

	public static final String REMOTE_ADDR = "REMOTE_ADDR";

	public static final String TITLE = "TITLE";

	public static final String TXT_COLOR = "TXTCOLOR";

	public static final String TBL_BG_COLOR = "TBLBGCOLOR";

	public static final String TBL_TXT_COLOR = "TBLTXTCOLOR";

	public static final String BUTTON_BG_COLOR = "BUTTONBGCOLOR";

	public static final String BUTTON_TXT_COLOR = "BUTTONTXTCOLOR";

	public static final String FONT_TYPE = "FONTTYPE";

	public static final String LOGO = "LOGO";

	public static final String PAYMENT_METHOD = "PM";

	public static final String PAYMENT_BRAND = "BRAND";

	/* Fraud Checking data */
	
	public static final String AIRLINE_NAME = "AIAIRNAME"; 
	
	public static final String PASSENGER_NAME = "AIPASNAME"; 
	
	public static final String CUSTOMER_NAME = "CN";

	public static final String CUSTOMER_EMAIL = "EMAIL";

	public static final String CUSTOMER_TELEPHONENO = "OWNERTELNO";

	public static final String CUSTOMER_COUNTRY = "OWNERCTY";

	public static final String CUSTOMER_ADDRESS = "OWNERADDRESS";

	public static final String CUSTOMER_TOWN = "OWNERTOWN";

	public static final String CUSTOMER_ZIP = "OWNERZIP";

	public static final String DATATYPE = "DATATYPE";

	public static final String DATATYPE_TRAVEL = "TRAVEL";

	public static final String TICKETNO = "AITINUM";

	public static final String TICKETDATE = "AITIDATE";

	public static String DEPARTURE_AIRPORT = "AIORCITY";

	public static String DEPARTURE_AIRPORT_NAME = "AIORCITYL";

	public static String ARRIVAL_AIRPORT = "AIDESTCITY";

	public static String ARRIVAL_AIRPORT_NAME = "AIDESTCITYL";

	public static String CARRIER_CODE = "AICARRIER";

	public static String FLIGHTNO = "AIFLNUM";

	public static String FLIGHT_DATE = "AIFLDATE";
	/* COMPLUS is used to pass returned parameter in the feedback request */
	public static String COMPLUS = "COMPLUS";

	public static String DYNAMIC_TEMPLATE = "TP";
	
	public static String NO_OF_STOPOVERS = "AISTOPOV";
	
	public static String CLASS_OF_SERVICE = "AICLASS";
	
	public static final String ALIAS = "ALIAS";

	public OgoneRequest() {

	}

}
