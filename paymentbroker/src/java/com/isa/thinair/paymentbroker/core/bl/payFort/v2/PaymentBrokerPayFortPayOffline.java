package com.isa.thinair.paymentbroker.core.bl.payFort.v2;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.PayFortOnlineParam;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class PaymentBrokerPayFortPayOffline extends PaymentBrokerPayFortOfflineTemplate {

	private static final String BILL_NUMBER = "billNumber";
	private static final String BILL_NUMBER_LABEL_KEY = "fawry_Reference_Number";
	private static final String PAYFORT_FAWRY_BILL_NUMBER_NOTIFICATION_VM = "payfort_fawry_bill_number_notification";

	private String paymentPartner;

	private boolean isQueryingSupported;

	private static final ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		log.debug("[PaymentBrokerPayFortPayOffline::getRequestData()] Begin ");

		String merchantRefID = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Map<String, String> postDataMap = getPostDataMap(ipgRequestDTO, merchantRefID);
		String sessionID = StringUtils.defaultString(ipgRequestDTO.getSessionID());
		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantRefID,
				ipgRequestDTO.getApplicationTransactionId(), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", ipgRequestDTO.getIpgIdentificationParamsDTO().getFQIPGConfigurationName(),
				null, false);

		Map<String, String> response = getResponseFromPayFort(postDataMap, getIpgURL());

		if (response != null && StringUtils.isNotEmpty(response.get(PayFortOnlineParam.BILL_NUMBER.getName()))) {
			String billNumber = response.get(PayFortOnlineParam.BILL_NUMBER.getName());
			ccTransaction.setResponseText(PaymentBrokerUtils.getRequestDataAsString(response));
			ccTransaction.setAidCccompnay(billNumber);
			ccTransaction.setTransactionId(billNumber);
			ipgRequestResultsDTO.setBillNumber(billNumber);
			ipgRequestResultsDTO.setBillNumberLabelKey(BILL_NUMBER_LABEL_KEY);

			if (isSignatureValid(response, getShaResponsePhrase()) && isBillCreationSuccess(response)) {
				// For display purpose
				logIfDebugEnabled(
						"PaymentBrokerPayFortPayOffline PG Request Data : " + strRequestParams + ", sessionID : " + sessionID);
				ipgRequestResultsDTO.setRequestData(strRequestParams);
				ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
				ipgRequestResultsDTO.setPostDataMap(response);

				if (isSendEmailNotification()) {
					sendBillNumberToContact(ipgRequestDTO, billNumber);
				}

			} else {
				ccTransaction.setErrorSpecification(StringUtils
						.defaultString(response.get(PayFortOnlineParam.RESPONSE_CODE.getName()), "Signature Mismatch"));
				ipgRequestResultsDTO.setPaymentBrokerRefNo(0);
				ipgRequestResultsDTO.setErrorCode(StringUtils
						.defaultString(response.get(PayFortOnlineParam.RESPONSE_MESSAGE.getName()), "Signature Mismatch"));
				log.error("PaymentBrokerPayFortPayOffline PG Request Data fails with error message : " + ipgRequestResultsDTO
						.getErrorCode());
			}
			PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(ccTransaction);
		}
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantRefID);
		log.debug("PayFort Online Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		return ipgRequestResultsDTO;
	}

	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	// During the implementation querying the fawry records was not supported by payfort side
	// Once they implemented the querying functionality it is possible to enable scheduler job processing
	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag)
			throws ModuleException {
		if (isQueryingSupported) {
			return super.resolvePartialPayments(colIPGQueryDTO, refundFlag);
		}
		return null;
	}

	// Refund is not enabled for FAWRY option
	// Query operation is not enabled at the moment once it is enabled can remove this method and
	// #resolvePartialPayments method (Super class will handle logic once query operation is available)
	@Override
	public boolean isEnableRefundByScheduler() {
		return isQueryingSupported;
	}

	private Map<String, String> getPostDataMap(IPGRequestDTO ipgRequestDTO, String refNo) throws ModuleException {
		Map<String, String> postDataMap = new LinkedHashMap<>();
		postDataMap.put(PayFortOnlineParam.SERVICE_COMMAND.getName(), "BILL_PRESENTMENT");
		postDataMap.put(PayFortOnlineParam.ACCESS_CODE.getName(), getMerchantAccessCode());
		postDataMap.put(PayFortOnlineParam.AMOUNT.getName(), getISOFormattedAmount(ipgRequestDTO.getAmount()));
		postDataMap.put(PayFortOnlineParam.MERCHANT_IDENTIFIER.getName(), getMerchantId());
		postDataMap.put(PayFortOnlineParam.MERCHANT_REFERENCE.getName(), refNo);
		postDataMap.put(PayFortOnlineParam.CURRENCY.getName(),
				ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		postDataMap.put(PayFortOnlineParam.LANGUAGE.getName(), getLocale());
		postDataMap.put(PayFortOnlineParam.PAYMENT_PARTNER.getName(), paymentPartner);

		if (StringUtils.isNotEmpty(ipgRequestDTO.getExpiryDate())) {
			postDataMap.put(PayFortOnlineParam.REQUEST_EXPIRY_DATE.getName(), ipgRequestDTO.getExpiryDate());
		} else {
			postDataMap.put(PayFortOnlineParam.REQUEST_EXPIRY_DATE.getName(),
					PaymentBrokerPayFortUtils.getFormattedDate(ipgRequestDTO.getInvoiceExpirationTime()));
		}
		postDataMap.put(PayFortOnlineParam.SIGNATURE.getName(), getSignature(postDataMap, getShaRequestPhrase()));
		return postDataMap;
	}

	private static boolean isBillCreationSuccess(Map fields) {
		return BILL_CREATION_SUCCESS_STATUS_CODE.equals(fields.get(PayFortOnlineParam.STATUS.getName()));
	}

	public void setPaymentPartner(String paymentPartner) {
		this.paymentPartner = paymentPartner;
	}

	private void sendBillNumberToContact(IPGRequestDTO ipgRequestDTO, String billNumber) {
		HashMap map = new HashMap();
		map.put(FIRST_NAME, StringUtils.capitalize(ipgRequestDTO.getContactFirstName()));
		map.put(BILL_NUMBER, billNumber);
		map.put(PNR, ipgRequestDTO.getPnr());
		map.put(CARRIER_NAME, AppSysParamsUtil.getDefaultCarrierName());
		map.put(VALID_PERIOD, PaymentBrokerPayFortUtils.getRemainingTimeFromNow(ipgRequestDTO.getInvoiceExpirationTime()));
		PaymentBrokerPayFortUtils.sendEmail(PAYFORT_FAWRY_BILL_NUMBER_NOTIFICATION_VM, map, ipgRequestDTO, null);
	}

	public String getPaymentPartner() {
		return paymentPartner;
	}

	public void setIsQueryingSupported(boolean isQueryingSupported) {
		this.isQueryingSupported = isQueryingSupported;
	}
}
