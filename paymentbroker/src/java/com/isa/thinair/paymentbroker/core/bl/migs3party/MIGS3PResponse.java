/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.migs3party;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.paymentbroker.api.util.PaymentConstants;

/**
 * MIGS payment response
 * 
 * @author Kanchana
 */
public class MIGS3PResponse {

	public static final String NO_VALUE = "";
	public static final String VALID_HASH = "VALID HASH.";
	public static final String INVALID_HASH = "INVALID HASH.";
	public static final String INVALID_RESPONSE = "INVALID RESPONSE";

	// Standard Receipt Data
	private String title;
	private String amount;
	private String locale;
	private String batchNo;
	private String command;
	private String message;
	private String version;
	private String cardType;
	private String orderInfo;
	private String receiptNo;
	private String merchantID;
	private String merchTxnRef;
	private String authorizeID;
	private String transactionNo;
	private String acqResponseCode;
	private String txnResponseCode;
	private String secureHash;

	// 3-D Secure Data
	private String transType3DS;
	private String verStatus3DS;
	private String token3DS;
	private String secureLevel3DS;
	private String enrolled3DS;
	private String xid3DS;
	private String eci3DS;
	private String status3DS;

	// Capture Data
	private String shopTransNo;
	private String authorisedAmount;
	private String capturedAmount;
	private String refundedAmount;
	private String ticketNumber;

	// Specific QueryDR Data
	private String drExists;
	private String multipleDRs;

	private String sessionID;

	private String ccLast4Digits;

	// Response as a Map
	Map response = null;

	public void setReponse(Map reponse) {
		this.response = reponse;
		title = null2unknown((String) reponse.get("Title"));
		amount = null2unknown((String) reponse.get("vpc_Amount"));
		locale = null2unknown((String) reponse.get("vpc_Locale"));
		batchNo = null2unknown((String) reponse.get("vpc_BatchNo"));
		command = null2unknown((String) reponse.get("vpc_Command"));
		message = null2unknown((String) reponse.get("vpc_Message"));
		version = null2unknown((String) reponse.get("vpc_Version"));
		cardType = null2unknown((String) reponse.get("vpc_Card"));
		orderInfo = null2unknown((String) reponse.get("vpc_OrderInfo"));
		receiptNo = null2unknown((String) reponse.get("vpc_ReceiptNo"));
		merchantID = null2unknown((String) reponse.get("vpc_Merchant"));
		merchTxnRef = null2unknown((String) reponse.get("vpc_MerchTxnRef"));
		authorizeID = null2unknown((String) reponse.get("vpc_AuthorizeId"));
		transactionNo = null2unknown((String) reponse.get("vpc_TransactionNo"));
		acqResponseCode = null2unknown((String) reponse.get("vpc_AcqResponseCode"));
		txnResponseCode = null2unknown((String) reponse.get("vpc_TxnResponseCode"));
		secureHash = null2unknown((String) reponse.get("vpc_SecureHash"));
		ccLast4Digits = null2unknown((String) reponse.get("vpc_CardNum"));

		// 3-D Secure Data
		transType3DS = null2unknown((String) reponse.get("vpc_VerType"));
		verStatus3DS = null2unknown((String) reponse.get("vpc_VerStatus"));
		token3DS = null2unknown((String) reponse.get("vpc_VerToken"));
		secureLevel3DS = null2unknown((String) reponse.get("vpc_VerSecurityLevel"));
		enrolled3DS = null2unknown((String) reponse.get("vpc_3DSenrolled"));
		xid3DS = null2unknown((String) reponse.get("vpc_3DSXID"));
		eci3DS = null2unknown((String) reponse.get("vpc_3DSECI"));
		status3DS = null2unknown((String) reponse.get("vpc_3DSstatus"));

		// Capture Data
		shopTransNo = null2unknown((String) reponse.get("vpc_ShopTransactionNo"));
		authorisedAmount = null2unknown((String) reponse.get("vpc_AuthorisedAmount"));
		capturedAmount = null2unknown((String) reponse.get("vpc_CapturedAmount"));
		refundedAmount = null2unknown((String) reponse.get("vpc_RefundedAmount"));
		ticketNumber = null2unknown((String) reponse.get("vpc_TicketNo"));

		// Specific QueryDR Data
		drExists = null2unknown((String) reponse.get("vpc_DRExists"));
		multipleDRs = null2unknown((String) reponse.get("vpc_FoundMultipleDRs"));

		sessionID = null2unknown((String) reponse.get(PaymentConstants.IPG_SESSION_ID));
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();

		// create a list
		List fieldNames = new ArrayList(response.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) response.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				resBuff.append('&');
			}
		}
		return resBuff.toString();
	}

	/**
	 * Validate Response with a given hash value
	 * 
	 * @param securehash
	 * @return
	 */
	public boolean validateHash(String SecureSecret) {
		String createdSecureHash = MIGSPaymentUtils.hashAllFieldsMigsSHA256(response, SecureSecret);
		if (secureHash.equalsIgnoreCase(createdSecureHash))
			return true;
		else
			return false;
	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	public String getAcqResponseCode() {
		return acqResponseCode;
	}

	public String getAmount() {
		return amount;
	}

	public String getAuthorizeID() {
		return authorizeID;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public String getCardType() {
		return cardType;
	}

	public String getCommand() {
		return command;
	}

	public String getEci3DS() {
		return eci3DS;
	}

	public String getEnrolled3DS() {
		return enrolled3DS;
	}

	public String getLocale() {
		return locale;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getMerchTxnRef() {
		return merchTxnRef;
	}

	public String getMessage() {
		return message;
	}

	public String getOrderInfo() {
		return orderInfo;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public String getSecureLevel3DS() {
		return secureLevel3DS;
	}

	public String getStatus3DS() {
		return status3DS;
	}

	public String getTitle() {
		return title;
	}

	public String getToken3DS() {
		return token3DS;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public String getTransType3DS() {
		return transType3DS;
	}

	public String getTxnResponseCode() {
		return txnResponseCode;
	}

	public String getVersion() {
		return version;
	}

	public String getVerStatus3DS() {
		return verStatus3DS;
	}

	public String getXid3DS() {
		return xid3DS;
	}

	public String getAuthorisedAmount() {
		return authorisedAmount;
	}

	public String getCapturedAmount() {
		return capturedAmount;
	}

	public String getRefundedAmount() {
		return refundedAmount;
	}

	public String getShopTransNo() {
		return shopTransNo;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public String getSecureHash() {
		return secureHash;
	}

	public void setSecureHash(String secureHash) {
		this.secureHash = secureHash;
	}

	public String getDrExists() {
		return drExists;
	}

	public String getMultipleDRs() {
		return multipleDRs;
	}

	/**
	 * @return the ccLast4Digits
	 */
	public String getCcLast4Digits() {
		return ccLast4Digits;
	}

	/**
	 * @param ccLast4Digits
	 *            the ccLast4Digits to set
	 */
	public void setCcLast4Digits(String ccLast4Digits) {
		this.ccLast4Digits = ccLast4Digits;
	}

}
