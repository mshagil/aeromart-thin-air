/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.telemoney;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Dhanushka Ranatunga
 */
public class TeleMoneyIPGRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5553419992922285783L;
	public static final String ACTION = "action";
	public static final String AMT = "amt";
	public static final String MID = "mid";
	public static final String REF = "ref";
	public static final String CUR = "cur";
	public static final String STATUSURL = "statusurl";
	public static final String RETURNURL = "returnurl";
	public static final String[] requestParams = { ACTION, AMT, MID, REF, CUR, STATUSURL, RETURNURL };

	private String merchantTxnId;

	private String amount;

	private String merchantId;

	private String currency;

	private String statusUrl;

	private String returnUrl;

	private String ipgUrl;

	public TeleMoneyIPGRequest() {
	}

	public TeleMoneyIPGRequest(String merchantId, String merchantTxnId, String amount, String currency, String statusUrl,
			String returnUrl, String ipgUrl) {
		this.setMerchantId(merchantId);
		this.setMerchantTxnId(merchantTxnId);
		this.setAmount(amount);
		this.setCurrency(currency);
		this.setStatusUrl(statusUrl);
		this.setReturnUrl(returnUrl);
		this.setIpgUrl(ipgUrl);
	}

	public TeleMoneyIPGRequest(Map paramMap) {
		if (paramMap != null) {
			this.setMerchantId(PlatformUtiltiies.nullHandler(paramMap.get(MID)));
			this.setMerchantTxnId(PlatformUtiltiies.nullHandler(paramMap.get(REF)));
			this.setAmount(PlatformUtiltiies.nullHandler(paramMap.get(AMT)));
			this.setCurrency(PlatformUtiltiies.nullHandler(paramMap.get(CUR)));
			this.setStatusUrl(PlatformUtiltiies.nullHandler(paramMap.get(STATUSURL)));
			this.setReturnUrl(PlatformUtiltiies.nullHandler(paramMap.get(RETURNURL)));
			this.setIpgUrl(PlatformUtiltiies.nullHandler(paramMap.get(ACTION)));
		}
	}

	/**
	 * @return Returns the amount.
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the currency.
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            The currency to set.
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return Returns the merchantId.
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            The merchantId to set.
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return Returns the merchantTxnId.
	 */
	public String getMerchantTxnId() {
		return merchantTxnId;
	}

	/**
	 * @param merchantTxnId
	 *            The merchantTxnId to set.
	 */
	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}

	/**
	 * @return Returns the returnUrl.
	 */
	public String getReturnUrl() {
		return returnUrl;
	}

	/**
	 * @param returnUrl
	 *            The returnUrl to set.
	 */
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	/**
	 * @return Returns the statusUrl.
	 */
	public String getStatusUrl() {
		return statusUrl;
	}

	/**
	 * @param statusUrl
	 *            The statusUrl to set.
	 */
	public void setStatusUrl(String statusUrl) {
		this.statusUrl = statusUrl;
	}

	/**
	 * @return Returns the ipgUrl.
	 */
	public String getIpgUrl() {
		return ipgUrl;
	}

	/**
	 * @param ipgUrl
	 *            The ipgUrl to set.
	 */
	public void setIpgUrl(String ipgUrl) {
		this.ipgUrl = ipgUrl;
	}

	private static Map getRequestParamMap(String requestText) {
		Map requestParamMap = new HashMap();
		for (int i = 0; i < requestParams.length; i++) {
			String strValue = requestText.substring(requestText.indexOf(requestParams[i]),
					requestText.indexOf("'>", requestText.indexOf(requestParams[i])));
			String[] element = strValue.split("'");
			requestParamMap.put(requestParams[i], element[element.length - 1].trim());
		}
		return requestParamMap;
	}

	public String getRequest() {
		StringBuilder sb = new StringBuilder();
		sb.append("<form id=frm1 method=POST " + ACTION + "='" + this.getIpgUrl() + "'>");
		sb.append("<input type=hidden name='" + AMT + "' value='" + this.getAmount() + "'>");
		sb.append("<input type=hidden name='" + MID + "' value='" + this.getMerchantId() + "'>");
		sb.append("<input type=hidden name='" + REF + "' value='" + this.getMerchantTxnId() + "'>");
		sb.append("<input type=hidden name='" + CUR + "' value='" + this.getCurrency() + "'>");
		sb.append("<input type=hidden name='" + STATUSURL + "' value='" + this.getStatusUrl() + "'>");
		sb.append("<input type=hidden name='" + RETURNURL + "' value='" + this.getReturnUrl() + "'>");
		sb.append("</form>");

		return sb.toString();
	}

	public String getQuerryRequest() {
		StringBuilder sb = new StringBuilder();
		sb.append("<form id=frm1 method=POST " + ACTION + "='" + this.getIpgUrl() + "'>");
		sb.append("<input type=hidden name='" + MID + "' value='" + this.getMerchantId() + "'>");
		sb.append("<input type=hidden name='" + REF + "' value='" + this.getMerchantTxnId() + "'>");
		sb.append("<input type=hidden name='" + STATUSURL + "' value='" + this.getStatusUrl() + "'>");
		sb.append("</form>");

		return sb.toString();
	}

}