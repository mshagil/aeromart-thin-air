package com.isa.thinair.paymentbroker.core.bl.ogone;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;

public class PaymentBrokerOgoneECommerceImpl extends PaymentBrokerOgoneTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerOgoneECommerceImpl.class);

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String finalAmount = PaymentBrokerUtils.getFormattedAmount(ipgRequestDTO.getAmount(), ipgRequestDTO.getNoOfDecimals());

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		/* Language = Language_CountryCode, en_EN will be used if this is incorrect. */
		String userLanguage = ipgRequestDTO.getSessionLanguageCode();
		String language = userLanguage + "_" + ipgRequestDTO.getIpCountryCode();

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(OgoneRequest.PSPID, getMerchantId());
		postDataMap.put(OgoneRequest.ORDERID, merchantTxnId);
		postDataMap.put(OgoneRequest.AMOUNT, finalAmount);
		postDataMap.put(OgoneRequest.CURRENCY, ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		postDataMap.put(OgoneRequest.OPERATION, getOperation());		
		postDataMap.put(OgoneRequest.WIN3DS, WIN3DS_MAINW);
		postDataMap.put(OgoneRequest.ACCEPTURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(OgoneRequest.DECLINEURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(OgoneRequest.EXCEPTIONURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(OgoneRequest.LANGUAGE, language);		
		postDataMap.put(OgoneRequest.TBL_BG_COLOR, "#ECECEC");
		postDataMap.put(OgoneRequest.TBL_TXT_COLOR, "#333333");
		postDataMap.put(OgoneRequest.BUTTON_BG_COLOR, "#ED1E24");
		postDataMap.put(OgoneRequest.BUTTON_TXT_COLOR, "white");
		postDataMap.put(OgoneRequest.FONT_TYPE, "Verdana,Arial,Helvetica,sans-serif");
		postDataMap.put(OgoneRequest.TITLE, globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME));
		postDataMap.put(OgoneRequest.TXT_COLOR, "#ED1E24");		
		addPaymentMethod(postDataMap, ipgRequestDTO.getPaymentMethod());
		OgonePaymentUtils.addDynamicTemplateUrl(isEnableDynamicTemplate(), postDataMap, getDynamicTemplateUrls(), userLanguage);

		/* Fraud Checking data */
		OgonePaymentUtils.addFraudCheckingData(postDataMap, ipgRequestDTO);

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);		

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();		

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);	
		
		/* Add tracking ID */
		postDataMap.put(OgoneRequest.COMPLUS, OgonePaymentUtils.getTrackingParameter(ccTransaction.getTransactionRefNo(), ipgRequestDTO));
		
		try {
			String shaSign = OgonePaymentUtils.computeSHA1(postDataMap, getSha1In());
			postDataMap.put(OgoneRequest.SHASIGN, shaSign);
			strRequestParams = strRequestParams + "," + OgoneRequest.SHASIGN + "=" + shaSign;
		} catch (Exception e) {
			log.debug("Error computing SHA-1 : " + e.toString());
			throw new ModuleException(e.getMessage());
		}
		
		String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());
		
		if (log.isDebugEnabled()) {
			log.debug("OGONE Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}
		
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}
	
	private void addPaymentMethod(Map<String, String> postDataMap, String paymentMethod) {
		Map<String, String> paymentMethodValues = getPaymentMethods();
		if (paymentMethodValues != null && !paymentMethodValues.isEmpty() && isNotEmpty(paymentMethod)) {
			Map<String, String> paymentMethodData = getPaymentMethodBrandMap(paymentMethodValues.get(paymentMethod));
			if (isNotEmpty(paymentMethodData.get(OgoneRequest.PAYMENT_METHOD))) {
				postDataMap.put(OgoneRequest.PAYMENT_METHOD, paymentMethodData.get(OgoneRequest.PAYMENT_METHOD));
				if (isNotEmpty(paymentMethodData.get(OgoneRequest.PAYMENT_BRAND))) {
					postDataMap.put(OgoneRequest.PAYMENT_BRAND, paymentMethodData.get(OgoneRequest.PAYMENT_BRAND));
				}
			}
		}
	}
	
	/**
	 * Eg.
	 *paymentData= PM=CreditCard,BRAND=BCMC
	 *
	 * 
	 */
	private Map<String, String> getPaymentMethodBrandMap(String paymentData) {
		Map<String, String> paymentMethodData = new HashMap<String, String>();
		String[] keyValue =  null;
		
		if (isNotEmpty(paymentData)) {
			String[] paymentDataArr = paymentData.split(",");			
			if (paymentDataArr.length > 0) {				
				for (int i = 0; i < paymentDataArr.length; i++) {
					if (isNotEmpty(paymentDataArr[i])) {
						keyValue = paymentDataArr[i].split("=");
						if (keyValue != null &&  keyValue.length > 1) {
							if (isNotEmpty(keyValue[0]) && isNotEmpty(keyValue[1])) {
								paymentMethodData.put(keyValue[0], keyValue[1]);
							}
						}
					}
				}
			}
		}
		
		return  paymentMethodData;
	}
	
	private static boolean isNotEmpty(String code) {
		boolean isEmpty = false;
		if (code != null && !code.trim().isEmpty()) {
			isEmpty = true;
		}		
		return isEmpty;
	}
	
}
