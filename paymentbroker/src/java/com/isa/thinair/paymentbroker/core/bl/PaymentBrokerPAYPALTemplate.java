package com.isa.thinair.paymentbroker.core.bl;

import java.util.Properties;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.core.bl.paypal.PAYPALPaymentUtils;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public abstract class PaymentBrokerPAYPALTemplate extends PaymentBrokerTemplate {

	protected String mode;

	protected String apiUsername;

	protected String apiPassword;

	protected String signature;

	protected String environment;

	protected String certificateFilePath;

	protected String privateKeyPassword;

	protected String useProxy;

	protected String redirectUrl;

	protected String cancelUrl;

	protected String note;

	protected String orderDescription;

	protected IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	public abstract String getPaymentGatewayName() throws ModuleException;

	@Override
	public Properties getProperties() {
		Properties props = new Properties();
		props.put(PAYPALPaymentUtils.brokerType, PlatformUtiltiies.nullHandler(brokerType));
		props.put(PAYPALPaymentUtils.requestMethod, PlatformUtiltiies.nullHandler(requestMethod));
		props.put(PAYPALPaymentUtils.mode, PlatformUtiltiies.nullHandler(mode));
		props.put(PAYPALPaymentUtils.apiUsername, PlatformUtiltiies.nullHandler(apiUsername));
		props.put(PAYPALPaymentUtils.apiPassword, PlatformUtiltiies.nullHandler(apiPassword));
		props.put(PAYPALPaymentUtils.signature, PlatformUtiltiies.nullHandler(signature));
		props.put(PAYPALPaymentUtils.environment, PlatformUtiltiies.nullHandler(environment));
		props.put(PAYPALPaymentUtils.certificateFilePath, PlatformUtiltiies.nullHandler(certificateFilePath));
		props.put(PAYPALPaymentUtils.privateKeyPassword, PlatformUtiltiies.nullHandler(privateKeyPassword));
		props.put(PAYPALPaymentUtils.useProxy, PlatformUtiltiies.nullHandler(useProxy));
		props.put(PAYPALPaymentUtils.proxyHost, PlatformUtiltiies.nullHandler(proxyHost));
		props.put(PAYPALPaymentUtils.proxyPort, PlatformUtiltiies.nullHandler(proxyPort));
		props.put(PAYPALPaymentUtils.redirectUrl, PlatformUtiltiies.nullHandler(redirectUrl));
		props.put(PAYPALPaymentUtils.cancelUrl, PlatformUtiltiies.nullHandler(cancelUrl));
		props.put(PAYPALPaymentUtils.note, PlatformUtiltiies.nullHandler(note));
		props.put(PAYPALPaymentUtils.orderDescription, PlatformUtiltiies.nullHandler(orderDescription));
		return props;
	}

	/**
	 * Validates and set IPGIdentificationParamsDTO
	 * 
	 * @param ipgIdentificationParamsDTO
	 * @throws ModuleException
	 */
	public void setIPGIdentificationParams(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(ipgIdentificationParamsDTO);
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	/**
	 * @return Returns the brokerType.
	 */
	public String getBrokerType() {
		return brokerType;
	}

	/**
	 * @param brokerType
	 *            The brokerType to set.
	 */
	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}

	/**
	 * @return Returns the mode.
	 */
	public String getMode() {
		return brokerType;
	}

	/**
	 * @param mode
	 *            The mode to set.
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return Returns the apiUsername.
	 */
	public String getApiUsername() {
		return apiUsername;
	}

	/**
	 * @param apiUsername
	 *            The apiUsername to set.
	 */
	public void setApiUsername(String apiUsername) {
		this.apiUsername = apiUsername;
	}

	/**
	 * @return Returns the apiPassword.
	 */
	public String getApiPassword() {
		return apiPassword;
	}

	/**
	 * @param apiPassword
	 *            The apiPassword to set.
	 */
	public void setApiPassword(String apiPassword) {
		this.apiPassword = apiPassword;
	}

	/**
	 * @return Returns the signature.
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * @param signature
	 *            The signature to set.
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * @return Returns the environment.
	 */
	public String getEnvironment() {
		return environment;
	}

	/**
	 * @param environment
	 *            The environment to set.
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	/**
	 * @return Returns the certificateFilePath.
	 */
	public String getCertificateFilePath() {
		return certificateFilePath;
	}

	/**
	 * @param certificateFilePath
	 *            The certificateFilePath to set.
	 */
	public void setCertificateFilePath(String certificateFilePath) {
		this.certificateFilePath = certificateFilePath;
	}

	/**
	 * @return Returns the privateKeyPassword.
	 */
	public String getPrivateKeyPassword() {
		return privateKeyPassword;
	}

	/**
	 * @param privateKeyPassword
	 *            The privateKeyPassword to set.
	 */
	public void setPrivateKeyPassword(String privateKeyPassword) {
		this.privateKeyPassword = privateKeyPassword;
	}

	/**
	 * @return Returns the useProxy.
	 */
	public String getUseProxy() {
		return useProxy;
	}

	/**
	 * @param useProxy
	 *            The useProxy to set.
	 */
	public void setUseProxy(String useProxy) {
		this.useProxy = useProxy;
	}

	/**
	 * @return Returns the proxyHost.
	 */
	public String getProxyHost() {
		return proxyHost;
	}

	/**
	 * @param proxyHost
	 *            The proxyHost to set.
	 */
	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	/**
	 * @return Returns the proxyPort.
	 */
	public String getProxyPort() {
		return proxyPort;
	}

	/**
	 * @param proxyPort
	 *            The proxyPort to set.
	 */
	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	/**
	 * @return Returns the requestMethod.
	 */
	public String getRequestMethod() {
		return requestMethod;
	}

	/**
	 * @param requestMethod
	 *            The requestMethod to set.
	 */
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	/**
	 * @return Returns the merchantId.
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            The merchantId to set.
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return Returns the redirectUrl.
	 */
	public String getRedirectUrl() {
		return redirectUrl;
	}

	/**
	 * @param redirectUrl
	 *            The redirectUrl to set.
	 */
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	/**
	 * @return Returns the cancelUrl.
	 */
	public String getCancelUrl() {
		return cancelUrl;
	}

	/**
	 * @param cancelUrl
	 *            The cancelUrl to set.
	 */
	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}

	/**
	 * @return Returns the note.
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            The cancelUrl to set.
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return Returns the orderDescription.
	 */
	public String getOrderDescription() {
		return orderDescription;
	}

	/**
	 * @param orderDescription
	 *            The orderDescription to set.
	 */
	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}

}
