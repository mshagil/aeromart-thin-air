package com.isa.thinair.paymentbroker.core.bl.ipay88;

/**
 * The IPay88Request class to contain request variable constants
 * 
 */
public class IPay88Request {

	public static final String MERCHANTCODE = "MerchantCode";

	// optional field
	public static final String PAYMENTID = "PaymentId";

	public static final String REFNO = "RefNo";

	public static final String AMOUNT = "Amount";

	public static final String CURRENCY = "Currency";

	public static final String PRODDESC = "ProdDesc";

	public static final String USERNAME = "UserName";

	public static final String USEREMAIL = "UserEmail";

	public static final String USERCONTACT = "UserContact";

	// optional field
	public static final String REMARK = "Remark";

	public static final String SIGNATURE = "Signature";

	public static final String RESPONSEURL = "ResponseURL";

	public static final String BACKENDURL = "BackendURL";

	public IPay88Request() {

	}

}
