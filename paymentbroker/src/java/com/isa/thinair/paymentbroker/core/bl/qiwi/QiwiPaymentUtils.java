package com.isa.thinair.paymentbroker.core.bl.qiwi;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.ibm.misc.BASE64Encoder;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;


public class QiwiPaymentUtils {
	
	private static Log log = LogFactory.getLog(QiwiPaymentUtils.class);
	private static final String NO_VALUE = "";

	public static String composeComment(QiwiPRequest qiwiRequest){
		String comment = "";
		if(qiwiRequest.getPnr() != null){
			comment = qiwiRequest.getMerchantName() + " PNR : " + qiwiRequest.getPnr() ;
		}
		comment += "	Qiwi Invoice Id : " + qiwiRequest.getTransactionID();
		comment += "	Qiwi Mobile No : " + qiwiRequest.getQiwiMobileNumber();
		return comment;
	}
	
	public static String getValidatedQiwiMobileNo(String qiwiMobileNumber){

		Pattern pattern;
		Matcher matcher;
		qiwiMobileNumber = qiwiMobileNumber.trim();
		qiwiMobileNumber = (qiwiMobileNumber.charAt(0) != '+') ? qiwiMobileNumber = "+" + qiwiMobileNumber : qiwiMobileNumber;
		// pattern canbe found in the qiwi doc
		final String PATTERN = "^tel:\\+\\d{1,15}$";
		qiwiMobileNumber = "tel:" + qiwiMobileNumber;
		pattern = Pattern.compile(PATTERN);
		matcher = pattern.matcher(qiwiMobileNumber);
		
		return matcher.matches() ? qiwiMobileNumber : null ;
	}
	
	public static String composeQiwiInvoiceCreationURL(QiwiPRequest qiwiRequest){
		String createInvoiceURL = qiwiRequest.getQiwiInvoiceCreationURL();
		createInvoiceURL = createInvoiceURL.replaceFirst("\\?", qiwiRequest.getQiwiMerchantID());
		createInvoiceURL = createInvoiceURL.replaceFirst("\\?", qiwiRequest.getTransactionID());
		
		return createInvoiceURL;
	}
	
	public static String getUniqueRefundID(){
		
		UUID myuuid = UUID.randomUUID();
		long highbits = myuuid.getMostSignificantBits();
		long lowbits = myuuid.getLeastSignificantBits();
		
		String id = String.valueOf(highbits) + String.valueOf(lowbits);
		id = id.replaceAll("-", "");
	    SecureRandom random = new SecureRandom();
	    int beginIndex = random.nextInt(15);       //Begin index + length of your string < data length
	    int endIndex = beginIndex + 4;            //Length of string which you want

	    String yourID = id.substring(beginIndex, endIndex);
		  
		return yourID;
	}
	
	public static String getFormattedAmount(String amount){
		Float f = Float.parseFloat(amount);
		DecimalFormat df = new DecimalFormat("0.00");
		df.setMaximumFractionDigits(2);
		return df.format(f );
	}
	/**
	 * @param postDataMap
	 * 
	 * Creates a form for the given request method(POST, PUT .etc)
	 * 
	 * */
	public static String getQiwiRedirectHTML(Map<String, String> postDataMap) {
		StringBuffer sb = new StringBuffer();

		sb.append("<form name=\"paymentForm\" action=\"" + postDataMap.get(QiwiPRequest.REDIRECT_URL) + "\" method=\"POST\">");
		sb.append("</form>");

		return sb.toString();
	}

	public static String composeQiwiRedirectURL(IPGRequestDTO ipgRequestDTO, QiwiPRequest qiwiRequest) {
		if(qiwiRequest.isOfflineMode()){
			return "";
		}
		String url = qiwiRequest.getQiwiRedirectURL();
		String[] parts = url.split("\\?");
		url = parts[0] + "?" +  parts[1] + qiwiRequest.getMerchantID() + parts[2] + qiwiRequest.getTransactionID();
		url += "&successUrl=" + ipgRequestDTO.getReturnUrl() + "&failUrl=" + ipgRequestDTO.getReturnUrl();
		
		log.debug("[QiwiPaymentUtil::composeQiwiRedirectURL] " + url);
		return url;
	}

	public static String composeQiwiInvoiceStatusReqURL(QiwiPRequest req){
		String url = req.getQiwiInvoiceStatusReqURL();
		String[] parts = url.split("\\?");
		url = parts[0] + req.getMerchantID() + parts[1] + req.getTransactionID();
		
		log.debug("[QiwiPaymentUtil::composeQiwiInvoiceStatusReqURL] " + url);
		return url;
	}
	
	public static QiwiPResponse processQiwiResponse(String response){

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			ByteArrayInputStream stream = new ByteArrayInputStream(response.getBytes());
			Document doc = dBuilder.parse(stream);
			doc.getDocumentElement().normalize();
			
			Node node = doc.getElementsByTagName("response").item(0);
			Element eElement = (Element) node;
			
			String resultCode = eElement.getElementsByTagName("result_code").item(0).getTextContent();
			
			Node descriptionNode = eElement.getElementsByTagName("description").item(0);
			
			if(descriptionNode != null){
				String description = eElement.getElementsByTagName("description").item(0).getTextContent();
				QiwiPResponse qiwiResponse = new QiwiPResponse();
				qiwiResponse.setResultCode(resultCode);
				qiwiResponse.setDescription(description);
				if(!resultCode.equals("0")){
					qiwiResponse.setStatus(QiwiPRequest.FAIL);
					qiwiResponse.setError(resultCode);
				}
				return qiwiResponse;
			}
			
			Node refundNode = eElement.getElementsByTagName("refund").item(0);
			if(refundNode != null){
				String amount = eElement.getElementsByTagName("amount").item(0).getTextContent();
				String status = eElement.getElementsByTagName("status").item(0).getTextContent();
				String error = eElement.getElementsByTagName("error").item(0).getTextContent();
				String refundId = eElement.getElementsByTagName("refund_id").item(0).getTextContent();
				
				QiwiPResponse qiwiResponse = new QiwiPResponse(resultCode,
						refundId, amount, null, status, null, "REFUND Req : "
								+ status + " For " + refundId, error);
				qiwiResponse.setRefundId(refundId);
				return qiwiResponse;
			}

			eElement = (Element)doc.getElementsByTagName("bill").item(0);
			String billId = eElement.getElementsByTagName("bill_id").item(0).getTextContent();
			String amount = eElement.getElementsByTagName("amount").item(0).getTextContent();
			String currency = eElement.getElementsByTagName("ccy").item(0).getTextContent();
			String status = eElement.getElementsByTagName("status").item(0).getTextContent();
			String user = eElement.getElementsByTagName("user").item(0).getTextContent();
			String comment = eElement.getElementsByTagName("comment").item(0).getTextContent();
			String error = eElement.getElementsByTagName("error").item(0).getTextContent();
					
			QiwiPResponse qiwiResponse = new QiwiPResponse(resultCode, billId, amount, currency, status, user, comment, error );
			return qiwiResponse;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getErrorInfo(QiwiPResponse res){
		String errorCode = res.getError();
		String resultCode = res.getResultCode();
		String status = res.getStatus();
		
		errorCode = PlatformUtiltiies.nullHandler(errorCode);
		resultCode =  PlatformUtiltiies.nullHandler(resultCode);
		status = PlatformUtiltiies.nullHandler(status);
		
		if(errorCode.equals("0")){
			return "Success";
		}else if(errorCode.equals("5") || resultCode.equals("5")){
			return "The format of the request parameters is incorrect";			
		}else if(errorCode.equals("13") || resultCode.equals("13")){
			return "Server is busy, try again later";			
		}else if(errorCode.equals("150") || resultCode.equals("150")){
			return "Authorization error (e.g. invalid login/password)";			
		}else if(errorCode.equals("210") || resultCode.equals("210")){
			return "Invoice not found";			
		}else if(errorCode.equals("215") || resultCode.equals("215")){
			return "Invoice with this bill_id already exists";			
		}else if(errorCode.equals("241") || resultCode.equals("241")){
			return "Invoice amount less than minimum";			
		}else if(errorCode.equals("242") || resultCode.equals("242")){
			return "Amount is greater than amount of bill";			
		}else if(errorCode.equals("298") || resultCode.equals("298")){
			return "User not registered";			
		}else if(errorCode.equals("300") || resultCode.equals("300")){
			return "Technical error. Please Check Your Qiwi Account Phone Number";			
			
		}else if(status.equals(QiwiPResponse.INVOICE_EXPIRED)){
			return "Invoice expired. Invoice has not been paid";
		}else if(status.equals(QiwiPResponse.INVOICE_REJECTED)){
			return "Invoice has been rejected";
		}else if(status.equals(QiwiPResponse.INVOICE_UNPAID)){
			return "Payment processing error. Invoice has not been paid";
		}else if(status.equals(QiwiPResponse.INVOICE_WAITING)){
			return "Invoice issued, pending payment";
		}
		return null;
	}
	
	public static String getErrorCode(QiwiPResponse res){
		String errorCode = res.getError();
		String resultCode = res.getResultCode();
		String status = res.getStatus();
		
		errorCode = PlatformUtiltiies.nullHandler(errorCode);
		resultCode =  PlatformUtiltiies.nullHandler(resultCode);
		status = PlatformUtiltiies.nullHandler(status);
		
		if(errorCode.equals("5") || resultCode.equals("5")){
			return "qiwi.invalid.request";			
		}else if(errorCode.equals("13") || resultCode.equals("13")){
			return "qiwi.server.busy";			
		}else if(errorCode.equals("150") || resultCode.equals("150")){
			return "qiwi.authorization.failure";			
		}else if(errorCode.equals("210") || resultCode.equals("210")){
			return "qiwi.no.invoice";			
		}else if(errorCode.equals("215") || resultCode.equals("215")){
			return "qiwi.duplicate.invoice";			
		}else if(errorCode.equals("241") || resultCode.equals("241")){
			return "qiwi.deficient.amount";			
		}else if(errorCode.equals("242") || resultCode.equals("242")){
			return "qiwi.invalid.refund.amount";			
		}else if(errorCode.equals("298") || resultCode.equals("298")){
			return "qiwi.unregistered.user";			
		}else if(errorCode.equals("300") || resultCode.equals("300")){
			return "qiwi.unregistered.user";			
		}
			return "qiwi.default";			
	}
	
	public static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}
	
	public static IPGConfigsDTO getIPGConfigs(String merchantID, String password) {
		IPGConfigsDTO iPGConfigsDTO = new IPGConfigsDTO();
		iPGConfigsDTO.setMerchantID(merchantID);
		iPGConfigsDTO.setPassword(password);
		return iPGConfigsDTO;
	}
	
	public static QiwiPResponse checkInvoiceStatus(QiwiPRequest req) {
		
		log.debug("[QiwiPaymentUtil::checkInvoiceStatus] " + req.toString());
		
		HttpClient client = new HttpClient();
		HttpMethod method = new GetMethod(
				QiwiPaymentUtils.composeQiwiInvoiceStatusReqURL(req));

		method.setRequestHeader("Accept", "text/xml");
		method.setRequestHeader(
				"Authorization",
				"Basic "
						+ new BASE64Encoder().encode((req.getLogin() + ":" + req
								.getPassword()).getBytes()));
		method.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded; charset=utf-8");

		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler(3, false));
		
		try {
			client.executeMethod(method);
			String response = null;
			response = method.getResponseBodyAsString();
			
			log.debug("[QiwiPaymentUtil::checkInvoiceStatus : Response ] " + response);
			
			return QiwiPaymentUtils.processQiwiResponse(response);
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	public static String getRefundURL(QiwiPRequest req){
		String url = req.getRefundURL().trim();  // https://w.qiwi.com/api/v2/prv/?/bills/?/refund/?
		String[] parts = url.split("\\?");
		url =  parts[0] + req.getMerchantID() + parts[1] + req.getTransactionID() + parts[2] + req.getRefundID();
		
		log.debug("[QiwiPaymentUtil::getRefundURL] " + url);
		return url;
	}
	
	/**
	 * @param req
	 * 			QiwiPRequest object with configuration data	 * 
	 * @throws ModuleException 
	 * 
	 * */
	
	public static QiwiPResponse initiateRefund(QiwiPRequest req) throws ModuleException {
		
		NameValuePair[] putParameters = new NameValuePair[]{
				new NameValuePair(QiwiPRequest.AMOUNT, req.getAmount())
		};
		
	 	HttpClient client = new HttpClient();
	 	PutMethod putMethod = new PutMethod(QiwiPaymentUtils.getRefundURL(req));
	  
	    putMethod.addRequestHeader("Accept", "application/xml"); 
	    putMethod.addRequestHeader("Authorization", "Basic " + QiwiPaymentUtils.getAuthorizationToken(req.getLogin(), req.getPassword()));
	    putMethod.addRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
		
	    putMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));	
	    putMethod.setRequestBody(EncodingUtil.formUrlEncode(putParameters, "UTF-8"));
	    
	    try {
			// Execute the method.
			int response = client.executeMethod(putMethod);

			if (response != HttpStatus.SC_OK) {
				log.info("[QiwiPaymentUtil::initiateRefund] Method failed: " + response);
			}
			
			// Read the response body.
			byte[] responseBodyBytes = putMethod.getResponseBody();
			
		    String responseBody = new String(responseBodyBytes);   
	        log.debug("[QiwiPaymentUtil::initiateRefund] " + responseBody);	        
	    
	        return QiwiPaymentUtils.processQiwiResponse(responseBody);
	        
		} catch (HttpException e) {
			log.error("initiateRefund : Fatal protocol violation: " + e.getMessage());
		} catch (IOException e) {
			log.error("initiateRefund : Fatal transport error: " + e.getMessage());
		} finally {
			// Release the connection.
			putMethod.releaseConnection();
		}
		return null;
	    
	}
	
	public static QiwiPResponse checkRefundStatus(QiwiPRequest req) throws ModuleException {
		HttpClient client = new HttpClient();
		GetMethod method = new GetMethod(QiwiPaymentUtils.getRefundURL(req));

		method.setQueryString(new org.apache.commons.httpclient.NameValuePair[] {
		        new org.apache.commons.httpclient.NameValuePair(QiwiPRequest.AMOUNT, req.getAmount())});		
		method.setRequestHeader("Accept", "text/xml");
		method.setRequestHeader("Authorization","Basic " + new BASE64Encoder().encode((req.getLogin() + ":" + req.getPassword()).getBytes()));
		method.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded; charset=utf-8");

		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler(3, false));
		
		try {
			client.executeMethod(method);
			String response = method.getResponseBodyAsString();
			
			log.debug("[QiwiPaymentUtil::checkRefundStatus] " + response);
			
			return QiwiPaymentUtils.processQiwiResponse(response);
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			method.releaseConnection();
		}
		return null;	    
	}	
	
	
	public static String getAuthorizationToken(String login, String password){
		BASE64Encoder enc = new BASE64Encoder();
	    String userpassword = login + ":" + password;
	    String encodedAuthorization = enc.encode(userpassword.getBytes());
	    
	    log.debug("[QiwiPaymentUtil::getAuthorizationToken] " + encodedAuthorization);
	    return encodedAuthorization;
	}
	
	public static QiwiPResponse createQiwiInvoice(Map<String, String> postDataMap){	
		
		log.info("[QiwiPaymentUtil::createQiwiInvoice] : Start");
	
		NameValuePair[] putParameters = {	
			 new NameValuePair(QiwiPRequest.USER, postDataMap.get(QiwiPRequest.USER)),	   	
			 new NameValuePair(QiwiPRequest.AMOUNT, postDataMap.get(QiwiPRequest.AMOUNT)),
			 new NameValuePair( QiwiPRequest.CURRENCY , postDataMap.get(QiwiPRequest.CURRENCY)),
		     new NameValuePair(QiwiPRequest.COMMENT, postDataMap.get(QiwiPRequest.COMMENT)),
		     new NameValuePair(QiwiPRequest.LIFETIME, postDataMap.get(QiwiPRequest.LIFETIME)),	    
		     new NameValuePair(QiwiPRequest.MERCHANT_NAME, postDataMap.get(QiwiPRequest.MERCHANT_NAME))
		};
	    
	    log.debug("[QiwiPaymentUtil::createQiwiInvoice LIFETIME : ] " + postDataMap.get(QiwiPRequest.LIFETIME));
	    log.debug("[QiwiPaymentUtil::createQiwiInvoice USER : ] " 	  + postDataMap.get(QiwiPRequest.COMMENT));

	    HttpClient client = new HttpClient();
	    PutMethod putMethod = new PutMethod(postDataMap.get(QiwiPRequest.FORM_ACTION));	    
	    
	    putMethod.setRequestBody(EncodingUtil.formUrlEncode(putParameters, "UTF-8"));
	    
	    putMethod.addRequestHeader("Accept", "application/xml"); 
	    putMethod.addRequestHeader("Authorization", "Basic " + postDataMap.get(QiwiPRequest.AUTHORIZATION));
	    putMethod.addRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
	    	    
	    putMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler(3, false));
	    
		try {
			
			log.info("[QiwiPaymentUtil::createQiwiInvoice] executing  'client.executeMethod' ");
			int response = client.executeMethod(putMethod);

			if (response != HttpStatus.SC_OK) {
				log.info("[QiwiPaymentUtil::createQiwiInvoice] Method failed: " + response);
			}
			
			// Read the response body.
			byte[] responseBodyBytes = putMethod.getResponseBody();
			
		    String responseBody = new String(responseBodyBytes);   
	        log.debug("[QiwiPaymentUtil::createQiwiInvoice] " + responseBody);	        
	    
	       return QiwiPaymentUtils.processQiwiResponse(responseBody);
	        
		} catch (HttpException e) {
			log.error("Fatal protocol violation: " + e.getMessage());
		} catch (IOException e) {
			log.error("Fatal transport error: " + e.getMessage());
		} finally {
			// Release the connection.
			putMethod.releaseConnection();
			log.info("[QiwiPaymentUtil::createQiwiInvoice] : Start");
		}
		return null;
	}
	
	public static String composeServerToServerResponse(String resultCode){

		String responseStr =  "";
		org.jdom.Document doc = new org.jdom.Document();
		org.jdom.Element response = new org.jdom.Element("result")
				.addContent(new org.jdom.Element("result_code").setText(resultCode));
		
		doc.addContent(response);
		responseStr = new XMLOutputter().outputString(doc);
		
		log.debug("[QiwiPaymentUtil::composeServerToServerResponse] " + responseStr);		
		return responseStr;
	}

}
