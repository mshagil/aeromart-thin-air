package com.isa.thinair.paymentbroker.core.bl.payFort.v2;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.PayFortOnlineParam;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.util.Map;
import java.util.LinkedHashMap;

public class PaymentBrokerPayFortPayOnline extends PaymentBrokerPayFortTemplate {

    private static final Log log = LogFactory.getLog(PaymentBrokerPayFortPayOnline.class);

    public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
        log.debug("[PaymentBrokerPayFortPayOnline::getRequestData()] Begin ");

        String refNo = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
                ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

        Map<String, String> postDataMap = getPostDataMap(ipgRequestDTO, refNo);
        String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

        String sessionID = StringUtils.defaultString(ipgRequestDTO.getSessionID());

        CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), refNo,
                ipgRequestDTO.getApplicationTransactionId(), bundle.getString("SERVICETYPE_AUTHORIZE"),
                strRequestParams + "," + sessionID, "",
                ipgRequestDTO.getIpgIdentificationParamsDTO().getFQIPGConfigurationName(), null, false);

        log.debug("PayFort Online Request Data : " + strRequestParams + ", sessionID : " + sessionID);

        return createIpgRequestResultsDTO(refNo, postDataMap, ccTransaction);
    }

    @Override
    public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
        log.debug("[PaymentBrokerPayFortPayOnline::getResponseData()] Begin ");

        String status, errorCode = null;
        CreditCardTransaction creditCardTransaction = saveCreditCardTransactionDetails(ipgResponseDTO);

        if (isSignatureValid(fields, getShaResponsePhrase())) {
            if (isSuccessPurchase(fields)) {
                status = IPGResponseDTO.STATUS_ACCEPTED;
            } else {
                String responseCode = StringUtils.right((String) fields.get(PayFortOnlineParam.RESPONSE_CODE.getName()), 3);
                status = IPGResponseDTO.STATUS_REJECTED;
                errorCode = CANCELLED_TRANSACTION_RESPONSE_CODE.equals(responseCode) ?
                "payfort.transaction.cancelled" : "payfort.transaction.failed";
            }
        } else {
            status = IPGResponseDTO.STATUS_REJECTED;
            errorCode = "payfort.invalid.response";
        }

        ipgResponseDTO.setStatus(status);
        ipgResponseDTO.setErrorCode(errorCode);
        ipgResponseDTO.setApplicationTransactionId(creditCardTransaction.getTransactionId());
        ipgResponseDTO.setAuthorizationCode((String) fields.get(PayFortOnlineParam.AUTHORIZATION_CODE.getName()));
        // Has to change this for installments
        ipgResponseDTO.setCardType(getPaymentType(fields).getTypeValue());
        ipgResponseDTO.setCcLast4Digits(getCreditCardLast4Digits(fields));

        String payFortOnlineResponse = PaymentBrokerUtils.getRequestDataAsString(fields);
        updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), payFortOnlineResponse, errorCode,
                ipgResponseDTO.getAuthorizationCode(), (String) fields.get(PayFortOnlineParam.FORT_ID.getName()),
                IPGResponseDTO.STATUS_ACCEPTED.equals(status) ? 1 : 0);

        log.debug("[PaymentBrokerPayFortPayOnline::getResponseData()] End -");
        return ipgResponseDTO;
    }

    private static String getCreditCardLast4Digits(Map fields) {
        String cardNumber = StringUtils.EMPTY;
        if (!CollectionUtils.isEmpty(fields)) {
            cardNumber = (String) fields.get(PayFortOnlineParam.CARD_NUMBER.getName());
            if (StringUtils.isNotEmpty(cardNumber)) {
                cardNumber = StringUtils.right(cardNumber, 4);
            }
        }
        return cardNumber;
    }

    private IPGRequestResultsDTO createIpgRequestResultsDTO(String refNo, Map<String, String> postDataMap,
                                                            CreditCardTransaction ccTransaction) {
        IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
        ipgRequestResultsDTO.setRequestData(PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL()));
        ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
        ipgRequestResultsDTO.setAccelAeroTransactionRef(refNo);
        ipgRequestResultsDTO.setPostDataMap(postDataMap);
        return ipgRequestResultsDTO;
    }

    private CreditCardTransaction saveCreditCardTransactionDetails(IPGResponseDTO ipgResponseDTO) {
        CreditCardTransaction oCreditCardTransaction =
                loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());
        // Calculate response time
        String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
                ipgResponseDTO.getResponseTimeStamp());
        long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
                ipgResponseDTO.getResponseTimeStamp());
        // Update response time
        oCreditCardTransaction.setComments(strResponseTime);
        oCreditCardTransaction.setResponseTime(timeDiffInMillis);
        PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);
        return oCreditCardTransaction;
    }

    private Map<String, String> getPostDataMap(IPGRequestDTO ipgRequestDTO, String refNo) throws ModuleException {
        Map<String, String> postDataMap = new LinkedHashMap<>();
        postDataMap.put(PayFortOnlineParam.COMMAND.getName(), "PURCHASE");
        postDataMap.put(PayFortOnlineParam.ACCESS_CODE.getName(), getMerchantAccessCode());
        postDataMap.put(PayFortOnlineParam.AMOUNT.getName(), getISOFormattedAmount(ipgRequestDTO.getAmount()));
        postDataMap.put(PayFortOnlineParam.MERCHANT_IDENTIFIER.getName(), getMerchantId());
        postDataMap.put(PayFortOnlineParam.MERCHANT_REFERENCE.getName(), refNo);
        postDataMap.put(PayFortOnlineParam.CURRENCY.getName(),
                ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
        postDataMap.put(PayFortOnlineParam.LANGUAGE.getName(), getLocale());
        postDataMap.put(PayFortOnlineParam.CUSTOMER_EMAIL.getName(), ipgRequestDTO.getContactEmail());
        postDataMap.put(PayFortOnlineParam.RETURN_URL.getName(), ipgRequestDTO.getReturnUrl());
        postDataMap.put(PayFortOnlineParam.CUSTOMER_NAME.getName(),
                ipgRequestDTO.getContactFirstName() + " " + ipgRequestDTO.getContactLastName());
        postDataMap.put(PayFortOnlineParam.PNR.getName(), ipgRequestDTO.getPnr());

        if (StringUtils.isNotEmpty(ipgRequestDTO.getCardName())) {
            if (StringUtils.equalsIgnoreCase(INSTALLMENTS, ipgRequestDTO.getCardName())) {
                postDataMap.put(PayFortOnlineParam.INSTALLMENTS.getName(), "STANDALONE");
            } else {
                postDataMap.put(PayFortOnlineParam.PAYMENT_OPTION.getName(), ipgRequestDTO.getCardName());
            }
        }

        if (StringUtils.isNotEmpty(ipgRequestDTO.getContactPhoneNumber())) {
            postDataMap.put(PayFortOnlineParam.PHONE_NUMBER.getName(), ipgRequestDTO.getContactPhoneNumber());
        }

        postDataMap.put(PayFortOnlineParam.SIGNATURE.getName(), getSignature(postDataMap, getShaRequestPhrase()));
        return postDataMap;
    }
}
