package com.isa.thinair.paymentbroker.core.bl.saman;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SRTMRefundServiceResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.ogone.OgonePaymentUtils;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientBD;

/**
 * 
 * @author Janaka Padukka
 * 
 */
public class PaymentBrokerSamanTemplate extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerSamanTemplate.class);
	protected static final String VERIFICATION_AMOUNTS_ERROR = "saman.2000";
	protected static final String DEFAULT_LANGUAGE = "En";
	protected static final Integer REFUND_SUCCESS = 1;
	protected static final Integer PARTIAL_REFUND_NON_SAMAN_CARD = -17;
	protected static final Integer OTHER_BANKING_NW_ERROR = -1;
	protected static final String CARD_TYPE_GENERIC = "GC";
	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	
	
	public static final String  SRTM_REFUND_SUCESS = "Success";
	public static final String  SRTM_REFUND_INVALIDE_PARAMETER = "InvalidParameter";
    public static final String  SRTM_REFUND_AUTHENTICATE_USER_FAIL = "AuthenticateUserFail";
    public static final String  SRTM_REFUND_UNKNOW = "Unknown";
    public static final String  SRTM_REFUND_DUPLICATE_REQUEST = "DuplicateRequest";
    public static final String  SRTM_REFUND_ORIGINAL_AMOUNT_INCORRECT = "OriginalAmountIncorrect";
    public static final String  SRTM_REFUND_OBJECT_NOT_FOUND = "ObjectNotFound";
    public static final String  SRTM_REFUND_TRANSACTION_SATATEIS_FINAL = "TransactionStateisFinal";
    public static final String  SRTM_REFUND_CARD_EXISTS = "CardExists";
    public static final String  SRTM_REFUND_CARD_NOT_FOUND = "CardNotFound";
    public static final String  SRTM_REFUND_fATAL = "Fatal";
    public static final String  SRTM_REFUND_WRONG_STATE_FOR_DELETE = "WrongStateForDelete";
	
    
	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerSamanTemplate::refund()] Begin - SRTM");
		String errorCode = "";
		String transactionMsg;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		// TODO: when refund is a partial refund then refund amount should be round-down all the other cases
		// should be round-up
		String refundAmountStr = SamanPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(), false);

		Double refundAmount = new Double(refundAmountStr);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;
			Integer paymentBrokerRefNo;
			SamanRequestDTO refundRequest = new SamanRequestDTO();
			refundRequest.setMerchantId(getMerchantId());
			refundRequest.setMerchantPassword(getPassword());
			refundRequest.setRefundUser(getRefundUser());
			refundRequest.setRefundPassword(getRefundPass());
			refundRequest.setExeTime(Integer.parseInt(getExeTime().trim()));
			refundRequest.setReferenceNo(ccTransaction.getTransactionId());
			refundRequest.setRefundAmount(refundAmount);
			SRTMRefundServiceResponseDTO srtmRefExcResponse;
			String srtmRefResNumber = "";

			if (log.isDebugEnabled()) {
				log.debug("Calling SRTM Refund_Reg ");
			}
			SRTMRefundServiceResponseDTO srtmRefRegResponse = refundSRTMReg(refundRequest, merchTnxRef, creditCardPayment.getTemporyPaymentId());
			paymentBrokerRefNo = srtmRefRegResponse.getTransactionRefNo();
			
			if (SRTM_REFUND_SUCESS.equals(srtmRefRegResponse.getErrorCode())) {
				if (isImmediateRefund()){
					Long transactionReferanceID =  srtmRefRegResponse.getReferenceId();
					srtmRefResNumber = transactionReferanceID.toString();
				    srtmRefExcResponse = refundSRTMExc(refundRequest, merchTnxRef, creditCardPayment.getTemporyPaymentId(),transactionReferanceID);
				    paymentBrokerRefNo = srtmRefExcResponse.getTransactionRefNo();
				    srtmRefResNumber =  srtmRefExcResponse.getReferenceId().toString();
						if (SRTM_REFUND_SUCESS.equals(srtmRefExcResponse.getErrorCode())) {
							log.info(" SRTM REFUND_REG Succes, REFUND_EXC Succes ");
							errorCode = "";
							sr.setSuccess(true);
							sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
								PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
						} else {
							log.info(" SRTM REFUND_REG Succes, REFUND_EXC Fail ");
							String[] error = SamanPaymentUtils.getMappedUnifiedError(srtmRefExcResponse.getErrorCode());
							errorCode = error[0];
							transactionMsg = error[1];
							sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
						}
				} else {
					log.info(" SRTM REFUND_REG Succes, Not a Immidiate refund. ");
					errorCode = "";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
				}
			} else {
				log.info(" SRTM REFUND_REG Fail, Not call REFUND_EXC  ");
				String[] error = SamanPaymentUtils.getMappedUnifiedError(srtmRefRegResponse.getErrorCode());
				errorCode = error[0];
				transactionMsg = error[1];
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}			

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, srtmRefResNumber);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}

		log.debug("[PaymentBrokerSamanTemplate::refund()] End");
		return sr;
	}

	public ServiceResponce refundOld(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerSamanTemplate::refund()] Begin");
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		// TODO: when refund is a partial refund then refund amount should be round-down all the other cases
		// should be round-up
		String refundAmountStr = SamanPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(), false);

		Double refundAmount = new Double(refundAmountStr);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();

			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

			Integer paymentBrokerRefNo;

			SamanRequestDTO refundRequest = new SamanRequestDTO();
			refundRequest.setMerchantId(getMerchantId());
			refundRequest.setMerchantPassword(getPassword());
			refundRequest.setReferenceNo(ccTransaction.getTransactionId());
			refundRequest.setRefundAmount(refundAmount);

			CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef,
					creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), refundRequest.toString(),
					"", getPaymentGatewayName(), null, false);

			// Sends the refund request and gets the response
			SamanResponseDTO refundResponse = null;
			try {
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				refundResponse = ebiWebervices.reverseTransaction(refundRequest);
				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}
			if (log.isDebugEnabled()) {
				log.debug("Refund Response : " + refundResponse.toString());
			}

			boolean isSRTMRefundCallRequired = false;

			if (REFUND_SUCCESS.equals(refundResponse.getRefundResult())) {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				String[] error = SamanPaymentUtils.getMappedUnifiedError(refundResponse.getRefundResult());
				errorCode = error[0];
				transactionMsg = error[1];
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

				if (PARTIAL_REFUND_NON_SAMAN_CARD.equals(refundResponse.getRefundResult())
						|| OTHER_BANKING_NW_ERROR.equals(refundResponse.getRefundResult())) {
					isSRTMRefundCallRequired = true;

					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

					refundRequest.setRefundDateTimeStr(dateFormat.format(new Date()));
					refundRequest.setTransResNo(merchTnxRef);
					refundRequest.setRefundResNo(updatedMerchTnxRef);
					refundRequest.setRefundReferenceNo(refundResponse.getReferenceNo());
					refundRequest.setRefundErrorCode(refundResponse.getRefundResult().toString());
					refundRequest.setRefundErrorDesc(refundResponse.getRefundResultDesc());
				} else {
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				}

			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), refundResponse.toString(), errorSpecification,
					refundResponse.getReferenceNo(), refundResponse.getReferenceNo(), tnxResultCode);

			if (isSRTMRefundCallRequired) {
				if (log.isDebugEnabled()) {
					log.debug("Other card Calling SRTM Refund ");
				}
				int srtmResponse = refundSRTM(refundRequest, merchTnxRef, creditCardPayment.getTemporyPaymentId());

				if (REFUND_SUCCESS.intValue() == srtmResponse) {
					errorCode = "";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
				} else {
					String[] error = SamanPaymentUtils.getMappedUnifiedError(refundResponse.getRefundResult());
					errorCode = error[0];
					transactionMsg = error[1];
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				}
			}

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, refundResponse.getReferenceNo());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}

		log.debug("[PaymentBrokerSamanTemplate::refund()] End");
		return sr;
	}

	
	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");

	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}
		reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		return reverseResult;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {

		log.info("[PaymentBrokerSamanTemplate::resolvePartialPayments()] Begin");

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			query = getPaymentQueryDR();

			try {
				ServiceResponce serviceResponce = query.query(oCreditCardTransaction,
						OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					if (refundFlag) {
						// Do refund
						String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
								.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

						oPrevCCPayment = new PreviousCreditCardPayment();
						oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

						oCCPayment = new CreditCardPayment();
						oCCPayment.setPreviousCCPayment(oPrevCCPayment);
						oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
						oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
						oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
						oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
						oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

						ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
								oCCPayment.getTnxMode());

						if (srRev.isSuccess()) {
							// Change State to 'IS'
							oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
						} else {
							// Change State to 'IF'
							oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					} else {
						// Change State to 'IP'
						oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerSamanTemplate::resolvePartialPayments()] End");
		return sr;

	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}

	private int refundSRTM(SamanRequestDTO refundRequest, String merchTnxRef, int temporyPaymentId) throws ModuleException {

		log.debug("[PaymentBrokerSamanTemplate::refundSRTM()] Begin");
		int tnxResultCode = -20;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;

		if (isValidSRTMRefundRequest(refundRequest, merchTnxRef)) {
			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

			CreditCardTransaction ccNewTransaction = auditTransaction(refundRequest.getRefundResNo(), getMerchantId(),
					updatedMerchTnxRef, temporyPaymentId, bundle.getString("SERVICETYPE_REFUND"), refundRequest.toString(), "",
					getPaymentGatewayName(), null, false);

			String srtmRespStr = null;
			Integer paymentBrokerRefNo;
			int resultCode = -20;
			try {
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				srtmRespStr = ebiWebervices.reverseTransactionSRTM(refundRequest);
				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			if (log.isDebugEnabled()) {
				log.debug("SRTM Refund Response : " + srtmRespStr);
			}

			if (srtmRespStr != null) {
				resultCode = Integer.parseInt(srtmRespStr);
			}

			if (resultCode < 0) {
				// when Unsuccessful operation a negative integer will be returned
				status = IPGResponseDTO.STATUS_REJECTED;
				String[] error = SamanPaymentUtils.getMappedUnifiedError(resultCode);
				errorCode = error[0];
				transactionMsg = error[1];
				tnxResultCode = resultCode;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

			} else {

				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), srtmRespStr, errorSpecification, srtmRespStr,
					srtmRespStr, tnxResultCode);

			log.debug("[PaymentBrokerSamanTemplate::refundSRTM()] End");

		}
		return tnxResultCode;

	}

	
	private SRTMRefundServiceResponseDTO refundSRTMReg(SamanRequestDTO refundRequest, String merchTnxRef, int temporyPaymentId) throws ModuleException {

		log.debug("[PaymentBrokerSamanTemplate::refundSRTMReg()] Begin");
		int tnxResultCode = -20;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		SRTMRefundServiceResponseDTO srtmRfndRegRes = null;
		String transactionReferanceID ;

		if (isValidSRTMRefundRegRequest(refundRequest, merchTnxRef)) {
			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND_REG;
			refundRequest.setTransReqReferance(updatedMerchTnxRef);
			
			CreditCardTransaction ccNewTransaction = auditTransaction(refundRequest.getRefundResNo(), getMerchantId(),
					updatedMerchTnxRef, temporyPaymentId, bundle.getString("SERVICETYPE_REFUND"), refundRequest.getRefund_RegString(), "",
					getPaymentGatewayName(), null, false);
			SRTMRefundServiceResponseDTO srtmRfndRes = null;
			Integer paymentBrokerRefNo;
			String resultCode = "";
			try {
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				srtmRfndRegRes = ebiWebervices.reverseTransactionRegSRTM(refundRequest, merchTnxRef,new Long(temporyPaymentId));
				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
				srtmRfndRegRes.setTransactionRefNo(paymentBrokerRefNo);
				transactionReferanceID = srtmRfndRegRes.getReferenceId().toString();

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			if (log.isDebugEnabled()) {
				log.debug("SRTM Refund_Reg Response : " + srtmRfndRes);
			}

			if (srtmRfndRegRes != null) {
				resultCode = srtmRfndRegRes.getErrorCode();
			}

			if (!resultCode.equals(SRTM_REFUND_SUCESS)) {
				// when Unsuccessful operation a negative integer will be returned
				status = IPGResponseDTO.STATUS_REJECTED;
				String[] error = SamanPaymentUtils.getMappedUnifiedError(resultCode);
				errorCode = error[0];
				transactionMsg = error[1];
				tnxResultCode = 1;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

			} else {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
			}

			String response =  srtmRfndRegRes!= null ?srtmRfndRegRes.toString():"";
			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response , errorSpecification, transactionReferanceID,
					transactionReferanceID, tnxResultCode);

			log.debug("[PaymentBrokerSamanTemplate::refundSRTMReg()] End");

		}
		return srtmRfndRegRes;

	}
	
	private SRTMRefundServiceResponseDTO refundSRTMExc(SamanRequestDTO refundRequest, String merchTnxRef, int temporyPaymentId, Long refundRegID) throws ModuleException {

		log.debug("[PaymentBrokerSamanTemplate::refundSRTMReg()] Begin");
		int tnxResultCode = -20;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		SRTMRefundServiceResponseDTO srtmRfndExcRes = null;
		String transactionReferanceID ;

		if (isValidSRTMRefundExcRequest(refundRequest, merchTnxRef)) {
			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND_EXC;
			refundRequest.setTransReqReferance(updatedMerchTnxRef);

			CreditCardTransaction ccNewTransaction = auditTransaction(refundRequest.getRefundResNo(), getMerchantId(),
					updatedMerchTnxRef, temporyPaymentId, bundle.getString("SERVICETYPE_REFUND"), refundRequest.getRefund_ExcString(refundRegID), "",
					getPaymentGatewayName(), null, false);
			SRTMRefundServiceResponseDTO srtmRfndRes = null;
			Integer paymentBrokerRefNo;
			String resultCode = "";
			try {
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				srtmRfndExcRes = ebiWebervices.reverseTransactionExcSRTM(refundRequest, refundRegID);
				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
				srtmRfndExcRes.setTransactionRefNo(paymentBrokerRefNo);
				transactionReferanceID = srtmRfndExcRes.getReferenceId().toString();

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			if (log.isDebugEnabled()) {
				log.debug("SRTM Refund_Reg Response : " + srtmRfndRes);
			}

			if (srtmRfndExcRes != null) {
				resultCode = srtmRfndExcRes.getErrorCode();
			}

			if (!resultCode.equals(SRTM_REFUND_SUCESS)) {
				// when Unsuccessful operation a negative integer will be returned
				status = IPGResponseDTO.STATUS_REJECTED;
				String[] error = SamanPaymentUtils.getMappedUnifiedError(resultCode);
				errorCode = error[0];
				transactionMsg = error[1];
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

			} else {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
			}

			String response =  srtmRfndExcRes!= null?srtmRfndExcRes.toString():"";
			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(),response, errorSpecification, transactionReferanceID,
					transactionReferanceID, tnxResultCode);

			log.debug("[PaymentBrokerSamanTemplate::refundSRTMReg()] End");

		}
		return srtmRfndExcRes;

	}

	private boolean isValidSRTMRefundRequest(SamanRequestDTO refundRequest, String merchTnxRef) {

		if (refundRequest == null || merchTnxRef == null) {
			log.debug("Missing param in saman SRTM request");
			return false;
		}

		if (refundRequest.getMerchantId() == null || refundRequest.getMerchantPassword() == null
				|| refundRequest.getReferenceNo() == null || refundRequest.getRefundAmount() == null
				|| refundRequest.getRefundAmount().doubleValue() < 0 || refundRequest.getRefundDateTimeStr() == null
				|| refundRequest.getTransResNo() == null || refundRequest.getRefundResNo() == null
				|| refundRequest.getRefundReferenceNo() == null || refundRequest.getRefundErrorCode() == null
				|| refundRequest.getRefundErrorDesc() == null) {
			log.debug("Invalid param found in SamanRequestDTO");
			return false;
		}

		return true;
	}
	private boolean isValidSRTMRefundRegRequest(SamanRequestDTO refundRequest, String merchTnxRef) {

		if (refundRequest == null || merchTnxRef == null) {
			log.debug("Missing param in saman SRTM request");
			return false;
		}

		if (refundRequest.getMerchantId() == null || refundRequest.getMerchantPassword() == null
				|| refundRequest.getRefundUser() == null || refundRequest.getRefundPassword() == null 
				|| refundRequest.getRefundAmount() == null 
				|| refundRequest.getRefundAmount().doubleValue() < 0 ){
			log.debug("Invalid param found in SamanRequestDTO");
			return true;
		}

		return true;
	}
	private boolean isValidSRTMRefundExcRequest(SamanRequestDTO refundRequest, String merchTnxRef) {

		if (refundRequest == null || merchTnxRef == null) {
			log.debug("Missing param in saman SRTM request");
			return false;
		}

		if (refundRequest.getMerchantId() == null || refundRequest.getMerchantPassword() == null
				|| refundRequest.getRefundUser() == null || refundRequest.getRefundPassword() == null 
				|| refundRequest.getRefundAmount() == null 
				|| refundRequest.getRefundAmount().doubleValue() < 0 ){
			log.debug("Invalid param found in SamanRequestDTO");
			return true;
		}

		return true;
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
