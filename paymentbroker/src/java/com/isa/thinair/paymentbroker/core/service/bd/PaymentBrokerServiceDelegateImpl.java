package com.isa.thinair.paymentbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;

/**
 * @author Srikantha
 */
@Remote
public interface PaymentBrokerServiceDelegateImpl extends PaymentBrokerBD {

}
