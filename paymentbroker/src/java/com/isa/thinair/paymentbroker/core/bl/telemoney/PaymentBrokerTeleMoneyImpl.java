/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.paymentbroker.core.bl.telemoney;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Dhanushka Ranatunga
 */
public class PaymentBrokerTeleMoneyImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerTeleMoneyImpl.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	/**
	 * Performs a charge operation
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	/**
	 * Get the Request Data
	 * 
	 * @throws ModuleException
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		log.debug("Inside getRequestData(IPGRequestDTO ipgRequestDTO) ");

		GlobalConfig globalConfig = PaymentBrokerModuleUtil.getGlobalConfig();
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		String amount = ipgRequestDTO.getAmount();
		String baseCurrency = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		TeleMoneyIPGRequest teleMoneyIPGRequest = new TeleMoneyIPGRequest(merchantId, merchantTxnId, amount, baseCurrency,
				ipgRequestDTO.getStatusUrl(), ipgRequestDTO.getReturnUrl(), ipgURL);
		String request = teleMoneyIPGRequest.getRequest();

		if (log.isDebugEnabled()) {
			log.debug("******** IPG CHARGE REQUEST FORM PARAMETERS : " + request);
			log.debug("******** IPG CHARGE REQUEST SESSION ID : " + ipgRequestDTO.getSessionID());
		}

		String serviceType = bundle.getString("SERVICETYPE_AUTHORIZE");
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), merchantId, merchantTxnId,
				ipgRequestDTO.getApplicationTransactionId(), serviceType, request, null, getPaymentGatewayName(), null, false);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(request);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());

		log.debug("Exit getRequestData(IPGRequestDTO ipgRequestDTO) ");
		return ipgRequestResultsDTO;
	}

	/**
	 * Get the Response Data
	 */
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ripgResponseDTO) throws ModuleException {
		log.debug("Inside getReponseData(Map receiptyMap, int paymentBrokerRefNo) ");

		CreditCardTransaction ccTransaction = loadAuditTransactionByTnxRefNo(ripgResponseDTO.getPaymentBrokerRefNo());
		String merchantTnxId = ccTransaction.getTempReferenceNum();

		if (merchantTnxId == null || merchantTnxId.length() == 0) {
			throw new ModuleException("paymentbroker.capturestatus.merchanttnxid.not.found");
		}

		CreditCardTransaction ccStatusTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransactionByMerchantTransId(
				merchantTnxId + MODE_OF_SERVICE_STATUS_PREFIX);
		String status = IPGResponseDTO.STATUS_REJECTED;
		// TODO need to derive the card type
		int cardType = getStandardCardType(null);

		if (ccStatusTransaction == null) {
			log.info(" Status update not received for Merchant Id [" + merchantTnxId + MODE_OF_SERVICE_STATUS_PREFIX + "]");
			// TODO Handle Query function
			// It the query exist then make the payment response success
		} else {
			int tnxResultCode = 0;

			// This means it was successful
			if (ccStatusTransaction.getResultCode() == 0 && ccStatusTransaction.getTransactionResultCode() == 1) {
				tnxResultCode = 1;
				status = IPGResponseDTO.STATUS_ACCEPTED;
			}

			updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), ccStatusTransaction.getResponseText(),
					ccStatusTransaction.getErrorSpecification(), ccStatusTransaction.getAidCccompnay(),
					ccStatusTransaction.getTransactionId(), tnxResultCode);
		}

		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode("");
		ipgResponseDTO.setMessage(ccTransaction.getErrorSpecification());
		ipgResponseDTO.setApplicationTransactionId(ccTransaction.getTempReferenceNum());
		ipgResponseDTO.setAuthorizationCode(ccTransaction.getAidCccompnay());
		ipgResponseDTO.setCardType(cardType);

		log.debug("Exit getReponseData(Map receiptyMap, int paymentBrokerRefNo) ");
		return ipgResponseDTO;
	}

	/**
	 * Refund the credit card transaction
	 */
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.REFUND_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	/**
	 * Reverse the credit card transaction
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		// TODO Nili 10:27 AM 4/16/2008 Once we have the integration manual start working on this
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.REVERSE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	/**
	 * Returns Payment Gateway Name
	 * 
	 * @throws ModuleException
	 */
	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(ipgIdentificationParamsDTO);
		return ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * Resolves partial payments [ Invokes via a scheduler operation ]
	 */
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "paymentbroker.generic.operation.notsupported");
		return sr;
	}

	/**
	 * Capture any payment amount
	 */
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	/**
	 * Capture any status response
	 */
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		String serviceType = bundle.getString("SERVICETYPE_CAPTURE_STATUS");
		TeleMoneyIPGResponse teleMoneyIPGResponse = new TeleMoneyIPGResponse(receiptyMap);

		boolean isSuccess = false;

		if (TeleMoneyIPGResponse.STATUS_YES.equals(teleMoneyIPGResponse.getStatus())) {
			isSuccess = true;
		}

		auditTransactionAtOnce(teleMoneyIPGResponse.getMerchantId(), teleMoneyIPGResponse.getMerchantTxnId()
				+ MODE_OF_SERVICE_STATUS_PREFIX, serviceType, null, TeleMoneyIPGResponse.toString(receiptyMap),
				getPaymentGatewayName(), teleMoneyIPGResponse.getReverseId(), teleMoneyIPGResponse.getErrorMsg(),
				teleMoneyIPGResponse.getApprovalCode(), isSuccess);

		return new DefaultServiceResponse(true);
	}

	/**
	 * Returns the card type as integer
	 * 
	 * @param card
	 *            the card
	 * @return card type
	 */
	private int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && card.length() > 0 && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO - Implement card configuration data
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
