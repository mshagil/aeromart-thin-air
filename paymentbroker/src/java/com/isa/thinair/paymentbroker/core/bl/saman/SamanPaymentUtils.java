package com.isa.thinair.paymentbroker.core.bl.saman;

import java.math.BigDecimal;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;

/**
 * 
 * @author Janaka Padukka
 * 
 */
public class SamanPaymentUtils {

	private static final String ERROR_CODE_PREFIX = "saman.";

	/**
	 * Retrieves unified application error code and description from the payment gateway returned errors. Payment error
	 * codes are denoted by saman.0XXX pattern and web services errors are by saman.1XXX
	 * 
	 * @param error
	 * @return
	 */
	public static String[] getMappedUnifiedError(Object error) {

		String[] errorDesc = new String[2];

		if (error instanceof String) {

			String errorString = error.toString();

			if (errorString.equalsIgnoreCase("Canceled By User")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0001";
				errorDesc[1] = "The transaction canceled by user.";
			} else if (errorString.equalsIgnoreCase("InvalidAmount")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0002";
				errorDesc[1] = "The reverse amount not equal to the original amount.";
			} else if (errorString.equalsIgnoreCase("InvalidTransaction")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0003";
				errorDesc[1] = "The original transaction not found for this reverse amount.";
			} else if (errorString.equalsIgnoreCase("InvalidCardNumber")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0004";
				errorDesc[1] = "The card number is not valid.";
			} else if (errorString.equalsIgnoreCase("NoSuchIssuer")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0005";
				errorDesc[1] = "The card issuer is not valid.";
			} else if (errorString.equalsIgnoreCase("ExpiredCardPickUp")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0006";
				errorDesc[1] = "The card is expired.";
			} else if (errorString.equalsIgnoreCase("AllowablePINTriesExceededPickUp")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0007";
				errorDesc[1] = "The PIN has entered incorrectly for 3 times and locked!";
			} else if (errorString.equalsIgnoreCase("IncorrectPIN")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0008";
				errorDesc[1] = "The PIN has entered is incorrect";
			} else if (errorString.equalsIgnoreCase("ExceedsWithdrawalAmountLimit")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0009";
				errorDesc[1] = "The original amount is exceeded the amount limit defined by card issuer";
			} else if (errorString.equalsIgnoreCase("TransactionCannotBeCompleted")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0010";
				errorDesc[1] = "Not possible to do this transaction at this time.";
			} else if (errorString.equalsIgnoreCase("ResponseReceivedTooLate")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0011";
				errorDesc[1] = "It is a time out in banking network.";
			} else if (errorString.equalsIgnoreCase("SuspectedFraudPickUp")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0012";
				errorDesc[1] = "The CVV2 or Exp.Date is incorrect.";
			} else if (errorString.equalsIgnoreCase("NoSufficientFunds")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0013";
				errorDesc[1] = "No Sufficient Funds.";
			} else if (errorString.equalsIgnoreCase("IssuerDown_Slm")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0014";
				errorDesc[1] = "The card issuer is inoperative";
			} else if (errorString.equalsIgnoreCase("tme error")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0015";
				errorDesc[1] = "Other banking network error";
			} else if (errorString.equalsIgnoreCase("Merchant PerTransactionLimit Limit Violated")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0016";
				errorDesc[1] = "The request amount is exceeded from Merchant Per Transaction Limit.";
			}else if (errorString.equalsIgnoreCase("InvalidParameter")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0017";
				errorDesc[1] = "Request Parameters are incorrect";
			}else if (errorString.equalsIgnoreCase("AuthenticateUserFail")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0018";
				errorDesc[1] = "User Authentication fail";
			}else if (errorString.equalsIgnoreCase("DuplicateRequest")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0019";
				errorDesc[1] = "Duplicated request on PG from Merchent";
			}else if (errorString.equalsIgnoreCase("OriginalAmountIncorrect")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0020";
				errorDesc[1] = "Refund amount is higher than Paid amount";
			}else if (errorString.equalsIgnoreCase("ObjectNotFound")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0021";
				errorDesc[1] = "Refund object not be found in PG";
			}else if (errorString.equalsIgnoreCase("TransactionStateisFinal")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0022";
				errorDesc[1] = "Transaction state is final.";
			}else if (errorString.equalsIgnoreCase("CardExists")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0023";
				errorDesc[1] = "Request Parameters are incorrect";
			}else if (errorString.equalsIgnoreCase("CardNotFound")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0024";
				errorDesc[1] = "Credit card not found";
			}else if (errorString.equalsIgnoreCase("Fatal")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0025";
				errorDesc[1] = "Fatal Error";
			}else if (errorString.equalsIgnoreCase("WrongStateForDelete")) {
				errorDesc[0] = ERROR_CODE_PREFIX + "0017";
				errorDesc[1] = "Current transaction state not allows deletion";
			} else {
				errorDesc[0] = ERROR_CODE_PREFIX + "0999";
				errorDesc[1] = "Unidentified Payment Error-" + errorString;
			}

		} else {

			int errorCode;
			if (error instanceof Double) {
				errorCode = ((Double) error).intValue();
			} else {
				errorCode = ((Integer) error).intValue();
			}

			switch (errorCode) {
			case -1:
				errorDesc[0] = ERROR_CODE_PREFIX + "1001";
				errorDesc[1] = "The financial network internal error.";
				break;
			case -2:
				errorDesc[0] = ERROR_CODE_PREFIX + "1002";
				errorDesc[1] = "Deposits are not equal.";
				break;
			case -3:
				errorDesc[0] = ERROR_CODE_PREFIX + "1003";
				errorDesc[1] = "Inputs contain invalid characters.";
				break;
			case -4:
				errorDesc[0] = ERROR_CODE_PREFIX + "1004";
				errorDesc[1] = "Merchants Authentication Failed.";
				break;
			case -5:
				errorDesc[0] = ERROR_CODE_PREFIX + "1005";
				errorDesc[1] = "Database Exception.";
				break;
			case -6:
				errorDesc[0] = ERROR_CODE_PREFIX + "1006";
				errorDesc[1] = "The document has been completely reversed.";
				break;
			case -7:
				errorDesc[0] = ERROR_CODE_PREFIX + "1007";
				errorDesc[1] = "Reference number is null.";
				break;
			case -8:
				errorDesc[0] = ERROR_CODE_PREFIX + "1008";
				errorDesc[1] = "Invalid characters in reverse value.";
				break;
			case -9:
				errorDesc[0] = ERROR_CODE_PREFIX + "1009";
				errorDesc[1] = "Inputs Length is more than valid limit.";
				break;
			case -10:
				errorDesc[0] = ERROR_CODE_PREFIX + "1010";
				errorDesc[1] = "Reference number is not as Base64 form (contains invalid characters)";
				break;
			case -11:
				errorDesc[0] = ERROR_CODE_PREFIX + "1011";
				errorDesc[1] = "Inputs length is less than valid limit";
				break;
			case -12:
				errorDesc[0] = ERROR_CODE_PREFIX + "1012";
				errorDesc[1] = "The reverse value is negative.";
				break;
			case -13:
				errorDesc[0] = ERROR_CODE_PREFIX + "1013";
				errorDesc[1] = "The reversed value for detailed reverse is more than non reversed value of reference number.";
				break;
			case -14:
				errorDesc[0] = ERROR_CODE_PREFIX + "1014";
				errorDesc[1] = "The Transaction is not defined.";
				break;
			case -15:
				errorDesc[0] = ERROR_CODE_PREFIX + "1015";
				errorDesc[1] = "The Reversed Amount is not integer.";
				break;
			case -16:
				errorDesc[0] = ERROR_CODE_PREFIX + "1016";
				errorDesc[1] = "Internal Error";
				break;
			case -17:
				errorDesc[0] = ERROR_CODE_PREFIX + "1017";
				errorDesc[1] = "Partial reverse with non Saman Bank Cards";
				break;
			case -18:
				errorDesc[0] = ERROR_CODE_PREFIX + "1018";
				errorDesc[1] = "Invalid IP address.";
				break;
				
			case 12:
				errorDesc[0] = ERROR_CODE_PREFIX + "1012";
				errorDesc[1] = "Original Transaction not found.";
				break;
			case 94:
				errorDesc[0] = ERROR_CODE_PREFIX + "1094";
				errorDesc[1] = "Duplicate Request.";
				break;
			case 64:
				errorDesc[0] = ERROR_CODE_PREFIX + "1064";
				errorDesc[1] = "Exceeds Amount Limit.";
				break;
			default:
				errorDesc[0] = ERROR_CODE_PREFIX + "1999";
				errorDesc[1] = "Unidentified WS Error -" + error;
				break;

			}
		}
		return errorDesc;

	}

	public static IPGConfigsDTO getIPGConfigs(String merchantID, String password) {
		IPGConfigsDTO iPGConfigsDTO = new IPGConfigsDTO();
		iPGConfigsDTO.setMerchantID(merchantID);
		iPGConfigsDTO.setPassword(password);
		return iPGConfigsDTO;
	}

	/**
	 * Saman PG doesn't support decimal values, Saman PG supports only for IRR and PG accepts only Integer values
	 * 
	 * @param amount
	 * @return
	 */
	public static String getFormattedAmount(String amount, boolean roundUp) {

		BigDecimal amt = new BigDecimal(amount);

		if (roundUp) {
			amt = amt.setScale(0, BigDecimal.ROUND_UP);
		} else {
			amt = amt.setScale(0, BigDecimal.ROUND_DOWN);
		}

		amount = amt.toString();

		// BigDecimal value = AccelAeroCalculator.multiply(new BigDecimal(amount),
		// AccelAeroCalculator.parseBigDecimal(Math.pow(10, noOfDecimalPoints)));

		return amount;
	}
}
