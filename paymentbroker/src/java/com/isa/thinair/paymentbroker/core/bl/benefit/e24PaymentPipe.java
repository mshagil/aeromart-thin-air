/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.aciworldwide.commerce.gateway.plugins.NotEnoughDataException
 *  com.aciworldwide.commerce.gateway.plugins.SecureResource
 */
package com.isa.thinair.paymentbroker.core.bl.benefit;

import com.isa.thinair.paymentbroker.core.bl.benefit.NotEnoughDataException;
import com.isa.thinair.paymentbroker.core.bl.benefit.SecureResource;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.StringTokenizer;

public final class e24PaymentPipe {
    public static final int SUCCESS = 0;
    public static final int FAILURE = -1;
    private static final int BUFFER = 2320;
    private static final String strIDOpen = "<id>";
    private static final String strPasswordOpen = "<password>";
    private static final String strWebAddressOpen = "<webaddress>";
    private static final String strPortOpen = "<port>";
    private static final String strContextOpen = "<context>";
    private static final String strIDClose = "</id>";
    private static final String strPasswordClose = "</password>";
    private static final String strWebAddressClose = "</webaddress>";
    private static final String strPortClose = "</port>";
    private static final String strContextClose = "</context>";
    private String webAddress = "";
    private String port = "";
    private String id = "";
    private String password = "";
    private String action = "";
    private String transId = "";
    private String amt = "";
    private String responseURL = "";
    private String trackId = "";
    private String udf1 = "";
    private String udf2 = "";
    private String udf3 = "";
    private String udf4 = "";
    private String udf5 = "";
    private String paymentPage = "";
    private String paymentId = "";
    private String result = "";
    private String auth = "";
    private String ref = "";
    private String avr = "";
    private String date = "";
    private String currency = "";
    private String errorURL = "";
    private String language = "";
    private String context = "";
    private String resourcePath = "";
    private String alias = "";
    private String responseCode = "";
    private String cvv2Verification = "";
    String error = "";
    private String rawResponse = "";
    private boolean debugOn = false;
    private StringBuffer debugMsg = new StringBuffer();

    protected synchronized String getWebAddress() {
        return this.webAddress;
    }

    protected synchronized void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    protected synchronized String getPort() {
        return this.port;
    }

    protected synchronized void setPort(String port) {
        this.port = port;
    }

    protected synchronized void setId(String id) {
        this.id = id;
    }

    protected synchronized String getId() {
        return this.id;
    }

    protected synchronized void setPassword(String password) {
        this.password = password;
    }

    protected synchronized String getPassword() {
        return this.password;
    }

    public synchronized void setAction(String action) {
        this.action = action;
    }

    public synchronized String getAction() {
        return this.action;
    }

    public synchronized String getCvv2Verification() {
        return this.cvv2Verification;
    }

    public synchronized void setTransId(String transid) {
        this.transId = transid;
    }

    public synchronized String getTransId() {
        return this.transId;
    }

    public synchronized void setAmt(String amt) {
        this.amt = amt;
    }

    public synchronized String getAmt() {
        return this.amt;
    }

    public synchronized void setResponseURL(String responseURL) {
        this.responseURL = responseURL;
    }

    public synchronized String getResponseURL() {
        return this.responseURL;
    }

    public synchronized void setTrackId(String trackid) {
        this.trackId = trackid;
    }

    public synchronized String getTrackId() {
        return this.trackId;
    }

    public synchronized void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

    public synchronized String getUdf1() {
        return this.udf1;
    }

    public synchronized void setUdf2(String udf2) {
        this.udf2 = udf2;
    }

    public synchronized String getUdf2() {
        return this.udf2;
    }

    public synchronized void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public synchronized String getUdf3() {
        return this.udf3;
    }

    public synchronized void setUdf4(String udf4) {
        this.udf4 = udf4;
    }

    public synchronized String getUdf4() {
        return this.udf4;
    }

    public synchronized void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

    public synchronized String getUdf5() {
        return this.udf5;
    }

    public synchronized String getPaymentPage() {
        return this.paymentPage;
    }

    public synchronized String getPaymentId() {
        return this.paymentId;
    }

    public synchronized void setPaymentId(String payId) {
        this.paymentId = payId;
    }

    public synchronized void setPaymentPage(String payPage) {
        this.paymentPage = payPage;
    }

    public synchronized String getRedirectContent() {
        return new String("<META HTTP-EQUIV=\"Refresh\" Content=\"0; URL=" + this.paymentPage + "?PaymentID=" + this.paymentId + "\">");
    }

    public synchronized String getResult() {
        return this.result;
    }

    public synchronized String getResponseCode() {
        return this.responseCode;
    }

    public synchronized String getAuth() {
        return this.auth;
    }

    public synchronized String getAvr() {
        return this.avr;
    }

    public synchronized String getDate() {
        return this.date;
    }

    public synchronized String getRef() {
        return this.ref;
    }

    public synchronized String getCurrency() {
        return this.currency;
    }

    public synchronized void setCurrency(String currency) {
        this.currency = currency;
    }

    public synchronized String getLanguage() {
        return this.language;
    }

    public synchronized void setLanguage(String language) {
        this.language = language;
    }

    public synchronized String getErrorURL() {
        return this.errorURL;
    }

    public synchronized void setErrorURL(String url) {
        this.errorURL = url;
    }

    protected synchronized void setContext(String context) {
        this.context = context;
    }

    public synchronized String getResourcePath() {
        return this.resourcePath;
    }

    public synchronized void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public synchronized String getAlias() {
        return this.alias;
    }

    public synchronized void setAlias(String alias) {
        this.alias = alias;
    }

    public synchronized String getErrorMsg() {
        return this.error;
    }

    public synchronized String getRawResponse() {
        return this.rawResponse;
    }

    public synchronized short performPaymentInitialization() throws NotEnoughDataException {
        String response;
        StringBuffer buf = new StringBuffer();
        SecureResource secureResource = new SecureResource();
        secureResource.setDebugOn(this.debugOn);
        secureResource.setAlias(this.alias);
        secureResource.setResourcePath(this.resourcePath);
        if (!secureResource.getSecureSettings()) {
            if (this.debugOn) {
                this.debugMsg.append(secureResource.getDebugMsg());
            }
            this.error = secureResource.getError();
            return -1;
        }
        this.id = secureResource.getTermID();
        this.password = secureResource.getPassword();
        this.port = secureResource.getPort();
        this.webAddress = secureResource.getWebAddress();
        this.context = secureResource.getContext();
        if (this.id.length() > 0) {
            buf.append("id=" + this.id + "&");
        }
        if (this.password.length() > 0) {
            buf.append("password=" + this.password + "&");
        }
        if (this.amt.length() > 0) {
            buf.append("amt=" + this.amt + "&");
        }
        if (this.currency.length() > 0) {
            buf.append("currencycode=" + this.currency + "&");
        }
        if (this.action.length() > 0) {
            buf.append("action=" + this.action + "&");
        }
        if (this.language.length() > 0) {
            buf.append("langid=" + this.language + "&");
        }
        if (this.responseURL.length() > 0) {
            buf.append("responseURL=" + this.responseURL + "&");
        }
        if (this.errorURL.length() > 0) {
            buf.append("errorURL=" + this.errorURL + "&");
        }
        if (this.trackId.length() > 0) {
            buf.append("trackid=" + this.trackId + "&");
        }
        if (this.udf1.length() > 0) {
            buf.append("udf1=" + this.udf1 + "&");
        }
        if (this.udf2.length() > 0) {
            buf.append("udf2=" + this.udf2 + "&");
        }
        if (this.udf3.length() > 0) {
            buf.append("udf3=" + this.udf3 + "&");
        }
        if (this.udf4.length() > 0) {
            buf.append("udf4=" + this.udf4 + "&");
        }
        if (this.udf5.length() > 0) {
            buf.append("udf5=" + this.udf5 + "&");
        }
        if ((response = this.sendMessage(buf.toString(), "PaymentInitHTTPServlet")) == null) {
            return -1;
        }
        int colonIdx = response.indexOf(":");
        if (colonIdx == -1) {
            this.error = "Payment Initialization returned an invalid response: " + response;
            return -1;
        }
        this.paymentId = response.substring(0, colonIdx);
        this.paymentPage = response.substring(colonIdx + 1);
        return 0;
    }

    public synchronized short performTransaction() throws NotEnoughDataException {
        String response;
        StringBuffer buf = new StringBuffer();
        SecureResource secureResource = new SecureResource();
        secureResource.setDebugOn(this.debugOn);
        secureResource.setAlias(this.alias);
        secureResource.setResourcePath(this.resourcePath);
        if (!secureResource.getSecureSettings()) {
            if (this.debugOn) {
                this.debugMsg.append(secureResource.getDebugMsg());
            }
            this.error = secureResource.getError();
            return -1;
        }
        this.id = secureResource.getTermID();
        this.password = secureResource.getPassword();
        this.port = secureResource.getPort();
        this.webAddress = secureResource.getWebAddress();
        this.context = secureResource.getContext();
        if (this.id.length() > 0) {
            buf.append("id=" + this.id + "&");
        }
        if (this.password.length() > 0) {
            buf.append("password=" + this.password + "&");
        }
        if (this.amt.length() > 0) {
            buf.append("amt=" + this.amt + "&");
        }
        if (this.action.length() > 0) {
            buf.append("action=" + this.action + "&");
        }
        if (this.paymentId.length() > 0) {
            buf.append("paymentid=" + this.paymentId + "&");
        }
        if (this.transId.length() > 0) {
            buf.append("transid=" + this.transId + "&");
        }
        if (this.trackId.length() > 0) {
            buf.append("trackid=" + this.trackId + "&");
        }
        if (this.udf1.length() > 0) {
            buf.append("udf1=" + this.udf1 + "&");
        }
        if (this.udf2.length() > 0) {
            buf.append("udf2=" + this.udf2 + "&");
        }
        if (this.udf3.length() > 0) {
            buf.append("udf3=" + this.udf3 + "&");
        }
        if (this.udf4.length() > 0) {
            buf.append("udf4=" + this.udf4 + "&");
        }
        if (this.udf5.length() > 0) {
            buf.append("udf5=" + this.udf5 + "&");
        }
        if ((response = this.sendMessage(buf.toString(), "PaymentTranHTTPServlet")) == null) {
            return -1;
        }
        ArrayList fields = this.parseResults(response);
        if (fields == null) {
            return -1;
        }
        this.result = (String)fields.get(0);
        this.auth = (String)fields.get(1);
        this.ref = (String)fields.get(2);
        this.avr = (String)fields.get(3);
        this.date = (String)fields.get(4);
        this.transId = (String)fields.get(5);
        this.trackId = (String)fields.get(6);
        this.udf1 = (String)fields.get(7);
        this.udf2 = (String)fields.get(8);
        this.udf3 = (String)fields.get(9);
        this.udf4 = (String)fields.get(10);
        this.udf5 = (String)fields.get(11);
        this.responseCode = (String)fields.get(12);
        this.cvv2Verification = (String)fields.get(13);
        return 0;
    }

    private synchronized String sendMessage(String msg, String servletName) throws NotEnoughDataException {
        StringBuffer urlBuf = new StringBuffer();
        if (this.debugOn) {
            this.debugMsg.append("\n---------- " + servletName + ": " + String.valueOf(new Timestamp(System.currentTimeMillis())) + " ----------");
        }
        if (this.port.equals("443")) {
            System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
        }
        try {
            if (this.webAddress.length() <= 0) {
                this.error = "No URL specified.";
                return null;
            }
            if (this.port.equals("443")) {
                urlBuf.append("https://");
            } else {
                urlBuf.append("http://");
            }
            urlBuf.append(this.webAddress);
            if (this.port.length() > 0) {
                urlBuf.append(":");
                urlBuf.append(this.port);
            }
            if (this.context.length() > 0) {
                if (!this.context.startsWith("/")) {
                    urlBuf.append("/");
                }
                urlBuf.append(this.context);
                if (!this.context.endsWith("/")) {
                    urlBuf.append("/");
                }
            } else {
                urlBuf.append("/");
            }
            urlBuf.append("servlet/");
            urlBuf.append(servletName);
            if (this.debugOn) {
                this.debugMsg.append("\nAbout to create the URL to: " + urlBuf.toString());
            }
            URL u = new URL(urlBuf.toString());
            if (this.debugOn) {
                this.debugMsg.append("\nAbout to create http connection");
            }
            URLConnection c = u.openConnection();
            if (this.debugOn) {
                this.debugMsg.append("\nCreated connection");
            }
            c.setDoInput(true);
            c.setDoOutput(true);
            c.setUseCaches(false);
            c.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            if (this.debugOn) {
                this.debugMsg.append("\nREQUEST: " + msg);
            }
            if (msg.length() > 0) {
                if (this.debugOn) {
                    this.debugMsg.append("\nabout to write DataOutputSteam");
                }
                DataOutputStream dos = new DataOutputStream(c.getOutputStream());
                if (this.debugOn) {
                    this.debugMsg.append("\nafter DataOutputStream");
                }
                dos.writeBytes(msg);
                dos.flush();
                dos.close();
                BufferedReader dis = new BufferedReader(new InputStreamReader(c.getInputStream()));
                this.rawResponse = dis.readLine();
                if (this.debugOn) {
                    this.debugMsg.append("\nReceived RESPONSE: " + this.rawResponse);
                }
                return this.rawResponse;
            }
            this.error = "No Data To Post!";
            throw new NotEnoughDataException(this.error);
        }
        catch (Exception e) {
            this.clearFields();
            this.error = "Failed to make connection:\n" + e;
            return null;
        }
    }

    private ArrayList parseResults(String response) {
        ArrayList<String> fields = new ArrayList<String>(4);
        try {
            if (response.startsWith("!ERROR!")) {
                this.error = response;
                return null;
            }
            StringTokenizer st = new StringTokenizer(response, ":\r\n", true);
            String token = "";
            boolean prevDelim = false;
            while (st.hasMoreElements()) {
                token = st.nextToken();
                if (!token.startsWith(":")) {
                    fields.add(token);
                    prevDelim = false;
                    continue;
                }
                if (prevDelim) {
                    fields.add("");
                }
                prevDelim = true;
            }
            return fields;
        }
        catch (Exception e) {
            System.out.println(e);
            this.error = "Internal Error!";
            return null;
        }
    }

    public void clearFields() {
        this.error = "";
        this.paymentPage = "";
        this.paymentId = "";
        this.responseCode = "";
    }

    public synchronized boolean getDebugOn() {
        return this.debugOn;
    }

    public synchronized void setDebug(boolean state) {
        this.debugOn = state;
    }

    public synchronized String getDebugMsg() {
        if (this.debugOn && this.debugMsg != null) {
            return this.debugMsg.toString();
        }
        return "";
    }
}
