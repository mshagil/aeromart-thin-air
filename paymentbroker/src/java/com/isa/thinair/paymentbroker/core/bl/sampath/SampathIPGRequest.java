package com.isa.thinair.paymentbroker.core.bl.sampath;

public class SampathIPGRequest {

	public static final String ACTION_CHARGE = "SaleTxnNoPage";

	private static final String ACTION_REVERSE = "reverse";

	private static final String ACTION_REFUND = "refund";

	private String action;

	private String cardNumber;

	private String currencyCode;

	private String cvcNumber;

	private String expiryMonth;

	private String expiryYear;

	private String cardHolderName;

	private String languageCode;

	private String merchantId;

	private String merchantTransactionId;

	private String merchantVariable1;

	private String merchantVariable2;

	private String merchantVariable3;

	private String merchantVariable4;

	private String productType;

	private String returnURL;

	private String transactionAmount;

	/**
	 * @return Returns the action.
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action
	 *            The action to set.
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return Returns the cardHolderName.
	 */
	public String getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * @param cardHolderName
	 *            The cardHolderName to set.
	 */
	public void setCardHolderName(String holderName) {
		this.cardHolderName = holderName;
	}

	/**
	 * @return Returns the cardNumber.
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber
	 *            The cardNumber to set.
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return Returns the currencyCode.
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            The currencyCode to set.
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return Returns the cvcNumber.
	 */
	public String getCvcNumber() {
		return cvcNumber;
	}

	/**
	 * @param cvcNumber
	 *            The cvcNumber to set.
	 */
	public void setCvcNumber(String cvcNumber) {
		this.cvcNumber = cvcNumber;
	}

	/**
	 * @return Returns the expiryMonth.
	 */
	public String getExpiryMonth() {
		return expiryMonth;
	}

	/**
	 * @param expiryMonth
	 *            The expiryMonth to set.
	 */
	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	/**
	 * @return Returns the expiryYear.
	 */
	public String getExpiryYear() {
		return expiryYear;
	}

	/**
	 * @param expiryYear
	 *            The expiryYear to set.
	 */
	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}

	/**
	 * @return Returns the languageCode.
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * @param languageCode
	 *            The languageCode to set.
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return Returns the merchantId.
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            The merchantId to set.
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return Returns the merchantTransactionId.
	 */
	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}

	/**
	 * @param merchantTransactionId
	 *            The merchantTransactionId to set.
	 */
	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	/**
	 * @return Returns the merchantVariable1.
	 */
	public String getMerchantVariable1() {
		return merchantVariable1;
	}

	/**
	 * @param merchantVariable1
	 *            The merchantVariable1 to set.
	 */
	public void setMerchantVariable1(String merchantVariable1) {
		this.merchantVariable1 = merchantVariable1;
	}

	/**
	 * @return Returns the merchantVariable2.
	 */
	public String getMerchantVariable2() {
		return merchantVariable2;
	}

	/**
	 * @param merchantVariable2
	 *            The merchantVariable2 to set.
	 */
	public void setMerchantVariable2(String merchantVariable2) {
		this.merchantVariable2 = merchantVariable2;
	}

	/**
	 * @return Returns the merchantVariable3.
	 */
	public String getMerchantVariable3() {
		return merchantVariable3;
	}

	/**
	 * @param merchantVariable3
	 *            The merchantVariable3 to set.
	 */
	public void setMerchantVariable3(String merchantVariable3) {
		this.merchantVariable3 = merchantVariable3;
	}

	/**
	 * @return Returns the merchantVariable4.
	 */
	public String getMerchantVariable4() {
		return merchantVariable4;
	}

	/**
	 * @param merchantVariable4
	 *            The merchantVariable4 to set.
	 */
	public void setMerchantVariable4(String merchantVariable4) {
		this.merchantVariable4 = merchantVariable4;
	}

	/**
	 * @return Returns the productType.
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType
	 *            The productType to set.
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * @return Returns the returnURL.
	 */
	public String getReturnURL() {
		return returnURL;
	}

	/**
	 * @param returnURL
	 *            The returnURL to set.
	 */
	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	/**
	 * @return Returns the transactionAmount.
	 */
	public String getTransactionAmount() {
		return transactionAmount;
	}

	/**
	 * @param transactionAmount
	 *            The transactionAmount to set.
	 */
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	/**
	 * Returns the XML Request Messages
	 * 
	 * @param loadMaskXML
	 * @return
	 */
	public String[] getXML(boolean loadMaskXML) {
		validateRequest();

		StringBuilder sbOriginal = new StringBuilder();

		sbOriginal.append("<req>");
		sbOriginal.append("<action>" + action + "</action>");
		sbOriginal.append("<card_number>" + cardNumber + "</card_number>");
		sbOriginal.append("<cur>" + currencyCode + "</cur>");
		sbOriginal.append("<cvc>" + cvcNumber + "</cvc>");
		sbOriginal.append("<exp_month>" + expiryMonth + "</exp_month>");
		sbOriginal.append("<exp_year>" + expiryYear + "</exp_year>");
		sbOriginal.append("<holder_name>" + cardHolderName + "</holder_name>");
		sbOriginal.append("<lang>" + languageCode + "</lang>");
		sbOriginal.append("<mer_id>" + merchantId + "</mer_id>");
		sbOriginal.append("<mer_txn_id>" + merchantTransactionId + "</mer_txn_id>");
		sbOriginal.append("<mer_var1>" + merchantVariable1 + "</mer_var1>");
		sbOriginal.append("<mer_var2>" + merchantVariable2 + "</mer_var2>");
		sbOriginal.append("<mer_var3>" + merchantVariable3 + "</mer_var3>");
		sbOriginal.append("<mer_var4>" + merchantVariable4 + "</mer_var4>");
		sbOriginal.append("<prod_type>" + productType + "</prod_type>");
		sbOriginal.append("<ret_url>" + returnURL + "</ret_url>");
		sbOriginal.append("<txn_amt>" + transactionAmount + "</txn_amt>");
		sbOriginal.append("</req>");

		StringBuilder sbMask = new StringBuilder();

		if (loadMaskXML) {
			sbMask.append("<req>");
			sbMask.append("<action>" + action + "</action>");
			sbMask.append("<card_number>" + maskCCNumber(cardNumber) + "</card_number>");
			sbMask.append("<cur>" + currencyCode + "</cur>");
			sbMask.append("<cvc>" + cvcNumber + "</cvc>");
			sbMask.append("<exp_month>" + expiryMonth + "</exp_month>");
			sbMask.append("<exp_year>" + expiryYear + "</exp_year>");
			sbMask.append("<holder_name>" + cardHolderName + "</holder_name>");
			sbMask.append("<lang>" + languageCode + "</lang>");
			sbMask.append("<mer_id>" + merchantId + "</mer_id>");
			sbMask.append("<mer_txn_id>" + merchantTransactionId + "</mer_txn_id>");
			sbMask.append("<mer_var1>" + merchantVariable1 + "</mer_var1>");
			sbMask.append("<mer_var2>" + merchantVariable2 + "</mer_var2>");
			sbMask.append("<mer_var3>" + merchantVariable3 + "</mer_var3>");
			sbMask.append("<mer_var4>" + merchantVariable4 + "</mer_var4>");
			sbMask.append("<prod_type>" + productType + "</prod_type>");
			sbMask.append("<ret_url>" + returnURL + "</ret_url>");
			sbMask.append("<txn_amt>" + transactionAmount + "</txn_amt>");
			sbMask.append("</req>");
		}

		return new String[] { sbOriginal.toString(), sbMask.toString() };
	}

	/**
	 * Mask the Credit Card Number
	 * 
	 * @param cardNumber
	 * @return
	 */
	private static String maskCCNumber(String cardNumber) {
		String maskCCNumber = replaceNull(cardNumber);

		if (maskCCNumber.length() > 0) {
			int maskCount = maskCCNumber.substring(0, maskCCNumber.length() - 4).length();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < maskCount; i++) {
				sb.append("*");
			}

			maskCCNumber = sb.toString() + maskCCNumber.substring(maskCCNumber.length() - 4);
		}

		return maskCCNumber;
	}

	private void validateRequest() {
		if (!isValidAction()) {
			throw new RuntimeException("Sampath IPG: Invalid action name: " + action);
		}

		if (replaceNull(cardNumber).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid cardNumber: " + cardNumber);
		}

		if (replaceNull(currencyCode).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid currencyCode: " + currencyCode);
		}

		if (replaceNull(cvcNumber).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid cvcNumber: " + cvcNumber);
		}

		if (replaceNull(expiryMonth).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid expiryMonth: " + expiryMonth);
		}

		if (replaceNull(expiryYear).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid expiryYear: " + expiryYear);
		}

		if (replaceNull(cardHolderName).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid expiryYear: " + cardHolderName);
		}

		if (replaceNull(merchantId).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid merchantId: " + merchantId);
		}

		if (replaceNull(merchantTransactionId).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid merchantTransactionId: " + merchantTransactionId);
		}

		if (replaceNull(productType).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid productType: " + productType);
		}

		if (replaceNull(returnURL).equals("")) {
			// throw new RuntimeException("Sampath IPG: Invalid returnURL: " + returnURL);
		}

		if (replaceNull(transactionAmount).equals("")) {
			throw new RuntimeException("Sampath IPG: Invalid transactionAmount: " + transactionAmount);
		}

	}

	private boolean isValidAction() {
		boolean blnValid = false;

		if (action != null) {
			if (ACTION_CHARGE.equals(action)) {
				blnValid = true;
			} else if (ACTION_REVERSE.equals(action)) {
				blnValid = true;
			} else if (ACTION_REFUND.equals(action)) {
				blnValid = true;
			}
		}

		return blnValid;
	}

	private static String replaceNull(String str) {
		return (str == null) ? "" : str.trim();
	}
}
