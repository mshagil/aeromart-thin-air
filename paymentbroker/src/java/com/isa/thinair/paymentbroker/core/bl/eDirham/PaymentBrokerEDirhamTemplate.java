package com.isa.thinair.paymentbroker.core.bl.eDirham;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.ogone.OgonePaymentUtils;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerEDirhamTemplate extends PaymentBrokerTemplate implements PaymentBroker {

	protected String operation;

	protected String nextOperation;

	protected String bankId;

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected String terminalId;

	protected String serviceMainCodeSubCode;

	protected String sha1In;

	protected String sha1Out;

	protected String serverToServerReqURL;

	protected String applicationNumber;

	protected String serviceQuantity;

	protected List<String> supportedLanguages;

	protected static final String CARD_TYPE_GENERIC = "GC";

	private static Log log = LogFactory.getLog(PaymentBrokerEDirhamTemplate.class);

	public Properties getProperties() {
		Properties props = super.getProperties();
		props.put("bankId", PlatformUtiltiies.nullHandler(bankId));
		props.put("terminalId", PlatformUtiltiies.nullHandler(terminalId));
		props.put("serverToServerReqURL", PlatformUtiltiies.nullHandler(serverToServerReqURL));
		props.put("serviceMainCodeSubCode", PlatformUtiltiies.nullHandler(serviceMainCodeSubCode));
		props.put("applicationNumber", PlatformUtiltiies.nullHandler(applicationNumber));
		props.put("serviceQuantity", PlatformUtiltiies.nullHandler(serviceQuantity));
		props.put("supportedLanguages", PlatformUtiltiies.nullHandler(supportedLanguages));
		return props;
	}


	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REFUND_OPERATION_NOT_SUPPORTED);
			return defaultServiceResponse;
		} else {
			int tnxResultCode;
			String errorCode;
			String transactionMsg;
			String status;
			String errorSpecification;
			String merchTnxRef;

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			String amount = PaymentBrokerUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());

			PaymentQueryDR queryDR = getPaymentQueryDR();
			ServiceResponce serviceResponce = queryDR.query(ccTransaction,
					EDirhamPaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
					PaymentBrokerInternalConstants.QUERY_CALLER.REFUND);

			if (creditCardPayment.getPreviousCCPayment() != null && serviceResponce.isSuccess()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

				ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
				if (!serviceResponse.isSuccess()) {
					return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
							ccTransaction.getPaymentGatewayConfigurationName());
				}

				merchTnxRef = ccTransaction.getTempReferenceNum();

				String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

				Map<String, String> postDataMap = new LinkedHashMap<String, String>();
				postDataMap.put(EDirhamRequest.ACTION, EDirhamPaymentUtils.EDIRHAM_OPERATION.REFUND.getOperation());
				postDataMap.put(EDirhamRequest.BANKID, getBankId());
				postDataMap.put(EDirhamRequest.MERCHANTID, getMerchantId());
				postDataMap.put(EDirhamRequest.TERMINALID, getTerminalId());
				postDataMap.put(EDirhamRequest.TRANSACTIONREQUESTDATE, EDirhamPaymentUtils.getTransactionReqTime());
				postDataMap.put(EDirhamRequest.CURRENCY,
						getNumericCurrencyCode(ipgIdentificationParamsDTO.getPaymentCurrencyCode()));
				postDataMap.put(EDirhamRequest.FIELD63_ORIGINALTRANSACTIONUNIQUEID, merchTnxRef);
				postDataMap.put(EDirhamRequest.FIELD61_SERVICEMAINCODESUBCODE, "000000-0008");
				postDataMap.put(EDirhamRequest.FIELD61_SERVICECODEQUANTITY, "1");
				postDataMap.put(EDirhamRequest.TRANSACTIONAMOUNT, creditCardPayment.getAmount());

				postDataMap.put("PROXY_HOST", getProxyHost());
				postDataMap.put("PROXY_PORT", getProxyPort());
				postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
				postDataMap.put("URL", getServerToServerReqURL());

				String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

				CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef,
						creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), strRequestParams, "",
						getPaymentGatewayName(), null, false);

				postDataMap.put(EDirhamRequest.UNIQUETRANSACTIONID, ccNewTransaction.getTemporyPaymentId().toString());
				Integer paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

				try {
					String secureHash = EDirhamPaymentUtils.computeSecureHash(postDataMap, getSha1In(),
							EDirhamPaymentUtils.EDIRHAM_OPERATION.REFUND);
					postDataMap.put(EDirhamRequest.SECUREHASH, secureHash);
					strRequestParams = strRequestParams + "," + EDirhamRequest.SECUREHASH + "=" + secureHash;
				} catch (Exception e) {
					log.debug("Error computing SECUREHASH : " + e.toString());
					throw new ModuleException(e.getMessage());
				}

				EDirhamResponse refundResponse = EDirhamPaymentUtils.getEDirhamResponse(postDataMap, getSha1In(),
						EDirhamPaymentUtils.EDIRHAM_OPERATION.REFUND);

				log.debug("Refund Response : " + refundResponse.toString());

				DefaultServiceResponse sr = new DefaultServiceResponse(false);

				if (refundResponse.getStatus().equals("")) {
					errorCode = "";
					transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
					errorSpecification = "";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
				} else {
					errorCode = EDirhamPaymentUtils.getErrorInfo(refundResponse);
					transactionMsg = refundResponse.getStatusMessage();
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				}

				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), refundResponse.toString(), errorSpecification,
						refundResponse.getStatus(),
						refundResponse.getConfirmationID() + ":" + refundResponse.getRetrievalRefNumber(), tnxResultCode);

				sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, refundResponse.getConfirmationID());
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
				return sr;
			} else {
				DefaultServiceResponse sr = new DefaultServiceResponse(false);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

				return sr;
			}
		}
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		log.info("[PaymentBrokerEDirhamTemplate::resolvePartialPayments()] Begin");

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;
		PaymentQueryDR query;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			query = getPaymentQueryDR();

			try {
				
				ServiceResponce serviceResponce = query.query(oCreditCardTransaction,
						OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);				
				
					// If successful payment remains at Bank
					if (serviceResponce.isSuccess()) {
						String responseStr = (String) serviceResponce
								.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);

						BigDecimal receivedEDirhamFee = AccelAeroCalculator.getDefaultBigDecimalZero();

						BigDecimal paymentAmount = ReservationModuleUtils.getReservationBD().getTempPaymentAmount(
								oCreditCardTransaction.getTemporyPaymentId());						

						if (EDirhamPaymentUtils.TRANSACTION_SUCCESSFULL.equals(responseStr)) {

							BigDecimal receivedPayAmount = new BigDecimal((String)serviceResponce.getResponseParam(PaymentConstants.AMOUNT));
							receivedEDirhamFee = new BigDecimal((String)serviceResponce.getResponseParam(PaymentConstants.EDIRHAM_AMOUNT));
							// TODO handle other fees if they are in the response

							receivedPayAmount = AccelAeroCalculator.divide(receivedPayAmount, 100);
							receivedEDirhamFee = AccelAeroCalculator.divide(receivedEDirhamFee, 100);

							log.debug("[PaymentBrokerEDirhamECommerceImpl::getReponseData()] - Payment Amount-" + paymentAmount
									+ " receivedPayAmount-" + receivedPayAmount + " receivedEDirhamFee-" + receivedEDirhamFee);

							if (paymentAmount.compareTo(receivedPayAmount.subtract(receivedEDirhamFee)) == 0) {
								ipgQueryDTO.setBaseAmount(receivedPayAmount);
								ipgQueryDTO.seteDirhamFee(receivedEDirhamFee);
								boolean isResConfirmationSuccess = ConfirmReservationUtil
										.isReservationConfirmationSuccess(ipgQueryDTO);

								if (isResConfirmationSuccess) {
									if (log.isDebugEnabled()) {
										log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
									}

								} else {
									if (refundFlag) {
										// Do refund
										String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
												.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

									oPrevCCPayment = new PreviousCreditCardPayment();
									oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
									oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

									oCCPayment = new CreditCardPayment();
									oCCPayment.setPreviousCCPayment(oPrevCCPayment);
									oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
									oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
									oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
									oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
									oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
									oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

									ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
											oCCPayment.getTnxMode());

									if (srRev.isSuccess()) {
											// Change State to 'IS'
											oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
										log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
										} else {
											// Change State to 'IF'
											oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
										log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
										}
									} else {
										// Change State to 'IP'
										oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
									log.info("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
									}
							}
						} else {
							if (refundFlag) {
								// Do refund
								String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
										.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

								oPrevCCPayment = new PreviousCreditCardPayment();
								oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
								oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

								oCCPayment = new CreditCardPayment();
								oCCPayment.setPreviousCCPayment(oPrevCCPayment);
								oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
								oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
								oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
								oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
								oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
								oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

								ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
										oCCPayment.getTnxMode());

								if (srRev.isSuccess()) {
									// Change State to 'IS'
									oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
									log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
							} else {
									// Change State to 'IF'
									oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
									log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
							}
							} else {
								// Change State to 'IP'
								oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
								log.info("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					} else {
						// No Payments exist
						// Update status to 'II'
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				} else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}
				
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerBehpardakhtECommerceImpl::resolvePartialPayments()] End");
		return sr;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stubdsff
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	protected String getStandardCardType(String card) {
		// 1-1: Visa Card, 1-2: Master Card,1-9: GII Co-branded Mag-Stripe Card,1-10: Personal GII Co-Branded Chip Card
		// 1-11: Corporate GII Co-Branded Chip Card,1-12: GII PLC Mag-Stripe Card,1-13: GII PLC Chip Card
		String mapTypeStr = "1-9"; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			mapTypeStr = (String) mapCardType.get(card);
		}
		return mapTypeStr;
	}

	/**
	 * This method use to get 4217 ISO standard numeric code for currency. AED = 784; EUR = 978;
	 * 
	 * @param currencyCode
	 *            String containing the currency code
	 * @return relevant currency numeric code
	 */
	protected String getNumericCurrencyCode(String currency) throws ModuleException {

		if (currency != null && !currency.equals("")) {
			String currencyCode = PaymentBrokerUtils.getPaymentBrokerDAO().getCurrencyNumericCode(currency,
					ipgIdentificationParamsDTO.getIpgId());
			if (currencyCode != null && !currencyCode.isEmpty()) {
				return currencyCode;
			} else {
				throw new ModuleException("pay.gateway.error.edirham.currencyNotSupported");
			}
		} else {
			throw new ModuleException("pay.gateway.error.edirham.currencyNotSupported");
		}
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getNextOperation() {
		return nextOperation;
	}

	public void setNextOperation(String nextOperation) {
		this.nextOperation = nextOperation;
	}

	public String getSha1In() {
		return sha1In;
	}

	public void setSha1In(String sha1In) {
		this.sha1In = sha1In;
	}

	public String getSha1Out() {
		return sha1Out;
	}

	public void setSha1Out(String sha1Out) {
		this.sha1Out = sha1Out;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getServerToServerReqURL() {
		return serverToServerReqURL;
	}

	public void setServerToServerReqURL(String serverToServerReqURL) {
		this.serverToServerReqURL = serverToServerReqURL;
	}

	public String getServiceMainCodeSubCode() {
		return serviceMainCodeSubCode;
	}

	public void setServiceMainCodeSubCode(String serviceMainCodeSubCode) {
		this.serviceMainCodeSubCode = serviceMainCodeSubCode;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getServiceQuantity() {
		return serviceQuantity;
	}

	public void setServiceQuantity(String serviceQuantity) {
		this.serviceQuantity = serviceQuantity;
	}


	public List<String> getSupportedLanguages() {
		return supportedLanguages;
	}


	public void setSupportedLanguages(List<String> supportedLanguages) {
		this.supportedLanguages = supportedLanguages;
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
