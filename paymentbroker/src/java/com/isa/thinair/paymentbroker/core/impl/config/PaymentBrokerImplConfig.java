package com.isa.thinair.paymentbroker.core.impl.config;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.platform.api.IConfig;

public class PaymentBrokerImplConfig implements IConfig {

	private PaymentBroker paymentBroker;
	private Map paymentBrokerMap;
	private Map<String, PaymentBroker> searchSet = new HashMap<String, PaymentBroker>();

	/**
	 * @return Returns the paymentBroker.
	 */
	public PaymentBroker getPaymentBroker() {
		return paymentBroker;
	}

	/**
	 * @param paymentBroker
	 *            The paymentBroker to set.
	 */
	public void setPaymentBroker(PaymentBroker paymentBroker) {
		this.paymentBroker = paymentBroker;
	}

	/**
	 * @return Returns the paymentBrokerMap.
	 */
	public Map getPaymentBrokerMap() {
		return paymentBrokerMap;
	}

	/**
	 * @param paymentBrokerMap
	 *            The paymentBrokerMap to set.
	 */
	public void setPaymentBrokerMap(Map paymentBrokerMap) {
		this.paymentBrokerMap = paymentBrokerMap;
	}

	/**
	 * @return Returns the searchSet.
	 */
	// public PaymentBroker getPaymentBroker(PAYMENT_GATEWAY_TYPE paymentGateway) {
	// if (searchSet.size() == 0) {
	// String value;
	// for (Iterator iter = getPaymentBrokerMap().keySet().iterator(); iter.hasNext();) {
	// value = (String) iter.next();
	// searchSet.put(value.toUpperCase(), (PaymentBroker) getPaymentBrokerMap().get(value));
	// }
	// }
	//
	// return searchSet.get(paymentGateway.toString().toUpperCase());
	// }

}
