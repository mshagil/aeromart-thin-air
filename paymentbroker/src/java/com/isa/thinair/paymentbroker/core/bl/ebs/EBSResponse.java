package com.isa.thinair.paymentbroker.core.bl.ebs;

import java.util.Date;

import com.isa.thinair.paymentbroker.core.bl.ogone.OgoneResponse;

public class EBSResponse extends OgoneResponse {
	
	public static final String XML_RESPONSE = "response";
	
	public static final String XML_TRANSACTIONID = "transationId";
	
	public static final String XML_PAYMENTID = "paymentId";
	
	public static final String XML_AMOUNT = "amount";
	
	public static final String XML_DATETIME = "dateTime";
	
	public static final String XML_MODE = "mode";
	
	public static final String XML_REFERENCENO = "referenceNo";
	
	public static final String XML_TRANSATIONTYPE = "transationType";
	
	public static final String XML_STATUS = "status";
	
	public static final String XML_ISFLAGGED = "isFlagged";
	
	private String ebsResponse;
	
	private String transationId;
	
	private String paymentId;
	
	private Date dateTime;
	
	private String mode;
		
	private String referenceNo;
	
	private String transationType;	
	
	private String isFagged;
	
	public static String ORDERID = "ORDERID";
	
	public static String AMOUNT = "AMOUNT";

	public static String CURRENCY = "CURRENCY";

	public String getEbsResponse() {
		return ebsResponse;
	}

	public void setEbsResponse(String ebsResponse) {
		this.ebsResponse = ebsResponse;
	}

	public String getTransationId() {
		return transationId;
	}

	public void setTransationId(String transationId) {
		this.transationId = transationId;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getTransationType() {
		return transationType;
	}

	public void setTransationType(String transationType) {
		this.transationType = transationType;
	}

	public String getIsFagged() {
		return isFagged;
	}

	public void setIsFagged(String isFagged) {
		this.isFagged = isFagged;
	}	

}
