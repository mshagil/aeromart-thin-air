package com.isa.thinair.paymentbroker.core.bl.payFort.v2;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.PayFortOnlineParam;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerPayFortTemplate extends PaymentBrokerTemplate implements PaymentBroker {

    protected static final ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
    private static final Log log = LogFactory.getLog(PaymentBrokerPayFortTemplate.class);

    protected static final String BILL_CREATION_SUCCESS_STATUS_CODE = "46";
    protected static final String INVOICE_GENERATION_SUCCESS = "48";
    protected static final String CANCELLED_TRANSACTION_RESPONSE_CODE = "072";
    protected static final String INSTALLMENTS = "installments";
    private static final String SUCCESS_PURCHASE_STATUS_CODE = "14";
    private static final String CHECK_STATUS_SUCCESS_STATUS_CODE = "12";
    private static final String REFUND_SUCCESS_STATUS_CODE = "06";

    private String encryptionKey;
    private String serviceName;
    private String itemPrefix;
    private String expireTimeInMinutes;
    private String shaRequestPhrase;
    private String shaResponsePhrase;
    private int currencyMultiplier;
    private boolean isOfflinePGW = false;

    public String getExpireTimeInMinutes() {
        return expireTimeInMinutes;
    }

    public void setExpireTimeInMinutes(String expireTimeInMinutes) {
        this.expireTimeInMinutes = expireTimeInMinutes;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public String getItemPrefix() {
        return itemPrefix;
    }

    public void setItemPrefix(String itemPrefix) {
        this.itemPrefix = itemPrefix;
    }

    @Override
    public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
                                  TnxModeEnum tnxMode) throws ModuleException {
        log.debug("[PaymentBrokerPayFortPayOnline::refund()] Begin");
        int tnxResultCode;
        String errorCode = "";
        String errorSpecification = "";
        Integer newTransacRefNo;
        Map<String, String> response;

        CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
                creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
        String merchTnxRef = ccTransaction.getTempReferenceNum();
        String refundAmountStr = creditCardPayment.getAmount();
        DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);

        if (creditCardPayment.getPreviousCCPayment() != null && BooleanUtils.toBoolean(getRefundEnabled())) {

            if (log.isDebugEnabled()) {
                log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
                log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
                log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
                log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
                log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
                log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
            }

            Map<String, String> postDataMap = createRefundDataMap(ccTransaction.getTempReferenceNum(),
                    creditCardPayment.getCurrency(), creditCardPayment.getAmount());
            String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
            String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

            try {
                response = getResponseFromPayFort(postDataMap, getReversalURL());
            } catch (Exception e) {
                log.error(e);
                throw new ModuleException("paymentbroker.error.unknown");
            }

            CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef,
                    creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), strRequestParams, "",
                    getPaymentGatewayName(), null, false);
            newTransacRefNo = ccNewTransaction.getTransactionRefNo();

            Integer paymentBrokerRefNo = ccTransaction.getTransactionRefNo();
            log.info("[PaymentBrokerPayFortPayOnline::refund()] Refund Status : " + response.toString());

            if (isValidRefund(response)) {
                tnxResultCode = 1;
                defaultServiceResponse.setSuccess(true);
                defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
                        PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
            } else {
                errorCode = response.get(PayFortOnlineParam.RESPONSE_CODE.getName());
                tnxResultCode = 0;
                errorSpecification = StringUtils.defaultString(response.get(PayFortOnlineParam.RESPONSE_MESSAGE.getName()));
                defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, errorSpecification);
            }
            String strResponseParams = PaymentBrokerUtils.getRequestDataAsString(response);
            updateAuditTransactionByTnxRefNo(newTransacRefNo, strResponseParams, errorSpecification,
                    response.get(PayFortOnlineParam.FORT_ID.getName()),
                    response.get(PayFortOnlineParam.RESPONSE_MESSAGE.getName()), tnxResultCode);
            defaultServiceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
            defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, null);
            defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

        } else if (Boolean.FALSE.toString().equals(getRefundEnabled())) {
            log.info("[PaymentBrokerPayFortPayOnline::refund()] Refund not Enabled");
            defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
                    PaymentBrokerInternalConstants.MessageCodes.REFUND_OPERATION_NOT_SUPPORTED);
        } else {
            log.info("[PaymentBrokerPayFortPayOnline::refund()] Cannot Refund");
            defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
                    PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
        }
        log.debug("[PaymentBrokerPayFortPayOnline::refund()] End");
        return defaultServiceResponse;
    }

    @Override
    public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
                                  TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
        CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
                creditCardPayment.getPaymentBrokerRefNo());

        if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
            DefaultServiceResponse sr = new DefaultServiceResponse(true);
            sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
            sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
            return sr;
        }

        throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
    }

    @Override
    public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
                                   TnxModeEnum tnxMode) throws ModuleException {
        ServiceResponce reverseResult;

        if (!Boolean.TRUE.toString().equals(getRefundEnabled())) {

            DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
            defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
                    PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
            return defaultServiceResponse;
        }
        reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
        return reverseResult;
    }

    @Override
    public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
                                          TnxModeEnum tnxMode) throws ModuleException {
        throw new ModuleException("paymentbroker.generic.operation.notsupported");
    }

    @Override
    public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
        throw new ModuleException("paymentbroker.generic.operation.notsupported");
    }

    @Override
    public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
            throws ModuleException {
        throw new ModuleException("paymentbroker.generic.operation.notsupported");
    }

    @Override
    public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
        throw new ModuleException("paymentbroker.generic.operation.notsupported");
    }

    @Override
    public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
        // TODO Auto-generated method stub
        return null;
    }

    public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
        log.info("[PaymentBrokerPayFortV2::resolvePartialPayments()] Begin");
        Collection<Integer> oResultIF = new ArrayList<>();
        Collection<Integer> oResultIP = new ArrayList<>();
        Collection<Integer> oResultII = new ArrayList<>();
        Collection<Integer> oResultIS = new ArrayList<>();

        DefaultServiceResponse response = new DefaultServiceResponse(false);

        for (IPGQueryDTO ipgQueryDTO : (Collection<IPGQueryDTO>) colIPGQueryDTO) {

            CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
            try {
                Map<String, String> queryResponse = getQueryResponse(oCreditCardTransaction);
                // If successful payment remains at Bank
                if (isSuccessPaymentExists(queryResponse)) {
                    ipgQueryDTO.setPaymentType(getPaymentType(queryResponse));
                    ipgQueryDTO.setCreditCardNo(StringUtils.defaultString(queryResponse.get(PayFortOnlineParam.CARD_NUMBER.getName())));
                    boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);
                    if (isResConfirmationSuccess) {
                        logIfDebugEnabled("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());

                    } else {
                        if (refundFlag && Boolean.TRUE.toString().equals(getRefundEnabled())) {
                            if (isRefundSuccess(oCreditCardTransaction, ipgQueryDTO)) {
                                // Change State to 'IS'
                                oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
                                logIfDebugEnabled("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
                            } else {
                                // Change State to 'IF'
                                oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
                                logIfDebugEnabled("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
                            }
                        } else {
                            // Change State to 'IP'
                            oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
                            logIfDebugEnabled("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
                        }
                    }
                } else if (isTransactionStillOpen(queryResponse)) {
                    log.info("Transaction is still open for " + ipgQueryDTO.getPnr());
                } else {
                    log.info("No Payment Exist at Bank for pnr : " + ipgQueryDTO.getPnr());
                    oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
                    log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
                }
            } catch (ModuleException e) {
                log.info("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
            }
        }
        response.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
        response.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
        response.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
        response.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);
        response.setSuccess(true);

        log.info("[PaymentBrokerPayFortPayOnline::resolvePartialPayments()] End");
        return response;
    }

    @Override
    public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
        throw new ModuleException("paymentbroker.generic.operation.notsupported");
    }

    @Override
    public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
        throw new ModuleException("paymentbroker.generic.operation.notsupported");
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
        throw new ModuleException("paymentbroker.generic.operation.notsupported");
    }

    @Override
    public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO, IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
        throw new ModuleException("paymentbroker.generic.operation.notsupported");
    }

    @Override
    public String getPaymentGatewayName() throws ModuleException {
        return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
    }

    @Override
    public Properties getProperties() {
        Properties props = super.getProperties();
        props.put("encryptionKey", PlatformUtiltiies.nullHandler(encryptionKey));
        props.put("serviceName", PlatformUtiltiies.nullHandler(serviceName));
        props.put("itemPrefix", PlatformUtiltiies.nullHandler(itemPrefix));
        props.put("expireTimeInMinutes", PlatformUtiltiies.nullHandler(expireTimeInMinutes));
        return props;
    }

    @Override
    public boolean isEnableRefundByScheduler() {
        return true;
    }

    public String getShaRequestPhrase() {
        return shaRequestPhrase;
    }

    public void setShaRequestPhrase(String shaRequestPhrase) {
        this.shaRequestPhrase = shaRequestPhrase;
    }

    public String getShaResponsePhrase() {
        return shaResponsePhrase;
    }

    public void setShaResponsePhrase(String shaResponsePhrase) {
        this.shaResponsePhrase = shaResponsePhrase;
    }

    protected static String getSignature(Map<String, String> dataForPost, String passPhrase) {
        String signature = passPhrase;
        List<String> sortedKeys = new ArrayList<>(dataForPost.size());
        sortedKeys.addAll(dataForPost.keySet());
        Collections.sort(sortedKeys);

        for (String key : sortedKeys) {
            String value = dataForPost.get(key);
            signature = signature + key + "=" + value;
        }
        signature = signature + passPhrase;
        return Hashing.sha256()
                .hashString(signature, StandardCharsets.UTF_8)
                .toString();
    }

    protected Map<String, String> getResponseFromPayFort(Map<String, String> queryDataMap, String url) throws ModuleException {
        HttpClient httpClient = new HttpClient();
        if (isUseProxy()) {
            httpClient.getHostConfiguration().setProxy(getProxyHost(), NumberUtils.toInt(getProxyPort()));
        }
        PostMethod postMethod = new PostMethod(url);
        Map<String, String> response;
        try {
            Gson gson = new Gson();
            StringRequestEntity requestEntity = new StringRequestEntity(
                    gson.toJson(queryDataMap),
                    "application/json",
                    StandardCharsets.UTF_8.toString());
            postMethod.setRequestEntity(requestEntity);
            httpClient.executeMethod(postMethod);
            response = gson.fromJson(IOUtils.toString(postMethod.getResponseBodyAsStream()),
                    new TypeToken<Map<String, String>>() {
                    }.getType());
        } catch (IOException ioe) {
            log.error(ioe);
            throw new ModuleException(ioe, "paymentbroker.error.ioException");
        } catch (Exception e) {
            log.error(e);
            throw new ModuleException(e, "paymentbroker.communication.failure");
        }
        return response;
    }

    private Map<String, String> createQueryDataMap(CreditCardTransaction creditCardTransaction) {
        Map<String, String> queryDataMap = new HashMap<>();
        queryDataMap.put(PayFortOnlineParam.QUERY_COMMAND.getName(), "CHECK_STATUS");
        queryDataMap.put(PayFortOnlineParam.ACCESS_CODE.getName(), getMerchantAccessCode());
        queryDataMap.put(PayFortOnlineParam.MERCHANT_IDENTIFIER.getName(), getMerchantId());
        queryDataMap.put(PayFortOnlineParam.MERCHANT_REFERENCE.getName(), creditCardTransaction.getTempReferenceNum());
        queryDataMap.put(PayFortOnlineParam.LANGUAGE.getName(), getLocale());
        queryDataMap.put(PayFortOnlineParam.SIGNATURE.getName(), getSignature(queryDataMap, getShaRequestPhrase()));
        return queryDataMap;
    }

    private Map<String, String> createRefundDataMap(String merchantReference, String currencyCode, String amount) throws ModuleException {
        Map<String, String> queryDataMap = new HashMap<>();
        queryDataMap.put(PayFortOnlineParam.COMMAND.getName(), "REFUND");
        queryDataMap.put(PayFortOnlineParam.ACCESS_CODE.getName(), getMerchantAccessCode());
        queryDataMap.put(PayFortOnlineParam.MERCHANT_IDENTIFIER.getName(), getMerchantId());
        queryDataMap.put(PayFortOnlineParam.MERCHANT_REFERENCE.getName(), merchantReference);
        queryDataMap.put(PayFortOnlineParam.AMOUNT.getName(), getISOFormattedAmount(amount));
        queryDataMap.put(PayFortOnlineParam.CURRENCY.getName(), currencyCode);
        queryDataMap.put(PayFortOnlineParam.LANGUAGE.getName(), getLocale());
        queryDataMap.put(PayFortOnlineParam.SIGNATURE.getName(), getSignature(queryDataMap, getShaRequestPhrase()));
        return queryDataMap;
    }

    protected static boolean isSignatureValid(Map fields, String passPhrase) {
        String receivedSignature = (String) fields.get(PayFortOnlineParam.SIGNATURE.getName());
        Map<String, String> responseData = new HashMap<>();
        // Has to rethink about this
        for (PayFortOnlineParam param : PayFortOnlineParam.values()) {
            if (param != PayFortOnlineParam.SIGNATURE) {
                String paramValue = (String) fields.get(param.getName());
                if (paramValue != null) {
                    responseData.put(param.getName(), paramValue);
                }
            }
        }
        return StringUtils.equals(receivedSignature, getSignature(responseData, passPhrase));
    }

    protected String getISOFormattedAmount(String amount) throws ModuleException {
        return AccelAeroCalculator.multiply(AccelAeroCalculator.
                getTwoScaledBigDecimalFromString(amount), currencyMultiplier).toBigInteger().toString();
    }

    protected static void logIfDebugEnabled(String message) {
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
    }

    private boolean isTransactionStillOpen(Map<String, String> response) throws ModuleException {
        // Transaction status is empty for open transactions
        // But cannot rely on it since if payFort implement this for open transactions then functionality will be different
        return response != null && isOfflinePGW &&
                StringUtils.equals(CHECK_STATUS_SUCCESS_STATUS_CODE, response.get(PayFortOnlineParam.STATUS.getName()));
    }

    private boolean isSuccessPaymentExists(Map<String, String> response) throws ModuleException {
        return response != null && isSignatureValid(response, getShaResponsePhrase())
                && StringUtils.equals(CHECK_STATUS_SUCCESS_STATUS_CODE, response.get(PayFortOnlineParam.STATUS.getName()))
                && StringUtils.equals(SUCCESS_PURCHASE_STATUS_CODE, response.get(PayFortOnlineParam.TRANSACTION_STATUS.getName()));
    }

    private Map<String, String> getQueryResponse(CreditCardTransaction creditCardTransaction) throws ModuleException {
        Map<String, String> queryDataMap = createQueryDataMap(creditCardTransaction);
        return getResponseFromPayFort(queryDataMap, getQueryURL());
    }

    private boolean isRefundSuccess(CreditCardTransaction creditCardTransaction, IPGQueryDTO ipgQueryDTO) throws ModuleException {
        Map<String, String> queryDataMap = createRefundDataMap(creditCardTransaction.getTempReferenceNum()
                , ipgQueryDTO.getPaymentCurrencyCode(), ipgQueryDTO.getAmount().toString());
        Map<String, String> response = getResponseFromPayFort(queryDataMap, getReversalURL());
        return isValidRefund(response);
    }

    private boolean isValidRefund(Map<String, String> response) {
        return response != null && isSignatureValid(response, getShaResponsePhrase())
                && StringUtils.equals(REFUND_SUCCESS_STATUS_CODE, response.get(PayFortOnlineParam.STATUS.getName()));
    }

    protected static boolean isSuccessPurchase(Map fields) {
        return SUCCESS_PURCHASE_STATUS_CODE.equals(fields.get(PayFortOnlineParam.STATUS.getName()));
    }

    protected boolean isReservationExist(IPGQueryDTO ipgQueryDTO) {
        boolean isExist = false;
        if (ipgQueryDTO != null) {
            if (ipgQueryDTO.getPnr() != null && ipgQueryDTO.getPnr().trim().length() != 0) {
                isExist = true;
            }
        }
        return isExist;
    }

    public void setCurrencyMultiplier(int currencyMultiplier) {
        this.currencyMultiplier = currencyMultiplier;
    }

    protected static PaymentType getPaymentType(Map receiptMap) {
        PaymentType paymentType = null;
        if (!CollectionUtils.isEmpty(receiptMap)) {
            String paymentOption = (String) receiptMap.get(PayFortOnlineParam.PAYMENT_OPTION.getName());
            if (StringUtils.isNotEmpty(paymentOption)) {
                if ("MASTERCARD".equalsIgnoreCase(paymentOption)) {
                    paymentType = PaymentType.CARD_MASTER;
                } else if ("VISA".equalsIgnoreCase(paymentOption)) {
                    paymentType = PaymentType.CARD_VISA;
                } else {
                    paymentType = PaymentType.CARD_GENERIC;
                }
            }
        }
        return paymentType != null ? paymentType : PaymentType.CARD_GENERIC;
    }

    public void setIsOfflinePGW(boolean isOfflinePGW) {
        this.isOfflinePGW = isOfflinePGW;
    }
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
