/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.hsbc;

import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;

/**
 * @author Asiri
 */
public class HsbcIPGRequest {

	private IPGRequestDTO ipgRequestDTO;

	private CreditCardPayment creditCardPayment;

	private String merchantTxnRef;

	private String paymentClientPath;

	private String merchantId;

	private String locale;

	private String returnUrl;

	/**
	 * @return the ipgRequestDTO
	 */
	public IPGRequestDTO getIpgRequestDTO() {
		return ipgRequestDTO;
	}

	/**
	 * @param ipgRequestDTO
	 *            the ipgRequestDTO to set
	 */
	public void setIpgRequestDTO(IPGRequestDTO ipgRequestDTO) {
		this.ipgRequestDTO = ipgRequestDTO;
	}

	/**
	 * @return the creditCardPayment
	 */
	public CreditCardPayment getCreditCardPayment() {
		return creditCardPayment;
	}

	/**
	 * @param creditCardPayment
	 *            the creditCardPayment to set
	 */
	public void setCreditCardPayment(CreditCardPayment creditCardPayment) {
		this.creditCardPayment = creditCardPayment;
	}

	/**
	 * @return the merchantTxnRef
	 */
	public String getMerchantTxnRef() {
		return merchantTxnRef;
	}

	/**
	 * @param merchantTxnRef
	 *            the merchantTxnRef to set
	 */
	public void setMerchantTxnRef(String merchantTxnRef) {
		this.merchantTxnRef = merchantTxnRef;
	}

	/**
	 * @return the paymentClientPath
	 */
	public String getPaymentClientPath() {
		return paymentClientPath;
	}

	/**
	 * @param paymentClientPath
	 *            the paymentClientPath to set
	 */
	public void setPaymentClientPath(String paymentClientPath) {
		this.paymentClientPath = paymentClientPath;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 *            the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return the returnUrl
	 */
	public String getReturnUrl() {
		return returnUrl;
	}

	/**
	 * @param returnUrl
	 *            the returnUrl to set
	 */
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
}