package com.isa.thinair.paymentbroker.core.bl.ccAvenue;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueRequest;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueResponse;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.saman.SamanPaymentUtils;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerCCAvenueECommerceImpl extends PaymentBrokerCCAvenueTemplate {
	
	private static final String VERIFICATION_AMOUNTS_ERROR = "ccAvenue.200";
	private static Log log = LogFactory.getLog(PaymentBrokerCCAvenueECommerceImpl.class);

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerCCAvenueECommerceImpl::getRequestData()] Begin ");

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		
		
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();

		String finalAmount = CCAvenuePaymentUtils.formatAmount(ipgRequestDTO.getAmount());  // Numeric (12,2)

		CCAvenueRequest req = new CCAvenueRequest();
		req.setEncryptionKey(getEncryptionKey());
		req.setAccessCode(getAccessCode());
		req.setMerchantId(getMerchantId());
		
		postDataMap.put(CCAvenueRequest.MERCHANT_ID, getMerchantId());
		postDataMap.put(CCAvenueRequest.ORDER_ID, merchantTxnId);
		postDataMap.put(CCAvenueRequest.AMOUNT, finalAmount);
		postDataMap.put(CCAvenueRequest.REDIRECT_URL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(CCAvenueRequest.CANCEL_URL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(CCAvenueRequest.CURRENCY, ipgIdentificationParamsDTO.getPaymentCurrencyCode());
		postDataMap.put(CCAvenueRequest.LANGUAGE, getLocale());  // locale to display ccAvenue payment page
		postDataMap.put(CCAvenueRequest.BILLING_NAME, ipgRequestDTO.getContactFirstName() + " " + ipgRequestDTO.getContactLastName());  // customer name
		postDataMap.put(CCAvenueRequest.BILLING_EMAIL, ipgRequestDTO.getContactEmail());  
		postDataMap.put(CCAvenueRequest.BILLING_COUNTRY, ipgRequestDTO.getContactCountryName());
		
		postDataMap.put(CCAvenueRequest.BILLING_TEL, getMobileNumber(ipgRequestDTO)); // customer phone
		postDataMap.put(CCAvenueRequest.MERCHANT_PARAM1, "PNR : " + ipgRequestDTO.getPnr());  // pnr
		postDataMap.put(CCAvenueRequest.COMMAND , CCAvenueRequest.COMMAND_INITIATE_TRANS);
		postDataMap.put(CCAvenueRequest.ACCESS_CODE, req.getAccessCode());
		
		postDataMap.put(CCAvenueRequest.CCAVENUE_PAYMENT_OPTION, getCcAvenuePaymentOption());
		postDataMap.put(CCAvenueRequest.CCAVENUE_CARD_TYPE, getCcAvenueCardType());

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				requestDataForm + "," + sessionID, "", getPaymentGatewayName(), null, false);

		if (log.isDebugEnabled()) {
			log.debug("CCAvenue PG Request Data : " + requestDataForm + ", sessionID : " + sessionID);
		}

		postDataMap.put(CCAvenueRequest.MERCHANT_PARAM2, ccTransaction.getTransactionRefNo() + CCAvenueRequest.MERCHANT_PARAM_DELIMETER + ipgIdentificationParamsDTO.getIpgId() 
				+ CCAvenueRequest.MERCHANT_PARAM_DELIMETER + ipgIdentificationParamsDTO.getPaymentCurrencyCode());
		// the above map entry list will be use to encrypt
		postDataMap.put(CCAvenueRequest.ENC_REQUEST , CCAvenuePaymentUtils.encrypt(postDataMap, req));
		
		requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());
		
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		log.debug("[PaymentBrokerCCAvenueECommerceImpl::getRequestData()] End ");
		return ipgRequestResultsDTO;
	}

	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		log.debug("[PaymentBrokerCCAvenueECommerceImpl::getReponseData()] Begin ");

		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification = "";
		String merchantTxnReference;

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		CCAvenueResponse resp = new CCAvenueResponse();
		resp.setEncryptionKey(getEncryptionKey());
		resp.setResponse(receiptyMap);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerCCAvenueECommerceImpl::getReponseData()] Response -" + resp.toString());
		}

		if (CCAvenueResponse.RESPONSE_SUCCESS.equalsIgnoreCase(resp.getOrderStatus())) {

			// If verification ok, then status should be set as accepted
			BigDecimal paymentAmount = ReservationModuleUtils.getReservationBD().getTempPaymentAmount(
					oCreditCardTransaction.getTemporyPaymentId());

			String paymentAmountStr = paymentAmount.toString();

			PaymentQueryDR oNewQuery = getPaymentQueryDR();
			// set merchant transaction reference
			oCreditCardTransaction.setTransactionId(resp.getTracking_id());
			IPGConfigsDTO ipgConfigs = SamanPaymentUtils.getIPGConfigs(getMerchantId(), getPassword());
			ipgConfigs.setIpgURL(getIpgURL());
			ipgConfigs.setPaymentAmount(paymentAmountStr);
			ipgConfigs.setAccessCode(getAccessCode());
			ipgConfigs.setRefunding(false);
			
			ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction,ipgConfigs, QUERY_CALLER.VERIFY);
			if (serviceResponce.isSuccess()) {
				String responseAmount = (String) serviceResponce.getResponseParam(PaymentConstants.AMOUNT);
				responseAmount = responseAmount.trim();
				
				if (log.isDebugEnabled()) {
					log.debug("[PaymentBrokerCCAvenueECommerceImpl::getReponseData()] - Payment Amount-" + paymentAmount
							+ " Verified Amount-" + responseAmount);
					log.debug("[PaymentBrokerCCAvenueECommerceImpl::getReponseData()] - Formatted Payment Amount-"
							+ paymentAmountStr + " Formatted Verified Amount-" + responseAmount);
				}

				if (paymentAmountStr.equals(responseAmount)) {
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
				} else {
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorCode = VERIFICATION_AMOUNTS_ERROR;
					errorSpecification = "Payment and verifcation amounts different!!!";
					// Different amount in response
					log.error("[PaymentBrokerCCAvenueECommerceImpl::getReponseData()] - Payment and verification amounts are different");
				}

			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorCode = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE).toString();
				errorSpecification = "" + serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
			}

		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			String[] error = CCAvenuePaymentUtils.getMappedUnifiedError(resp.getStatusCode());
			errorCode = error[0];
			errorSpecification = "" + error[1];
		}

		merchantTxnReference = resp.getOrderId();

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(merchantTxnReference);
		// We don't receive card type information, hence settting it as generic
		ipgResponseDTO.setCardType(getStandardCardType(CARD_TYPE_GENERIC));
		String ccAvenueResponse = resp.toString();
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), ccAvenueResponse, errorSpecification,
				merchantTxnReference, resp.getTracking_id(), tnxResultCode);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerCCAvenueECommerceImpl::getReponseData()] - Staus-" + status + " Error Code-" + errorCode
					+ " Error Spec-" + errorSpecification);
		}

		log.debug("[PaymentBrokerCCAvenueECommerceImpl::getReponseData()] End");

		return ipgResponseDTO;
	}

	protected int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	private String getMobileNumber(IPGRequestDTO ipgRequestDTO) {
		String mobileNumber = ipgRequestDTO.getContactMobileNumber();
		String phoneNumber = ipgRequestDTO.getContactPhoneNumber();
		if (mobileNumber.equals("--") || mobileNumber.equals("-")) {
			if (phoneNumber.equals("--") || phoneNumber.equals("-")) {
				mobileNumber = "";
			} else {
				mobileNumber = phoneNumber;
			}
		}
		return mobileNumber;
	}	
	
}