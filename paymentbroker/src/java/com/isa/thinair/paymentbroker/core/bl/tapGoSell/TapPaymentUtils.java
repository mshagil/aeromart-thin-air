package com.isa.thinair.paymentbroker.core.bl.tapGoSell;

import java.security.Key;
import java.util.Formatter;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TapPaymentUtils {

	private static Log log = LogFactory.getLog(TapPaymentUtils.class);

	public static final String MerchantID = "$X_MerchantID";
	public static final String UserName = "$X_UserName";
	public static final String ReferenceID = "$X_ReferenceID";
	public static final String Mobile = "$X_Mobile";
	public static final String CurrencyCode = "$X_CurrencyCode";
	public static final String Total = "$X_Total";
	public static final String AccountId = "$x_account_id";
	public static final String TapRefference = "$x_ref";
	public static final String Result = "$x_result";
	public static final String MerchantRefference = "$&x_referenceid";

	public static final String DEFAULT_CURRENCY = "KWD";
	public static final String DEFAULT_LANGUAGE = "EN";

	public static final String PAYMENT_REFERENCE = "ref";
	public static final String PAYMENT_URL = "payment_url";
	public static final String TAP_PAYMENT_URL = "tap_payment_url";
	public static final String REDIRECTION_MECHANISM = "REDIRECTION_MECHANISM";
	
	private static final String RESPONSE_CODE_0 = "Transaction Approved";
	private static final String RESPONSE_CODE_1 = "SUCCESS";
	private static final String RESPONSE_CODE_2 = "Already Refunded";
	private static final String RESPONSE_CODE_101 = "Error with Input Parameters";
	private static final String RESPONSE_CODE_102 = "Transaction Not Available";
	private static final String RESPONSE_CODE_1000 = "Merchant ID not found";
	private static final String RESPONSE_CODE_1003 = "Product details not found";
	private static final String RESPONSE_CODE_1106 = "Invalid Hash Key / String";
	private static final String PAYMENT_FAILED = "Tap Payement Faield";
	private static final String PAYMENT_WS_FAILED = "Tap Payement web service invocation Faield";
	private static final String PENDING = "Tap Payement refund pending";
	private static final String REFUNDED = "Tap Payement already refunded";

	private static final String PAYREQ_HASHSTRINGFORMAT = "X_MerchantID$X_MerchantIDX_UserName$X_UserNameX_ReferenceID$X_ReferenceIDX_Mobile$X_MobileX_CurrencyCode$X_CurrencyCodeX_Total$X_Total";
	private static final String PAYRES_HASHSTRINGFORMAT = "x_account_id$x_account_idx_ref$x_refx_result$x_resultx_referenceid$&x_referenceid"; 

	public static String getErrorDescription(String code) {

		String errorDescription;
		switch (code) {
		case "0":
			errorDescription = RESPONSE_CODE_0;
			break;
		case "1":
			errorDescription = RESPONSE_CODE_1;
			break;
		case "2":
			errorDescription = RESPONSE_CODE_2;
			break;
		case "101":
			errorDescription = RESPONSE_CODE_101;
			break;
		case "102":
			errorDescription = RESPONSE_CODE_102;
			break;
		case "1000":
			errorDescription = RESPONSE_CODE_1000;
			break;
		case "1003":
			errorDescription = RESPONSE_CODE_1003;
			break;
		case "1106":
			errorDescription = RESPONSE_CODE_1106;
			break;
		case "FAILED":
			errorDescription = PAYMENT_FAILED;
			break;
		case "WSFAILED":
			errorDescription = PAYMENT_WS_FAILED;
			break;
		case "PENDING":
			errorDescription = PENDING;
			break;
		case "REFUNDED":
			errorDescription = REFUNDED;
			break;
		default:
			errorDescription = "Generic Error For Tap Patment Gateway";
			break;
		}

		return errorDescription;
	}

	public static String getHashStringForPayRq(String merchantId, String user, String refId, String mobileNo,
			String currencyCode, String total, String apiKey) {
		String hashInput = PAYREQ_HASHSTRINGFORMAT.replace(MerchantID, merchantId).replace(UserName, user)
				.replace(ReferenceID, refId).replace(Mobile, mobileNo).replace(CurrencyCode, currencyCode).replace(Total, total);
		return generateHAMC_SHA256(apiKey, hashInput);
	}

	public static String
			getHashStringForPayRs(String merchantId, String tapRef, String result, String merchantRef, String apiKey) {
		String hashInput = PAYRES_HASHSTRINGFORMAT.replace(AccountId, merchantId).replace(TapRefference, tapRef)
				.replace(Result, result).replace(MerchantRefference, merchantRef);
		return generateHAMC_SHA256(apiKey, hashInput);
	}
	
	public static String generateRefundRequestTxt(String referenceID, String orderID, String amount, String currencyCode,
			String userName, String merchantID) {
		StringBuffer resBuff = new StringBuffer();
		resBuff.append("Tap GoSell Refund Request Details [");
		resBuff.append(",referenceID: " + referenceID);
		resBuff.append(", orderID: " + orderID);
		resBuff.append(",amount: " + amount);
		resBuff.append(",currencyCode: " + currencyCode);
		resBuff.append(",userName: " + userName);
		resBuff.append(",merchantID: " + merchantID);
		resBuff.append(" ] ");
		return resBuff.toString();
	}


	private static String generateHAMC_SHA256(String secretKey, String text) {

		String hMac = "";
		try {
			Key sk = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
			Mac mac = Mac.getInstance(sk.getAlgorithm());
			mac.init(sk);
			final byte[] hmac = mac.doFinal(text.getBytes());
			hMac = toHexString(hmac);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return hMac;
	}

	private static String toHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);

		Formatter formatter = new Formatter(sb);
		for (byte b : bytes) {
			formatter.format("%02x", b);
		}
		return sb.toString();
	}
	
	// public static String generateHAMC_SHA256(String secret, String message) {
	// String result = "";
	// try {
	// final Charset asciiCs = Charset.forName("US-ASCII");
	// Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
	// final SecretKeySpec secret_key = new javax.crypto.spec.SecretKeySpec(asciiCs.encode(secret).array(),
	// "HmacSHA256");
	// sha256_HMAC.init(secret_key);
	// final byte[] mac_data = sha256_HMAC.doFinal(asciiCs.encode(message).array());
	//
	// for (final byte element : mac_data) {
	// result += Integer.toString((element & 0xff) + 0x100, 16).substring(1);
	// }
	// } catch (NoSuchAlgorithmException | InvalidKeyException e) {
	// log.error("[TapPaymentUtils::generateHAMC_SHA256()] - Failed to generate signature for Tap PGW");
	// }
	//
	// return result;
	// }

}
