package com.isa.thinair.paymentbroker.core.bl.eDirham;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerEDirhamQueryDR extends PaymentBrokerEDirhamTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerEDirhamQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			QUERY_CALLER operCaller)
			throws ModuleException {
		log.debug("[PaymentBrokerEDirhamQueryDR::query()] Begin");

		String errorSpecification = null;
		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();
		String queryParamString = "RefNo:" + oCreditCardTransaction.getTransactionId() + ",MerchantId:" + getMerchantId();

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), queryParamString, "", getPaymentGatewayName(), null, false);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		log.debug("Query Request Params : " + queryParamString);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(EDirhamRequest.ACTION, EDirhamPaymentUtils.EDIRHAM_OPERATION.AUTO_UPDATE.getOperation());
		postDataMap.put(EDirhamRequest.MERCHANTID, ipgConfigsDTO.getMerchantID());
		postDataMap.put(EDirhamRequest.BANKID, getBankId());
		postDataMap.put(EDirhamRequest.PUN, merchantTxnReference);
		postDataMap.put(EDirhamRequest.TRANSACTIONREQUESTDATE, EDirhamPaymentUtils.getTransactionReqTime());

		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());
		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("URL", getServerToServerReqURL());

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		try {
			String secureHash = EDirhamPaymentUtils.computeSecureHash(postDataMap, getSha1In(),
					EDirhamPaymentUtils.EDIRHAM_OPERATION.AUTO_UPDATE);
			postDataMap.put(EDirhamRequest.SECUREHASH, secureHash);
			strRequestParams = strRequestParams + "," + EDirhamRequest.SECUREHASH + "=" + secureHash;
		} catch (Exception e) {
			log.debug("Error computing SECUREHASH : " + e.toString());
			throw new ModuleException(e.getMessage());
		}

		// String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getServerToServerReqURL());

		if (log.isDebugEnabled()) {
			log.debug("E-Dirham Request Data : " + strRequestParams);
		}
		EDirhamResponse response = EDirhamPaymentUtils.getEDirhamResponse(postDataMap, getSha1In(),
				EDirhamPaymentUtils.EDIRHAM_OPERATION.AUTO_UPDATE);
		String status;

		if (response != null && response.getOriginalTransactionStatus().equals(EDirhamPaymentUtils.TRANSACTION_SUCCESSFULL)) {
			log.debug("[PaymentBrokerEDirhamQueryDR::query() : Extract Status confirmed]  ConfirmationID :"
					+ response.getConfirmationID());

			status = IPGResponseDTO.STATUS_ACCEPTED;
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.AMOUNT, response.getTransactionAmount());
			sr.addResponceParam(PaymentConstants.EDIRHAM_AMOUNT, response.geteDirhamFees());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());

		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			String error = EDirhamPaymentUtils.getErrorInfo(response);

			errorSpecification = "Status:" + response.getStatus() + " Error code:" + response.getStatus() + " Error Desc:"
					+ response.getStatusMessage();
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, error);
		}

		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), status, errorSpecification,
				oCreditCardTransaction.getTransactionId(), oCreditCardTransaction.getTransactionId(),
				Integer.parseInt(response.getStatus()));

		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getTransactionId());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, response.getOriginalTransactionStatus());

		log.debug("[PaymentBrokerEDirhamQueryDR::query() : Query Status :  " + status + "] ");
		log.debug("[PaymentBrokerEDirhamQueryDR::query()] End");

		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

}
