package com.isa.thinair.paymentbroker.core.bl.ipay88;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * The PaymentBrokerIPay88Template class to get Response Data and Charge
 * 
 * extends PaymentBrokerTemplate and implements PaymentBroker
 * 
 */
public class PaymentBrokerIPay88Template extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerIPay88Template.class);

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected String merchantCode;

	protected String merchantKey;

	protected String responseURL;

	protected String backendURL;

	protected static final String ERROR_CODE_PREFIX = "ipay88.0";

	protected static final String PAYMENT_SUCCESS = "1";

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerOgoneImpl::getReponseData()] Begin ");

		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		int cardType = 5; // default card type

		String hashValidated = null;
		String responseMismatch = "";
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		IPay88Response iPay88Resp = new IPay88Response();
		iPay88Resp.setReponse(receiptyMap);
		log.debug("[PaymentBrokerIPay88Impl::getReponseData()] Mid Response -" + iPay88Resp);

		Map<String, String> dataMapForHash = new LinkedHashMap<String, String>();
		dataMapForHash.put(IPay88Utils.MERCHANTKEY, getMerchantKey());
		dataMapForHash.put(IPay88Response.MERCHANTCODE, getMerchantCode());
		dataMapForHash.put(IPay88Response.PAYMENTID, iPay88Resp.getPaymentId());
		dataMapForHash.put(IPay88Response.REFNO, iPay88Resp.getRefNo());
		// . and , should be removed from the amount to calculate hash
		dataMapForHash.put(IPay88Response.AMOUNT, iPay88Resp.getAmount().replaceAll("[.,]", ""));
		dataMapForHash.put(IPay88Response.CURRENCY, iPay88Resp.getCurrency());
		dataMapForHash.put(IPay88Response.STATUS, iPay88Resp.getStatus());

		if (iPay88Resp != null && !(iPay88Resp.getStatus().trim().equals(""))) {

			// Validate the Hash if required
			if (iPay88Resp.validateSHA1Hash(dataMapForHash)) {
				// Secure Hash validation succeeded, add a data field to be
				// displayed later.
				hashValidated = IPay88Response.VALID_HASH;

				// Check for correct response
				errorExists = true;

				// Check if response is not equal
				if (oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(iPay88Resp.getRefNo())) {
					errorExists = false;
				} else {
					responseMismatch = IPay88Response.INVALID_RESPONSE;
				}
			} else {
				// Secure Hash validation failed, add a data field to be
				// displayed later.
				errorExists = true;
				hashValidated = IPay88Response.INVALID_HASH;
			}
		} else {
			// Secure Hash was not validated,
			hashValidated = IPay88Response.NO_VALUE;
			errorCode = ERROR_CODE_PREFIX;
		}

		if (PAYMENT_SUCCESS.equals(iPay88Resp.getStatus()) && !errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "" + responseMismatch;
		} else {
			String responseStatus = iPay88Resp.getStatus();
			log.debug("We received an unknown status for the transaction. Order id : " + iPay88Resp.getPaymentId()
					+ " :response status: " + responseStatus + " :error desc: " + iPay88Resp.getErrDesc());

			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = status + " " + iPay88Resp.getErrDesc();
			errorSpecification = status + " " + iPay88Resp.getErrDesc();
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(oCreditCardTransaction.getTransactionId());
		ipgResponseDTO.setAuthorizationCode(iPay88Resp.getAuthCode());
		ipgResponseDTO.setCardType(cardType);

		String strIPay88Response = PaymentBrokerUtils.getRequestDataAsString(receiptyMap);

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), strIPay88Response, errorSpecification,
				iPay88Resp.getAuthCode(), iPay88Resp.getTransId(), tnxResultCode);

		log.debug("[PaymentBrokerIPAY88Impl::getReponseData()] End -" + iPay88Resp.getTransId() + "");

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * @return the bundle
	 */
	public static ResourceBundle getBundle() {
		return bundle;
	}

	/**
	 * @param bundle
	 *            the bundle to set
	 */
	public static void setBundle(ResourceBundle bundle) {
		PaymentBrokerIPay88Template.bundle = bundle;
	}

	/**
	 * @return the merchantKey
	 */
	public String getMerchantKey() {
		return merchantKey;
	}

	/**
	 * @param merchantKey
	 *            the merchantKey to set
	 */
	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}

	/**
	 * @return the responseURL
	 */
	public String getResponseURL() {
		return responseURL;
	}

	/**
	 * @param responseURL
	 *            the responseURL to set
	 */
	public void setResponseURL(String responseURL) {
		this.responseURL = responseURL;
	}

	/**
	 * @return the backendURL
	 */
	public String getBackendURL() {
		return backendURL;
	}

	/**
	 * @param backendURL
	 *            the backendURL to set
	 */
	public void setBackendURL(String backendURL) {
		this.backendURL = backendURL;
	}

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
