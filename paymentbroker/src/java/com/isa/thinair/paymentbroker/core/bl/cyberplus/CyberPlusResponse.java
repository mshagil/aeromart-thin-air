package com.isa.thinair.paymentbroker.core.bl.cyberplus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CyberPlusResponse {

	private static Log log = LogFactory.getLog(CyberPlusResponse.class);

	public static final String NO_VALUE = "";

	public static final String VALID_SIGNATURE = "VALID SIGNATUER.";

	public static final String INVALID_SIGNATURE = "INVALID SIGNATURE.";

	public static final String INVALID_RESPONSE = "INVALID RESPONSE";

	private String actionMode;
	private String amount;
	private String authResult;
	private String cardBrand;
	private String cardNumber;
	private String ctxMode;
	private String currency;
	private String paymentConfig;
	private String signature;
	private String siteId;
	private String transDate;
	private String transId;
	private String result;
	private String version;
	private String threedsStatus;
	private String userInfo;
	private String authNumber;
	private String paymentCertificate;

	// Response as a Map
	Map response = null;

	public void setReponse(Map reponse) {
		this.response = reponse;
		result = null2unknown((String) reponse.get("vads_result"));
		signature = null2unknown((String) reponse.get("signature"));
		authResult = null2unknown((String) reponse.get("vads_auth_result"));
		cardBrand = null2unknown((String) reponse.get("vads_card_brand"));
		threedsStatus = null2unknown((String) reponse.get("vads_threeds_status"));
		cardNumber = null2unknown((String) reponse.get("vads_card_number"));
		siteId = null2unknown((String) reponse.get("vads_site_id"));
		transId = null2unknown((String) reponse.get("vads_trans_id"));
		version = null2unknown((String) reponse.get("vads_version"));
		amount = null2unknown((String) reponse.get("vads_amount"));
		actionMode = null2unknown((String) reponse.get("vads_action_mode"));
		currency = null2unknown((String) reponse.get("vads_currency"));
		ctxMode = null2unknown((String) reponse.get("vads_ctx_mode"));
		paymentConfig = null2unknown((String) reponse.get("vads_payment_config"));
		transDate = null2unknown((String) reponse.get("vads_trans_date"));
		userInfo = null2unknown((String) reponse.get("vads_user_info"));
		authNumber = null2unknown((String) reponse.get("vads_auth_number"));
		paymentCertificate = null2unknown((String) reponse.get("vads_payment_certificate"));
	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();

		// create a list
		List fieldNames = new ArrayList(response.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) response.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				resBuff.append('&');
			}
		}
		if (resBuff.toString().length() < 2500) {
			return resBuff.toString();
		} else {
			// Response is too large to save in db field.
			log.debug("[CyberPlusResponse::toString()] Response before trimming -" + resBuff.toString());
			return resBuff.toString().substring(0, 2500);
		}
	}

	/*
	 * Validate Response signature
	 * 
	 * @param certificate
	 * 
	 * @return
	 */
	public boolean validateSignature(String certificate) {
		String createdSignature = CyberPlusPaymentUtils.getSignature(response, certificate);
		if (signature.equalsIgnoreCase(createdSignature))
			return true;
		else
			return false;
	}

	public String getActionMode() {
		return actionMode;
	}

	public void setActionMode(String actionMode) {
		this.actionMode = actionMode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAuthResult() {
		return authResult;
	}

	public void setAuthResult(String authResult) {
		this.authResult = authResult;
	}

	public String getCardBrand() {
		return cardBrand;
	}

	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCtxMode() {
		return ctxMode;
	}

	public void setCtxMode(String ctxMode) {
		this.ctxMode = ctxMode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentConfig() {
		return paymentConfig;
	}

	public void setPaymentConfig(String paymentConfig) {
		this.paymentConfig = paymentConfig;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getTransDate() {
		return transDate;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getThreedsStatus() {
		return threedsStatus;
	}

	public void setThreedsStatus(String threedsStatus) {
		this.threedsStatus = threedsStatus;
	}

	public Map getResponse() {
		return response;
	}

	public void setResponse(Map response) {
		this.response = response;
	}

	public String getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(String userInfo) {
		this.userInfo = userInfo;
	}

	public String getAuthNumber() {
		return authNumber;
	}

	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}

	public String getPaymentCertificate() {
		return paymentCertificate;
	}

	public void setPaymentCertificate(String paymentCertificate) {
		this.paymentCertificate = paymentCertificate;
	}
}
