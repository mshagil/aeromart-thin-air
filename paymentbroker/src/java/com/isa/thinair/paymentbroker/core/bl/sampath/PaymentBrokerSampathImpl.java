/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.paymentbroker.core.bl.sampath;

import java.security.Provider;
import java.security.Security;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.ISACreditCardValidator;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.NumberUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Sudheera
 * 
 * @since 2.0
 */
public class PaymentBrokerSampathImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static Log log = LogFactory.getLog(PaymentBrokerSampathImpl.class);

	public static final String PARAM_ENCRYPTED_RECEIPT_PAY = "encryptedReceiptPay";

	private static final String TRANSACTION_STATUS_ACCEPTED = "ACCEPTED";

	private static final String TRANSACTION_STATUS_REJECTED = "REJECTED";

	private static final int NO_DEC_PLACES = 2;

	private static final String DEFAULT_TRANSACTION_CODE = "5"; // Auth
																// declined

	/**
	 * Sampath Payment Gatewat sets Security.insertProviderAt(new IBMJCE(), 2); in CShroff2 class in ipgclient2.jar
	 * 
	 * This overrides AccelAero SunJCE which is used for Encryption Module Nilindra 12:19 PM 7/5/2007
	 */
	public PaymentBrokerSampathImpl() {

		Provider[] providers = Security.getProviders();
		int intSunJCEIndex = 1;
		Provider jceProvider = null;
		int i = 0;

		for (Provider provider : providers) {
			i++;
			if (provider.getName().equals("SunJCE")) {
				jceProvider = provider;
				break;
			}
		}

		if (jceProvider != null && i != intSunJCEIndex) {
			Security.removeProvider(jceProvider.getName());
			Security.insertProviderAt(jceProvider, intSunJCEIndex);
		}
	}

	/**
	 * Prepare the POST [REQUEST] Message
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		GlobalConfig globalConfig = PaymentBrokerModuleUtil.getGlobalConfig();
		SampathIPG sampathIPG = new SampathIPG(proxyHost, proxyPort, secureKeyLocation, logPath, ipgURL);
		SampathIPGRequest ipgRequest = new SampathIPGRequest();

		String returnUrl = ipgRequestDTO.getReturnUrl();
		String pnr = ipgRequestDTO.getPnr();
		AppIndicatorEnum appIndicatorEnum = ipgRequestDTO.getApplicationIndicator();

		String amount = (String) ipgRequestDTO.getAmount();
		String cardType = (String) ipgRequestDTO.getCardType();
		String cardNo = (String) ipgRequestDTO.getCardNo();
		String secCode = (String) ipgRequestDTO.getSecureCode();
		String holderName = (String) ipgRequestDTO.getHolderName();
		String strExpiry = (String) ipgRequestDTO.getExpiryDate(); // YYMM

		ipgRequest.setAction(SampathIPGRequest.ACTION_CHARGE);
		ipgRequest.setCardHolderName(holderName);
		ipgRequest.setCardNumber(cardNo);
		ipgRequest.setCurrencyCode(globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
		ipgRequest.setCvcNumber(secCode);

		// Expiry Date - yymm
		ipgRequest.setExpiryMonth(strExpiry.substring(2, 4));
		ipgRequest.setExpiryYear("20" + strExpiry.substring(0, 2));

		String merchantTnxId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		ipgRequest.setMerchantId(merchantId);
		ipgRequest.setMerchantTransactionId(merchantTnxId);
		ipgRequest.setProductType(this.getPaymentSampathType(cardType));
		ipgRequest.setReturnURL(returnUrl);
		ipgRequest.setTransactionAmount(NumberUtil.formatAsDecimal(amount, NO_DEC_PLACES));

		// additional info
		ipgRequest.setMerchantVariable1(pnr);
		ipgRequest.setMerchantVariable2(String.valueOf(appIndicatorEnum.getCode()));
		String[] plainTextRequest = ipgRequest.getXML(true);

		log.info("plainTextInvoice:" + plainTextRequest[1]);
		String encryptedRequest = sampathIPG.getEncryptedRequest(plainTextRequest[0]);
		log.info("encryptedRequest:" + encryptedRequest);

		StringBuffer sb = new StringBuffer();
		sb.append("<form id=frm1 method=POST action='" + ipgURL + "'>");
		sb.append("<input type=hidden name=encryptedInvoicePay value='" + encryptedRequest + "'>");
		// sb.append("<input type=hidden name=cache value='true'>");
		sb.append("<input type=hidden name=redirectMsg value='Payment is being directed to Sampath Payment Gateway now.......'>");
		sb.append("</form>");

		String serviceType = bundle.getString("SERVICETYPE_AUTHORIZE");
		CreditCardTransaction ccTransaction = auditTransaction(pnr, merchantId, merchantTnxId,
				ipgRequestDTO.getApplicationTransactionId(), serviceType, plainTextRequest[1], "", getPaymentGatewayName(), null,
				false);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(sb.toString());
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());

		return ipgRequestResultsDTO;
	}

	/**
	 * Returns the Sampath Payment Type
	 * 
	 * @param integer
	 * @return
	 */
	private String getPaymentSampathType(String cardType) {
		Map<String, String> mapCardName = new HashMap<String, String>();

		mapCardName.put("1", "MASTERCARD");
		mapCardName.put("2", "VISA");
		mapCardName.put("3", "AMEX");
		mapCardName.put("4", "DINERS");

		return mapCardName.get(cardType);
	}

	/**
	 * Decompose the Response Data
	 */
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO oipgResponseDTO) throws ModuleException {
		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
		String encryptedReceiptPay = (String) receiptyMap.get(PARAM_ENCRYPTED_RECEIPT_PAY);

		SampathIPG sampathIPG = new SampathIPG(proxyHost, proxyPort, secureKeyLocation, logPath, ipgURL);
		SampathIPGResponse response = new SampathIPGResponse();
		String plainTextReceipt = sampathIPG.getDecryptedResponse(encryptedReceiptPay);

		response.setXml(plainTextReceipt);

		String status = response.getTransactionStatus();
		String message = response.getFailReason();
		String ipgTxnId = response.getIpgTransactionId();
		String appTxnId = response.getMerchantTransactionId();
		String authorizationCode = response.getAuthorizationCode();
		String errorCode = null;
		String errorSpecification;
		int tnxResultCode;

		if (TRANSACTION_STATUS_ACCEPTED.equals(status)) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "";
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = DEFAULT_TRANSACTION_CODE;
			errorSpecification = status + " " + errorCode + " " + message;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(message);
		ipgResponseDTO.setApplicationTransactionId(appTxnId);
		ipgResponseDTO.setAuthorizationCode(authorizationCode);

		updateAuditTransactionByTnxRefNo(oipgResponseDTO.getPaymentBrokerRefNo(), plainTextReceipt, errorSpecification,
				authorizationCode, null, tnxResultCode);

		return ipgResponseDTO;
	}

	/**
	 * Carry out the Charge operation
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		checkCreditCardParameters(creditCardPayment);

		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	/**
	 * Carry out the Refund operation
	 */
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		checkCreditCardParameters(creditCardPayment);

		if (creditCardPayment.getPreviousCCPayment() != null) {
			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return serviceResponse;
			}
		}

		String amount = PlatformUtiltiies.nullHandler(creditCardPayment.getAmount());
		String merchantTnxId = composeMerchantTransactionId(creditCardPayment.getAppIndicator(),
				creditCardPayment.getTemporyPaymentId(), PaymentConstants.MODE_OF_SERVICE.REFUND);

		CreditCardTransaction creditCardTransaction = auditTransaction(creditCardPayment.getPnr(), merchantId, merchantTnxId,
				creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_MANUAL_REFUND"),
				"Manual Refund requested for " + amount, "Manual Refund responded for " + amount, getPaymentGatewayName(), null,
				true);

		DefaultServiceResponse sr = new DefaultServiceResponse(true);
		sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());

		return sr;
	}

	/**
	 * Carry out the reverse operation
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		checkCreditCardParameters(creditCardPayment);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.REVERSE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	/**
	 * Check Credit card parameters
	 * 
	 * @param ccPayment
	 * @throws CommonsDataAccessException
	 */
	private static void checkCreditCardParameters(CreditCardPayment ccPayment) throws ModuleException {

		// If ccPayment is null or the necessary inputs are null throw the exception
		if (ccPayment == null || ccPayment.getCardholderName() == null || ccPayment.getCardNumber() == null
				|| ccPayment.getExpiryDate() == null || ccPayment.getCurrency() == null || ccPayment.getAmount() == null) {
			throw new ModuleException("paymentbroker.invalid.parameters");
		}

		ISACreditCardValidator cardValidator = new ISACreditCardValidator();

		if (!cardValidator.isValidExpiryDate(ccPayment.getExpiryDate())) {
			throw new ModuleException("paymentbroker.invalid.expdate");
		}

		if (!cardValidator.isValidCVV(ccPayment.getCvvField())) {
			throw new ModuleException("paymentbroker.invalid.cvv");
		}
	}

	/**
	 * Returns Payment Gateway Name
	 * 
	 * @throws ModuleException
	 */
	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(ipgIdentificationParamsDTO);
		return ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * Resolves partial payments [ Invokes via a scheduler operation ]
	 */
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "paymentbroker.generic.operation.notsupported");
		return sr;
	}

	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_STATUS_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO - Implement card configuration data
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}