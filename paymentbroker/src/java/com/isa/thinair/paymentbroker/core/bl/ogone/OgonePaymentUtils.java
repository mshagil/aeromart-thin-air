package com.isa.thinair.paymentbroker.core.bl.ogone;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import sun.misc.BASE64Decoder;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelSegmentDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.webplatform.api.dto.XMLResponse;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;

public class OgonePaymentUtils {

	private static Log log = LogFactory.getLog(OgonePaymentUtils.class);

	private static final String ERROR_CODE_PREFIX = "ogone.";
	
	private static String DEFAULT_TEMPLATE_LANGUAGE = "en";

	public static enum OGONE_OPERATION {
		RES, DES, RFD
	};

	public static enum ECI {
		MOTO(1), ECOMMERCE(7);

		int eciValue;

		ECI(int eciValue) {
			this.eciValue = eciValue;
		}

		public int getECIValue() {
			return eciValue;
		}
	};

	public static XMLResponse getOgoneXMLResponse(Map<String, String> postDataMap) {

		HostConfiguration hcon = new HostConfiguration();
		if (postDataMap.get("USE_PROXY").equals("Y")) {
			hcon.setProxy(postDataMap.get("PROXY_HOST"), Integer.valueOf(postDataMap.get("PROXY_PORT")));
		}

		HttpClient client = new HttpClient();
		client.setHostConfiguration(hcon);

		String xmlResponseString = null;

		PostMethod method = new PostMethod(postDataMap.get("URL"));

		for (String key : postDataMap.keySet()) {

			if (log.isDebugEnabled() && key.equalsIgnoreCase("PROXY_HOST") || key.equalsIgnoreCase("PROXY_PORT")
					|| key.equalsIgnoreCase("URL") || key.equalsIgnoreCase("USE_PROXY")) {
				log.debug("OGONE Proxy Details : " + key + " : " + postDataMap.get(key));
			}

			if (!key.equals("PROXY_HOST") && !key.equals("PROXY_PORT") && !key.equals("URL") && !key.equals("USE_PROXY")) {
				method.addParameter(key, postDataMap.get(key));
				if (log.isDebugEnabled()) {
					if (!key.equalsIgnoreCase(OgoneRequest.CARDNO) && !key.equalsIgnoreCase(OgoneRequest.CVC)
							&& !key.equalsIgnoreCase(OgoneRequest.ED) && !key.equalsIgnoreCase(OgoneRequest.PSWD)) {
						log.debug("OGONE Request : " + key + " : " + postDataMap.get(key));
					}
				}
			}
		}

		try {
			log.debug("Executing the OGONE Request for URL : " + postDataMap.get("URL"));
			int returnCode = client.executeMethod(method);

			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
				log.debug("The Post method is not implemented by this URI");
				return null;
			} else {
				xmlResponseString = method.getResponseBodyAsString();
				log.debug("XML Response : " + xmlResponseString);

				return parseXML(xmlResponseString);
			}
		} catch (Exception e) {
			log.debug("Exception while parsing the XML response " + e.getMessage(), e);
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	public static XMLResponse parseXML(String xmlResponseString) {
		XMLResponse xmlResponse = new XMLResponse();
		DOMParser parser = new DOMParser();
		try {
			log.debug("OGONE, Start parsing XML string...");
			parser.parse(new InputSource(new java.io.StringReader(xmlResponseString)));
			log.debug("OGONE, Parsed XML succesfully");
		} catch (Exception e) {
			log.debug("Error when parsing XML response string : " + e.getMessage());
		}
		Document doc = parser.getDocument();

		// get the root <ncresponse>
		Element docElement = doc.getDocumentElement();

		if (docElement != null) {
			// get the XMLResponse object
			xmlResponse = getXMLResponse(docElement);
		}

		return xmlResponse;
	}

	/**
	 * Take an ncresponse element and read the values in, create an XMLResponse object and return it
	 * 
	 * @param ncresponse
	 * @return
	 */
	private static XMLResponse getXMLResponse(Element ncresponse) {
		XMLResponse xmlResponse = new XMLResponse();

		log.debug("OGONE, Creating XMLresponse...");
		xmlResponse.setOrderId(ncresponse.getAttribute(OgoneResponse.ORDERID));
		xmlResponse.setPayId(ncresponse.getAttribute(OgoneResponse.PAYID));
		xmlResponse.setNcStatus(ncresponse.getAttribute(OgoneResponse.NCSTATUS));
		xmlResponse.setNcError(ncresponse.getAttribute(OgoneResponse.NCERROR));
		xmlResponse.setNcErrorPlus(ncresponse.getAttribute(OgoneResponse.NCERRORPLUS));
		xmlResponse.setAcceptance(ncresponse.getAttribute(OgoneResponse.ACCEPTANCE));
		xmlResponse.setStatus(ncresponse.getAttribute(OgoneResponse.STATUS));
		xmlResponse.setEci(ncresponse.getAttribute(OgoneResponse.ECI));
		xmlResponse.setAmount(ncresponse.getAttribute(OgoneResponse.AMOUNT));
		xmlResponse.setCurrency(ncresponse.getAttribute(OgoneResponse.CURRENCY));
		xmlResponse.setPaymentMethod(ncresponse.getAttribute(OgoneResponse.PAYMENT_METHOED));
		xmlResponse.setBrand(ncresponse.getAttribute(OgoneResponse.BRAND));
		xmlResponse.setAlias(ncresponse.getAttribute(OgoneResponse.ALIAS));

		log.debug("OGONE, Creating HTML_ANSWER if exist...");
		// get child nodelist of <ncresponse> element
		NodeList nl = ncresponse.getElementsByTagName("HTML_ANSWER");
		String htmlFor3DS = null;
		if (nl != null && nl.getLength() > 0) {
			log.debug("OGONE, Creating HTML_ANSWER...");
			Element el = (Element) nl.item(0);
			htmlFor3DS = el.getFirstChild().getNodeValue();
			byte[] decodedBytes = null;

			try {
				BASE64Decoder decoder = new BASE64Decoder();
				decodedBytes = decoder.decodeBuffer(htmlFor3DS);
			} catch (Exception e) {
				log.debug("Exception while parsing the XML response" + e.getMessage());
			}

			if (decodedBytes != null) {
				xmlResponse.setHtml(new String(decodedBytes));
			}
		}

		log.debug("OGONE, XMLresponse created succesfully...");
		return xmlResponse;
	}

	public static String computeSHA1(Map<String, String> postDataMap, String Passphrase) throws Exception {

		StringBuilder stringToHash = new StringBuilder();
		Map<String, String> dataMap = new HashMap<String, String>();

		List<String> keyList = new ArrayList<String>();
		for (String key : postDataMap.keySet()) {
			dataMap.put(key.toUpperCase(), postDataMap.get(key));
			keyList.add(key.toUpperCase());
		}

		Collections.sort(keyList);

		for (String key : keyList) {
			if (!key.equals("SHASIGN") && !key.equals("IPG_SESSION_ID")) {
				stringToHash.append(key).append("=").append(dataMap.get(key)).append(Passphrase);
			}
		}

		return computeSHA1(stringToHash.toString()).toUpperCase();
	}

	private static String convertToHex(byte[] data) {

		StringBuffer buf = new StringBuffer();

		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();

	}

	public static String computeSHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {

		MessageDigest md;
		md = MessageDigest.getInstance("SHA-1");
		byte[] sha1hash = new byte[40];
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		sha1hash = md.digest();
		return convertToHex(sha1hash);

	}

	/**
	 * This method filter the application specific error code on specified priority
	 * 
	 * @param migsResp
	 * @return
	 */
	public static String getFilteredErrorCode(String status, String ncError) {

		String errorCode = "";

		// #1. Check for response status
		errorCode = getResponseErrorDescription(ncError);

		// #2. Check for response ErrorCode
		if (errorCode.equals("")) {
			errorCode = getMatchingResponseCodeFromStatus(status);
			if (errorCode.equals("")) {
				if (log.isDebugEnabled()) {
					log.debug("===================================================================================================");
					log.debug("= [OgonePaymentUtils::getFilteredErrorCode]Issuer Error Code found : " + ncError);
					log.debug("===================================================================================================");
				}
				errorCode = ERROR_CODE_PREFIX + "100";
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("[OgonePaymentUtils::getFilteredErrorCode]Return Code :" + errorCode);
		}
		return errorCode;
	}

	/**
	 * Returns the application specific error code for the Issuer response
	 * 
	 * @param responseStatus
	 *            the Issuer response code
	 * @return the matching error code
	 */
	private static String getMatchingResponseCodeFromStatus(String responseStatus) {
		String result = "";

		// check if a response code code is null or empty
		if (responseStatus != null && !responseStatus.equals("") && !responseStatus.isEmpty()) {
			if (log.isDebugEnabled()) {
				log.debug("[OgonePaymentUtils::getMatchingResponseCodeFromStatus]Issuer Error Code found :" + responseStatus);
			}

			int input = 0;
			try {
				input = Integer.parseInt(responseStatus);
				switch (input) {
				case 0:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 1:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 2:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 4:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 40:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 41:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 50:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 51:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 52:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 55:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 56:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 57:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 59:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 6:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 61:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 62:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 63:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 64:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 7:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 71:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 72:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 73:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 74:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 75:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 8:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 81:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 82:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 83:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 84:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 85:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 9:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 91:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 92:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 93:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 94:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 95:
					result = ERROR_CODE_PREFIX + "default";
					break;
				case 99:
					result = ERROR_CODE_PREFIX + "default";
					break;
				default:
					result = "";
				}
			} catch (NumberFormatException e) {
				// If return code is not an Integer return ""
				log.error(e);
				result = "";
			}

			return result;

		} else {
			return "";
		}
	}

	/***
	 * In general, error codes starting with 2: uncertain status. Will evolve to a final status. 3: transaction declined
	 * by the acquirer 4: transaction declined. It could be only a temporary technical problem. Please retry a little
	 * bit later. 5: validation/configuration error (f.i. : currency not allowed on your account,...).
	 * 
	 * @param ncError
	 * @return
	 */
	private static String getResponseErrorDescription(String ncError) {
		String result = "";

		if (ncError != null) {
			if (ncError.trim().equals("30051001")) {
				result = ERROR_CODE_PREFIX + "30051001";
			} else if (ncError.trim().equals("30511001")) {
				result = ERROR_CODE_PREFIX + "30511001";
			} else if (ncError.trim().equals("40001134")) {
				result = ERROR_CODE_PREFIX + "40001134";
			}
			if (log.isDebugEnabled()) {
				log.debug("===================================================================================================");
				log.debug("= [OgonePaymentUtils::getResponseErrorDescription]Issuer Error Code found : " + ncError
						+ " Returned common error code : " + result);
				log.debug("===================================================================================================");
			}
		}

		return result;
	}

	/**
	 * This function uses the returned status code retrieved from the Digital Response and returns an appropriate
	 * description for the status code
	 * 
	 * @param statusCode
	 *            String containing the status
	 * @return description String containing the appropriate description
	 */
	public static String getResponseStatusDescription(String statusCode, String ncError) {

		String result = "";

		try {
			int input = Integer.parseInt(statusCode);

			switch (input) {
			case 0: {
				result = "Card payment failed. Check card details.";
				if (ncError != null && ncError.equals("50001129")) {
					result = "Refund refused. Partial refunds are allowed after one day.";
				}
				break;
			}
			case 1:
				result = "Card payment cancelled.";
				break;
			case 2:
				result = "Authorization Declined.";
				break;
			case 4:
				result = "Order stored.";
				break;
			case 40:
				result = "Stored waiting external result.";
				break;
			case 41:
				result = "Waiting client payment.";
				break;
			case 5:
				result = "Authorized.";
				break;
			case 50:
				result = "Transaction has not been completed successfully. For further information, kindly consult the bank.";
				break;
			case 51:
				result = "Transaction has not been completed successfully. For further information, kindly consult the bank.";
				break;
			case 52:
				result = "Transaction has not been completed successfully. For further information, kindly consult the bank.";
				break;
			case 55:
				result = "Transaction has not been completed successfully. For further information, kindly consult the bank.";
				break;
			case 56:
				result = "OK with scheduled payments.";
				break;
			case 57:
				result = "Error in scheduled payments.";
				break;
			case 59:
				result = "Transaction has not been completed successfully. For further information, kindly consult the bank.";
				break;
			case 6:
				result = "Authorized and cancelled.";
				break;
			case 61:
				result = "Payment request deletion waiting.";
				break;
			case 62:
				result = "Payment request deletion uncertain.";
				break;
			case 63:
				result = "Payment request deletion refused.";
				break;
			case 64:
				result = "Authorized and cancelled.";
				break;
			case 7:
				result = "Payment deleted successfully.";
				break;
			case 71:
				result = "Payment deletion pending.";
				break;
			case 72:
				result = "Payment deletion uncertain.";
				break;
			case 73:
				result = "Payment deletion refused.";
				break;
			case 74:
				result = "Payment deleted successfully.";
				break;
			case 75:
				result = "Deletion processed by merchant.";
				break;
			case 8:
				result = "Refund successful.";
				break;
			case 81:
				result = "Refund pending.";
				break;
			case 82:
				result = "Refund uncertain.";
				break;
			case 83:
				result = "Refund refused.";
				break;
			case 84:
				result = "Payment declined by the acquirer.";
				break;
			case 85:
				result = "Refund processed by merchant.";
				break;
			case 9:
				result = "Payment requested.";
				break;
			case 91:
				result = "Payment processing.";
				break;
			case 92:
				result = "Payment uncertain.";
				break;
			case 93:
				result = "Payment refused.";
				break;
			case 94:
				result = "Refund declined by the acquirer.";
				break;
			case 95:
				result = "Payment processed by merchant.";
				break;
			case 99:
				result = "Being processed.";
				break;
			default:
				result = "Authorization Declined.";
			}
		} catch (NumberFormatException e) {
			// If return code is not an Integer return ""
			log.error(e);
			result = "Unable to be determined.";
		}
		return result;
	}

	public static IPGConfigsDTO getIPGConfigs(String merchantID, String userName, String password) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);
		oIPGConfigsDTO.setUsername(userName);
		oIPGConfigsDTO.setPassword(password);

		return oIPGConfigsDTO;
	}

	public static XMLResponseDTO assembleXMLResponseDTO(XMLResponse xmlResponse) throws ModuleException {

		if (xmlResponse == null) {
			if (log.isErrorEnabled()) {
				log.error("### Payment response is null. Please check payment gateway communication(configuration/proxy) setting ###");
			}
			throw new ModuleException("paymentbroker.gateway.not.respond");
		}
		XMLResponseDTO xmlResponseDTO = new XMLResponseDTO();

		xmlResponseDTO.setAcceptance(xmlResponse.getAcceptance());
		xmlResponseDTO.setAmount(xmlResponse.getAmount());
		xmlResponseDTO.setBrand(xmlResponse.getBrand());
		xmlResponseDTO.setCurrency(xmlResponse.getCurrency());
		xmlResponseDTO.setEci(xmlResponse.getEci());
		xmlResponseDTO.setHtml(xmlResponse.getHtml());
		xmlResponseDTO.setNcError(xmlResponse.getNcError());
		xmlResponseDTO.setNcErrorPlus(xmlResponse.getNcErrorPlus());
		xmlResponseDTO.setNcStatus(xmlResponse.getNcStatus());
		xmlResponseDTO.setOrderId(xmlResponse.getOrderId());
		xmlResponseDTO.setPayId(xmlResponse.getPayId());
		xmlResponseDTO.setPayIdSub(xmlResponse.getPayIdSub());
		xmlResponseDTO.setPaymentMethod(xmlResponse.getPaymentMethod());
		xmlResponseDTO.setStatus(xmlResponse.getStatus());
		xmlResponseDTO.setAlias(xmlResponse.getAlias());

		return xmlResponseDTO;
	}

	/**
	 * Add Fraud handling data
	 * 
	 * @param postDataMap
	 * @param ipgRequestDTO
	 */
	public static void addFraudCheckingData(Map<String, String> postDataMap, IPGRequestDTO ipgRequestDTO) {
		String phoneNo = ipgRequestDTO.getContactMobileNumber();
		if (phoneNo == null || phoneNo.trim().isEmpty()) {
			phoneNo = ipgRequestDTO.getContactPhoneNumber();
		}
		if (phoneNo != null)
			phoneNo = StringUtil.extractNumberFromString(phoneNo);
		String email = ipgRequestDTO.getEmail();
		String countryCode = ipgRequestDTO.getContactCountryCode();
		String customerName = BeanUtils.nullHandler(ipgRequestDTO.getContactFirstName()) + " " + BeanUtils.nullHandler(ipgRequestDTO.getContactLastName());

		postDataMap.put(OgoneRequest.DATATYPE, OgoneRequest.DATATYPE_TRAVEL);
		postDataMap.put(OgoneRequest.TICKETNO, StringUtil.getSubString(ipgRequestDTO.getPnr(), 16));
		postDataMap
				.put(OgoneRequest.TICKETDATE, CalendarUtil.formatDate(Calendar.getInstance().getTime(), CalendarUtil.PATTERN3));
		if (email != null && !email.trim().isEmpty())
			postDataMap.put(OgoneRequest.CUSTOMER_EMAIL, email);
		if (countryCode != null && !countryCode.trim().isEmpty())
			postDataMap.put(OgoneRequest.CUSTOMER_COUNTRY, countryCode);
		if (phoneNo != null && !phoneNo.trim().isEmpty())
			postDataMap.put(OgoneRequest.CUSTOMER_TELEPHONENO, phoneNo);
		
		if (!customerName.trim().isEmpty())
			postDataMap.put(OgoneRequest.PASSENGER_NAME, customerName);
		
		postDataMap.put(OgoneRequest.AIRLINE_NAME, AppSysParamsUtil.getDefaultCarrierName());

		if (ipgRequestDTO.getTravelDTO() != null) {
			Collection<TravelSegmentDTO> segments = ipgRequestDTO.getTravelDTO().getTravelSegments();

			//Active cabinClassCodes
			Map<String, String> activeCabinClassMap = CommonsServices.getGlobalConfig().getActiveCabinClassesMap();
			
			if (segments != null) {
				int flightID = 1;
				for (TravelSegmentDTO segmentDTO : segments) {
					postDataMap.put(OgoneRequest.FLIGHTNO + flightID, StringUtil.getSubString(segmentDTO.getFlightNumber(), 4));
					postDataMap.put(OgoneRequest.FLIGHT_DATE + flightID,
							CalendarUtil.formatDate(segmentDTO.getFlightDate(), CalendarUtil.PATTERN4));
					postDataMap
							.put(OgoneRequest.CARRIER_CODE + flightID, StringUtil.getSubString(segmentDTO.getCarrierCode(), 4));
					postDataMap.put(OgoneRequest.DEPARTURE_AIRPORT + flightID,
							StringUtil.getSubString(segmentDTO.getDepartureAirportCode(), 5));
					postDataMap.put(OgoneRequest.DEPARTURE_AIRPORT_NAME + flightID,
							StringUtil.getSubString(segmentDTO.getDepartureAirportName(), 20));
					postDataMap.put(OgoneRequest.ARRIVAL_AIRPORT + flightID,
							StringUtil.getSubString(segmentDTO.getArrivalAirportCode(), 5));
					postDataMap.put(OgoneRequest.ARRIVAL_AIRPORT_NAME + flightID,
							StringUtil.getSubString(segmentDTO.getArrivalAirportName(), 20));
					postDataMap.put(OgoneRequest.NO_OF_STOPOVERS + flightID,segmentDTO.getNoOfStopvers().toString());
					
					if(activeCabinClassMap != null && !activeCabinClassMap.isEmpty() && activeCabinClassMap.containsKey(segmentDTO.getClassOfService())) {
						postDataMap.put(OgoneRequest.CLASS_OF_SERVICE + flightID,activeCabinClassMap.get(segmentDTO.getClassOfService()));
					}
					
					flightID++;
				}
			}
		}
	}

	public static String getTrackingParameter(int transactionRefNo, IPGRequestDTO ipgRequestDTO) {
		return transactionRefNo + "_" + ipgRequestDTO.getIpgIdentificationParamsDTO().getIpgId() + "_"
				+ ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode();
	}
	
	private static boolean isNotEmpty(String code) {
		boolean isEmpty = false;
		if (code != null && !code.trim().isEmpty()) {
			isEmpty = true;
		}		
		return isEmpty;
	}	
	
	protected static void addDynamicTemplateUrl(boolean isEnableDynamicTemplate, Map<String, String> postDataMap, 
			Map<String, String> dynamicTemplateUrl, String language) {
		
		if (StringUtil.isNullOrEmpty(language)) {
			language = DEFAULT_TEMPLATE_LANGUAGE;
		}
		
		String  templateUrl = dynamicTemplateUrl.get(language.toLowerCase());
		
		if (StringUtil.isNullOrEmpty(templateUrl)) {
			templateUrl = dynamicTemplateUrl.get(DEFAULT_TEMPLATE_LANGUAGE);
		}
		
		if (isEnableDynamicTemplate && isNotEmpty(templateUrl)) {
			postDataMap.put(OgoneRequest.DYNAMIC_TEMPLATE, templateUrl);
		}
	}

}
