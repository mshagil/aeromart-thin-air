/*
 * Decompiled with CFR 0_102.
 */
package com.isa.thinair.paymentbroker.core.bl.benefit;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.Inflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class SecureResource {
    private static final int BUFFER = 3480;
    private static final String TRANPORTAL_ID_NODE = "id";
    private static final String PASSWORD_NODE = "password";
    private static final String WEB_ADDRESS_NODE = "webaddress";
    private static final String PORT_NODE = "port";
    private static final String CONTEXT_NODE = "context";
    private static final String NODE_OPEN = "<";
    private static final String NODE_CLOSE = ">";
    private static final String NODE_TERMINATE = "</";
    private static final String TAG_ID_OPEN = "<id>";
    private static final String TAG_PASSWORD_OPEN = "<password>";
    private static final String TAG_WEBADDRESS_OPEN = "<webaddress>";
    private static final String TAG_PORT_OPEN = "<port>";
    private static final String TAG_CONTEXT_OPEN = "<context>";
    private static final String TAG_ID_CLOSE = "</id>";
    private static final String TAG_PASSWORD_CLOSE = "</password>";
    private static final String TAG_WEBADDRESS_CLOSE = "</webaddress>";
    private static final String TAG_PORT_CLOSE = "</port>";
    private static final String TAG_CONTEXT_CLOSE = "</context>";
    private String strResourcePath = "";
    private String strAlias = "";
    private String termID = "";
    private String password = "";
    private String port = "";
    private String context = "";
    private String webAddress = "";
    private String error = "";
    private boolean bDebugOn = true;
    private StringBuffer debugMsg;
    private boolean bSecureResourceDecoded;
    private static final String TRANSACTION_PREFIX = "Tran";
    private static final String TRANSACTION_SECTION_DELIMITER = "#";
    private static byte[] key;

    public SecureResource() {
        this.setupKey();
    }

    public synchronized boolean getSecureSettings() {
        if (this.strResourcePath == null) {
            this.error = "No resource path specified.";
            return false;
        }
        String strData = "";
        if (!this.createReadableZip()) {
            return false;
        }
        strData = this.readZip(this.getAlias() + ".xml");
        if (strData.equals("")) {
            return false;
        }
        this.destroyUnSecureResource();
        if (!this.parseSettings(strData)) {
            return false;
        }
        return true;
    }

    public synchronized boolean getTransactions(ArrayList transactions) {
        if (this.strResourcePath == null) {
            this.error = "No resource path specified.";
            return false;
        }
        String strData = "";
        if (!this.createReadableZip()) {
            return false;
        }
        ArrayList entries = new ArrayList();
        if (this.readZipEntries(entries)) {
            for (int i = 0; i < entries.size(); ++i) {
                String entryName = (String)entries.get(i);
                if (!entryName.startsWith("Tran")) continue;
                transactions.add(entryName);
            }
        }
        this.destroyUnSecureResource();
        if (!this.parseSettings(strData)) {
            return false;
        }
        return true;
    }

    public synchronized boolean getTerminalAliases(ArrayList aliases) {
        if (this.strResourcePath == null) {
            this.error = "No resource path specified.";
            return false;
        }
        String strData = "";
        if (!this.createReadableZip()) {
            return false;
        }
        ArrayList entries = new ArrayList();
        if (this.readZipEntries(entries)) {
            for (int i = 0; i < entries.size(); ++i) {
                String entryName = (String)entries.get(i);
                if (entryName.startsWith("Tran")) continue;
                aliases.add(entryName.substring(0, entryName.indexOf(".xml")));
            }
        }
        this.destroyUnSecureResource();
        if (!this.parseSettings(strData)) {
            return false;
        }
        return true;
    }

    public synchronized String getTransaction(String transactionName, String version) {
        if (this.strResourcePath == null) {
            this.error = "No resource path specified.";
            return null;
        }
        if (transactionName == null) {
            this.error = "No transaction name specified.";
            return null;
        }
        if (version == null) {
            version = "1.0.0";
        }
        String strData = "";
        if (!this.createReadableZip()) {
            return null;
        }
        strData = this.readZip("Tran#" + transactionName + "#" + version + ".xml");
        if (strData.equals("")) {
            return null;
        }
        this.destroyUnSecureResource();
        return strData;
    }

    private synchronized void destroyUnSecureResource() {
        try {
            File delFile = new File(this.getResourcePath() + "resource.cgz");
            delFile.delete();
            this.bSecureResourceDecoded = false;
            if (this.bDebugOn) {
                this.addDebugMessage("Decoded Resource Destroyed.");
            }
        }
        catch (Exception e) {
            this.bSecureResourceDecoded = true;
        }
    }

    private synchronized boolean createReadableZip() {
        if (this.bSecureResourceDecoded) {
            return true;
        }
        byte[] data = new byte[3480];
        BufferedInputStream zip = null;
        try {
            if (this.bDebugOn) {
                this.addDebugMessage("Locating Secure Resource.");
            }
            FileInputStream fz = new FileInputStream(this.getResourcePath() + "resource.cgn");
            zip = new BufferedInputStream(fz);
            FileOutputStream xorZip = new FileOutputStream(this.getResourcePath() + "resource.cgz");
            int offset = 0;
            int count = 0;
            if (this.bDebugOn) {
                this.addDebugMessage("Decoding Secure Resource.");
            }
            while ((count = zip.read(data)) != -1) {
                xorZip.write(this.simpleXOR(data), offset, count);
                offset+=count;
            }
            fz.close();
            zip.close();
            xorZip.close();
            this.bSecureResourceDecoded = true;
        }
        catch (Exception e) {
            this.error = "Failed to create readable ZIP " + e;
            return false;
        }
        return true;
    }

    private synchronized String readZip(String entryFileName) {
        if (this.getResourcePath() == null || this.getResourcePath().equals("")) {
            this.error = "Error Accessing Secure Resource. Resource Path not set.";
            return null;
        }
        if (entryFileName == null || entryFileName.equals("")) {
            this.error = "Error Accessing Secure Resource. Terminal Alias not set.";
            return null;
        }
        String strData = "";
        byte[] data = new byte[3480];
        try {
            File file;
            ZipFile zip;
            ZipEntry zipEntry;
            if (this.bDebugOn) {
                this.addDebugMessage("Accessing Decoded Secure Resource.");
            }
            if ((zipEntry = (zip = new ZipFile(file = new File(this.getResourcePath() + "resource.cgz"))).getEntry(entryFileName)) == null) {
                zip.close();
                this.error = "The ZIP Entry " + this.getAlias() + ".xml does not exist.";
                return strData;
            }
            if (this.bDebugOn) {
                this.addDebugMessage("Resource Entry Retrieved.");
            }
            InputStream readStream = zip.getInputStream(zipEntry);
            int offset = 0;
            int count = 0;
            while ((count = readStream.read(data)) != -1) {
                String strTemp = new String(this.simpleXOR(data));
                strData = strData + strTemp;
                offset+=count;
            }
            zip.close();
            if (this.bDebugOn) {
                this.addDebugMessage("Resource Entry Parsed.");
            }
        }
        catch (Exception e) {
            this.error = "Failed to read ZIP " + e;
            return strData;
        }
        return strData;
    }

    private synchronized boolean readZipEntries(ArrayList entries) {
        if (this.getResourcePath() == null || this.getResourcePath().equals("")) {
            this.error = "Error Accessing Secure Resource. Resource Path not set.";
            return false;
        }
        try {
            if (this.bDebugOn) {
                this.addDebugMessage("Accessing Decoded Secure Resource.");
            }
            File file = new File(this.getResourcePath() + "resource.cgz");
            ZipFile zip = new ZipFile(file);
            Enumeration<? extends ZipEntry> eEntries = zip.entries();
            while (eEntries.hasMoreElements()) {
                ZipEntry zEntry = eEntries.nextElement();
                if (this.bDebugOn) {
                    this.addDebugMessage("Processing Resource Entry..." + (zEntry != null ? zEntry.getName() : ""));
                }
                entries.add(zEntry.getName());
            }
            zip.close();
            if (this.bDebugOn) {
                this.addDebugMessage("Resource Entry Parsed.");
            }
        }
        catch (Exception e) {
            this.error = "Failed to read ZIP " + e;
            return false;
        }
        return true;
    }

    private synchronized boolean parseSettings(String settings) {
        int begin = 0;
        int end = 0;
        try {
            if (this.bDebugOn) {
                this.addDebugMessage("Parsing Settings.");
            }
            begin = settings.indexOf("<id>") + "<id>".length();
            end = settings.indexOf("</id>");
            this.setTermID(settings.substring(begin, end));
            begin = settings.indexOf("<password>") + "<password>".length();
            end = settings.indexOf("</password>");
            this.setPassword(settings.substring(begin, end));
            begin = settings.indexOf("<webaddress>") + "<webaddress>".length();
            end = settings.indexOf("</webaddress>");
            this.setWebAddress(settings.substring(begin, end));
            begin = settings.indexOf("<port>") + "<port>".length();
            end = settings.indexOf("</port>");
            this.setPort(settings.substring(begin, end));
            begin = settings.indexOf("<context>") + "<context>".length();
            end = settings.indexOf("</context>");
            this.setContext(settings.substring(begin, end));
            return true;
        }
        catch (Exception e) {
            this.error = "Error parsing internal settings file. Error:" + e;
            return false;
        }
    }

    private synchronized byte[] simpleXOR(byte[] byteInput) {
        if (this.bDebugOn) {
            this.addDebugMessage("Decoding Buffer.");
        }
        byte[] result = new byte[byteInput.length];
        int m = 0;
        block0 : while (m < byteInput.length) {
            for (int k = 0; k < key.length; ++k) {
                result[m] = (byte)(byteInput[m] ^ key[k]);
                if (++m == byteInput.length) continue block0;
            }
        }
        return result;
    }

    private synchronized void setupKey() {
        byte[] cryptKey = new String("789<1=<<<10=02310<04404:=9:268<0229;<412679?9<0=27;:07==733>?39<;9886;26<><:<>;5:044;74?>>422?;2>5018;862?85<6;3?8721361<365?20<581107>32>2>0;:1<<??72<==<82>68><6;:8;;78?:9?0188?1?0=3?").getBytes();
        byte[] uncryptKey = new byte[93];
        if (this.bDebugOn) {
            this.addDebugMessage("Setting up the key to the Secure Resource file.");
        }
        int outIx = 0;
        outIx = 0;
        int inIx = 0;
        while (inIx < cryptKey.length - 1) {
            byte endByte;
            int byte1 = new Integer(cryptKey[inIx] - 48);
            int byte2 = new Integer(cryptKey[inIx + 1] - 48);
            uncryptKey[outIx] = endByte = (byte)(byte1 * 16 + byte2 & 255);
            inIx+=2;
            ++outIx;
        }
        StringBuffer sbuf = new StringBuffer();
        try {
            Inflater decompress = new Inflater();
            decompress.setInput(uncryptKey);
            int bytesLeft = 0;
            byte[] outbuf = new byte[16];
            do {
                int bytesInflated = decompress.inflate(outbuf);
                bytesLeft = decompress.getRemaining();
                String str2 = new String(outbuf, 0, bytesInflated);
                sbuf.append(str2);
            } while (bytesLeft > 0);
            decompress.end();
        }
        catch (Exception e) {
            this.error = "Error decrypting decryption Key. " + e;
        }
        byte[] tempKeyBytes = sbuf.toString().getBytes();
        key = new byte[116];
        for (int keyBytes = 0; keyBytes < key.length; ++keyBytes) {
            SecureResource.key[keyBytes] = tempKeyBytes[keyBytes];
        }
    }

    public synchronized boolean isDebugOn() {
        return this.bDebugOn;
    }

    public synchronized void setDebugOn(boolean debugOn) {
        this.bDebugOn = debugOn;
    }

    private synchronized void addDebugMessage(String message) {
        if (this.bDebugOn) {
            if (this.debugMsg == null) {
                this.debugMsg = new StringBuffer();
            }
            this.debugMsg.append(message);
        }
    }

    public synchronized String getDebugMsg() {
        return this.debugMsg.toString();
    }

    public synchronized String getResourcePath() {
        return this.strResourcePath;
    }

    public synchronized void setResourcePath(String strResourcePath) {
        this.strResourcePath = strResourcePath;
    }

    public synchronized String getAlias() {
        return this.strAlias;
    }

    public synchronized void setAlias(String strAlias) {
        this.strAlias = strAlias;
    }

    public synchronized String getContext() {
        return this.context;
    }

    public synchronized void setContext(String context) {
        this.context = context;
    }

    public synchronized String getPassword() {
        return this.password;
    }

    public synchronized void setPassword(String password) {
        this.password = password;
    }

    public synchronized String getPort() {
        return this.port;
    }

    public synchronized void setPort(String port) {
        this.port = port;
    }

    public synchronized String getTermID() {
        return this.termID;
    }

    public synchronized void setTermID(String termID) {
        this.termID = termID;
    }

    public synchronized String getWebAddress() {
        return this.webAddress;
    }

    public synchronized void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public synchronized String getError() {
        return this.error;
    }
}
