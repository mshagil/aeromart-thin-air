package com.isa.thinair.paymentbroker.core.bl.cybersource;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerCyberSourceHOPImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static final Log log = LogFactory.getLog(PaymentBrokerCyberSourceHOPImpl.class);

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static final String CYBER_SOURCE_ERROR_PREFIX = "cyberSource.";

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		String strOrderInfo = CyberSourcePaymentUtils.getOrderInfo(ipgRequestDTO.getPnr(), merchantTxnId,
				ipgRequestDTO.getRequestedCarrierCode(), ipgRequestDTO.getUserIPAddress());

		String finalAmount = CyberSourcePaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount(),
				ipgRequestDTO.getNoOfDecimals());
		String sessionId = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		String billStreet1 = ipgRequestDTO.getContactAddressLine1() + ipgRequestDTO.getContactAddressLine2();
		if (billStreet1 != null && billStreet1.trim().isEmpty()) {
			billStreet1 = "UNKNOWN";
		}

		String email = (ipgRequestDTO.getContactEmail() == null) ? "" : ipgRequestDTO.getContactEmail();

		CyberSourceRequest ipgRequest = new CyberSourceRequest(finalAmount, merchantTxnId, ipgRequestDTO.getContactFirstName(),
				ipgRequestDTO.getContactLastName(), billStreet1, ipgRequestDTO.getContactCity(),
				ipgRequestDTO.getContactCountryCode(), email, ipgRequestDTO.getContactPostalCode());
		ipgRequest.composeCyberSourceConfigData(getMerchantId(), getSecureSecret(), getSerialNumber(), getVersion(),
				getMerchantPostURL(), ipgRequestDTO.getReceiptUrl(), getReceiptLinkText(), sessionId);

		String storeData = PaymentBrokerUtils.getRequestDataAsString(ipgRequest.getRequestFields());
		String requestData = new CyberSourceUtils(getMerchantId(), getSecureSecret(), getSerialNumber()).insertSignature(
				ipgRequest.getRequestFields(), getIpgURL());

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				ipgRequestDTO.getApplicationTransactionId(), bundle.getString("SERVICETYPE_AUTHORIZE"), storeData, "",
				getPaymentGatewayName(), null, false);

		if (log.isDebugEnabled()) {
			log.debug("#################################################################################");
			log.debug(" Payment Broker ref no (Transaction_Ref_No): " + ccTransaction.getTransactionRefNo());
			log.debug(" Merchant Transaction Id : " + merchantTxnId);
			log.debug(" Tempory Payment Transaction Id " + ipgRequestDTO.getApplicationTransactionId());
			log.debug(" Session Id: " + sessionId);
			log.debug("#################################################################################");
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestData);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);

		return ipgRequestResultsDTO;
	}

	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		log.debug("[PaymentBrokerCyberSourceHOPImpl::getReponseData()] Begin ");

		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		String merchantTxnReference;
		String authorizeID;

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		CyberSourceResponse csResponse = new CyberSourceResponse(fields);
		String hashValidated;
		String responseMismatch = "";

		boolean errorExists = true;
		if (csResponse != null && !csResponse.getDecision().isEmpty()) {
			if (csResponse.verifyTransactionSignature(getSecureSecret())) {
				hashValidated = CyberSourceResponse.VALID_HASH;

				if (oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(csResponse.getOrderNumber())) {
					errorExists = false;
				} else
					responseMismatch = CyberSourceResponse.INVALID_RESPONSE;

			} else {
				// Secure Hash validation failed, add a data field to be
				// displayed later.
				hashValidated = CyberSourceResponse.INVALID_HASH;
			}
		} else {
			// hash is not validated
			hashValidated = CyberSourceResponse.NO_VALUE;
			errorCode = "DEFAULT_TRANSACTION_CODE"; // TODO
		}

		merchantTxnReference = csResponse.getOrderNumber();
		authorizeID = csResponse.getAuthorizationCode();
		int cardType = csResponse.getCardType();

		if (csResponse != null && csResponse.getDecision().equals(CyberSourceResponse.DECISION_ACCEPT) && !errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "";

		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;

			errorCode = CYBER_SOURCE_ERROR_PREFIX + csResponse.getReasonCode();
			errorSpecification = CyberSourcePaymentUtils
					.getResponseStatusDescription(Integer.parseInt(csResponse.getReasonCode()));
		}

		log.info("**********Response of the PG:  " + status + ": " + tnxResultCode + "************************");

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(errorSpecification);
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authorizeID);
		ipgResponseDTO.setCardType(cardType);

		String csStringResponse = PaymentBrokerUtils.getRequestDataAsString(csResponse.getResponse());

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), csStringResponse, errorSpecification,
				authorizeID, csResponse.getRequestID(), tnxResultCode);

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}
		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
