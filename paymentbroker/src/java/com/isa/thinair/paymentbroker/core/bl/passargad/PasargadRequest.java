package com.isa.thinair.paymentbroker.core.bl.passargad;

public class PasargadRequest {

	public static final String MERCHANT_CODE = "MERCHANT_CODE";

	public static final String TERMINAL_CODE = "TERMINAL_CODE";

	public static final String MERCHANT_ACCOUNT = "MERCHANT_ACCOUNT";

	public static final String INVOICE_NUMBER = "INVOICE_NUMBER";

	public static final String ACTION = "ACTION";

	public static final String AMOUNT = "AMOUNT";

	public static final String IPG_URL = "IPG_URL";

	public static final String REDIRECT_URL = "REDIRECT_URL";

	public static final String TIMESTAMP = "TIMESTAMP";

	public static final String KEYSTORE_LOCATION = "KEYSTORE_LOCATION";
	
	public static final String KEYSTORE_ALIAS = "KEYSTORE_ALIAS";

	public static final String INVOICE_DATE = "INVOICE_DATE";

	public static final String TRANSACTION_REF_NUMBER = "tref";
	
	public static final String REDIRECTION_MECHANISM = "REDIRECTION_MECHANISM";
	
	public static final String BROKER_TYPE = "BROKER_TYPE";
}
