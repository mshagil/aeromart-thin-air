package com.isa.thinair.paymentbroker.core.bl.eFawteer;

import com.isa.thinair.paymentbroker.api.constants.EFawateerConstants;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class EFawateerPaymentUtils {
	private static final Log log = LogFactory.getLog(EFawateerPaymentUtils.class);

	private EFawateerPaymentUtils() {
	}

	public static boolean isSignatureValid(String plainText, String signature, String keystoreName) {
		try {
			String keyStorePath = PlatformConstants.getConfigRootAbsPath() + keystoreName;
			FileInputStream publicKeyFile = new FileInputStream(keyStorePath);
			CertificateFactory certificateInstance = CertificateFactory.getInstance(EFawateerConstants.X_509);
			X509Certificate certificate = (X509Certificate) certificateInstance.generateCertificate(publicKeyFile);
			PublicKey publicKey = certificate.getPublicKey();
			Signature publicSignature = Signature.getInstance(EFawateerConstants.SHA256_WITH_RSA);
			publicSignature.initVerify(publicKey);
			publicSignature.update(plainText.getBytes(StandardCharsets.UTF_16LE));
			byte[] signatureBytes = Base64.getDecoder().decode(signature);
			return publicSignature.verify(signatureBytes);
		} catch (Exception e) {
			log.error("Error verifying the signature", e);
		}
		return false;
	}

	public static String getSignature(String plaintext, String secureKeyLocation) {
		try {
			String keyStorePath = PlatformConstants.getConfigRootAbsPath() + secureKeyLocation;
			File privateKeyFile = new File(keyStorePath);
			DataInputStream inputStream = new DataInputStream(new FileInputStream(privateKeyFile));
			byte[] privateKeyBytes = new byte[(int) privateKeyFile.length()];
			inputStream.read(privateKeyBytes);
			inputStream.close();
			KeyFactory keyFactory = KeyFactory.getInstance(EFawateerConstants.RSA);
			PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(privateKeyBytes);
			RSAPrivateKey privateKey = (RSAPrivateKey) keyFactory.generatePrivate(privateSpec);
			Signature privateSignature = Signature.getInstance(EFawateerConstants.SHA256_WITH_RSA);
			privateSignature.initSign(privateKey);
			privateSignature.update(plaintext.getBytes(StandardCharsets.UTF_16LE));
			byte[] signature = privateSignature.sign();
			String signatureText = Base64.getEncoder().encodeToString(signature);
			return signatureText;
		} catch (Exception e) {
			log.error("Error generating the signature", e);
		}
		return null;
	}

}
