package com.isa.thinair.paymentbroker.core.bl.ogone;

import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelSegmentDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.dto.XMLResponse;

public class PaymentBrokerOgoneMOTOImpl extends PaymentBrokerOgoneTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerOgoneMOTOImpl.class);

	/**
	 * Performs card payment operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {

		String merchantTxnId;
		String errorCode;
		String transactionMsg;
		int txnResultCode;
		String status;
		String errorSpecification;
		String cardNumber;
		String cardExpiry;
		String cvc;

		// Get a merchant transaction id(OrderId) to identify txn in the payment server, in case of failure.
		merchantTxnId = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		cardNumber = creditCardPayment.getCardNumber();
		cardExpiry = creditCardPayment.getExpiryDate();
		cvc = creditCardPayment.getCvvField();

		String formatedAmount = PaymentBrokerUtils.getFormattedAmount(creditCardPayment.getAmount(),
				creditCardPayment.getNoOfDecimalPoints());

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(OgoneRequest.AMOUNT, formatedAmount);
		postDataMap.put(OgoneRequest.CURRENCY, creditCardPayment.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		postDataMap.put(OgoneRequest.ECI, String.valueOf(OgonePaymentUtils.ECI.MOTO.getECIValue()));
		postDataMap.put(OgoneRequest.OPERATION, getOperation());
		postDataMap.put(OgoneRequest.ORDERID, merchantTxnId);
		postDataMap.put(OgoneRequest.PSPID, getMerchantId());
		postDataMap.put(OgoneRequest.USERID, getUser());
		postDataMap.put(OgoneRequest.REMOTE_ADDR, creditCardPayment.getUserIP());
		// Add Fraud Checking data
		postDataMap.put(OgoneRequest.DATATYPE, OgoneRequest.DATATYPE_TRAVEL);
		postDataMap.put(OgoneRequest.TICKETNO, StringUtil.getSubString(pnr, 16));
		postDataMap
				.put(OgoneRequest.TICKETDATE, CalendarUtil.formatDate(Calendar.getInstance().getTime(), CalendarUtil.PATTERN3));
		TravelDTO travelData = creditCardPayment.getTravelDTO();

		if (travelData != null) {
			String email = travelData.getEmailAddress();
			String countryCode = travelData.getResidenceCountryCode();
			String phoneNo = travelData.getContactNumber();

			if (phoneNo != null)
				phoneNo = StringUtil.extractNumberFromString(phoneNo);

			if (email != null && !email.trim().isEmpty())
				postDataMap.put(OgoneRequest.CUSTOMER_EMAIL, email);
			if (countryCode != null && !countryCode.trim().isEmpty())
				postDataMap.put(OgoneRequest.CUSTOMER_COUNTRY, countryCode);
			if (phoneNo != null && !phoneNo.trim().isEmpty())
				postDataMap.put(OgoneRequest.CUSTOMER_TELEPHONENO, phoneNo);

			Collection<TravelSegmentDTO> segments = creditCardPayment.getTravelDTO().getTravelSegments();

			if (segments != null) {
				int flightID = 1;
				for (TravelSegmentDTO segmentDTO : segments) {
					postDataMap.put(OgoneRequest.FLIGHTNO + flightID, StringUtil.getSubString(segmentDTO.getFlightNumber(), 4));
					postDataMap.put(OgoneRequest.FLIGHT_DATE + flightID,
							CalendarUtil.formatDate(segmentDTO.getFlightDate(), CalendarUtil.PATTERN4));
					postDataMap
							.put(OgoneRequest.CARRIER_CODE + flightID, StringUtil.getSubString(segmentDTO.getCarrierCode(), 4));
					postDataMap.put(OgoneRequest.DEPARTURE_AIRPORT + flightID,
							StringUtil.getSubString(segmentDTO.getDepartureAirportCode(), 5));
					postDataMap.put(OgoneRequest.DEPARTURE_AIRPORT_NAME + flightID,
							StringUtil.getSubString(segmentDTO.getDepartureAirportName(), 20));
					postDataMap.put(OgoneRequest.ARRIVAL_AIRPORT + flightID,
							StringUtil.getSubString(segmentDTO.getArrivalAirportCode(), 5));
					postDataMap.put(OgoneRequest.ARRIVAL_AIRPORT_NAME + flightID,
							StringUtil.getSubString(segmentDTO.getArrivalAirportName(), 20));
					flightID++;
				}
			}
		}

		// get request parameters before adding card details to log.
		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		postDataMap.put(OgoneRequest.PSWD, getPassword());
		postDataMap.put(OgoneRequest.CARDNO, cardNumber);
		postDataMap.put(OgoneRequest.ED, cardExpiry.substring(2) + cardExpiry.substring(0, 2));
		postDataMap.put(OgoneRequest.CVC, cvc);
		postDataMap.put(OgoneRequest.CUSTOMER_NAME, creditCardPayment.getCardholderName());

		try {
			String shaSign = OgonePaymentUtils.computeSHA1(postDataMap, getSha1In());
			postDataMap.put(OgoneRequest.SHASIGN, shaSign);
			strRequestParams = strRequestParams + OgoneRequest.SHASIGN + "=" + shaSign;
		} catch (Exception e) {
			log.debug("Error computing SHA-1 : " + e.toString());
			throw new ModuleException(e.getMessage());
		}

		// Add other required parameters to create the post request.
		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());
		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("URL", getIpgURL());

		CreditCardTransaction ccTransaction = auditTransaction(pnr, getMerchantId(), merchantTxnId,
				new Integer(creditCardPayment.getTemporyPaymentId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams, "", getPaymentGatewayName(), null, false);

		Integer paymentBrokerRefNo;
		XMLResponse response;

		// Sends the payment request and gets the response
		try {
			response = OgonePaymentUtils.getOgoneXMLResponse(postDataMap);

			paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		// create a hash map for the response data
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);

		if (AUTHORIZATION_SUCCESS.equals(response.getStatus()) || PAYMENT_SUCCESS.equals(response.getStatus())) {
			errorCode = "";
			transactionMsg = response.getNcErrorPlus();
			status = IPGResponseDTO.STATUS_ACCEPTED;
			txnResultCode = 1;
			errorSpecification = "";
			serviceResponse.setSuccess(true);
		} else {
			errorCode = OgonePaymentUtils.getFilteredErrorCode(response.getStatus(), response.getNcError());
			transactionMsg = OgonePaymentUtils.getResponseStatusDescription(response.getStatus(), response.getNcError());
			status = IPGResponseDTO.STATUS_REJECTED;
			txnResultCode = 0;
			errorSpecification = status + " "
					+ OgonePaymentUtils.getResponseStatusDescription(response.getStatus(), response.getNcError()) + " "
					+ response.getNcError() + " " + response.getNcErrorPlus();

			// Log unknown status transactions
			if (response.getStatus().equals("50") || response.getStatus().equals("51") || response.getStatus().equals("52")
					|| response.getStatus().equals("55") || response.getStatus().equals("59")) {
				log.debug("We received an unknown status for the transaction. Order id : " + response.getOrderId());
			}
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorSpecification,
				response.getAcceptance(), response.getPayId(), txnResultCode);

		// Populate service response object
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAcceptance());
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

		return serviceResponse;
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}
		reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		return reverseResult;
	}

}
