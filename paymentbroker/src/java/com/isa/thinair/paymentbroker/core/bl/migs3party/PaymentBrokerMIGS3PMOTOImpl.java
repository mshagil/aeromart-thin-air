/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.migs3party;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * The payment broker client implementation of the MOTO version of MIGS
 * 
 * @author Asiri Liyanage
 */
public class PaymentBrokerMIGS3PMOTOImpl extends PaymentBrokerTemplate implements PaymentBroker {

	static Log log = LogFactory.getLog(PaymentBrokerMIGS3PMOTOImpl.class);

	public static SSLSocketFactory s_sslSocketFactory = null;

	private static final String IPG_URL_PART_MOTO = "vpcdps";
	private static final String PAY_APPROVED_MESSAGE = "Approved";
	private static final String TRANSACTION_RESULT_SUCCESS = "0";
	private static final String PAY_COMMAND = "pay";
	private static final String REFUND_COMMAND = "refund";
	private static final String VOID_COMMAND = "voidPurchase";
	private static final String CAPTURE_COMMAND = "capture";
	private static final Double hundred = new Double(100);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	static {
		X509TrustManager s_x509TrustManager = new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] {};
			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) {
			}
		};

		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		try {
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { s_x509TrustManager }, null);
			s_sslSocketFactory = context.getSocketFactory();
		} catch (Exception e) {
			log.error("FATAL ERROR ", e);
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Performs card refund operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;
		String merchTnxRef = "";
		String ipgURLRefund = getIpgURL() + IPG_URL_PART_MOTO;

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
					PaymentConstants.MODE_OF_SERVICE.REFUND);
			String amount = MIGSPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());

			Map fields = new HashMap();
			fields.put(MIGS3PRequest.VPC_VERSION, getVersion());
			fields.put(MIGS3PRequest.VPC_COMMAND, REFUND_COMMAND);
			fields.put(MIGS3PRequest.VPC_ACC_CODE, getMerchantAccessCode());
			fields.put(MIGS3PRequest.VPC_MERCH_TXN_REF, merchTnxRef);
			fields.put(MIGS3PRequest.VPC_MERCHANT, getMerchantId());
			fields.put(MIGS3PRequest.VPC_TXN_NO, ccTransaction.getTransactionId());
			fields.put(MIGS3PRequest.VPC_AMOUNT, amount);
			fields.put(MIGS3PRequest.VPC_ORDER_INFO,
					MIGSPaymentUtils.getOrderInfo(creditCardPayment.getPnr(), merchTnxRef, "", null));
			fields.put(MIGS3PRequest.VPC_USER, getUser());
			fields.put(MIGS3PRequest.VPC_PASSWORD, getPassword());

			String refundResponse = null;
			Integer paymentBrokerRefNo;

			// Sends the refund request and gets the response
			try {
				Object[] responses = doPost(pnr, creditCardPayment.getTemporyPaymentId(), ipgURLRefund, fields, false, null);
				refundResponse = (String) responses[0];
				paymentBrokerRefNo = (Integer) responses[1];

			} catch (UnsupportedEncodingException uee) {
				log.error(uee);
				throw new ModuleException("paymentbroker.error.unsupportedencoding");
			} catch (IOException ioe) {
				log.error(ioe);
				throw new ModuleException("paymentbroker.error.ioException");
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Refund Response : " + refundResponse);

			// create a hash map for the response data
			Map responseFields = MIGSPaymentUtils.createMapFromResponse(refundResponse);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			MIGS3PResponse migsResp = new MIGS3PResponse();
			migsResp.setReponse(responseFields);

			if (TRANSACTION_RESULT_SUCCESS.equals(migsResp.getTxnResponseCode())
					&& PAY_APPROVED_MESSAGE.equals(migsResp.getMessage())) {
				status = IPGResponseDTO.STATUS_ACCEPTED;
				errorCode = "";
				transactionMsg = migsResp.getMessage();
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				/* errorCode = getMatchingTxnErrorCode(migsResp.getTxnResponseCode()); */
				errorCode = PaymentResponseCodesUtil.getFilteredErrorCodeMOTO(migsResp);
				if (migsResp.getMessage().contains("Excessive refund atttempted")) {
					transactionMsg = "Excessive refund Amount is atttempted";
				} else {
					transactionMsg = PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode());
				}

				tnxResultCode = 0;
				errorSpecification = status + " " + transactionMsg + " " + migsResp.getMessage();
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), migsResp.toString(), errorSpecification,
					migsResp.getAuthorizeID(), migsResp.getTransactionNo(), tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, migsResp.getAuthorizeID());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

			return sr;
		}
	}

	/**
	 * Performs card void operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce voidPurchase(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;
		String merchTnxRef = "";
		String ipgURLRefund = getIpgURL() + IPG_URL_PART_MOTO;

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
					PaymentConstants.MODE_OF_SERVICE.VOID);
			String amount = MIGSPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());

			Map fields = new HashMap();
			fields.put(MIGS3PRequest.VPC_VERSION, getVersion());
			fields.put(MIGS3PRequest.VPC_COMMAND, VOID_COMMAND);
			fields.put(MIGS3PRequest.VPC_ACC_CODE, getMerchantAccessCode());
			fields.put(MIGS3PRequest.VPC_MERCH_TXN_REF, merchTnxRef);
			fields.put(MIGS3PRequest.VPC_MERCHANT, getMerchantId());
			fields.put(MIGS3PRequest.VPC_TXN_NO, ccTransaction.getTransactionId());
			fields.put(MIGS3PRequest.VPC_AMOUNT, amount);
			fields.put(MIGS3PRequest.VPC_ORDER_INFO,
					MIGSPaymentUtils.getOrderInfo(creditCardPayment.getPnr(), merchTnxRef, "", null));
			fields.put(MIGS3PRequest.VPC_USER, getVoidUser());
			fields.put(MIGS3PRequest.VPC_PASSWORD, getVoidPassword());

			String reversalResponse = null;
			Integer paymentBrokerRefNo;

			// Sends the refund request and gets the response
			try {
				Object[] responses = doPost(pnr, creditCardPayment.getTemporyPaymentId(), ipgURLRefund, fields, false, null);
				reversalResponse = (String) responses[0];
				paymentBrokerRefNo = (Integer) responses[1];

			} catch (UnsupportedEncodingException uee) {
				log.error(uee);
				throw new ModuleException("paymentbroker.error.unsupportedencoding");
			} catch (IOException ioe) {
				log.error(ioe);
				throw new ModuleException("paymentbroker.error.ioException");
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Reversal Response : " + reversalResponse);

			// create a hash map for the response data
			Map responseFields = MIGSPaymentUtils.createMapFromResponse(reversalResponse);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			MIGS3PResponse migsResp = new MIGS3PResponse();
			migsResp.setReponse(responseFields);

			if (TRANSACTION_RESULT_SUCCESS.equals(migsResp.getTxnResponseCode())
					&& PAY_APPROVED_MESSAGE.equals(migsResp.getMessage())) {
				status = IPGResponseDTO.STATUS_ACCEPTED;
				errorCode = "";
				transactionMsg = migsResp.getMessage();
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_VOID);
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				/* errorCode = getMatchingTxnErrorCode(migsResp.getTxnResponseCode()); */
				errorCode = PaymentResponseCodesUtil.getFilteredErrorCodeMOTO(migsResp);
				transactionMsg = PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode());
				tnxResultCode = 0;
				errorSpecification = status + " " + transactionMsg + " " + migsResp.getMessage();
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), migsResp.toString(), errorSpecification,
					migsResp.getAuthorizeID(), migsResp.getTransactionNo(), tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, migsResp.getAuthorizeID());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REVERSAL);

			return sr;
		}
	}

	/**
	 * Performs card payment operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {

		String merchantTxnId;
		String errorCode;
		String transactionMsg;
		int txnResultCode;
		String status;
		String errorSpecification;
		String cardNumber;
		String cardExpiry;
		String cvv;
		Map creditCardInfoMap;

		String ipgURLPay = getIpgURL() + IPG_URL_PART_MOTO;

		// Get a merchant transaction id to identify txn in the payment server, in case of failure.
		merchantTxnId = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		cardNumber = creditCardPayment.getCardNumber();
		cardExpiry = creditCardPayment.getExpiryDate();
		cvv = creditCardPayment.getCvvField();

		String formatedAmount = MIGSPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
				creditCardPayment.getNoOfDecimalPoints());

		// Creating the order information by buddling the PNR and the Merchant Txn Id together
		String strOrderInfo = MIGSPaymentUtils.getOrderInfo(creditCardPayment.getPnr(), merchantTxnId, "",
				creditCardPayment.getUserIP());

		creditCardInfoMap = new HashMap();
		creditCardInfoMap.put(MIGS3PRequest.VPC_VERSION, getVersion());
		creditCardInfoMap.put(MIGS3PRequest.VPC_COMMAND, PAY_COMMAND);
		creditCardInfoMap.put(MIGS3PRequest.VPC_ACC_CODE, getMerchantAccessCode());
		creditCardInfoMap.put(MIGS3PRequest.VPC_MERCH_TXN_REF, merchantTxnId);
		creditCardInfoMap.put(MIGS3PRequest.VPC_MERCHANT, getMerchantId());
		creditCardInfoMap.put(MIGS3PRequest.VPC_ORDER_INFO, strOrderInfo);
		creditCardInfoMap.put(MIGS3PRequest.VPC_AMOUNT, formatedAmount);
		creditCardInfoMap.put(MIGS3PRequest.VPC_CARD_NUM, cardNumber);
		creditCardInfoMap.put(MIGS3PRequest.VPC_CARD_EXP, cardExpiry);
		creditCardInfoMap.put(MIGS3PRequest.VPC_CARD_CVV, cvv);

		String resQS = null;
		Integer paymentBrokerRefNo;

		// Sends the payment request and gets the response
		try {
			Object[] responses = doPost(pnr, creditCardPayment.getTemporyPaymentId(), ipgURLPay, creditCardInfoMap, true,
					cardDetailConfigData);
			resQS = (String) responses[0];
			paymentBrokerRefNo = (Integer) responses[1];

		} catch (UnsupportedEncodingException uee) {
			log.error(uee);
			throw new ModuleException("paymentbroker.error.unsupportedencoding");
		} catch (IOException ioe) {
			log.error(ioe);
			throw new ModuleException("paymentbroker.error.ioException");
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		// create a hash map for the response data
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		Map responseFields = MIGSPaymentUtils.createMapFromResponse(resQS);
		MIGS3PResponse migsResp = new MIGS3PResponse();
		migsResp.setReponse(responseFields);

		if (TRANSACTION_RESULT_SUCCESS.equals(migsResp.getTxnResponseCode())) {
			errorCode = "";
			transactionMsg = migsResp.getMessage();
			status = IPGResponseDTO.STATUS_ACCEPTED;
			txnResultCode = 1;
			errorSpecification = "";
			serviceResponse.setSuccess(true);
		} else {
			/* errorCode = getMatchingTxnErrorCode(migsResp.getTxnResponseCode()); */
			errorCode = PaymentResponseCodesUtil.getFilteredErrorCodeMOTO(migsResp);
			transactionMsg = PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode());
			status = IPGResponseDTO.STATUS_REJECTED;
			txnResultCode = 0;
			errorSpecification = status + " " + PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode())
					+ " " + migsResp.getMessage();
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), migsResp.toString(), errorSpecification,
				migsResp.getAuthorizeID(), migsResp.getTransactionNo(), txnResultCode);

		// Populate service response object
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, migsResp.getAuthorizeID());
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

		return serviceResponse;
	}

	/**
	 * Performs reverse card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		ServiceResponce sr = null;
		boolean refundRequired = false;

		if (isVoidBeforeRefund()) {
			sr = voidPurchase(creditCardPayment, pnr, appIndicator, tnxMode);
			if (sr.isSuccess()) {
				return sr;
			} else {
				refundRequired = true;
			}
		} else {
			refundRequired = true;
		}

		// refund replaces the reverse functionality
		if (refundRequired) {
			sr = refund(creditCardPayment, pnr, appIndicator, tnxMode);
			if (!sr.isSuccess()) {
				DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
				return defaultServiceResponse;
			}
		}

		return sr;
	}

	/**
	 * Creates the message to send to the MIGS server
	 * 
	 * @param ipgRequestDTO
	 *            IPG payment request
	 */
	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/**
	 * Reads the response returns from the MIGS server
	 * 
	 * @param encryptedReceiptPay
	 *            the secured response returns from the IPG
	 */
	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/**
	 * Returns Payment Gateway Name
	 */
	@Override
	public String getPaymentGatewayName() {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * This method is for performing a Form POST operation from input data parameters.
	 * 
	 * @param vpc_Host
	 *            - is a String containing the vpc URL
	 * @param dataFields
	 *            - is a String containing the post data key value pairs
	 * @return - is body data of the POST data response
	 */
	private Object[] doPost(String pnr, int temporyPaymentId, String vpc_Host, Map dataFields, boolean isCharge,
			List<CardDetailConfigDTO> cardDetailConfigData) throws IOException, ModuleException, UnsupportedEncodingException {

		InputStream is;
		OutputStream os;
		int vpc_Port = 443;
		String fileName = "";
		boolean useSSL = false;
		StringBuffer postData = new StringBuffer();
		StringBuffer dummyData = new StringBuffer();
		MIGS3PRequest migsReq = new MIGS3PRequest();
		migsReq.appendQueryFields(postData, dummyData, dataFields, cardDetailConfigData);

		// determine if SSL encryption is being used
		if (vpc_Host.substring(0, 8).equalsIgnoreCase("HTTPS://")) {
			useSSL = true;
			// remove 'HTTPS://' from host URL
			vpc_Host = vpc_Host.substring(8);
			// get the filename from the last section of vpc_URL
			fileName = vpc_Host.substring(vpc_Host.lastIndexOf("/"));
			// get the IP address of the VPC machine
			vpc_Host = vpc_Host.substring(0, vpc_Host.lastIndexOf("/"));
		}

		// use the next block of code if using a proxy server
		if (useProxy) {
			Socket s = new Socket(proxyHost, Integer.parseInt(proxyPort));
			os = s.getOutputStream();
			is = s.getInputStream();
			// use next block of code if using SSL encryption
			if (useSSL) {
				String msg = "CONNECT " + vpc_Host + ":" + vpc_Port + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n\r\n";
				os.write(msg.getBytes());
				byte[] buf = new byte[4096];
				int len = is.read(buf);
				String res = new String(buf, 0, len);

				// check if a successful HTTP connection
				if (res.indexOf("200") < 0) {
					throw new IOException("Proxy would now allow connection - " + res);
				}

				// write output to VPC
				SSLSocket ssl = (SSLSocket) s_sslSocketFactory.createSocket(s, vpc_Host, vpc_Port, true);
				ssl.startHandshake();
				os = ssl.getOutputStream();
				// get response data from VPC
				is = ssl.getInputStream();
				// use the next block of code if NOT using SSL encryption
			} else {
				fileName = vpc_Host;
			}
			// use the next block of code if NOT using a proxy server
		} else {
			// use next block of code if using SSL encryption
			if (useSSL) {
				Socket s = s_sslSocketFactory.createSocket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
				// use next block of code if NOT using SSL encryption
			} else {
				Socket s = new Socket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
			}
		}

		String req = "POST " + fileName + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n"
				+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: " + postData.toString().length()
				+ "\r\n\r\n" + postData.toString();

		String serviceType;

		if (isCharge) {
			serviceType = bundle.getString("SERVICETYPE_AUTHORIZE");
		} else {
			serviceType = bundle.getString("SERVICETYPE_REFUND");
		}

		CreditCardTransaction ccTransaction = auditTransaction(pnr, getMerchantId(),
				(String) dataFields.get(MIGS3PRequest.VPC_MERCH_TXN_REF), new Integer(temporyPaymentId), serviceType,
				dummyData.toString(), "", getPaymentGatewayName(), null, false);

		log.debug("******** IPG REFUND/CHARGE REQUEST URL : " + dummyData.toString());

		os.write(req.getBytes());
		String res = new String(MIGSPaymentUtils.readAll(is));

		log.debug("Immediate refund response file : " + res);

		// check if a successful connection
		if (res.indexOf("200") < 0) {
			throw new IOException("Connection Refused - " + res);
		}

		if (res.indexOf("404 Not Found") > 0) {
			throw new IOException("File Not Found Error - " + res);
		}

		int resIndex = res.indexOf("\r\n\r\n");
		String body = res.substring(resIndex + 4, res.length());

		log.debug("Response file body : " + body);
		return new Object[] { body, new Integer(ccTransaction.getTransactionRefNo()) };
	}

	@SuppressWarnings("unchecked")
	public ServiceResponce capture(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		String merchTnxRef;

		if (creditCardPayment.getPreviousCCPayment() != null) {

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
					PaymentConstants.MODE_OF_SERVICE.CAPTURE);
			String amount = MIGSPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());

			String vpc_Txn_No = MIGSPaymentUtils.getVPCTnxNo(ccTransaction, creditCardPayment.getPreviousCCPayment());

			Map fields = new HashMap();
			fields.put(MIGS3PRequest.VPC_VERSION, getVersion());
			fields.put(MIGS3PRequest.VPC_COMMAND, CAPTURE_COMMAND);
			fields.put(MIGS3PRequest.VPC_ACC_CODE, getMerchantAccessCode());
			fields.put(MIGS3PRequest.VPC_MERCH_TXN_REF, merchTnxRef);
			fields.put(MIGS3PRequest.VPC_MERCHANT, getMerchantId());
			fields.put(MIGS3PRequest.VPC_TXN_NO, vpc_Txn_No);
			fields.put(MIGS3PRequest.VPC_AMOUNT, amount);
			// fields.put(MIGS3PRequest.VPC_ORDER_INFO, MIGSPaymentUtils.getOrderInfo(creditCardPayment.getPnr(),
			// merchTnxRef));
			fields.put(MIGS3PRequest.VPC_USER, getUser());
			fields.put(MIGS3PRequest.VPC_PASSWORD, getPassword());

			PaymentBrokerMIGS3Capture oPaymentBroakerMIGS3Capture = new PaymentBrokerMIGS3Capture();
			ServiceResponce sr = oPaymentBroakerMIGS3Capture.capture(creditCardPayment, pnr, appIndicator, tnxMode, fields,
					getIpgURL(), getMerchantId());
			return sr;

		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_CAPTURE);
			return sr;
		}

	}

	/**
	 * Performs Capture card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		ServiceResponce sr = capture(creditCardPayment, pnr, appIndicator, tnxMode);

		if (!sr.isSuccess()) {
			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.CAPTURE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}

		return sr;
	}

	/**
	 * Resolves partial payments [ Invokes via a scheduler operation ]
	 */
	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "paymentbroker.generic.operation.notsupported");
		return sr;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_STATUS_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
