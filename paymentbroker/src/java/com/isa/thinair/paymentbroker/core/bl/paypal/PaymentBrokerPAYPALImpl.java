package com.isa.thinair.paymentbroker.core.bl.paypal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PAYPALRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.ProfileDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerPAYPALTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.paypal.soap.api.DoDirectPaymentRequestType;
import com.paypal.soap.api.DoDirectPaymentResponseType;
import com.paypal.soap.api.DoExpressCheckoutPaymentRequestType;
import com.paypal.soap.api.DoExpressCheckoutPaymentResponseType;
import com.paypal.soap.api.ErrorType;
import com.paypal.soap.api.GetExpressCheckoutDetailsRequestType;
import com.paypal.soap.api.GetExpressCheckoutDetailsResponseType;
import com.paypal.soap.api.GetTransactionDetailsRequestType;
import com.paypal.soap.api.GetTransactionDetailsResponseType;
import com.paypal.soap.api.RefundTransactionRequestType;
import com.paypal.soap.api.RefundTransactionResponseType;
import com.paypal.soap.api.SetExpressCheckoutRequestType;
import com.paypal.soap.api.SetExpressCheckoutResponseType;

public class PaymentBrokerPAYPALImpl extends PaymentBrokerPAYPALTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerPAYPALImpl.class);

	private PAYPALRequestDTO paypalRequestDTO = new PAYPALRequestDTO();

	private ProfileDTO profileDTO = new ProfileDTO();

	private UserInputDTO userInputDTO = new UserInputDTO();

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	private String DO_DIRECT_PAYMENT = "DoDirectPayment";

	private String SET_EXPRESS_CHECKOUT = "SetExpressCheckout";

	private String GET_EXPRESS_CHECKOUT = "GetExpressCheckout";

	private String DO_EXPRESS_CHECKOUT = "DoExpressCheckout";

	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	public void setProfileData() {
		Properties ipgProps = getProperties();
		profileDTO.setMode(ipgProps.getProperty(PAYPALPaymentUtils.mode));
		profileDTO.setApiUsername(ipgProps.getProperty(PAYPALPaymentUtils.apiUsername));
		profileDTO.setApiPassword(ipgProps.getProperty(PAYPALPaymentUtils.apiPassword));
		profileDTO.setSignature(ipgProps.getProperty(PAYPALPaymentUtils.signature));
		profileDTO.setEnvironment(ipgProps.getProperty(PAYPALPaymentUtils.environment));
		profileDTO.setCertificateFilePath(ipgProps.getProperty(PAYPALPaymentUtils.certificateFilePath));
		profileDTO.setPrivateKeyPassword(ipgProps.getProperty(PAYPALPaymentUtils.privateKeyPassword));
		profileDTO.setUseProxy(ipgProps.getProperty(PAYPALPaymentUtils.useProxy));
		profileDTO.setProxyHost(ipgProps.getProperty(PAYPALPaymentUtils.proxyHost));
		profileDTO.setProxyPort(ipgProps.getProperty(PAYPALPaymentUtils.proxyPort));
		profileDTO.setRedirectUrl(ipgProps.getProperty(PAYPALPaymentUtils.redirectUrl));
		profileDTO.setCancelUrl(ipgProps.getProperty(PAYPALPaymentUtils.cancelUrl));
		profileDTO.setNote(ipgProps.getProperty(PAYPALPaymentUtils.note));
		profileDTO.setOrderDescription(ipgProps.getProperty(PAYPALPaymentUtils.orderDescription));

		paypalRequestDTO.setProfileDTO(profileDTO);
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		String status;
		String errorCode;
		Integer paymentBrokerRefNo;
		int txnResultCode;
		String errorSpecification;

		CreditCardTransaction cccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
		setProfileData();
		userInputDTO.setTransactionId(cccTransaction.getTransactionId());

		userInputDTO.setRefundType(PAYPALPaymentUtils.PARTIAL_REFUND);

		String amount = creditCardPayment.getAmount();

		userInputDTO.setAmount(amount);

		paypalRequestDTO.setUserInputDTO(userInputDTO);

		// create a hash map for the response data
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		// Get a merchant transaction id to identify txn in the payment server, in case of failure.
		String merchantTxnId = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
				PaymentConstants.MODE_OF_SERVICE.REFUND);

		WSClientBD paypalWebervices = ReservationModuleUtils.getWSClientBD();

		RefundTransactionRequestType refundRequest = paypalWebervices.prepareRefund(paypalRequestDTO);
		CreditCardTransaction ccTransaction = auditTransaction(pnr, "", merchantTxnId,
				new Integer(creditCardPayment.getTemporyPaymentId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				XMLStreamer.compose(refundRequest), "", creditCardPayment.getIpgIdentificationParamsDTO()
				.getFQIPGConfigurationName(), null, false);

		paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());

		RefundTransactionResponseType refundResponse = paypalWebervices.doRefundWebServiceCall(paypalRequestDTO, refundRequest);

		if (refundResponse.getAck().getValue().toString().equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS)) {
			status = PAYPALPaymentUtils.ACCEPTED;
			txnResultCode = 1;
			errorSpecification = "";
			errorCode = "";
			serviceResponse.setSuccess(true);
		} else {
			status = PAYPALPaymentUtils.REJECTED;
			txnResultCode = 0;
			ErrorType[] errorArry = refundResponse.getErrors();
			errorCode = errorArry[0].getErrorCode().toString();
			errorSpecification = status + " " + PAYPALPaymentUtils.getFilteredErrorCode(errorCode) + " " + "";
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), XMLStreamer.compose(refundResponse), errorSpecification,
				"", refundResponse.getRefundTransactionID(), txnResultCode);

		// Populate service response object
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, "");
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE,
				PAYPALPaymentUtils.getFilteredErrorCode(errorCode));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "");

		return serviceResponse;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		ServiceResponce sr = null;
		sr = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		if (!sr.isSuccess()) {
			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}
		return sr;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		return getRequestData(ipgRequestDTO, null);
	}

	/**
	 * Creates the message to send to the PAYPAL server
	 * 
	 * @param ipgRequestDTO
	 *            IPG payment request
	 */
	@SuppressWarnings("unchecked")
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		String requestData = null;
		CreditCardTransaction ccTransaction = null;
		String merchantTxnId = null;
		String transactionId = null;
		int tnxResultCode = 0;
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		userInputDTO = ipgRequestDTO.getUserInputDTO();
		setProfileData();
		paypalRequestDTO.setUserInputDTO(userInputDTO);

		WSClientBD paypalWebervices = ReservationModuleUtils.getWSClientBD();

		if (ipgRequestDTO.isIntExtPaymentGateway()) {
			DoDirectPaymentRequestType directPaymentRequest = paypalWebervices
					.prepareDoDirectPaymentRequestType(paypalRequestDTO);
			requestData = PaymentBrokerUtils.compressMessage(XMLStreamer.compose(directPaymentRequest));

			merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
					ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

			String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
			ccTransaction = PaymentBrokerUtils.getPaymentBrokerBD().auditTransaction(ipgRequestDTO.getPnr(), null, merchantTxnId,
					new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
					requestData + "," + sessionID, "", ipgRequestDTO.getIpgIdentificationParamsDTO().getFQIPGConfigurationName(),
					null, false);

			DoDirectPaymentResponseType directPaymentResponse = paypalWebervices.doDirectPaymentWebServiceCall(paypalRequestDTO,
					directPaymentRequest);
			if (directPaymentResponse.getTransactionID() != null) {
				transactionId = directPaymentResponse.getTransactionID();
			}
			if (directPaymentResponse.getAck().getValue().toString().equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS)) {
				tnxResultCode = 1;
			} else {
				tnxResultCode = 0;
			}

			PaymentBrokerUtils.getPaymentBrokerBD().saveResponse(DO_DIRECT_PAYMENT, ccTransaction.getTransactionRefNo(),
					PaymentBrokerUtils.compressMessage(XMLStreamer.compose(directPaymentResponse)), transactionId, tnxResultCode);

			ipgRequestResultsDTO.setDirectPaymentResponse(directPaymentResponse);
			ipgRequestResultsDTO.setRequestData(requestData);
		} else {
			SetExpressCheckoutRequestType setExpressCheckoutRequest = paypalWebervices
					.prepareSetExpressCheckoutRequestType(paypalRequestDTO);
			requestData = PaymentBrokerUtils.compressMessage(XMLStreamer.compose(setExpressCheckoutRequest));

			merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
					ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

			String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

			ccTransaction = PaymentBrokerUtils.getPaymentBrokerBD().auditTransaction(ipgRequestDTO.getPnr(), null, merchantTxnId,
					new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
					requestData + "," + sessionID, "", ipgRequestDTO.getIpgIdentificationParamsDTO().getFQIPGConfigurationName(),
					null, false);

			SetExpressCheckoutResponseType setExpressCheckoutResponse = paypalWebervices.doSetExpressCheckoutWebServiceCall(
					paypalRequestDTO, setExpressCheckoutRequest);

			PaymentBrokerUtils.getPaymentBrokerBD().saveResponse(SET_EXPRESS_CHECKOUT, ccTransaction.getTransactionRefNo(),
					PaymentBrokerUtils.compressMessage(XMLStreamer.compose(setExpressCheckoutResponse)), "", 0);

			ipgRequestResultsDTO.setSetExpressCheckoutResponse(setExpressCheckoutResponse);
			ipgRequestResultsDTO.setRequestData(createPPExpressCheckoutRedirectUrl(setExpressCheckoutResponse));
		}

		log.info("Save requested message");
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		log.info("Set requested data");

		return ipgRequestResultsDTO;
	}

	public String createPPExpressCheckoutRedirectUrl(Object responseValue) {
		String ppExpressCheckoutPaymentPageUrl;
		SetExpressCheckoutResponseType ppresponse = (SetExpressCheckoutResponseType) responseValue;
		ppExpressCheckoutPaymentPageUrl = profileDTO.getRedirectUrl() + ppresponse.getToken();
		return ppExpressCheckoutPaymentPageUrl;
	}

	/**
	 * Reads the response returns from the PAYPAL server
	 * 
	 * @param encryptedReceiptPay
	 *            the secured response returns from the IPG
	 */
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		String errorCode = null;
		String status = null;
		String message = ""; // paypal reponse some message TODO
		String authorizeID = null;// TODO
		int tnxResultCode = 1;// accepted:1 , rejected:0
		String merchantTxnReference = ""; // depend on the response
		String trimedDigits = null; // last 4 digits
		String transactionId = null;
		userInputDTO = ipgResponseDTO.getUserInputDTO();
		setProfileData();
		paypalRequestDTO.setUserInputDTO(userInputDTO);
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerBD().saveTransaction(oCreditCardTransaction);

		if (ipgResponseDTO.getBrokerType().equalsIgnoreCase(PAYPALPaymentUtils.EXTERNAL)) {
			// second request
			WSClientBD paypalWebervices = ReservationModuleUtils.getWSClientBD();
			GetExpressCheckoutDetailsRequestType getExpressCheckoutRequest = paypalWebervices
					.prepareGetExpressCheckoutDetailsRequestType(paypalRequestDTO);
			PaymentBrokerUtils.getPaymentBrokerBD().saveRequest(GET_EXPRESS_CHECKOUT, ipgResponseDTO.getPaymentBrokerRefNo(),
					PaymentBrokerUtils.compressMessage(XMLStreamer.compose(getExpressCheckoutRequest)));
			GetExpressCheckoutDetailsResponseType getExpressCheckoutResponse = paypalWebervices
					.doGetExpressCheckoutWebServiceCall(paypalRequestDTO, getExpressCheckoutRequest);
			PaymentBrokerUtils.getPaymentBrokerBD().saveResponse(GET_EXPRESS_CHECKOUT, ipgResponseDTO.getPaymentBrokerRefNo(),
					PaymentBrokerUtils.compressMessage(XMLStreamer.compose(getExpressCheckoutResponse)), null, 0);

			// third request
			DoExpressCheckoutPaymentRequestType doExpressCheckoutRequest = paypalWebervices
					.prepareDoExpressCheckoutPaymentRequestType(getExpressCheckoutResponse
							.getGetExpressCheckoutDetailsResponseDetails());
			PaymentBrokerUtils.getPaymentBrokerBD().saveRequest(DO_EXPRESS_CHECKOUT, ipgResponseDTO.getPaymentBrokerRefNo(),
					PaymentBrokerUtils.compressMessage(XMLStreamer.compose(doExpressCheckoutRequest)));
			DoExpressCheckoutPaymentResponseType doExpressCheckoutResponse = paypalWebervices.doDoExpressCheckoutWebServiceCall(
					paypalRequestDTO, doExpressCheckoutRequest);
			if (doExpressCheckoutResponse.getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo(0).getTransactionID() != null) {
				transactionId = doExpressCheckoutResponse.getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo(0)
						.getTransactionID();
			}

			if (doExpressCheckoutResponse.getAck().getValue().toString().equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS)) {
				status = PAYPALPaymentUtils.ACCEPTED;
				tnxResultCode = 1;
			} else {
				status = PAYPALPaymentUtils.REJECTED;
				tnxResultCode = 0;
				ErrorType[] errorArry = ipgResponseDTO.getPaypalResponse().getDoDirectPaymentResponseType().getErrors();
				errorCode = errorArry[0].getErrorCode().toString();
			}
			PaymentBrokerUtils.getPaymentBrokerBD().saveResponse(DO_EXPRESS_CHECKOUT, ipgResponseDTO.getPaymentBrokerRefNo(),
					PaymentBrokerUtils.compressMessage(XMLStreamer.compose(doExpressCheckoutResponse)), transactionId,
					tnxResultCode);

			ipgResponseDTO.setCardType(Integer.parseInt(ipgResponseDTO.getPaypalResponse().getCreditCardType()));
		} else if (ipgResponseDTO.getBrokerType().equalsIgnoreCase(PAYPALPaymentUtils.INTERNAL_EXTERNAL)) {
			if (ipgResponseDTO.getPaypalResponse().getDoDirectPaymentResponseType().getAck().getValue().toString()
					.equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS)) {
				status = PAYPALPaymentUtils.ACCEPTED;
			} else {
				status = PAYPALPaymentUtils.REJECTED;
				ErrorType[] errorArry = ipgResponseDTO.getPaypalResponse().getDoDirectPaymentResponseType().getErrors();
				errorCode = errorArry[0].getErrorCode().toString();
			}
			trimedDigits = userInputDTO.getCVV2();
			ipgResponseDTO.setCardType(Integer.parseInt(ipgResponseDTO.getPaypalResponse().getCreditCardType()));
			ipgResponseDTO.setCcLast4Digits(ipgResponseDTO.getPaypalResponse().getCVV2());
		}
		ipgResponseDTO.setCcLast4Digits(trimedDigits);
		ipgResponseDTO.setStatus(status);// used in isSuccess method and when
		ipgResponseDTO.setErrorCode(PAYPALPaymentUtils.getFilteredErrorCode(errorCode));// used when transaction has
																						// been failed and by one PB
		ipgResponseDTO.setMessage(message);// used by one PB TODO
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);// only used in validateTransaction
		ipgResponseDTO.setAuthorizationCode(authorizeID);// TODO

		return ipgResponseDTO;
	}

	/**
	 * Updates the Audit Transaction
	 * 
	 * @param transactionRefNo
	 * @return
	 */
	protected CreditCardTransaction loadAuditTransactionByTnxRefNo(int transactionRefNo) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(transactionRefNo);
		return creditCardTransaction;
	}

	/**
	 * Updates the Audit Transaction
	 * 
	 * @param transactionRefNo
	 * @param plainTextReceipt
	 * @param errorSpecification
	 * @param authorizationCode
	 * @param transactionId
	 * @param tnxResultCode
	 */
	protected void updateAuditTransactionByTnxRefNo(int transactionRefNo, String plainTextReceipt, String errorSpecification,
			String authorizationCode, String transactionId, int tnxResultCode) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(transactionRefNo);
		creditCardTransaction.setResponseText(plainTextReceipt);
		creditCardTransaction.setErrorSpecification(errorSpecification);
		creditCardTransaction.setAidCccompnay(authorizationCode);
		creditCardTransaction.setResultCode(0);
		creditCardTransaction.setTransactionResultCode(tnxResultCode);
		creditCardTransaction.setTransactionId(transactionId);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(creditCardTransaction);
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		String status;
		String errorCode;
		Integer paymentBrokerRefNo;
		int txnResultCode;
		String errorSpecification;
		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();
		Collection oResultIF = new ArrayList();
		Collection oResultIP = new ArrayList();
		Collection oResultII = new ArrayList();
		Collection oResultIS = new ArrayList();
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		setProfileData();

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();
			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			try {
				userInputDTO.setTransactionId(oCreditCardTransaction.getTransactionId());
				paypalRequestDTO.setUserInputDTO(userInputDTO);
				WSClientBD paypalWebervices = ReservationModuleUtils.getWSClientBD();
				GetTransactionDetailsRequestType pprequest = paypalWebervices
						.prepareDoGetTransactionDetailsCode(paypalRequestDTO);
				String merchantTxnId = composeMerchantTransactionId(ipgQueryDTO.getAppIndicatorEnum(),
						oCreditCardTransaction.getTemporyPaymentId(), PaymentConstants.MODE_OF_SERVICE.QUERYDR);
				// CreditCardTransaction ccTransaction =
				// auditTransaction(oCreditCardTransaction.getTransactionReference(), "",
				// merchantTxnId, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				// bundle.getString("SERVICETYPE_AUTHORIZE"), XMLStreamer.compose(pprequest), "",
				// PAYPALPaymentUtils.paypal,
				// null, false);
				GetTransactionDetailsResponseType serviceResponce = paypalWebervices.doGetTransactionDetailsCode(pprequest,
						paypalRequestDTO);

				if (serviceResponce.getAck().getValue().toString().equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS)) {
					status = PAYPALPaymentUtils.ACCEPTED;
					txnResultCode = 1;
					errorSpecification = "";
					errorCode = "";
				} else {
					status = PAYPALPaymentUtils.REJECTED;
					txnResultCode = 0;
					ErrorType[] errorArry = serviceResponce.getErrors();
					errorCode = errorArry[0].getErrorCode().toString();
					errorSpecification = status + " " + PAYPALPaymentUtils.getFilteredErrorCode(errorCode) + " " + "";
				}
				// paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());
				// updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), XMLStreamer.compose(serviceResponce),
				// errorSpecification, "", serviceResponce.getCorrelationID(), txnResultCode);

				// If successful payment remains at Bank
				if (serviceResponce.getPaymentTransactionDetails().getPaymentInfo().getPaymentStatus().getValue().toString()
						.equalsIgnoreCase(PAYPALPaymentUtils.COMPLETED)) {
					if (refundFlag) {
						// Do refund

						oPrevCCPayment = new PreviousCreditCardPayment();
						oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());

						oCCPayment = new CreditCardPayment();
						oCCPayment.setPreviousCCPayment(oPrevCCPayment);
						oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
						oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
						oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
						oCCPayment.setTnxMode(TnxModeEnum.SECURE_3D);
						oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
						oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

						ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
								oCCPayment.getTnxMode());

						if (srRev.isSuccess()) {
							// Change State to 'IS'
							oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moing to IS:" + oCreditCardTransaction.getTemporyPaymentId());
						} else {
							// Change State to 'IF'
							oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moing to IF:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					} else {
						// Change State to 'IP'
						oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moing to IP:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}
			} catch (ModuleException e) {
				log.info("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);
		sr.setSuccess(true);

		return sr;
	}

	@Override
	public void setIPGIdentificationParams(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getRefundWithoutCardDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
