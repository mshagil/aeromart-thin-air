package com.isa.thinair.paymentbroker.core.bl.knet;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.aciworldwide.commerce.gateway.plugins.e24PaymentPipe;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.bl.knet.KnetResponse.RESPONSETYPE;
import com.isa.thinair.paymentbroker.core.bl.knet.KnetResponse.RESULTSTATUS;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;

/**
 * 
 * @author Pradeep Karunanayake
 *
 */
public class PaymentBrokerKnetECommerceImpl extends PaymentBrokerKnetTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerKnetECommerceImpl.class);
	
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO,
			List<CardDetailConfigDTO> configDataList) throws ModuleException {
		
		if (log.isDebugEnabled())
			log.debug("###Start to process the request..: ");
		String responseURL;
		String finalAmount   = ipgRequestDTO.getAmount();
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		
		e24PaymentPipe pipe = new e24PaymentPipe();		
        pipe.setAction(String.valueOf(KnetRequest.ACTION.PURCHASE.getAction()));
        if (!KnetRequest.CURRENCY.equalsIgnoreCase(ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode())){
        	log.error("Payment gateway doesn't support the currency");
        	throw new ModuleException("module.invalid.payment.currency");
        }
        pipe.setCurrency(KnetRequest.CURRENCY_CODE_KWD);      
        if(ipgRequestDTO.isServiceAppFlow()){
        	responseURL = KnetRequest.SERVICE_APP_RESPONSE_URL;
        } else {
        	responseURL = KnetRequest.RESPONSE_URL;
        }
        pipe.setResponseURL(responseURL);
        pipe.setErrorURL(responseURL);
        pipe.setAmt(finalAmount);
        pipe.setResourcePath(getSecureKeyLocation());
        pipe.setAlias(getMerchantId()); 
        pipe.setLanguage(KnetUtil.getLanguageCode(ipgRequestDTO.getSessionLanguageCode()));
        pipe.setTrackId(merchantTxnId);
        KnetUtil.addAdditionalData(ipgRequestDTO, pipe);
        pipe.setUdf5(ipgRequestDTO.getPnr());        
        
        String paymentRequestString = KnetUtil.concatPaymentRequestString(ipgRequestDTO, pipe);
        CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				paymentRequestString, "", getPaymentGatewayName(), null, false);       
        // We set this parameters to payment response validate
        int paymentBrokerRefNo = ccTransaction.getTransactionRefNo();
        pipe.setUdf3(ipgRequestDTO.getIpgIdentificationParamsDTO().getIpgId() + "_" + ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
        pipe.setUdf4(String.valueOf(paymentBrokerRefNo));
        // send the Payment Initialization message
        if (log.isDebugEnabled())
        	log.debug("try to generate paymentID -PNR is " + ipgRequestDTO.getPnr());
        try {
	        if(pipe.performPaymentInitialization() != pipe.SUCCESS) {
	            log.error("Error sending Payment Initialization Request: " + pipe.getErrorMsg());                  
	            throw new ModuleException("paymentbroker.gateway.not.respond");
	        }
        } catch(Exception ex) {
        	log.error("Error sending Payment Initialization Request: ", ex);   
        	throw new ModuleException("paymentbroker.gateway.not.respond");
        }  
        if (log.isDebugEnabled())
        	log.debug("After Gererated payment ID - PNR is " + ipgRequestDTO.getPnr());
        // Get results        
        String payID = pipe.getPaymentId();
        String payURL = pipe.getPaymentPage();        
        paymentRequestString = KnetUtil.concatPaymentRequestString(ipgRequestDTO, pipe);
        if (log.isDebugEnabled())
        	log.debug(paymentRequestString);  
        
        updateAuditTransactionByTnxRefNo(paymentBrokerRefNo, paymentRequestString , payID);    
        
        IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(payURL + "?PaymentID=" + payID );
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);	
		
		if (log.isDebugEnabled())
			log.debug("End of request data process..");
		return ipgRequestResultsDTO;
	}	

	@SuppressWarnings("unchecked")	
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		if (log.isDebugEnabled()) 
			log.debug("Knet response start:" + ipgResponseDTO.getTemporyPaymentId());		

		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		String merchantTxnReference;
		String authorizeID;
		int cardType = 5; // Generic Card type		
		
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		KnetResponse knetResp = new KnetResponse();
		knetResp.setResponse(receiptyMap);
		
		if (log.isDebugEnabled())
			log.debug("Knet Response Mid Response :" + knetResp);
		
		if (oCreditCardTransaction == null || !oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(knetResp.getTrackid()) || 
				(knetResp.getPaymentId() == null || knetResp.getPaymentId().isEmpty()) || !oCreditCardTransaction.getGatewayPaymentID().equals(knetResp.getPaymentId())) {
			errorExists = true;
		}	

		merchantTxnReference = knetResp.getTrackid();
		authorizeID = "Knet Ref:" + knetResp.getRef();	
		if (authorizeID.length() >= 30) {
			authorizeID = authorizeID.substring(0, 30);
		}		
		// Validate response
		// Knet is sending intermediate response with out redirecting browser. So we are keeping this response to validate data(KnetNotifyPaymentResponseHandlerAction)
		// We are receiving final response via (HandleInterlineIPGResponseAction)
		// both response should be same
		boolean isValidResponse = true;
		if (!errorExists && knetResp.getResponseType() == RESPONSETYPE.DIRECT && 
				RESULTSTATUS.CAPTURED == knetResp.getResult() &&
				oCreditCardTransaction.getTransactionResultCode() != 1) {
			isValidResponse = false;
			if (log.isInfoEnabled())
				log.info("Fatal error.User try to changed the payment response data or invalid response from gateway.."+ knetResp.getUdf5());
			throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
		}
		
		if (RESULTSTATUS.CAPTURED == knetResp.getResult() && !errorExists && isValidResponse) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "";
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = KnetUtil.getErrorCodeWithPrefix(knetResp.getResult());
			errorSpecification = KnetUtil.getErrorCodeDescription(knetResp.getResult());
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(errorSpecification);
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authorizeID);
		ipgResponseDTO.setCardType(cardType);		
		ipgResponseDTO.setCcLast4Digits("");
		// Add Transaction Details
		IPGTransactionResultDTO ipgTransactionResultDTO = new IPGTransactionResultDTO();
		// TODO move to configuration or db field
		boolean showTransactionDetail =  true;
		if (RESULTSTATUS.CAPTURED == knetResp.getResult())  {
			ipgTransactionResultDTO.setTransactionStatus(IPGResponseDTO.STATUS_ACCEPTED);
		} else {
			ipgTransactionResultDTO.setTransactionStatus(IPGResponseDTO.STATUS_REJECTED);
		}
		ipgTransactionResultDTO.setTransactionTime(CalendarUtil.formatDate(ipgResponseDTO.getRequestTimsStamp(), CalendarUtil.PATTERN5));
		ipgTransactionResultDTO.setShowTransactionDetail(showTransactionDetail);		
		ipgTransactionResultDTO.setMerchantTrackID(merchantTxnReference + "/" + oCreditCardTransaction.getTransactionReference());
		ipgTransactionResultDTO.setReferenceID(knetResp.getRef());
		ipgResponseDTO.setIpgTransactionResultDTO(ipgTransactionResultDTO);

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), knetResp.toString(), errorSpecification,
				authorizeID, knetResp.getTranid(), tnxResultCode);
		if (log.isDebugEnabled())
			log.debug("[Knet Response::getReponseData()] End -" + knetResp.getTrackid() + ":"+knetResp.getUdf5());

		return ipgResponseDTO;
	}

}
