package com.isa.thinair.paymentbroker.core.bl.cybersource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CyberSourcePaymentUtils {

	private static final Log log = LogFactory.getLog(CyberSourcePaymentUtils.class);

	/**
	 * Returns the formatted amount
	 * 
	 * @param amount
	 * @return
	 */
	public static String getFormattedAmount(String amount, int noOfDecimalPoints) {
		// if (SystemPropertyUtil.checkRemoveCardPaymentDecimals()) {
		//
		// // FIXME Review for refund request fail in IBE for decimal places
		// // Old code - amount = Long.toString(Math.round(Double.parseDouble(amount)));
		// BigDecimal amt = new BigDecimal(amount);
		// amt = amt.setScale(0, BigDecimal.ROUND_UP);
		// amount = amt.toString();
		// }
		//
		// BigDecimal value = AccelAeroCalculator.multiply(new BigDecimal(amount),
		// AccelAeroCalculator.parseBigDecimal(Math.pow(10, noOfDecimalPoints)));
		return amount;
	}

	/**
	 * Returns the order information
	 * 
	 * @param pnr
	 * @param merchantTxnId
	 * @param carrierCode
	 *            TODO
	 * @return
	 */
	public static String getOrderInfo(String pnr, String merchantTxnId, String carrierCode, String userIP) {

		if (userIP == null) {
			userIP = "";
		} else {
			userIP = "-" + userIP;
		}

		return carrierCode + pnr + merchantTxnId + userIP;
	}

	public static String getResponseStatusDescription(int input) {
		String result = "";
		try {
			// refer pg:94 HOP_UG

			switch (input) {

			case 100:
				result = "Successful transaction.";
				break;
			case 102:
				result = "One or more fields in the request are missing or invalid.";
				break;
			case 110:
				result = "Authorization was partially approved.";
				break;
			case 150:
				result = "Error: General system failure.";
				break;
			case 151:
				result = "Error: The request was received, but a server time-out occurred.";
				break;
			case 152:
				result = "Error: The request was received, but a service did not finish running in time.";
				break;
			case 200:
				result = "The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the Address Verification Service (AVS) check.";
				break;
			case 201:
				result = "The issuing bank has questions about the request.";
				break;
			case 202:
				result = "The card is expired.";
				break;
			case 203:
				result = "The card was declined. No other information was provided by the issuing bank.";
				break;
			case 204:
				result = "The account has insufficient funds";
				break;
			case 205:
				result = "The card was stolen or lost.";
				break;
			case 207:
				result = "The issuing bank was unavailable.";
				break;
			case 208:
				result = "The card is inactive or not authorized for card-not-present transactions.";
				break;
			case 210:
				result = "The credit limit for the card has been reached.";
				break;
			case 211:
				result = "The card verification number is invalid.";
				break;
			case 222:
				result = "The customer's bank account is frozen.";
				break;
				//TODO: 230, 231 Use as it is or since tracking is possible
			case 230:
				result = "The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the card verification number check";
				break;
			case 231:
				 result = "The account number is invalid. Request a different card or other form of payment.";
				 break;
			case 232:
				result = "The card type is not accepted by the payment processor.";
				break;
			case 234:
				result = "There is a problem with your CyberSource merchant configuration.";
				break;
			case 236:
				result = "A processor failure occurred.";
				break;
			case 240:
				result = "The card type sent is invalid or does not correlate with the credit card number.";
				break;
			case 250:
				result = "Error: The request was received, but a time-out occurred with the payment processor.";
				break;
			case 476:
				result = "The customer cannot be authenticated.";
				break;
			case 520:
				result = "The authorization request was approved by the issuing bank but declined by CyberSource based on your Smart Authorization settings.";
				break;

			default:
				result = "Authorization Declined.";
			}
		} catch (NumberFormatException e) {
			log.error(e);
			result = "Unable to be determined.";
		}
		return result;
	}

}
