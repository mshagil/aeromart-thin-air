package com.isa.thinair.paymentbroker.core.bl.payFort;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_HomeRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_HomeResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerPayFortPay_AT_HomeECommerceImpl extends PaymentBrokerPayFortTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerPayFortPay_AT_StoreECommerceImpl.class);

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected static final String VERIFICATION_HASH_ERROR = "payfort.payment.mismatch";
	protected static final String PAYFORT_RESPONSE_ERROR = "payfort.pay_at_home.payment.error";
	protected static final String DATE_FORMAT = "dd-MM-yyyy HH:mm";
	protected static final String USER_CANCELED = "payfort.payment.user.cancel";
	protected static final String BACK_TO_MERCHANT = "payfort.payment.user.backToMerchant";

	/**
	 * Send the request to the PayFort pay@home and get the initial response
	 * 
	 * @param IPGRequestDTO
	 *            the secured response returns from the IPG
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerPayFortPay_AT_HomeECommerceImpl::getRequestData()] Begin ");

		Pay_AT_HomeResponseDTO payResponse = null;
		boolean avoid = false;
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		String merchantTxnId = "";
		XMLGregorianCalendar expireDateCalander = null;
		XMLGregorianCalendar currentCalendar = null;
		String languageCode = DEFAULT_LANGUAGE;

		Long timeToSpare = null;
		String signatureSHA1 = "";
		Pay_AT_HomeRequestDTO requestDTO = new Pay_AT_HomeRequestDTO();
		// initializing data to be send for web service calls
		merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		// BigDecimal finalAmount = PayFortPaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount(), true);
		BigDecimal finalAmount = new BigDecimal(ipgRequestDTO.getAmount());
		Calendar calander = Calendar.getInstance();
		calander.setTime(CalendarUtil.getCurrentZuluDateTime());
		if (Integer.parseInt(getExpireTimeInMinutes()) > 0) {
			calander.add(Calendar.MINUTE, Integer.parseInt(getExpireTimeInMinutes()));
		} else {
			calander.add(Calendar.MINUTE, roundUpExpireDate(ipgRequestDTO.getExpireMTCOffLinePeriod(), 60));
		}
		String expireDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, calander.getTime());
		expireDateCalander = convertStringDateToXmlGregorianCalendar(expireDate, DATE_FORMAT, false);
		String currentDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, CalendarUtil.getCurrentZuluDateTime());
		currentCalendar = convertStringDateToXmlGregorianCalendar(currentDate, DATE_FORMAT, true);
		// TO-DO replace for country code
		String signature = finalAmount + ipgRequestDTO.getIpCountryCode() + "" + ipgRequestDTO.getContactFirstName() + " "
				+ ipgRequestDTO.getContactLastName() + ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode()
				+ expireDateCalander + "" + getItemPrefix() + ipgRequestDTO.getPnr() + "" + getMerchantId() + "" + merchantTxnId
				+ getServiceName() + ipgRequestDTO.getPnr() + getEncryptionKey();
		try {
			signatureSHA1 = SHA1(signature);
		} catch (NoSuchAlgorithmException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		} catch (UnsupportedEncodingException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		// setting initialized data into the DTO
		requestDTO.setAmount(finalAmount);
		requestDTO.setClientCountry(ipgRequestDTO.getIpCountryCode());
		requestDTO.setClientName(ipgRequestDTO.getContactFirstName() + " " + ipgRequestDTO.getContactLastName());
		requestDTO.setCurrency(ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		requestDTO.setExpiryDate(expireDateCalander);
		requestDTO.setItemName(getItemPrefix() + ipgRequestDTO.getPnr());
		requestDTO.setMerchantID(getMerchantId());
		requestDTO.setMsgDate(currentCalendar);
		requestDTO.setOrderID(merchantTxnId);
		requestDTO.setServiceName(getServiceName());
		requestDTO.setTicketNumber(ipgRequestDTO.getPnr());
		requestDTO.setSignature(signatureSHA1);

		// Remote web service call for payfort
		try {
			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			payResponse = ebiWebervices.payAtHomePaymentTransaction(requestDTO);

		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();

		if (payResponse != null) {
			if (payResponse.getStatus() != null && payResponse.getStatus().equals(PayFortPaymentUtils.SUCCESS)) {

				String checkSumSHA1 = "";
				String checkSum = payResponse.getOrderReference() + getEncryptionKey();
				try {
					checkSumSHA1 = SHA1(checkSum);
				} catch (NoSuchAlgorithmException e1) {
					log.error(e1);
					throw new ModuleException("paymentbroker.error.unknown");
				} catch (UnsupportedEncodingException e1) {
					log.error(e1);
					throw new ModuleException("paymentbroker.error.unknown");
				}

				// setting post data for interline postcard detail action
				postDataMap.put(payResponse.ORDER_REF, payResponse.getOrderReference());
				postDataMap.put(payResponse.CHECK_SUM, checkSumSHA1);
				postDataMap.put(payResponse.SERVICE_NAME, getServiceName());
				// TO-DO replace language
				postDataMap.put(payResponse.LANGUAGE, ipgRequestDTO.getSessionLanguageCode());
				// postDataMap.put(payResponse.LANGUAGE, languageCode);

				String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

				String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

				String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());

				if (ipgRequestDTO.getSessionLanguageCode() != null) {
					strRequestParams += ",sessionLanguage : " + ipgRequestDTO.getSessionLanguageCode();
				}

				CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
						new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
						strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

				if (log.isDebugEnabled()) {
					log.debug("PaymentBrokerPayFortPay_AT_StoreECommerceImpl PG Request Data : " + strRequestParams
							+ ", sessionID : " + sessionID);
				}

				ipgRequestResultsDTO.setRequestData(requestDataForm);
				ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());

			} else {
				ipgRequestResultsDTO.setRequestData("");
				ipgRequestResultsDTO.setPaymentBrokerRefNo(new Integer(0));
			}

		}

		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		log.debug("[PaymentBrokerPayFortPay_AT_HomeECommerceImpl::getRequestData()] End ");
		return ipgRequestResultsDTO;
	}

	/**
	 * Reads the response returns from the PayFort Pay@Home
	 * 
	 * @param encryptedReceiptPay
	 *            the secured response returns from the IPG
	 */
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerPayFortTemplate::getReponseData()] Begin ");

		int tnxResultCode = -1;
		String errorCode = "";
		String status = "";
		String errorSpecification = "";
		int cardType = 5; // generic card type
		String responseMismatch = "";
		String strRequestParams = "";
		XMLGregorianCalendar currentCalendar = null;
		CheckNotificationResponseDTO payFortNotifyingResponse = new CheckNotificationResponseDTO();
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		String orderId = "";

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(ipgResponseDTO.getTemporyPaymentId());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		if (fields.containsKey("status") && fields.get("status").equals("cancel")) {
			orderId = (String) fields.get(PayFortPaymentUtils.ORDER_ID);
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = USER_CANCELED;
			errorSpecification = "Cancel By The User";
			strRequestParams = PaymentBrokerUtils.getRequestDataAsString(fields);
			// Different amount in response
			log.error("[PaymentBrokerPayFortTemplate::getReponseData()] - User Cancel at the pay@home PGW page.");
		} else if (fields.containsKey("orderReference") && fields.containsKey("status") && fields.get("status").equals("success")) {

			orderId = (String) fields.get(PayFortPaymentUtils.ORDER_ID);
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = BACK_TO_MERCHANT;
			errorSpecification = "Back To Merchat By User";
			strRequestParams = PaymentBrokerUtils.getRequestDataAsString(fields);
			// Different amount in response
			log.error("[PaymentBrokerPayFortTemplate::getReponseData()] - click back to merchant by user from pay@home PGW page.");

		} else if (fields.containsKey("responseType") && fields.containsKey("status")
				&& fields.get("responseType").equals("PAYATHOMEREASSIGNORDER")) {
			IPGResponseDTO reassignIPGResponseDTO;
			reassignIPGResponseDTO = getReponseReassign(fields, ipgResponseDTO);
			return reassignIPGResponseDTO;

		} else {

			CheckNotificationDTO payFortNotifyingRequest = new CheckNotificationDTO();
			// set map values to the DTO
			payFortNotifyingRequest.setRequest(fields);

			String currentDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, CalendarUtil.getCurrentZuluDateTime());
			currentCalendar = convertStringDateToXmlGregorianCalendar(currentDate, DATE_FORMAT, true);
			payFortNotifyingRequest.setMsgDate(currentCalendar);

			String signature = payFortNotifyingRequest.getCurrency() + "" + payFortNotifyingRequest.getMerchantID() + ""
					+ payFortNotifyingRequest.getOrderID() + "" + payFortNotifyingRequest.getPayment() + ""
					+ payFortNotifyingRequest.getServiceName() + "" + payFortNotifyingRequest.getStatus() + ""
					+ payFortNotifyingRequest.getTicketNumber() + getEncryptionKey();

			String signatureSHA1 = "";
			try {
				signatureSHA1 = SHA1(signature);
			} catch (NoSuchAlgorithmException e1) {
				log.error(e1);
				throw new ModuleException("paymentbroker.error.unknown");
			} catch (UnsupportedEncodingException e1) {
				log.error(e1);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			payFortNotifyingRequest.setSignature(signatureSHA1);
			log.debug("[PaymentBrokerPayFortTemplate::getReponseData()] ");

			// calling remote web service call
			try {
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				payFortNotifyingResponse = ebiWebervices.checkNotification(payFortNotifyingRequest);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			if (payFortNotifyingResponse != null) {
				if (payFortNotifyingResponse.getStatus().equals(PayFortPaymentUtils.SUCCESS)) {

					orderId = payFortNotifyingResponse.getOrderID();
					postDataMap.put(payFortNotifyingResponse.CURRENCY, payFortNotifyingResponse.getCurrency());
					postDataMap.put(payFortNotifyingResponse.MERCHANT_ID, payFortNotifyingResponse.getMerchantID());
					postDataMap.put(payFortNotifyingResponse.ORDER_ID, payFortNotifyingResponse.getOrderID());
					postDataMap.put(payFortNotifyingResponse.PAYMENT, payFortNotifyingResponse.getPayment());
					postDataMap.put(payFortNotifyingResponse.SIGNATURE, payFortNotifyingResponse.getSignature());
					postDataMap.put(payFortNotifyingResponse.TICKET_NUMBER,
							(String) fields.get(PayFortPaymentUtils.TICKET_NUMBER));

					strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

					if (checkHashForNotifiction(postDataMap)) {
						status = IPGResponseDTO.STATUS_ACCEPTED;
						tnxResultCode = 1;
						errorSpecification = "" + responseMismatch;
					} else {
						status = IPGResponseDTO.STATUS_REJECTED;
						tnxResultCode = 0;
						errorCode = VERIFICATION_HASH_ERROR;
						errorSpecification = "Security Validation failed!!!";
						// Different amount in response
						log.error("[PaymentBrokerPayFortTemplate::getReponseData()] - SHA1 signation not satisfy from PayFort PGW");
					}

				} else {
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorCode = PAYFORT_RESPONSE_ERROR;
					if (!ipgResponseDTO.isOnholdCreated()) {
						errorCode = payFortNotifyingResponse.getErrorCode();
					}
					errorSpecification = "" + payFortNotifyingResponse.getErrorDesc();

				}
			}
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(orderId);
		ipgResponseDTO.setCardType(getStandardCardType(CARD_TYPE_GENERIC));

		updateAuditTransactionByTnxRefNo(oCreditCardTransaction.getTransactionRefNo(), strRequestParams, errorSpecification, "",
				oCreditCardTransaction.getTransactionReference(), tnxResultCode);

		return ipgResponseDTO;
	}

	private IPGResponseDTO getReponseReassign(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		CheckNotificationDTO payFortNotifyingRequest = new CheckNotificationDTO();
		payFortNotifyingRequest.setReassignRequest(fields);
		CheckNotificationResponseDTO payFortNotifyingResponse = new CheckNotificationResponseDTO();
		XMLGregorianCalendar currentCalendar = null;

		String currentDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, CalendarUtil.getCurrentZuluDateTime());
		currentCalendar = convertStringDateToXmlGregorianCalendar(currentDate, DATE_FORMAT, true);
		payFortNotifyingRequest.setMsgDate(currentCalendar);

		String signature = payFortNotifyingRequest.getCurrency() + "" + payFortNotifyingRequest.getMerchantID() + ""
				+ payFortNotifyingRequest.getOrderID() + "" + payFortNotifyingRequest.getPayment() + ""
				+ payFortNotifyingRequest.getServiceName() + "" + payFortNotifyingRequest.getStatus() + "" + getEncryptionKey();

		String signatureSHA1 = "";
		try {
			signatureSHA1 = SHA1(signature);
		} catch (NoSuchAlgorithmException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		} catch (UnsupportedEncodingException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		payFortNotifyingRequest.setSignature(signatureSHA1);
		log.debug("[PaymentBrokerPayFortTemplate::getReponseData()] ");

		// calling remote web service call
		try {
			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			payFortNotifyingResponse = ebiWebervices.checkNotification(payFortNotifyingRequest);

		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		return null;
	}

	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {

		log.info("[PaymentBrokerPayFortPay_AT_HomeECommerceImpl::resolvePartialPayments()] Begin");

		CheckOrderResponseDTO checkOrderResponseDTO;
		Map<String, String> receiptyMap = new HashMap<String, String>();
		TempPaymentTnx tmpPayment = null;

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			tmpPayment = ReservationModuleUtils.getReservationBD().loadTempPayment(oCreditCardTransaction.getTemporyPaymentId());
			String orderId = oCreditCardTransaction.getTempReferenceNum();

			receiptyMap.put(PayFortPaymentUtils.CURRENCY, tmpPayment.getPaymentCurrencyCode());
			receiptyMap.put(PayFortPaymentUtils.MERCHANT_ID, getMerchantId());
			receiptyMap.put(PayFortPaymentUtils.PAYMENT, "PAYatHOME");
			receiptyMap.put(PayFortPaymentUtils.SERVICE_NAME, getServiceName());
			receiptyMap.put(PayFortPaymentUtils.ORDER_ID, orderId);

			try {

				checkOrderResponseDTO = checkTransactionStatus(receiptyMap, oCreditCardTransaction);

				if (checkOrderResponseDTO != null) {
					if (checkOrderResponseDTO.getOrderStatus().equalsIgnoreCase("PROCESSED")) {

						boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);

						if (isResConfirmationSuccess) {
							if (log.isDebugEnabled()) {
								log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					} else if (checkOrderResponseDTO.getOrderStatus().equalsIgnoreCase("PENDING")) {
						// Change State to 'IS'
						// oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
						// log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
					} else if (checkOrderResponseDTO.getOrderStatus().equalsIgnoreCase("EXPIRED")) {
						// Change State to 'IF'
						oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
					} else {
						// No Payments exist
						// Update status to 'II'
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				} else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}

			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerPayFortPay_AT_HomeECommerceImpl::resolvePartialPayments()] End");
		return sr;

	}
}
