/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.isa.thinair.paymentbroker.api.dto.*;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Srikantha
 */
public class PaymentBrokerManager {

	private static Log log = LogFactory.getLog(PaymentBrokerManager.class);

	/**
	 * pre validate the payment before payment collection.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(params.getIdentificationParams());
		return paymentBroker.preValidatePayment(params);
	}

	/**
	 * advise how to collect the payment to the caller.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(params.getIdentificationParams());
		return paymentBroker.advisePaymentCollection(params);
	}

	/**
	 * post commit the payment once payment is collected.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(params.getIdentificationParams());
		return paymentBroker.postCommitPayment(params);
	}

	/**
	 * Performing refund operation
	 * 
	 * @throws ModuleException
	 */
	public static ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(creditCardPayment.getIpgIdentificationParamsDTO());

		return paymentBroker.refund(creditCardPayment, pnr, appIndicator, tnxMode);
	}

	/**
	 * Authorize
	 * 
	 * @throws ModuleException
	 */
	public static ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(creditCardPayment.getIpgIdentificationParamsDTO());
		return paymentBroker.charge(creditCardPayment, pnr, appIndicator, tnxMode, cardDetailConfigData);
	}

	/**
	 * Reverse the card payment
	 * 
	 * @throws ModuleException
	 */
	public static ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(creditCardPayment.getIpgIdentificationParamsDTO());
		return paymentBroker.reverse(creditCardPayment, pnr, appIndicator, tnxMode);
	}

	/**
	 * Returns true if offline payments cancellation is allowed from PGW
	 *
	 * @throws ModuleException
	 */
	public static boolean isOfflinePaymentsCancellationAllowed(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
		return paymentBroker.isOfflinePaymentsCancellationAllowed();
	}

	/**
	 * Performing Capture operation
	 * 
	 * @throws ModuleException
	 */
	public static ServiceResponce capture(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(creditCardPayment.getIpgIdentificationParamsDTO());

		return paymentBroker.capturePayment(creditCardPayment, pnr, appIndicator, tnxMode);
	}

	/**
	 * Resolves partial payments [ Invokes via a scheduler operation ]
	 * 
	 * @throws ModuleException
	 * 
	 *             FIXME
	 */
	public static ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		Collection<IPGIdentificationParamsDTO> colIPGIdentificationDTO = getAllIpgIdentificationParamsDTOs(colIPGQueryDTO);

		// Currently only one payment gateway needs to be send
		if (colIPGIdentificationDTO.size() != 1) {
			throw new ModuleException("paymentbroker.generic.operation.notsupported");
		} else {
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = colIPGIdentificationDTO.iterator().next();
			PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
			return paymentBroker.resolvePartialPayments(colIPGQueryDTO, refundFlag);
		}
	}

	public static ServiceResponce resolvePartialPaymentsForVouchers(Collection colIPGQueryDTO, boolean refundFlag)
			throws ModuleException {
		Collection<IPGIdentificationParamsDTO> colIPGIdentificationDTO = getAllIpgIdentificationParamsDTOs(colIPGQueryDTO);

		// Currently only one payment gateway needs to be send
		if (colIPGIdentificationDTO.size() != 1) {
			throw new ModuleException("paymentbroker.generic.operation.notsupported");
		} else {
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = colIPGIdentificationDTO.iterator().next();
			PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
			return paymentBroker.resolvePartialPaymentsForVouchers(colIPGQueryDTO, refundFlag);
		}
	}

	private static Collection<IPGIdentificationParamsDTO> getAllIpgIdentificationParamsDTOs(Collection colIPGQueryDTO) {
		Collection<IPGIdentificationParamsDTO> colIPGIdentificationDTO = new ArrayList<>();
		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();
		IPGQueryDTO ipgQueryDTO;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();
			if (!colIPGIdentificationDTO.contains(ipgQueryDTO.getIpgIdentificationParamsDTO())) {
				colIPGIdentificationDTO.add(ipgQueryDTO.getIpgIdentificationParamsDTO());
			}
		}
		return colIPGIdentificationDTO;
	}

	/**
	 * Returns the request data with auditing the request
	 * 
	 * @throws ModuleException
	 */
	public static IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgRequestDTO.getIpgIdentificationParamsDTO());
		return paymentBroker.getRequestData(ipgRequestDTO);
	}

	/**
	 * 
	 * @param ipgRequestDTO
	 * @param configDataList
	 * @return
	 * @throws ModuleException
	 */
	public static IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgRequestDTO.getIpgIdentificationParamsDTO());
		return paymentBroker.getRequestData(ipgRequestDTO, configDataList);
	}

	/**
	 * Capture the status response
	 * 
	 * @throws ModuleException
	 */
	public static ServiceResponce captureStatusResponse(Map receiptyMap, IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
		return paymentBroker.captureStatusResponse(receiptyMap);
	}

	/**
	 * Returns the RESPONSE with auditing the response
	 * 
	 * @throws ModuleException
	 */
	public static IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
		return paymentBroker.getReponseData(receiptyMap, ipgResponseDTO);
	}

	/**
	 * Gets properties
	 * 
	 * @throws ModuleException
	 */
	public static Properties getProperties(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
		return paymentBroker.getProperties();
	}

	/**
	 * Returns IPG configuration name.
	 * 
	 * @throws ModuleException
	 */
	public static String constrcutFullyQualifiedIPGConfigurationName(IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		String fullyQualifiedIPGConfigurationName = ipgIdentificationParamsDTO.getFQIPGConfigurationName();

		if (fullyQualifiedIPGConfigurationName == null) {
			if (ipgIdentificationParamsDTO == null || ipgIdentificationParamsDTO.getIpgId() == null) {
				throw new ModuleException("paymentbroker.invalid.identification.params");
			}
			fullyQualifiedIPGConfigurationName = ipgIdentificationParamsDTO.getIpgId().toString();
			if (ipgIdentificationParamsDTO.getPaymentCurrencyCode() != null
					&& !"".equals(ipgIdentificationParamsDTO.getPaymentCurrencyCode())) {
				fullyQualifiedIPGConfigurationName += "_" + ipgIdentificationParamsDTO.getPaymentCurrencyCode();
			}
			ipgIdentificationParamsDTO.setFQIPGConfigurationName(fullyQualifiedIPGConfigurationName);
		}
		return fullyQualifiedIPGConfigurationName;
	}

	/**
	 * Validates IPG identification parameters.
	 * 
	 * @throws ModuleException
	 */
	public static boolean validateIPGIdentificationParams(IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		if (!PaymentBrokerModuleUtil.getImplConfig().getPaymentBrokerMap()
				.containsKey(constrcutFullyQualifiedIPGConfigurationName(ipgIdentificationParamsDTO))) {
			throw new ModuleException("paymentbroker.invalid.identification.params");
		}
		ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);
		return true;
	}

	/**
	 * checks whether any IPG available with given parameters.
	 * 
	 * @throws ModuleException
	 */
	public static boolean checkForIPG(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		return PaymentBrokerModuleUtil.getImplConfig().getPaymentBrokerMap()
				.containsKey(constrcutFullyQualifiedIPGConfigurationName(ipgIdentificationParamsDTO));
	}
	
	public static String checkForIPGPerPGWId(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		for (Object key : PaymentBrokerModuleUtil.getImplConfig().getPaymentBrokerMap().keySet()) {
			String pgwName = (String) key;
			if (pgwName.startsWith(constrcutFullyQualifiedIPGConfigurationName(ipgIdentificationParamsDTO))) {
				return pgwName;
			}
		}
		return null;
	}

	/**
	 * Saves data in the T_CCARD_PAYMENT_STATUS table using a DTO
	 * @param creditCardPaymentStatusDTO
	 * @param ipgIdentificationParamsDTO
	 * @return
	 * @throws ModuleException
	 */
	public static CreditCardTransaction auditCCTransaction(CreditCardPaymentStatusDTO creditCardPaymentStatusDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
		return paymentBroker.auditCCTransaction(creditCardPaymentStatusDTO, ipgIdentificationParamsDTO);
	}

	/**
	 * returns the payment gateway collection of schedule refund enabled IPG identities.
	 * 
	 * @throws ModuleException
	 */
	public static Collection getSchedulerRefundEnabledIPGs() throws ModuleException {
		Collection refundByScheduelrEnabledIPGs = new ArrayList();
		Map configs = PaymentBrokerModuleUtil.getImplConfig().getPaymentBrokerMap();
		if (configs != null) {
			PaymentBroker pb = null;
			String ipgConfigurationName = null;
			for (Iterator configKeysIt = configs.keySet().iterator(); configKeysIt.hasNext();) {
				ipgConfigurationName = (String) configKeysIt.next();
				pb = (PaymentBroker) configs.get(ipgConfigurationName);
				if (pb.isEnableRefundByScheduler()) {
					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgConfigurationName);
					ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);
					refundByScheduelrEnabledIPGs.add(ipgIdentificationParamsDTO);
				}
			}
		}
		return refundByScheduelrEnabledIPGs;
	}

	/**
	 * returns the payment gateway collection of schedule refund enabled IPG identities.
	 * 
	 * @throws ModuleException
	 */
	public static boolean isRefundWithoutCardDetailsEnabled(String ipgName) throws ModuleException {
		Map configs = PaymentBrokerModuleUtil.getImplConfig().getPaymentBrokerMap();
		if (configs != null) {
			PaymentBroker paymentBroker = (PaymentBroker) configs.get(ipgName);
			return Boolean.valueOf(paymentBroker.getRefundWithoutCardDetails());
		}
		return false;
	}

	/**
	 * Returns the payment gateway available for the currency chosen.
	 * 
	 * @throws ModuleException
	 */
	public static Map getPaymentOptions(IPGPaymentOptionDTO ipgPaymentOptionDTO) throws ModuleException {

		Map paymentOptMap = new HashMap<String, String>();
		boolean isOfflinePayment = ipgPaymentOptionDTO.isOfflinePayment();
		List baseCurrencyList = PaymentBrokerUtils.getPaymentBrokerDAO().getDefaultCurrencyPaymentGateway("IBE", null,
				isOfflinePayment);
		paymentOptMap.put("BASE_CURRENCY", baseCurrencyList);
		List paymentGatewayList = PaymentBrokerUtils.getPaymentBrokerDAO().getPaymentGateways(ipgPaymentOptionDTO);
		paymentOptMap.put("PAYMENT_GATEWAY", paymentGatewayList);
		return paymentOptMap;
	}

	public static List<IPGPaymentOptionDTO> getPaymentGateways(IPGPaymentOptionDTO ipgPaymentOptionDTO) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getPaymentGateways(ipgPaymentOptionDTO);
	}

	/**
	 * private method to get the payment broker
	 * 
	 * @throws ModuleException
	 */
	private static PaymentBroker getPaymentBroker(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBroker paymentBroker = (PaymentBroker) PaymentBrokerModuleUtil.getImplConfig().getPaymentBrokerMap()
				.get(constrcutFullyQualifiedIPGConfigurationName(ipgIdentificationParamsDTO));
		if (paymentBroker == null) {
			throw new ModuleException(PaymentConstants.MSG_CODE_PAYMENTBROKER_NOT_FOUND);
		}

		paymentBroker.setIPGIdentificationParams(ipgIdentificationParamsDTO);
		return paymentBroker;
	}

	/**
	 * Returns the xmlResponse
	 * 
	 * @throws ModuleException
	 */
	public static XMLResponseDTO getXMLResponse(IPGIdentificationParamsDTO ipgIdentificationParamsDTO, Map postDataMap)
			throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
		return paymentBroker.getXMLResponse(postDataMap);
	}
	
	public static void handleDeferredResponse(Map<String, String> receiptyMap,	IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
		paymentBroker.handleDeferredResponse(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);
	}

	/**
	 * Gets Payment Broker
	 * 
	 * @throws ModuleException
	 */
	public static PaymentBroker getPaymentBrokerInfo(IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgIdentificationParamsDTO);
		return paymentBroker;
	}

	public static IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		PaymentBroker paymentBroker = getPaymentBroker(ipgRequestDTO.getIpgIdentificationParamsDTO());
		return paymentBroker.getRequestDataForAliasPayment(ipgRequestDTO);
	}
}
