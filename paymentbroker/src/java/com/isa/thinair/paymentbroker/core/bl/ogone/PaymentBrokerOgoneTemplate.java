package com.isa.thinair.paymentbroker.core.bl.ogone;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.dto.XMLResponse;

public class PaymentBrokerOgoneTemplate extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerOgoneTemplate.class);

	protected static final String ERROR_CODE_PREFIX = "ogone.0";

	protected static final String WIN3DS_MAINW = "MAINW";

	protected static final String AUTHORIZATION_SUCCESS = "5";

	protected static final String PAYMENT_SUCCESS = "9";

	protected static final String REFUND_SUCCESS = "8";

	protected static final String REFUND_PENDING = "81";

	protected static final String CANCELLATION_SUCCESS = "61";

	protected static final String AUTHORIZED_AND_CANCELLED = "6";

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected String sha1In;

	protected String sha1Out;

	protected String maintenanceURL;

	protected String operation;
	
	protected Map<String, String> dynamicTemplateUrls;
	
	protected boolean enableDynamicTemplate;
	
	public Properties getProperties() {
		Properties props = super.getProperties();
		props.put("dynamicTemplateUrls", PlatformUtiltiies.nullHandler(dynamicTemplateUrls));	
		props.put("enableDynamicTemplate", PlatformUtiltiies.nullHandler(enableDynamicTemplate));	
		return props;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@SuppressWarnings("rawtypes")
	@Override
	/**
	 * Reads the response returns from the OGONE server
	 * @param the response returns from OGONE
	 */
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerOgoneImpl::getReponseData()] Begin ");

		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		String merchantTxnReference;
		String authorizeID;
		int cardType = 5; // default card type

		String hashValidated = null;
		String responseMismatch = "";
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		OgoneResponse ogoneResp = new OgoneResponse();
		ogoneResp.setReponse(fields);
		log.debug("[PaymentBrokerOgoneImpl::getReponseData()] Mid Response -" + ogoneResp);
		if (ogoneResp != null && !(ogoneResp.getStatus().trim().equals(""))) {

			// Validate the Hash if required
			if (ogoneResp.getShaRequired().equals("N") || ogoneResp.validateSHAHash(getSha1Out())) {
				// Secure Hash validation succeeded, add a data field to be
				// displayed later.
				hashValidated = OgoneResponse.VALID_HASH;

				// Check for correct response
				errorExists = true;

				// CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(paymentBrokerRefNo);
				// Check if response is not equal
				if (oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(ogoneResp.getOrderId())) {
					errorExists = false;
				} else {
					responseMismatch = OgoneResponse.INVALID_RESPONSE;

					// Use QueryDR to verify the transaction
					PaymentQueryDR QueryDR = getPaymentQueryDR();
					ServiceResponce serviceResponce = QueryDR.query(oCreditCardTransaction,
							OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
							PaymentBrokerInternalConstants.QUERY_CALLER.INVALIDRESPONSE);
					if (serviceResponce.isSuccess()) {
						errorExists = false;
						//FIXME: This is a temporary hack for IBE payment failure reported by AARESAA-12031
						ogoneResp.setOrderId(serviceResponce.getResponseParam(PaymentConstants.ORDER_ID).toString());
						ogoneResp.setCurrency(serviceResponce.getResponseParam(PaymentConstants.CURRENCY).toString());
						ogoneResp.setPaymentMethod(serviceResponce.getResponseParam(PaymentConstants.PAYMENT_METHOD).toString());
						ogoneResp.setAmount(serviceResponce.getResponseParam(PaymentConstants.AMOUNT).toString());
					}
				}
			} else {
				// Secure Hash validation failed, add a data field to be
				// displayed later.
				errorExists = true;
				hashValidated = OgoneResponse.INVALID_HASH;

				PaymentQueryDR QueryDR = getPaymentQueryDR();
				ServiceResponce serviceResponce = QueryDR.query(oCreditCardTransaction,
						OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.HASHMISMATCH);
				if (serviceResponce.isSuccess()) {
					errorExists = false;
					ogoneResp.setOrderId(serviceResponce.getResponseParam(PaymentConstants.ORDER_ID).toString());
					ogoneResp.setCurrency(serviceResponce.getResponseParam(PaymentConstants.CURRENCY).toString());
					ogoneResp.setPaymentMethod(serviceResponce.getResponseParam(PaymentConstants.PAYMENT_METHOD).toString());
					ogoneResp.setAmount(serviceResponce.getResponseParam(PaymentConstants.AMOUNT).toString());
				}
			}
		} else {
			// Secure Hash was not validated,
			hashValidated = OgoneResponse.NO_VALUE;
			errorCode = ERROR_CODE_PREFIX;
		}

		merchantTxnReference = ogoneResp.getOrderId();
		authorizeID = ogoneResp.getAcceptance();
		cardType = getStandardCardType(ogoneResp.getBrand());

		if ((AUTHORIZATION_SUCCESS.equals(ogoneResp.getStatus()) || PAYMENT_SUCCESS.equals(ogoneResp.getStatus()))
				&& !errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "" + responseMismatch;
		} else {
			String responseStatus = ogoneResp.getStatus();

			// Log unknown status transactions
			if (responseStatus.equals("50") || responseStatus.equals("51") || responseStatus.equals("52")
					|| responseStatus.equals("55") || responseStatus.equals("59")) {
				log.debug("We received an unknown status for the transaction. Order id : " + ogoneResp.getOrderId());
			}

			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;

			errorCode = OgonePaymentUtils.getFilteredErrorCode(ogoneResp.getStatus(), ogoneResp.getNcError());

			errorSpecification = status + " " + hashValidated + " "
					+ OgonePaymentUtils.getResponseStatusDescription(ogoneResp.getStatus(), ogoneResp.getNcError()) + " "
					+ ogoneResp.getNcError() + " " + ogoneResp.getNcErrorPlus() + " " + responseMismatch;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(ogoneResp.getNcErrorPlus());
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authorizeID);
		ipgResponseDTO.setCardType(cardType);
		ipgResponseDTO.setAlias(ogoneResp.getAlias());
		String trimedDigits = "";
		if (ogoneResp.getCardNo() != null && ogoneResp.getCardNo().length() > 4) {
			trimedDigits = ogoneResp.getCardNo().substring(ogoneResp.getCardNo().length() - 4);
		}

		ipgResponseDTO.setCcLast4Digits(trimedDigits);
		String ogoneResponse = ogoneResp.toString();

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), ogoneResponse, errorSpecification,
				ogoneResp.getAcceptance(), ogoneResp.getPayId(), tnxResultCode);

		log.debug("[PaymentBrokerOgoneImpl::getReponseData()] End -" + ogoneResp.getOrderId() + "");

		return ipgResponseDTO;
	}
	
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		
		if (AppSysParamsUtil.enableServerToServerMessages()) {			
			int paymentBrokerRefNo = ipgResponseDTO.getPaymentBrokerRefNo();
			CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(paymentBrokerRefNo);
			if (oCreditCardTransaction != null) {
				OgoneResponse ogoneResp = new OgoneResponse();
				ogoneResp.setReponse(receiptyMap);
				if (ogoneResp != null && !(ogoneResp.getStatus().trim().equals(""))) {
					/* Validate the Hash if required */
					if ((ogoneResp.getShaRequired().equals("N") || ogoneResp.validateSHAHash(getSha1Out()))
							&& oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(ogoneResp.getOrderId())
							&& (AUTHORIZATION_SUCCESS.equals(ogoneResp.getStatus()) || PAYMENT_SUCCESS.equals(ogoneResp.getStatus()))) {
						/* if payment response does not process TransactionResultCode will be 0 */
						if (oCreditCardTransaction.getTransactionResultCode() != 1) {
							if (log.isDebugEnabled()) 
								log.debug(ogoneResp.toString());
							/* Save Card Transaction response Details */
							ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
							Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
							Date now = Calendar.getInstance().getTime();
							String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
							long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);
	
							oCreditCardTransaction.setComments(strResponseTime);
							oCreditCardTransaction.setResponseTime(timeDiffInMillis);
							oCreditCardTransaction.setTransactionResultCode(1);
							oCreditCardTransaction.setDeferredResponseText(ogoneResp.toString());
							oCreditCardTransaction.setAidCccompnay(ogoneResp.getAcceptance());
	
							PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);
	
							IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(paymentBrokerRefNo);
							/* Update with payment response details */
							ipgQueryDTO.setPaymentType(getPaymentType(getStandardCardType(ogoneResp.getBrand())));
							ipgQueryDTO.setCreditCardNo(ogoneResp.getCardNo());							
							// TODO:set AuthorizationID
							if (ipgQueryDTO != null) {
								boolean isResConfirmationSuccess = false;
								if (AppSysParamsUtil.isConfirmOnholdReservationByServerToServerMessages()) {
									isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);
								}
	
								// Refund the payment
								if (AppSysParamsUtil.isRefundPaymentsByServerToServerMessages() && !isResConfirmationSuccess && 
										isEnableRefundByScheduler() && AppSysParamsUtil.isQueryDRRefund()) {
									if (log.isDebugEnabled())
										log.debug("Refunding the payment..:" + oCreditCardTransaction.getTemporyPaymentId());
	
									PaymentQueryDR query = getPaymentQueryDR();
									ServiceResponce serviceResponce = query.query(oCreditCardTransaction,
											OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
											PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);
	
									if (serviceResponce.isSuccess()) {
										String status = "";
										/* Do refund */
										//TODO need to check txnNo
										String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
												.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));
										PreviousCreditCardPayment oPrevCCPayment = new PreviousCreditCardPayment();
										oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
										oPrevCCPayment.setPgSpecificTxnNumber(txnNo);
	
										CreditCardPayment oCCPayment = new CreditCardPayment();
										oCCPayment.setPreviousCCPayment(oPrevCCPayment);
										oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
										oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
										oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
										oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
										oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
										oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());
	
										ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
												oCCPayment.getTnxMode());
	
										if (srRev.isSuccess()) {
											// Change State to 'IS'
											status = ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_SUCCESS;
											if (log.isDebugEnabled()) {
												log.debug("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
											}
										} else {
											// Change State to 'IF'
											status = ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED;
											if (log.isDebugEnabled()) {
												log.debug("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
											}
										}
	
										List<Integer> tranctionIDList = new ArrayList<Integer>();
										tranctionIDList.add(oCreditCardTransaction.getTemporyPaymentId());
										reservationBD.updateTempPaymentEntriesForBulkRecords(tranctionIDList, status);
									} else {
										/*
										 * We will receive the invalid response. We are not updating the status Scheduler
										 * will pick the transaction and try to refund the payment again. this is happening
										 * due to payment gateway is not responding or refund is fail at that moment
										 */
									}
								} else {
									/*
									 * No need to take actions. 1. confirm reservation status update 2. Refund not enable
									 */
								}
							}
						}
	
					} else {
						log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
					}
				} else {
					log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
				}
			} else {
				log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
			}
		}
	}

	public ServiceResponce cancel(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
			log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
			log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.VOID;

			Map<String, String> postDataMap = new LinkedHashMap<String, String>();
			postDataMap.put(OgoneRequest.PSPID, getMerchantId());
			if (getUser() != null && !getUser().equals("")) {
				postDataMap.put(OgoneRequest.USERID, getUser());
				postDataMap.put(OgoneRequest.PSWD, getPassword());
			}
			postDataMap.put(OgoneRequest.ORDERID, merchTnxRef);
			postDataMap.put(OgoneRequest.OPERATION, OgonePaymentUtils.OGONE_OPERATION.DES.toString());

			try {
				String shaSign = OgonePaymentUtils.computeSHA1(postDataMap, getSha1In());
				postDataMap.put(OgoneRequest.SHASIGN, shaSign);
			} catch (Exception e) {
				log.debug("Error computing SHA-1 : " + e.toString());
				throw new ModuleException(e.getMessage());
			}

			String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

			// Add other required parameters to create the post request.
			postDataMap.put("PROXY_HOST", getProxyHost());
			postDataMap.put("PROXY_PORT", getProxyPort());
			postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
			postDataMap.put("URL", getMaintenanceURL());

			XMLResponse response = null;
			Integer paymentBrokerRefNo;

			CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef,
					creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REVERSAL"), strRequestParams, "",
					getPaymentGatewayName(), null, false);

			// Sends the cancel request and gets the response

			try {
				// Cancel request.
				response = OgonePaymentUtils.getOgoneXMLResponse(postDataMap);
				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Cancel Response : " + response.toString());

			DefaultServiceResponse sr = new DefaultServiceResponse(false);

			if (CANCELLATION_SUCCESS.equals(response.getStatus()) || AUTHORIZED_AND_CANCELLED.equals(response.getStatus())) {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_CANCEL);
			} else {
				// TODO
				errorCode = OgonePaymentUtils.getFilteredErrorCode(response.getStatus(), response.getNcError());
				transactionMsg = OgonePaymentUtils.getResponseStatusDescription(response.getStatus(), response.getNcError());
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:"
						+ OgonePaymentUtils.getResponseStatusDescription(response.getStatus(), response.getNcError()) + " : "
						+ response.getNcErrorPlus();
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), response.toString(), errorSpecification,
					response.getAcceptance(), response.getPayId() + ":" + response.getPayIdSub(), tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAcceptance());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.setResponseCode("Failed");
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);

			return sr;
		}
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
		String amount = PaymentBrokerUtils.getFormattedAmount(creditCardPayment.getAmount(),
				creditCardPayment.getNoOfDecimalPoints());

		PaymentQueryDR queryDR = getPaymentQueryDR();
		ServiceResponce serviceResponce = queryDR.query(ccTransaction,
				OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
				PaymentBrokerInternalConstants.QUERY_CALLER.REFUND);
		/***
		 * Delete payments for refunds if cancelBeforeFefund flag is enabled. Should delete the full payment amount.
		 * Cannot delete a part of a payment. Should refund the transaction amount to refund a part of a transaction
		 * even if it is possible to delete.
		 ***/
		String responceStatus = (String) serviceResponce.getResponseParam(PaymentConstants.STATUS);
		if (!StringUtil.isNullOrEmpty(responceStatus) && isCancelBeforeRefund() && !PAYMENT_SUCCESS.equals(responceStatus)) {
			String amountInPGW = PaymentBrokerUtils.getFormattedAmount(serviceResponce.getResponseParam(PaymentConstants.AMOUNT)
					.toString(), creditCardPayment.getNoOfDecimalPoints());
			if (amount.equals(amountInPGW)) {
				ServiceResponce deleteResponse = cancel(creditCardPayment, pnr, appIndicator, tnxMode);
				if (deleteResponse.isSuccess()) {
					return deleteResponse;
				}
			}

		}

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
			log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
			log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();

			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

			Map<String, String> postDataMap = new LinkedHashMap<String, String>();
			postDataMap.put(OgoneRequest.PSPID, getMerchantId());
			if (getUser() != null && !getUser().equals("")) {
				postDataMap.put(OgoneRequest.USERID, getUser());
				postDataMap.put(OgoneRequest.PSWD, getPassword());
			}
			postDataMap.put(OgoneRequest.ORDERID, merchTnxRef);
			postDataMap.put(OgoneRequest.AMOUNT, amount);
			postDataMap.put(OgoneRequest.OPERATION, OgonePaymentUtils.OGONE_OPERATION.RFD.toString());

			try {
				String shaSign = OgonePaymentUtils.computeSHA1(postDataMap, getSha1In());
				postDataMap.put(OgoneRequest.SHASIGN, shaSign);
			} catch (Exception e) {
				log.debug("Error computing SHA-1 : " + e.toString());
				throw new ModuleException(e.getMessage());
			}

			String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

			// Add other required parameters to create the post request.
			postDataMap.put("PROXY_HOST", getProxyHost());
			postDataMap.put("PROXY_PORT", getProxyPort());
			postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
			postDataMap.put("URL", getMaintenanceURL());

			XMLResponse response = null;
			Integer paymentBrokerRefNo;

			CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef,
					creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), strRequestParams, "",
					getPaymentGatewayName(), null, false);

			// Sends the refund request and gets the response

			try {
				// Refund request.
				response = OgonePaymentUtils.getOgoneXMLResponse(postDataMap);

				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Refund Response : " + response.toString());

			DefaultServiceResponse sr = new DefaultServiceResponse(false);

			if (REFUND_SUCCESS.equals(response.getStatus()) || REFUND_PENDING.equals(response.getStatus())) {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
			} else {
				errorCode = OgonePaymentUtils.getFilteredErrorCode(response.getStatus(), response.getNcError());
				transactionMsg = OgonePaymentUtils.getResponseStatusDescription(response.getStatus(), response.getNcError());
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorSpecification,
					response.getAcceptance(), response.getPayId() + ":" + response.getPayIdSub(), tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAcceptance());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

			return sr;
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;
		Date currentTime = Calendar.getInstance().getTime();
		Calendar queryValidTime = Calendar.getInstance();

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			query = getPaymentQueryDR();

			try {
				ServiceResponce serviceResponce = query.query(oCreditCardTransaction,
						OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				queryValidTime.setTime(ipgQueryDTO.getTimestamp());
				queryValidTime.add(Calendar.MINUTE, AppSysParamsUtil.getTimeForPaymentStatusUpdate());

				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);

					if (isResConfirmationSuccess) {
						if (log.isDebugEnabled()) {
							log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
						}

					} else {
						if (refundFlag) {
							// Do refund
							String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
									.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

							oPrevCCPayment = new PreviousCreditCardPayment();
							oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
							oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

							oCCPayment = new CreditCardPayment();
							oCCPayment.setPreviousCCPayment(oPrevCCPayment);
							oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
							oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
							oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
							oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
							oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
							oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

							ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
									oCCPayment.getTnxMode());

							if (srRev.isSuccess()) {
								// Change State to 'IS'
								oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							} else {
								// Change State to 'IF'
								oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							}
						} else {
							// Change State to 'IP'
							oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
					} else {
						// Can not move payment status. with in status change for 3D secure payments
						log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
			} catch (ModuleException e) {
				log.info("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		return sr;
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}
		reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		return reverseResult;
	}
	
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
	

	/**
	 * Returns the card type as integer
	 * 
	 * @param card
	 *            the card
	 * @return card type
	 */
	protected int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	private PaymentType getPaymentType(int cardID) {

		PaymentType paymentType = PaymentType.CARD_GENERIC;
		switch (cardID) {
		case 1:
			paymentType = PaymentType.CARD_MASTER;
			break;
		case 2:
			paymentType = PaymentType.CARD_VISA;
			break;
		case 3:
			paymentType = PaymentType.CARD_AMEX;
			break;
		case 4:
			paymentType = PaymentType.CARD_DINERS;
			break;
		case 6:
			paymentType = PaymentType.CARD_CMI;
			break;
		}
		return paymentType;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		return OgonePaymentUtils.assembleXMLResponseDTO(OgonePaymentUtils.getOgoneXMLResponse(postDataMap));
	}

	public String getSha1In() {
		return sha1In;
	}

	public void setSha1In(String sha1In) {
		this.sha1In = sha1In;
	}

	public String getSha1Out() {
		return sha1Out;
	}

	public void setSha1Out(String sha1Out) {
		this.sha1Out = sha1Out;
	}

	public String getMaintenanceURL() {
		return maintenanceURL;
	}

	public void setMaintenanceURL(String maintenanceURL) {
		this.maintenanceURL = maintenanceURL;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}


	public boolean isEnableDynamicTemplate() {
		return enableDynamicTemplate;
	}

	public void setEnableDynamicTemplate(boolean enableDynamicTemplate) {
		this.enableDynamicTemplate = enableDynamicTemplate;
	}

	public Map<String, String> getDynamicTemplateUrls() {
		return dynamicTemplateUrls;
	}

	public void setDynamicTemplateUrls(Map<String, String> dynamicTemplateUrls) {
		this.dynamicTemplateUrls = dynamicTemplateUrls;
	}

}
