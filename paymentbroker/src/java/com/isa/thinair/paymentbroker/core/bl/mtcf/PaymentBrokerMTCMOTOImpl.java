/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.mtcf;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Maroc Telecommerce Client Implementation for Call Center
 * 
 * @author Byorn De Silva
 */
public class PaymentBrokerMTCMOTOImpl extends PaymentBrokerTemplate implements PaymentBroker {

	static Log log = LogFactory.getLog(PaymentBrokerMTCMOTOImpl.class);

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	public static SSLSocketFactory s_sslSocketFactory = null;

	private static final String TRANSACTION_RESULT_SUCCESS = "0";
	private static final String MTC_CARD_TYPE_VISA = "VISA";
	private static final String MTC_CARD_TYPE_MASTER = "MC";
	private static final int ACCELAERO_CARD_TYPE_MASTER = 1;
	private static final String SUPPORTED_CURRENCY = "MAD";
	private static final String MTC_ERROR_CODE_PREFIX = "mtc.";

	public static interface CVV_STATES {
		public static final String PRESENT = "1";
		public static final String ILLEGIGLE = "2";
		public static final String ABSENT = "9";
	}

	/**
	 * Performs card payment operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {

		String merchantTxnId;
		String errorCode;
		String transactionMsg;
		String resultCodeString = null;
		int txnResultCode;
		String status;
		String errorSpecification;
		String cardNumber;
		String cardExpiry;
		String cvv;
		String cvsState = CVV_STATES.PRESENT;
		String cardOwner;
		String cardType;
		Map<String, String> creditCardInfoMap;

		String ipgURLPay = getIpgURL();

		if (AppSysParamsUtil.isCCPrefixCardCheckEnable() && creditCardPayment != null) { // BIN check for cc no's

			if (!PaymentBrokerUtils.getPaymentBrokerDAO().isBinPrefixExists(creditCardPayment.getCardNumber())) {
				throw new ModuleException("paymentbroker.bin.prefix.notsupported");
			}

		}

		// Get a merchant transaction id to identify txn in the payment server, in case of failure.
		merchantTxnId = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		cardNumber = creditCardPayment.getCardNumber();
		cardExpiry = creditCardPayment.getExpiryDate();
		cvv = creditCardPayment.getCvvField();

		if (cardExpiry != null) {
			cardExpiry = cardExpiry.substring(2, 4) + "/" + cardExpiry.substring(0, 2);
		}

		if (cvv == null) {
			cvsState = CVV_STATES.ABSENT;
		}
		cardOwner = creditCardPayment.getCardholderName();
		cardType = creditCardPayment.getCardType() == ACCELAERO_CARD_TYPE_MASTER ? MTC_CARD_TYPE_MASTER : MTC_CARD_TYPE_VISA;
		// BigDecimal value = AccelAeroCalculator.multiply(new BigDecimal(amount),
		String formatedAmount = creditCardPayment.getAmount();

		creditCardInfoMap = new LinkedHashMap<String, String>();
		creditCardInfoMap.put(MTCFatouratiRequest.ORDERID, merchantTxnId);
		creditCardInfoMap.put(MTCFatouratiRequest.USER, getUser());
		creditCardInfoMap.put(MTCFatouratiRequest.PASSWORD, getPassword());
		creditCardInfoMap.put(MTCFatouratiRequest.AMOUNT, formatedAmount);
		creditCardInfoMap.put(MTCFatouratiRequest.CURRENCY, SUPPORTED_CURRENCY);
		creditCardInfoMap.put(MTCFatouratiRequest.CARDTYPE, cardType);
		creditCardInfoMap.put(MTCFatouratiRequest.CARDNUMBER, cardNumber);
		creditCardInfoMap.put(MTCFatouratiRequest.CARDEXPIRY, cardExpiry);
		creditCardInfoMap.put(MTCFatouratiRequest.CARDOWNER, cardOwner);
		creditCardInfoMap.put(MTCFatouratiRequest.CVSCODE, cvv);
		creditCardInfoMap.put(MTCFatouratiRequest.CVSSTATE, cvsState);
		creditCardInfoMap.put(MTCFatouratiRequest.MERCHANTID, getMerchantId());
		creditCardInfoMap.put(MTCFatouratiRequest.EMAILADD, getEmailDefault());

		MTCFatouratiXMLResponse response = null;
		Integer paymentBrokerRefNo;

		// Sends the payment request and gets the response
		try {
			AccelAeroHTTPClient accelAeroHTTPClient = new AccelAeroHTTPClient();
			Object[] responses = accelAeroHTTPClient.doSubmitCardTransaction(pnr, creditCardPayment.getTemporyPaymentId(),
					ipgURLPay, creditCardInfoMap, true, merchantTxnId, this);
			response = (MTCFatouratiXMLResponse) responses[0];
			paymentBrokerRefNo = (Integer) responses[1];
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			}
			log.error(e);
			throw new ModuleException(e, "paymentbroker.error.unknown");
		}

		// create a hash map for the response data
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);

		if (TRANSACTION_RESULT_SUCCESS.equals(response.getErrorNb())) { // Success scenario
			errorCode = "";
			transactionMsg = response.getErrorMsg();
			status = IPGResponseDTO.STATUS_ACCEPTED;
			txnResultCode = 1;
			errorSpecification = "";
			serviceResponse.setSuccess(true);
			// @todo save cvv value from response.
		} else { // Failure scenarios
			errorCode = MTC_ERROR_CODE_PREFIX
					+ ((response.getErrorNb() == null || "".equals(response.getErrorNb())) ? "default" : response.getErrorNb()
							.trim());
			transactionMsg = response.getErrorMsg();
			status = IPGResponseDTO.STATUS_REJECTED;
			txnResultCode = 0;
			errorSpecification = status + " " + transactionMsg;
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.getAuthorizationResponseAsString(),
				errorSpecification, response.getAuthorizationNb(), response.getOrderNb(), txnResultCode, transactionMsg,
				response.getPayID());

		// Populate service response object
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAuthorizationNb());
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

		return serviceResponse;
	}

	/**
	 * Creates the message to send to the MIGS server
	 * 
	 * @param ipgRequestDTO
	 *            IPG payment request
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/**
	 * Reads the response returns from the MIGS server
	 * 
	 * @param encryptedReceiptPay
	 *            the secured response returns from the IPG
	 */
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/**
	 * Returns Payment Gateway Name
	 */
	@Override
	public String getPaymentGatewayName() {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// NOTE when trying refund reversal will try, so only transactions within 24 hrs will refund. transactions older
		// than
		// 24 hrs wont refund
		return reverse(creditCardPayment, pnr, appIndicator, tnxMode);
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;
		String merchTnxRef = null;
		// the pay id
		String transactionId;
		String ipgURLRefund = getReversalURL();

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			// the pay id returned from mtc.
			transactionId = ccTransaction.getTransactionId();

			merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
					PaymentConstants.MODE_OF_SERVICE.REFUND);

			Map<String, String> fields = new LinkedHashMap<String, String>();

			fields.put(MTCFatouratiRequest.USER, getReversalUser());
			fields.put(MTCFatouratiRequest.USERPASSWORD, getReversalPassword());
			fields.put(MTCFatouratiRequest.STORE, getMerchantId());
			fields.put(MTCFatouratiRequest.ORDER_NUMBER, ccTransaction.getTransactionId());

			if (isReversalChecksumRequired()) {

				String hashString = getReversalSecureSecret() + getMerchantId() + ccTransaction.getTransactionId();

				String urlEncodedHashString;
				MessageDigest md;
				try {
					urlEncodedHashString = URLEncoder.encode(hashString, "UTF-8");
					md = MessageDigest.getInstance("MD5");
				} catch (UnsupportedEncodingException uee) {
					log.error(uee);
					throw new ModuleException(uee, "");
				} catch (NoSuchAlgorithmException nsae) {
					log.error(nsae);
					throw new ModuleException(nsae, "");
				}
				md.reset();
				md.update((urlEncodedHashString).getBytes());
				String checksum = StringUtil.byteArrayToHexString(md.digest());
				checksum = checksum.toLowerCase();

				fields.put(MTCFatouratiRequest.CHECKSUM, checksum);
			}

			MTCFatouratiXMLResponse response = null;
			Integer paymentBrokerRefNo;

			// Sends the refund request and gets the response
			try {
				AccelAeroHTTPClient accelAeroHTTPClient = new AccelAeroHTTPClient();
				Object[] responses = accelAeroHTTPClient.doSubmitCardTransaction(pnr, creditCardPayment.getTemporyPaymentId(),
						ipgURLRefund, fields, false, merchTnxRef, this);
				response = (MTCFatouratiXMLResponse) responses[0];
				paymentBrokerRefNo = (Integer) responses[1];
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			DefaultServiceResponse sr = new DefaultServiceResponse(false);

			if (TRANSACTION_RESULT_SUCCESS.equals(response.getErrorNb())) {
				status = IPGResponseDTO.STATUS_ACCEPTED;
				errorCode = "";
				transactionMsg = response.getMsg();
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				errorCode = response.getErrorNb();
				transactionMsg = response.getErrorMsg();
				tnxResultCode = 0;
				errorSpecification = status + " " + transactionMsg;

				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			// todo need to check.!
			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.getCancelResponseAsString(),
					errorSpecification, response.getAuthorizationNb(), transactionId, tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAuthorizationNb());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

			return sr;
		}
	}

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
