package com.isa.thinair.paymentbroker.core.bl.tapOffline;

public class TapOfflineResponse {
	
	public static final String TAP_REF_ID = "TapRefID";
	
	public static final String RES_CODE = "ResCode";
	
	public static final String RES_MESSAGE = "ResMsg";
	
	public static final String PAY_MODE = "Paymode";
	
	public static final String PAY_REF_ID = "PayRefID";	
	
	private String tapRefID;
	
	private String resCode;
	
	private String resMsg;
	
	private String paymode;
	
	private String payRefID;

	public String getTapRefID() {
		return tapRefID;
	}

	public void setTapRefID(String tapRefID) {
		this.tapRefID = tapRefID;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResMsg() {
		return resMsg;
	}

	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}

	public String getPaymode() {
		return paymode;
	}

	public void setPaymode(String paymode) {
		this.paymode = paymode;
	}

	public String getPayRefID() {
		return payRefID;
	}

	public void setPayRefID(String payRefID) {
		this.payRefID = payRefID;
	}
	
	
}
