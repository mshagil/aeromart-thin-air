package com.isa.thinair.paymentbroker.core.bl.mpg;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerMpgQueryDR extends PaymentBrokerTemplate implements
		PaymentQueryDR {

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction,
			IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {
		ServiceResponce sr = new DefaultServiceResponse(true);
		
		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

}
