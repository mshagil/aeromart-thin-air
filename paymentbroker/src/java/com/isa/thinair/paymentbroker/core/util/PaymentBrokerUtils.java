package com.isa.thinair.paymentbroker.core.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerConfig;
import com.isa.thinair.paymentbroker.core.persistence.dao.PaymentBrokerDAO;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * @author Srikantha
 */
public class PaymentBrokerUtils {

	private static final Log log = LogFactory.getLog(PaymentBrokerUtils.class);

	/**
	 * @param moduleName
	 * @return IModule
	 */
	public static IModule lookupModule(String moduleName) {
		return LookupServiceFactory.getInstance().getModule(moduleName);
	}

	/**
	 * @return IModule
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(
				PaymentbrokerConstants.MODULE_NAME);
	}

	public static PaymentBrokerConfig getPaymentBrokerConfig() {
		return (PaymentBrokerConfig) LookupServiceFactory
				.getInstance()
				.getBean(
						"isa:base://modules/paymentbroker?id=paymentbrokerModuleConfig");
	}

	/**
	 * Returns the business delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName,
			String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName,
				BDKeyWithoutLocality, getPaymentBrokerConfig(),
				"paymentbroker", "paymentbroker.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName,
			String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName,
				serviceName, getPaymentBrokerConfig(), "paymentbroker",
				"paymentbroker.config.dependencymap.invalid");
	}

	public final static CommonMasterBD getCommonServiceBD() {
		try {
			return (CommonMasterBD) lookupEJB3Service(
					AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating CommonMasterBD" + e.getMessage());
			throw new ModuleRuntimeException(e, "common.masterbd.loading.error");
		}
	}

	public final static PaymentBrokerBD getPaymentBrokerBD() {
		try {
			return (PaymentBrokerBD) lookupEJB3Service(
					PaymentbrokerConstants.MODULE_NAME,
					PaymentBrokerBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating PaymentBrokerBD" + e.getMessage());
			throw new ModuleRuntimeException(e,
					"payment.brokerbd.loading.error");
		}
	}

	/**
	 * @param logger
	 * @param strMessage
	 */
	public static void debug(Log logger, String strMessage) {
		if (logger.isDebugEnabled()) {
			logger.debug(strMessage);
		}
	}

	/**
	 * @param logger
	 * @param strMessage
	 */
	public static void info(Log logger, String strMessage) {
		if (logger.isInfoEnabled()) {
			logger.info(strMessage);
		}
	}

	/**
	 * @param logger
	 * @param strMessage
	 */
	public static void warn(Log logger, String strMessage) {
		if (logger.isWarnEnabled()) {
			logger.warn(strMessage);
		}
	}

	/**
	 * @param logger
	 * @param strMessage
	 */
	public static void error(Log logger, String strMessage) {
		if (logger.isErrorEnabled()) {
			logger.error(strMessage);
		}
	}

	/**
	 * @param logger
	 * @param strMessage
	 * @param exception
	 */
	public static void error(Log logger, String strMessage, Exception exception) {
		if (logger.isErrorEnabled()) {
			logger.error(strMessage, exception);
		}
	}

	/**
	 * Returns the global config
	 * 
	 * @return
	 */
	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(
				PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	/**
	 * Returns a DAO for air reservation module
	 * 
	 * @param daoName
	 * @return
	 */
	private static Object getDAO(String daoName) {
		String daoFullName = "isa:base://modules/"
				+ PaymentbrokerConstants.MODULE_NAME + "?id=" + daoName
				+ "Proxy";
		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(daoFullName);
	}

	/**
	 * Creating the instance of payment broker DAO and return
	 * 
	 * @return PaymentBrokerDAO
	 */
	public static PaymentBrokerDAO getPaymentBrokerDAO() {
		return (PaymentBrokerDAO) getDAO("PaymentBrokerDAO");
	}

	/**
	 * TODO Encrypt data
	 * 
	 * Get Database store data
	 * 
	 * @param configDataList
	 * @param fieldName
	 * @param value
	 * @return
	 * @throws ModuleException
	 */
	@Deprecated
	public static String getDBStroreData(
			List<CardDetailConfigDTO> configDataList, String fieldName,
			String value) {
		StringBuffer cardStoreSb = new StringBuffer();

		// if (configDataList == null)
		// throw new
		// ModuleException("paymentbroker.cardconfigaration.not.found");

		String repDigitPattern = PaymentGatewayCard.REPLACE_PATTERN_DIGIT;
		String repStringPattern = PaymentGatewayCard.REPLACE_PATTERN_CHAR;
		String repChar = PaymentGatewayCard.REPLACE_STRING;
		boolean isFiledNameValid = false;
		String whatToStore = null;

		if (value != null && !value.trim().isEmpty() && fieldName != null
				&& !fieldName.trim().isEmpty()) {
			for (CardDetailConfigDTO configDTO : configDataList) {
				whatToStore = configDTO.getWhatToStore();
				isFiledNameValid = fieldName.equalsIgnoreCase(configDTO
						.getFieldName());
				// Card Number
				if (PaymentGatewayCard.FieldName.CARDNUMBER.code()
						.equalsIgnoreCase(fieldName) && isFiledNameValid) {
					if (PaymentGatewayCard.CardNumber.ALL.code()
							.equalsIgnoreCase(whatToStore)
							|| value.trim().length() < 5) {
						cardStoreSb.append(value);
					} else if (PaymentGatewayCard.CardNumber.FIRSTSIXDIGITANDLASTFOURDIGIT
							.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.substring(0, 6));
						cardStoreSb.append(value.substring(6,
								value.length() - 4).replaceAll(repDigitPattern,
								repChar));
						cardStoreSb.append(value.substring(value.length() - 4));
					} else if (PaymentGatewayCard.CardNumber.LASTFOURDIGIT
							.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.substring(0,
								value.length() - 4).replaceAll(repDigitPattern,
								repChar));
						cardStoreSb.append(value.substring(value.length() - 4));
					} else if (PaymentGatewayCard.CardNumber.NONE.code()
							.equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.replaceAll(repDigitPattern,
								repChar));
					}
					break;

				} else if (PaymentGatewayCard.FieldName.EXPIRYDATE.code()
						.equalsIgnoreCase(fieldName) && isFiledNameValid) {
					// Card expiry date
					if (PaymentGatewayCard.CardExpiryDate.ALL.code()
							.equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value);
					} else if (PaymentGatewayCard.CardExpiryDate.NONE.code()
							.equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.replaceAll(repDigitPattern,
								repChar));
					}
					break;
				} else if (PaymentGatewayCard.FieldName.CVV.code().equals(
						fieldName)
						&& isFiledNameValid) {
					// Card CVV
					if (PaymentGatewayCard.CardCVV.ALL.code().equalsIgnoreCase(
							whatToStore)) {
						cardStoreSb.append(value);
					} else if (PaymentGatewayCard.CardCVV.NONE.code()
							.equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.replaceAll(repDigitPattern,
								repChar));
					}
					break;
				} else if (PaymentGatewayCard.FieldName.CARDHOLDERNAME.code()
						.equals(fieldName) && isFiledNameValid) {
					// Card Holder name
					if (PaymentGatewayCard.CardHolderName.ALL.code()
							.equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value);
					} else if (PaymentGatewayCard.CardCVV.NONE.code()
							.equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.replaceAll(repStringPattern,
								repChar));
					}
					break;
				}
			}
		}

		return cardStoreSb.toString();
	}

	/**
	 * Returns the formatted amount
	 * 
	 * @param amount
	 * @return
	 */
	public static String getFormattedAmount(String amount, int noOfDecimalPoints) {
		if (SystemPropertyUtil.checkRemoveCardPaymentDecimals()) {
			amount = Long.toString(Math.round(Double.parseDouble(amount)));
		}

		BigDecimal value = AccelAeroCalculator.multiply(new BigDecimal(amount),
				AccelAeroCalculator.parseBigDecimal(Math.pow(10,
						noOfDecimalPoints)));
		return String.valueOf(value.intValue());
	}

	/**
	 * Returns the formatted thousand amount
	 * 
	 * @param amount
	 * @return
	 */
	public static String getFormattedThousandAmount(String amount, int noOfDecimalPoints) {
//		if (SystemPropertyUtil.checkRemoveCardPaymentDecimals()) {
//			amount = Long.toString(Math.round(Double.parseDouble(amount)));
//		}

		BigDecimal value = new BigDecimal(amount);

		DecimalFormat decimalFormatter = new DecimalFormat("#,###.00");

		return decimalFormatter.format(value);
	}

	/**
	 * Returns the String generated by appending key,value pairs in map
	 * 
	 * @param postDataMap
	 * @return
	 */
	public static String getRequestDataAsString(Map<String, String> postDataMap) {
		StringBuffer sb = new StringBuffer();

		if (postDataMap != null) {
			for (String fieldName : postDataMap.keySet()) {
				if (sb.length() > 0) {
					sb.append(",");
				}
				sb.append(fieldName + " : " + postDataMap.get(fieldName));
			}
		}

		return sb.toString();
	}

	/**
	 * This method is used to generate the HTTP POST request.
	 * 
	 * @param postDataMap
	 *            map containing request parameters
	 * @param ipgURL
	 *            URL to submit the form data
	 * @return String to post from html page.
	 */
	public static String getPostInputDataFormHTML(
			Map<String, String> postDataMap, String ipgURL) {
		return getForm(postDataMap, ipgURL, "post");
	}

	/**
	 * This method is used to generate the HTTP GET request.
	 * 
	 * @param postDataMap
	 *            map containing request parameters
	 * @param ipgURL
	 *            URL to submit the form data
	 * @return String to post from html page.
	 */
	public static String getInputDataGETFormHTML(
			Map<String, String> postDataMap, String ipgURL) {

		return getForm(postDataMap, ipgURL, "get");
	}

	private static String getForm(Map<String, String> postDataMap,
			String ipgURL, String method) {

		StringBuffer sb = new StringBuffer();
		sb.append("<form name=\"paymentForm\" action=\"" + ipgURL
				+ "\" method=\"" + method + "\">");
		for (String fieldName : postDataMap.keySet()) {
			sb.append("<input type=\"hidden\" name=\"" + fieldName
					+ "\" value=\"" + postDataMap.get(fieldName) + "\"/>");
		}
		sb.append("</form>");
		log.info("Form request====" + sb.toString());
		return sb.toString();
	}

	/**
	 * This method is used to generate the HTTP POST request.
	 * 
	 * @param postDataMap
	 *            map containing request parameters
	 * @param ipgURL
	 *            URL to submit the form data
	 * @return String to post from html page.
	 */
	public static String getPostInputDataFormHTMLInIframe(
			Map<String, String> postDataMap, String ipgURL) {
		StringBuffer sb = new StringBuffer();
		sb.append("<table align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background: url(\'../images/body_bg.png\') repeat-x top #CADE97\"><tr><td>"
				+ "<table width=\"940\" height=\"101px\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" style=\"background: url(\'../images/header.jpg\') no-repeat center\">"
				+ "<tr><td align=\"left\" class=\"topBanner\">"
				+ "<a href=\"http://www.flyzest.com/index.php\">"
				+ "<img border=\"0\" alt=\"Zestair logo\" src=\"../images/zestair-logo.png\" style=\"margin: 20px 0 0 25px\"/>"
				+ "</a></td><td align=\"right\" class=\"topBanner1\">"
				+ "<a href=\"#\"><img border=\"0\" alt=\"Zestair logo\" src=\"../images/flyzest4.png\" style=\"margin: -11px 8px 0 0\"/>"
				+ "</a></td></tr></table></td></tr><tr><td align=\"center\">");
		sb.append("<iframe name=\"bankNet\" id=\"bankNet\" src=\"\" width=\"940\" height=\"700\" frameborder=\"0\"></iframe>");
		sb.append("<form name=\"paymentForm\" action=\""
				+ ipgURL
				+ "\" method=\"post\" target=\"bankNet\" style=\"display:none\">");
		for (String fieldName : postDataMap.keySet()) {
			sb.append("<input type=\"hidden\" name=\"" + fieldName
					+ "\" value=\"" + postDataMap.get(fieldName) + "\"/>");
		}
		sb.append("</form>");
		sb.append("</td></tr><tr><td align=\"center\" height=\"100\" valign=\"top\">"
				+ "<table width=\"940\" cellspacing=\"0\" height=\"30px\" style=\"background: url(\'../images/footer-bg.jpg\') no-repeat center\" cellpadding=\"0\" border=\"0\" align=\"center\">"
				+ "<tr><td align=\"left\" style=\"padding: 0 0 0 14px;color:white;font-family:arial\">	Copyright ZestAirways Inc. 2011. All Rights Reserved</td></tr></table></td></tr></table>");
		return sb.toString();
	}

	public static void saveRequest(String currentState, int paymentBrokerRefNo,
			String request) {
		CreditCardTransaction oCreditCardTransaction = new PaymentBrokerUtils()
				.loadAuditTransactionByTnxRefNo(paymentBrokerRefNo);

		if (currentState.equalsIgnoreCase("GetExpressCheckout")) {
			oCreditCardTransaction.setIntermediateRequest1(request);
			log.debug("Save GET_EXPRESS_CHECKOUT web service requested data");
		} else if (currentState.equalsIgnoreCase("DoExpressCheckout")) {
			oCreditCardTransaction.setIntermediateRequest2(request);
			log.debug("Save DO_EXPRESS_CHECKOUT web service requested data");
		}
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(
				oCreditCardTransaction);
	}

	public static void saveResponse(String currentState,
			int paymentBrokerRefNo, String response, String transactionId,
			int tnxResultCode) {
		CreditCardTransaction oCreditCardTransaction = new PaymentBrokerUtils()
				.loadAuditTransactionByTnxRefNo(paymentBrokerRefNo);

		if (currentState.equalsIgnoreCase("SetExpressCheckout")) {
			oCreditCardTransaction.setTransactionId(transactionId);
			oCreditCardTransaction.setResultCode(tnxResultCode);
			oCreditCardTransaction.setResponseText(response);
			log.debug("Save SetExpressCheckout web service response data");
		} else if (currentState.equalsIgnoreCase("GetExpressCheckout")) {
			oCreditCardTransaction.setIntermediateResponse1(response);
			log.debug("Save GET_EXPRESS_CHECKOUT web service response data");
		} else if (currentState.equalsIgnoreCase("DoExpressCheckout")) {
			oCreditCardTransaction.setIntermediateResponse2(response);
			oCreditCardTransaction.setTransactionId(transactionId);
			oCreditCardTransaction.setResultCode(tnxResultCode);
			log.debug("Save DO_EXPRESS_CHECKOUT web service response data");
		} else if (currentState.equalsIgnoreCase("DoDirectPayment")) {
			oCreditCardTransaction.setTransactionId(transactionId);
			oCreditCardTransaction.setResultCode(tnxResultCode);
			oCreditCardTransaction.setResponseText(response);
			log.debug("Save DO_DIRECT_PAYMENT web service response data");
		}
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(
				oCreditCardTransaction);
	}

	/**
	 * Updates the Audit Transaction
	 * 
	 * @param transactionRefNo
	 * @param plainTextReceipt
	 * @param errorSpecification
	 * @param authorizationCode
	 * @param transactionId
	 * @param tnxResultCode
	 */
	public static void updateAuditTransactionByTnxRefNo(int transactionRefNo,
			String plainTextReceipt, String errorSpecification,
			String authorizationCode, String transactionId, int tnxResultCode) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils
				.getPaymentBrokerDAO().loadTransaction(transactionRefNo);

		creditCardTransaction.setResponseText(plainTextReceipt);
		creditCardTransaction.setErrorSpecification(errorSpecification);
		creditCardTransaction.setAidCccompnay(authorizationCode);

		creditCardTransaction.setResultCode(0);
		creditCardTransaction.setTransactionResultCode(tnxResultCode);
		creditCardTransaction.setTransactionId(transactionId);

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(
				creditCardTransaction);
	}

	/**
	 * Updates the Audit Transaction
	 * 
	 * @param transactionRefNo
	 * @return
	 */
	protected CreditCardTransaction loadAuditTransactionByTnxRefNo(
			int transactionRefNo) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils
				.getPaymentBrokerDAO().loadTransaction(transactionRefNo);
		return creditCardTransaction;
	}

	public static String compressMessage(String message) {
		message = message.replaceAll(">[\n]+<", "><");
		message = message.replaceAll(">[\\s]+<", "><");
		return message;
	}

	/**
	 * Audit the Transaction
	 * 
	 * @param pnr
	 * @param merchantId
	 * @param merchantTnxId
	 * @param temporyPaymentId
	 * @param serviceType
	 * @param strRequest
	 * @param strResponse
	 * @param paymentGatwayName
	 * @param isSuccess
	 * @return
	 */
	public static CreditCardTransaction auditTransaction(String pnr,
			String merchantId, String merchantTnxId, Integer temporyPaymentId,
			String serviceType, String strRequest, String strResponse,
			String paymentGatwayName, String transactionId, boolean isSuccess) {
		CreditCardTransaction transaction = new CreditCardTransaction();

		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
		transaction.setTransactionTimestamp(fmt.format(new Date()));
		transaction.setTransactionReference(pnr);
		transaction.setTransactionId(transactionId);
		transaction.setTransactionRefCccompany(merchantId);
		transaction.setTempReferenceNum(merchantTnxId);
		transaction.setTemporyPaymentId(temporyPaymentId);
		transaction.setServiceType(serviceType);
		transaction.setRequestText(strRequest);
		transaction.setResponseText(strResponse);
		transaction.setPaymentGatewayConfigurationName(paymentGatwayName);

		if (isSuccess) {
			transaction.setResultCode(0);
			transaction.setTransactionResultCode(1);
		}

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(transaction);

		return transaction;
	}
	
}
