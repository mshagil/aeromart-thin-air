package com.isa.thinair.paymentbroker.core.bl.payFort;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerPayFortPay_AT_StoreECommerceImpl extends PaymentBrokerPayFortTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerPayFortPay_AT_StoreECommerceImpl.class);

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected static final String VERIFICATION_HASH_ERROR = "payfort.payment.mismatch";
	protected static final String PAYFORT_RESPONSE_ERROR = "payfort.pay_at_store.payment.error";
	protected static final String DATE_FORMAT = "dd-MM-yyyy HH:mm";

	private String payAtstoreScriptUrl;
	
	public String getPayAtstoreScriptUrl() {
		return payAtstoreScriptUrl;
	}

	public void setPayAtstoreScriptUrl(String payAtstoreScriptUrl) {
		this.payAtstoreScriptUrl = payAtstoreScriptUrl;
	}

	/**
	 * Send the request to the PayFort pay@store and get the initial response
	 * 
	 * @param IPGRequestDTO
	 *            the secured response returns from the IPG
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerPayFortPay_AT_StoreECommerceImpl::getRequestData()] Begin ");

		Pay_AT_StoreResponseDTO payResponse = null;
		boolean avoid = false;
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		String merchantTxnId = "";
		XMLGregorianCalendar expireDateCalander = null;
		XMLGregorianCalendar currentCalendar = null;
		String languageCode = DEFAULT_LANGUAGE;

		Long timeToSpare = null;
		String signatureSHA1 = "";
		Pay_AT_StoreRequestDTO requestDTO = new Pay_AT_StoreRequestDTO();

		// initializing data to be send for web service calls
		merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		// BigDecimal finalAmount = PayFortPaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount(), true);
		BigDecimal finalAmount = new BigDecimal(ipgRequestDTO.getAmount());
		String mobileNumber = ipgRequestDTO.getContactMobileNumber().replace("-", "");
		mobileNumber = mobileNumber.replace("+", "");
		Calendar calander = Calendar.getInstance();
		calander.setTime(CalendarUtil.getCurrentZuluDateTime());
		if (Integer.parseInt(getExpireTimeInMinutes()) > 0) {
			calander.add(Calendar.MINUTE, Integer.parseInt(getExpireTimeInMinutes()));
		} else {
			calander.add(Calendar.MINUTE, roundUpExpireDate(ipgRequestDTO.getExpireMTCOffLinePeriod(), 60));
		}

		String expireDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, calander.getTime());
		expireDateCalander = convertStringDateToXmlGregorianCalendar(expireDate, DATE_FORMAT, false);
		String currentDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, CalendarUtil.getCurrentZuluDateTime());
		currentCalendar = convertStringDateToXmlGregorianCalendar(currentDate, DATE_FORMAT, true);
		// set the signature
		String signature = finalAmount + "" + ipgRequestDTO.getContactEmail() + "" + mobileNumber + ""
				+ ipgRequestDTO.getContactFirstName() + " " + ipgRequestDTO.getContactLastName()
				+ ipgIdentificationParamsDTO.getPaymentCurrencyCode() + expireDateCalander + "" + getItemPrefix()
				+ ipgRequestDTO.getPnr() + "" + getMerchantId() + "" + merchantTxnId + getServiceName() + ipgRequestDTO.getPnr()
				+ getEncryptionKey();
		try {
			signatureSHA1 = SHA1(signature);
		} catch (NoSuchAlgorithmException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		} catch (UnsupportedEncodingException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		}
		// timeToSpare = ipgRequestDTO.getInvoiceExpirationTime().getTime() - new Date().getTime();

		// setting initialized data into the DTO
		requestDTO.setAmount(finalAmount);
		requestDTO.setClientEmail(ipgRequestDTO.getContactEmail());
		requestDTO.setClientMobile(mobileNumber);
		requestDTO.setClientName(ipgRequestDTO.getContactFirstName() + " " + ipgRequestDTO.getContactLastName());
		requestDTO.setCurrency(ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		requestDTO.setExpiryDate(expireDateCalander);
		requestDTO.setItemName(getItemPrefix() + ipgRequestDTO.getPnr());
		requestDTO.setMerchantID(getMerchantId());
		requestDTO.setMsgDate(currentCalendar);
		requestDTO.setOrderID(merchantTxnId);
		requestDTO.setServiceName(getServiceName());
		requestDTO.setTicketNumber(ipgRequestDTO.getPnr());
		requestDTO.setSignature(signatureSHA1);

		// Remote web service call for payfort
		try {
			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			payResponse = ebiWebervices.payAtStorePaymentTransaction(requestDTO);

		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();

		if (payResponse != null) {
			if (payResponse.getStatus() != null && payResponse.getStatus().equals(PayFortPaymentUtils.SUCCESS)) {

				postDataMap.put(payResponse.VOUCHER_ID, payResponse.getVoucherNumber());
				postDataMap.put(payResponse.REQUEST_ID, payResponse.getRequestID());
				String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
				String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

				CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
						new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
						strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

				if (log.isDebugEnabled()) {
					log.debug("PaymentBrokerPayFortPay_AT_StoreECommerceImpl PG Request Data : " + strRequestParams
							+ ", sessionID : " + sessionID);
				}

				ipgRequestResultsDTO.setRequestData(strRequestParams);
				ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());

			} else {
				ipgRequestResultsDTO.setRequestData("");
				ipgRequestResultsDTO.setPaymentBrokerRefNo(new Integer(0));
			}

		}

		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);
		ipgRequestResultsDTO.setPayFort(true);

		log.debug("[PaymentBrokerPayFortPay_AT_StoreECommerceImpl::getRequestData()] End ");
		return ipgRequestResultsDTO;
	}

	/**
	 * Reads the response returns from the PayFort Pay@Store
	 * 
	 * @param encryptedReceiptPay
	 *            the secured response returns from the IPG
	 */
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerPayFortTemplate::getReponseData()] Begin ");

		int tnxResultCode = -1;
		String errorCode = "";
		String status = "";
		String errorSpecification = "";
		int cardType = 5; // generic card type
		String responseMismatch = "";
		String strRequestParams = "";
		XMLGregorianCalendar currentCalendar = null;
		CheckNotificationResponseDTO payFortNotifyingResponse = new CheckNotificationResponseDTO();
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(ipgResponseDTO.getTemporyPaymentId());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		CheckNotificationDTO payFortNotifyingRequest = new CheckNotificationDTO();
		// set map values to the DTO
		payFortNotifyingRequest.setRequest(fields);

		String currentDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, CalendarUtil.getCurrentZuluDateTime());
		currentCalendar = convertStringDateToXmlGregorianCalendar(currentDate, DATE_FORMAT, true);
		payFortNotifyingRequest.setMsgDate(currentCalendar);

		String signature = payFortNotifyingRequest.getCurrency() + "" + payFortNotifyingRequest.getMerchantID() + ""
				+ payFortNotifyingRequest.getOrderID() + "" + payFortNotifyingRequest.getPayment() + ""
				+ payFortNotifyingRequest.getServiceName() + "" + payFortNotifyingRequest.getStatus() + ""
				+ payFortNotifyingRequest.getTicketNumber() + getEncryptionKey();

		String signatureSHA1 = "";
		try {
			signatureSHA1 = SHA1(signature);
		} catch (NoSuchAlgorithmException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		} catch (UnsupportedEncodingException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		payFortNotifyingRequest.setSignature(signatureSHA1);
		log.debug("[PaymentBrokerPayFortTemplate::getReponseData()] ");

		// calling remote web service call
		try {
			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			payFortNotifyingResponse = ebiWebervices.checkNotification(payFortNotifyingRequest);

		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		if (payFortNotifyingResponse != null) {
			if (payFortNotifyingResponse.getStatus().equals(PayFortPaymentUtils.SUCCESS)) {

				postDataMap.put(payFortNotifyingResponse.CURRENCY, payFortNotifyingResponse.getCurrency());
				postDataMap.put(payFortNotifyingResponse.MERCHANT_ID, payFortNotifyingResponse.getMerchantID());
				postDataMap.put(payFortNotifyingResponse.ORDER_ID, payFortNotifyingResponse.getOrderID());
				postDataMap.put(payFortNotifyingResponse.PAYMENT, payFortNotifyingResponse.getPayment());
				postDataMap.put(payFortNotifyingResponse.SIGNATURE, payFortNotifyingResponse.getSignature());
				postDataMap.put(payFortNotifyingResponse.TICKET_NUMBER, (String) fields.get(PayFortPaymentUtils.TICKET_NUMBER));

				strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

				if (checkHashForNotifiction(postDataMap)) {
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
					errorSpecification = "" + responseMismatch;
				} else {
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorCode = VERIFICATION_HASH_ERROR;
					errorSpecification = "Security Validation failed!!!";
					// Different amount in response
					log.error("[PaymentBrokerPayFortTemplate::getReponseData()] - SHA1 signation not satisfy from PayFort PGW");
				}

			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorCode = PAYFORT_RESPONSE_ERROR;
				if (!ipgResponseDTO.isOnholdCreated()) {
					errorCode = payFortNotifyingResponse.getErrorCode();
				}
				errorSpecification = "" + payFortNotifyingResponse.getErrorDesc();

			}
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(oCreditCardTransaction.getTransactionReference());
		ipgResponseDTO.setCardType(cardType);

		updateAuditTransactionByTnxRefNo(oCreditCardTransaction.getTransactionRefNo(), strRequestParams, errorSpecification, "",
				oCreditCardTransaction.getTransactionReference(), tnxResultCode);

		return ipgResponseDTO;
	}

	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {

		log.info("[PaymentBrokerPayFortPay_AT_StoreECommerceImpl::resolvePartialPayments()] Begin");

		CheckOrderResponseDTO checkOrderResponseDTO;
		Map<String, String> receiptyMap = new HashMap<String, String>();
		TempPaymentTnx tmpPayment = null;

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			tmpPayment = ReservationModuleUtils.getReservationBD().loadTempPayment(oCreditCardTransaction.getTemporyPaymentId());
			String orderId = oCreditCardTransaction.getTempReferenceNum();

			receiptyMap.put(PayFortPaymentUtils.CURRENCY, tmpPayment.getPaymentCurrencyCode());
			receiptyMap.put(PayFortPaymentUtils.MERCHANT_ID, getMerchantId());
			receiptyMap.put(PayFortPaymentUtils.PAYMENT, "PAYatSTORE");
			receiptyMap.put(PayFortPaymentUtils.SERVICE_NAME, getServiceName());
			receiptyMap.put(PayFortPaymentUtils.ORDER_ID, orderId);

			try {

				checkOrderResponseDTO = checkTransactionStatus(receiptyMap, oCreditCardTransaction);

				if (checkOrderResponseDTO != null) {
					if (checkOrderResponseDTO.getOrderStatus().equalsIgnoreCase("PROCESSED")) {

						boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);

						if (isResConfirmationSuccess) {
							if (log.isDebugEnabled()) {
								log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					} else if (checkOrderResponseDTO.getOrderStatus().equalsIgnoreCase("PENDING")) {
						// Change State to 'IS'
						// oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
						// log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
					} else if (checkOrderResponseDTO.getOrderStatus().equalsIgnoreCase("EXPIRED")) {
						// Change State to 'IF'
						oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
					} else {
						// No Payments exist
						// Update status to 'II'
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				} else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerPayFortPay_AT_StoreECommerceImpl::resolvePartialPayments()] End");
		return sr;

	}
	
	@Override
	public Properties getProperties() {
		Properties props = super.getProperties();
		props.put("payAtstoreScriptUrl", PlatformUtiltiies.nullHandler(payAtstoreScriptUrl));
		return props;
	}
	
}
