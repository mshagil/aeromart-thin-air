package com.isa.thinair.paymentbroker.core.bl.payFort.v2;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.PayFortOnlineParam;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class PaymentBrokerPayFortOfflineTemplate extends PaymentBrokerPayFortTemplate {

    protected static final Log log = LogFactory.getLog(PaymentBrokerPayFortOfflineTemplate.class);

    protected static final String FIRST_NAME = "firstName";
    protected static final String CARRIER_NAME = "carrierName";
    protected static final String PNR = "pnr";
    protected static final String VALID_PERIOD = "validPeriod";

    private boolean sendEmailNotification;

    @Override
    public void handleDeferredResponse(Map<String, String> receiptMap, IPGResponseDTO ipgResponseDTO,
                                       IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
        log.info("[PaymentBrokerPayFortPayOffline::handleDeferredResponse()] Begin");
        if (!isSignatureValid(receiptMap, getShaResponsePhrase())) {
            log.error("Signature mismatch: " + ipgResponseDTO.getPaymentBrokerRefNo());
            throw new ModuleException("Signature mismatch");
        } else {
            if (isSuccessPurchase(receiptMap)) {
                int temporaryPaymentId = ipgResponseDTO.getTemporyPaymentId();
                CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(temporaryPaymentId);
                ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
                String pnr = oCreditCardTransaction.getTransactionReference();
                LCCClientReservation commonReservation = null;
                Reservation reservation = null;
                String status = "";

			        /* Save Card Transaction response Details */
                Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
                Date now = Calendar.getInstance().getTime();
                String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
                long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);

                oCreditCardTransaction.setComments(strResponseTime);
                oCreditCardTransaction.setResponseTime(timeDiffInMillis);
                oCreditCardTransaction.setTransactionResultCode(1);
                oCreditCardTransaction.setDeferredResponseText(PaymentBrokerUtils.getRequestDataAsString(receiptMap));

                PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

                IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(oCreditCardTransaction.getTransactionRefNo());

                if (ipgQueryDTO != null) {
                     /* Update with payment response details */

                    LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
                    modes.setPnr(ipgQueryDTO.getPnr());
                    modes.setRecordAudit(false);
                    ipgQueryDTO.setPaymentType(getPaymentType(receiptMap));
                    ipgQueryDTO.setCreditCardNo(StringUtils.defaultString(receiptMap.get(PayFortOnlineParam.CARD_NUMBER.getName())));

                    // for dry interline validation fix
                    IPGIdentificationParamsDTO defIPGIdentificationParamsDTO = ipgQueryDTO.getIpgIdentificationParamsDTO();
                    if (defIPGIdentificationParamsDTO.getIpgId() == null) {
                        defIPGIdentificationParamsDTO.setIpgId(ipgResponseDTO.getPaymentBrokerRefNo());
                        ipgQueryDTO.setIpgIdentificationParamsDTO(defIPGIdentificationParamsDTO);
                    }

                    /** NO RESERVATION OR CANCELED RESERVATION */
                    if (!isReservationExist(ipgQueryDTO)
                            || ReservationInternalConstants.ReservationStatus.CANCEL.equals(ipgQueryDTO.getPnrStatus())) {

                        /**
                         * When confirming a reservation made from another carrier reservation does not exists in own
                         * reservation table , therefore we need to load the reservation through LCC and clarify reservation
                         * exists or not
                         **/
                        LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr,
                                PaymentBrokerPayFortUtils.getApplicationEngine(oCreditCardTransaction));
                        commonReservation = AirproxyModuleUtils.getAirproxyReservationBD()
                                .searchReservationByPNR(pnrModesDTO, null, getTrackingInfo(ipgQueryDTO));
                        status = commonReservation.getStatus();

                        if (StringUtils.isEmpty(status) || ReservationInternalConstants.ReservationStatus.CANCEL.equals(status)) {

                            log.info("[handleDeferredResponse()] Refunding Payment -> No Reservation has been created.");
                            ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);

                        } else {
                            ipgQueryDTO.setPnr(commonReservation.getPNR());
                            ipgQueryDTO.setGroupPnr(commonReservation.isGroupPNR() ? commonReservation.getPNR() : null);
                            ipgQueryDTO.setPnrStatus(commonReservation.getStatus());
                        }
                    }

                    /** ONHOLD RESERVATION */
                    if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())) {

                        if (commonReservation == null) {
                            try {
                                reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
                                status = reservation.getStatus();
                            } catch (ModuleException e) {
                                log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
                            }
                        }

                        if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(status)) {
                            // This needs to handled when query operation is available from payFort
                            //if (isSuccessPaymentExists(oCreditCardTransaction)) {
                            //}
                            if (ConfirmReservationUtil
                                    .isReservationConfirmationSuccess(ipgQueryDTO)) {
                                log.info("Reservation confirmation successful:" + ipgResponseDTO.getPaymentBrokerRefNo());
                            } else {
                                log.debug("[handleDeferredResponse()] Refunding Payment -> Onhold confirmation failed.");
                                throw new ModuleException("Onhold confirmation failed:" + pnr);
                            }

                        }
                    }

                    /** ALREADY CONFIRMED RESERVATION */
                    if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(ipgQueryDTO.getPnrStatus())) {

                        if (reservation == null && commonReservation == null) {
                            try {
                                reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
                                status = reservation.getStatus();
                            } catch (ModuleException e) {
                                log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
                            }
                        }

                        TempPaymentTnx tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(temporaryPaymentId);

                        if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(status)
                                && "RS".equals(tempPaymentTnx.getStatus())) {
                            log.debug("[handleDeferredResponse()] Confirm Payment -> Reservation already confirmed :" + pnr);

                        } else {
                            log.debug("[handleDeferredResponse()] Confirm Payment -> confirm by another payment gateway :" + pnr);
                            throw new ModuleException("confirm by another payment gateway :" + pnr);
                        }
                    }

                } else {
                    log.error("PaymentBrokerPayFortPayOffline temp transaction not found:" + ipgResponseDTO.getPaymentBrokerRefNo());
                    throw new ModuleException("Duplicate Receipt....");
                }
            } else {
                log.error("PaymentBrokerPayFortPayOffline ststus change is not a success purchase");
                throw new ModuleException("Purchase notification status is not success");
            }
        }
        log.info("[PaymentBrokerPayFortTemplate::handleDeferredResponse()] End");
    }

    private static LCCClientPnrModesDTO getPnrModesDTO(String pnr, ApplicationEngine appIndicator) {
        LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
        pnrModesDTO.setPnr(pnr);
        pnrModesDTO.setLoadFares(true);
        pnrModesDTO.setLoadLastUserNote(false);
        pnrModesDTO.setLoadLocalTimes(false);
        pnrModesDTO.setLoadOndChargesView(true);
        pnrModesDTO.setLoadPaxAvaBalance(true);
        pnrModesDTO.setLoadSegView(true);
        pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
        pnrModesDTO.setLoadSegViewBookingTypes(true);
        pnrModesDTO.setRecordAudit(true);
        pnrModesDTO.setLoadSeatingInfo(true);
        pnrModesDTO.setLoadSSRInfo(true);
        pnrModesDTO.setLoadSegViewReturnGroupId(true);
        pnrModesDTO.setLoadPaxAvaBalance(true);
        pnrModesDTO.setLoadMealInfo(true);
        pnrModesDTO.setLoadBaggageInfo(true);
        pnrModesDTO.setAppIndicator(appIndicator);
        pnrModesDTO.setLoadExternalPaxPayments(true);
        pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
        pnrModesDTO.setLoadEtickets(true);
        return pnrModesDTO;
    }

    private static TrackInfoDTO getTrackingInfo(IPGQueryDTO ipgQueryDTO) {
        TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
        trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
        trackInfoDTO.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
        trackInfoDTO.setCallingInstanceId(SystemPropertyUtil.getInstanceId());
        return trackInfoDTO;
    }

    public boolean isSendEmailNotification() {
        return sendEmailNotification;
    }

    public void setSendEmailNotification(boolean sendEmailNotification) {
        this.sendEmailNotification = sendEmailNotification;
    }
}
