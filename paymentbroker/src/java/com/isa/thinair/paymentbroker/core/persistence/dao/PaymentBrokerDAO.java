/**
 * 
 */
package com.isa.thinair.paymentbroker.core.persistence.dao;


import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.CountryPaymentCardBehaviourDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;

/**
 * @author Srikantha
 * 
 */
public interface PaymentBrokerDAO {
	/**
	 * 
	 * @param mapResult
	 */
	public int composeTransaction(Map mapResult, int temporyPaymentId, String serviceType, String paymentGatewayName);

	/**
	 * @param transactionNM
	 */
	public void saveTransaction(CreditCardTransaction transactionNM);

	/**
	 * @param transactionRefNo
	 * @return CreditCardTransaction
	 */
	public CreditCardTransaction loadTransaction(int transactionRefNo);

	/**
	 * Load all the card transactions related to a reservation against temporary payment id
	 * 
	 * @param transactionRefNo
	 * @return
	 */
	public Map<Integer, CreditCardTransaction> loadTransactionMap(String transactionRefNo);

	/**
	 * Load the Transaction by Tempory Payment Id
	 * 
	 * @param temporyPayId
	 * @return
	 */
	public CreditCardTransaction loadTransactionByTempPayId(int temporyPayId);

	/**
	 * Load the Transaction by transacionRefference
	 * 
	 * @param temporyPayId
	 * @return
	 */
	public CreditCardTransaction loadTransactionByReference(String transacionRefference);

	/**
	 * Load the Transaction by Merchant transaction id
	 * 
	 * @param merchantTnxId
	 * @return
	 */
	public CreditCardTransaction loadTransactionByMerchantTransId(String merchantTnxId);

	/**
	 * The payment gateway available for a currency is retrieved
	 * 
	 * @param ipgPaymentOptionDTO
	 * @return
	 */
	public List getPaymentGateway(IPGPaymentOptionDTO ipgPaymentOptionDTO);

	/**
	 * Returns the temporary transaction ID
	 * @param PNR
	 * @param paymentGateway
	 * @param transactionStatus
	 * @return
	 */
	public Integer getLatestTempTransactionID(String PNR, String paymentGateway, String transactionStatus);

	/**
	 * The default payment gateway available is retrieved
	 * 
	 * @param
	 * @return
	 */
	public List getDefaultCurrencyPaymentGateway(Integer ipgId);

	/**
	 * The payment gateway for the given currency, if not default currency payment gateways
	 * 
	 * @param ipgPaymentOptionDTO
	 * @return List
	 * @throws ModuleException 
	 */
	public List<IPGPaymentOptionDTO> getPaymentGateways(IPGPaymentOptionDTO ipgPaymentOptionDTO) throws ModuleException;

	/**
	 * The default payment gateway available is retrieved
	 * 
	 * @param
	 * @return
	 */
	public List getDefaultCurrencyPaymentGateway(String modeulcode, Integer ipgId, boolean isOfflinePayment);

	/**
	 * Returns the payment having support for given currency or not.
	 * 
	 * @param
	 * @return
	 */
	public boolean isPaymentGatewaySupportsCurrency(Integer ipgId, String currencyCode);

	/**
	 * Returns the payment having support for given currency or not.
	 * 
	 * @param module
	 * 
	 * @param
	 * @return
	 */
	public boolean isPaymentGatewaySupportedCurrency(String currencyCode, ApplicationEngine module);

	/**
	 * Returns payment gateway currency for an given transaction
	 * 
	 * @param isOwnTnx
	 *            TODO
	 * @param
	 * @return
	 */
	public String getPaymentGatewayNameForCCTransaction(Integer tnxId, boolean isOwnTnx);

	public String getPaymentGatewayNameForLCCCCTnx(Integer tptId);

	public boolean isBinPrefixExists(String ccNo);

	/**
	 * Currency Numeric code(4217 ISO standard) from currency code.
	 * 
	 * @param
	 * @return ipgPaymentOptionDTO
	 */
	public String getCurrencyNumericCode(String code, Integer ipgId);

	/**
	 * 
	 * @param paymentGatewayID
	 * @return
	 */
	public List<CardDetailConfigDTO> getPaymentGatewayCardConfigData(String paymentGatewayID);

	public Collection getContryPaymentCardBehavior(String countryCode, Collection paymentGatewayIDs);

	public Collection<CountryPaymentCardBehaviourDTO> getONDWiseCountryCardBehavior(String ondCode, Collection paymentGatewayIDs)  throws ModuleException ;

	public Collection getCreditCardsListForAgent(Collection paymentGatewayIDs);

	/**
	 * Returns tempory payment information
	 * 
	 * @param tnxId
	 * @return
	 */
	public TempPaymentTnx getTempPaymentInfo(int tnxId);

	/**
	 * Return payment methods icon names
	 * 
	 * @param countryCode
	 * @return
	 * @throws ModuleException 
	 */
	public Set<String> getPaymentMethodNames(PaymentMethodDetailsDTO methodDetailsDTO) throws ModuleException;

	/**
	 * Return payment gateway configs to a given provider name (i.e Ogone)
	 * 
	 * @param providerName
	 * @return List<IPGPaymentOptionDTO>
	 */
	public List<IPGPaymentOptionDTO> getActivePaymentGatewayByProviderName(String providerName);
	
	public boolean isDuplicateTransactionExists(String paramString);

	public boolean isDuplicateBankTransactionExists(String bankTnxId);

	public boolean isCreditCardFeeFromPGW(Integer ipgId);
	
	public String getPaymentGatewayNameFromCCTransaction(int temporyPayId);
	
	public List getDefaultCurrencyPaymentGatewayForAccountTab(Integer ipgId);

	public CreditCardTransaction loadOfflineLatestTransactionByReference(String transactionReference);
	
	public CreditCardTransaction loadOfflineConfirmedTransactionByReference(String transactionReference);
	
	public BigDecimal getPaymentAmountFromTPTId(Integer tempTnxId);
	
	public boolean updateTempPaymentEntryForCancellingInvoice(Integer tempTnxId);
}
