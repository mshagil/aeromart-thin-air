package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public class RefFatouratiWSProxy implements com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWS {
  private String _endpoint = null;
  private com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWS refFatouratiWS = null;
  
  public RefFatouratiWSProxy() {
    _initRefFatouratiWSProxy();
  }
  
  public RefFatouratiWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initRefFatouratiWSProxy();
  }
  
  private void _initRefFatouratiWSProxy() {
    try {
      refFatouratiWS = (new com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWSServiceLocator()).getRefFatouratiWSPort();
      if (refFatouratiWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)refFatouratiWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)refFatouratiWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (refFatouratiWS != null)
      ((javax.xml.rpc.Stub)refFatouratiWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWS getRefFatouratiWS() {
    if (refFatouratiWS == null)
      _initRefFatouratiWSProxy();
    return refFatouratiWS;
  }
  
  public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenRes genRefFatourati(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReq arg0) throws java.rmi.RemoteException{
    if (refFatouratiWS == null)
      _initRefFatouratiWSProxy();
    return refFatouratiWS.genRefFatourati(arg0);
  }
  
  
}