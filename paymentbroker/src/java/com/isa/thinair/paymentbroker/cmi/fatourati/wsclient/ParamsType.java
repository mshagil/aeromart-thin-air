/**
 * ParamsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public class ParamsType  implements java.io.Serializable {
    /* nom du champ */
    private java.lang.String libelle;

    /* Libelle en cas d’affichage */
    private java.lang.String nomChamp;

    /* La valeur du champ */
    private java.lang.String valeurChamp;

    public ParamsType() {
    }

    public ParamsType(
           java.lang.String libelle,
           java.lang.String nomChamp,
           java.lang.String valeurChamp) {
           this.libelle = libelle;
           this.nomChamp = nomChamp;
           this.valeurChamp = valeurChamp;
    }


    /**
     * Gets the libelle value for this ParamsType.
     * 
     * @return libelle   * nom du champ
     */
    public java.lang.String getLibelle() {
        return libelle;
    }


    /**
     * Sets the libelle value for this ParamsType.
     * 
     * @param libelle   * nom du champ
     */
    public void setLibelle(java.lang.String libelle) {
        this.libelle = libelle;
    }


    /**
     * Gets the nomChamp value for this ParamsType.
     * 
     * @return nomChamp   * Libelle en cas d’affichage
     */
    public java.lang.String getNomChamp() {
        return nomChamp;
    }


    /**
     * Sets the nomChamp value for this ParamsType.
     * 
     * @param nomChamp   * Libelle en cas d’affichage
     */
    public void setNomChamp(java.lang.String nomChamp) {
        this.nomChamp = nomChamp;
    }


    /**
     * Gets the valeurChamp value for this ParamsType.
     * 
     * @return valeurChamp   * La valeur du champ
     */
    public java.lang.String getValeurChamp() {
        return valeurChamp;
    }


    /**
     * Sets the valeurChamp value for this ParamsType.
     * 
     * @param valeurChamp   * La valeur du champ
     */
    public void setValeurChamp(java.lang.String valeurChamp) {
        this.valeurChamp = valeurChamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ParamsType)) return false;
        ParamsType other = (ParamsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.libelle==null && other.getLibelle()==null) || 
             (this.libelle!=null &&
              this.libelle.equals(other.getLibelle()))) &&
            ((this.nomChamp==null && other.getNomChamp()==null) || 
             (this.nomChamp!=null &&
              this.nomChamp.equals(other.getNomChamp()))) &&
            ((this.valeurChamp==null && other.getValeurChamp()==null) || 
             (this.valeurChamp!=null &&
              this.valeurChamp.equals(other.getValeurChamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLibelle() != null) {
            _hashCode += getLibelle().hashCode();
        }
        if (getNomChamp() != null) {
            _hashCode += getNomChamp().hashCode();
        }
        if (getValeurChamp() != null) {
            _hashCode += getValeurChamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ParamsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "paramsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("libelle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "libelle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomChamp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomChamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valeurChamp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valeurChamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
