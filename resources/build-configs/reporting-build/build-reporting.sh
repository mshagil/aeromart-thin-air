#!/bin/sh

builddir=/usr/isadata/build/reporting
distdir=$builddir/dist
antlog=$builddir/logs/ant.log
log=$builddir/logs/log.log

replacedir=$builddir/toreplace

if [ -e "$antlog" ]; then
	rm -f $antlog
fi

if [ -e "$log" ]; then
    rm -f $log
fi

if [ -e "$distdir" ]; then
	rm -rf $distdir
	mkdir $distdir
fi

#prepare backend
cvs -d :pserver:builduser:password@10.200.2.11:/cvsroot/isa co ThinAir

cp -f $replacedir/MenuData.xml ThinAir/airadmin/src/web/WEB-INF/


cd ThinAir
ant prepare-dist-jars >> $antlog 2>&1

ant dist-reporting-ear >> $antlog 2>&1
ant -Dproject.config.dir=$distdir/isaconfig build-config-repository-for-reporting-ear >> $antlog 2>&1
cp target/dist/aa-reporting.ear $distdir
ant -Dproject.config.dir=$distdir/isaconfig copy-templates >> $antlog 2>&1

echo "Done" >> $log 2>&1
echo "Done"
exit 0
