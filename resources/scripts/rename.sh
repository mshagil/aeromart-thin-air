#!/bin/bash

filePath="$1"
originalString="$2"
newString="$3"
filterString2="$4"
ignoreString='liquidstamp'
filterString='jpg|gif|png|css'



if [[ "${filePath}x" != 'x' && -f "$filePath" ]]; then

	basePath=$(dirname "$filePath")
	
	if [[ "${basePath}x" != 'x' && -d "$basePath" ]]; then
		
		fileName=${filePath##*/}

		if [[ "${fileName}x" != 'x' ]]; then

			retval=$(echo $fileName | egrep -iq "*$originalString\.($filterString)$"; echo $?)
			
			if [[ $retval -eq 0 ]]; then
				
				retval=$(echo "$fileName" | egrep -q "^($ignoreString)"; echo $?)
				
				if [[ $retval -ne 0 ]]; then
					
					updatedFileName=$(echo $fileName | sed "s/$originalString.\($filterString2\)$/$newString.\1/g")
					cd $basePath
					mv $fileName ${updatedFileName}
				fi
			fi
		fi
	fi
fi
