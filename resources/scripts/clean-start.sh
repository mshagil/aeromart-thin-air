#!/bin/sh

JBOSS_HOME=/usr/jboss-4.0.1sp1app
JBOSS_SERVER=$JBOSS_HOME/server/all
timestamp=`date '+%Y%m%d_%H%M'`

if [ -e "nohup.out" ]; then
        mv nohup.out nohup-$timestamp-68.out                    
        zip nohup-$timestamp-68.out.zip nohup-$timestamp-68.out
        rm -f nohup-$timestamp-68.out
fi

if [ -e "$JBOSS_SERVER/tmp" ]; then
	rm -rf $JBOSS_SERVER/tmp
fi

if [ -e "$JBOSS_SERVER/work" ]; then
	rm -rf $JBOSS_SERVER/work
fi

nohup $JBOSS_HOME/bin/run.sh -c all
