import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Configuration Utilities for AccelAero
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ConfigUtils {

	/** Holds the FIRST INDEX OF VALUE */
	public static final String FIRST_INDEX = "<value>";

	/** Holds the LAST INDEX OF VALUE */
	public static final String LAST_INDEX = "</value>";

	/**
	 * Returns the Configuration file
	 * 
	 * @param dir
	 * @param element
	 * @return
	 */
	public static File getFileName(File dir, String element, String text) {
		File file = new File(dir, element);

		if (file.isDirectory()) {
			String[] children = file.list();
			for (int i = 0; i < children.length; i++) {
				if (children[i].indexOf(text) != -1) {
					return new File(dir + "/" + element, children[i]);
				}
			}

			throw new NullPointerException();

		} else {
			return file;
		}
	}

	/**
	 * Return Contents
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static Collection getContents(File file) throws Exception {
		System.out.println(" [+] EDITED " + file.getName());
		return readFile(file);
	}

	/**
	 * Process Operation
	 * 
	 * @param file
	 * @param contents
	 * @throws Exception
	 */
	public static void process(File file, Object[] contents) throws Exception {
		String oldFileName = file.getAbsolutePath();
		String newFileName = file.getAbsolutePath() + "$$TEMP$$";
		writeFile(newFileName, contents);
		deleteFile(file);
		renameFile(new File(newFileName), new File(oldFileName));
	}

	/**
	 * Delete File
	 * 
	 * @param file
	 */
	private static void deleteFile(File file) {
		boolean success = file.delete();

		if (!success) {
			throw new NullPointerException();
		}
	}

	/**
	 * Read a file
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private static Collection readFile(File file) throws IOException {
		Collection elements = new ArrayList();
		BufferedReader in = new BufferedReader(new FileReader(file));
		String str;
		while ((str = in.readLine()) != null) {
			elements.add(str);
		}
		in.close();

		return elements;
	}

	/**
	 * Rename the file
	 * 
	 * @param oldFile
	 * @param newFile
	 */
	private static void renameFile(File oldFile, File newFile) {
		boolean success = oldFile.renameTo(newFile);
		if (!success) {
			throw new NullPointerException();
		}
	}

	/**
	 * Creates a file
	 * 
	 * @param fileName
	 * @param contents
	 * @throws IOException
	 */
	private static void writeFile(String fileName, Object[] contents) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
		for (int i = 0; i < contents.length; i++) {
			if (i > 0) {
				out.newLine();
			}
			out.write(contents[i].toString());
		}
		out.close();
	}

	/**
	 * Returns the Build version
	 * 
	 * @return
	 */
	public static String getBuildVersion() {
		Format formatter = new SimpleDateFormat("yyyyMMddHHmm");
		return formatter.format(new Date());
	}

	/**
	 * Deletes all files and subdirectories under dir. Returns true if all deletions were successful. If a deletion
	 * fails, the method stops attempting to delete and returns false.
	 * 
	 * @param dir
	 * @return
	 */
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		// The directory is now empty so delete it
		return dir.delete();
	}
}
