/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.research.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Nasly
 * @hibernate.class table = "t_sampleobject"
 */
public class ResearchOjbect extends Persistent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6959511557061037496L;
	private String id;
	private String message;

	/**
	 * 
	 * @hibernate.id column = "ID" generator-class = "assigned"
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @hibernate.property column = "MESSAGE"
	 */
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
