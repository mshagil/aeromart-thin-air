/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.research.core.persistence.hibernate;

import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.research.api.model.ResearchOjbect;
import com.isa.thinair.research.core.persistence.dao.IResearchDAO;

/**
 * @author Nasly
 * @isa.module.dao-impl dao-name="researchDAO"
 */
public class ResearchDAO extends PlatformHibernateDaoSupport implements IResearchDAO {
	public ResearchDAO() {
	}

	public void saveOrUpdate(ResearchOjbect obj) {
		hibernateSave(obj);
	}

	public void delete(String primaryKey) {
		ResearchOjbect obj = new ResearchOjbect();
		obj.setId(primaryKey);
		delete(obj);
	}

}
