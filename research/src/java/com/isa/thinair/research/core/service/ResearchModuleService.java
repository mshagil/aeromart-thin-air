/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.research.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Nasly
 * 
 *         SampleModule's service interface
 * @isa.module.service-interface module-name="researchmodule" description="research module"
 */
public class ResearchModuleService extends DefaultModule {
}
