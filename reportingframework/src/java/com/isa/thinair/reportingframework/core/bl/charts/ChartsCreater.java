/**
 * 
 */
package com.isa.thinair.reportingframework.core.bl.charts;

import java.sql.ResultSet;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import com.isa.thinair.reportingframework.api.dto.BarChartDTO;
import com.isa.thinair.reportingframework.api.dto.CategoryChartCommonDTO;
import com.isa.thinair.reportingframework.api.dto.ChartCommonsDTO;
import com.isa.thinair.reportingframework.api.dto.LineChartDTO;
import com.isa.thinair.reportingframework.api.dto.PieChartDTO;
import com.isa.thinair.reportingframework.core.commons.exception.ReportingException;
import com.isa.thinair.reportingframework.core.util.ChartConstants.CHART_DATA_SET_TYPES;
import com.isa.thinair.reportingframework.core.util.ChartConstants.MAIN_CHART_TYPES;

/**
 * Creates 2D and 3D Charts
 * 
 * @author Navod Ediriweera
 * @since Jul 09, 2009
 */
public class ChartsCreater {

	/**
	 * Creates a 3D PieChart
	 * 
	 * @param pieChartDTO
	 * @return
	 */
	public static Chart createPieChart3D(PieChartDTO pieChartDTO) throws ReportingException {

		JFreeChart jFreeChart = ChartFactory.createPieChart3D(pieChartDTO.getTitle() == null ? "" : pieChartDTO.getTitle(),
				ChartDataParser.createPieChartDataSet(pieChartDTO.getChartResultSet()), pieChartDTO.isShowLegend(),
				pieChartDTO.isEnableToolTips(), pieChartDTO.isGenerateURL());
		jFreeChart = modifyChart(jFreeChart, pieChartDTO);// modifing chart

		Chart chart = new Chart();
		chart.setJFreeChart(jFreeChart);
		return chart;
	}

	/**
	 * Creates a 2D Pie Chart
	 * 
	 * @param chartDTO
	 * @return
	 */
	public static Chart createPieChart2D(PieChartDTO pieChartDTO) throws ReportingException {
		JFreeChart jFreeChart = ChartFactory.createPieChart(pieChartDTO.getTitle() == null ? "" : pieChartDTO.getTitle(),
				ChartDataParser.createPieChartDataSet(pieChartDTO.getChartResultSet()), pieChartDTO.isShowLegend(),
				pieChartDTO.isEnableToolTips(), pieChartDTO.isGenerateURL());

		jFreeChart = modifyChart(jFreeChart, pieChartDTO);// modifing chart

		Chart chart = new Chart();
		chart.setJFreeChart(jFreeChart);
		return chart;
	}

	/**
	 * Creates a 3D Bar Chart
	 * 
	 * @param barChartDTO
	 * @return
	 */
	public static Chart createBarChart3D(BarChartDTO barChartDTO) throws ReportingException {
		JFreeChart jFreeChart = null;
		DefaultCategoryDataset categoryDataset = getBarAndLineChartData(barChartDTO.getChartResultSet(),
				barChartDTO.getResultSetType());

		jFreeChart = ChartFactory.createBarChart3D(barChartDTO.getTitle() == null ? "" : barChartDTO.getTitle(), barChartDTO
				.getXAxisName(), barChartDTO.getYAxisName(), categoryDataset,
				barChartDTO.getPlotOrientation() == null ? PlotOrientation.VERTICAL : barChartDTO.getPlotOrientation(),
				barChartDTO.isShowLegend(), barChartDTO.isEnableToolTips(), barChartDTO.isGenerateURL());
		jFreeChart = modifyChart(jFreeChart, barChartDTO);// modifing chart

		Chart chart = new Chart();
		chart.setJFreeChart(jFreeChart);
		return chart;
	}

	/**
	 * Creates a 2D bar chart
	 * 
	 * @param barChartDTO
	 * @return
	 */
	public static Chart createBarChart2D(BarChartDTO barChartDTO) throws ReportingException {
		JFreeChart jFreeChart = null;
		DefaultCategoryDataset categoryDataset = getBarAndLineChartData(barChartDTO.getChartResultSet(),
				barChartDTO.getResultSetType());

		jFreeChart = ChartFactory.createBarChart(barChartDTO.getTitle() == null ? "" : barChartDTO.getTitle(), barChartDTO
				.getXAxisName(), barChartDTO.getYAxisName(), categoryDataset,
				barChartDTO.getPlotOrientation() == null ? PlotOrientation.VERTICAL : barChartDTO.getPlotOrientation(),
				barChartDTO.isShowLegend(), barChartDTO.isEnableToolTips(), barChartDTO.isGenerateURL());

		jFreeChart = modifyChart(jFreeChart, barChartDTO);// modifing chart
		Chart chart = new Chart();
		chart.setJFreeChart(jFreeChart);
		return chart;
	}

	/**
	 * Creates a 2D Line Chart
	 * 
	 * @param lineChartDTO
	 * @return
	 */
	public static Chart createLineChart2D(LineChartDTO lineChartDTO) throws ReportingException {
		DefaultCategoryDataset categoryDataset = getBarAndLineChartData(lineChartDTO.getChartResultSet(),
				lineChartDTO.getResultSetType());
		JFreeChart jchart = ChartFactory.createLineChart(lineChartDTO.getTitle() == null ? "" : lineChartDTO.getTitle(), // chart
																															// title
				lineChartDTO.getXAxisName() == null ? "" : lineChartDTO.getXAxisName(), // domain axis label
				lineChartDTO.getYAxisName() == null ? "" : lineChartDTO.getYAxisName(), // range axis label
				categoryDataset, // data
				lineChartDTO.getPlotOrientation() == null ? PlotOrientation.VERTICAL : lineChartDTO.getPlotOrientation(),// orientation
				lineChartDTO.isShowLegend(), // include legend
				lineChartDTO.isEnableToolTips(), // tooltips
				lineChartDTO.isGenerateURL() // urls
				);

		jchart = modifyChart(jchart, lineChartDTO);// modifing chart

		Chart chart = new Chart();
		chart.setJFreeChart(jchart);
		return chart;
	}

	/**
	 * Create LineChart 3D
	 * 
	 * @param lineChartDTO
	 * @return
	 */
	public static Chart createLineChart3D(LineChartDTO lineChartDTO) throws Exception {
		DefaultCategoryDataset categoryDataset = getBarAndLineChartData(lineChartDTO.getChartResultSet(),
				lineChartDTO.getResultSetType());

		JFreeChart jchart = ChartFactory.createLineChart3D(lineChartDTO.getTitle() == null ? "" : lineChartDTO.getTitle(), // chart
																															// title
				lineChartDTO.getXAxisName() == null ? "" : lineChartDTO.getXAxisName(), // domain axis label
				lineChartDTO.getYAxisName() == null ? "" : lineChartDTO.getYAxisName(), // range axis label
				categoryDataset, // data
				lineChartDTO.getPlotOrientation() == null ? PlotOrientation.VERTICAL : lineChartDTO.getPlotOrientation(),// orientation
				lineChartDTO.isShowLegend(), // include legend
				lineChartDTO.isEnableToolTips(), // tooltips
				lineChartDTO.isGenerateURL() // urls
				);

		jchart = modifyChart(jchart, lineChartDTO);// modifing chart

		Chart chart = new Chart();
		chart.setJFreeChart(jchart);
		return chart;
	}

	/************************************************************************/
	/**
	 * Returns Bar and Line Chart data according to the result set type.
	 */
	private static DefaultCategoryDataset getBarAndLineChartData(ResultSet rs, String rsType) throws ReportingException {
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();
		if (rsType == null) {
			throw new ReportingException("Invalid Chart ResultSet Type");
		}
		if (rsType.equals(CHART_DATA_SET_TYPES.THREE_COLUMN_DATA)) {
			categoryDataset = ChartDataParser.createThreeColumnDCDataSet(rs);
		} else if (rsType.equals(CHART_DATA_SET_TYPES.N_COLUMN_DATA)) {
			categoryDataset = ChartDataParser.createDCDataSetFrom_N_Columns(rs);
		} else {
			throw new ReportingException("Invalid Chart ResultSet Type");
		}
		return categoryDataset;
	}

	/**
	 * Modifiy Chart Display properties
	 */
	private static JFreeChart modifyChart(JFreeChart jFreeChart, ChartCommonsDTO chartCommonsDTO) {

		if (chartCommonsDTO.getBackGroundPaint() != null) {
			jFreeChart.setBackgroundPaint(chartCommonsDTO.getBackGroundPaint());
		}
		if (chartCommonsDTO.getBoarderPaint() != null) {
			jFreeChart.setBorderPaint(chartCommonsDTO.getBoarderPaint());
		}
		Plot plot = jFreeChart.getPlot();
		plot.setForegroundAlpha(chartCommonsDTO.getForeGroundAlpha());
		if (chartCommonsDTO.getPlotBackGroundPaint() != null) {
			plot.setBackgroundPaint(chartCommonsDTO.getPlotBackGroundPaint());
		}
		// setting PieChart (PiePlot) Specific Data
		if (chartCommonsDTO.getChartMainType() != null && chartCommonsDTO.getChartType().equals(MAIN_CHART_TYPES.PIE_CHART)) {
			PieChartDTO pieChartDTO = (PieChartDTO) chartCommonsDTO;
			PiePlot piePlot = (PiePlot) jFreeChart.getPlot();
			piePlot.setStartAngle(pieChartDTO.getStartAngle());
			piePlot.setInteriorGap(pieChartDTO.getInteriorGap());
		}
		// setting Rendere for Category charts
		if (chartCommonsDTO.getChartMainType() != null
				&& chartCommonsDTO.getChartMainType().equals(MAIN_CHART_TYPES.CATEGORY_CHART)) {
			CategoryChartCommonDTO categoryChartCommonDTO = (CategoryChartCommonDTO) chartCommonsDTO;
			CategoryPlot categoryPlot = (CategoryPlot) plot;
			if (!categoryChartCommonDTO.isShowDecimalsYAxis()) {
				// Removing the decimal point from the Range (Y) axis
				NumberAxis rangeAxis = (NumberAxis) categoryPlot.getRangeAxis();
				rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			}
			if (categoryChartCommonDTO.getRendere() != null) { // Setting Rendere
				categoryPlot.setRenderer(categoryChartCommonDTO.getRendere());
			}
		}
		return jFreeChart;
	}
}
