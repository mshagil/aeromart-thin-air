/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.reportingframework.core.bl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.servlet.ServletResponse;
import javax.sql.DataSource;

import com.isa.thinair.reportingframework.api.dto.ReportParam;

import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.reportingframework.core.commons.exception.ReportingException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * Reporting Manager to locate report templates and create various types of reports
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReportingManager {
	/** Holds the logger instance */
	private static final Log log = LogFactory.getLog(ReportingManager.class);

	/** Holds the template actual name and the reference name */
	private static Properties properties = new Properties();

	/**
	 * Puposely avoided to create the object
	 */
	private ReportingManager() {
	}

	/**
	 * Store compiled ireport templates
	 * 
	 * @param templateReferenceName
	 *            template reference name
	 * @param templateName
	 *            actual file name of the template (.jrxml file)
	 * @throws ReportingException
	 */
	public static void storeIreportTemplateRefs(String templateReferenceName, String iReportTemplateName)
			throws ReportingException {
		log.debug("Inside storeIreportTemplateRefs");

		JasperReport jasperReport = null;

		try {
			jasperReport = JasperCompileManager.compileReport(iReportTemplateName);
		} catch (JRException e) {
			log.error("Can not compile template name : " + iReportTemplateName);
			ReportingException reportingException = new ReportingException(" Can not compile template name ... "
					+ iReportTemplateName, e);
			throw reportingException;
		}

		storeTemplate(templateReferenceName, jasperReport);
		log.debug("Exit storeIreportTemplateRefs");
	}

	/**
	 * Store jasper report templates
	 * 
	 * @param templateReferenceName
	 *            template reference name
	 * @param templateName
	 *            actual file name of the template (.jasper file)
	 * @throws ReportingException
	 */
	public static void storeJasperTemplateRefs(String templateReferenceName, String jasperTemplateName) throws ReportingException {
		log.debug("Inside storeJasperTemplateRefs");

		JasperReport jasperReport = null;

		try {
			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(jasperTemplateName);
		} catch (JRException e) {
			log.error("Can not load the template name : " + jasperTemplateName, e);
			ReportingException reportingException = new ReportingException("Can not load the template name : "
					+ jasperTemplateName, e);
			throw reportingException;
		}

		storeTemplate(templateReferenceName, jasperReport);
		log.debug("Exit storeJasperTemplateRefs");
	}

	/**
	 * Stores the templates in the memory
	 * 
	 * @param templateReferenceName
	 * @param jasperReport
	 */
	private static void storeTemplate(String templateReferenceName, JasperReport jasperReport) {
		log.debug("Inside storeTemplate");
		properties.put(templateReferenceName, jasperReport);
		log.debug("Exit storeTemplate");
	}

	/**
	 * Create HTML Report
	 * 
	 * @param templateName
	 *            the template reference name
	 * @param parameters
	 *            parameters that needs to be passed to the report
	 * @param resultSet
	 *            data that needs to passed to the report
	 * @param imagesMap
	 *            images that need to the report
	 * @param imagesURI
	 *            URI of the report images
	 * @param writer
	 *            write for the report
	 * @param outputStream
	 *            output stream of the report
	 * @throws ReportingException
	 */
	public static byte[] createHTMLReport(String templateName, Map<String,Object> parameters, ResultSet resultSet, Map<Object, Object> imagesMap,
			String imagesURI, ServletResponse servletResponse) throws ReportingException {
		log.debug("Inside createHTMLReport");

		JasperPrint jasperPrint = constructReport(templateName, parameters, resultSet);
		byte[] byteStream = ExportManager.htmlExport(servletResponse, jasperPrint, imagesMap, imagesURI);

		log.debug("Exit createHTMLReport");
		return byteStream;
	}

	public static byte[] createHTMLReport(String templateName, Map<String,Object> parameters, ResultSet resultSet,
			Map<Object, Object> imagesMap, String imagesURI) throws ReportingException {
		log.debug("Inside createHTMLReport");

		JasperPrint jasperPrint = constructReport(templateName, parameters, resultSet);
		byte[] byteStream = ExportManager.htmlExport(jasperPrint, imagesMap, imagesURI);

		log.debug("Exit createHTMLReport");
		return byteStream;
	}

	/**
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param imagesMap
	 * @param imagesURI
	 * @param servletResponse
	 * @return
	 * @throws ReportingException
	 */
	public static byte[] createHTMLReport(String templateName, Map<String,Object> parameters, Collection<?> colData, Map<Object, Object> imagesMap,
			String imagesURI, ServletResponse servletResponse) throws ReportingException {
		log.debug("Inside createHTMLReport");

		JasperPrint jasperPrint = constructReport(templateName, parameters, colData);
		byte[] byteStream = ExportManager.htmlExport(servletResponse, jasperPrint, imagesMap, imagesURI);

		log.debug("Exit createHTMLReport");
		return byteStream;
	}

	/**
	 * Creates a PDF report
	 * 
	 * @param templateName
	 *            template reference name
	 * @param parameters
	 *            parameters that need to send to the report
	 * @param resultSet
	 *            data set for the report
	 * @param writer
	 *            writer for the report
	 * @param outputStream
	 *            output stream for the report
	 * @throws ReportingException
	 */
	public static byte[]
			createPDFReport(String templateName, Map<String,Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
					throws ReportingException {
		log.debug("Inside createPDFReport");
		
		JasperPrint jasperPrint = constructReport(templateName, parameters, resultSet);
		byte[] byteStream = ExportManager.pdfExport(servletResponse, jasperPrint);

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	/**
	 * Creates a PDF report
	 * 
	 * @param templateName
	 *            template reference name
	 * @param parameters
	 *            parameters that need to send to the report
	 * @param resultSet
	 *            data set for the report
	 * @param writer
	 *            writer for the report
	 * @param outputStream
	 *            output stream for the report
	 * @throws ReportingException
	 */
	public static byte[] createPDFReport(String templateName, Map<String,Object> parameters, ResultSet resultSet) throws ReportingException {
		log.debug("Inside createPDFReport");

		JasperPrint jasperPrint = constructReport(templateName, parameters, resultSet);
		byte[] byteStream = ExportManager.pdfExport(jasperPrint);

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	/**
	 * Creates a Pdf Report Using a Collection of Dtos
	 * 
	 * @param templateName
	 *            template reference name
	 * @param parameters
	 *            parameters that need to send to the report
	 * @param colData
	 *            collection for the report
	 * @param servletResponse
	 *            the servletResponse
	 * @return
	 * @throws ReportingException
	 */
	public static byte[]
			createPDFReport(String templateName, Map<String,Object> parameters, Collection<?> colData, ServletResponse servletResponse)
					throws ReportingException {
		log.debug("Inside createPDFReport");
	
		JasperPrint jasperPrint = constructReport(templateName, parameters, colData);
		byte[] byteStream = ExportManager.pdfExport(servletResponse, jasperPrint);

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	/**
	 * Creates the Excel Report
	 * 
	 * @param templateName
	 *            template reference name
	 * @param parameters
	 *            parameters that needs to be send to the report
	 * @param resultSet
	 *            data that need to be passed to the report
	 * @param writer
	 *            writer for the report
	 * @param outputStream
	 *            outputstream for the report
	 * @throws ReportingException
	 */
	public static byte[]
			createXLSReport(String templateName, Map<String,Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
					throws ReportingException {
		log.debug("Inside createXLSReport");
		parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		JasperPrint jasperPrint = constructReport(templateName, parameters, resultSet);
		byte[] byteStream = ExportManager.excelExport(servletResponse, jasperPrint);

		log.debug("Exit createXLSReport");
		return byteStream;
	}

	public static byte[] createXLSReport(String templateName, Map<String,Object> parameters, ResultSet resultSet)
			throws ReportingException {
		log.debug("Inside createXLSReport");
		parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		JasperPrint jasperPrint = constructReport(templateName, parameters, resultSet);
		byte[] byteStream = ExportManager.excelExport(jasperPrint);

		log.debug("Exit createXLSReport");
		return byteStream;
	}

	/**
	 * Creates a XSL report from the collection of DTOs
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param servletResponse
	 * @return
	 * @throws ReportingException
	 */
	public static byte[]
			createXLSReport(String templateName, Map<String,Object> parameters, Collection<?> colData, ServletResponse servletResponse)
					throws ReportingException {
		log.debug("Inside createXLSReport");
		parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		JasperPrint jasperPrint = constructReport(templateName, parameters, colData);
		byte[] byteStream = ExportManager.excelExport(servletResponse, jasperPrint);

		log.debug("Exit createXLSReport");
		return byteStream;
	}

	/**
	 * Creates csv Report from the collection of DTOs
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param servletResponse
	 * @return
	 * @throws ReportingException
	 */
	public static byte[] createCSVReport(String templateName, Map<String,Object> parameters, ResultSet resultSet, Collection<?> colData,
			ServletResponse servletResponse) throws ReportingException {
		log.debug("Inside createCSVReport");

		String rptDelimiter = PlatformUtiltiies.nullHandler(parameters.get("DELIMITER"));
		byte[] byteStream = null;
		JasperPrint jasperPrint = null;
		parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		if (resultSet != null) {
			jasperPrint = constructReport(templateName, parameters, resultSet);
		} else if (colData != null) {
			jasperPrint = constructReport(templateName, parameters, colData);
		} else {
			throw new ReportingException("No Valid data provider found");
		}

		if (rptDelimiter.length() > 0) {
			if (servletResponse != null) {
				byteStream = ExportManager.csvExport(servletResponse, jasperPrint, rptDelimiter);
			} else {
				byteStream = ExportManager.csvExport(jasperPrint, rptDelimiter);
			}
		} else {
			if (servletResponse != null) {
				byteStream = ExportManager.csvExport(servletResponse, jasperPrint);
			} else {
				byteStream = ExportManager.csvExport(jasperPrint);
			}
		}

		log.debug("Exit createCSVReport");
		return byteStream;
	}

	public static void createCSVReportV2(Map<String,Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
			throws ReportingException {
		log.debug("Inside createCSVReportV2");

		String rptDelimiter = PlatformUtiltiies.nullHandler(parameters.get("DELIMITER"));
		ExportManager.csvExport(parameters, resultSet, rptDelimiter, servletResponse);

		log.debug("Exit createCSVReportV2");
	}

	public static byte[] createReport(ReportParam.ReportType reportType, String templateName, Map<String,Object> parameters, Map<String, ResultSet> resultSets, ServletResponse servletResponse)
			throws ReportingException {
		log.debug("Inside createPDFReport");
		byte[] byteStream = null;

		JasperPrint jasperPrint = constructReport(templateName, parameters, resultSets);

		switch (reportType) {
		case PDF:
			byteStream = servletResponse != null ? ExportManager.pdfExport(servletResponse, jasperPrint) : ExportManager.pdfExport(jasperPrint);
			break;
		case HTML:
			byteStream = servletResponse != null ? ExportManager.htmlExport(servletResponse, jasperPrint, null, null): ExportManager.htmlExport(jasperPrint, null, null);
			break;
		case CSV:
			byteStream = servletResponse != null ? ExportManager.csvExport(servletResponse, jasperPrint) : ExportManager.csvExport(jasperPrint);
			break;
		case XSL:
			byteStream = servletResponse != null ? ExportManager.excelExport(servletResponse, jasperPrint) : ExportManager.excelExport(jasperPrint);
			break;
		}

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	/**
	 * Construct the report
	 * 
	 * @param templateName
	 *            template reference name
	 * @param parameters
	 *            parameters that need to be passed to the report
	 * @param resultSet
	 *            data that needs to passed to the report
	 * @return JasperPrint object
	 * @throws ReportingException
	 */
	private static JasperPrint constructReport(String templateName, Map<String,Object> parameters, ResultSet resultSet)
			throws ReportingException {
		log.debug("Inside constructReport");

		JasperReport jasperReport = locateReport(templateName);
		JasperPrint jasperPrint = null;
	//	parameters.put("TableDataSource", new JRResultSetDataSource(resultSet));
	//	parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.FALSE);
		
		// To provide Report Translation
		String reportName = jasperReport.getName();
		String lang = (String) parameters.get("language");		
		
		if ((lang == null)){
			// for reports which are not translated
		}
		else{
			String REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE = "resources/localization/localization" + ((reportName == null) ? "" : "_"+reportName);
			Locale locale = new Locale(lang);
			ResourceBundle rb = null;
			try {
				rb = ResourceBundle.getBundle(REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE, locale);
			} catch (MissingResourceException ex) {

				log.info("Requested resource bundle not found, loading the default");

				String REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE_DEFAULT = "resources/localization/localization" + "_"
						+ "CompanyPaymentReportDefault";
				rb = ResourceBundle.getBundle(REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE_DEFAULT, locale);
			}
	
			parameters.put(JRParameter.REPORT_LOCALE,locale);
			parameters.put(JRParameter.REPORT_RESOURCE_BUNDLE,rb);
		}

		try {
		//	parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			jasperPrint = JasperFillManager.fillReport(jasperReport,parameters, new JRResultSetDataSource(resultSet));
		} catch (JRException e) {
			log.error("Error occured", e);
			ReportingException reportingException = new ReportingException("Can not contruct report " + templateName);
			throw reportingException;
		}catch (Exception e) {
			e.printStackTrace();
		}

		log.debug("Exit constructReport");
		return jasperPrint;
	}

	private static JasperPrint constructReport(String templateName, Map<String,Object> parameters, Map<String, ResultSet> resultSets)
			throws ReportingException {
		log.debug("Inside constructReport");

		JasperReport jasperReport = locateReport(templateName);
		JasperPrint jasperPrint = null;
		//	parameters.put("TableDataSource", new JRResultSetDataSource(resultSet));
		//	parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.FALSE);

		// To provide Report Translation
		String reportName = jasperReport.getName();
		String lang = (String) parameters.get("language");

		if ((lang == null)){
			// for reports which are not translated
		}
		else{
			String REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE = "resources/localization/localization" + ((reportName == null) ? "" : "_"+reportName);
			Locale locale = new Locale(lang);
			ResourceBundle rb = null;
			try {
				rb = ResourceBundle.getBundle(REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE, locale);
			} catch (MissingResourceException ex) {

				log.info("Requested resource bundle not found, loading the default");

				String REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE_DEFAULT = "resources/localization/localization" + "_"
						+ "CompanyPaymentReportDefault";
				rb = ResourceBundle.getBundle(REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE_DEFAULT, locale);
			}

			parameters.put(JRParameter.REPORT_LOCALE,locale);
			parameters.put(JRParameter.REPORT_RESOURCE_BUNDLE,rb);

		}

		try {
			Path tempDirectoryPath = Files.createTempDirectory("report");
			JRSwapFile swapFile = new JRSwapFile(tempDirectoryPath.toString(), 1024, 128);
			JRSwapFileVirtualizer virtualizer = new JRSwapFileVirtualizer (256, swapFile, true);
			parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

		} catch (IOException e) {
			log.error("Error occured", e);
			ReportingException reportingException = new ReportingException("Can not contruct report " + templateName);
			throw reportingException;
		}

		try {
			//	parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			jasperPrint = JasperFillManager.fillReport(jasperReport,parameters, createReportDataSource(resultSets));
		} catch (JRException e) {
			log.error("Error occured", e);
			ReportingException reportingException = new ReportingException("Can not contruct report " + templateName);
			throw reportingException;
		}catch (Exception e) {
			e.printStackTrace();
		}

		log.debug("Exit constructReport");
		return jasperPrint;
	}


	private static JasperPrint constructReport(String templateName, Map<String,Object> parameters, Collection<?> colData) throws ReportingException {
		log.debug("Inside constructReport");

		JasperReport jasperReport = locateReport(templateName);
		JasperPrint jasperPrint = null;
		
		// To provide Report Translation
		String reportName = jasperReport.getName();
		String lang = (String) parameters.get("language");		
			
		if ((lang == null)){
			// for reports which are not translated
		}
		else{
			String REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE = "resources/localization/localization" + (reportName == null ? "" : "_"+reportName);
			Locale locale = new Locale(lang);
			ResourceBundle rb = null;
			rb = ResourceBundle.getBundle(REPORT_CODES_MAPPINGS_RESOURCE_BUNDLE, locale);
			parameters.put(JRParameter.REPORT_LOCALE,locale);
			parameters.put(JRParameter.REPORT_RESOURCE_BUNDLE,rb);
		}

		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, createReportDataSource(colData));
		} catch (JRException e) {
			log.error(e);
			ReportingException reportingException = new ReportingException("Can not contruct report " + templateName);
			throw reportingException;
		}

		log.debug("Exit constructReport");
		return jasperPrint;
	}

	private static JRDataSource createReportDataSource(Collection<?> dataCollection) {
		JRBeanCollectionDataSource dataSource;
		dataSource = new JRBeanCollectionDataSource(dataCollection);
		return dataSource;
	}

	private static JRDataSource createReportDataSource(Map<String, ResultSet> dataCollection) {
		List dataSourcesList = new ArrayList<>();
		
		Map mainReportDSElementMap = new HashMap();
		Iterator it = dataCollection.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        mainReportDSElementMap.put(pair.getKey(), pair.getKey());
	    }
	    
	    dataSourcesList.add(mainReportDSElementMap);
		JRDataSource dataSource = new JRMapCollectionDataSource(dataSourcesList);

		return dataSource;
	}

	/**
	 * Will locate the report template file by giving the reference template name
	 * 
	 * @param templateName
	 * @return
	 * @throws ReportingException
	 */
	private static JasperReport locateReport(String templateName) throws ReportingException {
		log.debug("Inside locateReport");

		JasperReport jasperReport = (JasperReport) properties.get(templateName);

		if (jasperReport == null) {
			ReportingException reportingException = new ReportingException(templateName + " template reference not found....");
			throw reportingException;
		}

		log.debug("Exit locateReport");
		return jasperReport;
	}
	
	public static byte[] createHTMLReport(String templateName, Map<String,Object> parameters, Collection<?>  colData,
			Map<Object, Object> imagesMap, String imagesURI) throws ReportingException {
		log.debug("Inside createHTMLReport");

		JasperPrint jasperPrint = constructReport(templateName, parameters, colData);
		byte[] byteStream = ExportManager.htmlExport(jasperPrint, imagesMap, imagesURI);

		log.debug("Exit createHTMLReport");
		return byteStream;
	}
	public static byte[] createXLSReport(String templateName, Map<String,Object> parameters, Collection<?> colData)
			throws ReportingException {
		log.debug("Inside createXLSReport");
		parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		JasperPrint jasperPrint = constructReport(templateName, parameters, colData);
		byte[] byteStream = ExportManager.excelExport(jasperPrint);

		log.debug("Exit createXLSReport");
		return byteStream;
	}
	public static byte[] createPDFReport(String templateName, Map<String,Object> parameters, Collection<?>  colData) throws ReportingException {
		log.debug("Inside createPDFReport");

		JasperPrint jasperPrint = constructReport(templateName, parameters, colData);
		byte[] byteStream = ExportManager.pdfExport(jasperPrint);

		log.debug("Exit createPDFReport");
		return byteStream;
	}
}
