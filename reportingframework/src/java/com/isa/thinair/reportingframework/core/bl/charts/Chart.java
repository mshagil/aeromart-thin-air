/**
 * 
 */
package com.isa.thinair.reportingframework.core.bl.charts;

import org.jfree.chart.JFreeChart;

/**
 * JFreeChart Wrapper
 * 
 * @author Navod Ediriweera
 * @since Jul 09, 2009
 */
public class Chart {

	protected JFreeChart jFreeChart;
	protected int height = 500;
	protected int width = 300;

	/**
	 * @return the jFreeChart
	 */
	public JFreeChart getJFreeChart() {
		return jFreeChart;
	}

	/**
	 * @param freeChart
	 *            the jFreeChart to set
	 */
	public void setJFreeChart(JFreeChart freeChart) {
		jFreeChart = freeChart;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

}
