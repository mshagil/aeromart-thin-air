package com.isa.thinair.reportingframework.api.dto;

public interface ReportStructure {

	interface GST {
		interface GSTR1 {
			String S_4A = "S_4A";
			String S_5A = "S_5A";
			String S_6A = "S_6A";
			String S_7A_1 = "S_7A_1";
			String S_7B_1 = "S_7B_1";
			String S_7A = "S_7A";
			String S_8A = "S_8A";
			String S_9 = "S_9";
			String S_9B = "S_9B";
			String S_10 = "S_10";
			String S_10A = "S_10A";
			String S_11 = "S_11";
			String S_11A = "S_11A";
			String S_12 = "S_12";
			String S_13_1 = "S_13_1";
			String S_13_2 = "S_13_2";
			String S_13_2A = "S_13_2A";
			String S_14 = "S_14";
		}

		interface GSTR3 {
			String S_5 = "S_5";
			String S_6_1 = "S_6_1";
			String S_6_2 = "S_6_2";
			String S_6_3 = "S_6_3";
			String S_6_4 = "S_6_4";
			String S_6_5 = "S_6_5";
			String S_6_6 = "S_6_6";
			String S_6_7 = "S_6_7";
			String S_7_1 = "S_7_1";
			String S_7_2 = "S_7_2";
			String S_7_3 = "S_7_3";
			String S_7_4 = "S_7_4";
			String S_7_5 = "S_7_5";
			String S_7_6 = "S_7_6";
			String S_7_7 = "S_7_7";
			String S_8 = "S_8";
			String S_9A = "S_9A";
			String S_9B = "S_9B";
			String S_10 = "S_10";
			String S_11 = "S_11";
			String S_12 = "S_12";
			String S_3 = "S_3";
			String S_4_1 = "S_4_1";
			String S_4_2 = "S_4_2";
			String S_4_3_I = "S_4_3_I";
			String S_4_3_II = "S_4_3_II";
			String S_5_A = "S_5_A";
			String S_5_B = "S_5_B";
			String S_6 = "S_6";
			String S_7 = "S_7";
			String S_9 = "S_9";
			String S_13 = "S_13";
			String S_14 = "S_14";
			String S_15 = "S_15";
			String S_16 = "S_16";

		}
	}
}
