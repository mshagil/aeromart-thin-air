package com.isa.thinair.reportingframework.api.dto;

import java.awt.Color;
import java.io.Serializable;
import java.sql.ResultSet;

import org.jfree.chart.plot.PlotOrientation;

import com.isa.thinair.reportingframework.core.bl.charts.ChartDataParser;
import com.isa.thinair.reportingframework.core.util.ChartConstants;

/**
 * This class is the Common Data Transfer object used to transfer chart related data to the framework
 * 
 * @author Navod Ediriweera
 * @since Jul 08, 2009
 * 
 */
public abstract class ChartCommonsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8867800671858718554L;
	protected String title;
	protected PlotOrientation plotOrientation;
	protected boolean showLegend;
	protected boolean enableToolTips;
	protected boolean generateURL;
	protected Color backGroundPaint;
	protected Color plotBackGroundPaint;
	protected float foreGroundAlpha;
	protected Color boarderPaint;
	protected boolean boarderVisible;
	protected int width;
	protected int height;
	/**
	 * Data for the Chart. See {@link ChartDataParser} for result Set standards
	 */
	protected ResultSet chartResultSet;
	/**
	 * Chart Result Set Standard.
	 * 
	 * @see {@link ChartDataParser} , {@link ChartConstants.CHART_DATA_SET_TYPES}
	 */
	protected String resultSetType;
	/**
	 * Set according to the implemented DTO. <br>
	 * see {@link ChartConstants.CHART_TYPES} for DTO types
	 */
	protected final String chartType;
	/**
	 * Set internally when creating a chart. Is used to identify abstract types <br>
	 * see {@link ChartConstants.MAIN_CHART_TYPES} for available abstract chart types.
	 */
	protected String chartMainType;

	/**
	 * @param chartType
	 *            The name of the char. <br>
	 *            Corrosponding to the value in {@link ChartConstants.CHART_TYPES}
	 */
	public ChartCommonsDTO(String chartType) {
		this.title = "";
		this.chartType = chartType;
		this.plotOrientation = PlotOrientation.VERTICAL;
		this.showLegend = true;
		this.enableToolTips = false;
		this.generateURL = false;

		this.backGroundPaint = Color.WHITE;
		this.foreGroundAlpha = 0.60f;
		this.boarderPaint = Color.WHITE;
		this.boarderVisible = true;
		this.width = 250;
		this.height = 400;
		this.resultSetType = "";
	}

	/**
	 * Set internally when creating a categoy chart. Can be used to identify other abstract types <br>
	 * see {@link ChartConstants.MAIN_CHART_TYPES} for available abstract chart types.
	 * 
	 * @return the chartMainType
	 */
	public String getChartMainType() {
		return chartMainType;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the plotBackGroundPaint
	 */
	public Color getPlotBackGroundPaint() {
		return plotBackGroundPaint;
	}

	/**
	 * @param plotBackGroundPaint
	 *            the plotBackGroundPaint to set
	 */
	public void setPlotBackGroundPaint(Color plotBackGroundPaint) {
		this.plotBackGroundPaint = plotBackGroundPaint;
	}

	/**
	 * @return the boarderVisible
	 */
	public boolean isBoarderVisible() {
		return boarderVisible;
	}

	/**
	 * @param boarderVisible
	 *            the boarderVisible to set
	 */
	public void setBoarderVisible(boolean boarderVisible) {
		this.boarderVisible = boarderVisible;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the chartResultSet
	 */
	public ResultSet getChartResultSet() {
		return chartResultSet;
	}

	//
	//
	// /**
	// * @param chartResultSet the chartResultSet to set
	// */
	// public void setChartResultSet(ResultSet chartResultSet) {
	// this.chartResultSet = chartResultSet;
	// }
	/**
	 * @param chartResultSet
	 *            the SQL ResultSet to set
	 * @param type
	 *            The type of the chart result set, corrosponding to the value in
	 *            {@link ChartConstants.CHART_DATA_SET_TYPES} <br>
	 *            See {@link ChartDataParser} for SQL resultset standards
	 */
	public void setChartResultSetWithType(ResultSet chartResultSet, String type) {
		this.chartResultSet = chartResultSet;
		this.resultSetType = type;
	}

	/**
	 * @return the resultSetType
	 */
	public String getResultSetType() {
		return resultSetType;
	}

	/**
	 * @param resultSetType
	 *            the resultSetType to set
	 */
	public void setResultSetType(String resultSetType) {
		this.resultSetType = resultSetType;
	}

	/**
	 * @return the chartType
	 */
	public String getChartType() {
		return chartType;
	}

	/**
	 * @return the boarderPaint
	 */
	public Color getBoarderPaint() {
		return boarderPaint;
	}

	/**
	 * @param boarderPaint
	 *            the boarderPaint to set
	 */
	public void setBoarderPaint(Color boarderPaint) {
		this.boarderPaint = boarderPaint;
	}

	public float getForeGroundAlpha() {
		return foreGroundAlpha;
	}

	public void setForeGroundAlpha(float foreGroundAlpha) {
		this.foreGroundAlpha = foreGroundAlpha;
	}

	/**
	 * @return the backGroundPaint
	 */
	public Color getBackGroundPaint() {
		return backGroundPaint;
	}

	/**
	 * @param backGroundPaint
	 *            the backGroundPaint to set
	 */
	public void setBackGroundPaint(Color backGroundPaint) {
		this.backGroundPaint = backGroundPaint;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param plotOrientation
	 *            The plotOrientation to set.
	 */
	public void setPlotOrientation(PlotOrientation plotOrientation) {
		this.plotOrientation = plotOrientation;
	}

	/**
	 * @return Returns the plotOrientation.
	 */
	public PlotOrientation getPlotOrientation() {
		return plotOrientation;
	}

	/**
	 * @param showLegend
	 *            The showLegend to set.
	 */
	public void setShowLegend(boolean showLegend) {
		this.showLegend = showLegend;
	}

	/**
	 * @return Returns the showLegend.
	 */
	public boolean isShowLegend() {
		return showLegend;
	}

	/**
	 * @param enableToolTips
	 *            The enableToolTips to set.
	 */
	public void setEnableToolTips(boolean enableToolTips) {
		this.enableToolTips = enableToolTips;
	}

	/**
	 * @return Returns the enableToolTips.
	 */
	public boolean isEnableToolTips() {
		return enableToolTips;
	}

	/**
	 * @param generateURL
	 *            The generateURL to set.
	 */
	public void setGenerateURL(boolean generateURL) {
		this.generateURL = generateURL;
	}

	/**
	 * @return Returns the generateURL.
	 */
	public boolean isGenerateURL() {
		return generateURL;
	}
}
