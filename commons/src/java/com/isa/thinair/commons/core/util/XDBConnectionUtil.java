/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * Air Reservation module specific utilities
 * 
 * @author Byorn
 * @since 1.0
 */
public class XDBConnectionUtil extends DefaultModuleConfig {
	
	String accountingSystem;
	
	String driverClass;

	String serverIp;

	String port;

	String databaseServer;
	
	String url;

	String dbUserName;

	String dbPassword;

	public String getDatabaseServer() {
		return databaseServer;
	}

	public void setDatabaseServer(String databaseServer) {
		this.databaseServer = databaseServer;
	}

	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * get External Connection
	 * 
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Connection getExternalConnection() throws CommonsDataAccessException {
		
		Connection connection = null;

		String driverName = getDriverClass();
		String url = getDBUrl();
		String username = getDbUserName();
		String password = getDbPassword();

		// Load the JDBC driver
		try {
			Class.forName(driverName);

			connection = (Connection) DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {

			throw new CommonsDataAccessException(e, "exdb.loaddriver.failed");
		} catch (SQLException e) {

			throw new CommonsDataAccessException(e, "exdb.getconnection.failed");

		}
		return connection;
	}

	private String getDBUrl() {
		String serverName = getServerIp();
		String portNumber = getPort();
		String mydatabase = serverName + ":" + portNumber + getDatabaseServer();

		return getUrl() + mydatabase;
	}

	/**
	 * 
	 * @param connection
	 * @throws CommonsDataAccessException
	 */
	public static void rollback(Connection connection) throws CommonsDataAccessException {

		try {
			connection.rollback();
			connection.close();
		} catch (SQLException e) {
			throw new CommonsDataAccessException(e, "exdb.rollback.error");

		}

	}

	/**
	 * 
	 * @param connection
	 * @throws CommonsDataAccessException
	 */
	public static void commit(Connection connection) throws CommonsDataAccessException {

		try {
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			throw new CommonsDataAccessException(e, "exdb.commit.error");

		}

	}

	public DriverManagerDataSource getExternalDatasource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(getDriverClass());
		dataSource.setUrl(getDBUrl());
		dataSource.setUsername(getDbUserName());
		dataSource.setPassword(getDbPassword());
		return dataSource;
	}

	public String getAccountingSystem() {
		return accountingSystem;
	}

	public void setAccountingSystem(String accountingSystem) {
		this.accountingSystem = accountingSystem;
	}

}
