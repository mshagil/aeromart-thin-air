package com.isa.thinair.commons.core.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

/**
 * @author Nilindra Fernando
 */
public class PerfUtil {
	
	private static final NumberFormat BYTES_FORMATTER = new DecimalFormat("###,###,###,###");
	
	private static final NumberFormat DECIMAL_FORMATTER = new DecimalFormat("0.00");
	
	/**
	 * Returns the total execution time
	 * 
	 * @param startDateTime
	 * @param endDateTime
	 * @return
	 */
	public static String getExecutionTime(Date startDateTime, Date endDateTime) {
		 // Get difference in milliseconds
	    long diffMillis = endDateTime.getTime() - startDateTime.getTime();
		BigDecimal diffSeconds = (new BigDecimal(Long.toString(diffMillis))).divide(
				new BigDecimal("1000"), 2, BigDecimal.ROUND_UP); 
		
	    return " [processed in " + diffSeconds + " seconds]";
	}

	
	/**
	 * Returns the memory information
	 * 
	 * @return
	 */
	public static String getMemoryInfo() {
	    long heapSize = Runtime.getRuntime().totalMemory();
	    
	    // Get maximum size of heap in bytes. The heap cannot grow beyond this size.
	    // Any attempt will result in an OutOfMemoryException.
	    long heapMaxSize = Runtime.getRuntime().maxMemory();
	    
	    // Get amount of free memory within the heap in bytes. This size will increase
	    // after garbage collection and decrease as new objects are created.
	    long heapFreeSize = Runtime.getRuntime().freeMemory();
	
	    String memInfo = " [HeapSize:" + getFileSize(heapSize) + ":HeapFreeSize:" 
	    + getFileSize(heapFreeSize) + ":heapMaxSize:" + getFileSize(heapMaxSize);
	    
	    return memInfo;
	}
	
	private static String getFileSize(long bytes) {
		String fileSizeInBytes = BYTES_FORMATTER.format(bytes);         
		String fileSizeInfo = "";
		
		double size = bytes / 1024;
		
		if (size == 0) {
			fileSizeInfo = fileSizeInBytes + " bytes";	
		} else if (size < 1024) {
			fileSizeInfo = decimalFormat(size) + " KB [" + fileSizeInBytes + " bytes]";	
		} else if (size / 1024 < 1024){
			fileSizeInfo = decimalFormat(size / 1024) + " MB [" + fileSizeInBytes + " bytes]";	
		} else if (size / 1024 / 1024 < 1024) {
			fileSizeInfo = decimalFormat(size / 1024 / 1024) + " GB [" + fileSizeInBytes + " bytes]";	
		}
		
		return fileSizeInfo;
	}
	
	private static String decimalFormat(double amount) {
	    return DECIMAL_FORMATTER.format(amount);  
	}
}
