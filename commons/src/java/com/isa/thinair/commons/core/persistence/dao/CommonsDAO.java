/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.persistence.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.AdminFeeAgentTO;
import com.isa.thinair.commons.api.dto.AirportMessage;
import com.isa.thinair.commons.api.dto.BaseContactConfigDTO;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.dto.BookingClassCharMappingTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTranslationTemplateDTO;
import com.isa.thinair.commons.api.dto.CabinClassDTO;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.ChargeApplicabilityTO;
import com.isa.thinair.commons.api.dto.ErrorLogTO;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.GDSPaxConfigDTO;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.HubAirportTO;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.ReturnValidityPeriodTO;
import com.isa.thinair.commons.api.dto.SSRCategoryDTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.dto.ServiceBearerDTO;
import com.isa.thinair.commons.api.dto.ServiceClientDTO;
import com.isa.thinair.commons.api.dto.ServiceClientTTYDTO;
import com.isa.thinair.commons.api.dto.TermsTemplateDTO;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;
import com.isa.thinair.commons.api.dto.WSContactConfigDTO;
import com.isa.thinair.commons.api.dto.WSPaxConfigDTO;
import com.isa.thinair.commons.api.dto.XBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.XBEPaxConfigDTO;
import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AirportMessagesUtil;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.platform.core.commons.exception.PlatformRuntimeException;

import org.springframework.util.CollectionUtils;

/**
 * Class to access database for the usage of platform
 * 
 * @author Lasantha Pambagoda
 * @TODO need to move this to an interface
 */
public class CommonsDAO extends PlatformBaseJdbcDAOSupport {

	/** The logger */
	private final Log log = LogFactory.getLog(getClass());

	/** field bizParamMap for all the carriers */
	private HashMap<String, HashMap<String, String>> allBizParamsMap;

	// fields mapped via DI
	/** data souce to get system params */
	private DataSource dataSource;

	/** query to execute for biz params */
	private String queryStringForBizProperties;

	private String queryStringForSalesChannelsCodes;

	private HashMap<String, Integer> salesChannelsMap;

	/** Holds the query for the Booking class mapping */
	private String queryStringForGDSBookingClassMapping;

	private String queryStringForCSBookingClassMapping;

	/** Holds the bookingClassMapping Map */
	private HashMap<BookingClassCharMappingTO, String> bookingClassCharMappingMap;

	private HashMap<Integer, Map<String, String>> gdsBookingClassMap;

	private HashMap<Integer, Map<String, String>> csBookingClassMap;

	private List<PaxCategoryTO> paxCategoryList;
	
	private List<AdminFeeAgentTO> adminFeeAgentList;

	private HashMap<String, PaxTypeTO> paxTypesMap;
	private String queryStringForPaxTypes;

	/** Holds contact details related cofigurations */
	private List<BaseContactConfigDTO> contactConfigDTOList;
	private List<UserRegConfigDTO> userRegConfigDTOList;
	private String queryStringForContactDetailsConfig;

	/** Holds pax details related configurations */
	private List<BasePaxConfigDTO> paxConfigDTOList;
	private String queryStringForXBEPaxConfig;
	private String queryStringForIBEPaxConfig;
	private String queryStringForWSPaxConfig;
	private String queryStringForGDSPaxConfig;

	/** Holds the interlined air lines map */
	private HashMap<String, InterlinedAirLineTO> interlinedAirlinesMap;

	/** Hold interlined airline codes list */
	private List<String> interlinedAirlineCodesList;

	private String queryStringForInterlinedAirlines;

	private String queryStringForInterlinedAirlineCodes;

	private XDBConnectionUtil xdbDataSourceUtil;

	/** Holds the query for the Fare Categories */
	private String queryStringForFareCategories;

	/** Holds the query for the charge applicabilites */
	private String queryChargeApplicabilities;

	/** Query string for hubs */
	private String queryStringForHubs;

	private String queryActiveCurrencyCodes;

	/** Holds query for the carriers in the system */
	private String queryForCarriers;

	/** Holds query for the interline airline carrier codes */
	private String queryForInterlinedAirlineCodes;

	/** Holds query for the own airline carrier codes with blocked seats carriers */
	private String queryForOwnAirlineCarriers;

	/** Holds query for the own airline carrier codes with blocked seats carriers with exclusion of bus carriers */
	private String queryForOwnAirCarriers;

	/** Holds query for currencies which are not set for show Itinerary charge break down */
	private String queryForHideChargeCurrencies;

	/** Holds query for the active cabin classes in the system */
	private String queryForActiveCabinClasses;

	private String queryForActiveCabinClassInfo;

	/** Hold query for PetroFac enabled Agents */
	private String queryForPetroFacAgents;
	/** Holds PetroFac enabled Agents - key:AgentCode,Value:Capture_Pay_Ref ('Y') */
	private HashMap<String, String> petroFacAgentsMap;

	private String queryStringForActualPayModes;

	private String queryStringForPaxCategory;

	private String queryForCarrierAirlineMap;

	/** holds carrier code & airline code map */
	private Map<String, String> carrierAirlineCodeMap;

	/** Holds carriers in the system */
	private Map<String, String> carriersMap;

	private Set<String> interlineAirlineCodes;

	private Set<String> ownAirlineCarrierCodes;

	/** Bus carriers are excluded **/
	private Set<String> ownAirCarrierCodes;

	private Set<String> chargeHideCurrencies;

	/** Holds cabin classes in the system */
	private Map<String, String> cabinClassesMap;

	private Map<String, CabinClassDTO> cabinClassDTOMap;

	private HashMap<String, String> iataModelMap;

	private HashMap<String, GDSStatusTO> gdsMap;

	private HashMap<String, List<ServiceClientTTYDTO>> ServiceClientTTYbyRequestTypeMap;

	private HashMap<String, ServiceClientDTO> serviceClientsMap;

	private HashMap<String, ServiceBearerDTO> serviceBearersMap;

	private Integer nextVal;

	// Haider 23Oct08
	/** Holds query for publish mechanism in the system */
	private String queryForPublishMechanism;
	/** Holds publish mechanism */
	private Map<String, String> publishMechanismMap;

	/** Holds cabin classes in the system */
	private HashMap<String, ArrayList<String>> airportMessagesMap;

	private String queryStringForAirportMessages;

	private String sqlCreateErrorLog;

	/** Holds SSR categories in the system */
	private Map<Integer, SSRCategoryDTO> ssrCategoryMap;

	/** Holds query for the SSR Category in the system */
	private String queryForSSRCategory;

	/** Holds query for returnValidityPeriod in the system */
	private String queryForReturnValidityPeriod;

	private Map<Integer, ReturnValidityPeriodTO> returnValidityPeriods;

	/** Holds query for ssr info in the system */
	private String queryForAllSSRInfo;

	private Map<Integer, SSRInfoDTO> sSRInfoMap;

	private String querySSRApplicableAirports;

	/**
	 * @return Map<ssrId+airportCode,true/false>
	 */
	private Map<String, Boolean> sSRApplicableAirportsMap;

	private String querySSRCategoryEMail;
	private Map<Integer, String> sSRCategoryEMailMap;

	/** Added by Lalanthi to populate AAServices -> Charge group names */
	private Map<String, String> chargeGroupCodeNDescriptionMap;

	private String sqlChargeGroupCodeNDescription;

	private String sqlCodeShareBcCosMapping;

	private HashMap<Integer, String> actualPayModes;

	public String queryStringGroundStationDetails;

	public String queryStringGroundStationParentDetails;

	public String queryStringAirportDetails;

	private Map<String, String> mapGroundStationNames;

	private Map<String, Collection<String>> mapPrimaryStationCode;

	private Map<String, String> mapPrimaryStationBySubStation;

	private Map<String, List<String[]>> mapParentStationByGroundStation;

	private Map<String, String> mapTopLevelStationByConnectedStation;

	private Map<String, String[]> mapAllAirportCodes;

	private Map<String, String> connectingAirportDetail;

	private String queryStringBusConnectingAirports;

	private String queryStringDefaultLogicalCCDetails;

	private String queryStringForLogicalCabinClasses;

	private String queryStringForLogicalCabinClassesTranslations;

	private Map<String, LogicalCabinClassDTO> availableLogicalCCMap;

	private Set<String> busConnectingAirports;

	private List<CabinClassWiseDefaultLogicalCCDetailDTO> defaultLogicalCCDetails;

	private String queryStringAirportMessages;

	private String queryStringForChargeAdjustmentTypes;

	private List<ChargeAdjustmentTypeDTO> chargeAdjustmentTypes;

	private String queryStringPaxCountryConfigs;

	private String queryStringForAgents;

	private Map<String, String> agentByCodes;

	private String queryStringForSingleEntryMappedGdsBCs;

	private HashMap<Integer, List<String>> gdsMappedBookingClassList;

	private String queryStringForBookingClassesByLogicalCC;

	private Map<String, List<String>> bookingClassByLogicalCCMap;

	private String queryStringForServiceTypeByOperationType;

	private Map<Integer, List<String>> serviceTypeByOperationType;
	
	private String queryStringForServiceTaxReportingCategory;

	private Collection<String> airportCodes;
	
	private String queryStringForAdminFeeAgent;
	
	private Collection<String> cityCodes;

	/**
	 * method to get biz params form the database
	 * 
	 * @param ds
	 * @param qureyString
	 * @param keyColoumName
	 * @param valueColumnName
	 * @return
	 */
	public HashMap<String, HashMap<String, String>> getBizParams() {
		allBizParamsMap = new HashMap<String, HashMap<String, String>>();

		if (dataSource != null && queryStringForBizProperties != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForBizProperties, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {
						// populate systemParamMap
						HashMap<String, String> carrierBizParams;
						while (rs.next()) {
							carrierBizParams = allBizParamsMap.get(rs.getString("CARRIER_CODE"));

							if (carrierBizParams == null) {
								carrierBizParams = new HashMap<String, String>();
								allBizParamsMap.put(rs.getString("CARRIER_CODE"), carrierBizParams);
							}

							carrierBizParams.put(rs.getString("PARAM_KEY"), rs.getString("PARAM_VALUE"));
						}
					}
					return null;
				}
			});
		}
		return allBizParamsMap;
	}

	public HashMap<String, Integer> getSalesChannelMap() {
		salesChannelsMap = new HashMap<String, Integer>();
		if (dataSource != null && queryStringForSalesChannelsCodes != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForSalesChannelsCodes, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							salesChannelsMap.put(rs.getString("DESCRIPTION"), new Integer(rs.getInt("SALES_CHANNEL_CODE")));
						}
					} else {
						throw new PlatformRuntimeException("platform.data.saleschannels.notconfigured");
					}
					return salesChannelsMap;
				}
			});
		}
		return salesChannelsMap;
	}

	public Collection<AirportMessage> getNewAirportMessages() {
		final Map<Integer, AirportMessage> airportMessages = new HashMap<Integer, AirportMessage>();
		if (dataSource != null && queryStringAirportMessages != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringAirportMessages, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					if (rs != null) {
						while (rs.next()) {
							Integer airportMessageId = new Integer(rs.getInt("AIRPORT_MSG_ID"));
							if (airportMessages.get(airportMessageId) == null)
								airportMessages.put(airportMessageId, new AirportMessage());
							AirportMessage airportMessage = airportMessages.get(airportMessageId);
							airportMessage.setAirport(rs.getString("AIRPORT_CODE"));
							airportMessage.setStage(rs.getString("STAGE"));
							airportMessage.getSalesChannels().add(String.valueOf(rs.getInt("SALES_CHANNEL_CODE")));
							airportMessage.setAirportMessageId(airportMessageId);
							airportMessage.setDepArrType(rs.getString("DEP_ARR"));
							airportMessage.setMessage(rs.getString("AIRPORT_MESSAGE"));
							airportMessage.setTimestampFrom(rs.getTimestamp("TIMESTAMP_FROM"));
							airportMessage.setTimestampTo(rs.getTimestamp("TIMESTAMP_TO"));
							airportMessage.setTravelValidityFrom(rs.getTimestamp("TRAVEL_VALIDITY_FROM"));
							airportMessage.setTravelValidityTo(rs.getTimestamp("TRAVEL_VALIDITY_TO"));
							airportMessage.setI18nMessageKey(rs.getString("I18N_MESSAGE_KEY"));
							String ondApplyStatus = rs.getString("OND_APPLY_STATUS");
							if (ondApplyStatus != null) {
								if (ondApplyStatus.equals(AirportMessagesUtil.INCLUDE))
									airportMessage.getInclusionONDs().add(rs.getString("OND_CODE"));
								if (ondApplyStatus.equals(AirportMessagesUtil.EXCLUDE))
									airportMessage.getExclusionONDs().add(rs.getString("OND_CODE"));
							}
							String flightApplyStatus = rs.getString("FLIGHT_APPLY_STATUS");
							if (flightApplyStatus != null) {
								if (flightApplyStatus.equals(AirportMessagesUtil.INCLUDE))
									airportMessage.getInclusionFligths().add(rs.getString("FLIGHT_NUMBER"));
								if (flightApplyStatus.equals(AirportMessagesUtil.EXCLUDE))
									airportMessage.getExclusionFlights().add(rs.getString("FLIGHT_NUMBER"));
							}
						}
					}
					return airportMessages.values();
				}
			});
		}
		return airportMessages.values();
	}

	public Map<String, Object[]> getAirportInfo(String airportCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		final Map<String, Object[]> aptShortName = new HashMap<String, Object[]>();

		Object[] params = null;
		if (airportCode == null) {
			params = new Object[] { "ACT" };
		} else {
			params = new Object[] { "ACT", airportCode };
		}

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT A.AIRPORT_CODE,A.AIRPORT_NAME,A.AIRPORT_SHORT_NAME,A.TERMINAL,S.COUNTRY_CODE,C.CITY_CODE, ST.STATE_CODE");
		sb.append(" FROM T_AIRPORT A, T_STATION S, T_CITY C, T_STATE ST");
		sb.append(" WHERE A.STATION_CODE = S.STATION_CODE AND A.CITY_ID = C.CITY_ID(+) AND S.STATE_ID = ST.STATE_ID(+) ");
		sb.append(" AND A.STATUS       = ? ");
		sb.append((airportCode != null ? "   AND A.AIRPORT_CODE = ?" : ""));

		template.query(sb.toString(), params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					aptShortName.put(
							rs.getString("AIRPORT_CODE"),
							new Object[] { rs.getString("AIRPORT_NAME"), rs.getString("AIRPORT_SHORT_NAME"),
									rs.getString("TERMINAL"), rs.getString("COUNTRY_CODE"), rs.getString("CITY_CODE"), rs.getString("STATE_CODE") });
				}
				return aptShortName;
			}
		});
		return aptShortName;
	}
	
	public Map<String, Object[]> getCityInfo(String cityCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		final Map<String, Object[]> cityShortName = new HashMap<String, Object[]>();

		Object[] params = null;
		if (cityCode == null) {
			params = new Object[] { "ACT" };
		} else {
			params = new Object[] { "ACT", cityCode };
		}

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT S.COUNTRY_CODE,C.CITY_CODE, ST.STATE_CODE");
		sb.append(" FROM T_AIRPORT A, T_STATION S, T_CITY C, T_STATE ST");
		sb.append(" WHERE A.STATION_CODE = S.STATION_CODE AND A.CITY_ID = C.CITY_ID(+) AND S.STATE_ID = ST.STATE_ID(+) ");
		sb.append(" AND C.STATUS       = ? ");
		sb.append((cityCode != null ? "   AND C.CITY_CODE = ?" : ""));

		template.query(sb.toString(), params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					cityShortName.put(
							rs.getString("CITY_CODE"),
							new Object[] { rs.getString("STATE_CODE"),
									rs.getString("COUNTRY_CODE") });
				}
				return cityShortName;
			}
		});
		return cityShortName;
	}

	public Map<String, String> getStateCodesForAirport(List<String> airportCodes) {
		Map<String, String> results = new HashMap<>();
		if (!CollectionUtils.isEmpty(airportCodes)) {
			JdbcTemplate template = new JdbcTemplate(getDataSource());
			String sqlInForAirportCode = "( " + getSqlInValuesStr(airportCodes) + " ) ";
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT A.AIRPORT_CODE, ST.STATE_CODE");
			sb.append(" FROM T_AIRPORT A, T_STATION S, T_STATE ST");
			sb.append(" WHERE A.STATION_CODE = S.STATION_CODE AND S.STATE_ID = ST.STATE_ID(+) ");
			sb.append(" AND A.AIRPORT_CODE IN " + sqlInForAirportCode);

			template.query(sb.toString(), ArrayUtils.EMPTY_OBJECT_ARRAY, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					while (rs.next()) {
						results.put(rs.getString("AIRPORT_CODE"), rs.getString("STATE_CODE"));
					}
					return null;
				}
			});
		}
		return results;
	}

	/**
	 * Retrieves the list of charge adjustment types defined in the system.
	 * 
	 * @return : List of {@link ChargeAdjustmentTypeDTO}s.
	 */
	public List<ChargeAdjustmentTypeDTO> getChargeAdjustmentTypes() {
		chargeAdjustmentTypes = new ArrayList<ChargeAdjustmentTypeDTO>();
		if (dataSource != null && queryStringForChargeAdjustmentTypes != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForChargeAdjustmentTypes, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							ChargeAdjustmentTypeDTO chgAdjustmentType = new ChargeAdjustmentTypeDTO();
							chgAdjustmentType.setChargeAdjustmentTypeId(rs.getInt("charge_adjustment_type_id"));
							chgAdjustmentType.setChargeAdjustmentTypeName(rs.getString("adjustment_type_name"));
							chgAdjustmentType.setDescription(rs.getString("descripion"));
							chgAdjustmentType.setRefundableChargeCode(rs.getString("refundable_charge_code"));
							chgAdjustmentType.setNonRefundableChargeCode(rs.getString("nonrefundable_charge_code"));
							chgAdjustmentType.setRefundablePrivilegeId(rs.getString("refundable_privilege_id"));
							chgAdjustmentType.setNonRefundablePrivilegeId(rs.getString("nonrefundable_privilege_id"));
							if ("Y".equals(rs.getString("default_adjustment"))) {
								chgAdjustmentType.setDefaultAdjustment(true);
							} else {
								chgAdjustmentType.setDefaultAdjustment(false);
							}
							chgAdjustmentType.setDisplayOrder(rs.getInt("display_order"));
							chgAdjustmentType.setStatus(rs.getString("status"));

							chargeAdjustmentTypes.add(chgAdjustmentType);
						}
					}
					return chargeAdjustmentTypes;
				}
			});
		}
		return chargeAdjustmentTypes;
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getInterlineCarrierCodes() {
		JdbcTemplate jt = new JdbcTemplate(getDataSource());

		// Final SQL
		String sql = "SELECT carrier_code, description, status FROM t_carrier a  WHERE status ='ACT' and a.interline_enabled = 'Y' order by a.carrier_code desc";
		Collection<String> carrierCodes = (Collection<String>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> carrierCodes = new HashSet<String>();

				if (rs != null) {
					while (rs.next()) {
						carrierCodes.add(rs.getString("carrier_code"));
					}
				}
				return carrierCodes;
			}
		});
		return carrierCodes;
	}

	/**
	 * Get all TermsTemplate's stored in the database.
	 * 
	 * @return Collection of {@link TermsTemplateDTO}
	 */
	public Collection<TermsTemplateDTO> getTermsAndConditionsTemplates() {
		JdbcTemplate jt = new JdbcTemplate(getDataSource());

		String sql = "SELECT * FROM T_TERMS_TEMPLATE";

		@SuppressWarnings("unchecked")
		Collection<TermsTemplateDTO> termsTemplates = (Collection<TermsTemplateDTO>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<TermsTemplateDTO> termsTemplates = new ArrayList<TermsTemplateDTO>();

				if (rs != null) {
					while (rs.next()) {
						TermsTemplateDTO template = new TermsTemplateDTO();

						template.setCarriers(rs.getString("CARRIERS"));
						template.setLanguage(rs.getString("LANGUAGE_CODE"));

						// Get content from the clob that's in hexadecimal.
						String templateContentHex = StringUtil.clobToString(rs.getClob("TERMS_TEMPLATE_CONTENT"));

						// Convert the hexadecimal string to normal string.
						template.setTemplateContent(StringUtil.getUnicode(templateContentHex));

						template.setTemplateID(rs.getInt("TERMS_TEMPLATE_ID"));
						template.setTemplateName(rs.getString("TERMS_TEMPLATE_NAME"));

						termsTemplates.add(template);
					}
				}
				return termsTemplates;
			}
		});

		return termsTemplates;
	}

	/**
	 * @return Returns the dataSource.
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource
	 *            The dataSource to set.
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @return Returns the queryStringForBizProperties.
	 */
	public String getQueryStringForBizProperties() {
		return queryStringForBizProperties;
	}

	/**
	 * @param queryStringForBizProperties
	 *            The queryStringForBizProperties to set.
	 */
	public void setQueryStringForBizProperties(String queryStringForBizProperties) {
		this.queryStringForBizProperties = queryStringForBizProperties;
	}

	public String getQueryStringForSalesChannelsCodes() {
		return queryStringForSalesChannelsCodes;
	}

	public void setQueryStringForSalesChannelsCodes(String queryStringForSalesChannelsCodes) {
		this.queryStringForSalesChannelsCodes = queryStringForSalesChannelsCodes;
	}

	/**
	 * @return Returns the xdbDataSourceUtil.
	 */
	public XDBConnectionUtil getXdbDataSourceUtil() {
		return xdbDataSourceUtil;
	}

	/**
	 * @param xdbDataSourceUtil
	 *            The xdbDataSourceUtil to set.
	 */
	public void setXdbDataSourceUtil(XDBConnectionUtil xdbDataSourceUtil) {
		this.xdbDataSourceUtil = xdbDataSourceUtil;
	}

	public String getSqlChargeGroupCodeNDescription() {
		return sqlChargeGroupCodeNDescription;
	}

	public void setSqlChargeGroupCodeNDescription(String sqlChargeGroupCodeNDescription) {
		this.sqlChargeGroupCodeNDescription = sqlChargeGroupCodeNDescription;
	}

	/**
	 * @return Returns the paxTypesMap.
	 */
	public HashMap<String, PaxTypeTO> getPaxTypesMap() {
		paxTypesMap = new HashMap<String, PaxTypeTO>();

		if (dataSource != null && queryStringForPaxTypes != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForPaxTypes, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						PaxTypeTO paxtypeTO;

						String paxTypeCode;
						String description;
						long version;
						String fareAmountType;
						BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						boolean applyFare;
						boolean applyModCharge;
						boolean applyCnxCharge;
						boolean fareRefundable;
						boolean xbeBookingsAllowed;
						int cutOffAgeInYears;
						BigDecimal noShowChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

						while (rs.next()) {

							paxTypeCode = PlatformUtiltiies.nullHandler(rs.getString("PAX_TYPE_CODE"));
							description = PlatformUtiltiies.nullHandler(rs.getString("DESCRIPTION"));
							version = rs.getLong("VERSION");
							fareAmountType = PlatformUtiltiies.nullHandler(rs.getString("FARE_AMOUNT_TYPE"));
							fareAmount = rs.getBigDecimal("FARE_AMOUNT");
							applyFare = PlatformUtiltiies.nullHandler(rs.getString("APPLY_FARE")).equals("Y") ? true : false;
							applyModCharge = PlatformUtiltiies.nullHandler(rs.getString("APPLY_MOD_CHARGE")).equals("Y") ? true
									: false;
							applyCnxCharge = PlatformUtiltiies.nullHandler(rs.getString("APPLY_CNX_CHARGE")).equals("Y") ? true
									: false;
							fareRefundable = PlatformUtiltiies.nullHandler(rs.getString("REFUNDABLE_FLAG")).equals("Y") ? true
									: false;
							xbeBookingsAllowed = PlatformUtiltiies.nullHandler(rs.getString("XBE_BOOKINGS_ALLOWED")).equals("Y") ? true
									: false;
							cutOffAgeInYears = rs.getInt("CUT_OFF_AGE_IN_YEARS");
							noShowChargeAmount = rs.getBigDecimal("NO_SHOW_CHARGE");

							paxtypeTO = new PaxTypeTO();
							paxtypeTO.setPaxTypeCode(paxTypeCode);
							paxtypeTO.setDescription(description);
							paxtypeTO.setVersion(version);
							paxtypeTO.setFareAmountType(fareAmountType);
							paxtypeTO.setFareAmount(fareAmount);
							paxtypeTO.setApplyFare(applyFare);
							paxtypeTO.setApplyModCharge(applyModCharge);
							paxtypeTO.setApplyCnxCharge(applyCnxCharge);
							paxtypeTO.setFareRefundable(fareRefundable);
							paxtypeTO.setXbeBookingsAllowed(xbeBookingsAllowed);
							paxtypeTO.setCutOffAgeInYears(cutOffAgeInYears);
							paxtypeTO.setNoShowChargeAmount(noShowChargeAmount);

							/** JIRA: AARESAA-2715 */
							if (paxTypeCode.equalsIgnoreCase(PaxTypeTO.INFANT)) {
								paxtypeTO.setAgeLowerBoundaryInDays(rs.getInt("CUTOFF_AGE_LOW_IN_DAYS"));
							} else if (paxTypeCode.equalsIgnoreCase(PaxTypeTO.CHILD)) {
								paxtypeTO.setAgeLowerBoundaryInDays(rs.getInt("CUTOFF_AGE_LOW_IN_DAYS"));
								paxtypeTO.setAgeLowerBoundaryInMonths(rs.getInt("CUTOFF_AGE_LOW_IN_MONS"));
							} else if (paxTypeCode.equalsIgnoreCase(PaxTypeTO.ADULT)) {
								paxtypeTO.setAgeLowerBoundaryInMonths(rs.getInt("CUTOFF_AGE_LOW_IN_MONS"));
							}

							paxTypesMap.put(paxTypeCode, paxtypeTO);

						}
					} else {
						throw new PlatformRuntimeException("platform.data.paxTypes.notconfigured");
					}
					return paxTypesMap;
				}
			});
		}
		return paxTypesMap;
	}

	public List<BaseContactConfigDTO> getContactDeailsConfig(String appName) {
		contactConfigDTOList = new ArrayList<BaseContactConfigDTO>();
		if (dataSource != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			if (queryStringForContactDetailsConfig != null) {
				if (AppIndicatorEnum.APP_XBE.toString().equals(appName)) {
					template.query(queryStringForContactDetailsConfig, new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							if (rs != null) {
								while (rs.next()) {
									XBEContactConfigDTO xbeConfig = new XBEContactConfigDTO();
									xbeConfig.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
									xbeConfig.setGroupId(rs.getInt("GROUP_ID"));
									xbeConfig.setXbeVisibility((PlatformUtiltiies.nullHandler(rs.getString("XBE_VISIBILITY"))
											.equals("Y") ? true : false));
									xbeConfig.setMandatory((PlatformUtiltiies.nullHandler(rs.getString("MANDATORY")).equals("Y") ? true
											: false));
									xbeConfig.setValidationGroup(PlatformUtiltiies.nullHandler(rs.getString("VALIDATION_GROUP")));
									xbeConfig.setMaxLength((PlatformUtiltiies.nullHandler(rs.getString("MAX_LENGTH"))));
									contactConfigDTOList.add(xbeConfig);
								}
							}

							return contactConfigDTOList;
						}
					});
				} else if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appName)) {
					template.query(queryStringForContactDetailsConfig, new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							if (rs != null) {
								while (rs.next()) {
									IBEContactConfigDTO ibeConfig = new IBEContactConfigDTO();
									ibeConfig.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
									ibeConfig.setGroupId(rs.getInt("GROUP_ID"));
									ibeConfig.setIbeVisibility((PlatformUtiltiies.nullHandler(rs.getString("IBE_VISIBILITY"))
											.equals("Y") ? true : false));
									ibeConfig.setMandatory((PlatformUtiltiies.nullHandler(rs.getString("MANDATORY")).equals("Y") ? true
											: false));
									ibeConfig.setValidationGroup(PlatformUtiltiies.nullHandler(rs.getString("VALIDATION_GROUP")));
									ibeConfig.setMaxLength((PlatformUtiltiies.nullHandler(rs.getString("MAX_LENGTH"))));
									contactConfigDTOList.add(ibeConfig);
								}
							}

							return contactConfigDTOList;
						}
					});
				} else {
					template.query(queryStringForContactDetailsConfig, new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							if (rs != null) {
								while (rs.next()) {
									WSContactConfigDTO wsConfig = new WSContactConfigDTO();
									wsConfig.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
									wsConfig.setGroupId(rs.getInt("GROUP_ID"));
									wsConfig.setWsVisibility((PlatformUtiltiies.nullHandler(rs.getString("WS_VISIBILITY"))
											.equals("Y") ? true : false));
									wsConfig.setMandatory((PlatformUtiltiies.nullHandler(rs.getString("MANDATORY")).equals("Y") ? true
											: false));
									wsConfig.setValidationGroup(PlatformUtiltiies.nullHandler(rs.getString("VALIDATION_GROUP")));
									wsConfig.setMaxLength((PlatformUtiltiies.nullHandler(rs.getString("MAX_LENGTH"))));
									contactConfigDTOList.add(wsConfig);
								}
							}

							return contactConfigDTOList;
						}
					});
				}
			}
		}

		return contactConfigDTOList;
	}

	public List<UserRegConfigDTO> getUserRegConfig() {
		userRegConfigDTOList = new ArrayList<UserRegConfigDTO>();

		if (dataSource != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			if (queryStringForContactDetailsConfig != null) {
				template.query(queryStringForContactDetailsConfig, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs != null) {
							while (rs.next()) {
								UserRegConfigDTO userConfig = new UserRegConfigDTO();
								userConfig.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
								userConfig.setGroupId(rs.getInt("GROUP_ID"));
								userConfig.setUserRegVisibilty((PlatformUtiltiies.nullHandler(
										rs.getString("IBE_USERREG_VISIBILITY")).equals("Y") ? true : false));
								userConfig.setMandatory((PlatformUtiltiies.nullHandler(rs.getString("MANDATORY")).equals("Y") ? true
										: false));
								userConfig.setValidationGroup(PlatformUtiltiies.nullHandler(rs.getString("VALIDATION_GROUP")));
								userConfig.setMaxLength((PlatformUtiltiies.nullHandler(rs.getString("MAX_LENGTH"))));
								userRegConfigDTOList.add(userConfig);
							}
						}
						return userRegConfigDTOList;
					}
				});
			}
		}

		return userRegConfigDTOList;
	}

	public List<BasePaxConfigDTO> getPaxDetailsConfig(String appName) {
		paxConfigDTOList = new ArrayList<BasePaxConfigDTO>();
		if (dataSource != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);

			if (AppIndicatorEnum.APP_XBE.toString().equals(appName)) {

				if (queryStringForXBEPaxConfig != null) {
					template.query(queryStringForXBEPaxConfig, new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							if (rs != null) {
								while (rs.next()) {
									XBEPaxConfigDTO xbePaxConfig = new XBEPaxConfigDTO();
									xbePaxConfig.setPaxConfigId(rs.getInt("PAX_INFO_CONFIG_ID"));
									xbePaxConfig.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
									xbePaxConfig.setPaxCatCode(PlatformUtiltiies.nullHandler(rs.getString("PAX_CATEGORY_CODE")));
									xbePaxConfig.setPaxTypeCode(PlatformUtiltiies.nullHandler(rs.getString("PAX_TYPE_CODE")));
									xbePaxConfig.setSsrCode(PlatformUtiltiies.nullHandler(rs.getString("SSR_CODE")));
									xbePaxConfig.setUniqueness(PlatformUtiltiies.nullHandler(rs.getString("UNIQUENESS")));
									xbePaxConfig.setUniquenessGroup(PlatformUtiltiies.nullHandler(rs
											.getString("UNIQUENESS_GROUP")));
									xbePaxConfig.setXbeVisibility((PlatformUtiltiies.nullHandler(rs.getString("XBE_VISIBILITY"))
											.equals("Y") ? true : false));
									xbePaxConfig.setXbeMandatory((PlatformUtiltiies.nullHandler(rs.getString("XBE_MANDATORY"))
											.equals("Y") ? true : false));
									xbePaxConfig.setAutoCheckinVisibility(CommonsConstants.YES.equals(rs
											.getString("AUTOMATIC_CHECKIN_VISIBILITY")));
									xbePaxConfig.setAutoCheckinMandatory(CommonsConstants.YES.equals(rs
											.getString("AUTOMATIC_CHECKIN_MANDATORY")));
									xbePaxConfig.setMinLength(rs.getInt("MIN_LENGTH"));
									paxConfigDTOList.add(xbePaxConfig);
								}
							}
							return paxConfigDTOList;
						}
					});
				}
			} else if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appName)) {
				if (queryStringForIBEPaxConfig != null) {
					template.query(queryStringForIBEPaxConfig, new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							if (rs != null) {
								while (rs.next()) {
									IBEPaxConfigDTO ibePaxConfig = new IBEPaxConfigDTO();
									ibePaxConfig.setPaxConfigId(rs.getInt("PAX_INFO_CONFIG_ID"));
									ibePaxConfig.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
									ibePaxConfig.setPaxCatCode(PlatformUtiltiies.nullHandler(rs.getString("PAX_CATEGORY_CODE")));
									ibePaxConfig.setPaxTypeCode(PlatformUtiltiies.nullHandler(rs.getString("PAX_TYPE_CODE")));
									ibePaxConfig.setSsrCode(PlatformUtiltiies.nullHandler(rs.getString("SSR_CODE")));
									ibePaxConfig.setUniqueness(PlatformUtiltiies.nullHandler(rs.getString("UNIQUENESS")));
									ibePaxConfig.setUniquenessGroup(PlatformUtiltiies.nullHandler(rs
											.getString("UNIQUENESS_GROUP")));
									ibePaxConfig.setIbeVisibility((PlatformUtiltiies.nullHandler(rs.getString("IBE_VISIBILITY"))
											.equals("Y") ? true : false));
									ibePaxConfig.setIbeMandatory((PlatformUtiltiies.nullHandler(rs.getString("IBE_MANDATORY"))
											.equals("Y") ? true : false));
									ibePaxConfig.setMinLength(rs.getInt("MIN_LENGTH"));
									ibePaxConfig.setAutoCheckinVisibility((PlatformUtiltiies.nullHandler(
											rs.getString("AUTOMATIC_CHECKIN_VISIBILITY")).equals("Y") ? true : false));
									ibePaxConfig.setAutoCheckinMandatory((PlatformUtiltiies.nullHandler(
											rs.getString("AUTOMATIC_CHECKIN_MANDATORY")).equals("Y") ? true : false));
									paxConfigDTOList.add(ibePaxConfig);
								}
							}
							return paxConfigDTOList;
						}
					});
				}
			} else if (AppIndicatorEnum.APP_GDS.toString().equals(appName)) {
				if (queryStringForGDSPaxConfig != null) {
					template.query(queryStringForGDSPaxConfig, new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							if (rs != null) {
								while (rs.next()) {
									GDSPaxConfigDTO gdsPaxConfig = new GDSPaxConfigDTO();
									gdsPaxConfig.setPaxConfigId(rs.getInt("PAX_INFO_CONFIG_ID"));
									gdsPaxConfig.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
									gdsPaxConfig.setPaxCatCode(PlatformUtiltiies.nullHandler(rs.getString("PAX_CATEGORY_CODE")));
									gdsPaxConfig.setPaxTypeCode(PlatformUtiltiies.nullHandler(rs.getString("PAX_TYPE_CODE")));
									gdsPaxConfig.setSsrCode(PlatformUtiltiies.nullHandler(rs.getString("SSR_CODE")));
									gdsPaxConfig.setUniqueness(PlatformUtiltiies.nullHandler(rs.getString("UNIQUENESS")));
									gdsPaxConfig.setUniquenessGroup(PlatformUtiltiies.nullHandler(rs
											.getString("UNIQUENESS_GROUP")));
									gdsPaxConfig.setGdsMandatory((PlatformUtiltiies.nullHandler(rs.getString("GDS_MANDATORY"))
											.equals("Y") ? true : false));
									gdsPaxConfig.setMinLength(rs.getInt("MIN_LENGTH"));
									paxConfigDTOList.add(gdsPaxConfig);
								}
							}
							return paxConfigDTOList;
						}
					});
				}
			} else {
				if (queryStringForWSPaxConfig != null) {
					template.query(queryStringForWSPaxConfig, new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							if (rs != null) {
								while (rs.next()) {
									WSPaxConfigDTO wsPaxConfig = new WSPaxConfigDTO();
									wsPaxConfig.setPaxConfigId(rs.getInt("PAX_INFO_CONFIG_ID"));
									wsPaxConfig.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
									wsPaxConfig.setPaxCatCode(PlatformUtiltiies.nullHandler(rs.getString("PAX_CATEGORY_CODE")));
									wsPaxConfig.setPaxTypeCode(PlatformUtiltiies.nullHandler(rs.getString("PAX_TYPE_CODE")));
									wsPaxConfig.setSsrCode(PlatformUtiltiies.nullHandler(rs.getString("SSR_CODE")));
									wsPaxConfig.setUniqueness(PlatformUtiltiies.nullHandler(rs.getString("UNIQUENESS")));
									wsPaxConfig.setUniquenessGroup(PlatformUtiltiies.nullHandler(rs.getString("UNIQUENESS_GROUP")));
									wsPaxConfig.setWsVisibility((PlatformUtiltiies.nullHandler(rs.getString("WS_VISIBILITY"))
											.equals("Y") ? true : false));
									wsPaxConfig.setWsMandatory((PlatformUtiltiies.nullHandler(rs.getString("WS_MANDATORY"))
											.equals("Y") ? true : false));
									wsPaxConfig.setMinLength(rs.getInt("MIN_LENGTH"));
									paxConfigDTOList.add(wsPaxConfig);

								}
							}
							return paxConfigDTOList;
						}
					});
				}
			}
		}

		return paxConfigDTOList;
	}

	public HashMap<String, InterlinedAirLineTO> getInterlinedAirlinesMap() {
		interlinedAirlinesMap = new HashMap<String, InterlinedAirLineTO>();

		if (dataSource != null && queryStringForInterlinedAirlines != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForInterlinedAirlines, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						InterlinedAirLineTO interlinedAirLineTO;

						String carrierCode;
						String agentCode;
						String userId;
						String password;
						int interlineCutOverInMinutes;
						int transferGapInMinutes;
						int transferCutOverInMinutes;
						String status;
						String description;

						while (rs.next()) {
							carrierCode = PlatformUtiltiies.nullHandler(rs.getString("CARRIER_CODE"));
							agentCode = PlatformUtiltiies.nullHandler(rs.getString("AGENT_CODE"));
							userId = PlatformUtiltiies.nullHandler(rs.getString("USER_ID"));
							password = PlatformUtiltiies.nullHandler(rs.getString("PASSWORD"));
							status = PlatformUtiltiies.nullHandler(rs.getString("STATUS"));
							description = PlatformUtiltiies.nullHandler(rs.getString("DESCRIPTION"));
							interlineCutOverInMinutes = Util.getTotalMinutes(PlatformUtiltiies.nullHandler(rs
									.getString("INTERLINE_CUTOVER")));
							transferGapInMinutes = Util.getTotalMinutes(PlatformUtiltiies.nullHandler(rs
									.getString("TRANSFER_GAP")));
							transferCutOverInMinutes = Util.getTotalMinutes(PlatformUtiltiies.nullHandler(rs
									.getString("TRANSFER_CUTOVER")));

							interlinedAirLineTO = new InterlinedAirLineTO();
							interlinedAirLineTO.setCarrierCode(carrierCode);
							interlinedAirLineTO.setAgentCode(agentCode);
							interlinedAirLineTO.setUserId(userId);
							interlinedAirLineTO.setPassword(password);
							interlinedAirLineTO.setStatus(status);
							interlinedAirLineTO.setAirlineDesc(description);

							interlinedAirLineTO.setInterlineCutOverInMinutes(interlineCutOverInMinutes);
							interlinedAirLineTO.setTransferGapInMinutes(transferGapInMinutes);
							interlinedAirLineTO.setTransferCutOverInMinutes(transferCutOverInMinutes);

							interlinedAirlinesMap.put(carrierCode, interlinedAirLineTO);
						}
					} else {
						throw new PlatformRuntimeException("platform.data.interlinedAirlines.notConfigured");
					}

					return interlinedAirlinesMap;
				}
			});
		}
		return interlinedAirlinesMap;
	}

	public List<String> getInterlinedAirlineCodesList() {
		interlinedAirlineCodesList = new ArrayList<String>();
		if (dataSource != null && queryStringForInterlinedAirlineCodes != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForInterlinedAirlineCodes, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							interlinedAirlineCodesList.add(PlatformUtiltiies.nullHandler(rs.getString("airline_code")));
						}
					}
					return interlinedAirlineCodesList;
				}
			});
		}

		return interlinedAirlineCodesList;
	}

	// Haider store the msg code in an array
	public HashMap<String, ArrayList<String>> getAirportMessagesMap() {
		airportMessagesMap = new HashMap<String, ArrayList<String>>();
		if (dataSource != null && queryStringForAirportMessages != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForAirportMessages, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {

						String originAirport;
						String messageCode;

						while (rs.next()) {
							ArrayList<String> arrMsgCode = null;
							originAirport = PlatformUtiltiies.nullHandler(rs.getString("ORIGIN_AIRPORT_CODE"));
							messageCode = PlatformUtiltiies.nullHandler(rs.getString("MESSAGE_CODE"));
							// Haider
							arrMsgCode = airportMessagesMap.get(originAirport);
							if (arrMsgCode == null)
								arrMsgCode = new ArrayList<String>();
							arrMsgCode.add(messageCode);
							airportMessagesMap.put(originAirport, arrMsgCode);
						}
					} else {
						throw new PlatformRuntimeException("platform.data.interlinedAirlines.notConfigured");
					}

					return airportMessagesMap;
				}
			});
		}
		return airportMessagesMap;
	}

	/**
	 * Loads hub information.
	 */
	public Map<Integer, HubAirportTO> loadHubs() {

		final Map<Integer, HubAirportTO> hubs = new HashMap<Integer, HubAirportTO>();

		if (dataSource != null && queryStringForHubs != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForHubs, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {

						HubAirportTO hubAirportTO;
						int hubId;
						while (rs.next()) {
							hubAirportTO = new HubAirportTO();
							hubId = rs.getInt("HUB_ID");
							hubAirportTO.setHubId(hubId);
							hubAirportTO.setAirportCode(rs.getString("AIRPORT_CODE"));
							hubAirportTO.setMinCxnTime(rs.getString("MIN_CXN_TIME"));
							hubAirportTO.setMaxCxnTime(rs.getString("MAX_CXN_TIME"));
							hubAirportTO.setInwardsCarrierCode(rs.getString("IB_CARRIER_CODE"));
							hubAirportTO.setOutwardsCarrierCode(rs.getString("OB_CARRIER_CODE"));

							hubs.put(hubId, hubAirportTO);
						}
					} else {
						throw new PlatformRuntimeException("platform.data.hubs.notconfigured");
					}
					return hubs;
				}
			});
		}
		return hubs;
	}

	/**
	 * Loads currency information.
	 */
	public List<String> loadActiveCurrency() {

		final List<String> currencies = new ArrayList<String>();

		if (dataSource != null && queryActiveCurrencyCodes != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryActiveCurrencyCodes, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							currencies.add(rs.getString("currency_code"));
						}
					} else {
						throw new PlatformRuntimeException("platform.data.currencies.notconfigured");
					}
					return currencies;
				}
			});
		}
		return currencies;
	}

	public List<PaxCategoryTO> getPaxCategoryList() {
		paxCategoryList = new ArrayList<PaxCategoryTO>();

		if (dataSource != null && queryStringForPaxCategory != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryStringForPaxCategory, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							PaxCategoryTO paxCatTO = new PaxCategoryTO();
							paxCatTO.setPaxCategoryCode(PlatformUtiltiies.nullHandler(rs.getString("pax_category_code")));
							paxCatTO.setPaxCategoryDesc(PlatformUtiltiies.nullHandler(rs.getString("pax_category_desc")));
							paxCatTO.setDefaultCurrency(PlatformUtiltiies.nullHandler(rs.getString("default_currency_code")));
							paxCatTO.setDefaultSortOrder(rs.getInt("default_sort_order"));
							paxCatTO.setStatus(PaxCategoryTO.PaxCategoryStatusCodes.valueOf(PlatformUtiltiies.nullHandler(rs
									.getString("status"))));

							paxCategoryList.add(paxCatTO);
						}
					}
					return paxCategoryList;
				}
			});
		}

		return paxCategoryList;
	}
	
	public List<AdminFeeAgentTO> getAdminFeeAgentList() {
		adminFeeAgentList = new ArrayList<AdminFeeAgentTO>();

		if (dataSource != null && queryStringForAdminFeeAgent != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryStringForAdminFeeAgent, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							AdminFeeAgentTO adminFeeAgentTO = new AdminFeeAgentTO();
							adminFeeAgentTO.setAdminFeeAgentCode(PlatformUtiltiies.nullHandler(rs.getString("AGENT_CODE")));
							adminFeeAgentList.add(adminFeeAgentTO);
						}
					}
					return adminFeeAgentList;
				}
			});
		}

		return adminFeeAgentList;
	}

	/**
	 * @return Returns the BookingClassMappingMap.
	 */
	public Map<BookingClassCharMappingTO, String> getBookingClassCharMappingMap() {
		bookingClassCharMappingMap = new HashMap<BookingClassCharMappingTO, String>();

		if (dataSource != null && queryStringForGDSBookingClassMapping != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForGDSBookingClassMapping, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						String mappedBC;
						BookingClassCharMappingTO bookingClassCharMappingTO = null;

						while (rs.next()) {
							bookingClassCharMappingTO = new BookingClassCharMappingTO();
							bookingClassCharMappingTO.setGdsId(rs.getInt("gds_id"));
							bookingClassCharMappingTO.setBookingCode(PlatformUtiltiies.nullHandler(rs.getString("booking_code")));
							mappedBC = PlatformUtiltiies.nullHandler(rs.getString("mapped_bc"));

							if (mappedBC != null && !mappedBC.equals("")) {
								bookingClassCharMappingMap.put(bookingClassCharMappingTO, mappedBC);
							}
						}

					} else {
						throw new PlatformRuntimeException("platform.data.bookingClassMapping.notconfigured");
					}
					return bookingClassCharMappingMap;
				}
			});
		}
		return bookingClassCharMappingMap;
	}

	/**
	 * @return Returns the BookingClassMappingMap.
	 */
	public Map<Integer, Map<String, String>> getGDSBookingClassMap() {
		gdsBookingClassMap = new HashMap<Integer, Map<String, String>>();

		if (dataSource != null && queryStringForGDSBookingClassMapping != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForGDSBookingClassMapping, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						String bc;
						String mappedBC;
						HashMap<String, String> bcMap = null;
						while (rs.next()) {
							Integer gdsId = rs.getInt("gds_id");
							if (gdsBookingClassMap.get(gdsId) == null) {
								bcMap = new HashMap<String, String>();
								gdsBookingClassMap.put(gdsId, bcMap);
							}
							bc = PlatformUtiltiies.nullHandler(rs.getString("booking_code"));
							mappedBC = PlatformUtiltiies.nullHandler(rs.getString("mapped_bc"));
							if (!mappedBC.equals("") && !bc.equals("")) {
								gdsBookingClassMap.get(gdsId).put(bc, mappedBC);
							}
						}

					} else {
						throw new PlatformRuntimeException("platform.data.bookingClassMapping.notconfigured");
					}
					return gdsBookingClassMap;
				}
			});
		}
		return gdsBookingClassMap;
	}

	/**
	 * @return Returns the BookingClassMappingMap.
	 */
	public Map<Integer, Map<String, String>> getCSBookingClassMap() {
		csBookingClassMap = new HashMap<Integer, Map<String, String>>();

		if (dataSource != null && queryStringForCSBookingClassMapping != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForCSBookingClassMapping, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						String bc;
						String extBc;
						HashMap<String, String> bcMap = null;
						while (rs.next()) {
							Integer gdsId = rs.getInt("gds_id");
							if (csBookingClassMap.get(gdsId) == null) {
								bcMap = new HashMap<String, String>();
								csBookingClassMap.put(gdsId, bcMap);
							}
							bc = PlatformUtiltiies.nullHandler(rs.getString("mapped_bc"));
							extBc = PlatformUtiltiies.nullHandler(rs.getString("ext_booking_code"));
							if (!extBc.equals("") && !bc.equals("")) {
								csBookingClassMap.get(gdsId).put(bc, extBc);
							}
						}

					} else {
						throw new PlatformRuntimeException("platform.data.bookingClassMapping.notconfigured");
					}
					return csBookingClassMap;
				}
			});
		}
		return csBookingClassMap;
	}

	/**
	 * @return Returns the One to One mapped MAPPED_BC GDS wise.
	 * 
	 *         very important when processing AVS messages
	 */
	public Map<Integer, List<String>> getSingleEntryMappedGDSBookingClasses() {
		gdsBookingClassMap = new HashMap<Integer, Map<String, String>>();
		gdsMappedBookingClassList = new HashMap<Integer, List<String>>();

		if (dataSource != null && queryStringForSingleEntryMappedGdsBCs != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForSingleEntryMappedGdsBCs, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						String mappedBC;
						List<String> bcList = null;
						while (rs.next()) {
							Integer gdsId = rs.getInt("gds_id");
							if (gdsMappedBookingClassList.get(gdsId) == null) {
								bcList = new ArrayList<String>();
								gdsMappedBookingClassList.put(gdsId, bcList);
							}

							mappedBC = PlatformUtiltiies.nullHandler(rs.getString("mapped_bc"));
							if (!"".equals(mappedBC)) {
								gdsMappedBookingClassList.get(gdsId).add(mappedBC);
							}
						}

					} else {
						throw new PlatformRuntimeException("platform.data.bookingClassMapping.notconfigured");
					}
					return gdsMappedBookingClassList;
				}
			});
		}
		return gdsMappedBookingClassList;
	}

	public Map<Integer, List<String>> getServiceTypesByOperationTypeMap() {
		serviceTypeByOperationType = new HashMap<Integer, List<String>>();
		if (dataSource != null && queryStringForServiceTypeByOperationType != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForServiceTypeByOperationType, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {

							int operationTypeId = rs.getInt("OPERATION_TYPE_ID");
							String serviceType = rs.getString("IATA_SERVICE_TYPE");

							if (!StringUtil.isNullOrEmpty(serviceType)) {
								List<String> list = new ArrayList<String>(Arrays.asList(serviceType.split(",")));
								serviceTypeByOperationType.put(operationTypeId, list);
							}

						}

					} else {
						throw new PlatformRuntimeException("platform.data.service.type.notconfigured");
					}
					return serviceTypeByOperationType;
				}
			});
		}
		return serviceTypeByOperationType;
	}

	@SuppressWarnings("unchecked")
	public Merchant getMerchantByID(String merchantID) {

		Merchant merchant = null;
		Object[] params = new Object[] { merchantID };

		String getMerchantQuery = "SELECT * FROM t_merchant m WHERE m.merchant_id= ? AND m.status = 'ACT'";

		JdbcTemplate templete = new JdbcTemplate(dataSource);

		merchant = (Merchant) templete.query(getMerchantQuery, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Merchant merchant = null;
				if (rs != null) {
					if (rs.next()) {
						merchant = new Merchant();
						merchant.setMerchantID(rs.getString("MERCHANT_ID"));
						merchant.setDisplayName(rs.getString("DISPLAY_NAME"));
						merchant.setUsername(rs.getString("USERNAME"));
						merchant.setPassword(rs.getString("PASSWORD"));
						merchant.setSecureHashKey(rs.getString("SECURE_HASH_KEY"));
						merchant.setStatus(rs.getString("STATUS"));
						merchant.setEntityCode(rs.getString("ENTITY_CODE"));
					}

				} else {
					throw new PlatformRuntimeException("platform.data.bookingClassMapping.notconfigured");
				}
				return merchant;
			}
		});

		return merchant;
	}

	public Map<String, List<String>> getBookingClassByLogicalCCMap() {
		bookingClassByLogicalCCMap = new HashMap<String, List<String>>();
		if (dataSource != null && queryStringForBookingClassesByLogicalCC != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForBookingClassesByLogicalCC, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						List<String> bcList = null;
						while (rs.next()) {
							String logicalCC = rs.getString("LOGICAL_CABIN_CLASS_CODE");
							String bookingCode = rs.getString("BOOKING_CODE");
							if (bookingClassByLogicalCCMap.get(logicalCC) == null) {
								bcList = new ArrayList<String>();
								bookingClassByLogicalCCMap.put(logicalCC, bcList);
							}
							bookingClassByLogicalCCMap.get(logicalCC).add(bookingCode);
						}

					} else {
						throw new PlatformRuntimeException("platform.data.bookingClassMapping.notconfigured");
					}
					return bookingClassByLogicalCCMap;
				}
			});
		}
		return bookingClassByLogicalCCMap;
	}

	/**
	 * @param paxTypesMap
	 *            The paxTypesMap to set.
	 */
	public void setPaxTypesMap(HashMap<String, PaxTypeTO> paxTypesMap) {
		this.paxTypesMap = paxTypesMap;
	}

	/**
	 * @return Returns the fareCategoryCollection
	 */
	@SuppressWarnings("unchecked")
	public Map<String, FareCategoryTO> getFareCategoryMap() {
		final Map<String, FareCategoryTO> mapSortedFareCategoryTO = new LinkedHashMap<String, FareCategoryTO>();

		if (dataSource != null && queryStringForFareCategories != null) {
			List<Map<String, Object>> fareCatList = null;
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			fareCatList = templete.queryForList(queryStringForFareCategories);

			if (fareCatList != null) {
				FareCategoryTO fareCategoryTO = null;

				for (Iterator<Map<String, Object>> iter = fareCatList.iterator(); iter.hasNext();) {
					int defaultSortOrder;
					String fareCategoryCode, fareCategoryDesc;
					String showComments, isDefault, status;

					Map<String, Object> dataMap = iter.next();

					fareCategoryCode = PlatformUtiltiies.nullHandler(dataMap.get("FARE_CAT_ID"));
					fareCategoryDesc = PlatformUtiltiies.nullHandler(dataMap.get("DESCRIPTION"));
					defaultSortOrder = ((BigDecimal) dataMap.get("DEFAULT_SORT_ORDER")).intValue();
					showComments = PlatformUtiltiies.nullHandler(dataMap.get("SHOW_COMMENTS"));
					isDefault = PlatformUtiltiies.nullHandler(dataMap.get("IS_DEFAULT"));
					status = PlatformUtiltiies.nullHandler(dataMap.get("STATUS"));

					fareCategoryTO = new FareCategoryTO();
					fareCategoryTO.setFareCategoryCode(fareCategoryCode);
					fareCategoryTO.setFareCategoryDesc(fareCategoryDesc);
					fareCategoryTO.setDefaultSortOrder(defaultSortOrder);
					fareCategoryTO.setShowComments(showComments.equals("Y") ? true : false);
					fareCategoryTO.setDefault(isDefault.equals("Y") ? true : false);
					fareCategoryTO.setStatus(FareCategoryTO.FareCategoryStatusCodes.valueOf(status));

					if (!mapSortedFareCategoryTO.containsKey(fareCategoryCode)) {
						mapSortedFareCategoryTO.put(fareCategoryCode, fareCategoryTO);
					}
				}
			} else {
				throw new PlatformRuntimeException("platform.data.farecategories.notconfigured");
			}
		}

		return mapSortedFareCategoryTO;
	}

	public Map<Integer, ChargeApplicabilityTO> loadChargeApplicabilitiesMap() {
		final Map<Integer, ChargeApplicabilityTO> chargeApplicabilitiesMap = new LinkedHashMap<Integer, ChargeApplicabilityTO>();

		if (dataSource != null && queryChargeApplicabilities != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryChargeApplicabilities, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						ChargeApplicabilityTO chargeApplicabilityTO;
						int appliesTo;

						while (rs.next()) {
							chargeApplicabilityTO = new ChargeApplicabilityTO();
							appliesTo = rs.getInt("APPLIES_TO");

							chargeApplicabilityTO.setAppliesTo(appliesTo);
							chargeApplicabilityTO.setAppliesToDesc(rs.getString("APPLIES_TO_DESC"));
							chargeApplicabilityTO.setVisibility(rs.getInt("VISIBILITY") == 1 ? true : false);

							chargeApplicabilitiesMap.put(appliesTo, chargeApplicabilityTO);
						}
					} else {
						throw new PlatformRuntimeException("platform.data.chargeapplicabilities.notconfigured");
					}
					return chargeApplicabilitiesMap;
				}
			});
		}
		return chargeApplicabilitiesMap;
	}

	/**
	 * @return Returns the queryStringForFareCategories.
	 */
	public String getQueryStringForFareCategories() {
		return queryStringForFareCategories;
	}

	/**
	 * @param queryStringForFareCategories
	 *            The queryStringForFareCategories to set.
	 */
	public void setQueryStringForFareCategories(String queryStringForFareCategories) {
		this.queryStringForFareCategories = queryStringForFareCategories;
	}

	public String getQueryStringForPaxTypes() {
		return queryStringForPaxTypes;
	}

	public void setQueryStringForPaxTypes(String queryStringForPaxTypes) {
		this.queryStringForPaxTypes = queryStringForPaxTypes;
	}

	/**
	 * @return the queryStringForContactDetailsConfig
	 */
	public String getQueryStringForContactDetailsConfig() {
		return queryStringForContactDetailsConfig;
	}

	/**
	 * @param queryStringForContactDetailsConfig
	 *            the queryStringForContactDetailsConfig to set
	 */
	public void setQueryStringForContactDetailsConfig(String queryStringForContactDetailsConfig) {
		this.queryStringForContactDetailsConfig = queryStringForContactDetailsConfig;
	}

	/**
	 * @return the queryStringForHubs
	 */
	public String getQueryStringForHubs() {
		return queryStringForHubs;
	}

	/**
	 * @return the queryStringForActualPayModes
	 */
	public String getQueryStringForActualPayModes() {
		return queryStringForActualPayModes;
	}

	/**
	 * @param queryStringForActualPayModes
	 *            the queryStringForActualPayModes to set
	 */
	public void setQueryStringForActualPayModes(String queryStringForActualPayModes) {
		this.queryStringForActualPayModes = queryStringForActualPayModes;
	}

	/**
	 * @param queryStringForHubs
	 *            the queryStringForHubs to set
	 */
	public void setQueryStringForHubs(String queryStringForHubs) {
		this.queryStringForHubs = queryStringForHubs;
	}

	/**
	 * @return Returns the queryStringForInterlinedAirlines.
	 */
	public String getQueryStringForInterlinedAirlines() {
		return queryStringForInterlinedAirlines;
	}

	/**
	 * @param queryStringForInterlinedAirlines
	 *            The queryStringForInterlinedAirlines to set.
	 */
	public void setQueryStringForInterlinedAirlines(String queryStringForInterlinedAirlines) {
		this.queryStringForInterlinedAirlines = queryStringForInterlinedAirlines;
	}

	/**
	 * @return the queryStringForInterlinedAirlineCodes
	 */
	public String getQueryStringForInterlinedAirlineCodes() {
		return queryStringForInterlinedAirlineCodes;
	}

	/**
	 * @param queryStringForInterlinedAirlineCodes
	 *            the queryStringForInterlinedAirlineCodes to set
	 */
	public void setQueryStringForInterlinedAirlineCodes(String queryStringForInterlinedAirlineCodes) {
		this.queryStringForInterlinedAirlineCodes = queryStringForInterlinedAirlineCodes;
	}

	/**
	 * @return Returns the queryChargeApplicabilities.
	 */
	public String getQueryChargeApplicabilities() {
		return queryChargeApplicabilities;
	}

	/**
	 * @param queryChargeApplicabilities
	 *            The queryChargeApplicabilities to set.
	 */
	public void setQueryChargeApplicabilities(String queryChargeApplicabilities) {
		this.queryChargeApplicabilities = queryChargeApplicabilities;
	}

	/**
	 * @return Map<String,String> of carriers in the system keyed by carrier code
	 */
	public Map<String, String> getCarriersMap(String status) {
		carriersMap = new HashMap<String, String>();
		if (dataSource != null && queryForCarriers != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForCarriers, new Object[] { status == null ? "%" : status }, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							carriersMap.put(rs.getString("CARRIER_CODE"), rs.getString("DESCRIPTION"));
						}
					} else {
						throw new PlatformRuntimeException("platform.data.carriers.notconfigured");
					}
					return carriersMap;
				}
			});
		}
		return carriersMap;
	}

	/**
	 * @return Map<String,String> of carrier & there airline code in the system keyed by carrier code
	 */
	public Map<String, String> getCarrierAilineCodeMap() {
		carrierAirlineCodeMap = new HashMap<String, String>();
		if (dataSource != null && queryForCarrierAirlineMap != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForCarrierAirlineMap, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							carrierAirlineCodeMap.put(rs.getString("CARRIER_CODE"), rs.getString("AIRLINE_CODE"));
						}
					} else {
						throw new PlatformRuntimeException("platform.data.carriers.notconfigured");
					}
					return carrierAirlineCodeMap;
				}
			});
		}
		return carrierAirlineCodeMap;
	}

	public Set<String> getInterlinedAirlineCodes() {
		interlineAirlineCodes = new HashSet<String>();
		if (dataSource != null && queryForCarriers != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryForInterlinedAirlineCodes, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							interlineAirlineCodes.add(rs.getString("carrier_code"));
						}
					}
					return interlineAirlineCodes;
				}
			});
		}
		return interlineAirlineCodes;
	}

	public Set<String> getOwnAirlineCarrierCodes() {
		ownAirlineCarrierCodes = new HashSet<String>();
		if (dataSource != null && queryForOwnAirlineCarriers != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryForOwnAirlineCarriers, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							ownAirlineCarrierCodes.add(rs.getString("carrier_code"));
						}
					}
					return ownAirlineCarrierCodes;
				}
			});
		}
		return ownAirlineCarrierCodes;
	}

	// Bus carriers are excluded
	public Set<String> getOwnAirCarrierCodesExcludingBus() {
		ownAirCarrierCodes = new HashSet<String>();
		if (dataSource != null && queryForOwnAirCarriers != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryForOwnAirCarriers, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							ownAirCarrierCodes.add(rs.getString("carrier_code"));
						}
					}
					return ownAirCarrierCodes;
				}
			});
		}
		return ownAirCarrierCodes;
	}

	public Set<String> getChargehideCurrencies() {
		chargeHideCurrencies = new HashSet<String>();
		if (dataSource != null && queryForHideChargeCurrencies != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryForHideChargeCurrencies, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							chargeHideCurrencies.add(rs.getString("currency_code"));
						}
					}
					return chargeHideCurrencies;
				}
			});
		}
		return chargeHideCurrencies;
	}

	public Set<String> getBusConnectingAirports() {
		busConnectingAirports = new HashSet<String>();
		if (dataSource != null && queryForCarriers != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryStringBusConnectingAirports, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							busConnectingAirports.add(rs.getString("connecting_airport"));
						}
					}
					return busConnectingAirports;
				}
			});
		}
		return busConnectingAirports;
	}

	public List<CabinClassWiseDefaultLogicalCCDetailDTO> getDefaultLogicalCCDetails() {
		defaultLogicalCCDetails = new ArrayList<CabinClassWiseDefaultLogicalCCDetailDTO>();
		if (dataSource != null && queryForCarriers != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryStringDefaultLogicalCCDetails, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					while (rs.next()) {
						CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO = new CabinClassWiseDefaultLogicalCCDetailDTO();
						defaultLogicalCCDetailDTO.setCcCode(rs.getString("cabin_class_code"));
						defaultLogicalCCDetailDTO.setDefaultLogicalCCCode(rs.getString("logical_cabin_class_code"));
						defaultLogicalCCDetailDTO.setLogicalCCDes(rs.getString("description"));
						defaultLogicalCCDetailDTO.setLogicalCCRank(rs.getInt("logical_cc_rank"));
						defaultLogicalCCDetails.add(defaultLogicalCCDetailDTO);
					}
					return defaultLogicalCCDetails;
				}
			});
		}
		return defaultLogicalCCDetails;
	}

	public Map<String, LogicalCabinClassDTO> getAvailableLogicalCCMap() {
		availableLogicalCCMap = new HashMap<String, LogicalCabinClassDTO>();

		if (dataSource != null && queryStringForLogicalCabinClasses != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryStringForLogicalCabinClasses, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				//	Map<String, LogicalCabinClassDTO> logicalCabinClassHolder = new HashMap<String, LogicalCabinClassDTO>();

					while (rs.next()) {

						String logicalCabinCode = rs.getString("logical_cabin_class_code");

						if (!availableLogicalCCMap.containsKey(logicalCabinCode)) {	
							LogicalCabinClassDTO logicalCC= new LogicalCabinClassDTO();
							logicalCC.setLogicalCCCode(logicalCabinCode);
							logicalCC.setDescriptionI18nKey(rs.getString("description_i18n_message_key"));
							logicalCC.setCabinClassCode(rs.getString("cabin_class_code"));
							logicalCC.setStatus(rs.getString("status"));
							logicalCC.setNestRank(rs.getInt("logical_cc_rank"));
							logicalCC.setAllowSingleMealOnly(CommonsConstants.YES.equals(rs.getString("asm")));
							logicalCC.setFreeSeatEnabled(CommonsConstants.YES.equals(rs.getString("fse")));
							logicalCC.setFreeFlexiEnabled(CommonsConstants.YES.equals(rs.getString("ffe")));
							logicalCC.setFlexiDescriptioni18nMessageKey(rs.getString("flexi_desc_i18n_message_key"));
							logicalCC.setCommentI18nMessageKey(rs.getString("comments_i18n_message_key"));
							logicalCC.setFlexiCommentI18nMessageKey(rs.getString("flexi_comment_i18n_message_key"));
//							logicalCC.setBundleDescTemplateId(rs.getInt("bundle_desc_template_id"));
//							if (rs.wasNull()) {
//								logicalCC.setBundleDescTemplateId(null);
//							}

							//Population of display temmplates for the logical cabin class
							Integer bundleDescTemplateId = rs.getInt("bundle_desc_template_id");
							if(bundleDescTemplateId != null && bundleDescTemplateId > 0){
								BundleFareDescriptionTemplateDTO descriptionTemplate = new BundleFareDescriptionTemplateDTO();
								logicalCC.setBundleDescTemplateId(bundleDescTemplateId);
								descriptionTemplate.setTemplateID(bundleDescTemplateId);
								descriptionTemplate.setTemplateName(rs.getString("bundle_desc_template_name"));
								descriptionTemplate.setDefaultFreeServicesContent(StringUtil.getUnicode(rs.getString("default_free_services_template")));
								descriptionTemplate.setDefaultPaidServicesContent(StringUtil.getUnicode(rs.getString("default_paid_services_template")));
								logicalCC.setBundleFareDescriptionTemplateDTO(descriptionTemplate);
								
							}
							availableLogicalCCMap.put(logicalCabinCode, logicalCC);
						}

						LogicalCabinClassDTO logicalCC = availableLogicalCCMap.get(logicalCabinCode);
						BundleFareDescriptionTemplateDTO  descriptionTemplateDTO = logicalCC.getBundleFareDescriptionTemplateDTO();
			
						
						String transLanguageCode = rs.getString("bflcode");
						if(descriptionTemplateDTO != null && descriptionTemplateDTO.getTemplateID() != null && !StringUtil.isNullOrEmpty(transLanguageCode)){
							Map<String, BundleFareDescriptionTranslationTemplateDTO> displayTranslationsMap=descriptionTemplateDTO.getTranslationTemplates();
							
							if (!displayTranslationsMap.containsKey(transLanguageCode)){
								BundleFareDescriptionTranslationTemplateDTO translationDTO = new BundleFareDescriptionTranslationTemplateDTO();
								translationDTO.setTemplateID(descriptionTemplateDTO.getTemplateID());
								translationDTO.setLanguageCode(transLanguageCode);
								translationDTO.setFreeServicesContent(StringUtil.getUnicode(rs.getString("free_services_template")));
								translationDTO.setPaidServicesContent(StringUtil.getUnicode(rs.getString("paid_services_template")));
								displayTranslationsMap.put(transLanguageCode, translationDTO);
							}
							
							
						}
					}
					
					return availableLogicalCCMap;
				}
			});
		}

		return availableLogicalCCMap;
	}

	/**
	 * Returns a Map that will hold all the internationalized messages T_i18n_MESSAGES table Key will be the
	 * i18n_MESSAGE_KEY. Value is a Map<String,String> the key is the language code and value will be the
	 * internationalized message content.
	 * 
	 * @return Map containing the results
	 */
	public Map<String, Map<String, String>> getInternationalizedMessages() {
		final Map<String, Map<String, String>> internationalizedMessages = new HashMap<String, Map<String, String>>();

		if (dataSource != null && queryStringForLogicalCabinClassesTranslations != null) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			template.query(queryStringForLogicalCabinClassesTranslations,
					new String[] { I18nTranslationUtil.I18nMessageCategory.MCC_DES.toString() }, new ResultSetExtractor() {
						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							while (rs.next()) {
								String i18n_message_key = rs.getString("i18n_message_key");
								String message_locale = rs.getString("message_locale");
								StringBuffer msgBuffer = new StringBuffer();
								String aux;
								try {
									BufferedReader br = new BufferedReader(rs.getClob("message_content").getCharacterStream());
									while ((aux = br.readLine()) != null) {
										msgBuffer.append(aux);
									}
								} catch (IOException e) {
									log.error("Error converting Clob to String", e);
								}
								String message_content = StringUtil.getUnicode(msgBuffer.toString().trim());
								if (internationalizedMessages.get(i18n_message_key) == null) {
									internationalizedMessages.put(i18n_message_key, new HashMap<String, String>());
								}
								internationalizedMessages.get(i18n_message_key).put(message_locale, message_content);
							}
							return internationalizedMessages;
						}
					});
		}
		return internationalizedMessages;
	}

	public String getQueryForInterlinedAirlineCodes() {
		return queryForInterlinedAirlineCodes;
	}

	public void setQueryForInterlinedAirlineCodes(String queryForInterlinedAirlineCodes) {
		this.queryForInterlinedAirlineCodes = queryForInterlinedAirlineCodes;
	}

	public String getQueryForCarriers() {
		return queryForCarriers;
	}

	public void setQueryForCarriers(String queryForCarriers) {
		this.queryForCarriers = queryForCarriers;
	}

	public String getQueryStringForPaxCategory() {
		return queryStringForPaxCategory;
	}

	public void setQueryStringForPaxCategory(String queryStringForPaxCategory) {
		this.queryStringForPaxCategory = queryStringForPaxCategory;
	}

	/**
	 * @return Map<String,String> of cabin classes of the system
	 */
	public Map<String, String> getActiveCabinClasses() {
		cabinClassesMap = new LinkedHashMap<String, String>();
		if (dataSource != null && queryForActiveCabinClasses != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForActiveCabinClasses, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							cabinClassesMap.put(rs.getString("cabin_class_code"), rs.getString("description"));
						}
					} else {
						throw new PlatformRuntimeException("platform.data.cabinclasses.notconfigured");
					}
					return cabinClassesMap;
				}
			});
		}
		return cabinClassesMap;
	}

	public Map<String, CabinClassDTO> getActiveCabinClassDTOs() {
		cabinClassDTOMap = new HashMap<String, CabinClassDTO>();
		if (dataSource != null && queryForActiveCabinClassInfo != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForActiveCabinClassInfo, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
					if (resultSet != null) {
						while (resultSet.next()) {
							CabinClassDTO cabinClassDTO = new CabinClassDTO(resultSet.getString("cabin_class_code"), resultSet
									.getInt("ranking"));
							cabinClassDTOMap.put(cabinClassDTO.getCabinClassCode(), cabinClassDTO);
						}
					} else {
						throw new PlatformRuntimeException("platform.data.cabinclasses.notconfigured");
					}
					return cabinClassDTOMap;
				}
			});
		}
		return cabinClassDTOMap;
	}

	/**
	 * @return Map<String,String> publish mechanism
	 */
	public Map<String, String> getPublishMechanism() {
		publishMechanismMap = new HashMap<String, String>();
		if (dataSource != null && queryForPublishMechanism != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForPublishMechanism, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							publishMechanismMap.put(rs.getString("SCHED_PUBLISH_MECHANISM_CODE"),
									rs.getString("SCHED_PUBLISH_MECHANISM_DESC"));
						}
					} else {
						throw new PlatformRuntimeException("platform.data.cabinclasses.notconfigured");
					}
					return publishMechanismMap;
				}
			});
		}
		return publishMechanismMap;
	}

	public String getQueryForActiveCabinClasses() {
		return queryForActiveCabinClasses;
	}

	public void setQueryForActiveCabinClasses(String queryForActiveCabinClasses) {
		this.queryForActiveCabinClasses = queryForActiveCabinClasses;
	}

	public String getQueryForActiveCabinClassInfo() {
		return queryForActiveCabinClassInfo;
	}

	public void setQueryForActiveCabinClassInfo(String queryForActiveCabinClassInfo) {
		this.queryForActiveCabinClassInfo = queryForActiveCabinClassInfo;
	}

	public HashMap<String, String> getIATAAircraftModelCodes() {

		JdbcTemplate templete = new JdbcTemplate(dataSource);
		iataModelMap = new HashMap<String, String>();

		String sql = "SELECT MODEL_NUMBER, IATA_AIRCRAFT_TYPE FROM T_AIRCRAFT_MODEL";

		templete.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					String gds_id = rs.getString("MODEL_NUMBER");
					String gds_code = rs.getString("IATA_AIRCRAFT_TYPE");
					iataModelMap.put(gds_id, gds_code);
				}
				return null;
			}
		});

		return iataModelMap;
	}

	public String getQueryForPublishMechanism() {
		return queryForPublishMechanism;
	}

	public HashMap<String, GDSStatusTO> getGdsConfigs() {

		JdbcTemplate templete = new JdbcTemplate(dataSource);
		gdsMap = new HashMap<String, GDSStatusTO>();

		String sql = "SELECT gds_id, gds_code, status, airline_res_modifiable, carrier_code, cs_carrier, user_id, aa_validating_carrier, flight_inv_modifiable, process_avs,marketing_flight_prefix,apply_global_onhold_time_limit, send_oal_segments, record_oal_segments ,type_b_sync_type, payment_type, enable_route_wise_auto_publish, ATTACH_DEF_ANCI_TEMPLATE,enable_schedule_auto_split, add_ancillary,use_aa_ets, type_a_sender_sub_id, prime_flight_enabled, TAX_REFUNDABLE FROM t_gds where status = 'ACT'";
		
		templete.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					String gds_id = rs.getString("gds_id");
					String gds_code = rs.getString("gds_code");
					String status = rs.getString("status");
					String modifiable = rs.getString("airline_res_modifiable");
					boolean isModifiable = modifiable.equals("Y") ? true : false;
					String carrierCode = rs.getString("carrier_code");
					String codeShareCarrier = rs.getString("cs_carrier");
					boolean isCodeShareCarrier = codeShareCarrier.equals("Y") ? true : false;
					String userId = rs.getString("user_id");
					String aaValidatingCarrier = rs.getString("aa_validating_carrier");
					boolean isAAValidatingCarrier = aaValidatingCarrier.equals("Y") ? true : false;
					String flightInvModifiable = rs.getString("flight_inv_modifiable");
					boolean isFlightInvModifiable = flightInvModifiable.equals("Y") ? true : false;
					String processAvs = rs.getString("process_avs");
					boolean isProcessAvs = processAvs.equals("Y") ? true : false;
					String marketingFlightPrefix = rs.getString("marketing_flight_prefix");
					String globalOHDTimeLimitApplicability = rs.getString("apply_global_onhold_time_limit");
					boolean applyGlobalOHDTimeLimit = globalOHDTimeLimitApplicability.equals("Y") ? true : false;
					String sendOALSegments = rs.getString("send_oal_segments");
					boolean isSendOALSegments = sendOALSegments.equals("Y") ? true : false;
					String recordOALSegments = rs.getString("record_oal_segments");
					boolean isRecordOALSegments = recordOALSegments.equals("Y") ? true : false;
					int typeBSyncType = rs.getInt("type_b_sync_type");
					String paymentType = rs.getString("payment_type");
					String enableAutoPublish = rs.getString("enable_route_wise_auto_publish");
					boolean isEnableAutopublish = ("Y").equals(enableAutoPublish) ? true : false;
					String enableAttachDefaultAnciTemplate = rs.getString("ATTACH_DEF_ANCI_TEMPLATE");
					boolean isenableAttachDefaultAnciTemplate = ("Y").equals(enableAttachDefaultAnciTemplate) ? true : false;
					String enableAutoScheduleSplit = rs.getString("enable_schedule_auto_split");
					boolean isEnableAutoScheduleSplit = ("Y").equals(enableAutoScheduleSplit) ? true : false;
					String  useAeroMartETS = rs.getString("use_aa_ets");
					boolean isUseAeroMartETS = ("Y").equals(useAeroMartETS) ? true : false;
					boolean isAddAncillary = "Y".equals(rs.getString("add_ancillary"));
					boolean refundGDSTax = "Y".equals(rs.getString("TAX_REFUNDABLE"));
					String typeASenderSubId = rs.getString("type_a_sender_sub_id");
					boolean primeFlightEnabled = ("Y").equals(rs.getString("prime_flight_enabled"));
					
					GDSStatusTO statusTO = new GDSStatusTO(gds_code, status, isModifiable, carrierCode, isCodeShareCarrier,
							userId, Integer.parseInt(gds_id), isAAValidatingCarrier, isFlightInvModifiable, isProcessAvs,
							applyGlobalOHDTimeLimit, isSendOALSegments, isRecordOALSegments, typeBSyncType, paymentType,
							isEnableAutopublish, isenableAttachDefaultAnciTemplate, isAddAncillary,typeASenderSubId, primeFlightEnabled, refundGDSTax);
					statusTO.setMarketingFlightPrefix(marketingFlightPrefix);
					statusTO.setEnableAutoScheduleSplit(isEnableAutoScheduleSplit);
					statusTO.setUseAeroMartETS(isUseAeroMartETS);
					
					gdsMap.put(gds_id, statusTO);
				}
				return null;
			}
		});

		return gdsMap;
	}

	public HashMap<String, List<ServiceClientTTYDTO>> getServiceClientTTYInfo() {
		JdbcTemplate templete = new JdbcTemplate(dataSource);
		ServiceClientTTYbyRequestTypeMap = new HashMap<String, List<ServiceClientTTYDTO>>();

		String sql = "SELECT * FROM MB_T_SERVICE_CLIENT_REQ_TTY";

		templete.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					String requestTypeCode = rs.getString("REQUEST_TYPE_CODE");
					String serviceClientCode = rs.getString("SERVICE_CLIENT_CODE");
					String inTTYAddress = rs.getString("IN_TTY_ADDRESS");
					String outTTYAddress = rs.getString("OUT_TTY_ADDRESS");
					ServiceClientTTYDTO serviceClientTTYDTO = new ServiceClientTTYDTO(serviceClientCode, inTTYAddress,
							outTTYAddress);
					if (ServiceClientTTYbyRequestTypeMap.get(requestTypeCode) != null) {
						ServiceClientTTYbyRequestTypeMap.get(requestTypeCode).add(serviceClientTTYDTO);
					} else {
						List<ServiceClientTTYDTO> serviceClientTTYDTOs = new ArrayList<ServiceClientTTYDTO>();
						serviceClientTTYDTOs.add(serviceClientTTYDTO);
						ServiceClientTTYbyRequestTypeMap.put(requestTypeCode, serviceClientTTYDTOs);
					}
				}
				return null;
			}
		});

		return ServiceClientTTYbyRequestTypeMap;
	}

	public HashMap<String, ServiceClientDTO> getServiceClients() {

		JdbcTemplate templete = new JdbcTemplate(dataSource);
		serviceClientsMap = new HashMap<String, ServiceClientDTO>();

		String sql = "select service_client_code, add_address_to_msg_body, email_domain, api_url from mb_t_service_client";

		templete.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					String serviceClientCode = rs.getString("service_client_code");
					String addAddressToMsgBody = rs.getString("add_address_to_msg_body");
					boolean isAddAddressToMsgBody = addAddressToMsgBody.equals("Y") ? true : false;
					String emailDomain = rs.getString("email_domain");
					String apiUrl = rs.getString("api_url");
					ServiceClientDTO serviceClientDTO = new ServiceClientDTO();
					serviceClientDTO.setServiceClientCode(serviceClientCode);
					serviceClientDTO.setAddAddressToMessageBody(isAddAddressToMsgBody);
					serviceClientDTO.setEmailDomain(emailDomain);
					serviceClientDTO.setApiUrl(apiUrl);
					serviceClientsMap.put(serviceClientCode, serviceClientDTO);
				}
				return null;
			}
		});

		return serviceClientsMap;
	}

	public HashMap<String, ServiceBearerDTO> getServiceBearers() {

		JdbcTemplate templete = new JdbcTemplate(dataSource);
		serviceBearersMap = new HashMap<String, ServiceBearerDTO>();

		String sql = "select gds_code, airline_code, sita_address, out_from_mail_address from mb_t_service_bearer";

		templete.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					String serviceClientCode = rs.getString("gds_code");
					String airlineCode = rs.getString("airline_code");
					String sitaAddress = rs.getString("sita_address");
					String outgoingEmailFromAddress = rs.getString("out_from_mail_address");
					ServiceBearerDTO serviceBearerDTO = new ServiceBearerDTO();
					serviceBearerDTO.setAirlineCode(airlineCode);
					serviceBearerDTO.setSitaAddress(sitaAddress);
					serviceBearerDTO.setOutgoingEmailFromAddress(outgoingEmailFromAddress);
					serviceBearersMap.put(serviceClientCode, serviceBearerDTO);
				}
				return null;
			}
		});

		return serviceBearersMap;
	}

	public Collection<String> getAllAirportCodes() {

		JdbcTemplate templete = new JdbcTemplate(dataSource);
		airportCodes = new ArrayList<>();
		String sql = "select ap.airport_code from t_airport ap";

		templete.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					airportCodes.add(rs.getString("airport_code"));
				}
				return null;
			}
		});

		return airportCodes;
	}
	
	public Collection<String> getAllAvailableCityCodes() {

		JdbcTemplate templete = new JdbcTemplate(dataSource);
		cityCodes = new ArrayList<>();
		String sql = "SELECT c.city_code FROM t_airport A JOIN t_city c ON A.city_id = c.city_id "
				+ "WHERE A.status = 'ACT' AND A.online_status = 1 " + "GROUP BY c.city_code,c.city_name HAVING count(*)>1 ";

		templete.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					cityCodes.add(rs.getString("city_code"));
				}
				return null;
			}
		});

		return cityCodes;
	}

	public Integer getSequenceNextValue(String sequenceName) {

		JdbcTemplate templete = new JdbcTemplate(dataSource);
		nextVal = null;

		String sql = "SELECT " + sequenceName + ".NEXTVAL FROM dual";

		templete.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				rs.next();
				nextVal = rs.getInt("NEXTVAL");

				return null;
			}
		});

		return nextVal;
	}

	public void setQueryForPublishMechanism(String queryForPublishMechanism) {
		this.queryForPublishMechanism = queryForPublishMechanism;
	}

	/**
	 * @return Returns the queryStringForAirportMessages.
	 */
	public String getQueryStringForAirportMessages() {
		return queryStringForAirportMessages;
	}

	/**
	 * @param queryStringForAirportMessages
	 *            The queryStringForAirportMessages to set.
	 */
	public void setQueryStringForAirportMessages(String queryStringForAirportMessages) {
		this.queryStringForAirportMessages = queryStringForAirportMessages;
	}

	/**
	 * creates a error log
	 * 
	 * @param errorLog
	 * @return
	 */
	public int createErrorLog(ErrorLogTO errorLog) {
		JdbcTemplate templete = new JdbcTemplate(dataSource);
		Integer seq = getSequenceNextValue("S_ERR_LOG");
		int count = 0;
		Object[] values = new Object[] { seq, new Date(), errorLog.getUserId(), errorLog.getPnr(), errorLog.getModule(),
				errorLog.getSubject(), errorLog.getContent() };

		count = templete.update(sqlCreateErrorLog, values);

		return count;
	}

	/**
	 * @return the sqlCreateErrorLog
	 */
	public String getSqlCreateErrorLog() {
		return sqlCreateErrorLog;
	}

	/**
	 * @param sqlCreateErrorLog
	 *            the sqlCreateErrorLog to set
	 */
	public void setSqlCreateErrorLog(String sqlCreateErrorLog) {
		this.sqlCreateErrorLog = sqlCreateErrorLog;
	}

	/**
	 * @return Map<String,String> of SSR categories in the system keyed by SSR Category code
	 */
	public Map<Integer, SSRCategoryDTO> getSsrCategoryMap() {
		ssrCategoryMap = new HashMap<Integer, SSRCategoryDTO>();
		if (dataSource != null && queryForSSRCategory != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForSSRCategory, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					if (rs != null) {
						while (rs.next()) {
							SSRCategoryDTO ssrCategory = new SSRCategoryDTO();
							ssrCategory.setSSRCategoryId(new Integer(rs.getString("SSR_CAT_ID")));
							ssrCategory.setDescription(rs.getString("DESCRIPTION"));

							ssrCategory.setAddNotifyStartCutoverTime(rs.getInt("ADD_NOTIFY_START_CUTOVER_MINS"));
							ssrCategory.setAddNotifyStopCutoverTime(rs.getInt("ADD_NOTIFY_END_CUTOVER_MINS"));

							ssrCategory.setCancelNotifyStartCutoverTime(rs.getInt("CNX_NOTIFY_START_CUTOVER_MINS"));
							ssrCategory.setCancelNotifyStopCutoverTime(rs.getInt("CNX_NOTIFY_END_CUTOVER_MINS"));

							ssrCategory.setSSRBookingCutoverTime(rs.getInt("BOOKING_CUTOVER_IN_MINS"));

							ssrCategory.setToBeSendWithPNLADL("Y".equals(rs.getString("SEND_IN_PNL_ADL")));
							ssrCategory.setToBeSendWithMail("Y".equals(rs.getString("SEND_IN_EMAIL")));

							ssrCategory.setNotificationAttempts(rs.getInt("NOTFICATION_ATTEMPTS"));
							ssrCategory.setNotifyStartCutoverTime(rs.getInt("NOTIFY_START_CUTOVER_MINS"));
							ssrCategory.setNotifyStopCutoverTime(rs.getInt("NOTIFY_STOP_CUTOVER_MINS"));

							ssrCategoryMap.put(ssrCategory.getSSRCategoryId(), ssrCategory);
						}
					} else {
						throw new PlatformRuntimeException("platform.data.ssrcategory.notconfigured");
					}
					return ssrCategoryMap;
				}
			});
		}
		return ssrCategoryMap;
	}

	public String getQueryForSSRCategory() {
		return queryForSSRCategory;
	}

	public void setQueryForSSRCategory(String queryForSSRCategory) {
		this.queryForSSRCategory = queryForSSRCategory;
	}

	public Map<Integer, ReturnValidityPeriodTO> getReturnValidityPeriods() {
		returnValidityPeriods = new HashMap<Integer, ReturnValidityPeriodTO>();
		if (dataSource != null && queryForReturnValidityPeriod != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForReturnValidityPeriod, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					ReturnValidityPeriodTO rvp;

					if (rs != null) {
						while (rs.next()) {
							rvp = new ReturnValidityPeriodTO();
							rvp.setId(rs.getInt("RETURN_VALIDITY_ID"));
							rvp.setDescription(rs.getString("RETURN_VALIDITY_DESCRIPTION"));
							rvp.setSalesChannelCode(rs.getInt("SALES_CHANNEL_CODE"));
							rvp.setValidityPeriod(rs.getInt("VALIDITY_PERIOD"));
							rvp.setUnitOfMessure(rs.getString("UNIT_OF_MEASURE"));
							rvp.setStatus(rs.getString("STATUS"));

							returnValidityPeriods.put(rvp.getId(), rvp);
						}
					}

					return returnValidityPeriods;
				}
			});
		}

		return returnValidityPeriods;
	}

	/**
	 * @return the queryForReturnValidityPeriod
	 */
	public String getQueryForReturnValidityPeriod() {
		return queryForReturnValidityPeriod;
	}

	/**
	 * @param queryForReturnValidityPeriod
	 *            the queryForReturnValidityPeriod to set
	 */
	public void setQueryForReturnValidityPeriod(String queryForReturnValidityPeriod) {
		this.queryForReturnValidityPeriod = queryForReturnValidityPeriod;
	}

	public String getQueryForAllSSRInfo() {
		return this.queryForAllSSRInfo;
	}

	public void setQueryForAllSSRInfo(String queryForAllSSRInfo) {
		this.queryForAllSSRInfo = queryForAllSSRInfo;
	}

	public String getQueryForPetroFacAgents() {
		return queryForPetroFacAgents;
	}

	public void setQueryForPetroFacAgents(String queryForPetroFacAgents) {
		this.queryForPetroFacAgents = queryForPetroFacAgents;
	}

	public Map<Integer, SSRInfoDTO> getSSRInfoMap() {
		sSRInfoMap = new HashMap<Integer, SSRInfoDTO>();

		if (dataSource != null && queryForAllSSRInfo != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForAllSSRInfo, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							SSRInfoDTO ssr = new SSRInfoDTO();
							ssr.setSSRId(rs.getInt("SSR_ID"));
							ssr.setSSRCode(rs.getString("SSR_CODE"));
							ssr.setSSRName(rs.getString("SSR_NAME"));
							ssr.setSSRDescription(rs.getString("SSR_DESC"));
							ssr.setStatus(rs.getString("STATUS"));
							ssr.setSSRSubCategoryId(rs.getInt("SSR_SUB_CAT_ID"));
							ssr.setSSRCategoryId(rs.getInt("SSR_CAT_ID"));
							ssr.setShownInIBE(CommonsConstants.YES.equals(rs.getString("SHOW_IN_IBE")));
							ssr.setShownInXBE(CommonsConstants.YES.equals(rs.getString("SHOW_IN_XBE")));
							ssr.setShownInWs(CommonsConstants.YES.equals(rs.getString("SHOW_IN_WS")));
							ssr.setShownInGDS(CommonsConstants.YES.equals(rs.getString("SHOW_IN_GDS")));

							sSRInfoMap.put(ssr.getSSRId(), ssr);
						}
					}

					return sSRInfoMap;
				}
			});
		}

		return sSRInfoMap;
	}

	public String getQuerySSRApplicableAirports() {
		return this.querySSRApplicableAirports;
	}

	public void setQuerySSRApplicableAirports(String querySSRApplicableAirports) {
		this.querySSRApplicableAirports = querySSRApplicableAirports;
	}

	public Map<String, Boolean> getSSRApplicableAirportsMap() {
		sSRApplicableAirportsMap = new HashMap<String, Boolean>();

		if (dataSource != null && querySSRApplicableAirports != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(querySSRApplicableAirports, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							sSRApplicableAirportsMap.put(rs.getString("SSR_ID") + rs.getString("AIRPORT_CODE"), new Boolean(true));
						}
					}

					return sSRApplicableAirportsMap;
				}
			});
		}

		return sSRApplicableAirportsMap;
	}

	public String getQuerySSRCategoryEMail() {
		return this.querySSRCategoryEMail;
	}

	public void setQuerySSRCategoryEMail(String querySSRCategoryEMail) {
		this.querySSRCategoryEMail = querySSRCategoryEMail;
	}

	public Map<Integer, String> getSSRCategoryEMailMap() {
		sSRCategoryEMailMap = new HashMap<Integer, String>();

		if (dataSource != null && querySSRCategoryEMail != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(querySSRCategoryEMail, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							sSRCategoryEMailMap.put(rs.getInt("ssrcatid"), rs.getString("emailaddress"));
						}
					}

					return sSRCategoryEMailMap;
				}
			});
		}

		return sSRCategoryEMailMap;
	}

	/**
	 * Retrieves petrofac enabled agents Map (AARESAA:3128 - Lalanthi)
	 * 
	 * @return
	 */
	public HashMap<String, String> getPetroFacEnabledAgentsMap() {
		petroFacAgentsMap = new HashMap<String, String>();
		if (dataSource != null && queryForPetroFacAgents != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryForPetroFacAgents, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							petroFacAgentsMap.put(rs.getString("AGENT_CODE"), rs.getString("CAPTURE_PAY_REF"));
						}
					}
					return petroFacAgentsMap;
				}
			});
		}
		return petroFacAgentsMap;
	}

	/***
	 * Returns Charge group code and description Map
	 * 
	 * @return
	 */
	public Map<String, String> getChargeGroupCodeNDescMap() {
		chargeGroupCodeNDescriptionMap = new HashMap<String, String>();
		if (dataSource != null && sqlChargeGroupCodeNDescription != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(sqlChargeGroupCodeNDescription, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							chargeGroupCodeNDescriptionMap.put(rs.getString("CHARGE_GROUP_CODE"),
									rs.getString("CHARGE_GROUP_DESC"));
						}
					}
					return chargeGroupCodeNDescriptionMap;
				}
			});
		}
		return chargeGroupCodeNDescriptionMap;
	}

	/**
	 * Retruns a map of actual payment methods the system allows
	 * 
	 * @return
	 */
	@SuppressWarnings("all")
	public HashMap<Integer, String> getActualPaymentMethods() {
		actualPayModes = new HashMap<Integer, String>();
		if (dataSource != null && queryStringForActualPayModes != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForActualPayModes, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							actualPayModes.put(rs.getInt("PAYMENT_MODE_ID"), rs.getString("PAYMENT_MODE"));
						}
					}
					return actualPayModes;
				}
			});
		}
		return actualPayModes;
	}

	public Map<String, String> getCodeShareBcCosMap() {
		final Map<String, String> codeShareBcCosMap = new HashMap<String, String>();
		if (dataSource != null && sqlCodeShareBcCosMapping != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(sqlCodeShareBcCosMapping, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							codeShareBcCosMap.put(rs.getString("BOOKING_CODE"), rs.getString("CABIN_CLASS_CODE"));
						}
					}
					return codeShareBcCosMap;
				}
			});
		}
		return codeShareBcCosMap;
	}

	/**
	 * @return the queryActiveCurrencyCodes
	 */
	public String getQueryActiveCurrencyCodes() {
		return queryActiveCurrencyCodes;
	}

	/**
	 * @param queryActiveCurrencyCodes
	 *            the queryActiveCurrencyCodes to set
	 */
	public void setQueryActiveCurrencyCodes(String queryActiveCurrencyCodes) {
		this.queryActiveCurrencyCodes = queryActiveCurrencyCodes;
	}

	/**
	 * IMPORTANT : DO NOT MODIFY WITHOUT CHECKING USAGE
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map[] getParentStationDetailsMap() {
		mapParentStationByGroundStation = new HashMap<String, List<String[]>>();
		mapTopLevelStationByConnectedStation = new HashMap<String, String>();
		connectingAirportDetail = new HashMap<String, String>();
		if (dataSource != null && queryStringGroundStationParentDetails != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringGroundStationParentDetails, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							String airportCode = rs.getString("airport_code");
							String airportDesc = rs.getString("airport_name");
							String groundStationCode = rs.getString("connecting_airport");
							List<String[]> existing = mapParentStationByGroundStation.get(groundStationCode);
							if (existing == null) {
								existing = new ArrayList<String[]>();
								mapParentStationByGroundStation.put(groundStationCode, existing);
							}
							existing.add(new String[] { airportCode, airportDesc });
							mapTopLevelStationByConnectedStation.put(airportCode, groundStationCode);
							connectingAirportDetail.put(airportCode, airportDesc);
						}
					}
					return new Map[] { mapParentStationByGroundStation, mapTopLevelStationByConnectedStation,
							connectingAirportDetail };
				}
			});
		}
		return new Map[] { mapParentStationByGroundStation, mapTopLevelStationByConnectedStation, connectingAirportDetail };
	}

	/**
	 * IMPORTANT : DO NOT MODIFY WITHOUT CHECKING USAGE
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map[] getGroundStationDetailsMap() {
		mapGroundStationNames = new HashMap<String, String>();
		mapPrimaryStationCode = new HashMap<String, Collection<String>>();
		mapPrimaryStationBySubStation = new HashMap<String, String>();
		if (dataSource != null && queryStringGroundStationDetails != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringGroundStationDetails, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Collection<String> colGroundStationList;
					if (rs != null) {
						while (rs.next()) {
							mapGroundStationNames.put(rs.getString("SUB_STATION_SHORTNAME"), rs.getString("SUB_STATION_DESC"));
							if (!mapPrimaryStationCode.containsKey(rs.getString("AIRPORT_CODE"))) {
								colGroundStationList = new ArrayList<String>();
								colGroundStationList.add(rs.getString("SUB_STATION_SHORTNAME"));
								mapPrimaryStationCode.put(rs.getString("AIRPORT_CODE"), colGroundStationList);
							} else {
								mapPrimaryStationCode.get(rs.getString("AIRPORT_CODE"))
										.add(rs.getString("SUB_STATION_SHORTNAME"));
							}
							mapPrimaryStationBySubStation.put(rs.getString("SUB_STATION_SHORTNAME"), rs.getString("AIRPORT_CODE"));
						}
					}
					return new Map[] { mapGroundStationNames, mapPrimaryStationCode, mapPrimaryStationBySubStation };
				}
			});
		}
		return new Map[] { mapGroundStationNames, mapPrimaryStationCode, mapPrimaryStationBySubStation };
	}

	/**
	 * IMPORTANT : DO NOT MODIFY WITHOUT CHECKING USAGE
	 * 
	 * @return
	 */
	public Map<String, String[]> getAllAirportCodeData() {
		mapAllAirportCodes = new HashMap<String, String[]>();

		if (dataSource != null && queryStringAirportDetails != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringAirportDetails, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							String airportCode = rs.getString("airport_code");
							String airportDesc = rs.getString("airport_name");
							mapAllAirportCodes.put(airportCode, new String[] { airportCode, airportDesc });
						}
					}
					return mapAllAirportCodes;
				}
			});
		}
		return mapAllAirportCodes;
	}

	public Map<String, String> getAgentsByAgentCode() {
		agentByCodes = new HashMap<String, String>();
		if (dataSource != null && queryStringForAgents != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForAgents, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					while (rs.next()) {
						String agentCode = rs.getString("agent_code");
						String agentName = rs.getString("agent_name");
						agentByCodes.put(agentCode, agentName);
					}
					return agentByCodes;
				}
			});
		}

		return agentByCodes;

	}

	@SuppressWarnings("unchecked")
	public Map<String, Map<String, List<PaxCountryConfigDTO>>> getPaxCountryWiseConfigurations() {
		Map<String, Map<String, List<PaxCountryConfigDTO>>> paxCountryConfigs = new HashMap<String, Map<String, List<PaxCountryConfigDTO>>>();

		if (dataSource != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			Map<String, Map<String, List<PaxCountryConfigDTO>>> ret = (Map<String, Map<String, List<PaxCountryConfigDTO>>>) templete
					.query(queryStringPaxCountryConfigs, new ResultSetExtractor() {
						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							Map<String, Map<String, List<PaxCountryConfigDTO>>> ret = new HashMap<String, Map<String, List<PaxCountryConfigDTO>>>();

							Map<String, List<PaxCountryConfigDTO>> paxXBECountryConfig = new HashMap<String, List<PaxCountryConfigDTO>>();
							Map<String, List<PaxCountryConfigDTO>> paxIBECountryConfig = new HashMap<String, List<PaxCountryConfigDTO>>();
							Map<String, List<PaxCountryConfigDTO>> paxWSCountryConfig = new HashMap<String, List<PaxCountryConfigDTO>>();
							Map<String, List<PaxCountryConfigDTO>> paxGDSCountryConfig = new HashMap<String, List<PaxCountryConfigDTO>>();

							if (rs != null) {
								while (rs.next()) {
									PaxCountryConfigDTO paxCountryConfigDTO = new PaxCountryConfigDTO();

									String countryCode = PlatformUtiltiies.nullHandler(rs.getString("COUNTRY_CODE"));

									paxCountryConfigDTO.setCountryCode(countryCode);
									paxCountryConfigDTO.setFieldName(PlatformUtiltiies.nullHandler(rs.getString("FIELD_NAME")));
									paxCountryConfigDTO.setPaxCatCode(PlatformUtiltiies.nullHandler(rs
											.getString("PAX_CATEGORY_CODE")));
									paxCountryConfigDTO.setPaxTypeCode(PlatformUtiltiies.nullHandler(rs
											.getString("PAX_TYPE_CODE")));
									paxCountryConfigDTO.setAutoCheckinVisibility((PlatformUtiltiies.nullHandler(
											rs.getString("AUTOMATIC_CHECKIN_VISIBILITY")).equals("Y") ? true : false));
									paxCountryConfigDTO.setAutoCheckinMandatory((PlatformUtiltiies.nullHandler(
											rs.getString("AUTOMATIC_CHECKIN_MANDATORY")).equals("Y") ? true : false));

									PaxCountryConfigDTO ibePaxCountryConfigDTO = (PaxCountryConfigDTO) paxCountryConfigDTO
											.clone();
									PaxCountryConfigDTO wsPaxCountryConfigDTO = (PaxCountryConfigDTO) paxCountryConfigDTO.clone();
									PaxCountryConfigDTO gdsPaxCountryConfigDTO = (PaxCountryConfigDTO) paxCountryConfigDTO
											.clone();
									paxCountryConfigDTO.setMandatory(PlatformUtiltiies.nullHandler(rs.getString("XBE_MANDATORY"))
											.equals("Y") ? true : false);
									ibePaxCountryConfigDTO.setMandatory(PlatformUtiltiies.nullHandler(
											rs.getString("IBE_MANDATORY")).equals("Y") ? true : false);
									wsPaxCountryConfigDTO.setMandatory(PlatformUtiltiies
											.nullHandler(rs.getString("WS_MANDATORY")).equals("Y") ? true : false);
									gdsPaxCountryConfigDTO.setMandatory(PlatformUtiltiies.nullHandler(
											rs.getString("GDS_MANDATORY")).equals("Y") ? true : false);

									if (paxXBECountryConfig.get(countryCode) == null) {
										paxXBECountryConfig.put(countryCode, new ArrayList<PaxCountryConfigDTO>());
									}
									if (paxIBECountryConfig.get(countryCode) == null) {
										paxIBECountryConfig.put(countryCode, new ArrayList<PaxCountryConfigDTO>());
									}
									if (paxWSCountryConfig.get(countryCode) == null) {
										paxWSCountryConfig.put(countryCode, new ArrayList<PaxCountryConfigDTO>());
									}
									if (paxGDSCountryConfig.get(countryCode) == null) {
										paxGDSCountryConfig.put(countryCode, new ArrayList<PaxCountryConfigDTO>());
									}
									paxXBECountryConfig.get(countryCode).add(paxCountryConfigDTO);
									paxIBECountryConfig.get(countryCode).add(ibePaxCountryConfigDTO);
									paxWSCountryConfig.get(countryCode).add(wsPaxCountryConfigDTO);
									paxGDSCountryConfig.get(countryCode).add(gdsPaxCountryConfigDTO);
								}
							}

							ret.put(AppIndicatorEnum.APP_XBE.toString(), paxXBECountryConfig);
							ret.put(AppIndicatorEnum.APP_IBE.toString(), paxIBECountryConfig);
							ret.put(AppIndicatorEnum.APP_ANDROID.toString(), paxIBECountryConfig);
							ret.put(AppIndicatorEnum.APP_IOS.toString(), paxIBECountryConfig);
							ret.put(AppIndicatorEnum.APP_WS.toString(), paxWSCountryConfig);
							ret.put(AppIndicatorEnum.APP_GDS.toString(), paxGDSCountryConfig);

							return ret;
						}
					});
			paxCountryConfigs.putAll(ret);
		}

		return paxCountryConfigs;
	}
	
	public Map<String, String> getCountryServiceTaxReportCategory() {
		final Map<String, String> countryServiceTaxReportCategoryMap = new HashMap<String, String>();
		if (dataSource != null && queryStringForServiceTaxReportingCategory != null) {

			JdbcTemplate templete = new JdbcTemplate(dataSource);
			templete.query(queryStringForServiceTaxReportingCategory, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						while (rs.next()) {
							countryServiceTaxReportCategoryMap.put(rs.getString("COUNTRY_CODE"), rs.getString("SERVICE_TAX_DESCRIPTION"));
						}
					}
					return countryServiceTaxReportCategoryMap;
				}
			});
		}
		return countryServiceTaxReportCategoryMap;
	}


	public String getQueryStringGroundStationDetails() {
		return queryStringGroundStationDetails;
	}

	public void setQueryStringGroundStationDetails(String queryStringGroundStationDetails) {
		this.queryStringGroundStationDetails = queryStringGroundStationDetails;
	}

	public Map<String, String> getMapPrimaryStationBySubStation() {
		return mapPrimaryStationBySubStation;
	}

	public void setMapPrimaryStationBySubStation(Map<String, String> mapPrimaryStationBySubStation) {
		this.mapPrimaryStationBySubStation = mapPrimaryStationBySubStation;
	}

	public String getQueryStringGroundStationParentDetails() {
		return queryStringGroundStationParentDetails;
	}

	public void setQueryStringGroundStationParentDetails(String queryStringGroundStationParentDetails) {
		this.queryStringGroundStationParentDetails = queryStringGroundStationParentDetails;
	}

	public Map<String, String[]> getMapAllAirportCodes() {
		return mapAllAirportCodes;
	}

	public void setMapAllAirportCodes(Map<String, String[]> mapAllAirportCodes) {
		this.mapAllAirportCodes = mapAllAirportCodes;
	}

	public String getQueryStringAirportDetails() {
		return queryStringAirportDetails;
	}

	public void setGdsbookingClassMap(HashMap<Integer, Map<String, String>> gdsBookingClassMap) {
		this.gdsBookingClassMap = gdsBookingClassMap;
	}

	public void setQueryStringAirportDetails(String queryStringAirportDetails) {
		this.queryStringAirportDetails = queryStringAirportDetails;
	}

	public Map<String, String> getMapTopLevelStationByConnectedStation() {
		return mapTopLevelStationByConnectedStation;
	}

	public void setMapTopLevelStationByConnectedStation(Map<String, String> mapTopLevelStationByConnectedStation) {
		this.mapTopLevelStationByConnectedStation = mapTopLevelStationByConnectedStation;
	}

	public Map<String, String> getConnectingAirportDetail() {
		return connectingAirportDetail;
	}

	/**
	 * @param connectingAirportDetail
	 *            the connectingAirportDetail to set
	 */
	public void setConnectingAirportDetail(Map<String, String> connectingAirportDetail) {
		this.connectingAirportDetail = connectingAirportDetail;
	}

	public void setBookingClassMappingMap(HashMap<BookingClassCharMappingTO, String> bookingClassMappingMap) {
		this.bookingClassCharMappingMap = bookingClassMappingMap;
	}

	public String getQueryStringForXBEPaxConfig() {
		return queryStringForXBEPaxConfig;
	}

	public void setQueryStringForXBEPaxConfig(String queryStringForXBEPaxConfig) {
		this.queryStringForXBEPaxConfig = queryStringForXBEPaxConfig;
	}

	public String getQueryStringForIBEPaxConfig() {
		return queryStringForIBEPaxConfig;
	}

	public void setQueryStringForIBEPaxConfig(String queryStringForIBEPaxConfig) {
		this.queryStringForIBEPaxConfig = queryStringForIBEPaxConfig;
	}

	public String getQueryStringForWSPaxConfig() {
		return queryStringForWSPaxConfig;
	}

	public void setQueryStringForWSPaxConfig(String queryStringForWSPaxConfig) {
		this.queryStringForWSPaxConfig = queryStringForWSPaxConfig;
	}

	public String getQueryStringBusConnectingAirports() {
		return queryStringBusConnectingAirports;
	}

	public void setQueryStringBusConnectingAirports(String queryStringBusConnectingAirports) {
		this.queryStringBusConnectingAirports = queryStringBusConnectingAirports;
	}

	public String getQueryStringDefaultLogicalCCDetails() {
		return queryStringDefaultLogicalCCDetails;
	}

	public void setQueryStringDefaultLogicalCCDetails(String queryStringDefaultLogicalCCDetails) {
		this.queryStringDefaultLogicalCCDetails = queryStringDefaultLogicalCCDetails;
	}

	public String getQueryStringForLogicalCabinClasses() {
		return queryStringForLogicalCabinClasses;
	}

	public void setQueryStringForLogicalCabinClasses(String queryStringForLogicalCabinClasses) {
		this.queryStringForLogicalCabinClasses = queryStringForLogicalCabinClasses;
	}

	public void setBusConnectingAirports(Set<String> busConnectingAirports) {
		this.busConnectingAirports = busConnectingAirports;
	}

	public String getQueryForOwnAirlineCarriers() {
		return queryForOwnAirlineCarriers;
	}

	public void setQueryForOwnAirlineCarriers(String queryForOwnAirlineCarriers) {
		this.queryForOwnAirlineCarriers = queryForOwnAirlineCarriers;
	}

	public String getQueryStringAirportMessages() {
		return queryStringAirportMessages;
	}

	public void setQueryStringAirportMessages(String queryStringAirportMessages) {
		this.queryStringAirportMessages = queryStringAirportMessages;
	}

	public String getQueryStringForChargeAdjustmentTypes() {
		return queryStringForChargeAdjustmentTypes;
	}

	public void setQueryStringForChargeAdjustmentTypes(String queryStringForChargeAdjustmentTypes) {
		this.queryStringForChargeAdjustmentTypes = queryStringForChargeAdjustmentTypes;
	}

	public String getQueryStringPaxCountryConfigs() {
		return queryStringPaxCountryConfigs;
	}

	public void setQueryStringPaxCountryConfigs(String queryStringPaxCountryConfigs) {
		this.queryStringPaxCountryConfigs = queryStringPaxCountryConfigs;
	}

	public String getQueryForCarrierAirlineMap() {
		return queryForCarrierAirlineMap;
	}

	public void setQueryForCarrierAirlineMap(String queryForCarrierAirlineMap) {
		this.queryForCarrierAirlineMap = queryForCarrierAirlineMap;
	}

	public String getQueryStringForLogicalCabinClassesTranslations() {
		return queryStringForLogicalCabinClassesTranslations;
	}

	public void setQueryStringForLogicalCabinClassesTranslations(String queryStringForLogicalCabinClassesTranslations) {
		this.queryStringForLogicalCabinClassesTranslations = queryStringForLogicalCabinClassesTranslations;
	}

	public String getQueryStringForAgents() {
		return queryStringForAgents;
	}

	public void setQueryStringForAgents(String queryStringForAgents) {
		this.queryStringForAgents = queryStringForAgents;
	}

	public void setCsBookingClassMap(HashMap<Integer, Map<String, String>> csBookingClassMap) {
		this.csBookingClassMap = csBookingClassMap;
	}

	public String getQueryStringForCSBookingClassMapping() {
		return queryStringForCSBookingClassMapping;
	}

	public void setQueryStringForCSBookingClassMapping(String queryStringForCSBookingClassMapping) {
		this.queryStringForCSBookingClassMapping = queryStringForCSBookingClassMapping;
	}

	public String getQueryStringForGDSBookingClassMapping() {
		return queryStringForGDSBookingClassMapping;
	}

	public void setQueryStringForGDSBookingClassMapping(String queryStringForGDSBookingClassMapping) {
		this.queryStringForGDSBookingClassMapping = queryStringForGDSBookingClassMapping;
	}

	public String getQueryStringForSingleEntryMappedGdsBCs() {
		return queryStringForSingleEntryMappedGdsBCs;
	}

	public void setQueryStringForSingleEntryMappedGdsBCs(String queryStringForSingleEntryMappedGdsBCs) {
		this.queryStringForSingleEntryMappedGdsBCs = queryStringForSingleEntryMappedGdsBCs;
	}

	public String getQueryStringForBookingClassesByLogicalCC() {
		return queryStringForBookingClassesByLogicalCC;
	}

	public void setQueryStringForBookingClassesByLogicalCC(String queryStringForBookingClassesByLogicalCC) {
		this.queryStringForBookingClassesByLogicalCC = queryStringForBookingClassesByLogicalCC;
	}

	public String getQueryStringForGDSPaxConfig() {
		return queryStringForGDSPaxConfig;
	}

	public void setQueryStringForGDSPaxConfig(String queryStringForGDSPaxConfig) {
		this.queryStringForGDSPaxConfig = queryStringForGDSPaxConfig;
	}

	public String getQueryStringForServiceTypeByOperationType() {
		return queryStringForServiceTypeByOperationType;
	}

	public void setQueryStringForServiceTypeByOperationType(String queryStringForServiceTypeByOperationType) {
		this.queryStringForServiceTypeByOperationType = queryStringForServiceTypeByOperationType;
	}

	public String getSqlCodeShareBcCosMapping() {
		return sqlCodeShareBcCosMapping;
	}

	public void setSqlCodeShareBcCosMapping(String sqlCodeShareBcCosMapping) {
		this.sqlCodeShareBcCosMapping = sqlCodeShareBcCosMapping;
	}

	public String getQueryForHideChargeCurrencies() {
		return queryForHideChargeCurrencies;
	}

	public void setQueryForHideChargeCurrencies(String queryForHideChargeCurrencies) {
		this.queryForHideChargeCurrencies = queryForHideChargeCurrencies;
	}

	public String getQueryForOwnAirCarriers() {
		return queryForOwnAirCarriers;
	}

	public void setQueryForOwnAirCarriers(String queryForOwnAirCarriers) {
		this.queryForOwnAirCarriers = queryForOwnAirCarriers;
	}

	public String getQueryStringForServiceTaxReportingCategory() {
		return queryStringForServiceTaxReportingCategory;
	}

	public void setQueryStringForServiceTaxReportingCategory(String queryStringForServiceTaxReportingCategory) {
		this.queryStringForServiceTaxReportingCategory = queryStringForServiceTaxReportingCategory;
	}

	public String getQueryStringForAdminFeeAgent() {
		return queryStringForAdminFeeAgent;
	}

	public void setQueryStringForAdminFeeAgent(String queryStringForAdminFeeAgent) {
		this.queryStringForAdminFeeAgent = queryStringForAdminFeeAgent;
	}
	
}
