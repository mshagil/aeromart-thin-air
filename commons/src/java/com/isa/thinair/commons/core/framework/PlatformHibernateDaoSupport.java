package com.isa.thinair.commons.core.framework;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;


@Repository
public abstract class PlatformHibernateDaoSupport {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory(){
		return this.sessionFactory;
	}
	
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	public void hibernateSaveOrUpdateAll(Collection<?> objects) {
		
		Session currentSession = getSession();
		for (Object object : objects) {
			currentSession.saveOrUpdate(object);
		}
	}
	
	public void delete(Object object) {
		getSession().delete(object);
	}
	
	public void update(Object object) {
		getSession().update(object);
	}
	
	public void deleteAll(Collection<?> objects){
		Session currentSession = getSession();
		for (Object object : objects) {
			currentSession.delete(object);
		}
	}
	
	
	public Serializable hibernateSave(Object object){
		return getSession().save(object);
	}
	
	public Serializable saveEntity(String entityName, Object object){
		return getSession().save(entityName, object);
	}
	
	
	public void hibernateSaveOrUpdate(Object object){
		getSession().saveOrUpdate(object);
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> find(String hql, Class<T> clazz){	
		Query query = getSession().createQuery(hql);		
		return (List<T>)query.list();
	}
	
	public <T> List<T> find(String hql, Object value, Class<T> clazz) {	
		return find(hql, new Object[] {value}, clazz);
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> List<T> find(String hql, Object[] values, Class<T> clazz) {
		Query query = getSession().createQuery(hql);

		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				query.setParameter(i, values[i]);
			}
		}
		return (List<T>) query.list();

	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> clazz, Serializable id ){
		 return (T)getSession().get(clazz, id);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T load(Class<T> clazz, Serializable id){
		 return (T)getSession().load(clazz, id);
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> loadAll(Class<T> clazz){
		Criteria criteria = getSession().createCriteria(clazz);
		return (List<T>)criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> findByNamedQuery(String queryName, Object[] values, Class<T> clazz) {
		Query query = getSession().getNamedQuery(queryName);

		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				query.setParameter(i, values[i]);
			}
		}
		return (List<T>) query.list();

	}
	
	public Object merge(Object object){
		return getSession().merge(object);
	}
	
	public void lock(Object object, LockMode lockMode){
		getSession().lock(object, lockMode);
	}
	
	public void evict(Object object){
		getSession().evict(object);
	}
	
	public void flush(){
		getSession().flush();
	}
	

}
