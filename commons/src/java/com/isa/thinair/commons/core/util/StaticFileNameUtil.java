package com.isa.thinair.commons.core.util;

/**
 * This is used exclusively for changing the static file's names to compatible with the _ISA_STATIC_FILE append which is
 * added to solve the proxy caching of static files
 * 
 * Each static file is appended with the '_no_cache' and at the build time this part will get replace with some other
 * constant so the actual file name will change. The actual file will also get renamed to make the filename reference
 * correct
 * 
 * Each time you include a file name ref of following filetype in the java call this utill should be used to get the
 * actual file name after rename is happened.
 * 
 * JPG/GIF/PNG/CSS
 * 
 * @author mekanayake
 * 
 */
public class StaticFileNameUtil {

	private static final char EXTENSION_SEPARATOR = '.';

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	/**
	 * Converting [path][filename].[extension] ==> [path][filename_no_cache].[extension] Must used with all the
	 * JPG/GIF/PNG/CSS ref in the java code
	 * 
	 * @param staticFileName
	 * @return
	 */
	public static String getCorrected(String staticFileName) {
		if (staticFileName != null) {
			int dot = staticFileName.lastIndexOf(EXTENSION_SEPARATOR);
			String filename = staticFileName.substring(0, dot);
			String extension = staticFileName.substring(dot + 1);

			StringBuilder sb = new StringBuilder();
			sb.append(filename).append(globalConfig.getStaticFileSuffix()).append(".").append(extension);
			return sb.toString();
		} else {
			return staticFileName;
		}
	}

}
