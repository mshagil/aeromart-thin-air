/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Feb 25, 2005
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

/**
 * Commons Framework: Reusable components and templates
 * 
 * @author sudheera
 * 
 *         ServerMessage is a property factory to get server side messages
 * 
 */

public class ServerMessage {
	// property file system property name
	private static final String SYS_PROP_NAME = "server.message.properties";

	// default property file if the system property not set
	private static String resouceBundleName = "server-message";

	private static final PropertyFactory pf = getFactory();

	/**
	 * gets the property
	 * 
	 * @param key
	 * @return String the property
	 */
	public static String getProperty(String key) {
		return pf.getProperty(key);
	}

	/**
	 * gets the property factory
	 * 
	 * @return
	 */
	private static final PropertyFactory getFactory() {
		String resouceBundleName = System.getProperty(SYS_PROP_NAME);

		if (resouceBundleName == null || resouceBundleName.equals("")) {
			resouceBundleName = ServerMessage.resouceBundleName;
		}

		return new PropertyFactory(resouceBundleName);
	}

}