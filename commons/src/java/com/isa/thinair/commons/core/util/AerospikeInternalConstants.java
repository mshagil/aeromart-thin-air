package com.isa.thinair.commons.core.util;

public interface AerospikeInternalConstants {

	public static final String UDF_FOLDER = "udf";

	public interface UDFIpToCountry {
		public static final String SET_NAME = "ip_to_country";
		public static final String CALLING_METHOD = "filter_country_by_id";
	}

	public interface SET_IP_To_COUNTRY {
		public static final String BIN_START_RANGE = "start_range";
		public static final String BIN_END_RANGE = "end_range";
		public static final String BIN_COUNTRY_CODE = "country_code";
	}
	
	public interface UDFAppParameter {
		public static final String SET_NAME = "app_parameter";
		public static final String CALLING_METHOD = "get_param_value";
	}
	
	public interface SET_APP_PARAMETER {
		public static final String BIN_PARAM_VALUE = "param_value";
		public static final String BIN_CARRIER_CODE = "carrier_code";
	}
	
	public interface OndForCash {
		public static final String SET_NAME = "ond_4_cache";
		public static final String CALLING_METHOD = "get_ond";
	}

	public interface SET_OndCashing {
		
		public static final String OPERATING_CARRIER = "operating_Cr";
	}
}
