package com.isa.thinair.commons.core.persistence.dao;

import java.util.List;

import com.isa.thinair.commons.api.model.IpToCountry;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface CommonsHibernateDAO {
	
	public List<String> getActiveDomainNames();
	
	public void saveOrUpdateEntity(String entityName, Object object);
	
	public List<IpToCountry> getUnsynedIPToCountryData() throws ModuleException;

	public void saveOrUpdateAllIPToCountries(List<IpToCountry> ipToCountries);

}
