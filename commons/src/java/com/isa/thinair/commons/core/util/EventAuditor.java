package com.isa.thinair.commons.core.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * Composes
 * 
 * 1. Elastic Search Friendlier logs
 * 
 * 2. General Logs
 * 
 *
 */
public class EventAuditor {

	private static final String EVENT_LOG = "event-log";

	private static final Log log = LogFactory.getLog(EVENT_LOG);
	private static final Log logger = LogFactory.getLog(EventAuditor.class);

	public static void audit(String operation, Exception ex) {
		StringBuilder message = new StringBuilder();

		message.append(AuditParameter.OPERATION.getCode());
		message.append(operation);
		message.append(Delimeters.END_AND_CONTINUE.getCode());

		message.append(AuditParameter.ERROR.getCode());
		message.append(ex.getMessage());
		message.append(Delimeters.END_AND_CONTINUE.getCode());

		message.append(AuditParameter.STACK.getCode());
		message.append(getStackTrace(ex, 1));
		message.append(Delimeters.END.getCode());

		// Elastic Search friendly logger
		log.error(message.toString());

		// Detail Logger
		logger.error(operation, ex);

	}

	private static String getStackTrace(Exception ex, int lines) {
		StackTraceElement[] stackTrace = ex.getStackTrace();
		StringBuffer stackBuf = new StringBuffer();
		if (stackTrace != null) {
			for (int i = 0; i < lines; i++) {
				stackBuf.append(stackTrace[i].toString()).append(Delimeters.CONTINUE.getCode());
			}
		}
		return stackBuf.toString();
	}

	public enum AuditParameter {
		OPERATION("OPERATION=<"), ERROR("ERROR=<"), STACK("STACK=<");
		private String code;

		private AuditParameter(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	public enum Delimeters {

		END_AND_CONTINUE(">,"), END(">"), CONTINUE(":");

		private String code;

		private Delimeters(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

}
