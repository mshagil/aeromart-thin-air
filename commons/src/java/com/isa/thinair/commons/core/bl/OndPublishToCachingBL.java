package com.isa.thinair.commons.core.bl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.ondpublish.OndPublishDestinationDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishOriginDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishToCachingDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishedTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;

/*
 * rajithaB : prepare for caching..
 */
public class OndPublishToCachingBL {

	private static Log log = LogFactory.getLog(OndPublishToCachingBL.class);

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void prepareListForCaching(OndPublishedTO ondsPublished) {

		if (ondsPublished == null) {
			log.error("Empty ond list");
			return;
		}

		List<OndPublishToCachingDTO> ondListToCache = new ArrayList<OndPublishToCachingDTO>();

		List<OndPublishOriginDTO> origins = ondsPublished.getOrigins();

		if (origins != null && !origins.isEmpty()) {
			for (OndPublishOriginDTO origin : origins) {

				List<OndPublishDestinationDTO> ondDestinations = new ArrayList<OndPublishDestinationDTO>();
				String originAirport = origin.getOndLocation().getOndLocationCode();

				ondDestinations = origin.getDestinations();

				for (OndPublishDestinationDTO destination : ondDestinations) {

					OndPublishToCachingDTO ondPublishedToCache = new OndPublishToCachingDTO();

					String destinationAirport = destination.getOndLocation().getOndLocationCode();
					String originDestination = originAirport.concat(destinationAirport);

					ondPublishedToCache.setOriginAndDestination(originDestination);
					ondPublishedToCache.setOperatingCarriers(destination.getOperatingCarriers());

					ondListToCache.add(ondPublishedToCache);
				}

			}

			try {
				CommonsServices.getAerospikeCachingService().syncOndPublishToCache(ondListToCache);
			} catch (ModuleException e) {
				log.error("Error when saving onds ", e);
			}
		} else {
			log.warn("Not origin/destination combinations found!!! to cache");
		}
	}

}
