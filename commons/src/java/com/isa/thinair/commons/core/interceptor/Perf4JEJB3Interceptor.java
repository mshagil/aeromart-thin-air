package com.isa.thinair.commons.core.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;

import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Nilindra Fernando
 */
public class Perf4JEJB3Interceptor {

	private static final boolean isPer4JEnable = SystemPropertyUtil.isPerf4JEnable();

	@AroundInvoke
	public Object logCall(InvocationContext context) throws Exception {

		if (isPer4JEnable) {
			String className = Perf4JBaseUtils.getClassName(context.getTarget());
			String methodName = PlatformUtiltiies.nullHandler(context.getMethod().getName());
			StopWatch stopWatch = new Log4JStopWatch("EJB-" + className + "-" + methodName);
			Object object = context.proceed();
			stopWatch.stop();
			return object;
		} else {
			return context.proceed();
		}
	}
}
