package com.isa.thinair.commons.core.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.GoogleRecaptcha;
import com.isa.thinair.commons.api.constants.CommonsConstants.HumanVerificationConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.constants.CommonsConstants.LoyaltyPointsConversionParams;
import com.isa.thinair.commons.api.dto.AdminFeeAgentTO;
import com.isa.thinair.commons.api.dto.CommonTemplatingDTO;
import com.isa.thinair.commons.api.dto.FlightTypeDTO;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.BSP_FTP;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.constants.SystemParamKeys.PENALTY_BASED;
import com.isa.thinair.commons.core.constants.SystemParamKeys.RQ_AUTO_FQ;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class AppSysParamsUtil {

	private static final String DEFAULT_DATA_SEPARATOR = ",";

	private static final String DEFAULT_VALUE_SEPARATOR = "=";

	private static final String SEMI_COLON_SEPARATOR = ";";

	private static final String EMPTY_STRING = "";

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	private static Map<String, String> supportedLanguagesMap = new HashMap<String, String>();

	private static Map<String, String> xbeSupportedLanguagesMap = new HashMap<String, String>();

	private static Map<String, String> ibeUrlMap = new HashMap<String, String>();

	private static final String YES = "Y";
	
	private static final String SPECIFIC = "S";

	private static Map<String, String> paymentServiceChargesMap = null;

	private static List<FlightTypeDTO> lstFlightTypeDTO = null;

	private static List<String> promoListByPriority = null;

	private static Set<String> paxStatusForAutoFlown = null;

	private static List<String> paymentSettlementPriority = null;

	static {
		setStaticData();
	}

	private static void setStaticData() {
		setIBESupportedLanguages();
		setIBEUrls();
		setXBESupportedLanguages();
	}

	/****************************************************************************************
	 * UTILITY METHODS *
	 ****************************************************************************************/
	/**
	 * Return the key value pair map for the given text. If text is empty or null, empty map will be returned
	 * 
	 * @param text
	 * @param dataEntrySeparator
	 * @param keyValueSeparator
	 * @return
	 */
	private static Map<String, String> getKeyValuePair(String text, String dataEntrySeparator, String keyValueSeparator) {
		if (dataEntrySeparator == null) {
			dataEntrySeparator = DEFAULT_DATA_SEPARATOR;
		}
		if (keyValueSeparator == null) {
			keyValueSeparator = DEFAULT_VALUE_SEPARATOR;
		}
		Map<String, String> nameValuePair = new LinkedHashMap<String, String>();
		if (text != null && !"".equals(text.trim())) {
			if (text.indexOf(dataEntrySeparator) != -1) {
				String[] token = text.split(dataEntrySeparator);

				for (String element2 : token) {
					if (element2.indexOf(keyValueSeparator) != -1) {
						String[] element = element2.split(keyValueSeparator);
						if (element.length > 1) {
							nameValuePair.put(element[0].trim(), element[1].trim());
						} else {
							nameValuePair.put(element[0].trim(), null);
						}
					}
				}
			} else if (text.indexOf(keyValueSeparator) != -1) {
				String[] element = text.split(keyValueSeparator);
				if (element != null) {
					nameValuePair.put(element[0].trim(), element[1].trim());
				}
			}
		}
		return nameValuePair;
	}

	/**
	 * Return the key value pair map for the given text. If text is empty or null, empty map will be returned
	 * 
	 * @param text
	 * @param entrySeparator
	 * @return
	 */
	private static Map<String, String> getKeyValuePair(String text, String entrySeparator) {
		return getKeyValuePair(text, entrySeparator, null);
	}

	/**
	 * Return the key value pair map for the given text. If text is empty or null, empty map will be returned
	 */
	private static Map<String, String> getKeyValuePair(String text) {
		return getKeyValuePair(text, null, null);
	}

	private static List<Map<String, String>> getKeyValueList(String text) {
		List<String> itemList = getValueListBySeparator(text, SEMI_COLON_SEPARATOR);
		List<Map<String, String>> retList = new ArrayList<Map<String, String>>();

		for (String item : itemList) {
			retList.add(getKeyValuePair(item));
		}

		return retList;
	}

	private static List<String> getValueList(String text) {
		return getValueListBySeparator(text, DEFAULT_DATA_SEPARATOR);
	}

	private static List<String> getValueListBySeparator(String text, String entrySeparator) {
		List<String> retList = new ArrayList<String>();
		if (text != null && !"".equals(text.trim())) {
			if (text.indexOf(entrySeparator) != -1) {
				String[] values = text.split(entrySeparator);

				for (String value : values) {
					retList.add(value.trim());
				}
			} else {
				retList.add(text);
			}
		}
		return retList;
	}

	private static int getInt(Map<String, String> configMap, String key, int defaultValue) {
		if (configMap.containsKey(key)) {
			return Integer.parseInt(configMap.get(key));
		}
		return defaultValue;
	}

	private static String getStringFromConfigMap(String appParamKey, String key, String defaultValue, String entrySeparator) {
		Map<String, String> configMap = getKeyValuePair(globalConfig.getBizParam(appParamKey), entrySeparator);
		if (configMap.containsKey(key)) {
			return configMap.get(key);
		}
		return defaultValue;
	}

	private static String getStringFromConfigMap(String appParamKey, String key, String defaultValue) {
		Map<String, String> configMap = getKeyValuePair(globalConfig.getBizParam(appParamKey));
		if (configMap.containsKey(key)) {
			return configMap.get(key);
		}
		return defaultValue;
	}

	private static int getIntFromConfigMap(String appParamKey, String key, int defaultValue) {
		Map<String, String> config = getKeyValuePair(globalConfig.getBizParam(appParamKey));

		return getInt(config, key, defaultValue);
	}

	public static long getTimeInMillis(String timeFormat) {
		timeFormat = PlatformUtiltiies.nullHandler(timeFormat);

		if (timeFormat.length() > 0) {
			String[] arrDuration = timeFormat.split(":");
			if (arrDuration.length == 3) {
				return Long.parseLong(arrDuration[0]) * 24 * 60 * 60 * 1000 + Long.parseLong(arrDuration[1]) * 60 * 60 * 1000
						+ Long.parseLong(arrDuration[2]) * 60 * 1000;
			} else if (arrDuration.length == 2) {
				return Long.parseLong(arrDuration[0]) * 60 * 60 * 1000 + Long.parseLong(arrDuration[1]) * 60 * 1000;
			} else if (arrDuration.length == 1) {
				return Long.parseLong(arrDuration[0]) * 60 * 1000;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}

	/**
	 * Get a Double list from a string use this method only if the use this method only if the string contains numeric
	 * values
	 * 
	 * @param text
	 * @param entrySeparator
	 * @return
	 */
	private static List<Double> getValueList(String text, String entrySeparator) {
		List<Double> retList = new ArrayList<Double>();
		if (text != null && !"".equals(text.trim())) {
			if (text.indexOf(entrySeparator) != -1) {
				String[] values = text.split(entrySeparator);

				for (String string : values) {
					retList.add(Double.parseDouble(string));
				}
			} else {
				retList.add(Double.parseDouble(text));
			}
		}
		return retList;
	}

	/**
	 * Get a value of pair from a string Eg : if the string is 50,60,70 ==> it will return Pair<50,60> , Pair<60,70> and
	 * Pair<70,null>
	 * 
	 * @param text
	 * @return
	 */
	public static List<Pair<Double, Double>> getDoublePairOfValuesFromString(String text, String entrySeparator) {
		List<Pair<Double, Double>> pairs = new ArrayList<Pair<Double, Double>>();

		List<Double> valueList = getValueList(text, entrySeparator);
		Collections.sort(valueList);

		// Not using for each loop because pairs needs to be filled checking if
		// there is a another value
		Iterator<Double> it = valueList.iterator();
		Double left = null;
		Double right = null;
		Double preRight = null;
		int i = 0;
		while (it.hasNext()) {
			left = null;
			right = null;

			if (i == 0) {
				left = it.next();
				preRight = left;
			}

			if (it.hasNext()) {
				right = it.next();
			}

			pairs.add(Pair.of(preRight, right));
			preRight = right;

			i++;
		}

		// Set the max value
		if (valueList.size() > 1) {
			Double nullDouble = null;
			pairs.add(Pair.of(preRight, nullDouble));
		}

		return pairs;
	}

	/****************************************************************************************
	 * END OF UTILITY METHODS *
	 ****************************************************************************************/

	public static long getInternationalBufferDurationInMillis() {
		return Long.parseLong(globalConfig.getBizParam(SystemParamKeys.INTERNATIONAL_CHANGEBUFFER_TIME)) * 60 * 60 * 1000;
	}

	public static long getDomesticBufferDurationInMillis() {
		return Long.parseLong(globalConfig.getBizParam(SystemParamKeys.DOMESTIC_CHANGEBUFFER_TIME)) * 60 * 60 * 1000;
	}

	public static long getOnholdBufferDurationInMillis() {
		return Long.parseLong(globalConfig.getBizParam(SystemParamKeys.ONHOLD_STOP_BUFFER_TIME)) * 60 * 60 * 1000;
	}

	public static int getOnholdBufferDurationInHours() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.ONHOLD_STOP_BUFFER_TIME));
	}

	public static int getInternationalModBufferDurationInHours() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.INTERNATIONAL_CHANGEBUFFER_TIME));
	}

	public static int getDomesticModBufferDurationInHours() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.DOMESTIC_CHANGEBUFFER_TIME));
	}

	public static int getFlightClosureGapInMins() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
	}

	public static int getOutboundFlexiPrecentage() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.OUTBOUND_RETURN_FLEXI_CHARGE_PERCENTAGE));
	}

	public static String getEBIServiceChargeCode() {
		return globalConfig.getBizParam(SystemParamKeys.EXTERNAL_PAYMENT_SERVICE_CHARGE_CODE);
	}

	public static boolean applyCreditCardCharge() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_CREDITCARD_CHARGES));
	}
	public static boolean isEnableAdvanceRollForwardOperation(){
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ADVANCE_ROLLFORWARD_OPERATION));
	}
	public static boolean isCreditCardPaymentForKiosksEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CREDITCARD_PAYMENTS_KIOSKS));
	}

	public static boolean isQueryDRRefund() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.QUERY_DR_REFUND));
	}

	public static int getTimeForQueryDROperation() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.QUERY_DR_SCHED_TIME_MIN));
	}

	public static int getTimeForPaymentStatusUpdate() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.PAYMENT_STATUS_UPDATE_HOLD_MIN));
	}

	public static int getTimeForSeatMapSeatRelease() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.SEAT_MAP_SCHED_SEAT_RELEASE_MIN));
	}

	// TODO NILI. Remove this app parameters once in HEAD
	public static int getOutboundReturnFarePercentage() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.OUTBOUND_RETURN_FARE_PERCENTAGE));
	}

	// TODO NILI. Remove this app parameters once in HEAD
	public static int getOpenEndedOutboundFarePercentage() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.OPEN_ENDED_OUTBOUND_RETURN_FARE_PERCENTAGE));
	}

	public static boolean isConfirmReservationIfPaymentExistsAtIPG() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CONFIRM_RESERVATION_IF_PAYMENT_EXISTS_AT_IPG));
	}
	
	public static boolean  isStandByPaxExcluded() {
		   return YES.equals(globalConfig.getBizParam(SystemParamKeys.EXCLUDE_STANDBY_PAX));
	}


	/**
	 * Find if the agent enabled additional WS validations.
	 * 
	 * @param carrierCode
	 * @return
	 */
	public static boolean isWebServiceExtaraValidationSkipCarrier(String carrierCode) {

		String[] carrierCodes = globalConfig.getBizParam(SystemParamKeys.WEBSERVICE_EXTRA_VALIDATION_SKIP_AGENCIES).trim()
				.split(",");
		for (String skipCarrier : carrierCodes) {
			if (skipCarrier.equals(carrierCode)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Find if the agent is enabled the fraud check for CC
	 * 
	 * @param carrierCode
	 * @return
	 */
	public static boolean isFraudEnabledAgent(String carrierCode) {
		List<String> carrierCodes = getValueList(globalConfig.getBizParam(SystemParamKeys.WEBSERVICE_FRAUD_ENABLE_AGENCIES));
		if (carrierCodes.contains(carrierCode)) {
			return true;
		}
		return false;
	}

	public static long getMinimumReturnTransitTimeInMillis() {
		String minRetTransTime = globalConfig.getBizParam(SystemParamKeys.MIN_RETURN_TRANSITION_TIME);
		return getTimeInMillis(minRetTransTime);
	}

	/**
	 * Transit time threshold in milliseconds.
	 * 
	 * @return Transit time threshold
	 */
	public static long getTransitTimeThresholdInMillis() {
		String minTransitionTimeStr = globalConfig.getBizParam(SystemParamKeys.MIN_CHARGEABLE_TRANSIT_DURATION);
		String hours = minTransitionTimeStr.substring(0, minTransitionTimeStr.indexOf(":"));
		String mins = minTransitionTimeStr.substring(minTransitionTimeStr.indexOf(":") + 1);

		return ((Long.parseLong(hours) * 60) + Long.parseLong(mins)) * 60 * 1000;
	}

	/**
	 * The Hub airport.
	 * 
	 * @return The Hub
	 * @deprecated
	 */
	@Deprecated
	public static String getSystemHubAirport() {
		return globalConfig.getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
	}

	/**
	 * The carrier code.
	 * 
	 * @return String
	 */
	public static String getDefaultCarrierCode() {
		return globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
	}

	/**
	 * return the default carrier name
	 * 
	 * @return
	 */
	public static String getDefaultCarrierName() {
		return globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME);
	}

	/**
	 * return the default airlineID code
	 * 
	 * @return String
	 */
	public static String getDefaultAirlineIdentifierCode() {
		return globalConfig.getBizParam(SystemParamKeys.DEFAULT_AIRLINE_IDENTIFIER_CODE);
	}

	public static String getRequiredDefaultAirlineIdentifierCode() {
		if (isAllowAirlineCarrierPrefixForAgentsAndUsers()) {
			return globalConfig.getBizParam(SystemParamKeys.DEFAULT_AIRLINE_IDENTIFIER_CODE);
		} else {
			return "";
		}
	}

	/**
	 * Required
	 * 
	 * @return
	 */
	public static boolean isPNLEticketEnabled() {
		if (globalConfig.getBizParam(SystemParamKeys.TICKET_FORMAT).equalsIgnoreCase(CommonsConstants.PNLticketType.TKNE)) {
			return true;
		} else {
			return false;
		}
	}

	public static String getEticketFormat() {
		return globalConfig.getBizParam(SystemParamKeys.TICKET_FORMAT);
	}

	/**
	 * Return the credit consume agent for given carrier
	 * 
	 * @param carrier
	 * @return
	 */
	public static String getCreditConsumeAgent(String carrier) {

		Map<String, String> carrierAgentMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.CREDTI_CONSUME_AGENTS));
		return carrierAgentMap.get(carrier);
	}

	public static List<String> getCreditConsumeAgents() {
		Map<String, String> carrierAgentMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.CREDTI_CONSUME_AGENTS));
		List<String> agentList = new ArrayList<String>();
		agentList.addAll(carrierAgentMap.values());
		return agentList;
	}

	/**
	 * Return the base currency
	 */
	public static String getBaseCurrency() {
		return globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
	}

	/**
	 * Return the default payment currency
	 */
	public static String getDefaultPGCurrency() {
		return globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARD_PAYMENT_CURRENCY);
	}

	/**
	 * Returns the seat selection/modification cutover for XBE
	 */
	public static long getXBESeatmapStopCutoverInMillis() {
		// Expected format HH:mm
		String strCutover = globalConfig.getBizParam(SystemParamKeys.SEATMAP_XBE_SELECTION_MOD_STOP_CUTOVER);

		String[] strCutoverComponents = strCutover.split(":");

		return Long.parseLong(strCutoverComponents[0]) * 60 * 60 * 1000 + Long.parseLong(strCutoverComponents[1]) * 60 * 1000;
	}

	/**
	 * Returns the airport transfer selection/modification cutover for domestic
	 */
	public static long getAirportTransferStopCutoverForDomesticInMillis() {
		// Expected format HH:mm
		String strCutover = globalConfig.getBizParam(SystemParamKeys.AIRPORT_TRANSFER_SELECTION_MOD_STOP_CUTOVER_FOR_DOMESTIC);

		String[] strCutoverComponents = strCutover.split(":");

		return Long.parseLong(strCutoverComponents[0]) * 60 * 60 * 1000 + Long.parseLong(strCutoverComponents[1]) * 60 * 1000;
	}

	/**
	 * Returns the airport transfer selection/modification cutover for domestic
	 */
	public static long getAirportTransferStopCutoverForInternationalInMillis() {
		// Expected format HH:mm
		String strCutover = globalConfig
				.getBizParam(SystemParamKeys.AIRPORT_TRANSFER_SELECTION_MOD_STOP_CUTOVER_FOR_INTERNATIONAL);

		String[] strCutoverComponents = strCutover.split(":");

		return Long.parseLong(strCutoverComponents[0]) * 60 * 60 * 1000 + Long.parseLong(strCutoverComponents[1]) * 60 * 1000;
	}

	/**
	 * Returns the seat selection/modification cutover for IBE
	 */
	public static long getIBESeatmapStopCutoverInMillis() {
		// Expected format HH:mm
		String strCutover = globalConfig.getBizParam(SystemParamKeys.IBE_SEATMAP_CUTTOFF);

		String[] strCutoverComponents = strCutover.split(":");

		return Long.parseLong(strCutoverComponents[0]) * 60 * 60 * 1000 + Long.parseLong(strCutoverComponents[1]) * 60 * 1000;
	}

	/**
	 * Will Extract the Carrier Code from the flight number
	 * 
	 * @param flightNumber
	 * @return
	 */
	public static String extractCarrierCode(String flightNumber) {
		flightNumber = PlatformUtiltiies.nullHandler(flightNumber);
		if (flightNumber.length() > 2) {
			return flightNumber.substring(0, 2);
		} else {
			return "";
		}
	}

	/**
	 * Returns the Carrier Name
	 * 
	 * @param carrierCode
	 * @param enabledForLocalCarrier
	 * @return
	 */
	public static String getCarrierDesc(String carrierCode) {
		Map<String, InterlinedAirLineTO> interlinedAirlinesMap = globalConfig.getActiveInterlinedCarriersMap();
		String strCarrierName = "";

		if (interlinedAirlinesMap.containsKey(carrierCode)) {
			InterlinedAirLineTO interlinedAirLineTO = interlinedAirlinesMap.get(carrierCode);
			strCarrierName = interlinedAirLineTO.getAirlineDesc();
		} else {
			strCarrierName = globalConfig.getAllSysCarriersMap().get(carrierCode);
		}

		return strCarrierName;
	}

	/**
	 * Returns No of days that cookie expires
	 */
	public static String getNoOfCookieExpiryDays() {
		return globalConfig.getBizParam(SystemParamKeys.NO_OF_COOKIE_EXPIRY_DAYS);
	}

	public static boolean isCookieSaveFunctionalityEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_COOKIE_SAVE_FUNCTIONALITY));
	}

	/**
	 * Compose the CommonTemplatingDTO Object
	 * 
	 * @return
	 */
	public static CommonTemplatingDTO composeCommonTemplatingDTO(String carrierCode) {

		CommonTemplatingDTO commonTemplatingDTO = new CommonTemplatingDTO();

		commonTemplatingDTO.setCarrierCode(
				PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE, carrierCode)));
		commonTemplatingDTO.setCarrierName(
				PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME, carrierCode)));
		commonTemplatingDTO.setAirlineDomain(
				PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.AIRLINE_DOMAIN, carrierCode)));
		commonTemplatingDTO.setUnsubscribeEMail(
				PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.UNSUBSCRIBE_EMAIL, carrierCode)));

		commonTemplatingDTO.setAccelAeroClientID(PlatformUtiltiies
				.nullHandler(globalConfig.getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER, carrierCode)));

		String url = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, carrierCode));

		if (url.startsWith("http://") || url.startsWith("https://")) {
			int index = url.indexOf("/");
			if (index != -1) {
				url = url.substring(index + 2);
				commonTemplatingDTO.setAirlineSubDomain(url);
			}
		} else {
			commonTemplatingDTO.setAirlineSubDomain(url);
		}

		return commonTemplatingDTO;
	}

	public static boolean isShowPaxETKT() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_PAX_ETICKET));
	}

	public static boolean isShowTravelInsurance() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_TRAVEL_INSURANCE));
	}

	public static boolean isShowSeatMap() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_SEAT_MAP));
	}

	public static boolean isShowMeals() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_MEAL));
	}

	public static boolean isShowBaggages() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_BAGGAGE));
	}

	public static boolean isShowHolidaysLink() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_HOLIDAYS_URL_IN_IBE));
	}

	public static boolean isShowRentACarLink() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_RENT_A_CAR_URL_IN_IBE));
	}

	public static long getGdsConfirmationOnholdDurationInMillis() {
		return Long.parseLong(globalConfig.getBizParam(SystemParamKeys.GDS_CONFIRMATION_ONHOLD_DURATION)) * 60 * 60 * 1000;
	}

	public static long getGdsTicktingOnholdDurationInMillis() {
		return Long.parseLong(globalConfig.getBizParam(SystemParamKeys.GDS_TICKTING_ONHOLD_DURATION)) * 60 * 60 * 1000;
	}

	public static boolean isFareDiscountEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_FARE_DISCOUNT));
	}

	public static boolean isAgentInstructionsEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AGENT_INSTRUCTIONS));
	}

	public static boolean isOwnerAgentTransferEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_TRANSFER_OWNER_AGENT));
	}

	public static boolean isLinkedFaresBasedOnMasaterFaresEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_LINKED_FARES_BASED_ON_MASTER_FARES));
	}

	public static boolean isFlightUserNoteEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FLIGHT_USER_NOTE));
	}

	public static boolean isShowNoFareSplitOption() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_NO_FARE_SPLIT_OPTION));
	}

	public static List<FlightTypeDTO> availableFlightTypes() {

		if (lstFlightTypeDTO == null) {
			lstFlightTypeDTO = new ArrayList<FlightTypeDTO>();

			String appParamFltTypes = globalConfig.getBizParam(SystemParamKeys.FLIGHT_TYPE_AVAILABILITY);
			String[] fltTypes = appParamFltTypes.split(DEFAULT_DATA_SEPARATOR);

			for (String fltType : fltTypes) {
				FlightTypeDTO fltTypeDTO = new FlightTypeDTO();
				String[] fltAttributes = fltType.split(SEMI_COLON_SEPARATOR);
				String[] fltTypeWithVal = fltAttributes[0].split(DEFAULT_VALUE_SEPARATOR);
				String[] fltDesWithVal = fltAttributes[1].split(DEFAULT_VALUE_SEPARATOR);
				String[] fltTypeStatusWithVal = fltAttributes[2].split(DEFAULT_VALUE_SEPARATOR);
				fltTypeDTO.setFlightType(fltTypeWithVal[1]);
				fltTypeDTO.setFlightTypeDesction(fltDesWithVal[1]);
				fltTypeDTO.setFlightTypeStatus(fltTypeStatusWithVal[1]);
				lstFlightTypeDTO.add(fltTypeDTO);
			}
		}
		return lstFlightTypeDTO;
	}

	/**
	 * Returns the mapded agent for given external GDS booking
	 * 
	 * @param marketingCarrier
	 * @return
	 */
	public static String getGdsConnectivityAgent(String marketingCarrier) {
		Map<String, String> gdsAgentMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.GDS_CONNECTIVITY_AGENT));
		return gdsAgentMap.get(marketingCarrier.trim());
	}

	public static boolean isCityBasedAvailabilityEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CITY_BASED_AVAILABILITY));
	}

	public static boolean isChildOnlyBookingsAllowed() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CHILD_BOOKING));
	}

	public static boolean isLogicalCabinClassEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_LOGICAL_CABIN_CLASS));
	}

	public static boolean isStorePaymentsInAgentCurrencyEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY));
	}

	public static boolean isInvoiceInAgentCurrencyEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY));
	}

	public static boolean isAllowLocalCreditLimitDefinition() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_LOCAL_CREDIT_LIMIT_DEF));
	}

	public static boolean isCurrencyRoundupEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CURRENCY_ROUNDUP));
	}

	public static boolean isHideStopOverEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.HIDE_STOP_OVER_SEG));
	}

	public static boolean isBlockNonPayingAgents() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.BLOCK_UNSETTLED_AGENTS));
	}

	public static int getNoOfInvoicesForBlockAgentOperation() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.NUMBER_OF_INVOICES_TO_CONSIDER_FOR_BLOCK_OPERATION));
	}

	public static boolean isHideAddressInItinarary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.HIDE_COMPANY_ADDR_IN_ITNR));
	}

	public static boolean isExcludeSegFareFromConAndRet() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.EXCLUDE_SEG_FARES_FROM_CONNECTION_AND_RETURN));
	}

	public static boolean isDisplayIBEListsDynamicallyEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_IBE_LISTS_DYNAMICALLY));
	}

	public static boolean isCreditCardPaymentsEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CREDIT_CARD_PAYMENTS_ENABLED));
	}

	public static boolean isXBECreditCardPaymentsEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.XBE_CREDIT_CARD_PAYMENTS_ENABLED));
	}

	public static boolean isReservationOnholdEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.RESERVATION_ONHOLD_ENABLED));
	}

	public static boolean isCOSSelectionEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.COS_SELECTION_ENABLED));
	}

	public static boolean isShowAlertsInIBEEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_ALERTS_IBE_ENABLED));
	}

	public static String getNonsecureIBEUrl() {
		return globalConfig.getBizParam(SystemParamKeys.NON_SECURE_IBE_URL);
	}

	public static String getSecureIBEUrl() {
		return globalConfig.getBizParam(SystemParamKeys.SECURE_IBE_URL);
	}

	public static String getSecureServiceAppIBEUrl() {
		return globalConfig.getBizParam(SystemParamKeys.SECURE_SERVICE_APP_IBE_URL);
	}

	public static boolean isGoogleAnalyticsEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.GOOGLE_ANALYTICS_ENABLED));
	}

	public static String getGoogleAnalyticsId() {
		return globalConfig.getBizParam(SystemParamKeys.GOOGLE_ANALYTICS_ID);
	}
	
	public static String getGoogleAnalyticsIdForServiceApp() {
		return globalConfig.getBizParam(SystemParamKeys.GOOGLE_ANALYTICS_ID_FOR_SERVICEAPP);
	}

	public static boolean getIsONDWisePayment() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_OND_WISE_PAYMENT));
	}

	public static boolean showHalfReturnFaresInIBECalendar() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_HALF_RETURN_FARES_IN_IBE_CALENDAR));
	}

	public static boolean disableRefreshingIBCalendarInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISABLE_REFRESHING_IB_CALENDAR_IN_IBE));
	}

	public static String getGoogleAnalyticsCurrency() {
		return globalConfig.getBizParam(SystemParamKeys.GOOGLE_ANALYTICS_CURRENCY);
	}

	public static String getAirlineDomainName() {
		return globalConfig.getBizParam(SystemParamKeys.AIRLINE_DOMAIN);
	}

	public static String getHolidaysURL() {
		return globalConfig.getBizParam(SystemParamKeys.HOLIDAYS_URL);
	}

	public static String getHolidaysAncilaryReminderURL() {
		String ancillaryReminderURL = globalConfig.getBizParam(SystemParamKeys.HOLIDAY_ANCILLARY_REMINDER_URL);
		if (ancillaryReminderURL == null || ancillaryReminderURL.trim().length() == 0) {
			ancillaryReminderURL = getHolidaysURL();
		}
		return ancillaryReminderURL;
	}

	public static String getOctopussiteID() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_OCTOPUS_SITE_ID);
	}

	public static String getRentACarURL() {
		return globalConfig.getBizParam(SystemParamKeys.RENT_A_CAR_URL);
	}

	public static String getDefaultCOS() {
		return globalConfig.getBizParam(SystemParamKeys.CLASS_OF_SERVICE);
	}

	public static String getCarrierAgent() {
		return globalConfig.getBizParam(SystemParamKeys.CARRIER_AGENT);
	}

	public static boolean isEnforceSameHigher() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENFORCE_SAME_HIGHER));
	}

	public static boolean isShowIBEChargesBreakdownEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_IBE_CHARGES_BREAKDOWN));
	}

	public static boolean getShowCustomizedItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_CUSTOMIZED_ITINERARY));
	}

	public static boolean isSkipDurationToleranceForShedMsgs() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_DURATION_TOLERANCE_FOR_SHEDULE_MSGS));
	}

	public static boolean isScheduleMsgProcessErrorEmailEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SCHEDULE_MSG_PROCESS_ERROR_EMAIL));
	}

	public static List<String> getScheduleMsgProcessErrorEmailRecipients() {
		String recipientStr = globalConfig.getBizParam(SystemParamKeys.SCHEDULE_MSG_PROCESS_ERROR_EMAIL_RECIPIENT);
		String[] recipients = recipientStr.split(DEFAULT_DATA_SEPARATOR);
		return Arrays.asList(recipients);
	}
	
	public static boolean isExcludeOneWayFareFromConRet() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.EXCLUDE_ONEWAY_FARES_FROM_CONNECTION_RETURN));
	}
	/**
	 * returns supported languages map for the IBE
	 * 
	 * @return
	 */
	public static void setIBESupportedLanguages() {
		supportedLanguagesMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.SUPPORTED_IBE_LANGUAGES));
	}

	public static void setXBESupportedLanguages() {
		xbeSupportedLanguagesMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.XBE_SUPPORTED_LANGUAGES));
	}

	/**
	 * returns ibe url map
	 * 
	 * @return
	 */
	public static void setIBEUrls() {
		ibeUrlMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.IBE_URLS));
	}

	/**
	 * returns supported languages for IBE
	 * 
	 * @return Map
	 */
	public static Map<String, String> getIBESupportedLanguages() {
		return supportedLanguagesMap;
	}

	public static Map<String, String> getXBESupportedLanguages() {
		return xbeSupportedLanguagesMap;
	}

	/**
	 * returns ibe urls
	 * 
	 * @return Map
	 */
	public static Map<String, String> getIBEUrls() {
		return ibeUrlMap;
	}

	public static boolean isOnHoldRateEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.USE_LAST_MOD_RATES));
	}

	public static boolean isOpenEndedReturnEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.OPEN_ENDED_RETURN_SUPPORT));
	}

	public static boolean isSegmentExpiryDateDisplayEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_RES_SEG_EXP_DATE));
	}

	public static boolean isCancelConfirmExpiredOpenReturnSegments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CANCEL_EXPIRED_OPEN_ENDED_SEGMENTS));
	}

	public static boolean isUnregisterdUserCanCancelSegment() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CANCEL_SEGMENT_BY_UNREGISTERED_USER));
	}

	public static boolean isUnregisterdUserCanCancelReservation() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CANCEL_RESERVATION_BY_UNREGISTERED_USER));
	}

	public static boolean isUnregisterdUserCanMofidyReservation() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.MODIFY_RESERVATION_BY_UNREGISTERED_USER));
	}

	public static boolean isUnregisterdUserCanAddServices() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ADD_SERVICES_BY_UNREGISTERED_USER));
	}

	public static boolean isUnregisterdUserCanEditServices() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.EDIT_SERVICES_BY_UNREGISTERED_USER));
	}

	public static String getUnregisterdUserAllowdBookingChanels() {
		return globalConfig.getBizParam(SystemParamKeys.GET_BOOKING_CHANNELS_FOR_UNREGISTERED_USER);
	}

	public static boolean isCaptchaEnabledForUnregisterdUser() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CAPTCHA_ENABLED_FOR_UNREGISTERED_USER));
	}

	public static boolean isAirportServicesEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_AIRPORT_SERVICES));
	}

	public static boolean isAirportTransferEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_AIRPORT_TRANSFER));
	}

	public static boolean isAllowEditAirportTransfers() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.EDIT_AIRPORT_TRANSFER));
	}

	public static boolean isONDBaggaeEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_OND_BAGGAGES));
	}

	public static boolean isInFlightServicesEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_IN_FLIGHT_SERVICES));
	}

	public static String[] ibeGroupSSR() {
		String strSSR = globalConfig.getBizParam(SystemParamKeys.IBE_GROUP_SSR);
		if (strSSR != null && !strSSR.equals("")) {
			return strSSR.split(",");
		}
		return null;
	}

	public static int getShowSeatCount() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.SHOW_SEATS));
	}

	public static boolean isCCPrefixCardCheckEnable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CC_BIN_PRFIX_CHECK));
	}

	public static boolean isRegisterdUserCanAddServices() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ADD_SERVICES_BY_REGISTERED_USER));
	}

	public static boolean isRegisterdUserCanEditServices() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.EDIT_SERVICES_BY_REGISTERED_USER));
	}

	public static boolean isIBEAddSeat() {
		return (isShowSeatMap() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ADD_SEAT)));
	}

	public static boolean isIBEEditSeat() {
		return (isShowSeatMap() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_EDIT_SEAT)));
	}

	public static boolean isIBEAddInsurance() {
		return (isShowTravelInsurance() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ADD_INSURANCE)));
	}

	public static boolean isIBEEditInsurance() {
		return (isShowTravelInsurance() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_EDIT_INSURANCE)));
	}

	public static boolean isIBEAddMeal() {
		return (isShowMeals() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ADD_MEAL)));
	}

	public static boolean isIBEEditMeal() {
		return (isShowMeals() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_EDIT_MEAL)));
	}

	public static boolean isIBEAddBaggage() {
		return (isShowBaggages() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ADD_BAGGAGE)));
	}

	public static boolean isIBEEditBaggage() {
		return (isShowBaggages() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_EDIT_BAGGAGE)));
	}

	public static boolean isIBEAddAirportServices() {
		return (isAirportServicesEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ADD_HALA)));
	}

	public static boolean isIBEEditAirportServices() {
		return (isAirportServicesEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_EDIT_HALA)));
	}

	public static boolean isIBEAddAirportTransfers() {
		return (isAirportTransferEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_AIRPORT_TRANSFER)));
	}

	public static boolean isIBEAddBusSegment() {

		return (isGroundServiceEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ADD_BUS)));

	}

	public static boolean isLoyalityEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ENABLE_LOYALITY));
	}

	public static boolean isIntegrateMashreqWithLMS() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.INTEGRATE_MASHREQ_WITH_LMS));
	}

	public static String getLoyaltyUserForWeb() {
		return globalConfig.getBizParam(SystemParamKeys.LOAYLTY_USER_ID_FOR_WEB);
	}

	/**
	 * App parameter to enable Check for duplicate names in a flight segment when making a reservation AARESAA-1953.
	 * 
	 * @return
	 */
	public static boolean isDuplicateNameCheckEnabled() {
		if (getDuplicateNameCheckEnabled().equals(YES)) {
			return true;
		}

		return false;
	}

	public static String getDuplicateNameCheckEnabled() {
		return PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.DUPLICATE_NAME_CHECK));
	}

	public static boolean isBlacklistpassengerCheckEnabled() {
		if (getblacklistpaxEnabled().equals(YES)) {
			return true;
		}

		return false;
	}
	
	public static String getblacklistpaxEnabled(){
		return PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.ENABLE_BLACKLIST_PAX));
	}
	/**
	 * param to send hala notifications instantly when adding/modifying a hala service
	 * 
	 * @return
	 */
	public static boolean isHalaInstantNotificationsEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.HALA_INSTANT_NOTIFICATION));
	}

	/**
	 * param to enable multple meal selection
	 * 
	 * @return
	 */
	public static boolean isMultipleMealSelectionEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.MULTIPLE_MEAL_SELECTION));
	}

	/**
	 * param to enable disable inventory checking for SSR
	 * 
	 * @return
	 */
	public static boolean isInventoryCheckForSSREnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_INV_CHECK_FOR_SSR));
	}

	/**
	 * Check For Invoice Generation Method Either "P" periodically or "T" vice
	 * 
	 * @return true if Transaction vice Invoice Enable
	 */
	public static boolean isTransactInvoiceEnable() {
		if ("T".equals(globalConfig.getBizParam(SystemParamKeys.INVOICE_NO_GENERATION_METHOD))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the number of navigation steps to show in IBE
	 * 
	 * @return
	 */
	public static int getIBENavSteps() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.IBE_NAV_STEPS));
	}

	/**
	 * Returns the number of navigation steps to show in IBE
	 * 
	 * @return
	 */
	public static String getIBEBannerType() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_BANNER_TYPE);
	}

	public static boolean showLCCResultsInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_LCC_RESULTS));
	}

	public static boolean isLCCConnectivityEnabled() {
		if (SystemPropertyUtil.isForceEnableLccConnectivity()) {
			return true;
		} else {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.LCC_CONNCECTIVITY_ENABLED));
		}
	}

	private static Map<String, String> getNameValuePair(String text) {
		Map<String, String> nameValuePair = new LinkedHashMap<String, String>();
		if (text != null && !"".equals(text.trim())) {
			if (text.indexOf(",") != -1) {
				String[] token = text.split(",");

				for (String element2 : token) {
					if (element2.indexOf("=") != -1) {
						String[] element = element2.split("=");
						nameValuePair.put(element[0], element[1]);
					}
				}
			} else if (text.indexOf("=") != -1) {
				String[] element = text.split("=");
				nameValuePair.put(element[0], element[1]);
			}
		}
		return nameValuePair;
	}

	public static String[] getLCCConnectivityCredentials() {
		String text = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.LCC_CONNCECTIVITY_CREDENTIALS));
		Map<String, String> credentailsMap = getKeyValuePair(text);

		if (credentailsMap.size() == 2) {
			return credentailsMap.values().toArray(new String[] {});
		} else {
			return new String[] { "", "" };
		}
	}

	public static Map<String, String> getFAWRYCredentials() {
		String text = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.FAWRY_USER_CREDENTIALS));
		Map<String, String> credentailsMap = getKeyValuePair(text);

		if (credentailsMap.size() == 4) {
			return credentailsMap;
		} else {
			return null;
		}
	}

	public static List<String> getFAWRYAccessControlList() {
		String text = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.FAWRY_ACCESS_CONTROL_LIST));
		List<String> aclList = getValueList(text);

		if (aclList.size() > 0) {
			return aclList;
		} else {
			return null;
		}
	}

	public static Map<String, String> getAmadeusCredentials() {
		String text = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.AMADEUS_USER_CREDENTIALS));
		Map<String, String> credentailsMap = getKeyValuePair(text);

		if (credentailsMap.size() == 3) {
			return credentailsMap;
		} else {
			return null;
		}
	}

	public static Map<String, String> getAmadeusCompatibilityMap() {
		String text = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.AMADEUS_COMPATIBILITY));
		Map<String, String> compatMap = getKeyValuePair(text);
		return compatMap;
	}

	public static List<String> getAmadeusAccessControlList() {
		String text = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.AMADEUS_ACCESS_CONTROL_LIST));
		List<String> aclList = getValueList(text);
		if (aclList.size() > 0) {
			return aclList;
		} else {
			return null;
		}
	}

	public static boolean isFawryCDMRoundingEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.FAWRY_CDM_ROUNDING_ENABLED));
	}

	public static String getChargeGroupDesc(String chargeGroupCode) throws ModuleException {
		Map<String, String> mapChargeGroupCodeDesc = globalConfig.getChargeGroupCodeNDescMap();
		return mapChargeGroupCodeDesc.get(chargeGroupCode);
	}

	public static boolean isTestStstem() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SYSTEM_TYPE));
	}

	public static long getMasterDataCacheTimeout() {
		return globalConfig.getMasterDataCacheTimeout();
	}

	public static String getCarrierUrl(String key) {
		return globalConfig.getCarrierUrls().get(key);
	}

	public static Map<String, String> getCarrierUrls() {
		return globalConfig.getCarrierUrls();
	}

	public static boolean isAllowIBECancellationServicesReservation() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ALLOW_CANCELLATION));
	}

	public static boolean isAllowHalfReturnFaresForNewBookings() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_HALF_RETURN_FARES_FOR_NEW_BOOKINGS));
	}

	public static boolean isAllowHalfReturnFaresForModification() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_HALF_RETURN_FARES_FOR_MODIFICATION));
	}

	public static boolean isAllowHalfReturnSegmentCombinability() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_HALF_RETURN_SEGMENT_COMBINABILITY));
	}

	public static boolean isIBEReturnEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ALLOW_RETURN));
	}

	public static boolean isAllowCapturePayRef() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_CAPTURE_PAY_REF));
	}

	public static boolean isDownLoadCSVCashSalesReport() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.VIEW_CASH_SALES_EXPORT));
	}

	/**
	 * Return the FLA flight performance json string
	 */
	public static String getFlaFlightPerformance() {
		return globalConfig.getBizParam(SystemParamKeys.FLA_FLIGHT_PERFORMANCE);
	}

	/**
	 * Return the FLA graph channels
	 */
	public static String getFlaGraphChannels() {
		return globalConfig.getBizParam(SystemParamKeys.FLA_GRAPH_BKNG_CHNL);
	}

	public static boolean isScheduledReportsAllowed() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SCHEDULE_REPORTS));
	}

	/**
	 * Return the reporting period
	 */
	public static Integer getReportingPeriod() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD));
	}

	public static Integer getMISReriod() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.MIS_MAX_SEARCH_PERIOD));
	}

	/**
	 * Get the adjustment carrier list. If no carrier is included then null is return.
	 * 
	 * @return
	 */
	public static String[] getAdjustmentCarrierList() {
		String carrierCodes = globalConfig.getBizParam(SystemParamKeys.ADJUSTMENT_CARRIER_LIST);
		if (carrierCodes == null || carrierCodes.trim().isEmpty()) {
			return null;
		} else {
			return globalConfig.getBizParam(SystemParamKeys.ADJUSTMENT_CARRIER_LIST).split(",");
		}
	}

	public static String getFTPLocationForRevenueReports() {
		return globalConfig.getBizParam(SystemParamKeys.FTP_FOR_GENERATED_REVENUE_REPORT);
	}

	public static String getFTPProcGeneratedServerLoginDetails() {
		return globalConfig.getBizParam(SystemParamKeys.FTP_SERVER_LOGIN_DETAILS);
	}

	public static String getReportURLPRefix() {
		return globalConfig.getBizParam(SystemParamKeys.REPORT_URL_PREFIX);
	}

	public static boolean isAncillaryReminderEnable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ANCI_REMINDER));
	}

	public static String getAncillaryReminderSchedulerTimeDuration() {
		return globalConfig.getBizParam(SystemParamKeys.ANCI_REMINDER_SCHEDULER_TIME_DURATION);
	}

	public static String getMealCutOverTime() {
		return globalConfig.getBizParam(SystemParamKeys.MEAL_CUTOVERTIME);
	}

	public static String getBaggageFinalCutOverTime() {
		return globalConfig.getBizParam(SystemParamKeys.BAGGAGE_SELECTION_FOR_FINAL_CUT_OVERTIME);
	}

	public static String getFreshBaggageCutOverTime() {
		return globalConfig.getBizParam(SystemParamKeys.BAGGAGE_SELECTION_FOR_FRESH_BAGGAGE_CUT_OVERTIME);
	}

	public static String getAlterBaggageCutOverTime() {
		return globalConfig.getBizParam(SystemParamKeys.BAGGAGE_SELECTION_FOR_ALTER_BAGGAGE_CUT_OVERTIME);
	}

	public static String getStandByFreshBaggageCutOverTime() {
		return globalConfig.getBizParam(SystemParamKeys.STANDBY_BAGGAGE_SELECTION_FOR_FRESH_BAGGAGE_CUT_OVERTIME);
	}

	public static String getStandByAlterBaggageCutOverTime() {
		return globalConfig.getBizParam(SystemParamKeys.STANDBY_BAGGAGE_SELECTION_FOR_ALTER_BAGGAGE_CUT_OVERTIME);
	}

	public static boolean isBaggageMandatory() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.BAGGAGE_MANDATORY));
	}

	public static long getSSRCutOverTimeInMillis() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.SSR_SELECTION_CUTOVER_TIME));
	}

	public static String getCalendarViews() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_CALENDAR_VIEWS);
	}

	public static boolean isCalendarGradiantView() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_CALENDAR_GRADIENT_VIEW));
	}

	public static boolean isModifySegmentBalanceDisplayAvailabilitySearch() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_MODIFY_SEGMENT_BALANCE_DISPLAY_AS));
	}

	public static String getMealDisplayVersion() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_MEAL_DISPLAY_VERSION);
	}

	public static String getSeatMapDisplayVersion() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_SEAT_MAP_DISPLAY_VERSION);
	}

	public static String getCalendarType() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_CALENDAR_TYPE);
	}

	public static boolean getSessionTimeOutDisplay() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_SESSION_TIMEOUT_DISPLAY));
	}

	public static boolean getImageCapchaDisplayAvailabilitySearch() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_IMAGE_CAPCHA_AS));
	}

	public static boolean isOnHoldEnable(OnHold onHoldApp) {
		return YES.equals(getOnHoldProcessData(globalConfig.getBizParam(SystemParamKeys.APPLICATION_ONHOLD), onHoldApp));
	}

	public static String getOnHoldDuration(OnHold onHoldApp) {
		return getOnHoldProcessData(globalConfig.getBizParam(SystemParamKeys.ONHOLD_DURATION), onHoldApp);
	}

	public static String getOnHoldDisplayTime(OnHold onHoldApp) {
		return getOnHoldProcessData(globalConfig.getBizParam(SystemParamKeys.ONHOLD_DISPLAY_TIME), onHoldApp);
	}

	public static String getOnHoldBufferStartCutOver(OnHold onHoldApp) {
		return getOnHoldProcessData(globalConfig.getBizParam(SystemParamKeys.ONHOLD_BUFFER_START_CUT_OVER), onHoldApp);
	}

	public static String getOnHoldBufferEndCutOver(OnHold onHoldApp, String agentCode) {
		return getOnHoldProcessData(globalConfig.getBizParam(SystemParamKeys.ONHOLD_BUFFER_END_CUT_OVER), onHoldApp, agentCode);
	}

	public static String getOnHoldDurationWithInOnHoldBufferTime(OnHold onHoldApp, String agentCode) {
		return getOnHoldProcessData(globalConfig.getBizParam(SystemParamKeys.ONHOLD_DURATION_WITHIN_ONHOLD_BUFFER_TIME),
				onHoldApp, agentCode);
	}

	public static String getDomesticOnHoldDurationWithInOnHoldBufferTime(OnHold onHoldApp, String agentCode) {
		return getOnHoldProcessData(globalConfig.getBizParam(SystemParamKeys.DOMESTIC_ONHOLD_DURATION_WITHIN_ONHOLD_BUFFER_TIME),
				onHoldApp, agentCode);
	}

	public static boolean getOnHoldSendSms(OnHold onHoldApp) {
		return YES.equals(getOnHoldProcessData(globalConfig.getBizParam(SystemParamKeys.ONHOLD_SEND_SMS), onHoldApp));
	}

	public static boolean isApplyRouteBookingClassLevelOnholdReleaseTime() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_ROUTE_BOOKING_CLASS_LEVEL_ONHOLD_TIME));
	}

	public static boolean showCCDetailsInItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_CC_INFO_IN_ITINERARY_ALWAYS));
	}

	private static String getOnHoldProcessData(String applicationTypes, OnHold onHoldApp, String agentCode) {

		String appData = null;
		Map<String, String> appMap = getKeyValuePair(applicationTypes);

		if (agentCode != null && !agentCode.trim().isEmpty()) {
			appData = appMap.get(agentCode);
		}
		if (appData == null || appData.trim().isEmpty()) {
			appData = appMap.get(onHoldApp.toString());
		}

		return appData;
	}

	private static String getOnHoldProcessData(String applicationTypes, OnHold onHoldApp) {
		return getOnHoldProcessData(applicationTypes, onHoldApp, null);
	}

	public static boolean isDefaultTimeZulu() {
		return globalConfig.getBizParam(SystemParamKeys.DEFAULT_TIMEZONE).equalsIgnoreCase("ZULU") ? true : false;
	}

	public static boolean isGroundServiceEnabled() {
		return globalConfig.getBizParam(SystemParamKeys.ENABLE_GROUND_SERVICE).equalsIgnoreCase(YES) ? true : false;
	}

	public static boolean isCustomerRegiterConfirmation() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_CUSTOMER_REGISTER_CONFIRMATION));
	}

	public static boolean isAutoLoginAfterRegistration() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_CUSTOMER_AUTO_LOGIN_AFTER_REGISTRATION));
	}

	public static boolean isIbeDesignVersionAvail() {
		Map<String, String> designMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.IBE_DESIGN_VERSION_AVAILABLE),
				SEMI_COLON_SEPARATOR);
		return YES.equalsIgnoreCase(designMap.get("enableVersions"));// YES.equalsIgnoreCase(designMap.get("enableVersions"));
	}

	public static String getDefaultDesign() {
		Map<String, String> designMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.IBE_DESIGN_VERSION_AVAILABLE),
				SEMI_COLON_SEPARATOR);
		String defaultVersion = "0";
		if (designMap.containsKey("default")) {
			defaultVersion = designMap.get("default");
		}
		return defaultVersion;
	}

	/** +----------------------------------------------------------------+ */
	/** | RAK INSURANCE RELATED Parameters | */
	/** +----------------------------------------------------------------+ */

	/**
	 * Check whether this client has RAK insurance enabled (AIRLINE_66)
	 * 
	 * @return
	 */
	public static boolean isRakEnabled() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return YES.equals(rakInsMap.get("IS_ENABLED"));
	}

	public static String getRakModificationCutoverTime() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return rakInsMap.get("MOD_CUTOVER");
	}

	public static String getRakFirstPublishCutoverTime() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return rakInsMap.get("FIRST_PUBLISH");

	}

	public static String getRakLastPublishCutoverTime() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return rakInsMap.get("LAST_PUBLISH");
	}

	public static String getRakMaxPublishRetries() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return rakInsMap.get("MAX_RETRIES");
	}

	public static String getRakGroupPolicy() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return rakInsMap.get("GROUP_POLICY");
	}

	public static String getRakInsuranceUrl() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return rakInsMap.get("URL");
	}

	public static String getRakContactNo() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return rakInsMap.get("PHONE");
	}

	public static String getRakUploadStatusCheckGap() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.RAK_INSURANCE),
				SEMI_COLON_SEPARATOR);
		return rakInsMap.get("RETRY_GAP");
	}

	public static String getSettlementBankName() {
		Map<String, String> bankInformationMap = getKeyValuePair(
				globalConfig.getBizParam(SystemParamKeys.SETTLEMENT_BANK_INFORMATION), SEMI_COLON_SEPARATOR);
		return bankInformationMap.get("BANK_NAME");
	}

	public static String getSettlementBankAddress() {
		Map<String, String> bankInformationMap = getKeyValuePair(
				globalConfig.getBizParam(SystemParamKeys.SETTLEMENT_BANK_INFORMATION), SEMI_COLON_SEPARATOR);
		return bankInformationMap.get("BANK_ADDRESS");
	}

	public static String getSettlementBankSwiftCode() {
		Map<String, String> bankInformationMap = getKeyValuePair(
				globalConfig.getBizParam(SystemParamKeys.SETTLEMENT_BANK_INFORMATION), SEMI_COLON_SEPARATOR);
		return bankInformationMap.get("SWIFT_NAME");
	}

	public static String getSettlementBankAccountNumber() {
		Map<String, String> bankInformationMap = getKeyValuePair(
				globalConfig.getBizParam(SystemParamKeys.SETTLEMENT_BANK_INFORMATION), SEMI_COLON_SEPARATOR);
		return bankInformationMap.get("ACCOUNT_NUMBER");
	}

	public static String getSettlementBankIBANNo() {
		Map<String, String> bankInformationMap = getKeyValuePair(
				globalConfig.getBizParam(SystemParamKeys.SETTLEMENT_BANK_INFORMATION), SEMI_COLON_SEPARATOR);
		return bankInformationMap.get("IBAN_NO");
	}

	public static String getSettlementBankBeneficiaryName() {
		Map<String, String> bankInformationMap = getKeyValuePair(
				globalConfig.getBizParam(SystemParamKeys.SETTLEMENT_BANK_INFORMATION), SEMI_COLON_SEPARATOR);
		return bankInformationMap.get("BENEFICIARY_NAME");
	}

	public static final boolean isTravelAgentIncentiveAnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_INCENTIVE_SCHEMES));
	}

	public static int getRecordsPerReservationSearchPage() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.RECORDS_PER_RESERVATION_SEARCH_PAGE));
	}

	public static Collection<String> getChargeGroupFulFillmentOrderForPayment() {
		return getValueList(globalConfig.getBizParam(SystemParamKeys.RES_CHARGE_GROUP_FULFILLMENT_ORDER_FOR_PAYMENT));
	}

	public static Collection<String> getChargeGroupFulFillmentOrderForRefund() {
		return getValueList(globalConfig.getBizParam(SystemParamKeys.RES_CHARGE_GROUP_FULFILLMENT_ORDER_FOR_REFUND));
	}

	public static long getPublicReservationCutoverInMillis() {
		return getTimeInMillis(
				globalConfig.getBizParam(SystemParamKeys.PUBLIC_RESERVATION_CUTOFF_TIME_FOR_INTERNATIONAL));
	}

	public static long getAgentReservationCutoverInMillis() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.AGENT_RESERVATION_CUTOFF_TIME));
	}

	public static long getStandByAgentReservationCutoverInMillis() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.STANDBY_AGENT_RESERVATION_CUTOFF_TIME));
	}

	public static long getReservationCutoverForDomesticInMillis() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.RESERVATION_CUTOFF_TIME_FOR_DOMESTIC_FLIGHTS));
	}

	/**
	 * Return the base currency
	 */
	public static String getFAWRYCurrency() {
		return globalConfig.getBizParam(SystemParamKeys.FAWRY_CURRENCY);
	}

	public static boolean isEnableTransactionGranularity() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_TRANSACTION_GRANULARITY_TRACKING));
	}

	public static boolean showDOBInItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_DOB_IN_ITINERARY));
	}

	public static boolean showPassportInItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_PASSPORT_IN_ITINERARY));
	}

	public static boolean hideChargesInItineraryPassengerDetails() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_CHARGES_IN_ITINERARY));
	}

	public static String getStaticFileSuffix() {
		return globalConfig.getStaticFileSuffix();
	}

	public static String getReportLogo(boolean withStaticFileSuffix) {
		String reportLogo = globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		if (withStaticFileSuffix) {
			reportLogo = StaticFileNameUtil.getCorrected(reportLogo);
		}
		return reportLogo;
	}

	public static boolean isIBEBlockSeatEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_BLOCK_SEAT_ENABLE));
	}

	public static boolean isShowRegUserLoginInPaxDetailPage() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_REG_USER_LOGIN_IN_PAX_PAGE));
	}

	public static boolean isEnableDynamicBestOffersCaching() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DYNAMIC_BEST_OFFERS_CACHING));
	}

	public static boolean isEnableDynamicBestOffers() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DYNAMIC_BEST_OFFERS));
	}

	public static int getInventorySeatBlockDuration(UserPrincipal userPrincipal) {
		Integer salesChannel = null;
		if (userPrincipal == null) {
			salesChannel = SalesChannelsUtil.SALES_CHANNEL_WEB; // IBE user
		} else {
			salesChannel = userPrincipal.getSalesChannel();
		}

		String durationTxt = globalConfig.getBizParam(SystemParamKeys.INVENTORY_SEAT_BLOCK_DURATION).trim();
		Map<String, String> nameValuePairs = getKeyValuePair(durationTxt);

		Integer duration = null;
		if (nameValuePairs.get(salesChannel.toString()) != null) {
			duration = Integer.valueOf(nameValuePairs.get(salesChannel.toString()));
		} else { // return the default duration
			duration = Integer.valueOf(nameValuePairs.get("*"));
		}
		return duration;
	}

	public static boolean showCalFareInSelectedCurrency() {
		// TODO INTRODUCE KEY VALUE PAIRS!!
		String paramValues[] = globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_FARE_PARAM).split(DEFAULT_DATA_SEPARATOR);
		return YES.equals(paramValues[0]);
	}

	public static boolean showTotalFareInCalendar() {
		// TODO INTRODUCE KEY VALUE PAIRS!!
		String paramValues[] = globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_FARE_PARAM).split(DEFAULT_DATA_SEPARATOR);
		return YES.equals(paramValues[1]);
	}

	public static boolean showIBEPriceBreakdownInSelectedCurrency() {
		// TODO INTRODUCE KEY VALUE PAIRS!!
		String paramValues[] = globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_FARE_PARAM).split(DEFAULT_DATA_SEPARATOR);
		return YES.equals(paramValues[2]);
	}

	public static BigDecimal getIBECalBoundry() {
		// TODO INTRODUCE KEY VALUE PAIRS!!
		String paramValues[] = globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_FARE_PARAM).split(DEFAULT_DATA_SEPARATOR);
		return new BigDecimal(paramValues[3]);
	}

	public static BigDecimal getIBECalBreakpoint() {
		// TODO INTRODUCE KEY VALUE PAIRS!!
		String paramValues[] = globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_FARE_PARAM).split(DEFAULT_DATA_SEPARATOR);
		return new BigDecimal(paramValues[4]);
	}

	public static boolean isFraudCheckEnabled(int channel) {
		List<String> fraudCheckChannels = getValueList(
				globalConfig.getBizParam(SystemParamKeys.EC_FRAUD_CHK_ENABLED_CHANNELS));
		if (fraudCheckChannels.contains(channel + "")) {
			return true;
		}
		return false;
	}

	/**
	 * Return the total number of mintues
	 * 
	 * @param duration
	 *            should be in hh:mm format only
	 * @return total number of minutes
	 * @throws ModuleException
	 *             throws if hh:mm format is violated
	 */
	public static int getTotalMinutes(String duration) throws ModuleException {
		if (duration == null || duration.equals("")) {
			return 0;
		}

		String[] arrDuration = duration.split(":");
		if (arrDuration.length != 2) {
			return 0;
		}

		return (new Integer(arrDuration[0]).intValue() * 60) + new Integer(arrDuration[1]).intValue();
	}

	/**
	 * Return on hold duration
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static int getOnholdDuration() throws ModuleException {
		return getTotalMinutes(getOnHoldDuration(OnHold.AGENTS));
	}

	/**
	 * Return on hold duration within Buffer Time
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static int getOnholdDurationWithinBufferTime(String agentCode) throws ModuleException {
		return getTotalMinutes(getOnHoldDurationWithInOnHoldBufferTime(OnHold.AGENTS, agentCode));
	}

	/**
	 * Return the number of decimal points
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static int getNumberOfDecimalPoints() {
		String strDecimalPoints = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.DECIMALS));
		return new Integer(strDecimalPoints).intValue();
	}

	/**
	 * Whether or not to show credit card information in itinerary always
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static boolean showCreditCardInfoInItineraryAlways() throws ModuleException {
		return YES.equals(PlatformUtiltiies
				.nullHandler(globalConfig.getBizParam(SystemParamKeys.SHOW_CC_INFO_IN_ITINERARY_ALWAYS)));
	}

	/**
	 * Send SMS for on hold reservations
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static boolean sendSMSForOnholdReservations() throws ModuleException {
		return YES.equals(PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.SEND_ONHOLD_SMS)));
	}

	/**
	 * Returns cancel ond fare percentage
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static int getCancelONDFarePercentage() throws ModuleException {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.CANCEL_OND_FARE_PERCENTAGE));
	}

	/**
	 * Returns the default carrier code
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getCarrierCode() throws ModuleException {
		return globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
	}

	public static List<String> getOwnAirlineCodes() throws ModuleException {
		String carrierCodes = globalConfig.getBizParam(SystemParamKeys.AIRLINE_OWN_CARRIER_CODES);
		if (carrierCodes == null || carrierCodes.isEmpty()) {
			carrierCodes = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}
		String[] codes = carrierCodes.split(DEFAULT_DATA_SEPARATOR);
		return Arrays.asList(codes);
	}

	/**
	 * Returns modify ond fare percentage
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static int getModifyONDFarePercentage() throws ModuleException {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.MODIFY_OND_FARE_PERCENTAGE));
	}

	/**
	 * Return the check in time difference in hours
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getCheckInTimeDifference() throws ModuleException {
		return globalConfig.getBizParam(SystemParamKeys.CHECK_IN_TIME);
	}

	public static List<String> getAgentTopupEmailDestination() throws ModuleException {
		String emailsRawString = globalConfig.getBizParam(SystemParamKeys.AGENT_TOPUP_EMAIL_DESTINATION_ADDRESS);
		return new ArrayList<String>(Arrays.asList(emailsRawString.split(DEFAULT_DATA_SEPARATOR)));
	}

	public static String getAgentTopupMinAmount() throws ModuleException {
		return globalConfig.getBizParam(SystemParamKeys.AGENT_TOPUP_MIN_AMOUNT);
	}
	
	public static String getCheckInClosingTime() throws ModuleException {
		return globalConfig.getBizParam(SystemParamKeys.CHECK_IN_CLOSING_TIME);
	}

	public static boolean isIncludeStandbyPaxInPNLAndADL() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.INCLUDE_STANDBY_PAX_IN_PNL));
	}

	public static boolean isAccelAeroSpyEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_ACCELAERO_SPY_ENABLED));
	}

	public static boolean isAllowPercentageWiseModificationCharges() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.PERCENTAGE_WISE_MODIFICATION_CHARGES));
	}

	public static boolean isAllowModifySegmentDateIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_MODIFY_SEGMENT_BY_DATE));
	}

	public static boolean isAllowModifySegmentRouteIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_MODIFY_SEGMENT_BY_ROUTE));
	}

	public static Integer getOffsetFromStandardStartDayOfWeek() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.OFFSET_FROM_THE_STANDED_FIRST_DAY_OF_WEEK));
	}

	public static boolean sendCCDetailsWithPNL() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SEND_CC_DETAILS_WITH_PNL));
	}

	public static boolean isAllowAirlineCarrierPrefixForAgentsAndUsers() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.MAINTAIN_AIRLINE_CARRIER_PREFIX));
	}

	public static Map<String, String> getExtPmtServiceChargeCodeMap() {
		if (paymentServiceChargesMap == null) {
			paymentServiceChargesMap = getKeyValuePair(
					globalConfig.getBizParam(SystemParamKeys.EXTERNAL_PAYMENT_SERVICE_CHARGE_CODE));
		}
		return paymentServiceChargesMap;
	}

	public static String getAdminEmails() {
		return globalConfig.getBizParam(SystemParamKeys.ADMIN_EMAIL_ID);
	}

	public static boolean isExchangeRateAutomationEnabledInTheSystem() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.EXCHANE_RATE_AUTOMATION_ENABLED));
	}

	public static boolean isFOCMealSelectionEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.FOC_MEAL_SELECTION_ENABLED));
	}

	public static boolean isDynamicOndListPopulationEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.DYNAMIC_OND_LIST_CONFIGS, "ENABLED", null));
	}

	public static boolean appendTimestampToOndJs() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.DYNAMIC_OND_LIST_CONFIGS, "TIMESTAMP", "Y"));
	}

	public static String getDynamicOndFilePath() {
		return getStringFromConfigMap(SystemParamKeys.DYNAMIC_OND_LIST_CONFIGS, "PATH", null);
	}

	public static String getDynamicOndListUrl() {
		String url = getStringFromConfigMap(SystemParamKeys.DYNAMIC_OND_LIST_CONFIGS, "URL", null);
		if (url != null) {
			SimpleDateFormat sdFmt = new SimpleDateFormat("yyyyMMdd");
			return url + "?relversion=" + sdFmt.format(new Date());
		} else {
			return null;
		}
	}

	public static Integer getDynamicOndLookupDuration() {
		return getIntFromConfigMap(SystemParamKeys.DYNAMIC_OND_LIST_CONFIGS, "DURATION", 0);
	}

	public static boolean isPrintEmailIndItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.TICKET_PRINT_EMAIL_PAX_WISE_FORMAT));
	}

	public static boolean showCOSInItinerary() {
		// return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_COS_IN_ITINERARY));
		return false;
	}

	public static boolean showInsuranceOptionInUI() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_INSURANCE_OPTIONS_IN_UI));
	}

	public static boolean isHubBasedAlertsEnabled() {
		String hubsBasedAlert = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFY_FOR_HUBS); // Get from app
																								// parameter
		if (hubsBasedAlert != null && !hubsBasedAlert.trim().equals("")) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean isHttpsEnabledForIBE() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.HTTPS_COMPATIBILITY_MAP, "IBE", null));
	}

	public static List<INSURANCEPROVIDER> getInsuranceProvidersList() {
		List<INSURANCEPROVIDER> insuranceProviders = new ArrayList<INSURANCEPROVIDER>();
		List<String> insuranceProviderCodes = getSystemEnableInsuranceProvidersList(
				globalConfig.getBizParam(SystemParamKeys.INSURANCE_PROVIDER));

		if (insuranceProviderCodes != null && !insuranceProviderCodes.isEmpty()) {
			for (String insuranceProviderCode : insuranceProviderCodes) {
				if (INSURANCEPROVIDER.AIG.toString().equalsIgnoreCase(insuranceProviderCode)) {
					insuranceProviders.add(INSURANCEPROVIDER.AIG);
				} else if (INSURANCEPROVIDER.RAK.toString().equalsIgnoreCase(insuranceProviderCode)) {
					insuranceProviders.add(INSURANCEPROVIDER.RAK);
				} else if (INSURANCEPROVIDER.CCC.toString().equalsIgnoreCase(insuranceProviderCode)) {
					insuranceProviders.add(INSURANCEPROVIDER.CCC);
				} else if (INSURANCEPROVIDER.ACE.toString().equalsIgnoreCase(insuranceProviderCode)) {
					insuranceProviders.add(INSURANCEPROVIDER.ACE);
				} else if (INSURANCEPROVIDER.TUNE.toString().equalsIgnoreCase(insuranceProviderCode)) {
					insuranceProviders.add(INSURANCEPROVIDER.TUNE);
				}
			}
		} else {
			insuranceProviders.add(INSURANCEPROVIDER.NONE);
		}

		return insuranceProviders;
	}
	
	public static String getInsurnaceLink() {
		String link = "";
		List<String> activeProviders = getSystemEnableInsuranceProvidersList(globalConfig
				.getBizParam(SystemParamKeys.INSURANCE_PROVIDER));
		if (activeProviders != null && !activeProviders.isEmpty()) {
			for (String activeProvider : activeProviders) {
				link = getStringFromConfigMap(SystemParamKeys.INSURANCE_URL, activeProvider, "", SEMI_COLON_SEPARATOR);
				if (link != null && !link.trim().isEmpty()) {
					break;
				}
			}
		}
		return link;
	}

	private static List<String> getSystemEnableInsuranceProvidersList(String providers) {
		List<String> insuranceProviderCodesList = new ArrayList<String>();
		if (providers != null) {
			String[] insProviders = providers.split(DEFAULT_DATA_SEPARATOR);
			String[] insProvider = null;
			for (String insProviderValue : insProviders) {
				insProvider = insProviderValue.split(DEFAULT_VALUE_SEPARATOR);
				if (YES.equalsIgnoreCase(insProvider[1])) {
					insuranceProviderCodesList.add(insProvider[0]);
				}
			}

		}
		return insuranceProviderCodesList;
	}

	// FIXME - remove PH
	public static boolean isApplyNewFareRule() {
		boolean isApp = false;
		String client = globalConfig.getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER);
		if ("PH".equalsIgnoreCase(client)) {
			isApp = true;
		}
		return isApp;
	}

	public static boolean enableTotalPriceQuoteForAvailableFlights() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_TOTAL_PRICE_QUOTE_FOR_AVAIL_FLIGHTS));
	}

	public static boolean isDisplayContactDetailOnPNRSummary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_CONTACT_DETAILS_ON_PNR_SUMMARY));
	}

	/**
	 * Param to get per IP OHD booking count allowed in IBE
	 * 
	 * @return
	 */
	public static int getMaxPerIpOHDBookings() {
		return getIntFromConfigMap(SystemParamKeys.IBE_ONHOLD_RESTRICTION_CONFIG, "PER_IP_COUNT", 0);
	}

	/**
	 * Param to get per flight OHD booking allowed in IBE
	 * 
	 * @return
	 */
	public static int getMaxPerFlightOHDBookings() {
		return getIntFromConfigMap(SystemParamKeys.IBE_ONHOLD_RESTRICTION_CONFIG, "PER_FLIGHT_COUNT", 0);
	}

	/**
	 * Param to get OHD booking cutoff days in IBE
	 * 
	 * @return
	 */
	public static int getOHDBookingCutoffDays() {
		return getIntFromConfigMap(SystemParamKeys.IBE_ONHOLD_RESTRICTION_CONFIG, "CUTOFF_DAYS", 0);
	}

	/**
	 * Param to get per pax OHD booking allowed in IBE
	 * 
	 * @return
	 */
	public static int getMaxPerPaxOHDBookings() {
		return getIntFromConfigMap(SystemParamKeys.IBE_ONHOLD_RESTRICTION_CONFIG, "PER_PAX_COUNT", 0);
	}

	/**
	 * Param to per pax OHD booking validation type This value can be either PAXNAME or EMAIL
	 * 
	 * @return
	 */
	public static String getOHDPaxValidationType() {
		return getStringFromConfigMap(SystemParamKeys.IBE_ONHOLD_RESTRICTION_CONFIG, "PAX_VALIDATION_TYPE", "");
	}

	/**
	 * Param on hold ImageCapcha
	 * 
	 * @return
	 */
	public static boolean isOHDImageCapcha() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ONHOLD_RESTRICTION_CONFIG, "IMAGECAPCHA", ""));
	}

	/**
	 * Holds whether the agent credit limit updation enabled or not
	 * 
	 * @return
	 */
	public static boolean isAgentCreditLimitUpdationEnabled() {
		return YES.equals(PlatformUtiltiies
				.nullHandler(globalConfig
						.getBizParam(SystemParamKeys.ADJ_AGT_CREDITLIMIT_AND_BANKGUARANTEE_WHEN_EX_RATE_CHD)));
	}

	public static String getBSPDefaultFareBasisCode() {
		return globalConfig.getBizParam(SystemParamKeys.BSP_DEFAULT_FARE_BASIS_CODE);
	}

	public static String getBSPTicketingAirlineCode() {
		return globalConfig.getBizParam(SystemParamKeys.BSP_TICKETING_AIRLINE_CODE);
	}

	public static boolean isBspPaymentsAcceptedForMCCreateReservation() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.BSP_ENABLING_PARAMS_FOR_AIRLINE, "MC_CREATE", ""));
	}

	public static boolean isBspPaymentsAcceptedForMCAlterReservation() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.BSP_ENABLING_PARAMS_FOR_AIRLINE, "MC_ALTER", ""));
	}

	public static boolean isBspPaymentsAcceptedForOCCreateReservation() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.BSP_ENABLING_PARAMS_FOR_AIRLINE, "OC_CREATE", ""));
	}

	public static boolean isBspPaymentsAcceptedForOCAlterReservation() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.BSP_ENABLING_PARAMS_FOR_AIRLINE, "OC_ALTER", ""));
	}

	/**
	 * Holds whether the agent BSP credit limit updation enabled or not
	 * 
	 * @return
	 */
	public static boolean isMaintainAgentBspCreditLimit() {
		return YES
				.equals(PlatformUtiltiies
						.nullHandler(globalConfig.getBizParam(SystemParamKeys.MAINTAIN_AGENT_BSP_CREDIT_LIMIT)));
	}

	public static String getBSPSFTPHost() {
		return getStringFromConfigMap(SystemParamKeys.BSP_SFTP_CREDENTIALS, "HOST", "", SEMI_COLON_SEPARATOR);
	}

	public static String getBSPSFTPUserName() {
		return getStringFromConfigMap(SystemParamKeys.BSP_SFTP_CREDENTIALS, "USERNAME", "", SEMI_COLON_SEPARATOR);
	}

	public static String getBSPSFTPPassword() {
		return getStringFromConfigMap(SystemParamKeys.BSP_SFTP_CREDENTIALS, "PASSWORD", "", SEMI_COLON_SEPARATOR);
	}

	public static String getBSPOperationStatus() {
		return getStringFromConfigMap(SystemParamKeys.BSP_OPERATION_STATUS_AND_COUNTRY_CODE, "OP_STAT", "", SEMI_COLON_SEPARATOR);
	}

	public static Map<String, Map<String, String>> getBSPSFTPConfig() {
		List<Map<String, String>> countryWiseConfigs = getKeyValueList(
				globalConfig.getBizParam(SystemParamKeys.BSP_SFTP_CREDENTIALS));
		Map<String, Map<String, String>> configMap = new HashMap<String, Map<String, String>>();
		for (Map<String, String> countryConfig : countryWiseConfigs) {
			String country = countryConfig.get(BSP_FTP.COUNTRY.toString());
			configMap.put(country, countryConfig);
		}
		return configMap;
	}

	public static String getBSPTestCountryCode() {
		return getStringFromConfigMap(SystemParamKeys.BSP_OPERATION_STATUS_AND_COUNTRY_CODE, "COUNTRY_CODE", "",
				SEMI_COLON_SEPARATOR);
	}

	public static String getBSPRETFileVersion() {
		return getStringFromConfigMap(SystemParamKeys.BSP_OPERATION_STATUS_AND_COUNTRY_CODE, "RET_VERSION", "",
				SEMI_COLON_SEPARATOR);
	}

	public static String getHubAirport() {
		return globalConfig.getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
	}

	public static boolean isBSPEnhancedUserActivated() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.BSP_ENHANCED_USER_ENABLED));
	}

	public static boolean isAgentWiseTicketEnable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.AGENT_WISE_TICKET_ENABLE));
	}

	/**
	 * @return : true if XBE availability search allows booking class selection. false otherwise.
	 */
	public static boolean isBCSelectionAtAvailabilitySearchEnabled() {
		return YES.equals(
				PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.BC_SELECTION_AT_AVAILABILITY_SEARCH)));
	}

	public static boolean isXBETrackingEnable() {
		return YES.equalsIgnoreCase(
				getStringFromConfigMap(SystemParamKeys.GOOGLE_ANALYTICS_XBE, "ENABLED", null, SEMI_COLON_SEPARATOR));
	}

	public static String getGoogleTrackingId() {
		return getStringFromConfigMap(SystemParamKeys.GOOGLE_ANALYTICS_XBE, "TRACKINGID", null, SEMI_COLON_SEPARATOR);
	}

	/*
	 * IBE Modification Restriction parameters
	 * 
	 * IBE_62 : 'restrictionEnabled=Y;allowedChannelCodes=4,15'
	 */
	public static boolean isIBEModificationRestrictionEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_MODIFICATION_RESTRICTION_ENABLED, "restrictionEnabled", null,
				SEMI_COLON_SEPARATOR));
	}

	public static List<String> allowedChannelCodesForIBEModification() {
		String channelStr = getStringFromConfigMap(SystemParamKeys.IBE_MODIFICATION_RESTRICTION_ENABLED, "allowedChannelCodes",
				null, SEMI_COLON_SEPARATOR);
		List<String> channelList = getValueList(channelStr);
		if (channelList.size() > 0) {
			return channelList;
		}
		return null;
	}

	public static boolean isIBEModificationAllowedForDry() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_MODIFICATION_RESTRICTION_ENABLED, "allowDry", null,
				SEMI_COLON_SEPARATOR));
	}

	public static boolean isIBEModificationAllowedForInterline() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_MODIFICATION_RESTRICTION_ENABLED, "allowInterline", null,
				SEMI_COLON_SEPARATOR));
	}

	public static boolean isEndorsementsEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENDORSEMENTS_ENABLED));
	}

	public static boolean isCountryPaxConfigEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.COUNTRY_PAX_CONFIG_ENABLED));
	}

	public static boolean isShowFareDefInDepAirPCurr() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FARE_DEF_BY_DEP_COUNTRY_CURRENCY));
	}

	public static boolean isUpdateTaxWithROEChanges() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.UPDATE_TAX_AMOUNT_WITH_ROE_CHANGES));
	}

	public static boolean isShowInfantETicketInPNLADL() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_INFANT_E_TKT_IN_PNL_ADL));
	}

	public static boolean isShowFareAmtsInItineraryChargesDisplay() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FARE_AMTS_IN_ITINERARY_CHARGES_DISPLAY));
	}

	public static boolean isAllowEmailsToAgentsInFlightReprotect() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_EMAIL_AGENTS_FLIGHT_REPROTECT));
	}

	public static boolean isDescriptiveInvenoryAuditEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DESCRIPTIVE_INVENTORY_AUDIT_ENABLED));
	}

	public static boolean isCancelOutOfSequnceSegments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CANCEL_OUT_OF_SEQUNCE_SEGMENTS));
	}

	public static boolean isModificationFilterEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.MODIFICATION_FILTER_ENABLE));
	}

	public static int getMinimumSeatsCutOverToShowInIbe() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.MINIMUM_NUMBER_OF_SEATS_TO_SHOW_IN_IBE));
	}

	/*
	 * ReantACar dynamic url Related parameters
	 * 
	 * AIRLINE_106 : 'rentACarDynamicURL=Y;clientId=99999
	 */
	public static boolean isRentACarLinkDynamic() {
		return YES.equals(
				getStringFromConfigMap(SystemParamKeys.RENT_A_CAR_DYNAMIC_URL, "rentACarDynamicURL", null, SEMI_COLON_SEPARATOR));
	}

	public static String getRentACarClientId() {
		return getStringFromConfigMap(SystemParamKeys.RENT_A_CAR_DYNAMIC_URL, "clientId", null, SEMI_COLON_SEPARATOR);
	}

	public static String getRentACarAncillaryClientId() {
		return getStringFromConfigMap(SystemParamKeys.RENT_A_CAR_DYNAMIC_URL, "anciClientId", null, SEMI_COLON_SEPARATOR);
	}

	public static String getRentACarManageBookingClientId() {
		return getStringFromConfigMap(SystemParamKeys.RENT_A_CAR_DYNAMIC_URL, "modBookingClientId", null, SEMI_COLON_SEPARATOR);
	}

	public static boolean isHotelLinkLoadWithinFrame() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.HOTEL_LINK_LOAD_WITHIN_FRAME));
	}

	public static boolean isDataTransferToSageEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DATA_TRANSFER_TO_SAGE));
	}

	/*
	 * Void Reservations Related parameters
	 * 
	 * AIRLINE_99 : 'voidEnabled=Y;numberOfHoursAfterBooking=24;numberOfHoursBeforeDeparture=8;refundNonRefundables=N'
	 */
	public static boolean isVoidReservationsEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.VOID_RESERVATIONS, "voidEnabled", null, SEMI_COLON_SEPARATOR));
	}

	public static int getNumberOfHoursAfterBookingForVoidReservations() {
		return Integer.parseInt(getStringFromConfigMap(SystemParamKeys.VOID_RESERVATIONS, "numberOfHoursAfterBooking", null,
				SEMI_COLON_SEPARATOR));
	}

	public static boolean isVoidReservationsRefundNonRefundablesEnabled() {
		return YES.equals(
				getStringFromConfigMap(SystemParamKeys.VOID_RESERVATIONS, "refundNonRefundables", null, SEMI_COLON_SEPARATOR));
	}

	public static int getNumberOfHoursBeforeDepartureForVoidReservations() {
		return Integer.parseInt(getStringFromConfigMap(SystemParamKeys.VOID_RESERVATIONS, "numberOfHoursBeforeDeparture", null,
				SEMI_COLON_SEPARATOR));
	}

	public static boolean isTicketValidityEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_TICKET_VALIDITY));
	}

	public static boolean isDBMessageSynchronise() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.DASHBOARD_MSG_SYNC, "ENABLE_SYNC", null, SEMI_COLON_SEPARATOR));
	}

	public static String getDBMessageSyncTime() {
		return getStringFromConfigMap(SystemParamKeys.DASHBOARD_MSG_SYNC, "SYNC_TIME_IN_MIN", null, SEMI_COLON_SEPARATOR);
	}

	public static boolean isModifyFQOnInitialSegmentFQDate() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.MODIFY_QUOTE_ON_ORG_SEG_FAREQUOTE_DATE));
	}

	public static int getDefaultTicketValidityPeriodInMonths() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.DEFAULT_TICKET_VALIDITY_MONTHS));
	}

	/* Void Reservations Related parameters end */

	public static boolean isUpdateToFlownEvenBalPayExists() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.UPDATE_TO_FLOWN_EVEN_BAL_PAY_EXISTS));
	}

	// JIRA 6929
	public static boolean isGroupChargesByReportingChargeGroupCodeEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.GROUP_CHARGES_BY_REPORTING_CHARGE_GROUP_CODE_ENABLED));
	}

	public static boolean isAllCoSChangeFareEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALL_CC_FARE_CHANGE));
	}

	public static boolean allowOnlyAvailableFixedSeatsAndSkipStdAndNstdSeats() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_ONLY_AVAILABLE_FIXED_SEATS_AND_SKIP_STD_NONSTD_SEATS));
	}

	public static boolean transferSementsOnFareOnd() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.TRANSFER_SEG_ON_OND_FARE));
	}

	public static boolean isSplitFareSalesValidityEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SPLIT_FARE_SALES_VALIDITY));
	}

	public static boolean isShowFwdBookingReportAdditionalInfo() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FWD_BOOK_REPORT_ADDITIONAL_DETAIL));
	}

	public static boolean isBankGuaranteeNotificationEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.BANK_GUARANTEE_NOTIFICATION_EMAIL_STATUS));
	}

	public static List<String> getBankGuaranteeExpiryNotificationIntervals() {
		return getValueList(globalConfig.getBizParam(SystemParamKeys.BANK_GUARANTEE_EXPIRY_NOTIFY_INTERVALS));
	}

	public static List<Pair<Double, Double>> getCreditLimitUtilizationPercentagePairs() {
		return getDoublePairOfValuesFromString(globalConfig.getBizParam(SystemParamKeys.CREDIT_LIMIT_UTILIZATION_PERCENTAGES),
				DEFAULT_DATA_SEPARATOR);
	}

	public static boolean isRBDPNLADLEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_RBD_PNL_ADL));
	}

	public static String getSplitAdultInfantRatio() {
		return globalConfig.getBizParam(SystemParamKeys.SPLIT_ADULT_INFANT_RATIO);
	}

	public static boolean isShowFlightStopOvrDurInfo() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FLT_STOP_OVER_DETAIL));
	}

	public static boolean isHideAllocateScreenExtraDetails() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.HIDE_ALLOCATE_SCREEN_EXTRA_DETAIL));
	}

	public static boolean isApplyBookingCnxModChargesForCancelFlights() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_BOOKING_CNX_MOD_CHARGES_FOR_CANCEL_FLIGHTS));
	}

	public static boolean isApplyModChargeForSameFlightBookingModifications() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_MOD_CHARGE_FOR_SAME_FLIGHT_BOOKING_MODIFICATIONS));
	}

	public static boolean isDCSConnectivityEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DCS_CONNCECTIVITY_ENABLED));
	}

	public static String[] getDCSConnectivityCredentials() {
		String text = PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.DCS_CONNCECTIVITY_CREDENTIALS));
		Map<String, String> credentailsMap = getKeyValuePair(text);

		if (credentailsMap.size() == 2) {
			return credentailsMap.values().toArray(new String[] {});
		} else {
			return new String[] { "", "" };
		}
	}

	public static boolean isLogicalCabinClassInventoryRestricted() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.LOGICAL_CABIN_CLASS_INVENTORY_RESTRICTED));
	}

	public static boolean enableFareDiscountCodes() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FARE_DISCOUNT_CODE_LIST));
	}

	public static boolean enableMultipleGSAsForAStation() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_MULTIPLE_GSA_FOR_A_STATION));
	}

	public static boolean isUpdateEticketStatusWithPFS() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.PFS_PROCESS_CONFIG, "UPDATE_ETICKET_STATUS", null));
	}

	public static boolean isProcessNorecWithPFS() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.PFS_PROCESS_CONFIG, "PROCESS_NOREC", null));
	}

	public static boolean isShowPSPTFilter() {
		return YES
				.equals(getStringFromConfigMap(SystemParamKeys.FLIGHT_MANIFEST_WINDOW_DISPLAY_OPTIONS, "SHOW_PSPT_FILTER", null));
	}

	public static boolean isShowDOCSFilter() {
		return YES
				.equals(getStringFromConfigMap(SystemParamKeys.FLIGHT_MANIFEST_WINDOW_DISPLAY_OPTIONS, "SHOW_DOCS_FILTER", null));
	}

	public static boolean isSkipNoShowSegmentCancelCharge() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_NO_SHOW_SEGMENT_CNX_CHARGE));
	}

	public static boolean isEnableAircraftCapacityDowngradeToDifferentCC() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AIRCRAFT_CAPACITY_DOWNGRADE_TO_DIFFERENT_CC));
	}

	public static boolean isSendAgentEmailOnReservationModified() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.EMAIL_AGENT_ON_RESERVATION_MODIFICATION));
	}

	public static boolean isLoadReservationOrderByPaxName() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.LOAD_RESERVATION_ORDER_BY_PAX_NAME));
	}

	public static boolean isRequoteEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.REQUOTE_ENABLED));
	}

	public static boolean isSkipSalesChannelCheckForFlown() {
		if (AppSysParamsUtil.isRequoteEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_SALES_CHANNEL_CHECK_FOR_FLOWN));
		}
		return false;
	}

	public static boolean isIBEPaxCreditAutoRefundEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ENABLE_PAX_CREDIT_AUTO_REFUND_FOR_CC));
	}

	public static boolean isShowFacebook() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ENABLE_SOCIAL_NETWORKS_FOR_IBE, "SHOW_FACE_BOOK", null));
	}

	public static boolean isShowTwitter() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ENABLE_SOCIAL_NETWORKS_FOR_IBE, "SHOW_TWITTER", null));
	}

	public static boolean isShowGooglePlus() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ENABLE_SOCIAL_NETWORKS_FOR_IBE, "SHOW_GOOGLE_PLUS", null));
	}

	public static boolean isShowLinkedln() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ENABLE_SOCIAL_NETWORKS_FOR_IBE, "SHOW_LINKEDLN", null));
	}

	public static boolean isShowInlineSocialFeeds() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ENABLE_SOCIAL_NETWORKS_FOR_IBE, "SHOW_INLINE", null));
	}

	public static String getFacebookDialogFeedAppId() {
		return getStringFromConfigMap(SystemParamKeys.IBE_LIVE_STREAM_FACEBOOK_APP_CONFIGS, "APP_ID", null);
	}

	public static boolean isShowDetailedInfoInFacebook() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_LIVE_STREAM_FACEBOOK_APP_CONFIGS, "SHOW_LONG_NAME", null));
	}

	public static String getMarketingPopUpDelay() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_MARKETING_POP_UP_DELAY);
	}

	public static boolean isLoadPaxNamesInBlockLetters() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.LOAD_PAX_NAMES_IN_BLOCK_LETTERS));
	}

	public static boolean isCheckDuplicatePassportNumberOnlyWithinAdults() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DUPLICATE_PASSPORT_NUMBER_CHECK_ONLY_WITHIN_ADULT));
	}

	public static boolean isFlexiFareEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_FLEXI_FARES));
	}

	// TODO Re-factor all references
	public static String getSystemDefaultLanguage() {
		return globalConfig.getBizParam(SystemParamKeys.DEFAULT_LANGUAGE);
	}

	public static boolean isAutomaticSeatAssignmentEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.AUTO_SEAT_ASSIGNMENT));
	}

	public static String getAutoSeatAssignmentStartingRowNumberForIBE() {
		return getStringFromConfigMap(SystemParamKeys.SEAT_AUTO_ASSIGNMENT_CONFIGS, ApplicationEngine.IBE.toString(), null);
	}

	public static String getAutoSeatAssignmentStartingRowNumberForXBE() {
		return getStringFromConfigMap(SystemParamKeys.SEAT_AUTO_ASSIGNMENT_CONFIGS, ApplicationEngine.XBE.toString(), null);
	}

	public static boolean isEnableInterlineSupportForOTAWebServices() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_INTERLINE_SUPPORT_FOR_OTA_WEBSERVICES));
	}

	public static boolean isShowBCWiseOverBookingInInventory() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_BC_OVERBOOK_IN_INVENTORY));
	}

	public static boolean isWeeklyInvoiceEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_WEEKLY_INVOICE));
	}

	public static int getDayNumberToGenerateInvoices() {
		// Set the default to Monday
		int dayNum = 2;

		int configDayNum = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.DAY_NUMBER_TO_GENERATE_WEEKLY_INVOICE));
		if (configDayNum > 0 && configDayNum < 8) {
			dayNum = configDayNum;
		}

		return dayNum;
	}

	public static FLOWN_FROM getFlownCalculateMethod() {
		return FLOWN_FROM.getEnum(globalConfig.getBizParam(SystemParamKeys.FLOWN_CALCULATE_METHOD));
	}

	public static boolean isAllowReturnParticipateInHalfReturn() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_RETURN_PARTICIPATE_IN_HALF_RETURN));
	}

	public static int getMaximumFlightNumberLength() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.MAX_FLIGHT_NUMBER_LENGTH));
	}

	public static boolean isRequoteAutoFareQuoteEnabled() {
		return YES.equals(getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.REQUOTE_AUTO_FAREQUOTE))
				.get(RQ_AUTO_FQ.ENABLE.toString()));
	}

	public static boolean isRequoteAutoFlightSelectionEnabled() {
		return YES.equals(getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.REQUOTE_AUTO_FAREQUOTE))
				.get(RQ_AUTO_FQ.AUTO_FLT_SELECT.toString()));
	}

	public static boolean isRequoteAutoAddOndFareQuoteEnabled() {
		return YES.equals(getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.REQUOTE_AUTO_FAREQUOTE))
				.get(RQ_AUTO_FQ.AUTO_ADD_FQ.toString()));
	}

	public static boolean isRequoteAutoCancelOndFareQuoteEnabled() {
		return YES.equals(getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.REQUOTE_AUTO_FAREQUOTE))
				.get(RQ_AUTO_FQ.AUTO_CNX_FQ.toString()));
	}

	public static boolean isSkipCancelChargeForFlown() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_CNX_CHARGE_FLOWN_SEG_EXISTS));
	}

	/**
	 * Retrieves the app_parameter and returns true if the value is 'Y'. Else returns false
	 */
	public static boolean isShowFareRuleInfoInSummary() {
		return YES.equals(String.valueOf(globalConfig.getBizParam(SystemParamKeys.SHOW_FARE_RULES_IN_QUOTE_SUMMARY)));
	}

	public static boolean isHideBaseCurrencyDecimalsInItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.HIDE_BASE_CURRENCY_DECIMALS_IN_ITINERARY));
	}

	/**
	 * Retrieves the app_parameter and returns true if the value is 'Y'. Else returns false
	 */
	public static boolean isHideTypeOfPaymentInAgentSettlement() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.TYPE_OF_PAYMENT_FROM_AGENT_SETTLEMENT));
	}

	public static boolean displayTimeUsingPersianFormat(String selectedLang) {

		String appParamFltTypes = globalConfig.getBizParam(SystemParamKeys.DISPLAY_TIME_USING_PERSIAN_LANGUAGE_AND_FORMAT);
		String[] values = appParamFltTypes.split(SEMI_COLON_SEPARATOR);

		for (String value : values) {
			String[] langCodes = value.split(DEFAULT_VALUE_SEPARATOR);
			if (langCodes[1].equals(selectedLang)) {
				return values[0].split(DEFAULT_VALUE_SEPARATOR)[1].equals("Y");
			}
		}
		return false;
	}

	/**
	 * Returns the loaded email address from the database.
	 */
	public static String getAlternativeEmailForXBE() {
		return globalConfig.getBizParam(SystemParamKeys.ALTERNATIVE_EMAIL_FOR_XBE);
	}

	/**
	 * Returns whether user notes are mandatory or not in XBE.
	 */
	public static boolean isUserNoteMandatory() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.USER_NOTE_MANDATORY));
	}

	public static boolean isCancelNoShowSegment() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CANCEL_NOSHOW_SEGMENT));
	}

	public static boolean displayOnlyAllocatedLogicalCabinClasses() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_ONLY_ALLOCATED_LOGICAL_CABIN_CLASSES));
	}

	public static boolean skipInvChkForFlownInRequote() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_INV_CHK_FOR_FLOWN_IN_REQUOTE));
	}

	public static boolean setBCForExistingSegments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SET_BC_FOR_EXISTING_SEGMENTS));
	}

	public static boolean isShowFarebasisCodesInCompanyPayments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_FAREBASIS_CODES_IN_COMPANY_PAYMENTS));
	}

	public static boolean isJNTaxApplyForCnxModNoShowCharges() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_JN_TAX_APPLY_FOR_CNX_MOD_NOSHOW_CHARGES));
	}

	public static String getDefaultDateFormatForXBE() {
		return globalConfig.getBizParam(SystemParamKeys.DEFAULT_DATE_FORMAT_FOR_XBE);
	}

	public static String getServiceTaxLabel() {
		return globalConfig.getBizParam(SystemParamKeys.SERVICE_TAX_LABEL);
	}

	public static String getTaxRegistrationNumberLabel() {
		return globalConfig.getBizParam(SystemParamKeys.TAX_REGISTRATION_NUMBER_LABEL);
	}


	public static String getTaxRegistrationNumberLabelForIBE() {
		return globalConfig.getBizParam(SystemParamKeys.TAX_REGISTRATION_NUMBER_LABEL_FOR_IBE);
	}

	public static boolean isAllowSetOriginUserAsSchedulerForGoShowBookings() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SET_ORIGIN_USER_AS_SCHEDULER_FOR_GOSHOW_BOOKINGS));
	}

	public static boolean isDomesticFareDiscountEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DOMESTIC_FARE_DISCOUNT));
	}

	public static int getDomesticFareDiscountPercentage() throws ModuleException {
		if (AppSysParamsUtil.isDomesticFareDiscountEnabled()) {
			return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.DOMESTIC_FARE_DISCOUNT_PERC));
		}
		return 0;
	}

	public static String isToNotifyAnOfficial() {
		String appParamFltTypes = globalConfig.getBizParam(SystemParamKeys.IS_TO_NOTIFY_AN_OFFICIAL);
		String[] values = appParamFltTypes.split(SEMI_COLON_SEPARATOR);
		String enabledFlag = values[0].split(DEFAULT_VALUE_SEPARATOR)[1];

		return enabledFlag;
	}

	public static boolean isAutoCancellationEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.AUTO_CANCELLATION_ENABLED));
	}

	public static boolean isAllowMulticitySearchForAllXbeUsers() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_MULTICITY_SEARCH_FOR_ALL_XBE_USERS));
	}

	public static String getAutoCancellationTriggerTime() {
		return globalConfig.getBizParam(SystemParamKeys.AUTO_CANCELLATION_TRIGGER_TIME);
	}

	public static int getAutoCancellationTriggerTimeInMunites() throws ModuleException {
		return getTotalMinutes(getAutoCancellationTriggerTime());
	}

	public static String getAutoCancellationTriggerTimeInBuffer() {
		return globalConfig.getBizParam(SystemParamKeys.AUTO_CANCELLATION_TRIGGER_DURING_BUFFER);
	}

	public static int getAutoCancellationTriggerTimeInBufferInMunites() throws ModuleException {
		return getTotalMinutes(getAutoCancellationTriggerTimeInBuffer());
	}

	public static boolean isAutoCancellationTriggerTimeFromFlightDepartureTime() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.AUTO_CANCELLATION_TRIGGER_TIME_FROM_FLIGHT_DEPARTURE_TIME));
	}

	public static boolean isEnableBaggageForStandby() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_BAGGAE_SELECTION_FOR_SBY_BOOKINGS));
	}

	public static boolean isEnableSSRForStandby() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SSR_SELECTION_FOR_SBY_BOOKINGS));
	}

	public static boolean showPaymentMethodsOnIBE() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.SHOW_PAYMENT_METHODS_ICONS, "Enable_IBE", "N"));
	}

	public static Set<String> getDefaultPaymentMethods() {
		String defaultPaymentMethodsText = getStringFromConfigMap(SystemParamKeys.SHOW_PAYMENT_METHODS_ICONS, "Static_Icons", "");
		if (defaultPaymentMethodsText != null) {
			defaultPaymentMethodsText = defaultPaymentMethodsText.replaceAll("[{}]", "").replaceAll(";", ",");
		}
		return new HashSet<String>(getValueList(defaultPaymentMethodsText));
	}

	public static boolean doAdjustmentForChargeRefund() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.AUTOMATIC_ADJUSTMENT_FOR_TAX_REFUND));
	}

	public static boolean enableIBEFakeDiscounts() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_IBE_FAKE_DISCOUNTS));
	}

	public static boolean showChargesPaymentsInItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_CHARGES_PAYMENTS_IN_ITINERARY));
	}

	public static boolean isPromotionModuleEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.PROMOTION_MODULE_ENABLED));
	}

	public static int getNextSeatFreePromoSeatRowsToSkip() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.PROMOTIONS_NEXT_SEAT_FREE_ROWS_TO_SKIP));
	}

	public static String getNextSeatFreeApprovalConfigs() {
		return globalConfig.getBizParam(SystemParamKeys.PROMOTION_NEXT_SEAT_FREE_APPROVAL_CONF);
	}

	public static String getFlexiDatePromotionApproveCutOffTime() {
		return globalConfig.getBizParam(SystemParamKeys.PROMOTION_FLEXI_DATE_APPROVAL_CUTOFF);
	}

	public static String getNextSeatPromotionMailNotifierCutOffTime() {
		return getStringFromConfigMap(SystemParamKeys.NEXT_SEAT_FREE_FLEXI_DATE_NOTIFIER_CONFIGS, "NEXT_SEAT", null);
	}

	public static String getFlexidDatePromotionMailNotifierCutOffTime() {
		return getStringFromConfigMap(SystemParamKeys.NEXT_SEAT_FREE_FLEXI_DATE_NOTIFIER_CONFIGS, "FLEXI_DATE", null);
	}

	public static boolean isOnlineCheckInReminderEnable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ONLINE_CHECKIN_REMINDER));
	}

	public static String getOnlineCheckInReminderSchedulerTimeDuration() {
		return globalConfig.getBizParam(SystemParamKeys.ONLINE_CHECKIN_REMINDER_SCHEDULER_TIME_DURATION);
	}

	public static String getIBEOnlineCheckInURL() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_ONLINE_CHECK_IN_URL);
	}

	/* Server To Server Messages */
	public static boolean enableServerToServerMessages() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_SERVER_TO_SERVER_MESSAGES, "Enable_ServerToServer", "N"));
	}

	public static boolean isConfirmOnholdReservationByServerToServerMessages() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_SERVER_TO_SERVER_MESSAGES, "Confirm_ONHOLD", "N"));
	}

	public static boolean isRefundPaymentsByServerToServerMessages() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_SERVER_TO_SERVER_MESSAGES, "Refund_Payment", "N"));
	}

	public static boolean isStandbyTicketValidityShownInItinerary() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.STANDBY_TRAVEL_VALIDITY_PARAMS, "ENABLED", "N"));
	}

	public static Integer standbyTicketTravelValidityInDays() {
		return getIntFromConfigMap(SystemParamKeys.STANDBY_TRAVEL_VALIDITY_PARAMS, "DURATION", 0);
	}

	public static boolean isApplyModChargeForSameFlightHighCOSToLowCOS() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_MOD_CHARGE_FOR_SAME_FLIGHT_HCOS_TO_LCOS));
	}

	public static boolean isAllowModifyNoShowSegmentsIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ALLOW_NO_SHOW_SEGMENTS_TO_MODIFY));
	}

	public static boolean isShowAncillaryBreakDown() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_ANCILLARY_BREAKDOWN));
	}

	public static boolean isSocialLoginFromFacebookEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ALLOW_SOCIAL_LOGIN, "FB", null));
	}

	public static String getFacebookAppIdForSocialLogin() {
		return getStringFromConfigMap(SystemParamKeys.IBE_ALLOW_SOCIAL_LOGIN, "FB_APP_ID", null);
	}

	public static boolean isSocialLoginFromLinkedInEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ALLOW_SOCIAL_LOGIN, "LINKEDIN", null));
	}

	public static String getLinkedInAPIKeyForSocialLogin() {
		return getStringFromConfigMap(SystemParamKeys.IBE_ALLOW_SOCIAL_LOGIN, "LINKEDIN_API", null);
	}

	public static boolean isAllowLoadReservationsUsingPaxNameInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ALLOW_LOAD_RES_USING_PAX_NAME));
	}

	public static boolean hideExchangesCharges() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.HIDE_EXCHANGED_CHARGES));
	}

	public static boolean displayDirectFlightsFirst() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_DIRECT_FLIGHTS_FIRST));
	}

	public static boolean isSocialSeatingEnabledWithFacebook() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ALLOW_SOCIAL_SEATING, "FB", null));
	}

	public static String getFacebookAppIdForSocialSeating() {
		return getStringFromConfigMap(SystemParamKeys.IBE_ALLOW_SOCIAL_SEATING, "FB_APP_ID", null);
	}

	public static boolean isSocialSeatingEnablesWithLinkedIn() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_ALLOW_SOCIAL_SEATING, "LINKEDIN", null));
	}

	public static String getLinkedInApiIDForSocialSeating() {
		return getStringFromConfigMap(SystemParamKeys.IBE_ALLOW_SOCIAL_SEATING, "LINKEDIN_API", null);
	}

	public static boolean isFareAdjustmentEnabledForModification() {
		if (AppSysParamsUtil.isRequoteEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_FARE_ADJUSTMENT_FOR_MODIFICATION));
		}
		return false;
	}

	public static Map<String, String> getGDSUsers() {
		String gdsDetails = globalConfig.getBizParam(SystemParamKeys.GDS_CONNCECTIVITY_CREDENTIALS);
		Map<String, String> gdsUsersMap = new HashMap<String, String>();

		if (YES.equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {
			gdsUsersMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.GDS_CONNCECTIVITY_CREDENTIALS));
		}
		return gdsUsersMap;
	}

	public static boolean isWaitListedReservationsEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_WAIT_LISTED_RESERVATIONS));
	}

	public static boolean isOpenJawMultiCitySearchEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_OPENJAW_MULTICITY_SEARCH));
	}

	public static boolean isAgentCommmissionEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AGENT_COMMISSION));
	}

	public static boolean isAllowNOSHOPaxAutoRefundForIBEBookings() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AUTO_REFUND_FOR_NOSHO_PAX_IN_IBE_BOOKINGS));
	}

	public static boolean isUpdateInfantStateWhenProcessParentPFS() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.UPDATE_INF_STATE_WHEN_PROCESS_PARENT_PFS));
	}

	public static boolean isAgentCreditRefundEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SET_PAX_CREDIT_AUTO_REFUNDABLE_TO_AGENTS));
	}

	public static List<String> getAgentTypesWhichAgentCreditRefundEnabled() {

		return getValueList(globalConfig.getBizParam(SystemParamKeys.PAX_CREDIT_AUTO_REFUNDABLE_ALLOWED_AGENT_TYPES));
	}

	public static boolean isShowPaxNamesInUpperCaseOnFltManifest() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_PAX_NAMES_IN_UPPER_CASE_ON_FLT_MANIFEST));
	}

	public static boolean allowAddInsurnaceForInfants() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ADD_INSURANCE_FOR_INFANTS));
	}

	public static boolean showAgentInfoForCashPayments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_AGENT_FOR_CASH_PAYMENTS_IN_ACC_TAB));
	}

	public static boolean isIBEAllowModifyBookingsToLowPriorityCOS() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ALLOW_MODIFY_BOOKINGS_TO_LOW_PRIORITY_COS));
	}

	public static boolean hideZeroCashPaymentRecordInXBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.HIDE_ZERO_CASH_PAYMENT_RECORD));
	}

	public static boolean isPromoCodeEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_PROMO_CODE));
	}

	public static boolean showFareMaskOptionInXBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FARE_MASK_OPTION));
	}

	public static boolean showCharterAgentInXBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_CHARTER_AGENT_OPTION));
	}

	/*
	 * return list of available promotion types sorted by priority
	 */
	public static List<String> getPromoTypesByPriority() {
		if (promoListByPriority == null) {
			promoListByPriority = new ArrayList<String>();
			Map<String, String> priorityMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.PROMO_PRIORITY));
			SortedMap<String, String> sortedMap = new TreeMap<String, String>(priorityMap);

			for (Entry<String, String> entry : sortedMap.entrySet()) {
				promoListByPriority.add(entry.getValue());
			}
		}

		return promoListByPriority;
	}

	public static boolean isShowDiscountOnItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_DISCOUNT_ON_ITINERARY));
	}

	public static List<String> enabledFareTypes() {

		String fareTypes = globalConfig.getBizParam(SystemParamKeys.SUPPORTED_FARE_TYPES);
		String[] values = fareTypes.split(DEFAULT_DATA_SEPARATOR);

		return Arrays.asList(values);
	}

	public static boolean isFareBiasingEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_FARE_BIASING_ENABLED));
	}

	public static boolean isAdjustFareDiffWhileCabinClassChange() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ADJUST_FARE_DIFFERENCE_WHILE_CABIN_CLASS_CHANGE));
	}

	public static boolean isProcessPFSAfterETLProcessed() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.PROCESS_PFS_AFTER_ETL_PROCESSED));
	}

	public static boolean isAllowHalfReturnFaresForOpenReturnBookings() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_HALF_RETURN_FARES_FOR_OPEN_RETURN));
	}

	public static boolean isNSAutoRefundHoldProcessEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_NS_AUTOREFUND_HOLDPROCESS));
	}

	public static String getNSAutoRefundHoldDuration() {
		return globalConfig.getBizParam(SystemParamKeys.NS_AUTO_REFUND_HOLD_DURATION);
	}

	public static boolean showNestedAvailableSeats() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_NESTED_AVAILABLE_SEATS));

	}

	public static boolean isOnlyGoshoFaresVisibleInFlightCutoffTime() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.VISIBLE_ONLY_GOSHOW_FARES_WITHIN_FLIGHT_CUTOFF_TIME));
	}

	public static boolean isEnableEticketStatusTransitionRecording() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_RECORDING_ETICKET_TRANSITIONS));
	}

	// TODO: AIRLINE_163 app param and the changes to be removed after completing
	// "Re-quote only for Required ONDs [AIRLINE_177]" in XBE & IBE.
	// With the AIRLINE_163 changes only segment fares can be skipped but in many of the airlines uses
	// mix of fares SEG/CON/RET and there are many combinations.
	@Deprecated
	public static boolean isSkipNonModifiedSegFareONDs() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_UNTUTCHED_SECTOR_FARES_IN_REQUOTE_FLOW));
	}

	public static boolean isVoidAutoRefundEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.AUTO_REFUND_VOID_RESERVATIONS));
	}

	public static boolean isSameFareModificationEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SAME_FARE_MODIFICATION));
	}

	public static boolean isRoundUpOverwriteFeeEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ROUND_UP_OVERWRITE_FEE));
	}

	public static boolean isAncillaryPreferenceMessageEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ANCILLARY_PREFERENCE_MESSAGE));
	}

	/*
	 * Inventory Nesting can be controlled by AIRLINE_162. Need to remove AIRLINE_178 from all DBs as well once all
	 * systems comes to this code version
	 */
	@Deprecated
	public static boolean isReturnFareNestingEnabled() {
		return AppSysParamsUtil.showNestedAvailableSeats()
				&& YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_RBD_NESTING_FOR_RETURN_FARES));
	}

	public static List<String> getBaggageSelectionCriterionByPriority() {
		return getValueList(globalConfig.getBizParam(SystemParamKeys.BAGGAGE_SELECTION_CRITERION));
	}

	public static boolean isRoundupChildAndInfantFaresWhenCalculatingFromAdultFare() {
		return YES.equals(
				globalConfig.getBizParam(SystemParamKeys.ROUNDUP_CHILD_AND_INFANT_FARES_WHEN_CALCULATING_FROM_ADULT_FARE));
	}

	public static boolean isJNTaxApplicableForAnci() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_JN_TAX_APPLICABLE_FOR_ANCI));
	}

	public static boolean isActiveActiveEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ACTIVE_ACTIVE_ENABLE));
	}

	public static boolean isReportingDbAvailable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_REPORTING_DB_AVAILABLE));
	}

	public static boolean splitPromoDiscountIntoRefundableAndNonRefundables() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SPLIT_DISCOUNT_AMOUNT_INTO_REFUND_NON_REFUND));
	}

	public static List<String> getAllowedImageFormatsForUpload() {

		if (globalConfig.getBizParam(SystemParamKeys.ALLOWED_IMAGE_FORMATS_FOR_UPLOAD) != null) {

			String agentTypes = globalConfig.getBizParam(SystemParamKeys.ALLOWED_IMAGE_FORMATS_FOR_UPLOAD);
			String[] values = agentTypes.split(DEFAULT_DATA_SEPARATOR);

			return Arrays.asList(values);
		} else {
			return new ArrayList<String>();
		}
	}

	public static boolean isReQuoteOnlyRequiredONDs() {
		if (AppSysParamsUtil.isRequoteEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_UNTUTCHED_OND_IN_REQUOTE_FLOW));
		}
		return false;
	}

	public static int getPenaltyONDFarePercentage() throws ModuleException {
		if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs()) {
			return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.PENALTY_OND_FARE_PERCENTAGE_IN_REQUOTE_FLOW));
		}
		return 0;
	}

	public static boolean showPassenegerContactDetailsInItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_PASSENGER_CONTACT_DETAILS_IN_ITINERARY));
	}

	public static boolean showStationContactDetailsInItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_STATION_CONTACT_DETAILS_IN_ITINERARY));
	}

	public static boolean enableDowngradeAircraftModel() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DOWNGRADE_AIRCRAFTMODEL));
	}

	public static boolean skipPaxTypeForPFSProcessing() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_PAX_TYPE_FOR_PFS_PROCESSING));
	}

	public static long getPassenegerNameModificationCutoffTimeInMillis() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.PASENGER_NAME_MODIFICATION_CUT_OFF_TIME));
	}

	public static boolean isAnciOfferEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ANCI_OFFER_SWITCH));
	}

	public static String getIbeBestOffersUrl() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_BEST_OFFERS_URL);
	}

	public static String getIbeSurveyUrl() {
		return globalConfig.getBizParam(SystemParamKeys.IBE_SURVEY_LINK_URL);
	}

	public static boolean isCookieBasedSurchargesApplyForIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_COOKIE_BASED_SURCHARGES_FOR_IBE));
	}

	public static boolean isSubJourneyDetectionEnabled() {
		if (AppSysParamsUtil.isRequoteEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SUB_JOURNEY_DETECTION));
		}
		return false;
	}

	public static PENALTY_BASED getPenaltyCalculateMethod() {
		return PENALTY_BASED.getEnum(globalConfig.getBizParam(SystemParamKeys.PENALTY_CALCULATE_METHOD));
	}

	public static boolean applyExtendedNameModificationFunctionality() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_EXTENDED_NAME_MODIFICATION_FUNCTIONALITY));
	}

	public static boolean isEnableExitPopupForIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_IBE_EXITPOPUP));
	}

	public static int getMaximumNumberOfExitPopupDisplayTimesPerPage() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.IBE_EXIT_POPUP_DISPLAY_TIMES_PER_PAGE));
	}

	public static int getLoginApiTokenValidityPriod() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.LOGIN_API_TOKEN_VALIDITY_PERIOD));
	}

	public static boolean isEnableFareProration() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_FARE_PRORATION));
	}

	/**
	 * Enable admin fee for all payment methods
	 * 
	 * @return
	 */
	public static boolean isAdminFeeRegulationEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ADMIN_FEE_REGULATION_ENABLED));
	}

	/**
	 * channel wise configuration to enable admin fee for all payment methods
	 * 
	 * @return
	 */
	public static boolean isAdminFeeChannelWiseConfigured(ApplicationEngine appIndicator, String agentCode) {
		Map<String, String> adminFeeEnableMap = getKeyValuePair(
				globalConfig.getBizParam(SystemParamKeys.ADMIN_FEE_CHANNEL_WISE_CONFIGURED), DEFAULT_DATA_SEPARATOR);

		if (adminFeeEnableMap != null && adminFeeEnableMap.size() > 0) {
			for (String moduleName : adminFeeEnableMap.keySet()) {
				if (moduleName.equals(appIndicator.toString())) {
					String[] paramValues = adminFeeEnableMap.get(moduleName).split(DEFAULT_DATA_SEPARATOR);
					if (agentCode != null && !"".equals(agentCode) && SPECIFIC.equals(paramValues[0])) {
						Collection<AdminFeeAgentTO> adminFeeAgentList = globalConfig.getAdminFeeAgentCachedList();
						for (AdminFeeAgentTO adminFeeAgentTO : adminFeeAgentList) {
							if (agentCode.equals(adminFeeAgentTO.getAdminFeeAgentCode())) {
								return true;
							}
						}
						return false;
					} else {
						return YES.equals(paramValues[0]);
					}
				}
			}
		}
		return false;
	}

	public static boolean isEnableUserDefineTransitTime() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_USER_DEFINE_TRANSIT_TIME));
	}

	public static boolean isMatchOfferedFareForCookieBasedSurcharges() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.MATCH_EVEN_OFFERED_FARE_TO_APPLY_COOKIE_BASED_SURCHARGES));
	}

	public static boolean isEnableTotalPriceBasedMinimumFareQuote() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.TOTAL_PRICE_BASED_MINIMUM_FARE_QUOTE));
	}

	public static boolean isOfflinePaymentEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_OFFLINE_PAYMENT_ENABLE));
	}

	public static boolean isFlightDetailsPublishingEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_FLIGHT_DETAILS_PUBLISHING));
	}

	public static boolean isCargoFlashFlightPublishingEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CARGO_FLASH_PUBLISHING));
	}

	public static boolean isAllowLoadReservationFromManifest() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_LOAD_RESERVATION_FROM_MANIFEST));
	}

	public static boolean isAllowProcessPfsBeforeFlightDeparture() {
		return globalConfig.isAllowProcessPfsBeforeFlightDeparture();
	}

	public static boolean isExternalInternationalFlightInfoCaptureEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_EXTERNAL_INTERNATIONAL_FLIGHT_CAPTURING));
	}

	public static boolean isAutoDateFillInPaxReprotectFlightSearchScreenEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AUTO_DATE_FILL_IN_PAX_REPROTECT_FLIGHT_SEARCH_SCREEN));
	}

	public static boolean isSendInwardSurfaceSegmentInPNLADL() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SEND_INWARD_SURFACE_SEGMENT_DETAILS_IN_PNLADL));
	}

	public static boolean isSendOnwardSurfaceSegmentInPNLADL() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SEND_ONWARD_SURFACE_SEGMENT_DETAILS_IN_PNLADL));
	}

	public static boolean isTicketValidityForSubJourney() {
		if (AppSysParamsUtil.isSubJourneyDetectionEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.TICKET_VALIDITY_FOR_SUB_JOURNEY_ONLY));
		}
		return false;
	}

	public static List<String> fareDiscountEnabledPaxTypes() {

		String paxTypes = globalConfig.getBizParam(SystemParamKeys.FARE_DISCOUNT_SUPPORTED_PAX_TYPES);
		String[] values = paxTypes.split(DEFAULT_DATA_SEPARATOR);

		return Arrays.asList(values);
	}

	public static boolean isRefundNonRefundableFaresInExchange() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.REFUND_NON_REFUND_FARE_IN_EXCHAGE));
	}

	public static boolean isMaintainSeperateADLTransmissionIntervalAfterCutOffTime() {
		return YES.equals(
				globalConfig.getBizParam(SystemParamKeys.MAINTAIN_SEPERATE_ADL_TRANSMISSION_REPEAT_INTERVAL_AFTER_CUTOFF_TIME));
	}

	public static boolean isReturnBucketCacheEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_RETURN_BUCKET_CACHE));
	}

	public static int getExitPopupTimeGapForDisaplay() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.IBE_EXIT_POPUP_DISPLAY_TIME_GAP));
	}

	public static int getIbeExitPopupEnableStep() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.IBE_EXIT_POPUP_ENABLE_STEP));
	}

	public static boolean isShowIbeSocialLoginInPaxPage() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_SHOW_SOCIAL_LOGIN_IN_PAX_PAGE));
	}

	public static String getIATAEMDSFormCode() {
		return globalConfig.getBizParam(SystemParamKeys.IATA_EMDS_FORM_CODE);
	}

	public static boolean isBundledFaresEnabled(String requestedSystem) {
		String paramValue = globalConfig.getBizParam(SystemParamKeys.IS_BUNDLED_FARES_ENABLED);
		if (paramValue == null || paramValue.length() == 1) {
			return YES.equals(paramValue);
		} else {
			return YES.equals(getStringFromConfigMap(SystemParamKeys.IS_BUNDLED_FARES_ENABLED, requestedSystem, null));
		}
	}

	public static boolean isFlexiEnabledInAnci() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_FLEXI_ENABLE_IN_ANCI));
	}

	public static Integer getFare_Valid_Till() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.Fare_Valid_Till));
	}

	public static Integer getTarget_Payment_Date() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.Target_Payment_Date));
	}

	public static boolean isJnTaxApplicableForHandlingCCAdjustmentCharges() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_JN_TAX_APPLICABLE_FOR_HANDLING_CC_ADJ_CHARGES));
	}

	public static String getDefaultSelectedCOSInAiradminSetupFareScreen() {
		return globalConfig.getBizParam(SystemParamKeys.DEFAULT_SELECTED_COS_AIRADMIN_SETUP_FARE_SCREEN);
	}

	public static boolean isConnRouteOnDMergeEnabled() {
		if (AppSysParamsUtil.isRequoteEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CONN_ROUTE_OND_MERGE));
		}
		return false;
	}

	public static boolean isCalendarViewEnabledForIBERequote() {
		if (AppSysParamsUtil.isRequoteEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_CALENDAR_ENABLED_FOR_REQUOTE));
		}

		return false;
	}

	public static boolean isValidateConnectionFlightForOpenJaw() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CONN_FLIGHT_VALIDATION_FOR_OPEN_JAW));
	}

	public static boolean isConsiderBCConfigForInfantSurcharge() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CONSIDER_BC_CONFIG_FOR_INFANT_SURCHARGE));
	}

	public static boolean isMarkFlightPnrFlownAutomatically() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AUTO_FLOWN_PROCESS));
	}

	public static boolean isReturnFromSameCountryDoubleOpenJaw() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.VALIDATE_RET_FROM_SAME_COUNTRY_DOUBLE_OPEN_JAW));
	}

	public static boolean isReturnFromSameCountrySingleOpenJaw() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.VALIDATE_RET_FROM_SAME_COUNTRY_SINGLE_OPEN_JAW));
	}

	public static Set<String> getPaxStatusForAutoFlown() {
		if (paxStatusForAutoFlown == null) {
			if (isMarkFlightPnrFlownAutomatically()) {
				paxStatusForAutoFlown = new HashSet<String>(
						getValueList(globalConfig.getBizParam(SystemParamKeys.AUTO_FLOWN_PAX_STATUS)));
			} else {
				paxStatusForAutoFlown = new HashSet<String>();
			}
		}
		return paxStatusForAutoFlown;
	}

	public static boolean isApplyPenaltyWithinValidity() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_PENALTY_WITHIN_VALIDITY));
	}

	public static boolean isCheckPrivilegeForFlownFlightsClosedBC() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FLOWN_FLIGHT_CLOSED_BC));
	}

	public static boolean isAutoUpdateGroundSegStateWhenAirSegFlown() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.UPDATE_GROUND_SEG_STATUS_WHEN_AIR_SEG_IS_FLOW_IN_PFS_PROCESS));
	}

	public static String getNomialCodesSupportsForReporting() {
		String nomialCodesForRET = globalConfig.getBizParam(SystemParamKeys.NOMINAL_CODES_RELATED_TO_REPORT_FILE);
		return nomialCodesForRET;
	}

	public static String getRetFileAgentTypes() {
		String agentTypeForRET = globalConfig.getBizParam(SystemParamKeys.RAPID_RET_FILE_AGENT_TYPES);
		return agentTypeForRET;
	}

	public static String getHotFileAgentTypes() {
		String agentTypeForHOT = globalConfig.getBizParam(SystemParamKeys.RAPID_HOT_FILE_AGENT_TYPES);
		return agentTypeForHOT;
	}

	public static boolean isRoundupChargesWhenCalculatedInBaseCurrency() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ROUNDINGUP_CHARGES_CALCULATED_IN_BASE_CURRENCY));
	}

	public static boolean isSystemChargeAdjustmentForDueAmountEnabled() {
		if (AppSysParamsUtil.isRequoteEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CHARGE_ADJ_FOR_DUE_AMOUNT));
		}
		return false;
	}

	public static boolean isAllowHalfReturnFaresForOpenJawJourney() {
		if (AppSysParamsUtil.isOpenJawMultiCitySearchEnabled()) {
			return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_HALF_RETURN_FARES_FOR_OPEN_JAW));
		}
		return false;
	}

	public static boolean isSplitFareImmediateEffectEnable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SPLIT_FARE_SALES_VALIDITY_IMMEDIATE_EFFECT));
	}

	public static int getSplitFareImmediateEnableCutofTime() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.SPLIT_FARE_SALES_VALIDITY_IMMEDIATE_EFFECT_CUTOFF_TIME));
	}

	public static int getMulticitySearchMaximumNoONDIBE() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.MULTICITY_ALLOWED_MAXIMUM_NO_OND_IBE));
	}

	public static int getMulticitySearchMaximumNoONDXBE() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.MULTICITY_ALLOWED_MAXIMUM_NO_OND_XBE));
	}

	public static int getMaxDepartureReturnVarience(AppIndicatorEnum app) {
		int varience = 0;
		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(app)) {
			varience = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_VARIANCE_IBE));
		} else if (app.equals(AppIndicatorEnum.APP_XBE)) {
			varience = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_VARIANCE_XBE));
		}
		return varience;
	}

	public static boolean restrictModificationForCancelledOrTotallyFlownBookings() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.RESTRICT_MOD_CNX_FLOWN_BOOKINGS));
	}

	public static int getNoOfBackDaysToReconcileCardPayments() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.NO_OF_BACK_DAYS_TO_RECONCILE_CARD_PAYMENTS));
	}

	public static String getEnableOfflineReportParams() {
		return globalConfig.getBizParam(SystemParamKeys.ENABLE_OFFLINE_REPORT_FOR_PAST_DATES);
	}

	public static boolean isOHDPaymentEamilEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_OHD_PAYMENT_REMINDER, "ENABLED", "N"));
	}

	public static long getPublicReservationCutoverInMillisForDomestic() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.PUBLIC_RESERVATION_CUTOFF_TIME_FOR_DOMESTIC));
	}

	public static boolean isDeliveryMethodDisplayEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_DELIVERY_METHOD_IN_SITA_CONFIGURAION_SCREEN));
	}

	public static boolean addMobileAreaCodePrefix() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_MOBILE_AREA_CODE_PREFIX));
	}

	public static boolean skipAnciModificationCutoffForNorec() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_ANCI_MODIFICATION_CUTOFF_FOR_NOREC));
	}

	public static boolean applyFQTVInAllSegments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_FQTV_FOR_ALL_SEGMENTS));
	}

	public static boolean isOHDPaymentEamilReminderEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.IBE_OHD_PAYMENT_REMINDER, "REMINDER", "N"));
	}

	public static Integer getOHDPaymentReminderDuration() {
		return getIntFromConfigMap(SystemParamKeys.IBE_OHD_PAYMENT_REMINDER, "REMIND_BEFORE_MINS", 0);
	}

	public static String getPaymentPartnerURL() {
		return getStringFromConfigMap(SystemParamKeys.IBE_OHD_PAYMENT_REMINDER, "URL", null);
	}

	public static boolean isIBEHumanVerficationEnabled() {
		return YES.equals(
				getStringFromConfigMap(SystemParamKeys.IBE_HUMAN_VERIFICATION_PARAMS, HumanVerificationConstants.ENABLED, null,
						SEMI_COLON_SEPARATOR));
	}

	public static String getIPHitCountHeaderName() {
		return getStringFromConfigMap(SystemParamKeys.IBE_HUMAN_VERIFICATION_PARAMS, HumanVerificationConstants.HEADER_NAME, null,
				SEMI_COLON_SEPARATOR).trim();
	}

	public static int maxPerIPHitCount() {
		return Integer.parseInt(
				getStringFromConfigMap(SystemParamKeys.IBE_HUMAN_VERIFICATION_PARAMS, HumanVerificationConstants.MAX_IP_HITS,
						null, SEMI_COLON_SEPARATOR));
	}

	public static int maxPerSessionHitCount() {
		return Integer.parseInt(getStringFromConfigMap(SystemParamKeys.IBE_HUMAN_VERIFICATION_PARAMS,
				HumanVerificationConstants.MAX_SESSION_HITS, null, SEMI_COLON_SEPARATOR));
	}

	public static boolean isLCCDataSyncEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_LCC_DATA_SYNC));
	}

	public static boolean isEmailDomainValidationEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.EMAIL_DOMAIN_VALIDATION, "ENABLED", "N"));
	}

	public static boolean isValidateDomainAddressInEmail() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.EMAIL_DOMAIN_VALIDATION, "VALIDATE_DOMAIN", "N"));
	}

	public static boolean isValidateIfValidEmailDomain() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.EMAIL_DOMAIN_VALIDATION, "VALIDATE_EMAIL_DOMAIN", "N"));
	}

	public static boolean isReportWaitingEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.REPORT_WAIT_PARAMETERS, "ENABLED", "N"));
	}

	public static int reportWaitInterval() {
		return Integer.parseInt(
				getStringFromConfigMap(SystemParamKeys.REPORT_WAIT_PARAMETERS, "INTERVAL", null, DEFAULT_DATA_SEPARATOR));
	}

	public static int reportTimeOut() {
		return Integer.parseInt(
				getStringFromConfigMap(SystemParamKeys.REPORT_WAIT_PARAMETERS, "TIME_OUT", null, DEFAULT_DATA_SEPARATOR));
	}

	public static boolean isRefundIBEAnciModifyCredit() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.REFUND_IBE_ANCI_MODIFY_CREDIT_AMOUNT));
	}

	public static int getOnholdOfflinePaymentTimeInMillis() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.ONHOLD_OFFLINE_PAYMENT_TIME)) * 60 * 1000;
	}

	public static boolean isEnableFareRuleLevelNCC(ApplicationEngine appIndicator) {
		Map<String, String> nameChangeMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.ENABLE_FARE_RULE_LEVEL_NCC),
				SEMI_COLON_SEPARATOR);
		if (nameChangeMap != null && nameChangeMap.size() > 0) {
			for (String moduleName : nameChangeMap.keySet()) {
				if (moduleName.equals(appIndicator.toString())) {
					String[] paramValues = nameChangeMap.get(moduleName).split(DEFAULT_DATA_SEPARATOR);
					return YES.equals(paramValues[0]);
				}
			}
		}
		return false;
	}

	public static boolean isEnableFareRuleLevelNCCForFlown(ApplicationEngine appIndicator) {
		Map<String, String> nameChangeMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.ENABLE_FARE_RULE_LEVEL_NCC),
				SEMI_COLON_SEPARATOR);
		if (nameChangeMap != null && nameChangeMap.size() > 0) {
			for (String moduleName : nameChangeMap.keySet()) {
				if (moduleName.equals(appIndicator.toString())) {
					String[] paramValues = nameChangeMap.get(moduleName).split(DEFAULT_DATA_SEPARATOR);
					return YES.equals(paramValues[1]);
				}
			}
		}
		return false;
	}

	public static int getNameChangeMaxCount(ApplicationEngine appIndicator) {
		Map<String, String> nameChangeMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.ENABLE_FARE_RULE_LEVEL_NCC),
				SEMI_COLON_SEPARATOR);
		if (nameChangeMap != null && nameChangeMap.size() > 0) {
			for (String moduleName : nameChangeMap.keySet()) {
				if (moduleName.equals(appIndicator.toString())) {
					String[] paramValues = nameChangeMap.get(moduleName).split(DEFAULT_DATA_SEPARATOR);
					return Integer.parseInt(paramValues[2]);
				}
			}
		}
		return 0;
	}

	/**
	 * Check whether bundled service fee is refundable if service re-protection fails for flight cancellation,
	 * disruption or re-protection
	 * 
	 * @return
	 */
	public static boolean isBundledServiceFeeRefundableForFlightDisruption() {
		return YES
				.equals(globalConfig.getBizParam(SystemParamKeys.REFUND_BUNDLED_SERVICE_FEE_FOR_FLIGHT_CNX_DISRUPTION_REPROTECT));
	}

	public static String getImageUploadPath() {
		return globalConfig.getBizParam(SystemParamKeys.MEAL_IMG_PATH);
	}

	public static String getItineraryAdvertisementUploadPath() {
		return globalConfig.getBizParam(SystemParamKeys.ITINERARY_ADVERTISEMENT_IMAGE_URL);
	}

	public static boolean isTrackingEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.TRACKING_ENABLED));
	}

	public static int getMaxChildAdultCount() {
		String count = globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_ADULTS);
		if (count != null && !count.equals("")) {
			return Integer.parseInt(count);
		} else {
			return 0;
		}
	}
	
	public static int getMaxAdultCountIBE() {
		String paramVal = globalConfig.getBizParam(SystemParamKeys.MAX_ADULT_COUNT_ALLOWED_FOR_RESERVATION);
		String count = paramVal.split(",")[0].split("=")[1];
		if (count != null && !count.equals("")) {
			return Integer.parseInt(count);
		} else {
			return 0;
		}
	}
	
	public static int getMaxAdultCountXBE() {
		String paramVal = globalConfig.getBizParam(SystemParamKeys.MAX_ADULT_COUNT_ALLOWED_FOR_RESERVATION);
		String count = paramVal.split(",")[1].split("=")[1];
		if (count != null && !count.equals("")) {
			return Integer.parseInt(count);
		} else {
			return 0;
		}
	}
	
	public static int getMaxAdultCountWS() {
		String paramVal = globalConfig.getBizParam(SystemParamKeys.MAX_ADULT_COUNT_ALLOWED_FOR_RESERVATION);
		String count = paramVal.split(",")[2].split("=")[1];
		if (count != null && !count.equals("")) {
			return Integer.parseInt(count);
		} else {
			return 0;
		}
	}
	
	public static int getMaxAdultCountGDS() {
		String paramVal = globalConfig.getBizParam(SystemParamKeys.MAX_ADULT_COUNT_ALLOWED_FOR_RESERVATION);
		String count = paramVal.split(",")[3].split("=")[1];
		if (count != null && !count.equals("")) {
			return Integer.parseInt(count);
		} else {
			return 0;
		}
	}

	public static boolean isLMSEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_LOYALTY_MANAGEMENT_SYSTEM));
	}

	public static String getLoyaltyManagedCurrencyCode() {
		return getStringFromConfigMap(SystemParamKeys.LOYALTY_POINTS_CONVERSION_RATES, LoyaltyPointsConversionParams.CURRENCY,
				Currency.getInstance(Locale.US).getCurrencyCode());
	}

	public static BigDecimal getLoyaltyPointsToCurrencyConversionRate() {
		return new BigDecimal(getStringFromConfigMap(SystemParamKeys.LOYALTY_POINTS_CONVERSION_RATES,
				LoyaltyPointsConversionParams.POINTS_TO_CURRENCY, "1"));
	}

	public static BigDecimal getCurrencyToLoyaltyPointsConversionRate() {
		return new BigDecimal(getStringFromConfigMap(SystemParamKeys.LOYALTY_POINTS_CONVERSION_RATES,
				LoyaltyPointsConversionParams.CURRENCY_TO_POINTS, "1"));
	}

	public static boolean isSSREnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_SSR_ENABLE));
	}

	public static boolean isRetFileGenerationForCustomNominalCodesEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_RET_FILE_GENERATION_FOR_CUSTOM_NOMINAL_CODES));
	}

	public static boolean isAllowAddInfantToPartiallyFlownBookings() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_ADD_INFANT_TO_PARTIALLY_FLOWN_BOOKINGS));
	}

	public static String getBankGuaranteeNotifyingEmailAddresses() {
		return globalConfig.getBizParam(SystemParamKeys.BANK_GURANTEE_CREDIT_LIMIT_CC_ADDRESS);
	}

	public static boolean isGDSIntergrationEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.GDS_INTEGRATED));
	}

	public static boolean isPosWebFaresEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_POS_WEB_FARES));
	}

	public static boolean isBSPCreditLimitAlertingEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_BSP_CREDIT_LIMIT_ALERTS));
	}

	public static boolean isGSAStructureVersion2Enabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.GSA_STRUCTURE_V2_ENABLED));
	}

	public static boolean isPaymentMethodCapturedFromReceiptNumberEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_PAYAMENT_METHOD_CAPTURED_FROM_RECEIPT_NUMBER));
	}

	public static boolean skipPersianDateChangeInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_SKIP_DATE_CHANGE_TO_PERSIAN));
	}

	/** User Account Security Configuration **/
	public static boolean isEnablePasswordExpiry() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.USER_ACCOUNT_SECURITY, "ENABLE_PASSWORD_EXPIRY", "N"));
	}

	public static int getPasswordExpiryDays() {
		return Integer.parseInt(getStringFromConfigMap(SystemParamKeys.USER_ACCOUNT_SECURITY, "PASSWORD_EXPIRY_DAYS", "N"));
	}

	public static boolean isEnablePasswordHistory() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.USER_ACCOUNT_SECURITY, "ENABLE_PASSWORD_HISTORY", "N"));
	}

	public static boolean isEnableAccountLock() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.USER_ACCOUNT_SECURITY, "ENABLE_ACCOUNT_LOCK", "N"));
	}

	public static boolean includeAccountLock(String userID) {
		boolean isInclude = true;
		String excludeUsers = getStringFromConfigMap(SystemParamKeys.USER_ACCOUNT_SECURITY, "EXCLUDE_USERS", "N");
		List<String> userList = getValueListBySeparator(excludeUsers, SEMI_COLON_SEPARATOR);
		if (userList.contains(userID)) {
			isInclude = false;
		}
		return isInclude;
	}

	public static int getUserLoginMaxAttempts() {
		return Integer.parseInt(getStringFromConfigMap(SystemParamKeys.USER_ACCOUNT_SECURITY, "ACCOUNT_LOCK_ATTEMPTS", "N"));
	}

	public static int getAccountLockReleaseMinutes() {
		return Integer
				.parseInt(getStringFromConfigMap(SystemParamKeys.USER_ACCOUNT_SECURITY, "ACCOUNT_LOCK_RELEASE_MINUTES", "N"));
	}

	public static boolean isEnableUserAudit() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.USER_ACCOUNT_SECURITY, "ENABLE_USER_AUDIT", "N"));
	}

	public static long getAPIReservationCutoverForDomesticInMillis() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.API_RESERVATION_CUTOFF_TIME_FOR_DOMESTIC));
	}

	public static boolean isShowTicketDetailsInPnlAdl() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_TICKET_DETAILS_IN_PNL_ADL));
	}

	public static long getAPIReservationCutoverForInternationalInMillis() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.API_RESERVATION_CUTOFF_TIME_FOR_INTERNATIONAL));
	}

	public static boolean displayFFPNoInXBEToolBar() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_FFP_NO_IN_XBE_TOOLBAR));
	}

	public static boolean displaySpecificErrorsForIncorrectLogin() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_SPECIFIC_ERRORS_FOR_INCORRECT_LOGIN));
	}

	public static boolean isDisplayNotificationForLastNameInContactDetailsInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_NOTIFICATION_FOR_LAST_NAME_IN_CONTACT_DETAILS_IN_IBE));
	}

	public static boolean isPreventAddingDuplicatesInRecordNSettleScreen() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.PREVENT_ADDING_DUPLICATES_IN_RECORD_AND_SETTLE_SCREEN));
	}

	public static int getMaximumPassengerCount() {
		String count = globalConfig.getBizParam(SystemParamKeys.MAX_PAX_AGENT);
		if (count != null && !count.equals("")) {
			return Integer.parseInt(count);
		} else {
			return 0;
		}
	}

	public static boolean isIBEAddSSR() {
		return (isInFlightServicesEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ADD_SSR)));
	}

	public static boolean isIBEEditSSR() {
		return (isInFlightServicesEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_EDIT_SSR)));
	}

	public static boolean isEnableStopOverInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_STOPOVER_FOR_IBE));
	}

	public static boolean stopRefundingTotalAmountWhenNoshowRulesApplied() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.STOP_REFUNDING_TOTAL_PAYMENT_WHEN_NOSHOW_RULE_APPLIES));
	}

	public static boolean isMaintainSSRWiseCutoffTime() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.MAINTAIN_SSR_WISE_CUTOFF_TIME));
	}

	public static boolean isRequestNationalIDForReservationsHavingDomesticSegments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.REQUEST_NATIONALID_FOR_RESERVATIONS_HAVING_DOMESTIC_SEGMENT));
	}

	public static boolean isThumbnailLogoEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_THUMBNAIL_LOGO_IN_ITINERARY));
	}

	public static boolean isEnableSegmentModificationForCancelledFlightSegments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SEGMENT_MODIFICATION_FOR_CANCELLED_FLIGHT_SEGMENTS));
	}

	public static boolean isCodeShareEnable() {
		boolean isCodeShareEnable = false;

		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {
			Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				for (String gdsId : gdsStatusMap.keySet()) {
					GDSStatusTO gdsStatusTO = gdsStatusMap.get(gdsId);
					if (gdsStatusTO.isCodeShareCarrier()) {
						isCodeShareEnable = true;
						break;
					}
				}
			}
		}
		return isCodeShareEnable;
	}

	public static boolean isAllowSameFlightModificationInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_SAME_FLIGHT_MODIFICATION_IN_IBE));
	}

	public static boolean showDecimalPlacesForCurrencyValuesInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_DECIMAL_PLACES_IN_CUREENCY_VALUES));
	}

	public static boolean showDescriptionForFareBundlesInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_DESCRIPTION_FOR_FARE_BUNDLES_IN_IBE));
	}

	public static boolean isApplyNoShowRulesForCodeSharePNR() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_NO_SHOW_RULES_FOR_CODE_SHARE_PNR));
	}

	public static Integer getRoundUpCHINFareToNearest() {
		return Integer.valueOf(globalConfig.getBizParam(SystemParamKeys.ROUNDUP_CHINF_FARE));
	}

	public static boolean isIATAAircraftTypeMandatory() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IATA_AIRCRAFT_TYPE_MANDATORY));
	}

	public static boolean isCachingEnableforOndPublished() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CACHING_FOR_OND_PUBLISHED));
	}

	public static String getGoogleTagManagerKey() {
		return globalConfig.getBizParam(SystemParamKeys.GOOGLE_TAG_MANAGER_KEY);
	}

	public static long getCheckingTimeDifferenceInMiliSeconds() throws ModuleException {
		return getTimeInMillis(getCheckInTimeDifference());
	}

	public static long getCurrencyExchangeRateCacheTimout() {
		return getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.CURRENCY_EXRATE_TIMEOUT_FOR_SERVICE_APP));
	}

	public static String getDefaultAirlineUrl() {
		return PlatformUtiltiies
				.nullHandler(globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, AppSysParamsUtil.getDefaultCarrierCode()));
	}
	
	
	public static String getAirlineReservationURL() {
		return PlatformUtiltiies
				.nullHandler(globalConfig.getBizParam(SystemParamKeys.AIRLINE_RESERVATION_URL));
	}

	public static boolean isMaintainSeperateCALTransmissionIntervalAfterCutOffTime() {
		return YES.equals(
				globalConfig.getBizParam(SystemParamKeys.MAINTAIN_SEPERATE_CAL_TRANSMISSION_REPEAT_INTERVAL_AFTER_CUTOFF_TIME));
	}

	public static boolean isDisplayOtherAirlineSegments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_OTHER_AIRLINE_SEGMENTS));
	}

	public static boolean isSendIbObConnectionsForOtherAirlineSegments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SEND_IBOB_CONNECTIONS_FOR_OTHER_AIRLINE_SEGMENTS_IN_PNL_ADL));
	}

	public static String getAirlineUrlToDisplayInItinerary(String carrierCode) {
		return PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.ITINERARY_AIRLINE_URL, carrierCode));
	}

	public static String getItineraryLanguageForInternationalFlightBookings(String languageCode) {
		return PlatformUtiltiies.nullHandler(getStringFromConfigMap(
				SystemParamKeys.CONFIGURED_LANGUAGE_FOR_ITINERARY_IN_INT_FLT_BOOKINGS, languageCode, null));
	}

	public static String getCommonBookingCodesForExternalSSMRecap() {
		return PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.COMMON_BOOKING_CLASS_CODES_STRING));
	}

	public static boolean isExternalSSMRecapEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.EXTERNAL_SSM_RECAP_INTEGRATED));
	}

	public static boolean isGDSCsConfigsFromCache() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.GDS_CODESHARE_CONFIGS_FROM_CACHE));
	}

	public static boolean isSplitFromPreviousSubSchedules() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SPLIT_FROM_SUB_SCHEDULES));
	}

	public static boolean isSendSsmCnlMessageForReSchedule() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SEND_CNL_MSG_FOR_RE_SCHEDULE));
	}

	public static boolean isCancelPreviousScheduleOnDSTChanges() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CANCEL_EXISTING_SCHEDULE_ON_DST_CHANGE));
	}

	public static boolean isEnableSplitScheduleWhenDSTApplicable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SPLIT_SCHEDULE_FOR_DST_CHANGES));
	}

	public static int getPaymentAttemptLimit() {
		return Integer.valueOf(globalConfig.getBizParam(SystemParamKeys.IBE_PAYMENT_ATTEMPT_LIMIT));
	}

	public static boolean isCancelExpiredOhdResEnable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CANCEL_EXPRIED_ONHOLD_RESERVATION));
	}

	public static boolean isIbeAddPaxTypeEnable() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_ENABLE_PAX_TYPE));
	}

	public static boolean isAeroMartPayEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AEROMART_PAY));
	}

	public static boolean isAllowItineraryAdvertisement() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ITINERARY_ADVERTISEMENT));
	}

	public static boolean isSelectedCurrencyIPGs() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_SELECTED_CURRENCY_IPG));
	}

	public static String getClientCallCenterURL() {
		Map<String, String> rakInsMap = getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.CLIENT_CALL_CENTER_URL),
				SEMI_COLON_SEPARATOR);
		if (YES.equals(rakInsMap.get("IS_ENABLED"))) {
			return rakInsMap.get("CALLCENTER_URL");
		}
		return EMPTY_STRING;
	}

	public static boolean isSelfReprotectionAllowed(){
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.PASSENGER_SELF_REPROTECTION));
	}
	
	public static boolean isSeatMealPreferenceEnabled(){
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SEAT_MEAL_PREFERENCE));
	}
	
	public static boolean isAddFamilyMemberEnabled(){
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ADD_FAMILY_MEMBERS));
	}

	public static boolean isEnableDefaultInvCreationWithBuildFlights() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DEFAULT_INV_CREATION_WITH_BUILD_FLIGHTS));
	}

	public static boolean isVoucherEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_VOUCHER_AND_GIFT_VOUCHER));
	}

	public static boolean isVoucherNameValidationEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_VOUCHER_PAX_NAME_VALIDATION));
	}

	public static boolean isRecaptchaEnabled() {
		return YES.equals(getStringFromConfigMap(SystemParamKeys.GOOGLE_RECAPTCHA_CONFIG, GoogleRecaptcha.ENABLED, null,
				SEMI_COLON_SEPARATOR));
	}

	public static String getRecaptchaURL() {
		return getStringFromConfigMap(SystemParamKeys.GOOGLE_RECAPTCHA_CONFIG, GoogleRecaptcha.URL, null, SEMI_COLON_SEPARATOR);
	}

	public static String getRecaptchaSiteKey() {
		return getStringFromConfigMap(SystemParamKeys.GOOGLE_RECAPTCHA_CONFIG, GoogleRecaptcha.SITE_KEY, null,
				SEMI_COLON_SEPARATOR);
	}

	public static String getRecaptchaSecretKey() {
		return getStringFromConfigMap(SystemParamKeys.GOOGLE_RECAPTCHA_CONFIG, GoogleRecaptcha.SECRET_KEY, null,
				SEMI_COLON_SEPARATOR);
	}

	public static boolean isEnableAutoPublishSchedule() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_AUTO_PUBLISH_SCHEDULE));
	}

	public static boolean isEnableOfficerEmailAlertAtFlightCnx() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_OFFICER_EMAIL_ALERT_AT_FLIGHT_CNX));
	}

	public static boolean isEnablePaxReprotectPostCNX() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_REPROTECT_POST_CNX));
	}

	public static boolean isOfficerContactInfoMobileNumMandatory() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_OFFICER_CONATACT_MOBILE_NUM_MANDATORY));
	}

	public static boolean isOfficerContactInfoEmailMandatory() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IS_OFFICER_CONATACT_EMAIL_MANDATORY));
	}

	public static boolean isEnableAttachRouteWiseDefAnciTemplAtSSIMProc() {
		return YES
				.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ATTACH_ROUTE_WISE_DEFAULT_ANCI_TEMPLATE_SSIM_PROCESSING));
	}

	public static boolean isEnableSendSsmAsmSubMessages() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SEND_SSM_ASM_SUB_MESSAGES));
	}

	public static boolean isEnableOverlappingbySsmAsm() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_OVERLAPPING_BY_SSM_ASM));
	}

	public static boolean isSplitAndUpdateScheduleForScheduleConflicts() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SPLIT_SCHEDULE_FOR_SCHEDULE_CONFLICT_WHEN_PROCESS_SSM));
	}

	public static boolean isEnableMessageQueuingForSsmAsm() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_MSG_QUEUING_SSM_ASM));
	}

	public static boolean isEnableDelayProcessingSsmAsmCnlAction() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DELAY_PROCESSING_SSM_ASM_CNL_MSG));
	}

	public static int getSsmAsmCnlActionProcessingHoldTimeInSeconds() {
		return Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.SSM_ASM_CNL_ACTION_PROCESSING_HOLD_TIME));
	}

	public static boolean isEnablePalCalMessage() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_PALCAL_MESSAGE));
	}

	public static boolean displayGdsRlocInCompanyPaymentsReport() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_GDS_RLOC_COMPANY_PAYMENTS_REPORT));
	}

	public static boolean enableElectronicMCO() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ELECTRONIC_MCO));
	}

	public static boolean isEnableFareRuleComments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_FARE_RULE_COMMENTS));
	}

	public static String getAeroMartPayTokenTimeOut() {
		return getStringFromConfigMap(SystemParamKeys.AEROMART_PAY_CONFIGS, "TOKEN_TIMEOUT", null);
	}

	public static long getExternalAgentCreditBlockDuration() {
		return AppSysParamsUtil.getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.EXTERNAL_AGENT_CREDIT_BLOCK_DURATION));
	}

	public static boolean isCaptchaEnabledForXbeLogin() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.XBE_LOGIN_CAPTCHA_ENABLE));
	}

	public static boolean isCaptchaEnabledForAirAdminLogin() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.AIRADMIN_LOGIN_CAPTCHA_ENABLE));
	}

	public static boolean isEnableOndSplitForBusFareQuote() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.OND_SPLIT_FOR_BUS_FARE_QUOTE));
	}

	public static boolean isFareRuleDetailsEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.FARE_RULE_DETAILS_IN_API));
	}

	public static boolean isAssignDefaultMealChargesTemplate() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ASSIGN_DEFAULT_MEAL_CHARGES_TEMPLATE));
	}

	public static boolean isAssignDefaultSeatChargesTemplate() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ASSIGN_DEFAULT_SEAT_CHARGES_TEMPLATE));
	}

	public static boolean removeActionAlertInTransfer() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.REMOVE_ACTION_ALERT_WHEN_SELECTION_TRANSFER));
	}

	public static boolean displayMultilegDetailsInItinerary() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_MULTILEG_STOPOVER_DETAILS));
	}

	public static boolean isAnciSummaryAtPaymentEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ANCI_SUMMARY_AT_PAYMENT_PAGE));
	}

	public static boolean isShowMultilegStopOversInIBE() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.IBE_DISPLAY_MULTILEG_STOPOVERS));
	}

	public static boolean isPaxWiseTicketingEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.PAX_WISE_TICKETING_ENABLED));
	}

	public static boolean isDisplayFlexiAsSeparteBundle() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_FLEXI_AS_SEPERATE_BUNDLE));
	}

	public static boolean isBundleFareToQuoteIndependentOfBCSurChargeExcluition() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_BUNDLE_FARE_QUOTE_INDEPENDENT_OF_SUR_EXCLUDE_FLAG));
	}

	public static String getExternalAgentURL() {
		return globalConfig.getBizParam(SystemParamKeys.EXTERNAL_AGENT_URL);
	}

	public static boolean isEnableElectronicMCO() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ELECTRONIC_MCO));
	}

	public static String getReportsStorageLocation() {
		return PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.REPORTS_STORAGE_LOCATION));
	}

	public static boolean isEnableSequentialMessageProcessing() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SEQUENTIAL_PROCESSING_FOR_IN_MSG));
	}

	public static boolean enableIATAMealCodesInPNLADL() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_IATA_MEAL_CODES_FOR_PNL));
	}

	public static List<String> getSequentialProcessingAllowedMessageTypes() {
		String msgTypes = globalConfig.getBizParam(SystemParamKeys.SEQUENTIAL_PROCESSING_ALLOWED_MSG_TYPES);
		String[] values = msgTypes.split(DEFAULT_DATA_SEPARATOR);

		return Arrays.asList(values);
	}

	public static boolean isShowIBEV3ChargesBreakdownInPaymentPage() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_IBE_V3_CHARGES_BREAKDOWN_IN_PAYMENT_PAGE));
	}

	public static boolean allowAvailabilityPageToHaveCheckBoxToAcceptTermsAndConditions() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CHECK_BOX_TO_ACCEPT_TERMS_IN_AVAILABILITY_PAGE_IBE_V3));
	}

	public static String getCallCenterUrl() {
		return getStringFromConfigMap(SystemParamKeys.ONHOLD_EMAIL_NOTIFICATION_PROPERTIES, "call_center_url", "",
				DEFAULT_DATA_SEPARATOR);
	}

	public static String getSalesShopUrl() {
		return getStringFromConfigMap(SystemParamKeys.ONHOLD_EMAIL_NOTIFICATION_PROPERTIES, "before_sales_shop_url", "",
				DEFAULT_DATA_SEPARATOR);
	}

	public static String getCashCollectionUrl() {
		return getStringFromConfigMap(SystemParamKeys.ONHOLD_EMAIL_NOTIFICATION_PROPERTIES, "after_sales_shop_url", "",
				DEFAULT_DATA_SEPARATOR);
	}

	public static boolean isEnableRouteSelectionForAgents() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_ROUTE_SELECTION_FOR_AEGNT));
	}

	public static boolean isEnableFlightSelectionInManifestForAgentAllowedRoutes() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_FLIGHTS_IN_ALLOWED_ROUTES_FOR_AGENTS_IN_MANIFEST));
	}
	
	public static boolean isEnableAeroMartETS() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AEROMART_ETS));
	}

	public static boolean allowForceCNFBookingModification() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_IBE_FORCE_CONFIRM_MODIFY));
	}
	
	public static boolean sendFileLinkWithDomainName() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SEND_REPORT_LINK_WITH_DOMAIN_NAME));
	}
	
	public static boolean useOldIbeUrlInServiceAppConfirmRegister() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.USE_OLD_IBE_URL_IN_SERVICEAPP_CONFIRM_REGISTRATION));
	}
	
	public static boolean skipDummyPayments() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_DUMMY_PAYMENTS));
	}
	public static boolean showPaymentDateWithTimestamp() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_PAYMENT_DATE_WITH_TIMESTAMP));
	}

	public static boolean displayAmountsInSeperateColumnsWithCategory() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_ANCI_SURCHARGES_PENALTY_IN_SEPERATE_COLUMNS));
	}

	public static boolean showAgentCreditInAgentCurrency() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_AGENT_CREDIT_IN_AGENT_CURRENCY));
	}

	public static boolean isByPassInvChecksForSeatRelease() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.BY_PASS_INVENTORY_CHECK_FOR_SEAT_RELEASE));
	}	

	public static List<String> getRestrictedPaymentModes(String product) {
		String text = globalConfig.getBizParam(SystemParamKeys.RESTRICTED_PAYMENT_MODES);
		Map<String, String> productWiseRestrictedModes = getKeyValuePair(text);

		List<String> restrictedPaymentModes = new ArrayList<>();
		if (productWiseRestrictedModes.containsKey(product)) {
			restrictedPaymentModes = getValueListBySeparator(productWiseRestrictedModes.get(product), SEMI_COLON_SEPARATOR);
		}

		return restrictedPaymentModes;
	}

	public static boolean isBundleFarePopupEnabled(ApplicationEngine applicationEngine) {
		return YES.equals(getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.ENABLE_BUNDLE_FARE_POPUP))
				.get(applicationEngine.toString()));
	}

	public static boolean isOpenAllTheEticketsInPastFlights() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.OPEN_ALL_THE_ETICKETS_IN_PAST_FLIGHTS));
	}

	public static String getBundleFareDescriptionTemplateVersion() {
		return StringUtil.isNullOrEmpty(globalConfig.getBizParam(SystemParamKeys.BUNDLE_FARE_DESCRIPTION_TEMPLATE_VERSION)) ?
				"v1" :
				globalConfig.getBizParam(SystemParamKeys.BUNDLE_FARE_DESCRIPTION_TEMPLATE_VERSION).toLowerCase();
	}
	
	public static boolean isBundleFareDescriptionTemplateV2Enabed() {
		return StringUtil.isNullOrEmpty(globalConfig.getBizParam(SystemParamKeys.BUNDLE_FARE_DESCRIPTION_TEMPLATE_VERSION))
				|| !"v2".equals(AppSysParamsUtil.getBundleFareDescriptionTemplateVersion().toLowerCase()) ? false : true;
	}

	public static boolean enableModifiedQueryForBestOffers() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.USE_MODIFIED_QUERY_FOR_BEST_OFFERS));
	}
	
	public static String getEnvironmentIdentificationCode() {
		return globalConfig.getBizParam(SystemParamKeys.ENVIRONMENT_IDENTIFICATION_CODE);
	}
	
	public static boolean isEnableEventPublication() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_EVENT_PUBLICATION));
	}

	public static List<String> getPaymentSettlementPriority() {
		if (paymentSettlementPriority == null) {
			paymentSettlementPriority = new ArrayList<String>();
			Map<String, String> priorityMap = getKeyValuePair(
					globalConfig.getBizParam(SystemParamKeys.PAYMENT_SETTLEMENT_PRIORITY));
			SortedMap<String, String> sortedMap = new TreeMap<String, String>(priorityMap);

			for (Entry<String, String> entry : sortedMap.entrySet()) {
				paymentSettlementPriority.add(entry.getValue());
			}
		}

		return paymentSettlementPriority;
	}

	public static String getGstGoodsServices() {
		return globalConfig.getBizParam(SystemParamKeys.GST_GOODS_SERVICES);
	}

	public static String getGstHsnSac() {
		return globalConfig.getBizParam(SystemParamKeys.GST_HSN_SAC);
	}

	public static String getTaxRegistrationNumberEnabledCountries() {
		return PlatformUtiltiies.nullHandler(globalConfig.getBizParam(SystemParamKeys.TAX_REGISTRATION_NUMBER_VISIBLE_COUNTRIES));
	}

	public static boolean isGSTJNMigrationEnabled() {
		// TODO Auto-generated method stub
		return false;

	}	

	public static boolean isConsiderSegmentReturnFlagInGSTCalculation() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.CONSIDER_SEGMENT_RETURN_FLAG_IN_GST_CALCULATION));
	}

	public static boolean enableCutoffTimeForAPIReservation() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CUTOFF_TIME_FOR_API_RESERVATION));
	}
	
	public static Collection<String> getChargeGroupsToQuoteGSTOnNonTicketingRevenue() {
		return getValueList(globalConfig.getBizParam(SystemParamKeys.NON_TICKETING_REVENUE_GST_QUOTABLE_CHARGE_GROUPS));

	}

	public static int getIBEPasswordResetTimeOut(){
		return Integer.valueOf(globalConfig.getBizParam(SystemParamKeys.IBE_CUSTOMER_PASSWORD_RESET_TIMEOUT));
	}

	public static boolean isEnableSSMforSplitSchedule() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SSM_FOR_SPLIT_SCHEDULE));
	}
	

	public static boolean isApplyBSPTransactionFee(ApplicationEngine applicationEngine) {
		return YES.equals(getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.APPLY_CHANNEL_WISE_BSP_FEE))
				.get(applicationEngine.toString()));
	}


	public static boolean isDetailAuditEnabledForProcessingScheduleMessages() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DETAIL_AUDIT_FOR_PROCESSING_SCHEDULE_MSG));
	}

	public static boolean isDefaultBundledSelectionEnabled(String applicationEngine) {
		return YES
				.equals(getKeyValuePair(globalConfig.getBizParam(SystemParamKeys.ENABLE_CHANNEL_WISE_DEFAULT_BUNDLED_SELECTION))
						.get(applicationEngine));
	}
	
	public static boolean allowRemoveInfantWhenFutureSegmentsNotThere() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ALLOW_REMOVE_INFANT_WHEN_FUTURE_SEGMENTS_NOT_THERE));

	}
	
	public static boolean sendEmailForFlightAlterationByScheduleMessages() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERED_BY_SCHEDULE_MSG));
	}

	public static boolean sendSMSForFlightAlterationByScheduleMessages() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERED_BY_SCHEDULE_MSG));
	}
	
	public static boolean isRestrictSegmentModificationByETicketStatus() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.RESTRICT_MODIFICATION_FOR_CHECKEDIN_BOARDED_SGEMENTS));
	}

	public static boolean displayOwnerAgentColumnInRevNTaxReport() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.DISPLAY_OWNER_AGENT_COLUMN_IN_REV_TAX_REPORT));
	}
	
	public static boolean displayCreditDebitEntriesInOtherWay() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_DISPLAY_CONFIGURATION_CREDIT_DEBIT_ENTRIES));
	}

	public static boolean skipMODChargesForOpenSegmentModification() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SKIP_MOD_CHARGE_FOR_OPEN_SEGMENT_MODIFICATION));
	}
	
	public static boolean applyHandlingFeeOnModification() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.APPLY_HANDLING_CHARGES_ON_MODIFICATION));
	}

	public static boolean showFCCSegBCInventoryAllocGrouping() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FCC_SEG_BC_INVENTORY_ALLOC_GROUPING));
	}

	public static boolean showFCCSegBCInventoryOneWayReturnPaxCounts() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.SHOW_FCC_SEG_BC_INVENTORY_ONEWAY_RETURN_PAXCOUNTS));
	}	
	
	public static int getMinRequiredSeatsForBTReservations() {
		return Integer.valueOf(globalConfig.getBizParam(SystemParamKeys.MIN_REQUIRED_SEATS_FOR_BT_RES));
	}

	public static int getCutOffTimeForCancelLMSBlockedCredit() throws ModuleException {
		return getTotalMinutes(globalConfig.getBizParam(SystemParamKeys.CANCEL_LMS_BLOCKED_CREDITS_CUTOFF_TIME));
	}

	public static String getAdditionalSentenceOnInvoice() {
		return globalConfig.getBizParam(SystemParamKeys.ADDITIONAL_SENTENCE_ON_INVOICE);
	}

	public static boolean enableCityBasesFunctionality() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_CITY_BASED_FUNCTIONALITY));
	}
	
	public static boolean isAutomaticCheckinEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_AUTO_CHECKIN));
	}
	
	public static String getAutoCheckinStopSellingGapMins() {
		return globalConfig.getBizParam(SystemParamKeys.ACI_SEATMAP_GAP);
	}
	
	public static String getCitySearchOnDDescriptionText() {
		return globalConfig.getBizParam(SystemParamKeys.CITY_SEARCH_OND_DESCRIPTION_TEXT);
	}

	public static String getPnlDepartureGapMins() {
		return globalConfig.getBizParam(SystemParamKeys.PNL_DEPARTURE_GAP);
	}

	public static String getOnlineCheckInURLText() {
		return globalConfig.getBizParam(SystemParamKeys.ONLINE_CHECK_IN_URL_IN_IBE);
	}
	public static String getBulkCheckInURLText() {
		return globalConfig.getBizParam(SystemParamKeys.BULK_CHECK_IN_URL_IN_IBE);
	}
	public static boolean isOTPEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_OTP_FOR_GIFT_VOUCHER));
	}
	
	public static String getTimeDifferenceToDisplayOnlineCheckin() {
		return globalConfig.getBizParam(SystemParamKeys.TIME_DIFFERENCE_FOR_ONLINE_CHECK_IN_IBE);
	}
	
	public static String getMaxDatePeriodsForPromotionCriteria() {
		return globalConfig.getBizParam(SystemParamKeys.MAX_NO_OF_DATE_PERIODS_FOR_PROMOTION_CRITERIA);
	}
	public static boolean isAllowedModificationForPastFlight() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.TIME_CHANGE_CANCELLATION_AND_REPROTECTION_FOR_PAST_FLIGHTS));
	}

	public static int getPastFlightActivationDurationInDays() {
		return Integer.valueOf(globalConfig.getBizParam(SystemParamKeys.PAST_FLIGHT_ACTIVATION_DURATION_IN_DAYS));
	}

	public static List<String> getAllowableMsgTypes(){
		return getValueList(globalConfig.getBizParam(SystemParamKeys.ALLOWABLE_MSG_TYPES));
	}

	public static boolean isAllowedUpdateScheduledFlightToOutOfSchedOrFreq() {

		return YES.equals(globalConfig
				.getBizParam(SystemParamKeys.ALLOW_UPDATE_SCHEDULED_FLIGHT_TO_OUT_OF_SCHEDULE_PERIOD_OR_FREQUENCY));
	}

	public static boolean isEnableSingleStepRedeemingProcess() {
		return isLMSEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_SINGLE_STEP_REDEEMING_PROCESS));
	}

	public static int getCutOffTimeForCancelIssuedUnconsumedLMSPoints() throws ModuleException {
		return getTotalMinutes(globalConfig.getBizParam(SystemParamKeys.CANCEL_ISSUED_UNCONSUMED_LMS_CREDITS_CUTOFF_TIME));
	}

	public static boolean proceedingPaymentWithLmsCreditCancelServiceFailiure() {
		return isLMSEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.PROCEED_PAYMENT_LMS_CANCEL_SERVICE_FAILURE));
	}
	
	public static boolean isLMSMemberAuthenticationRequired() {
		return isLMSEnabled() && YES.equals(globalConfig.getBizParam(SystemParamKeys.LMS_MEMBER_AUTHENTICATION_REQUIRED));
	}

	public static boolean isEnabledGoQuoServiceAsSSR() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.ENABLE_GOQUO_SERVICE_AS_SSR));
	}
	
	public static boolean shouldRemoveReprotectedBundledAnciForOHD() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.REMOVE_REPROTECTED_BUNDLED_ANCI_FOR_OHD));
	}
	
	public static boolean isAerospikeIPToCountryEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.AEROSPIKE_IP_TO_COUNTRY_ENABLED));
	}

	public static boolean isXBEMinimumFareEnabled() {
		return YES.equals(globalConfig.getBizParam(SystemParamKeys.XBE_MINIMUM_FARE_SEARCH_ENABLED));
	}
}
