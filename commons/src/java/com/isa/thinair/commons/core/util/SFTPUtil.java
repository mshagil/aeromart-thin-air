package com.isa.thinair.commons.core.util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class SFTPUtil {

	private Session session = null;
	private ChannelSftp sftpChannel = null;

	private String host = null;
	private int port = 22;
	private String username = null;
	private String password = null;
	private String strictHostChk = "no";
	private final String SFTP_PROTOCOL = "sftp";

	private boolean useHttpProxy = false;
	private String httpProxyHost = null;
	private int httpProxyPort;

	public SFTPUtil(String host, String username, String password) {
		this.host = host;
		this.username = username;
		this.password = password;
	}

	public SFTPUtil(String host, int port, String username, String password) {
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
	}

	public void connectAndLogin() throws JSchException {
		if (session == null || !session.isConnected()) {
			JSch jsch = new JSch();
			session = jsch.getSession(username, host, port);
			session.setConfig("StrictHostKeyChecking", strictHostChk);
			session.setPassword(password);

			if (isUseHttpProxy()) {
				session.setProxy(new ProxyHTTP(getHttpProxyHost(), getHttpProxyPort()));
			}
			session.connect();
		}

		if (sftpChannel == null || sftpChannel.isClosed()) {
			Channel channel = session.openChannel(SFTP_PROTOCOL);
			channel.connect();
			sftpChannel = (ChannelSftp) channel;
		}

	}

	public void closeAndLogout() {
		if (sftpChannel != null) {
			sftpChannel.exit();
		}
		if (session != null) {
			session.disconnect();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		closeAndLogout();
		super.finalize();
	}

	private boolean isUseHttpProxy() {
		return useHttpProxy;
	}

	public void setHttpProxy(String httpHost, Integer httpPort) {
		if (httpHost != null && !"".equals(httpHost.trim()) && httpPort != null && httpPort > 0) {
			this.useHttpProxy = true;
			this.httpProxyHost = httpHost;
			this.httpProxyPort = httpPort;
		} else {
			this.useHttpProxy = false;
		}
	}

	private String getHttpProxyHost() {
		return httpProxyHost;
	}

	private int getHttpProxyPort() {
		return httpProxyPort;
	}

	public void changeDirectory(String path) throws SftpException, JSchException {
		connectAndLogin();
		sftpChannel.cd(path);
	}

	public void uploadFile(String localPath) throws SftpException, JSchException {
		connectAndLogin();
		sftpChannel.put(localPath);
	}

	public void uploadFile(String localPath, String remotePath) throws SftpException, JSchException {
		connectAndLogin();
		sftpChannel.put(localPath, remotePath);
	}

	public void downloadFile(String localPath, String remotePath) throws SftpException, JSchException {
		connectAndLogin();
		sftpChannel.get(localPath, remotePath);
	}
}
