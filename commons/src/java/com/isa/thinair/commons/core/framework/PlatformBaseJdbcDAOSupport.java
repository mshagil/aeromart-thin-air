package com.isa.thinair.commons.core.framework;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

public class PlatformBaseJdbcDAOSupport {

	private DataSource dataSource;

	private Properties queries;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Properties getQueries() {
		return queries;
	}

	public void setQueries(Properties queries) {
		this.queries = queries;
	}

	public String getQuery(String queryId) {
		return getQueries().getProperty(queryId);
	}

	public String setStringToPlaceHolderIndex(String sqlString, int placeHolderValue, String stringToInsert) {
		StringBuffer queryBuff = new StringBuffer(sqlString);

		String placeHolder = "{" + placeHolderValue + "}";
		int startIndex = queryBuff.toString().indexOf(placeHolder);
		if (startIndex != -1)
			queryBuff.delete(startIndex, startIndex + placeHolder.length());
		queryBuff.insert(startIndex, stringToInsert);
		return queryBuff.toString();
	}

	public String getQuery(String queryId, String... args) {
		StringBuffer queryBuff = new StringBuffer(getQuery(queryId));
		if (args != null) {
			for (int i = 0; i < args.length; i++) {
				String placeHolder = "{" + i + "}";
				int startIndex = queryBuff.toString().indexOf(placeHolder);
				if (startIndex != -1)
					queryBuff.delete(startIndex, startIndex + placeHolder.length());
				queryBuff.insert(startIndex, args[i]);
			}
		}
		return queryBuff.toString();
	}

	public String getSqlInPlaceholders(Object[] values) {
		int size = values.length;
		StringBuffer inStrBuffer = new StringBuffer();
		for (int i = 0; i < size; i++)
			inStrBuffer.append("?,");
		String inStr = inStrBuffer.toString();
		return inStr.substring(0, inStr.lastIndexOf(','));
	}

	@SuppressWarnings("rawtypes")
	public String getSqlInPlaceholders(List values) {
		int size = values.size();
		StringBuffer inStrBuffer = new StringBuffer();
		for (int i = 0; i < size; i++)
			inStrBuffer.append("?,");
		String inStr = inStrBuffer.toString();
		return inStr.substring(0, inStr.lastIndexOf(','));
	}

	/**
	 * Returns the values in the collection in SQL IN clause compliant string
	 */
	@SuppressWarnings("rawtypes")
	public String getSqlInValuesStr(Collection values) {
		StringBuffer inStrBuffer = new StringBuffer();
		Iterator valuesIt = values.iterator();
		while (valuesIt.hasNext()) {
			Object value = valuesIt.next();
			if (value instanceof String)
				inStrBuffer.append("'" + (String) value + "',");
			else
				// for non string ??
				inStrBuffer.append(value + ",");
		}
		String inStr = inStrBuffer.toString();
		return inStr.substring(0, inStr.lastIndexOf(','));
	}
}
