/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.constants;

/**
 * Constant file to keep system param keys
 * 
 * @author Lasantha Pambagoda
 */
public class SystemParamKeys {

	/** +----------------------------------------------------------------+ */
	/** | System Parameters | */
	/** +----------------------------------------------------------------+ */
	public static final String FLT_SEARCH_VARIANCE_DEF_IBE = "SYSTEM_1";

	public static final String FLT_SEARCH_VARIANCE_DEF_XBE = "SYSTEM_2";

	public static final String FLT_SEARCH_VARIANCE_IBE = "SYSTEM_3";

	public static final String FLT_SEARCH_VARIANCE_XBE = "SYSTEM_4";

	public static final String OPTIMIZE_MAX_FLIGHTS = "SYSTEM_9";

	public static final String NO_OF_ATTEMPT_CC_PAYMENT = "SYSTEM_10";

	public static final String DATE_FORMAT_FOR_DATE_STRINGS = "SYSTEM_12";

	public static final String ACCELAERO_CLIENT_IDENTIFIER = "SYSTEM_13";

	public static final String GDS_INTEGRATED = "SYSTEM_14";

	public static final String SYSTEM_TYPE = "SYSTEM_15";

	public static final String IS_ACCELAERO_SPY_ENABLED = "SYSTEM_16";

	public static final String DEFAULT_LANGUAGE = "SYSTEM_17";

	public static final String SYSTEM_LOG_TRACKING = "SYSTEM_18";

	public static final String PG_GMT_CUTOVER_TIME = "PG_1";

	public static final String PAYMENT_GATEWAY_TYPE_CODE_IBE = "PG_2";

	public static final String PAYMENT_GATEWAY_TYPE_CODE_XBE = "PG_3";

	public static final String ACCOUNT_CODE = "FINANCE_1";

	public static final String CARRIER_AGENT = "AGENT_1";

	public static final String ROUNDUP_CHINF_FARE = "SYSTEM_19";

	public static final String EXTERNAL_SSM_RECAP_INTEGRATED = "SYSTEM_20";

	public static final String FARE_RULE_DETAILS_IN_API = "SYSTEM_21";

	public static final String USE_MODIFIED_QUERY_FOR_BEST_OFFERS = "SYSTEM_22";
	
	public static final String ENVIRONMENT_IDENTIFICATION_CODE = "SYSTEM_23";
	
	public static final String ENABLE_EVENT_PUBLICATION = "SYSTEM_24";

	/** +----------------------------------------------------------------+ */
	/** | Scheduled Services | */
	/** +----------------------------------------------------------------+ */
	public static final String MINIMUM_TIME_ALLOWED_FOR_SCHEDULE = "SCHD_1";

	public static final String DEFAULT_OPERATION_TYPE = "SCHD_4";

	public static final String DEFAULT_SCHEDULE_STATUS = "SCHD_5";

	public static final String DEFAULT_FLIGHT_STATUS = "SCHD_6";

	public static final String QUERY_DR_REFUND = "SCHD_7";

	public static final String QUERY_DR_SCHED_TIME_MIN = "SCHD_8";

	public static final String CLEAR_TMPSEATS_INTERVAL = "SCHDSERV_4"; // TODO remove this

	public static final String PNL_DEPARTURE_GAP = "SCHDSERV_1";

	public static final String ADL_INTERVAL = "SCHDSERV_2";

	public static final String LAST_ADL = "SCHDSERV_3";

	public static final String FLIGHT_CLOSURE_DEPARTURE_GAP = "SCHDSERV_6";

	public static final String BLOCK_UNSETTLED_AGENTS = "SCHDSERV_7";

	public static final String TRANSFER_INVOICES = "SCHDSERV_8";

	public static final String SCHEDULE_REPORTS = "SCHDSERV_9";

	public static final String ADJ_AGT_CREDITLIMIT_AND_BANKGUARANTEE_WHEN_EX_RATE_CHD = "SCHDSERV_10";

	public static final String CONFIRM_RESERVATION_IF_PAYMENT_EXISTS_AT_IPG = "SCHDSERV_11";

	public static final String PAYMENT_STATUS_UPDATE_HOLD_MIN = "SCHDSERV_12";

	public static final String ADL_INTERVAL_AFTER_CUTOFF_TIME = "SCHDSERV_13";

	public static final String PAL_DEPATURE_GAP = "SCHDSERV_14";

	public static final String CAL_INTERVAL = "SCHDSERV_15";

	public static final String LAST_CAL = "SCHDSERV_16";

	public static final String CAL_INTERVAL_AFTER_CUTOFF_TIME = "SCHDSERV_17";

	public static final String ACI_SEATMAP_GAP = "SCHDSERV_18";

	public static final String ACI_SEND_STATUS_GAP = "SCHDSERV_19";

	/** +----------------------------------------------------------------+ */
	/** | Sales Parameters | */
	/** +----------------------------------------------------------------+ */
	public static final String PAYMENT_TERMS = "SALES_2";

	@Deprecated
	public static final String ACCOUNT_NO = "SALES_3";

	public static final String ACCOUNT_NAME = "SALES_4";

	@Deprecated
	public static final String BANK_NAME = "SALES_5";

	@Deprecated
	public static final String BANK_ADDRESS = "SALES_6";

	@Deprecated
	public static final String SWIFT_CODE = "SALES_7";

	public static final String NUMBER_OF_INVOICES_TO_CONSIDER_FOR_BLOCK_OPERATION = "SALES_8";

	public static final String INVOICE_NO_FORMAT = "SALES_9";

	public static final String INVOICE_NO_GENERATION_METHOD = "SALES_10";

	public static final String INVOICE_ADDITIONAL_DETAILS = "SALES_11";

	public static final String VIEW_CASH_SALES_EXPORT = "SALES_12";

	public static final String SETTLEMENT_BANK_INFORMATION = "SALES_13";

	/** +----------------------------------------------------------------+ */
	/** | Airline Parameters | */
	/** +----------------------------------------------------------------+ */

	public static final String MIN_STOP_OVERTIME = "AIRLINE_2";

	public static final String ADMIN_EMAIL_ID = "AIRLINE_3";

	public static final String BASE_CURRENCY = "AIRLINE_4";

	public static final String HUB_FOR_THE_SYSTEM = "AIRLINE_5";

	public static final String OFFSET_FROM_THE_STANDED_FIRST_DAY_OF_WEEK = "AIRLINE_6";

	public static final String COMP_ADD1 = "AIRLINE_7";

	public static final String COMP_ADD2 = "AIRLINE_8";

	public static final String COMP_EMAIL = "AIRLINE_9";

	public static final String COMP_TEL = "AIRLINE_10";

	public static final String AIRLINE_URL = "AIRLINE_11";

	public static final String KIOSK_HOME_PAGE_URL = "AIRLINE_12";

	public static final String DEFAULT_CARRIER_CODE = "AIRLINE_13";

	public static final String REPORT_LOGO = "AIRLINE_14";

	public static final String CARRIER_NAME = "AIRLINE_15";

	public static final String DISPLAY_SSR_REMAKRS_IN_FLIGHT_MANIFEST = "AIRLINE_16";

	public static final String CURRENCY_ROUNDUP = "AIRLINE_17";

	public static final String ALERT_ON_ROLE_CHANGE = "AIRLINE_18";

	public static final String EMAIL_ADDRESS_FOR_ALERT_ON_ROLE_CHANGE = "AIRLINE_19";

	public static final String ENGINE_VISIBILITY = "AIRLINE_20";

	public static final String REPORTING_PERIOD = "AIRLINE_21";

	public static final String CHILD_VISIBILITY = "AIRLINE_22";

	public static final String AIRLINE_DOMAIN = "AIRLINE_23";

	public static final String SHOW_CARRIER_LOGO_WITH_FLT_INFO = "AIRLINE_24";

	public static final String ACCELAERO_IBE_URL = "AIRLINE_25";

	public static final String SHOW_OPERATING_CARRIER_LEGEND = "AIRLINE_26";

	public static final String SHOW_SEAT_MAP = "AIRLINE_27";

	public static final String SHOW_TRAVEL_INSURANCE = "AIRLINE_29";

	public static final String ALLOW_REPROTECT_PAX_TO_TODAYS_DEPARTURES = "AIRLINE_30";

	public static final String AGENT_COMMISSION = "AIRLINE_31";

	public static final String ENABLE_HOLIDAYS_URL_IN_IBE = "AIRLINE_32";

	public static final String STORE_PAYMENTS_IN_AGENTS_CURRENCY = "AIRLINE_33";

	public static final String INVOICE_IN_AGENTS_CURRENCY = "AIRLINE_34";

	public static final String REPORTS_IN_AGENTS_CURRENCY = "AIRLINE_35";

	public static final String ALLOW_LOCAL_CREDIT_LIMIT_DEF = "AIRLINE_36";

	public static final String SHOW_BREAKDOWN = "AIRLINE_37";

	public static final String MAX_ADMIN_USER_COUNT = "AIRLINE_38";

	public static final String HIDE_STOP_OVER_SEG = "AIRLINE_39";

	public static final String HIDE_COMPANY_ADDR_IN_ITNR = "AIRLINE_40";

	public static final String DISPLAY_IBE_LISTS_DYNAMICALLY = "AIRLINE_41";

	public static final String SEND_CC_DETAILS_WITH_PNL = "AIRLINE_42";

	public static final String INVOICE_EMAIL_REPLY_ADDRESS = "AIRLINE_43";

	public static final String ENABLE_RENT_A_CAR_URL_IN_IBE = "AIRLINE_44";

	public static final String SHOW_MEAL = "AIRLINE_45";

	public static final String SHOW_BAGGAGE = "AIRLINE_82";

	public static final String MEAL_IMG_PATH = "AIRLINE_46";

	public static final String SHOW_FARE_DEF_BY_DEP_COUNTRY_CURRENCY = "AIRLINE_47";

	public static final String IBE_ENABLE_LOYALITY = "AIRLINE_49";

	public static final String INTEGRATE_MASHREQ_WITH_LMS = "AIRLINE_248";

	public static final String CARRIERS_TO_SEND_PNL_ADL = "AIRLINE_51";

	public static final String WEBSERVICE_EXTRA_VALIDATION_SKIP_AGENCIES = "AIRLINE_52";

	public static final String SHOW_IN_FLIGHT_SERVICES = "AIRLINE_53";

	public static final String MULTIPLE_MEAL_SELECTION = "AIRLINE_54";

	public static final String WEBSERVICE_FRAUD_ENABLE_AGENCIES = "AIRLINE_55";

	public static final String INCLUDE_STANDBY_PAX_IN_PNL = "AIRLINE_56";

	public static final String ITINERARY_ADVERTISEMENT_IMAGE_URL = "AIRLINE_57";

	public static final String ENABLE_INV_CHECK_FOR_SSR = "AIRLINE_58";

	public static final String ENABLE_HOLIDAY_INTEGRATION = "AIRLINE_59";

	public static final String ADJUSTMENT_CARRIER_LIST = "AIRLINE_60";

	public static final String FTP_FOR_GENERATED_REVENUE_REPORT = "AIRLINE_61";

	public static final String FTP_SERVER_LOGIN_DETAILS = "AIRLINE_62";

	public static final String REPORT_URL_PREFIX = "AIRLINE_63";

	public static final String ANCI_REMINDER_SCHEDULER_TIME_DURATION = "AIRLINE_64";

	public static final String ENABLE_ANCI_REMINDER = "AIRLINE_65";

	public static final String RAK_INSURANCE = "AIRLINE_66";

	public static final String ENABLE_GROUND_SERVICE = "AIRLINE_67";

	public static final String DEFAULT_TIMEZONE = "AIRLINE_68";

	public static final String EC_FRAUD_CHK_ENABLED_CHANNELS = "AIRLINE_69";

	public static final String PERCENTAGE_WISE_MODIFICATION_CHARGES = "AIRLINE_70";

	public static final String DEFAULT_AIRLINE_IDENTIFIER_CODE = "AIRLINE_71";

	public static final String CREDTI_CONSUME_AGENTS = "AIRLINE_72";

	public static final String MAINTAIN_AIRLINE_CARRIER_PREFIX = "AIRLINE_73";

	public static final String TICKET_FORMAT = "AIRLINE_74";

	public static final String EXCHANE_RATE_AUTOMATION_ENABLED = "AIRLINE_75";

	public static final String FOC_MEAL_SELECTION_ENABLED = "AIRLINE_76";

	public static final String TICKET_PRINT_EMAIL_PAX_WISE_FORMAT = "AIRLINE_77";

	public static final String DYNAMIC_OND_LIST_CONFIGS = "AIRLINE_78";

	public static final String SHOW_INSURANCE_OPTIONS_IN_UI = "AIRLINE_79";

	public static final String MEAL_NOTIFY_FOR_HUBS = "AIRLINE_80";

	public static final String INSURANCE_PROVIDER = "AIRLINE_81";

	public static final String DISPLAY_CONTACT_DETAILS_ON_PNR_SUMMARY = "AIRLINE_83";

	public static final String HOLIDAY_ANCILLARY_REMINDER_URL = "AIRLINE_84";

	public static final String COUNTRY_PAX_CONFIG_ENABLED = "AIRLINE_94";

	public static final String SHOW_INFANT_E_TKT_IN_PNL_ADL = "AIRLINE_95";

	public static final String ALLOW_EMAIL_AGENTS_FLIGHT_REPROTECT = "AIRLINE_96";

	public static final String DESCRIPTIVE_INVENTORY_AUDIT_ENABLED = "AIRLINE_97";

	public static final String BSP_DEFAULT_FARE_BASIS_CODE = "AIRLINE_85";

	public static final String BSP_TICKETING_AIRLINE_CODE = "AIRLINE_86";

	public static final String BSP_SFTP_CREDENTIALS = "AIRLINE_87";

	public static final String E_TICKET_FORM_CODE = "AIRLINE_88";

	public static final String MAINTAIN_AGENT_BSP_CREDIT_LIMIT = "AIRLINE_89";

	public static final String BSP_ENABLING_PARAMS_FOR_AIRLINE = "AIRLINE_90";

	public static final String BSP_ENHANCED_USER_ENABLED = "AIRLINE_91";

	public static final String AGENT_WISE_TICKET_ENABLE = "AIRLINE_92";

	public static final String PAPER_TICKET_FORM_CODE = "AIRLINE_93";

	public static final String CANCEL_OUT_OF_SEQUNCE_SEGMENTS = "AIRLINE_98";

	public static final String VOID_RESERVATIONS = "AIRLINE_99";

	public static final String MODIFICATION_FILTER_ENABLE = "AIRLINE_101";

	public static final String GROUP_CHARGES_BY_REPORTING_CHARGE_GROUP_CODE_ENABLED = "AIRLINE_102";

	public static final String UPDATE_TO_FLOWN_EVEN_BAL_PAY_EXISTS = "AIRLINE_103";

	public static final String ENABLE_LINKED_FARES_BASED_ON_MASTER_FARES = "AIRLINE_104";

	public static final String FLIGHT_TYPE_AVAILABILITY = "AIRLINE_105";

	public static final String ENABLE_LOGICAL_CABIN_CLASS = "AIRLINE_106";

	public static final String ALLOW_ONLY_AVAILABLE_FIXED_SEATS_AND_SKIP_STD_NONSTD_SEATS = "AIRLINE_107";

	public static final String FLIGHT_MANIFEST_WINDOW_DISPLAY_OPTIONS = "AIRLINE_108";

	public static final String SPLIT_FARE_SALES_VALIDITY = "AIRLINE_109";

	public static final String SHOW_FWD_BOOK_REPORT_ADDITIONAL_DETAIL = "AIRLINE_110";

	public static final String PFS_PROCESS_CONFIG = "AIRLINE_111";

	public static final String BANK_GUARANTEE_NOTIFICATION_EMAIL_STATUS = "AIRLINE_112";

	public static final String BANK_GUARANTEE_EXPIRY_NOTIFY_INTERVALS = "AIRLINE_113";

	public static final String CREDIT_LIMIT_UTILIZATION_PERCENTAGES = "AIRLINE_114";

	public static final String ENABLE_RBD_PNL_ADL = "AIRLINE_115";

	public static final String SPLIT_ADULT_INFANT_RATIO = "AIRLINE_116";

	public static final String SHOW_FLIGHT_USER_NOTE = "AIRLINE_117";

	public static final String UPDATE_TAX_AMOUNT_WITH_ROE_CHANGES = "AIRLINE_118";

	public static final String SHOW_FLT_STOP_OVER_DETAIL = "AIRLINE_119";

	public static final String EMAIL_AGENT_ON_RESERVATION_MODIFICATION = "AIRLINE_120";

	public static final String ENABLE_AIRCRAFT_CAPACITY_DOWNGRADE_TO_DIFFERENT_CC = "AIRLINE_121";

	public static final String HIDE_ALLOCATE_SCREEN_EXTRA_DETAIL = "AIRLINE_122";

	public static final String SHOW_BAGGAGE_ALLOWANCE_REMARKS = "AIRLINE_123";

	public static final String SHOW_OND_BAGGAGES = "AIRLINE_124";

	public static final String LOAD_RESERVATION_ORDER_BY_PAX_NAME = "AIRLINE_125";

	public static final String AUTO_SEAT_ASSIGNMENT = "AIRLINE_126";

	public static final String ENABLE_DUPLICATE_PASSPORT_NUMBER_CHECK_ONLY_WITHIN_ADULT = "AIRLINE_127";

	public static final String LOAD_PAX_NAMES_IN_BLOCK_LETTERS = "AIRLINE_128";

	public static final String SHOW_BC_OVERBOOK_IN_INVENTORY = "AIRLINE_129";

	public static final String ENABLE_FLEXI_FARES = "AIRLINE_130";

	public static final String ENABLE_WEEKLY_INVOICE = "AIRLINE_131";

	public static final String DAY_NUMBER_TO_GENERATE_WEEKLY_INVOICE = "AIRLINE_132";

	public static final String MAX_FLIGHT_NUMBER_LENGTH = "AIRLINE_133";

	public static final String LOGICAL_CABIN_CLASS_INVENTORY_RESTRICTED = "AIRLINE_134";

	public static final String ENABLE_BAGGAE_SELECTION_FOR_SBY_BOOKINGS = "AIRLINE_135";

	public static final String ENABLE_SSR_SELECTION_FOR_SBY_BOOKINGS = "AIRLINE_136";

	public static final String SHOW_FARE_DISCOUNT_CODE_LIST = "AIRLINE_137";

	public static final String ENABLE_MULTIPLE_GSA_FOR_A_STATION = "AIRLINE_138";

	public static final String HIDE_BASE_CURRENCY_DECIMALS_IN_ITINERARY = "AIRLINE_139";

	public static final String CANCEL_NOSHOW_SEGMENT = "AIRLINE_140";

	public static final String DISPLAY_TIME_USING_PERSIAN_LANGUAGE_AND_FORMAT = "AIRLINE_141";

	public static final String DISPLAY_ONLY_ALLOCATED_LOGICAL_CABIN_CLASSES = "AIRLINE_142";

	public static final String PROMOTIONS_NEXT_SEAT_FREE_ROWS_TO_SKIP = "AIRLINE_143";

	public static final String BANK_GURANTEE_CREDIT_LIMIT_CC_ADDRESS = "AIRLINE_144";

	public static final String BSP_OPERATION_STATUS_AND_COUNTRY_CODE = "AIRLINE_145";

	public static final String SET_ORIGIN_USER_AS_SCHEDULER_FOR_GOSHOW_BOOKINGS = "AIRLINE_146";

	public static final String SKIP_INV_CHK_FOR_FLOWN_IN_REQUOTE = "AIRLINE_147";

	public static final String SET_BC_FOR_EXISTING_SEGMENTS = "AIRLINE_148";

	public static final String SHOW_PAYMENT_METHODS_ICONS = "AIRLINE_149";

	public static final String SHOW_ANCILLARY_BREAKDOWN = "AIRLINE_150";

	public static final String DISPLAY_FAREBASIS_CODES_IN_COMPANY_PAYMENTS = "AIRLINE_151";

	public static final String UPDATE_INF_STATE_WHEN_PROCESS_PARENT_PFS = "AIRLINE_152";

	public static final String SET_PAX_CREDIT_AUTO_REFUNDABLE_TO_AGENTS = "AIRLINE_153";

	public static final String PAX_CREDIT_AUTO_REFUNDABLE_ALLOWED_AGENT_TYPES = "AIRLINE_154";

	public static final String SHOW_PAX_NAMES_IN_UPPER_CASE_ON_FLT_MANIFEST = "AIRLINE_156";

	public static final String ADJUST_FARE_DIFFERENCE_WHILE_CABIN_CLASS_CHANGE = "AIRLINE_157";

	public static final String ADD_INSURANCE_FOR_INFANTS = "AIRLINE_158";

	public static final String AUTO_CANCELLATION_TRIGGER_TIME_FROM_FLIGHT_DEPARTURE_TIME = "AIRLINE_159";

	public static final String PROCESS_PFS_AFTER_ETL_PROCESSED = "AIRLINE_160";

	public static final String ENABLE_NS_AUTOREFUND_HOLDPROCESS = "AIRLINE_161";

	public static final String SHOW_NESTED_AVAILABLE_SEATS = "AIRLINE_162";

	public static final String SKIP_UNTUTCHED_SECTOR_FARES_IN_REQUOTE_FLOW = "AIRLINE_163";

	public static final String PROMOTION_MODULE_ENABLED = "AIRLINE_164";

	public static final String PROMOTION_FLEXI_DATE_APPROVAL_CUTOFF = "AIRLINE_165";

	public static final String PROMOTION_NEXT_SEAT_FREE_APPROVAL_CONF = "AIRLINE_166";

	public static final String AUTO_REFUND_VOID_RESERVATIONS = "AIRLINE_167";

	public static final String ENABLE_SAME_FARE_MODIFICATION = "AIRLINE_168";

	public static final String ROUNDUP_CHILD_AND_INFANT_FARES_WHEN_CALCULATING_FROM_ADULT_FARE = "AIRLINE_169";

	public static String AUTO_REFUND_NORMAL_TO_RES_OWNER = "AIRLINE_170";

	public static String AUTO_REFUND_VOID_TO_RES_OWNER = "AIRLINE_171";

	public static final String NEXT_SEAT_FREE_FLEXI_DATE_NOTIFIER_CONFIGS = "AIRLINE_172";

	public static String ENABLE_ANCILLARY_PREFERENCE_MESSAGE = "AIRLINE_173";

	public static final String BAGGAGE_SELECTION_CRITERION = "AIRLINE_174";

	public static final String ACTIVE_ACTIVE_ENABLE = "AIRLINE_175";

	public static final String ANCI_OFFER_SWITCH = "AIRLINE_176";

	public static String SKIP_UNTUTCHED_OND_IN_REQUOTE_FLOW = "AIRLINE_177";

	public static final String ENABLE_RBD_NESTING_FOR_RETURN_FARES = "AIRLINE_178";

	public static String ENABLE_ROUND_UP_OVERWRITE_FEE = "AIRLINE_179";

	public static String SHOW_PASSENGER_CONTACT_DETAILS_IN_ITINERARY = "AIRLINE_180";

	public static String SHOW_STATION_CONTACT_DETAILS_IN_ITINERARY = "AIRLINE_181";

	public static final String SHOW_NO_FARE_SPLIT_OPTION = "AIRLINE_182";

	public static final String SHOW_SELECTED_CURRENCY_OPTION = "AIRLINE_183";

	public static final String APPLY_COOKIE_BASED_SURCHARGES_FOR_IBE = "AIRLINE_184";

	public static final String ENABLE_IBE_EXITPOPUP = "AIRLINE_185";

	public static final String ENABLE_FARE_PRORATION = "AIRLINE_186";

	public static final String APPLY_EXTENDED_NAME_MODIFICATION_FUNCTIONALITY = "AIRLINE_187";

	public static final String SEAT_AUTO_ASSIGNMENT_CONFIGS = "AIRLINE_188";

	public static final String ENABLE_USER_DEFINE_TRANSIT_TIME = "AIRLINE_189";

	public static final String IS_BUNDLED_FARES_ENABLED = "AIRLINE_190";

	public static String SKIP_PAX_TYPE_FOR_PFS_PROCESSING = "AIRLINE_191";

	public static final String MATCH_EVEN_OFFERED_FARE_TO_APPLY_COOKIE_BASED_SURCHARGES = "AIRLINE_192";

	public static String ENABLE_DOWNGRADE_AIRCRAFTMODEL = "AIRLINE_193";

	public static final String ENABLE_FLIGHT_DETAILS_PUBLISHING = "AIRLINE_194";

	public static final String ENABLE_CARGO_FLASH_PUBLISHING = "AIRLINE_195";

	public static final String ENABLE_EXTERNAL_INTERNATIONAL_FLIGHT_CAPTURING = "AIRLINE_196";

	public static final String IATA_EMDS_FORM_CODE = "AIRLINE_197";

	public static final String IS_FLEXI_ENABLE_IN_ANCI = "AIRLINE_198";

	public static final String ENABLE_AUTO_DATE_FILL_IN_PAX_REPROTECT_FLIGHT_SEARCH_SCREEN = "AIRLINE_199";

	public static final String USER_ACCOUNT_SECURITY = "AIRLINE_209";

	public static final String Fare_Valid_Till = "AIRLINE_200";

	public static final String Target_Payment_Date = "AIRLINE_201";

	public static final String IS_REPORTING_DB_AVAILABLE = "AIRLINE_202";

	public static final String REFUND_NON_REFUND_FARE_IN_EXCHAGE = "AIRLINE_203";

	public static final String GSA_STRUCTURE_V2_ENABLED = "AIRLINE_204";

	public static final String DEFAULT_SELECTED_COS_AIRADMIN_SETUP_FARE_SCREEN = "AIRLINE_205";

	public static final String MAINTAIN_SEPERATE_ADL_TRANSMISSION_REPEAT_INTERVAL_AFTER_CUTOFF_TIME = "AIRLINE_206";

	public static final String PAYMENT_SETTLEMENT_PRIORITY = "AIRLINE_207";

	public static final String NO_OF_COOKIE_EXPIRY_DAYS = "AIRLINE_208";

	public static final String ALLOW_LOAD_RESERVATION_FROM_MANIFEST = "AIRLINE_210";

	public static final String SEND_INWARD_SURFACE_SEGMENT_DETAILS_IN_PNLADL = "AIRLINE_211";

	public static final String SEND_ONWARD_SURFACE_SEGMENT_DETAILS_IN_PNLADL = "AIRLINE_212";

	public static final String ENABLE_ROUNDINGUP_CHARGES_CALCULATED_IN_BASE_CURRENCY = "AIRLINE_214";

	public static final String UPDATE_GROUND_SEG_STATUS_WHEN_AIR_SEG_IS_FLOW_IN_PFS_PROCESS = "AIRLINE_215";

	public static final String AIRLINE_OWN_CARRIER_CODES = "AIRLINE_216";

	public static final String SHOW_CHARGES_PAYMENTS_IN_ITINERARY = "AIRLINE_217";

	public static final String ENABLE_SPLIT_FARE_SALES_VALIDITY_IMMEDIATE_EFFECT = "AIRLINE_218";

	public static final String SPLIT_FARE_SALES_VALIDITY_IMMEDIATE_EFFECT_CUTOFF_TIME = "AIRLINE_219";

	public static final String DISPLAY_DELIVERY_METHOD_IN_SITA_CONFIGURAION_SCREEN = "AIRLINE_220";

	public static final String ENABLE_COOKIE_SAVE_FUNCTIONALITY = "AIRLINE_221";

	public static final String ENABLE_OFFLINE_REPORT_FOR_PAST_DATES = "AIRLINE_223";

	public static final String SHOW_MOBILE_AREA_CODE_PREFIX = "AIRLINE_224";

	public static final String SKIP_ANCI_MODIFICATION_CUTOFF_FOR_NOREC = "AIRLINE_225";

	public static final String ENABLE_LCC_DATA_SYNC = "AIRLINE_226";

	public static final String TRACKING_ENABLED = "AIRLINE_227";

	public static final String EMAIL_DOMAIN_VALIDATION = "AIRLINE_228";

	public static final String STOP_REFUNDING_TOTAL_PAYMENT_WHEN_NOSHOW_RULE_APPLIES = "AIRLINE_229";

	public static final String DISPLAY_SPECIFIC_ERRORS_FOR_INCORRECT_LOGIN = "AIRLINE_230";

	public static final String REPORT_WAIT_PARAMETERS = "AIRLINE_231";

	public static final String ENABLE_LOYALTY_MANAGEMENT_SYSTEM = "AIRLINE_232";

	public static final String LOYALTY_POINTS_CONVERSION_RATES = "AIRLINE_233";

	public static final String ENABLE_RET_FILE_GENERATION_FOR_CUSTOM_NOMINAL_CODES = "AIRLINE_234";

	public static final String MAINTAIN_SSR_WISE_CUTOFF_TIME = "AIRLINE_235";

	public static final String ENABLE_BSP_CREDIT_LIMIT_ALERTS = "AIRLINE_236";

	public static final String ENABLE_POS_WEB_FARES = "AIRLINE_237";

	public static final String SHOW_TICKET_DETAILS_IN_PNL_ADL = "AIRLINE_238";

	public static final String SHOW_THUMBNAIL_LOGO_IN_ITINERARY = "AIRLINE_240";

	public static final String ENABLE_ITINERARY_ADVERTISEMENT = "AIRLINE_239";

	public static final String ENABLE_BLACKLIST_PAX = "AIRLINE_242";

	public static final String ENABLE_DEFAULT_INV_CREATION_WITH_BUILD_FLIGHTS = "AIRLINE_243";

	public static final String ENABLE_VOUCHER_AND_GIFT_VOUCHER = "AIRLINE_244";

	public static final String ENABLE_OTP_FOR_GIFT_VOUCHER = "AIRLINE_354";

	public static final String ENABLE_VOUCHER_PAX_NAME_VALIDATION = "AIRLINE_245";

	public static final String ENABLE_SEGMENT_MODIFICATION_FOR_CANCELLED_FLIGHT_SEGMENTS = "AIRLINE_241";

	public static final String PASSENGER_SELF_REPROTECTION = "AIRLINE_246";

	public static final String SHOW_CHARGES_IN_ITINERARY = "AIRLINE_247";

	public static final String ENABLE_PAYAMENT_METHOD_CAPTURED_FROM_RECEIPT_NUMBER = "AIRLINE_251";

	public static final String ENABLE_SEAT_MEAL_PREFERENCE = "AIRLINE_249";

	public static final String ENABLE_ADD_FAMILY_MEMBERS = "AIRLINE_250";

	public static final String APPLY_NO_SHOW_RULES_FOR_CODE_SHARE_PNR = "AIRLINE_252";

	public static final String IATA_AIRCRAFT_TYPE_MANDATORY = "AIRLINE_253";

	public static final String ENABLE_CACHING_FOR_OND_PUBLISHED = "AIRLINE_255";

	public static final String DISPLAY_OTHER_AIRLINE_SEGMENTS = "AIRLINE_256";

	public static final String SEND_IBOB_CONNECTIONS_FOR_OTHER_AIRLINE_SEGMENTS_IN_PNL_ADL = "AIRLINE_257";

	public static final String ITINERARY_AIRLINE_URL = "AIRLINE_258";

	public static final String COMMON_BOOKING_CLASS_CODES_STRING = "AIRLINE_259";

	public static final String SPLIT_FROM_SUB_SCHEDULES = "AIRLINE_260";

	public static final String SEND_CNL_MSG_FOR_RE_SCHEDULE = "AIRLINE_261";

	public static final String CURRENCY_EXRATE_TIMEOUT_FOR_SERVICE_APP = "AIRLINE_262";

	public static final String CANCEL_EXISTING_SCHEDULE_ON_DST_CHANGE = "AIRLINE_263";

	public static final String GOOGLE_TAG_MANAGER_KEY = "AIRLINE_264";

	public static final String ENABLE_SPLIT_SCHEDULE_FOR_DST_CHANGES = "AIRLINE_265";

	public static final String SKIP_DURATION_TOLERANCE_FOR_SHEDULE_MSGS = "AIRLINE_266";

	public static final String ENABLE_SCHEDULE_MSG_PROCESS_ERROR_EMAIL = "AIRLINE_267";

	public static final String SCHEDULE_MSG_PROCESS_ERROR_EMAIL_RECIPIENT = "AIRLINE_268";

	public static final String GOOGLE_RECAPTCHA_CONFIG = "AIRLINE_269";

	public static final String ALLOW_AUTO_PUBLISH_SCHEDULE = "AIRLINE_270";

	public static final String ENABLE_OFFICER_EMAIL_ALERT_AT_FLIGHT_CNX = "AIRLINE_271";

	public static final String ALLOW_REPROTECT_POST_CNX = "AIRLINE_272";

	public static final String IS_OFFICER_CONATACT_MOBILE_NUM_MANDATORY = "AIRLINE_273";

	public static final String IS_OFFICER_CONATACT_EMAIL_MANDATORY = "AIRLINE_274";

	public static final String ENABLE_ATTACH_ROUTE_WISE_DEFAULT_ANCI_TEMPLATE_SSIM_PROCESSING = "AIRLINE_275";

	public static final String ENABLE_SEND_SSM_ASM_SUB_MESSAGES = "AIRLINE_276";

	public static final String ENABLE_OVERLAPPING_BY_SSM_ASM = "AIRLINE_277";

	public static final String CARRIERS_TO_SEND_PAL_CAL = "AIRLINE_278";

	public static final String MAINTAIN_SEPERATE_CAL_TRANSMISSION_REPEAT_INTERVAL_AFTER_CUTOFF_TIME = "AIRLINE_279";

	public static final String ENABLE_MSG_QUEUING_SSM_ASM = "AIRLINE_280";

	public static final String SPLIT_SCHEDULE_FOR_SCHEDULE_CONFLICT_WHEN_PROCESS_SSM = "AIRLINE_281";

	public static final String ENABLE_AEROMART_PAY = "AIRLINE_282";

	public static final String ENABLE_PALCAL_MESSAGE = "AIRLINE_283";

	public static final String CLIENT_CALL_CENTER_URL = "AIRLINE_284";

	public static final String ENABLE_DELAY_PROCESSING_SSM_ASM_CNL_MSG = "AIRLINE_285";

	public static final String SSM_ASM_CNL_ACTION_PROCESSING_HOLD_TIME = "AIRLINE_286";

	public static final String DISPLAY_GDS_RLOC_COMPANY_PAYMENTS_REPORT = "AIRLINE_287";

	public static final String AEROMART_PAY_CONFIGS = "AIRLINE_288";

	public static final String EXTERNAL_AGENT_CREDIT_BLOCK_DURATION = "AIRLINE_289";

	public static final String REPORTS_STORAGE_LOCATION = "AIRLINE_290";

	public static final String ENABLE_ELECTRONIC_MCO = "AIRLINE_291";

	public static final String MCO_FORM_CODE = "AIRLINE_292";

	public static final String XBE_LOGIN_CAPTCHA_ENABLE = "AIRLINE_293";

	public static final String OND_SPLIT_FOR_BUS_FARE_QUOTE = "AIRLINE_296";

	public static final String ASSIGN_DEFAULT_MEAL_CHARGES_TEMPLATE = "AIRLINE_297";

	public static final String ASSIGN_DEFAULT_SEAT_CHARGES_TEMPLATE = "AIRLINE_298";

	public static final String DISPLAY_MULTILEG_STOPOVER_DETAILS = "AIRLINE_294";

	public static final String ENABLE_FARE_RULE_COMMENTS = "AIRLINE_295";

	public static final String ENABLE_SEQUENTIAL_PROCESSING_FOR_IN_MSG = "AIRLINE_299";

	public static final String SEQUENTIAL_PROCESSING_ALLOWED_MSG_TYPES = "AIRLINE_300";

	public static final String PAX_WISE_TICKETING_ENABLED = "AIRLINE_301";

	public static final String DISPLAY_FLEXI_AS_SEPERATE_BUNDLE = "AIRLINE_302";

	public static final String AIRADMIN_LOGIN_CAPTCHA_ENABLE = "AIRLINE_307";

	public static final String ONHOLD_EMAIL_NOTIFICATION_PROPERTIES = "AIRLINE_306";

	public static final String ENABLE_ROUTE_SELECTION_FOR_AEGNT = "AIRLINE_305";

	public static final String ENABLE_FLIGHTS_IN_ALLOWED_ROUTES_FOR_AGENTS_IN_MANIFEST = "AIRLINE_308";

	public static final String ENABLE_AEROMART_ETS = "AIRLINE_309";

	public static final String RESTRICTED_PAYMENT_MODES = "AIRLINE_310";

	public static final String ENABLE_BUNDLE_FARE_POPUP = "AIRLINE_311";

	public static final String SEND_REPORT_LINK_WITH_DOMAIN_NAME = "AIRLINE_312";

	public static final String ENABLE_IATA_MEAL_CODES_FOR_PNL = "AIRLINE_313";

	public static final String GST_GOODS_SERVICES = "AIRLINE_315";

	public static final String GST_HSN_SAC = "AIRLINE_316";

	public static final String ENABLE_CHANNEL_WISE_DEFAULT_BUNDLED_SELECTION = "AIRLINE_317";

	public static final String SKIP_DUMMY_PAYMENTS = "AIRLINE_318";

	public static final String NON_TICKETING_REVENUE_GST_QUOTABLE_CHARGE_GROUPS = "AIRLINE_319";

	public static final String SHOW_PAYMENT_DATE_WITH_TIMESTAMP = "AIRLINE_320";

	public static final String SHOW_AGENT_CREDIT_IN_AGENT_CURRENCY = "AIRLINE_321";

	public static final String ENABLE_SSM_FOR_SPLIT_SCHEDULE = "AIRLINE_322";

	public static final String DISPLAY_ANCI_SURCHARGES_PENALTY_IN_SEPERATE_COLUMNS = "AIRLINE_323";

	public static final String ENABLE_DETAIL_AUDIT_FOR_PROCESSING_SCHEDULE_MSG = "AIRLINE_324";

	public static final String CANCEL_LMS_BLOCKED_CREDITS_CUTOFF_TIME = "AIRLINE_331";

	public static final String DISPLAY_OWNER_AGENT_COLUMN_IN_REV_TAX_REPORT = "AIRLINE_328";

	public static final String RESTRICT_MODIFICATION_FOR_CHECKEDIN_BOARDED_SGEMENTS = "AIRLINE_314";

	public static final String ENABLE_DISPLAY_CONFIGURATION_CREDIT_DEBIT_ENTRIES = "AIRLINE_329";

	public static final String ENABLE_CITY_BASED_FUNCTIONALITY = "AIRLINE_335";

	public static final String AIRLINE_RESERVATION_URL = "AIRLINE_338";

	public static final String EXCLUDE_STANDBY_PAX = "AIRLINE_330";

	public static final String SHOW_FCC_SEG_BC_INVENTORY_ALLOC_GROUPING = "AIRLINE_333";

	public static final String MIN_REQUIRED_SEATS_FOR_BT_RES = "AIRLINE_334";

	public static final String ADDITIONAL_SENTENCE_ON_INVOICE = "AIRLINE_336";

	public static final String APPLY_CHANNEL_WISE_BSP_FEE = "AIRLINE_326";

	public static final String SHOW_FCC_SEG_BC_INVENTORY_ONEWAY_RETURN_PAXCOUNTS = "AIRLINE_351";

	public static final String ENABLE_AUTO_CHECKIN = "AIRLINE_345";

	public static final String AUTO_CHCK_SEAT_ROW_BLOCK = "AIRLINE_346";

	public static final String ENABLE_SINGLE_STEP_REDEEMING_PROCESS = "AIRLINE_349";

	public static final String MAX_NO_OF_DATE_PERIODS_FOR_PROMOTION_CRITERIA = "AIRLINE_357";

	public static final String CANCEL_ISSUED_UNCONSUMED_LMS_CREDITS_CUTOFF_TIME = "AIRLINE_350";

	public static final String PROCEED_PAYMENT_LMS_CANCEL_SERVICE_FAILURE = "AIRLINE_352";

	public static final String LMS_MEMBER_AUTHENTICATION_REQUIRED = "AIRLINE_353";

	public static final String OPEN_ALL_THE_ETICKETS_IN_PAST_FLIGHTS = "AIRLINE_337";

	public static final String TIME_CHANGE_CANCELLATION_AND_REPROTECTION_FOR_PAST_FLIGHTS = "AIRLINE_339";

	public static final String PAST_FLIGHT_ACTIVATION_DURATION_IN_DAYS = "AIRLINE_340";

	public static final String ALLOWABLE_MSG_TYPES = "AIRLINE_342";

	public static final String ALLOW_UPDATE_SCHEDULED_FLIGHT_TO_OUT_OF_SCHEDULE_PERIOD_OR_FREQUENCY = "AIRLINE_344";

	public static final String ADVANCE_ROLLFORWARD_OPERATION = "AIRLINE_347";
	
	public static final String AEROSPIKE_IP_TO_COUNTRY_ENABLED = "AIRLINE_360";

	public static final String BUNDLE_FARE_DESCRIPTION_TEMPLATE_VERSION = "AIRLINE_358";
	/** +----------------------------------------------------------------+ */
	/** | Reservation Parameters | */
	/** +----------------------------------------------------------------+ */
	public static final String HANDLING_CHARGES = "RES_3";

	public static final String ON_HOLD_DURATION = "RES_4";

	public static final String OUTBOUND_RETURN_FARE_PERCENTAGE = "RES_5";

	public static final String FLT_SEARCH_ADULTS = "RES_7";

	public static final String MIN_TRANSIT_TIME = "RES_8";

	public static final String MAX_TRANSIT_TIME = "RES_9";

	public static final String SYSTEM_CREDIT_PERIOD = "RES_10";

	public static final String CHECK_IN_TIME = "RES_12";

	public static final String INSURCHG = "RES_13";

	public static final String ON_HOLD_DURATION_INMODBUFTIME = "RES_15";

	public static final String DEPT_DATE = "RES_16";

	public static final String RETU_DATE = "RES_17";

	public static final String DECIMALS = "RES_18";

	public static final String CLASS_OF_SERVICE = "RES_19";

	public static final String MAX_PAX_AGENT = "RES_20";

	public static final String FAQ_URL = "RES_22";

	public static final String INSURANCE_URL = "RES_23";

	public static final String INTERNATIONAL_CHANGEBUFFER_TIME = "RES_24";

	public static final String AGENT_RESERVATION_CUTOFF_TIME = "RES_25";

	public static final String PUBLIC_RESERVATION_CUTOFF_TIME_FOR_INTERNATIONAL = "RES_26";

	public static final String SHOW_SEATS = "RES_27";

	public static final String MIN_RETURN_TRANSITION_TIME = "RES_28";

	public static final String PAGE_TIME_OUT_MILISECONDS = "RES_29";

	public static final String KIOSK_OHD_DURATION = "RES_30";

	public static final String KIOSK_OHD_DURATION_DISPLAY = "RES_31";

	public static final String KIOSK_SES_TIMEOUT = "RES_32";

	public static final String KIOSK_SES_TIMEOUT_MSGD = "RES_33";

	public static final String CANCEL_OND_FARE_PERCENTAGE = "RES_34";

	public static final String IBE_OHD_DURATION = "RES_35";

	public static final String IBE_OHD_DURATION_DISPLAY = "RES_36";

	public static final String MIN_CHARGEABLE_TRANSIT_DURATION = "RES_37";

	public static final String MODIFY_OND_FARE_PERCENTAGE = "RES_38";

	public static final String CHILD_BOOKING = "RES_39";

	public static final String EXTERNAL_PAYMENT_SERVICE_CHARGE_CODE = "RES_40";

	public static final String MAX_ALERTS_TO_SHOW = "RES_41";

	public static final String SHOW_CC_INFO_IN_ITINERARY_ALWAYS = "RES_42";

	public static final String APPLY_CREDITCARD_CHARGES = "RES_43";

	public static final String SEND_ONHOLD_SMS = "RES_44";

	public static final String ENABLE_CREDITCARD_PAYMENTS_KIOSKS = "RES_45";

	public static final String ONHOLD_STOP_BUFFER_TIME = "RES_46";

	public static final String UNSUBSCRIBE_EMAIL = "RES_48";

	public static final String SEND_EMAILS_FOR_FLIGHT_ALTERATIONS = "RES_49";

	public static final String SEND_SMS_FOR_FLIGHT_ALTERATIONS = "RES_50";

	public static final String SMS_FOR_CONFIRMED_BOOKINGS = "RES_53";

	public static final String SMS_FOR_ON_HOLD_BOOKINGS = "RES_52";

	public static final String SEAT_MAP_SCHED_SEAT_RELEASE_MIN = "SCHD_9";
	// not used anymore
	public static final String GENERATE_E_TICKET = "RES_51";

	public static final String SHOW_RES_SEG_EXP_DATE = "RES_54";

	public static final String SHOW_PASSPORT_IN_ITINERARY = "RES_55";

	public static final String SHOW_DOB_IN_ITINERARY = "RES_56";

	public static final String GDS_CONFIRMATION_ONHOLD_DURATION = "RES_57";

	public static final String GDS_TICKTING_ONHOLD_DURATION = "RES_58";

	public static final String SEATMAP_XBE_SELECTION_MOD_STOP_CUTOVER = "RES_59";

	public static final String IBE_SEATMAP_CUTTOFF = "RES_60";

	// not used anymore
	public static final String GENERATE_PAX_ETICKET = "RES_61";

	public static final String SHOW_PAX_ETICKET = "RES_62";

	public static final String ENFORCE_SAME_HIGHER = "RES_63";

	public static final String EXCLUDE_SEG_FARES_FROM_CONNECTION_AND_RETURN = "RES_64";

	public static final String SMS_PHONE_NO = "RES_65";

	public static final String CHARGE_ROUND_ENABLE = "RES_66";

	public static final String CHARGE_ROUND_ROUND = "RES_67";

	public static final String CHARGE_ROUND_BREAKPOINT = "RES_68";

	public static final String DEFAULT_CARD_PAYMENT_CURRENCY = "RES_69";

	public static final String USE_LAST_MOD_RATES = "RES_70";

	public static final String CC_SUPERVISOR_MOBILE_NO = "RES_71";

	public static final String CC_SUPERVISOR_EMAIL = "RES_72";

	public static final String CC_BIN_PRFIX_CHECK = "RES_73";

	public static final String OPEN_ENDED_OUTBOUND_RETURN_FARE_PERCENTAGE = "RES_74";

	public static final String OPEN_ENDED_RETURN_SUPPORT = "RES_75";

	public static final String CANCEL_EXPIRED_OPEN_ENDED_SEGMENTS = "RES_76";

	public static final String MEAL_CUTOVERTIME = "RES_77";

	public static final String SHOW_AIRPORT_SERVICES = "RES_78";

	public static final String MEAL_NOTIFY_EMAIL = "RES_79";

	public static final String MEAL_NOTIFICATION_START_CUTOVER = "RES_80";

	public static final String MEAL_NOTIFICATION_STOP_CUTOVER = "RES_81";

	public static final String MEAL_NOTIFICATION_ATTEMPTS = "RES_82"; // AARESAA-1953

	public static final String DUPLICATE_NAME_CHECK = "RES_83";

	public static final String LOYALITY_AGENT_CODE = "RES_84";

	public static final String HALA_INSTANT_NOTIFICATION = "RES_85";

	public static final String LOAYLTY_USER_ID_FOR_WEB = "RES_86";

	public static final String SESSION_TIMEOUT_BANNER_POPUP = "RES_87";

	public static final String ALLOW_HALF_RETURN_FARES_FOR_NEW_BOOKINGS = "RES_88";

	public static final String ALLOW_HALF_RETURN_FARES_FOR_MODIFICATION = "RES_89";

	public static final String ALLOW_CAPTURE_PAY_REF = "RES_90";

	public static final String OUTBOUND_RETURN_FLEXI_CHARGE_PERCENTAGE = "RES_91";

	public static final String ALLOW_HALF_RETURN_SEGMENT_COMBINABILITY = "RES_92";

	public static final String APPLICATION_ONHOLD = "RES_93";

	public static final String ONHOLD_DURATION = "RES_94";

	public static final String ONHOLD_DISPLAY_TIME = "RES_95";

	public static final String ONHOLD_BUFFER_START_CUT_OVER = "RES_96";

	public static final String ONHOLD_BUFFER_END_CUT_OVER = "RES_97";

	public static final String ONHOLD_DURATION_WITHIN_ONHOLD_BUFFER_TIME = "RES_98";

	public static final String ONHOLD_SEND_SMS = "RES_99";

	public static final String RES_CHARGE_GROUP_FULFILLMENT_ORDER_FOR_PAYMENT = "RES_100";

	public static final String RES_CHARGE_GROUP_FULFILLMENT_ORDER_FOR_REFUND = "RES_101";

	public static final String ENABLE_TRANSACTION_GRANULARITY_TRACKING = "RES_102";

	public static final String INVENTORY_SEAT_BLOCK_DURATION = "RES_103";

	public static final String ENABLE_DYNAMIC_BEST_OFFERS_CACHING = "RES_104";

	public static final String CHECK_IN_CLOSING_TIME = "RES_105";

	public static final String ENABLE_DYNAMIC_BEST_OFFERS = "RES_106";

	public static final String ENABLE_TOTAL_PRICE_QUOTE_FOR_AVAIL_FLIGHTS = "RES_107";

	public static final String BAGGAGE_CUTOVERTIME = "RES_108";

	public static final String BAGGAGE_MANDATORY = "RES_109";

	public static final String APPLY_ROUTE_BOOKING_CLASS_LEVEL_ONHOLD_TIME = "RES_110";

	public static final String ENDORSEMENTS_ENABLED = "RES_111";

	public static final String DOMESTIC_CHANGEBUFFER_TIME = "RES_112";

	public static final String SHOW_FARE_AMTS_IN_ITINERARY_CHARGES_DISPLAY = "RES_113";

	public static final String GDS_CONNECTIVITY_AGENT = "RES_114";

	public static final String ENABLE_CITY_BASED_AVAILABILITY = "RES_115";

	public static final String DOMESTIC_ONHOLD_DURATION_WITHIN_ONHOLD_BUFFER_TIME = "RES_116";

	public static final String ENABLE_TICKET_VALIDITY = "RES_117";

	public static final String ENABLE_FARE_DISCOUNT = "RES_118";

	public static final String ENABLE_AGENT_INSTRUCTIONS = "RES_119";

	public static final String ENABLE_TRANSFER_OWNER_AGENT = "RES_120";

	public static final String ENABLE_DATA_TRANSFER_TO_SAGE = "RES_121";

	public static final String RESERVATION_CUTOFF_TIME_FOR_DOMESTIC_FLIGHTS = "RES_122";

	public static final String DEFAULT_TICKET_VALIDITY_MONTHS = "RES_123";

	public static final String MODIFY_QUOTE_ON_ORG_SEG_FAREQUOTE_DATE = "RES_124";

	public static final String APPLY_BOOKING_CNX_MOD_CHARGES_FOR_CANCEL_FLIGHTS = "RES_125";

	public static final String APPLY_MOD_CHARGE_FOR_SAME_FLIGHT_BOOKING_MODIFICATIONS = "RES_126";

	public static final String SKIP_NO_SHOW_SEGMENT_CNX_CHARGE = "RES_127";

	public static final String REQUOTE_ENABLED = "RES_128";

	public static final String ENABLE_INTERLINE_SUPPORT_FOR_OTA_WEBSERVICES = "RES_129";

	public static final String BAGGAGE_SELECTION_FOR_FINAL_CUT_OVERTIME = "RES_142";

	public static final String BAGGAGE_SELECTION_FOR_FRESH_BAGGAGE_CUT_OVERTIME = "RES_143";

	public static final String BAGGAGE_SELECTION_FOR_ALTER_BAGGAGE_CUT_OVERTIME = "RES_144";

	public static final String FLOWN_CALCULATE_METHOD = "RES_130";

	public static final String ALLOW_RETURN_PARTICIPATE_IN_HALF_RETURN = "RES_131";

	public static final String SKIP_CNX_CHARGE_FLOWN_SEG_EXISTS = "RES_132";

	public static final String IS_JN_TAX_APPLY_FOR_CNX_MOD_NOSHOW_CHARGES = "RES_133";

	public static final String HTTPS_COMPATIBILITY_MAP = "RES_134";

	public static final String AUTO_CANCELLATION_TRIGGER_TIME = "RES_135";

	public static final String AUTO_CANCELLATION_TRIGGER_DURING_BUFFER = "RES_136";

	public static final String ENABLE_DOMESTIC_FARE_DISCOUNT = "RES_137";

	public static final String DOMESTIC_FARE_DISCOUNT_PERC = "RES_138";

	public static final String IS_TO_NOTIFY_AN_OFFICIAL = "RES_139";

	public static final String AUTO_CANCELLATION_ENABLED = "RES_140";

	public static final String APPLY_MOD_CHARGE_FOR_SAME_FLIGHT_HCOS_TO_LCOS = "RES_141";

	public static final String ENABLE_FARE_ADJUSTMENT_FOR_MODIFICATION = "RES_145";

	public static final String ENABLE_ONLINE_CHECKIN_REMINDER = "RES_146";

	public static final String ONLINE_CHECKIN_REMINDER_SCHEDULER_TIME_DURATION = "RES_147";

	public static final String STANDBY_TRAVEL_VALIDITY_PARAMS = "RES_148";

	public static final String ENABLE_WAIT_LISTED_RESERVATIONS = "RES_149";

	public static final String STANDBY_AGENT_RESERVATION_CUTOFF_TIME = "RES_150";

	public static final String STANDBY_BAGGAGE_SELECTION_FOR_FRESH_BAGGAGE_CUT_OVERTIME = "RES_151";

	public static final String STANDBY_BAGGAGE_SELECTION_FOR_ALTER_BAGGAGE_CUT_OVERTIME = "RES_152";

	public static final String ENABLE_PROMO_CODE = "RES_154";

	public static final String PROMO_PRIORITY = "RES_155";

	public static final String ENABLE_AUTO_REFUND_FOR_NOSHO_PAX_IN_IBE_BOOKINGS = "RES_153";

	public static final String ENABLE_OPENJAW_MULTICITY_SEARCH = "RES_156";

	public static final String SHOW_DISCOUNT_ON_ITINERARY = "RES_157";

	public static final String ALLOW_HALF_RETURN_FARES_FOR_OPEN_RETURN = "RES_158";

	public static final String NS_AUTO_REFUND_HOLD_DURATION = "RES_159";

	public static final String ENABLE_AGENT_COMMISSION = "RES_160";

	public static final String VISIBLE_ONLY_GOSHOW_FARES_WITHIN_FLIGHT_CUTOFF_TIME = "RES_161";

	public static final String IS_JN_TAX_APPLICABLE_FOR_ANCI = "RES_162";

	public static final String SUPPORTED_FARE_TYPES = "RES_163";

	public static final String IS_FARE_BIASING_ENABLED = "RES_164";

	public static final String PENALTY_OND_FARE_PERCENTAGE_IN_REQUOTE_FLOW = "RES_165";

	public static final String ENABLE_RECORDING_ETICKET_TRANSITIONS = "RES_169";

	public static final String PASENGER_NAME_MODIFICATION_CUT_OFF_TIME = "RES_166";

	public static final String SPLIT_DISCOUNT_AMOUNT_INTO_REFUND_NON_REFUND = "RES_167";

	public static final String ALLOWED_IMAGE_FORMATS_FOR_UPLOAD = "RES_168";

	public static final String TOTAL_PAYMENT_DUE_REMINDER_TIMEGAP = "RES_169";

	public static final String ENABLE_SUB_JOURNEY_DETECTION = "RES_170";

	public static final String PENALTY_CALCULATE_METHOD = "RES_171";

	public static final String TOTAL_PRICE_BASED_MINIMUM_FARE_QUOTE = "RES_172";

	public static final String SHOW_AIRPORT_TRANSFER = "RES_174";

	public static final String ENABLE_RETURN_BUCKET_CACHE = "RES_177";

	public static final String IS_JN_TAX_APPLICABLE_FOR_HANDLING_CC_ADJ_CHARGES = "RES_175";

	public static final String EDIT_AIRPORT_TRANSFER = "RES_176";

	public static final String TICKET_VALIDITY_FOR_SUB_JOURNEY_ONLY = "RES_178";

	public static final String FARE_DISCOUNT_SUPPORTED_PAX_TYPES = "RES_179";

	public static final String ENABLE_CONN_ROUTE_OND_MERGE = "RES_180";

	public static final String NOMINAL_CODES_RELATED_TO_REPORT_FILE = "RES_181";

	public static final String RAPID_RET_FILE_AGENT_TYPES = "RES_182";

	public static final String RAPID_HOT_FILE_AGENT_TYPES = "RES_183";

	public static final String ENABLE_CONN_FLIGHT_VALIDATION_FOR_OPEN_JAW = "RES_184";

	public static final String CONSIDER_BC_CONFIG_FOR_INFANT_SURCHARGE = "RES_186";

	public static final String VALIDATE_RET_FROM_SAME_COUNTRY_DOUBLE_OPEN_JAW = "RES_187";

	public static final String VALIDATE_RET_FROM_SAME_COUNTRY_SINGLE_OPEN_JAW = "RES_188";

	public static final String ENABLE_AUTO_FLOWN_PROCESS = "RES_189";

	public static final String AUTO_FLOWN_PAX_STATUS = "RES_190";

	public static final String APPLY_PENALTY_WITHIN_VALIDITY = "RES_191";

	public static final String ENABLE_CHARGE_ADJ_FOR_DUE_AMOUNT = "RES_192";

	public static final String ALLOW_HALF_RETURN_FARES_FOR_OPEN_JAW = "RES_193";

	public static final String PUBLIC_RESERVATION_CUTOFF_TIME_FOR_DOMESTIC = "RES_194";

	public static final String RESTRICT_MOD_CNX_FLOWN_BOOKINGS = "RES_195";

	public static final String NO_OF_BACK_DAYS_TO_RECONCILE_CARD_PAYMENTS = "RES_196";

	public static final String REFUND_BUNDLED_SERVICE_FEE_FOR_FLIGHT_CNX_DISRUPTION_REPROTECT = "RES_197";

	public static final String ONHOLD_OFFLINE_PAYMENT_TIME = "RES_198";

	public static final String AIRPORT_TRANSFER_SELECTION_MOD_STOP_CUTOVER_FOR_DOMESTIC = "RES_199";

	public static final String AIRPORT_TRANSFER_SELECTION_MOD_STOP_CUTOVER_FOR_INTERNATIONAL = "RES_200";

	public static final String ENABLE_FARE_RULE_LEVEL_NCC = "RES_201";

	public static final String ALLOW_MULTICITY_SEARCH_FOR_ALL_XBE_USERS = "RES_202";

	public static final String API_RESERVATION_CUTOFF_TIME_FOR_DOMESTIC = "RES_203";

	public static final String API_RESERVATION_CUTOFF_TIME_FOR_INTERNATIONAL = "RES_204";

	public static final String REQUEST_NATIONALID_FOR_RESERVATIONS_HAVING_DOMESTIC_SEGMENT = "RES_205";

	public static final String ALLOW_ADD_INFANT_TO_PARTIALLY_FLOWN_BOOKINGS = "RES_206";

	public static final String MAX_ADULT_COUNT_ALLOWED_FOR_RESERVATION = "RES_207";

	public static final String IS_TO_NOTIFY_AN_OFFICIAL_XBE = "RES_208";

	public static final String REMOVE_ACTION_ALERT_WHEN_SELECTION_TRANSFER = "RES_212";

	public static final String CANCEL_EXPRIED_ONHOLD_RESERVATION = "RES_210";

	public static final String ADMIN_FEE_REGULATION_ENABLED = "RES_209";

	public static final String ADMIN_FEE_CHANNEL_WISE_CONFIGURED = "RES_211";

	public static final String ALLOW_BUNDLE_FARE_QUOTE_INDEPENDENT_OF_SUR_EXCLUDE_FLAG = "RES_213";

	public static final String SKIP_SALES_CHANNEL_CHECK_FOR_FLOWN = "RES_214";

	public static final String TAX_REGISTRATION_NUMBER_VISIBLE_COUNTRIES = "RES_215";

	public static final String EXCLUDE_ONEWAY_FARES_FROM_CONNECTION_RETURN = "RES_216";

	public static final String ALLOW_REMOVE_INFANT_WHEN_FUTURE_SEGMENTS_NOT_THERE = "RES_220";

	public static final String CONSIDER_SEGMENT_RETURN_FLAG_IN_GST_CALCULATION = "RES_217";

	public static final String ENABLE_CUTOFF_TIME_FOR_API_RESERVATION = "RES_218";

	public static final String BY_PASS_INVENTORY_CHECK_FOR_SEAT_RELEASE = "RES_219";

	public static final String SEND_EMAILS_FOR_FLIGHT_ALTERED_BY_SCHEDULE_MSG = "RES_222";

	public static final String SEND_SMS_FOR_FLIGHT_ALTERED_BY_SCHEDULE_MSG = "RES_223";

	public static final String SKIP_MOD_CHARGE_FOR_OPEN_SEGMENT_MODIFICATION = "RES_221";

	public static final String CITY_SEARCH_OND_DESCRIPTION_TEXT = "RES_225";

	public static final String APPLY_HANDLING_CHARGES_ON_MODIFICATION = "RES_224";

	public static final String ENABLE_GOQUO_SERVICE_AS_SSR = "RES_228";
	
	public static final String REMOVE_REPROTECTED_BUNDLED_ANCI_FOR_OHD = "RES_229";
	/** +----------------------------------------------------------------+ */
	/** | IBE Parameters | */
	/** +----------------------------------------------------------------+ */
	public static final String NON_SECURE_IBE_URL = "IBE_01";

	public static final String SECURE_IBE_URL = "IBE_02";

	public static final String SHOW_ALERTS_IBE_ENABLED = "IBE_03";

	public static final String GOOGLE_ANALYTICS_ENABLED = "IBE_04";

	public static final String GOOGLE_ANALYTICS_ID = "IBE_05";

	public static final String HOLIDAYS_URL = "IBE_06";

	public static final String CREDIT_CARD_PAYMENTS_ENABLED = "IBE_07";

	public static final String LOAD_AGENT_TRANSACTION_POPUP = "XBE_10";

	public static final String DEFAULT_DATE_FORMAT_FOR_XBE = "XBE_01";

	public static final String RESERVATION_ONHOLD_ENABLED = "IBE_08";

	public static final String COS_SELECTION_ENABLED = "IBE_09";

	public static final String SHOW_IBE_CHARGES_BREAKDOWN = "IBE_10";

	public static final String SUPPORTED_IBE_LANGUAGES = "IBE_11";

	public static final String IBE_URLS = "IBE_12";

	public static final String RENT_A_CAR_URL = "IBE_13";

	public static final String IBE_TEMPLATE_CODE = "IBE_14";

	public static final String XBE_CREDIT_CARD_PAYMENTS_ENABLED = "XBE_09";

	public static final String MODIFY_RESERVATION_BY_UNREGISTERED_USER = "IBE_15";

	public static final String CANCEL_SEGMENT_BY_UNREGISTERED_USER = "IBE_16";

	public static final String CANCEL_RESERVATION_BY_UNREGISTERED_USER = "IBE_17";

	public static final String ADD_SERVICES_BY_UNREGISTERED_USER = "IBE_18";

	public static final String EDIT_SERVICES_BY_UNREGISTERED_USER = "IBE_19";

	public static final String GET_BOOKING_CHANNELS_FOR_UNREGISTERED_USER = "IBE_20";

	public static final String CAPTCHA_ENABLED_FOR_UNREGISTERED_USER = "IBE_21";

	public static final String SSR_SELECTION_CUTOVER_TIME = "IBE_22";

	public static final String IBE_GROUP_SSR = "IBE_23";

	public static final String ADD_SERVICES_BY_REGISTERED_USER = "IBE_24";

	public static final String EDIT_SERVICES_BY_REGISTERED_USER = "IBE_25";

	public static final String IBE_ADD_SEAT = "IBE_26";

	public static final String IBE_EDIT_SEAT = "IBE_27";

	public static final String IBE_ADD_INSURANCE = "IBE_28";

	public static final String IBE_EDIT_INSURANCE = "IBE_29";

	public static final String IBE_ADD_MEAL = "IBE_30";

	public static final String IBE_EDIT_MEAL = "IBE_31";

	public static final String IBE_ADD_BAGGAGE = "IBE_58";

	public static final String IBE_EDIT_BAGGAGE = "IBE_59";

	public static final String IBE_ADD_HALA = "IBE_32";

	public static final String IBE_EDIT_HALA = "IBE_33";

	public static final String IBE_NAV_STEPS = "IBE_34";

	public static final String IBE_BANNER_TYPE = "IBE_35";

	public static final String IBE_SHOW_LCC_RESULTS = "IBE_36";

	public static final String IBE_ALLOW_CANCELLATION = "IBE_37";

	public static final String IBE_ALLOW_RETURN = "IBE_38";

	public static final String GOOGLE_ANALYTICS_CURRENCY = "IBE_39";

	public static final String IBE_CALENDAR_VIEWS = "IBE_40";

	public static final String IBE_CALENDAR_GRADIENT_VIEW = "IBE_41";

	public static final String IBE_MODIFY_SEGMENT_BALANCE_DISPLAY_AS = "IBE_42";

	public static final String IBE_MEAL_DISPLAY_VERSION = "IBE_43";

	public static final String IBE_SEAT_MAP_DISPLAY_VERSION = "IBE_44";

	public static final String IBE_CALENDAR_TYPE = "IBE_45";

	public static final String IBE_SESSION_TIMEOUT_DISPLAY = "IBE_46";

	public static final String IBE_IMAGE_CAPCHA_AS = "IBE_47";

	public static final String ENABLE_REGISTERED_USER_ACCESS_IN_KIOSK = "IBE_48";

	public static final String IBE_OCTOPUS_SITE_ID = "IBE_49";

	public static final String IBE_BLOCK_SEAT_ENABLE = "IBE_50";

	public static final String IBE_SHOW_FARE_PARAM = "IBE_51";

	public static final String IBE_SHOW_REG_USER_LOGIN_IN_PAX_PAGE = "IBE_52";

	public static final String IBE_SHOW_CUSTOMIZED_ITINERARY = "IBE_53";

	public static final String IBE_MODIFY_SEGMENT_BY_DATE = "IBE_54";

	public static final String IBE_MODIFY_SEGMENT_BY_ROUTE = "IBE_55";

	public static final String IBE_CUSTOMER_REGISTER_CONFIRMATION = "IBE_56";

	public static final String IBE_DESIGN_VERSION_AVAILABLE = "IBE_57";

	public static final String IBE_ADD_BUS = "IBE_60";

	public static final String IBE_ONHOLD_RESTRICTION_CONFIG = "IBE_61";

	public static final String IBE_MODIFICATION_RESTRICTION_ENABLED = "IBE_62";

	public static final String RENT_A_CAR_DYNAMIC_URL = "IBE_63";

	public static final String HOTEL_LINK_LOAD_WITHIN_FRAME = "IBE_64";

	public static final String IBE_ENABLE_PAX_CREDIT_AUTO_REFUND_FOR_CC = "IBE_65";

	public static final String IBE_ENABLE_SOCIAL_NETWORKS_FOR_IBE = "IBE_66";

	public static final String IBE_LIVE_STREAM_FACEBOOK_APP_CONFIGS = "IBE_67";

	public static final String IBE_MARKETING_POP_UP_DELAY = "IBE_68";

	public static final String MINIMUM_NUMBER_OF_SEATS_TO_SHOW_IN_IBE = "IBE_69";

	public static final String IBE_ALLOW_NO_SHOW_SEGMENTS_TO_MODIFY = "IBE_70";

	public static final String IBE_ALLOW_SOCIAL_LOGIN = "IBE_71";

	/**
	 * Key denoting whether to sort flights in IBE availability search so direct flights appear first.
	 */
	public static final String DISPLAY_DIRECT_FLIGHTS_FIRST = "IBE_72";

	public static final String IBE_ALLOW_SOCIAL_SEATING = "IBE_73";

	public static final String IBE_SERVER_TO_SERVER_MESSAGES = "IBE_74";

	public static final String ENABLE_IBE_FAKE_DISCOUNTS = "IBE_75";

	public static final String IBE_ONLINE_CHECK_IN_URL = "IBE_76";

	public static final String IBE_ALLOW_LOAD_RES_USING_PAX_NAME = "IBE_77";

	public static final String IBE_ALLOW_MODIFY_BOOKINGS_TO_LOW_PRIORITY_COS = "IBE_78";

	public static final String IBE_SURVEY_LINK_URL = "IBE_80";

	public static final String IBE_BEST_OFFERS_URL = "IBE_81";

	public static final String IBE_EXIT_POPUP_DISPLAY_TIMES_PER_PAGE = "IBE_82";

	public static final String LOGIN_API_TOKEN_VALIDITY_PERIOD = "IBE_83";

	@Deprecated
	// TODO : Remove from db
	public static final String IBE_OFFLINE_PAYMENT_ENABLE = "IBE_84";

	public static final String IBE_EXIT_POPUP_DISPLAY_TIME_GAP = "IBE_85";

	public static final String IBE_EXIT_POPUP_ENABLE_STEP = "IBE_86";

	public static final String IBE_AIRPORT_TRANSFER = "IBE_87";

	public static final String IBE_SHOW_SOCIAL_LOGIN_IN_PAX_PAGE = "IBE_88";

	public static final String IBE_CUSTOMER_AUTO_LOGIN_AFTER_REGISTRATION = "IBE_89";

	public static final String IBE_CALENDAR_ENABLED_FOR_REQUOTE = "IBE_90";

	public static final String MULTICITY_ALLOWED_MAXIMUM_NO_OND_IBE = "IBE_91";

	public static final String APPLY_FQTV_FOR_ALL_SEGMENTS = "IBE_92";

	public static final String REFUND_IBE_ANCI_MODIFY_CREDIT_AMOUNT = "IBE_93";

	public static final String IBE_HUMAN_VERIFICATION_PARAMS = "IBE_94";

	public static final String IBE_OHD_PAYMENT_REMINDER = "IBE_95";

	public static final String DISPLAY_NOTIFICATION_FOR_LAST_NAME_IN_CONTACT_DETAILS_IN_IBE = "IBE_96";

	public static final String IBE_SSR_ENABLE = "IBE_97";

	public static final String IBE_ADD_SSR = "IBE_98";

	public static final String IBE_EDIT_SSR = "IBE_99";

	public static final String ENABLE_STOPOVER_FOR_IBE = "IBE_100";

	public static final String ALLOW_SAME_FLIGHT_MODIFICATION_IN_IBE = "IBE_101";

	public static final String IBE_SKIP_DATE_CHANGE_TO_PERSIAN = "IBE_102";

	public static final String CONFIGURED_LANGUAGE_FOR_ITINERARY_IN_INT_FLT_BOOKINGS = "IBE_103";

	public static final String SECURE_SERVICE_APP_IBE_URL = "IBE_104";

	public static final String IBE_ENABLE_PAX_TYPE = "IBE_107";

	public static final String IBE_PAYMENT_ATTEMPT_LIMIT = "IBE_108";

	public static final String IBE_SELECTED_CURRENCY_IPG = "IBE_109";

	public static final String ENABLE_ANCI_SUMMARY_AT_PAYMENT_PAGE = "IBE_111";

	public static final String IBE_DISPLAY_MULTILEG_STOPOVERS = "IBE_110";

	public static final String GOOGLE_ANALYTICS_ID_FOR_SERVICEAPP = "IBE_115";
	public static final String SHOW_IBE_V3_CHARGES_BREAKDOWN_IN_PAYMENT_PAGE = "IBE_112";

	public static final String CHECK_BOX_TO_ACCEPT_TERMS_IN_AVAILABILITY_PAGE_IBE_V3 = "IBE_113";

	public static final String ALLOW_IBE_FORCE_CONFIRM_MODIFY = "IBE_114";

	public static final String SHOW_DECIMAL_PLACES_IN_CUREENCY_VALUES = "IBE_118";

	public static final String USE_OLD_IBE_URL_IN_SERVICEAPP_CONFIRM_REGISTRATION = "IBE_116";

	public static final String SHOW_DESCRIPTION_FOR_FARE_BUNDLES_IN_IBE = "IBE_117";

	public static final String ENABLE_CHAMWINGS_OHD_POPUP = "IBE_121";

	public static final String IBE_CUSTOMER_PASSWORD_RESET_TIMEOUT = "IBE_120";

	public static final String IBE_OND_WISE_PAYMENT = "IBE_119";

	public static final String SHOW_HALF_RETURN_FARES_IN_IBE_CALENDAR = "IBE_124";

	public static final String DISABLE_REFRESHING_IB_CALENDAR_IN_IBE = "IBE_125";

	public static final String TAX_REGISTRATION_NUMBER_LABEL_FOR_IBE = "IBE_123";

	public static final String ONLINE_CHECK_IN_URL_IN_IBE = "IBE_127";

	public static final String BULK_CHECK_IN_URL_IN_IBE = "IBE_128";

	public static final String TIME_DIFFERENCE_FOR_ONLINE_CHECK_IN_IBE = "IBE_129";

	/** +----------------------------------------------------------------+ */
	/** | Travel Agent - Incentive | */
	/** +----------------------------------------------------------------+ */

	public static final String ENABLE_INCENTIVE_SCHEMES = "XBE_11";

	public static final String RECORDS_PER_RESERVATION_SEARCH_PAGE = "XBE_12";

	/** Show Booking Class selection combo box at XBE Availability Search */
	public static final String BC_SELECTION_AT_AVAILABILITY_SEARCH = "XBE_13";

	/* enabling Google tracking for XBE */
	public static final String GOOGLE_ANALYTICS_XBE = "XBE_14";

	/** Enable XBE Dashboard Messages Sync */
	public static final String DASHBOARD_MSG_SYNC = "XBE_15";

	/** Enable XBE Utility Tools Section */
	public static final String ENABLE_UTILITY_TOOLS = "XBE_16";

	public static final String ALL_CC_FARE_CHANGE = "XBE_17";

	public static final String TRANSFER_SEG_ON_OND_FARE = "XBE_18";

	public static final String BANK_GUARANTEE_NOTIFICATION_ATTEMPTS = "XBE_19";

	public static final String REQUOTE_AUTO_FAREQUOTE = "XBE_20";

	/**
	 * Key pointing to the APP_PARAMETER entry to specify whether FareRule information should be displayed in the
	 * FareQuote table summary.
	 */
	public static final String SHOW_FARE_RULES_IN_QUOTE_SUMMARY = "XBE_21";

	/**
	 * Key pointing to the APP_PARAMETER entry to specify whether Type Of Payment should be displayed in the Agent
	 * Settlement screen.
	 */
	public static final String TYPE_OF_PAYMENT_FROM_AGENT_SETTLEMENT = "XBE_22";

	/**
	 * Key pointing to the alternative email to be set if email in blank in XBE. This is a zest requirement and will
	 * only be activated if a value is present in the database, and email validation for XBE is enabled.
	 */
	public static final String ALTERNATIVE_EMAIL_FOR_XBE = "XBE_23";

	/**
	 * Key pointing to the APP_PARAMETER entry to specify whether User Note will be mandatory for XBE
	 */
	public static final String USER_NOTE_MANDATORY = "XBE_24";

	public static final String AUTOMATIC_ADJUSTMENT_FOR_TAX_REFUND = "XBE_25";
	/**
	 * Key denoting whether to hide exchanged charges from account tab and itinerary.
	 */
	public static final String HIDE_EXCHANGED_CHARGES = "XBE_26";

	public static final String SHOW_AGENT_FOR_CASH_PAYMENTS_IN_ACC_TAB = "XBE_27";

	public static final String HIDE_ZERO_CASH_PAYMENT_RECORD = "XBE_28";

	public static final String SHOW_FARE_MASK_OPTION = "XBE_29";

	public static final String SHOW_CHARTER_AGENT_OPTION = "XBE_30";

	public static final String SHOW_FLOWN_FLIGHT_CLOSED_BC = "XBE_31";

	public static final String DISPLAY_FFP_NO_IN_XBE_TOOLBAR = "XBE_33";

	public static final String MULTICITY_ALLOWED_MAXIMUM_NO_OND_XBE = "XBE_32";

	public static final String PREVENT_ADDING_DUPLICATES_IN_RECORD_AND_SETTLE_SCREEN = "XBE_34";

	public static final String XBE_SUPPORTED_LANGUAGES = "XBE_35";

	public static final String EXTERNAL_AGENT_URL = "XBE_36";

	public static final String AGENT_TOPUP_EMAIL_DESTINATION_ADDRESS = "XBE_39";

	public static final String AGENT_TOPUP_MIN_AMOUNT = "XBE_40";

	public static final String SERVICE_TAX_LABEL = "XBE_37";

	public static final String TAX_REGISTRATION_NUMBER_LABEL = "XBE_38";
	
	public static final String XBE_MINIMUM_FARE_SEARCH_ENABLED = "XBE_41";

	/** +----------------------------------------------------------------+ */
	/** | LCC Parameters | */
	/** +----------------------------------------------------------------+ */

	public static final String LCC_CONNCECTIVITY_ENABLED = "LCC_01";

	public static final String LCC_CONNCECTIVITY_CREDENTIALS = "LCC_02";

	/** +----------------------------------------------------------------+ */
	/** | FLA Parameters | */
	/** +----------------------------------------------------------------+ */

	public static final String FLA_FLIGHT_PERFORMANCE = "FLA_01";

	public static final String FLA_GRAPH_BKNG_CHNL = "FLA_02";

	/** +----------------------------------------------------------------+ */
	/** | FAWRY Parameters | */
	/** +----------------------------------------------------------------+ */

	public static final String FAWRY_CDM_ROUNDING_ENABLED = "FAWRY_01";

	public static final String FAWRY_USER_CREDENTIALS = "FAWRY_02";

	public static final String FAWRY_ACCESS_CONTROL_LIST = "FAWRY_03";

	public static final String FAWRY_CURRENCY = "FAWRY_04";

	/** +----------------------------------------------------------------+ */
	/** | MIS Parameters | */
	/** +----------------------------------------------------------------+ */
	public static final String MIS_MAX_SEARCH_PERIOD = "MIS_01";

	/** +----------------------------------------------------------------+ */
	/** | DCS Parameters | */
	/** +----------------------------------------------------------------+ */

	public static final String DCS_CONNCECTIVITY_ENABLED = "DCS_01";

	public static final String DCS_CONNCECTIVITY_CREDENTIALS = "DCS_02";

	/** +----------------------------------------------------------------+ */
	/** | GDS Parameters | */
	/** +----------------------------------------------------------------+ */

	// This is no longer using. Use userId from t_gds
	@Deprecated
	public static final String GDS_CONNCECTIVITY_CREDENTIALS = "GDS_01";

	public static final String GDS_RESERVATION_CUTOFF_TIME = "GDS_02";

	public static final String GDS_CODESHARE_CONFIGS_FROM_CACHE = "GDS_03";

	public enum OnHold {
		AGENTS, APIAGENTS, KIOSK, IBE, DEFAULT, IBEPAYMENT, MYID, IBEOFFLINEPAYMENT,IOS,ANDROID;

		public static OnHold getEnum(String strEnum) {
			for (OnHold enm : OnHold.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return OnHold.DEFAULT;
		}

	};

	public enum FLOWN_FROM {
		PFS, ETICKET, DATE;

		public static FLOWN_FROM getEnum(String strEnum) {
			for (FLOWN_FROM enm : FLOWN_FROM.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return FLOWN_FROM.DATE;
		}
	}

	public enum PENALTY_BASED {
		NEW_FARE, OLD_FARE;

		public static PENALTY_BASED getEnum(String strEnum) {
			for (PENALTY_BASED enm : PENALTY_BASED.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return PENALTY_BASED.NEW_FARE;
		}
	}

	public enum RQ_AUTO_FQ {
		ENABLE, AUTO_FLT_SELECT, AUTO_ADD_FQ, AUTO_CNX_FQ
	}

	public enum BSP_FTP {
		COUNTRY, HOST, USERNAME, PASSWORD;
	}

	/** +----------------------------------------------------------------+ */
	/** | AMADEUS Parameters | */
	/** +----------------------------------------------------------------+ */

	public static final String AMADEUS_USER_CREDENTIALS = "AMADEUS_01";

	public static final String AMADEUS_ACCESS_CONTROL_LIST = "AMADEUS_02";

	public static final String AMADEUS_COMPATIBILITY = "AMADEUS_03";

	/** PLEASE ADD NEW PARAMETERS IN TO MATCHING SECTION **/
}
