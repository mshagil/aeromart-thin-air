package com.isa.thinair.commons.core.util;

import java.math.BigDecimal;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;

public class AccelAeroRounderPolicy {

	public static final int DEFAULT_NUMBER_OF_DECIMAL_POINTS = 2;

	public final static BigDecimal LMS_THREASHOLD_AMOUNT = new BigDecimal(0.02);

	/**
	 * Returns the Rounded value
	 * 
	 * @param input
	 *            the value to be rounded
	 * @param boundry
	 *            the boundary value (1,10, 100 ..)
	 * @param breakPoint
	 *            the break point
	 * @return BigDecimal the rounded value
	 */
	public static BigDecimal getRoundedValue(BigDecimal input, BigDecimal boundry, BigDecimal breakPoint) {

		if (boundry != null && boundry.doubleValue() != 0 && breakPoint != null) {
			BigDecimal divValue = input.divide(boundry, 0, BigDecimal.ROUND_FLOOR);
			BigDecimal modValue = input.remainder(boundry);
			BigDecimal difference = new BigDecimal(0);

			if (modValue.compareTo(new BigDecimal(0)) != 0) {
				if (breakPoint.compareTo(new BigDecimal(0)) == 0) {
					divValue = divValue.add(new BigDecimal(1));
				} else if (breakPoint.compareTo(boundry) != 0) {
					difference = input.subtract(divValue.multiply(boundry));
					if (difference.compareTo(breakPoint) == 0 || difference.compareTo(breakPoint) > 0) {
						divValue = divValue.add(new BigDecimal(1));
					}
				}

			}
			return divValue.multiply(boundry);
		} else {
			return input;
		}
	}

	public static BigDecimal convertAndRoundToNearestCeilingInt(BigDecimal value, BigDecimal multiplyingExchangeRate,
			BigDecimal boundaryValue, BigDecimal breakPointValue) {
		boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
		BigDecimal totVal;

		if (isCurrencyRoundUpEnabled) {
			if (boundaryValue != null && boundaryValue.doubleValue() != 0 && breakPointValue != null) {
				totVal = getRoundedValue(AccelAeroCalculator.multiply(value, multiplyingExchangeRate), boundaryValue,
						breakPointValue);
			} else {
				totVal = AccelAeroCalculator.getRoundUpNearestCeilingInt(value, multiplyingExchangeRate);
			}
		} else {
			totVal = AccelAeroCalculator.getRoundUpNearestCeilingInt(value, multiplyingExchangeRate);
		}

		return totVal;
	}

	public static BigDecimal convertAndRound(BigDecimal value, BigDecimal multiplyingExchangeRate, BigDecimal boundaryValue,
			BigDecimal breakPointValue) {
		boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
		BigDecimal totVal;

		if (isCurrencyRoundUpEnabled) {
			if (boundaryValue != null && boundaryValue.doubleValue() != 0 && breakPointValue != null) {
				totVal = getRoundedValue(AccelAeroCalculator.multiply(value, multiplyingExchangeRate), boundaryValue,
						breakPointValue);
			} else {
				totVal = AccelAeroCalculator.multiplyDefaultScale(value, multiplyingExchangeRate);
			}
		} else {
			totVal = AccelAeroCalculator.multiplyDefaultScale(value, multiplyingExchangeRate);
		}

		return totVal;
	}
	
	public static BigDecimal convertAndRoundToBase(BigDecimal value, BigDecimal dividingExchangeRate, BigDecimal boundaryValue,
			BigDecimal breakPointValue) {
		boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
		BigDecimal totVal;

		if (isCurrencyRoundUpEnabled) {
			if (boundaryValue != null && boundaryValue.doubleValue() != 0 && breakPointValue != null) {
				totVal = getRoundedValue(AccelAeroCalculator.divide(value, dividingExchangeRate), boundaryValue,
						breakPointValue);
			} else {
				totVal = AccelAeroCalculator.divide(value, dividingExchangeRate);
			}
		} else {
			totVal = AccelAeroCalculator.divide(value, dividingExchangeRate);
		}

		return totVal;
	}

	public static BigDecimal convertAndRound(BigDecimal value, PayCurrencyDTO payCurrencyDTO) {
		return convertAndRound(value, payCurrencyDTO.getPayCurrMultiplyingExchangeRate(), payCurrencyDTO.getBoundaryValue(),
				payCurrencyDTO.getBreakPointValue());
	}

	public static void main(String[] args) {
		BigDecimal a = new BigDecimal("97.315436");
		BigDecimal exchangeRate = new BigDecimal("11.695906432748538011695906432749");
		BigDecimal boundaryValue = new BigDecimal("0.001");
		BigDecimal breakPointValue = new BigDecimal("0.0005");

		BigDecimal totVal = getRoundedValue(a, boundaryValue, breakPointValue);

		System.out.println(totVal);
	}
}
