package com.isa.thinair.commons.core.util;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Converts a collection to a string in the format of SQL in clause
 * 
 */

public class Util {

	public static final String CODE_TYPE_STRING = "STRING";
	public static final String CODE_TYPE_INTEGER = "INTEGER";
	public static final String CODE_TYPE_STRING_WITHOUT_QUOTE = "StringWithoutQuote";
	private static final String DEFAULT_PLACE_HOLDER_MARK = "{}";

	public static final String LCC_PUBLISHED = "PUB";

	public static final String LCC_TOBE_PUBLISHED = "TBP";

	public static final String LCC_TOBE_REPUBLISHED = "TBR";

	public static final String LCC_SKIP = "SKP";

	public static final String AIRLINE_AGENT_TYPE = "AIRLINE";

	private static Log log = LogFactory.getLog(Util.class);

	/**
	 * Replaces a given string place holders with given string
	 * 
	 * @param srcString
	 * @param placeHolderValue
	 * @param stringToInsert
	 * @return
	 */
	public static String setStringToPlaceHolderIndex(String srcString, String placeHolderValue, String stringToInsert) {
		return setStringToPlaceHolderIndex(srcString, placeHolderValue, stringToInsert, null);
	}

	public static String setStringToPlaceHolderIndex(String srcString, String placeHolderValue, String stringToInsert,
			String placeHolderMark) {
		String placeHolderMarkStart;
		String placeHolderMarkEnd;
		String placeHolder;
		if (placeHolderMark == null) {
			placeHolderMark = DEFAULT_PLACE_HOLDER_MARK;
		}
		if (placeHolderMark.length() == 1) {
			placeHolder = placeHolderMark + placeHolderValue;
		}else {
			placeHolderMarkStart = placeHolderMark.substring(0, 1);
			placeHolderMarkEnd = placeHolderMark.substring(1, 2);
			placeHolder = placeHolderMarkStart + placeHolderValue + placeHolderMarkEnd;
		}
		StringBuffer queryBuff = new StringBuffer(srcString);
		int startIndex = queryBuff.toString().indexOf(placeHolder);
		while (startIndex != -1) {
			queryBuff.delete(startIndex, startIndex + placeHolder.length());
			queryBuff.insert(startIndex, stringToInsert);
			startIndex = queryBuff.toString().indexOf(placeHolder);
		}

		return queryBuff.toString();
	}

	/**
	 * Replaces a given string place holders with given values in the hash map for the hash map keys
	 * 
	 * @param srcString
	 * @param replaceKeyValuePairs
	 * @return
	 */
	public static String setStringToPlaceHolderIndex(String srcString, HashMap<String, Object> replaceKeyValuePairs) {
		return setStringToPlaceHolderIndex(srcString, replaceKeyValuePairs, null);
	}

	public static String setStringToPlaceHolderIndex(String srcString, HashMap<String, Object> replaceKeyValuePairs,
			String placeHolderMark) {
		for (Iterator<String> iterKeyValue = replaceKeyValuePairs.keySet().iterator(); iterKeyValue.hasNext();) {
			String key = (String) iterKeyValue.next();
			Object keyValue = replaceKeyValuePairs.get(key);
			if (keyValue != null) {
				String strValue = "";
				if (keyValue instanceof String) {
					strValue = (String) keyValue;
				} else if (keyValue instanceof Integer) {
					strValue = ((Integer) keyValue).toString();
				} else if (keyValue instanceof Date) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
					strValue = dateFormat.format((Date) keyValue);
				}
				srcString = setStringToPlaceHolderIndex(srcString, key, strValue, placeHolderMark);
			}
		}
		return srcString;
	}

	/**
	 * Remove unused tags from the given string
	 * 
	 * @param srcString
	 * @param placeHolderMark
	 * @param selectionCrierionString
	 *            : If null, simply delete the tags with the placeHolderMark. If not null, check whether this string
	 *            exists in a tag identified by placeHolderMark. If so remove the tag.
	 * @return
	 */
	public static String removeUnUsedTagsFromString(String srcString, String placeHolderMark, String selectionCrierionString) {
		String placeHolderMarkStart;
		String placeHolderMarkEnd;
		if (placeHolderMark == null) {
			placeHolderMark = DEFAULT_PLACE_HOLDER_MARK;
		}
		placeHolderMarkStart = placeHolderMark.substring(0, 1);
		placeHolderMarkEnd = placeHolderMark.substring(1, 2);

		StringBuffer queryBuff = new StringBuffer(srcString);

		// String placeHolder = placeHolderMarkStart + placeHolderMarkEnd;
		int startIndex = queryBuff.toString().indexOf(placeHolderMarkStart);
		int endIndex = queryBuff.toString().indexOf(placeHolderMarkEnd);
		while ((endIndex - startIndex) > 0) {
			if (selectionCrierionString == null
					|| queryBuff.substring(startIndex, endIndex + 1).toString().indexOf(selectionCrierionString) > -1) {
				// If null or check whether selectionCrierionChar exists in a tag identified by placeHolderMark.
				// If so remove the tag.
				queryBuff.delete(startIndex, endIndex + 1);
				startIndex = -1;
				endIndex = -1;
			}
			// queryBuff.insert(startIndex, "");
			startIndex = queryBuff.toString().indexOf(placeHolderMarkStart, startIndex + 1);
			endIndex = queryBuff.toString().indexOf(placeHolderMarkEnd, endIndex + 1);
		}
		return queryBuff.toString();
	}

	@SuppressWarnings("rawtypes")
	public static String buildStringInClauseContent(Collection codes) {
		return buildInClauseContent(codes, CODE_TYPE_STRING, null, null);
	}

	@SuppressWarnings("rawtypes")
	public static String buildStringInClauseContent(Collection codes, String WrappingString) {
		return buildInClauseContent(codes, CODE_TYPE_STRING, WrappingString, null);
	}

	@SuppressWarnings("rawtypes")
	public static String buildStringInClauseContent(Collection codes, String WrappingString, String seperationString) {
		return buildInClauseContent(codes, CODE_TYPE_STRING, WrappingString, seperationString);
	}

	@SuppressWarnings("rawtypes")
	public static String buildIntegerInClauseContent(Collection codes) {
		return buildInClauseContent(codes, CODE_TYPE_INTEGER, null, null);
	}

	@SuppressWarnings("rawtypes")
	public static String buildIntegerInClauseContent(Collection codes, String WrappingString) {
		return buildInClauseContent(codes, CODE_TYPE_INTEGER, WrappingString, null);
	}

	@SuppressWarnings("rawtypes")
	public static String buildIntegerInClauseContent(Collection codes, String WrappingString, String seperationString) {
		return buildInClauseContent(codes, CODE_TYPE_INTEGER, WrappingString, seperationString);
	}

	public static String buildInClauseContent(String code, int numberOfTimes, String type, String WrappingString,
			String seperationString) {
		List<Object> codes = new ArrayList<Object>();
		for (int i = 0; i < numberOfTimes; i++) {
			codes.add(code);
		}
		return buildInClauseContent(codes, type, WrappingString, seperationString);
	}

	@SuppressWarnings("rawtypes")
	private static String buildInClauseContent(Collection codes, String type, String WrappingString, String seperationString) {
		String quateString;
		String codesString = "";
		if (seperationString == null)
			seperationString = ",";
		if (type.equals(CODE_TYPE_INTEGER)) {
			quateString = WrappingString == null ? "" : WrappingString;
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = ((Integer) iter.next()).toString();
				codesString += quateString + strCode + quateString + seperationString;
			}
		} else if (type.equals(CODE_TYPE_STRING_WITHOUT_QUOTE)) {
			quateString = WrappingString == null ? "" : WrappingString;
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = (String) iter.next();
				codesString += quateString + strCode + quateString + seperationString;
			}
		} else {
			quateString = WrappingString == null ? "'" : WrappingString;
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = (String) iter.next();
				codesString += quateString + strCode + quateString + seperationString;
			}
		}

		if (codesString.equals(""))
			return null;
		else
			codesString = codesString.substring(0, codesString.length() - seperationString.length());
		return codesString;
	}
	
	@SuppressWarnings("rawtypes")
	public static String buildINTOClauseForMultipleInsert(String strTableName, List<String> columns, Collection codes) {

		String strColumns = "";
		int i = 0;
		for (String column : columns) {
			if (i != 0) {
				strColumns += "," + column;
			} else {
				strColumns += column;
			}
			i++;
		}

		String codesString = "";
		for (Iterator iter = codes.iterator(); iter.hasNext();) {
			String strCode = iter.next().toString();
			codesString += " INTO " + strTableName + " (" + strColumns + ") VALUES ('" + strCode + "') ";
		}

		if (codesString.equals(""))
			return null;

		return codesString;
	}

	/**
	 * Method returns a string contains (key = 'value' OR key = 'value' ..) , replacement for IN clause (ORA error when
	 * IN( ) clause cannot exceed 1000 records.)
	 * 
	 * @param key
	 * @param valueList
	 * @return
	 */
	public static String getReplaceStringForIN(String key, Collection valueList) {
		Object value = null;
		StringBuilder oraStr = new StringBuilder("( ");
		if (valueList != null && valueList.size() > 0 && !key.equals("")) {
			int count = 1;
			oraStr.append(key).append(" IN (");
			for (Iterator<Object> iter = valueList.iterator(); iter.hasNext();) {
				value = iter.next();
				// Date instance
				if (value instanceof Date) {
					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
					value = "to_date('" + format.format((Date) value) + "','DD-MON-YYYY HH24:mi:ss')";
				}
				// Add single quotes to string instances
				else if (value instanceof String)
					value = "'" + value + "'";
				// After 1000 values starts a new IN followed by OR
				if (count == 1000) {
					oraStr.append(" , ").append(value).append(")");
					oraStr.append(" OR ").append(key).append(" IN (");
					count = 0;
				} else if (count == 1)
					oraStr.append(value);
				else
					oraStr.append(" , ").append(value);
				count++;
			}
			oraStr.append(")");
		}

		else if (valueList != null && valueList.size() < 1 && !key.equals(""))
			oraStr.append(key).append("=").append(value);

		else
			log.error("Invalid database Statement" + "key :" + key + "ValueList : " + valueList);

		String returnValue = oraStr.toString();
		if (returnValue.endsWith(" OR " + key + " IN ()"))
			returnValue = returnValue.substring(0, returnValue.lastIndexOf(" OR " + key + " IN ()"));

		return returnValue + ")";
	}

	/**
	 * Copy property values from the origin bean to the destination bean for all cases where the property names are the
	 * same.
	 * 
	 * @param dest
	 * @param orig
	 * @throws ModuleException
	 */
	public static void copyProperties(Object dest, Object orig) throws ModuleException {
		try {
			BeanUtils.copyProperties(dest, orig);
		} catch (IllegalAccessException e) {
			throw new ModuleException("platform.data.object.propertycopyerror");
		} catch (InvocationTargetException e) {
			throw new ModuleException("platform.data.object.propertycopyerror");
		}
	}

	/**
	 * Returns the total minutes. The timeformat should be hours:mins [e.g. 23:50]
	 * 
	 * @param timeFormat
	 * @return
	 */
	public static int getTotalMinutes(String timeStampFormat) {
		String hours = timeStampFormat.substring(0, timeStampFormat.indexOf(":"));
		String mins = timeStampFormat.substring(timeStampFormat.indexOf(":") + 1);

		return (new Integer(hours).intValue() * 60) + new Integer(mins).intValue();
	}

	@SuppressWarnings("rawtypes")
	public static String constructINStringForCharactors(Collection codes) {
		String inStr = "";
		Iterator iter = codes.iterator();
		while (iter.hasNext()) {

			inStr = inStr.equals("") ? "'" + iter.next().toString() + "'" : inStr + "," + "'" + iter.next().toString() + "'";

		}
		return inStr;
	}

	@SuppressWarnings("rawtypes")
	public static String constructINStringForInts(Collection codes) {
		String inStr = "";
		Iterator iter = codes.iterator();
		while (iter.hasNext()) {

			inStr = inStr.equals("") ? iter.next().toString() : inStr + "," + iter.next().toString();

		}
		return inStr;
	}

	public static boolean getValue(Boolean bln) {
		return (bln == null) ? false : bln.booleanValue();
	}

	public static Long getIPNumber(String addr) {
		long num = 0;
		if (addr != null) {
			String[] addrArray = addr.split("\\.");
			if (addrArray.length == 4) { // only support ipv4
				for (int i = 0; i < addrArray.length; i++) {
					int power = 3 - i;
					num += ((Integer.parseInt(addrArray[i]) % 256 * Math.pow(256, power)));
				}
			}
		}
		return num;
	}

	public static boolean compareAge(Date dob, Date deptDate, int ageLimit) {
		int[] ageDiffArr = getDateDifferenceInDDMMYYYY(dob, deptDate);
		boolean boolReturn = true;
		if (ageDiffArr[2] > ageLimit) {
			boolReturn = false;
		}
		if (boolReturn) {
			if (ageDiffArr[2] == ageLimit) {
				if (ageDiffArr[0] == 0 && ageDiffArr[1] == 0) {
					boolReturn = true;
				} else {
					boolReturn = false;
				}
			}
		}

		return boolReturn;
	}

	public static boolean checkInfantMinAgeInDays(Date paxDOB, Date depDateTime, int infantAgeLowerBoundaryInDays) {
		long oneDay = 1000 * 60 * 60 * 24;
		long dobInMili = paxDOB.getTime();
		long deptInMili = depDateTime.getTime();

		long numOfDays = -1;

		if (deptInMili > dobInMili) {
			numOfDays = (deptInMili - dobInMili) / oneDay;
		}

		if (numOfDays > 0 && numOfDays <= infantAgeLowerBoundaryInDays) {
			return false;
		} else {
			return true;
		}
	}

	public static int[] getDateDifferenceInDDMMYYYY(Date date1, Date date2) {

		int[] monthDay = { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		Calendar fromDate;
		Calendar toDate;
		int increment = 0;
		int[] ageDiffArr = new int[3];

		int year;
		int month;
		int day;

		Calendar d1 = GregorianCalendar.getInstance();
		d1.setTime(date1);

		Calendar d2 = GregorianCalendar.getInstance();
		d2.setTime(date2);

		if (d1.getTime().getTime() > d2.getTime().getTime()) {
			fromDate = d2;
			toDate = d1;
		} else {
			fromDate = d1;
			toDate = d2;
		}

		if (fromDate.get(Calendar.DAY_OF_MONTH) > toDate.get(Calendar.DAY_OF_MONTH)) {
			increment = monthDay[fromDate.get(Calendar.MONTH)];
		}

		GregorianCalendar cal = new GregorianCalendar();
		boolean isLeapYear = cal.isLeapYear(fromDate.get(Calendar.YEAR));

		if (increment == -1) {
			if (isLeapYear) {
				increment = 29;
			} else {
				increment = 28;
			}
		}

		// DAY CALCULATION
		if (increment != 0) {
			day = (toDate.get(Calendar.DAY_OF_MONTH) + increment) - fromDate.get(Calendar.DAY_OF_MONTH);
			increment = 1;
		} else {
			day = toDate.get(Calendar.DAY_OF_MONTH) - fromDate.get(Calendar.DAY_OF_MONTH);
		}

		// MONTH CALCULATION
		if ((fromDate.get(Calendar.MONTH) + increment) > toDate.get(Calendar.MONTH)) {
			month = (toDate.get(Calendar.MONTH) + 12) - (fromDate.get(Calendar.MONTH) + increment);
			increment = 1;
		} else {
			month = (toDate.get(Calendar.MONTH)) - (fromDate.get(Calendar.MONTH) + increment);
			increment = 0;
		}

		// YEAR CALCULATION
		year = toDate.get(Calendar.YEAR) - (fromDate.get(Calendar.YEAR) + increment);

		ageDiffArr[0] = day;
		ageDiffArr[1] = month;
		ageDiffArr[2] = year;

		return ageDiffArr;
	}

	public static String extractFlightNumber(String flightData) {
		String flightNo = "";
		if (flightData != null) {
			if (flightData.length() > 2) {
				flightNo = flightData.substring(2);
			}
		}
		return flightNo;
	}

	public static String getDepartureAirportCode(String segmentCode) {
		String departureAirportCode = "";
		if (segmentCode != null) {
			String[] segmentCodes = segmentCode.split(CommonsConstants.SEGMENT_SEPARATOR);
			if (segmentCodes.length >= 1) {
				departureAirportCode = segmentCodes[0];
			}
		}
		return departureAirportCode;
	}

	public static String getAirvalAirportCode(String segmentCode) {
		String arrivalAirportCode = "";
		if (segmentCode != null) {
			String[] segmentCodes = segmentCode.split(CommonsConstants.SEGMENT_SEPARATOR);
			if (segmentCodes.length >= 2) {
				arrivalAirportCode = segmentCodes[segmentCodes.length - 1];
			}
		}
		return arrivalAirportCode;
	}

	public static String getTimeHHMM(long time) {
		String hhmm = "";
		long days = TimeUnit.MILLISECONDS.toDays(time);
		long hours = TimeUnit.MILLISECONDS.toHours(time - TimeUnit.DAYS.toMillis(days));
		long minutes = TimeUnit.MILLISECONDS.toMinutes(time - (TimeUnit.DAYS.toMillis(days) + TimeUnit.HOURS.toMillis(hours)));
		hhmm = days * 24 + hours + ":" + minutes;
		return hhmm;
	}

	public static int getHHMMTimeDurationInMints(String HHMMDuration) {
		String[] arrHHMM = HHMMDuration.split(":");
		int mintsDuration = (Integer.parseInt(arrHHMM[0]) * 60) + Integer.parseInt(arrHHMM[1]);
		return mintsDuration;
	}

	/**
	 * Replace place holder string from an original string
	 * 
	 * @param originalString
	 * @param placeHolders
	 * @return
	 */
	public static String replacePlaceHolderValues(String originalString, String stringToReplace, String... placeHolders) {

		for (String placeHolder : placeHolders) {
			originalString = originalString.replaceAll(placeHolder, stringToReplace);
		}
		return originalString;
	}

	public static String reverseOndCode(String ond) {
		String newOnd = "";
		if (ond != null) {
			String[] airports = ond.split("/");
			for (int i = airports.length - 1; i >= 0; i--) {
				if (!newOnd.equals(""))
					newOnd += "/";

				newOnd += airports[i];
			}

		}

		return newOnd;
	}

	public static boolean isNull(String value) {
		return (value == null);
	}

	public static String replaceRoute(String originalText, String textToReplace) {
		StringBuffer newText = new StringBuffer();

		String placeHolderMark = DEFAULT_PLACE_HOLDER_MARK;

		String placeHolderMarkStart = "\\" + placeHolderMark.substring(0, 1);
		String placeHolderMarkEnd = "\\" + placeHolderMark.substring(1, 2);

		if ((originalText.split(placeHolderMarkStart)).length > 1 && (originalText.split(placeHolderMarkEnd)).length > 1) {
			newText.append((originalText.split(placeHolderMarkStart))[0]);
			newText.append(textToReplace);
			newText.append((originalText.split(placeHolderMarkEnd))[1]);
		} else {
			newText.append(originalText);
		}

		return newText.toString();
	}

	public static String buildIntegerInClauseContent(String field, List<Integer> items) {
		StringBuffer inClosure = new StringBuffer();
		int loops = items.size() / 1000;
		int start = 0;
		int end = items.size();

		for (int i = 0; i <= loops; i++) {
			start = i * 1000;
			end = ((i + 1) * 1000) > items.size() ? items.size() : ((i + 1) * 1000);
			if (i != 0) {
				inClosure.append("or ");
			}
			inClosure.append(field).append(" IN (").append(buildIntegerInClauseContent(items.subList(start, end))).append(") ");
		}
		return inClosure.toString();
	}

	public static String buildIntegerSQLInClauseContent(Collection<Integer> items) {
		StringBuffer inClosure = new StringBuffer();

		int i = 0;
		if (items != null && !items.isEmpty()) {
			for (Integer integer : items) {
				if (i != 0) {
					inClosure.append(", ");
				}
				inClosure.append(integer);
				i++;
			}
		}

		return inClosure.toString();
	}

	public static String buildIntegerAndClauseContent(Collection codes, String fieldName) {
		return buildAndClauseContent(codes, CODE_TYPE_INTEGER, fieldName);
	}

	public static String buildIntegerOrClauseContent(Collection codes, String fieldName) {
		return buildOrClauseContent(codes, CODE_TYPE_INTEGER, fieldName);
	}

	public static String buildStringAndClauseContent(Collection codes, String fieldName) {
		return buildAndClauseContent(codes, CODE_TYPE_STRING, fieldName);
	}

	private static String buildAndClauseContent(Collection codes, String type, String fieldName) {
		String codeString = "";
		String equal = "=";
		String condition = " AND ";

		if (type.equals(CODE_TYPE_INTEGER)) {
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = ((Integer) iter.next()).toString();
				codeString += fieldName + equal + strCode;
				if (iter.hasNext()) {
					codeString = (!codeString.equals("")) ? (codeString + condition) : codeString;
				}
			}
		} else {
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = (iter.next()).toString();
				codeString += fieldName + equal + "'" + strCode + "'";
				if (iter.hasNext()) {
					codeString = (!codeString.equals("")) ? (codeString + condition) : codeString;
				}
			}
		}
		return codeString;
	}

	private static String buildOrClauseContent(Collection codes, String type, String fieldName) {
		String codeString = "";
		String equal = "=";
		String condition = " OR ";

		if (type.equals(CODE_TYPE_INTEGER)) {
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = ((Integer) iter.next()).toString();
				codeString += fieldName + equal + strCode;
				if (iter.hasNext()) {
					codeString = (!codeString.equals("")) ? (codeString + condition) : codeString;
				}
			}
		} else {
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = (iter.next()).toString();
				codeString += fieldName + equal + "'" + strCode + "'";
				if (iter.hasNext()) {
					codeString = (!codeString.equals("")) ? (codeString + condition) : codeString;
				}
			}
		}
		return " ( " + codeString + " ) ";

	}

	public static <T> List<List<T>> sliceListToListOfLists(List<T> inputList, int sliceSize) {

		int inputListSize = inputList.size();
		List<List<T>> outputList = new ArrayList<List<T>>();
		if (inputListSize <= sliceSize) {
			outputList.add(inputList);
			return outputList;
		}

		int noOfSlices = inputListSize / sliceSize;
		int lastSliceSize = inputListSize % sliceSize;

		for (int i = 0; i < noOfSlices; i++) {
			outputList.add(inputList.subList(i * sliceSize, (i + 1) * sliceSize));
		}

		if (lastSliceSize > 0) {
			outputList.add(inputList.subList(noOfSlices * sliceSize, (noOfSlices * sliceSize) + lastSliceSize));
		}

		return outputList;
	}

	/**
	 * Making the String Compatible Spaces
	 *
	 * @param value
	 *            the string to make compatible
	 * @return String the compatible String
	 */
	public static String makeStringCompliant(String value) {
		char[] charArray = PlatformUtiltiies.nullHandler(value).toUpperCase().toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			int ascii = (int) charArray[i];
			if (ascii < 65 | ascii > 90) {
				charArray[i] = ' ';
			}
		}
		String g = new String(charArray);
		return g.replaceAll(" ", "");
	}
}
