package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class OriginatorInformation implements Serializable {	

    private SystemDetails senderSystemDetails;

    private String agentId;

    private String officeId;

    private String systemId;

    private Location agentLocation;

    private SystemDetails initiatorSystemDetails;

    private String agentCountryCode;

    private String agentCurrencyCode;

    private String agentLanguageCode;

    private String originatorTypeCode;

    private String originatorAuthorityCode;

    private String communicationNumber;

    private String partyId;

    public SystemDetails getSenderSystemDetails() {
        return senderSystemDetails;
    }

    public void setSenderSystemDetails(SystemDetails senderSystemDetails) {
        this.senderSystemDetails = senderSystemDetails;
    }

    public String getAgentId() {
        return agentId;
    }

    public OriginatorInformation setAgentId(String agentId) {
        this.agentId = agentId;
        return this;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public Location getAgentLocation() {
        return agentLocation;
    }

    public void setAgentLocation(Location agentLocation) {
        this.agentLocation = agentLocation;
    }

    public SystemDetails getInitiatorSystemDetails() {
        return initiatorSystemDetails;
    }

    public void setInitiatorSystemDetails(SystemDetails initiatorSystemDetails) {
        this.initiatorSystemDetails = initiatorSystemDetails;
    }

    public String getAgentCountryCode() {
        return agentCountryCode;
    }

    public void setAgentCountryCode(String agentCountryCode) {
        this.agentCountryCode = agentCountryCode;
    }

    public String getAgentCurrencyCode() {
        return agentCurrencyCode;
    }

    public void setAgentCurrencyCode(String agentCurrencyCode) {
        this.agentCurrencyCode = agentCurrencyCode;
    }

    public String getAgentLanguageCode() {
        return agentLanguageCode;
    }

    public void setAgentLanguageCode(String agentLanguageCode) {
        this.agentLanguageCode = agentLanguageCode;
    }

    public String getOriginatorTypeCode() {
        return originatorTypeCode;
    }

    public void setOriginatorTypeCode(String originatorTypeCode) {
        this.originatorTypeCode = originatorTypeCode;
    }

    public String getOriginatorAuthorityCode() {
        return originatorAuthorityCode;
    }

    public void setOriginatorAuthorityCode(String originatorAuthorityCode) {
        this.originatorAuthorityCode = originatorAuthorityCode;
    }

    public String getCommunicationNumber() {
        return communicationNumber;
    }

    public void setCommunicationNumber(String communicationNumber) {
        this.communicationNumber = communicationNumber;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

}
