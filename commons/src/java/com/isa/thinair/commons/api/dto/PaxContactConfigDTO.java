package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.List;

public class PaxContactConfigDTO implements Serializable {

	private static final long serialVersionUID = 6350592002340885124L;

	private List PaxConfigList;
	private List contactConfigList;
	private PaxValildationTO paxValidation;

	/**
	 * @return the paxConfigList
	 */
	public List getPaxConfigList() {
		return PaxConfigList;
	}

	/**
	 * @param paxConfigList
	 *            the paxConfigList to set
	 */
	public void setPaxConfigList(List paxConfigList) {
		PaxConfigList = paxConfigList;
	}

	/**
	 * @return the contactConfigList
	 */
	public List getContactConfigList() {
		return contactConfigList;
	}

	/**
	 * @param contactConfigList
	 *            the contactConfigList to set
	 */
	public void setContactConfigList(List contactConfigList) {
		this.contactConfigList = contactConfigList;
	}

	public PaxValildationTO getPaxValidation() {
		return paxValidation;
	}

	public void setPaxValidation(PaxValildationTO paxValidation) {
		this.paxValidation = paxValidation;
	}

}
