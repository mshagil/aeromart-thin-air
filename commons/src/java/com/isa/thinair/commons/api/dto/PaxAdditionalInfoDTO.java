package com.isa.thinair.commons.api.dto;

import java.util.Date;

/**
 * 
 * @author Manoj Dhanushka 
 * 		   In most methods, pax additional details are passed as arguments. When we are adding new
 *         fields to pax additional details, we have to change all method signatures. This class is 
 *         added to avoid that issue
 */
public class PaxAdditionalInfoDTO {
	
	private String passportNo;
	private Date passportExpiry;
	private String passportIssuedCntry;
	private String employeeId;
	private Date dateOfJoin;
	private String idCategory;
	private String placeOfBirth; 
	private String travelDocumentType; 
	private String visaDocNumber;
	private String visaDocPlaceOfIssue ;
	private Date visaDocIssueDate; 
	private String visaApplicableCountry;
	private String ffid;
	private String nationalIDNo;

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public Date getPassportExpiry() {
		return passportExpiry;
	}

	public void setPassportExpiry(Date passportExpiry) {
		this.passportExpiry = passportExpiry;
	}

	public String getPassportIssuedCntry() {
		return passportIssuedCntry;
	}

	public void setPassportIssuedCntry(String passportIssuedCntry) {
		this.passportIssuedCntry = passportIssuedCntry;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Date getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public String getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(String idCategory) {
		this.idCategory = idCategory;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getTravelDocumentType() {
		return travelDocumentType;
	}

	public void setTravelDocumentType(String travelDocumentType) {
		this.travelDocumentType = travelDocumentType;
	}

	public String getVisaDocNumber() {
		return visaDocNumber;
	}

	public void setVisaDocNumber(String visaDocNumber) {
		this.visaDocNumber = visaDocNumber;
	}

	public String getVisaDocPlaceOfIssue() {
		return visaDocPlaceOfIssue;
	}

	public void setVisaDocPlaceOfIssue(String visaDocPlaceOfIssue) {
		this.visaDocPlaceOfIssue = visaDocPlaceOfIssue;
	}

	public Date getVisaDocIssueDate() {
		return visaDocIssueDate;
	}

	public void setVisaDocIssueDate(Date visaDocIssueDate) {
		this.visaDocIssueDate = visaDocIssueDate;
	}

	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public String getNationalIDNo() {
		return nationalIDNo;
	}

	public void setNationalIDNo(String nationalIDNo) {
		this.nationalIDNo = nationalIDNo;
	}
}
