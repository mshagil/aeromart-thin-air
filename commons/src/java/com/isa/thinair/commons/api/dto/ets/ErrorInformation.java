package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class ErrorInformation implements Serializable {

    private String applicationErrorCode;

    private String subjectQualifier;

    private List<String> text;

    public String getApplicationErrorCode() {
        return applicationErrorCode;
    }

    public void setApplicationErrorCode(String applicationErrorCode) {
        this.applicationErrorCode = applicationErrorCode;
    }

    public String getSubjectQualifier() {
        return subjectQualifier;
    }

    public void setSubjectQualifier(String subjectQualifier) {
        this.subjectQualifier = subjectQualifier;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }
}
