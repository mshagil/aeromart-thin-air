package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public enum FormOfPaymentIndicator implements Serializable {

    AGT,
    CA,
    CC,
    CK,
    DP,
    GR,
    MS,
    NR,
    PT,
    SGR,
    UN;

    public String value() {
        return name();
    }

    public static FormOfPaymentIndicator fromValue(String v) {
        return valueOf(v);
    }

}
