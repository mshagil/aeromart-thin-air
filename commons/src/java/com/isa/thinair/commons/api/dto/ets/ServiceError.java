package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class ServiceError implements Serializable {
	
    private String code;
    
    private String message;

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
