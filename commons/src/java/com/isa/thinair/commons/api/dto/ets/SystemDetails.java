package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class SystemDetails implements Serializable {
	
    private String companyCode;

    private Location location;

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
