package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * Holds configurations related to customer registration & profile update
 * 
 * @author rumesh
 * 
 */

public class UserRegConfigDTO extends BaseContactConfigDTO implements Serializable {

	private static final long serialVersionUID = -7864851490231007464L;

	private boolean userRegVisibilty;

	/**
	 * @return the userRegVisibilty
	 */
	public boolean isUserRegVisibilty() {
		return userRegVisibilty;
	}

	/**
	 * @param userRegVisibilty
	 *            the userRegVisibilty to set
	 */
	public void setUserRegVisibilty(boolean userRegVisibilty) {
		this.userRegVisibilty = userRegVisibilty;
	}
}
