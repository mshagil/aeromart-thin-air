package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class Location implements Serializable {
	
    private String locationCode;

    private String locationName;

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
