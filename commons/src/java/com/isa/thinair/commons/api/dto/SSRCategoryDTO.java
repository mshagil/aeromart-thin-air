package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class SSRCategoryDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5021609907641133400L;
	private Integer sSRCategoryId;
	private String description;
	private String status;

	private int addNotifyStartCutoverTime;
	private int addNotifyStopCutoverTime;

	private int cancelNotifyStartCutoverTime;
	private int cancelNotifyStopCutoverTime;

	private int sSRBookingCutoverTime;

	private boolean toBeSendWithPNLADL;

	private boolean toBeSendWithMail;

	private int notifyStartCutoverTime;

	public int getNotifyStartCutoverTime() {
		return notifyStartCutoverTime;
	}

	public void setNotifyStartCutoverTime(int notifyStartCutoverTime) {
		this.notifyStartCutoverTime = notifyStartCutoverTime;
	}

	public int getNotifyStopCutoverTime() {
		return notifyStopCutoverTime;
	}

	public void setNotifyStopCutoverTime(int notifyStopCutoverTime) {
		this.notifyStopCutoverTime = notifyStopCutoverTime;
	}

	public int getNotificationAttempts() {
		return notificationAttempts;
	}

	public void setNotificationAttempts(int notificationAttempts) {
		this.notificationAttempts = notificationAttempts;
	}

	private int notifyStopCutoverTime;
	private int notificationAttempts;

	public Integer getSSRCategoryId() {
		return this.sSRCategoryId;
	}

	public void setSSRCategoryId(Integer categoryId) {
		this.sSRCategoryId = categoryId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getAddNotifyStartCutoverTime() {
		return this.addNotifyStartCutoverTime;
	}

	public void setAddNotifyStartCutoverTime(int startCutoverTime) {
		this.addNotifyStartCutoverTime = startCutoverTime;
	}

	public int getAddNotifyStopCutoverTime() {
		return this.addNotifyStopCutoverTime;
	}

	public void setAddNotifyStopCutoverTime(int stopCutoverTime) {
		this.addNotifyStopCutoverTime = stopCutoverTime;
	}

	public int getCancelNotifyStartCutoverTime() {
		return this.cancelNotifyStartCutoverTime;
	}

	public void setCancelNotifyStartCutoverTime(int cancelNotifyStartCutoverTime) {
		this.cancelNotifyStartCutoverTime = cancelNotifyStartCutoverTime;
	}

	public int getCancelNotifyStopCutoverTime() {
		return this.cancelNotifyStopCutoverTime;
	}

	public void setCancelNotifyStopCutoverTime(int cancelNotifyStopCutoverTime) {
		this.cancelNotifyStopCutoverTime = cancelNotifyStopCutoverTime;
	}

	public boolean isToBeSendWithPNLADL() {
		return this.toBeSendWithPNLADL;
	}

	public void setToBeSendWithPNLADL(boolean toBeSendWithPNLADL) {
		this.toBeSendWithPNLADL = toBeSendWithPNLADL;
	}

	public boolean isToBeSendWithMail() {
		return this.toBeSendWithMail;
	}

	public void setToBeSendWithMail(boolean toBeSendWithMail) {
		this.toBeSendWithMail = toBeSendWithMail;
	}

	public int getSSRBookingCutoverTime() {
		return this.sSRBookingCutoverTime;
	}

	public void setSSRBookingCutoverTime(int bookingCutoverTime) {
		this.sSRBookingCutoverTime = bookingCutoverTime;
	}

}
