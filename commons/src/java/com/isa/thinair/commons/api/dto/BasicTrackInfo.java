package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * Contain basic tracking info listed below
 * <ul>
 * <li>callingInstanceId</li>
 * <li>sessionId</li>
 * </ul>
 * 
 * @author Dilan Anuruddha
 * 
 */
public class BasicTrackInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String callingInstanceId;

	/** Holds the sessionId */
	private String sessionId;
	
	/** Holds the marketing user id */
	private String marketingUserId;

	public BasicTrackInfo() {
		super();
	}

	public String getCallingInstanceId() {
		return callingInstanceId;
	}

	public void setCallingInstanceId(String callingInstanceId) {
		this.callingInstanceId = callingInstanceId;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId
	 *            the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the marketingUserId
	 */
	public String getMarketingUserId() {
		return marketingUserId;
	}

	/**
	 * @param marketingUserId
	 *            the marketingUserId to set
	 */
	public void setMarketingUserId(String marketingUserId) {
		this.marketingUserId = marketingUserId;
	}

}