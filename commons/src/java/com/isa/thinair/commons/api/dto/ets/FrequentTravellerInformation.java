package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class FrequentTravellerInformation implements Serializable {

    private String airlineDesignator;

    private String frequentTravellerNumber;

    public String getAirlineDesignator() {
        return airlineDesignator;
    }

    public void setAirlineDesignator(String airlineDesignator) {
        this.airlineDesignator = airlineDesignator;
    }

    public String getFrequentTravellerNumber() {
        return frequentTravellerNumber;
    }

    public void setFrequentTravellerNumber(String frequentTravellerNumber) {
        this.frequentTravellerNumber = frequentTravellerNumber;
    }
}
