package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class PricingTicketingDetails implements Serializable {

    private List<PricingTicketingIndicators> pricingTicketingIndicators;

    private String ticketingTime;

    private CountryLocation ticketingLocation;

    public List<PricingTicketingIndicators> getPricingTicketingIndicators() {
        return pricingTicketingIndicators;
    }

    public void setPricingTicketingIndicators(List<PricingTicketingIndicators> pricingTicketingIndicators) {
        this.pricingTicketingIndicators = pricingTicketingIndicators;
    }

    public String getTicketingTime() {
        return ticketingTime;
    }

    public void setTicketingTime(String ticketingTime) {
        this.ticketingTime = ticketingTime;
    }

    public CountryLocation getTicketingLocation() {
        return ticketingLocation;
    }

    public void setTicketingLocation(CountryLocation ticketingLocation) {
        this.ticketingLocation = ticketingLocation;
    }

    public static class PricingTicketingIndicators implements Serializable {

        private String key;

        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
