package com.isa.thinair.commons.api.client;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;

public class BDLookupConfig {
	private String _moduleName;

	private Properties _jndiProperties = new Properties();

	private Map<String, String> _businessDelegates = new HashMap<String, String>();

	private String _locality;

	public static interface Locality {
		public final String REMOTE = "remote";

		public final String LOCAL = "local";
	}

	public BDLookupConfig() {
	}

	public BDLookupConfig(String moduleName) {
		_moduleName = moduleName;
	}

	public Map getBusinessDelegates() {
		return _businessDelegates;
	}

	public void setBusinessDelegates(Map<String, String> delegates) {
		_businessDelegates = delegates;
	}

	public Properties getJndiProperties() {
		return _jndiProperties;
	}

	public void setJndiProperties(Properties properties) {
		_jndiProperties = properties;
	}

	public String getLocality() {
		return _locality;
	}

	public void setLocality(String locality) {
		this._locality = locality;
	}

	public String getModuleName() {
		return _moduleName;
	}

	public void setModuleName(String moduleName) {
		this._moduleName = moduleName;
	}

	public String getFQClassName(String bdKey) {
		String fullBDKey = bdKey + "." + getLocality();
		String fullyQulifiedClassName;
		if (getBusinessDelegates().containsKey(fullBDKey)) {
			fullyQulifiedClassName = (String) getBusinessDelegates().get(fullBDKey);
		} else {
			System.out.println("Requested bd key does not exists for module [moduleName=" + getModuleName() + ", requestedBDkey="
					+ fullBDKey + "]");
			throw new ModuleRuntimeException("commons.client.bdconfig.notfound");
		}
		return fullyQulifiedClassName;
	}
}
