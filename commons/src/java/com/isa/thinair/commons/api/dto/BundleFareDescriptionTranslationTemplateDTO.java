package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class BundleFareDescriptionTranslationTemplateDTO implements Serializable {

	private static final long serialVersionUID = 11283543657568798L;

	private Integer templateID;

	private String languageCode;

	private String freeServicesContent;

	private String paidServicesContent;

	private Long version;

	public Integer getTemplateID() {
		return templateID;
	}

	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	public String getFreeServicesContent() {
		return freeServicesContent;
	}

	public void setFreeServicesContent(String freeServicesContent) {
		this.freeServicesContent = freeServicesContent;
	}

	public String getPaidServicesContent() {
		return paidServicesContent;
	}

	public void setPaidServicesContent(String paidServicesContent) {
		this.paidServicesContent = paidServicesContent;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
}
