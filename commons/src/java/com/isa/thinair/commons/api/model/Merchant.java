package com.isa.thinair.commons.api.model;

/**
 * @author chethiya
 * @hibernate.class table = "T_MERCHANT"
 * 
 **/
public class Merchant {

	private String merchantID;

	private String displayName;

	private String username;

	private String password;

	private String secureHashKey;

	private String status;
	
	private String entityCode;

	/**
	 * @return the domainName
	 * @hibernate.id column = "MERCHANT_ID" generator-class = "assigned"
	 */
	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	/**
	 * @return the displayName
	 * @hibernate.property column = "DISPLAY_NAME"
	 */
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the username
	 * @hibernate.property column = "USERNAME"
	 */
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 * @hibernate.property column = "PASSWORD"
	 */
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the secureHashKey
	 * @hibernate.property column = "SECURE_HASH_KEY"
	 */
	public String getSecureHashKey() {
		return secureHashKey;
	}

	public void setSecureHashKey(String secureHashKey) {
		this.secureHashKey = secureHashKey;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the entityCode
	 * @hibernate.property column = "ENTITY_CODE"
	 */

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

}
