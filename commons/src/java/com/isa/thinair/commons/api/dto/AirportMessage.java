package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class AirportMessage implements Serializable {

	private static final long serialVersionUID = 8293307023844929515L;

	private String airport;

	private String stage;

	private Set<String> salesChannels;

	private Integer airportMessageId;

	private String depArrType;

	private String message;

	private Set<String> inclusionONDs;

	private Set<String> exclusionONDs;

	private Set<String> inclusionFligths;

	private Set<String> exclusionFlights;

	private Date timestampFrom;

	private Date timestampTo;

	private Date travelValidityFrom;

	private Date travelValidityTo;

	private String i18nMessageKey;

	public String getAirport() {
		return airport;
	}

	public void setAirport(String airport) {
		this.airport = airport;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public Integer getAirportMessageId() {
		return airportMessageId;
	}

	public void setAirportMessageId(Integer airportMessageId) {
		this.airportMessageId = airportMessageId;
	}

	public String getDepArrType() {
		return depArrType;
	}

	public void setDepArrType(String depArrType) {
		this.depArrType = depArrType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Set<String> getInclusionONDs() {
		if (inclusionONDs == null)
			inclusionONDs = new HashSet<String>();
		return inclusionONDs;
	}

	public Set<String> getExclusionONDs() {
		if (exclusionONDs == null)
			exclusionONDs = new HashSet<String>();
		return exclusionONDs;
	}

	public Set<String> getInclusionFligths() {
		if (inclusionFligths == null)
			inclusionFligths = new HashSet<String>();
		return inclusionFligths;
	}

	public Set<String> getExclusionFlights() {
		if (exclusionFlights == null)
			exclusionFlights = new HashSet<String>();
		return exclusionFlights;
	}

	public Set<String> getSalesChannels() {
		if (salesChannels == null)
			salesChannels = new HashSet<String>();
		return salesChannels;
	}

	public Date getTimestampFrom() {
		return timestampFrom;
	}

	public void setTimestampFrom(Date timestampFrom) {
		this.timestampFrom = timestampFrom;
	}

	public Date getTimestampTo() {
		return timestampTo;
	}

	public void setTimestampTo(Date timestampTo) {
		this.timestampTo = timestampTo;
	}

	public Date getTravelValidityFrom() {
		return travelValidityFrom;
	}

	public void setTravelValidityFrom(Date travelValidityFrom) {
		this.travelValidityFrom = travelValidityFrom;
	}

	public Date getTravelValidityTo() {
		return travelValidityTo;
	}

	public void setTravelValidityTo(Date travelValidityTo) {
		this.travelValidityTo = travelValidityTo;
	}

	public String getI18nMessageKey() {
		return i18nMessageKey;
	}

	public void setI18nMessageKey(String i18nMessageKey) {
		this.i18nMessageKey = i18nMessageKey;
	}

}
