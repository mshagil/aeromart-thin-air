package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * 
 * @author rajitha
 *
 */
public class FlightStatusUpdateReqDTO implements Serializable{

	private String flightNumber;
	private Date departureDate;
	private Collection<TicketUpdateStatusDetails> records;
	private RequestIdentity requestIdentity;
	private String userId;
	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public Date getDepartureDate() {
		return departureDate;
	}
	
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	
	public Collection<TicketUpdateStatusDetails> getRecords() {
		return records;
	}
	
	public void setRecords(Collection<TicketUpdateStatusDetails> ticketStatusDetails) {
		this.records = ticketStatusDetails;
	}
	
	public RequestIdentity getRequestIdentity() {
		return requestIdentity;
	}
	
	public void setRequestIdentity(RequestIdentity requestIdentity) {
		this.requestIdentity = requestIdentity;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
