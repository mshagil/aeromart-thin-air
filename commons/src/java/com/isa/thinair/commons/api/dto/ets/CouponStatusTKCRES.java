package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class CouponStatusTKCRES implements Serializable {

	private  MessageFunction messageFunction;

	public MessageFunction getMessageFunction() {
		return messageFunction;
	}
	

	public void setMessageFunction(MessageFunction messageFunction) {
		this.messageFunction = messageFunction;
	}
	
}
