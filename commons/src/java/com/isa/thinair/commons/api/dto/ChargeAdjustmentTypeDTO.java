package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * The transfer object denoting the charge adjustment type.
 * 
 * @author sanjaya
 * 
 */
public class ChargeAdjustmentTypeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The adjustment type id */
	private Integer chargeAdjustmentTypeId;

	/** The adjustment type name */
	private String chargeAdjustmentTypeName;

	/** The description of the adjustment charge type */
	private String description;

	/** The charge code associated with the refundable type */
	private String refundableChargeCode;

	/** The charge code associated with the non refundable type */
	private String nonRefundableChargeCode;

	/** The privlege associated with allowing refundable adjustments of this type */
	private String refundablePrivilegeId;

	/** The privlege associated with allowing non refundable adjustments of this type */
	private String nonRefundablePrivilegeId;

	/** Whether this is the default charge adjustment */
	private Boolean defaultAdjustment;

	/** The display order of the adjustment type */
	private Integer displayOrder;

	/** The staus of the adjustment type record */
	private String status;

	/** ACTive status of the entry. */
	public static final String STATUS_ACTIVE = "ACT";

	/** INActive status of the entry. */
	public static final String STATUS_INACTIVE = "INA";

	/**
	 * @return
	 */
	public Integer getChargeAdjustmentTypeId() {
		return chargeAdjustmentTypeId;
	}

	/**
	 * @param chargeAdjustmentTypeId
	 */
	public void setChargeAdjustmentTypeId(Integer chargeAdjustmentTypeId) {
		this.chargeAdjustmentTypeId = chargeAdjustmentTypeId;
	}

	/**
	 * @return
	 */
	public String getChargeAdjustmentTypeName() {
		return chargeAdjustmentTypeName;
	}

	/**
	 * @param chargeAdjustmentTypeName
	 */
	public void setChargeAdjustmentTypeName(String chargeAdjustmentTypeName) {
		this.chargeAdjustmentTypeName = chargeAdjustmentTypeName;
	}

	/**
	 * @return : The charge adjustment type description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description The charge adjustment type description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return : The charge code related to refundable charge adjustments of this type.
	 */
	public String getRefundableChargeCode() {
		return refundableChargeCode;
	}

	/**
	 * @param refundableChargeCode : The charge code related to refundable charge adjustments of this type to set.
	 */
	public void setRefundableChargeCode(String refundableChargeCode) {
		this.refundableChargeCode = refundableChargeCode;
	}

	/**
	 * @return : The charge code related to non-refundable charge adjustments of this type.
	 */
	public String getNonRefundableChargeCode() {
		return nonRefundableChargeCode;
	}

	/**
	 * @param nonRefundableChargeCode The charge code related to non-refundable charge adjustments of this type to set.
	 */
	public void setNonRefundableChargeCode(String nonRefundableChargeCode) {
		this.nonRefundableChargeCode = nonRefundableChargeCode;
	}

	/**
	 * @return : The privilege related to refundable adjustments.
	 */
	public String getRefundablePrivilegeId() {
		return refundablePrivilegeId;
	}

	/**
	 * @param refundablePrivilegeId : The privilege related to refundable adjustments to set.
	 */
	public void setRefundablePrivilegeId(String refundablePrivilegeId) {
		this.refundablePrivilegeId = refundablePrivilegeId;
	}

	/**
	 * @return
	 */
	public String getNonRefundablePrivilegeId() {
		return nonRefundablePrivilegeId;
	}

	/**
	 * @param nonRefundablePrivilegeId : The privilege related to non-refundable adjustments.
	 */
	public void setNonRefundablePrivilegeId(String nonRefundablePrivilegeId) {
		this.nonRefundablePrivilegeId = nonRefundablePrivilegeId;
	}

	/**
	 * @return : true if this entry is the default adjustment type. false otherwise.
	 */
	public Boolean getDefaultAdjustment() {
		return defaultAdjustment;
	}

	/**
	 * @param defaultAdjustment
	 *            : The default adjustment flag to set.
	 */
	public void setDefaultAdjustment(Boolean defaultAdjustment) {
		this.defaultAdjustment = defaultAdjustment;
	}

	/**
	 * @return : The display order.
	 */
	public Integer getDisplayOrder() {
		return displayOrder;
	}

	/**
	 * @param displayOrder
	 *            : The display order to set.
	 */
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	/**
	 * @return : The status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            : The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ChargeAdjustmentTypeDTO [ ");
		sb.append(" chargeAdjustmentTypeId = " + chargeAdjustmentTypeId);
		sb.append(" chargeAdjustmentTypeName = " + chargeAdjustmentTypeName);
		sb.append(" description	 = " + description);
		sb.append(" refundableChargeCode =" + refundableChargeCode);
		sb.append(" nonRefundableChargeCode = " + nonRefundableChargeCode);
		sb.append(" refundablePrivilegeId	= " + refundablePrivilegeId);
		sb.append(" nonRefundablePrivilegeId	= " + nonRefundablePrivilegeId);
		sb.append(" defaultAdjustment = " + defaultAdjustment);
		sb.append(" displayOrder = " + displayOrder);
		sb.append(" status = " + status);
		sb.append("]");
		return sb.toString();
	}
}