package com.isa.thinair.commons.api.exception;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class MessagesUtil {

	private static final String SYSTEM_MESSAGES_CODES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/systemmessages";
	private static final String USER_MESSEAGES_CODES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/usermessages";
	private static final String MODULES_CODES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/modulecodes";
	private static final String EXCEPTION_CODES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/exceptioncodes";

	private static final String DEFULT_MODULE_DESC_CODE = "module.desc.code.empty";
	private static final String DEFULT_USER_MESSAGE_CODE = "um.module.usermessage.code.empty";
	private static final String DEFULT_ERROR_CODE = "module.default.error";

	private static ResourceBundle systemMessagesCodes;
	private static ResourceBundle moduleDescCodes;
	private static ResourceBundle userMessagesCodes;
	private static ResourceBundle exceptionCodes;

	private static Boolean budlesLoaded = new Boolean(false);

	private static void loadResourceBundlesIfNotLoaded() {
		if (!budlesLoaded.booleanValue()) {
			synchronized (budlesLoaded) {
				Locale locale = new Locale("en");
				systemMessagesCodes = ResourceBundle.getBundle(SYSTEM_MESSAGES_CODES_RESOURCE_BUNDLE_NAME, locale);
				moduleDescCodes = ResourceBundle.getBundle(MODULES_CODES_RESOURCE_BUNDLE_NAME, locale);
				userMessagesCodes = ResourceBundle.getBundle(USER_MESSEAGES_CODES_RESOURCE_BUNDLE_NAME, locale);
				exceptionCodes = ResourceBundle.getBundle(EXCEPTION_CODES_RESOURCE_BUNDLE_NAME, locale);
				budlesLoaded = new Boolean(true);
			}
		}
	}

	public static String getMessage(String messageCode) {
		loadResourceBundlesIfNotLoaded();

		messageCode = maskNull(messageCode);

		if (messageCode.isEmpty()) {
			return getPropertyValue(userMessagesCodes, DEFULT_USER_MESSAGE_CODE);
		}

		if (messageCode.startsWith("um.")) {
			String message = getPropertyValue(userMessagesCodes, messageCode);
			if (!message.isEmpty())
				return message;
		} else {
			String message = getPropertyValue(systemMessagesCodes, messageCode);
			if (!message.isEmpty())
				return message;
		}

		return getPropertyValue(userMessagesCodes, DEFULT_USER_MESSAGE_CODE);
	}

	public static String getErrorCode(String oracleErrorKey) {
		loadResourceBundlesIfNotLoaded();
		oracleErrorKey = maskNull(oracleErrorKey);

		if (oracleErrorKey.isEmpty()) {
			return getPropertyValue(exceptionCodes, DEFULT_ERROR_CODE);
		} else {
			String error = getPropertyValue(exceptionCodes, oracleErrorKey);

			if (error.isEmpty()) {
				return getPropertyValue(exceptionCodes, DEFULT_ERROR_CODE);
			} else {
				return error;
			}
		}
	}

	public static String getModuleDescription(String moduleCode) {
		loadResourceBundlesIfNotLoaded();
		moduleCode = maskNull(moduleCode);

		if (moduleCode.isEmpty()) {
			return getPropertyValue(moduleDescCodes, DEFULT_MODULE_DESC_CODE);
		} else {
			String message = getPropertyValue(moduleDescCodes, moduleCode);

			if (message.isEmpty()) {
				return getPropertyValue(moduleDescCodes, DEFULT_MODULE_DESC_CODE);
			} else {
				return message;
			}
		}
	}

	private static String maskNull(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	private static String getPropertyValue(ResourceBundle resourceBundle, String key) {
		String value = null;

		try {
			value = resourceBundle.getString(key);
		} catch (NullPointerException e) {
		} catch (MissingResourceException e) {
		} catch (ClassCastException e) {
		}

		return maskNull(value);
	}
}
