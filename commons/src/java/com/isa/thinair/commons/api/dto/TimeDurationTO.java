package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class TimeDurationTO implements Serializable, Cloneable {
	private static final long serialVersionUID = 2791077357242044014L;
	private static final int MIN_FOR_HOUR = 60;
	private static final int MIN_FOR_DAY = 24 * MIN_FOR_HOUR;
	private static final int MIN_FOR_MONMTH = 30 * MIN_FOR_DAY;
	private static final int MIN_FOR_YEAR = 365 * MIN_FOR_DAY;

	private long duratiionMinutes;
	private int years;
	private int months;
	private int days;
	private int hours;
	private int minutes;

	/**
	 * duratiionMinutes = (years * 365 + months * 30 + days) * 24 * 60 * 60 + hours * 60 + minutes
	 * 
	 * @param duratiionMinutes
	 */
	public TimeDurationTO(long duratiionMinutes) {
		this.duratiionMinutes = duratiionMinutes;

		if (duratiionMinutes >= MIN_FOR_YEAR) {
			years = (int) (duratiionMinutes / MIN_FOR_YEAR);
			duratiionMinutes = duratiionMinutes % MIN_FOR_YEAR;
		}

		if (duratiionMinutes >= MIN_FOR_MONMTH) {
			months = (int) (duratiionMinutes / MIN_FOR_MONMTH);
			duratiionMinutes = duratiionMinutes % MIN_FOR_MONMTH;
		}

		if (duratiionMinutes >= MIN_FOR_DAY) {
			days = (int) (duratiionMinutes / MIN_FOR_DAY);
			duratiionMinutes = duratiionMinutes % MIN_FOR_DAY;
		}

		if (duratiionMinutes >= MIN_FOR_HOUR) {
			hours = (int) (duratiionMinutes / MIN_FOR_HOUR);
			duratiionMinutes = duratiionMinutes % MIN_FOR_HOUR;
		}

		minutes = (int) duratiionMinutes;
	}

	public TimeDurationTO(int years, int months, int days, int hours, int minutes) {
		this.years = years;
		this.months = months;
		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
		duratiionMinutes = (years * 365 + months * 30 + days) * 24 * 60 + hours * 60 + minutes;
	}

	public long getDuratiionMinutes() {
		return duratiionMinutes;
	}

	public int getYears() {
		return years;
	}

	public int getMonths() {
		return months;
	}

	public int getDays() {
		return days;
	}

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public static void main(String[] args) {
		TimeDurationTO td1 = new TimeDurationTO(5, 4, 3, 2, 1);
		TimeDurationTO td2 = new TimeDurationTO(td1.getDuratiionMinutes());

		System.out.println(td2);

	}

}
