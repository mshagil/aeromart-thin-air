package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class TicketNumberDetails implements Serializable {

    private String ticketNumber;

    private String documentType;

    private String totalItems;

    private String dataIndicator;

    private List<InteractiveFreeText> text;

    private PricingTicketingDetails pricingTicketingDetails;

    private List<TicketCoupon> ticketCoupon;

    private DateAndTimeInformation dateAndTime;

    private OriginatorInformation originatorInformation;

    private ErrorInformation errorInformation;

    private ConsumerRefInformation consumerRef;

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public String getDataIndicator() {
        return dataIndicator;
    }

    public void setDataIndicator(String dataIndicator) {
        this.dataIndicator = dataIndicator;
    }

    public List<InteractiveFreeText> getText() {
        return text;
    }

    public void setText(List<InteractiveFreeText> text) {
        this.text = text;
    }

    public PricingTicketingDetails getPricingTicketingDetails() {
        return pricingTicketingDetails;
    }

    public void setPricingTicketingDetails(PricingTicketingDetails pricingTicketingDetails) {
        this.pricingTicketingDetails = pricingTicketingDetails;
    }

    public List<TicketCoupon> getTicketCoupon() {
        return ticketCoupon;
    }

    public void setTicketCoupon(List<TicketCoupon> ticketCoupon) {
        this.ticketCoupon = ticketCoupon;
    }

    public DateAndTimeInformation getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(DateAndTimeInformation dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public OriginatorInformation getOriginatorInformation() {
        return originatorInformation;
    }

    public void setOriginatorInformation(OriginatorInformation originatorInformation) {
        this.originatorInformation = originatorInformation;
    }

    public ErrorInformation getErrorInformation() {
        return errorInformation;
    }

    public void setErrorInformation(ErrorInformation errorInformation) {
        this.errorInformation = errorInformation;
    }

    public ConsumerRefInformation getConsumerRef() {
        return consumerRef;
    }

    public void setConsumerRef(ConsumerRefInformation consumerRef) {
        this.consumerRef = consumerRef;
    }
}
