package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * DTO for representing a hub airport.
 * 
 * @author Mohamed Nasly
 */
public class HubAirportTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6008980424536108569L;

	/** The unique identifier */
	private int hubId;

	/** The Hub airport */
	private String airportCode;

	/** Minimum connection time */
	private String minCxnTime;

	/** Maximum connection time */
	private String maxCxnTime;

	/** Carrier code of the inwards flight to hub */
	private String inwardsCarrierCode;

	/** Carrier code of the outwards flight from hub */
	private String outwardsCarrierCode;

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getMaxCxnTime() {
		return maxCxnTime;
	}

	public void setMaxCxnTime(String maxCxnTime) {
		this.maxCxnTime = maxCxnTime;
	}

	public String getMinCxnTime() {
		return minCxnTime;
	}

	public void setMinCxnTime(String minCxnTime) {
		this.minCxnTime = minCxnTime;
	}

	/**
	 * @return the inwardsCarrierCode
	 */
	public String getInwardsCarrierCode() {
		return inwardsCarrierCode;
	}

	/**
	 * @param inwardsCarrierCode
	 *            the inwardsCarrierCode to set
	 */
	public void setInwardsCarrierCode(String inwardsCarrierCode) {
		this.inwardsCarrierCode = inwardsCarrierCode;
	}

	/**
	 * @return the outwardsCarrierCode
	 */
	public String getOutwardsCarrierCode() {
		return outwardsCarrierCode;
	}

	/**
	 * @param outwardsCarrierCode
	 *            the outwardsCarrierCode to set
	 */
	public void setOutwardsCarrierCode(String outwardsCarrierCode) {
		this.outwardsCarrierCode = outwardsCarrierCode;
	}

	/**
	 * @return Returns the hubId.
	 */
	public int getHubId() {
		return hubId;
	}

	/**
	 * @param hubId
	 *            The hubId to set.
	 */
	public void setHubId(int hubId) {
		this.hubId = hubId;
	}
}
