/**
 * 
 */
package com.isa.thinair.commons.api.dto.ondpublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Janaka Padukka
 * 
 */
public class OndPublishAirportDTO implements Serializable {

	private String airportCode;
	
	private boolean city = false;

	private List<LanguageDescriptionDTO> languageDescriptions;

	private List<String> cityAirportCodes;

	public void addLanguageDescription(LanguageDescriptionDTO langDesc) {

		if (languageDescriptions == null) {
			languageDescriptions = new ArrayList<LanguageDescriptionDTO>();
		}
		languageDescriptions.add(langDesc);

	}

	public List<LanguageDescriptionDTO> getLanguageDescriptions() {
		return languageDescriptions;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public void setLanguageDescriptions(
			List<LanguageDescriptionDTO> languageDescriptions) {
		this.languageDescriptions = languageDescriptions;
	}

	public boolean isCity() {
		return city;
	}

	public void setCity(boolean city) {
		this.city = city;
	}

	public List<String> getCityAirportCodes() {
		if (this.cityAirportCodes == null) {
			this.cityAirportCodes = new ArrayList<>();
		}
		return cityAirportCodes;
	}

	public void setCityAirportCodes(List<String> cityAirportCodes) {
		this.cityAirportCodes = cityAirportCodes;
	}
	
}
