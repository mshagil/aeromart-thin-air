package com.isa.thinair.commons.api.dto.ondpublish;

import java.io.Serializable;

import org.apache.commons.lang3.Validate;

public class OndLocationDTO implements Serializable {
	
	private static final long serialVersionUID = -4333510469540886015L;

	private String ondLocationCode;

	private Boolean city;

	public OndLocationDTO(String ondPointCode, Boolean city) {
		Validate.notNull(ondPointCode, "Origin Code Cannot Be Null");
		Validate.notEmpty(ondPointCode, "Origin Code Cannot Be Empty");
		Validate.notNull(city, "City flag Cannot Be Null");

		this.ondLocationCode = ondPointCode;
		this.city = city;
	}
	
	public String getOndLocationCode() {
		return ondLocationCode;
	}

	public void setOndLocationCode(String ondLocationCode) {
		this.ondLocationCode = ondLocationCode;
	}

	public Boolean getCity() {
		return city;
	}

	public void setCity(Boolean city) {
		this.city = city;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((ondLocationCode == null) ? 0 : ondLocationCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OndLocationDTO other = (OndLocationDTO) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (ondLocationCode == null) {
			if (other.ondLocationCode != null)
				return false;
		} else if (!ondLocationCode.equals(other.ondLocationCode))
			return false;
		return true;
	}

}