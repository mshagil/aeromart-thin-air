/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.commons.api.dto;

public class KeyValue implements Comparable<KeyValue> {

	private Object key;

	private Object value;

	public KeyValue(Object key, Object value) {
		this.key = key;
		this.value = value;
	}

	public Object getKey() {
		return key;
	}

	public void setKey(Object key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public int compareTo(KeyValue anotherKeyValue) throws ClassCastException {
		int retValue = 0;

		if (!(anotherKeyValue instanceof KeyValue))
			throw new ClassCastException("A keyValue object expected.");
		Object anotherKey = ((KeyValue) anotherKeyValue).getKey();
		Object anotherValue = ((KeyValue) anotherKeyValue).getValue();

		retValue = compare(key, anotherKey);

		if (retValue == 0) {
			retValue = compare(value, anotherValue);
		}

		return retValue;
	}

	/**
	 * compares two strings
	 * 
	 * @param ob1
	 * @param object2
	 * @return
	 */
	private int compare(Object ob1, Object object2) {
		int retValue = 0;

		if (ob1 == null) {
			if (object2 == null) {
				retValue = 0;
			} else {
				retValue = 1;
			}
		} else if (key.equals(object2)) {
			retValue = 0;
		}

		return retValue;
	}

}
