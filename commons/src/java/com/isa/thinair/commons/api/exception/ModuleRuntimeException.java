/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.exception;

/**
 * The runtime exception every module exposes to the clients
 * 
 * @author Nasly
 * 
 */
public class ModuleRuntimeException extends CommonsRuntimeException {

	private static final long serialVersionUID = -8474371969318125189L;

	public ModuleRuntimeException(Throwable cause, String exceptionCode, String moduleCode) {
		super(cause, exceptionCode, moduleCode);
	}

	public ModuleRuntimeException(Throwable cause, String exceptionCode) {
		super(cause, exceptionCode);
	}

	public ModuleRuntimeException(String exceptionCode, String moduleCode) {
		super(exceptionCode, moduleCode);
	}

	public ModuleRuntimeException(String exceptionCode) {
		super(exceptionCode);
	}

}
