package com.isa.thinair.commons.api.constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public interface CommonsConstants {

	public static final String NEWLINE = System.getProperty("line.separator");
	public static final int INDIVIDUAL_MEMBER = 0;
	public static SimpleDateFormat DATE_FORMAT_FOR_LOGGING = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public static String STATUS_ACTIVE = "ACT";
	public static String STATUS_INACTIVE = "INA";

	public static String YES = "Y";
	public static String NO = "N";

	public static char CHR_YES = 'Y';
	public static char CHR_NO = 'N';

	public static String SEGMENT_SEPARATOR = "/";

	public enum CreditLimitUpdateStatus {
		NOTAPPLICALBE("N"), PENDING("P"), SUCCESS("S"), FAILED("F");

		private String code;

		private CreditLimitUpdateStatus(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}

	}

	public enum BookingCategory {

		STANDARD("STD", "Standard"), CHARTER("CHT", "Charter"), HR("HRR", "HR");

		private String code;
		private String name;

		private BookingCategory(String code, String name) {
			this.code = code;
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	/**
	 * As per IATA Passenger Conference manual RESOLUTION 722g ELECTRONIC TICKET <br />
	 * section 5.4 COUPON STATUS INDICATORS
	 */
	public enum EticketStatus {
		CHECKEDIN("C"), EXCHANGED("E"), FLOWN("F"), BOARDED("L"), OPEN("O"), PRINTED("P"), REFUNDED("R"), SUSPENDED("S"), VOID(
				"V"), PRINT_EXCHANGED("X"), CLOSED("Z"),AIRPORT_CONTROL("A"), FIM("G"), IRREGULAR_OPERATION("I"), NOTIFICATION("N"), PAPER_TICKET("T"),UNAVAILABLE("U"), REFUND_TFC_ONLY("Y");

		private String code;

		private EticketStatus(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum FinalEticketStatus {
		EXCHANGED("E"), FLOWN("F"), PRINTED("P"), REFUNDED("R"), VOID("V"), PRINT_EXCHANGED("X"), CLOSED("Z");

		private String code;

		private FinalEticketStatus(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum PaxStatus {

		GO_SHOW("G", "GOSHO", "Go Show Passenger"), NO_SHOW("N", "NOSHO", "No Show Passenger"), NO_REC("R", "NOREC",
				"NOREC Passenger"), OFF_LD("O", "OFFLD", "Off Load Passenger"), IN_VOL("I", "INVOL", "INVOL Passenger"), CF_MWL(
				"W", "CFMWL", "CFMWL Passenger"), GO_SHN("S", "GOSHN", "GOSHN Passenger"), CH_GSG("D", "CHGSG", "CHGSG Passenger"), CH_GFL(
				"F", "CHGFL", "CHGFL Passenger"), ID_PAD("P", "IDPAD", "IDPAD Show Passenger");

		private String code;
		private String name;
		private String description;

		private PaxStatus(String code, String name, String description) {
			this.code = code;
			this.name = name;
			this.description = name;
		}

		public String code() {
			return this.code;
		}

		public String getName() {
			return name;
		}

		public String getDescription() {
			return description;
		}

	}

	public enum SSRCategoryCode {
		STANDARD(1), HALA(2);

		private Integer code;

		private SSRCategoryCode(int code) {
			this.code = new Integer(code);
		}

		public Integer getCode() {
			return this.code;
		}

	}

	public static interface PNLticketType {
		public static String TKNA = "TKNA";
		public static String TKNE = "TKNE";
	}

	public enum SSRCode {
		PSPT, TKNA, TKNE, DOCS, FQTV, ID, DOCO
	}
	
	public static interface TempPaymentConstants {
		public static String PAYMENT_SUCCESS = "PS";
		public static String RESERVATION_SUCCESS = "RS";
		public static String TOTAL_AMOUNT = "TOTAL_AMOUNT";
		public static char PRODUCT_TYPE_GOQUO = 'G';
		public static String EXTERNAL_REFERENCE = "EXTERNAL_REFERENCE";
	}

	public enum FlightType {
		DOMESTIC("DOM"), INTERNATIONL("INT");
		String type;

		private FlightType(String type) {
			this.type = type;
		}

		public String getType() {
			return this.type;
		}
	}

	// CCC- Cabinet Chaubet Courtage
	public enum INSURANCEPROVIDER {
		AIG, RAK, CCC, ACE, TUNE, NONE
	}

	public static interface AmadeusConstants {
		public static final String DEFAULT_BOOKING_CLASS_ENABLED = "defaultBCEnabled";
		public static final String DEFAULT_BOOKING_CLASS = "defaultBC";
		public static final String DEFAULT_FARE_RULE = "defaultFareRule";
		public static final String DSU_FILE_NAME = "dsuFileName";
	}

	public static interface ChargeRateOperationType {

		public static final int ANY = 0; // Applicable both make and modify
		public static final int MAKE_ONLY = 1; // Applicable for make only
		public static final int MODIFY_ONLY = 2; // Applicable for modify only

	}

	public static interface ChargeRateJourneyType {

		public static final int ANY = 0; // Applicable both make and modify

		public static final int ONEWAY = 1; // Applicable for anyway journey only

		public static final int RETURN = 2; // Applicable for return journey only

	}

	public static interface WSConstants {
		public static final int ALLOW_MAKE_RESERVATION = 0;
		public static final int ALLOW_FIND_RESERVATION = 1;
		public static final int ALLOW_MODIFY_RESERVATION = 2;
	}

	public static interface CarrierServiceType {

		public static final String BUS_SERVICE = "BUS";
		public static final String AIRCRAFT_SERVICE = "AIRCRAFT";

	}

	public enum BaggageCriterion {
		BOOKING_CLASS, SALES_CHANNEL, AGENT, FLIGHT_NUMBER, OND
	}

	// Default value is 3
	public enum SeatFactorConditionApplyFor {
		APPLY_FOR_FIRST_SEGMENT(1, "Apply For First Segment"), APPLY_FOR_LAST_SEGMENT(2, "Apply For Last Segment"), APPLY_FOR_MOTST_RESTRICTIVE(
				3, "Apply For Most Restrictive Segment");

		private int condition;
		private String description;

		SeatFactorConditionApplyFor(int condition, String description) {
			this.condition = condition;
			this.description = description;
		}

		public int getCondition() {
			return condition;
		}

		public String getDescription() {
			return description;
		}
	}

	String ALL_DESIGNATOR_STRING = "ALL";
	int ALL_DESIGNATOR_INT = -1;

	public static interface HumanVerificationConstants {
		public static final String ENABLED = "ENABLED";
		public static final String HEADER_NAME = "HEADER_NAME";
		public static final String MAX_IP_HITS = "MAX_IP_HITS";
		public static final String MAX_SESSION_HITS = "MAX_SESSION_HITS";
		public static final String VALIDATE_CAPTCHA = "validateCaptcha";
		public static final String CAPTCHA_VALIDATED = "captchValidated";
	}

	public static interface LoyaltyPointsConversionParams {
		public static final String CURRENCY = "CURRENCY_CODE";
		public static final String POINTS_TO_CURRENCY = "POINTS_TO_CURRENCY";
		public static final String CURRENCY_TO_POINTS = "CURRENCY_TO_POINTS";
	}
	
	public enum InsuranceExternalContenKey {
		   PLAN_COVERED_INFOMATION_DESCRIPTION, 
		   PUSH_PLAN_SELECT_YES_DESCRIPTION,
		   PLAN_TITEL,
		   PLAN_DESC,
		   POPUP_YES_BUTTON_TEXT,
		   POPUP_NO_BUTTON_TEXT,
		   PLAN_TERMS_AND_CONDITIONS
	}
	
	public enum ErrorType {
		PAYMENT_ERROR, OTHER_ERROR, TRANSACTION_ID_ERROR, PAYMENT_ERROR_ONHOLD, VOUCHER_CREATION_FAILED
	}

	public enum SearchType {
		OND_CODE, AGENT, USER
	}

	public static interface TimeMode {
		public static String LOCAL_TIME_MODE = "LT";
		public static String ZULU_TIME_MODE = "UTC";
	}

	public static interface GoogleRecaptcha {
		public static final String ENABLED = "ENABLED";
		public static final String SITE_KEY = "SITE-KEY";
		public static final String SECRET_KEY = "SECRET-KEY";
		public static final String URL = "URL";
	}
	
	public enum ModifyOperation {

		CREATE_RES(1), MODIFY_SEGMENT(2), CANCEL_SEGMENT(3), ADD_SEGMENT(4), NAME_CHANGE(5), MODIFY_ANCI(6), REMOVE_PAX(
				7), ADD_INFANT(8), CANCEL_PNR(9), CHARGE_ADJ(10);

		private final Integer operation;

		ModifyOperation(Integer operation) {
			this.operation = operation;
		}

		public Integer getOperation() {
			return operation;
		}

		public static List<Integer> getReservationOperations() {

			List<Integer> modifyOps = new ArrayList<Integer>();

			for (ModifyOperation operation : ModifyOperation.values()) {
				modifyOps.add(operation.getOperation());
			}

			return modifyOps;
		}
		
		public static List<String> getReservationModifyingOperations() {

			List<String> modifyResOps = new ArrayList<String>();

			for (ModifyOperation operation : ModifyOperation.values()) {
				if(!operation.getOperation().equals(CREATE_RES.getOperation())){
					modifyResOps.add(new Integer(operation.getOperation()).toString());
				}
			}

			return modifyResOps;
		}

	}
	
	public static interface ChargeRateReservationFlows{

		public static final int ANY = 0;

		public static final int CREATE = 1;

		public static final int MODIFY = 2;
	}

	public enum ModuleErrorType {
		PAYMENT_ERROR, OTHER_ERROR
	}

	/**
	 * Holds The Message Identifiers
	 */
	public static interface MessageIdentifiers {

		public static final String PRL = "PRL";
		public static final String ETL = "ETL";

	}

	
	public static interface BatchStatus{

		public static final String DONE = "DONE";

		public static final String WAITING = "WAITING";

		public static final String IN_PROGRESS = "IN_PROGRESS";
		
		public static final String ERROR = "ERROR";
		
		public static final String TERMINATED = "TERMINATED";
		

	}
}
