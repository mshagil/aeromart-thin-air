/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * Holds the data transfer information for the Interlined Airlines
 * 
 * @author Noshani
 * @since 2.0
 */
public class InterlinedAirLineTO implements Serializable {

	private static final long serialVersionUID = 7499656035447977495L;

	/** Holds the carrier code. */
	private String carrierCode;

	/** Holds the status. */
	private String status;

	/** Holds the operating agent's code */
	private String agentCode;

	/** Holds the operating user id */
	private String userId;

	/** Holds the operating user password */
	private String password;

	/** Holds the interlined cut over in minutes */
	private int interlineCutOverInMinutes;

	/** Holds the transfer gap in minutes */
	private int transferGapInMinutes;

	/** Holds the transfer cut over in minutes */
	private int transferCutOverInMinutes;

	/** Holds the airline description */
	private String airlineDesc;

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the carrierCode.
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return Returns the interlineCutOverInMinutes.
	 */
	public int getInterlineCutOverInMinutes() {
		return interlineCutOverInMinutes;
	}

	/**
	 * @param interlineCutOverInMinutes
	 *            The interlineCutOverInMinutes to set.
	 */
	public void setInterlineCutOverInMinutes(int interlineCutOverInMinutes) {
		this.interlineCutOverInMinutes = interlineCutOverInMinutes;
	}

	/**
	 * @return Returns the password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the transferCutOverInMinutes.
	 */
	public int getTransferCutOverInMinutes() {
		return transferCutOverInMinutes;
	}

	/**
	 * @param transferCutOverInMinutes
	 *            The transferCutOverInMinutes to set.
	 */
	public void setTransferCutOverInMinutes(int transferCutOverInMinutes) {
		this.transferCutOverInMinutes = transferCutOverInMinutes;
	}

	/**
	 * @return Returns the transferGapInMinutes.
	 */
	public int getTransferGapInMinutes() {
		return transferGapInMinutes;
	}

	/**
	 * @param transferGapInMinutes
	 *            The transferGapInMinutes to set.
	 */
	public void setTransferGapInMinutes(int transferGapInMinutes) {
		this.transferGapInMinutes = transferGapInMinutes;
	}

	/**
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the airlineDesc.
	 */
	public String getAirlineDesc() {
		return airlineDesc;
	}

	/**
	 * @param airlineDesc
	 *            The airlineDesc to set.
	 */
	public void setAirlineDesc(String airlineDesc) {
		this.airlineDesc = airlineDesc;
	}
}
