package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class AppParameterDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String paramKey;
	private String paramValue;
	private String carrierCode;
	
	public String getParamKey() {
		return paramKey;
	}
	public String getParamValue() {
		return paramValue;
	}
	public String getCarrierCode() {
		return carrierCode;
	}
	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
}
