package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class AeroMartUpdateCouponStatusRq implements Serializable {

	private List<TicketNumberDetails> tickets;

	public List<TicketNumberDetails> getTickets() {
		return tickets;
	}

	public void setTickets(List<TicketNumberDetails> tickets) {
		this.tickets = tickets;
	}
}
