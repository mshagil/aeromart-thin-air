package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class OfficerMobNumsConfigsSearchDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String officerName;
	private String mobNumber;
	
	
	
	
	public String getMobNumber() {
		return mobNumber;
	}
	public void setMobNumber(String mobNumber) {
		this.mobNumber = mobNumber;
	}
	public String getOfficerName() {
		return officerName;
	}
	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}
	
	

}
