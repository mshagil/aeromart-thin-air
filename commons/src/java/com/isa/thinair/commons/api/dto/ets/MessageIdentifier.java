package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class MessageIdentifier implements Serializable{
	
    private String messageType;

    private String controllingAgency;

    private String version;

    private String releaseNumber;

	public String getMessageType() {
		return messageType;
	}

	public String getControllingAgency() {
		return controllingAgency;
	}

	public String getVersion() {
		return version;
	}

	public String getReleaseNumber() {
		return releaseNumber;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public void setControllingAgency(String controllingAgency) {
		this.controllingAgency = controllingAgency;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setReleaseNumber(String releaseNumber) {
		this.releaseNumber = releaseNumber;
	}

}
