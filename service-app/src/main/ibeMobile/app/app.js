(function () {
    'use strict';

    angular.module('ibeMobileApp', [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ui.router',
            'ngSanitize',
            'ngTouch',
            'ngDialog',
            'LocalStorageModule',
            'angular-loading-bar'
        ])

        .run(['$rootScope', 'masterDataService', 'availabilityService', 'ngDialog',
            function ($rootScope, masterDataService, availabilityService, ngDialog) {
                var masterData = {};

                masterDataService.retrieveCountries().then(function (response) {
                    masterData.CountryList = response.data.countryList;
                    setMasterData(masterData);
                });
                masterDataService.retrieveExchangeRates().then(function (response) {
                    masterData.ExchangeRates = response.data.currencyExRates;
                    setMasterData(masterData);
                });
                masterDataService.retrieveApplicationParams().then(function (response) {
                    masterData.AppParams = response.data;
                    setMasterData(masterData);
                });
                masterDataService.retrieveCommonLists().then(function (response) {
                    masterData.dropDownLists = response.data;
                    setMasterData(masterData);
                });

                function setMasterData(masterData) {
                    if (masterData.CountryList !== undefined &&
                        masterData.ExchangeRates !== undefined &&
                        masterData.AppParams !== undefined &&
                        masterData.dropDownLists !== undefined) {

                        masterDataService.setMasterData(masterData);
                    }
                }

                $rootScope.$on('$stateChangeStart', function (event, to, toP, from) {
                    // Avoid navigation on other routes
                    if (from.name === 'confirm_onhold' || from.name === 'confirm') {
                        event.preventDefault();
                    }

                    if ((from.name === 'availability' || from.name === 'return') && !isValidForward(to, from)) {
                        event.preventDefault();
                        //TODO: Move template to file
                        ngDialog.open({
                            template: '<div id="myModal" class="reveal-modal" data-reveal ' +
                            'aria-labelledby="modalTitle" aria-hidden="true" role="dialog">' +
                            '<p>Please select a flight and press \'Proceed\'.</p>' +
                            '<a class="button" aria-label="Close" ng-click="closeThisDialog(0)">Close</a>' +
                            '</div>',
                            plain: true
                        });
                    }
                });

                function isValidForward(to, from) {
                    var valid = true;

                    if (from.name === 'availability') {
                        if (to.name === 'passenger' && !availabilityService.getAvailabilityOutputData()) {
                            valid = false;
                        }
                        if (to.name === 'return' && !availabilityService.getDepartureFlightData()) {
                            valid = false;
                        }
                    }

                    if (from.name === 'return') {
                        if (to.name === 'passenger' && !availabilityService.getAvailabilityOutputData()) {
                            valid = false;
                        }
                    }

                    return valid;
                }
            }]);
})();
