(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('ReturnCtrl', [
            '$state',
            '$timeout',
            '$stateParams',
            '$anchorScroll',
            'availabilityService',
            ReturnController
        ]);

    function ReturnController($state, $timeout, $stateParams, $anchorScroll, availabilityService) {

        // Global vars.
        var self = this;
        var searchParams;

        // Properties.
        self.selectedFlightIndex = 0;
        self.isReturn = $stateParams.retDate !== 'N';
        self.currency = $stateParams.currency;

        self.isOneWayFlightInfoAvailable = false;
        self.totalPrice = 0;

        self.timelineData = [];
        self.fareClasses = [];

        self.fromAirport = '';
        self.toAirport = '';
        self.moment = moment; // get moment.js reference.

        self.enableProceed = false;

        (function init() {
            $anchorScroll();
            searchParams = availabilityService.getAvailabilityParams();

            self.fromAirport = searchParams.to;
            self.toAirport = searchParams.from;
            self.currency = searchParams.currency;

            // Get departure flight details.
            var departureFlightDetails = availabilityService.getDepartureFlightData(),
                departureFlight = departureFlightDetails.flight;
            //departureFlightFare = departureFlightDetails.fare;

            var params = {
                'journeyInfo': [{
                    origin: departureFlight.originCode,
                    destination: departureFlight.destinationCode,
                    departureDateTime: getFormatedDate(departureFlight.departureDate, departureFlight.departureTime),
                    arrivalDateTime: getFormatedDate(departureFlight.arrivalDate, departureFlight.arrivalTime),
                    departureVariance: 3,
                    specificFlights: availabilityService.getSpecificFlights(departureFlight)
                },
                    {
                        origin: departureFlight.destinationCode,
                        destination: departureFlight.originCode,
                        departureDateTime: getFormatedDate(searchParams.retDate),
                        departureVariance: 3
                    }],
                'travellerQuantity': {
                    adultCount: searchParams.adult,
                    childCount: searchParams.child,
                    infantCount: searchParams.infant
                },
                'preferences': {
                    cabinClass: 'Y',
                    logicalCabinClass: 'Y',
                    currency: searchParams.currency
                }
            };

            setPromoDetailsToParams(params);

            availabilityService.getReturnFlightDetails(params, function (response) {
                self.timelineData = response.timelineInfo[1].results;
                self.fareClasses = response.fareClasses;

                var inbound = availabilityService.getInboundFlight();

                _.forEach(self.timelineData, function (flights) {
                    _.forEach(flights.flights, function (flight, flightIndex) {
                        if (_.isEqual(flight, inbound.flight)) {
                            _.forEach(flight.flightFaresAvailability, function (fare) {
                                if (_.isEqual(fare, inbound.fare)) {
                                    fare.selectedFare = true;
                                    self.selectedFlightIndex = flightIndex;
                                }
                            });
                        }
                    });
                });

            }, function (error) {
                console.log(error);
            });

            var availableFlights = availabilityService.getAvailabilityOutputData();

            if (!_.isEmpty(availableFlights)) {
                self.enableProceed = true;
                self.isReturnFlightInfoAvailable = true;
                self.totalPrice = availableFlights.totalPrice;
            }
        })();

        self.navigateToNext = function () {
            $state.go('passenger');
        };

        // Functions.
        self.changeSelectedFlight = function (index) {
            self.selectedFlightIndex = index;
        };

        // Timeline date changed event.
        self.fareDateSelected = function ($event, timeline) {

            angular.forEach(self.timelineData, function (value) {
                value.selected = false;
            });

            timeline.selected = true;
            self.flights = timeline.flights;
        };

        // Used to select timeline selected dates and load according fligts.
        self.setInitialSelection = function functionName($event, timeline) {
            if (timeline.selected) {
                self.flights = timeline.flights;
                uiCenterSelectedDate();
            }
        };

        self.setDepartureFlightDetails = function (returnFlight, fare) {
            // Get departure flight details.
            var departureFlight = availabilityService.getDepartureFlightData().flight;

            var arrivalFlight = {
                flight: returnFlight,
                fare: fare
            };

            // Set inbound flight details to service
            availabilityService.setInboundFlight(arrivalFlight);

            var params = {
                journeyInfo: [{
                    origin: departureFlight.originCode,
                    destination: departureFlight.destinationCode,
                    departureDateTime: getFormatedDate(departureFlight.departureDate, departureFlight.departureTime),
                    arrivalDateTime: getFormatedDate(departureFlight.arrivalDate, departureFlight.arrivalTime),
                    departureVariance: 0,
                    specificFlights: [{
                        filghtDesignator: departureFlight.flightNumber,
                        segmentCode: departureFlight.originCode + '/' + departureFlight.destinationCode,
                        departureDateTime:
                            getFormatedDate(departureFlight.departureDate, departureFlight.departureTime),
                        arrivalDateTime: getFormatedDate(departureFlight.arrivalDate, departureFlight.arrivalTime),
                        flightSegmentRPH: departureFlight.flightRPH
                    }]
                }, {
                    origin: returnFlight.originCode,
                    destination: returnFlight.destinationCode,
                    departureDateTime: getFormatedDate(returnFlight.departureDate, returnFlight.departureTime),
                    arrivalDateTime: getFormatedDate(returnFlight.arrivalDate, returnFlight.arrivalTime),
                    departureVariance: 0,
                    specificFlights: [{
                        filghtDesignator: returnFlight.flightNumber,
                        segmentCode: returnFlight.originCode + '/' + returnFlight.destinationCode,
                        departureDateTime: getFormatedDate(returnFlight.departureDate, returnFlight.departureTime),
                        arrivalDateTime: getFormatedDate(returnFlight.arrivalDate, returnFlight.arrivalTime),
                        flightSegmentRPH: returnFlight.flightRPH
                    }]
                }],
                travellerQuantity: {
                    adultCount: searchParams.adult,
                    childCount: searchParams.child,
                    infantCount: searchParams.infant
                },
                preferences: {
                    cabinClass: 'Y',
                    logicalCabinClass: 'Y',
                    currency: searchParams.currency
                }
            };

            setPromoDetailsToParams(params);

            self.enableProceed = true;

            availabilityService.getFareQuote(params, function (response) {
                self.enableProceed = true;
                self.isReturnFlightInfoAvailable = true;
                if(response.selectedFlightPricing != undefined &&  response.selectedFlightPricing !== null){
                	self.totalPrice = response.selectedFlightPricing.total.price;
                }
                availabilityService.setAvailabilityOutputData({totalPrice: self.totalPrice});

            }, function (error) {
                console.log(error);
            });
        };

        function getFormatedDate(date, time) {
            return moment(date + ' ' + (time ? time : '00:00'), 'DD/MM/YYYY hh:mm').format().split('+')[0];
        }

        function setPromoDetailsToParams(params) {
            if (!_.isEmpty(searchParams.promoCode)) {
                params.preferences.promotion = {
                    code : searchParams.promoCode,
                    type : 'PROMO_CODE'
                };
            }
        }

        // CEnter selected date on init.
        function uiCenterSelectedDate() {
            $timeout(function () {

                var elementWidth = 164, //$('.flight-look .is-active').width(),
                    screenWidth = window.screen.width,
                    leftOffset = (screenWidth - elementWidth) / 2,
                    aa = $('.flight-lookup .is-active').offset().left;

                $('.flight-look')
                    .scrollLeft(aa)
                    .scrollLeft(leftOffset + 20);

            }, 50);
        }

    } // End of AvailabilityController.

})();
