(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('availabilityService', [
            'remoteService',
            'availabilityAdaptor',
            'languageService',
            'commonLocalStorageService',
            availabilityService
        ]);

    function availabilityService(remoteService, availabilityAdaptor, languageService, commonLocalStorageService) {
        var self = this;
        var JSON_DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss';
        var flightData = {}, returnData = {};
        var outboundFlightData = {}, inboundFlightData = {};

        self.setAvailabilityParams = function (params) {
            // Clear localStorage if availability request is different from before
            if (!_.isEqual(self.getAvailabilityParams(), params)) {
                commonLocalStorageService.clearAll();
                self.removeFlightData();

                commonLocalStorageService.setAvailabilityParams(params);
                languageService.setSelectedLanguage(params.lang);
                //TODO: Should clear passenger details if count is different when modify search is available
            }
        };
        self.getAvailabilityParams = function () {
            return commonLocalStorageService.getAvailabilityParams();
        };

        self.setDepartureFlightData = function (flight) {
            commonLocalStorageService.setDepartureFlightData(flight);
        };
        self.getDepartureFlightData = function () {
            return commonLocalStorageService.getDepartureFlightData();
        };

        self.setAvailabilityOutputData = function (outputData) {
            commonLocalStorageService.setAvailabilityOutputData(outputData);
        };
        self.getAvailabilityOutputData = function () {
            return commonLocalStorageService.getAvailabilityOutputData();
        };

        self.getReturnFlightDetails = function (params, successFn, errorFn) {

            var path = '/availability/search/fare/flight/calendar/return';

            self.departureDate = params.journeyInfo[1].departureDateTime;

            var diff = 0;
            var isAllOnd = false;
            //var outBoundDate = moment(params.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT);
            var inboundDate = moment(params.journeyInfo[1].departureDateTime, JSON_DATE_FORMAT);
            var inboundStartDate = moment(inboundDate, JSON_DATE_FORMAT).subtract(3, 'days');

            if (params.journeyInfo[0].departureVariance == 3) {
                isAllOnd = true;
            }

            if (moment(inboundStartDate, JSON_DATE_FORMAT).isBefore(moment())) {
                diff = moment().diff(moment(inboundStartDate, JSON_DATE_FORMAT), 'days');
            }

            if (moment(inboundDate, JSON_DATE_FORMAT)
                    .isBefore(moment(params.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT))) {
                params.journeyInfo[1].departureDateTime =
                    moment(params.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT)
                        .add(120, 'seconds').format(JSON_DATE_FORMAT);
            }

            var modifiedVarience = params.journeyInfo[1].departureVariance + diff;
            params.journeyInfo[1].departureVariance = modifiedVarience;
            params.journeyInfo[0].departureVariance = modifiedVarience;
            self.departureVariance = modifiedVarience;

            if (_.isEmpty(returnData)) {
                remoteService
                    .post(path, params)
                    .success(function (response) {

                        var timeLineInfo,
                            fareClasses;

                        if (isAllOnd) {
                            timeLineInfo =
                                availabilityAdaptor.adaptTimelineForAllOnd(response, params, self.departureVariance);
                            fareClasses = availabilityAdaptor.adaptFareClasses(response);
                            returnData = {
                                'timelineInfo': timeLineInfo.data,
                                'fareClasses': fareClasses
                            };
                        }
                        else {
                            response.originDestinationResponse.shift(); // to remove outbound flights
                            timeLineInfo =
                                availabilityAdaptor.adaptTimeline(response, self.departureDate, self.departureVariance);
                            fareClasses = availabilityAdaptor.adaptFareClasses(response);

                            returnData = {
                                'timelineInfo': timeLineInfo.data.results,
                                'fareClasses': fareClasses
                            };
                        }

                        successFn(returnData);
                    })
                    .error(function (response) {
                        errorFn(response);
                    });
            } else {
                successFn(returnData);
            }
        };

        self.getFlightDetails = function (params, successFn) {
            var path = '/availability/search/fare/flight/calendar/minimum';

            if (_.isEmpty(flightData)) {
                remoteService
                    .post(path, params)
                    .success(function (response) {

                        var timeLineInfo = availabilityAdaptor
                            .adaptTimeline(response,
                                params.journeyInfo[0].departureDateTime, params.journeyInfo[0].departureVariance);
                        var fareClasses = availabilityAdaptor.adaptFareClasses(response);
                        var promoInfo =
                            response.selectedFlightPricing !== null ? response.selectedFlightPricing.promoInfo : [];

                        flightData = {
                            'timelineInfo': timeLineInfo.data.results,
                            'fareClasses': fareClasses,
                            'promoInfo': promoInfo
                        };

                        successFn(flightData);
                    });
            } else {
                successFn(flightData);
            }
        };

        self.getFareQuote = function (params, successFn) {

            var path = '/availability/selected/pricing/minimum';

            remoteService
                .post(path, params)
                .success(function (response) {

                    successFn(response);
                });
        };

        self.removeReturnData = function () {
            returnData = {};
        };

        self.removeFlightData = function () {
            flightData = {};
        };

        self.setOutboundFlight = function (outbound) {
            outboundFlightData = outbound;
        };

        self.getOutboundFlight = function () {
            return outboundFlightData;
        };

        self.setInboundFlight = function (inbound) {
            inboundFlightData = inbound;
        };

        self.getInboundFlight = function () {
            return inboundFlightData;
        };

        self.getSpecificFlights = function (flight) {
            var departureDateTime =
                moment(flight.departureDate + ' ' + flight.departureTime, 'DD/MM/YYYY hh:mm').format().split('+')[0];
            var arrivalDateTime =
                moment(flight.arrivalDate + ' ' + flight.arrivalTime, 'DD/MM/YYYY hh:mm').format().split('+')[0];

            var tempSpecific = [{
                filghtDesignator: flight.flightNumber,
                segmentCode: flight.originCode + '/' + flight.destinationCode,
                departureDateTime: departureDateTime,
                arrivalDateTime: arrivalDateTime,
                flightSegmentRPH: flight.flightRPH
            }];

            if(flight.stops.length){
                for(var j=0;j<flight.stops.length;j++){
                    var stopData= flight.stops[j];
                    var fTemp={};
                    fTemp.filghtDesignator=stopData.destinationCode;
                    fTemp.segmentCode=stopData.originCode+"/"+stopData.destinationCode;
                    var dep_time = stopData.departureDate+" "+stopData.departureTime;
                    fTemp.departureDateTime=moment(dep_time, "DD/MM/YYYY HH:mm").format("YYYY-MM-DDTHH:mm:ss");
                    fTemp.flightSegmentRPH=stopData.flightRPH;
                    fTemp.routeRefNumber = stopData.flightRPH;
                    tempSpecific.push(fTemp);
                }
            }

            return tempSpecific;
        }

    } // End of availabilityService.

})();
