(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('AvailabilityCtrl', [
            '$state',
            '$timeout',
            '$stateParams',
            'availabilityService',
            'applicationService',
            AvailabilityController
        ]);

    function AvailabilityController($state, $timeout, $stateParams, availabilityService, applicationService) {

        // Global vars.
        var self = this;

        // Properties.
        self.selectedFlightIndex = 0;
        self.isReturn = $stateParams.retDate !== 'N';
        self.currency = $stateParams.currency;

        self.isOneWayFlightInfoAvailable = false;
        self.totalPrice = 0;

        self.timelineData = [];
        self.fareClasses = [];
        self.promoInfo = [];
        self.flightFare= 0;
        self.paxPricerDetails={}

        self.fromAirport = '';
        self.toAirport = '';
        self.moment = moment; // get moment.js reference.

        self.setPromoCodeToParams = function (params) {
            if (!_.isEmpty($stateParams.promoCode)) {
                params.preferences.promotion = {};
                params.preferences.promotion.type = 'PROMO_CODE';
                params.preferences.promotion.code = $stateParams.promoCode;
            }
        };

        (function init() {
            //When main page is set all the localStorage data is removed

            self.enableProceed = false;
            //main.appStatus = true;

            self.fromAirport = $stateParams.from; //commonLocalStorageService.getAirportName($stateParams.from);
            self.toAirport = $stateParams.to; // commonLocalStorageService.getAirportName($stateParams.to);

            availabilityService.setAvailabilityParams($stateParams);

            // Flight type is set to adjust the progress bar
            if (self.isReturn) {
                applicationService.setFlightType(applicationService.FLIGHT_TYPES.RETURN);
            } else {
                applicationService.setFlightType(applicationService.FLIGHT_TYPES.ONE_WAY);
            }

            var params = {
                'travellerQuantity': {
                    'adultCount': $stateParams.adult,
                    'childCount': $stateParams.child,
                    'infantCount': $stateParams.infant
                },
                'preferences': {
                    'cabinClass': 'Y',
                    'logicalCabinClass': 'Y',
                    'currency': $stateParams.currency
                },
                'journeyInfo': [{
                    'origin': $stateParams.from,
                    'destination': $stateParams.to,
                    'departureDateTime': moment($stateParams.depDate, 'DD-MM-YYYY').format().split('+')[0],
                    'departureVariance': 3
                }]
            };

            self.setPromoCodeToParams(params);

            availabilityService.getFlightDetails(params, function (response) {
            
                self.timelineData = response.timelineInfo;
                self.fareClasses = response.fareClasses;
                self.promoInfo = response.promoInfo || [];
         
                var outbound = availabilityService.getOutboundFlight();
                
                _.forEach(self.timelineData, function (flights) {
                    _.forEach(flights.flights, function (flight, flightIndex) {
                        if (_.isEqual(flight, outbound.flight)) {
                            _.forEach(flight.flightFaresAvailability, function (fare) {
                                if (_.isEqual(fare, outbound.fare)) {
                                    fare.selectedFare = true;
                                    self.selectedFlightIndex = flightIndex;
                                }
                            });
                        }
                    });
                });

            }, function (error) {
                console.log(error);
            });

            var oneWayAvailable = availabilityService.getAvailabilityOutputData();
            var returnAvailable = availabilityService.getDepartureFlightData();
       
            if (!_.isEmpty(oneWayAvailable)) {
                self.totalPrice = oneWayAvailable.totalPrice;
                self.tax= oneWayAvailable.tax;
                self.flightFare = oneWayAvailable.fare;
                self.paxPricerDetails = oneWayAvailable.paxPricerDetails;
                self.enableProceed = true;
                self.isOneWayFlightInfoAvailable = true;
            }
            if (!_.isEmpty(returnAvailable)) {
                self.totalPrice = returnAvailable.totalPrice;
                self.enableProceed = true;
            }

        })();

        // Functions.
        self.changeSelectedFlight = function (index) {
            self.selectedFlightIndex = index;
        };

        self.navigateToNext = function () {
            if (self.isReturn) {
                $state.go('return');
            } else {
                $state.go('passenger');
            }
        };

        // Timeline date changed event.
        self.fareDateSelected = function ($event, timeline) {

            angular.forEach(self.timelineData, function (value) {
                value.selected = false;
            });

            timeline.selected = true;

            self.flights = timeline.flights;
        };

        // Used to select timeline selected dates and load according fligts.
        self.setInitialSelection = function functionName($event, timeline) {
            if (timeline.selected) {
                self.flights = timeline.flights;
                uiCenterSelectedDate();
            }
        };

        self.setDepartureFlightDetails = function (flight, fare) {
            // self.flightFare= fare.fareValue;

            var departureDateTime =
                moment(flight.departureDate + ' ' + flight.departureTime, 'DD/MM/YYYY hh:mm').format().split('+')[0];
            var arrivalDateTime =
                moment(flight.arrivalDate + ' ' + flight.arrivalTime, 'DD/MM/YYYY hh:mm').format().split('+')[0];
            //var returnDepartureDateTime =
            //    moment($stateParams.retDate + ' ' + '00:00', 'DD/MM/YYYY hh:mm').format().split('+')[0];
            // console.log(fare.fareValue)

            var departureFlight = {
                flight: flight,
                fare: fare
            };

            // Set outbound flight details to service
            availabilityService.setOutboundFlight(departureFlight);
            
            if (!self.isReturn) {

                var params = {
                    journeyInfo: [{
                        origin: $stateParams.from,
                        destination: $stateParams.to,
                        departureDateTime: departureDateTime,
                        arrivalDateTime: arrivalDateTime,
                        departureVariance: 0,
                        specificFlights: availabilityService.getSpecificFlights(departureFlight.flight)
                    }],
                    travellerQuantity: {
                        adultCount: $stateParams.adult,
                        childCount: $stateParams.child,
                        infantCount: $stateParams.infant
                    },
                    preferences: {
                        cabinClass: 'Y',
                        logicalCabinClass: 'Y',
                        currency: $stateParams.currency
                    }
                };
            
                self.setPromoCodeToParams(params);
                availabilityService.getFareQuote(params, function (response) {
                    self.enableProceed = true;
                    self.isOneWayFlightInfoAvailable = true;
                    if(response.selectedFlightPricing != undefined &&  response.selectedFlightPricing !== null){
                        self.totalPrice = response.selectedFlightPricing.total.price;
                        self.tax = response.selectedFlightPricing.total.tax;
                        self.flightFare = response.selectedFlightPricing.total.fare;
                        self.paxPricerDetails = response.selectedFlightPricing.total.paxWise;
                    }
                    availabilityService.setAvailabilityOutputData({totalPrice: self.totalPrice, tax:self.tax, fare:self.flightFare, paxPricerDetails: self.paxPricerDetails});

                }, function (error) {
                    console.log(error);
                });
            }
            else {
                // Set departure flight details.
                availabilityService.setDepartureFlightData(departureFlight);

                // Remove return data forcing a refresh
                availabilityService.removeReturnData();
                self.enableProceed = true;

            }

        };

        self.validFlightsAvailable = function (flights) {
            var faresAvailable = false;
            _.forEach(flights, function (flight) {
                if (flight.fareValue !== 'null') {
                    faresAvailable = true;
                }
            });

            return faresAvailable;
        };

        // Center selected date on init.
        function uiCenterSelectedDate() {
            $timeout(function () {

                //elementWidth = 164, //$('.flight-look .is-active').width(),
                //screenWidth = window.screen.width,
                //leftOffset = (screenWidth - elementWidth) / 2,

                var offset = $('.flight-lookup .is-active').offset().left;

                $('.flight-look').scrollLeft(offset);

            }, 50);
        }

    } // End of AvailabilityController.

})();
