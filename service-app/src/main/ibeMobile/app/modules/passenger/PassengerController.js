(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('PassengerCtrl', [
            '$state',
            'passengerService',
            'masterDataService',
            'availabilityService',
            '$anchorScroll',
            PassengerController
        ]);

    function PassengerController($state, passengerService, masterDataService, availabilityService, $anchorScroll) {
        // Global vars.
        var self = this;
        var categoryCodes = {
            'Adult': 'AD',
            'Child': 'CH',
            'Infant': 'IN'
        };

        // Properties.
        self.passengers = passengerService.getPassengers();
        self.outputData = {};
        self.selectedPax = 0;
        self.selectedContactPax = 0;
        self.countryList = masterDataService.masterData.CountryList;
        self.titleList = masterDataService.masterData.dropDownLists.paxTypeWiseTitles;

        init();
        function init() {
            $anchorScroll();
            // Set total price.
            self.outputData = availabilityService.getAvailabilityOutputData();
        }

        // Setting the display name for the passenger detail accordion
        self.getDisplayName = function (index) {
            var currentPassenger = self.passengers[index];
            if (currentPassenger.firstName || currentPassenger.lastName) {
                return (currentPassenger.title || '') +
                    ' ' + (currentPassenger.firstName || '') +
                    ' ' + (currentPassenger.lastName || '');
            } else {
                return currentPassenger.category + ' ' + currentPassenger.paxSequence;
            }
        };

        self.getPaxTitles = function (category) {
            return self.titleList[categoryCodes[category]];
        };

        self.getAdults = function () {
            return _.filter(self.passengers, function (pax) {
                return pax.category == 'Adult';
            });
        };

        self.navigateToContact = function () {

            if (!_.isNull(self.selectedContactPax)) {
                passengerService.setPassengers(self.passengers, self.passengers[self.selectedContactPax]);
            } else {
                passengerService.setPassengers(self.passengers);
            }

            $state.go('contactPassenger');
        };

        self.getAgeRules = function (passengerCategory) {
            return passengerService.getAgeRules(passengerCategory);
        };

        self.getToday = function () {
            return moment().format('YYYY-MM-DD');
        };

        self.setSelectedContactPax = function (index) {
            if (index === self.selectedContactPax) {
                self.selectedContactPax = null;
            } else {
                self.selectedContactPax = index;
            }
        };

        self.setCurrentPax = function(index) {
            if(self.selectedPax != index) {
                self.selectedPax = index;
            } else {
                self.selectedPax = -1;
            }
        };

    }

})();
