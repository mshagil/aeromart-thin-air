(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .controller('PostPaymentCtrl',
            ['paymentService',
                '$location',
                'applicationService',
                'commonLocalStorageService',
                'constantFactory',
                '$state',
                '$sce',
                '$window',
                'languageService', PostPaymentCtrl]);

    function PostPaymentCtrl(paymentService,
                             $location,
                             applicationService,
                             commonLocalStorageService,
                             constantFactory,
                             $state,
                             $sce,
                             $window,
                             languageService) {
        var self = this;

        var urlParams = $location.search();

        (function init() {
            callToConfirmPayment();
        })();

        this.paymentRetry = false;
        this.paymentRetryMsg = '';

        function callToConfirmPayment() {
            //var urlParams = commonLocalStorageService.getData('postMap');
            //applicationService.setApplicationMode(commonLocalStorageService.getData('APP_MODE'));

            var params = {
                'paymentReceiptMap': urlParams
            };
            params.paymode = applicationService.getApplicationMode();

            //TODO: Implement services
            //languageService.setSelectedLanguage(commonLocalStorageService.getData('langTOStore'));
            //currencyService.setSelectedCurrency(commonLocalStorageService.getData('CurrencyToStore'));

            paymentService.paymentReceipt(params, function (response) {
                    commonLocalStorageService.remove('postMap');
                    commonLocalStorageService.remove('langTOStore');
                    commonLocalStorageService.remove('CurrencyToStore');

                    commonLocalStorageService.setData('PNR', response.pnr);
                    commonLocalStorageService.setData('PNR_lastName', response.contactLastName);
                    commonLocalStorageService.setData('PNR_firstDepartureDate', response.firstDepartureDate);

                    // Set igpTransactionResults if provided
                    if (response.ipgTransactionResult) {
                        commonLocalStorageService.setData('ipgResults', response.ipgTransactionResult);
                    }

                    //TODO: Implement Currency service and saving currency
                    $state.go('confirm', {
                        lang: languageService.getSelectedLanguage(),
                        currency: 'USD',
                        mode: applicationService.getApplicationMode()
                    });
                },
                function (response) {
                    if (response.errorType === 'PAYMENT_ERROR') {
                        self.paymentRetry = true;
                        self.paymentRetryMsg = $sce.trustAsHtml(response.error);
                    } else {
                        alertService.setPageAlertMessage(response);
                    }
                });

        } // End of callToConfirmPayment

        this.startOver = function() {
            // Get the url to be redirected to
            $window.location.href = constantFactory.AppConfigStringValue("secureServiceAppURL");
        }
    }

})();

