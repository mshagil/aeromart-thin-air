(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('flightService', [
            flightService
        ]);

    function flightService() {
        var self = this;

        // Loop though the ond.js file and find create a map of destinations available for each given origins
        self.getToAirports = function () {
            var tempOndDataMap = [];

            for (var originIndex = 0; originIndex < origins.length; originIndex++) {
                var tempDestArray = [];

                for (var destIndex in origins[originIndex]) {

                    // check whether operating carrier exists
                    var curDest = origins[originIndex][destIndex];

                    if (curDest[0] != null) {
                        var currentSize = tempDestArray.length;
                        tempDestArray[currentSize] = [];
                        tempDestArray[currentSize] = airports[curDest[0]];
                    }
                }
                if (tempDestArray.length > 0) {
                    tempOndDataMap[airports[originIndex].code] = tempDestArray;
                }
            }
            return tempOndDataMap;
        };
    }
})();
