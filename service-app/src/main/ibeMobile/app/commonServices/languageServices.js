(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .service('languageService', ['localStorageService', languageService]);

    function languageService(localStorageService) {
        var self = this;
        var _lang = null;

        self.setSelectedLanguage = function (langCode) {
            localStorageService.set('APP_LANGUAGE', langCode);
            _lang = langCode;
        };

        self.getSelectedLanguage = function () {
            if (_lang === null) {
                _lang = localStorageService.get('APP_LANGUAGE');
            }
            return _lang;
        };
    }
})();
