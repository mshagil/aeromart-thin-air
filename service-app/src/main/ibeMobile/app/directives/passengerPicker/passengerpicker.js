(function () {
    'use strict';

    angular.module('ibeMobileApp')
        .directive('passengerPicker', function () {
            return {
                scope: {
                    isOpen: '=',
                    passengerDetails: '='
                },
                templateUrl: 'directives/passengerPicker/passengerPicker.html',
                restrict: 'E',
                link: function postLink(scope) {
                    scope.showError = false;
                    scope.errorMessage = '';
                },
                controller: ['$scope', '$timeout', function ($scope, $timeout) {
                    $scope.showDropDown = function () {
                        $scope.isOpen = true;
                    };

                    $scope.hideDropDown = function () {
                        $scope.showError = false;
                        $scope.isOpen = false;
                    };

                    $scope.addPassenger = function (key) {
                        var validityMessage = $scope.isValid($scope.passengerDetails[key].value + 1, key);
                        if (validityMessage.validity) {
                            $scope.passengerDetails[key].value++;
                            $scope.showError = false;
                        } else {
                            $scope.activateError(validityMessage.message);
                        }

                        $scope.paxDetailsChange();
                    };

                    $scope.removePassenger = function (key) {
                        var validityMessage = $scope.isValid($scope.passengerDetails[key].value - 1, key);
                        if (validityMessage.validity) {
                            $scope.passengerDetails[key].value--;
                            $scope.showError = false;
                        } else {
                            $scope.activateError(validityMessage.message);
                        }

                        $scope.paxDetailsChange();
                    };

                    $scope.activateError = function (message) {
                        $scope.showError = true;
                        $scope.errorMessage = message;

                        $timeout(function () {
                            $scope.showError = false;
                        }, 2500);
                    };

                    //These rules can be moved to controller where the directive is called if being generalized
                    $scope.isValid = function (newVal, type) {

                        var isValidMessage = {
                            validity: true,
                            message: ''
                        };

                        if (type === 'adult') {
                            //When removing adults, the number of infants should be reduced accordingly
                            if (newVal < $scope.passengerDetails.infant.value &&
                                newVal >= $scope.passengerDetails[type].min) {
                                $scope.passengerDetails.infant.value = newVal;
                            }

                            if (newVal + $scope.passengerDetails.child.value + 1 > 10) {
                                isValidMessage.validity = false;
                                isValidMessage.message = 'Adults and Children total should be 9 or less';
                                return isValidMessage;
                            }
                        }

                        if (type === 'child') {
                            if ($scope.passengerDetails.adult.value + newVal + 1 > 10) {
                                isValidMessage.validity = false;
                                isValidMessage.message = 'Adults and Children total should be 9 or less';
                                return isValidMessage;
                            }
                        }

                        if (type === 'infant' && newVal > $scope.passengerDetails.adult.value) {
                            isValidMessage.validity = false;
                            isValidMessage.message = 'Only one infant per adult is allowed';
                            return isValidMessage;
                        }

                        if (newVal <= $scope.passengerDetails[type].max &&
                            newVal >= $scope.passengerDetails[type].min) {
                            return isValidMessage;
                        } else {
                            isValidMessage.validity = false;
                            isValidMessage.message = 'The ' + type + ' count should be between ' +
                                $scope.passengerDetails[type].min +
                                ' and ' + $scope.passengerDetails[type].max;
                            return isValidMessage;
                        }
                    };

                    //Regenerate the value shown in the input
                    $scope.paxDetailsChange = function () {
                        var displayText = '';
                        angular.forEach($scope.passengerDetails, function (paxType) {
                            var label = '';
                            if (paxType.value === 1) {
                                label = paxType.labelSingle;
                            } else {
                                label = paxType.labelMulti;
                            }

                            displayText = displayText + paxType.value + ' ' + label + ' ';
                        });

                        $scope.displayText = displayText;
                    };
                    $scope.paxDetailsChange();

                    $scope.clickOutside = function () {
                        $scope.hideDropDown();
                    };

                    $scope.onItemClick = function ($event) {
                        $event.target.select();
                    };

                    $scope.passengerAmountChange = function (newValue, oldValue, key) {
                        newValue = Number(newValue);
                        oldValue = Number(oldValue);

                        if (_.isNaN(newValue)) {
                            $scope.activateError('Value must be set to a number');
                            $scope.passengerDetails[key].value = oldValue;
                            return;
                        }

                        var validityMessage = $scope.isValid(newValue, key);
                        if (validityMessage.validity) {
                            $scope.passengerDetails[key].value = newValue;
                            $scope.showError = false;
                        } else {
                            $scope.activateError(validityMessage.message);
                            $scope.passengerDetails[key].value = oldValue;
                            return;
                        }

                        $scope.paxDetailsChange();
                    };
                }]
            };
        });
})();
