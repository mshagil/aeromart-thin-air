var copy = require('copy');
var DEMO_CLIENT = 'demo';

var client = process.env.npm_package_config_client || DEMO_CLIENT;

console.log('Copying AeroMart base files');
copy('app/**/*.*', 'www', function (err, files) {
    console.log('Base file copy complete!');

    if (client != DEMO_CLIENT) {
        console.log('Copying Client files for ' + client);

        copy('clients/' + client + '/**/*.*', 'www', function (err, files) {
            console.log('Client files copied');
        });
    }

});
