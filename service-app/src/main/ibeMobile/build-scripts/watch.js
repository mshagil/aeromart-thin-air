var watch = require('watch');
var copy = require('copy');
var sass = require('node-sass');
var fs = require('fs');

var client = process.env.npm_package_config_client || DEMO_CLIENT;

var appPath = 'app';
var clientPath = 'clients/' + client;

watch.watchTree(appPath, function (f, curr, prev) {


    if (typeof f == "object" && prev === null && curr === null) {
        // Finished walking the tree
    } else if (curr.nlink === 0) {
        // f was removed
        //TODO: if f !exist in client remove it
    } else {
        // f was changed
        if (!isFileInClient(f)) {
            console.log('------ FILE CHANGED in /app - Updating... ------');
            updateFile(f, appPath);
        }
    }
});

watch.watchTree(clientPath, function (f, curr, prev) {
    console.log('------ FILE CHANGED in /'+ clientPath +' - Updating... ------');
    if (typeof f == "object" && prev === null && curr === null) {
        // Finished walking the tree
    } else if (curr.nlink === 0) {
        // f was removed
        //TODO: if f !exist in /app remove it
        //TODO: if f exist in /app, move app/../file to www/../file
    } else {
        updateFile(f, clientPath);
    }
});

function isFileInClient(file) {
    // Check for file in client folder
    try{
        var stats = fs.lstatSync(file.replace(appPath, clientPath));
        return true;
    }catch(ex){
        return false;
    }
}

function updateFile(file, path) {
    //Copy file to www
    var ext = file.substr(file.lastIndexOf('.') + 1);
    var copyToPath = file.replace(path, './www');
    copyToPath = copyToPath.substring(0, copyToPath.lastIndexOf("/") + 1);

    copy(file, copyToPath, {flatten: true}, function (err, files) {
        if (err) { console.log('error' + err); }

        if (ext === 'scss') {
            //Compile scss and move app.css
            console.log('scss changed: Compiling scss');

            sass.render({
                file: './www/assets/scss/app.scss',
                outFile: './www/assets/css/app.css',
                sourceMap: true
            }, function(error, result){
                fs.writeFile('./www/assets/css/app.css', result.css, function(err){
                    if(!err){
                        console.log('---------------- Update Complete ----------------');
                    }
                });
            })

        }else{
            console.log('---------------- Update Complete ----------------');
        }
    });

}