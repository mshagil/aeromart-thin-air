'use strict';
var templatecache = require('ng-templatecache');
var glob = require('glob');
var read = require('read-file');
var fs = require('fs');

var entries = [];

var options = {
    cwd: 'www/'
};

console.log('---Appending files: Starting---');

glob('modules/**/*.html', options, function (err, files) {
    for (var i = 0; i < files.length; i++) {
        var currentPath = 'www/' + files[i];
        var file = read.sync(currentPath, {encoding: 'utf8'});
        entries.push({content: file, path: files[i]});
    }
    //var cache = templatecache({entries: entries, module: 'ibeMobileApp'});
    //fs.appendFileSync('dist/scripts/scripts.js', cache);

    glob('directives/**/*.html', options, function (err, files) {
        for (var i = 0; i < files.length; i++) {
            var currentPath = 'www/' + files[i];
            var file = read.sync(currentPath, {encoding: 'utf8'});
            entries.push({content: file, path: files[i]});
        }
        console.log('---Appending files: Complete---');
        var cache = templatecache({entries: entries, module: 'ibeMobileApp'});
        fs.appendFileSync('dist/scripts/scripts.js', cache);
    });
});
