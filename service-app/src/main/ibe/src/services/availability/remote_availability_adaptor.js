(function(angular) {
  var app = angular.module("ibeAPIAdaptor");
  var JSON_DATE_FORMAT = "YYYY-MM-DDTHH:mm:ss";
  var DASH_SEPERATED_DATE = "DD/MM/YYYY";
  app
          .service(
                  'availabilityAdaptor',
                  [
                      '$http',
                      'commonLocalStorageService',
                      'constantFactory',
                      function($http, commonLocalStorageService, constantFactory) {

                                this.adaptAvailabilityReq = function(availRQ) {
                                  // TODO assumes receiving only one journeyInfo
                                  // in availRQ.(for single
                                  // sequence number)
                                  var availabilitySearchReq = {};
                                  availabilitySearchReq.travellerQuantity = {};
                                  availabilitySearchReq.travellerQuantity.adultCount = availRQ.passengers.adults;
                                  availabilitySearchReq.travellerQuantity.childCount = availRQ.passengers.children;
                                  availabilitySearchReq.travellerQuantity.infantCount = availRQ.passengers.infants;
                                  availabilitySearchReq.preferences = {};
                                  availabilitySearchReq.preferences.cabinClass = availRQ.cabinClass;
                                  availabilitySearchReq.preferences.logicalCabinClass = availRQ.cabinClass;
                                  availabilitySearchReq.preferences.currency = availRQ.currency;
                                  availabilitySearchReq.preferences.promotion = {};
                                  availabilitySearchReq.preferences.promotion.type = '';
                                  availabilitySearchReq.preferences.promotion.code = '';
                                  if (availRQ.promoCode) {
                                    availabilitySearchReq.preferences.promotion.type = 'PROMO_CODE';
                                    availabilitySearchReq.preferences.promotion.code = availRQ.promoCode;
                                  }
                                  availabilitySearchReq.journeyInfo = [];
                                  availabilitySearchReq.journeyInfo[0] = {};
                                  availabilitySearchReq.journeyInfo[0].origin = availRQ.origin.toUpperCase();
                                  availabilitySearchReq.journeyInfo[0].destination = availRQ.destination.toUpperCase();
                                  availabilitySearchReq.journeyInfo[0].originCity = (availRQ.originCity == 'Y' ? true : false);
                                  availabilitySearchReq.journeyInfo[0].destinationCity = (availRQ.destCity == 'Y' ? true : false);
                                  availabilitySearchReq.journeyInfo[0].departureDateTime = availRQ.departureDate;
                                  var diff = 0;
                                  var startDate = moment(availRQ.departureDate, JSON_DATE_FORMAT).subtract(3, 'days');
                                  if (moment(startDate, JSON_DATE_FORMAT).isBefore(moment())) {
                                    diff = moment().diff(moment(startDate, JSON_DATE_FORMAT), 'days');
                                  }
                                  if(availRQ.departureVariance > 0){
                                	  availabilitySearchReq.journeyInfo[0].departureVariance = availRQ.departureVariance + diff;
                                  }else{
                                	  availabilitySearchReq.journeyInfo[0].departureVariance = availRQ.departureVariance;
                                  }
                                  return availabilitySearchReq;

                                },

                                this.adaptFareClasses = function(response) {
                                  var fareTypeInfo = [];
                                  for (var i = 0; i < response.fareClasses.length; i++) {
                                    var fareClass = {};
                                    fareClass.fareTypeId = response.fareClasses[i].fareClassCode;
                                    fareClass.fareTypeName = response.fareClasses[i].description;
                                    fareClass.description = response.fareClasses[i].comment;
                                    fareClass.imageUrl = response.fareClasses[i].imageUrl;
                                    fareClass.rank = response.fareClasses[i].rank;
                                    fareClass.uniqueIdentification = i;
                                    fareClass.fareClassType = response.fareClasses[i].fareClassType;
                                    fareClass.fareClassId = response.fareClasses[i].fareClassId;
                                    fareClass.templateDTO = response.fareClasses[i].templateDTO;
                                    fareTypeInfo.push(fareClass);
                                  }
                                  fareTypeInfo.sort(fareClassCompare);
                                  return fareTypeInfo;
                                },

                                this.adaptTimeline = function(response, departureDate, departureVariance) {
                                  var timelineInfo = {};
                                  timelineInfo.success = response.success;
                                  timelineInfo.data = {};
                                  timelineInfo.data.results = [];
                                  for (var ond = 0; ond < response.originDestinationResponse.length; ond++) {
                                    var ondRes = response.originDestinationResponse[ond];
                                    if(ondRes.availableOptions.length > 0){
                                      ondRes.availableOptions.sort(sortAvailOptByDepartureDate);
                                    }
                                    var prevDeptDate = "";
                                    var prevFare = 0;
                                    for (var avOpt = 0; avOpt < ondRes.availableOptions.length; avOpt++) {
                                      var availOpt = ondRes.availableOptions[avOpt];
                                      var timeLineData = {};
                                      // get minimum fare value from avail fare
                                      // classes
                                      var availableFareClasses = availOpt.availableFareClasses;
                                      var avFarePrices = [];
                                      for (var i = 0; i < availableFareClasses.length; i++) {
                                        if (availableFareClasses[i].price != null && availableFareClasses[i].price > 0) {
                                          avFarePrices.push(availableFareClasses[i].price);
                                        }
                                      }
                                      if (avFarePrices.length > 0) {
                                        var sortedFareArr = avFarePrices.sort(function(prev, next) {
                                          return prev - next
                                        });
                                        timeLineData.fareValue = sortedFareArr[0];
                                      } else {
                                        timeLineData.fareValue = -1;
                                      }
                                      timeLineData.selected = availOpt.selected;
                                      timeLineData.flights = [];
                                      var flight = _adaptAvailOption(availOpt, ondRes.origin, ondRes.destination);
                                      if (prevDeptDate !== "" && prevDeptDate == flight.departureDate) {
                                        if (timeLineData.fareValue != -1
                                                && (timeLineData.fareValue < prevFare || prevFare === -1)) {
                                          timelineInfo.data.results[timelineInfo.data.results.length - 1].fareValue = timeLineData.fareValue;
                                          prevFare = timeLineData.fareValue;
                                        }
                                        if (timeLineData.selected) {
                                          timelineInfo.data.results[timelineInfo.data.results.length - 1].selected = true;
                                        }
                                        timelineInfo.data.results[timelineInfo.data.results.length - 1].flights
                                                .push(flight);
                                      } else {
                                        prevFare = timeLineData.fareValue;
                                        timeLineData.flights.push(flight);
                                        timeLineData.available = availOpt.seatAvailable;
                                        var departureDateTime = availOpt.segments[0].departureDateTime;
                                        var dateArr = departureDateTime.local.split("T")[0].split("-");
                                        timeLineData.date = dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
                                        prevDeptDate = timeLineData.date;
                                        timelineInfo.data.results.push(timeLineData);
                                      }
                                    }
                                  }
                                  var results = timelineInfo.data.results;
                                  if (departureVariance > 0 && results.length < 7) {
                                    var startDate = moment(departureDate, JSON_DATE_FORMAT).subtract(3, 'days');
                                    var diff = 0;
                                    if (moment(startDate, JSON_DATE_FORMAT).isBefore(moment())) {
                                      diff = moment().diff(moment(startDate, JSON_DATE_FORMAT), 'days');
                                    }
                                    startDate = moment(startDate, JSON_DATE_FORMAT).add(diff, 'days')
                                    var endDate = moment(departureDate, JSON_DATE_FORMAT).add(3 + diff, 'days');
                                    var newTimeLineArr = [];
                                    var existTimeLineData = {};
                                    while (startDate <= endDate) {
                                      var isExistDate = false;
                                      for (var i = 0; i < results.length; i++) {
                                        if (moment(results[i].date, DASH_SEPERATED_DATE).isSame(startDate)) {
                                          isExistDate = true;
                                          existTimeLineData = results[i];
                                          break;
                                        }
                                      }
                                      if (!isExistDate) {
                                        var dummyTimeLine = angular.copy(dummyTimeLineData);
                                        dummyTimeLine.date = moment(startDate, JSON_DATE_FORMAT).format(
                                                DASH_SEPERATED_DATE);
                                        if (moment(departureDate, JSON_DATE_FORMAT).isSame(
                                                moment(startDate, JSON_DATE_FORMAT))) {
                                          dummyTimeLine.selected = true;
                                        }
                                        newTimeLineArr.push(dummyTimeLine);
                                      } else {
                                        newTimeLineArr.push(existTimeLineData);
                                      }
                                      startDate = moment(startDate, JSON_DATE_FORMAT).add(1, 'days');
                                    }
                                    timelineInfo.data.results = newTimeLineArr;
                                  } else if (departureVariance === 0 && results.length === 0) {
                                    var dummyEmptyTimeLine = angular.copy(dummyTimeLineData);
                                    dummyEmptyTimeLine.date = moment(departureDate, JSON_DATE_FORMAT).format(
                                            DASH_SEPERATED_DATE);
                                    timelineInfo.data.results.push(dummyEmptyTimeLine);
                                  }
                                  _AddDateWiseFareClasses(this.adaptFareClasses(response), timelineInfo, departureDate); //
                                  for (var fl = 0; fl < timelineInfo.data.results.length; fl++) {
                                    timelineInfo.data.results[fl].flights.sort(sortDateWiseFlights);
                                  }
                                  return timelineInfo;
                                }
                        this.adaptTimelineForAllOnd = function(response, availRQ, departureVariance) {
                          var timelineInfo = {};
                          timelineInfo.success = response.success;
                          timelineInfo.data = [];

                          for (var ond = 0; ond < response.originDestinationResponse.length; ond++) {
                            timelineInfo.data[ond] = {};
                            timelineInfo.data[ond].results = [];
                            var ondRes = response.originDestinationResponse[ond];
                            ondRes.availableOptions.sort(sortAvailOptByDepartureDate);
                            var prevDeptDate = "";
                            var prevFare = 0;
                            for (var avOpt = 0; avOpt < ondRes.availableOptions.length; avOpt++) {
                              var availOpt = ondRes.availableOptions[avOpt];
                              var timeLineData = {};
                              // get minimum fare value from avail fare classes
                              var availableFareClasses = availOpt.availableFareClasses;
                              var avFarePrices = [];
                              for (var i = 0; i < availableFareClasses.length; i++) {
                                if (availableFareClasses[i].price != null && availableFareClasses[i].price > 0) {
                                  avFarePrices.push(availableFareClasses[i].price);
                                }
                              }
                              if (avFarePrices.length > 0) {
                                var sortedFareArr = avFarePrices.sort(function(prev, next) {
                                  return prev - next
                                });
                                timeLineData.fareValue = sortedFareArr[0];
                              } else {
                                timeLineData.fareValue = -1;
                              }
                              timeLineData.selected = availOpt.selected;
                              timeLineData.flights = [];
                              var flight = _adaptAvailOption(availOpt, ondRes.origin, ondRes.destination);
                              if (prevDeptDate !== "" && prevDeptDate == flight.departureDate) {
                                if (timeLineData.fareValue != -1
                                        && (timeLineData.fareValue < prevFare || prevFare === -1)) {
                                  timelineInfo.data[ond].results[timelineInfo.data[ond].results.length - 1].fareValue = timeLineData.fareValue;
                                  prevFare = timeLineData.fareValue;
                                }
                                if (timeLineData.selected) {
                                  timelineInfo.data[ond].results[timelineInfo.data[ond].results.length - 1].selected = true;
                                }
                                timelineInfo.data[ond].results[timelineInfo.data[ond].results.length - 1].flights
                                        .push(flight);
                              } else {
                                prevFare = timeLineData.fareValue;
                                timeLineData.flights.push(flight);
                                timeLineData.available = availOpt.seatAvailable;
                                var departureDateTime = availOpt.segments[0].departureDateTime;
                                var dateArr = departureDateTime.local.split("T")[0].split("-");
                                timeLineData.date = dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
                                prevDeptDate = timeLineData.date;
                                timelineInfo.data[ond].results.push(timeLineData);
                              }
                            }
                          }
                          var timeInfo = timelineInfo.data;
                          for (var j = 0; j < timeInfo.length; j++) {
                            var departureDate = availRQ.journeyInfo[j].departureDateTime;
                            if (departureVariance > 0 && timeInfo[j].results.length < 7) {
                              var startDate = moment(departureDate, JSON_DATE_FORMAT).subtract(3, 'days');
                              var diff = 0;
                              if (moment(startDate, JSON_DATE_FORMAT).isBefore(moment())) {
                                diff = moment().diff(moment(startDate, JSON_DATE_FORMAT), 'days');
                              }
                              startDate = moment(startDate, JSON_DATE_FORMAT).add(diff, 'days')
                              var endDate = moment(departureDate, JSON_DATE_FORMAT).add(3 + diff, 'days');
                              var newTimeLineArr = [];
                              var existTimeLineData = {};
                              while (startDate <= endDate) {
                                var isExistDate = false;
                                for (var i = 0; i < timeInfo[j].results.length; i++) {
                                  if (moment(timeInfo[j].results[i].date, DASH_SEPERATED_DATE).isSame(startDate)) {
                                    isExistDate = true;
                                    existTimeLineData = timeInfo[j].results[i];
                                    break;
                                  }
                                }
                                if (!isExistDate) {
                                  var dummyTimeLine = angular.copy(dummyTimeLineData);
                                  dummyTimeLine.date = moment(startDate, JSON_DATE_FORMAT).format(DASH_SEPERATED_DATE);
                                  if (moment(departureDate, JSON_DATE_FORMAT).isSame(
                                          moment(startDate, JSON_DATE_FORMAT))) {
                                    dummyTimeLine.selected = true;
                                  }
                                  newTimeLineArr.push(dummyTimeLine);
                                } else {
                                  newTimeLineArr.push(existTimeLineData);
                                }
                                startDate = moment(startDate, JSON_DATE_FORMAT).add(1, 'days');
                              }
                              timelineInfo.data[j].results = newTimeLineArr;
                            } else if (departureVariance === 0 && results.length === 0) {
                              var dummyEmptyTimeLine = angular.copy(dummyTimeLineData);
                              dummyEmptyTimeLine.date = moment(departureDate, JSON_DATE_FORMAT).format(
                                      DASH_SEPERATED_DATE);
                              timelineInfo.data[j].results.push(dummyEmptyTimeLine);
                            }

                            _AddDateWiseFare(this.adaptFareClasses(response), timelineInfo, j); //
                            for (var fl = 0; fl < timelineInfo.data[j].results.length; fl++) {
                              timelineInfo.data[j].results[fl].flights.sort(sortDateWiseFlights);
                            }
                          }

                          return timelineInfo;
                        }

                        this.adaptFareQuote = function(response){
                        	if(response.selectedFlightPricing != undefined && response.selectedFlightPricing.ondOWPricing.length > 1){
                        		/** replace total pricing  object for fake return discount **/
                        		var totalPaxWiseList = response.selectedFlightPricing.total.paxWise;
                        		var OWPriceList = response.selectedFlightPricing.ondOWPricing;
                        		var totalSegFareSurcharge = 0;
                        		var totalSegFareTax = 0;
                        		var totalSegFare = 0;

                        		// clear total pax wise objects' replaceable data
                        		for(var pw = 0; pw < totalPaxWiseList.length ; pw++){
                        			response.selectedFlightPricing.total.paxWise[pw].fare = 0;
                        			response.selectedFlightPricing.total.paxWise[pw].surcharge = 0;
                        			response.selectedFlightPricing.total.paxWise[pw].tax = 0;
                        			response.selectedFlightPricing.total.paxWise[pw].total = 0;
                        			response.selectedFlightPricing.total.paxWise[pw].totalFare = 0;
                        		}

                        		// replace data in total object
                        		for(var owp = 0;owp < OWPriceList.length; owp++){
                        			totalSegFareSurcharge += OWPriceList[owp].totalSurcharges;
                        			totalSegFareTax += OWPriceList[owp].totalTaxes;
                        			var paxPriceList = OWPriceList[owp].paxWise;
                        			for(var pwp = 0; pwp < paxPriceList.length; pwp++){
                        				totalSegFare += paxPriceList[pwp].totalFare;
                        				_replaceTotalPaxWise(response, paxPriceList[pwp]);
                        			}
                        		}
                        		response.selectedFlightPricing.total.surcharge = totalSegFareSurcharge;
                        		response.selectedFlightPricing.total.tax = totalSegFareTax+response.selectedFlightPricing.serviceTaxAmountForSegmentFares;
                        		response.selectedFlightPricing.total.fare = totalSegFare;
                        	}
                        	return response;
                        }

                        this.adaptBaggageRatesRequest = function (fare, flight, searchCriteria) {

                          return {

                            journeyInfo: [
                              {
                                origin: flight.originCode,
                                originCity: false,
                                destination: flight.destinationCode,
                                destinationCity: false,
                                departureDateTime: formatDateAndTimeForBaggageRequest(flight.departureDate,
                                    flight.departureTime),
                                arrivalDateTime: formatDateAndTimeForBaggageRequest(flight.arrivalDate, flight.arrivalTime),
                                departureVariance: 0,
                                specificFlights: adaptSpecificFlights(flight),

                                preferences: {
                                  additionalPreferences: {
                                    fareClassType: fare.fareClassType,
                                    fareClassId: fare.fareClassId
                                  }
                                }

                              }
                            ],
                            travellerQuantity: {
                              adultCount: searchCriteria.adult,
                              childCount: searchCriteria.child,
                              infantCount: searchCriteria.infant
                            },
                            preferences: {
                              cabinClass: searchCriteria.cabin,
                              logicalCabinClass: searchCriteria.cabin,
                              currency: searchCriteria.currency,
                              promotion: {
                                "type": "",
                                "code": searchCriteria.promoCode
                              }
                            }
                          };

                        };

                        function formatDateAndTimeForBaggageRequest(date, time) {
                          return moment(date + ' ' + time, 'DD/MM/YYYY HH:mm').
                              format("YYYY-MM-DDTHH:mm:ss");
                        }

                        function adaptSpecificFlights(flight) {
                          var specificFlights = [];
                          var firstSegmentCode;

                          if (_.isEmpty(flight.stops)) {
                            firstSegmentCode = flight.originCode + "/" + flight.destinationCode;
                          } else {
                            firstSegmentCode = flight.originCode + "/" + flight.stops[0].airportCode;
                          }

                          specificFlights[0] = {
                            filghtDesignator: flight.flightNumber,
                            segmentCode: firstSegmentCode,
                            departureDateTime: formatDateAndTimeForBaggageRequest(flight.departureDate,
                                flight.departureTime),
                            flightSegmentRPH: flight.flightRPH,
                            routeRefNumber: null

                          };

                          _.forEach(flight.stops, function (flight) {
                            var specificFlight = {
                              filghtDesignator: flight.flightNumber,
                              segmentCode: flight.originCode + "/" + flight.destinationCode,
                              departureDateTime: formatDateAndTimeForBaggageRequest(flight.departureDate,
                                  flight.departureTime),
                              flightSegmentRPH: flight.flightRPH,
                              routeRefNumber: null
                            };
                            specificFlights.push(specificFlight);
                          });

                          return specificFlights;

                        }

                        function _replaceTotalPaxWise(response, segPaxPrice){
                        	for(var tot = 0; tot < response.selectedFlightPricing.total.paxWise.length; tot++){
                        		if(response.selectedFlightPricing.total.paxWise[tot].paxType == segPaxPrice.paxType){
                        			response.selectedFlightPricing.total.paxWise[tot].fare += segPaxPrice.fare;
                        			response.selectedFlightPricing.total.paxWise[tot].surcharge += segPaxPrice.surcharge;
                        			response.selectedFlightPricing.total.paxWise[tot].tax += segPaxPrice.tax;
                        			response.selectedFlightPricing.total.paxWise[tot].total += segPaxPrice.total;
                        			response.selectedFlightPricing.total.paxWise[tot].totalFare += segPaxPrice.totalFare;
                        			break;
                        		}
                        	}
                        }

                        function _AddDateWiseFare(fareClasses, timelineInfo, j) {
                          var timeLineArr = timelineInfo.data[j].results;
                          for (var timeLnIndx = 0; timeLnIndx < timeLineArr.length; timeLnIndx++) {
                            var timeLineData = timeLineArr[timeLnIndx];
                            var dateWiseFareClassArr = [];
                            var nonExistFareId = "";
                            for (var opt = 0; opt < timeLineData.flights.length; opt++) {
                              var flight = timeLineData.flights[opt];
                              var flightAvailFareClsList = flight.flightFaresAvailability;
                              if (flightAvailFareClsList.length > 0) {
                                for (var fc = 0; fc < flightAvailFareClsList.length; fc++) {
                                  var avFareCls = flightAvailFareClsList[fc];
                                  var isExist = false;
                                  if (dateWiseFareClassArr.length > 0) {
                                    for (var dInx = 0; dInx < dateWiseFareClassArr.length; dInx++) {
                                      var dateWiseFc = dateWiseFareClassArr[dInx];
                                      if ((dateWiseFc.fareTypeId === avFareCls.fareTypeId)) {
                                        if (dateWiseFc.fareValue < avFareCls.fareValue) {
                                          isExist = true;
                                          break;
                                        } else {
                                          dateWiseFareClassArr.splice(dInx, 1);
                                          break;
                                        }
                                      }
                                    }
                                    if (!isExist) {
                                      for (var avlafc = 0; avlafc < fareClasses.length; avlafc++) {
                                        var fareClass = fareClasses[avlafc];
                                        if (fareClass.fareTypeId === avFareCls.fareTypeId) {
                                          fareClass.fareValue = avFareCls.fareValue;
                                          dateWiseFareClassArr.push(fareClass);
                                          break;
                                        }
                                      }
                                    }
                                  } else {
                                    for (var avlbfc = 0; avlbfc < fareClasses.length; avlbfc++) {
                                      var fareClass = fareClasses[avlbfc];
                                      if (fareClass.fareTypeId === avFareCls.fareTypeId) {
                                        fareClass.fareValue = avFareCls.fareValue;
                                        dateWiseFareClassArr.push(fareClass);
                                        break;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            if (dateWiseFareClassArr.length > 0) {
                              timelineInfo.data[j].results[timeLnIndx].fareClasses = dateWiseFareClassArr
                                      .sort(sortFareClassByPrice);
                            } else {
                              timelineInfo.data[j].results[timeLnIndx].fareClasses = fareClasses
                                      .sort(sortFareClassByPrice);
                            }
                          }
                          _addDummyForNonAvailFareClasses(timelineInfo, j);
                        }
                        function _addDummyForNonAvailFareClasses(timelineInfo, j) {
                          var timeLineArr = timelineInfo.data[j].results;
                          for (var timeLnIndx = 0; timeLnIndx < timeLineArr.length; timeLnIndx++) {
                            var timeLineData = timeLineArr[timeLnIndx];
                            for (var opt = 0; opt < timeLineData.flights.length; opt++) {
                              var flight = timeLineData.flights[opt];
                              var fAvlFareClsList = flight.flightFaresAvailability;
                              if (fAvlFareClsList.length) {
                                flight.isFlightFull = false;
                              } else {
                                flight.isFlightFull = true;
                              }

                              if (moment(flight.departureDate, JSON_DATE_FORMAT).isSame(
                                      moment(timeLineData.date, DASH_SEPERATED_DATE))) {
                                timeLineData.selected = true;
                              }

                              var newFlightFareAvailability = [];
                              for (var fc = 0; fc < timeLineData.fareClasses.length; fc++) {
                                var fareCls = timeLineData.fareClasses[fc];
                                var isExist = false;
                                var tempFareTypeId = "";
                                for (var avfc = 0; avfc < fAvlFareClsList.length; avfc++) {
                                  if (fAvlFareClsList[avfc].fareTypeId === fareCls.fareTypeId) {
                                    isExist = true;
                                    newFlightFareAvailability.push(fAvlFareClsList[avfc]);
                                    break;
                                  }
                                }

                                if (!isExist
                                    && constantFactory.AppConfigStringValue('bundleFareDescriptionTemplateVersion') != 'v2') {
                                  var dummyAvFc = angular.copy(dummyAvailFareClass);
                                  dummyAvFc.fareTypeId = fareCls.fareTypeId;
                                  newFlightFareAvailability.push(dummyAvFc);
                                }
                              }
                              timelineInfo.data[j].results[timeLnIndx].flights[opt].flightFaresAvailability = newFlightFareAvailability;
                            }
                          }
                        }
                        this.adaptFlightFare = function(response) {
                          var ondInfo = {};
                          ondInfo.success = response.success;
                          ondInfo.data = {};
                          ondInfo.data.fareTypeInfo = adaptFareClasses(response);
                          ondInfo.data.fareFareResults = _adaptAvailableOptions(response);
                          return ondInfo;
                        }

                        function _adaptAvailableOptions(response) {
                          var availOptionList = [];
                          var ondResList = response.originDestinationResponse;
                          for (var onds = 0; onds < ondResList.length; onds++) {
                            var ondRes = ondResList[onds];
                            for (var avOpt = 0; avOpt < ondRes.availableOptions.length; avOpt++) {
                              availOptionList.push(_adaptAvailOption(ondRes.availableOptions[avOpt], ondRes.origin,
                                      ondRes.destination));
                            }
                          }
                          return availOptionList;
                        }

                        function _adaptAvailOption(availOpt, origin, destination) {
                          var newOndRes = {};
                          var originLocations = availOpt.segments[0].segmentCode.split('/');
                          var destinationLocations = availOpt.segments[availOpt.segments.length -1].segmentCode.split('/');
                          newOndRes.originCode = originLocations[0];
                          newOndRes.destinationCode = destinationLocations[destinationLocations.length - 1];
                          newOndRes.originName = commonLocalStorageService.getAirportCityName(newOndRes.originCode, false);
                          newOndRes.destinationName = commonLocalStorageService.getAirportCityName(newOndRes.destinationCode, false);
                          // ond.js
                          newOndRes.careerCode = availOpt.segments[0].carrierCode;
                          newOndRes.returnFlag = false // hard coded
                          newOndRes.flightRPH = availOpt.segments[0].flightSegmentRPH;
                          newOndRes.routeRefNumber = availOpt.segments[0].routeRefNumber;
                          newOndRes.flightId = newOndRes.flightRPH.substring(2);
                          newOndRes.flightNumber = availOpt.segments[0].filghtDesignator;
                          newOndRes.travelMode = availOpt.segments[0].travelMode;
                          newOndRes.journeyType = availOpt.segments[0].journeyType;
                          newOndRes.equipmentModelNumber = availOpt.segments[0].equipmentModelNumber;
                          newOndRes.originCountryCode = availOpt.segments[0].originCountryCode;
                          var depDateTimeArr = availOpt.segments[0].departureDateTime.local.split("T");
                          var dateArr = depDateTimeArr[0].split("-");
                          newOndRes.departureDate = dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
                          var depTimeArr = depDateTimeArr[1].split(":");
                          newOndRes.departureTime = depTimeArr[0] + ":" + depTimeArr[1];
                          // Arrival Time
                          var arrDateTimeArr = availOpt.segments[availOpt.segments.length - 1].arrivalDateTime.local
                                  .split("T");
                          var arDateArr = arrDateTimeArr[0].split("-");
                          newOndRes.arrivalDate = arDateArr[2] + "/" + arDateArr[1] + "/" + arDateArr[0];
                          var arrTimeArr = arrDateTimeArr[1].split(":");
                          newOndRes.arrivalTime = arrTimeArr[0] + ":" + arrTimeArr[1];

                          // Arrival Time for first segment
                          var arrDateTimeFirstSeg = availOpt.segments[0].arrivalDateTime.local.split("T");
                          var arDateArrFirstSeg = arrDateTimeFirstSeg[0].split("-");
                          newOndRes.arrivalDateFirstSeg = arDateArrFirstSeg[2] + "/" + arDateArrFirstSeg[1] + "/"
                                  + arDateArrFirstSeg[0];
                          var arrTimeArrFirstSeg = arrDateTimeFirstSeg[1].split(":");
                          newOndRes.arrivalTimeFirstSeg = arrTimeArrFirstSeg[0] + ":" + arrTimeArrFirstSeg[1];

                          newOndRes.segmentDuration = _duration(availOpt.segments[0].departureDateTime.zulu,
                                  availOpt.segments[0].arrivalDateTime.zulu);
                          newOndRes.totalDuration = _duration(availOpt.segments[0].departureDateTime.zulu,
                                  availOpt.segments[availOpt.segments.length - 1].arrivalDateTime.zulu);

                          var showNextDay = function(date1, date2) {
                            var d1 = moment(date1, 'DD/MM/YYY');
                            var d2 = moment(date2, 'DD/MM/YYY');

                            var days = _getRoundedDays(availOpt.segments[0].departureDateTime.zulu,
                                    availOpt.segments[availOpt.segments.length - 1].arrivalDateTime.zulu);
                            if (d1 < d2) {
                            	return '(+1)';
                              //return '(+'+days+')';
                            } else {
                              return '';
                            }
                          };
                          newOndRes.nextDayFlag = showNextDay(newOndRes.departureDate, newOndRes.arrivalDate);
                          // availOpt.totalDuration;
                          newOndRes.stops = [];
                          for (var seg = 1; seg < availOpt.segments.length; seg++) {
                            newOndRes.stops.push(_adaptStop(availOpt.segments[seg - 1], availOpt,
                                    availOpt.segments[seg]));
                          }
                          newOndRes.flightFaresAvailability = [];
                          for (var avFareCls = 0; avFareCls < availOpt.availableFareClasses.length; avFareCls++) {
                            var avFareClsArr = availOpt.availableFareClasses;
                            newOndRes.flightFaresAvailability.push(_adaptAvailFareCls(avFareClsArr[avFareCls]));
                          }
                          newOndRes.flightFaresAvailability.sort(sortFareClassByPrice);
                          return newOndRes;
                        }

                        function _duration(firstDeparture, lastArrival) {
                          var from = moment(firstDeparture, JSON_DATE_FORMAT);
                          var to = moment(lastArrival, JSON_DATE_FORMAT);
                          var duration = moment.duration(to.diff(from));
                          return duration.days() + ":" + duration.hours() + ":" + duration.minutes();
                          // return moment.utc(to.diff(from)).format("HH:mm");
                        }

                        function _getRoundedDays(firstDeparture, lastArrival) {
                          var from = moment(firstDeparture, JSON_DATE_FORMAT);
                          var to = moment(lastArrival, JSON_DATE_FORMAT);
                          var duration = moment.duration(to.diff(from));
                          var h = parseInt(duration.days()) * 24 + parseInt(duration.hours());
                          var days = Math.ceil(h / 24);
                          return days;
                        }
                        function _adaptStop(prevSeg, availOpt, segment) {
                          var stop = {};
                          stop.originCode = segment.segmentCode.split("/")[0];
                          stop.destinationCode = segment.segmentCode.split("/")[1];
                          stop.originName = commonLocalStorageService.getAirportCityName(stop.originCode, false);
                          stop.destinationName = commonLocalStorageService.getAirportCityName(stop.destinationCode, false);

                          stop.travelMode = segment.travelMode;
                          stop.journeyType = segment.journeyType;
                            var arrSegDateTimeArr = segment.arrivalDateTime.local.split("T");
                          // stop.arrivalDate =
                          // arrSegDateTimeArr[0].replace("-", "/");
                          // var arrSegDateTimeArr =
                          // segment.arrivalDateTime.local.split("T");
                          var arrDateArr = arrSegDateTimeArr[0].split("-");
                          stop.arrivalDate = arrDateArr[2] + "/" + arrDateArr[1] + "/" + arrDateArr[0];
                          var arrTimeArr = arrSegDateTimeArr[1].split(":");
                          stop.arrivalTime = arrTimeArr[0] + ":" + arrTimeArr[1];

                          var depSegDateTimeArr = segment.departureDateTime.local.split("T");
                          // stop.departureDate =
                          // depSegDateTimeArr[0].replace("-", "/");
                          var depDateArr = depSegDateTimeArr[0].split("-");
                          stop.departureDate = depDateArr[2] + "/" + depDateArr[1] + "/" + depDateArr[0];
                          var depTimeArr = depSegDateTimeArr[1].split(":");
                          stop.departureTime = depTimeArr[0] + ":" + depTimeArr[1];

                          stop.airportCode = segment.segmentCode.split("/")[0];
                          stop.airportName = commonLocalStorageService.getAirportCityName(stop.airportCode, false);
                          stop.careerCode = segment.carrierCode;
                          stop.equipmentModelNumber = segment.equipmentModelNumber;
                          stop.flightRPH = segment.flightSegmentRPH;
                          stop.routeRefNumber = segment.routeRefNumber;
                          stop.flightNumber = segment.filghtDesignator;
                          stop.segmentDuration = _duration(segment.departureDateTime.zulu,
                                  segment.arrivalDateTime.zulu);
                          stop.durationWaiting = _duration(prevSeg.arrivalDateTime.zulu,
                                  segment.departureDateTime.zulu);
                          return stop;
                        }

                        function _adaptAvailFareCls(availFareCls) {
                          var flightFareAv = {};
                          flightFareAv.availableSeats = availFareCls.availableSeats;
                          if (availFareCls.additionalFareClassInfo == null) {
                            flightFareAv.additionalFareClassInfo = null;
                          } else {
                            flightFareAv.additionalFareClassInfo = {};
                            flightFareAv.additionalFareClassInfo.discountAvailable = availFareCls.additionalFareClassInfo.discountAvailable;
                            flightFareAv.additionalFareClassInfo.discountAmount = availFareCls.additionalFareClassInfo.discountAmount;
                          }
                          flightFareAv.fareValue = availFareCls.price;
                          flightFareAv.fareTypeId = availFareCls.fareClassCode;
                          flightFareAv.returnFare = false; // TODO hard coded
                          flightFareAv.visibleChannels = availFareCls.visibleChannels;
                          return flightFareAv;
                        }

                        function _AddDateWiseFareClasses(fareClasses, timelineInfo, departureDate) {
                          var timeLineArr = timelineInfo.data.results;
                          for (var timeLnIndx = 0; timeLnIndx < timeLineArr.length; timeLnIndx++) {
                            var timeLineData = timeLineArr[timeLnIndx];
                            var dateWiseFareClassArr = [];
                            var nonExistFareId = "";
                            for (var opt = 0; opt < timeLineData.flights.length; opt++) {
                              var flight = timeLineData.flights[opt];
                              var flightAvailFareClsList = flight.flightFaresAvailability;
                              if (flightAvailFareClsList.length > 0) {
                                for (var fc = 0; fc < flightAvailFareClsList.length; fc++) {
                                  var avFareCls = flightAvailFareClsList[fc];
                                  var isExist = false;
                                  if (dateWiseFareClassArr.length > 0) {
                                    for (var dInx = 0; dInx < dateWiseFareClassArr.length; dInx++) {
                                      var dateWiseFc = dateWiseFareClassArr[dInx];
                                      if ((dateWiseFc.fareTypeId === avFareCls.fareTypeId)) {
                                        if (dateWiseFc.fareValue < avFareCls.fareValue) {
                                          isExist = true;
                                          break;
                                        } else {
                                          dateWiseFareClassArr.splice(dInx, 1);
                                          break;
                                        }
                                      }
                                    }
                                    if (!isExist) {
                                      for (var avlafc = 0; avlafc < fareClasses.length; avlafc++) {
                                        var fareClass = fareClasses[avlafc];
                                        if (fareClass.fareTypeId === avFareCls.fareTypeId) {
                                          fareClass.fareValue = avFareCls.fareValue;
                                          dateWiseFareClassArr.push(fareClass);
                                          break;
                                        }
                                      }
                                    }
                                  } else {
                                    for (var avlbfc = 0; avlbfc < fareClasses.length; avlbfc++) {
                                      var fareClass = fareClasses[avlbfc];
                                      if (fareClass.fareTypeId === avFareCls.fareTypeId) {
                                        fareClass.fareValue = avFareCls.fareValue;
                                        dateWiseFareClassArr.push(fareClass);
                                        break;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            if (dateWiseFareClassArr.length > 0) {
                              timelineInfo.data.results[timeLnIndx].fareClasses = dateWiseFareClassArr
                                      .sort(sortFareClassByPrice);
                            } else {
                              timelineInfo.data.results[timeLnIndx].fareClasses = fareClasses
                                      .sort(sortFareClassByPrice);
                            }
                          }
                          _addDummyForNonAvailFareCls(timelineInfo, departureDate);
                        }

                        function _addDummyForNonAvailFareCls(timelineInfo, departureDate) {
                          var timeLineArr = timelineInfo.data.results;
                          for (var timeLnIndx = 0; timeLnIndx < timeLineArr.length; timeLnIndx++) {
                            var timeLineData = timeLineArr[timeLnIndx];
                            for (var opt = 0; opt < timeLineData.flights.length; opt++) {
                              var flight = timeLineData.flights[opt];
                              var fAvlFareClsList = flight.flightFaresAvailability;
                              if (fAvlFareClsList.length) {
                                flight.isFlightFull = false;
                              } else {
                                flight.isFlightFull = true;
                              }

                              if (moment(departureDate, JSON_DATE_FORMAT)) {
                                if (moment(departureDate, JSON_DATE_FORMAT).isSame(
                                        moment(timeLineData.date, DASH_SEPERATED_DATE))) {
                                  timeLineData.selected = true;
                                }
                              }

                              var newFlightFareAvailability = [];
                              for (var fc = 0; fc < timeLineData.fareClasses.length; fc++) {
                                var fareCls = timeLineData.fareClasses[fc];
                                var isExist = false;
                                var tempFareTypeId = "";
                                for (var avfc = 0; avfc < fAvlFareClsList.length; avfc++) {
                                  if (fAvlFareClsList[avfc].fareTypeId === fareCls.fareTypeId) {
                                    isExist = true;
                                    newFlightFareAvailability.push(fAvlFareClsList[avfc]);
                                    break;
                                  }
                                }
                                if (!isExist) {
                                  var dummyAvFc = angular.copy(dummyAvailFareClass);
                                  dummyAvFc.fareTypeId = fareCls.fareTypeId;
                                  newFlightFareAvailability.push(dummyAvFc);
                                }
                              }
                              timelineInfo.data.results[timeLnIndx].flights[opt].flightFaresAvailability = newFlightFareAvailability;
                            }
                          }
                        }
                        // utill functions
                        function fareClassCompare(fareClassA, fareClassB) {
                          if (fareClassA.fareTypeId < fareClassB.fareTypeId)
                            return -1;
                          else if (fareClassA.fareTypeId > fareClassB.fareTypeId)
                            return 1;
                          else
                            return 0;
                        }
                        function sortFareClassByPrice(fareClassA, fareClassB) {
                          if (fareClassA.fareValue < fareClassB.fareValue)
                            return -1;
                          else if (fareClassA.fareValue > fareClassB.fareValue)
                            return 1;
                          else
                        	  if(fareClassA.rank < fareClassB.rank)
                                  return -1;
                              else if (fareClassA.rank > fareClassB.rank)
                                  return 1;
                              else
                                  return 0;
                        }
                        function sortAvailOptByDepartureDate(availOptA, availOptB) {
                          if (availOptA.segments[0].departureDateTime.local < availOptB.segments[0].departureDateTime.local)
                            return -1;
                          else if (availOptA.segments[0].departureDateTime.local > availOptB.segments[0].departureDateTime.local)
                            return 1;
                          else
                            return 0;
                        }
                        function sortDateWiseFlights(flightA, flightB) {
                          var depDateTimeStrA = flightA.departureDate + " " + flightA.departureTime;
                          var arriveDateTimeStrA = flightA.arrivalDate + " " + flightA.arrivalTime;
                          var depDateTimeStrB = flightB.departureDate + " " + flightB.departureTime;
                          var arriveDateTimeStrB = flightB.arrivalDate + " " + flightB.arrivalTime;
                          var hubCount = flightA.stops.length - flightB.stops.length;
                          if (flightA.stops.length > 0) {
                            arriveDateTimeStrA = flightA.stops[flightA.stops.length - 1].arrivalDate + " "
                                    + flightA.stops[flightA.stops.length - 1].arrivalTime;
                          }
                          if (flightB.stops.length > 0) {
                            arriveDateTimeStrB = flightB.stops[flightB.stops.length - 1].arrivalDate + " "
                                    + flightB.stops[flightB.stops.length - 1].arrivalTime;
                          }
                          var totalDurationA = moment(arriveDateTimeStrA, "DD-MM-YYYY HH:mm").diff(
                                  moment(depDateTimeStrA, "DD-MM-YYYY HH:mm"), 'minutes');
                          var totalDurationB = moment(arriveDateTimeStrB, "DD-MM-YYYY HH:mm").diff(
                                  moment(depDateTimeStrB, "DD-MM-YYYY HH:mm"), 'minutes');
                          var totalDuration = totalDurationA - totalDurationB;
                          if (hubCount != 0) { return hubCount; }
                          var comparison = moment(depDateTimeStrA, "DD-MM-YYYY HH:mm").diff(
                                  moment(depDateTimeStrB, "DD-MM-YYYY HH:mm"), 'minutes');
                          if (comparison != 0) { return comparison; }
                          return totalDuration;
                        }
                        // dummy data
                        var dummyAvailFareClass = {
                          "availableSeats": "",
                          "fareValue": "null",
                          "fareClassCode": "",
                          "returnFare": false,
                          "fareTypeId": "",
                          "visibleChannels": []
                        }
                        var dummyTimeLineData = {
                          "available": true,
                          "date": "",
                          "selected": false,
                          "flights": [],
                          "fareValue": "",
                          "fareClasses": []
                        }
                      }]);
})(angular);