(function(angular) {
  var app = angular.module('ibeAPIService');

  app.service('remoteAvailability', ['$http', 'availabilityAdaptor', 'ConnectionProps', 'popupService','alertService', function($http, availabilityAdaptor, ConnectionProps, popupService,alertService) {

    this._basePath = ConnectionProps.host;
    var JSON_DATE_FORMAT = "YYYY-MM-DDTHH:mm:ss";

    /*this.calendarFareSearch = function(availRQ, successFn, errorFn) {
      console.log('inside avail search');
      var __$this = this;
      this.retrieveFareTimeline(availRQ, function(timelineResponse) {
        __$this.retrieveFlightFare({}, function(fareResponse) {
          var data = angular.extend({}, timelineResponse, fareResponse);
          successFn(data);
        }, function(fareError) {
          errorFn();
        })
      }, function(errorResponse) {
        errorFn();
      });
    }*/

    this.retrieveFareTimeline = function(availRQ, successFn, errorFn) {
      var path = "/availability/search/fare/flight/calendar/minimum";
      self.departureDate = availRQ.departureDate;
      self.departureVariance = availRQ.departureVariance;
      var availabilitySearchReq = availabilityAdaptor.adaptAvailabilityReq(availRQ);
      var retFn = function() {
        this.success = function(response) {
        }, this.error = function(error) {
        }
      }
      $http({
        "method": 'POST',
        "url": this._basePath + path,
        "data": availabilitySearchReq
      }).success(function(response) {
        var data = {};
        var timeLineInfo = availabilityAdaptor.adaptTimeline(response, self.departureDate, self.departureVariance);
        var fareClasses = availabilityAdaptor.adaptFareClasses(response);
        var promoInfo = response.selectedFlightPricing !== null ? response.selectedFlightPricing.promoInfo : [];
        data = {
          'timelineInfo': timeLineInfo.data.results,
          'fareClasses': fareClasses,
          'promoInfo': promoInfo
        };
        successFn(data);
      }).error(function(response) {
        errorFn(response);
//        console.log(response);
      });
    }
      this.retrieveFareTimelineForAllOND = function(availRQ, successFn, errorFn) {
          var path = "/availability/search/fare/flight/calendar/minimum";
          self.departureDate = availRQ.journeyInfo[1].departureDateTime;

          var ibDiff = 0;
          var isAllOnd =false;
          if(availRQ.journeyInfo[0].departureVariance ==3){
              isAllOnd =true;
          }
          var outBoundDate = moment(availRQ.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT);
          var inboundDate = moment(availRQ.journeyInfo[1].departureDateTime, JSON_DATE_FORMAT);
          var inboundStartDate = moment(inboundDate, JSON_DATE_FORMAT).subtract(3, 'days');
          if (moment(inboundStartDate, JSON_DATE_FORMAT).isBefore(moment())) {
              ibDiff = moment().diff(moment(inboundStartDate, JSON_DATE_FORMAT), 'days');
          }
          var obDiff = 0;
          var startDate = moment(availRQ.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT).subtract(3, 'days');
          if (moment(startDate, JSON_DATE_FORMAT).isBefore(moment())) {
        	  obDiff = moment().diff(moment(startDate, JSON_DATE_FORMAT), 'days');
          }
          availRQ.journeyInfo[1].departureVariance = availRQ.journeyInfo[1].departureVariance + ibDiff;
          availRQ.journeyInfo[0].departureVariance = availRQ.journeyInfo[0].departureVariance + obDiff;
          self.departureVariance = availRQ.journeyInfo[0].departureVariance + obDiff;
          var retFn = function() {
              this.success = function(response) {
              }, this.error = function(error) {
              }
          }
          $http({
              "method": 'POST',
              "url": this._basePath + path,
              "data": availRQ
          }).success(function(response) {

              var data = {};
              var timeLineInfo,fareClasses;
              var promoInfo = response.selectedFlightPricing !== null ? response.selectedFlightPricing.promoInfo : [];
              if(isAllOnd){
                  timeLineInfo = availabilityAdaptor.adaptTimelineForAllOnd(response, availRQ, self.departureVariance);
                  fareClasses = availabilityAdaptor.adaptFareClasses(response);
                  data = {
                      'timelineInfo': timeLineInfo.data,
                      'fareClasses': fareClasses,
                      'promoInfo': promoInfo
                  };
              }
              else{
                  response.originDestinationResponse.shift(); // to remove outbound flights
                  timeLineInfo = availabilityAdaptor.adaptTimeline(response, self.departureDate, self.departureVariance);
                  fareClasses = availabilityAdaptor.adaptFareClasses(response);

                  data = {
                      'timelineInfo': timeLineInfo.data.results,
                      'fareClasses': fareClasses,
                      'promoInfo': promoInfo
                  };
              }


              successFn(data);
          }).error(function(response) {
              errorFn(response);
//              console.log(response);
          });
      }

      this.getBaggageInfo = function (fare, flight, searchCriteria, successFn, errorFn) {
          var request = availabilityAdaptor.adaptBaggageRatesRequest(fare, flight, searchCriteria);
          $http({
              "method": 'POST',
              "url": this._basePath + "/availability/baggage/rates",
              "data": request
          }).success(function (response) {
              successFn(response);
          }).error(function (response) {
              errorFn(response);
          });
      };

    this.retrieveReturnFareTimeline = function(availRQ, successFn, errorFn) {
        var path = "/availability/search/fare/flight/calendar/return";
        self.departureDate = availRQ.journeyInfo[1].departureDateTime;
        var diff = 0;
        var isAllOnd =false;
        var isObSelected =false;
        var isNxtPrev = false;
        if(availRQ.journeyInfo[0].departureVariance ==3){
            isAllOnd =true;
        }else if(availRQ.journeyInfo[0].departureVariance == 0 && availRQ.journeyInfo[1].departureVariance == 0){
        	isNxtPrev = true;
        }
        if(availRQ.journeyInfo[0].specificFlights!= undefined && availRQ.journeyInfo[0].specificFlights.length > 0){
        	isObSelected =true;
        }
        var outBoundDate = moment(availRQ.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT);
        var inboundDate = moment(availRQ.journeyInfo[1].departureDateTime, JSON_DATE_FORMAT);
        var inboundStartDate = moment(inboundDate, JSON_DATE_FORMAT).subtract(3, 'days');
        if (moment(inboundStartDate, JSON_DATE_FORMAT).isBefore(moment())) {
          diff = moment().diff(moment(inboundStartDate, JSON_DATE_FORMAT), 'days');
        }
        if(!isObSelected){
        	if(moment(inboundDate, JSON_DATE_FORMAT).isBefore(moment(availRQ.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT))){
        		if(moment(inboundDate, JSON_DATE_FORMAT).isBefore(moment().startOf('day'))){
        			popupService.alert("Your return flight date cannot be prior to your departure flight date. Please select a future return flight or change the dates.");
        			return;
        		}else if(moment(inboundDate, JSON_DATE_FORMAT).isSame(moment().startOf('day'))){
        			availRQ.journeyInfo[0].departureDateTime = moment().startOf('day').format(JSON_DATE_FORMAT);;
        			availRQ.journeyInfo[1].departureDateTime = moment(moment().startOf('day'), JSON_DATE_FORMAT).add(120,'seconds').format(JSON_DATE_FORMAT);
        		}else{
        			availRQ.journeyInfo[0].departureDateTime = moment(availRQ.journeyInfo[1].departureDateTime, JSON_DATE_FORMAT).add(-120,'seconds').format(JSON_DATE_FORMAT);
        		}
        	}
        }else{
        	if(isNxtPrev){
        		if(moment(inboundDate, JSON_DATE_FORMAT).startOf('day').isBefore(moment(availRQ.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT).startOf('day'))){
        			popupService.alert("Your return flight date cannot be prior to your departure flight date. Please select a future return flight or change the dates.");
        			return;
        		}
        	}else{
        		if(moment(inboundDate, JSON_DATE_FORMAT).isBefore(moment(availRQ.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT))){
        			availRQ.journeyInfo[1].departureDateTime = moment(availRQ.journeyInfo[0].departureDateTime, JSON_DATE_FORMAT).add(120,'seconds').format(JSON_DATE_FORMAT);
        			diff =0;
        			inboundStartDate = moment(availRQ.journeyInfo[1].departureDateTime, JSON_DATE_FORMAT).subtract(3, 'days');
        		    if (moment(inboundStartDate, JSON_DATE_FORMAT).isBefore(moment())) {
        		    	diff = moment().diff(moment(inboundStartDate, JSON_DATE_FORMAT), 'days');
        		    }
        		}
        	}
        }
        var modifiedVarience = availRQ.journeyInfo[1].departureVariance;
        if(!isNxtPrev){
        	modifiedVarience+= diff;
        }
       	availRQ.journeyInfo[1].departureVariance = modifiedVarience;
       	if(!isObSelected){
       		availRQ.journeyInfo[0].departureVariance = modifiedVarience;
       	}
       	self.departureVariance = modifiedVarience;

        var retFn = function() {
            this.success = function(response) {
            }, this.error = function(error) {
            }
        }
        $http({
            "method": 'POST',
            "url": this._basePath + path,
            "data": availRQ
        }).success(function(response) {

            var data = {};
            var timeLineInfo,fareClasses;
            if(isAllOnd){
                timeLineInfo = availabilityAdaptor.adaptTimelineForAllOnd(response, availRQ, self.departureVariance);
                fareClasses = availabilityAdaptor.adaptFareClasses(response);
                data = {
                    'timelineInfo': timeLineInfo.data,
                    'fareClasses': fareClasses
                };
            }
            else{
                if (_.isNumber(availRQ.selectedSecSeq)) {
                    response.originDestinationResponse = [response.originDestinationResponse[availRQ.selectedSecSeq]];
                } else {
                    response.originDestinationResponse.shift(); // to remove outbound flights
                }
                timeLineInfo = availabilityAdaptor.adaptTimeline(response, self.departureDate, self.departureVariance);
                fareClasses = availabilityAdaptor.adaptFareClasses(response);

                data = {
                    'timelineInfo': timeLineInfo.data.results,
                    'fareClasses': fareClasses
                };
            }


            successFn(data);
        }).error(function(response) {
            errorFn(response);
//            console.log(response);
        });
    }
    this.retrieveFlightFare = function(availRQ, successFn, errorFn) {
      var path = "/availability/search/fare/flight/calendar/minimum";
      var availabilitySearchReq = availabilityAdaptor.adaptAvailabilityReq(availRQ);
      availabilitySearchReq.journeyInfo[0].departureVariance = 0; // TODO
      // selected
      // date flight
      // search
      var retFn = function() {
        this.success = function(response) {
        }, this.error = function(error) {
        }
      }
      $http({
        "method": 'POST',
        "url": this._basePath + path,
        "data": availabilitySearchReq
      }).success(function(response) {
        var data = {};
        var ondInfo = availabilityAdaptor.adaptFlightFare(response);
        var data = {
          'ondInfo': ondInfo.data
        };
        successFn(data);
      }).error(function(response) {
        errorFn(response);
      });
    }

     this.retrieveNextPrevTimeLineData = function(availRQ, successFn, errorFn) {
      var path = '/availability/search/fare/flight/calendar';
      self.newDepDate = availRQ.departureDate;
      self.departureDate = self.newDepDate; 
      var availabilitySearchReq = availabilityAdaptor.adaptAvailabilityReq(availRQ);
      $http({
        "method": 'POST',
        "url": this._basePath + path,
        "data": availabilitySearchReq
      }).success(function(response) {
        var data = {};
        var timeLineInfo = availabilityAdaptor.adaptTimeline(response,self.newDepDate, 0);
        data = {
          'timelineInfo': timeLineInfo.data.results[0]
        };
        successFn(data);
      }).error(function(response) {
        errorFn();
//        console.log(response);
      });
    }

    this.retrieveFareQuoteData = function(availRQ, successFn, errorFn) {
      var path = '/availability/selected/pricing/minimum';
      $http({
        "method": 'POST',
        "url": this._basePath + path,
        "data": availRQ
      }).success(function(response) {
        var response = availabilityAdaptor.adaptFareQuote(response);
        successFn(response);
      }).error(function(response) {
        errorFn(response);
//        console.log(response);
      });
    }
    
    this.retrieveMultiCityFlightData = function(availRQ, successFn, errorFn) {
        var path = '/availability/search/flight/calendar';
        $http({
          "method": 'POST',
          "url": this._basePath + path,
          "data": availRQ
        }).success(function(response) {
          var data = {};
          var timeLineInfo = availabilityAdaptor.adaptTimeline(response, self.departureDate, 0);
          data = {
            'timelineInfo': timeLineInfo.data.results
          };
          successFn(data);
        }).error(function(response) {
          errorFn();
        });
      }
    
    this.retrieveMultiCityFares = function(availRQ, successFn, errorFn) {
        var path = "/availability/search/fare/flight/calendar";
        $http({
          "method": 'POST',
          "url": this._basePath + path,
          "data": availRQ
        }).success(function(response) {
          var data = {};
          var ondInfo = availabilityAdaptor.adaptTimeline(response, self.departureDate, self.departureVariance);
          var fareClasses = availabilityAdaptor.adaptFareClasses(response);
          var promoInfo = response.selectedFlightPricing !== null ? response.selectedFlightPricing.promoInfo : [];
          data = {
            'ondInfo': timeLineInfo.data.results,
            'fareClasses': fareClasses,
            'promoInfo': promoInfo
          };
          successFn(data);
        }).error(function(response) {
          errorFn(response);
        });
      }

  }]);
})(angular);
