/**
 *      Created by Chamil on 26.01.2016
 */

(function () {

    angular
        .module("ibeAPIAdaptor")
        .service("registrationAdaptorService", [registrationAdaptorService]);

    function registrationAdaptorService() {
        var self = this;

        self.registerUser = function (response) {
            var adapted = response;

            return setResponseStatus(adapted, response);
        };

        self.adaptRegisterUserRequest = function (params) {
            // If exists adapt questioner
            if (params.customer.customerQuestionaire) {
                if (params.customer.customerQuestionaire[1]) {
                    params.customer.customerQuestionaire[1] = 'Y';
                } else if (params.customer.customerQuestionaire[1] === false) {
                    params.customer.customerQuestionaire[1] = 'N';
                }

                params.customer.customerQuestionaire = _.map(params.customer.customerQuestionaire, function (answer, index) {
                    return {
                        questionKey: index,
                        questionAnswer: answer
                    }
                });
            }

            return params;
        };

        var setResponseStatus = function (adaptedResult, response) {
            adaptedResult.success = response.success || true;
            adaptedResult.messages = response.messages || null;
            adaptedResult.errors = response.errors || null;
            adaptedResult.warnings = response.warnings || null;
            return adaptedResult;
        };
    }

})();
