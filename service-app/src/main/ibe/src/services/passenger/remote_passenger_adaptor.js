(function(angular) {
  var app = angular.module("ibeAPIAdaptor");

  app.service('passengerAdaptor', ['$http',function($http) {

    this.adaptDuplicateCheckPax = function(passenger) {
      var duplicateCheckPax = {};
      var paxCategory = passenger.category || passenger.paxType;
      duplicateCheckPax.paxType = this.getPaxTypeCode(paxCategory);
      duplicateCheckPax.firstName = passenger.firstName;
      duplicateCheckPax.lastName = passenger.lastName;
      duplicateCheckPax.title = passenger.salutation || passenger.title;
      duplicateCheckPax.nationality = passenger.nationality;
      duplicateCheckPax.ffid = passenger.airewardsEmail;
      duplicateCheckPax.paxSequence = passenger.paxSequence;
      duplicateCheckPax.foidType = null;
      duplicateCheckPax.foidNumber = null;
      return duplicateCheckPax;
    },

    this.adaptFFIDCheckList = function(passenger) {
      var lmsPassenger = {};
      lmsPassenger.paxType = this.getPaxTypeCode(passenger.category);
      lmsPassenger.firstName = passenger.firstName;
      lmsPassenger.lastName = passenger.lastName;
      lmsPassenger.ffid = passenger.airewardsEmail;
      return lmsPassenger;
    },
    
    this.adaptPassenger = function(passenger) {
    	
    	var passengerInfo = {};
    	passengerInfo.title = passenger.salutation;
    	passengerInfo.firstName = passenger.firstName;
    	passengerInfo.lastName = passenger.lastName;
    	passengerInfo.nationality = passenger.nationality;
    	passengerInfo.paxType = this.getPaxTypeCode(passenger.category);
    	passengerInfo.paxSequence = passenger.paxSequence;
    	passengerInfo.travelWith = passenger.travelingWith;
    	passengerInfo.dateOfBirth = passenger.dob;
    	var additionalInfo = passenger.additionalInfo;
    	additionalInfo.ffid = passenger.airewardsEmail;
    	passengerInfo.additionalInfo = additionalInfo;
    	return passengerInfo;
    },
    
    this.adaptContact = function(contact) {
    	var reservationContact = {};
    	reservationContact.title = contact.salutation;
    	reservationContact.firstName = contact.firstName;
    	reservationContact.lastName = contact.lastName;
    	reservationContact.nationality = contact.nationality;
    	reservationContact.country = contact.country;
    	reservationContact.preferredLangauge = contact.language;
    	reservationContact.preferredCurrency = contact.preferredCurrency;
    	reservationContact.emailAddress = contact.email;
		var address = {};
		if (contact.address) {
			address.streetAddress1 = contact.address.streetAddress1;
			address.streetAddress2 = contact.address.streetAddress2;
			address.city = contact.address.city;
			address.zipCode = contact.address.zipCode;
		}
    	reservationContact.address = address;
    	var countryCodeLength = contact.mobileCountryCode.length;
    	var mobile = {};
		if(contact.mobileNo){
			mobile.countryCode = contact.mobileNo.countryCode;
			mobile.areaCode = contact.mobileNo.number.substring(0, 2);
			mobile.number = contact.mobileNo.number.substring(2);
		}

    	reservationContact.mobileNumber = mobile;
    	var mobileTravel = {};
		if(contact.mobileNoTravel){
			mobileTravel.countryCode = contact.mobileNoTravel.countryCode;
			mobileTravel.areaCode = contact.mobileNoTravel.number.substring(0, 2);
			mobileTravel.number = contact.mobileNoTravel.number.substring(2);
		}


    	reservationContact.telephoneNumber = mobileTravel;
    	reservationContact.fax = null;
    	reservationContact.zipCode = null;
    	reservationContact.state = (contact.state == null || contact.state == undefined) ? null : contact.state.trim();
    	reservationContact.taxRegNo = contact.taxRegNo;
        reservationContact.sendPromoEmail = contact.sendPromoEmail;
    	return reservationContact;
    }
    
    this.getPaxTypeCode = function(paxTypeName) {
      if (paxTypeName == 'Adult') {
        return 'AD';
      } else if (paxTypeName == 'Child') {
        return 'CH';
      } else if (paxTypeName == 'Infant') { return 'IN'; }
      return 'INVALID';
    }

  }]);
})(angular);