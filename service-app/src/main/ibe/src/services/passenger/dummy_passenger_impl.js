(function(angular) {
	var app = angular.module("ibeDummyService");
	var sectorSequenceNo = 0;
	var fareSequenceNo = 0;

	app.service('dummyPassenger', ["$http", function($http) {
    	
	this._basePath = '';
    
		this.duplicateCheck = function(duplicateCheckRQ, successFn, errorFn) {
			
			var path = 'sampleData/duplicate_check.json';
			var returndvalue = {};
			$http({
				"method": 'GET',
				"url": this._basePath + path,
				"data": {}
			}).success(function(response) {
				var data = response;
				successFn(data);
			}).error(function(response) {
				errorFn();
			});
		
		},

		this.lmsPassengerCheck = function(paxList, successFn, errorFn, allPassengersValid) {
			var path = 'sampleData/lms_passenger_validation.json';
			var returndvalue = {};
			var paxValidationList = {"lmsPaxDetails": [{"passengerSequence": 1, "ffid": "", "firstName": "aa", "lastName": "ss", "paxType": "AD" }, {"passengerSequence": 2, "ffid": "drathnayaka@isa.ae", "firstName": "Danusha", "lastName": "Rathnayaka",  "paxType": "AD" }]};
			$http({
				"method": 'GET',
				"url": this._basePath + path,
				"data": {}
			}).success(function(response) {
				var data = response.paxValidationList;
				successFn(paxValidationList, data, allPassengersValid);
			}).error(function(response) {
				errorFn();
			});
		}
	
	}]);

})(angular);