(function(angular) {
	var app = angular.module("ibeAPIService");

	app.service('remotePassenger', ['$http', 'passengerAdaptor', 'ConnectionProps', 'loadingService', function($http, passengerAdaptor, ConnectionProps, loadingService) {

		this._basePath = ConnectionProps.host;

		this.duplicateCheck = function(paxList, successFn, errorFn) {

			loadingService.showLoading();
			var duplicateCheckRQ = {};
			var duplicateCheckList = [];

			for (var i = 0; i < paxList.length; i++) {
				var paxCategory = paxList[i].category || paxList[i].paxType;

				// Only Adult and Child type of passengers are checked in the duplicate check
				if(paxCategory !== "Infant"){
					duplicateCheckList.push(passengerAdaptor.adaptDuplicateCheckPax(paxList[i]));
				}
			}
			duplicateCheckRQ.duplicateCheckList = duplicateCheckList;

			var path = '/passenger/duplicateCheck/';
			var returndvalue = {};
			$http({
				"method": 'POST',
				"url": this._basePath + path,
				"data": duplicateCheckRQ
			}).success(function(response) {
				loadingService.hideLoading();
				var data = response;
				successFn(data);
			}).error(function(response) {
				errorFn(response);
			});

		},

		this.lmsPassengerCheck = function(paxList, successFn, errorFn, allPassengersValid) {

			loadingService.showLoading();
			var validatePaxFFIDsRQ = {};
			var ffidCheckList = [];

			for (var i = 0; i < paxList.length; i++) {
				ffidCheckList[i] = passengerAdaptor.adaptFFIDCheckList(paxList[i]);
			}
			validatePaxFFIDsRQ.lmsPaxDetails = ffidCheckList;
			var path = '/passenger/lmsPaxValidation/';
			var returndvalue = {};
			$http({
				"method": 'POST',
				"url": this._basePath + path,
				"data": validatePaxFFIDsRQ
			}).success(function(response) {
				loadingService.hideLoading();
				var data = response.paxValidationList;
				successFn(ffidCheckList, data, allPassengersValid);
			}).error(function(response) {
				errorFn(response);
			});

		}

	}]);

})(angular);
