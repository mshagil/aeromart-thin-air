/**
 *      Created by Chamil on 26.01.2016
 */

 (function () {

     angular
        .module("ibeDummyService")
        .service("dummyViewProfile", ["$http", "$q", "viewProfileAdaptorService", dummyViewProfileService]);

    function dummyViewProfileService($http, $q, viewProfileAdaptorService) {
        var self = this;

        self.getUserProfileData = function (params, successFn, errorFn) {

            var path = 'sampleData/viewProfileResponse.json';
            var deferred = $q.defer();

            $http({
                "method": 'GET',
                "url": path,
                "data": params
            }).success(function (response) {
                successFn(response);
            }).error(function(response) {
                errorFn(response);
            });

            return deferred.promise;
        }
    } // End of dummyViewProfileService.

 })();
