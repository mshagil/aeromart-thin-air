/**
 *      Created by Chamil on 26.01.2016
 */

(function () {

    angular
        .module("ibeAPIAdaptor")
        .service("viewProfileAdaptorService", [viewProfileAdaptorService]);

    function viewProfileAdaptorService() {
        var self = this;

        self.formatViewProfileData = function (response) {
            if (response.customer.customerQuestionaire) {
                response.customer.customerQuestionaire =
                    _.map(
                        _.sortBy(response.customer.customerQuestionaire,
                            function (question) {
                                return question.questionKey;
                            }
                        ),
                        function (question) {
                            return question.questionAnswer;
                        }
                    );
            }

            var adapted = response;
            return setResponseStatus(adapted, response);
        };

        var setResponseStatus = function (adaptedResult, response) {
            adaptedResult.success = response.success || true;
            adaptedResult.messages = response.messages || null;
            adaptedResult.errors = response.errors || null;
            adaptedResult.warnings = response.warnings || null;
            return adaptedResult;
        };
    }

})();
