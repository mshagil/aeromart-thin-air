(function(angular) {
  var app = angular.module("ibeDummyService");
  var sectorSequenceNo = 0;
  var fareSequenceNo = 0;

  app.service('dummyVoucher', ['$http',function($http) {
    this._basePath = '';
    this.calendarFareSearch = function(availRQ, successFn, errorFn) {
      console.log('inside avail search');
      var __$this = this;
      this.retrieveFareTimeline(availRQ, function(timelineResponse) {
        __$this.retrieveFlightFare({}, function(fareResponse) {
          var data = angular.extend({}, timelineResponse, fareResponse);
          successFn(data);
        }, function(fareError) {
          errorFn();
        })
      }, function(errorResponse) {
        errorFn();
      });

    }

   

  }]);

})(angular);