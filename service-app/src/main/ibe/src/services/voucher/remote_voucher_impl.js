(function(angular) {
  var app = angular.module('ibeAPIService');

  app.service('remoteVoucher', ['$http', 'ConnectionProps', 'popupService','alertService','paymentAdaptor', 'commonLocalStorageService',
      function($http, ConnectionProps, popupService,alertService,paymentAdaptor, commonLocalStorageService) {

    this._basePath = ConnectionProps.host;
    var JSON_DATE_FORMAT = "YYYY-MM-DDTHH:mm:ss";

    this.retrieveVoucherBannerData = function (successFn, errorFn) {
        var path = "/voucher/getVouchers";
        return $http({
            "method": 'POST',
            "url": this._basePath + path,
            "headers":{'Content-Type' : 'application/json'},
            "data": {}
        }).success(function(response) {
            successFn(response);
        }).error(function(response) {
            errorFn(response);
        });
    };


     this.getVouchersFromGroupId = function (groupId, transactionId, successFn, errorFn, context) {
         var path = "/voucher/getVouchersByGroupId";
         return $http({
             "method": 'POST',
             "url": this._basePath + path,
             "headers": {'Content-Type': 'application/json'},
             "data": { groupId: groupId, voucher_transactionId: transactionId}
         }).success(function (response) {
                  successFn(response, context);
         }).error(function (response) {
                  errorFn(response);
         });
     };


     this.retrieveVoucherPaymentData = function(param) {
        var path = "/voucher/issueVouchers";
        return $http({
            "method": 'POST',
            "url": this._basePath + path,
            "headers":{'Content-Type' : 'application/json'},
            "data": param
        }).success(function(response) {
        	//delete param.test.key1;
            return response;
        }).error(function(response) {
            return response;
        });
    };

    this.loadPaymentOptions = function(successFn,errorFn) {
        var path = "/payment/issueVoucherPaymentOptions";
        return $http({
            "method": 'POST',
            "url": this._basePath + path,
            "headers":{'Content-Type' : 'application/json'},
            "data":{
                originCountryCode : commonLocalStorageService.getData('CARRIER_CODE')
            }
        }).success(function (response) {
        	var paymentOptions = paymentAdaptor.adaptPaymentOptions(response);
            successFn(paymentOptions);
        }).error(function (response) {
            errorFn(response);
        });
    };

  }]);
})(angular);
