(function (angular) {
    var app = angular.module("ibeDummyService");

    app.service("dummyAncillary",["$http","ConnectionProps","applicationService",function ($http, ConnectionProps, applicationService) {
        var self = this;

        this._basePath = ConnectionProps.host;

        this.ancillaryAvailability;

        this.availableAncillary;

        this.ancillaryInitialData;

        this.metaData;

        this.ancillaryInitialData = {
            "ondPreferences": {
                baggage: [

                    {
                        "ondId": "186935756",
                        "flightSegmentRPH": [
                            "001",
                            "002"
                        ]
                    },
                    {
                        "ondId": "186935757",
                        "flightSegmentRPH": [
                            "003",
                            "004"
                        ]

                    }
                ]
            }
        }

        this.ancillariesAvailability = function (ancillariesAvailabilityRq, success, error) {
            var path = 'sampleData/baggage_list.json';
            $http({
                "method": 'GET',
                "url": path,
                "data": ancillariesAvailabilityRq
            }).success(function (response) {
                var data = {};
                //var timeLineInfo = availabilityAdaptor.adaptTimeline(response);
                //data = {
                //    'timelineInfo': timeLineInfo.data.results[0]
                //};
                success(data);
            }).error(function (response) {
                error();
                console.log(response);
            });
        }

        this.initializeAncillary = function (ancillariesAvailabilityRq, success, error) {
            var path = 'sampleData/ancillary_init_response.json';
            if(ancillariesAvailabilityRq.reservationData.mode == 'MODIFY'){
                path = 'sampleData/ancillary_init_response.json';
            }
            $http({
                "method": 'GET',
                "url": path,
                "data": ancillariesAvailabilityRq
            }).success(function (response) {
                success(response);
            }).error(function (response) {
                error();
                console.log(response);
            });
        }

        this.loadAvailableAncillaryData = function (availableData,success, error,mode) {
            var path = 'sampleData/ancillary_init_response.json';
            if(mode == undefined){
                mode = 'create';
            }

            if(mode == 'modify'){
                path = 'sampleData/ancillary_init_response.json';
            }

            $http({
                "method": 'GET',
                "url": path,
                "data": availableData
            }).success(function (response) {
                self.metaData = response.availableAncillariesResponse.metaData;
                success(response);
            }).error(function (response) {
                error();
                console.log(response);
            });
        };

        this.getDefaults = function (successFn,errorFn) {
            var path = 'sampleData/defaultAncillarySelectionsResponse.json';
            $http({
                "method": 'POST',
                "url": path
            }).success(function (response) {
                successFn(response);
            }).error(function (error) {
                errorFn(error);
            });
        };

        this.setAvailableAncillaryData = function (selectedAncillaryData, success, error) {
            console.log("setting ancillary data:");
            console.log(selectedAncillaryData);

            var response = {
                "success": true
            };
            success(response);
        }

        this.loadMealList = function (availableData, success, error) {
            var path = 'sampleData/meal_list.json';
            $http({
                "method": 'GET',
                "url": path,
                "data": availableData
            }).success(function (response) {
                success(response);
            }).error(function (response) {
                error();
                console.log(response);
            });
        };
        this.loadSeatMapData = function (availableData, success, error) {
            var path = 'sampleData/seatMapWithValidation.json';
            $http({
                "method": 'GET',
                "url": path,
                "data": availableData
            }).success(function (response) {
                success(response);
            }).error(function (response) {
                error();
                console.log(response);
            });
        };
        this.loadBaggageData = function (availableData, success, error) {
            var path = 'sampleData/baggage_list_ond.json';
            $http({
                "method": 'GET',
                "url": path,
                "data": availableData
            }).success(function (response) {
                success(response);
            }).error(function (response) {
                error();
                console.log(response);
            });
        };
        this.retrieveInsuranceData = function (insuranceRq, success, error) {
            var path = 'sampleData/insuranceResponse_1.json';
            $http({
                "method": 'GET',
                "url": path,
                "data": {}
            }).success(function (response) {
                success(response);
            }).error(function (response) {
                error();
                console.log(response);
            });
        };
        this.loadAirportServicesData = function (airportServiceReq, success, error) {
            var path = 'sampleData/airportServicesResponse.json';
            $http({
                "method": 'GET',
                "url": path,
                "data": {}
            }).success(function (response) {
                success(response);
            }).error(function (response) {
                error();
                console.log(response);
            });
        };
        this.loadInFlightServices = function (airportServiceReq, success, error) {
            var path = 'sampleData/in_flight_service.json';
            $http({
                "method": 'GET',
                "url": path,
                "data": {}
            }).success(function (response) {
                success(response);
            }).error(function (response) {
                error();
                console.log(response);
            });
        };

        this.clearExtras = function() {
        	// clear the variable values when setting search criteria
            self.ancillaryAvailability = null;
            self.availableAncillary = null;
            self.ancillaryInitialData = null;
            self.metaData = null;
       };
       
    }]);
})(angular);