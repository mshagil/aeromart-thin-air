/**
 * Created by Baladewa on 11/04/15.
 **/


var commonServices = function () {
    var commonServices = {};

    /**
     * Application Event Listeners Service
     * Register ListListeners With the Event Type
     * @returns {{events: {APPLICATION_MODE_CHANGE: string}, notifyListeners: notifyListeners, registerListeners: registerListeners}}
     */
    commonServices.eventListeners = [function () {
        var listeners = {};

        var _events = {
            APPLICATION_MODE_CHANGE: 'application_mode_change', // Trigger when APP Mode Changes.
            CUSTOMER_LOGIN_STATUS_CHANGE: 'customer_login_status_change', //Trigger when customer login status change.
            SHOW_MODIFY_SEARCH: 'show_modify_search', // Trigger when modify search button clicked.
            SET_TOTAL: 'set_inclusive_total', // Trigger in payment page.
            SET_LMS_DETAILS: 'lms_details_changed', // Triggers when lms details need to update in login bar.
            CONTINUE_AFTER_SUMMERY : 'continue_after_summery'

        };

        var notifyListeners = function (type, data) {
            if (type != undefined && listeners[type] != undefined) {
                angular.forEach(listeners[type], function (listner) {
                    listner(data);
                })
            }
        };
        var registerListeners = function (type, listener) {
            if (listeners[type] !== undefined) {
                listeners[type].push(listener);
            } else {
                listeners[type] = [listener];
            }
        };

        return {
            events: _events,
            notifyListeners: notifyListeners,
            registerListeners: registerListeners
        }
    }]


    commonServices.dateService = [function () {
        this.DATE_FORMAT = {
            FULL: "YYYY-MM-DDTHH:mm:ss",
            TWO: "ddd DD MMM YYYY HH:mm",
            THREE: "HH:mm",
            FOUR: "ddd DD MMM YYYY",
            FIVE: "DD MMM YYYY",
            SIX: "YYYY-MM-DD"
        };

        this.getFormattedDate = function (sourceDate, sourceFormat, requestedFormat) {
            if (!!sourceDate) {
                var tmpDate = moment(sourceDate, sourceFormat);
                return tmpDate.format(requestedFormat);
            }
            return "";
        };
    }];

    /**
     *  Navigation service.
     *  Back to Home button functionality.
     */
    commonServices.navigationService = [
        '$window',
        'modifyReservationService',
        '$state',
        'languageService',
        'currencyService',
        '$rootScope',
        function ($window, modifyReservationService, $state, languageService, currencyService,$rootScope) {
            var self = this;
            var reprotectAlert = false;
            self.createFlowHome = function () {
                //$window.location = 'http://www.airarabia.com';
                $window.location = '/index.html'; //TODO REMOVE THIS IN PRODUCTION
            };

            self.modifyFlowHome = function () {
                modifyReservationService.reloadReservation();
            };

            self.userHome = function () {
                $state.go('modifyDashboard', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency()
                });
            };

            self.navigateTo = function (state) {
              //  console.log('navigate to -', state);
            };

            self.navigateToUrl = function (url) {
                $window.location = url;
            };

            self.navigateBack = function () {
                // TODO: Add business logic to navigate back
            }
            self.onStateChangeSuccess = function(success){
                $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
                    success({ev:ev, to:to, toParams:toParams, from:from, fromParams:fromParams})
                })

            }
            self.setReprotectAlertSatet = function(state){
                reprotectAlert = state
            }
            self.getReprotectAlertSatet = function(){
                return reprotectAlert
            }


        }];
    /*
    * Google Tagmanager Service
     */
    commonServices.GTMService = [function(){
        var dtLayer = [];
        this.setGoogleTagLayer = function(data){
            angular.forEach(data,function(value){
                dtLayer.push(value)
            })
        }
        this.getGoogleTagLayer = function(){
            return dtLayer;
        }
        this.resetGoogleTagLayer = function(){

        }
    }]

    /**
     * Step Progress Service
     */
    commonServices.stepProgressService = [function () {
        var _steps = [
            {id: 'search', title: 'lbl_header_search', status: 'completed'},
            {id: 'fare', title: 'lbl_header_selectFare', status: 'current'},
            {id: 'passenger', title: 'lbl_header_passenger', status: 'pending'},
            {id: 'extras', title: 'lbl_header_extras', status: 'pending'},
            {id: 'payment', title: 'lbl_header_payments', status: 'pending'},
            {id: 'confirm', title: '', status: 'pending'},
            {id: 'voucher', title: 'lbl_header_chooseVoucher', status: 'current'},
            {id: 'custDetail', title: 'lbl_header_customer DetailsAndPayment', status: 'pending'}
        ];

        var listeners = [];

        var notifyListeners = function () {
            angular.forEach(listeners, function (list) {
                list();
            });
        };

        this.registerListeners = function (listener) {
            listeners.push(listener);
        };

        this.getStepByState = function (state) {
            var _return = {};
            for (var i = 0; i < _steps.length; i++) {
                if (_steps[i].id === state) {
                    _return = _steps[i]
                    break;
                }
            }
            return _return
        }

        this.getSelectedStep = function () {
            var _return = {};
            angular.forEach(_steps, function (obj, key) {
                if (obj.status === 'current') {
                    _return = obj;
                }
            });
            return _return;
        };
        this.getAllSteps = function () {
            return _steps;
        };

        this.setCurrentStep = function (id) {
            var currentFound = false;
            angular.forEach(_steps, function (obj, key) {
                if (obj.id === id) {
                    obj.status = 'current';
                    currentFound = true;
                } else {
                    if (currentFound) {
                        obj.status = 'pending';
                    } else {
                        obj.status = 'completed';
                    }

                }
            });
            notifyListeners();
        }
    }];

    commonServices.httpServices = ['$http', function ($http) {
        var getDestination = function (url, searchVal) {
            return $http.get(url, {
                params: {
                    address: searchVal,
                    sensor: false
                }
            }).then(function (response) {
                return response.data.results.map(function (item) {
                    return item.formatted_address;
                });
            });
        };

        var getTimeLineData = function (url, params) {
            return $http.get(url, params)
                .success(function (data) {
                    return data;
                }).error(function (data) {
                    return data;
                });
        };

        var sendHttpRequest = function (method, url, params) {
            return $http({
                method: method,
                url: url,
                data: params
            })
                .success(function (data) {
                    return data;

                }).error(function (data) {
                    return data;
                });
        };
        var getData = function(url,success,fail){
            $http({
                method:"GET",
                url:url
            }).then(function succesCallback (response) {
                    success(response.data);
                },function errorCallback (response) {
                    fail(response.data);
                });
        }


        return {
            getDestination: getDestination,
            getTimeLineData: getTimeLineData,
            sendHttpRequest: sendHttpRequest,
            getData:getData
        };
    }];

    /**
     * Alert Service
     * @param $sce
     */
    commonServices.remoteStorage = [function () {
        var self = this;

        self.AllCabinTypes = [
            'Economy',
            'Business'
        ];
        self.cabins = {
            'Y': 'Economy',
            'C': 'Business'
        };

        self.getAllCabinsTypes = function () {
            return self.AllCabinTypes;
        }

        self.getCabinByKey = function (key) {
            return self.cabins[key] ? self.cabins[key] : undefined;
        };

        self.getCabinKeyByValue = function (value) {
            for (var key in self.cabins) {
                if (self.cabins[key] === value) {
                    return key;
                }
            }
        };

        this.getAirports = function () {
            return (typeof airports != 'undefined') ? airports:[{ code:'',en:''}];

        }

        this.ondDataMap = [];
        this.getToAirports= function(){
            if(typeof origins != 'undefined') {
		    for ( var originIndex=0;originIndex< origins.length;originIndex++) {
		    	if(!airports[originIndex].city) {
			        var tempDestArray = [];
			        for ( var destIndex in origins[originIndex]) {
			            // check whether operating carrier exists
			            var curDest = origins[originIndex][destIndex];
	
			            if(curDest[0] != null){
			                var currentSize = tempDestArray.length;
			                tempDestArray[currentSize] = [];
			                tempDestArray[currentSize] = airports[curDest[0]];
			            }
			        }
			        if (tempDestArray.length > 0) {
			            this.ondDataMap[airports[originIndex]['code']]= tempDestArray;
	
			        }
		    	}
                }
            }

            return this.ondDataMap;// airports.concat(origins);
        }
        
        this.ondDataMapForCityOrigin = [];
        this.getToAirportsForCityOrigin= function(){
            for ( var originIndex=0;originIndex< origins.length;originIndex++) {
            	if(airports[originIndex].city) {
	                var tempDestArray = [];
	                for ( var destIndex in origins[originIndex]) {
	                    // check whether operating carrier exists
	                    var curDest = origins[originIndex][destIndex];
	
	                    if(curDest[0] != null){
	                        var currentSize = tempDestArray.length;
	                        tempDestArray[currentSize] = [];
	                        tempDestArray[currentSize] = airports[curDest[0]];
	                    }
	                }
	                if (tempDestArray.length > 0) {
	                    this.ondDataMapForCityOrigin[airports[originIndex]['code']]= tempDestArray;
	
	                }
            	}
            }

            return this.ondDataMapForCityOrigin;// airports.concat(origins);
        }
        
        this.getAirportByCode = function (code) {
            var tmpAirport;
            if (!!airports) {
                for (var i = 0; i < airports.length; i++) {
                    var obj = airports[i];
                    if (obj.code == code && !obj.city) {
                        tmpAirport = obj;
                        break;
                    }
                }
            }
            return tmpAirport;
        };
    }];

    commonServices.alertService = ['$sce', '$location', 'currencyService', 'languageService', '$translate', function ($sce, $location, currencyService, languageService, $translate) {
        /**
         ** @type danger, warning, info
         */

        var arrAlert = {};
        var pageAlertMessage = "";
        var erroTyp = null;
        
        this.ERRORTYPE = {
            TRANSACTION_ID_ERROR: "TRANSACTION_ID_ERROR",
            OTHER_ERROR: "OTHER_ERROR"
        }

        var listeners = [];

        var notifyListeners = function () {
            angular.forEach(listeners, function (list) {
                list();
            });
        };

        this.setPageAlertMessage = function (errorObj) {
            pageAlertMessage = errorObj.error || "System Error";
            erroTyp = errorObj.errorType || "OTHER_ERROR";
            $location.path('/error/' + languageService.getSelectedLanguage() + '/' + currencyService.getSelectedCurrency());
            return false;
        };

        this.getPageAlertMessage = function () {
            return pageAlertMessage;
        };

        this.setErrorType = function(typ){
            erroTyp = typ;
        };

        this.getErrorType = function(){
            return erroTyp;
        }

        this.registerListeners = function (listener) {
            listeners.push(listener);
        };

        this.setAlert = function (msg, type, fn) {

            if($translate.instant(msg) != msg){
                msg = $translate.instant(msg);
            }

            arrAlert.msg = $sce.trustAsHtml(msg);
            arrAlert.type = type;
            arrAlert.fn = fn;
            notifyListeners();
        };

        this.getAlert = function () {
            return arrAlert;
        }
    }];

    /**
     * Loading Progress
     */
    commonServices.loadingService = ['$rootScope', '$timeout', function ($rootScope, $timeout) {
        var listeners = [], loadingStatus = false;

        var notifyListeners = function () {
            angular.forEach(listeners, function (list) {
                list();
            });
        };

        this.showLoading = function () {
            loadingStatus = true;
            notifyListeners();
        };

        this.hideLoading = function () {
            loadingStatus = false;
            notifyListeners();
        };

        this.registerListeners = function (listener) {
            listeners.push(listener);
        };

        this.getLoadingStatus = function () {
            return loadingStatus;
        };

        this.showPageLoading = function () {
            $rootScope.pageLoaded = false;
            $timeout(function () {
                $rootScope.pageLoaded = true;
            }, 10 * 60 * 1000);
        }

        this.hidePageLoading = function () {
            $rootScope.pageLoaded = true;
            $rootScope.pageTransistion = true;
        }
    }];

    commonServices.datePickerService = [function () {
        var datePicker = {};

        datePicker.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        var dateformats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'dd/MM/yyyy', 'yyyy-MM-dd'];

        datePicker.today = function () {
            return new Date();
        };
        datePicker.clearDate = function (date) {
            date = null;
            return date;
        };
        datePicker.disabled = function (date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        datePicker.toggleMin = function (mindate) {
            return mindate ? null : new Date();
        };
        datePicker.setDate = function (yy, mm, dd) {
            return new Date(yy, mm, dd);

        };
        datePicker.setDateFormat = function (n) {
            return dateformats[n];
        };
        datePicker.open = function ($event) {
            return true;
        };
        return datePicker;

    }];

    /**
     * Footer Quotation
     */
    commonServices.quoteService = ['$timeout', 'commonLocalStorageService', function ($timeout, commonLocalStorageService) {
        var listeners = [], quoteStatus = false, quoteDetails = {}, showSummaryDrawer = true, extraTotal = '0',serviceTaxTotal=0,serviceTaxForAdminFee=0;

        var EXTRA_TOTAL_KEY = "extra-total";
        var SELECTED_ANCI_KEY = "SELECTED_ANCI";

        this.getShowSummaryDrawer = function () {
            return showSummaryDrawer;
        };

        this.setShowSummaryDrawer = function (value) {
            showSummaryDrawer = value;
            notifyListeners();
        };
        
        
        this.setServiceTaxTotal = function (value) {
        	serviceTaxTotal = value;            
        };
        
        this.getServiceTaxTotal = function () {
        	return serviceTaxTotal;            
        };
        
        this.setServiceTaxForAdminFee = function (value) {
        	serviceTaxForAdminFee = value;            
        };
        
        this.getServiceTaxForAdminFee = function () {
        	return serviceTaxForAdminFee;            
        };
        

        var notifyListeners = function () {
            angular.forEach(listeners, function (list) {
                list();
            });
        };
        this.changeShowQuote = function (status) {
            //TODO SummaryDrawer update should not be bind with quoteStatus
            if(!_.isUndefined(status) && status === false){
                quoteStatus = false;
            }else {
                quoteStatus = !quoteStatus;
            }
            //quoteStatus = !quoteStatus;
            notifyListeners();
        };

        this.setShowQuoteStatus = function (status) {
            quoteStatus = status;
            notifyListeners();
        };

        function changeShowOnQuote() {

            quoteStatus = !quoteStatus;
            notifyListeners();
        };

        this.registerListeners = function (listener) {
            listeners.push(listener);
        };

        this.getQuoteStatus = function () {
            return quoteStatus;
        };
        this.setQuoteStatus = function (value) {
            quoteStatus = value;
            notifyListeners();
        };
        this.getExtraTotal = function () {
            return commonLocalStorageService.getData(EXTRA_TOTAL_KEY);
            //return extraTotal;
        };
        this.setExtraTotal = function (value) {
            //extraTotal = value;
            commonLocalStorageService.setData(EXTRA_TOTAL_KEY, value);
            notifyListeners();
        };
        this.setTotalWithAdminFee = function (value) {
            //extraTotal = value;
            commonLocalStorageService.setData('GoogleAnalytics_TotalWithAdminFee', value);
            notifyListeners();
        };
        this.getSelectedAnci = function () {
            return commonLocalStorageService.getData(SELECTED_ANCI_KEY);
        };
        this.setSelectedAnci = function (value) {
            commonLocalStorageService.setData(SELECTED_ANCI_KEY, value);
            notifyListeners();
        };
        var self = this;

    }];
    /**
     * Busy Loader
     */
    commonServices.busyLoaderService = ['$timeout', function ($timeout) {
        var listeners = [], drawerLoaderShow = true;
        this.registerListeners = function (listener) {
            listeners.push(listener);
        };
        var notifyListeners = function () {
            angular.forEach(listeners, function (list) {
                list();
            });
        };
        this.setShowBusyLoader = function () {
            drawerLoaderShow = true;
            notifyListeners();
        };
        this.setHideBusyLoader = function () {
            drawerLoaderShow = false;
            notifyListeners();
        };
        this.setShowLoader = function (value) {
            drawerLoaderShow = value;
            notifyListeners();
        };
        this.getBusyLoaderStatus = function () {
            return drawerLoaderShow
        };
        var self = this;

    }];

    /**
     * Local Storage
     */
    commonServices.commonLocalStorageService = [
        'localStorageService',
        '$location',
        'constantFactory',
        'ancillaryRemoteService',
        'languageService',
        function (localStorageService, $location, constantFactory, ancillaryRemoteService, languageService) {
            var eventListeners = {};
            var listeners = [], _airports = [], _currencies = [], _origins = [], _ond = [];

            var self = this;

            var keys = {
                userSignIn: 'USER_SIGN_IN',
                reservationList : 'RESERVATION_LIST'
            };

            this.EVENTS = {
                OUTBOUND_TRIP_INFO_CHANGED: 'outbound_trip_info_changed',
                SEARCH_CRITERIA: 'search_criteria'
            };

            var notifyListeners = function () {
                angular.forEach(listeners, function (list) {
                    list();
                });
            };

            var notifyEventListeners = function (type) {
                if (eventListeners[type] !== undefined) {
                    angular.forEach(eventListeners[type], function (listener) {
                        listener.call(this);
                    });
                }
            };

            this.registerListener = function (type, listener) {
                if (eventListeners[type] !== undefined) {
                    eventListeners[type].push(listener);
                } else {
                    eventListeners[type] = [listener];
                }
            };

            this.setUserSignInData = function (data) {
                localStorageService.set(keys.userSignIn, data);
            };

            this.getUserSignInData = function (data) {
                return localStorageService.get(keys.userSignIn);
            };

            this.setReservationData =  function(data){
                localStorageService.set(keys.reservationList, data);
            }
            this.getReservationData =  function(data){
                return localStorageService.get(keys.reservationList);
            }

            this.setConfirmedFlightData = function (data) {
                localStorageService.set('CONFIRMED_FLIGHT', data);
            };

            this.getConfirmedFlightData = function () {
                return localStorageService.get('CONFIRMED_FLIGHT');
            };

            //Setting confirmed flights on load
            this.setPreviousConfirmedFlightData = function (data) {
                localStorageService.set('PREV_CONFIRMED_FLIGHT', data);
            };

            this.getPreviousConfirmedFlightData = function () {
                return localStorageService.get('PREV_CONFIRMED_FLIGHT');
            };

            this.getOrderedFlights = function () {
                var confirmedFlights = localStorageService.get('CONFIRMED_FLIGHT');
                var flightList = {};

                var count = 0;

                angular.forEach(confirmedFlights, function(flight, index){
                    flightList[index] = {};
                    flightList[index][count] = flight.flight.flightRPH;

                    angular.forEach(flight.flight.stops, function(stop){
                        count++;
                        flightList[index][count] = stop.flightRPH;
                    })
                });

                var orderedFlightArray = [];
                angular.forEach(flightList, function(flight){
                    angular.forEach(flight, function(segment){
                        orderedFlightArray.push(segment);
                    });
                });

                return orderedFlightArray;
            };

            this.setOutboundTripInfo = function (data) {
                localStorageService.set('OUTBOUND_TRIP_INFO', data);
                notifyEventListeners(self.EVENTS.OUTBOUND_TRIP_INFO_CHANGED);
            };

            this.getOutboundTripInfo = function () {
                return localStorageService.get('OUTBOUND_TRIP_INFO');
            };

            this.setSearchCriteria = function (data) {
                var path = $location.path().split("/")[1];

                if (path === "fare") {
                    //Before search criteria is set, all values are emptied from the localStorage
                    //self.removeAllItems();
                    //TODO need to implement proper methods to clear local storage
                    localStorageService.remove("SEARCH_CRITERIA", "CONFIRMED_FLIGHT", "EXTRAS", "OUTBOUND_TRIP_INFO", "PAX_DETAIL", "INBOUND_TRIP_INFO", "SECTORS", "ANCILLARY",
                    							"QUOTE_DETAILS","ONHOLD_PNR", "SELECTED_ANCI","APP_MODE","PNR","GoogleAnalytics_TotalWithAdminFee", "RES_STATUS",
                    							"SELECTED_PAYMENT_OPTION", "extra-total", "MODIFY.RESERVATION.MODEL", "MODIFY.RESERVATION.UNCHANGED_MODEL", "USER_REGISTRATION_DETAILS",
                    							"MODIFY.NEW.OUTBOUNDFARE", "MODIFY.NEW.INBOUNDFARE","QUOTE");

                    localStorageService.set('SEARCH_CRITERIA', data);
                    ancillaryRemoteService.clearExtras();
                }

                if (path === 'fare' || path === 'modify' || path === 'signIn' || path === 'registration' || path === 'redirect') {
                  //  localStorageService.set('SEARCH_CRITERIA', data);
                	if(!!localStorageService.get('USER_SIGN_IN') && $location.path().split("/")[2] == 'reservation'){
                		localStorageService.remove("SEARCH_CRITERIA","APP_MODE","GoogleAnalytics_TotalWithAdminFee", "SELECTED_PAYMENT_OPTION",
    												"extra-total", "QUOTE_DETAILS", "RES_STATUS","QUOTE");
                	}else{
                		localStorageService.remove("SEARCH_CRITERIA","SELECTED_ANCI","APP_MODE","GoogleAnalytics_TotalWithAdminFee", "SELECTED_PAYMENT_OPTION",
                    							"extra-total", "QUOTE_DETAILS", "RES_STATUS","QUOTE");
                	}
                	if(!$location.path().split("/")[ $location.path().split("/").length-1] === "modify_segment"){
                		localStorageService.remove("PNR", "MODIFY.NEW.OUTBOUNDFARE", "MODIFY.NEW.INBOUNDFARE");
                	}
                	
                    if( $location.path().split("/")[2] == 'reservation'){
                    	localStorageService.set("PNR", $location.path().split("/")[6]);
                    }
                    if(path != 'modify'){
                        localStorageService.remove("PAX_DETAIL");
                    }
                    if(path != 'registration'){
                        localStorageService.remove("USER_REGISTRATION_DETAILS");
                    }
                    if (path === "fare") {                    
                        localStorageService.set('SEARCH_CRITERIA', data);                    
                    }
                }
                if (path === 'extras'){
                	 localStorageService.remove("GoogleAnalytics_TotalWithAdminFee");
                }
                var  carrierCode= constantFactory.AppConfigStringValue('defaultCarrierCode');
                //localStorageService.set('CARRIER_CODE', carrierCode);
                var sc = this.getSearchCriteria();
                sc.lang = data.lang;
                sc.currency = data.currency;
                notifyEventListeners(self.EVENTS.SEARCH_CRITERIA);

            };

            this.setOriginCountry = function(code){
                localStorageService.set('CARRIER_CODE', code);
            };

            this.getSearchCriteria = function () {
                return localStorageService.get('SEARCH_CRITERIA') || {};
            };

            this.setAirports = function (data) {
                _airports = data;
            };

            this.getAirportName = function (code) {
                var code = code || "";

                if (_airports !== undefined) {
                    angular.forEach(_airports, function (obj, key) {
                        if (obj.code.toUpperCase() === code.toUpperCase()) {
                        	if(obj[languageService.getSelectedLanguage().toLowerCase()] !== undefined){
                        		code = obj[languageService.getSelectedLanguage().toLowerCase()];
                        	}else {
                        		code = obj['en'];
                        	}
                        }
                    });
                }
                return code;
            };
            
            this.getAirportCityName = function (code, isCity) {
                var code = code || "";

                if (_airports !== undefined) {
                    angular.forEach(_airports, function (obj, key) {
                        if (obj.code.toUpperCase() === code.toUpperCase() && isCity === obj.city) {
                        	if(obj[languageService.getSelectedLanguage().toLowerCase()] !== undefined){
                        		code = obj[languageService.getSelectedLanguage().toLowerCase()];
                        	}else {
                        		code = obj['en'];
                        	}
                        }
                    });
                }
                return code;
            };

            //metadata > ondPreferences should be set
            this.setOndDetails = function (data) {
                localStorageService.set('OND_DETAILS', data);
            };

            //get Sector details for an ond based on the OndID
            this.getOndDetails = function () {
                return localStorageService.get('OND_DETAILS');
            };

            this.setCurrencies = function (data) {
                _currencies = data;
            };

            var getCurrencyTitleFromOndjs = function (code) {
                if (_currencies !== undefined) {
                    for (var i = 0; i < _currencies.length; i++) {
                        if (code === _currencies.code) {
                            return _currencies[i][languageService.getSelectedLanguage().toLowerCase()];
                            break;
                        } else {
                            return code;
                        }
                    }
                } else {
                    return code;
                }

            };

            this.getCurrencies = function () {
                var toSend = [];
                if (constantFactory.ExchangeRates() !== undefined) {
                    angular.forEach(constantFactory.ExchangeRates(), function (rate, key) {
                        var obj = {};
                        obj.title = getCurrencyTitleFromOndjs(rate.currencyCode);
                        obj.icon = rate.currencyCode.toLowerCase();
                        obj.code = rate.currencyCode;
                        toSend[toSend.length] = obj;
                    });
                }
                return toSend;
            };

            this.setOrigins = function (data) {
                _origins = data;
            };

            this.getOrigins = function () {
                return _origins;
            };

            this.removeAllItems = function () {
                //var lsKeys = localStorageService.keys();
                //console.log('<<=========LOCAL STORAGE KEYS===============>>');
                //console.log(lsKeys)
                localStorageService.clearAll();
                notifyListeners();
            };

            this.setMultiCitySearch = function (data) {
                localStorageService.set('MULTICITY_SEARCH_DATA', data);
                localStorageService.set('SEARCH_CRITERIA', data);
            };

            this.getMultiCitySearch = function () {
                return localStorageService.get('MULTICITY_SEARCH_DATA');
            };

            this.setMultiCitySelectedFlights = function (data) {
                localStorageService.set('MULTICITY_SELECTED_FLIGHTS', data);
            };

            this.getMultiCitySelectedFlights = function () {
                return localStorageService.get('MULTICITY_SELECTED_FLIGHTS');
            };

            this.setData = function (id, data) {
                localStorageService.set(id, data);
            };
            this.getData = function (id) {
                return localStorageService.get(id);
            };

            this.remove = function (key) {
                localStorageService.remove(key);
            };
            
            this.setDefaultCarrierCode = function(code){
                localStorageService.set('DEFAULT_CARRIER_CODE', code);
            };

            this.getConfirmedFlightDataForModExtras = function () {
                return localStorageService.get('CONFIRMED_FLIGHT_MOD_AT_EXTRAS');
            };
            
            this.setUserIpAddress = function(ip){
                localStorageService.set('USER_IP_ADDRESS', ip);
            };

            this.setConfirmedFlightDataDataForModExtras = function (data) {
                localStorageService.set('CONFIRMED_FLIGHT_MOD_AT_EXTRAS', data);
            };
            
            this.setUserRegistrationDetails = function(data){
                localStorageService.set('USER_REGISTRATION_DETAILS', data);
            };

        }];


    /**
     * Application Service Provider
     * - Maintain Application mode
     * @param eventListeners
     */
    commonServices.applicationService = [
        'eventListeners',
        'localStorageService',
        '$injector',
        'constantFactory',
        function (eventListeners, localStorageService, $injector,constantFactory) {
            var preDefinedRoutes = {
                "create": ['fare', 'passenger', 'extras', 'payments', 'confirmReservation', 'search'],
                "modify_segment": ['extras', 'payments', 'confirmReservation'],
                "modifyAnci": ['extras', 'payments', 'confirmReservation'],
                "multicity": ['fare', 'passenger', 'extras', 'payments', 'confirmReservation'],
                "balancePayment": ['payments', 'confirmReservation'],
                "purchaseVoucher": ['voucher', 'custDetail']
            };
            var airlineName = "",applicationConfigs = {};

            this.APP_MODES = {
                "CREATE_RESERVATION": "create",
                "MODIFY_RESERVATION": "modify",
                "CANCEL_RESERVATION": "cancel_reservation",
                "MODIFY_SEGMENT": "modify_segment",
                "ADD_MODIFY_ANCI": "modifyAnci",
                "MULTICITY": "multicity",
                "BALANCE_PAYMENT": "balancePayment",
                "NAME_CHANGE": "nameChange",
                "CONTACT_CHANGE": "changeContactInformation"
            };
            this.ORIGIN_COUNTRY={
                "E5" : "EG",
                "G9" : "AE",
                "9P" :"JO",
                "3O" : "MA",
                "C7" : "LK",
                "W5" : "IR",
                "Q9" : "KW",
                "QD" : "KH",
                "RM" : "AM",
                "6Q" : "SY",
                "RI" : "LY",
                "VP" : "MV"
            };
            var AIRLINE_CAREER_CODES={
                'AAA':['G9','E5','9P','3O'],
                'MHA' :['W5'],
                'ARM' : ['RM'],
                'JCA' : ['QD'],
                'SAW' : ['6Q'],
                'WAN' : ['Q9'],
                'RI'  : ['RI'],
                'VP'  : ['VP']
            };
            var loginStatusKey = "CUSTOMER_LOGIN_STATE";
            var transactionID = null;
            var customerLoginState = false;
            var self = this;
            var applicationMode = this.APP_MODES.CREATE_RESERVATION;
            var currentState;
            var fareRules = [];
            var isPersianCalendarType = (constantFactory.AppConfigStringValue('enablePersianCalendar') === 'true');

            this.setApplicationMode = function (mode) {
                if (!!mode) {
                    applicationMode = mode;
                    eventListeners.notifyListeners(eventListeners.events.APPLICATION_MODE_CHANGE);
                }
            };
            this.setCareerCodes = function(airline){
                if(AIRLINE_CAREER_CODES[airline.Id] != undefined) {
                    AIRLINE_CAREER_CODES[airline.Id] = airline.carriers;
                }else{
                    AIRLINE_CAREER_CODES[airline.Id] = [];
                    AIRLINE_CAREER_CODES[airline.Id] = airline.carriers;
                }
            }
            this.isValidCareerFor = function(airline){
                return _.contains(AIRLINE_CAREER_CODES[airline],this.getCarrerCode());
            }
            /*
            // @param airlineName : Name of the Airline
             */
            this.setAirLine = function(airlineName){
                airlineName = airlineName
            }
            this.getairline = function(){
                return airlineName;
            }
            // Set in onStateChange
            this.setCurrentState = function (state) {
                currentState = state;
            };
            this.setApplicationConfigs = function(configs){
                applicationConfigs = configs;
            }
            this.getApplicationConfigs = function(){
                return applicationConfigs;
            }

            this.getCurrentState = function () {
                return currentState;
            };

            this.setTransactionID = function (id) {
                transactionID = id;
                localStorageService.set('TRANSACTION_ID', id);
            };

            this.getTransactionID = function () {
                if (!!transactionID) {
                    return transactionID;
                } else {
                    return localStorageService.get('TRANSACTION_ID') || null;
                    return null
                }
            };

            this.getApplicationRoute = function () {
                return preDefinedRoutes[applicationMode];
            }

            this.getApplicationMode = function () {
                return applicationMode;
            };

            this.setCustomerLoggedState = function (isLoggedIn) {
                localStorageService.set(loginStatusKey, isLoggedIn);
                eventListeners.notifyListeners(eventListeners.events.CUSTOMER_LOGIN_STATUS_CHANGE);
            };

            this.getCustomerLoggedState = function () {
                return localStorageService.get(loginStatusKey) ? localStorageService.get(loginStatusKey) : false;
            };

            this.getFlightDirectionFromRph = function (flightRph) {
                var localStorage = $injector.get('commonLocalStorageService');
                var confirmedFlights = localStorage.getConfirmedFlightData();

                if(confirmedFlights[1]){
                    var count = 0, flightList = [];
                    var flight = confirmedFlights[1];

                    flightList[count] = flight.flight.flightRPH;
                    angular.forEach(flight.flight.stops, function(stop){
                        count++;
                        flightList[count] = stop.flightRPH;
                    });

                    if(flightList.indexOf(flightRph) > -1){
                        return "inbound"
                    }
                }

                return "outbound"
            };

            /**
             * Gets the flight direction by using the from and to airport codes of the given airports
             * @toAirportCode: The airport from which the plane is departing from
             * @fromAirportCode: The destination airport code
             * */
            this.getFlightDirection = function (toAirportCode, fromAirportCode,isModify) {
                var localStorage = $injector.get('commonLocalStorageService');
                if(isModify){
                    var confirmedFlights = localStorage.getConfirmedFlightData();
                    if(confirmedFlights[1] === undefined){
                        confirmedFlights = localStorage.getConfirmedFlightDataForModExtras();
                       if(confirmedFlights[0].changed === '1' || confirmedFlights[1].changed === '1'){
                           if(toAirportCode === confirmedFlights[0].flight.destinationCode || fromAirportCode ===  confirmedFlights[0].flight.originCode)
                        return 'outbound';
                        else if(confirmedFlights[1]!== undefined && (toAirportCode === confirmedFlights[1].flight.destinationCode || fromAirportCode ===  confirmedFlights[1].flight.originCode))
                            return 'inbound';
                        else
                            return 'outbound'; 
                       }else if(confirmedFlights[0].changed === '2'  || confirmedFlights[1].changed === '2'){
                           if(toAirportCode === confirmedFlights[0].flight.destinationCode || fromAirportCode ===  confirmedFlights[0].flight.originCode)
                        return 'inbound';
                        else if(confirmedFlights[1]!== undefined && (toAirportCode === confirmedFlights[1].flight.destinationCode || fromAirportCode ===  confirmedFlights[1].flight.originCode))
                            return 'outbound';
                        else
                            return 'outbound'; 
                       }else{
                              for(var i=0; i<Object.keys(confirmedFlights).length; i++){
                            if(toAirportCode === confirmedFlights[i].flight.originCode || fromAirportCode ===  confirmedFlights[i].flight.destinationCode)
                                  return 'outbound';
                            else if(toAirportCode === confirmedFlights[i].flight.destinationCode || fromAirportCode ===  confirmedFlights[i].flight.originCode)
                                return 'inbound';
                            else
                                return "inbound"; 
                            }
                       }
                    }else{
                        for(var i=0; i<Object.keys(confirmedFlights).length; i++){
                            if(toAirportCode === confirmedFlights[i].flight.originCode || fromAirportCode ===  confirmedFlights[i].flight.destinationCode)
                                  return 'outbound';
                            else if(toAirportCode === confirmedFlights[i].flight.destinationCode || fromAirportCode ===  confirmedFlights[i].flight.originCode)
                                return 'inbound';
                            else
                                return "inbound"; 
                            }
                        }
                        
                    
                }else{
                    var searchCriteria = localStorage.getSearchCriteria();

			        var destCityAirports = searchCriteria.destCityAirports || [];
			        var originCityAirports = searchCriteria.originCityAirports || [];
			        
			        
			        if ((toAirportCode === searchCriteria.to && searchCriteria.destCity === 'N')
			        		|| (searchCriteria.destCity === 'Y' && destCityAirports.includes(toAirportCode)) 
			        		|| (fromAirportCode === searchCriteria.from && searchCriteria.originCity === 'N')
			        		|| (searchCriteria.originCity === 'Y' && originCityAirports.includes(fromAirportCode)) ) {
			        	return "inbound"
			        } else if ((toAirportCode === searchCriteria.from  && searchCriteria.originCity === 'N')
			        		|| (searchCriteria.originCity === 'Y' && originCityAirports.includes(toAirportCode)) 
			        		|| (fromAirportCode === searchCriteria.to && searchCriteria.destCity === 'N')
			        		|| (searchCriteria.destCity === 'Y' && destCityAirports.includes(fromAirportCode)) ) {
			                return "outbound"
			        } else {
				        if (toAirportCode === searchCriteria.to || fromAirportCode === searchCriteria.from) {
				            return "inbound"
				        } else if (toAirportCode === searchCriteria.from || fromAirportCode === searchCriteria.to) {
				            return "outbound"
				        } else {
				            return null;
				        }                    
			        }
                }
            }
            
            this.getOriginCountry = function(code){
               return this.ORIGIN_COUNTRY[code];

            }
            this.getCarrerCode = function(){
                return constantFactory.AppConfigStringValue('defaultCarrierCode')
            }
            this.setFareRules = function(farerule){
                fareRules = farerule;
            }
            this.getFareRules = function(farerule){
                return fareRules;
            }
            this.getIsPersianCalendar= function(){
                return isPersianCalendarType;
            }
        }];


    /**
     * Currency service
     */
    commonServices.currencyService = ['localStorageService', 'constantFactory',
        function (localStorageService, constantFactory) {
        var currencyTable = null,
            app_currency = "AED",
            base_currency = constantFactory.AppConfigStringValue('baseCurrency'),
            dpg_currency = constantFactory.AppConfigStringValue('defaultPGCurrency'),
            isLoyaltyManagementEnabled = JSON.parse(constantFactory.AppConfigStringValue('enableLoyaltyManagement'));

        if(base_currency) {
            app_currency = base_currency;
        }

        this.setSelectedCurrency = function (currCode) {
            app_currency = currCode;
        };

        this.getSelectedCurrency = function () {
            return app_currency;
        };

        this.setCurrencyTable = function (cTable) {
            currencyTable = cTable;
        };

        this.getSelectedCurrencyBaseValue = function (tempCurrency) {
            var toSend = "NotFound";
            var currencyCode = tempCurrency || app_currency;
            angular.forEach(currencyTable, function (obj, key) {
                if (obj['currencyCode'].toUpperCase() === currencyCode) {
                    toSend = obj['baseToCurrExRate'];
                }
            });
            if (toSend === "NotFound") {
                toSend = 1;
            }
            return toSend;
        };

        this.setAppBaseCurrency = function (baseCurrency) {
            base_currency = baseCurrency;
        };
        this.setAppDefaultPGCurrency = function (pgCurrency) {
            dpg_currency = pgCurrency;
        };
        this.getAppDefaultPGCurrency = function (pgCurrency) {
            return dpg_currency;
        };
        this.getLoyaltyManagementStatus = function(){
            return isLoyaltyManagementEnabled;
        }

        this.getAppBaseCurrency = function(){
            return base_currency;
        }

        this.getCurrencyList = function () {
            return (typeof currencies != 'undefined') ? currencies: [{ code:'USD',en:'US Dollar'}] ; // this global variable is defined in ond.js
        }

        // Get currency object using currencyCode.
        this.getSelectedCurrencyObject = function(currencyCode) {
            var currencyObject;
            for (var i = 0; i < self.currencies.length; i++) {
                if (self.currencies[i].code === currencyCode) {
                    currencyObject = self.currencies[i];
                    break;
                }
            }
            return currencyObject;
        }

        this.getSelectedCurrencyInfo = function(currencyCode) {
            var currencyCode = currencyCode || app_currency;
            return _.find(currencyTable, function (obj) {
                return obj['currencyCode'].toUpperCase() === currencyCode;
            });
        }

    }];


    /**
     * Language service
     */
    commonServices.languageService = ['localStorageService', function (localStorageService) {
        var _lang = null, _page = null, labelsBkt = {};

        this.setSelectedLanguage = function (langCode) {
            localStorageService.set("APP_LANGUAGE", langCode);
            _lang = langCode;
        };

        this.setSelectedPage = function (page) {
            _page = page;
        };

        this.getSelectedPage = function () {
            return _page;
        };

        this.getSelectedLanguage = function () {
            if (_lang === null) {
                _lang = localStorageService.get("APP_LANGUAGE");
            }
            return _lang;
        };

    }];
//Scroll Service
    commonServices.scrollService = ['localStorageService', function (localStorageService) {
         this.scrollTo = function(eID) {
        
        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for ( var i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (this.pageYOffset) return this.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }

    };

    }];

    commonServices.IBEDataConsole = ['FrontendProps', function (FrontendProps) {
        this.log = function (param) {
            if (FrontendProps.devMode) {
                console.log(param);
            }
        }
    }];


    commonServices.captchaService = ['$http', 'ConnectionProps', function($http, ConnectionProps) {

       this.validateCaptcha = function (captchaParam, successFn, failFn, errorFn) {
            var path = "/captcha/verify";
            $http({
                "method": 'POST',
                "url": ConnectionProps.host + path,
                "data": captchaParam
            }).success(function (response) {
                if (response.success && response.captchaTrue) {
                    successFn(response);
                } else {
                    failFn(response);
                }
            }).error(function (response) {
                errorFn(response);
            });
        };

       this.getNewCaptcha = function getNewCaptcha() {
            return "jcaptcha.jpg?p=" + new Date().getTime();
        };
    }];
    
    
    commonServices.signoutServiceCheck = ['$http', 'applicationService', '$window','$state','languageService','currencyService','signInService','alertService',function($http, applicationService,$window,$state,languageService,currencyService,signInService,alertService) {
    	this.checkSignoutOperation = function(){
	    	 if (!applicationService.getCustomerLoggedState() &&  applicationService.getApplicationMode() !== 'create' 
	         	&&  ! $window.localStorage.getItem('PNR_LOGIN')) {
	             $state.go('signIn', {
	                lang: languageService.getSelectedLanguage(),
	                currency: currencyService.getSelectedCurrency(),
	                mode: applicationService.getApplicationMode()
	            });
	      }
    	}
    	
    	this.checkSessionExpiry = function(){
    		var userData = signInService.getUserDataFromLocalStorage();
            var loginId = _.isEmpty(userData) ? undefined : userData.loggedInCustomerDetails.emailId;

            if (loginId) {
                signInService.isServerHasLoggedInUser({ loginId: loginId }, function (data) {
                    if (!data.customerLoggedIn && data.success) {
                        signInService.signoutUser({},function () {
                            alertService.setAlert("Your session has expired.");
                            $state.go('signIn', {
            	                lang: languageService.getSelectedLanguage(),
            	                currency: currencyService.getSelectedCurrency(),
            	                mode: applicationService.getApplicationMode()
            	            });
                        }, function () {});
                    }
                }, function () {
                });
            }else{
            	if(!$window.localStorage.getItem('PNR_LOGIN'))
            	 $state.go('signIn', {
                     lang: languageService.getSelectedLanguage(),
                     currency: currencyService.getSelectedCurrency()
                 });
            }
    	}
     
     }];
    
    
    
    
    
    
    
    //this is for persian to english service
    commonServices.dateTransformationService =[function () {
                /*
            Converts a Gregorian date to Jalaali.
          */
         this.toJalaali= function(gy, gm, gd) {
            return (this.d2j(this.g2d(gy, gm, gd)))
          }

          /*
            Converts a Jalaali date to Gregorian.
          */
          this.toGregorian = function (jy, jm, jd) {
            return this.d2g(this.j2d(jy, jm, jd))
          }

          /*
            Checks whether a Jalaali date is valid or not.
          */
          this.isValidJalaaliDate = function (jy, jm, jd) {
            return  jy >= -61 && jy <= 3177 &&
                    jm >= 1 && jm <= 12 &&
                    jd >= 1 && jd <= this.jalaaliMonthLength(jy, jm)
          }

          /*
            Is this a leap year or not?
          */
         this.isLeapJalaaliYear =  function (jy) {
            return this.jalCal(jy).leap === 0
          }

          /*
            Number of days in a given month in a Jalaali year.
          */
        this.jalaaliMonthLength =   function (jy, jm) {
            if (jm <= 6) return 31
            if (jm <= 11) return 30
            if (this.isLeapJalaaliYear(jy)) return 30
            return 29
          }

          /*
            This function determines if the Jalaali (Persian) year is
            leap (366-day long) or is the common year (365 days), and
            finds the day in March (Gregorian calendar) of the first
            day of the Jalaali year (jy).

            @param jy Jalaali calendar year (-61 to 3177)
            @return
              leap: number of years since the last leap year (0 to 4)
              gy: Gregorian year of the beginning of Jalaali year
              march: the March day of Farvardin the 1st (1st day of jy)
            @see: http://www.astro.uni.torun.pl/~kb/Papers/EMP/PersianC-EMP.htm
            @see: http://www.fourmilab.ch/documents/calendar/
          */
          this.jalCal = function (jy) {
            // Jalaali years starting the 33-year rule.
            var breaks =  [ -61, 9, 38, 199, 426, 686, 756, 818, 1111, 1181, 1210
                          , 1635, 2060, 2097, 2192, 2262, 2324, 2394, 2456, 3178
                          ]
              , bl = breaks.length
              , gy = jy + 621
              , leapJ = -14
              , jp = breaks[0]
              , jm
              , jump
              , leap
              , leapG
              , march
              , n
              , i

            if (jy < jp || jy >= breaks[bl - 1])
              throw new Error('Invalid Jalaali year ' + jy)

            // Find the limiting years for the Jalaali year jy.
            for (i = 1; i < bl; i += 1) {
              jm = breaks[i]
              jump = jm - jp
              if (jy < jm)
                break
              leapJ = leapJ + this.div(jump, 33) * 8 + this.div(this.mod(jump, 33), 4)
              jp = jm
            }
            n = jy - jp

            // Find the number of leap years from AD 621 to the beginning
            // of the current Jalaali year in the Persian calendar.
            leapJ = leapJ + this.div(n, 33) * 8 + this.div(this.mod(n, 33) + 3, 4)
            if (this.mod(jump, 33) === 4 && jump - n === 4)
              leapJ += 1

            // And the same in the Gregorian calendar (until the year gy).
            leapG = this.div(gy, 4) - this.div((this.div(gy, 100) + 1) * 3, 4) - 150

            // Determine the Gregorian date of Farvardin the 1st.
            march = 20 + leapJ - leapG

            // Find how many years have passed since the last leap year.
            if (jump - n < 6)
              n = n - jump + this.div(jump + 4, 33) * 33
            leap = this.mod(this.mod(n + 1, 33) - 1, 4)
            if (leap === -1) {
              leap = 4
            }

            return  { leap: leap
                    , gy: gy
                    , march: march
                    }
          }

          /*
            Converts a date of the Jalaali calendar to the Julian Day number.

            @param jy Jalaali year (1 to 3100)
            @param jm Jalaali month (1 to 12)
            @param jd Jalaali day (1 to 29/31)
            @return Julian Day number
          */
          this.j2d = function (jy, jm, jd) {
            var r = this.jalCal(jy)
            return this.g2d(r.gy, 3, r.march) + (jm - 1) * 31 - this.div(jm, 7) * (jm - 7) + jd - 1
          }

          /*
            Converts the Julian Day number to a date in the Jalaali calendar.

            @param jdn Julian Day number
            @return
              jy: Jalaali year (1 to 3100)
              jm: Jalaali month (1 to 12)
              jd: Jalaali day (1 to 29/31)
          */
          this.d2j = function (jdn) {
            var gy = this.d2g(jdn).gy // Calculate Gregorian year (gy).
              , jy = gy - 621
              , r = this.jalCal(jy)
              , jdn1f = this.g2d(gy, 3, r.march)
              , jd
              , jm
              , k

            // Find number of days that passed since 1 Farvardin.
            k = jdn - jdn1f
            if (k >= 0) {
              if (k <= 185) {
                // The first 6 months.
                jm = 1 + this.div(k, 31)
                jd = this.mod(k, 31) + 1
                 //alert("jy:"+jy+"jm:"+jm+"jd:"+jd);
                return  { jy: jy
                        , jm: jm
                        , jd: jd
                        }
              } else {
                // The remaining months.
                k -= 186
              }
            } else {
              // Previous Jalaali year.
              jy -= 1
              k += 179
              if (r.leap === 1)
                k += 1
            }
            jm = 7 + this.div(k, 30)
            jd = this.mod(k, 30) + 1
//            debugger;
//            alert("jy:"+jy+"jm:"+jm+"jd:"+jd);
            return  { jy: jy
                    , jm: jm
                    , jd: jd
                    }
          }

          /*
            Calculates the Julian Day number from Gregorian or Julian
            calendar dates. This integer number corresponds to the noon of
            the date (i.e. 12 hours of Universal Time).
            The procedure was tested to be good since 1 March, -100100 (of both
            calendars) up to a few million years into the future.

            @param gy Calendar year (years BC numbered 0, -1, -2, ...)
            @param gm Calendar month (1 to 12)
            @param gd Calendar day of the month (1 to 28/29/30/31)
            @return Julian Day number
          */
          this.g2d = function (gy, gm, gd) {
            var d = this.div((gy + this.div(gm - 8, 6) + 100100) * 1461, 4)
                + this.div(153 * this.mod(gm + 9, 12) + 2, 5)
                + gd - 34840408
            d = d - this.div(this.div(gy + 100100 + this.div(gm - 8, 6), 100) * 3, 4) + 752
            //alert("in g2d"+d)
            return d
          }

          /*
            Calculates Gregorian and Julian calendar dates from the Julian Day number
            (jdn) for the period since jdn=-34839655 (i.e. the year -100100 of both
            calendars) to some millions years ahead of the present.

            @param jdn Julian Day number
            @return
              gy: Calendar year (years BC numbered 0, -1, -2, ...)
              gm: Calendar month (1 to 12)
              gd: Calendar day of the month M (1 to 28/29/30/31)
          */
          this.d2g = function (jdn) {
            var j
              , i
              , gd
              , gm
              , gy
            j = 4 * jdn + 139361631
            j = j + this.div(this.div(4 * jdn + 183187720, 146097) * 3, 4) * 4 - 3908
            i = this.div(this.mod(j, 1461), 4) * 5 + 308
            gd = this.div(this.mod(i, 153), 5) + 1
            gm = this.mod(this.div(i, 153), 12) + 1
            gy = this.div(j, 1461) - 100100 + this.div(8 - gm, 6)
            return  { gy: gy
                    , gm: gm
                    , gd: gd
                    }
          }

          /*
            Utility helper functions.
          */

         this.div = function (a, b) {
            return ~~(a / b)
          }

          this.mod = function (a, b) {
            return a - ~~(a / b) * b
          }
    }]
    commonServices.PersianDateFormatService = [function () {
    	this.reformatJalaaliMonthStringFromEnglish = function (ds) {
     	   var months = {1:"فروردین",2:"اردیبهشت",3:"خرداد",4:"تیر",5:"مرداد",6:"شهریور",7:"مهر",8:"آبان",9:"آذر",10:"دی",11:"بهمن",12:"اسفند"};
     	  return  months[ds];
     	}
    	
     this.reformatJalaaliDayStringFromEnglish =function (ds) {
    	 var day = {monday:"دوشنبه",tuesday:"سه شنبه",wednesday:"چهارشنبه",thursday:"پنجشنبه",friday:"جمعه",saturday:"شنبه",sunday:"یکشنبه"};
     	  return  day[ds];
     	}
     this.reformatDateString = function (ds,withTime) {
  	   var months = {jan:'01',feb:'02',mar:'03',apr:'04',may:'05',jun:'06',
  	                 jul:'07',aug:'08',sep:'09',oct:'10',nov:'11',dec:'12'};
  	  var b = ds.split(' ');
  	  if(withTime)
  		  return b[2] + '-' + months[b[1].toLowerCase()] + '-' + b[0]+'-'+b[3];
  	  else
  		  return b[2] + '-' + months[b[1].toLowerCase()] + '-' + b[0];
  	}
    }];

    return commonServices;
};
