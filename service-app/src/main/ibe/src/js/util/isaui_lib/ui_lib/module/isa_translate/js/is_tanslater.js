/**
 * Created by indika on 10/18/16.
 */
'use strict';
(function () {
    var isa_translate = angular.module("isaTranslate", []);
    var directives = {};

    directives.isaTranslate = ['$translate', function ($translate) {
        return {
            restrict: 'A',
            replace: false,
            terminal: true,
            priority: 10000, //this setting is important to make sure it executes before other directives
            scope:{
                key:'@'
            },


            link: function (scope, element, attrs) {
                $translate(scope.key).then(function (msg) {
                    if (msg != scope.key) {
                        element.html(msg);
                    } else {
                        element.css({'display':'none'});
                    }
                })

            }
        }
    }]
    isa_translate.directive(directives);
})()
