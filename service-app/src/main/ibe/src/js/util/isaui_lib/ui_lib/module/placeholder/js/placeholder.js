/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function(){
    var isa_placeholder = angular.module("isaplaceholder", []);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaplaceholder = [
        '$compile', '$http','$templateCache','$timeout',
        function ($compile, $http,$templateCache,$timeout) {
        var directive = {};
        //load relavent tamplate according to the button type

        directive.restrict = 'AEC';
        directive.scope = {placeholderCaption: '@',
                           placeholderSymbol:'@',
                           isVisible:'=',
                           isInputDisabled:'=',
                           onChange:'=',
                           bindTo:'@'};
        directive.require = //other sibling directives;
        directive.link = function (scope, element, attr) {
            scope.onChange = "";
            scope.modelVal = null;
            //var template = isaconfig.getTemplates("isaplaceholder");
            var template = $templateCache.get(isaconfig.getTemplates("isaplaceholder")['default']);

            var returnNgModel = function(title, scopeObj){
                var ttArry = title.split("."), valueToSend = scopeObj;
                for (var i=0;i<ttArry.length;i++){
                    valueToSend = valueToSend[ttArry[i]];
                }
                return valueToSend || null;
            };

            template = angular.element(template);
            element.append(template);
            $compile(template)(scope);

            var modalTitle = element.find('input').attr("ng-model");
            var inputScope = angular.element(element.find('input')).scope();
            scope.modelVal = returnNgModel(modalTitle, inputScope);
            if(scope.modelVal!==null){
                $(element).find('.placeholder').hide();
            }

            /*$templateRequest(template['default'],[false]).then(function(html){
                var returnNgModel = function(title, scopeObj){
                    var ttArry = title.split("."), valueToSend = scopeObj;
                    for (var i=0;i<ttArry.length;i++){
                        valueToSend = valueToSend[ttArry[i]]
                    }
                    return valueToSend || null;
                }

                 var template = angular.element(html);
                 element.append(template);
                 $compile(template)(scope);
                var modalTitle = element.find('input').attr("ng-model");
                var inputScope = angular.element(element.find('input')).scope();
                scope.modelVal = returnNgModel(modalTitle, inputScope);
                    if(scope.modelVal!==null){
                        $(element).find('.placeholder').hide();
                    }
            });*/

            scope.removePlaceholder = function(){
                if(scope.isInputDisabled) {
                    scope.isVisible = false;
                    element.find('input').focus();
                    $(element).find('.placeholder').hide();
                }
            };
            element.find('input').on('blur', function(e) {
                if((element.find('input').val().length === 0) ){
                    scope.isVisible = true;
                    $(element).find('.placeholder').show();
                    $('[bind-to = '+scope.bindTo+']').find('.placeholder').show();
                }
            });
            element.find('input').on('change paste keyup focus', function(e) {
                scope.isVisible = false;
                $(element).find('.placeholder').hide();
                $('[bind-to = '+scope.bindTo+']').find('.placeholder').hide();
            });


        };
        return directive;

    }];

    isa_placeholder.directive('compileCallback',function(){
        return {
            priority: 1001,
            scope : {
                compileCallback: "&"
            },
            link: function (scope){
                scope.compileCallback();
            }
        };
    });

    isa_placeholder.controller('phController',[
        '$scope', '$element',
        function($scope,$element){
        $scope.cmpileDone = function(){
//            console.log('cmopile done---------------------------->>>');
        };
    }]);
    isa_placeholder.directive(directives);
})();

