/**
 * Created by baladewa on 11/13/15.
 */
'use strict';

(function () {
    var isa_flight_select = angular.module("isaFlightSelect", []);
    var directives = {};
    var fareTooltip = {};
    var selectedOptions = {};

    var isaconfig = new IsaModuleConfig();
    directives.isaFlightSelect = [
        '$timeout', '$templateCache', '$compile', '$sce', '$rootScope','constantFactory','$window',
        function ($timeout, $templateCache, $compile, $sce, $rootScope,constantFactory,$window) {
            var directive = {};
            directive.restrict = 'AE';
            directive.scope = {
                headerInfo: "=",
                flightOptions: "=",
                currency: "@",
                onSelectFare: "&",
                onSelectFareNoPopup: '&',
                onConfirm: "&",
                isPopupVisible: '=',
                popupButtonCaption: '=',
                isBusyLoaderVisible: '=',
                templateHeader: '@',
                templateBody: '@',
                templatePopup: '@',
                savingsAvailable: '=',
                segmentId:'=',
                loadingMsg:"@",
                blockFare:'=',
                onEditFlight: "&",
                flightHeightList: "=",
                selectedSegment: "=",
                airports:"=",
                airlineMap: '=',
                showFareBundleDescription: '=',
                outerIndex:"=",
                isCreateFlow:"="
            };
            directive.link = function postLink(scope, element, attr) {

                scope.bodyTemplateVersion = constantFactory.AppConfigStringValue('bundleFareDescriptionTemplateVersion') || 'v1';

                if (!attr['templateHeader']) {
                    scope.templateHeader = 'default_header'
                }

                if (!attr['templateBody']) {
                    scope.templateBody = 'default_body'
                }

                if (!attr['templatePopup']) {
                    scope.templatePopup = 'default_popup'
                }

                // Logic has been changed to make header is not shown for v2
                // Needs to revist whether there is a better way to handle this
                if (!attr['isHeaderVisible']) {
                    scope.isHeaderVisible = true && scope.bodyTemplateVersion != 'v2';
                }
                scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');
                scope.fTypeWidth = {};
                scope.headerFType = !_.isEmpty(scope.headerInfo) ? scope.headerInfo : [];
                scope.bodyFType = [];
                scope.blockSection = false;
                scope.applyMask = {};
                scope.selectedFareItems = {};
                scope.baseAmount = 1;
                scope.isContinuClicked = false;
                scope.mobileDevice = $rootScope.deviceType.isMobileDevice;
                scope.largeDevice = $rootScope.deviceType.isLargeDevice;
                scope.smallDevice= $rootScope.deviceType.isSmallDevice;
                scope.isCreateFlow = scope.isCreateFlow || false;
                scope.isThreeTwentyDevice = $window.matchMedia("only screen and (max-width: 320px)").matches;

                scope.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');

                var templetes = {};

                if (scope.isHeaderVisible) {
                    templetes.header = {
                        url: isaconfig.getTemplates("isaFlightSelect")[scope.templateHeader],
                        html: ""
                    }
                }

                templetes.body = {
                    url: isaconfig.getTemplates("isaFlightSelect")[scope.bodyTemplateVersion][scope.templateBody],
                    html: ""
                };
                templetes.popup = {
                    url: isaconfig.getTemplates("isaFlightSelect")[scope.templatePopup],
                    html: ""
                };

                angular.forEach(templetes, function (template, key) {
                    template.html = angular.element($templateCache.get(template.url));
                    element.append(template.html);
                    $compile(template.html)(scope);

                    /*$templateRequest(template.url).then(function(html){
                     template.html = angular.element(html);
                     element.append(template.html);
                     $compile(template.html)(scope);
                     });*/
                });

                var farecomparator = function (a, b) {
                    return parseFloat(a.fareTypeId) - parseFloat(b.fareTypeId);
                };

                scope.setSavingsAvailable = function(availability){
                    scope.savingsAvailable[scope.segmentId] = availability;
                };

                scope.$watch("flightHeightList", function(newVal, oldVal){
                    if(scope.flightHeightList && _.isNumber(scope.selectedSegment)){
                        scope.beforeHeight = 0;
                        scope.afterHeight = 0;

                        for(var i = 0; i < scope.selectedSegment ; i++){
                            scope.beforeHeight += scope.flightHeightList[i];
                        }

                        for(var j = scope.selectedSegment + 1; j <= scope.flightHeightList.length - 1 ; j++){
                            scope.afterHeight += scope.flightHeightList[j];
                        }
                    }
                }, true);

                scope.$watch("headerInfo", function (newVal, oldVal) {
                    if (newVal !== oldVal && !!newVal) {
                        //newVal.sort(farecomparator);
                        scope.headerFType = newVal;
                    }
                });

                scope.setDurationOfWidth = function (stops) {
                    var dev = stops.length + 1;
                    var temp = 91 / dev;
                    var temp = parseInt(temp);
                    temp = temp - 3;
                    return {"margin-right": temp + "%"};
                };

                scope.$watch("isPopupVisible", function (newVal, oldVal) {
                    scope.closePopup();
                    scope.isContinuClicked = true;
                });
                scope.$watch("flightOptions", function (newVal, oldVal) {
                    if (newVal !== oldVal && newVal !== undefined) {
                        clearPreviousSelection();

                        _.forEach(scope.flightOptions, function (flight) {
                            flight.isFlightSelected = false;
                        });

                        angular.forEach(newVal, function (obj) {
                            if (obj !== undefined) {
                                if(scope.airlineMap && scope.airlineMap[obj.careerCode]) {
                                    obj.displayCarrier = scope.airlineMap[obj.careerCode]
                                } else {
                                    obj.displayCarrier =  obj.careerCode;
                                }

                                //obj.flightFaresAvailability.sort(farecomparator);
                                angular.forEach(obj.flightFaresAvailability, function (fare) {
                                // fare.isFlightSelect = false;
                                });
                                scope.bodyFType = obj.flightFaresAvailability;
                                if (scope.isHeaderVisible) {
                                    missMatchLength();
                                }
                            }
                        });
                    }
                });

                //TODO check this
                scope.selectedFarePopup = function(index){
                    
                    if(Object.keys(selectedOptions).length) {
                        if(selectedOptions[scope.segmentId] != undefined) {
                            var pos = {};
                            pos.left = selectedOptions[scope.segmentId].position.left;
                            pos.top = selectedOptions[scope.segmentId].position.top;
                            scope.selectedFareItems = selectedOptions[scope.segmentId].selectedFlightFare;
                            showPopup(selectedOptions[scope.segmentId].currentDateOption);
                            scope.onSelectFare(selectedOptions[scope.segmentId].passToEvent);
                        }
                    }
                };

                scope.selectFlight = function (selectedFlight) {
                    if (selectedFlight && selectedFlight.isFlightSelected) {
                        selectedFlight.isFlightSelected = false;
                        return;
                    }

                    _.forEach(scope.flightOptions, function (flight) {
                        flight.isFlightSelected = false;
                    });

                    if (selectedFlight && (!scope.isCreateFlow || !selectedFlight.isFlightFull)) {
                        selectedFlight.isFlightSelected = true;
                    }
                };

                scope.getMinimumFare = function (flight) {
                    var minimum;
                    _.forEach(flight.flightFaresAvailability, function (fare) {
                        if (!minimum) {
                            minimum = fare.fareValue;
                        } else {
                        	if (fare.fareValue != "null"){
                        		minimum = Math.min(minimum, fare.fareValue);
                        	}
                        }
                    });
                    return minimum;
                };

                scope.selectThisFare = function (event, flight, fare, parentId) {
                    selectedOptions[scope.segmentId] = {}
                    selectedOptions[scope.segmentId].position = {}
                    selectedOptions[scope.segmentId].selectedFlightFare = {};
                    selectedOptions[scope.segmentId].currentDateOption = {};
                    selectedOptions[scope.segmentId].flightFares = {};
                    selectedOptions[scope.segmentId].flightFares = flight && flight.flightFaresAvailability;
                    var pos = {};
                    pos.left = selectedOptions[scope.segmentId].position.left = angular.element(event.currentTarget.parentNode)[0].offsetLeft;
                    pos.top = selectedOptions[scope.segmentId].position.top=  angular.element(event.currentTarget.parentNode)[0].offsetTop;
                    clearPreviousSelection();
                    fare.isFlightSelect = true;
                    var currentDateOption = {
                        position: pos,
                        selectedFlight: flight,
                        selectedFare: fare
                    };
                    var selectedFlightFare = {
                        flightId: flight.flightId,
                        flightRPH: flight.flightRPH,
                        flightNumber: flight.flightNumber,
                        fareTypeId: fare.fareTypeId,
                        fareValue: fare.fareValue,
                        parentId: parentId
                    };
                    scope.selectedFareItems = selectedFlightFare;
                    //showPopup(currentDateOption);

                    var passToEvent = {
                        selectedFlight: flight,
                        selectedFare: fare
                    };
                    scope.onSelectFare(passToEvent);
                    selectedOptions[scope.segmentId].selectedFlightFare = selectedFlightFare;
                    selectedOptions[scope.segmentId].currentDateOption = currentDateOption;
                    selectedOptions[scope.segmentId].passToEvent = passToEvent;

                    scope.onConfirm({selectedFlightFares: _.pluck(selectedOptions, "flightFares")});
                    event.preventDefault();
                };

                scope.closePopup = function () {
                    scope.applyMask = {"display": "hide", "z-index": -10};
                    scope.blockSection = false;
                    scope.isPopupVisible = true;
                    scope.isContinuClicked = false;
                    //scope.selectedFareItems = {}
                };

                scope.showNextDay = function(){

                }

                console.log(scope.headerFType.length);
                function missMatchLength() {
                    // if (scope.bodyFType.length !== scope.headerFType.length) {
                    //     alert("Length Miss Match");
                    // } else {
                    //     var fLength = Math.floor(100 / scope.headerFType.length);
                    //     scope.fTypeWidth = {"width": fLength + "%"};
                    // }
                }

                function getFareType(faretpyId) {
                    var fareName;
                    angular.forEach(scope.headerFType, function (value, key) {
                        if (value.hasOwnProperty("fareTypeId")) {
                            if (value.fareTypeId === faretpyId) {
                                fareName = value.fareTypeName;
                            }
                            return false;
                        }
                    });
                    return fareName;
                }

                function showPopup(options) {
                    var popWidth = 305;
                    var popHeight = 210;
                    scope.blockSection = true;
                    scope.popupPos = {
                        "top": options.position.top - (popHeight - 70) / 2,
                        "left": options.position.left - (popWidth - 70) / 2,
                        "width": popWidth,
                        "height": popHeight
                    };
                    scope.applyMask = {"display": "block", "z-index": 10};
                    scope.farePopupHeader = getFareType(options.selectedFare.fareTypeId);

                    scope.flightArrivalTime = options.selectedFlight.arrivalTime;
                    scope.flightArrivalDate = options.selectedFlight.arrivalDate;
                    scope.flightDepartureTime = options.selectedFlight.departureTime;
                    scope.flightDepartureDate = options.selectedFlight.departureDate;
                    scope.originName = options.selectedFlight.originName;
                    scope.destinationName = options.selectedFlight.destinationName;

                    scope.incomingFare = options.selectedFare.fareValue;
                    scope.currencyType = scope.currency;

                }
                function clearPreviousSelection(){
                    for(var i=0;i< scope.flightOptions.length;i++){
                        var obj  = scope.flightOptions[i].flightFaresAvailability;
                        for(var j= 0;j<obj.length;j++){
                            if(obj[j].isFlightSelect){
                                obj[j].isFlightSelect = false;
                                break;
                            }
                        }

                    }
                }

                scope.onClickEditFlight = function (child, parent, flight) {
                    scope.selectFlight(flight);
                    if (child !== parent) {
                        scope.onEditFlight({id: child});
                    }

                };


            };
            return directive;
        }];
    isa_flight_select.directive(directives);
    isa_flight_select.filter('flightDuration', ['$translate', function ($translate) {
        return function (data) {
            var hoursString = $translate.instant('lbl_common_hours');
            var minutesString = $translate.instant('lbl_common_minutes');
            var directFlightString = $translate.instant('lbl_common_directFlight');
            var stopsString = $translate.instant('lbl_common_stops');
            var stopString = $translate.instant('lbl_common_stop');

            var duration = data.totalDuration;
//            data.stops[0]={flightNumber:"G5987"};
//            data.stops[1]={flightNumber:"G5987"};
//            data.stops[2]={flightNumber:"G5987"};
            var stops = (Object.keys(data.stops).length > 0 ) ? Object.keys(data.stops).length + ' '+ ( (Object.keys(data.stops).length > 1 ) ? stopsString : stopString ) : directFlightString;
            var days = (duration.split(":")[0] > 0) ? parseInt(duration.split(":")[0], 10) : '';
            var hours = (duration.split(":")[1] > 0) ? parseInt(duration.split(":")[1], 10)  : '';
            var totalH= parseInt(days*24) || 0 + parseInt(hours) || 0;
            var totalHours = (totalH>0)?totalH+ ' ' +hoursString:'';
            var minutes = (duration.split(":")[2] > 0) ? duration.split(":")[2] + ' ' + minutesString : '';
            return totalHours + ' ' + minutes + ' / ' + stops;
        };
    }]);
    isa_flight_select.filter('stopType', ['$sce', '$translate', function ($sce, $translate) {
        return function (data) {
            var daysString = $translate.instant('lbl_common_days');
            var hoursString = $translate.instant('lbl_common_hours');
            var minutesString = $translate.instant('lbl_common_minutes');

            var toolTip,wt= 0;
            if(data.durationWaiting){
                wt = data.durationWaiting.split(':');
            }
            var compareDate = function(date1, date2){
                var d1 = moment(date1, 'DD/MM/YYY');
                var d2 = moment(date2, 'DD/MM/YYY');
                if (d1 < d2){
                    return  ' (+1)';
                }else{
                    return  '';
                }

            };
//            var tmpFrom = angular.copy(data.airports);
//            var airportFrom = _.where(tmpFrom,{code:data.originCode});
            var airportFrom = [];
             airportFrom[0] = {
                 code:data.originCode,
                 name:data.originName
             }
            if(airportFrom.length) {
                var labelFrom = $translate.instant('lbl_flight_tooltip_from', {
                    fromName: data.originName,
                    fromCode: data.originCode
                });
            }
            var valueFrom = $translate.instant('lbl_flight_tooltip_from_dep_arr',{
           	 dep: data.departureTime,
        	 arr: data.arrivalTimeFirstSeg,
        	 plus: compareDate(data.departureDate, data.arrivalDateFirstSeg)
        	});

            var tmpTransit = angular.copy(data.airports);
            var airportTransit = _.where(tmpTransit,{code:data.airportCode});
            if(airportTransit.length) {
                var labelTransit = $translate.instant('lbl_flight_tooltip_transit', {
                    airportName: airportTransit[0].name,
                    airportCode: data.airportCode
                });
            }
            	
            var valueTransit = $translate.instant('lbl_flight_tooltip_transit_dep_arr',{
            	dep: data.departureTime,
            	arr: data.arrivalTime 
            	});
            var waitingTransit = $translate.instant('lbl_flight_tooltip_transit_waiting',{
            	days: days,
            	hours: hours,
            	minutes: minutes
            	});
                var tmpTransit = angular.copy(data.airports);
            var airportTransit = _.where(tmpTransit,{code:data.airportCode});
            if(airportTransit.length) {
                var labelTransit = $translate.instant('lbl_flight_tooltip_transit', {
                    airportName: airportTransit[0].name,
                    airportCode: data.airportCode
                });
            }
            	
            var valueTransit = $translate.instant('lbl_flight_tooltip_transit_dep_arr',{
            	dep: data.departureTime,
            	arr: data.arrivalTime 
            	});
            var waitingTransit = $translate.instant('lbl_flight_tooltip_transit_waiting',{
            	days: days,
            	hours: hours,
            	minutes: minutes
            	});
            var tmpTouchdown = angular.copy(data.airports);
            var airportTouchdown = _.where(tmpTouchdown,{code:data.airportCode});
            if(airportTouchdown.length) {
                var labelTouchdown = $translate.instant('lbl_flight_tooltip_touchdown', {
                    airportName: airportTouchdown[0].name,
                    airportCode: data.airportCode
                });
            }
            var valueTouchdown = $translate.instant('lbl_flight_tooltip_touchdown_dep_arr',{
            	arr: data.arrivalTime, 
            	dep: data.departureTime
            	});
            var waitingTouchdown = $translate.instant('lbl_flight_tooltip_touchdown_waiting',{
            	hours: hours,
            	minutes: minutes
            	}); 
            var labelDeparture =$translate.instant('msg_common_departure');
            var labelArrival = $translate.instant('msg_common_arrival');
            var toolTip,wt= 0;
            if(data.durationWaiting){
                wt = data.durationWaiting.split(':');
            }

            var days = (wt[0] > 0) ? wt[0] + ' ' + daysString : '';
            var hours = (wt[1] > 0) ? wt[1] + ' ' + hoursString : '';
            var minutes = (wt[2] > 0) ? wt[2] + ' ' + minutesString : '';
            var waitingTransit = $translate.instant('lbl_flight_tooltip_transit_waiting',{
            	days: days,
            	hours: hours,
            	minutes: minutes
            	});
            var waitingTouchdown = $translate.instant('lbl_flight_tooltip_touchdown_waiting',{
            	hours: hours,
            	minutes: minutes
            	}); 
            
            if(data.stops){
                toolTip = (
                    '<div class="fare-tooltip-wrappe"><span class=" fare-tooltip-heading">'+data.originName+'</span> ' +
                    '<span class="fare-tooltip-icon"></span></br>' +
                    '<span >' + data.flightNumber + '</span>' +
                    '<span> (' + data.equipmentModelNumber + ') </span>' + 
                    // Operated By: ' + data.displayCarrier + '</span>' +
                    '</br><span> '+labelDeparture+' <b>' + data.departureTime + ' </b><br/>'+labelArrival+' <b>' + data.arrivalTimeFirstSeg +
                    compareDate(data.departureDate, data.arrivalDateFirstSeg) + '</b></span><br/>' +
                    '<span class="fare-tooltip-icon"></span>'
                );
            }else if (data.flightNumber) {
                toolTip = (
                    '<div class="fare-tooltip-wrappe"><span class=" fare-tooltip-heading">'+data.originName+'</span>' +
                    '<span class="fare-tooltip-icon"></span></br>' +
                    '<span >' + data.flightNumber + '</span>' +
                    '<span> (' + data.equipmentModelNumber + ')  </span>' + 
                    // Operated By: ' + data.displayCarrier + '</span>' +
                    '</br><span> '+labelDeparture+' <b>' + data.departureTime + '</b><br>'+labelArrival+' <b>' + data.arrivalTime +
                    compareDate(data.departureDate, data.arrivalDate) + '</b></spna><br/>' +
                    '<span class="fare-tooltip-icon"></span>' +
                    '<span > Waiting time <b>' + days + '</b> <b>' + hours + '</b> <b>' + minutes + '</b></span></div>'
                );
            } else {
                toolTip = (
                    '<div class="fare-tooltip-wrappe"><span class=" fare-tooltip-heading">'+labelTouchdown+'</span> ' +
                    '</br><span>'+valueTouchdown+'</span><br/>' +
                    '<span class="fare-tooltip-icon"></span>' +
                    '<span > Waiting time <b>' + hours + '</b> <b>' + minutes + '</b></span></div>'
                );
            }
            return fareTooltip[toolTip] || (fareTooltip[toolTip] = $sce.trustAsHtml(toolTip));
        };
    }]);
    isa_flight_select.filter('fareTypeTip', ['$sce', function ($sce) {
        return function (data) {
            var toolTip;
            if (data != undefined && data.length > 0 ) {
                toolTip = (
                    '<div class="fare-tooltip-wrappe">' + data + '</div>'
                );
            }
            return $sce.trustAsHtml(toolTip);
        };
    }]);
})();
