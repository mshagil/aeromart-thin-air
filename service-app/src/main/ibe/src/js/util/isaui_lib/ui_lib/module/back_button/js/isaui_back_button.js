/**
 * Created by Nipuna on 10/05/17.
 */
'use strict';

var isa_back_button = angular.module("isaBackButton", []);
var directives = {};
directives.isaBackButton = [
    '$compile', '$http', '$templateCache', '$state', '$stateParams', 'languageService', 'currencyService', 'applicationService', '$rootScope',
    function ($compile, $http, $templateCache, $state, $stateParams, languageService, currencyService, applicationService, $rootScope) {
        var directive = {};
        var isaconfig = new IsaModuleConfig();

        directive.restrict = 'AEC';
        directive.scope = {caption: '=name'};

        directive.link = function (scope, element, attr) {

            var STATES = {
                FARE: 'fare',
                PASSENGER: 'passenger'
            };

            var MODES = {
                CREATE: 'create',
                CHANGE_CONTACT: 'changeContactInformation',
                NAME_CHANGE: 'nameChange'
            };

            scope.navigateBack = function () {
                var state = $state.current.name;
                var mode = $stateParams.mode;

                // call back function from common_services, navigation Service
                navigateTo(state, mode)
            };

            //TODO: Implementation of navigating based on mode and state name
            function navigateTo(state, mode) {

                //TODO: Discuss issues with this implementation
                $state.go($rootScope.fromState.fromStateName, $rootScope.fromState.fromParam);

                switch(state) {
                    case STATES.FARE:
                        switch(mode) {
                            case MODES.CREATE:
                                goToState(STATES.FARE);
                                break;
                            case MODES.CHANGE_CONTACT:
                            case MODES.NAME_CHANGE:
                                break;
                        }
                        break;

                    case STATES.PASSENGER:
                        switch(mode) {
                            case MODES.CREATE:
                                goToState(STATES.FARE);
                                break;
                            case MODES.CHANGE_CONTACT:
                            case MODES.NAME_CHANGE:
                                break;
                        }
                        break;
                }
            }

            function goToState(stateName){
                $state.go(stateName,
                    {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
            }

            var template = $templateCache.get(isaconfig.getTemplates("isaBackButton")['default']);

            template = angular.element(template);
            element.append(template);
            $compile(template)(scope);
        };

        return directive;

    }];
isa_back_button.directive(directives);

