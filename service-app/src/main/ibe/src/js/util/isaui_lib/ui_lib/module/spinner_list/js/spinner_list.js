/**
 * Created by indika on 10/16/15.
 */
'use strict';

(function () {
    var isa_spinnerList = angular.module("isaSpinnerList", ["isaspinner"]);
    var directives = {};
    var isaconfig = new IsaModuleConfig();

    directives.isaSpinnerList = [
        '$compile', '$http', '$templateCache','$rootScope',
        function ($compile, $http, $templateCache,$rootScope) {
            var directive = {};

            directive.restrict = 'AEC';
            directive.scope = {
                max: '=',
                min: '=',
                setCount: '&',
                value: '=',
                meal:'=',
                isVisible: '=',
                bindTo: '=',
                defaultValue: '=',
                spinnerSource: '=',
                currentItemSelection: '=',
                isMobileDevice: '=',
                selectedCategoryCode: '=',
                indexCategory: '=',
                selectedSegment: '=',
                indexSegment: '=',
                onElementParentClick:'&',
                index:'=',
                getInvent : '&'
            };
            directive.require = '';
            directive.link = function (scope, element, attr) {

                if (!("defaultValue" in attr)) {
                    scope.defaultValue = 1;
                }

                var count = 0;
                var template = $templateCache.get(isaconfig.getTemplates("isaSpinnerList")['default']);
                 scope.mDevice = $rootScope.deviceType.isMobileDevice;
                var currentTarget
                template = angular.element(template);
                element.append(template);
                $compile(template)(scope);

                scope.onIncriment = function (e, index, value) {
                    var elem = angular.element(e.currentTarget).parent().parent().find('input[type="text"]');
                    count = Number(elem.val());
                    count = parseInt(count);
                    count = scope.getInvent({oldCount:count, newCount: count + 1, key: index, value: value});
                    if (count <= scope.max) {
                    	this.currentItemSelection[index].count = count;
                    	this.currentItemSelection[index].oldCount = count;
                    	var data = {spinnerValue: count, index: index};
                    	if(scope.mDevice )
                    		data = {spinnerValue: count, index: index,fromMobileClick:true}
                    	scope.setCount(data);
                    }
                    //elem.val(count);
                };
                scope.onDecriment = function (e, index, value) {
                    var elem = angular.element(e.currentTarget).parent().parent().find('input[type="text"]');
                    count = Number(elem.val());
                    count = parseInt(count);
                    count = scope.getInvent({oldCount:count, newCount: count - 1, key: index, value: value});

                    if (count >= scope.min) {
                    	//elem.val(count);
                    	this.currentItemSelection[index].count = count;
                    	this.currentItemSelection[index].oldCount = count;
                    	var data = {spinnerValue: count, index: index};
                    	if(scope.mDevice )
                    		data = {spinnerValue: count, index: index,fromMobileClick:true}
                    	scope.setCount(data);
                    }
                };
                scope.onDoneClick = function (event,index ,isClose) {
                                    scope.onElementParentClick({event:event,index:index,isClose:isClose});
                                    if(event){
                                        event.preventDefault();
                                        event.stopPropagation();
                                    }
                                }
                angular.element('body').on('click', function (e) {
                    if (element.find(e.target).length <= 0) {
                        //scope.isVisible = false;
                    }
                });
                /*
                 count changed on enter key press
                 */
                scope.onKeyInput = function (e) {
                    if (e.keyCode == 13) {
                        var key = angular.element(e.currentTarget).data('meal-key')
                        currentTarget = e.currentTarget;
                        count = angular.element(e.currentTarget).val();
                        
                        var currentCount = (this.currentItemSelection[key].oldCount)?this.currentItemSelection[key].oldCount:0;
                        count = scope.getInvent({oldCount:currentCount, newCount: count, key: key, value: scope.meal});
                        if (count < scope.max && count > scope.min) {
                            var data = {spinnerValue: parseInt(count, 10), index: key};
                            scope.setCount(data);
                        } else if (count <= scope.min) {
                            var data = {spinnerValue: 0, index: key};
                            scope.setCount(data);
                        }
                        this.currentItemSelection[key].count = count;
                        this.currentItemSelection[key].oldCount = count;

                        scope.$watch(
                                function (scope) {
                                    return scope.setCount
                                },
                                function () {
                                    if (count > scope.max) {
                                        scope.setCount({spinnerValue: parseInt(scope.max, 10), index: key});
                                        angular.element(e.currentTarget).val(scope.max);
                                    }
                                }
                        );

                    }
                };

                scope.$watch('isVisible', function (visible, prev) {
                    if (visible && prev) {
                        var dropDown = element.find('.meals-qty-dropdown');
                        var keyElementList = dropDown.find('input[type="text"]');

                        angular.forEach(keyElementList, function (keyElement) {
                            var count = parseInt(angular.element(keyElement).val());
                            var key = angular.element(keyElement).data('meal-key');
                            var currentCount = this.currentItemSelection[key].count;

                            count = scope.getInvent({oldCount:currentCount, newCount: count, key: key, value: scope.meal});
                            this.currentItemSelection[key].count = count;

                            if (count <= scope.max && count > scope.min) {
                                var data = {spinnerValue: parseInt(count, 10), index: key};
                                scope.setCount(data);
                            }
                        });
                    }
                });

            };
            return directive;
        }];
    isa_spinnerList.directive(directives);
})();

