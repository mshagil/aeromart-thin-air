'use strict';

(function () {
    var isa_phone_number = angular.module("isaPhoneNumber", []);
    var directives = {};
    var isaConfig = new IsaModuleConfig();

    directives.isaPhoneNumber = ["$compile", "$http", "$templateCache", function ($compile, $http, $templateCache) {
        var directive = {};

        directive.restrict = 'AEC';

        directive.scope = {
            phoneNumber: '=',
            countryList: '=',
            phoneRequired: '=',
            fieldName: '=',
            formName: '@',
            labelVisible:'@labelVisible',
            configs:'=',
            validateWithList:'='
            
        };

        directive.link = function (scope, element) {
            var template = $templateCache.get(isaConfig.getTemplates("isaPhoneNumber")['default']);
            template = angular.element(template);
            element.append(template);
            $compile(template)(scope);

            scope.isLabelsVisible = !(scope.labelVisible === "false")

        };

        directive.controller = ["$scope","$element", function ($scope,$element) {
            $scope.classInput='';
            $scope.classAreaInput= '';
            $scope.countryCodeList = $scope.countryList.map(function(country){ return country.phoneCode});

            $scope.formatLabelCountry = function (model) {
                var tempN = $scope.countryList;
                if (tempN) {
                    for (var i = 0; i < tempN.length; i++) {
                        if (model === tempN[i].phoneCode) {
                            return tempN[i].phoneCode;
                        }
                    }
                }
            };

            $scope.onCountryChange = function(val){
                $scope.phoneNumber.countryCode = val.phoneCode;
            };
            /**
             *  Filter typeaheads according to view value.
             */
            $scope.startsWith = function(phoneName, viewValue) {
                if(viewValue === " "){
                    return true;
                }else{
                    return phoneName.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
                }
            }
        }];

        return directive;
    }];
    isa_phone_number.directive(directives);
})();
