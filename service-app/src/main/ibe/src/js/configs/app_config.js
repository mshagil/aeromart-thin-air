/**
 * Created by indika on 11/5/15.
 */
var IBEConfig = function(){
    var ibeModule = {};
    var _root = "ui_lib/module";
    //all
    var modules = {
        travelFare : {
            name: 'travelFare',
            templates: {
                default:"modules/reservation/fare/templates/travel.fare.tpl.html",
                modifySearch:"modules/reservation/fare/templates/modify.search.tpl.html",
                header : 'modules/reservation/fare/templates/progress.tpl.html',
                createSummeryDrawer : "modules/reservation/fare/templates/create.summery.drawer.tpl.html"
            }
        },
        multiCity : {
            name: 'multiCity',
            templates: {
                default:"modules/reservation/multi_city/templates/multi.city.search.tpl.html",
                fare:"modules/reservation/multi_city/templates/multi.city.fare.tpl.html",
                flight:"modules/reservation/multi_city/templates/multi.city.flight.tpl.html",
                header : 'modules/reservation/fare/templates/progress.tpl.html',
                createSummeryDrawer : "modules/reservation/fare/templates/create.summery.drawer.tpl.html"
            }
        },
        passenger:{
            name:'passenger',
            templates : { 
                default:"modules/reservation/passenger/templates/passenger.tpl.html",
                material:"modules/reservation/passenger/templates/passenger.material.tpl.html",
                modifySearch:"modules/reservation/fare/templates/modify.search.tpl.html"
            }
        },
        extras:{
            name:'extras',
            templates : {
                default:"modules/reservation/addons/templates/extras.tpl.html",
                modifySearch:"modules/reservation/fare/templates/modify.search.tpl.html"
            }
        },
        payment:{
            name:'payment',
            templates : {
                default:"modules/reservation/payments/templates/payment.tpl.html",
                postPayment:"modules/reservation/payments/templates/payment_post.tpl.html",
                voucherPostPayment:"modules/reservation/payments/templates/voucher_post_payment.tpl.html",
                createPaymentSummary:"modules/reservation/payments/templates/payment_summary_create.tpl.html",
                modifyPaymentSummary:"modules/reservation/payments/templates/payment_summary_modify.tpl.html",
                onholdPaymentSummary:"modules/reservation/payments/templates/payment_summary_onhold.tpl.html"
            }
        },
        confirm:{
            name:'confirm',
            templates : {
                default:"modules/reservation/confirm/templates/confirm.reservation.tpl.html",
                onhold:"modules/reservation/confirm/templates/confirm.onhold.tpl.html"
            }
        },
        extraBaggage:{
            name:'extrasBaggage',
            templates : {
                default:"modules/reservation/addons/templates/extras.baggage.tpl.html"
            }
        },
        extraSeats:{
            name:'extraSeats',
            templates : {
                default:"modules/reservation/addons/templates/extras.seats.tpl.html"
            }
        },
        extraMeal:{
            name:'extraMeal',
            templates : {
                default:"modules/reservation/addons/templates/extras.meal.tpl.html"
            }
        },
        extraAutomaticCheckin:{
            name:'extraAutomaticCheckin',
            templates : {
                default:"modules/reservation/addons/templates/extras.automatic.checkin.tpl.html"
            }
        },
        extraInsurance:{
            name:'extraInsurance',
            templates : {
                default:"modules/reservation/addons/templates/extras.insurance.tpl.html"
            }
        },
        extraAirportTransfer:{
            name:'extraAirportTransfer',
            templates : {
                default:"modules/reservation/addons/templates/extras.airport.transfer.tpl.html"
            }
        },
        extraflexibility:{
            name:'extraflexibility',
            templates : {
                default:"modules/reservation/addons/templates/extras.flexi.tpl.html"
            }
        },
        extraServices:{
            name:'extraServices',
            templates : {
                default:"modules/reservation/addons/templates/extras.services.tpl.html"
            }
        },
        extraAirportServices:{
            name:'extraAirportServices',
            templates : {
                default:"modules/reservation/addons/templates/extras.airport.services.tpl.html"
            }
        },
        modifyDashboard: {
            name: 'modifyDashboard',
            templates: {
                default: "modules/modification/dashboard/templates/dashboard.tpl.html",
                header : "modules/modification/dashboard/templates/dashboard.header.tpl.html"
            }
        },
        modifyReservation : {
            name:'modifyReservation',
            templates : {
                default:"modules/modification/reservation/templates/reservation.load.tpl.html",
                lookup:"modules/modification/reservation/templates/reservation.lookup.tpl.html",
                modificationDrawer : "modules/modification/reservation/templates/modification.summery.drawer.tpl.html",
                nameChangeDrawer : "modules/modification/reservation/templates/modification.namechange.drawer.tpl.html"
            }
        },
        voucher : {
            name:'voucher',
            templates : {
	            default:"modules/reservation/voucher/templates/voucher.tpl.html",
	            header : "modules/reservation/voucher/templates/voucher.header.tpl.html"
            }
        },
        voucherThank : {
            name:'voucherThank',
            templates : {
	            default:"modules/reservation/voucher/templates/voucher.thank.tpl.html",
	            header : "modules/reservation/voucher/templates/voucher.header.tpl.html"
            }
        },
        modifyFlight : {
            name:'modifyFlight',
            templates  : {
                default : "modules/modification/flight/templates/modification.flights.home.tpl.html",
                selectFlightHome : "modules/modification/flight/templates/modification.select.flight.home.tpl.html",
                selectedFlight : "modules/modification/flight/templates/modification.selected.flight.options.tpl.html",
                flightModificationDrawer : "modules/modification/flight/templates/modification.modify.flight.drawer.tpl.html",
                flightSearch : "modules/modification/flight/templates/modification.flight.search.tpl.html",
                flightSearchResult : "modules/modification/flight/templates/modification.flight.search.result.tpl.html",
                changedFlight : "modules/modification/flight/templates/modification.flight.changed.flight.tpl.html"
            }
        },
        viewReservations : {
            name:'viewReservations',
            templates : {
                default:"modules/user/viewReservations/templates/viewReservations.tpl.html",
                header : "modules/modification/dashboard/templates/dashboard.header.tpl.html"

              }
        },
        viewFamilyFriends : {
            name:'viewFamilyFriends',
            templates : {
                default:"modules/user/familyMembers/templates/family.members.tpl.html",
                header : "modules/modification/dashboard/templates/dashboard.header.tpl.html"
            }
        },
        viewSeat : {
            name:'viewSeat',
            templates : {
                default:"modules/user/viewSeatMealPreferences/templates/view.seat.meal.preferences.tpl.html",
                header : "modules/modification/dashboard/templates/dashboard.header.tpl.html"
            }
        },
        loginBar: {
            name: 'loginBar',
            templates: {
                default: "modules/common/login_bar/templates/login_bar.tpl.html"
            }
        },
        banner: {
            name: 'banner',
            templates: {
                default: "modules/common/pageBanner/banner.html"
            }
        },
        liveChat: {
            name: 'liveChat',
            templates:{
                default: "modules/common/liveChat/liveChat.html"
            }
        },
        signIn: {
            name: 'signIn',
            templates: {
                default: "modules/user/signIn/templates/signIn.tpl.html"
            }
        },
        redirect: {
            name: 'redirect',
            templates: {
                default: "modules/user/registration/templates/redirection.tpl.html"
            }
        },
        registration: {
            name: 'registration',
            templates: {
                default: "modules/user/registration/templates/registration.tpl.html",
                changePassword: "modules/user/registration/templates/change_password.tpl.html"
            }
        },
        viewCreditHistory : {
            name:'viewCreditHistory',
            templates : {
                default:"modules/user/viewCreditHistory/templates/viewCreditHistory.tpl.html",
                header : "modules/modification/dashboard/templates/dashboard.header.tpl.html"
            }
        },
        404:{
            name:'404',
            templates : {
                default:"modules/404.tpl.html"
            }
        },
        errorPage:{
            name:'errorPage',
            templates : {
                default:"modules/common/error_page/templates/error.tpl.html"
            }
        },
        confirmPage:{
            name:'confirmPage',
            templates : {
                default:"modules/common/confirm_page/templates/confirm.tpl.html",
                customer: "modules/common/confirm_page/templates/customer_confirm.tpl.html"
            }
        },
        reProtect : {
            name: 'reProtect',
            templates: {
                default:"modules/reservation/passangerReProtection/templates/self.reprotect.tpl.html",
                header : 'modules/reservation/passangerReProtection/templates/self.reprotect.header.tpl.html'
        
            }
        },
    };

    ibeModule.getModuleNames = function(){
        var modNamess = [];
        for(var key in modules) {
            modNamess.push(key);
        }
        return modNamess;

    };
    ibeModule.getTemplates = function(name){
        return modules[name]['templates'];
    };
    return ibeModule;

};
