/**
 * Created by sumudu on 1/25/16.
 */

'use strict';

angular.module('IBE').config([
    'localStorageServiceProvider',
    '$translateProvider',
    '$httpProvider',
    function (localStorageServiceProvider, $translateProvider,$httpProvider) {

        //localStorageProvider config
        localStorageServiceProvider
            .setPrefix('IBE')
            .setStorageType('sessionStorage')
            .setNotify(true, true);

        //setting up language files loaging
        $translateProvider.preferredLanguage('en');
        $translateProvider.useLoader('asyncLanguageLoader');


        //Set HTTP Interceptor
        $httpProvider.interceptors.push('ibeHttpInterceptor');

    }]);
