/**
 *  Created by Nipuna on 3/15/16.
 */

(function () {

    angular
        .module("IBE")
        .filter('listFilter', [listFilter]);

    function listFilter() {

        /**
         * @param: modelValue - The value that's selected in the ng-model
         * @param: modelCode - The code that's selected in the ng-model
         * @param: viewCode - The code that's selected in the view
         * @param: itemList - The list to be filtered though
         */
        return function (modelValue, itemList, modelCode, viewCode) {
            var i;
            var viewValue = ""; //The value to be set for the view

            for (i = 0; i < itemList.length; i++) {
                if(itemList[i][modelCode] === modelValue){
                    viewValue = itemList[i][viewCode];
                }
            }

            return viewValue;

        }; // End of return filter function.

    } // End of listFilter.

})();
