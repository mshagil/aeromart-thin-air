/**
 *      Created by Chamil on 19.01.2016
 *      This file holds functions for the dashboard service.
 */

(function (argument) {

    // Define dashboard service.
    angular
        .module("modification.dashboard")
        .service("dashboardSvc", ["signInService", "confirmRemoteService", "customerRemoteService", dashboardService]);

    function dashboardService(signInService, confirmRemoteService, customerRemoteService) {
        var self = this;

        self.getReservationList = getReservationList;
        self.resendConfirmationEmail =  resendConfirmationEmail;


        function getReservationList(params, successFn, errorFn) {
            customerRemoteService.getReservationList(params, successFn, errorFn);
        }


        function resendConfirmationEmail(params, successFn, errorFn) {
            customerRemoteService.resendConfirmationEmail(params, successFn, errorFn);
        }


        self.getDashboardReservations = function (loginParams, successFn, errorFn) {

            userSignInRemoteService
                .signInUser(loginParams)
                .then(function (data) {
                    successFn(data);
                }, function (data) {
                    errorFn(data);
                });
        };

        self.getUrlByTag = function (urlRef, successFn, errorFn) {
            return confirmRemoteService.getUrlByTag(urlRef, function (data) {
                // Set url type to differentiate the url.
                data.urlRef = urlRef["urlRef"];
                successFn(data);
            }, errorFn);
        };


    } // End of dashboardService function.

})(); // End of IIFE.
