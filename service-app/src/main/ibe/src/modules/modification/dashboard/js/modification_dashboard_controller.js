/**
 *      Created by Chamil on 19.01.2016
 *      This file holds functions for the dashboard controller.
 */

(function (angular) {

    // Define dashboard controller.
    angular
        .module("modification.dashboard")
        .controller("dashboardCtrl", [
            "$state",
            "$window",
            "languageService",
            "currencyService",
            "applicationService",
            "dashboardSvc",
            "commonLocalStorageService",
            'IBEDataConsole',
            'loadingService',
            'eventListeners',
            'DTO',
            'datePickerService',
            'constantFactory',
            'registrationService',
            'customerRemoteService',
            'alertService',
            'signInService',
            'PersianDateFormatService',
            'dateTransformationService',
            'formValidatorService',
            'popupService',
            'remoteCustomer',
            '$injector',
            'remoteStorage',
            'remotePayment',
            '$translate',
            '$timeout',
            'signoutServiceCheck',
            dashboardController]);

    function dashboardController($state, $window, languageService, currencyService, applicationService, dashboardSvc, commonLocalStorageService,
        IBEDataConsole, loadingService, eventListeners, DTO, datePickerService, constantFactory, registrationService, customerRemoteService, alertService, signInService,PersianDateFormatService,dateTransformationService,formValidatorService, popupService,remoteCustomer,$injector,remoteStorage, remotePayment,$translate,$timeout,signoutServiceCheck) {

        // Global vars.
        var self = this;
        signoutServiceCheck.checkSessionExpiry();
        var airportsList;

        self.maxDate = (moment().subtract(2, 'years')).toDate();

        // Get FORM VALIDATIONS
        self.formValidation = formValidatorService.getValidations();
        
        // Properties.
        self.hasUserJoinedLMS; // show airwards registration section.
        self.isEmailConfirmed;
        self.hasJoinAirwards = "false"; // radio buttons.
        self.hasMashreqCard = "false";
        self.isLoyaltyManagementEnabled = (constantFactory.AppConfigStringValue('enableLoyaltyManagement') === 'true');

        self.dateformat = datePickerService.setDateFormat(4); // Holds date format for calendar controllers.
        self.languageList = constantFactory.Langauges();
        self.isFamilyEmailRequired = false;
        self.emailId;
        self.airwardsDOB;
        self.language = languageService.getSelectedLanguage() ;
        self.referringEmail;
        self.familyEmail;
        self.passport;
        self.firstName;
        self.lastName;
        self.language=languageService.getSelectedLanguage();
        self.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');

        self.accountNumber; // Mashreq account number
        self.mashreqDOB; // Mashreq DoB
        self.mashreqDOBOpened = false; // Mashreq DoB open
        self.isSaveCardBusyLoaderVisible = false;
        self.loadingMessageForSaveCreditCard = "msg_save_creditcard_loadingMsg";
        
        /**
         * This controller and template is used for multiple page states, these are set at initialization
         * Viewport elements will be controlled based on this state
         *
         * UPDATE: Update user without LMS or Mashreq
         * UPDATE_LMS: Update user with LMS but without Mashreq
         * UPDATE_LMS_MASHREQ: Update user with LMS and Mashreq
         * */
        self.pageStates = {
            "UPDATE": "update_nolms",
            "UPDATE_LMS": "update_lms",
            "UPDATE_LMS_MASHREQ": "update_lms_mashreq"
        };
        self.pageState = self.pageStates.UPDATE;

        // Properties for JqueryUi date picker
        self.maxDate = angular.copy(moment().subtract(2, 'years')).toDate();
        self.minDate = angular.copy(moment().subtract(99, 'years').add(1, 'days')).toDate();
        self.dateOptions = {
            changeYear: true,
            changeMonth: true,
            minDate: self.minDate,
            maxDate: self.maxDate,
            dateFormat: 'dd/mm/yy',
            weekHeader: "Wk",
            yearRange: "-100:+0",
            showOn: "both",
            buttonImage: "images/calendar.png",
            disabled: self.isLmsMember
        };

        // Functions.
        self.viewAllReservations = viewAllReservations;
        self.viewStatements = viewStatements;
        self.redirectToReservation = redirectToReservation;
        self.bookFlight = bookFlight;
        self.joinAirewards = joinAirewards;
        self.setFamilyEmailStatus = setFamilyEmailStatus;
        self.dpOnClick = dropdownOnClick;
        self.formatLabelLanguage = formatLabelLanguage;
        self.resendEmail = resendEmail;
        self.isPersianCalendarType = applicationService.getIsPersianCalendar();
        self.deleteUserCard = deleteUserCard;
        self.openAddNewCardPopup = openAddNewCardPopup;
        self.savedCard = savedCard;
        self.onlineCheckIn = onlineCheckIn;
        self.getAirportCode = getAirportCode;
        self.isWebCheckInEnabled = isWebCheckInEnabled;
        /**
         *  Init function.
         */
        init();
        function init(){

            // Show topNav.
            loadingService.showPageLoading();

            var userData = commonLocalStorageService.getUserSignInData();
            airportsList = remoteStorage.getAirports();

            signInService.setMashreqStatus(userData.hasMashreqLoyalty);

            // Setting the pageState for LMS registered users
            if(!_.isNull(userData.loggedInLmsDetails)){
                if(signInService.getMashreqStatus() !== "false"){
                    self.pageState = self.pageStates.UPDATE_LMS_MASHREQ;
                }else{
                    // Set state as UPDATE_LMS if update user has lmsDetails
                    self.pageState = self.pageStates.UPDATE_LMS;
                }
            }

            if (_.isEmpty(userData)) {
                $state.go('signIn', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency()
                });
            }

            setUIComponentValues(userData);

            dashboardSvc.getReservationList({}, function (response) {
                userData.reservationList = response.reservationDetails;
                commonLocalStorageService.setReservationData(userData.reservationList);
                setUIComponentValues(userData);

                // Hide topNav loading.
                hidePageLoading();
            }, function (error) {
                // Hide topNav loading.
                hidePageLoading();
            })
            getSignUserCardDetails();
            

        } // End of Init function.


        /**
         *  Resend email confirmation for LMS.
         */
        function resendEmail() {

            customerRemoteService.resendConfirmationEmail({}, function (response) {
                if (response.success) {
                    alertService.setAlert("Confirmation email is successfully resent.&lrm;", "success");
                }
            }, function (error) {

            });
        } // End of resendEmail.

        /**
         * getSignUserCardDetails() is use to get saved card details of SignIn User
         * @param userData
         */
        function getSignUserCardDetails() {
        	remoteCustomer.getSignInUserPaymentDetails(function(paymentCardObject) {
        		if(paymentCardObject == undefined || paymentCardObject.length < 1) {        			
        			sessionStorage.removeItem("saveCardListObj");
        			self.paymentCardObject = null;
        			return false;
				}
				
				for (var index = 0; index < paymentCardObject.length; index++) {
					var cardDigits = "****"
							+ paymentCardObject[index].noLastDigits;
							paymentCardObject[index].noLastDigits = cardDigits;
							var dateFormat = paymentCardObject[index].expiryDate.split("-");
							paymentCardObject[index].expiryDate = dateFormat[0] + "-" + dateFormat[1];
				}
				self.paymentCardObject = paymentCardObject;				
				sessionStorage.setItem("saveCardListObj", JSON.stringify(paymentCardObject));				
	        }, function (error){
	        	
	        });
        }
        
        /**
         * deleteUserCard() is use to delete specific card details of SignIn User
         * @param customerAliasId
         */
        function deleteUserCard(customerAlias) {
        	
        	popupService.confirm("Are you sure to delete this card?&lrm;", function (result) {
            	var cardDetailObject = {
            		"customerAliasId" : customerAlias
            	};
            	remoteCustomer.deleteUserCardDetail(cardDetailObject,function(response){
            		if(response.success == false) {
            			return false;
            		}
            		$translate("msg_successfully_card_deleted").then(function(msgSuccessfullyCardDeleted) {
            			popupService.alert(msgSuccessfullyCardDeleted);
            		});
            		
            		getSignUserCardDetails();
            	},function(error){
            		$translate("msg_card_delete_error").then(function(msgCardDeleteError) {
            			popupService.alert(msgCardDeleteError);
            		});
            		
            		return false
            	});

            }); // End of popupService.confirm.
        	
        }

        /**
         * openAddNewCardPopup() is use to add new card
         */
        function openAddNewCardPopup() {
        	 var userData = commonLocalStorageService.getUserSignInData();
        	 //ideally this should support for multiple PG's
        	 var paymentOption = userData.iPGPaymentOptionDTO;
        	  
        	 if(!_.isNull(paymentOption)){
				   var pgConfigs = {
                    'className' : 'save-card-template',
					'paymentGateway' : paymentOption.paymentGateway,
					'baseCurrency' : paymentOption.baseCurrency,
					'aliasBasePaymentAmount' : paymentOption.aliasBasePaymentAmount
				};
        		popupService.openSaveCardDialog('save.card.tpl.html',pgConfigs,self.savedCard ,function(){});
        	 }
      	
            
        }
        
        function savedCard(saveCardObject) {
        	self.isSaveCardBusyLoaderVisible = true;
        	var userData = commonLocalStorageService.getUserSignInData();
        	var object = { 
    			"paymentOptions": {
	    			"paymentGateways": [{
        		        "gatewayId": userData.iPGPaymentOptionDTO.paymentGateway,
        		        "cards": [{
        		        	"paymentAmount": userData.iPGPaymentOptionDTO.aliasBasePaymentAmount,
        		        	"cardName" : saveCardObject.cardName,
        		        	"cardType": saveCardObject.cardType,
        		            "expiryDate": saveCardObject.expiryDate,
        		            "cardNo": saveCardObject.cardNo,
        		            "cardHoldersName": saveCardObject.cardHoldersName,
        		            "cardCvv": saveCardObject.cardCvv
        		        }]
	    			}]
	  			}	
        	};

        	remotePayment.saveUserCardDetail(object,function(response){
        		self.isSaveCardBusyLoaderVisible = false;
        		if(response.success == false) {
                    $translate("msg_card_save_error").then(function(msgCardSaveError) {
                        popupService.alert(msgCardSaveError);
                    });
        			return false;
        		}
        		getSignUserCardDetails();
        		alertService.setAlert("msg_successfully_card_added",'success');
        		
        	},function(error){
        		self.isSaveCardBusyLoaderVisible = false;
        		$translate("msg_card_save_error").then(function(msgCardSaveError) {
        			popupService.alert(msgCardSaveError);
        		});
        		return false
        	});

        }
        
        /**
         *  Set UI components according to user data. no need to validate userData object.
         *  If userData object is empty init function will change state to dashboard.
         */
        function setUIComponentValues(userData) {

            self.totalCustomerCredit = userData.totalCustomerCredit;
            self.lmsDetails = (userData.loggedInLmsDetails !== null) ? userData.loggedInLmsDetails.availablePoints : 0;
            var unsortedReservation = commonLocalStorageService.getReservationData();
            
            //Only set the reservations if getReservationData returns values
            if(unsortedReservation){
                self.reservations = commonLocalStorageService.getReservationData().slice(0,5); 
                if(window.matchMedia("only screen and (max-width: 767px)").matches) {
                    for(var reservLv1 = 0;reservLv1 < self.reservations.length;reservLv1++) {
                        for(var reservLv2=0;reservLv2<self.reservations[reservLv1].segmentDetails.length;reservLv2++){
                                self.reservations[reservLv1].segmentDetails[reservLv2]['dayarrivalTime']=moment(self.reservations[reservLv1].segmentDetails[reservLv2].departureDateTime).format('ddd DD MMMM  YYYY hh:mm');
	                    	self.reservations[reservLv1].segmentDetails[reservLv2]['daydepartureTime']= moment(self.reservations[reservLv1].segmentDetails[reservLv2].departureDateTime).format('ddd DD MMMM  YYYY hh:mm');
                        }
                    }
//                    angular.forEach(self.reservations, function(reservationList) {
//                        angular.forEach(reservationList.segmentDetails, function(segmentDetail) {
//                            segmentDetail.departureDateTime = moment(segmentDetail.departureDateTime).format('ddd DD MMMM  YYYY hh:mm');
//                            segmentDetail.arrivalDateTime = moment(segmentDetail.arrivalDateTime).format('ddd DD MMMM  YYYY hh:mm');                          
//                        });
//                    });
                }
            }
            
            self.loyaltyManagmentEnabled = userData.loyaltyManagmentEnabled;
            self.firstName = userData.loggedInCustomerDetails.firstName;
            self.lastName = userData.loggedInCustomerDetails.lastName;

            self.emailId = (userData.loggedInCustomerDetails !== null) ? userData.loggedInCustomerDetails.emailId : null;
            self.hasUserJoinedLMS = userData.loggedInLmsDetails !== null;
            self.isEmailConfirmed = userData.loggedInLmsDetails && userData.loggedInLmsDetails.emailConfirmed;
            self.isCreditAvailable = userData.totalCustomerCredit > 0;


            self.isReservationsAvailable = (self.reservations && self.reservations.length > 0);

            self.claimMissingPointsUrl;
            self.redeemPointsUrl;
            self.earnPointsUrl;
            self.faqUrl;
            self.contactUsUrl;
            self.partnersUrl;
            self.pointsCalculatorUrl;
            self.activitiesUrl;

            // Set dashboard urls.//CALCULATOR, EARN
            var tags = ["RETRO", "FAQS", "REDEEM", "CONTACT", "PARTNERS", "ACTIVITIES"];

            // Only call the crossPortalLogin requests if the user is an LMS user
            if(self.loyaltyManagmentEnabled && self.hasUserJoinedLMS){
                //Set dashboard urls.
                setDashBoardUrls(tags);
            }
            if(unsortedReservation && self.isPersianCalendarType){
            	if(self.reservations !='undefined') {
	            	for(var reservLv1 = 0;reservLv1 < self.reservations.length;reservLv1++) {
	            		for(var reservLv2=0;reservLv2<self.reservations[reservLv1].segmentDetails.length;reservLv2++){
	            			var arrivalStringWithTime = PersianDateFormatService.reformatDateString(self.reservations[reservLv1].segmentDetails[reservLv2].arrivalDateTime,true);
	            			var departureStringWithTime = PersianDateFormatService.reformatDateString(self.reservations[reservLv1].segmentDetails[reservLv2].departureDateTime,true);
	            			
	            			var arrivateString = arrivalStringWithTime.split("-");
	            			var departureString = departureStringWithTime.split("-")
	            			
	                    	var jalaaliArrivalObject = dateTransformationService.toJalaali(parseInt(arrivateString[0]),parseInt(arrivateString[1]),parseInt(arrivateString[2]));
	                    	
	                    	var jalaaliDepartureObject = dateTransformationService.toJalaali(parseInt(departureString[0]),parseInt(departureString[1]),parseInt(departureString[2]));
	                    	
	                    	var arrivalTime = jalaaliArrivalObject.jd+"-"+jalaaliArrivalObject.jm+"-"+jalaaliArrivalObject.jy+" "+arrivateString[3];
	                    	
	                    	var departureTime = jalaaliDepartureObject.jd+"-"+jalaaliDepartureObject.jm+"-"+jalaaliDepartureObject.jy+" "+departureString[3];
	                    	
	                    	self.reservations[reservLv1].segmentDetails[reservLv2]['persianarrivalTime']=arrivalTime;
	                    	self.reservations[reservLv1].segmentDetails[reservLv2]['persiandepartureTime']=departureTime;
	                    	
	            		}
	            	}
            	}
            	
            }
        }


        function onlineCheckIn(event, pnr, segment) {
			var airportCode = getAirportCode(segment.orignNDest.split(' / ')[0]);
			var departureDate = moment(segment.departureDateTime, 'Do MMM YYYY hh:mm').format('YYYY-MM-DD');
			var webCheckIn = constantFactory.AppConfigStringValue("getOnlineCheckInURLText");
			webCheckIn += "?pnr=" + pnr + "&depDate=" + departureDate + "&depAirport=" + airportCode;
			$window.open(webCheckIn);
			event.stopPropagation();
		}

		function getAirportCode(airportName) {
			for (var index = 0; index < airportsList.length; index++) {

				if (airportsList[index].en == airportName) {
					return airportsList[index].code;
				}
			}
		}
		

		function isWebCheckInEnabled(segment) {
			var currentSystemDateTime = new Date();
			var tempCurrentDateTime = moment(currentSystemDateTime).format('DD MMM YYYY hh:mm');
			var tempDepartureDateTime = segment.departureDateTime;
			var formatCurrentDateTime = new Date(tempCurrentDateTime);
			var formatDepartureDateTime = new Date(tempDepartureDateTime);
			var timeDifference = formatDepartureDateTime.getTime() - formatCurrentDateTime.getTime();

			if (timeDifference > 0) {
				var diffHours = timeDifference / (60 * 60 * 1000);
				var webCheckInTimeDifference = constantFactory.AppConfigStringValue("getTimeDifferenceToDisplayOnlineCheckin");
				if (diffHours < webCheckInTimeDifference) {
					return true;
				}

			}

			return false;

		}

        /**
         *  If user signout from dashboard page, redirect user
         *  to sign in page.
         */
        eventListeners.registerListeners(eventListeners.events.CUSTOMER_LOGIN_STATUS_CHANGE, function () {

            if (!applicationService.getCustomerLoggedState()) {

                $state.go('signIn', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()
                });
            }

        });


        function joinAirewards(form) {

            if (!form.$valid) {
                return;
            }

            var LMSValidationParams = DTO.LMSValidationRequest;
            LMSValidationParams.emailId    = self.emailId;
            LMSValidationParams.firstName  = self.firstName;
            LMSValidationParams.lastName   = self.lastName;
            LMSValidationParams.headOfEmailId  = self.familyEmail;
            LMSValidationParams.refferedEmailId= self.referringEmail;
            LMSValidationParams.registration   = true;

            if (self.hasMashreqCard == "true") {
                var loyaltyProfileDetails = {};
                loyaltyProfileDetails.loyaltyAccountNo = self.accountNumber;
                loyaltyProfileDetails.city = self.city;
                loyaltyProfileDetails.dateOfBirth = self.mashreqDOB;
            }

            if(self.pageState == self.pageStates.UPDATE_LMS){
                //Call Mashreq registration for existing LMS users
                registrationService.mashreqRegister(loyaltyProfileDetails,
                    function mashreqRegisterSuccess(data) {
                        if(data.success){
                            alertService.setAlert("User details update successfully.", 'success');
                        }
                    }, function mashreqRegisterError(response){
                        if(response.error){
                            alertService.setAlert(response.error, 'danger');
                        }else{
                            alertService.setAlert("Mashreq registration failed", 'danger');
                        }
                    }
                )
            }else{
                registrationService.LMSValidation(LMSValidationParams, function (response) {
                    IBEDataConsole.log('lms validation is complete');
                    IBEDataConsole.log(response);

                    if(!response.success){
                        alertService.setAlert("Something went wrong, Please try again.", 'danger');
                        return;
                    }

                    if (response.accountExists) {
                        alertService.setAlert(response.messages[0], 'danger');
                        return;
                    }
                    else if (response.lmsNameMismatch) {
                        alertService.setAlert(response.messages[0], 'danger');
                        return;
                    }
                    else if (!response.existingFamilyHeadEmail && self.familyEmail) {
                        alertService.setAlert(response.messages[0], 'danger');
                        return;
                    }
                    else if (!response.existingRefferdEmail && self.referringEmail) {
                        alertService.setAlert(response.messages[0], 'danger');
                        return;
                    }

                    var LMSRegistrationParams = DTO.LMSRegistrationRequest;
                    LMSRegistrationParams.customerID = self.customerId;
                    LMSRegistrationParams.lmsDetails.dateOfBirth = self.airwardsDOB;
                    LMSRegistrationParams.lmsDetails.ffid = self.customerId;
                    LMSRegistrationParams.lmsDetails.headOFEmailId = self.familyEmail;
                    LMSRegistrationParams.lmsDetails.language = self.language;
                    LMSRegistrationParams.lmsDetails.passportNum = self.passport;
                    LMSRegistrationParams.lmsDetails.refferedEmail = self.referringEmail;

                    if (self.hasMashreqCard == "true") {
                        var loyaltyProfileDetails = {};
                        loyaltyProfileDetails.loyaltyAccountNo = self.accountNumber;
                        loyaltyProfileDetails.city = self.city;
                        loyaltyProfileDetails.dateOfBirth = self.mashreqDOB;
                        LMSRegistrationParams.loyaltyProfileDetails = loyaltyProfileDetails;
                    }

                    customerRemoteService.LMSRegistration(LMSRegistrationParams, function (response) {
                        IBEDataConsole.log('lms registration complete');
                        IBEDataConsole.log(response);

                        //TODO: instead of hardcoding, use a service to get logged in users details
                        // without providing password.
                        var userData = commonLocalStorageService.getUserSignInData();
                        userData.loggedInLmsDetails = {
                            availablePoints: 0,
                            ffid: userData.loggedInCustomerDetails.emailId,
                            emailConfirmed: false
                        };
                        userData.loyaltyManagmentEnabled = true;

                        signInService.setUserDataToLocalStorage(userData);

                        // Rearrange ui components.
                        init();

                        alertService.setAlert("LMS registration is successful.", "success");
                    }, function (error) {
                        IBEDataConsole.log('lms registration failed');
                        if(error.error){
                            alertService.setAlert(error.error, "danger");
                        }else{
                            alertService.setAlert("Something went wrong, Please try again.", "danger");
                        }
                    });

                }, function error(error) {
                    IBEDataConsole.log(error);
                    if(error.error){
                        alertService.setAlert(error.error, "danger");
                    } else {
                        alertService.setAlert("Something went wrong, Please try again.", "danger");
                    }
                });
            }
        }


        function bookFlight() {
            self.isModifyVisible = true;

            // Clear modify search section.
            var searchParams = {
                from: "",
                to: "",
                depDate: "N",
                retDate: "N",
                cabin: 'Y', // "Y":"Economy", "C":"Business"
                promoCode: "",
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency()
            };
            commonLocalStorageService.setSearchCriteria(searchParams);

            eventListeners.notifyListeners(eventListeners.events.SHOW_MODIFY_SEARCH);
        }


        /**
         *  Calculate year difference between provided date to set
         *  required state.
         */
        function setFamilyEmailStatus (dob) {
            if (!dob) {
                return;
            }

            self.isFamilyEmailRequired = moment().diff(moment(dob), 'years') < 12;
        }


        // Open calendar ?
        function dropdownOnClick($event,opened,isEnabled){
            if(isEnabled) {
                self[opened] = datePickerService.open();
            }
        }


        function setDashBoardUrls(urlList) {

            angular.forEach(urlList, function (tag, i) {

                var params = { "urlRef": tag };
                dashboardSvc.getUrlByTag(params, function (response) {

                    if (!response.success) {
                        return;
                    }

                    switch (response.urlRef) {
                        case "RETRO": {
                            self.claimMissingPointsUrl = response.url;
                        } break;
                        case "REDEEM": {
                            self.redeemPointsUrl = response.url;
                        } break;
                        case "FAQS": {
                            self.faqUrl = response.url;
                        } break;
                        case "EARN": {
                            self.earnPointsUrl = response.url;
                        } break;
                    };

                }, function (response) {

                });

            }); // Emd of forEach.
        }


        function redirectToReservation (pnr, lastName, departureDate) {
            var userData = commonLocalStorageService.getUserSignInData();
            if (_.isEmpty(userData)) {
                $state.go('signIn', {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency()
                });
            }else{
            	  // Go to login page if not logged in
                customerRemoteService.isUserLoggedIn({ loginId: userData.loggedInCustomerDetails.emailId },
                    function(response){
                        if(response.customerLoggedIn){
                            // Format date to YYYY-MM-DD.
                            departureDate = moment(departureDate, 'Do MMM YYYY hh:mm').format('YYYY-MM-DD');

                            // Redirect to reservation page.
                            // modify/reservation/:lang/:currency/:pnr/:lastName/:departureDate'
                            $state.go('modifyReservation', {
                                lang: languageService.getSelectedLanguage(),
                                currency: currencyService.getSelectedCurrency(),
                                mode: applicationService.getApplicationMode(),
                                pnr: pnr,
                                lastName: lastName,
                                departureDate: departureDate
                            });
                        }else{
                            goToLogin();
                            
                        }
                    },
                    function(error){
                        IBEDataConsole.log("Error getting logged in status");
                        goToLogin();
                    }
                );
            }

          

        } // End of redirectToReservation function.


        function goToLogin(){
        	localStorage.setItem("login_status",false);
            alertService.setAlert("Session expired, please log in again.", "warning");

            $state.go('signIn', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()}
            );
            $timeout(function () {
                $state.reload();
            }, 0);
        }


        function viewAllReservations() {
            $state.go('viewReservations',
                {lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()}
            );
        }


        function viewStatements() {
            $state.go('viewCreditHistory',
                {lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency(),
                    mode: applicationService.getApplicationMode()}
            );
        }


        function formatLabelLanguage(model) {
            var tempN = self.languageList;
            if (tempN) {
                for (var i = 0; i < tempN.length; i++) {
                    if (model === tempN[i].languageCode) {
                        return tempN[i].language;
                        break;
                    }
                }
            }
        }


        self.onLanguageChange = function (item) {
        };


        /**
         *  Hide page loading.
         */
        function hidePageLoading() {
            loadingService.hidePageLoading();
        }


    } // End of dashboardController function.

})(angular); // End of IIFE.
