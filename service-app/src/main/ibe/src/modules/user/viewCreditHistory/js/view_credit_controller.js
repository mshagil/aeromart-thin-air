/**
 *      Created by Dushyantha
 *      05.02.2016
 *      viewCreditHistory controller.
 */
(function (argument) {

    angular
        .module("viewCreditHistory", [])
        .controller("viewCreditHistoryCtrl", ["$state","signInService", "languageService", "currencyService", "IBEDataConsole",
            "alertService","userSignInRemoteService", "applicationService", "commonLocalStorageService", "constantFactory","$rootScope", viewCreditHistoryCtrl]);

    function viewCreditHistoryCtrl($state, signInService, languageService, currencyService, IBEDataConsole,alertService,userSignInRemoteService,applicationService, commonLocalStorageService, constantFactory, $rootScope) {
        var self = this;
        self.isUserLoggedIn;
        self.airlineCredits;
        this.creditHistoryAllData ='';
        self.callCenterLink = constantFactory.AppConfigStringValue('secureServiceAppURL') + '/call-centre';
        if(!$rootScope.deviceType.isMobileDevice){
            
            $rootScope.removePaddingFromXSContainerClass=true;
        }
        
        var init = function(){

            // Hide backToHome url on page if user logged in. A back to home
            // url is available in login bar.
            self.isUserLoggedIn = signInService.getLoggedInStatus();

            // Set airline credits.
            var userData = commonLocalStorageService.getUserSignInData();
            self.airlineCredits = !_.isEmpty(userData) ? userData.totalCustomerCredit : 0;

            userSignInRemoteService.viewCreditHistory('', function (creditData) {

                self.creditHistoryAllData = creditData;
                setPagination();
            }, function (error) {
                alertService.setPageAlertMessage(error);
            });
        }();

        this.redirectToReservation = function (pnr, lastName, departureDate) {
            // Format date to YYYY-MM-DD.
            departureDate = moment(departureDate, 'Do MMM YYYY hh:mm').format('YYYY-MM-DD');

            // Redirect to reservation page.
            // modify/reservation/:lang/:currency/:pnr/:lastName/:departureDate'
            $state.go('modifyReservation', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode(),
                pnr: pnr,
                lastName: lastName,
                departureDate: departureDate
            });
        };
        this.backToDashboard  = function(){
            $state.go('modifyDashboard', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
        };

        function setPagination(){
            var itemsPerPage = 10;
            var startingIndex = 1;
            var endIndex = itemsPerPage;
            self.startingIndex  = startingIndex;
            self.endingIndex  = endIndex;
            self.totalRecords = self.creditHistoryAllData.length;
            self.isPrev = false;
            self.isNext = false;
            if(self.creditHistoryAllData.length>itemsPerPage){
                self.isNext =true;
            }
            loadPageData();
            self.prevPage = function () {
                if(self.isPrev && self.startingIndex !=1){
                    startingIndex= self.startingIndex-itemsPerPage;
                    endIndex = startingIndex+(itemsPerPage-1);
                    if(startingIndex=== 1){
                        self.isPrev = false;
                    }
                    else{
                        self.isPrev =true;
                    }
                    self.isNext = true
                    self.startingIndex = startingIndex;
                    self.endingIndex  = endIndex;
                    loadPageData();
                }

            };
            self.nextPage = function () {
                if(self.isNext && self.endingIndex !=self.totalRecords){
                    startingIndex= self.endingIndex+1;
                    endIndex = startingIndex+(itemsPerPage-1);
                    if(endIndex>self.totalRecords){
                        endIndex = self.totalRecords;
                        self.isNext =false;

                    }
                    else{
                        self.isNext = true;
                    }
                    self.isPrev = true;
                    self.startingIndex = startingIndex;
                    self.endingIndex  = endIndex;
                    loadPageData();
                }

            };



        }
        function loadPageData(){
            self.creditHistoryPageData=self.creditHistoryAllData.slice((self.startingIndex-1), (self.endingIndex))
        }


    }

})();
