/**
 *      Created by Chamil on 08/02.2016
 *      Registration module.
 */

 (function () {

     angular
        .module("registration")
        .controller("registrationCtrl", [
            "$state",
            "$location",
            "loadingService",
            "customerRemoteService",
            "userProfileRemoteService",
            "alertService",
            "registrationService",
            "datePickerService",
            "constantFactory",
            "languageService",
            "currencyService",
            "applicationService",
            "DTO",
            "signInService",
            "commonLocalStorageService",
            "IBEDataConsole",
            "remoteMaster",
            "popupService",
            "dateTransformationService",
            'formValidatorService',
            'signoutServiceCheck',
            regController]);

    function regController($state, $location, loadingService, customerRemoteService, userProfileRemoteService,
                           alertService, registrationService, datePickerService, constantFactory, languageService,
                           currencyService, applicationService, DTO, signInService, commonLocalStorageService,
                           IBEDataConsole, remoteMaster, popupService,dateTransformationService, formValidatorService,signoutServiceCheck) {

        var self = this;

        /**
         * This controller and template is used for multiple page states, these are set at initialization
         * Viewport elements will be controlled based on this state
         *
         * REGISTER: Register as new user
         * UPDATE: Update user without LMS or Mashreq
         * UPDATE_LMS: Update user with LMS but without Mashreq
         * UPDATE_LMS_MASHREQ: Update user with LMS and Mashreq
         * */
        self.pageStates = {
            "REGISTER": "register",
            "UPDATE": "update_nolms",
            "UPDATE_LMS": "update_lms",
            "UPDATE_LMS_MASHREQ": "update_lms_mashreq"
        };

        // Show topNav.
        loadingService.showPageLoading();
        var path = $location.path().split("/")[1];
        if(path !== 'registration' && path !== 'resetPassword')
        	signoutServiceCheck.checkSessionExpiry();

        // Get FORM VALIDATIONS
        self.formValidation = formValidatorService.getValidations();
        
        //TODO: Move to a DTO
        self.classInput = "";
        self.customerId = "";
        self.password;
        self.namePhoneField = 'phone';
        self.salutation;
        self.firstName;
        self.lastName;
        self.nationality;
        self.nationalityName;
        self.zipCode;
        self.country;
        self.mobile = {
            countryCode: "",
            areaCode:"",
            number: ""

        };
        self.phone = {
            countryCode: "",
            areaCode:"",
            number: ""

        };
        self.fax = {
            countryCode: "",
            areaCode:"",
            number: ""

        };

        self.hasJoinAirwards = "true";
        self.isLmsMember = false; // Set to true only when updating the profile of existing member
        self.lmsEmail = null;

        self.airwardsDOB;
        self.language = languageService.getSelectedLanguage();
        self.passport;
        self.referringEmail;
        self.familyEmail; // headOFEmailId
        self.selectedLanguage = languageService.getSelectedLanguage();

        self.hasMashreqCard = "false"; // Checkbox value Mashreq card

        // DTO for Mashreq loyalty
        self.loyaltyProfileDetails = {
            loyaltyAccountNo: null,
            dateOfBirth: null,
            city: null
        };

        // DTO for customerQuestionaire
        // Default values from previous IBE defaults
        self.customerQuestionaire = [
            null,
            null, // Promo material checkbox
            null, // Promo email type
            null, // SMS preference
            null, // Language
            null  // Custom language
        ];

        self.yourPrefsAvailable = false; // Visibility of the Your Preferences section

        self.secretQuestion = null;
        self.secretAnswer = null;
        self.alternativeEmailId = null;

        self.contactConfig = {}; // Configuration for registration fields that are visible and mandatory

        // Since registration page is used to as update profile page
        // and LMS Registration page, we need to keep the page status.
        self.isUpdateProfilePage = false;

        // Collections for typeahead controllers.
        self.paxTypeWiseSalutation = constantFactory.PaxTypeWiseTitle();
        self.salutationList = self.paxTypeWiseSalutation["AD"];
        //self.salutationList = constantFactory.PaxTitles();
        self.nationalityList = constantFactory.Nationalities();
        self.countryList = constantFactory.Countries();
        self.languageList = constantFactory.Langauges();
        self.carrierCode= constantFactory.AppConfigStringValue('defaultCarrierCode');
        self.isPersianCalendarType = applicationService.getIsPersianCalendar();


        self.dateformat = datePickerService.setDateFormat(4); // Holds date format for calendar controllers.
        self.isFamilyEmailRequired = false; // Holds validation require status for Family Head Email field.
        self.moreDetails = "https://airewards.airarabia.com/portal/"; // Holds url for the more details page.
        self.isSendPromoEmailChecked = true; // checkbox for promo email list .


        self.register = register; // Function for registering and updating users
        self.setFamilyEmailStatus = setFamilyEmailStatus;
        self.dpOnClick = dropdownOnClick;
        self.setSalutation = setSalutation;
        self.backToDashboard = backToDashboard;
        self.changePassword = changePassword;

        self.isLoaderVisible = false;
        self.isResetPassword = false;

        // Properties for JqueryUi date picker
        self.maxDate = angular.copy(moment().subtract(2, 'years')).toDate();
        self.minDate = angular.copy(moment().subtract(99, 'years').add(1, 'days')).toDate();
        self.dateOptions = {
            changeYear: true,
            changeMonth: true,
            minDate: self.minDate,
            maxDate: self.maxDate,
            dateFormat: 'dd/mm/yy',
            weekHeader: "Wk",
            yearRange: "-100:+0",
            showOn: "both",
            buttonImage: "images/calendar.png",
            disabled: self.isLmsMember
        };
        
        //Properties for Jalali calendar
        self.jalaaliObjectForMaxDate = dateTransformationService.toJalaali(self.maxDate.getFullYear(), self.maxDate.getMonth(), self.maxDate.getDate());
        self.jalaaliObjectForMinDate = dateTransformationService.toJalaali(self.minDate.getFullYear(), self.minDate.getMonth(), self.minDate.getDate());
        self.defautlValueForJalDob = self.jalaaliObjectForMaxDate.jd+"/"+self.jalaaliObjectForMaxDate.jm+"/"+self.jalaaliObjectForMaxDate.jy;
        self.maxDateForJal = "\'" + self.jalaaliObjectForMaxDate.jd + "/" + self.jalaaliObjectForMaxDate.jm + "/" + self.jalaaliObjectForMaxDate.jy + "\'";
        self.minDateForJal = "\'" + self.jalaaliObjectForMinDate.jd + "/" + self.jalaaliObjectForMinDate.jm + "/" + self.jalaaliObjectForMinDate.jy + "\'";
        self.admOptions = {
            calType: "jalali",
            dtpType: "date",
            zIndex: 9,
            autoClose: true,
            format: "DD/MM/YYYY",
            multiple: false,
            default: self.defautlValueForJalDob,
        };
                        	
        /**
         *  Get logged in user profile Information.
         */
        function getUserProfileInformation() {

            self.isLoaderVisible = true; // Show loader till data loading for the form.

            // Get logged in users details.
            registrationService.getUserProfileData({}, function (response) {

                // Hide loader after data loading.
                self.isLoaderVisible = false;

                if (!response.success) {
                    return;
                }

                self.customerDateOfBirth = response.customer.dateOfBirth;
                self.customerId = response.customer.customerContact.emailAddress;
                self.alternativeEmailId = response.customer.customerContact.alternativeEmailId;
                self.customerId = (self.customerId).toLowerCase();
                self.password = response.customer.password;
                self.confirmPassword = response.customer.password;
                self.firstName = response.customer.firstName;
                self.lastName = response.customer.lastName;
                self.nationality = response.customer.nationalityCode + ""; // convert int value to string.
                self.zipCode = response.customer.customerContact.address.zipCode;
                self.country = response.customer.countryCode;

                self.customerQuestionaire = response.customer.customerQuestionaire;

                self.secretAnswer = response.customer.secretAnswer;
                self.secretQuestion = response.customer.secretQuestion;
                self.salutation = getSalutationByTitle(response.customer.title).titleCode;                
                if(response.customer.customerContact.mobileNumber){
                    var mobileDetails = response.customer.customerContact.mobileNumber;
                    self.mobile = {
                        countryCode: mobileDetails.countryCode,
                        number: mobileDetails.areaCode + mobileDetails.number
                    };
                }

                self.fax = response.customer.customerContact.fax;
                self.phone = response.customer.customerContact.telephoneNumber;
                self.isSendPromoEmailChecked = response.customer.sendPromoEmail;

                if (!_.isEmpty(response.lmsDetails)) {
                    if(signInService.getMashreqStatus() !== "false"){
                        self.pageState = self.pageStates.UPDATE_LMS_MASHREQ;
                    }else{
                        // Set state as UPDATE_LMS if update user has lmsDetails
                        self.pageState = self.pageStates.UPDATE_LMS;
                    }

                    self.hasJoinAirwards = "true";
                    self.isLmsMember = true;
                    self.lmsEmail = response.lmsDetails.ffid;
                    self.airwardsDOBlocal = new Date(response.lmsDetails.dateOfBirth);
                    self.language = response.lmsDetails.language;
                    self.passport = response.lmsDetails.passportNum;
                    self.referringEmail = response.lmsDetails.refferedEmail;
                    self.familyEmail = response.lmsDetails.headOFEmailId;
                }
                else {
                    self.hasJoinAirwards = "false";
                }

                if (!_.isEmpty(response.mashreqDetails)) {
                    self.hasMashreqCard = "true";
                    self.accountNumber = response.mashreqDetails.accountNumber;
                    self.mashreqDOB = new Date(response.mashreqDetails.dateOfBirth);
                    self.city = response.mashreqDetails.city;
                }
                else {
                    self.hasMashreqCard = "false";
                }

            }, function (response) {
                self.isLoaderVisible = false;
                alertService.setAlert(response.error, "danger")
            });

        } // End of getUserProfileInformation.


        /**
         *  Calculate year difference between provided date to set
         *  required state.
         */
        function setFamilyEmailStatus (dob) {
            if (!dob) {
                return;
            }
            self.isFamilyEmailRequired = moment().diff(moment(dob), 'years') < 12;
        }


        // Open calendar ?
        function dropdownOnClick($event,opened,isEnabled){
            if(isEnabled) {
                self[opened] = datePickerService.open();
            }
        }


        /**
         *  Set salutation value.
         */
        function setSalutation(salutation) {
            self.salutation = salutation;
        }


        /**
         *  Typeahead formatting functions.
         */
        self.formatLabelNationality = function (model) {
            var tempN = self.nationalityList;
            if (tempN) {
                for (var i = 0; i < tempN.length; i++) {
                    if (model === tempN[i].code) {
                        self.nationalityName = tempN[i].name;
                        return tempN[i].name;
                        break;
                    }
                }
            }
        };
        self.formatLabelCountry = function (model) {
            var tempN = self.countryList;
            if (tempN) {
                for (var i = 0; i < tempN.length; i++) {
                    if (model === tempN[i].code) {
                        return tempN[i].name;
                        break;
                    }
                }
            }
        };
        self.formatLabelLanguage = function (model) {
            var tempN = self.languageList;
            if (tempN) {
                for (var i = 0; i < tempN.length; i++) {
                    if (model === tempN[i].languageCode) {
                        return tempN[i].language;
                        break;
                    }
                }
            }
        };


        /**
         *  Typeahead on change functions.
         */
        self.onNationalityChange = function (item) {
        };
        self.onLanguageChange = function (item) {
        };
        self.onCountryChange = function (item) {
            //Resets mobile number to reflect the coutnry code
            self.mobile.countryCode = item.phoneCode;

            if(self.fax){
                self.fax.countryCode = item.phoneCode;
            }else{
                self.fax = {
                    countryCode: item.phoneCode,
                    number: ""
                }
            }
        };
        self.isValidCareerFor = function (airlineName){
               return applicationService.isValidCareerFor(airlineName);
        }               


        /**
         *  Form submit function.
         */
        function register(form) {

            if (!form.$valid) {
            	if(form.email.$invalid){
            		angular.element( document.querySelector( '#email' )).focus();
            		window.scroll(0,300);
            	}
            	else if(form.password.$invalid){
            		angular.element( document.querySelector( '#password' )).focus();
            		window.scroll(0,300);
            	}
            	else if(form.confirmPassword.$invalid){
            		angular.element( document.querySelector( '#confirmPassword' )).focus();
            		window.scroll(0,300);
            	}
            	else if(form.salutation.$invalid){
            		angular.element( document.querySelector( '#salutation-button' )).focus();
            		window.scroll(0,300);
            	}
            	else if(form.firstName.$invalid){
            		angular.element( document.querySelector( '#firstName' )).focus();
            		window.scroll(0,300);
            	}
            	else if(form.lastName.$invalid){
            		angular.element( document.querySelector( '#lastName' )).focus();
            		window.scroll(0,400);
            	}
            	else if(form.nationality.$invalid){
            		angular.element( document.querySelector( '#nationality' )).focus();
            		window.scroll(0,400);
            	}
            	else if(form.country.$invalid){
            		angular.element( document.querySelector( '#country' )).focus();
            		window.scroll(0,500);
            	}
            	else if(form.telephone.$invalid){
            		angular.element( document.querySelector( '#telNumber' )).focus();
            		window.scroll(0,500);
            	}
            	else if(form.airwardsDOB.$invalid){
            		angular.element( document.querySelector( '#airewards-dob' )).focus();
            	}
            	else if(form.refEmail.$invalid){
            		angular.element( document.querySelector( '#refEmail' )).focus();
            	}
            	else if(form.familyEmail.$invalid){
            		angular.element( document.querySelector( '#familyEmail' )).focus();
            	}       	
            	
                return;
            }
            
            window.scroll(0,0);

            // Save or update user details.
            setUserDetails(self.isUpdateProfilePage);

        } // End of register.

        /**
         * Form submit function for chage password.
         * @param form
         */
        function changePassword(form) {
            if (!form.$valid) {
                return;
            }

            var params = {
                currentPassword : self.password,
                newPassword: self.newPassword,
                confirmPassword: self.confirmPassword,
                passwordResetToken: self.passwordResetToken
            };

            registrationService.changePassword(params, function (data) {
                // on success.
                var message;
                if (data.success) {
                    message = self.isResetPassword ? "msg_passwordResetSuccess" : "msg_passwordChangeSuccess";
                    alertService.setAlert(message, 'success');
                }
                if (self.isResetPassword) {
                    $state.go('signIn',{
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency()
                    });
                } else {
                    self.backToDashboard();
                }

            }, function (error) {
                // on error.
                alertService.setAlert(error.error, 'danger');
            });
        }

        /**
         *  Set user DTO.
         */
        function setUserDetails(isUpdate) {

            var hasJoinAirwards = self.hasJoinAirwards === "true" && self.isLoyaltyManagementEnabled;
            var hasMashreqCard = self.hasMashreqCard === "true";
            // var airWardsDOB = new Date(moment(self.airwardsDOB).locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"));

            var lmsDetails = {
                "dateOfBirth": self.airwardsDOB,
                "ffid": self.customerId,
                "headOFEmailId": self.familyEmail,
                "language": self.language,
                "passportNum": self.passport,
                "refferedEmail": self.referringEmail,
                "appCode" : "APP_IBE"
            };
            
            // Move to DTO - other registration - passenger page.
            var params = {
                "customer": {
                    "title": self.salutation,
                    "firstName": self.firstName,
                    "lastName": self.lastName,
                    "customerId": self.customerId,
                    "password": self.password,
                    "gender": null,
                    "dateOfBirth": self.customerDateOfBirth,
                    "countryCode": self.country,
                    "nationalityCode": self.nationality,
                    "nationalityName": self.nationalityName,
                    "status": null,
                    "secretQuestion": self.secretQuestion,
                    "secretAnswer": self.secretAnswer,
                    "customerQuestionaire": self.customerQuestionaire,
                    "customerContact": {
                        "address": {
                            "addresline": null,
                            "addresStreet": null,
                            "city": null,
                            "zipCode": self.zipCode
                        },
                        "alternativeEmailId": self.alternativeEmailId,
                        "emailAddress": self.customerId,
                        "mobileNumber": {
                            "countryCode": self.mobile.countryCode,
                            "areaCode": self.mobile.number ? self.mobile.number.substring(0,2) : "",
                            "number": self.mobile.number ? self.mobile.number.substring(2,self.mobile.number.length) : ""
                        },
                        "telephoneNumber": self.phone,
                        "fax": self.fax,
                        "emergencyContact": null
                    },
                    "sendPromoEmail": self.isSendPromoEmailChecked
                },
                "lmsDetails": hasJoinAirwards ? lmsDetails : null, //TODO: when loging in as LMS user check status
                "loyaltyProfileDetails": hasMashreqCard ? self.loyaltyProfileDetails : null,
                "optLMS": hasJoinAirwards
            };

            var LMSValidationReq = {
                emailId: self.customerId,
                firstName: self.firstName,
                lastName: self.lastName,
                headOfEmailId: self.familyEmail,
                refferedEmailId: self.referringEmail,
                registration: true
            };

            if (self.isUpdateProfilePage) {
                LMSValidationReq.registration = false;
                updateUser(LMSValidationReq, params);
            }
            else {
                registerUser(LMSValidationReq, params);
            }

        }

        // Register user service call.
        function registerUserServiceCall(userParams) {

            registrationService.registerUser(userParams, function (data) {
                // on success.
                if (!data.success) {
                    return;
                }

                var isUserJoinedLMS = self.isLoyaltyManagementEnabled && !_.isEmpty(userParams.lmsDetails);

                // logic.
                redirect(true, isUserJoinedLMS);

            }, function (error) {
                // on error.
                alertService.setAlert(error.error, 'danger');
            });
        }

        /**
         *  User registration function.
         */
        function registerUser(LMSParams, userParams) {
            customerRemoteService.validateLMSDetails(LMSParams, function (response) {

                if(!response.success){
                    return;
                }

                if(response.lmsNameMismatch){
                    alertService.setAlert(response.messages[0], 'warning');
                    return;
                }

                if (self.hasJoinAirwards === "true") {
                    if(!response.existingFamilyHeadEmail && LMSParams.headOfEmailId){
                        alertService.setAlert(response.messages[0], 'warning');
                        return;
                    }
                    else if(!response.existingRefferdEmail && LMSParams.refferedEmailId){
                        alertService.setAlert(response.messages[0], 'warning');
                        return;
                    }

                    if (response.accountExists) {
                        // Show dialog and continue to lms registration
                        popupService.confirm("The account with this Airewards ID already exists. If you want to merge please click Ok.", function () {
                            registerUserServiceCall(userParams);
                        });
                        return;
                    }
                }

                commonLocalStorageService.setUserRegistrationDetails(userParams);

                registerUserServiceCall(userParams);

            },function(error){
                alertService.setAlert(error.error, 'danger');
            }); // End of validateLMSDetails controller.

        } // End of registerUser.


        function updateUserServiceCall(userParams) {
            // Update user service call.
            registrationService.updateUser(userParams, function (data) {
                if (!data.success) {
                    self.isLoaderVisible = false;
                    alertService.setAlert("User registration failed.", 'danger');
                    return;
                }
                self.isLoaderVisible = false;
                var updateSuccessMessage = "User details update successfully.";
                alertService.setAlert(updateSuccessMessage, 'success');

                var userData = commonLocalStorageService.getUserSignInData();
                userData.loggedInCustomerDetails.title = userParams.customer.title;
                userData.loggedInCustomerDetails.country = userParams.customer.countryCode;
                userData.loggedInCustomerDetails.nationality = userParams.customer.nationalityCode;
                userData.loggedInCustomerDetails.nationalityName = userParams.customer.nationalityName;
                userData.loggedInCustomerDetails.mobile = userParams.customer.customerContact.mobileNumber;
                userData.loggedInCustomerDetails.sendPromoEmail = userParams.customer.sendPromoEmail;
                if (userParams.lmsDetails) {
                    userData.loggedInLmsDetails.ffid = userParams.lmsDetails.ffid;
                }

                // Set updated user properties to local storage.
                signInService.setUserDataToLocalStorage(userData);


                if (self.pageState === self.pageStates.UPDATE_LMS && self.hasMashreqCard == "true") {
                    registrationService.mashreqRegister(self.loyaltyProfileDetails,
                        function mashreqRegisterSuccess(data) {
                            if (data.success) {
                                alertService.setAlert("User details update successfully.", 'success');
                            }
                        }, function mashreqRegisterError(response) {
                            if (response.error) {
                                response.error =
                                    "<ul>" +
                                    "<li>" + updateSuccessMessage + "</li>" +
                                    "<li>" + response.error + "</li>" +
                                    "</ul>";
                                alertService.setAlert(response.error, 'warning');
                            } else {
                                alertService.setAlert("Mashreq registration failed", 'danger');
                            }
                        }
                    )
                }

            }, function (error) {
                self.isLoaderVisible = false;
                alertService.setAlert(error.error, 'danger');
            });
        }

        /**
         *  User update function.
         */
        function updateUser(LMSParams, userParams) {
            if(userParams.customer.customerContact.emailAddress){
                userParams.customer.customerContact.emailAddress = (userParams.customer.customerContact.emailAddress).toUpperCase();
            }

            self.isLoaderVisible = true;

            if(self.hasJoinAirwards === "true"){
                customerRemoteService.validateLMSDetails(LMSParams, function (response) {

                    if(!response.success){
                        return;
                    }

                        if(response.existingFamilyHeadEmail && !LMSParams.headOfEmailId){
                            alertService.setAlert(response.messages[0], 'warning');
                            return;
                        }
                        else if(response.existingRefferdEmail && !self.referringEmail){
                            alertService.setAlert(response.messages[0], 'warning');
                            return;
                        }
                    updateUserServiceCall(userParams);


                },function(error){
                    alertService.setAlert(error.error, 'danger');
                    self.isLoaderVisible = false;
                }); // End of validateLMSDetails controller.
            }else{
                updateUserServiceCall(userParams);
            }

            if(self.hasJoinAirwards === "false"){
                self.hasJoinAirwards = false;
            }

            if(self.hasJoinAirwards && !self.isLmsMember){
                joinAirewards();
            }

        } // End of updateUser.


        function lmsRegisterServiceCall(LMSRegistrationParams) {
            customerRemoteService.LMSRegistration(LMSRegistrationParams, function (response) {
                IBEDataConsole.log('lms registration complete');
                IBEDataConsole.log(response);

                //TODO: instead of hardcoding, use a service to get logged in users details
                // without providing password.
                var userData = commonLocalStorageService.getUserSignInData();
                userData.loggedInLmsDetails = {
                    availablePoints: 0,
                    ffid: userData.loggedInCustomerDetails.emailId,
                    emailConfirmed: false
                };
                userData.loyaltyManagmentEnabled = true;

                signInService.setUserDataToLocalStorage(userData);

                self.lmsEmail = userData.loggedInCustomerDetails.emailId;

                alertService.setAlert("LMS registration is successful.", "success");
            }, function (error) {
                IBEDataConsole.log('lms registration failed');
                alertService.setAlert("Something went wrong, Please try again.", "danger");
            });
        }

        function joinAirewards() {

            var LMSValidationParams = DTO.LMSValidationRequest;

            LMSValidationParams.emailId    = self.customerId;
            LMSValidationParams.firstName  = self.firstName;
            LMSValidationParams.lastName   = self.lastName;
            LMSValidationParams.headOfEmailId  = self.familyEmail;
            LMSValidationParams.refferedEmailId= self.referringEmail;
            LMSValidationParams.registration   = true;

            registrationService.LMSValidation(LMSValidationParams, function (response) {
                IBEDataConsole.log('lms validation is complete');
                IBEDataConsole.log(response);

                var LMSRegistrationParams = DTO.LMSRegistrationRequest;
                LMSRegistrationParams.customerID = self.customerId;
                LMSRegistrationParams.lmsDetails.dateOfBirth = self.airwardsDOB;
                LMSRegistrationParams.lmsDetails.ffid = self.customerId;
                LMSRegistrationParams.lmsDetails.headOFEmailId = self.familyEmail;
                LMSRegistrationParams.lmsDetails.language = self.language;
                LMSRegistrationParams.lmsDetails.passportNum = self.passport;
                LMSRegistrationParams.lmsDetails.refferedEmail = self.referringEmail;

                if(!response.success){
                    alertService.setAlert("Something went wrong, Please try again.", 'danger');
                    return;
                }

                if (response.accountExists) {
                    popupService.confirm("The account with this Airewards ID already exists. If you want to merge please click Ok.", function () {
                        lmsRegisterServiceCall(LMSRegistrationParams);
                    });
                    return;
                }
                else if (response.lmsNameMismatch) {
                    alertService.setAlert(response.messages[0], 'danger');
                    return;
                }
                else if (response.existingFamilyHeadEmail) {
                    alertService.setAlert(response.messages[0], 'danger');
                    return;
                }
                else if (response.existingRefferdEmail) {
                    alertService.setAlert(response.messages[0], 'danger');
                    return;
                }



                if(self.pageState === self.pageStates.UPDATE && self.hasMashreqCard == "true"){
                    LMSRegistrationParams.loyaltyProfileDetails = self.loyaltyProfileDetails;
                }
                lmsRegisterServiceCall(LMSRegistrationParams);

            }, function error(error) {
                IBEDataConsole.log(error);
                alertService.setAlert("Something went wrong, Please try again.", "danger");
            });
        }


        /**
         *  Back button click function.
         */
        function backToDashboard() {

            // Change stage to modifyDashboard page.
            $state.go('modifyDashboard', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });
        }


        /**
         *  Redirect user after registration service call
         *  completed.
         */
        function redirect(isLogginSuccess, isUserJoinedLMS) {

            $state.go('redirect', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                success: isLogginSuccess,
                isJoinedLMS: isUserJoinedLMS
            });
        }


        /**
         *  Get salutation using salutation code.
         */
        function getSalutationByTitle(code) {
            if (!self.salutationList || code === undefined) {
                return;
            }
            for (var i = 0; i < self.salutationList.length; i++) {
                if (self.salutationList[i].titleCode == code) {
                    return self.salutationList[i];
                    break;
                }
            }
        }

        /**
         *  Set secret question from to the registeration model.
         */
        self.setSecretQuestion = function(secretQuestion){
            self.secretQuestion = secretQuestion.key;
        };


        /**
         *  Get the 2D array of security question and transform to key value pair.
         */
        function setSecurityQuestions(questionResponse) {
            self.secretQuestionList = _.reduce(questionResponse, function(base, question){
                var questionObject = {
                    'key': question[0],
                    'value': question[1]
                };
                base.push(questionObject);
                return base;
            }, []);
        }


        /**
         * Check whether the LMS form should be visible
         * Visible when in pageStates, UPDATE_LMS_MASHREQ or UPDATE_LMS AND
         * When in REGISTER or UPDATE pageStates with hasJoinAirewards checked
         */
        self.lmsFormVisible = function(){
            if(self.pageState === self.pageStates.REGISTER || self.pageState === self.pageStates.UPDATE){
                if(!self.isLoyaltyManagementEnabled){
                    return false;
                }
                return self.hasJoinAirwards === 'true';
            }else{
                return true;
            }
        };


        /**
         * Check whether the Mashreq heading should be visible
         * When in REGISTER or UPDATE pageStates with hasJoinAirewards checked AND
         * When in pageState UPDATE_LMS
         * Disabled when pageState is UPDATE_LMS_MASHREQ
         */
        self.mashreqHeaderVisible = function(){
            if(self.pageState === self.pageStates.REGISTER || self.pageState === self.pageStates.UPDATE){
                return self.hasJoinAirwards === 'true';
            }else if(self.pageState === self.pageStates.UPDATE_LMS){
                return true;
            }else{
                return false;
            }
        };

        // Hide topNav.
        loadingService.hidePageLoading();

        /**
         *  Filter typeaheads according to view value.
         */
        self.startsWith = function(state, viewValue) {

            if(!viewValue || viewValue === " "){
                return true;
            }else{
                return state.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
            }
        };
        
        /**
         *  Init function.
         */
        (function () {
            // Retrieve field configuration required by registration and upgrade flow
            self.isLoyaltyManagementEnabled = currencyService.getLoyaltyManagementStatus();
            remoteMaster.retrivePassengerRegistrationParams({}).then(function(response){
                if(self.carrierCode != 'W5'){
                    self.phoneNumberConfigs ={countryCode:true,areaCode:false,phoneNumber:true}
                }else{
                    self.phoneNumberConfigs ={countryCode:true,areaCode:false,phoneNumber:true}
                }

                // Adapting contact configurations to scope
                self.contactConfig = _.reduce(response.data.configList, function(base, value){
                    base[value.fieldName] = value;
                    return base;
                }, {}) ;

                // Setting security questions
                if(response.data.questionInfo){
                    setSecurityQuestions(response.data.questionInfo);
                }

                // Setting yourPrefsAvailablity to true if any of the pref fields are enabled
                if(self.contactConfig.promotionPref.userRegVisibilty
                    || self.contactConfig.emailPref.userRegVisibilty
                    || self.contactConfig.smsPref.userRegVisibilty
                    || self.contactConfig.languagePref.userRegVisibilty){
                    self.yourPrefsAvailable = true;
                }

                if ($state.current.data && ($state.current.data.isUpdateProfile || $state.current.data.isChangePassword
                    || $state.current.data.isResetPassword)) {

                    if($state.current.data.isUpdateProfile) {
                        self.isUpdateProfilePage = true; // Hide registration page content.
                        // Set state as UPDATE if is profile update
                        self.pageState = self.pageStates.UPDATE;
                        getUserProfileInformation();
                    }

                    if ($state.current.data.isChangePassword) {
                        self.customerId = commonLocalStorageService.getUserSignInData().
                            loggedInCustomerDetails.emailId.toLowerCase();
                    }

                    if ($state.current.data.isResetPassword && $state.params.token) {
                        self.isResetPassword = true;
                        self.isLoaderVisible = true;
                        self.passwordResetToken = $state.params.token;
                        registrationService.getEmailFromToken($state.params.token, function (data) {
                            // on success.
                            self.customerId = data.email;
                            self.isLoaderVisible = false;
                        }, function (error) {
                            // on error.
                            self.isLoaderVisible = false;
                            alertService.setAlert(error.error, 'danger');
                        });
                    }
                }else{
                    // Set the page status to REGISTER by default
                    self.pageState = self.pageStates.REGISTER;

                    // Setting default values for the REGISTER pageState
                    if(self.contactConfig.promotionPref.userRegVisibilty){
                        self.customerQuestionaire[1] = true;
                    }if(self.contactConfig.emailPref.userRegVisibilty){
                        self.customerQuestionaire[2] = 'HTML';
                    }if(self.contactConfig.smsPref.userRegVisibilty){
                        self.customerQuestionaire[3] = 'No';
                    }if(self.contactConfig.languagePref.userRegVisibilty){
                        self.customerQuestionaire[4] = 'en';
                    }
                }
            });

        })();

    } // End of regController.

 })();
