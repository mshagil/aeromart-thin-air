/**
 *      Created by Chamil on 08/02.2016
 *      Registration module, redirection controller.
 */

 (function () {

     angular
        .module("registration")
        .controller("redirectionCtrl", ["$timeout", '$state', "$stateParams", 'languageService', 'currencyService', 'applicationService', "loadingService","constantFactory", redirectionController]);

    function redirectionController($timeout, $state, $stateParams, languageService, currencyService, applicationService, loadingService,constantFactory) {
        var self = this;

        // Show topNav.
        loadingService.showPageLoading();

        self.isLoginSuccess = $stateParams.success;
        self.isJoinedLMS = $stateParams.isJoinedLMS;
        self.redirectToSignIn = redirectToSignIn;
        self.logoLink = constantFactory.AppConfigStringValue('defaultAirlineURL');
        self.isConfirmEnabled = constantFactory.AppConfigStringValue('customerRegisterConfirmation');


        function redirectToSignIn() {

            if (!self.isLoginSuccess) {
                return;
            }

            $state.go('signIn', {
                lang: languageService.getSelectedLanguage(),
                currency: currencyService.getSelectedCurrency(),
                mode: applicationService.getApplicationMode()
            });

        } // End of redirectToSignIn.


        // Hide topNav.
        loadingService.hidePageLoading();

    } // End of redirection controller.

})();
