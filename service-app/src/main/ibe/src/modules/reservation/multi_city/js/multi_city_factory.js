'use strict';
/**
 * Created by nipuna on 12/23/16.
 */

var multiCityFactory = function(){
    var factory = {};
    factory.multiCityFactory = [
        function(){
            var journeyInfoSegment = function() {
                this.origin = null;
                this.destination = null;
                this.departureDateTime = null;
                this.departureVariance = 3;
            };

            return {
                JourneyInfoSegment: function () {
                    return new journeyInfoSegment();
                }
            }
        }];
    return factory;
};
