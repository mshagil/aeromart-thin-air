'use strict';
/**
 * Created by nipuna on 12/22/16.
 */

var dummyQuoteDetailSummary = {
    "totalTaxAndSurcharges": 2330000,
    "REMOVED_segmentFare": [
        {
            "ondSequence": 0,
            "totalPrice": 19570000,
            "totalSurcharges": 0,
            "totalTaxes": 670000,
            "paxWise": [
                {
                    "paxType": "AD",
                    "total": 19570000,
                    "fare": 9450000,
                    "surcharge": 0,
                    "tax": 670000,
                    "noOfPax": 2,
                    "totalFare": 18900000
                }
            ],
            "sectorInfo": {
                "origin": "IKA",
                "originName": "Tehran - Imam Khomeini International Airport",
                "destination": "PVG",
                "destinationName": "Shanghai-Terminal2-Pudong Intl airport",
                "departureDate": "2017-01-17T00:00:00",
                "departureVariance": 3,
                "cabinClass": "Y",
                "promoCode": "",
                "passengers": {
                    "adults": "2",
                    "children": "0",
                    "infants": "0"
                },
                "loadingMsg": "msg_fare_loadingMsgForOutbound"
            }
        },
        {
            "ondSequence": 1,
            "totalPrice": 20560000,
            "totalSurcharges": 0,
            "totalTaxes": 1660000,
            "paxWise": [
                {
                    "paxType": "AD",
                    "total": 20560000,
                    "fare": 9450000,
                    "surcharge": 0,
                    "tax": 1660000,
                    "noOfPax": 2,
                    "totalFare": 18900000
                }
            ],
            "sectorInfo": {
                "origin": "PVG",
                "originName": "Shanghai-Terminal2-Pudong Intl airport",
                "destination": "IKA",
                "destinationName": "Tehran - Imam Khomeini International Airport",
                "departureDate": "2017-01-21T00:00:00",
                "departureVariance": 3,
                "cabinClass": "Y",
                "promoCode": "",
                "passengers": {
                    "adults": "2",
                    "children": "0",
                    "infants": "0"
                },
                "loadingMsg": "msg_fare_loadingMsgForInbound"
            }
        },
        {
            "ondSequence": 1,
            "totalPrice": 20560000,
            "totalSurcharges": 0,
            "totalTaxes": 1660000,
            "paxWise": [
                {
                    "paxType": "AD",
                    "total": 20560000,
                    "fare": 9450000,
                    "surcharge": 0,
                    "tax": 1660000,
                    "noOfPax": 2,
                    "totalFare": 18900000
                }
            ],
            "sectorInfo": {
                "origin": "PVG",
                "originName": "Shanghai-Terminal2-Pudong Intl airport",
                "destination": "IKA",
                "destinationName": "Tehran - Imam Khomeini International Airport",
                "departureDate": "2017-01-21T00:00:00",
                "departureVariance": 3,
                "cabinClass": "Y",
                "promoCode": "",
                "passengers": {
                    "adults": "2",
                    "children": "0",
                    "infants": "0"
                },
                "loadingMsg": "msg_fare_loadingMsgForInbound"
            }
        }
    ],
    "REMOVED_selectedCurrency": "IRR",
    "totalForJourney": {
        "fare": 37800000,
        "surcharge": 0,
        "tax": 2330000,
        "price": 40130000,
        "discount": {
            "type": null,
            "amount": 0
        },
        "paxWise": [
            {
                "paxType": "AD",
                "total": 40130000,
                "fare": 18900000,
                "surcharge": 0,
                "tax": 2330000,
                "noOfPax": 2,
                "totalFare": 37800000,
                "$$hashKey": "object:1186"
            }
        ]
    },
    "totalPrice": 40130000,
    "totalIbeFareDiscount": 0,
    "pax": {
        "adult": "2",
        "child": "0",
        "infant": "0"
    },
    "promoInfo": [],
    "extrasAndFlightTotal": null,
    "journeyType": "multi-city",
    "sectorsData": [
        {
            "sequenceNo": 0,
            "REMOVED_ondInfo": {
                "origin": "IKA",
                "originName": "Tehran - Imam Khomeini International Airport",
                "destination": "PVG",
                "destinationName": "Shanghai-Terminal2-Pudong Intl airport",
                "departureDate": "2017-01-17T00:00:00",
                "departureVariance": 3,
                "cabinClass": "Y",
                "promoCode": "",
                "passengers": {
                    "adults": "2",
                    "children": "0",
                    "infants": "0"
                },
                "loadingMsg": "msg_fare_loadingMsgForOutbound"
            },
            "REMOVED_timeLineData": {
                "data": [
                    {
                        "available": true,
                        "date": "14/01/2017",
                        "selected": false,
                        "flights": [],
                        "fareValue": "",
                        "fareClasses": [
                            {
                                "fareTypeId": "Y_D",
                                "fareTypeName": "Economy Class",
                                "description": "Economy Class",
                                "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                                "rank": null,
                                "uniqueIdentification": 0,
                                "fareClassType": "DEFAULT",
                                "fareClassId": null,
                                "fareValue": 14650000,
                                "additionalFareFee": null
                            }
                        ]
                    },
                    {
                        "fareValue": 14650000,
                        "selected": false,
                        "flights": [
                            {
                                "originCode": "IKA",
                                "destinationCode": "PVG",
                                "originName": "Tehran - Imam Khomeini International Airport",
                                "destinationName": "Shanghai-Terminal2-Pudong Intl airport",
                                "careerCode": "W5",
                                "returnFlag": false,
                                "flightRPH": "0",
                                "routeRefNumber": null,
                                "flightId": "",
                                "flightNumber": "W5077",
                                "travelMode": "FLIGHT",
                                "journeyType": "INTERNATIONAL",
                                "equipmentModelNumber": "AB340-6",
                                "departureDate": "15/01/2017",
                                "departureTime": "21:10",
                                "arrivalDate": "16/01/2017",
                                "arrivalTime": "09:55",
                                "arrivalDateFirstSeg": "16/01/2017",
                                "arrivalTimeFirstSeg": "09:55",
                                "segmentDuration": "0:8:15",
                                "totalDuration": "0:8:15",
                                "nextDayFlag": "(+1)",
                                "stops": [],
                                "flightFaresAvailability": [
                                    {
                                        "availableSeats": -1,
                                        "additionalFareClassInfo": null,
                                        "fareValue": 14650000,
                                        "fareTypeId": "Y_D",
                                        "returnFare": false,
                                        "visibleChannels": [
                                            "Public"
                                        ]
                                    }
                                ],
                                "isFlightFull": false
                            }
                        ],
                        "available": true,
                        "date": "15/01/2017",
                        "fareClasses": [
                            {
                                "fareTypeId": "Y_D",
                                "fareTypeName": "Economy Class",
                                "description": "Economy Class",
                                "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                                "rank": null,
                                "uniqueIdentification": 0,
                                "fareClassType": "DEFAULT",
                                "fareClassId": null,
                                "fareValue": 14650000,
                                "additionalFareFee": null
                            }
                        ]
                    },
                    {
                        "fareValue": 14650000,
                        "selected": false,
                        "flights": [
                            {
                                "originCode": "IKA",
                                "destinationCode": "PVG",
                                "originName": "Tehran - Imam Khomeini International Airport",
                                "destinationName": "Shanghai-Terminal2-Pudong Intl airport",
                                "careerCode": "W5",
                                "returnFlag": false,
                                "flightRPH": "1",
                                "routeRefNumber": null,
                                "flightId": "",
                                "flightNumber": "W5077",
                                "travelMode": "FLIGHT",
                                "journeyType": "INTERNATIONAL",
                                "equipmentModelNumber": "AB340-6",
                                "departureDate": "16/01/2017",
                                "departureTime": "21:10",
                                "arrivalDate": "17/01/2017",
                                "arrivalTime": "09:55",
                                "arrivalDateFirstSeg": "17/01/2017",
                                "arrivalTimeFirstSeg": "09:55",
                                "segmentDuration": "0:8:15",
                                "totalDuration": "0:8:15",
                                "nextDayFlag": "(+1)",
                                "stops": [],
                                "flightFaresAvailability": [
                                    {
                                        "availableSeats": -1,
                                        "additionalFareClassInfo": null,
                                        "fareValue": 14650000,
                                        "fareTypeId": "Y_D",
                                        "returnFare": false,
                                        "visibleChannels": [
                                            "Public"
                                        ]
                                    }
                                ],
                                "isFlightFull": false
                            }
                        ],
                        "available": true,
                        "date": "16/01/2017",
                        "fareClasses": [
                            {
                                "fareTypeId": "Y_D",
                                "fareTypeName": "Economy Class",
                                "description": "Economy Class",
                                "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                                "rank": null,
                                "uniqueIdentification": 0,
                                "fareClassType": "DEFAULT",
                                "fareClassId": null,
                                "fareValue": 14650000,
                                "additionalFareFee": null
                            }
                        ]
                    },
                    {
                        "available": true,
                        "date": "17/01/2017",
                        "selected": true,
                        "flights": [],
                        "fareValue": "",
                        "fareClasses": [
                            {
                                "fareTypeId": "Y_D",
                                "fareTypeName": "Economy Class",
                                "description": "Economy Class",
                                "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                                "rank": null,
                                "uniqueIdentification": 0,
                                "fareClassType": "DEFAULT",
                                "fareClassId": null,
                                "fareValue": 14650000,
                                "additionalFareFee": null
                            }
                        ]
                    },
                    {
                        "fareValue": 14650000,
                        "selected": false,
                        "flights": [
                            {
                                "originCode": "IKA",
                                "destinationCode": "PVG",
                                "originName": "Tehran - Imam Khomeini International Airport",
                                "destinationName": "Shanghai-Terminal2-Pudong Intl airport",
                                "careerCode": "W5",
                                "returnFlag": false,
                                "flightRPH": "2",
                                "routeRefNumber": null,
                                "flightId": "",
                                "flightNumber": "W5077",
                                "travelMode": "FLIGHT",
                                "journeyType": "INTERNATIONAL",
                                "equipmentModelNumber": "AB340-6",
                                "departureDate": "18/01/2017",
                                "departureTime": "21:10",
                                "arrivalDate": "19/01/2017",
                                "arrivalTime": "09:55",
                                "arrivalDateFirstSeg": "19/01/2017",
                                "arrivalTimeFirstSeg": "09:55",
                                "segmentDuration": "0:8:15",
                                "totalDuration": "0:8:15",
                                "nextDayFlag": "(+1)",
                                "stops": [],
                                "flightFaresAvailability": [
                                    {
                                        "availableSeats": -1,
                                        "additionalFareClassInfo": null,
                                        "fareValue": 14650000,
                                        "fareTypeId": "Y_D",
                                        "returnFare": false,
                                        "visibleChannels": [
                                            "Public"
                                        ],
                                        "isFlightSelect": true,
                                        "fareType": "Economy Class"
                                    }
                                ],
                                "isFlightFull": false,
                                "departureDateTime": "2017-01-18 21:10",
                                "arrivalDateTime": "2017-01-19 09:55"
                            }
                        ],
                        "available": true,
                        "date": "18/01/2017",
                        "fareClasses": [
                            {
                                "fareTypeId": "Y_D",
                                "fareTypeName": "Economy Class",
                                "description": "Economy Class",
                                "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                                "rank": null,
                                "uniqueIdentification": 0,
                                "fareClassType": "DEFAULT",
                                "fareClassId": null,
                                "fareValue": 14650000,
                                "additionalFareFee": null
                            }
                        ]
                    },
                    {
                        "available": true,
                        "date": "19/01/2017",
                        "selected": false,
                        "flights": [],
                        "fareValue": "",
                        "fareClasses": [
                            {
                                "fareTypeId": "Y_D",
                                "fareTypeName": "Economy Class",
                                "description": "Economy Class",
                                "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                                "rank": null,
                                "uniqueIdentification": 0,
                                "fareClassType": "DEFAULT",
                                "fareClassId": null,
                                "fareValue": 14650000,
                                "additionalFareFee": null
                            }
                        ]
                    },
                    {
                        "fareValue": 14650000,
                        "selected": false,
                        "flights": [
                            {
                                "originCode": "IKA",
                                "destinationCode": "PVG",
                                "originName": "Tehran - Imam Khomeini International Airport",
                                "destinationName": "Shanghai-Terminal2-Pudong Intl airport",
                                "careerCode": "W5",
                                "returnFlag": false,
                                "flightRPH": "3",
                                "routeRefNumber": null,
                                "flightId": "",
                                "flightNumber": "W5077",
                                "travelMode": "FLIGHT",
                                "journeyType": "INTERNATIONAL",
                                "equipmentModelNumber": "AB340-6",
                                "departureDate": "20/01/2017",
                                "departureTime": "21:10",
                                "arrivalDate": "21/01/2017",
                                "arrivalTime": "09:55",
                                "arrivalDateFirstSeg": "21/01/2017",
                                "arrivalTimeFirstSeg": "09:55",
                                "segmentDuration": "0:8:15",
                                "totalDuration": "0:8:15",
                                "nextDayFlag": "(+1)",
                                "stops": [],
                                "flightFaresAvailability": [
                                    {
                                        "availableSeats": -1,
                                        "additionalFareClassInfo": null,
                                        "fareValue": 14650000,
                                        "fareTypeId": "Y_D",
                                        "returnFare": false,
                                        "visibleChannels": [
                                            "Public"
                                        ]
                                    }
                                ],
                                "isFlightFull": false
                            }
                        ],
                        "available": true,
                        "date": "20/01/2017",
                        "fareClasses": [
                            {
                                "fareTypeId": "Y_D",
                                "fareTypeName": "Economy Class",
                                "description": "Economy Class",
                                "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                                "rank": null,
                                "uniqueIdentification": 0,
                                "fareClassType": "DEFAULT",
                                "fareClassId": null,
                                "fareValue": 14650000,
                                "additionalFareFee": null
                            }
                        ]
                    }
                ]
            },
            "REMOVED_flightInfo": {
                "fareFareResults": [
                    {
                        "originCode": "IKA",
                        "destinationCode": "PVG",
                        "originName": "Tehran - Imam Khomeini International Airport",
                        "destinationName": "Shanghai-Terminal2-Pudong Intl airport",
                        "careerCode": "W5",
                        "returnFlag": false,
                        "flightRPH": "2",
                        "routeRefNumber": null,
                        "flightId": "",
                        "flightNumber": "W5077",
                        "travelMode": "FLIGHT",
                        "journeyType": "INTERNATIONAL",
                        "equipmentModelNumber": "AB340-6",
                        "departureDate": "18/01/2017",
                        "departureTime": "21:10",
                        "arrivalDate": "19/01/2017",
                        "arrivalTime": "09:55",
                        "arrivalDateFirstSeg": "19/01/2017",
                        "arrivalTimeFirstSeg": "09:55",
                        "segmentDuration": "0:8:15",
                        "totalDuration": "0:8:15",
                        "nextDayFlag": "(+1)",
                        "stops": [],
                        "flightFaresAvailability": [
                            {
                                "availableSeats": -1,
                                "additionalFareClassInfo": null,
                                "fareValue": 14650000,
                                "fareTypeId": "Y_D",
                                "returnFare": false,
                                "visibleChannels": [
                                    "Public"
                                ],
                                "isFlightSelect": true,
                                "fareType": "Economy Class"
                            }
                        ],
                        "isFlightFull": false,
                        "departureDateTime": "2017-01-18 21:10",
                        "arrivalDateTime": "2017-01-19 09:55"
                    }
                ],
                "fareTypeInfo": [
                    {
                        "fareTypeId": "Y_D",
                        "fareTypeName": "Economy Class",
                        "description": "Economy Class",
                        "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                        "rank": null,
                        "uniqueIdentification": 0,
                        "fareClassType": "DEFAULT",
                        "fareClassId": null,
                        "fareValue": 14650000,
                        "additionalFareFee": null
                    }
                ]
            },
            "selectedFlight": {
                "flight": {
                    "originCode": "IKA",
                    "destinationCode": "PVG",
                    "originName": "Tehran - Imam Khomeini International Airport",
                    "destinationName": "Shanghai-Terminal2-Pudong Intl airport",
                    "careerCode": "W5",
                    "returnFlag": false,
                    "flightRPH": "2",
                    "routeRefNumber": null,
                    "flightId": "",
                    "flightNumber": "W5077",
                    "travelMode": "FLIGHT",
                    "journeyType": "INTERNATIONAL",
                    "equipmentModelNumber": "AB340-6",
                    "departureDate": "18/01/2017",
                    "departureTime": "21:10",
                    "arrivalDate": "19/01/2017",
                    "arrivalTime": "09:55",
                    "arrivalDateFirstSeg": "19/01/2017",
                    "arrivalTimeFirstSeg": "09:55",
                    "segmentDuration": "0:8:15",
                    "totalDuration": "0:8:15",
                    "nextDayFlag": "(+1)",
                    "stops": [],
                    "flightFaresAvailability": [
                        {
                            "availableSeats": -1,
                            "additionalFareClassInfo": null,
                            "fareValue": 14650000,
                            "fareTypeId": "Y_D",
                            "returnFare": false,
                            "visibleChannels": [
                                "Public"
                            ],
                            "isFlightSelect": true,
                            "fareType": "Economy Class"
                        }
                    ],
                    "isFlightFull": false,
                    "departureDateTime": "2017-01-18 21:10",
                    "arrivalDateTime": "2017-01-19 09:55"
                },
                "fare": {
                    "availableSeats": -1,
                    "additionalFareClassInfo": null,
                    "fareValue": 14650000,
                    "fareTypeId": "Y_D",
                    "returnFare": false,
                    "visibleChannels": [
                        "Public"
                    ],
                    "isFlightSelect": true,
                    "fareType": "Economy Class"
                }
            },
            "REMOVED_showBusyLoader": false,
            "REMOVED_busyLoaderMsg": "msg_fare_loadingMsgForOutbound",
            "fareClasses": [
                {
                    "fareTypeId": "Y_D",
                    "fareTypeName": "Economy Class",
                    "description": "Economy Class",
                    "imageUrl": "http://reservations.mahan.aero/W5/Y_N_en.png",
                    "rank": null,
                    "uniqueIdentification": 0,
                    "fareClassType": "DEFAULT",
                    "fareClassId": null
                }
            ],
            "$$hashKey": "object:1165"
        }
    ],
    "selectedCurrencyType": "IRR"
};

var multiCityService = function(){
    var service = {};
    service.multiCityService = [ 
        '$state', 
        'currencyService', 
        'languageService', 
        'commonLocalStorageService', 
        'multiCityFactory', 
        'multiCityRemoteService',
        'travelFareService',
        function($state, currencyService, languageService, commonLocalStorageService, multiCityFactory,
                 multiCityRemoteService, travelFareService){
            var self = this;

            var _searchList = null;
            var _flightSearchData = {};
            var _selectedFlightData = {};
            var _currentLang = languageService.getSelectedLanguage();
            var _fareQuote = {};

            var PROMO_TYPE = {
                promoCode: 'PROMO_CODE'
            };
            var MULTI_CITY_ROUTES = {
                search: 'multi-city',
                flight: 'multi-city-flight',
                fare: 'multi-city-fare'
            };

            // Service functions
            // --------------------------------------------------.
            /**
             * Save search criteria and navigate to flight page
             */
            self.navigateToSearchFlight = function(data) {
                // Save search data and navigate to flight page
                self.setFlightSearchData(data);
                navigateToRoute(MULTI_CITY_ROUTES.flight);
            };

            self.searchFlights = function(successFunction, errorFunction) {
                // If flightSearchData doesn't exist, navigate to search page
                if(_.isEmpty(self.getFlightSearchData())){
                    navigateToRoute(MULTI_CITY_ROUTES.search);
                }

                var flightSearchRq = getAdaptFlightSearchRq(self.getFlightSearchData());

                multiCityRemoteService.retrieveMultiCityAvailability(
                    flightSearchRq,
                    function(success){
                        _.map(success.originDestinationResponse, function(ond){
                            ond.originName = commonLocalStorageService.getAirportName(ond.origin);
                            ond.destinationName = commonLocalStorageService.getAirportName(ond.destination);
                        });

                        successFunction(success);
                    },
                    function(error){
                        errorFunction(error);
                    });
            };

            self.navigateToSelectFare = function(data){
                self.setSelectedFlights(data);
                navigateToRoute(MULTI_CITY_ROUTES.fare);
            };

            self.getFareResults = function(successFunction, errorFunction){
                var flightSearchData = self.getFlightSearchData();
                var selectedFlightData = self.getSelectedFlights();

                if(_.isEmpty(flightSearchData) || _.isEmpty(selectedFlightData)) {
                    navigateToRoute(MULTI_CITY_ROUTES.search);
                }

                var fareReq = getAdaptFareRq(flightSearchData, selectedFlightData);
                multiCityRemoteService.multiCityFareSearch(
                    fareReq,
                    function(success){
                        var fareClasses = success.fareClasses;
                        var originDestinationResponse = success.originDestinationResponse;

                        //Setting null when fare does not exist for a given flight
                        _.forEach(originDestinationResponse, function(ond){
                            _.forEach(ond.availableOptions, function(flight){
                                var flightFareClasses = flight.availableFareClasses;

                                flight.availableFareClasses = _.reduce(fareClasses, function(output, fareClass){
                                    var fare = _.findWhere(flightFareClasses, {fareClassCode: fareClass.fareClassCode});

                                    if(fare){
                                        fare.fareClassType = fareClass.fareClassType;
                                        fare.fareClassId = fareClass.fareClassId;
                                        output.push(fare);
                                    } else {
                                        output.push(null);
                                    }
                                    return output;
                                },[]);
                            })
                        });

                        successFunction(success);
                    },
                    function(error){
                        errorFunction(error);
                    });
            };

            self.getFareQuote = function(ondData, successFunction, errorFunction) {
                var flightSearchData = self.getFlightSearchData();
                var selectedFlightData = self.getSelectedFlights();

                var adaptedFareRequest = getAdaptFareRq(flightSearchData, selectedFlightData);

                _.forEach(adaptedFareRequest.journeyInfo, function(ond, index){
                    var selectedFare = ondData[index].selected;

                    ond.preferences = {
                        additionalPreferences: {
                            fareClassId: selectedFare.fareClassId,
                            fareClassType: selectedFare.fareClassType
                        }
                    };
                });

                multiCityRemoteService.multiCityFareQuote(adaptedFareRequest,
                    function(success){
                        var displayCharges = [];
                        _fareQuote = success;

                        // Adapt display charges for display purposes
                        _.forEach(success.selectedFlightPricing.ondOWPricing, function(ond){
                            _.forEach(ond.paxWise, function(paxWisePricing){
                                var passengerDispalyCharge = angular.copy(paxWisePricing);
                                passengerDispalyCharge.ondSequence = ond.ondSequence;

                                var ondDetails = _.find(success.originDestinationResponse, function(tempOnd){
                                    return tempOnd.ondSequence === passengerDispalyCharge.ondSequence;
                                });

                                if(ondDetails){
                                    passengerDispalyCharge.origin = ondDetails.origin;
                                    passengerDispalyCharge.destination = ondDetails.destination;

                                    passengerDispalyCharge.originName = commonLocalStorageService.getAirportName(ondDetails.origin);
                                    passengerDispalyCharge.destinationName = commonLocalStorageService.getAirportName(ondDetails.destination);
                                }

                                displayCharges.push(passengerDispalyCharge);
                            });
                        });

                        success.displayCharges = displayCharges;
                        successFunction(success);
                    },
                    function(error){
                        errorFunction(error);
                    }
                )
            };

            self.navigateToPassenger = function(originDestinationResponse){

                var confirmedFlights = [];
                _.forEach(originDestinationResponse, function(ond,key){
                    var ondDetails = {
                        fare: getAdaptedFlightForConfirmed(ond),
                        flight: ond.selected
                    };

                    confirmedFlights[ond.ondSequence] = ondDetails;
                });

                // Setting confirmed flight for frontend use
                var confirmedFlightData = adaptConfirmedFlightData(originDestinationResponse);
                commonLocalStorageService.setConfirmedFlightData(confirmedFlightData);

                // TODO: Adapt QuoteDetail Summary

                dummyQuoteDetailSummary;

                travelFareService.setQuoteDetails(getQuoteDetails(confirmedFlightData));



                navigateToRoute('passenger');
            };

            // Getters and Setters
            // --------------------------------------------------.
            self.getSearchList = function() {
                if(!_searchList){
                    _searchList = [];
                    _searchList.push(self.getSearchSegment());
                }

                return _searchList;
            };

            self.getSearchSegment = function() {
                return new multiCityFactory.JourneyInfoSegment();
            };

            self.setFlightSearchData = function(data) {
                commonLocalStorageService.setMultiCitySearch(data);
                _flightSearchData = data;
            };

            self.getFlightSearchData = function() {
                if(_.isEmpty(_flightSearchData)){
                    _flightSearchData = commonLocalStorageService.getMultiCitySearch();
                }
                return _flightSearchData;
            };

            self.setSelectedFlights = function(data) {
                commonLocalStorageService.setMultiCitySelectedFlights(data);
                _selectedFlightData = data;
            };

            self.getSelectedFlights = function() {
                if(_.isEmpty(_selectedFlightData)){
                    _selectedFlightData = commonLocalStorageService.getMultiCitySelectedFlights();
                }
                return _selectedFlightData;
            };

            // Private Functions
            // --------------------------------------------------.
            function getAdaptFlightSearchRq(data){
                var promotionObject = {};
                var selectedCurrency = currencyService.getSelectedCurrency();

                // Adapting promo code
                if(!_.isEmpty(data.promoCode)){
                    promotionObject = {
                        code: data.promoCode,
                        type: PROMO_TYPE.promoCode
                    }
                }

                // Adapting JourneyInfoSegment
                var journeyInfo = _.map(data.journeyInfo, function(segment){
                    var tempSegment = {};
                    tempSegment.destination = segment.destination.code;
                    tempSegment.origin = segment.origin.code;

                    tempSegment.originName = commonLocalStorageService.getAirportName(tempSegment.origin);
                    tempSegment.destinationName = commonLocalStorageService.getAirportName(tempSegment.destination);

                    //TODO: Remove if not needed
                    tempSegment.destinationCode = segment.destination.code;
                    tempSegment.originCode = segment.origin.code;
                    tempSegment.destinationName = segment.destination[_currentLang];
                    tempSegment.originName = segment.origin[_currentLang];

                    tempSegment.departureDateTime = segment.departureDateTime;
                    tempSegment.departureVariance = segment.departureVariance;
                    return tempSegment;
                });

                return {
                    travellerQuantity: {
                        adultCount: data.adult,
                        childCount: data.child,
                        infantCount: data.infant
                    },
                    preferences: {
                        cabinClass: data.cabin,
                        logicalCabinClass: data.cabin,
                        currency: selectedCurrency,
                        promotion: promotionObject
                    },
                    journeyInfo: journeyInfo
                }
            }

            function getAdaptFareRq(flightSearchData, selectedFlightData){
                flightSearchData = getAdaptFlightSearchRq(flightSearchData);

                _.forEach(flightSearchData.journeyInfo, function(flightData, index){
                    flightData.specificFlights = _.map(selectedFlightData[index], function (segment) {
                        return {
                            filghtDesignator: segment.filghtDesignator,
                            segmentCode: segment.segmentCode,
                            departureDateTime: segment.departureDateTime.zulu,
                            flightSegmentRPH: segment.flightSegmentRPH
                        };
                    });
                });
                
                return flightSearchData; 
            }

            function getAdaptedFlightForConfirmed(ond){
                var flight = ond.availableOptions[0];
            }

            function adaptConfirmedFlightData(selectedFlights){
                var adaptedConfirmedFlights = {};

                _.forEach(selectedFlights, function(ond){
                    var flight = ond.availableOptions[0];
                    var adaptedFlightSegments = getAdaptedSegment(flight.segments);

                    adaptedFlightSegments.origin = ond.origin;
                    adaptedFlightSegments.destination = ond.destination;

                    //TODO: Remove if not needed
                    adaptedFlightSegments.originName = ond.origin;
                    adaptedFlightSegments.originCode = ond.origin;
                    adaptedFlightSegments.destinationName = ond.destination;
                    adaptedFlightSegments.destinationCode = ond.destination;

                    adaptedConfirmedFlights[ond.ondSequence] = {
                        flight: adaptedFlightSegments,
                        fare: ond.selected
                    };
                });

                return adaptedConfirmedFlights;
            }

            function getAdaptedSegment(segments) {
                var adaptedSegments = segments[0];
                adaptedSegments.flightRPH = adaptedSegments.flightSegmentRPH;
                var stops = [];

                if(segments.length > 1){
                    for(var i=1; i++; i < segments.length){
                        stops.push(segments[i])
                    }
                }
                adaptedSegments.stops = stops;

                return adaptedSegments;
            }

            function navigateToRoute(route){
                // Helper function to change routes.
                $state.go(route, {
                    lang: languageService.getSelectedLanguage(),
                    currency: currencyService.getSelectedCurrency()
                });
            }

            function getQuoteDetails(confirmedFlightData) {
                var totalForJourney = _fareQuote.selectedFlightPricing.total;
                var sectorsData = [];

                _.forEach(confirmedFlightData, function(flight, key){
                    sectorsData.push({
                        selectedFlight : flight,
                        sequenceNo : key,
                        fareClasses : _fareQuote.fareClasses
                    });
                });

                return {
                    journeyType:"multi-city",
                    pax: {
                        adult: _flightSearchData.adult,
                        child: _flightSearchData.child,
                        infant: _flightSearchData.infant
                    },
                    selectedCurrencyType: _fareQuote.currency,
                    totalForJourney: totalForJourney,
                    totalPrice: totalForJourney.price,
                    totalIbeFareDiscount: totalForJourney.tax + totalForJourney.surcharge,
                    promoInfo: _fareQuote.selectedFlightPricing.promoInfo,
                    sectorsData: sectorsData
                }


            }
        }];
    return service;
};
