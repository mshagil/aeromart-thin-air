/**
 * Created by Nipuna on 01/22/16
 */
'use strict';
var extrasAirportServicesController = function () {
    var controllers = {};
    controllers.extrasAirportServicesController = [
        "passengerService",
        "ancillaryRemoteService",
        "loadingService",
        "ancillaryService",
        "IBEDataConsole",
        "quoteService",
        "currencyService",
        "languageService",
        "applicationService",
        "$state",
        "alertService",
        "modifyReservationService",
        "popupService",
        '$translate',
        'navigationService',
        "$rootScope",
        function (passengerService, ancillaryRemoteService, loadingService, ancillaryService, IBEDataConsole,
                  quoteService, currencyService, languageService, applicationService, $state, alertService,
                  modifyReservationService, popupService, $translate, navigationService,$rootScope) {   
                      
            if(!$rootScope.deviceType.isMobileDevice){
                $rootScope.removePaddingFromXSContainerClass=true;
            }
                 
            // For mobile menus           
            var self = this;
             // For mobile menus
            var sectorCount = 0, appMode, appState, urlParams, currentPnr;
            var ancillaryType = "SSR_AIRPORT";
            var availableAirPorts = [];
            self.accordionArray =[];
            self.currency = currencyService.getSelectedCurrency();

            self.servicesPreference = {}; //ancillary object for services
            self.availableServices = []; //holds available services for the current flight
            self.allServices = []; //holds the backend response with all service information
            self.paxData = {}; //list of passengers used for the passenger list directive;
            self.paxDataCollection = {}; //Collection of paxData objects for each segment
            self.selectedPax = 0; //index of the currently selected passenger
            self.passengerType = 0; //The type of passenger (0-Adult, 1-Child, 2-Infant)
            self.passengerType = 0; //The type of passenger (0-Adult, 1-Child, 2-Infant)

            // Holds current RPH and point of Airport
            self.airportKey = null;

            /**
             * Used to update the passenger.'assigned' and service.'selected' values on load or sector/passenger change
             * Used when
             *  1. Loading the page
             *  2. Changing passenger
             *  3. Changing sector
             * */
            self.updateSelectedValues = function () {
                //On set selected values for currently selected passenger and the currently selected sector
                var currentPrefs = _.find(self.servicesPreference.scopes[0].preferences,
                    function(prefs){
                        var prefId = prefs.preferanceId;
                        var currentPrefKey = prefId.flightSegmentRPH + prefId.point + prefId.airportCode;

                        if(self.airportKey == currentPrefKey) {
                            return true;
                        }
                    }
                );

                var currentPassengerPrefs = currentPrefs.passengers[self.selectedPax].selectedItems;
                if(!!self.allServices[self.airportKey]) {
                    angular.forEach(self.allServices[self.airportKey].items, function (service) {
                        service.selected = _.has(currentPassengerPrefs, service.ssrCode);
                    });
                }
                /**
                 * Checks if each passenger has values in the current services array and if so, mark them as assigned
                 * Used for the passenger list directives tick box
                 */
                angular.forEach(self.paxDataCollection[sectorCount], function (passenger, key) {
                    if(!!self.servicesPreference.scopes[0].preferences[sectorCount].passengers[key]) {
                        var currentServices = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[key].selectedItems;
                        if (!_.isEmpty(currentServices)) {
                            passenger.assigned = true;
                        }
                    }
                });
            };

            /**
             * On passenger change, change pax type and update the selected service list
             * @param pax passenger object
             * */
            self.passengerChange = function (pax) {
                //Change the pax type
                self.selectedPax = pax;
                if (self.paxData[pax].category === "Adult") {
                    self.passengerType = 0;
                    if(self.paxData[pax].infantSequence){
                        self.passengerType = 3;
                    }
                } else if (self.paxData[pax].category === "Child") {
                    self.passengerType = 1;
                } else if (self.paxData[pax].category === "Infant") {
                    self.passengerType = 2;
                }

                self.updateSelectedValues();
            };

            /**
             * Handeling event when option is clicked adds or removes the service depending on current status
             * @param option - option that was cliked on
             * */
            self.optionClick = function (clickedOption,index) {
//            	if(typeof index != 'undefined')
//            		sectorCount = index;
                var option = angular.copy(clickedOption);
                if(option.applicablityType == 'P') {
                  var currentServices = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[self.selectedPax];
                  if (currentServices.selectedItems[option.ssrCode]) {
                      delete currentServices.selectedItems[option.ssrCode];
                  } else {
                      option.id = option.ssrCode;
                      option.quantity = 1;
                      option.input = null;
                      option.price = option.charges[self.passengerType].amount;
                      currentServices.selectedItems[option.ssrCode] = option;
                  }
                  currentServices.itmeTotalCharge = ancillaryService.getMealTotalPerPassenger(currentServices.selectedItems);
                  self.paxDataCollection[sectorCount][self.selectedPax].assigned = (!_.isEmpty(currentServices.selectedItems));
                }
                else {
                  var paxCount = 0;
                  var passengers = self.servicesPreference.scopes[0].preferences[sectorCount].passengers;
                  var perPaxAmount = option.charges[0].amount / passengers.length;
                  angular.forEach(passengers, function (currentServices) {
                      if (currentServices.selectedItems[option.ssrCode]) {
                          delete currentServices.selectedItems[option.ssrCode];
                          self.paxDataCollection[sectorCount][paxCount].assigned = (!_.isEmpty(currentServices.selectedItems));
                      } else {
                        option.id = option.ssrCode;
                        option.quantity = 1;
                        option.input = null;
                        option.price = perPaxAmount;
                        currentServices.selectedItems[option.ssrCode] = option;
                        currentServices.itmeTotalCharge = ancillaryService.getMealTotalPerPassenger(currentServices.selectedItems);
                        self.paxDataCollection[sectorCount][paxCount].assigned = (!_.isEmpty(currentServices.selectedItems));
                    }
                      paxCount++;
                  });
                }
                setAirportServicesTotal();
            };

            /**
             * Delete selected service and update service preference when user click delete icon
             * @param el - selected object
             */
            self.onDelete = function (el) {
                var objkey = el.$parent.item.id;
                var passengerIndex = el.$parent.$parent.pasIndex;

                var currentServices = self.servicesPreference.scopes[0].preferences[sectorCount].passengers[passengerIndex];
                delete currentServices.selectedItems[objkey];

                self.updateSelectedValues();
                currentServices.itmeTotalCharge = ancillaryService.getMealTotalPerPassenger(currentServices.selectedItems);
                setAirportServicesTotal();
            };


            // Ancillary common functions
            self.nextSector = function (index) {
                if (_.isUndefined(index)) {
                    sectorCount++;

                    if (self.servicesPreference.scopes[0].preferences[sectorCount] == undefined) {
                        sectorCount = 0;
                    }

                    self.currentServicePreference = self.servicesPreference.scopes[0].preferences[sectorCount];
                    self.sectionSelection = {
                        index: sectorCount,
                        section: sectorCount
                    };
                } else {
                    self.currentServicePreference = self.servicesPreference.scopes[0].preferences[0];
                    sectorCount = index;
                }
                var selectedRPH = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.flightSegmentRPH;
                var selectedPoint = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.point;
                var selectedAirportCode = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.airportCode;
                self.airportKey = selectedRPH + selectedPoint + selectedAirportCode;
                if(!!self.allServices[self.airportKey]) {
                    self.availableServices = self.allServices[self.airportKey].items
                    self.noServices = false;
                }else{
                    self.availableServices = null;
                    self.noServices = true;
                }
                self.paxData = self.paxDataCollection[sectorCount];
                self.updateSelectedValues();
                self.airportNames = ancillaryService.getAirports(self.servicesPreference, sectorCount)
            };

            self.backToExtras = function () {
                $rootScope.extraType = '';
                if (!ancillaryService.getAncillarySelected(ancillaryType)) {
                    var ancillaryContinueMessage = $translate.instant('lbl_continue_message');
                    popupService.confirm(ancillaryContinueMessage,
                        function () {
                            ancillaryService.backToExtras();
                        }
                    );
                } else {
                    ancillaryService.backToExtras();
                }
            };

            self.skipServices = function () {
                ancillaryService.skipServices("SSR_AIRPORT");
                $rootScope.extraType = '';
            };

            self.backToHome = function() {
                navigationService.modifyFlowHome();
            };

            // Private functions
            function setAirportServicesTotal() {
                if(Object.keys(self.servicesPreference).length) {
                    angular.forEach(self.servicesPreference.scopes[0].preferences, function (pref) {
                        pref.total = ancillaryService.getSegmentTotal(pref.passengers);
                    });
                    self.servicesPreference.total = ancillaryService.getAncileryTotal(self.servicesPreference.scopes[0].preferences);
                }
            }

            function removeUnmatchedAirports() {
                var prf = [];
                    angular.forEach(self.servicesPreference.scopes[0].preferences, function (ancillaryPreference) {
                        var hasSelection = _.find(ancillaryPreference.passengers, function(p){
                            if(!!p.selectedItems) {
                                return Object.keys(p.selectedItems).length > 0;
                            }else{
                                false;
                            }
                        });
                        if(!!hasSelection){
                            //IF the some Airports does not provide services at the time user going to modify his booking those airport should be removed
                        	for (var i = 0; i < availableAirPorts.length; i++) {
                                if (ancillaryPreference.preferanceId.airportCode == availableAirPorts[i].airportCode && ancillaryPreference.preferanceId.flightSegmentRPH == availableAirPorts[i].flightSegmentRPH) {
                                	var anci = availableAirPorts[i];
                                    break;
                                }
                            }
                        	if (!!anci){
                        		ancillaryPreference.preferanceId =anci;
                        		prf.push(ancillaryPreference);
                        	}else{
                        		prf.push(ancillaryPreference);
                        	}
                        }else {
                            //IF the other Airports provide services at the time user going to modify his booking he should be abe to add them
                            for (var i = 0; i < availableAirPorts.length; i++) {
                                if (ancillaryPreference.preferanceId.airportCode == availableAirPorts[i].airportCode && ancillaryPreference.preferanceId.point == availableAirPorts[i].point && ancillaryPreference.preferanceId.flightSegmentRPH == availableAirPorts[i].flightSegmentRPH) {
                                    prf.push(ancillaryPreference);
                                    break;
                                }
                            }
                        }
                    });
                    self.servicesPreference.scopes[0].preferences = prf;
            }

            (function init() {
                window.scrollTo(0, 0);
                quoteService.setShowSummaryDrawer(false);

                appMode = applicationService.getApplicationMode();
                self.appMode = appMode;
                self.noServices = false;
                currentPnr = modifyReservationService.getReservationSearchCriteria();
                if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                    appState = 'modifyReservation';
                    urlParams = {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        pnr: currentPnr.pnr,
                        lastName: currentPnr.lastName,
                        departureDate: currentPnr.departureDate
                    }
                } else {
                    appState = 'extras';
                    urlParams = {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    }
                }

                ancillaryRemoteService.loadAirportServicesData(ancillaryType, function (response) {
                    loadingService.hidePageLoading();
                    if (response == null || !response.length) {
                        $state.go(appState, urlParams);
                    } else {
                        IBEDataConsole.log(response);
                        //self.allServices = response;

                        //self.servicesPreference = ancillaryService.getAncillaryFromResponse(ancillaryType, response);
                        //Set default sector count
                        self.sectionSelection = {index: sectorCount};
                            if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT ) {
                                if(ancillaryService.getAncillaryModel()['SSR_AIRPORT'] != undefined && appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                                    self.servicesPreference = ancillaryService.getAncillary('SSR_AIRPORT');
                                }else{
                                    self.servicesPreference = ancillaryService.getAncillaryFromResponse(ancillaryType, response);
                                }
                            }


                        angular.forEach(response, function (section, key) {
                            var currentSectorAirportKey = section.scope.flightSegmentRPH + section.scope.point + section.scope.airportCode;
                            //Filter out items with the same item Id to prevent duplicates sent from backend from displaying (AARESAA-25959)
                            section.items = _.uniq(section.items, 'itemId');
                            self.allServices[currentSectorAirportKey] = section;

                            var passengerList = passengerService.getPassengerDetailsForAnci();
                            var prefId = {};
                            prefId.airportCode = section.scope.airportCode;
                            prefId.point = section.scope.point;
                            prefId.flightSegmentRPH = section.scope.flightSegmentRPH;
                            self.paxDataCollection[key] = (_.extend({}, passengerList));
                            if (appMode == applicationService.APP_MODES.CREATE_RESERVATION ){
                                self.servicesPreference = ancillaryService.getAncillaryFromResponse(ancillaryType, response);
                            }
                            if (self.servicesPreference.scopes[0].preferences[key] != undefined) {
                                if (appMode != applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                                	if (self.servicesPreference.scopes[0].preferences[key].preferanceId == undefined){
                                		self.servicesPreference.scopes[0].preferences[key].preferanceId = prefId;
                                	}
                                }
                                availableAirPorts[key] = {};
                                availableAirPorts[key].airportCode = section.scope.airportCode;
                                availableAirPorts[key].point = section.scope.point;
                                availableAirPorts[key].flightSegmentRPH = section.scope.flightSegmentRPH
                            }
                        });
                        if(appMode == applicationService.APP_MODES.MODIFY_SEGMENT || appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI) {
                            removeUnmatchedAirports();
                        }
                        var selectedRPH = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.flightSegmentRPH;
                        var selectedPoint = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.point;
                        var selectedAirportCode = self.servicesPreference.scopes[0].preferences[sectorCount].preferanceId.airportCode;
                        self.airportKey = selectedRPH + selectedPoint + selectedAirportCode;
                        if(!!self.allServices[self.airportKey]) {
                            self.availableServices = self.allServices[self.airportKey].items;
                            self.noServices = false;
                        }else{
                            self.availableServices = null;
                            self.noServices = true;
                        }

                        self.paxData = self.paxDataCollection[sectorCount];

                        if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {

                            var allPassengers = passengerService.getPassengerDetails();

                            angular.forEach(self.paxDataCollection, function(currentPaxList){
                                // TODO: Should be moved to the step where the model is created in modification flow
                                angular.forEach(allPassengers.paxList,
                                    function (infant) {
                                        if (infant.type === "infant") {
                                            var parentPassenger = _.find(currentPaxList, function (passenger) {
                                                return passenger.paxSequence === infant.travelingWith;
                                            });
                                            parentPassenger.infantSequence = infant.paxSequence;
                                        }
                                    }
                                );
                            });
 
                        }

                        // Setting correct type for the first passenger
                        if(self.paxData[self.selectedPax].infantSequence){
                            self.passengerType = 3;
                        }
                                  
                        self.airportNames = ancillaryService.getAirports(self.servicesPreference, sectorCount)
                        setAirportServicesTotal();
                       //setAssigned on load if already selected
                       self.updateSelectedValues();
                    }
                }, function (error) {
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);

                });
//                console.log(self.servicesPreference)


                if(appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    var unmodifiedAnci = ancillaryService.getUnmodifiedAnci();

                    if(unmodifiedAnci && unmodifiedAnci[ancillaryType]) {
                        if(!_.isEmpty(unmodifiedAnci[ancillaryType].scopes[0].preferences)){
                            self.previousSelection = unmodifiedAnci[ancillaryType];
                        }
                    }
                }
            }());
        }];
    return controllers;
};