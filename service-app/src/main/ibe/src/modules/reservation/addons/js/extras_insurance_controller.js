'use strict';
var insuranceController = function(){
    var controller = {};
    controller.insuranceController = [
        '$location',
        '$rootScope',
        'extrasService',
        'ancillaryRemoteService',
        'IBEDataConsole',
        'ancillaryService',
        'loadingService',
        'currencyService',
        'alertService',
        'quoteService',
        'languageService',
        'applicationService',
        '$state',
        'modifyReservationService',
        '$sce',
        '$scope',
        '$timeout',
        'navigationService',
        function($location,$rootScope,extrasService,ancillaryRemoteService,IBEDataConsole,ancillaryService,
                 loadingService,currencyService,alertService, quoteService,languageService,applicationService,
                 $state,modifyReservationService,$sce,$scope,$timeout, navigationService){    
    
            // For mobile menus            
            var extrasModel = {},appMode,appState,urlParams,currentPnr;
            this.trustAsHtml = $sce.trustAsHtml;
            var _this = this;
            _this.insuranceFinalAmount=0;
            this.model = {
                isPopupOpen : false,
                allInsurancePolicies : [],
                changedPolicyId : -1,
                currencyType : currencyService.getSelectedCurrency(),
                changeGroup:[]
            };
            this.popUpData='';
            (function init(scope){
                 _this.insuranceFinalAmount=0;
                window.scrollTo(0, 0);
                quoteService.setShowSummaryDrawer(false);
                appMode = applicationService.getApplicationMode();
                self.appMode = appMode;
                currentPnr = modifyReservationService.getReservationSearchCriteria();
                if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    appState = 'modifyReservation'
                    urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),pnr:currentPnr.pnr,lastName:currentPnr.lastName,departureDate:currentPnr.departureDate}
                }else{
                    appState = 'extras';
                    urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),mode: applicationService.getApplicationMode()}
                }


                extrasModel = extrasService.getExtras();
                ancillaryRemoteService.retrieveInsuranceData({}, function (data) {
                    loadingService.hidePageLoading();
                    if(data == null){
                        $state.go(appState, urlParams);
                    }else {
                        _this.model.allInsurancePolicies = data;



                            //If ancillary for "INSURANCE" is created, use it to select the values
                            if (ancillaryService.checkAncillaryCreated("INSURANCE")) {
                                var selectedInsurancePolicies = [];

                                //Get the selected insurance policies
                                var insuranceAncillary = ancillaryService.getAncillary("INSURANCE");
                                var prefs = insuranceAncillary.scopes[0].preferences[0].selectedItems;

                                //Clear default selected values
                                angular.forEach(data, function (insurancePolicy, key) {
                                    _this.model.allInsurancePolicies[key].isSelected = false;
                                });

                                //Map the selected insurance policies with the response data
                                angular.forEach(prefs, function (selectedPolicy) {
                                    angular.forEach(data, function (insurancePolicy, t) {
                                        angular.forEach(insurancePolicy.planGroups, function (planGroups, j) {
                                            angular.forEach(planGroups.plans, function (plans, l) {
                                                if (selectedPolicy.id == plans.policyID) {
                                                    _this.model.allInsurancePolicies[t].planGroups[j].plans[l].isSelected = true;
                                                    _this.model.allInsurancePolicies[t].planGroups[j].plans[l].isDisplay = true;

                                                    selectedInsurancePolicies.push(plans);
                                                }
                                                else {
                                                   // _this.model.allInsurancePolicies[t].planGroups[j].plans[l].isSelected = false;
                                                   // _this.model.allInsurancePolicies[t].planGroups[j].plans[l].isDisplay = false;
                                                }

                                            });

                                        });
                                    });
                                });

                                //check default selection
                                //if plan group not any default selection set o index for display
                                for(var i =0;i< _this.model.allInsurancePolicies[0].planGroups.length;i++){
                                    var plans = _this.model.allInsurancePolicies[0].planGroups[i].plans;
                                    var t=0;
                                    for(var j=0;j<plans.length;j++){
                                        if((typeof plans[j].thingsCovered) =='string'){
                                            plans[j].thingsCovered=$sce.trustAsHtml(plans[j].thingsCovered);
                                        }

                                        if(plans[j].isSelected){
                                            t=t+1;
                                            _this.model.allInsurancePolicies[0].planGroups[i].planName = plans[j].itemName;
                                            
                                            _this.insuranceFinalAmount = plans[j].amount;
                                        }
                                        
                                       // plans[j].thingsCovered= plans[j].thingsCovered.replace(/\r?\n|\r/g,"");
                                    }
                                    if(t==0){
                                        plans[0].isDisplay = true;
                                        _this.model.allInsurancePolicies[0].planGroups[i].planName = plans[0].itemName
                                    }

                                }

                            }

                            ancillaryService.getAncillaryPreferenceForInsurance("INSURANCE", "ALL_SEGMENTS", _this.model.allInsurancePolicies);

                            _this.insurancePreferance = ancillaryService.getAncillary("INSURANCE");

                        $timeout(function() {
                            updateProviderName();
                            eventBinding();
                        }, 10)


                    }
                    insuranceTotal();
                }, function (error) {
                    //IBEDataConsole.log('Insurance data retrieval fails');
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);
                });



            })(this);


            function eventBinding(){
                if ($(".insurance_block input[name='chkIns']").length>0){
                    $(document).off("click").on("click", ".insurance_block input[type='checkbox']",  function(event){

                        var ele = $(event.currentTarget).closest(".insurance_block");
                        var group=$(event.currentTarget).closest(".provider_block");
                        _this.changeCheckBoxId =event.target.id;

                        var groupData = _this.model.allInsurancePolicies[0].planGroups[group[0].id];

                        if(isAnyInsuranceSelect(groupData.plans)){
                            onChangeInsurance(parseInt(ele[0].id),groupData);
                        }
                        else{
                            groupData.plans[ele[0].id].isSelected=true;
                            groupData.plans[ele[0].id].defaultSelection=true;
                           // document.getElementById(event.target.id).checked =true;
                           if(groupData.plans[ele[0].id].isSelected )
                                     _this.insuranceFinalAmount =_this.insuranceFinalAmount + groupData.plans[ele[0].id].amount;
                            //$("#"+group[0].id+" "+"#radAnciIns_Y")[0].checked =false;
                            $scope.$apply();
                            return;

                        }

                    });
                }

                if ($(".insurance_block input[name='radAnciIns']").length>0){
                    $(document).off("click").on("click", ".insurance_block input[name='radAnciIns']",  function(event){
                        var ele = $(event.currentTarget).closest(".insurance_block");
                        var group=$(event.currentTarget).closest(".provider_block");
                        _this.changeCheckBoxId =event.target.id;

                        var groupData = _this.model.allInsurancePolicies[0].planGroups[group[0].id];


                        if($('.insurance_block input:radio[name=radAnciIns]:checked').attr('id') == "radAnciIns_Y"){
                            groupData.plans[ele[0].id].isSelected=true;
                            groupData.plans[ele[0].id].defaultSelection=true;
                            document.getElementById(event.target.id).checked =true;
                           // _this.insuranceFinalAmount += groupData.plans[ele[0].id];
                            $scope.$apply();
                            return;
                        }
                        else{
                            onChangeInsurance(parseInt(ele[0].id),groupData);

                        }
                    });
                }

            }

            this.getInsurance = function(nextID,changeID){
                 if($rootScope.deviceType.isMobileDevice){
                     if(this.model.changeGroup.plans[changeID].isSelected){
                         /*for(var k=0;k<this.model.changeGroup.plans.length;k++){
                            if(this.model.changeGroup.plans[k].isSelected ){
                                if(document.getElementById('radAnciIns_Y_Rider') === undefined){
                                     _this.insuranceFinalAmount = this.model.changeGroup.plans[k].amount;
                                }
                            }
                            
                       }*/
//                        if(document.getElementById('radAnciIns_Y_Rider') !== undefined){
//                          var insuranceTypes = _this.model.allInsurancePolicies;
//                          var inslength = insuranceTypes[0].planGroups.length;
//                          if(inslength > 1){
//                              _this.insuranceFinalAmount = _this.insuranceFinalAmount - this.model.changeGroup.plans[k].amount;
//                          }
//                     }
                     }else{
                         this.model.changeGroup.planName = this.model.changeGroup.plans[nextID].itemName;
                        document.getElementById('insProductName').innerHTML = this.model.changeGroup.plans[nextID].itemName;
                        document.getElementById( _this.changeCheckBoxId).checked = true;
                   // this.model.changeGroup.plans[0].thingsCovered= document.getElementById('insurangeCover').innerHTML;
                        this.model.changeGroup.plans[changeID].isSelected = false;
                        this.model.changeGroup.plans[nextID].isSelected = true;
                        this.model.changeGroup.plans[nextID].defaultSelection=true;
                        this.model.changeGroup.plans[changeID].isDisplay =false;
                        this.model.changeGroup.plans[nextID].isDisplay =true;
                     }
                     this.model.isPopupOpen = false;
                    
                        document.getElementById('radAnciIns_Y').checked=true;
                 }else{
                     this.model.changeGroup.planName = this.model.changeGroup.plans[nextID].itemName;
                    document.getElementById('insProductName').innerHTML = this.model.changeGroup.plans[nextID].itemName;
                    document.getElementById( _this.changeCheckBoxId).checked = true;
               // this.model.changeGroup.plans[0].thingsCovered= document.getElementById('insurangeCover').innerHTML;
                    this.model.isPopupOpen = false;
                    this.model.changeGroup.plans[changeID].isSelected = false;
                    this.model.changeGroup.plans[nextID].isSelected = true;
                    this.model.changeGroup.plans[nextID].defaultSelection=true;
                    this.model.changeGroup.plans[changeID].isDisplay =false;
                    this.model.changeGroup.plans[nextID].isDisplay =true;
                    document.getElementById('radAnciIns_Y').checked=true;
                   
                 }
                
            };

            this.removeInsurance = function(index){
                this.model.isPopupOpen = false;
                for(var k=0;k<this.model.changeGroup.plans.length;k++){
                    if(_this.insuranceFinalAmount > 0  && this.model.changeGroup.plans[k].isSelected )
                     _this.insuranceFinalAmount =_this.insuranceFinalAmount - this.model.changeGroup.plans[k].amount;
                    this.model.changeGroup.plans[k].isSelected= false;
                    $("#"+index+" "+"#radAnciIns_Y")[0].checked =false;
                    
                }

            };
            function onChangeInsurance(id,groupData){
                _this.model.changeGroup = groupData;
                if(groupData.plans.length ===1){
                    if(_this.insuranceFinalAmount > 0  && _this.model.changeGroup.plans[id].isSelected )
                     _this.insuranceFinalAmount =_this.insuranceFinalAmount - _this.model.changeGroup.plans[id].amount;
                    _this.model.changeGroup.plans[id].isSelected =  !_this.model.changeGroup.plans[id].isSelected;
                    $scope.$apply();
                    ancillaryService.setAnciState('INSURANCE',true);

                }
                else {
                    if(groupData.plans.length> (id+1)){
                        _this.model.nextPolicyId = (id+1)
                        //this.model.nextPolicy = groupData.plans[this.model.nextPolicyId];
                    }
                    else if(groupData.plans.length ==1){
                        _this.model.nextPolicyId = 0;
                    }
                    else{
                        _this.model.nextPolicyId = id;
                        //this.model.nextPolicy = groupData.plans[this.model.nextPolicyId];
                    }

                    _this.model.changedPolicyId = id;

                    if(isInsuranceValid(groupData) && groupData.plans.length> (id+1)){
                            _this.popUpData = groupData.plans[_this.model.nextPolicyId];

                            // this.popUpData.thingsCovered=$sce.trustAsHtml(this.popUpData.thingsCovered);

                        _this.model.isPopupOpen = true;

                        $scope.$apply();
                        ancillaryService.setAnciState('INSURANCE',true);

                    }else{
                        alertService.setAlert('Exceeded maximum selection', 'warning');
                        _this.removeInsurance(id)
                    }
                }

            }

            function isInsuranceValid(groupData){
                var maxSelection = groupData.validation.maxSelections;
                var minSelection = groupData.validation.minSelections;
                var plans= groupData.plans;
                var t =0;
                for(var i =0;i<plans.length;i++){
                    if(plans[i].isSelected){
                        t=t+1;
                    }
                }
                if(t<= maxSelection){
                    return true;
                }
                else{
                    return false;
                }
               // groupData.selectedCount =t;
            }
            this.backToExtras = function(){
                updateInsurance();
                $rootScope.extraType = '';
                ancillaryService.backToExtras();
            };

            this.skipServices = function(){
                ancillaryService.skipServices("INSURANCE");
                $rootScope.extraType = '';
            };

            this.backToHome = function() {
                navigationService.modifyFlowHome();
            };

            function insuranceTotal() {
                angular.forEach(_this.insurancePreferance.scopes[0].preferences, function (pref, key) {
                    var insuranceObj = {}
                    insuranceObj.selectedItems = pref.selectedItems;
                    var anciTotal = ancillaryService.getSegmentTotal({pref:insuranceObj});
                    pref.total = anciTotal;
                })
                var segmentTotal = ancillaryService.getAncileryTotal(_this.insurancePreferance.scopes[0].preferences);
                _this.insurancePreferance.total = segmentTotal;

            }
            function updateInsurance(){
                var insuranceTypes =  _this.model.allInsurancePolicies;
                _this.insurancePreferance.scopes[0].preferences[0].selectedItems=[]

                for(var i = 0;i<insuranceTypes.length;i++){
                    for(var j =0 ;j< insuranceTypes[i].planGroups.length;j++){
                        for(var k=0;k<insuranceTypes[i].planGroups[j].plans.length;k++){
                            if(insuranceTypes[i].planGroups[j].plans[k].isSelected){
                                var temp ={}
                                temp.id = insuranceTypes[i].planGroups[j].plans[k].policyID ;
                                temp.quantity=null;
                                temp.input=null;
                                temp.price = insuranceTypes[i].planGroups[j].plans[k].amount;
                                //temp.amount= insuranceTypes[i].amount;
                                _this.insurancePreferance.scopes[0].preferences[0].selectedItems.push(temp);
                            }
                        }

                    }
                }
                insuranceTotal();
               // ancillaryService.updateAncillaryTotal("INSURANCE"); check with Indika
            }

            function updateProviderName(){
                var insuranceTypes =  _this.model.allInsurancePolicies;
                _this.insurancePreferance.scopes[0].preferences[0].selectedItems=[]

                for(var i = 0;i<insuranceTypes.length;i++){
                    for(var j =0 ;j< insuranceTypes[i].planGroups.length;j++){
                        if($("#"+j+" "+"#radAnciIns_Y").length){
                            $("#"+j+" "+"#radAnciIns_Y")[0].checked =false;
                        }
                        else{
                            $("#radAnciIns_Y_Rider")[0].checked =false;
                        }
                        for(var k=0;k<insuranceTypes[i].planGroups[j].plans.length;k++){
                            if($("#spnInsCost_Rider")[0]){
                                $("#spnInsCost_Rider")[0].innerHTML= currencyService.getSelectedCurrency()+" "+ insuranceTypes[i].planGroups[j].plans[k].amount;
                            }
                            if(insuranceTypes[i].planGroups[j].plans[k].isSelected && insuranceTypes[i].planGroups[j].plans[k].defaultSelection ){
                                if($("#"+j+" "+"#radAnciIns_Y").length){
                                    $("#"+j+" "+"#radAnciIns_Y")[0].checked =true;
                                    $("#"+j+" "+"#insProductName")[0].innerHTML = insuranceTypes[i].planGroups[j].plans[k].itemName;
                                }
                                else{
                                    $("#radAnciIns_Y_Rider")[0].checked =true;
                                    if($("#"+j+" "+"#insProductName_Rider") !== undefined)
                                    $("#"+j+" "+"#insProductName_Rider")[0].innerHTML = insuranceTypes[i].planGroups[j].plans[k].itemName;
                                }
/*
                                $("#"+j+" "+"#insProductName")[0].innerHTML = insuranceTypes[i].planGroups[j].plans[k].itemName;
                                $("#"+j+" "+"#radAnciIns_Y_Rider")[0].checked =true;*/

                            }
                        }
                        $scope.$apply();

                    }
                }

            }

            function isAnyInsuranceSelect(plans){
                var isSelect =false;
                for(var i = 0;i<plans.length;i++){
                    if(plans[i].isSelected){
                        isSelect =true;
                        return isSelect;
                    }
                }
                return isSelect;
            }

        }];
    return controller;
};
