/**
 * Created by indika on 12/4/15.
 */
var baggageController = function () {

    var controllers = {};

    controllers.baggageController = [
        "$scope", "$location", "stepProgressService", "$rootScope", "passengerService", "extrasFactory",
        "extrasService", "ancillaryService", "ancillaryRemoteService", "IBEDataConsole", "loadingService",
        "applicationService", "quoteService","languageService", "currencyService", "$state", "alertService",
        "modifyReservationService", "constantFactory", "commonLocalStorageService", "popupService", "navigationService",
        baggageController];

        function baggageController ($scope, $location, stepProgressService, $rootScope, passengerService,extrasFactory,
                                    extrasService, ancillaryService, ancillaryRemoteService, IBEDataConsole,
                                    loadingService,applicationService, quoteService, languageService, currencyService,
                                    $state,alertService,modifyReservationService, constantFactory,
                                    commonLocalStorageService, popupService, navigationService) {
            // For mobile menus
            if(!$rootScope.deviceType.isMobileDevice){
                $rootScope.removePaddingFromXSContainerClass=true;
            }
            
            // For mobile menus            
            var self = this;
            self.accordionArray = [];
            self.currency = currencyService.getSelectedCurrency();
            stepProgressService.setCurrentStep('extras');

            var sectorCount = 0;
            var trip = 0;
            var extrasModel = {};
            var selectedTemp, unModifiedSectorData, journeyType,appMode,appState,urlParams,currentPnr;
            var baggageWeightArray = {};

            var baggage = {
                price: 0,
                value: 0,
                baggageId:null

            };

            this.onSelectWeight = onSelectWeight;
            this.onEditBaggage = onEditBaggage;
            this.nextSector = nextSector;
            this.backToExtras = backToExtras;
            this.skipServices = skipServices;
            var unmodifiedweightsList = {};
            this.backToHome = backToHome;


            (function () {
                window.scrollTo(0, 0);

                extrasModel = extrasService.getExtras();
                quoteService.setShowSummaryDrawer(false);
                appMode = applicationService.getApplicationMode();
                self.appMode = appMode;
                currentPnr = modifyReservationService.getReservationSearchCriteria();

                if (appMode == applicationService.APP_MODES.ADD_MODIFY_ANCI || appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    if (ancillaryRemoteService.metaData) {
                        ancillaryService.setMetaData(ancillaryRemoteService.metaData.ondPreferences);
                    }
                    appState = 'modifyReservation';
                    self.appState = 'modifyReservation';
                    urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),pnr:currentPnr.pnr,lastName:currentPnr.lastName,departureDate:currentPnr.departureDate}
                }
                else{
                    appState = 'extras';
                    urlParams = {lang: languageService.getSelectedLanguage(), currency: currencyService.getSelectedCurrency(),mode: applicationService.getApplicationMode()}
                }

                self.tripType = Object.keys(extrasModel)[trip];
                self.showExtrasTotal = {index: sectorCount, section: self.tripType};
                self.sectorData = extrasModel;
                self.airportNames = {}

                if (Object.keys(passengerService.getPassengerDetails()).length) {
                    var paxData = passengerService.getPassengerDetails().paxList;
                }
                unModifiedSectorData = angular.copy(extrasModel);
                var param = "BAGGAGE";




                ancillaryRemoteService.loadBaggageData(param, function (response) {
                    loadingService.hidePageLoading();

                    if(response == null){
                        $state.go(appState, urlParams);
                    }
                    else {
                        IBEDataConsole.log(response);
                        //self.weightsList = _.sortBy( response[sectorCount].items, function(obj){ return parseInt(obj.weight,10)});
                        journeyType = response[sectorCount].scope.scopeType;
                        //renderWeightPanel(response.items)
                        //self.weightsList = sortedWeightList(response[0].items);

                        angular.forEach(response, function (sector, key) {
                            unmodifiedweightsList[sector.scope.ondId] = sector;
                            var flightSegment = sector.scope.ondId;
                            var scopeType = sector.scope.scopeType;
                            var sectorDetails = ancillaryService.getFlightDetails(flightSegment);
                            var ancillaryPrefs = ancillaryService.getAncillaryPreference(param, scopeType, flightSegment, sectorDetails);
                            baggageWeightArray[key] = sector.items;

                            //Note : Considering that default selected baggage is set as defaultItem in modify flow both modify and create flow will set default baggage selection in a similar manner

                            /*
                             * inset default weight in to each passenger model
                             */
                            var weightsList = sector.items
                            var defaultItem = _.findIndex(weightsList, {selected: true})

                            setDefaultSelectedBaggage(ancillaryPrefs, defaultItem, sector.items);

                            self.currentAncillary = ancillaryPrefs;

                        });
                        self.baggagePreferance = ancillaryService.getAncillaryFromResponse(param, response);
                        var selectedOndId = self.baggagePreferance.scopes[0].preferences[sectorCount].preferanceId;
                        if(unmodifiedweightsList[selectedOndId] !== undefined && unmodifiedweightsList[selectedOndId].items !== undefined) {
                        self.weightsList = sortedWeightList(unmodifiedweightsList[selectedOndId].items);
                        } else {
                        	self.weightsList = [];
			}
                        //When the baggage list is recived, the ancillary service object is created for each of the sectors.

                        self.currentScope = response[sectorCount].scope;

                        //self.baggagePreferance = ancillaryService.getAncillary(param);
                        self.currentSector = self.baggagePreferance.scopes[0].preferences[sectorCount];
                        self.passengers = self.baggagePreferance.scopes[0].preferences[sectorCount].passengers;

                        // Set weight unit for baggages (kg, lbs).
                        self.passengers.weightUnit = constantFactory.WeightUnit;
                        self.passengers.defaultWeight =  constantFactory.defaultWeight();
                        self.airportNames = getAirportName()
                        setBaggageTotal(appMode);

                    } // End of Else.

                }, function (error) {
                    loadingService.hidePageLoading();
                    alertService.setPageAlertMessage(error);
                });

                if(appMode == applicationService.APP_MODES.MODIFY_SEGMENT) {
                    var unmodifiedAnci = ancillaryService.getUnmodifiedAnci();

                    if(unmodifiedAnci && unmodifiedAnci[param]) {
                        self.previousSelection = unmodifiedAnci[param];
                    }
                }

        })(); // End of init function.
        function setDefaultSelectedBaggage(preferences,defaultItem,items) {
                // inset default weight in to each passenger model
                if (preferences.passengers != undefined) {
                    angular.forEach(preferences.passengers, function (pas, key) {
                        pas.selectedItems = (pas.selectedItems != undefined) ? pas.selectedItems : [];
                        if (appMode == applicationService.APP_MODES.CREATE_RESERVATION) {
                            if (pas.selectedItems[0] === undefined || Object.keys(pas.selectedItems).length == 0) {
                                if (defaultItem > -1) {
                                    pas.selectedItems[0] = baggage;
                                    pas.selectedItems[0].price = items[defaultItem].price;
                                    pas.selectedItems[0].value = items[defaultItem].weight;
                                    pas.selectedItems[0].id = items[defaultItem].baggageId;
                                    pas.selectedItems[0].quantity = 1;
                                    pas.selectedItems[0].input = null;
                                }
                            }
                        }
                        var passanger = angular.copy(preferences.passengers[key]);
                        preferences.passengers[key] = passanger;
                    });
                }

        };

        /*
            ordering the baggage weights in ascending order
             */
        function sortedWeightList(weightList){
            return weightList.sort(function(a,b) {
                var valueA = (isNaN(parseInt(a.weight))) ? 99999 : parseInt(a.weight);
                var valueB = (isNaN(parseInt(b.weight))) ? 99999 : parseInt(b.weight);
                return (valueA > valueB) ? 1 : ((valueA > valueB) ? -1 : 0);
            });

        }
            /*
            *get departure and arrival airport names in each segment or in the OND using airport code
             */
        function getAirportName () {
            var airport = {}
            var fromCode = "";
            var toCode = "";
            if (self.baggagePreferance.scopes[0].scopeType == "SEGMENT") {
                fromCode = self.currentSector.segments[0].from
                toCode = self.currentSector.segments[0].to
            } else if (self.baggagePreferance.scopes[0].scopeType == "OND") {
                var lastIndex = self.currentSector.segments.length - 1
                fromCode = self.currentSector.segments[0].from
                toCode = self.currentSector.segments[lastIndex].to
            }
            airport.searchCriteria = commonLocalStorageService.getSearchCriteria();

            airport.departureAirportName = extrasService.getAirportName(fromCode);
            airport.departureAirportCode = fromCode;

            airport.arrivalAirportName = extrasService.getAirportName(toCode);
            airport.arrivalAirportCode = toCode;

            return airport;
        };

        /*
         *  on select weight drop down panel click current selection will be processed
         *  and return the selected item
         *  @param id - selected dropdown index
         */
        function onEditBaggage (id,event) {
//            console.log(id);
//            console.log(self.currentAncillary)
                    var currentBaggageWeight = ( self.passengers[id] && self.passengers[id].selectedItems[0] && self.passengers[id].selectedItems[0].value != undefined) ? self.passengers[id].selectedItems[0].value : 0;
                    var selected = {}

            if (currentBaggageWeight > 0 || currentBaggageWeight != undefined) {
                        //scope.selectedIndex = id.id
                        var weightIndex = _.findIndex(self.weightsList, {weight: currentBaggageWeight})
        //                console.log(weightIndex)
                        selected.optionIndex = weightIndex;
                        selected.dropDownIndex = id;
                    } else {
                        if (selectedTemp) {
                            selected.optionIndex = selectedTemp.index;
                            selected.dropDownIndex = 0;
                        }
                    }
                    if(!!selected){
                        ancillaryService.setAnciState('BAGGAGE',true);
                    }
                    event.stopPropagation();
                    return selected;

        }


        /*
         *  When an item selected from the listed weight model will update with
         *  selected item.
         */
        function onSelectWeight (id, weight, price, index, section,baggageId,event) {
            var tempPref = angular.copy(self.baggagePreferance.scopes[0].preferences);
            var tempSegment = angular.copy(tempPref[sectorCount]);
            var tempPerson = angular.copy(tempSegment.passengers);
            tempPerson[section].selectedItems[0] = baggage;
            var selectedItem = angular.copy(tempPerson[section].selectedItems[0]);
            selectedItem.price = price;
            selectedItem.value = weight;
            selectedItem.baggageId = baggageId;

            selectedItem.id = baggageId;
            selectedItem.quantity = 1;
            selectedItem.input = null;
            tempPerson[section].selectedItems[0] = selectedItem;


            //tempSegment.total = anciTotal;
            //tempscope.total = segmentTotal;
            tempSegment.passengers = tempPerson;
            tempPref[sectorCount] = tempSegment;
            self.baggagePreferance.scopes[0].preferences = tempPref;
            //self.baggagePreferance.scopes[0].preferences[sectorCount] = tempSegment;
            //self.baggagePreferance = tempscope;
            self.passengers = self.baggagePreferance.scopes[0].preferences[sectorCount].passengers;

            setBaggageTotal(appMode);
            if(event){
                event.preventDefault();
                event.stopPropagation();
                
            }

        }


        /*
         *  calculate baggage total for each segment and the entire journey
         */
        function setBaggageTotal (appMode) {
            angular.forEach(self.baggagePreferance.scopes[0].preferences,function(pref,key){
            var anciTotal = ancillaryService.getSegmentTotal(pref.passengers,appMode);
                pref.total = (isNaN(anciTotal)) ? 0 : anciTotal;
            })
            var segmentTotal = ancillaryService.getAncileryTotal(self.baggagePreferance.scopes[0].preferences);
            self.baggagePreferance.total = segmentTotal;

        }


        function nextSector (index, section,$event) {
           
            // var obj =
            // extrasModel[Object.keys(extrasModel)[trip]][sectorCount].passenger
//            console.log(index)
            if (index === undefined) {
                sectorCount++;

                if (this.baggagePreferance.scopes[0].preferences[sectorCount] == undefined) {
                    sectorCount = 0;
                }
                //this.tripType = Object.keys(this.sectorData)[trip];
                this.currentSector = this.baggagePreferance.scopes[0].preferences[sectorCount];
                this.showExtrasTotal = {
                    index: sectorCount,
                    section: sectorCount
                }
            } else {
                //this.tripType = Object.keys(this.sectorData)[trip];
                this.currentSector = this.baggagePreferance.scopes[0].preferences[index];
                sectorCount = index;
                //trip = Object.keys(this.sectorData).indexOf(section);

            }
            self.passengers = self.baggagePreferance.scopes[0].preferences[sectorCount].passengers
            self.airportNames = getAirportName()
            var selectedOndId = self.baggagePreferance.scopes[0].preferences[sectorCount].preferanceId;
            if (unmodifiedweightsList[selectedOndId] !== undefined && unmodifiedweightsList[selectedOndId].items !== undefined) {
                self.weightsList = sortedWeightList(unmodifiedweightsList[selectedOndId].items);
            } else {
                self.weightsList = [];
            }
            var defaultItem = _.findIndex(baggageWeightArray[sectorCount], {selected: true});
            setDefaultSelectedBaggage(this.currentSector, defaultItem ,baggageWeightArray[sectorCount]);
            
            //console.log(this.currentSector);
            //TODO impliment a common method to travers through sectors
            //$rootScope.$emit('ON_NEXTSECTOR_CLICK',{index:index,section:section,sectorCount:sectorCount,trip:trip,sectorData:extrasModel});
            if($event){
                $event.preventDefault();
                $event.stopPropagation();
            }
        }

        function backToHome () {
            navigationService.modifyFlowHome();
        }

        function backToExtras () {
            var allItemsSelected = true;

            //Check if baggage is set for alll the sectors
            angular.forEach(self.baggagePreferance.scopes[0].preferences, function(pref, key){
                angular.forEach(pref.passengers, function(pax){
                    if(_.isEmpty(pax.selectedItems)){
                        allItemsSelected = false;
                    }
                })
            });
            $rootScope.extraType = '';
            if(allItemsSelected){
                ancillaryService.backToExtras();
            }else{
                popupService.confirm("You have not selected baggage for all passengers, do you want to continue?&lrm;", function(){
                    ancillaryService.backToExtras();
                }, function(){

                });
            }
        };


        function skipServices() {

            ancillaryService.skipServices("BAGGAGE");
            ancillaryService.setAnciState('BAGGAGE',false);
            $rootScope.extraType = '';
        };


        $scope.getLength = function (arr) {
            var length = Object.keys(arr).length - 1;
            return length;
        };


    }

    return controllers;
};