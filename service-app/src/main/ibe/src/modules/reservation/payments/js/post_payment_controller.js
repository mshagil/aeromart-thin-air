var postPaymentController = function () {

    var controllers = {};

    controllers.postPaymentController = [
        "stepProgressService", 
        "$location",
        "paymentRemoteService", 
        "paymentService",
        "loadingService", 
        "commonLocalStorageService", 
        "$state", 
        "languageService",
        "currencyService", 
        "alertService",
        "applicationService", "$sce",
        function (stepProgressService, 
                  $location, 
                  paymentRemoteService, 
                  paymentService, 
                  loadingService,
                  commonLocalStorageService, 
                  $state, 
                  languageService,
                  currencyService, 
                  alertService, 
                  applicationService, 
                  $sce) {

        (function init(){
            loadingService.showPageLoading();
            stepProgressService.setCurrentStep('payment');
            callToConfirmPayment();
        })();
            this.paymentRetry = false;
            this.paymentRetryMsg = "";
            var self = this;
        function callToConfirmPayment(){
            var urlParams = commonLocalStorageService.getData("postMap");
            var postParam = commonLocalStorageService.getData("postUrl");

            if(postParam){
                postParam = postParam.substr(postParam.indexOf('?')+1);
            }

            applicationService.setApplicationMode(commonLocalStorageService.getData('APP_MODE'));

            var params = {
                "paymentReceiptMap":urlParams
            };

            if(urlParams.encoded){
                params = {
                    "paymentReceiptMap":null,
                    "postParam": postParam
                };
            }

            params.paymode = applicationService.getApplicationMode();
            languageService.setSelectedLanguage(commonLocalStorageService.getData('langTOStore')) ;
            currencyService.setSelectedCurrency(commonLocalStorageService.getData('CurrencyToStore'));
  
            // TODO: uncomment after paymentReceipt function is completed.

             paymentRemoteService.paymentReceipt(params, function(response){
                     commonLocalStorageService.remove("postMap");
                     commonLocalStorageService.remove('langTOStore');
                     commonLocalStorageService.remove('CurrencyToStore');

                     commonLocalStorageService.setData('PNR', response.pnr);
                     commonLocalStorageService.setData('PNR_lastName', response.contactLastName);
                     commonLocalStorageService.setData('PNR_firstDepartureDate', response.firstDepartureDate);
                     commonLocalStorageService.setData('RES_STATUS', "CNF");
                     // Set igpTransactionResults if provided
                     if(response.ipgTransactionResult){
                         commonLocalStorageService.setData('ipgResults',response.ipgTransactionResult);
                     }

                     $state.go('confirm', {
                         lang: languageService.getSelectedLanguage(),
                         currency: currencyService.getSelectedCurrency(),
                         mode: applicationService.getApplicationMode()
                     });
             },
             function(responce){
                 if (responce.errorType === "PAYMENT_ERROR"){
                     self.paymentRetry = true;
                     paymentService.setPaymetErrorMsg(responce.error);
                     loadingService.hidePageLoading();
                     $state.go('payments',
                         {
                             lang: languageService.getSelectedLanguage(),
                             currency: currencyService.getSelectedCurrency(),
                             mode: applicationService.getApplicationMode()
                         }
                     )
                 }else if(responce.errorType === "PAYMENT_ERROR_ONHOLD"){
                     // TODO - Need to handle this issue
                     paymentRemoteService.getOnholdPNR(function(data){
                         var errorMsg = responce.error + " Your booking is placed ON-HOLD for "+data.onholdReleaseDuration+", please make the payment & confirm your booking. Bookings not paid for within the time limit will automatically be canceled"
                         commonLocalStorageService.setData('PAYMENTONHOLD', {pnr:data.pnr,error:errorMsg});
                         applicationService.setApplicationMode(applicationService.APP_MODES.BALANCE_PAYMENT);
                         commonLocalStorageService.setData('RES_STATUS', "OHD");
                         $state.go('modifyReservation', {
                             lang: languageService.getSelectedLanguage(),
                             currency: currencyService.getSelectedCurrency(),
                             pnr: data.pnr,
                             lastName: data.lastName,
                             departureDate: data.firstDepartureDate
                         });

                     },function(){

                     });
                 }else{
                     alertService.setPageAlertMessage(responce);
                 }
             });

        } // End of callToConfirmPayment

    }]; // End of controllers.postPaymentController.

    return controllers;

} // End of postPaymentController.
