/**
 * Created by indika on 11/5/15.
 */
var passengerService = function () {
    var service = {};

    service.passengerService = ["commonLocalStorageService", "$http", "httpServices", "eventListeners", "travelFareService", "customerRemoteService", "passengerFactory", passengerService];

    function passengerService(commonLocalStorageService, $http, httpServices, eventListeners, travelFareService, customerRemoteService, passengerFactory) {

        // Global vars.
        var self = this;
        var passengerDetails = {};


        // Properties.
        self.EVENTS = {
            PAX_DETAILS_CHANGED: "pax_details_changed"
        };


        // Functions.
        self.getPaxDetailsFromReservation = getPaxDetailsFromReservation;
        self.getParamsWithData = getParamsWithData;
        self.getSearchCriteria = getSearchCriteria;
        self.setPaxDetails = setPaxDetails;
        self.getPaxDetails = getPaxDetails;
        self.getPassengerDetails = getPassengerDetails;
        self.getPassengerDetailsForAnci = getPassengerDetailsForAnci;
        self.getForgetPassword = getForgetPassword;
        self.getPassengerList = getPassengerList;

        /**
         *  Password recovery service function.
         *  @param params: password recovery request object.
         *  @param successFn: success function.
         *  @param errorFn: error function.
         */
        function getForgetPassword(params, successFn, errorFn) {
            customerRemoteService.passwordRecovery(params, successFn, errorFn);
        }


        function getPaxDetailsFromReservation(reservationPaxInfo) {
            var paxDetails = {
                paxContact: {},
                paxDetailsCompleted: true,
                paxList: []
            };
            if (!!reservationPaxInfo) {
                angular.forEach(reservationPaxInfo, function (pax) {
                    paxDetails.paxList.push({
                        airewardsEmail: pax.additionalInfo.ffid,
                        category: pax.paxType,
                        type : pax.paxType.toLowerCase(),
                        displayName: pax.title + "." + pax.firstName + " " + pax.lastName,
                        dob: pax.dateOfBirth,
                        firstName: pax.firstName,
                        lastName: pax.lastName,
                        nationality: pax.nationality,
                        paxSequence: pax.paxSequence,
                        salutation: pax.title,
                        travelingWith: pax.travelingWith
                    });

                    if(pax.travelingWith){
                        var travelingWith = _.find(paxDetails.paxList, function(adult){
                            return pax.travelingWith === adult.paxSequence;
                        });
                        travelingWith.infantSequance = pax.paxSequence;
                    }
                });
            }
            return paxDetails;
        }


        function getParamsWithData(params){
        	var params = [];
            var flightDataList = commonLocalStorageService.getConfirmedFlightData();
            
            angular.forEach(flightDataList, function (flightData, key) {
            	var param = passengerFactory.PaxContactsCongigsParamsDTO();
            	param.origin = flightData.flight.originCode;
                param.destination = flightData.flight.destinationCode;
                param.appCode = "APP_IBE";
                param.carriers = flightData.flight.careerCode;
                params.push(param);
            });
            var paxConfigRQ = {};
            paxConfigRQ.configDetailsRQs = params;
            return paxConfigRQ;
        }


        function getSearchCriteria() {
            return commonLocalStorageService.getSearchCriteria();
        };


        function setPaxDetails(paxDetails) {
            passengerDetails = paxDetails;

            eventListeners.notifyListeners(this.EVENTS.PAX_DETAILS_CHANGED);
            commonLocalStorageService.setData('PAX_DETAIL', paxDetails);
        }


        function getPaxDetails() {
            return travelFareService.getSectors();
        };


        function getPassengerDetails() {
            return commonLocalStorageService.getData('PAX_DETAIL') || {};
        }


        function getPassengerDetailsForAnci(){
            var temp =commonLocalStorageService.getData('PAX_DETAIL') || {};
            var fullPaxList = temp.paxList;
            var paxList =[];
            angular.forEach(fullPaxList, function (passenger, key) {
                if(passenger.category !== 'Infant') {
                    paxList.push(passenger);
                }
            });
            return paxList;

        }
        function getPassengerList(){
            var pax = commonLocalStorageService.getData('PAX_DETAIL') || {};
            return pax.paxList

        }


    }

    return service;
};
