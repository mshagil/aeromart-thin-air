var voucherThankController = function () {
    var controllers = {};

    controllers.voucherThankController = [
        "$http",
        "$location",
        "$timeout",
        "$state",
        "$stateParams",
        "$rootScope",
        "stepProgressService",
        "paymentRemoteService",
        "$window",
        "popupService",
        "alertService",
        "languageService",
        "applicationService",
        "currencyService",
        "constantFactory",
        "commonLocalStorageService",
        "remoteVoucher",
        "loadingService",
        "voucherService",
        function ($http, $location, $timeout, $state, $stateParams,
                  $rootScope, stepProgressService, paymentRemoteService,
                  $window, popupService, alertService, languageService, applicationService, currencyService, constantFactory,
                  commonLocalStorageService, remoteVoucher, loadingService, voucherService) {
            /*
             * self.contactData=$rootScope.contactData;
             * self.resData=$rootScope.resData;
             *
             * console.log(self.resData, $rootScope.resData);
             */
            var self = this;
            self.resData = [];
            self.voucherGroupId = commonLocalStorageService.getData('voucherGroupId');
            self.voucher_transactionId = commonLocalStorageService.getData('voucher_transactionId');
            self.selectAll = true;
            this.steps = stepProgressService.getAllSteps();
            this.steps[6].status = "completed";
            this.steps[7].status = "completed";
            self.isUserLoggedIn = applicationService.getCustomerLoggedState();
            self.cardHolderName = null;
            voucherService.clearVoucherInformation();

            (function init() {
                loadingService.showPageLoading();
                //stepProgressService.setCurrentStep('payment');
                remoteVoucher.getVouchersFromGroupId(self.voucherGroupId, self.voucher_transactionId, function (response, self) {
                    if (!_.isEmpty(response.voucherDTOList)) {
                        self.cardHolderName = response.voucherDTOList[0].voucherPaymentDTO.cardHolderName;
                        self.resData = response.voucherDTOList;
                        if (self.selectAll) {
                            self.selectedVocher = _.clone(self.resData);
                            selectOrUnSelectAll(true);
                        }
                    }
                    loadingService.hidePageLoading();
                }, function () {
                    loadingService.hidePageLoading();
                }, self);

            })();


            applicationService.setTransactionID(null);


            self.selectAllCheck = function () {
                if (!_.isEmpty(self.selectedVocher)
                    && (self.resData.length == self.selectedVocher.length)) {
                    // It may look like #selectUnSelectAll method can be executed finally
                    // But it needs this trick because we change the reference here,
                    // previously selected data objects needs to reset here
                    // So please do not move if logic is not understood correctly.
                    selectOrUnSelectAll(false);
                    self.selectedVocher = [];
                    self.selectAll = false;
                } else {
                    self.selectAll = true;
                    self.selectedVocher = _.clone(self.resData);
                    selectOrUnSelectAll(true);
                }
            };

            function selectOrUnSelectAll(value) {
                _.each(self.selectedVocher, function (voucher) {
                    voucher.selected = value;
                })
            }

            self.vocherSelected = function (voucher) {
                if (_.contains(self.selectedVocher, voucher)) {
                    self.selectedVocher = _.without(self.selectedVocher, voucher);
                    voucher.selected = false;
                } else {
                    self.selectedVocher.push(voucher);
                    voucher.selected = true;
                }
                self.selectAll = !_.isEmpty(self.selectedVocher) && (self.resData.length == self.selectedVocher.length);
            };

            self.print = function () {
                if (self.selectedVocher.length > 0 || self.selectAll) {
                    var dialogTranslate = {
                        popupblocker: "Please enable pop-up blocker to print vochers",
                    };

                    for (var i = 0; i < self.selectedVocher.length; i++) {
                        delete self.selectedVocher[i].voucherPaymentDTO;
                    }
                    var params = {
                        voucherDTOList: self.selectedVocher
                    }

                    paymentRemoteService.printVouchers(params, function (response) {
                        var printWindow = $window.open("");
                        if (!printWindow) {
                            popupService.alert(dialogTranslate.popupblocker);
                        } else {
                            printWindow.document.body.innerHTML = response;
                            printWindow.print();
                        }
                    }, function (error) {
                        alertService.setAlert(error, "danger");
                    })
                } else {
                    alertService.setAlert("lbl_no_voucher_selected_msg", "danger");
                }
            }
            self.sendEmail = function () {
                if (self.selectedVocher.length > 0 || self.selectAll) {

                   for (var i = 0; i < self.selectedVocher.length; i++) {
                        delete self.selectedVocher[i].voucherPaymentDTO;
                    }
                    var params = {
                        voucherDTOList: self.selectedVocher,
                        cardHolderName: self.cardHolderName
                    }
                    paymentRemoteService.emailVouchers(params, function (response) {
                        alertService.setAlert(response.messages[0]);
                    }, function (error) {
                        alertService.setAlert(error, "danger");
                    })
                } else {
                    alertService.setAlert("lbl_no_voucher_selected_msg", "danger");
                }
            };

            self.finish = function () {
                // Get the url to be redirected to
                var url = constantFactory.AppConfigStringValue("defaultAirlineURL");
                commonLocalStorageService.remove('voucherGroupId');
                if (self.isUserLoggedIn) {
                    // Change stage to modifyDashboard page.
                    $state.go('modifyDashboard', {
                        lang: languageService.getSelectedLanguage(),
                        currency: currencyService.getSelectedCurrency(),
                        mode: applicationService.getApplicationMode()
                    });
                } else {
                    $window.location.href = url;
                }
            }
        }];
    return controllers;

};
