var voucherService = function () {
    var service = {};
    service.voucherService = [
        "commonLocalStorageService",
        "localStorageService",
        function (commonLocalStorageService, localStorageService) {

            var self = this;
            self.VOUCHER_DETAILS = 'voucher_details';


            self.setUpdatedVoucherInformation = function (voucherData) {
                localStorageService.set(self.VOUCHER_DETAILS, voucherData);
            };

            self.getVoucherInformation = function () {
                return localStorageService.get(self.VOUCHER_DETAILS) || [];
            };

            self.clearVoucherInformation = function () {
                localStorageService.set(self.VOUCHER_DETAILS, []);
            };

        }];
    return service;
};
