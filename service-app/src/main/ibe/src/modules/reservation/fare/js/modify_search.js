/**
 * Created by indika on 11/5/15.
 */
var modifySearchController = function () {
    var controllers = {}
    var searchData;
    var cabinClass = {"Y": "Economy", "C": "Business"};

    controllers.modifySearchController = [
        'travelFareService',
        '$location',
        '$rootScope',
        '$element',
        '$translate',
        'remoteStorage',
        'stepProgressService',
        'alertService',
        'httpServices',
        'datePickerService',
        'commonLocalStorageService',
        'currencyService',
        'eventListeners',
        'popupService',
        'ancillaryService',
        'languageService',
        'applicationService',
        'constantFactory',
        "formValidatorService",
        'dateTransformationService',
        function (travelFareService, $location, $rootScope, $element,$translate, remoteStorage, stepProgressService, alertService, httpServices,
                datePickerService, commonLocalStorageService, currencyService, eventListeners, popupService, ancillaryService, languageService, applicationService, constantFactory,  formValidatorService, dateTransformationService) {

            var self = this;
            var returnDate = '';
            var selectedCurrencyObj;
            // console.log(formValidatorService)
            self.searchValidation = formValidatorService.getValidations();
            console.log(self.searchValidation)
            self.origins = (typeof origins != 'undefined') ? origins : [];
            self.selectedCabinType = 'Economy';
            self.isValid = true;
            self.cabinTypes = [
                'Economy',
                'Business'
            ];

            self.minDepartureDate = datePickerService.today();
            self.minArrivalDate = datePickerService.today();
            self.isArrivalDate = true;
            self.isDepartureDate = true;
            self.maxChild = this.maxAdult = 9
            self.arrivalDatePlaceholder = {caption: "Arrival date", symbol: "*", isvisible: true};
            self.departureDate;
            self.returnDate;
            self.setCurrency = setCurrency;
            self.departureDateChanged = departureDateChanged;
            self.selectedLang = languageService.getSelectedLanguage();




            self.currencyList = currencyService.getCurrencyList();
            self.selectedLanguage = languageService.getSelectedLanguage();
            self.carrierCode = constantFactory.AppConfigStringValue('defaultCarrierCode');
            self.isPersianCalendarType = applicationService.getIsPersianCalendar();


            init();
            function init() {
                var currecyObject = getSelectedCurrencyObject(currencyService.getSelectedCurrency());
                self.setCurrency(currecyObject);
                self.isPromoEnabled = JSON.parse(constantFactory.AppConfigStringValue('enablePromoCode'));
            }

            function setCurrency(selectedCurrency) {
                selectedCurrencyObj = selectedCurrency;
                var languageSpecificCurrency = selectedCurrency[self.selectedLanguage] ? selectedCurrency[self.selectedLanguage] : selectedCurrency.en;
                self.selectedCurrency = languageSpecificCurrency;
            }


            self.searchCriteria = {
                "lang": 'EN',
                "currency": "AED",
                "from": "",
                "to": "",
                "depDate": "",
                "retDate": "",
                "adult": 1,
                "child": 0,
                "infant": 0,
                "cabin": "Y",
                "promoCode": ""
            };

            self.defaultCurrencyType = currencyService.getSelectedCurrency();
            self.airports = remoteStorage.getAirports();
            self.toAirports = remoteStorage.getToAirports();
            self.toAirportsForCityOrigin = remoteStorage.getToAirportsForCityOrigin();
            self.desAirports = self.airports;
            self.isModifyVisible = false;
            self.showCurrency = false;
            self.selected = "From Date";
            self.dateformat = datePickerService.setDateFormat(4);
            self.journeyType;

            self.dpOnClick = function ($event, opened, isEnabled) {
                if (isEnabled) {
                    if($rootScope.deviceType.isMobileDevice){
                        if(self.fromDateOpened){
                            self.fromDateOpened = false;
                        }
                        if(self.toDateOpened){
                            self.toDateOpened = false;
                        }
                    }
                    self[opened] = datePickerService.open();
                } else {
                    //$($event.currentTarget).parent().prev().prop('disabled',true);
                    //$('#search-departure-date').attr('disabled', true);
                }
            }

            eventListeners.registerListeners(eventListeners.events.SHOW_MODIFY_SEARCH, function () {
                self.showModifySearch();
            })

            self.showModifySearch = function () {
                console.log(currencyService.getSelectedCurrency())
                var isMobile = window.matchMedia("only screen and (max-width: 767px)").matches;
                if (isMobile && !self.isModifyVisible) {
                    $('body').addClass('fixedPos');
                } else if (isMobile && self.isModifyVisible) {
                    $('body').removeClass('fixedPos');
                }

                var selectedCurrency = getSelectedCurrencyObject(currencyService.getSelectedCurrency());
                if (selectedCurrency != undefined) {
                    var languageSpecificCurrency = selectedCurrency[self.selectedLanguage] ? selectedCurrency[self.selectedLanguage] : selectedCurrency.en;
                    self.selectedCurrency = languageSpecificCurrency;
                }

                searchData = commonLocalStorageService.getSearchCriteria();

                self.fromAirport = commonLocalStorageService.getAirportCityName(searchData.from, (searchData.originCity === 'Y') ? true : false );
                self.toAirPort = commonLocalStorageService.getAirportCityName(searchData.to, (searchData.destCity === 'Y') ? true : false );

                self.isModifyVisible = !self.isModifyVisible;
                self.searchFromAirport = self.fromAirport;
                self.searchCriteria.from = searchData.from;
                self.searchCriteria.to = searchData.to;
                
                self.searchCriteria.originCity = searchData.originCity;
                self.searchCriteria.destCity = searchData.destCity;

                self.departureDate = (new Date(formatDate(searchData.depDate)).getTime() !== 'NaN') ? new Date(formatDate(searchData.depDate)).getTime() : "";
                self.returnDate = (new Date(formatDate(searchData.retDate)).getTime() !== 'NaN') ? new Date(formatDate(searchData.retDate)).getTime() : "";

                //Set inital values for paxCount
                self.passengerDetails = {
                    adult: {
                        labelSingle: $translate.instant('lbl_common_adult'),
                        labelMulti: $translate.instant('lbl_common_adults'),
                        value: getNumber(searchData.adult, 1),
                        max: 9,
                        min: 1,
                        ageText: $translate.instant('lbl_modifysearch_adult_age', { max:12 })
                    },
                    child: {
                    	labelSingle: $translate.instant('lbl_common_child'),
                        labelMulti: $translate.instant('lbl_common_children'),
                        value: getNumber(searchData.child),
                        max: 8,
                        min: 0,
                        ageText: $translate.instant('lbl_modifysearch_child_age', { min:2, max:12 })
                    },
                    infant: {
                    	labelSingle: $translate.instant('lbl_common_infant'),
                        labelMulti: $translate.instant('lbl_common_infants'),
                        value: getNumber(searchData.infant),
                        max: 9,
                        min: 0,
                        ageText:$translate.instant('lbl_modifysearch_infant_age', { min:2 }) 
                    }
                };

                self.adultCount = self.maxInfant = searchData.adult;
                self.childCount = searchData.child;
                self.infantCount = searchData.infant;
                if(searchData !== undefined && searchData !== null && searchData !== '' && searchData.cabin !== undefined && searchData.cabin !== null && searchData.cabin !== '')
                	self.selectedCabinType = cabinClass[searchData.cabin];
                self.searchCriteria.promoCode = searchData.promoCode;

                if (searchData.retDate === "N") {
                    self.isArrivalDate = false;
                    self.arrivalDatePlaceholder.symbol = "";
                    self.arrivalDatePlaceholder.isvisible = true;
                } else {
                    self.isArrivalDate = true;
                    self.arrivalDatePlaceholder.symbol = "*";
                    self.arrivalDatePlaceholder.isvisible = false;
                }

            }; // End of modifySearch function.

            self.TripOptionSelect = function (event) {

                var type = $(event.currentTarget).attr('for');

                switch (type) {
                    case 'mod-oneway' :
                    {
                        self.isArrivalDate = false;
                        self.arrivalDatePlaceholder.symbol = "";
                        self.arrivalDatePlaceholder.isvisible = true;
                        returnDate = self.returnDate;
                        self.returnDate = "";
                    }
                        break;
                    case 'mod-return':
                    {
                        self.isArrivalDate = true;
                        self.arrivalDatePlaceholder.symbol = "*";

                        if (returnDate.length <= 0) {
                            self.arrivalDatePlaceholder.isvisible = true;
                            self.returnDate = self.departureDate;
                        }
                        else {
                            self.arrivalDatePlaceholder.isvisible = false;
                            self.returnDate = returnDate;
                        }
                    }
                        break;

                } // End of switch.

            }; // End of TripOptionSelect function.

            self.hideModifySearch = function () {
                self.isModifyVisible = false;
                $('body').removeClass('fixedPos');
            }

            self.getLocation = function (val) {
                var url = 'http://maps.googleapis.com/maps/api/geocode/json';
                return httpServices.getDestination(url, val);
            }

            self.onCabinItemSelect = function (val) {
                self.selectedCabinType = val;
            }

            self.getAdultCount = function (spinnerValue) {
                self.adultCount = spinnerValue
                self.maxChild = 9 - self.adultCount;
                self.maxInfant = self.adultCount;

                var infantCount = $('#search-infant-count').find('input').val();

                if (infantCount > self.adultCount) {
                    $('#search-infant-count').find('input').val(self.adultCount);
                }
            } // End of getAdultCount function.

            self.getChildCount = function (spinnerValue) {
                self.childCount = spinnerValue;
                self.maxAdult = 9 - this.childCount;
            }

            self.getInfantCount = function (spinnerValue) {
                self.infantCount = spinnerValue;
            }

            self.searchFlight = function (form) {
            	$('body').removeClass('fixedPos');
                stepProgressService.getStepByState('fare').status = "current";
                // Skip executing is the form is invalid.
                if (!form.$valid || self.noResults) {
                    return;
                }

                self.hideModifySearch();

                var frmApCode = (self.fromAirport.code !== undefined) ? self.fromAirport.code : searchData.from;
                var toApCode = (self.toAirPort.code !== undefined) ? self.toAirPort.code : searchData.to;
                
                var originCity = (self.fromAirport.city !== undefined) ? (self.fromAirport.city ? 'Y' : 'N') : searchData.originCity;
                var destCity = (self.toAirPort.city !== undefined) ? (self.toAirPort.city ? 'Y' : 'N') : searchData.destCity;


                if (frmApCode === toApCode) {
                    alertService.setAlert("Origin and Destination should be different.", "danger");
                    self.searchCriteria.to = undefined;
                    return;
                }
                if (self.returnDate) {
                    var departureDate = moment(self.departureDate).format('YYYY-MM-DD');
                    var returnDate = moment(self.returnDate).format('YYYY-MM-DD');
                    if (applicationService.getIsPersianCalendar()) {
                        departureDate = self.departureDate.split("/")[2] + "-" + self.departureDate.split("/")[1] + "-" + self.departureDate.split("/")[0];
                        returnDate = self.returnDate.split("/")[2] + "-" + self.returnDate.split("/")[1] + "-" + self.returnDate.split("/")[0];
                    }
                    if (moment(returnDate).isAfter(departureDate) || moment(returnDate).isSame(departureDate)) {

                    } else {
                        popupService.alert("Your return flight date cannot be prior to your departure flight date. Please select a future return flight or change the dates.")
                        return;
                    }
                }
                self.searchCriteria.adult = self.passengerDetails.adult.value;
                self.searchCriteria.child = self.passengerDetails.child.value;
                self.searchCriteria.infant = self.passengerDetails.infant.value;

                self.adultCount = self.searchCriteria.adult;
                self.childCount = self.searchCriteria.child;
                self.infantCount = self.searchCriteria.infant;

                self.searchCriteria.cabin = getCabinType(self.selectedCabinType);
                self.searchCriteria.currency = selectedCurrencyObj ? selectedCurrencyObj.code : currencyService.getSelectedCurrency();
                self.searchCriteria.from = formatAirPort(frmApCode, 'from');
                self.searchCriteria.originCity = originCity;
                self.searchCriteria.to = formatAirPort(toApCode, 'to');
                self.searchCriteria.destCity = destCity;
                self.searchCriteria.depDate = (moment(self.departureDate).format('DD-MM-YYYY') !== 'Invalid date') ? moment(self.departureDate).format('DD-MM-YYYY') : 'N';
                self.searchCriteria.retDate = (moment(self.returnDate).format('DD-MM-YYYY') !== 'Invalid date') ? moment(self.returnDate).format('DD-MM-YYYY') : 'N';
                self.searchCriteria.lang = languageService.getSelectedLanguage();
                if (applicationService.getIsPersianCalendar()) {
//                	if(self.isArrivalDate && self.returnDate !="")
//                		self.searchCriteria.retDate = self.returnDate.split("/")[0]+"-"+self.returnDate.split("/")[1]+"-"+self.returnDate.split("/")[2];
                    var depArray = [];
                    if (self.departureDate != "") {
                        depArray = self.departureDate.split("/");
                        var retArray = "";
                        var jalaaliObjectForRetDate = {};
                        var jalaaliObjectForDepDate = dateTransformationService.toGregorian(parseInt(depArray[2]), parseInt(depArray[1]), parseInt(depArray[0]));
                        if (self.isArrivalDate && self.returnDate != "") {
                            retArray = self.returnDate.split("/");
                            jalaaliObjectForRetDate = dateTransformationService.toGregorian(parseInt(retArray[2]), parseInt(retArray[1]), parseInt(retArray[0]));
                            self.searchCriteria.retDate = jalaaliObjectForRetDate.gd + "-" + jalaaliObjectForRetDate.gm + "-" + jalaaliObjectForRetDate.gy;
                        }
                        self.searchCriteria.depDate = jalaaliObjectForDepDate.gd + "-" + jalaaliObjectForDepDate.gm + "-" + jalaaliObjectForDepDate.gy;
                    }
                }

                var departureDate = self.searchCriteria.depDate + "/",
                        arrivalDate = self.searchCriteria.retDate + "/";


                var urlParam = 'fare/' + self.searchCriteria.lang + '/' +
                    self.searchCriteria.currency + '/' +
                    commonLocalStorageService.getData('CARRIER_CODE') + '/' +
                    frmApCode + '/' +
                    toApCode + '/' + 
                    departureDate + arrivalDate +
                    self.adultCount + '/' +
                    self.childCount + '/' +
                    self.infantCount + '/' + self.searchCriteria.cabin + 
                    ((self.searchCriteria.promoCode !== undefined && self.searchCriteria.promoCode !== "" && self.searchCriteria.promoCode !== null)? '/' + self.searchCriteria.promoCode :'/') ;
                var citySearchParam = ((originCity !== undefined && originCity !== "" && originCity !== null)? '/' + originCity :'/');
                	citySearchParam += ((destCity !== undefined && destCity !== "" && destCity !== null)? '/' + destCity :'/');
                	
	                urlParam +=citySearchParam;

                var searchParam = {
                    cabinClass: self.searchCriteria.cabin,
                    currency: self.searchCriteria.currency,
                    departureDate: (moment(self.departureDate).format('DD-MM-YYYY') !== 'Invalid date') ? moment(self.departureDate).format('DD-MM-YYYY') : 'N',
                    departureVariance: 3,
                    returnDate: (moment(self.returnDate).format('DD-MM-YYYY') !== 'Invalid date') ? moment(self.returnDate).format('DD-MM-YYYY') : 'N',
                    returnVariance: 3,
                    destination: toApCode,
                    destinationName: self.toAirPort,
                    origin: frmApCode,
                    originCity: originCity,
                    destCity: destCity,
                    originName: self.fromAirport,
                    passengers: {
                        adults: self.searchCriteria.adult,
                        children: self.searchCriteria.child,
                        infants: self.searchCriteria.infant
                    },
                    promoCode: self.searchCriteria.promoCode,
                    journeyType: self.isArrivalDate ? 'return' : 'oneway'
                };

                commonLocalStorageService.setSearchCriteria(self.searchCriteria);
                ancillaryService.clearAncillaryModel();

                // Application status is transfering to create mode.
                applicationService.setApplicationMode(applicationService.APP_MODES.CREATE_RESERVATION);
                //travelFareService.setSectorsOndInfo(searchParam);
                $location.path(urlParam);

            }; // End of searchFlight function.


            // Private functions ------

            //Get a number from string, return defaultValue or 0 if there is no number
            function getNumber(numString, defaultValue) {
                if (isNaN(parseInt(numString))) {
                    return defaultValue || 0;
                } else {
                    return parseInt(numString);
                }
            }
            function getAdultChildInfantTranslation(value) {
                return $translate.instant('lbl_common_' + value.toLowerCase());
            }

            function formatDate(date) {
                // return of date is undefined.
                if (!date) {
                    return;
                }

                var dateArray = date.split('-').reverse(),
                        formatedDate = dateArray.join('/');
                return formatedDate;
            }

            function getCabinType(type) {
                for (var cabinClassIndex in cabinClass) {
                    if (type === undefined || cabinClass[cabinClassIndex] === type) {
                        return cabinClassIndex;
                    }
                }
            }

            // Get currency object using currencyCode.
            function getSelectedCurrencyObject(currencyCode) {
                var currencyObject;
                for (var i = 0; i < self.currencyList.length; i++) {
                    if (self.currencyList[i].code === currencyCode) {
                        currencyObject = self.currencyList[i];
                        break;
                    }
                }
                return currencyObject;
            }

            // Format date for url param.
            function formatDateForParams(date) {
                // return of date is undefined.
                if (!date) {
                    return;
                }

                var dateArray = date.split('/'),
                        formatedDate = dateArray.join('-');
                return formatedDate;
            }

            function formatAirPort(obj, type) {

                if (typeof (obj) === "object") {
                    return obj.code;
                } else {
                    return obj;
                }
            }

            function departureDateChanged() {
                if (self.isArrivalDate) {
                    if (moment(self.departureDate).isAfter(self.returnDate)) {
                        self.returnDate = self.departureDate;
                    }
                }
            }

            this.filterDesAirport = function () {
            	if (self.fromAirport.city) {
            		self.desAirports = self.toAirportsForCityOrigin[self.fromAirport.code]
            	} else {
            		self.desAirports = self.toAirports[self.fromAirport.code]
            	}
                self.toAirPort = ''
            }

        }]; // End of modifySearchController function.

    return controllers;
};
