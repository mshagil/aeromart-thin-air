'use strict';

const getProxy = function () {


    var proxy = {
        passThrough:  true,
        host: process.env.AEROMART_THIN_AIR_SERVICE_IP || getDefaultURL(process.env.client),
        port: process.env.AEROMART_THIN_AIR_SERVICE_PORT || '443',
        protocol: 'https'
    };
    // Return it
    return proxy;
};

function getDefaultURL(client){
    console.log(client)
    switch (client) {
        case '--client=6Q':
            return '6qcmb9.isaaviations.com';
            //break;
        case '--client=G9':
            return 'g9cmb9.isaaviations.com';
        case '--client=KW':
            return 'kwcmb.isaaviations.com';
        default:
            return '6qcmb9.isaaviations11.com';
    }
};

module.exports = {
    clientProxy: getProxy()
};