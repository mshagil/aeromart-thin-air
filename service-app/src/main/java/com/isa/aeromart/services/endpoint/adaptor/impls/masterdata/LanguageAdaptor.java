package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.IBELanguage;
import com.isa.thinair.airmaster.api.model.Language;


public class LanguageAdaptor implements Adaptor<Language, IBELanguage>{

	@Override
	public IBELanguage adapt(Language source) {
		IBELanguage language = null;
		
		if(source.getIbeContent().equals("Y")){
			language = new IBELanguage();
			language.setLanguageCode(source.getLanguageCode());
			language.setLanguage(source.getLanguageName());
		}
		
		return language;
	}

}
