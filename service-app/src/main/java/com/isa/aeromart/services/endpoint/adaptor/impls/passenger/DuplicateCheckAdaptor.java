package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.passenger.DuplicateCheckRQ;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class DuplicateCheckAdaptor implements Adaptor<DuplicateCheckRQ, DuplicateValidatorAssembler> {

	private SYSTEM targetSystem;

	private List<SessionFlightSegment> flightSegments;

	public DuplicateCheckAdaptor(List<SessionFlightSegment> flightSegments, String selectedSystem) {
		this.targetSystem = ProxyConstants.SYSTEM.getEnum(selectedSystem);
		this.flightSegments = flightSegments;
	}

	@Override
	public DuplicateValidatorAssembler adapt(DuplicateCheckRQ duplicateCheckRQ) {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<>();
		List<LCCClientReservationPax> lccClientReservationPassengers = new ArrayList<>();
		DuplicateValidatorAssembler duplicateValidatorAssembler = new DuplicateValidatorAssembler();

		DuplicateCheckPaxAdaptor duplicateCheckPaxAdaptor = new DuplicateCheckPaxAdaptor();
		DuplicateCheckFlightSegAdaptor duplicateCheckFlightSegAdaptor = new DuplicateCheckFlightSegAdaptor();

		AdaptorUtils.adaptCollection(duplicateCheckRQ.getDuplicateCheckList(), lccClientReservationPassengers,
				duplicateCheckPaxAdaptor);
		AdaptorUtils.adaptCollection(flightSegments, flightSegmentTOs, duplicateCheckFlightSegAdaptor);

		duplicateValidatorAssembler.setTargetSystem(targetSystem);
		duplicateValidatorAssembler.setFlightSegments(flightSegmentTOs);
		duplicateValidatorAssembler.setPaxList(lccClientReservationPassengers);
		return duplicateValidatorAssembler;
	}

}
