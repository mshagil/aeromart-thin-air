package com.isa.aeromart.services.endpoint.service;

import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerContact;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerDetails;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerLoginRQ;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerSaveOrUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.customer.ForgotPasswordRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSCrossPortalLoginRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSDetails;
import com.isa.aeromart.services.endpoint.dto.customer.LMSRegisterRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSValidationRQ;
import com.isa.aeromart.services.endpoint.dto.customer.PasswordChangeRQ;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.wsclient.api.util.LMSConstants;

public class CustomerValidationServiceImpl implements CustomerValidationService{

	@Override
	public void validateLoginRQ(CustomerLoginRQ customerLoginRQ) throws ValidationException{
		
		if(customerLoginRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_LOGIN_REQUEST);
		}
		
		if(customerLoginRQ.getCustomerID() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(customerLoginRQ.getPassword() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
	}

	@Override
	public void validateRegisterUpdateRQ(CustomerSaveOrUpdateRQ customerSaveOrUpdateRQ) throws ValidationException {
		
		CustomerDetails customer;
		CustomerContact contact;
		LMSDetails lmsDetails;
		
		if(customerSaveOrUpdateRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_REGISTER_UPDATE_CUSTOMER_REQUEST);
		}
		
		if(customerSaveOrUpdateRQ.getCustomer() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}else{
			customer = customerSaveOrUpdateRQ.getCustomer();
		}
		
		if(customerSaveOrUpdateRQ.isOptLMS() && customerSaveOrUpdateRQ.getLmsDetails() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}else{
			lmsDetails = customerSaveOrUpdateRQ.getLmsDetails();
		}
		
		if(customer.getTitle() == null || customer.getTitle().isEmpty()){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(customer.getFirstName() == null || customer.getFirstName().isEmpty()){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(customer.getLastName() == null || customer.getLastName().isEmpty()){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(customer.getNationalityCode() == 0 ){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(customer.getCountryCode() == null || customer.getCountryCode().isEmpty()){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(customer.getCustomerContact() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}else{
			contact = customer.getCustomerContact();
		}
		
		if(contact.getEmailAddress() == null || contact.getEmailAddress().isEmpty()){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(contact.getMobileNumber() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		} else {
			validatePhoneNumber(contact.getMobileNumber());
		}
		
		if(customerSaveOrUpdateRQ.isOptLMS() && lmsDetails != null){
			if(lmsDetails.getDateOfBirth() == null){
				throw new ValidationException(ValidationException.Code.NULL_ERROR);
			}
			
			if(lmsDetails.getFfid() == null){
				throw new ValidationException(ValidationException.Code.NULL_ERROR);
			}
		}
				
	}

	private void validatePhoneNumber(PhoneNumber mobileNumber) {
		if (StringUtil.isNullOrEmpty(mobileNumber.getAreaCode())) {
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		} else if (StringUtil.isNullOrEmpty(mobileNumber.getCountryCode())) {
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		} else if (StringUtil.isNullOrEmpty(mobileNumber.getNumber())) {
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
	}

	@Override
	public void validateForgotPasswordRQ(ForgotPasswordRQ forgotPasswordRQ) throws ValidationException {
		if(forgotPasswordRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_FORGOTPASS_REQUEST);
		}
		
		if(forgotPasswordRQ.getCustomerID() == null || forgotPasswordRQ.getCustomerID().isEmpty()){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
	}

	@Override
	public void validateLMSValidationRQ(LMSValidationRQ lmsValidationReq, boolean isLoggedCustomer) throws ValidationException {
		if(lmsValidationReq == null){
			throw new ValidationException(ValidationException.Code.NULL_LMS_VALIDATION_REQUEST);
		}
		
		if(lmsValidationReq.getEmailId() == null || lmsValidationReq.getEmailId().isEmpty()){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		} else {
			AirCustomerServiceBD airCustomerServiceBD = ModuleServiceLocator.getCustomerBD();
			Customer customer = null;
			try {
				customer = airCustomerServiceBD.getCustomer(lmsValidationReq.getEmailId());
			} catch (ModuleException e) {
				throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
			}
			if (customer != null && customer.getEmailId().equalsIgnoreCase(lmsValidationReq.getEmailId()) && customer.getIsLmsMember() == 'Y' && !isLoggedCustomer) {
				throw new ValidationException(ValidationException.Code.CUSTOMER_ALREADY_REGISTERED);
			}
		}
	}

	@Override
	public void validateLMSRegisterRQ(LMSRegisterRQ lmsRegisterReq) throws ValidationException {
		
		LMSDetails lmsDetails;
		
		if(lmsRegisterReq == null){
			throw new ValidationException(ValidationException.Code.NULL_LMS_REGISTER_REQUEST);
		}
		
		if(lmsRegisterReq.getCustomerID() == null || lmsRegisterReq.getCustomerID().isEmpty()){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(lmsRegisterReq.getLmsDetails() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}else{
			lmsDetails = lmsRegisterReq.getLmsDetails();
		}
		
		if(lmsDetails.getDateOfBirth() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
		if(lmsDetails.getFfid() == null){
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
		
	}

	@Override
	public void validateProfileUpdate(CustomerDetails customer, int customerId) throws ValidationException {
		
		Customer customerModel = null;
		
		try {
			customerModel = ModuleServiceLocator.getCustomerBD().getCustomer(customerId);
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}
		
		if(customerModel == null){
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}
		
		if(!customer.getCustomerContact().getEmailAddress().equals(customerModel.getEmailId())){
			throw new ValidationException(ValidationException.Code.DIFFERENT_EMAIL);
		}
		
		if(!customer.getFirstName().equals(customerModel.getFirstName())){
			throw new ValidationException(ValidationException.Code.DIFFERENT_FIRSTNAME);
		}
		
		if(!customer.getLastName().equals(customerModel.getLastName())){
			throw new ValidationException(ValidationException.Code.DIFFERENT_LASTNAME);
		}

		if (customer.getDateOfBirth() != null || customerModel.getDateOfBirth() != null) {
			if (customer.getDateOfBirth() == null) {
				if (!customerModel.getDateOfBirth().equals(customer.getDateOfBirth())) {
					throw new ValidationException(ValidationException.Code.DIFFERENT_DATE_OF_BIRTH);
				}
			} else if (!customer.getDateOfBirth().equals(customerModel.getDateOfBirth())) {
				throw new ValidationException(ValidationException.Code.DIFFERENT_DATE_OF_BIRTH);
			}
		}
	}
	
	public void validateLMSDetails(LMSDetails lmsDetails) throws ValidationException{
		
		LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
		try{
			if (lmsDetails.getHeadOFEmailId() != null && !"".equals(lmsDetails.getHeadOFEmailId().trim())) {
				
				if (!StringUtil.isNullOrEmpty(lmsDetails.getFfid()) && !StringUtil.isNullOrEmpty(lmsDetails.getHeadOFEmailId()) && lmsDetails.getFfid().equalsIgnoreCase(lmsDetails.getHeadOFEmailId())) {
					throw new ValidationException(ValidationException.Code.FAMILY_HEAD_SAME_AS_LOGIN_EMAIL);
				}
				
				LmsMember headOfEmailFFID = lmsMemberDelegate.getLmsMember(lmsDetails.getHeadOFEmailId());
				if (headOfEmailFFID == null || headOfEmailFFID.getEmailConfirmed() == 'N') {
					throw new ValidationException(ValidationException.Code.FAMILYHEAD_EMAIL_DOES_NOT_EXIST);
				}
			}

			if (lmsDetails.getRefferedEmail() != null && !"".equals(lmsDetails.getRefferedEmail().trim())) {
				
				LmsMember referredEmailID = lmsMemberDelegate.getLmsMember(lmsDetails.getRefferedEmail());
				if (referredEmailID == null || referredEmailID.getEmailConfirmed() == 'N') {
					throw new ValidationException(ValidationException.Code.REFERRAL_EMAIL_DOES_NOT_EXIST);
				}
			}
			
		}catch(ModuleException e){
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}
		
		
	}

	public LoyaltyCustomerProfile validateLoyaltyCustomerProfile(LoyaltyCustomerProfile loyaltyCustomerProfile,
			boolean validateCustomerId) throws ValidationException {
		LoyaltyCustomerProfile loyaltyProfile = null;
		try {
			loyaltyProfile = ModuleServiceLocator.getLoyaltyCustomerBD().getLoyaltyCustomerByExample(loyaltyCustomerProfile);
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
		}

		if (loyaltyCustomerProfile.getCustomerId() == null && validateCustomerId) {
			throw new ValidationException(ValidationException.Code.LOYALTY_CUSTOMER_INVALID);
		}

		if (loyaltyCustomerProfile.getEmail() == null) {
			throw new ValidationException(ValidationException.Code.LOYALTY_LMS_INVALID);
		}

		if (loyaltyProfile == null) {
			throw new ValidationException(ValidationException.Code.LOYALTY_ACCOUNT_INVALID);
		} else {
			if (loyaltyProfile.getCustomerId() != null) {
				throw new ValidationException(ValidationException.Code.LOYALTY_ACCOUNT_LINKED);
			}
		}
		return loyaltyProfile;
	}

	@Override
	public void validateExistingLMSAccount(String emailId, boolean isReffered) throws ValidationException {
		LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();

		if (isReffered) {
			if (!LmsCommonUtil.isNonMandAccountAvailable(emailId, loyaltyManagementBD)) {
				throw new ValidationException(ValidationException.Code.LMS_INVALID_REFERED);
			}
		} else {
			try {
				LoyaltyMemberCoreDTO member = loyaltyManagementBD.getLoyaltyMemberCoreDetails(emailId);
				if (!LmsCommonUtil.isValidFamilyHeadAccount(emailId, member)) {
					if (!member.isEligibleForFamilyHead()
							&& member.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
						throw new ValidationException(ValidationException.Code.LMS_HEAD_FAMILY_ALREADY_LINKED);
					} else {
						throw new ValidationException(ValidationException.Code.LMS_INVALID_HEAD_FAMILY);
					}

				}
			} catch (ModuleException me) {
				throw new ValidationException(ValidationException.Code.LMS_INVALID_HEAD_FAMILY);
			}
		}
	}

	@Override
	public void validateNewRequestedLMSAccount(String email) throws ModuleException, ValidationException {
		Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(email);
		if (customer != null && customer.getLMSMemberDetails() != null) {
			throw new ValidationException(ValidationException.Code.LMS_ACCOUT_ALREADY_EXIST);
		}
	}
	
	@Override
	public void validateExistingName(LMSValidationRQ lmsValidationReq) throws ModuleException, ValidationException {

		LoyaltyMemberCoreDTO existingMember = ModuleServiceLocator.getLoyaltyManagementBD()
				.getLoyaltyMemberCoreDetails(lmsValidationReq.getEmailId());

		if (existingMember != null && existingMember.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
			if (!existingMember.getMemberFirstName().equalsIgnoreCase(lmsValidationReq.getFirstName())
					|| !existingMember.getMemberLastName().equalsIgnoreCase(lmsValidationReq.getLastName())) {
				if (!lmsValidationReq.isRegistration()) {
					throw new ValidationException(ValidationException.Code.LMS_REG_NAME_MISMATCH);
				} else {
					throw new ValidationException(ValidationException.Code.LMS_NAME_MISMATCH);
				}
			}
		}
	}

	@Override
	public void validateLMSCrossPortalLoginReq(LMSCrossPortalLoginRQ lmsCrossPortalLoginReq, String email)
			throws ModuleException {

		if(!AppSysParamsUtil.isLMSEnabled()){
			throw new ValidationException(ValidationException.Code.LMS_CROSS_PORTAL_LOGIN_INVALID);
		}
		
		if (!AppSysParamsUtil.isLMSMemberAuthenticationRequired()) {
			LoyaltyMemberCoreDTO existingMember = ModuleServiceLocator.getLoyaltyManagementBD()
					.getLoyaltyMemberCoreDetails(email);

			if (existingMember == null || existingMember.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				throw new ValidationException(ValidationException.Code.LMS_LOGIN_INVALID);
			}
		}

		if (lmsCrossPortalLoginReq == null || lmsCrossPortalLoginReq.getUrlRef() == null) {
			throw new ValidationException(ValidationException.Code.LMS_CROSS_PORTAL_LOGIN_INVALID);
		} else {
			switch (lmsCrossPortalLoginReq.getUrlRef()) {
			case "EARN":
				break;
			case "REDEEM":
				break;
			case "FAQS":
				break;
			case "RETRO":
				break;
			case "CONTACT":
				break;
            case "PARTNERS": 
            	break;
            case "CALCULATOR": 
            	break;
            case "ACTIVITIES": 
            	break;	
			default:
				throw new ValidationException(ValidationException.Code.LMS_CROSS_PORTAL_LOGIN_INVALID);
			}
		}
	}

	@Override
	public void validatePasswordChangeRQ(PasswordChangeRQ passwordChangeRQ, Integer customerId)
			throws ValidationException {
		
		if(customerId != null){
			Customer customerModel = null;
			
			try {
				customerModel = ModuleServiceLocator.getCustomerBD().getCustomer(customerId);
			} catch (ModuleException e) {
				throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
			}
			
			if(customerModel == null){
				throw new ValidationException(ValidationException.Code.SYSTEM_ERROR);
			}
			
			if(!passwordChangeRQ.getCurrentPassword().equals(customerModel.getPassword())){
				throw new ValidationException(ValidationException.Code.INCORRECT_CURRENT_PASSWORD);
			}
			
		}
		
		if(!passwordChangeRQ.getNewPassword().equals(passwordChangeRQ.getConfirmPassword())){
			throw new ValidationException(ValidationException.Code.PASSWORD_MISMATCH);
		}
		
	}
	
	
	
}
