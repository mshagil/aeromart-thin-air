package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.Rule;

public class ComplexRuleValidation extends Rule {

	private enum Operator {
		AND, OR
	}
	
	private String operator;
	
	private List<Rule> rules;

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }
}
