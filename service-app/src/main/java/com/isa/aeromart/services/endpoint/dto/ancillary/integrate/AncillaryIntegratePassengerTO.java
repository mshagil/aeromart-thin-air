package com.isa.aeromart.services.endpoint.dto.ancillary.integrate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;

public class AncillaryIntegratePassengerTO {

	private String passengerRph;

	private List<LCCClientExternalChgDTO> externalCharges;

	private List<LCCSelectedSegmentAncillaryDTO> ancillariesToAdd;

	private List<LCCSelectedSegmentAncillaryDTO> ancillariesToRemove;

	private BigDecimal netAmount;

    public AncillaryIntegratePassengerTO() {
		netAmount = BigDecimal.ZERO;
		ancillariesToAdd = new ArrayList<>();
		ancillariesToRemove = new ArrayList<>();
		setExternalCharges(new ArrayList<>());
    }

    public String getPassengerRph() {
		return passengerRph;
	}

	public void setPassengerRph(String passengerRph) {
		this.passengerRph = passengerRph;
	}

	public List<LCCClientExternalChgDTO> getExternalCharges() {
		return externalCharges;
	}

	public void setExternalCharges(List<LCCClientExternalChgDTO> externalCharges) {
		this.externalCharges = externalCharges;
	}

	// TODO -- rename
	public List<LCCSelectedSegmentAncillaryDTO> getSelectedAncillaries() {
		return ancillariesToAdd;
	}

	// TODO -- rename
	public void setSelectedAncillaries(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		this.ancillariesToAdd = selectedAncillaries;
	}

	public List<LCCSelectedSegmentAncillaryDTO> getAncillariesToRemove() {
		return ancillariesToRemove;
	}

	public void setAncillariesToRemove(List<LCCSelectedSegmentAncillaryDTO> ancillariesToRemove) {
		this.ancillariesToRemove = ancillariesToRemove;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }
}
