package com.isa.aeromart.services.endpoint.constant;

import java.util.Arrays;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class PaymentConsts {

	public static final int PAYMENT_ATTEMPT_LIMIT = AppSysParamsUtil.getPaymentAttemptLimit();
	public static final String CHARGE_BY_VALUE = "V";

	// Temporarily changed forward URL for Cinnamon mobile ibe
	public static final String WEB_FORWARD_URL_SUFFIX = "/ibe/reservation.html#/postPayment";

	public static final String WEB_VOUCHER_PURCHASE_FORWARD_URL_SUFFIX = "/ibe/reservation.html#/voucherPostPayment";

	public static final String MOBILE_FORWARD_URL_SUFFIX = "/mibe/#/postPayment";

	public static final String CONFIRM_ONHOLD_URL_SUFFIX = "/ibe/reservation.html#/confirmOnhold";

	public static final String STATUS_URL_SUFFIX = "/ibe/handleInterlineIPGResponse!executeStatusRequest.action";

	public static final String RECEIPT_URL_SUFFIX = "/ibe/receiptIPGResponse.action";

	public static final String PAYPAL_PG = "PAYPAL";
	public static final String CMI_FATOURATI="CMIFATOURATI";
	public static final String QIWI_OFFLINE = "Qiwi Offline";
	public static final String PAYFORT_PAY_AT_STORE_PG = "PAYFORT_PAY_AT_STORE";
	public static final String PAYFORT_PAY_AT_HOME_PG = "PAYFORT_PAY_AT_HOME";
	public static final String QIWI = "QIWI";
	public static final String PAY_FORT_ONLINE = "PAY_FORT_ONLINE";
	public static final String PAY_FORT_OFFLINE = "PAY_FORT_OFFLINE";
	public final static String REQ_PAYMENT_GATEWAY_ERROR = "pay.gateway.error.";
	public static final String TAP_OFFLINE_PG = "TAP_OFFLINE";
	public static final String EFAWATEER_OFFLINE = "EFAWATEER";

	public static final List<String> OFFLINE_PG_OPTIONS = Arrays.asList(PAYFORT_PAY_AT_STORE_PG, PAYFORT_PAY_AT_HOME_PG,
			TAP_OFFLINE_PG, PAY_FORT_OFFLINE, CMI_FATOURATI, EFAWATEER_OFFLINE);

	public static final List<String> OFFLINE_PG_OPTIONS_FOR_BALANCE_PAYMENT = Arrays
			.asList(PAYFORT_PAY_AT_STORE_PG, PAYFORT_PAY_AT_HOME_PG, TAP_OFFLINE_PG, EFAWATEER_OFFLINE);

	public static final String CASH = "CASH";

	public static final String STR_ERROR = "error";
	public static final String SESSION_TIMEOUT = "timeOut";
	public static final String RELEASE_TIME = "releaseTime";
	public static final String PNR = "pnr";
	public static final String TRACKING_NUMBER = "trackingNumber";
	public static final String SYS_ACCESS_POINT_KSK = "KSK";
	public static final String SYS_ACCESS_POINT = "IBE";
	public static final String REQ_KSK_USER = "sesKSKUser";

	public static final String HMAC_ALGO = "HmacSHA256";

	public enum OperationTypes {
		MAKE_ONLY(1), MODIFY_ONLY(2);

		private final Integer code;

		private OperationTypes(int code) {
			this.code = new Integer(code);
		}

		public Integer getCode() {
			return this.code;
		}
	}

	public enum BookingType {
		ONHOLD, NORMAL, NONE;

		public static BookingType getEnum(String strEnum) {
			for (BookingType enm : BookingType.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return BookingType.NONE;
		}
	}

}
