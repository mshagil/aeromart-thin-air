package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class TransferReservationSegmentsRQ extends TransactionalBaseRQ {
	
	private String language;
	
	private String type;
	
	private String pnr;

    private String alertId;
    
    private String pnrSegmentId;
    
	private String fltSegId;
	
	private Date departureDate;

	private Date arrivalDate;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getAlertId() {
		return alertId;
	}

	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}

	public String getPnrSegmentId() {
		return pnrSegmentId;
	}

	public void setPnrSegmentId(String pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

	public String getFltSegId() {
		return fltSegId;
	}

	public void setFltSegId(String fltSegId) {
		this.fltSegId = fltSegId;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

}
