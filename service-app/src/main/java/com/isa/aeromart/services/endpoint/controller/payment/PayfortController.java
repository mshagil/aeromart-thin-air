package com.isa.aeromart.services.endpoint.controller.payment;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PayFortPaymentUtils;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

@Controller
@RequestMapping(value = "payfort", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
public class PayfortController extends StatefulController {

	private static Log log = LogFactory.getLog(PayfortController.class);

	@ResponseBody
	@RequestMapping(value = "payAtStore/notification")
	public void payAtStoreNotificationCall(@RequestBody HttpServletRequest request) throws ModuleException {

		log.info("HandleInterlinePayFortAction payAtStoreNotificationCall: Start");

		if (log.isDebugEnabled()) {
			log.debug("### Start.. HandleInterlinePayFortAction " + request.getRequestedSessionId());
		}

		String responseStr = null;
		Map<String, String> NotificationMap = new HashMap<String, String>();
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();

		try {

			Map<String, String> receiptyMap = getReceiptMap(request);
			log.info(receiptyMap);

			// get notification info to map
			String XMLNotification = receiptyMap.get(PayFortPaymentUtils.NOTIFICATION);
			NotificationMap = getNotificationMap(XMLNotification);

			// load payment related properties and information
			List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(
					PayFortPaymentUtils.PAY_AT_STORE);
			IPGPaymentOptionDTO payFortPgwConfigs = pgwList.get(0);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					payFortPgwConfigs.getPaymentGateway(), payFortPgwConfigs.getBaseCurrency());

			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			TempPaymentTnx tempPaymentTnx = null;

			if (ipgProps != null && NotificationMap.size() > 0 && checkAuthentication(ipgProps, NotificationMap)) {
				String orderId = NotificationMap.get(PayFortPaymentUtils.ORDER_ID);
				IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
				Integer tempPayId = Integer.parseInt(getTempID(orderId));
				Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);
				ipgResponseDTO.setStatus(ipgResponseDTO.STATUS_ACCEPTED);
				NotificationMap.put("responseType", "PAYFORTGENARATEPAYMENTREFARANCE");

				tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(tempPayId);

				LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
				modes.setPnr(tempPaymentTnx.getPnr());
				modes.setRecordAudit(false);

				requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
				ipgResponseDTO.setTemporyPaymentId(tempPayId);

				Calendar cal = Calendar.getInstance();
				cal.setTime(ipgResponseDTO.getResponseTimeStamp());

				NotificationMap.remove("responseType");
				NotificationMap.put("responseType", "PAYMENTSUCCESS");
				NotificationMap.put("pnr", tempPaymentTnx.getPnr());
				NotificationMap.put(PayFortPaymentUtils.TICKET_NUMBER, tempPaymentTnx.getPnr());

				ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(NotificationMap, ipgResponseDTO,
						ipgIdentificationParamsDTO);

			} else {
				log.info("HandleInterlinePayFortAction payAtStoreNotificationCall: Basic Authorization Failure");

			}
			responseStr = dataToString(NotificationMap);
			log.info("HandleInterlinePayFortAction payAtStoreNotificationCall: Failed Confirm the Booking " + responseStr);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("HandleInterlinePayFortAction payAtStoreNotificationCall: Failed Confirm the Booking");
		}

		log.info("HandleInterlinePayFortAction payAtStoreNotificationCall: End");

	}

	/**
	 * Returns ReceiptMap. Map will holds all parameters send by the payment gateway. It is up the payment broker
	 * implementation extract the required parameter and values
	 * 
	 * @param request
	 * @return
	 */
	private static Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

	/**
	 * Returns Notification Map from PayFort. Map will holds all parameters send by the payment gateway. It is up the
	 * payment broker implementation extract the required parameter and values
	 * 
	 * @param request
	 * @return
	 */
	private static Map<String, String> getNotificationMap(String XMLNotification) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		Document document;

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
			document = builder.parse(new InputSource(new StringReader(XMLNotification)));
			Element rootElement = document.getDocumentElement();

			String merchantID = getString(PayFortPaymentUtils.MERCHANT_ID, rootElement);
			fields.put(PayFortPaymentUtils.MERCHANT_ID, merchantID);
			String orderID = getString(PayFortPaymentUtils.ORDER_ID, rootElement);
			fields.put(PayFortPaymentUtils.ORDER_ID, orderID);
			String amount = getString("amount", rootElement);
			fields.put("amount", amount);
			String currency = getString(PayFortPaymentUtils.CURRENCY, rootElement);
			fields.put(PayFortPaymentUtils.CURRENCY, currency);
			String payment = getString(PayFortPaymentUtils.PAYMENT, rootElement);
			fields.put(PayFortPaymentUtils.PAYMENT, payment);
			String serviceName = getString(PayFortPaymentUtils.SERVICE_NAME, rootElement);
			fields.put(PayFortPaymentUtils.SERVICE_NAME, serviceName);
			String signature = getString(PayFortPaymentUtils.SIGNATURE, rootElement);
			fields.put(PayFortPaymentUtils.SIGNATURE, signature);

		} catch (ParserConfigurationException e) {
			log.error(e);
		} catch (SAXException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}

		return fields;
	}

	private static String getString(String tagName, Element element) {
		NodeList list = element.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			NodeList subList = list.item(0).getChildNodes();

			if (subList != null && subList.getLength() > 0) {
				return subList.item(0).getNodeValue();
			}
		}

		return null;
	}

	private static String dataToString(Map<String, String> dataMap) {
		String resString = "";
		for (Map.Entry<String, String> entry : dataMap.entrySet()) {
			resString += entry.getKey() + ":" + entry.getValue() + ", ";
		}
		resString = resString.substring(0, resString.length() - 1);
		return resString;
	}

	private boolean checkAuthentication(Properties ipgProps, Map<String, String> NotificationMap) {

		String storeID = ipgProps.getProperty("merchantId");
		String merchantSymenticKey = ipgProps.getProperty("encryptionKey");
		String serviceName = ipgProps.getProperty("serviceName");

		// TO-DO need to replace "merchantID" and "serviceName" with proper way
		if (NotificationMap.get(PayFortPaymentUtils.MERCHANT_ID).equals(storeID)
				&& NotificationMap.get(PayFortPaymentUtils.SERVICE_NAME).equals(serviceName)) {
			String merchantID = NotificationMap.get(PayFortPaymentUtils.MERCHANT_ID);
			String orderID = NotificationMap.get(PayFortPaymentUtils.ORDER_ID);
			String amount = NotificationMap.get("amount");
			String currency = NotificationMap.get(PayFortPaymentUtils.CURRENCY);
			String payment = NotificationMap.get(PayFortPaymentUtils.PAYMENT);
			String serviceNameNotified = NotificationMap.get(PayFortPaymentUtils.SERVICE_NAME);
			String signatureNotified = NotificationMap.get(PayFortPaymentUtils.SIGNATURE);
			String signature = "";
			if (NotificationMap.containsKey("orderReference")) {

				String status = NotificationMap.get("status");
				String orderReference = NotificationMap.get("orderReference");
				String trackingNumber = NotificationMap.get("trackingNumber");

				signature = amount + "" + currency + "" + merchantID + "" + orderID + "" + orderReference + ""
						+ serviceNameNotified + "" + status + "" + trackingNumber + "" + merchantSymenticKey;
			} else {

				signature = amount + "" + currency + "" + merchantID + "" + orderID + "" + payment + "" + serviceNameNotified
						+ "" + merchantSymenticKey;

			}
			String signatureSHA1 = "";
			try {
				signatureSHA1 = SHA1(signature);
			} catch (NoSuchAlgorithmException e) {
				log.error(e);
			} catch (UnsupportedEncodingException e) {
				log.error(e);
			}

			if (signatureSHA1.equals(signatureNotified)) {
				return true;
			} else {
				return false;
			}

		}

		return false;
	}

	private static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md;
		md = MessageDigest.getInstance("SHA-1");
		byte[] sha1hash = new byte[40];
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		sha1hash = md.digest();
		return convertToHex(sha1hash);
	}

	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	private String getTempID(String orderID) {
		String tempPayID = "";
		if (orderID != null) {
			tempPayID = orderID.substring(1);
		}
		return tempPayID;
	}

}