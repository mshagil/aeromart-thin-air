package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class PaxContactDetailsResponse extends BaseRS{

	private Map<String , PaxContactDetailsField> contactDetails;

	public Map<String, PaxContactDetailsField> getContactDetails() {
		return contactDetails;
	}

	public void setContactDetails(Map<String, PaxContactDetailsField> contactDetails) {
		this.contactDetails = contactDetails;
	}

}
