package com.isa.aeromart.services.endpoint.dto.payment;

public class AdministrationFeeRQ extends ServiceTaxRQ {
	private String pnr;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
