package com.isa.aeromart.services.endpoint.dto.ancillary;

public class Passenger extends Person {

	private String passengerRph;

	private String type;
	
	private int travelWith;
	
	private String travelerRefNumber;

	public String getPassengerRph() {
		return passengerRph;
	}

	public void setPassengerRph(String passengerRph) {
		this.passengerRph = passengerRph;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTravelWith() {
		return travelWith;
	}

	public void setTravelWith(int travelWith) {
		this.travelWith = travelWith;
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}
	
}
