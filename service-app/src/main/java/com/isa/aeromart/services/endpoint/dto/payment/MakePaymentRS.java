package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.ActionStatus;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.booking.IPGTransactionResult;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class MakePaymentRS extends TransactionalBaseRS {

	public enum VoucherStatus {
		VOUCHER_SUCCESS, VOUCHER_FAILED
	}

	private String pnr;
	private Date firstDepartureDate;
	private String contactLastName;
	private PaymentStatus paymentStatus;
	private ActionStatus actionStatus;
	private boolean switchToExternal;
	private Date onHoldReleaseTime;
	private VoucherStatus voucherGeneration;
	private ExternalPGDetails externalPGDetails;
	private IPGTransactionResult ipgTransactionResult;
	private boolean cashPayment;
	private boolean offlinePayment;
	private String voucherGroupId;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public boolean isSwitchToExternal() {
		return switchToExternal;
	}

	public void setSwitchToExternal(boolean switchToExternal) {
		this.switchToExternal = switchToExternal;
	}

	public Date getOnHoldReleaseTime() {
		return onHoldReleaseTime;
	}

	public void setOnHoldReleaseTime(Date onHoldReleaseTime) {
		this.onHoldReleaseTime = onHoldReleaseTime;
	}

	public VoucherStatus getVoucherGeneration() {
		return voucherGeneration;
	}

	public void setVoucherGeneration(VoucherStatus voucherGeneration) {
		this.voucherGeneration = voucherGeneration;
	}

	public ExternalPGDetails getExternalPGDetails() {
		return externalPGDetails;
	}

	public void setExternalPGDetails(ExternalPGDetails externalPGDetails) {
		this.externalPGDetails = externalPGDetails;
	}

	public boolean isCashPayment() {
		return cashPayment;
	}

	public boolean isOfflinePayment() {
		return offlinePayment;
	}

	public void setCashPayment(boolean isCashPayment) {
		this.cashPayment = isCashPayment;
	}

	public void setOfflinePayment(boolean isOfflinePayment) {
		this.offlinePayment = isOfflinePayment;
	}

	public IPGTransactionResult getIpgTransactionResult() {
		return ipgTransactionResult;
	}

	public void setIpgTransactionResult(IPGTransactionResult ipgTransactionResult) {
		this.ipgTransactionResult = ipgTransactionResult;
	}

	public Date getFirstDepartureDate() {
		return firstDepartureDate;
	}

	public void setFirstDepartureDate(Date firstDepartureDate) {
		this.firstDepartureDate = firstDepartureDate;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getVoucherGroupId() {
		return voucherGroupId;
	}

	public void setVoucherGroupId(String voucherGroupId) {
		this.voucherGroupId = voucherGroupId;
	}
}
