package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.aeromart.services.endpoint.dto.availability.BasePreferences;
import com.isa.aeromart.services.endpoint.dto.availability.OriginDestinationInfo;
import com.isa.aeromart.services.endpoint.dto.availability.Preferences;
import com.isa.aeromart.services.endpoint.dto.common.SpecificFlight;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class OriginDestinationInfoAdaptor implements Adaptor<OriginDestinationInfo, OriginDestinationInformationTO> {
	private Preferences globalPref;
	private RPHGenerator rphGenerator;

	public OriginDestinationInfoAdaptor(Preferences globalPref, RPHGenerator rphGenerator) {
		this.globalPref = globalPref;
		this.rphGenerator = rphGenerator;
	}

	@Override
	public OriginDestinationInformationTO adapt(OriginDestinationInfo source) {

		OriginDestinationInformationTO target = new OriginDestinationInformationTO();

		target.setOrigin(source.getOrigin());
		target.setDestination(source.getDestination());
		
		if (AppSysParamsUtil.enableCityBasesFunctionality()) {
			target.setArrivalCitySearch(source.isDestinationCity());
			target.setDepartureCitySearch(source.isOriginCity());
		}

		// override specific preferences
		overrideGlobalPreferences(target, source.getPreferences());

		Date depatureDate = source.getDepartureDateTime();
		Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);

		depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, source.getDepartureVariance() * -1);
		Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
		depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, source.getDepartureVariance());

		if (source.getSpecificFlights() != null && source.getSpecificFlights().size() > 0) {
			OriginDestinationOptionTO ondOption = new OriginDestinationOptionTO();
			target.getOrignDestinationOptions().add(ondOption);
			AdaptorUtils.adaptCollection(source.getSpecificFlights(), ondOption.getFlightSegmentList(),
					new SelectedFlightSegmentAdaptor(rphGenerator));

			sortSpecificFlights(source.getSpecificFlights());
			depatureDate = getFirstDepartureDateTime(source.getSpecificFlights());
			depatureDateTimeStart = getFirstDepartureDateTime(source.getSpecificFlights());
			depatureDateTimeEnd = getLastDepartureDateTime(source.getSpecificFlights());
			target.setSpecificFlightsAvailability(true);
		}

		target.setPreferredDate(depatureDate);
		target.setDepartureDateTimeStart(depatureDateTimeStart);
		target.setDepartureDateTimeEnd(depatureDateTimeEnd);

		return target;
	}

	private void sortSpecificFlights(List<SpecificFlight> flights) {
		Collections.sort(flights, new Comparator<SpecificFlight>() {
			@Override
			public int compare(SpecificFlight first, SpecificFlight second) {
				return first.getDepartureDateTime().compareTo(second.getDepartureDateTime());
			}
		});
	}

	private Date getFirstDepartureDateTime(List<SpecificFlight> flights) {
		if (flights.size() > 0) {
			return flights.get(0).getDepartureDateTime();
		}
		return null;
	}

	private Date getLastDepartureDateTime(List<SpecificFlight> flights) {
		if (flights.size() > 0) {
			return flights.get(flights.size() - 1).getDepartureDateTime();
		}
		return null;
	}

	private void overrideGlobalPreferences(OriginDestinationInformationTO target, BasePreferences specificPreferences) {
		if (!StringUtil.isNullOrEmpty(specificPreferences.getBookingCode())
				&& !specificPreferences.getBookingCode().equalsIgnoreCase(globalPref.getBookingCode())) {
			target.setPreferredBookingClass(specificPreferences.getBookingCode());
		} else {
			target.setPreferredBookingClass(globalPref.getBookingCode());
		}
		if (!StringUtil.isNullOrEmpty(specificPreferences.getSeatType())
				&& !specificPreferences.getSeatType().equalsIgnoreCase(globalPref.getSeatType())) {
			target.setPreferredBookingType(specificPreferences.getSeatType());
		} else if (!StringUtil.isNullOrEmpty(globalPref.getSeatType())) {
			target.setPreferredBookingType(globalPref.getSeatType());
		} else {
			target.setPreferredBookingType(BookingClass.BookingClassType.NORMAL);
		}
		if (!StringUtil.isNullOrEmpty(specificPreferences.getCabinClass())
				&& !specificPreferences.getCabinClass().equalsIgnoreCase(globalPref.getCabinClass())) {
			target.setPreferredClassOfService(specificPreferences.getCabinClass());
		} else {
			target.setPreferredClassOfService(globalPref.getCabinClass());
		}
		if (!StringUtil.isNullOrEmpty(specificPreferences.getLogicalCabinClass())
				&& !specificPreferences.getLogicalCabinClass().equalsIgnoreCase(globalPref.getLogicalCabinClass())) {
			target.setPreferredLogicalCabin(specificPreferences.getLogicalCabinClass());
		}
		if (!StringUtil.isNullOrEmpty(specificPreferences.getAdditionalPreferences().getFareClassType())) {
			if (specificPreferences.getAdditionalPreferences().getFareClassType().equals(FareClassType.BUNDLE.toString())) {
				target.setPreferredBundleFarePeriodId(specificPreferences.getAdditionalPreferences().getFareClassId());

			}
		}
	}
}
