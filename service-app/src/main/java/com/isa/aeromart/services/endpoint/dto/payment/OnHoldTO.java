package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.Date;

public class OnHoldTO {
	private boolean allowOnHold;
	private Date onHoldReleaseTime;

	public boolean isAllowOnHold() {
		return allowOnHold;
	}

	public Date getOnHoldReleaseTime() {
		return onHoldReleaseTime;
	}

	public void setAllowOnHold(boolean allowOnHold) {
		this.allowOnHold = allowOnHold;
	}

	public void setOnHoldReleaseTime(Date onHoldReleaseTime) {
		this.onHoldReleaseTime = onHoldReleaseTime;
	}

}
