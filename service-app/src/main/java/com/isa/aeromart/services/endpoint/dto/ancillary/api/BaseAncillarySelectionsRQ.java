package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationDescriptor;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public abstract class BaseAncillarySelectionsRQ extends TransactionalBaseRQ {

    private ReservationDescriptor reservationData;

    private MetaData metaData;

    private List<SelectedAncillary> ancillaries;

    public ReservationDescriptor getReservationData() {
        return reservationData;
    }

    public void setReservationData(ReservationDescriptor reservationData) {
        this.reservationData = reservationData;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

	public List<SelectedAncillary> getAncillaries() {
		return ancillaries;
	}

	public void setAncillaries(List<SelectedAncillary> ancillaries) {
		this.ancillaries = ancillaries;
	}
}
