package com.isa.aeromart.services.endpoint.dto.ancillary;

public class ModifyReservation extends ReservationData {

	private String pnr;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
}
