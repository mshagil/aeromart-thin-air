package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.math.BigDecimal;
import java.util.List;

public class AncillaryPassenger {

    private String passengerRph;

    private List<PricedAncillaryType> ancillaryTypes;

    private BigDecimal amount;

	private List<MonetaryAmendment> amendments;

	public String getPassengerRph() {
		return passengerRph;
	}

	public void setPassengerRph(String passengerRph) {
		this.passengerRph = passengerRph;
	}

	public List<PricedAncillaryType> getAncillaryTypes() {
		return ancillaryTypes;
	}

	public void setAncillaryTypes(List<PricedAncillaryType> ancillaryTypes) {
		this.ancillaryTypes = ancillaryTypes;
	}

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}
}
