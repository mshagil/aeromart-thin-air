package com.isa.aeromart.services.endpoint.delegate.booking;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.booking.ItineraryRQ;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRQ;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationValidateRes;
import com.isa.aeromart.services.endpoint.dto.booking.TaxInvoiceRQ;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface BookingService {

	public BookingRS createBooking(CommonReservationAssembler reservationAssembler, AncillaryIntegrateTO ancillaryIntegrateTo, TrackInfoDTO trackInfoDTO);

	public LCCClientReservation balancePayment(BookingSessionStore bookingSessionStore, TrackInfoDTO trackInfoDTO,
			String customerID) throws Exception;

	public LCCClientReservation loadProxyReservation(String pnr, boolean isGroupPNR, TrackInfoDTO trackInfo, boolean recordAudit,
			boolean isRegisteredUser, String airlineCode, String marketingAirlineCode, boolean skipPromoAdjustment)
			throws ModuleException;

	public LoadReservationValidateRes validateLoadReservationRQ(LoadReservationRQ loadReservationRQ, TrackInfoDTO trackInfoDTO,
			String customerID);
	
	public boolean isCustomerIDUsed(String pnr, String customerID, TrackInfoDTO trackInfoDTO) throws ModuleException, ValidationException;

	public String getItineraryInfo(ItineraryRQ itineraryRQ, TrackInfoDTO trackInfo) throws ModuleException;
	
	public String getTaxInvoiceInfo(TaxInvoiceRQ taxInvoiceRQ, TrackInfoDTO trackInfo) throws ModuleException;
	
	public List<TaxInvoice> getTaxInvoiceList(TaxInvoiceRQ printTaxInvoiceRQ, TrackInfoDTO trackInfo) throws ModuleException;
	
	public boolean isTaxInvoiceAvailable(LoadReservationRQ loadReservationRQ) throws ModuleException;

	public void sendItineraryEmail(ItineraryRQ printItineraryRQ, TrackInfoDTO trackInfo) throws ModuleException;

	public void checkLoadingConstraint(LCCClientReservation reservation) throws ValidationException;

	public Map<String, Boolean> getGroundSegments(Set<LCCClientReservationSegment> segments) throws ModuleException;
}
