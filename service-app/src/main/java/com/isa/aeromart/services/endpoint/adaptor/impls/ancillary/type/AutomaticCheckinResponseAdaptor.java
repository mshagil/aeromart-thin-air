package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSource;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope.ScopeType;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.AutoCheckinInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.SystemProvided;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.autocheckin.AutomaticCheckinItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.autocheckin.AutomaticCheckinItems;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAutomaticCheckinDTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

/**
 * @author aravinth.r
 *
 */
public class AutomaticCheckinResponseAdaptor extends AncillariesResponseAdaptor {

	@Override
	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability.isAnciActive(availability,
				LCCAncillaryAvailabilityInDTO.LCCAncillaryType.AUTOMATIC_CHECKIN);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	@Override
	public AncillaryType toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify,
			MessageSource errorMessageSource) {

		AncillaryType ancillaryType = new AncillaryType();
		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.AUTOMATIC_CHECKIN.name());
		Applicability applicability = new Applicability();
		applicability.setApplicableType(Applicability.ApplicableType.PASSENGER.name());
		List<Provider> providers = new ArrayList<>();
		SystemProvided provider = new SystemProvided();
		provider.setProviderType(Provider.ProviderType.SYSTEM);
		Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO> source = (Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO>) ancillaries;
		AvailableAncillary availableAncillary = new AvailableAncillary();
		List<AvailableAncillaryUnit> availableAncillaryUnits = new ArrayList<AvailableAncillaryUnit>();
		Charge charge;
		String flightSegmentRPH;

		for (AirportServiceKeyTO airportServiceKeyTO : source.keySet()) {
			AutomaticCheckinItems autoCheckinItemsGroup = new AutomaticCheckinItems();
			List<AutomaticCheckinItem> autoChekinItems = new ArrayList<AutomaticCheckinItem>();
			SegmentScope segmentScope = new SegmentScope();

			LCCFlightSegmentAutomaticCheckinDTO lccFlightSegmentAutomaticCheckinDTO = source.get(airportServiceKeyTO);
			autoChekinItems = new ArrayList<>();
			autoCheckinItemsGroup = new AutomaticCheckinItems();
			AvailableAncillaryUnit availableAncillaryUnit = new AvailableAncillaryUnit();
			availableAncillaryUnit.setApplicability(applicability);
			flightSegmentRPH = rphGenerator.getRPH(lccFlightSegmentAutomaticCheckinDTO.getFlightSegmentTO().getFlightRefNumber());
			for (LCCAutomaticCheckinDTO automaticCheckin : lccFlightSegmentAutomaticCheckinDTO.getAutomcaticCheckin()) {
				AutomaticCheckinItem autoCheckinItem = new AutomaticCheckinItem();
				autoCheckinItem.setItemId(automaticCheckin.getAutoCheckinId().toString());
				segmentScope.setScopeType(ScopeType.SEGMENT);
				segmentScope.setFlightSegmentRPH(flightSegmentRPH);

				availableAncillaryUnit.setScope(segmentScope);

				charge = new Charge();
				charge.setChargeBasis(Charge.ChargeBasis.PER_PASSENGER.name());
				charge.setAmount(automaticCheckin.getAutomaticCheckinCharge());
				autoCheckinItem.setChargePerPax(charge);

				autoChekinItems.add(autoCheckinItem);
			}
			autoCheckinItemsGroup.setItems(autoChekinItems);
			availableAncillaryUnit.setItemsGroup(autoCheckinItemsGroup);
			availableAncillaryUnits.add(availableAncillaryUnit);
		}
		availableAncillary.setAvailableUnits(availableAncillaryUnits);
		provider.setAvailableAncillaries(availableAncillary);
		providers.add(provider);
		ancillaryType.setProviders(providers);

		return ancillaryType;
	}

	@Override
	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {
		Integer autocheckinId = null;
		if (item.getId() != null) {
			autocheckinId = Integer.valueOf(item.getId());
		}
		AutoCheckinInput autoCheckinInput = (AutoCheckinInput) item.getInput();
		LCCAutomaticCheckinDTO selectedAutomaticCheckin = new LCCAutomaticCheckinDTO();
		selectedAutomaticCheckin.setAutomaticCheckinCharge(item.getAmount());
		selectedAutomaticCheckin.setAutoCheckinId(autocheckinId);
		selectedAutomaticCheckin.setSeatPref(item.getName());

		if (autoCheckinInput != null) {
			selectedAutomaticCheckin.setEmail(autoCheckinInput.getEmail());
			selectedAutomaticCheckin.setSeatCode(autoCheckinInput.getSeatCode());
		}
		return selectedAutomaticCheckin;
	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

		List<SessionFlightSegment> segments = transactionalAncillaryService.getSelectedSegments(transactionId);
		transactionalAncillaryService.storeUpdatedSessionFlightSegment(transactionId, segments);

	}

}
