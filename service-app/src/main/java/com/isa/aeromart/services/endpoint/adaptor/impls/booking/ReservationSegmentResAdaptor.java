package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.Date;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationSegment;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class ReservationSegmentResAdaptor implements Adaptor<LCCClientReservationSegment, ReservationSegment> {

	private final static String YES = "Y";
	
	private RPHGenerator rphGenerator;

	public ReservationSegmentResAdaptor(RPHGenerator rphGenerator) {
		this.rphGenerator = rphGenerator;
	}

	@Override
	public ReservationSegment adapt(LCCClientReservationSegment lccClientReservationSegment) {
		ReservationSegment reservationSegment = new ReservationSegment();
		FlightSegmentResAdaptor flightSegmentResTransformer = new FlightSegmentResAdaptor(rphGenerator);

		String newResSegRPH = ModificationUtils.createResSegRph(lccClientReservationSegment.getCarrierCode(),
				lccClientReservationSegment.getSegmentSeq());

		reservationSegment.setCabinClass(lccClientReservationSegment.getCabinClassCode());
		reservationSegment.setCheckInClosingTime(lccClientReservationSegment.getCheckInClosingTime());
		reservationSegment.setCheckInTimeGap(lccClientReservationSegment.getCheckInTimeGap());
		reservationSegment.setFareGroupID(lccClientReservationSegment.getInterlineGroupKey());
		if (lccClientReservationSegment.getJourneySequence()!= null) {
			reservationSegment.setJourneySequence(lccClientReservationSegment.getJourneySequence().toString());
		}
		reservationSegment.setLogicalCabinClass(lccClientReservationSegment.getLogicalCabinClass());
		reservationSegment.setOpenReturnConfirmBefore(lccClientReservationSegment.getOpenRetConfirmBefore());
		reservationSegment.setOpenReturnTravelExpiry(lccClientReservationSegment.getOpenReturnTravelExpiry());
		reservationSegment.setReservationSegmentRPH(newResSegRPH);
		reservationSegment.setSeatType(lccClientReservationSegment.getBookingType());
		reservationSegment.setSegment(flightSegmentResTransformer.adapt(lccClientReservationSegment));
		reservationSegment.setSegmentSequence(lccClientReservationSegment.getSegmentSeq());
		reservationSegment.setStatus(lccClientReservationSegment.getStatus());
		String standardStatus = null;
		if (lccClientReservationSegment.isUnSegment()) {
			standardStatus = ReservationSegment.UNSEGMENT;
		}
		reservationSegment.setStandardStatus(standardStatus);
		reservationSegment.setSubStatus(lccClientReservationSegment.getSubStatus());
		reservationSegment.setFlownSegment(isFlownSegment(lccClientReservationSegment));
		if (lccClientReservationSegment.getReturnFlag() != null) {
			reservationSegment.setReturnSegment(YES.equals(lccClientReservationSegment.getReturnFlag()) ? true : false);
		}
		return reservationSegment;
	}
	
	/*
	 * There are several ways to get flown segments (by departureDate, E ticket status etc) For service-app it is
	 * overridden to take by departureDate
	 */
	private boolean isFlownSegment(LCCClientReservationSegment reservationSegmentDTO) {
		boolean isFlownSegemnt = false;
		Date currentDate = new Date();
		if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
			if (reservationSegmentDTO.getDepartureDateZulu() != null
					&& currentDate.compareTo(reservationSegmentDTO.getDepartureDateZulu()) > 0) {
				isFlownSegemnt = true;
			}
		}
		return isFlownSegemnt;
	}

}
