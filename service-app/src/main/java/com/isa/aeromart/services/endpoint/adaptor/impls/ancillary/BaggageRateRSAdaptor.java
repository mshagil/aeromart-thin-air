package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.Collection;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BaggageRateRS;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.FlightSegmentBaggageRate;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.FlightSegmentLite;

public class BaggageRateRSAdaptor implements Adaptor<BaggageRatesDTO, BaggageRateRS> {

	private RPHGenerator rphGenerator;

	public BaggageRateRSAdaptor(RPHGenerator rphGenerator) {
		this.rphGenerator = rphGenerator;
	}

	@Override
	public BaggageRateRS adapt(BaggageRatesDTO baggageRatesDTO) {
		BaggageRateRS baggageRateRS = new BaggageRateRS();
		replaceFlightSegmentRPHs(baggageRatesDTO.getBaggageRates());
		baggageRateRS.setBaggageRates(baggageRatesDTO.getBaggageRates());

		return baggageRateRS;
	}

	private void replaceFlightSegmentRPHs(Collection<FlightSegmentBaggageRate> flightBaggageRates) {
		for (FlightSegmentBaggageRate flightSegmentBaggageRate : flightBaggageRates) {
			for (FlightSegmentLite flightSegment : flightSegmentBaggageRate.getOndFlights()) {
				flightSegment.setFlightSegmentRPH(rphGenerator.getRPH(flightSegment.getFlightSegmentRPH()));
			}
		}
	}

}
