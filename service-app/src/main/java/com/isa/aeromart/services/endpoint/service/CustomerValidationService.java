package com.isa.aeromart.services.endpoint.service;

import com.isa.aeromart.services.endpoint.dto.customer.CustomerDetails;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerLoginRQ;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerSaveOrUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.customer.ForgotPasswordRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSCrossPortalLoginRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSDetails;
import com.isa.aeromart.services.endpoint.dto.customer.LMSRegisterRQ;
import com.isa.aeromart.services.endpoint.dto.customer.LMSValidationRQ;
import com.isa.aeromart.services.endpoint.dto.customer.PasswordChangeRQ;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface CustomerValidationService {

	public void validateLoginRQ(CustomerLoginRQ customerLoginRQ) throws ValidationException;
	
	public void validateRegisterUpdateRQ(CustomerSaveOrUpdateRQ customerSaveOrUpdateRQ) throws ValidationException;
	
	public void validateForgotPasswordRQ(ForgotPasswordRQ forgotPasswordRQ) throws ValidationException;
	
	public void validateLMSValidationRQ(LMSValidationRQ lmsValidationReq, boolean isLoggedCustomer) throws ValidationException;
	
	public void validateLMSRegisterRQ(LMSRegisterRQ lmsRegisterReq) throws ValidationException;
	
	public void validateProfileUpdate(CustomerDetails customer , int customerId) throws ValidationException;
	
	public void validateLMSDetails(LMSDetails lmsDetails) throws ValidationException;
	
	public LoyaltyCustomerProfile validateLoyaltyCustomerProfile(LoyaltyCustomerProfile loyaltyCustomerProfile,
			boolean validateCustomerId) throws ValidationException;

	public void validateExistingLMSAccount(String refferedEmailId, boolean isReffered) throws ValidationException;

	public void validateNewRequestedLMSAccount(String email) throws ValidationException, ModuleException;

	public void validateExistingName(LMSValidationRQ lmsValidationReq) throws ModuleException, ValidationException;

	public void validateLMSCrossPortalLoginReq(LMSCrossPortalLoginRQ lmsCrossPortalLoginReq, String email) throws ModuleException;
	
	public void validatePasswordChangeRQ(PasswordChangeRQ passwordChangeRQ, Integer customerId) throws ValidationException;

}
