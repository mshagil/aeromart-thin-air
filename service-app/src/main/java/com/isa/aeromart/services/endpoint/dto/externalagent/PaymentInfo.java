package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotEmpty;

public class PaymentInfo {
	@NotEmpty
	public String amount;
	@NotEmpty
	public String currencyCode;

	public String paymentMethod;

	public PaymentInfo() {
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

}