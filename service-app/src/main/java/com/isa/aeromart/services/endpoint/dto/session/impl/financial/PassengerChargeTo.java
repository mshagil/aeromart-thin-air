package com.isa.aeromart.services.endpoint.dto.session.impl.financial;

import java.util.List;

public class PassengerChargeTo<T> {

    private String passengerRph;

    private List<T> getPassengerCharges;

    public String getPassengerRph() {
        return passengerRph;
    }

    public void setPassengerRph(String passengerRph) {
        this.passengerRph = passengerRph;
    }

    public List<T> getGetPassengerCharges() {
        return getPassengerCharges;
    }

    public void setGetPassengerCharges(List<T> getPassengerCharges) {
        this.getPassengerCharges = getPassengerCharges;
    }
}
