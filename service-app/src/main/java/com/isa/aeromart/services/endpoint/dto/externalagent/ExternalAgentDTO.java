package com.isa.aeromart.services.endpoint.dto.externalagent;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airtravelagents.api.model.Agent;

public class ExternalAgentDTO {

	private String agentCode;

	private String agentName;

	private List<CreditLimitRS> creditLimits = new ArrayList<>();

	public ExternalAgentDTO(String agentCode, String availableCredit) {
		this.agentCode = agentCode;
	}

	public ExternalAgentDTO(Agent agent) {
		this.agentCode = agent.getAgentCode();
		this.agentName = agent.getAgentName();
		
		if (agent.getPaymentMethod().contains(Agent.PAYMENT_MODE_CREDITCARD)) {
			CreditLimitRS bspCredit = new CreditLimitRS();
			bspCredit.setPaymentType(Agent.PAYMENT_MODE_CREDITCARD);
			bspCredit.setCurrenyCode("-");
			bspCredit.setAvailabilityLimit("-");
			creditLimits.add(bspCredit);
		}
		if (agent.getPaymentMethod().contains(Agent.PAYMENT_MODE_ONACCOUNT)) {
			CreditLimitRS onAccCredit = new CreditLimitRS();
			onAccCredit.setPaymentType(Agent.PAYMENT_MODE_ONACCOUNT);
			onAccCredit.setCurrenyCode(agent.getCurrencyCode());
			onAccCredit.setAvailabilityLimit(agent.getAgentSummary().getAvailableCredit().toString());
			creditLimits.add(onAccCredit);
		}
		if (agent.getPaymentMethod().contains(Agent.PAYMENT_MODE_BSP)) {
			CreditLimitRS bspCredit = new CreditLimitRS();
			bspCredit.setPaymentType(Agent.PAYMENT_MODE_BSP);
			bspCredit.setCurrenyCode(agent.getCurrencyCode());
			bspCredit.setAvailabilityLimit(agent.getAgentSummary().getAvailableBSPCredit().toString());
			creditLimits.add(bspCredit);
		}
	}

	public String getAgentCode() {
		return agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public List<CreditLimitRS> getCreditLimits() {
		return creditLimits;
	}

	public void setCreditLimits(List<CreditLimitRS> creditLimits) {
		this.creditLimits = creditLimits;
	}

}
