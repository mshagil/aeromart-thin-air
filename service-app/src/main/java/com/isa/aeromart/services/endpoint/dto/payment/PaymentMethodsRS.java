package com.isa.aeromart.services.endpoint.dto.payment;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class PaymentMethodsRS extends TransactionalBaseRS {

	private List<String> paymentMethods;

	public List<String> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(List<String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

}
