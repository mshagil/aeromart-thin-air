package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.thinair.airreservation.api.model.ReservationPax;

public class ReservationPaxAdaptor implements Adaptor<Passenger, ReservationPax> {

	@Override
	public ReservationPax adapt(Passenger passenger) {

		ReservationPax reservationPax = new ReservationPax();
		reservationPax.setFirstName(passenger.getFirstName());
		reservationPax.setLastName(passenger.getLastName());
		reservationPax.setTitle(passenger.getTitle());
		reservationPax.setPaxSequence(passenger.getPaxSequence());

		return reservationPax;
	}

}
