package com.isa.aeromart.services.endpoint.dto.session.impl.modification;

import java.util.List;
import java.util.Map;

public class ReservationSegmentInfo {

	/**
	 * This holds the mapping of existing flight segment ids with prevailing reservation segment RPHs
	 * Should updated in fare requote call
	 */
	private Map<String,String> requoteSegmentMap;	
	private List<String> resSegRPHList;

	public Map<String, String> getRequoteSegmentMap() {
		return requoteSegmentMap;
	}

	public void setRequoteSegmentMap(Map<String, String> requoteSegmentMap) {
		this.requoteSegmentMap = requoteSegmentMap;
	}

	public List<String> getResSegRPHList() {
		return resSegRPHList;
	}

	public void setResSegRPHList(List<String> resSegRPHList) {
		this.resSegRPHList = resSegRPHList;
	}	

}
