package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ONDFlexiChargeAdaptor extends ONDBaseChargeAdaptor {
	private Map<Integer, Boolean> ondFlexiSelection;

	public ONDFlexiChargeAdaptor(ONDPriceAdaptor ondAdaptor, Map<Integer, Boolean> ondFlexiSelection) {
		super(ondAdaptor);
		this.ondFlexiSelection = ondFlexiSelection;
	}

	public void addOWFlexiFare(String paxType, List<ONDExternalChargeTO> flexies) {
		Iterator<ONDExternalChargeTO> iterator = flexies.iterator();
		while (iterator.hasNext()) {
			ONDExternalChargeTO fare = iterator.next();
			Iterator<ExternalChargeTO> ondExIt = fare.getExternalCharges().iterator();
			while (ondExIt.hasNext()) {
				ExternalChargeTO externalChrd = ondExIt.next();
				addCharge(paxType, externalChrd);
			}
		}
	}

	public void addCharge(String paxType, ExternalChargeTO flexiCharge) {
		boolean isFlexiSelect = false;
		OndOWPricing ondPrice = getOndPrice(flexiCharge.getOndSequence());
		if (ondFlexiSelection != null && !ondFlexiSelection.isEmpty() && ondFlexiSelection.get(flexiCharge.getOndSequence())) {
			isFlexiSelect = true;
		}
		if (isFlexiSelect) {
			ondPrice.setTotalPrice(AccelAeroCalculator.add(ondPrice.getTotalPrice(), flexiCharge.getAmount()));
			ondPrice.setTotalSurcharges(AccelAeroCalculator.add(ondPrice.getTotalSurcharges(), flexiCharge.getAmount()));
			PaxPrice paxPrice = getPaxPrice(paxType, flexiCharge);
			paxPrice.setTotal(AccelAeroCalculator.add(paxPrice.getTotal(), flexiCharge.getAmount()));
			paxPrice.setSurcharge(AccelAeroCalculator.add(paxPrice.getSurcharge(), flexiCharge.getAmount()));
		}

	}

}
