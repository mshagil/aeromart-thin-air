package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxCutOffYears;
import com.isa.thinair.commons.api.dto.PaxValildationTO;

public class PaxCutOffYearsAdaptor implements Adaptor<PaxValildationTO, PaxCutOffYears>{

	@Override
	public PaxCutOffYears adapt(PaxValildationTO source) {
		
		PaxCutOffYears paxCutOffYears = new PaxCutOffYears();
		
		paxCutOffYears.setAdultAgeCutOverYears(source.getAdultAgeCutOverYears());
		paxCutOffYears.setChildAgeCutOverYears(source.getChildAgeCutOverYears());
		paxCutOffYears.setInfantAgeCutOverYears(source.getInfantAgeCutOverYears());

		return paxCutOffYears;
	}

}
