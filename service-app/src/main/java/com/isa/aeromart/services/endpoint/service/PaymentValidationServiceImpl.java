package com.isa.aeromart.services.endpoint.service;

import com.isa.aeromart.services.endpoint.dto.payment.BinPromotionRQ;
import com.isa.aeromart.services.endpoint.dto.payment.EffectivePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRQ;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;

public class PaymentValidationServiceImpl implements PaymentValidationService{

	@Override
	public void vlaidateCreatePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException {
		
		if(paymentOptionsRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_OPTIONS_REQUEST);
		}
		
		validatePaymentOptionsRQ(paymentOptionsRQ);
		
	}

	@Override
	public void validateCreateEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException {
		if(effectivePaymentRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_EFFECTIVE_PAYMENT_REQUEST);
		}
		
		validatePaymentOptionsRQ(effectivePaymentRQ);
		
		if(effectivePaymentRQ.getPaymentOptions() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAYMENT_OPTIONS);
		}
	}

	@Override
	public void validateCreateMakePaymentRQ(MakePaymentRQ makePaymentRQ)throws ValidationException {
		if(makePaymentRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_MAKE_PAYMENT_REQUEST);
		}
		
		if(makePaymentRQ.getPaymentOptions() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAYMENT_OPTIONS);
		}
		
		if(makePaymentRQ.getContactInfo() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_CONTACT_INFO);
		}
		
	}

	@Override
	public void vlaidateBalancePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException {
		if(paymentOptionsRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_OPTIONS_REQUEST);
		}
		
		if(paymentOptionsRQ.getPnr() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PNR);
		}
	}

	@Override
	public void validateBalanceEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException {
		if(effectivePaymentRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_EFFECTIVE_PAYMENT_REQUEST);
		}
		
		if(effectivePaymentRQ.getPnr() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PNR);
		}
		
		if(effectivePaymentRQ.getPaymentOptions() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAYMENT_OPTIONS);
		}
	}

	@Override
	public void validateBalanceMakePaymentRQ(MakePaymentRQ makePaymentRQ)throws ValidationException {
		if(makePaymentRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_MAKE_PAYMENT_REQUEST);
		}
		
		if(makePaymentRQ.getPaymentOptions() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAYMENT_OPTIONS);
		}
		
		if(makePaymentRQ.getPnr() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PNR);
		}
	}

	@Override
	public void vlaidateReqoutePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException {
		
		if(paymentOptionsRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_OPTIONS_REQUEST);
		}
		
		if(paymentOptionsRQ.getPnr() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PNR);
		}
		
	}

	@Override
	public void validateReqouteEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException {
		if(effectivePaymentRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_EFFECTIVE_PAYMENT_REQUEST);
		}
		
		if(effectivePaymentRQ.getPnr() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PNR);
		}
		
		if(effectivePaymentRQ.getPaymentOptions() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAYMENT_OPTIONS);
		}
	}
	
	@Override
	public void validateReqouteMakePaymentRQ(MakePaymentRQ makePaymentRQ)throws ValidationException {
		if(makePaymentRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_MAKE_PAYMENT_REQUEST);
		}
		
		if(makePaymentRQ.getPaymentOptions() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAYMENT_OPTIONS);
		}
		
		if(makePaymentRQ.getContactInfo() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_CONTACT_INFO);
		}
		
		if(makePaymentRQ.getPnr() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PNR);
		}
		
	}

	@Override
	public void vlaidateModifyPaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException {
		
		if(paymentOptionsRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_OPTIONS_REQUEST);
		}
		
		//validatePaymentOptionsRQ(paymentOptionsRQ);
		
		if(paymentOptionsRQ.getPnr() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PNR);
		}
	}

	@Override
	public void validateModifyEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateModifyMakePaymentRQ(MakePaymentRQ makePaymentRQ) throws ValidationException {
		if(makePaymentRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_MAKE_PAYMENT_REQUEST);
		}
		
		if(makePaymentRQ.getPaymentOptions() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAYMENT_OPTIONS);
		}
		
		if(makePaymentRQ.getContactInfo() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_CONTACT_INFO);
		}
		
		if(makePaymentRQ.getPnr() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PNR);
		}
	}

	@Override
	public void vlaidateConfirmPaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) throws ValidationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateConfirmEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ) throws ValidationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateConfirmMakePaymentRQ(MakePaymentRQ makePaymentRQ) throws ValidationException {
		// TODO Auto-generated method stub
		
	}

	private void validatePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ){
		
		if(paymentOptionsRQ.getPaxInfo() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAX_INFO);
		}
		
		if(paymentOptionsRQ.getReservationContact() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_CONTACT_INFO);
		}
	}

	@Override
	public void validateBinPromotionRQ(BinPromotionRQ binPromotionRQ) {
		
		if(binPromotionRQ == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PROMOTION_REQUEST);
		}
		
		if(binPromotionRQ.getPaxInfo() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PAX_INFO);
		}
		
		if(binPromotionRQ.getReservationContact() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_CONTACT_INFO);
		}
		
		if(binPromotionRQ.getPromoCode() == null){
			throw new ValidationException(ValidationException.Code.NULL_PAYMENT_PROMOCODE);
		}
		
		if(binPromotionRQ.getCardNo() == null){
			throw new ValidationException(ValidationException.Code.ERROR_PAYMENT_CARD_NO);
		}
		
		if(binPromotionRQ.getPaymentGatewayId() == 0){
			throw new ValidationException(ValidationException.Code.ERROR_PAYMENT_PAYMENT_GATEWAY);
		}
		
	}

}
