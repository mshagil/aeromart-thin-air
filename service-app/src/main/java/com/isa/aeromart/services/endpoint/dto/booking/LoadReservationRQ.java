package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class LoadReservationRQ extends TransactionalBaseRQ {

	private String pnr;

	private String lastName;

	private Date departureDateTime;

	private boolean validateRQ;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public boolean isValidateRQ() {
		return validateRQ;
	}

	public void setValidateRQ(boolean validateRQ) {
		this.validateRQ = validateRQ;
	}

}
