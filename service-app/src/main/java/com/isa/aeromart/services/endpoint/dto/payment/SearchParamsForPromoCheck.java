package com.isa.aeromart.services.endpoint.dto.payment;

public class SearchParamsForPromoCheck {

	private int adultCount;
	private int childCount;
	private int infantCount;
	private String searchSystem;
	private int bankId;
	private String returnDate;

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public String getSearchSystem() {
		return searchSystem;
	}

	public void setSearchSystem(String searchSystem) {
		this.searchSystem = searchSystem;
	}

	public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

}
