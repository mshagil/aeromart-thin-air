package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class LMSConfirmationRS extends TransactionalBaseRS {

	public enum Operetion {
		MERGE, CONFIRM;
	}

	private boolean operationSuccess = false;

	private boolean nameMatch = true;

	private boolean valid = false;

	private Operetion operation;

	public boolean isOperationSuccess() {
		return operationSuccess;
	}

	public void setOperationSuccess(boolean operationSuccess) {
		this.operationSuccess = operationSuccess;
	}

	public boolean isNameMatch() {
		return nameMatch;
	}

	public void setNameMatch(boolean nameMatch) {
		this.nameMatch = nameMatch;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public Operetion getOperation() {
		return operation;
	}

	public void setOperation(Operetion operation) {
		this.operation = operation;
	}

}
