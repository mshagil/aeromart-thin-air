package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class DuplicateCheckFlightSegAdaptor implements Adaptor<SessionFlightSegment, FlightSegmentTO> {

	@Override
	public FlightSegmentTO adapt(SessionFlightSegment sessionFlightSegment) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setFlightRefNumber(sessionFlightSegment.getLCCFlightReference());
		flightSegmentTO.setOperatingAirline(sessionFlightSegment.getOperatingAirline());
		return flightSegmentTO;
	}

}
