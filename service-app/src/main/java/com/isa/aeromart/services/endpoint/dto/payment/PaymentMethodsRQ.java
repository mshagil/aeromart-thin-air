package com.isa.aeromart.services.endpoint.dto.payment;

import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class PaymentMethodsRQ extends TransactionalBaseRQ {

    public PaymentMethodsRQ() {
    }

    private String firstSegmentONDCode;

    private OperationTypes operationTypes;

    private String originCountryCode;

    public OperationTypes getOperationTypes() {
        return operationTypes;
    }

    public void setOperationTypes(OperationTypes operationTypes) {
        this.operationTypes = operationTypes;
    }

    public String getOriginCountryCode() {
        return originCountryCode;
    }

    public void setOriginCountryCode(String originCountryCode) {
        this.originCountryCode = originCountryCode;
    }

    public String getFirstSegmentONDCode() {
        return firstSegmentONDCode;
    }

    public void setFirstSegmentONDCode(String firstSegmentONDCode) {
        this.firstSegmentONDCode = firstSegmentONDCode;
    }
}
