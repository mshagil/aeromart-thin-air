package com.isa.aeromart.services.endpoint.dto.session.store;

import java.math.BigDecimal;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.BinPromoDiscountInfo;
import com.isa.aeromart.services.endpoint.dto.session.BookingTransaction;
import com.isa.aeromart.services.endpoint.dto.session.TotalPaymentInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;

public interface PaymentSessionStore extends PaymentBaseSessionStore {
	public static String SESSION_KEY = BookingTransaction.SESSION_KEY;

	public void storeTotalAvailableBalance(BigDecimal totalAvailableBalance);

	public BigDecimal getTotalAvailableBalance();

	public BookingRQ getbookingRQForPayment();

	public boolean isOnHoldCreatedForPayment();

	public void storeBalancePaymentRequiredInfo(List<FlightSegmentTO> fltSegments, PreferenceInfo prefInfo,
			List<ServiceTaxContainer> applicableServiceTaxes, TravellerQuantity travellerQuantity);

	public TotalPaymentInfo getTotalPaymentInformation();

	public BinPromoDiscountInfo getBinPromoDiscountInfo();

	public void storeBinPromoDiscountInfo(BinPromoDiscountInfo binPromoDiscountInfo);

	public String getPnr();

	// TODO: use a common lightweight object to hold required fields; get rid of discountAmount
	// recalculated with DiscountedFareDeatails
	public ReservationDiscountDTO getReservationDiscountDTO();

	public void storeReservationDiscountDTO(ReservationDiscountDTO reservationDiscountDTO);

	public void setDiscountAmount(BigDecimal discountAmount);

	public BigDecimal getDiscountAmount();

	public void setDiscountedFareDetails(DiscountedFareDetails discountedFareDetails);

	public DiscountedFareDetails getDiscountedFareDetails();

	// TODO remove and add directly to financial store
	public void addAvailabilityFlexiChargesToFinancialStore(List<Passenger> passengers);
	
	void storeResInfo(ReservationInfo resInfo);
	
}
