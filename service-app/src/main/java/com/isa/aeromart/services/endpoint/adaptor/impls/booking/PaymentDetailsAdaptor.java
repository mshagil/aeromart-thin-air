package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.PaymentDetails;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;

public class PaymentDetailsAdaptor implements Adaptor<LCCClientReservation, PaymentDetails>{

	@Override
	public PaymentDetails adapt(LCCClientReservation source) {
		
		PaymentDetails	paymentDetails = new PaymentDetails();
		
		paymentDetails.setCurrency(source.getLastCurrencyCode());
		
		return paymentDetails;
	}

}
