package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotEmpty;

public class ConfirmRQ extends CreditPaymentRQ {

	@NotEmpty
	private String blockReferenceId;

	public String getBlockReferenceId() {
		return blockReferenceId;
	}

	public void setBlockReferenceId(String blockReferenceId) {
		this.blockReferenceId = blockReferenceId;
	}

}
