package com.isa.aeromart.services.endpoint.dto.ancillary.integrate;

public class AncillaryIntegrateTO {

	private AncillaryIntegrateReservationTO reservation;

	public AncillaryIntegrateReservationTO getReservation() {
		return reservation;
	}

	public void setReservation(AncillaryIntegrateReservationTO reservation) {
		this.reservation = reservation;
	}

}
