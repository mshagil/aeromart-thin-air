package com.isa.aeromart.services.endpoint.delegate.modification;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.isa.aeromart.services.endpoint.dto.modification.FFIDChangePax;
import com.isa.aeromart.services.endpoint.dto.modification.FFIDUpdateRQ;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airproxy.api.LccSubOperationConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

@Service("modificationService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ModificationServiceAdaptor implements ModificationService {

	public void modifyFFIDInfo(FFIDUpdateRQ ffidUpdateRQ, LCCClientReservation reservation, TrackInfoDTO trackinfo)
			throws ModuleException {

		LCCClientReservation oldReservation = populateOldLccReservation(reservation);

		if (!ffidUpdateRQ.getFfidChangePaxList().isEmpty()) {
			populateAddEditFFIDs(ffidUpdateRQ, reservation);

			Collection<String> grantedOperations = new HashSet<>();

			grantedOperations.add(LccSubOperationConstants.ModifyReservation.ALTER_PAX_INFO);

			ModuleServiceLocator.getAirproxyReservationBD().modifyPassengerInfo(reservation, oldReservation,
					ffidUpdateRQ.isGroupPnr(), null, AppIndicatorEnum.APP_IBE.toString(), true, false, grantedOperations,
					trackinfo);
		}
	}

	private void populateAddEditFFIDs(FFIDUpdateRQ ffidUpdateRQ, LCCClientReservation reservation) {
		Set<LCCClientReservationPax> lccClientReservationPaxs = reservation.getPassengers();
		for (LCCClientReservationPax lccClientReservationPax : lccClientReservationPaxs) {
			for (FFIDChangePax ffidChange : ffidUpdateRQ.getFfidChangePaxList()) {
				if (ffidChange.getPaxSeqNo() == lccClientReservationPax.getPaxSequence().intValue()) {
					lccClientReservationPax.getLccClientAdditionPax().setFfid(ffidChange.getFfid());
				}
			}
		}
	}

	private LCCClientReservation populateOldLccReservation(LCCClientReservation lccClientReservation) {
		LCCClientReservation oldLccClientReservation = new LCCClientReservation();
		oldLccClientReservation.setPNR(lccClientReservation.getPNR());
		oldLccClientReservation.setLastUserNote(lccClientReservation.getLastUserNote());
		oldLccClientReservation.setVersion(lccClientReservation.getVersion());
		oldLccClientReservation.setItineraryFareMask(lccClientReservation.getItineraryFareMask());
		Collection<LCCClientReservationPax> colpaxs = lccClientReservation.getPassengers();
		for (LCCClientReservationPax lccClientReservationPax : colpaxs) {
			oldLccClientReservation.addPassenger(lccClientReservationPax);
		}
		return oldLccClientReservation;
	}

}
