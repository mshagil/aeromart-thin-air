package com.isa.aeromart.services.endpoint.dto.modification;

import com.isa.aeromart.services.endpoint.dto.availability.ModificationInfo;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class ReQuoteConfirmRQ extends TransactionalBaseRQ {
	private ModificationInfo modificationInfo;

	public ModificationInfo getModificationInfo() {
		return modificationInfo;
	}

	public void setModificationInfo(ModificationInfo modificationInfo) {
		this.modificationInfo = modificationInfo;
	}

}
