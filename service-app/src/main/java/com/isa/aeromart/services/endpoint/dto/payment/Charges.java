package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;

/** only used by payment summary response */
public class Charges {
	private BigDecimal fare;
	private BigDecimal surcharges;
	private BigDecimal taxes;
	private BigDecimal total;

	public BigDecimal getFare() {
		return fare;
	}

	public void setFare(BigDecimal fare) {
		this.fare = fare;
	}

	public BigDecimal getSurcharges() {
		return surcharges;
	}

	public void setSurcharges(BigDecimal surcharges) {
		this.surcharges = surcharges;
	}

	public BigDecimal getTaxes() {
		return taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
