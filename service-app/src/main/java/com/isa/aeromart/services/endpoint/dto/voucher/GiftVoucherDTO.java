package com.isa.aeromart.services.endpoint.dto.voucher;

import com.isa.thinair.commons.api.dto.VoucherDTO;

public class GiftVoucherDTO extends VoucherDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5406416356802520713L;

	private int quantity;

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
