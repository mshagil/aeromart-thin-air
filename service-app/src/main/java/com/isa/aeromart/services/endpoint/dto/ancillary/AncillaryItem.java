package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;


public abstract class AncillaryItem {

	private String itemId;

	private String itemName;
	
	private String description;
	
	private List<Integer> validations;
	
	private List<Charge> charges;
	
	private List<AdditionalInformation> additionalInfo;
	
	private InventoryInformation inventory;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Integer> getValidations() {
		return validations;
	}

	public void setValidations(List<Integer> validations) {
		this.validations = validations;
	}

	public List<Charge> getCharges() {
		return charges;
	}

	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}

	public List<AdditionalInformation> getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(List<AdditionalInformation> additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public InventoryInformation getInventory() {
		return inventory;
	}

	public void setInventory(InventoryInformation inventory) {
		this.inventory = inventory;
	}

}
