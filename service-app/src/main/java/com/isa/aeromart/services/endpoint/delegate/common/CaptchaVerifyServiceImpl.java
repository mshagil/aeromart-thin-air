package com.isa.aeromart.services.endpoint.delegate.common;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.webplatform.core.service.captchaService.CaptchaService;

public class CaptchaVerifyServiceImpl implements CaptchaVerifyService {

	@Override
	public boolean verifyCaptcha(String captcha, HttpServletRequest request) {
		return CaptchaService.getInstance().validateResponseForID(request.getSession().getId(), captcha);
	}

}
