package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;
import java.util.Date;

public class Card {

	private int cardType;
	private String cardName;// payOptRes
	private String displayName;// payOptRes, effMulPayRes
	private String cardCvv;// makePayReq
	private String cardHoldersName;// makePayReq
	private String cardNo; // makePayReq
	private Date expiryDate;// makePayReq
	private BigDecimal paymentAmount;// makePayReq
	private String cssClassName;
	private String i18nMsgKey;
	private boolean isAllowedInModification;
	private boolean saveCreditCard;
	private String alias;

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getCardCvv() {
		return cardCvv;
	}

	public void setCardCvv(String cardCvv) {
		this.cardCvv = cardCvv;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCardHoldersName() {
		return cardHoldersName;
	}

	public void setCardHoldersName(String cardHoldersName) {
		this.cardHoldersName = cardHoldersName;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getCssClassName() {
		return cssClassName;
	}

	public String getI18nMsgKey() {
		return i18nMsgKey;
	}

	public void setCssClassName(String cssClassName) {
		this.cssClassName = cssClassName;
	}

	public void setI18nMsgKey(String i18nMsgKey) {
		this.i18nMsgKey = i18nMsgKey;
	}

	public boolean isAllowedInModification() {
		return isAllowedInModification;
	}

	public void setAllowedInModification(boolean isAllowedInModification) {
		this.isAllowedInModification = isAllowedInModification;
	}

	public boolean isSaveCreditCard() {
		return saveCreditCard;
	}

	public void setSaveCreditCard(boolean saveCreditCard) {
		this.saveCreditCard = saveCreditCard;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
