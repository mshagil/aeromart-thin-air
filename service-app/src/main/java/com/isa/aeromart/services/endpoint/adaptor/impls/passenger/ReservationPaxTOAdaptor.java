package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class ReservationPaxTOAdaptor implements Adaptor<Passenger, ReservationPaxTO> {

	@Override
	public ReservationPaxTO adapt(Passenger passenger) {
		ReservationPaxTO reservationPaxTO = new ReservationPaxTO();
		reservationPaxTO.setFirstName(passenger.getFirstName());
		reservationPaxTO.setLastName(passenger.getLastName());
		reservationPaxTO.setDateOfBirth(passenger.getDateOfBirth());
		reservationPaxTO.setNationality(passenger.getNationality());
		reservationPaxTO.setSeqNumber(passenger.getPaxSequence());
		reservationPaxTO.setIsParent(passenger.getParentName() != null);
		reservationPaxTO.setPaxType(passenger.getPaxType());
		return reservationPaxTO;
	}

}
