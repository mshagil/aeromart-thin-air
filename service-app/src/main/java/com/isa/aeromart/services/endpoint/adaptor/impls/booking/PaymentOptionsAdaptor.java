package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.payment.Card;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptions;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.utils.payment.PaymentUtils;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.webplatform.api.v2.util.PaxPaymentUtil;
import com.isa.thinair.webplatform.api.v2.util.PaymentAssemblerComposer;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;

public class PaymentOptionsAdaptor implements Adaptor<PaymentOptions, PaymentAssemblerComposer> {

	private static Log log = LogFactory.getLog(PaymentOptionsAdaptor.class);

	private PaymentAssemblerComposer paymentComposer = null;

	private BookingSessionStore bookingSessionStore;

	private PaxPaymentUtil paxPayUtil;

	private Passenger passenger;
	
	private PayByVoucherInfo sessionPayByVoucherInfo;

	public PaymentOptionsAdaptor(PaymentAssemblerComposer paymentComposer, PaxPaymentUtil paxPayUtil,
			BookingSessionStore bookingSessionStore, Passenger passenger, PayByVoucherInfo sessionPayByVoucherInfo) {
		this.paymentComposer = paymentComposer;
		this.bookingSessionStore = bookingSessionStore;
		this.paxPayUtil = paxPayUtil;
		this.passenger = passenger;
		this.sessionPayByVoucherInfo = sessionPayByVoucherInfo;
	}

	@Override
	public PaymentAssemblerComposer adapt(PaymentOptions paymentOptions) {

		PaymentUtils paymentUtils = new PaymentUtils();

		PostPaymentDTO postPaymentDTO = bookingSessionStore.getPostPaymentInformation();

		IPGResponseDTO ipgResponseDTO = null;
		PayCurrencyDTO payCurrencyDTO = null;

		if (postPaymentDTO != null) {
			ipgResponseDTO = postPaymentDTO.getIpgResponseDTO();
			payCurrencyDTO = postPaymentDTO.getPayCurrencyDTO();
		}

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments = bookingSessionStore.getLoyaltyInformation()
				.getCarrierWiseLoyaltyPaymentInfo();

		Date currentDatetime = new Date();

		PaymentGateway paymentGateway = null;
		Card card = null;

		if (paymentOptions != null && paymentOptions.getPaymentGateways() != null
				&& !paymentOptions.getPaymentGateways().isEmpty()) {
			paymentGateway = paymentOptions.getPaymentGateways().get(0);
		}

		if (paymentGateway != null && paymentGateway.getCards() != null && !paymentGateway.getCards().isEmpty()) {
			card = paymentGateway.getCards().get(0);
		}

		boolean isExtCardPay = true;
		if (postPaymentDTO != null
				&& postPaymentDTO.getPaymentGateWay() != null
				&& postPaymentDTO.getPaymentGateWay().getBrokerType() != null
				&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(postPaymentDTO
						.getPaymentGateWay().getBrokerType()) && card != null) {
			isExtCardPay = false;
		}

		while (paxPayUtil.hasConsumablePayments()) {
			PaymentMethod payMethod = paxPayUtil.getNextPaymentMethod();
			BigDecimal paxPayAmt = paxPayUtil.getPaymentAmount(payMethod);
			String payCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
			if (payCurrencyDTO != null && payCurrencyDTO.getPayCurrencyCode() != null
					&& !payCurrencyDTO.getPayCurrencyCode().isEmpty()) {
				payCurrencyCode = payCurrencyDTO.getPayCurrencyCode();
			}
			if (payMethod == PaymentMethod.LMS) {
				for (Entry<String, LoyaltyPaymentInfo> opCarrierLoyaltyPaymentEntry : carrierWiseLoyaltyPayments.entrySet()) {
					LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = opCarrierLoyaltyPaymentEntry.getValue();
					Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
							.getPaxProductPaymentBreakdown();
					BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil.getPaxRedeemedTotal(carrierPaxProductRedeemed,
							passenger.getPaxSequence());

					String loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
					String[] rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
					if (payCurrencyDTO != null) {
						payCurrencyDTO.setTotalPayCurrencyAmount(carrierLmsMemberPayment);
					}
					if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
						paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs, carrierPaxProductRedeemed,
								carrierLmsMemberPayment, payCurrencyDTO, currentDatetime, opCarrierLoyaltyPaymentEntry.getKey());
					}
				}
			} else if (payMethod.equals(PaymentMethod.VOUCHER)) {
				if (paxPayAmt.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0 && sessionPayByVoucherInfo != null) {
					paymentComposer.addVoucherPayment(sessionPayByVoucherInfo, paxPayAmt, payCurrencyDTO, currentDatetime);
				}
			} else {

				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
				try {
					ipgIdentificationParamsDTO = paymentUtils.validateAndPrepareIPGConfigurationParamsDTO(
							paymentGateway.getGatewayId(), payCurrencyCode);
				} catch (ModuleException e) {
					log.error("Validating and preparing IPGConfigurationParamsDTO failed ==> PaymentOptionsAdaptor", e);
				}
				ipgResponseDTO.setCardType(card.getCardType());
				if (!isExtCardPay) {
					paymentComposer.addCreditCardPaymentInternal(paxPayAmt, ipgResponseDTO, ipgIdentificationParamsDTO,
							payCurrencyDTO, new Date(), card.getCardNo(),
							new SimpleDateFormat("yyMM").format(card.getExpiryDate()), card.getCardCvv(),
							card.getCardHoldersName(), null);
				} else {
					paymentComposer.addCreditCardPayment(paxPayAmt, ipgResponseDTO, ipgIdentificationParamsDTO, payCurrencyDTO,
							new Date(), null);
				}
			}
		}
		return paymentComposer;
	}

}
