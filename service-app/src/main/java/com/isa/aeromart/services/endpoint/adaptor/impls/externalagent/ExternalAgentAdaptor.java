package com.isa.aeromart.services.endpoint.adaptor.impls.externalagent;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.dto.externalagent.ConfirmRQ;
import com.isa.aeromart.services.endpoint.dto.externalagent.ExternalAgentDTO;
import com.isa.aeromart.services.endpoint.dto.externalagent.ConfirmRS;
import com.isa.aeromart.services.endpoint.dto.externalagent.CreditPaymentRQ;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airtravelagents.api.dto.ExternalAgentRQDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;

public class ExternalAgentAdaptor {

	public static PayCurrencyDTO getPayCurrencyDTO(String currencyCode, BigDecimal amount) {
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(currencyCode, null);
		payCurrencyDTO.setTotalPayCurrencyAmount(amount);
		return payCurrencyDTO;
	}

	public static PaymentReferenceTO getPaymentReferenceTO(CreditPaymentRQ confirmRQ) {
		PaymentReferenceTO paymentReferenceTO = new PaymentReferenceTO();
		paymentReferenceTO.setPaymentRef(confirmRQ.getOrderId());
		paymentReferenceTO.setPaymentRefType(PaymentReferenceTO.PAYMENT_REF_TYPE.valueOf(confirmRQ.getPaymentInfo().getPaymentMethod()));
		return paymentReferenceTO;
	}

	public static ExternalAgentDTO adaptAgent(Agent agent) {
		ExternalAgentDTO externalAgent = new ExternalAgentDTO(agent);
		return externalAgent;
	}

	public static ConfirmRS adaptExternalAgentRS(Agent agent) {
		ConfirmRS response = new ConfirmRS();
		return response;
	}

	public static ExternalAgentRQDTO adaptExternalAgentRQDTO(ConfirmRQ confirmRQ) {
		ExternalAgentRQDTO externalAgentRQDTO = new ExternalAgentRQDTO();
		externalAgentRQDTO.setAmount(confirmRQ.getPaymentInfo().getAmount());
		externalAgentRQDTO.setCurrencyCode(confirmRQ.getPaymentInfo().getCurrencyCode());
		externalAgentRQDTO.setBlockId(confirmRQ.getBlockReferenceId());
		externalAgentRQDTO.setOrderId(confirmRQ.getOrderId());
		return externalAgentRQDTO;
	}

	public static ExternalAgentRQDTO adaptExternalAgentRQDTO(CreditPaymentRQ confirmRQ) {
		ExternalAgentRQDTO externalAgentRQDTO = new ExternalAgentRQDTO();
		externalAgentRQDTO.setAmount(confirmRQ.getPaymentInfo().getAmount());
		externalAgentRQDTO.setCurrencyCode(confirmRQ.getPaymentInfo().getCurrencyCode());
		externalAgentRQDTO.setOrderId(confirmRQ.getOrderId());
		return externalAgentRQDTO;
	}

}
