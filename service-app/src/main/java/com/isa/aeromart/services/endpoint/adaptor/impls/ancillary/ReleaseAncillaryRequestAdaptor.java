package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.ReleaseAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.constants.AncillariesConstants.AncillaryType;

public class ReleaseAncillaryRequestAdaptor extends TransactionAwareAdaptor<ReleaseAncillaryRQ, Map<AncillaryType, Object>>{

	@Override
	public Map<AncillaryType, Object> adapt(ReleaseAncillaryRQ source) {
		Map<AncillariesConstants.AncillaryType, Object> transformedMap = new HashMap<>();

		AncillaryRequestAdaptor transformer;
		AncillariesConstants.AncillaryType ancillaryType;

		List<SelectedAncillary> ancillaries = source.getAncillaries();
		SYSTEM system = ProxyConstants.SYSTEM.getEnum(getTransactionalAncillaryService().getPreferencesForAncillary(source.getTransactionId()).getSelectedSystem());
		List<InventoryWrapper> inventory = getTransactionalAncillaryService().getFlightSegments(source.getTransactionId());
		RPHGenerator rphGenerator = getTransactionalAncillaryService().getRphGenerator(source.getTransactionId());
		List<SessionPassenger> passengers = getTransactionalAncillaryService().getReservationData(source.getTransactionId()).getPassengers();
		
		//release ancillary from front end
		for (SelectedAncillary selectedAncillary : ancillaries) {
			ancillaryType = AncillariesConstants.AncillaryType.resolveAncillaryType(selectedAncillary.getType());
			transformer = AncillaryAdaptorFactory.getRequestAdaptor(selectedAncillary.getType());
			if(transformer.blockSupported()){
				transformedMap.put(ancillaryType, transformer.toReleaseAncillaries(passengers, rphGenerator, inventory, source.getMetaData(), selectedAncillary.getPreferences(), system, source.getTransactionId(), getTrackInfoDTO()));
			}
		}		
		
		return transformedMap;
	}

}
