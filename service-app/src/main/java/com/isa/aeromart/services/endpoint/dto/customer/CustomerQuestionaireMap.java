package com.isa.aeromart.services.endpoint.dto.customer;

public class CustomerQuestionaireMap {

	private String questionKey;

    private String questionAnswer;

	public String getQuestionKey() {
		return questionKey;
	}

	public void setQuestionKey(String questionKey) {
		this.questionKey = questionKey;
	}

	public String getQuestionAnswer() {
		return questionAnswer;
	}

	public void setQuestionAnswer(String questionAnswer) {
		this.questionAnswer = questionAnswer;
	}

	
}
