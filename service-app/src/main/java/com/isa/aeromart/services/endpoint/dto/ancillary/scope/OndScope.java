package com.isa.aeromart.services.endpoint.dto.ancillary.scope;


import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;

public class OndScope extends Scope {

    private String ondId;

    public OndScope() {
        setScopeType(ScopeType.OND);
    }

    public String getOndId() {
        return ondId;
    }

    public void setOndId(String ondId) {
        this.ondId = ondId;
    }
    
    @Override
	public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (getClass() != obj.getClass()) {
	        return false;
	    }
	    final OndScope other = (OndScope) obj;
	   
	    if ((this.getOndId() == null) ? (other.getOndId() != null) : !this.getOndId().equals(other.getOndId())) {
	        return false;
	    }
	    return true;
	}
    
    @Override
	public int hashCode() {
	    int hash = 3;
	    hash = 53 * hash + (this.ondId != null ? this.ondId.hashCode() : 0);
	    return hash;
	}
}
