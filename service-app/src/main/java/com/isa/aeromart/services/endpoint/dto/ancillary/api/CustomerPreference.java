package com.isa.aeromart.services.endpoint.dto.ancillary.api;

public class CustomerPreference {
	private String seatType;

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

}
