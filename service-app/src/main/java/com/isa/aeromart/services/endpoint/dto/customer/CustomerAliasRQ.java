package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class CustomerAliasRQ extends TransactionalBaseRS {

	private long customerAliasId;

	public long getCustomerAliasId() {
		return customerAliasId;
	}

	public void setCustomerAliasId(long customerAliasId) {
		this.customerAliasId = customerAliasId;
	}

}
