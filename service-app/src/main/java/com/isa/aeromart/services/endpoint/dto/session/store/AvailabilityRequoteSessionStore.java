package com.isa.aeromart.services.endpoint.dto.session.store;

import com.isa.aeromart.services.endpoint.dto.session.RequoteTransaction;

public interface AvailabilityRequoteSessionStore extends AvailabilitySessionStore {
	public static String SESSION_KEY = RequoteTransaction.SESSION_KEY;

}
