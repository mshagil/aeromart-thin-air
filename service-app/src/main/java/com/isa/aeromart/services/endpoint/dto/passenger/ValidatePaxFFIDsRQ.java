package com.isa.aeromart.services.endpoint.dto.passenger;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class ValidatePaxFFIDsRQ extends TransactionalBaseRQ {

	private List<LMSPassenger> lmsPaxDetails;

	public List<LMSPassenger> getLmsPaxDetails() {
		return lmsPaxDetails;
	}

	public void setLmsPaxDetails(List<LMSPassenger> lmsPaxDetails) {
		this.lmsPaxDetails = lmsPaxDetails;
	}

}
