package com.isa.aeromart.services.endpoint.dto.masterdata;

public class OriginDestinationSupportInfo {

	private boolean isModifiedOND;

	public boolean isModifiedOND() {
		return isModifiedOND;
	}

	public void setModifiedOND(boolean isModifiedOND) {
		this.isModifiedOND = isModifiedOND;
	}
	
	
	
}
