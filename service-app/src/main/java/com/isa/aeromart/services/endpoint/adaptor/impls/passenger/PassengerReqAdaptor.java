package com.isa.aeromart.services.endpoint.adaptor.impls.passenger;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.AdditionalInfo;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

public class PassengerReqAdaptor implements Adaptor<Passenger, LCCClientReservationPax> {

	@Override
	public LCCClientReservationPax adapt(Passenger passenger) {
		LCCClientReservationPax lccClientReservationPax = new LCCClientReservationPax();
		lccClientReservationPax.setPaxSequence(passenger.getPaxSequence());
		lccClientReservationPax.setTitle(passenger.getTitle());
		lccClientReservationPax.setFirstName(passenger.getFirstName());
		lccClientReservationPax.setLastName(passenger.getLastName());
		lccClientReservationPax.setPaxType(passenger.getPaxType());
		lccClientReservationPax.setNationalityCode(passenger.getNationality());

		if (passenger.getDateOfBirth() != null) {
			lccClientReservationPax.setDateOfBirth(passenger.getDateOfBirth());
		}

		AdditionalInfo additionalInfo = passenger.getAdditionalInfo();
		if (additionalInfo != null) {
			AdditionalInfoReqAdaptor additionalInfoReqTransformer = new AdditionalInfoReqAdaptor();
			lccClientReservationPax.setLccClientAdditionPax(additionalInfoReqTransformer.adapt(additionalInfo));
		}
		return lccClientReservationPax;
	}

}
