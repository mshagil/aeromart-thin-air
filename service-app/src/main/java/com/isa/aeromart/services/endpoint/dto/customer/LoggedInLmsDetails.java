package com.isa.aeromart.services.endpoint.dto.customer;

public class LoggedInLmsDetails {
	
	private double availablePoints;
	
    private String ffid;
    
    private boolean emailConfirmed;

	public double getAvailablePoints() {
		return availablePoints;
	}

	public void setAvailablePoints(double availablePoints) {
		this.availablePoints = availablePoints;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public boolean isEmailConfirmed() {
		return emailConfirmed;
	}

	public void setEmailConfirmed(boolean emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}
    
}
