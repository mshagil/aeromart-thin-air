package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class PassengerTypeQuantityAdaptor implements Adaptor<List<Passenger>, List<PassengerTypeQuantityTO>> {

	@Override
	public List<PassengerTypeQuantityTO> adapt(List<Passenger> passengers) {
		List<PassengerTypeQuantityTO> passengerTypeQuantityTOs = new ArrayList<>();
		PassengerTypeQuantityTO adultPassengerTypeQuantityTO = new PassengerTypeQuantityTO();
		adultPassengerTypeQuantityTO.setPassengerType(PaxTypeTO.ADULT);
		adultPassengerTypeQuantityTO.setQuantity(0);
		PassengerTypeQuantityTO childPassengerTypeQuantityTO = new PassengerTypeQuantityTO();
		childPassengerTypeQuantityTO.setPassengerType(PaxTypeTO.CHILD);
		childPassengerTypeQuantityTO.setQuantity(0);
		PassengerTypeQuantityTO infantPassengerTypeQuantityTO = new PassengerTypeQuantityTO();
		infantPassengerTypeQuantityTO.setPassengerType(PaxTypeTO.INFANT);
		infantPassengerTypeQuantityTO.setQuantity(0);
		
		for (Passenger passenger : passengers) {
			if (passenger.getPaxType().equals(PaxTypeTO.ADULT)) {
				adultPassengerTypeQuantityTO.setQuantity(adultPassengerTypeQuantityTO.getQuantity() + 1);
			} else if (passenger.getPaxType().equals(PaxTypeTO.CHILD)) {
				childPassengerTypeQuantityTO.setQuantity(childPassengerTypeQuantityTO.getQuantity() + 1);
			} else if (passenger.getPaxType().equals(PaxTypeTO.INFANT)) {
				infantPassengerTypeQuantityTO.setQuantity(infantPassengerTypeQuantityTO.getQuantity() + 1);
			}
		}

		passengerTypeQuantityTOs.add(adultPassengerTypeQuantityTO);
		passengerTypeQuantityTOs.add(childPassengerTypeQuantityTO);
		passengerTypeQuantityTOs.add(infantPassengerTypeQuantityTO);

		return passengerTypeQuantityTOs;
	}

}
