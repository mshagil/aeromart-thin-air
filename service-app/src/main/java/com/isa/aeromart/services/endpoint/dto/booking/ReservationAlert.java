package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.List;

public class ReservationAlert {

	private String alertCategory;

	private String flightSegmentRPH;

	private List<AlertTO> alerts;

	public String getAlertCategory() {
		return alertCategory;
	}

	public void setAlertCategory(String alertCategory) {
		this.alertCategory = alertCategory;
	}

	public String getFlightSegmentRPH() {
		return flightSegmentRPH;
	}

	public void setFlightSegmentRPH(String flightSegmentRPH) {
		this.flightSegmentRPH = flightSegmentRPH;
	}

	public List<AlertTO> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<AlertTO> alerts) {
		this.alerts = alerts;
	}

}
