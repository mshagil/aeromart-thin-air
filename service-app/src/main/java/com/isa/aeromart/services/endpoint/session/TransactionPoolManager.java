package com.isa.aeromart.services.endpoint.session;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TransactionPoolManager {
	private final static int MAX_TRANSACTIONS_PER_SESSION = 3;
	private final static int FIRST_IN_QUEUE = 0;
	private List<String> transactions = new ArrayList<String>();
	private TransactionFactory transactionFactory;
	private Log logger = LogFactory.getLog(TransactionPoolManager.class);

	public TransactionPoolManager(TransactionFactory transactionFactory) {
		this.transactionFactory = transactionFactory;
	}

	public void addTransaction(String transactionId) {
		transactions.add(transactionId);
		logger.debug("Transaction Added : " + transactionId +" Session ID:" + transactionFactory.getHttpSession().getId());
		cleanUp();
	}

	public void removeTransaction(String transactionId) {
		if (transactions.contains(transactionId)) {
			transactions.remove(transactionId);
			logger.debug("Transaction Removed : " + transactionId +" Session ID:" + transactionFactory.getHttpSession().getId());
		}
	}

	private void cleanUp() {
		if (transactions.size() > MAX_TRANSACTIONS_PER_SESSION) {
			synchronized (transactions) {
				while (transactions.size() > MAX_TRANSACTIONS_PER_SESSION) {
					String id = transactions.get(FIRST_IN_QUEUE);
					transactionFactory.terminateTransaction(id);
					removeTransaction(id);
					logger.info("Oldest Transaction ID: " + id + " removed. Exceeded max transaction count allowed per session: "
							+ MAX_TRANSACTIONS_PER_SESSION +" Session ID:" + transactionFactory.getHttpSession().getId());
				}
			}
		}
	}
}
