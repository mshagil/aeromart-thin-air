package com.isa.aeromart.services.endpoint.dto.session.impl.financial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.utils.booking.FlexiUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class FinancialStore {

	// anci financial data
	private ReservationChargeTo<LCCClientExternalChgDTO> ancillaryExternalCharges;

	// payment financial data
	private ReservationChargeTo<LCCClientExternalChgDTO> paymentExternalCharges;

	// availability financial data (build from flexiChargesList
	private volatile ReservationChargeTo<LCCClientExternalChgDTO> availabilityExternalCharges=null;
	
	private final Object availabilityExternalChargesWriteLock = new Object();

	public BigDecimal getTotalExternalCharges() {
		return AccelAeroCalculator.add(getTotalAnicillaryCharges(), getTotalPaymentCharges(), getTotalAvailabilityCharges());
	}

	// ----------------------------------------

	public ReservationChargeTo<LCCClientExternalChgDTO> getAncillaryExternalCharges() {
		if (ancillaryExternalCharges == null) {
			ancillaryExternalCharges = new ReservationChargeTo<LCCClientExternalChgDTO>();
		}
		return ancillaryExternalCharges;
	}

	public void setAncillaryExternalCharges(ReservationChargeTo<LCCClientExternalChgDTO> ancillaryExternalCharges) {
		this.ancillaryExternalCharges = ancillaryExternalCharges;
	}

	public ReservationChargeTo<LCCClientExternalChgDTO> getPaymentExternalCharges() {
		if (paymentExternalCharges == null) {
			paymentExternalCharges = new ReservationChargeTo<LCCClientExternalChgDTO>();
		}
		return paymentExternalCharges;
	}

	public void setPaymentExternalCharges(ReservationChargeTo<LCCClientExternalChgDTO> paymentExternalCharges) {
		this.paymentExternalCharges = paymentExternalCharges;
	}

	public ReservationChargeTo<LCCClientExternalChgDTO> getAvailabilityExternalCharges() {
		if (availabilityExternalCharges == null) {
			availabilityExternalCharges = new ReservationChargeTo<LCCClientExternalChgDTO>();
		}
		return availabilityExternalCharges;
	}

	public void setAvailabilityExternalCharges(ReservationChargeTo<LCCClientExternalChgDTO> availabilityExternalCharges) {
		this.availabilityExternalCharges = availabilityExternalCharges;
	}

	public BigDecimal getTotalAnicillaryCharges() {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (ancillaryExternalCharges != null) {
			if (ancillaryExternalCharges.getReservationCharges() != null
					&& !ancillaryExternalCharges.getReservationCharges().isEmpty()) {
				for (LCCClientExternalChgDTO charge : ancillaryExternalCharges.getReservationCharges()) {
					total = total.add(charge.getAmount());
				}
			}

			if (ancillaryExternalCharges.getPassengers() != null && !ancillaryExternalCharges.getPassengers().isEmpty()) {
				for (PassengerChargeTo<LCCClientExternalChgDTO> passenger : ancillaryExternalCharges.getPassengers()) {
					for (LCCClientExternalChgDTO charge : passenger.getGetPassengerCharges()) {
						total = total.add(charge.getAmount());
					}
				}
			}

		}

		return total;
	}

	public BigDecimal getTotalAvailabilityCharges() {

		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (availabilityExternalCharges != null) {

			// currently not set
			if (availabilityExternalCharges.getReservationCharges() != null
					&& !availabilityExternalCharges.getReservationCharges().isEmpty()) {
				for (LCCClientExternalChgDTO charge : availabilityExternalCharges.getReservationCharges()) {
					total = total.add(charge.getAmount());
				}
			}

			// set with flexi charges added at Availability
			if (availabilityExternalCharges.getPassengers() != null && !availabilityExternalCharges.getPassengers().isEmpty()) {
				for (PassengerChargeTo<LCCClientExternalChgDTO> passenger : availabilityExternalCharges.getPassengers()) {
					for (LCCClientExternalChgDTO charge : passenger.getGetPassengerCharges()) {
						total = total.add(charge.getAmount());
					}
				}
			}

		}

		return total;
	}

	public BigDecimal getTotalPaymentCharges() {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paymentExternalCharges != null && paymentExternalCharges.getReservationCharges() != null
				&& !paymentExternalCharges.getReservationCharges().isEmpty()) {
			for (LCCClientExternalChgDTO charge : paymentExternalCharges.getReservationCharges()) {
				total = total.add(charge.getAmount());
			}
		}

		return total;
	}
	
	public BigDecimal getTotalPaxWisePaymentCharges() {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paymentExternalCharges != null && paymentExternalCharges.getPassengers() != null && !paymentExternalCharges.getPassengers().isEmpty()) {
			for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : paymentExternalCharges.getPassengers()) {
				if (passengerChargeTo.getGetPassengerCharges() != null
						&& !passengerChargeTo.getGetPassengerCharges().isEmpty()) {
					for (LCCClientExternalChgDTO paxCharge : passengerChargeTo.getGetPassengerCharges()) {
						total = total.add(paxCharge.getAmount());
					}
				}
			}

		}
		return total;
	}

	public Collection<LCCClientExternalChgDTO> getAllExternalCharges() {
		Collection<LCCClientExternalChgDTO> allExternalCharges = new ArrayList<LCCClientExternalChgDTO>();
		if (ancillaryExternalCharges != null) {
			if (ancillaryExternalCharges.getReservationCharges() != null
					&& !ancillaryExternalCharges.getReservationCharges().isEmpty()) {
				allExternalCharges.addAll(ancillaryExternalCharges.getReservationCharges());
			}

			if (ancillaryExternalCharges.getPassengers() != null && !ancillaryExternalCharges.getPassengers().isEmpty()) {
				for (PassengerChargeTo<LCCClientExternalChgDTO> passenger : ancillaryExternalCharges.getPassengers()) {
					if (ancillaryExternalCharges.getPassengers() != null && !ancillaryExternalCharges.getPassengers().isEmpty()) {
						allExternalCharges.addAll(passenger.getGetPassengerCharges());
					}

				}

			}
		}

		if (paymentExternalCharges != null) {
			if (!paymentExternalCharges.getReservationCharges().isEmpty()) {
				allExternalCharges.addAll(paymentExternalCharges.getReservationCharges());
			}
			if (paymentExternalCharges.getPassengers() != null && !paymentExternalCharges.getPassengers().isEmpty()) {
				for(PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : paymentExternalCharges.getPassengers()){
					allExternalCharges.addAll(passengerChargeTo.getGetPassengerCharges());
				}				
			}
			
		}

		// add flexi charges added at availability
		// FOR NOW, only passengerwise chareges are getting set
		// no need to check reservationCharges
		if (availabilityExternalCharges != null && availabilityExternalCharges.getPassengers() != null
				&& !availabilityExternalCharges.getPassengers().isEmpty()) {
			for (PassengerChargeTo<LCCClientExternalChgDTO> passenger : availabilityExternalCharges.getPassengers()) {
				if (availabilityExternalCharges.getPassengers() != null && !availabilityExternalCharges.getPassengers().isEmpty()) {
					allExternalCharges.addAll(passenger.getGetPassengerCharges());
				}

			}

		}
		return allExternalCharges;
	}

	// for now, only pax wise flexi charges
	public void buildAvailabilityFlexiCharges(List<SessionFlexiDetail> sessionFlexiDetailList, List<Passenger> passengers,
			List<SessionFlightSegment> segments, TravellerQuantity travellerQuantity) {
		// fix AARESAA-27794
		availabilityExternalCharges = null;

		synchronized (availabilityExternalChargesWriteLock) {

			// add paxwise segmentwise flexi external charges to Passengerwise AncillaryExternalCharges
			if (sessionFlexiDetailList != null && passengers != null) {

				List<PassengerChargeTo<LCCClientExternalChgDTO>> paxAncillaryExternalCharges = new ArrayList<PassengerChargeTo<LCCClientExternalChgDTO>>();

				Map<Integer, List<LCCClientFlexiExternalChgDTO>> paxWiseSegWiseFlexiExternalChargesMap = FlexiUtil
						.getPaxWiseSegmentWiseFlexiExternalCharges(segments, travellerQuantity, sessionFlexiDetailList,
								passengers);

				PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo;

				// per passenger get list of segment-wise flexi-charges
				for (Map.Entry mapEntry : paxWiseSegWiseFlexiExternalChargesMap.entrySet()) {
					passengerChargeTo = new PassengerChargeTo<LCCClientExternalChgDTO>();
					passengerChargeTo.setPassengerRph(mapEntry.getKey().toString());
					passengerChargeTo.setGetPassengerCharges((List<LCCClientExternalChgDTO>) mapEntry.getValue());
					paxAncillaryExternalCharges.add(passengerChargeTo);
				}

				availabilityExternalCharges = new ReservationChargeTo<LCCClientExternalChgDTO>();
				availabilityExternalCharges.getPassengers().addAll(paxAncillaryExternalCharges);
			}
		}

	}
}
