package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.*;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability.ApplicableType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope.ScopeType;
import com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput.InFlightSsrInput;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.SystemProvided;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.inflight.InFlightSsrItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.inflight.InFlightSsrItems;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import org.springframework.context.MessageSource;

import java.util.ArrayList;
import java.util.List;

public class SsrInFlightServiceResponseAdaptor extends AncillariesResponseAdaptor {
	@Override
	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability.isAnciActive(availability, LCCAncillaryAvailabilityInDTO.LCCAncillaryType.SSR);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	@Override
	public AncillaryType toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource) {

		AncillaryType ancillaryType = new AncillaryType();
		List<Provider> providers = new ArrayList<Provider>();
		SystemProvided provider = new SystemProvided();
		List<AvailableAncillary> availableAncillay = new ArrayList<AvailableAncillary>();
		AvailableAncillary availableAncillary = new AvailableAncillary();
		List<AvailableAncillaryUnit> availableUnits = new ArrayList<AvailableAncillaryUnit>();

		String flightSegmentRPH;
		FlightSegmentTO flightSegmentTO;

		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.SSR_IN_FLIGHT.name());
		provider.setProviderType(Provider.ProviderType.SYSTEM);
		List<LCCFlightSegmentSSRDTO> source = (List<LCCFlightSegmentSSRDTO>) ancillaries;

		for (LCCFlightSegmentSSRDTO lccFlightSegmentSSRDTO : source) {

			AvailableAncillaryUnit availableAncillaryUnit = new AvailableAncillaryUnit();
			SegmentScope SegmentScope = new SegmentScope();
			InFlightSsrItems inflightSsrItems = new InFlightSsrItems();
			List<InFlightSsrItem> inFlightSsrItemsList = new ArrayList<InFlightSsrItem>();

			flightSegmentTO = lccFlightSegmentSSRDTO.getFlightSegmentTO();
			flightSegmentRPH = rphGenerator.getRPH(flightSegmentTO.getFlightRefNumber());

			SegmentScope.setFlightSegmentRPH(flightSegmentRPH);
			SegmentScope.setScopeType(ScopeType.SEGMENT);
			availableAncillaryUnit.setScope(SegmentScope);

			Applicability applicability = new Applicability();
			applicability.setApplicableType(ApplicableType.PASSENGER.name());
			availableAncillaryUnit.setApplicability(applicability);

			for (LCCSpecialServiceRequestDTO lccSpecialServiceRequestDTO : lccFlightSegmentSSRDTO.getSpecialServiceRequest()) {

				InFlightSsrItem inFlightSsrItem = new InFlightSsrItem();
				List<Charge> charges = new ArrayList<Charge>();
				Charge charge = new Charge();

				inFlightSsrItem.setDescription(lccSpecialServiceRequestDTO.getDescription());
				inFlightSsrItem.setAvailableQty(lccSpecialServiceRequestDTO.getAvailableQty());
				inFlightSsrItem.setServiceQuantity(lccSpecialServiceRequestDTO.getServiceQuantity());
				inFlightSsrItem.setSsrCode(lccSpecialServiceRequestDTO.getSsrCode());
				inFlightSsrItem.setItemId(lccSpecialServiceRequestDTO.getSsrCode());
				inFlightSsrItem.setSsrName(lccSpecialServiceRequestDTO.getSsrName());

				charge.setChargeBasis(Charge.ChargeBasis.PER_PASSENGER.name());
				charge.setAmount(lccSpecialServiceRequestDTO.getCharge());
				charges.add(charge);

				inFlightSsrItem.setCharges(charges);
				inFlightSsrItemsList.add(inFlightSsrItem);
			}

			inflightSsrItems.setItems(inFlightSsrItemsList);
			availableAncillaryUnit.setItemsGroup(inflightSsrItems);
			availableUnits.add(availableAncillaryUnit);
		}

		availableAncillary.setAvailableUnits(availableUnits);
		availableAncillay.add(availableAncillary);
		provider.setAvailableAncillaries(availableAncillary);
		providers.add(provider);
		ancillaryType.setProviders(providers);

		return ancillaryType;
	}

	@Override
	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {
		LCCSpecialServiceRequestDTO selectedinFlihgtSSR = new LCCSpecialServiceRequestDTO();
		selectedinFlihgtSSR.setSsrCode(item.getId());
		selectedinFlihgtSSR.setCharge(item.getAmount());
		if (item.getInput() != null) {
			InFlightSsrInput input = (InFlightSsrInput) item.getInput();
			selectedinFlihgtSSR.setText(input.getComment());
		}

		return selectedinFlihgtSSR;
	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

	}
}
