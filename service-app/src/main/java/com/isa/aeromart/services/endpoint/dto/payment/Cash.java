package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.aeromart.services.endpoint.constant.PaymentConsts.BookingType;

/** only used in payOptRes **/
public class Cash {

	private Date onholdRelease;
	private BigDecimal payableAmount;
	private BookingType bookingType;
	private boolean imageCapcha = false;
	private String releaseTimeDisplay = "";

	public Cash() {
		bookingType = BookingType.ONHOLD;
	}

	public Date getOnholdRelease() {
		return onholdRelease;
	}

	public void setOnholdRelease(Date onholdRelease) {
		this.onholdRelease = onholdRelease;
	}

	public BookingType getBookingType() {
		return bookingType;
	}

	public boolean isImageCapcha() {
		return imageCapcha;
	}

	public String getReleaseTimeDisplay() {
		return releaseTimeDisplay;
	}

	public void setImageCapcha(boolean imageCapcha) {
		this.imageCapcha = imageCapcha;
	}

	public void setReleaseTimeDisplay(String releaseTimeDisplay) {
		this.releaseTimeDisplay = releaseTimeDisplay;
	}

	public BigDecimal getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(BigDecimal payableAmount) {
		this.payableAmount = payableAmount;
	}

}
