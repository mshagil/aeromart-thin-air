package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsConfig;
import com.isa.aeromart.services.endpoint.dto.masterdata.PaxDetailsField;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;


public class PaxDetailsConfigAdaptor implements Adaptor<IBEPaxConfigDTO, PaxDetailsField>{

	@Override
	public PaxDetailsField adapt(IBEPaxConfigDTO source) {
		
		PaxDetailsField paxDetailsField = new PaxDetailsField();
		PaxDetailsConfig paxDetailsConfigVisibility = new PaxDetailsConfig();
		PaxDetailsConfig paxDetailsConfigMandatory = new PaxDetailsConfig();
		PaxDetailsConfig autoCheckinVisibility = new PaxDetailsConfig();
		PaxDetailsConfig autoCheckinMandatory = new PaxDetailsConfig();
		
		paxDetailsField.setFieldName(source.getFieldName());
		
		if (ReservationInternalConstants.PassengerType.ADULT.equals(source.getPaxTypeCode())) {
			paxDetailsConfigVisibility.setAdult(source.isIbeVisibility());
			paxDetailsConfigMandatory.setAdult(source.isIbeMandatory());
			autoCheckinVisibility.setAdult(source.getAutoCheckinVisibility());
			autoCheckinMandatory.setAdult(source.getAutoCheckinMandatory());
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(source.getPaxTypeCode())) {
			paxDetailsConfigVisibility.setChild(source.isIbeVisibility());
			paxDetailsConfigMandatory.setChild(source.isIbeMandatory());
			autoCheckinVisibility.setAdult(source.getAutoCheckinVisibility());
			autoCheckinMandatory.setAdult(source.getAutoCheckinMandatory());
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(source.getPaxTypeCode())) {
			paxDetailsConfigVisibility.setInfant(source.isIbeVisibility());
			paxDetailsConfigMandatory.setInfant(source.isIbeMandatory());
			autoCheckinVisibility.setAdult(source.getAutoCheckinVisibility());
			autoCheckinMandatory.setAdult(source.getAutoCheckinMandatory());
		}
		
		paxDetailsField.setVisibility(paxDetailsConfigVisibility);
		paxDetailsField.setMandatory(paxDetailsConfigMandatory);
		paxDetailsField.setAutoCheckinVisibility(autoCheckinVisibility);
		paxDetailsField.setAutoCheckinMandatory(autoCheckinMandatory);
		paxDetailsField.setValidation("");
		
		return paxDetailsField;
	}

}
