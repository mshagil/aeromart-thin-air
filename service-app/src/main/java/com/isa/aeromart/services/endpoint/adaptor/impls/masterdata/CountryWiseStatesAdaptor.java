package com.isa.aeromart.services.endpoint.adaptor.impls.masterdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.masterdata.StateDetails;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.thinair.airmaster.api.model.State;

public class CountryWiseStatesAdaptor implements Adaptor<Map<String, List<State>>, Map<String, List<StateDetails>>>{
	
	@Override
	public Map<String, List<StateDetails>> adapt(Map<String, List<State>> stateMap) {
		StateAdaptor stateAdaptor = new StateAdaptor();
		Map<String, List<StateDetails>> countryWiseStates = new HashMap<>();
		for (Map.Entry<String, List<State>> entry : stateMap.entrySet()) {
			List<StateDetails> stateDetails = new ArrayList<>();
			AdaptorUtils.adaptCollection(entry.getValue(), stateDetails, (Adaptor)stateAdaptor);
			countryWiseStates.put(entry.getKey(), stateDetails);
		}
		return countryWiseStates;
	}

}
