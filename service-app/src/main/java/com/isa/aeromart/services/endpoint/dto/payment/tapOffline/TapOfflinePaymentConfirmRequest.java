package com.isa.aeromart.services.endpoint.dto.payment.tapOffline;

import java.io.Serializable;

public class TapOfflinePaymentConfirmRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String orderNumber;

	private String resCode;

	private String resMessage;

	private String payMode;

	private String payRefId;

	private String tapRefId;

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResMessage() {
		return resMessage;
	}

	public void setResMessage(String resMessage) {
		this.resMessage = resMessage;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getPayRefId() {
		return payRefId;
	}

	public void setPayRefId(String payRefId) {
		this.payRefId = payRefId;
	}

	public String getTapRefId() {
		return tapRefId;
	}

	public void setTapRefId(String tapRefId) {
		this.tapRefId = tapRefId;
	}

	@Override
	public String toString() {
		return "TapOfflinePaymentConfirmRequest [orderNumber=" + orderNumber + ", resCode=" + resCode + ", resMessage="
				+ resMessage + ", payMode=" + payMode + ", payRefId=" + payRefId + ", tapRefId=" + tapRefId
				+ ", getOrderNumber()=" + getOrderNumber() + ", getResCode()=" + getResCode() + ", getResMessage()="
				+ getResMessage() + ", getPayMode()=" + getPayMode() + ", getPayRefId()=" + getPayRefId() + ", getTapRefId()="
				+ getTapRefId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
	
	

}
