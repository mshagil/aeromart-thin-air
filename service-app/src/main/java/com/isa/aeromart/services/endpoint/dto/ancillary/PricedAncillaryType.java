package com.isa.aeromart.services.endpoint.dto.ancillary;

import java.util.List;

public class PricedAncillaryType {

	private String ancillaryType;

	private List<AncillaryScope> ancillaryScopes;

	private List<MonetaryAmendment> amendments;

	public String getAncillaryType() {
		return ancillaryType;
	}

	public void setAncillaryType(String ancillaryType) {
		this.ancillaryType = ancillaryType;
	}

	public List<AncillaryScope> getAncillaryScopes() {
		return ancillaryScopes;
	}

	public void setAncillaryScopes(List<AncillaryScope> ancillaryScopes) {
		this.ancillaryScopes = ancillaryScopes;
	}

	public List<MonetaryAmendment> getAmendments() {
		return amendments;
	}

	public void setAmendments(List<MonetaryAmendment> amendments) {
		this.amendments = amendments;
	}
}
