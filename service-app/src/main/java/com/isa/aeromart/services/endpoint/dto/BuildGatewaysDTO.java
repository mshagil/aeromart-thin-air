package com.isa.aeromart.services.endpoint.dto;

import com.isa.aeromart.services.endpoint.dto.payment.ServiceTaxRQ;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

import java.math.BigDecimal;

/**
 * Created by degorov on 6/20/17.
 */
public class BuildGatewaysDTO {
    private BigDecimal cardPayableAmount;
    private PaymentBaseSessionStore paymentBaseSessionStore;
    private boolean offlinePaymentEnable;
    private TrackInfoDTO trackInfoDTO;
    private boolean isOnHoldPnr;
    private String pnr;
    private ServiceTaxRQ serviceTaxRQ;
    boolean isBalancePayment; 

    public BuildGatewaysDTO() {
    }

    public BigDecimal getCardPayableAmount() {
        return cardPayableAmount;
    }

    public PaymentBaseSessionStore getPaymentBaseSessionStore() {
        return paymentBaseSessionStore;
    }

    public boolean isOfflinePaymentEnable() {
        return offlinePaymentEnable;
    }

    public TrackInfoDTO getTrackInfoDTO() {
        return trackInfoDTO;
    }

    public boolean isOnHoldPnr() {
        return isOnHoldPnr;
    }    

    public String getPnr() {
		return pnr;
	}       
	
	public boolean isBalancePayment() {
		return isBalancePayment;
	}

	public ServiceTaxRQ getServiceTaxRQ() {
		return serviceTaxRQ;
	}	

	public BuildGatewaysDTO setCardPayableAmount(BigDecimal cardPayableAmount) {
        this.cardPayableAmount = cardPayableAmount;
        return this;
    }

    public BuildGatewaysDTO setPaymentBaseSessionStore(PaymentBaseSessionStore paymentBaseSessionStore) {
        this.paymentBaseSessionStore = paymentBaseSessionStore;
        return this;
    }

    public BuildGatewaysDTO setOfflinePaymentEnable(boolean offlinePaymentEnable) {
        this.offlinePaymentEnable = offlinePaymentEnable;
        return this;
    }

    public BuildGatewaysDTO setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
        this.trackInfoDTO = trackInfoDTO;
        return this;
    }

    public BuildGatewaysDTO setOnHoldPnr(boolean onHoldPnr) {
        isOnHoldPnr = onHoldPnr;
        return this;
    }
    
    public BuildGatewaysDTO setPnr(String pnr) {
		this.pnr = pnr;
		return this;
	}
    
    public BuildGatewaysDTO setServiceTaxRQ(ServiceTaxRQ serviceTaxRQ) {
		this.serviceTaxRQ = serviceTaxRQ;
		return this;
	}
    
    public BuildGatewaysDTO setBalancePayment(boolean isBalancePayment) {
		this.isBalancePayment = isBalancePayment;
		return this;
	}

}
