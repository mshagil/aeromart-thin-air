package com.isa.aeromart.services.endpoint.dto.masterdata;

public class AirportDTO {

	private String airportCode;
	
	private String airportName;

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	
}
