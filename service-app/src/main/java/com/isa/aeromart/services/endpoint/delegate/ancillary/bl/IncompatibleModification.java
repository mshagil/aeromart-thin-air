package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.common.Transition;

public class IncompatibleModification extends DefaultAncillarySelectionStrategy {

    public PricedAncillaryType getDefaultPricedAncillaryType(PricedAncillaryType prevPricedAncillaryType,  AncillaryType availableAncillaryType, Transition<List<Scope>> transition) {
        PricedAncillaryType pricedAncillaryType = new PricedAncillaryType();
        pricedAncillaryType.setAncillaryType(prevPricedAncillaryType.getAncillaryType());
        pricedAncillaryType.setAncillaryScopes(new ArrayList<>());
        return pricedAncillaryType;
    }
}
