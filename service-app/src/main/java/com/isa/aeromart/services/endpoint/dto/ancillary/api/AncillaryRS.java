package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationDescriptor;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class AncillaryRS extends TransactionalBaseRS {

	private ReservationDescriptor reservationData;
	
	private MetaData metaData;
	
	private List<AncillaryType> ancillaries;

	public ReservationDescriptor getReservationData() {
		return reservationData;
	}

	public void setReservationData(ReservationDescriptor reservationData) {
		this.reservationData = reservationData;
	}

	public MetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}

	public List<AncillaryType> getAncillaries() {
		return ancillaries;
	}

	public void setAncillaries(List<AncillaryType> ancillaries) {
		this.ancillaries = ancillaries;
	}
}
