package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.List;

public class PaxTicketCoupons {

	private int paxSequence;

	private List<PaxSegmentTicketCoupons> reservationSegmentTicketCoupons;

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public List<PaxSegmentTicketCoupons> getReservationSegmentTicketCoupons() {
		return reservationSegmentTicketCoupons;
	}

	public void setReservationSegmentTicketCoupons(List<PaxSegmentTicketCoupons> reservationSegmentTicketCoupons) {
		this.reservationSegmentTicketCoupons = reservationSegmentTicketCoupons;
	}

}
