package com.isa.aeromart.services.endpoint.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.UpdateValidateFFIDAdaptor;
import com.isa.aeromart.services.endpoint.constant.CommonConstants;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.BookingType;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.delegate.passenger.PassengerService;
import com.isa.aeromart.services.endpoint.dto.ancillary.Ancillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.Assignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.BasicReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.ContactInformation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.ModifyReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.OndUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Passenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AncillaryAvailabilityRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AvailableAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BaseAncillarySelectionsRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.BlockAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.availability.OriginDestinationInfo;
import com.isa.aeromart.services.endpoint.dto.availability.Preferences;
import com.isa.aeromart.services.endpoint.dto.availability.RequoteRQ;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.masterdata.AirportMessagesRQ;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccResSegmentInfo;
import com.isa.aeromart.services.endpoint.dto.modification.FFIDUpdateRQ;
import com.isa.aeromart.services.endpoint.dto.modification.NameChangePassenger;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRQ;
import com.isa.aeromart.services.endpoint.dto.payment.BinPromotionRQ;
import com.isa.aeromart.services.endpoint.dto.payment.EffectivePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PayfortCheckRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRQ;
import com.isa.aeromart.services.endpoint.dto.ping.PingParams;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airmaster.api.dto.CountryDetailsDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.dto.ondpublish.OndLocationDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;

import org.springframework.util.CollectionUtils;

public class ValidationServiceImpl implements ValidationService {

	protected static final String INFANT = "INFANT";
	protected static final String ADULT = "ADULT";
	protected static final String CHILD = "CHILD";

	private TransactionalAncillaryService transactionalAnciService;
	
	private static Log log = LogFactory.getLog(ValidationServiceImpl.class);

	@Autowired
	private PassengerService passengerService;

	public TransactionalAncillaryService getTransactionalAnciService() {
		return transactionalAnciService;
	}

	public void setTransactionalAnciService(TransactionalAncillaryService transactionalAnciService) {
		this.transactionalAnciService = transactionalAnciService;
	}

	@Override
	public void isValidPingRequest(PingParams pingParams) throws ValidationException {
		// TODO Auto-generated method stub

		if (pingParams.getEchoToken() == null) {
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}
	}

	@Override
	public void nameChangeValidate(LCCClientReservation reservation, List<NameChangePassenger> nameChangePassengerList,
			SYSTEM system, TrackInfoDTO trackInfo) throws ValidationException, ModuleException {

		validateSystemParams(reservation.getInterlineModificationParams(), reservation.isGroupPNR(),
				reservation.getNameChangeCount(), ModificationUtils.isPartiallyFlownReservation(reservation.getSegments()));
		for (LCCClientReservationSegment segment : reservation.getSegments()) {
			if (segment.isUnSegment()
					&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())) {
				throw new ValidationException(ValidationException.Code.UN_SEG_FOUND);
			}
		}

		boolean unModifiedNameIncludes = false; // check whether : unmodified name sent as modified
		NameChangePassenger updatedNames;
		for (Iterator<NameChangePassenger> iterator = nameChangePassengerList.iterator(); iterator.hasNext();) {
			updatedNames = iterator.next();
			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				if (updatedNames.getPaxSeqNo() == pax.getPaxSequence()) {
					if (updatedNames.getFirstName().equals(pax.getFirstName())
							&& updatedNames.getLastName().equals(pax.getLastName())
							&& updatedNames.getTitle().equals(pax.getTitle())) {
						unModifiedNameIncludes = true;
						break;
					}
					if ((updatedNames.getFirstName().equalsIgnoreCase(pax.getFirstName()) && updatedNames.getLastName()
							.equalsIgnoreCase(pax.getLastName()))
							|| (updatedNames.getFirstName().equalsIgnoreCase(pax.getFirstName()) && updatedNames.getLastName()
									.equals(pax.getLastName()))
							|| (updatedNames.getFirstName().equals(pax.getFirstName()) && updatedNames.getLastName()
									.equalsIgnoreCase(pax.getLastName()))) {
						iterator.remove();
					}
				}
			}
		}

		if (unModifiedNameIncludes || nameChangePassengerList.size() == 0) {
			throw new ValidationException(ValidationException.Code.NO_CHANGE_FOUND);
		}
	}

	private void validateSystemParams(List<String> interlineModificationParams, boolean groupPNR, Integer nameChangeCount,
			boolean partiallyFlownRes) {
		boolean nameChangeEligible;
		boolean nameChangeEligibleFlown;
		if (groupPNR) {
			nameChangeEligible = interlineModificationParams.contains(ModifcationParamTypes.FARE_RULE_NCC_ENABLED);
			nameChangeEligibleFlown = interlineModificationParams.contains(ModifcationParamTypes.FARE_RULE_NCC_FOR_FLOWN_ENABLED);
		} else {
			nameChangeEligible = AppSysParamsUtil.isEnableFareRuleLevelNCC(ApplicationEngine.IBE);
			nameChangeEligibleFlown = AppSysParamsUtil.isEnableFareRuleLevelNCCForFlown(ApplicationEngine.IBE);

			if (((nameChangeEligible && !partiallyFlownRes) || (nameChangeEligible && partiallyFlownRes && nameChangeEligibleFlown))
					&& nameChangeCount >= AppSysParamsUtil.getNameChangeMaxCount(ApplicationEngine.IBE)) {
				throw new ValidationException(ValidationException.Code.NAME_CHANGE_LIMIT_EXCEEDED);
			}
		}
		if (!((nameChangeEligible && !partiallyFlownRes) || (partiallyFlownRes && nameChangeEligible && nameChangeEligibleFlown))) {
			throw new ValidationException(ValidationException.Code.NAME_CHANGE_NOT_ALLOWED);
		}
	}

	@Override
	public void checkDuplicateNames(List<LCCClientReservationPax> nameChangedpaxList, Set<LCCClientReservationSegment> segList,
			SYSTEM system, TrackInfoDTO trackInfo) throws ModuleException {

		if (AppSysParamsUtil.isDuplicateNameCheckEnabled()) {

			DuplicateValidatorAssembler dva = new DuplicateValidatorAssembler();
			List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
			for (LCCClientReservationSegment lccResSeg : segList) {
				FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
				flightSegmentTO.setFlightRefNumber(lccResSeg.getFlightSegmentRefNumber());
				flightSegmentTO.setOperatingAirline(lccResSeg.getCarrierCode());
				flightSegmentTOs.add(flightSegmentTO);
			}
			dva.setTargetSystem(system);
			dva.setFlightSegments(flightSegmentTOs);
			dva.setPaxList(nameChangedpaxList);
			boolean hasDuplicates = ModuleServiceLocator.getAirproxyReservationBD().checkForDuplicates(dva, null, trackInfo);

			if (hasDuplicates) {
				throw new ValidationException(ValidationException.Code.DUPLICATE_PAX_NAMES_FOUND);
			}
		}
	}

	@Override
	public void validateAvailabilityRQ(AvailabilitySearchRQ availabilitySearchReq, TrackInfoDTO trackInfo)
			throws ValidationException, ModuleException {

		if (availabilitySearchReq.getJourneyInfo().isEmpty()) {
			throw new ValidationException(ValidationException.Code.NULL_ERROR);
		}

		List<OriginDestinationInfo> journeyInfo = availabilitySearchReq.getJourneyInfo();

		for (OriginDestinationInfo originDestinationDepartureInfo : journeyInfo) {

			if (originDestinationDepartureInfo.getDepartureDateTime() == null
					|| originDestinationDepartureInfo.getDepartureDateTime().before(
							CalendarUtil.addMinutes(CalendarUtil.getStartTimeOfDate(new Date()), -1))) {
				throw new ValidationException(ValidationException.Code.WRONG_DEPARTURE_DATE);
			}

			validateDateVarience(originDestinationDepartureInfo.getDepartureDateTime(),
					originDestinationDepartureInfo.getDepartureVariance(), trackInfo.getAppIndicator());

			validatePassengerInfo(availabilitySearchReq.getTravellerQuantity(), trackInfo.getAppIndicator());

			String origin = originDestinationDepartureInfo.getOrigin();
			String destination = originDestinationDepartureInfo.getDestination();

			if (origin.equals(destination)) {
				throw new ValidationException(ValidationException.Code.EQUAL_ORIGIN_DESTINATION);
			}

			FlightSearchDTO flightSearchDTO = new FlightSearchDTO();
			flightSearchDTO.setFromAirport(originDestinationDepartureInfo.getOrigin());
			flightSearchDTO.setToAirport(originDestinationDepartureInfo.getDestination());
			if (!AddModifySurfaceSegmentValidations.isValidSearchForFreshBooking(flightSearchDTO)) {
				throw new ValidationException(ValidationException.Code.SEARCH_ONLY_BUS_SEGMENT);
			}

		}
		validateAvailabilityPreferences(availabilitySearchReq.getPreferences());
		validateReturnDate(availabilitySearchReq.getJourneyInfo());
		validateMinimumReturnTransitionTime(availabilitySearchReq.getJourneyInfo());

	}

	private void validateAvailabilityPreferences(Preferences preferences) throws ModuleException {

		boolean countryCodeExists = false, currencyCodeExists = false;
		// validate country code
		if (AppSysParamsUtil.isTestStstem() && preferences.getOriginCountryCode() != null
				&& !"undefined".equals(preferences.getOriginCountryCode())) {
			List<CountryDetailsDTO> countryDetailsDTOList = ModuleServiceLocator.getLocationBD().getCountryDetailsList();
			for (CountryDetailsDTO countryDetailsDTO : countryDetailsDTOList) {
				if (preferences.getOriginCountryCode().equals(countryDetailsDTO.getCountryCode())) {
					countryCodeExists = true;
				}
			}
			if (!countryCodeExists) {
				throw new ValidationException(ValidationException.Code.COUNTRY_CODE_INVALIED);
			}
		}
		// validate currency code
		if (preferences.getCurrency() != null) {
			List<Currency> currencyList = (List<Currency>) ModuleServiceLocator.getCommoMasterBD().getActiveCurrencies();
			for (Currency currency : currencyList) {
				if (preferences.getCurrency().equals(currency.getCurrencyCode())) {
					currencyCodeExists = true;
				}
			}
			if (!currencyCodeExists) {
				throw new ValidationException(ValidationException.Code.CURRENCY_CODE_INVALIED);
			}
		}
	}

	private void validateReturnDate(List<OriginDestinationInfo> originDestinationInfos) {
		if (originDestinationInfos.size() == OndSequence.RETURN_OND_SIZE) {
			OriginDestinationInfo outBound = originDestinationInfos.get(OndSequence.OUT_BOUND);
			OriginDestinationInfo inbound = originDestinationInfos.get(OndSequence.IN_BOUND);
			if (outBound.getOrigin().equals(inbound.getDestination()) && outBound.getDestination().equals(inbound.getOrigin())) {
				if ((inbound.getDepartureDateTime().before(CalendarUtil.addMilliSeconds( // when inbound flight search
						CalendarUtil.getStartTimeOfDate(outBound.getDepartureDateTime()), -1)))) {
					throw new ValidationException(ValidationException.Code.INBOUND_DATE_BEFORE_OUTBOUND);
				}
				if (inbound.getSpecificFlights() != null) { // when pricequote
					if ((inbound.getDepartureDateTime().before(outBound.getDepartureDateTime()))) {
						throw new ValidationException(ValidationException.Code.INBOUND_DATE_BEFORE_OUTBOUND);
					}
				}
			}
		}
	}

	private void validateMinimumReturnTransitionTime(List<OriginDestinationInfo> originDestinationInfos) {
		Long minReturnTransitionTime = CalendarUtil.convertHHMMtoMillis(CommonServiceUtil.getGlobalConfig().getBizParam(
				SystemParamKeys.MIN_RETURN_TRANSITION_TIME));
		Long returnTransitionTime = -1l;
		if (originDestinationInfos.size() == OndSequence.RETURN_OND_SIZE) {
			OriginDestinationInfo outBound = originDestinationInfos.get(OndSequence.OUT_BOUND);
			OriginDestinationInfo inbound = originDestinationInfos.get(OndSequence.IN_BOUND);
			if (outBound.getOrigin().equals(inbound.getDestination()) && outBound.getDestination().equals(inbound.getOrigin())
					&& inbound.getSpecificFlights() != null) {
				returnTransitionTime = CalendarUtil.getTimeDifferenceInMillis(outBound.getArrivalDateTime(),
						inbound.getDepartureDateTime());
				if (returnTransitionTime < minReturnTransitionTime) {
					throw new ValidationException(ValidationException.Code.MINIMUM_RETURN_TRANSITION_TIME_ERROR);
				}
			}
		}
	}

	private void validateDateVarience(Date preferredDate, Integer varience, AppIndicatorEnum appIndicator) throws ModuleException {

		int maxVarience = AppSysParamsUtil.getMaxDepartureReturnVarience(appIndicator);

		if (CalendarUtil.add(preferredDate, 0, 0, -3, 0, 0, 0).before(new Date())) {
			maxVarience = 3 + maxVarience;
		}
		if (varience > maxVarience) {
			throw new ValidationException(ValidationException.Code.INVALID_DEPARTURE_VARIENCE);
		}
	}

	private void validatePassengerInfo(TravellerQuantity travellerQty, AppIndicatorEnum appIndicator) {

		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appIndicator) && travellerQty.getAdultCount() == 0) {
			throw new ValidationException(ValidationException.Code.WRONG_PASSENGER_COUNT);
		}

		int totalPaxCount = travellerQty.getAdultCount() + travellerQty.getChildCount();
		int maxAllowedPaxCount = AppSysParamsUtil.getMaxChildAdultCount();

		if (totalPaxCount > maxAllowedPaxCount) {
			throw new ValidationException(ValidationException.Code.WRONG_PASSENGER_COUNT);
		}

		if (travellerQty.getAdultCount() < travellerQty.getInfantCount()) {
			throw new ValidationException(ValidationException.Code.WRONG_PASSENGER_COUNT);
		}
	}

	@Override
	public void validateAncillaryAvailabilityRQ(AncillaryAvailabilityRQ ancillaryAvailabilityRq) throws ValidationException {

		if (ancillaryAvailabilityRq == null || ancillaryAvailabilityRq.getReservationData() == null) {
			throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
		}

		if (ancillaryAvailabilityRq.getReservationData() instanceof BasicReservation) {

			BasicReservation basicReservationInfo = (BasicReservation) ancillaryAvailabilityRq.getReservationData();

			if (basicReservationInfo.getMode().isEmpty() || !(basicReservationInfo.getMode().equals(ReservationData.Mode.CREATE))) {

				throw new ValidationException(ValidationException.Code.WRONG_ANCI_MODE);
			}

			if (basicReservationInfo.getContactInfo() == null || basicReservationInfo.getPassengers() == null) {
				throw new ValidationException(ValidationException.Code.WRONG_ANCI_PASSENGER);
			}

			List<Passenger> passengerList = basicReservationInfo.getPassengers();

			for (Passenger passenger : passengerList) {
				if (passenger.getFirstName() == null
						|| passenger.getFirstName().isEmpty()
						|| passenger.getLastName() == null
						&& passenger.getLastName().isEmpty()
						&& (!passenger.getType().equals(INFANT) && (passenger.getNationality() == null || passenger
								.getNationality().isEmpty()))) {

					throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
				}

				if (passenger.getType().equals(INFANT)
						&& AnciCommon.resolvePassengerTypeForValidation(basicReservationInfo.getPassengers(),
								Integer.toString(passenger.getTravelWith())).equals(ADULT)) {
					throw new ValidationException(ValidationException.Code.WRONG_INFANT_ALLOCATION);
				}
			}
			
			List<INSURANCEPROVIDER> insuranceProviders = AppSysParamsUtil.getInsuranceProvidersList();
			if (!insuranceProviders.get(0).equals(INSURANCEPROVIDER.NONE)) {
				ContactInformation contactInfo = basicReservationInfo.getContactInfo();
				if ((contactInfo.getFirstName() == null || contactInfo.getFirstName().isEmpty())
						|| (contactInfo.getLastName() == null || contactInfo.getLastName().isEmpty())
						|| (contactInfo.getNationality() == null || contactInfo.getNationality().isEmpty())
						|| (contactInfo.getCountry() == null || contactInfo.getCountry().isEmpty())) {

					throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
				}
			}	

			if (ancillaryAvailabilityRq.getAncillaries() == null) {

				throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
			}

			List<Ancillary> anciList = ancillaryAvailabilityRq.getAncillaries();

			for (Ancillary anciItem : anciList) {
				ancillaryTypeCheck(anciItem);
			}

		}

		if (ancillaryAvailabilityRq.getReservationData() instanceof ModifyReservation) {

			ModifyReservation modifyResInfo = (ModifyReservation) ancillaryAvailabilityRq.getReservationData();

			if (modifyResInfo.getMode().isEmpty() || !(modifyResInfo.getMode().equals(ReservationData.Mode.MODIFY))) {

				throw new ValidationException(ValidationException.Code.WRONG_ANCI_MODE);
			}

			if (modifyResInfo.getPnr() == null || modifyResInfo.getPnr().isEmpty()) {
				throw new ValidationException(ValidationException.Code.NO_PNR_FOUND);
			}

		}

	}

	@Override
	public void validateAvailableAncillaryRQ(AvailableAncillaryRQ availableAncillariesRq) throws ValidationException {

		if (availableAncillariesRq == null) {
			throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
		}

		List<Ancillary> anciList = availableAncillariesRq.getAncillaries();
		for (Ancillary anciItem : anciList) {
			ancillaryTypeCheck(anciItem);
		}

	}

	@Override
	public void validateBlockAncillaryRQ(BlockAncillaryRQ blockAncillariesRequest) throws ValidationException {

		if (blockAncillariesRequest == null || blockAncillariesRequest.getAncillaries().isEmpty()) {
			throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
		}

		List<SelectedAncillary> anciList = blockAncillariesRequest.getAncillaries();
		for (SelectedAncillary selectedAnci : anciList) {

			ancillaryTypeCheck(selectedAnci);

			if (selectedAnci.getPreferences().isEmpty()) {
				throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
			}

			// temp for check qty
			String anciType = selectedAnci.getType();

			List<Preference> preferenceList = selectedAnci.getPreferences();

			anclillaryPreferencesValidation(preferenceList, null, anciType);

		}

	}

	@Override
	public void
			validateAllocateAncillaryRQ(BaseAncillarySelectionsRQ baseAncillaryRQ, SessionBasicReservation sessionReserveData)
					throws ValidationException {

		if (baseAncillaryRQ == null) {
			throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
		}
		
		if(sessionReserveData.isPostPaymentExist()){
			throw new ValidationException(ValidationException.Code.TRANSACTION_EXIST,new Throwable(sessionReserveData.getPnr()));
		}
		
		if (!baseAncillaryRQ.getAncillaries().isEmpty()) {

			List<String> passengerRphList = getPassengerRphList(sessionReserveData);

			MetaData metaData = baseAncillaryRQ.getMetaData();

			List<OndUnit> ondPreferncees = metaData.getOndPreferences();
			List<String> ondIdList = new ArrayList<String>();

			for (OndUnit ondUnit : ondPreferncees) {
				ondIdList.add(ondUnit.getOndId());
			}

			List<SelectedAncillary> anciList = baseAncillaryRQ.getAncillaries();
			for (SelectedAncillary selectedAnci : anciList) {

				ancillaryTypeCheck(selectedAnci);
				// temp for check qty
				String anciType = selectedAnci.getType();

				if (selectedAnci.getPreferences().isEmpty()) {
					throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
				}

				List<Preference> preferenceList = selectedAnci.getPreferences();
				passengerValidation(preferenceList, passengerRphList, sessionReserveData);
				anclillaryPreferencesValidation(preferenceList, ondIdList, anciType);

			}
		}
	}

	private List<String> getPassengerRphList(SessionBasicReservation sessionReserveData) {

		List<SessionPassenger> sessinPassengerList = sessionReserveData.getPassengers();
		List<String> passengerRphList = new ArrayList<String>();
		for (SessionPassenger passenger : sessinPassengerList) {
			passengerRphList.add(passenger.getPassengerRph());
		}

		return passengerRphList;
	}

	private void passengerValidation(List<Preference> preferenceList, List<String> passengerRphList,
			SessionBasicReservation sessionReserveData) {

		List<SessionPassenger> sessionPassengerList = sessionReserveData.getPassengers();

		for (SessionPassenger sessionPassenger : sessionPassengerList) {

			if (sessionPassenger.getType().equals(INFANT)) {
				throw new ValidationException(ValidationException.Code.WRONG_ANCI_ALLOCATION);
			}
		}

		// check whether the anci allocation passenger rph list and session rph lists are equal

		for (Preference preferedAnci : preferenceList) {

			if (preferedAnci.getAssignee().getAssignType().equals(Assignee.AssignType.PASSENGER)) {

				PassengerAssignee passenger = (PassengerAssignee) preferedAnci.getAssignee();
				String passengerRphToCheck = passenger.getPassengerRph();

				if (!passengerRphList.contains(passengerRphToCheck)) {
					throw new ValidationException(ValidationException.Code.INVALID_PASSENGER_ALLOCATION);
				}

			}
		}

	}

	private void ancillaryTypeCheck(Ancillary anciToCheck) {

		AncillariesConstants.AncillaryType.resolveAncillaryType(anciToCheck.getType());

	}

	private void anclillaryPreferencesValidation(List<Preference> preferenceList, List<String> ondIdList, String anciType) {

		for (Preference preferedAnci : preferenceList) {

			if (preferedAnci.getAssignee() == null) {
				throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
			}

			if (preferedAnci.getSelections().isEmpty()) {
				throw new ValidationException(ValidationException.Code.NULL_OR_REQUEST_EMPTY);
			}

			List<Selection> selectedAnciPreferences = preferedAnci.getSelections();
			for (Selection anciSelection : selectedAnciPreferences) {

				if (anciSelection.getScope() == null) {

					throw new ValidationException(ValidationException.Code.WRONG_ANCI_SCOPE);
				}

				if (anciSelection.getScope() instanceof OndScope) {
					OndScope ondScope = (OndScope) anciSelection.getScope();
					containsOndId(ondScope.getOndId(), ondIdList);
				}

				if (anciSelection.getSelectedItems() == null || anciSelection.getSelectedItems().isEmpty()) {
					throw new ValidationException(ValidationException.Code.NO_SELECTED_ITEMS);
				}

				List<SelectedItem> selectedItems = anciSelection.getSelectedItems();

				for (SelectedItem anciItem : selectedItems) {
					if (anciItem.getId() == null) {
						throw new ValidationException(ValidationException.Code.EMPTY_ANCI_ID);
					}
					if ((anciType.equals(AncillariesConstants.Type.MEAL))
							&& (anciItem.getQuantity() == null || anciItem.getQuantity().toString().equals("0"))) {
						throw new ValidationException(ValidationException.Code.ZERO_ANCI_QTY);
					}
				}

			}

		}

	}

	private void containsOndId(String ondId, List<String> ondIdList) {
		if (!(ondIdList.contains(ondId))) {
			throw new ValidationException(ValidationException.Code.WRONG_ANCI_OND_ID);
		}

	}

	@Override
	public void validateRequoteRQ(RequoteRQ requoteRQ, Set<LccResSegmentInfo> resSegs, RPHGenerator rphGen,
			Map<String, List<LccResSegmentInfo>> ondWiseResSegMap, LCCClientReservation clientReservation) {

		if ((requoteRQ.isCancelSegment() && requoteRQ.isModifySegment())
				|| (!requoteRQ.isCancelSegment() && !requoteRQ.isModifySegment())) {
			throw new ValidationException(ValidationException.Code.GENERIC_SYSTEM_ERROR);
		}

		if (requoteRQ.isCancelSegment()
				&& (requoteRQ.getModificationInfo().getCnxFlights() == null || requoteRQ.getModificationInfo().getCnxFlights()
						.isEmpty())) {
			throw new ValidationException(ValidationException.Code.GENERIC_SYSTEM_ERROR);
		}

		if (requoteRQ.isModifySegment()
				&& (requoteRQ.getModificationInfo().getModifiedFlights() == null
						|| requoteRQ.getModificationInfo().getModifiedFlights().isEmpty()
						|| requoteRQ.getFlightSegments() == null || requoteRQ.getFlightSegments().isEmpty())) {
			throw new ValidationException(ValidationException.Code.GENERIC_SYSTEM_ERROR);
		}

		for (LccResSegmentInfo segment : resSegs) {
			List<String> unsegmentRPHList = new ArrayList<>();
			if (segment.isUnSegment()) {
				unsegmentRPHList.add(ModificationUtils.createResSegRph(segment.getCarrierCode(), segment.getSegmentSeq()));
			}

			if (requoteRQ.isModifySegment() && !unsegmentRPHList.isEmpty()) {
				throw new ValidationException(ValidationException.Code.UN_SEG_FOUND);
			}
			if (requoteRQ.isCancelSegment() && unsegmentRPHList.size() > 0
					&& !unsegmentRPHList.containsAll(requoteRQ.getModificationInfo().getCnxFlights())) {
				throw new ValidationException(ValidationException.Code.UN_SEG_FOUND);
			}
		}

		if (requoteRQ.isModifySegment()) {
			// Map<String, List<LCCClientReservationSegment>> ondWiseResSegMap =
			// ModificationUtils.getOndWiseSegmentMap(resSegs);
			Map<String, List<String>> ondWiseResSegRPHMap = new HashMap<String, List<String>>();

			for (String key : ondWiseResSegMap.keySet()) {
				List<String> resSegUnqIdList = new ArrayList<String>();
				for (LccResSegmentInfo segment : ondWiseResSegMap.get(key)) {
					resSegUnqIdList.add(ModificationUtils.createResSegRph(segment.getCarrierCode(), segment.getSegmentSeq()));
				}
				ondWiseResSegRPHMap.put(key, resSegUnqIdList);
			}
			validateForModification(requoteRQ, rphGen, ondWiseResSegMap, ondWiseResSegRPHMap);
		}
		
		if(requoteRQ.isCancelSegment()){
			
			String newResOriginCode = "";
			
			String beforeCancelResOriginCode = getFirstSegment(resSegs).getSegmentCode().split("/")[0];
			
			for (String key : ondWiseResSegMap.keySet()) {
				
				List<String> resSegUnqIdList = new ArrayList<String>();
				for (LccResSegmentInfo segment : ondWiseResSegMap.get(key)) {
					resSegUnqIdList.add(ModificationUtils.createResSegRph(segment.getCarrierCode(), segment.getSegmentSeq()));
				}
				
				if(!requoteRQ.getModificationInfo().getCnxFlights().containsAll(resSegUnqIdList)){
					newResOriginCode = ondWiseResSegMap.get(key).get(0).getSegmentCode().split("/")[0];
					break;
				}

			}

			if (beforeCancelResOriginCode != null && !beforeCancelResOriginCode.equals("") && !newResOriginCode.equals("")) {
				if (!ReservationBeanUtil
						.isRequoteValidWithServiceTax(beforeCancelResOriginCode, newResOriginCode, clientReservation.isGroupPNR(),
								!CollectionUtils.isEmpty(clientReservation.getTaxInvoicesList()))) {
					throw new ValidationException(ValidationException.Code.INVALID_CANCEL_SEGMENT_OPERATION);
				}

				CommonReservationContactInfo contactInfo = clientReservation.getContactInfo();
				if (!ReservationBeanUtil
						.isContactInfoValidForRouteModWithServiceTax(newResOriginCode, contactInfo.getCountryCode(),
								contactInfo.getState(), contactInfo.getStreetAddress1(), clientReservation.isGroupPNR())) {
					throw new ValidationException(ValidationException.Code.INSUFFICIENT_CONTACT_INFO_FOR_ROUTE_MODIFICATION);
				}
			}
		}
		
	}

	private void validateForModification(RequoteRQ requoteRQ, RPHGenerator rphGen,
			Map<String, List<LccResSegmentInfo>> ondWiseResSegMap, Map<String, List<String>> ondWiseResSegRPHMap) {

		boolean validModifiedOND;

		for (ModifiedFlight modifiedFlt : requoteRQ.getModificationInfo().getModifiedFlights()) {
			validModifiedOND = false;
			for (String key : ondWiseResSegRPHMap.keySet()) {
				if (ondWiseResSegRPHMap.get(key).containsAll(modifiedFlt.getFromResSegRPH())) {
					validateSameFlightModificattion(ondWiseResSegMap.get(key), modifiedFlt.getToFlightSegRPH(), rphGen);
					validModifiedOND = true;

				}
			}
			if (!validModifiedOND) {
				throw new ValidationException(ValidationException.Code.GENERIC_SYSTEM_ERROR);
			}
		}
	}

	private void validateSameFlightModificattion(List<LccResSegmentInfo> resSegs, List<String> toFlightSegRPHList,
			RPHGenerator rphGen) {

		if (!AppSysParamsUtil.isAllowSameFlightModificationInIBE()) {
			List<String> fltSegUnqIdList = new ArrayList<String>();
			for (LccResSegmentInfo seg : resSegs) {
				fltSegUnqIdList.add(seg.getFlightSegmentRefNumber());
			}

			if (ModificationUtils.isSameFlightModification(fltSegUnqIdList, toFlightSegRPHList, rphGen)) {
				throw new ValidationException(ValidationException.Code.SAME_FLIGHT_MODIFICATION_FOUND);
			}
		}
	}

	@Override
	public void validateRequoteRS(FlightPriceRS flightPriceRS, boolean isModifysegment) {
		if (flightPriceRS == null || flightPriceRS.getSelectedPriceFlightInfo() == null) {
			throw new ValidationException(ValidationException.Code.FLIGHT_SEARCH_FARES_NOT_AVAILABLE);
		}
	}

	@Override
	public void validateAncillary(AncillaryAvailabilityRQ ancillariesAvailabilityRequest) throws ValidationException {

		List<Ancillary> anciList = ancillariesAvailabilityRequest.getAncillaries();
		for (Ancillary anciItem : anciList) {
			ancillaryTypeCheck(anciItem);
		}
	}

	@Override
	public void validateAirportMessageRQ(AirportMessagesRQ airportMessageRQ) throws ValidationException {
		validateAirportMessageStage(airportMessageRQ.getStage());
	}

	private void validateAirportMessageStage(String stage) {
		boolean isValid = false;
		if (stage == null) {
			throw new ValidationException(ValidationException.Code.INVALID_AIRPORT_MESSAGE_STAGE);
		}
		for (CommonConstants.AirportMessageStages existingStage : CommonConstants.AirportMessageStages.values()) {
			if (stage.equalsIgnoreCase(existingStage.getCode())) {
				isValid = true;
				break;
			}
		}
		if (!isValid) {
			throw new ValidationException(ValidationException.Code.INVALID_AIRPORT_MESSAGE_STAGE);
		}
	}

	/** PAYMENT RELATED */

	@Override
	public void validatePaymentMethodsRQ(PaymentMethodsRQ paymentMethodsRQ) {
		ValidationServiceUtils.checkRequest(paymentMethodsRQ);
		ValidationServiceUtils.checkTransactionId(paymentMethodsRQ.getTransactionId());
		if (!(paymentMethodsRQ.getOperationTypes() != null && !StringUtil.isNullOrEmpty(paymentMethodsRQ.getOperationTypes()
				.toString()))) {
			throw new ValidationException(ValidationException.Code.OPERATIONS_TYPE_EMPTY);
		}
		// ValidationServiceUtils.checkOriginCountryCode(paymentMethodsRQ.getOriginCountryCode());
	}

	@Override
	public void validatatePaymentSummaryRQ(PaymentSummaryRQ paymentSummaryRQ) {
		ValidationServiceUtils.checkRequest(paymentSummaryRQ);
		ValidationServiceUtils.checkTransactionId(paymentSummaryRQ.getTransactionId());
		ValidationServiceUtils.checkPaxListNullOrEmptyOrNoAdults(paymentSummaryRQ.getPaxInfo());

	}

	@Override
	public void validateAdminFeeRQ(AdministrationFeeRQ adminFeeRQ) {

		ValidationServiceUtils.checkRequest(adminFeeRQ);
		ValidationServiceUtils.checkTransactionId(adminFeeRQ.getTransactionId());
		ValidationServiceUtils.checkPnr(adminFeeRQ.getPnr());

	}

	@Override
	public void validatePayfortCheckRQ(PayfortCheckRQ payfortCheckRQ) {
		ValidationServiceUtils.checkRequest(payfortCheckRQ);
		ValidationServiceUtils.checkTransactionId(payfortCheckRQ.getTransactionId());
		ValidationServiceUtils.checkPnr(payfortCheckRQ.getPnr());
		if (StringUtil.isNullOrEmpty(payfortCheckRQ.getTransactionFee())) {
			throw new ValidationException(ValidationException.Code.TRANSACTION_FEE_EMPTY);
		}
	}

	@Override
	public void validateBinPromotionRQ(BinPromotionRQ binPromotionRQ) {
		ValidationServiceUtils.checkRequest(binPromotionRQ);
		ValidationServiceUtils.checkTransactionId(binPromotionRQ.getTransactionId());
		// ReservationContact would be eliminated
		// ValidationServiceUtils.checkReservationContact(binPromotionRQ.getReservationContact());
		ValidationServiceUtils.checkPaxListNullOrEmptyOrNoAdults(binPromotionRQ.getPaxInfo());
		if (StringUtil.isNullOrEmpty(binPromotionRQ.getPromoCode())) {
			throw new ValidationException(ValidationException.Code.PROMOCODE_EMPTY);
		}
		// ValidationServiceUtils.checkIPGCardId(binPromotionRQ.getCardType());
		ValidationServiceUtils.checkIPGCardNumber(binPromotionRQ.getCardNo());
		ValidationServiceUtils.checkIPGId(binPromotionRQ.getPaymentGatewayId());
	}

	@Override
	public void validatePaymentOptionsRQ(PaymentOptionsRQ paymentOptionsRQ) {
		ValidationServiceUtils.checkRequest(paymentOptionsRQ);
		ValidationServiceUtils.checkTransactionId(paymentOptionsRQ.getTransactionId());
		ValidationServiceUtils.checkPaxListNullOrEmptyOrNoAdults(paymentOptionsRQ.getPaxInfo());
		// ValidationServiceUtils.checkReservationContact(paymentOptionsRQ.getReservationContact());
		// ValidationServiceUtils.checkOriginCountryCode(paymentOptionsRQ.getOriginCountryCode());
	}

	@Override
	public void validateBalancePaymentOptionsRQ(PaymentOptionsRQ balancePaymentOptionsRQ, String bookingTransactionId) {
		ValidationServiceUtils.checkRequest(balancePaymentOptionsRQ);
		ValidationServiceUtils.checkPnr(balancePaymentOptionsRQ.getPnr());
		// ValidationServiceUtils.checkOriginCountryCode(balancePaymentOptionsRQ.getOriginCountryCode());
		ValidationServiceUtils.checkTransactionId(bookingTransactionId);
	}

	@Override
	public void validateRequotePaymentOptions(PaymentOptionsRQ requotePaymentOptionsRQ) {
		ValidationServiceUtils.checkRequest(requotePaymentOptionsRQ);
		ValidationServiceUtils.checkTransactionId(requotePaymentOptionsRQ.getTransactionId());
		ValidationServiceUtils.checkPnr(requotePaymentOptionsRQ.getPnr());
		// ValidationServiceUtils.checkOriginCountryCode(requotePaymentOptionsRQ.getOriginCountryCode());
	}

	@Override
	public void validateAnciModificationPaymentOptionsRQ(PaymentOptionsRQ anciModificationPaymentOptionsRQ) {
		ValidationServiceUtils.checkRequest(anciModificationPaymentOptionsRQ);
		ValidationServiceUtils.checkTransactionId(anciModificationPaymentOptionsRQ.getTransactionId());
		ValidationServiceUtils.checkPnr(anciModificationPaymentOptionsRQ.getPnr());
		// ValidationServiceUtils.checkOriginCountryCode(anciModificationPaymentOptionsRQ.getOriginCountryCode());
	}

	@Override
	public void validateEffectivePaymentRQ(EffectivePaymentRQ effectivePaymentRQ, CustomerSessionStore customerSessionStore) {
		ValidationServiceUtils.checkRequest(effectivePaymentRQ);
		ValidationServiceUtils.checkTransactionId(effectivePaymentRQ.getTransactionId());
		ValidationServiceUtils.checkLMSUserLogin(customerSessionStore);
		ValidationServiceUtils.checkPaxListNullOrEmptyOrNoAdults(effectivePaymentRQ.getPaxInfo());
		ValidationServiceUtils.checkPaymentOptions(effectivePaymentRQ.getPaymentOptions());
		ValidationServiceUtils.checkLmsOption(effectivePaymentRQ.getPaymentOptions().getLmsCredits());
		ValidationServiceUtils.checkLMSAmountEmptyOrNegative(
				effectivePaymentRQ.getPaymentOptions().getLmsCredits().getRedeemRequestAmount());

	}

	@Override
	public void validateEffectiveBalancePaymentRQ(EffectivePaymentRQ effectiveBalancePaymentRQ,
			CustomerSessionStore customerSessionStore) {
		ValidationServiceUtils.checkRequest(effectiveBalancePaymentRQ);
		ValidationServiceUtils.checkTransactionId(effectiveBalancePaymentRQ.getTransactionId());
		ValidationServiceUtils.checkLMSUserLogin(customerSessionStore);
		ValidationServiceUtils.checkPnr(effectiveBalancePaymentRQ.getPnr());
		ValidationServiceUtils.checkPaymentOptions(effectiveBalancePaymentRQ.getPaymentOptions());
		ValidationServiceUtils.checkLmsOption(effectiveBalancePaymentRQ.getPaymentOptions().getLmsCredits());
		ValidationServiceUtils.checkLMSAmountEmptyOrNegative(effectiveBalancePaymentRQ.getPaymentOptions().getLmsCredits()
				.getRedeemRequestAmount());
	}

	@Override
	public void validateRequoteEffectivePaymentRQ(EffectivePaymentRQ effectiverequotePaymentRQ,
			CustomerSessionStore customerSessionStore) {
		ValidationServiceUtils.checkRequest(effectiverequotePaymentRQ);
		ValidationServiceUtils.checkLMSUserLogin(customerSessionStore);
		ValidationServiceUtils.checkTransactionId(effectiverequotePaymentRQ.getTransactionId());
		ValidationServiceUtils.checkPnr(effectiverequotePaymentRQ.getPnr());
		ValidationServiceUtils.checkPaymentOptions(effectiverequotePaymentRQ.getPaymentOptions());
		ValidationServiceUtils.checkLmsOption(effectiverequotePaymentRQ.getPaymentOptions().getLmsCredits());
		ValidationServiceUtils.checkLMSAmountEmptyOrNegative(effectiverequotePaymentRQ.getPaymentOptions().getLmsCredits()
				.getRedeemRequestAmount());

	}

	@Override
	public void validateEffectiveAnciModificationRQ(EffectivePaymentRQ effectiveAnciModificationPaymentRQ,
			CustomerSessionStore customerSessionStore) {
		ValidationServiceUtils.checkRequest(effectiveAnciModificationPaymentRQ);
		ValidationServiceUtils.checkTransactionId(effectiveAnciModificationPaymentRQ.getTransactionId());
		ValidationServiceUtils.checkLMSUserLogin(customerSessionStore);
		ValidationServiceUtils.checkPnr(effectiveAnciModificationPaymentRQ.getPnr());
		ValidationServiceUtils.checkPaymentOptions(effectiveAnciModificationPaymentRQ.getPaymentOptions());
		ValidationServiceUtils.checkLmsOption(effectiveAnciModificationPaymentRQ.getPaymentOptions().getLmsCredits());
		ValidationServiceUtils.checkLMSAmountEmptyOrNegative(effectiveAnciModificationPaymentRQ.getPaymentOptions()
				.getLmsCredits().getRedeemRequestAmount());
	}

	@Override
	public void preValidateCommonPaymentRQ(MakePaymentRQ makePaymentRQ) {
		ValidationServiceUtils.checkRequest(makePaymentRQ);
		ValidationServiceUtils.checkTransactionId(makePaymentRQ.getTransactionId());
	}

	@Override
	public void validateCreateBookingPaymentRQ(MakePaymentRQ makePaymentRQ, PaymentBaseSessionStore paymentBaseSessionStore)
			throws ModuleException {
		ValidationServiceUtils.checkPaymentBaseSession(paymentBaseSessionStore);
		if (BookingType.ONHOLD != makePaymentRQ.getBookingType()) {
			ValidationServiceUtils.commonPaymentValidation(makePaymentRQ, paymentBaseSessionStore);
		}
	}

	@Override
	public void validateProcessBalancePayment(MakePaymentRQ balancePaymentRQ, PaymentBaseSessionStore paymentSessionStore)
			throws ModuleException {
		ValidationServiceUtils.checkPnr(balancePaymentRQ.getPnr());
		ValidationServiceUtils.checkPaymentBaseSession(paymentSessionStore);
		ValidationServiceUtils.commonPaymentValidation(balancePaymentRQ, paymentSessionStore);

	}

	@Override
	public void validateProcessRequotePayment(MakePaymentRQ requotePaymentRQ, PaymentBaseSessionStore paymentSessionStore)
			throws ModuleException {
		ValidationServiceUtils.checkPnr(requotePaymentRQ.getPnr());
		ValidationServiceUtils.checkPaymentBaseSession(paymentSessionStore);
		ValidationServiceUtils.commonPaymentValidation(requotePaymentRQ, paymentSessionStore);

	}

	@Override
	public void validateProcessAnciModificationPayment(MakePaymentRQ anciModificationPaymentRQ,
			PaymentBaseSessionStore paymentSessionStore) throws ModuleException {
		ValidationServiceUtils.checkPnr(anciModificationPaymentRQ.getPnr());
		ValidationServiceUtils.checkPaymentBaseSession(paymentSessionStore);
		ValidationServiceUtils.commonPaymentValidation(anciModificationPaymentRQ, paymentSessionStore);

	}

	@Override
	public void ffidAddEditValidate(FFIDUpdateRQ ffidUpdateRQ, LCCClientReservation reservation,
			Map<Integer, NameDTO> nameChangeMap)
			throws ValidationException, ModuleException {

		if (!reservation.getPNR().equals(ffidUpdateRQ.getPnr())) {
			throw new ValidationException(ValidationException.Code.INVALID_FFID_CHANGE_FOR_PNR);
		}

		if (ffidUpdateRQ.getFfidChangePaxList().isEmpty()) {
			throw new ValidationException(ValidationException.Code.NO_FFID_CHANGE);
		}

		UpdateValidateFFIDAdaptor updateValidateFFIDAdaptor = new UpdateValidateFFIDAdaptor(reservation, nameChangeMap);
		ValidatePaxFFIDsRQ validatePaxFFIDsRQ = updateValidateFFIDAdaptor.adapt(ffidUpdateRQ.getFfidChangePaxList());
		passengerService.validateLMSPax(validatePaxFFIDsRQ);
				
	}

	@Override
	public void validateReqouteFlightSearchRQ(AvailabilitySearchRQ availabilitySearchRq, LCCClientReservation reservation)
			throws ValidationException {
		
		List<LCCClientReservationSegment> sortedConfirmedSegList = getSortedConfirmedSegmentsList(reservation.getSegments());
		
		// There can be 2 separate segments but it is served by a connection booking class
		// So for that case remove inbound segment of the connection segs
		List<LCCClientReservationSegment> confirmedSegsWithoutConnectionInboundSegs = removeConnIBFltSegList(sortedConfirmedSegList);
		
		Date modifiedDepartureDate = convertDate(availabilitySearchRq.getJourneyInfo().get(0).getDepartureDateTime());

		OndLocationDTO oldOrigin = getOldOrigin(confirmedSegsWithoutConnectionInboundSegs);
		OndLocationDTO newOrigin = getNewOrigin(availabilitySearchRq, confirmedSegsWithoutConnectionInboundSegs,
				modifiedDepartureDate);

		
		
		boolean isModifiedSegmentFirstSegment = isModifiedSegmentFirstSegment(availabilitySearchRq, modifiedDepartureDate,
				confirmedSegsWithoutConnectionInboundSegs);

		if (isModifiedSegmentFirstSegment && !oldOrigin.equals(newOrigin)) {
			if (!ReservationBeanUtil
					.isRequoteValidWithServiceTax(oldOrigin.getOndLocationCode(), newOrigin, reservation.isGroupPNR(),
							!CollectionUtils.isEmpty(reservation.getTaxInvoicesList()))) {
				throw new ValidationException(ValidationException.Code.INVALID_ROUTE_MODIFICATION);
			}

			CommonReservationContactInfo contactInfo = reservation.getContactInfo();
			if (!ReservationBeanUtil
					.isContactInfoValidForRouteModWithServiceTax(newOrigin, contactInfo.getCountryCode(), contactInfo.getState(),
							contactInfo.getStreetAddress1(), reservation.isGroupPNR())) {
				throw new ValidationException(ValidationException.Code.INSUFFICIENT_CONTACT_INFO_FOR_ROUTE_MODIFICATION);
			}
		}
	}
	
	private List<LCCClientReservationSegment> removeConnIBFltSegList(List<LCCClientReservationSegment> sortedConfirmedSegList){
		List<LCCClientReservationSegment> removedConnIBFltSegList = new ArrayList<>();
		int journeySequence = -1;
		for (LCCClientReservationSegment seg: sortedConfirmedSegList){
			if (journeySequence == -1){
				journeySequence = seg.getJourneySequence();
				removedConnIBFltSegList.add(seg);
			} else {
				if (journeySequence == seg.getJourneySequence()){
					continue;
				} else {
					removedConnIBFltSegList.add(seg);
				}
			}
		}
		return removedConnIBFltSegList;
	}

	private boolean isModifiedSegmentFirstSegment(AvailabilitySearchRQ availabilitySearchRq, Date modifiedFlightDepartureDate,
			List<LCCClientReservationSegment> confirmedSegList) {
		
		boolean isModifiedSegmentFirstSegment = false;
		int selectedFlightSegSeq = Integer.parseInt(availabilitySearchRq.getSelectedFlightSegmentSequence());
		Date previousFirstSegmentDepartureDate = convertDate(confirmedSegList.get(0).getDepartureDate());
		
		if (modifiedFlightDepartureDate.before(previousFirstSegmentDepartureDate)
				|| modifiedFlightDepartureDate.equals(previousFirstSegmentDepartureDate)
				|| isSegSeqFirstConfirmedSeg(selectedFlightSegSeq, confirmedSegList)) {
			isModifiedSegmentFirstSegment = true;
		}
		
		return isModifiedSegmentFirstSegment;
	}
	
	private boolean isSegSeqFirstConfirmedSeg(int segmentSequence, List<LCCClientReservationSegment> confirmedSegList) {
		
		boolean isSegSeqFirstConfirmedSeg = false;
		
		if (confirmedSegList.get(0).getSegmentSeq().equals(segmentSequence)) {
			isSegSeqFirstConfirmedSeg = true;
		}
		
		return isSegSeqFirstConfirmedSeg;
	}

	private OndLocationDTO getNewOrigin(AvailabilitySearchRQ availabilitySearchRq,
			List<LCCClientReservationSegment> confirmedSegList, Date modifiedDepartureDate) {
		
		OndLocationDTO newOrigin = null;
		
		if (!confirmedSegList.isEmpty() && confirmedSegList.size() > 1 && confirmedSegList.get(1) != null) {
			Date nextFirstSegmentDepartureDate = convertDate(confirmedSegList.get(1).getDepartureDate());
			if ((modifiedDepartureDate.after(nextFirstSegmentDepartureDate) || modifiedDepartureDate
					.equals(nextFirstSegmentDepartureDate))) {
				newOrigin = new OndLocationDTO(confirmedSegList.get(1).getSegmentCode().split("/")[0], false);
			} else {
				newOrigin = new OndLocationDTO(availabilitySearchRq.getJourneyInfo().get(0).getOrigin(),
						availabilitySearchRq.getJourneyInfo().get(0).isOriginCity());
			}
		} else {
			newOrigin = new OndLocationDTO(availabilitySearchRq.getJourneyInfo().get(0).getOrigin(),
					availabilitySearchRq.getJourneyInfo().get(0).isOriginCity());
		}
		
		return newOrigin;
	}
	
	private OndLocationDTO getOldOrigin(List<LCCClientReservationSegment> sortedConfirmedSegments) {
		OndLocationDTO oldOriginLocation = new OndLocationDTO(sortedConfirmedSegments.get(0).getSegmentCode().split("/")[0],
				false);
		return oldOriginLocation;
		
	}
	
	private Date convertDate(Date date){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dateWithoutTime = null;
		try {
			dateWithoutTime = sdf.parse(sdf.format(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			log.error(e);
		}
		return dateWithoutTime;
		
	}
	
	private LccResSegmentInfo getFirstSegment(Set<LccResSegmentInfo> resSegs){
		
		List<LccResSegmentInfo> segments = new ArrayList<>();
		segments.addAll(resSegs);
		
		Collections.sort(segments, new Comparator<LccResSegmentInfo>() {

			@Override
			public int compare(LccResSegmentInfo seg1, LccResSegmentInfo seg2) {
				return seg1.getSegmentSeq().compareTo(seg2.getSegmentSeq());
			}
			
		});
		
		return segments.get(0);
		
	} 
	
	private List<LCCClientReservationSegment> getSortedConfirmedSegmentsList(Set<LCCClientReservationSegment> resSegs){
		
		List<LCCClientReservationSegment> confirmedSegments = new ArrayList<>();
		for (LCCClientReservationSegment seg: resSegs){
			if(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(seg.getStatus())){
				confirmedSegments.add(seg);
			}
		}
		
		Collections.sort(confirmedSegments, new Comparator<LCCClientReservationSegment>() {

			@Override
			public int compare(LCCClientReservationSegment seg1, LCCClientReservationSegment seg2) {
				return seg1.getSegmentSeq().compareTo(seg2.getSegmentSeq());
			}
			
		});

		return confirmedSegments;
		
	} 
	
	@Override
	public void validateRequoteRQForModification(Set<LccResSegmentInfo> resSegs, FlightPriceRQ flightPriceRQ,
			LCCClientReservation reservation) {
		LccResSegmentInfo firstConfirmedSegment = getFirstSegment(resSegs);
		String prevOrigin = firstConfirmedSegment.getSegmentCode().split("/")[0];

		if (!CollectionUtils.isEmpty(flightPriceRQ.getOrderedOriginDestinationInformationList()) && !ReservationBeanUtil
				.isRequoteValidWithServiceTax(prevOrigin,
						flightPriceRQ.getOrderedOriginDestinationInformationList().get(0).getOrigin(), reservation.isGroupPNR(),
						!CollectionUtils.isEmpty(reservation.getTaxInvoicesList()))) {
			throw new ValidationException(ValidationException.Code.INVALID_ROUTE_MODIFICATION);
		}
	}

	public void validateCreateAliasMakePaymentRQ(MakePaymentRQ makePaymentRQ, String customerId) throws ModuleException {
		ValidationServiceUtils.checkRequest(makePaymentRQ);
		ValidationServiceUtils.checkCustomerId(customerId);
		ValidationServiceUtils.commonPaymentValidation(makePaymentRQ, null);
		PaymentGateway selectedPaymentGateway = makePaymentRQ.getPaymentOptions().getPaymentGateways().get(0);
		ValidationServiceUtils.checkAlasPaymentAmount(selectedPaymentGateway);
	}
	
}
