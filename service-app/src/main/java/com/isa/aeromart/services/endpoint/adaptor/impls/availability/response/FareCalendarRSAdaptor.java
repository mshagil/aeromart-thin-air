package com.isa.aeromart.services.endpoint.adaptor.impls.availability.response;

import static com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.CommonRSAdaptor.getFareClasses;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.availability.FareCalendarRS;
import com.isa.aeromart.services.endpoint.dto.availability.FlightCalendarRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;

public class FareCalendarRSAdaptor implements Adaptor<FlightAvailRS, FareCalendarRS> {

	private AvailabilitySearchRQ availabilitySearchReq;
	private RPHGenerator rphGenerator;

	public FareCalendarRSAdaptor(AvailabilitySearchRQ availabilitySearchReq, RPHGenerator rphGenerator) {
		this.availabilitySearchReq = availabilitySearchReq;
		this.rphGenerator = rphGenerator;
	}

	@Override
	public FareCalendarRS adapt(FlightAvailRS source) {
		FareCalendarRS response = new FareCalendarRS();
		if (source != null) {
			CalendarRSAdaptor flightResponseAdaptor = CalendarRSAdaptor.getAdaptorForFareResponse(rphGenerator);
			FlightCalendarRS flightCalResponse = flightResponseAdaptor.adapt(source);
			response = new FareCalendarRS(flightCalResponse);
			response.setCurrency(availabilitySearchReq.getPreferences().getCurrency());
			response.setFareClasses(getFareClasses(source));
			response.setSuccess(true);
		}
		return response;
	}
}