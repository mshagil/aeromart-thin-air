package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

import java.util.List;

public class InsurancePlansGroup {

    private List<String> itemIds;

    private int minSelections;

    private int maxSelections;

    public List<String> getItemIds() {
        return itemIds;
    }

    public void setItemIds(List<String> itemIds) {
        this.itemIds = itemIds;
    }

    public int getMinSelections() {
        return minSelections;
    }

    public void setMinSelections(int minSelections) {
        this.minSelections = minSelections;
    }

    public int getMaxSelections() {
        return maxSelections;
    }

    public void setMaxSelections(int maxSelections) {
        this.maxSelections = maxSelections;
    }
}
