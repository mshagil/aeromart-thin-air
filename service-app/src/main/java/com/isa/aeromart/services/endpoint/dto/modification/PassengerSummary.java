package com.isa.aeromart.services.endpoint.dto.modification;

public class PassengerSummary {

	private PassengerInfo paxInfo;
	
	private CommonSummaryInfo paxCharges;

	public PassengerInfo getPaxInfo() {
		return paxInfo;
	}

	public void setPaxInfo(PassengerInfo paxInfo) {
		this.paxInfo = paxInfo;
	}

	public CommonSummaryInfo getPaxCharges() {
		return paxCharges;
	}

	public void setPaxCharges(CommonSummaryInfo paxCharges) {
		this.paxCharges = paxCharges;
	}
	
	
}
