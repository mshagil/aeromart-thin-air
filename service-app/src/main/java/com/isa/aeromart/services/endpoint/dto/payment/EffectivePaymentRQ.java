package com.isa.aeromart.services.endpoint.dto.payment;

public class EffectivePaymentRQ extends PaymentOptionsRQ {

	private PaymentOptions paymentOptions;

	public PaymentOptions getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(PaymentOptions paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

}
