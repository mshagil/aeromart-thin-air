package com.isa.aeromart.services.endpoint.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.HttpUtil;

public class LogFilter implements Filter {
	private static Log log = LogFactory.getLog(LogFilter.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (globalConfig.isLogIBEAccessInfo()) {
			try {

				HttpServletRequest httpReq = (HttpServletRequest) request;
				String requestURI = PlatformUtiltiies.nullHandler(httpReq.getRequestURI());

				if (isLogAccessIncluded(PlatformUtiltiies.nullHandler(requestURI))) {
					StringBuilder logHeaderMesgBuilder = new StringBuilder();
					logHeaderMesgBuilder.append("DOMAIN=<").append(HttpUtil.getHTTPHeaderValue(httpReq, "HOST")).append(">,");
					logHeaderMesgBuilder.append("SESSION_ID=<").append(httpReq.getRequestedSessionId()).append(">,");
					logHeaderMesgBuilder.append("REQUEST_URL=<").append(requestURI).append(">,");
					logHeaderMesgBuilder.append("IP=<").append(HttpUtil.getHTTPHeaderValue(httpReq, "X-Forwarded-For")).append(">,");
					logHeaderMesgBuilder.append("REMOTE_ADDRESS=<").append(httpReq.getRemoteAddr()).append(">,");
					logHeaderMesgBuilder.append("USER_AGENT=<").append(HttpUtil.getHTTPHeaderValue(httpReq, "User-Agent")).append(">,");
					logHeaderMesgBuilder.append("REFERER=<").append(HttpUtil.getHTTPHeaderValue(httpReq, "Referer")).append(">");
					if (globalConfig.isLogIBEReqParams() && isLogRequestPostData(requestURI)) {
						HttpRequestWrapper requestWrapper = new HttpRequestWrapper(httpReq);
						logHeaderMesgBuilder.append(",PAY_LOAD=<").append(requestWrapper.getPayload()).append(">");
						request = requestWrapper;
					}
					log.info(logHeaderMesgBuilder.toString());

				}
			} catch (Exception ex) {
				log.error("Error capturing access info", ex);
			}
		}
		chain.doFilter(request, response);
	}

	private boolean isLogRequestPostData(String requestURI) {
		if (globalConfig.getPostPayloadDumpURLKeywords() != null) {
			for (String keyword : globalConfig.getPostPayloadDumpURLKeywords()) {
				if (requestURI.indexOf(keyword) > -1) {
					return true;
				}
			}
		}

		return false;
	}

	private boolean isLogAccessIncluded(String requestURI) {
		if (requestURI.indexOf("service-app/controller") > -1) {
			return true;
		}
		return false;

	}

	@Override
	public void destroy() {

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
