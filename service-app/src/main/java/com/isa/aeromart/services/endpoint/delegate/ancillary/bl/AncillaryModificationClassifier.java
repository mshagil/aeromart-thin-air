package com.isa.aeromart.services.endpoint.delegate.ancillary.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryPassenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Assignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationAncillarySelection;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.AirportScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.OndScope;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySelection;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionMetaData;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AncillaryModificationClassifier {

	public enum Classification {
		TO_REMOVE,
		NO_MODIFICATION,
		TO_ADD
	}
	
	public PriceQuoteAncillaryRS getRemovedAncillaryPriceQuotation(ReservationAncillarySelection reservationAncillarySelection) {
		
		AncillaryPassenger passenger, removedAncillaryPassenger;
		PassengerAssignee PassengerAssignee;
		PricedSelectedItem removedPricedSelectedItem;
		List<PricedSelectedItem> removedPricedSelectedItemList;
		AncillaryScope removedAncillaryScope;
		PricedAncillaryType removePricedAncillaryType = null;
		
		PriceQuoteAncillaryRS priceQuoteAncillaryRS = new PriceQuoteAncillaryRS();
		priceQuoteAncillaryRS.setAncillaryReservation(new AncillaryReservation());
		priceQuoteAncillaryRS.getAncillaryReservation().setPassengers(new ArrayList<AncillaryPassenger>());
		priceQuoteAncillaryRS.setMetaData(new MetaData());
		
		AncillarySelection removeAncillary = getPassengerAncillarySelection(Classification.TO_REMOVE);
		priceQuoteAncillaryRS.getMetaData().setOndPreferences(removeAncillary.getMetaData().getOndUnits());
		
		ancillaryPassengerMap = new HashMap<String, AncillaryPassenger>();
		
		if (removeAncillary != null && removeAncillary.getAncillaryPreferences() != null
				&& removeAncillary.getAncillaryPreferences().size() > 0) {
			for (SelectedAncillary selectedAncillary : removeAncillary.getAncillaryPreferences()) {
				String anciType = selectedAncillary.getType();
				for (Preference preference : selectedAncillary.getPreferences()) {
					// removed ancillaries definitely has an AncillaryPassenger
					PassengerAssignee = (PassengerAssignee) preference.getAssignee();
					passenger = getAncillaryPassenger(reservationAncillarySelection.getAncillaryReservation().getPassengers(),
							PassengerAssignee.getPassengerRph());
					removedAncillaryPassenger = resolveAncillaryPassenger(PassengerAssignee.getPassengerRph());
					for (PricedAncillaryType pricedAncillaryType : passenger.getAncillaryTypes()) {
						removePricedAncillaryType = new PricedAncillaryType();
						removePricedAncillaryType.setAncillaryType(anciType);
						removePricedAncillaryType.setAncillaryScopes(new ArrayList<AncillaryScope>());
						if (pricedAncillaryType.getAncillaryType().equals(selectedAncillary.getType())) {
							removedAncillaryScope = null;
							for (Selection selection : preference.getSelections()) {
								for (AncillaryScope ancillaryScope : pricedAncillaryType.getAncillaryScopes()) {
									if (selection.getScope().equals(ancillaryScope.getScope())) {
										removedAncillaryScope = new AncillaryScope();
										removedAncillaryScope.setScope(ancillaryScope.getScope());
										removedPricedSelectedItemList = new ArrayList<PricedSelectedItem>();
										for (SelectedItem selectedItem : selection.getSelectedItems()) {
											for (PricedSelectedItem pricedSelectedItem : ancillaryScope.getAncillaries()) {
												if (selectedItem.getId() != null && selectedItem.getId().equals(pricedSelectedItem.getId())) {
													removedPricedSelectedItem = new PricedSelectedItem();
													removedPricedSelectedItem.setId(selectedItem.getId());
													removedPricedSelectedItem.setName(selectedItem.getName());
													removedPricedSelectedItem.setQuantity(selectedItem.getQuantity());
													removedPricedSelectedItem.setAmount(pricedSelectedItem.getAmount());
													removedPricedSelectedItemList.add(removedPricedSelectedItem);
												}
											}
										}
										if (removedPricedSelectedItemList.size() > 0) {
											removedAncillaryScope.setAncillaries(removedPricedSelectedItemList);
										}
									}
								}
							}
							if (removedAncillaryScope != null) {
								removePricedAncillaryType.getAncillaryScopes().add(removedAncillaryScope);
							}
						}
						if (removePricedAncillaryType != null && removePricedAncillaryType.getAncillaryScopes().size() > 0) {
							removedAncillaryPassenger.getAncillaryTypes().add(removePricedAncillaryType);
							ancillaryPassengerMap.put(PassengerAssignee.getPassengerRph(), removedAncillaryPassenger);
						}
					}
				}
			}
		}
		for(AncillaryPassenger ancillaryPassenger : ancillaryPassengerMap.values()){
			if(ancillaryPassenger.getAncillaryTypes().size() > 0){
				priceQuoteAncillaryRS.getAncillaryReservation().getPassengers().add(ancillaryPassenger);
			}
		}

		return priceQuoteAncillaryRS;
	}

	public AncillarySelection getPassengerAncillarySelection(Classification classification) {
		AncillarySelection selection = new AncillarySelection();
		AncillarySelection reservationAncillarySelection = getAncillarySelectionFromReservationAncillarySelection(previousSelection);
		if(classification.equals(Classification.TO_ADD)){
			selection = filteredAncillarySelection(newSelection, reservationAncillarySelection);
		} else if(classification.equals(Classification.TO_REMOVE)){
			selection = filteredAncillarySelection(reservationAncillarySelection, newSelection);
		}
		return selection;
	}

	private ReservationAncillarySelection previousSelection;

	private AncillarySelection newSelection;
	
	private Map<String , SelectedAncillary> selectedAncillaryMap;
	
	private Map<String , AncillaryPassenger> ancillaryPassengerMap;

	public void setPreviousSelection(ReservationAncillarySelection previousSelection) {
		this.previousSelection = previousSelection;
	}

	public void setNewSelection(AncillarySelection newSelection) {
		this.newSelection = newSelection;
	}
	
	private AncillaryPassenger getAncillaryPassenger(List<AncillaryPassenger> passengers, String passengetRPH){
		for(AncillaryPassenger passenger : passengers){
			if(passenger.getPassengerRph().equals(passengetRPH)){
				return passenger;
			}
		}
		return null;
	}
	
	private AncillarySelection getAncillarySelectionFromReservationAncillarySelection(ReservationAncillarySelection previousSelection){
		
		Preference preference;
		PassengerAssignee passengerAssignee;
		SelectedAncillary selectedAncillary;
		Selection selection;
		AncillarySelection ancillarySelection = new AncillarySelection();
		ancillarySelection.setAncillaryPreferences(new ArrayList<SelectedAncillary>());
		ancillarySelection.setMetaData(new SessionMetaData());
		
		AncillaryReservation ancillaryReservation = previousSelection.getAncillaryReservation();
		MetaData metaData = previousSelection.getMetaData();
		selectedAncillaryMap = new HashMap<String, SelectedAncillary>();
		
		for(AncillaryPassenger ancillaryPassenger : ancillaryReservation.getPassengers()){
			for(PricedAncillaryType pricedAncillaryType : ancillaryPassenger.getAncillaryTypes()){
				if(pricedAncillaryType.getAncillaryScopes().size() > 0){
					preference = new Preference();
					passengerAssignee = new PassengerAssignee();
					passengerAssignee.setAssignType(Assignee.AssignType.PASSENGER);
					passengerAssignee.setPassengerRph(ancillaryPassenger.getPassengerRph());
					preference.setAssignee(passengerAssignee);
					preference.setSelections(new ArrayList<Selection>());
					selectedAncillary = resolveSelectedAncillary(pricedAncillaryType.getAncillaryType());
					for(AncillaryScope ancillaryScope : pricedAncillaryType.getAncillaryScopes()){
						selection = new Selection();
						selection.setScope(ancillaryScope.getScope());
						selection.setSelectedItems(new ArrayList<SelectedItem>());
						selection.getSelectedItems().addAll(ancillaryScope.getAncillaries());
						preference.getSelections().add(selection);
					}
					selectedAncillary.getPreferences().add(preference);
					selectedAncillaryMap.put(pricedAncillaryType.getAncillaryType(), selectedAncillary);
				}
			}
		}
		for(SelectedAncillary selectedAncillaryRef : selectedAncillaryMap.values()){
			ancillarySelection.getAncillaryPreferences().add(selectedAncillaryRef);
		}
		ancillarySelection.getMetaData().setOndUnits(metaData.getOndPreferences());
		return ancillarySelection;
	}

	private SelectedAncillary resolveSelectedAncillary(String anciType){
		SelectedAncillary selectedAncillary;
		
		if(!selectedAncillaryMap.containsKey(anciType)){
			selectedAncillary = new SelectedAncillary();
			selectedAncillary.setType(anciType);
			selectedAncillary.setPreferences(new ArrayList<Preference>());
			selectedAncillaryMap.put(anciType, selectedAncillary);
		}
		return selectedAncillaryMap.get(anciType);
	}

	private AncillaryPassenger resolveAncillaryPassenger(String passengerRPH){
		AncillaryPassenger ancillaryPassenger;
		
		if(!ancillaryPassengerMap.containsKey(passengerRPH)){
			ancillaryPassenger = new AncillaryPassenger();
			ancillaryPassenger.setPassengerRph(passengerRPH);
			ancillaryPassenger.setAncillaryTypes(new ArrayList<PricedAncillaryType>());
			ancillaryPassenger.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			ancillaryPassengerMap.put(passengerRPH, ancillaryPassenger);
		}
		return ancillaryPassengerMap.get(passengerRPH);
	}
	
	private AncillarySelection filteredAncillarySelection(AncillarySelection component, AncillarySelection base){
		AncillarySelection ancillarySelection = new AncillarySelection();
		ancillarySelection.setMetaData(component.getMetaData());
		List<SelectedAncillary> ancillaryPreferences = new ArrayList<SelectedAncillary>();
		SelectedAncillary selectedAncillary;
		SelectedAncillary modifiedSelectedAncillary;
		SelectedAncillary baseSelectedAncillary;
		Preference preference;
		Preference modifiedPreference;
		Preference basePreference;
		Selection selection;
		Selection modifiedSelection;
		Selection baseSelect;
		SelectedItem selectedItem;
		List<SelectedItem> modifiedSelectedItem;
		Map<String, SelectedItem> modifiedSelectedItemMap;
		
		for(SelectedAncillary componentSelection : component.getAncillaryPreferences()){
			selectedAncillary = null;
			baseSelectedAncillary = null;
			modifiedSelectedAncillary = new SelectedAncillary();
			modifiedSelectedAncillary.setPreferences(new ArrayList<Preference>());
			for(SelectedAncillary baseSelection : base.getAncillaryPreferences()){
				if(componentSelection.getType().equals(baseSelection.getType())){
					selectedAncillary = componentSelection;
					modifiedSelectedAncillary.setType(componentSelection.getType());
					baseSelectedAncillary = baseSelection;
					break;
				}
			}
			if(selectedAncillary != null){
				for(Preference componentPreference : selectedAncillary.getPreferences()){
					modifiedPreference = new Preference();
					modifiedPreference.setSelections(new ArrayList<Selection>());
					preference = null;
					basePreference = null;
					for(Preference basePreferenceRef : baseSelectedAncillary.getPreferences()){
						if(componentPreference.getAssignee() instanceof PassengerAssignee && basePreferenceRef.getAssignee() instanceof PassengerAssignee){
							PassengerAssignee componentPassengerAssignee = (PassengerAssignee) componentPreference.getAssignee();
							PassengerAssignee basePassengerAssignee = (PassengerAssignee) basePreferenceRef.getAssignee();
							if(componentPassengerAssignee.getAssignType().equals(basePassengerAssignee.getAssignType()) && componentPassengerAssignee.getPassengerRph().equals(basePassengerAssignee.getPassengerRph())){
								preference = componentPreference;
								basePreference = basePreferenceRef;
								modifiedPreference.setAssignee(componentPassengerAssignee);
								break;
							}
						}
					}
					if(preference != null){
						for(Selection componentSelectionRef : preference.getSelections()){
							modifiedSelection = new Selection();
							modifiedSelection.setSelectedItems(new ArrayList<SelectedItem>());
							selection = null;
							baseSelect = null;
							for(Selection baseSelection : basePreference.getSelections()){
								if(componentSelectionRef.getScope() instanceof SegmentScope && baseSelection.getScope() instanceof SegmentScope){
									SegmentScope componentScope = (SegmentScope) componentSelectionRef.getScope();
									SegmentScope baseScope = (SegmentScope) baseSelection.getScope();
									if(componentScope.getScopeType().equals(baseScope.getScopeType()) && componentScope.getFlightSegmentRPH().equals(baseScope.getFlightSegmentRPH())){
										selection = componentSelectionRef;
										baseSelect = baseSelection;
										modifiedSelection.setScope(componentScope);
										break;
									}
								} else if(componentSelectionRef.getScope() instanceof AirportScope && baseSelection.getScope() instanceof AirportScope){
									AirportScope componentScope = (AirportScope) componentSelectionRef.getScope();
									AirportScope baseScope = (AirportScope) baseSelection.getScope();
									if(componentScope.getScopeType().equals(baseScope.getScopeType()) && componentScope.getFlightSegmentRPH().equals(baseScope.getFlightSegmentRPH()) && componentScope.getAirportCode().equals(baseScope.getAirportCode())){
										selection = componentSelectionRef;
										baseSelect = baseSelection;
										modifiedSelection.setScope(componentScope);
										break;
									}
								} else if(componentSelectionRef.getScope() instanceof OndScope && baseSelection.getScope() instanceof OndScope){
									OndScope componentScope = (OndScope) componentSelectionRef.getScope();
									OndScope baseScope = (OndScope) baseSelection.getScope();
									if(componentScope.getScopeType().equals(baseScope.getScopeType()) && componentScope.getOndId().equals(baseScope.getOndId())){
										selection = componentSelectionRef;
										baseSelect = baseSelection;
										modifiedSelection.setScope(componentScope);
										break;
									}
								}
							}
							if(selection != null){
								for(SelectedItem componentselectedItem : selection.getSelectedItems()){
									modifiedSelectedItemMap = new HashMap<String, SelectedItem>();
									modifiedSelectedItem = new ArrayList<SelectedItem>();
									for(SelectedItem baseSelectedItemRef : baseSelect.getSelectedItems()){
										if(componentselectedItem.getId().equals(baseSelectedItemRef.getId())){
											if(componentselectedItem.getQuantity() > baseSelectedItemRef.getQuantity()){
												selectedItem = new SelectedItem();
												selectedItem.setId(componentselectedItem.getId());
												selectedItem.setName(componentselectedItem.getName());
												selectedItem.setQuantity(componentselectedItem.getQuantity() - baseSelectedItemRef.getQuantity());
												modifiedSelectedItem.add(selectedItem);
											}
										} else {
											if(!modifiedSelectedItemMap.containsKey(componentselectedItem.getId())){
												modifiedSelectedItemMap.put(componentselectedItem.getId(), componentselectedItem);
												modifiedSelectedItem.add(componentselectedItem);
											}
										}
									}
									if(modifiedSelectedItem.size() > 0){
										modifiedSelection.getSelectedItems().addAll(modifiedSelectedItem);
									}
								}
							} else {
								modifiedPreference.getSelections().add(componentSelectionRef);
							}
							if(modifiedSelection != null && modifiedSelection.getSelectedItems().size() > 0){
								modifiedPreference.getSelections().add(modifiedSelection);
							}
						}
					} else {
						modifiedSelectedAncillary.getPreferences().add(componentPreference);
					}
					if(modifiedPreference != null && modifiedPreference.getSelections().size() > 0){
						modifiedSelectedAncillary.getPreferences().add(modifiedPreference);
					}
				}
			} else {
				ancillaryPreferences.add(componentSelection);
			}
			if(modifiedSelectedAncillary != null && modifiedSelectedAncillary.getPreferences().size() > 0){
				ancillaryPreferences.add(modifiedSelectedAncillary);
			}
		}
		
		if(ancillaryPreferences.size() > 0){
			ancillarySelection.setAncillaryPreferences(ancillaryPreferences);
		}
		
		return ancillarySelection;
	}

}
