package com.isa.aeromart.services.endpoint.dto.common;

import java.math.BigDecimal;

public class TaxInfo {

	private String taxType;
	private BigDecimal amount;

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
