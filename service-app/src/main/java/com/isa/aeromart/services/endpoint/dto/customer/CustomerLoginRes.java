package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class CustomerLoginRes extends BaseRS {

	private boolean loginSuccess = false;

	private boolean socialLogin = false;

	private LoggedInCustomer customer;

	private String totalCustomerCredit;

	private LMSDetails lmsDetails;

	public LoggedInCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(LoggedInCustomer customer) {
		this.customer = customer;
	}

	public boolean isLoginSuccess() {
		return loginSuccess;
	}

	public void setLoginSuccess(boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}

	public boolean isSocialLogin() {
		return socialLogin;
	}

	public void setSocialLogin(boolean socialLogin) {
		this.socialLogin = socialLogin;
	}

	public String getTotalCustomerCredit() {
		return totalCustomerCredit;
	}

	public void setTotalCustomerCredit(String totalCustomerCredit) {
		this.totalCustomerCredit = totalCustomerCredit;
	}

	public LMSDetails getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LMSDetails lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

}
