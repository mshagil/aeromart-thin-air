package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class ReservationDetailsGridDataRQ extends TransactionalBaseRQ {
	
	private String language;
	
	private String type;

    private String pnr;

    private String alertId;
    
    private String pnrSegmentId;
    
    private String oldFlightSegmentId;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getAlertId() {
		return alertId;
	}

	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}

	public String getPnrSegmentId() {
		return pnrSegmentId;
	}

	public void setPnrSegmentId(String pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

	public String getOldFlightSegmentId() {
		return oldFlightSegmentId;
	}

	public void setOldFlightSegmentId(String oldFlightSegmentId) {
		this.oldFlightSegmentId = oldFlightSegmentId;
	}

}
