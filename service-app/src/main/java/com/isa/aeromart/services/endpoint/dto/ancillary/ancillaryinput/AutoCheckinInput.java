package com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryInput;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class AutoCheckinInput extends AncillaryInput {

	private String email;

	private String seatCode;

	public AutoCheckinInput() {
		setAncillaryType(AncillariesConstants.Type.AUTOMATIC_CHECKIN);
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the seatCode
	 */
	public String getSeatCode() {
		return seatCode;
	}

	/**
	 * @param seatCode
	 *            the seatCode to set
	 */
	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}
}
