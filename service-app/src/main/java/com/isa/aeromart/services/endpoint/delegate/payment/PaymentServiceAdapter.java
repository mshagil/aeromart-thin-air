package com.isa.aeromart.services.endpoint.delegate.payment;

import com.google.common.base.Splitter;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.IPGTransactionRSAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationContactRQAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationContactResAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ServiceTaxRQAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.customer.CustomerAliasAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.AnciPassengerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.RequoteConfirmAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.PassengerResAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.ReservationPaxAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.payment.CardAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.payment.PaymentGatewayAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.adaptor.utils.PaybyVoucherInfoAdapter;
import com.isa.aeromart.services.endpoint.adaptor.utils.VoucherDTOAdapter;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.BookingType;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.OperationTypes;
import com.isa.aeromart.services.endpoint.delegate.ancillary.AncillaryService;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataService;
import com.isa.aeromart.services.endpoint.delegate.masterdata.MasterDataServiceAdaptor;
import com.isa.aeromart.services.endpoint.delegate.modification.ModificationService;
import com.isa.aeromart.services.endpoint.dto.BuildGatewaysDTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.availability.OndBookingClassInfo;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRS.PaymentStatus;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRQ;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationValidateRes;
import com.isa.aeromart.services.endpoint.dto.common.ClientCommonInfoDTO;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.PromoSummary;
import com.isa.aeromart.services.endpoint.dto.common.ReservationChargeDetail;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.AdditionalDetails;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRQ;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRS;
import com.isa.aeromart.services.endpoint.dto.payment.AmountObject;
import com.isa.aeromart.services.endpoint.dto.payment.BinPromotionDetails;
import com.isa.aeromart.services.endpoint.dto.payment.BinPromotionRQ;
import com.isa.aeromart.services.endpoint.dto.payment.Card;
import com.isa.aeromart.services.endpoint.dto.payment.Cash;
import com.isa.aeromart.services.endpoint.dto.payment.ComposePaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.ComposeRequotePaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.EffectivePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.ExternalPGDetails;
import com.isa.aeromart.services.endpoint.dto.payment.IPGHandlerRQ;
import com.isa.aeromart.services.endpoint.dto.payment.LmsCredits;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRS;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRS.VoucherStatus;
import com.isa.aeromart.services.endpoint.dto.payment.PayfortCheckRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PayfortCheckRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentGateway;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptions;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRS;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.ReservationChargesRQ;
import com.isa.aeromart.services.endpoint.dto.payment.ReservationChargesRS;
import com.isa.aeromart.services.endpoint.dto.payment.SearchParamsForPromoCheck;
import com.isa.aeromart.services.endpoint.dto.payment.ServiceTaxRQ;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherOption;
import com.isa.aeromart.services.endpoint.dto.session.AncillaryTransaction;
import com.isa.aeromart.services.endpoint.dto.session.BinPromoDiscountInfo;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.RequoteTransaction;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.TotalPaymentInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.ReservationChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.GiftVoucherSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentAncillarySessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore.LoyaltyPaymentOption;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.voucher.GiftVoucherDTO;
import com.isa.aeromart.services.endpoint.dto.voucher.IssueVoucherRQ;
import com.isa.aeromart.services.endpoint.dto.voucher.VoucherRedeemRQ;
import com.isa.aeromart.services.endpoint.dto.voucher.VoucherRedeemRS;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.service.ValidationService;
import com.isa.aeromart.services.endpoint.session.TransactionFactory;
import com.isa.aeromart.services.endpoint.utils.booking.BookingUtil;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.aeromart.services.endpoint.utils.customer.CustomerUtil;
import com.isa.aeromart.services.endpoint.utils.payment.ChargeDetailsBuilder;
import com.isa.aeromart.services.endpoint.utils.payment.PaymentUtils;
import com.isa.thinair.aircustomer.api.constants.CustomAliasConstants;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.utils.CustomerAliasGenerator;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.dto.RedeemCalculateRQ;
import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.PaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.service.AirproxyVoucherBD;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TemporyPaymentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.LmsBlockedCredit;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.airreservation.api.utils.GiftVoucherUtils;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.paymentbroker.api.constants.PaymentAPIConsts;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.CountryPaymentCardBehaviourDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelSegmentDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD.PaymentBrokerProperties.REQUEST_METHODS;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.PaymentEmailUtil;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.tapOffline.TapOfflineResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.Voucher;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.commons.MapGenerator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Service("paymentService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaymentServiceAdapter implements PaymentService {

	private static Log log = LogFactory.getLog(PaymentServiceAdapter.class);
	private PaymentUtils paymentUtils = new PaymentUtils();

	@Autowired
	ValidationService validationService;

	@Autowired
	private ConfirmPaymentServiceAdapter confirmPaymentService;

	@Autowired
	private AncillaryService ancillaryService;

	@Autowired
	private TransactionFactory transactionFactory;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private ModificationService modificationService;

	@Autowired
	private MessageSource errorMessageSource;
	private AppIndicatorEnum appIndicator;
	private BigDecimal serviceTaxForAdminFee = AccelAeroCalculator.getDefaultBigDecimalZero();

	private static final String PATTERN1 = "yyMM";

	@Override
	public PaymentMethodsRS getPaymentMethodsRS(PaymentMethodsRQ paymentMethodsRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validatePaymentMethodsRQ(paymentMethodsRQ);

		PaymentBaseSessionStore paymentBaseSessionStore = getPaymentBaseSessionStore(paymentMethodsRQ.getTransactionId());

		setOriginCountryCodeToSessionForTestSystem(paymentMethodsRQ.getOriginCountryCode(), paymentBaseSessionStore);

		String countryCode = getCountryCode(trackInfoDTO.getIpAddress(), paymentBaseSessionStore.getOriginCountryCodeForTest());

		boolean isAllowOnHold = paymentUtils.isCabinClassAllowOnHold(paymentBaseSessionStore.getFareSegChargeTOForPayment());

		boolean isOffLineEnabled = false;

		if (isAllowOnHold) {

			isOffLineEnabled = isOfflinePaymentEnable(paymentMethodsRQ.getOperationTypes());
		}

		PaymentMethodDetailsDTO paymentMethodDetailsDTO = (new PaymentMethodDetailsDTO())
				.setFirstSegmentONDCode(paymentMethodsRQ.getFirstSegmentONDCode()).setCountryCode(countryCode)
				.setEnableOffline(isOffLineEnabled);

		Set<String> paymentMethods = ModuleServiceLocator.getPaymentBrokerBD().getPaymentMethodNames(paymentMethodDetailsDTO);

		// TODO additional conditions to be checked
		if (isAllowOnHold && AppSysParamsUtil.isOnHoldEnable(OnHold.IBE)
				&& checkOnholdable(paymentBaseSessionStore, trackInfoDTO)) {
			if (paymentMethods == null) {
				paymentMethods = new HashSet<String>();
			}
			paymentMethods.add(PaymentConsts.CASH);
		}

		return adaptPaymentMethodsResponse(paymentMethodsRQ, paymentMethods);
	}

	public AdministrationFeeRS getAdminFeeRS(BigDecimal amount, AdministrationFeeRQ adminFeeRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		setAppIndicatorEnum(trackInfoDTO);
		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(adminFeeRQ.getTransactionId());
		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		AdministrationFeeRS adminFeeRS = new AdministrationFeeRS();
		composeAdminFeeRS(amount, amount, paymentSessionStore, adminFeeRS, false, adminFeeRQ.getTransactionId(), false,
				adminFeeRQ, false, trackInfoDTO);

		return adminFeeRS;
	}

	@Override
	public PaymentSummaryRS getPaymentSummaryRS(PaymentSummaryRQ paymentSummaryRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validatatePaymentSummaryRQ(paymentSummaryRQ);

		setAppIndicatorEnum(trackInfoDTO);

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(paymentSummaryRQ.getTransactionId());
		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		// Removing Service Tax from PaymentSessionStore in payment external charge
		paymentSessionStore.removePaymentPaxCharge(EXTERNAL_CHARGES.SERVICE_TAX);

		PaymentSummaryRS paymentSummaryRS = new PaymentSummaryRS();
		paymentSummaryRS.setTransactionId(paymentSummaryRQ.getTransactionId());
		paymentSummaryRS.setCurrency(AppSysParamsUtil.getBaseCurrency());

		paymentSessionStore.addAvailabilityFlexiChargesToFinancialStore(paymentSummaryRQ.getPaxInfo());
		TotalPaymentInfo totalPaymentInfo = paymentSessionStore.getTotalPaymentInformation();

		double totalAnciCharges = totalPaymentInfo.getTotalAncillaryCharges().doubleValue();
		BigDecimal totalWithAnciCharges = totalPaymentInfo.getTotalWithAncillaryCharges();

		PromoSummary promoSummary = null;
		BigDecimal totalAllInclusive = totalWithAnciCharges;
		BigDecimal amountForAdminFeeCalculation = null;

		if (isAdminFeeRegulatedAndAppliedForChannel()) {
			amountForAdminFeeCalculation = totalWithAnciCharges;
		}
		DiscountedFareDetails discountedFareDetails = null;
		if (!paymentSummaryRQ.isOnhold()) {

			ReservationDiscountDTO reservationDiscountDTO = null;

			// adminFee not applied for BIN promotion as it's calculated after AdminFee Calculation
			if (paymentSummaryRQ.isBinPromo() && paymentSessionStore.getBinPromoDiscountInfo() != null
					&& paymentSessionStore.getBinPromoDiscountInfo().getDiscountedFareDetails() != null
					&& paymentSessionStore.getBinPromoDiscountInfo().getReservationDiscountDTO() != null) {

				BinPromoDiscountInfo binPromoDiscountInfo = paymentSessionStore.getBinPromoDiscountInfo();
				discountedFareDetails = binPromoDiscountInfo.getDiscountedFareDetails();
				reservationDiscountDTO = binPromoDiscountInfo.getReservationDiscountDTO();

				if (PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY.equals(discountedFareDetails.getDiscountAs())) {
					totalAllInclusive = AccelAeroCalculator.add(totalWithAnciCharges,
							binPromoDiscountInfo.getDiscountAmount().negate());
					if (isAdminFeeRegulatedAndAppliedForChannel()) {
						amountForAdminFeeCalculation = totalAllInclusive;
					}
				}

			} else if (paymentSessionStore.getDiscountedFareDetails() != null) {
				discountedFareDetails = paymentSessionStore.getDiscountedFareDetails();
				reservationDiscountDTO = buildReservationDiscountDTO(paymentSummaryRQ.getPaxInfo(), paymentSessionStore,
						trackInfoDTO, discountedFareDetails, paymentSummaryRQ.getTransactionId());

				totalAllInclusive = calculateAndApplyPromotionsIfAny(totalWithAnciCharges, paymentSummaryRQ.getPaxInfo(),
						paymentSessionStore, paymentSummaryRQ.getTransactionId(), trackInfoDTO);

				if (isAdminFeeRegulatedAndAppliedForChannel()) {
					amountForAdminFeeCalculation = totalAllInclusive;
				}

			}

			if (discountedFareDetails != null && reservationDiscountDTO != null) {
				promoSummary = buildPromoInfo(discountedFareDetails, reservationDiscountDTO);
			}
		}

		if (paymentSummaryRQ.isBinPromo() && isAdminFeeRegulated()) {

			// BIN promotion can happen only after the initial setting
			if (!paymentSessionStore.isAdminFeeInitiated()) {
				throw new ModuleException("payment.api.administration.fee.regulation.error");
			}

			amountForAdminFeeCalculation = calculateAndApplyPromotionsIfAny(totalWithAnciCharges, paymentSummaryRQ.getPaxInfo(),
					paymentSessionStore, paymentSummaryRQ.getTransactionId(), trackInfoDTO);
		}

		// remove admin fee related charges from store
		paymentSessionStore.removePaymentCharge(EXTERNAL_CHARGES.CREDIT_CARD);
		paymentSessionStore.removePaymentCharge(EXTERNAL_CHARGES.JN_OTHER);

		applyServiceTax(paymentSummaryRQ.getTransactionId(), paymentSummaryRQ, trackInfoDTO);

		// Including applied service taxes when calculating adminstration fee
		if (isAdminFeeRegulatedAndAppliedForChannel()) {
			totalPaymentInfo = paymentSessionStore.getTotalPaymentInformation();
			if (AccelAeroCalculator.isGreaterThan(totalPaymentInfo.getTotalServiceTaxAmount(),
					AccelAeroCalculator.getDefaultBigDecimalZero())) {
				amountForAdminFeeCalculation = AccelAeroCalculator.add(amountForAdminFeeCalculation,
						totalPaymentInfo.getTotalServiceTaxAmount());
				totalAllInclusive = AccelAeroCalculator.add(totalAllInclusive, totalPaymentInfo.getTotalServiceTaxAmount());
			}
		}

		composeAdminFeeRS(amountForAdminFeeCalculation, totalAllInclusive, paymentSessionStore, paymentSummaryRS, false,
				paymentSummaryRQ.getTransactionId(), false, paymentSummaryRQ, trackInfoDTO);
		
		if (paymentSessionStore.getVoucherInformation() != null) {
			//user has gone to anci page after redeem the voucher in the payment page
			paymentSessionStore.getVoucherInformation().setIsTotalAmountPaidFromVoucher(false);
		}

		totalPaymentInfo = paymentSessionStore.getTotalPaymentInformation();

		paymentSummaryRS.setTotalAncillary(totalAnciCharges);
		paymentSummaryRS.setTaxAndSurcharges(totalPaymentInfo.getTotalCharges());
		paymentSummaryRS.setPaxWiseTotalFare(totalPaymentInfo.getPaxWiseTotalList());
		paymentSummaryRS.setPromoSummary(promoSummary);
		paymentSummaryRS.setReservationChargeDetails(composeReservationChargeDetails(paymentSessionStore));
		paymentSummaryRS.setSuccess(true);

		if (isAdminFeeRegulated()) {
			paymentSummaryRS.setTotalServiceTaxes(
					AccelAeroCalculator.parseBigDecimal(totalPaymentInfo.getTotalServiceTaxAmount().doubleValue()).doubleValue());
		} else {
			paymentSummaryRS.setTotalServiceTaxes(
					AccelAeroCalculator.parseBigDecimal(totalPaymentInfo.getTotalServiceTaxAmount().doubleValue()).doubleValue());
			paymentSummaryRS.setTotalAllInclusive(
					AccelAeroCalculator.add(totalAllInclusive, totalPaymentInfo.getTotalServiceTaxAmount()).doubleValue());
		}

		if (paymentSummaryRS.getTotalServiceTaxes() > 0) {
			paymentSummaryRS
					.setAdministrationFee(AccelAeroCalculator
							.subtract(AccelAeroCalculator.parseBigDecimal(paymentSummaryRS.getAdministrationFee()),
									AccelAeroCalculator.parseBigDecimal(paymentSummaryRS.getServiceTaxForAdminFee()))
							.doubleValue());
		}

		return paymentSummaryRS;
	}

	@Override
	public PayfortCheckRS checkPayfort(PayfortCheckRQ payfortCheckRQ, TrackInfoDTO trackInfoDto) {

		validationService.validatePayfortCheckRQ(payfortCheckRQ);
		PayfortCheckRS payfortCheckRS = buildPayfortCheckRS(payfortCheckRQ.getPnr(), payfortCheckRQ.getTransactionFee(),
				trackInfoDto);
		payfortCheckRS.setTransactionId(payfortCheckRQ.getTransactionId());
		return payfortCheckRS;
	}

	@Override
	public AdministrationFeeRS getBalanceAdminFee(AdministrationFeeRQ adminFeeRQ, TrackInfoDTO trackInfoDTO, String transactionId)
			throws ModuleException {

		validationService.validateAdminFeeRQ(adminFeeRQ);

		setAppIndicatorEnum(trackInfoDTO);

		getPaymentBaseSessionStore(transactionId).storeOperationType(OperationTypes.MAKE_ONLY);

		String operatingCarrier = "";
		String sellingCarrier = trackInfoDTO.getCarrierCode();
		String pnr = adminFeeRQ.getPnr();

		boolean isGroupPnr = deriveIsGroupPNR(trackInfoDTO, pnr);

		boolean isRegisteredUser = isRegisteredUser();

		LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPnr, isRegisteredUser, operatingCarrier,
				sellingCarrier, trackInfoDTO);

		if (reservation == null) {

			throw new ModuleException("payment.api.balance.payment.pnr.invalid");
		}

		AdministrationFeeRS adminFeeRS = new AdministrationFeeRS();

		composeAdminFeeRS(null, reservation.getTotalAvailableBalance(), getPaymentBaseSessionStore(transactionId), adminFeeRS,
				true, transactionId, false, adminFeeRQ, trackInfoDTO);

		return adminFeeRS;
	}

	@Override
	public AdministrationFeeRS getRequoteAdminFee(AdministrationFeeRQ adminFeeRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validateAdminFeeRQ(adminFeeRQ);

		setAppIndicatorEnum(trackInfoDTO);

		PaymentRequoteSessionStore paymentRequoteSessionStore = getRequotePaymentSessionStore(adminFeeRQ.getTransactionId());
		ReservationInfo resInfo = paymentRequoteSessionStore.getResInfo();

		paymentRequoteSessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		BigDecimal balanceToPay = paymentRequoteSessionStore.getBalanceToPayAmount();
		AdministrationFeeRS adminFeeRS = new AdministrationFeeRS();

		// In requote flow we already have service tax RQ info, therefore no need to get it from front end RQ's
		ServiceTaxRQ serviceTaxRq = new ServiceTaxRQ();
		serviceTaxRq.setCountryCode(resInfo.getContactInfo().getCountryCode());
		serviceTaxRq.setStateCode(resInfo.getContactInfo().getState());
		serviceTaxRq.setTransactionId(adminFeeRQ.getTransactionId());
		serviceTaxRq.setTaxRegNo(resInfo.getContactInfo().getTaxRegNo());
		paymentRequoteSessionStore.removePaymentPaxCharge(EXTERNAL_CHARGES.SERVICE_TAX);
		composeAdminFeeRS(balanceToPay, balanceToPay, getPaymentBaseSessionStore(adminFeeRQ.getTransactionId()), adminFeeRS,
				false, adminFeeRQ.getTransactionId(), false, serviceTaxRq, trackInfoDTO);

		return adminFeeRS;
	}

	@Override
	public AdministrationFeeRS getAnciModificationAdminFee(AdministrationFeeRQ adminFeeRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		validationService.validateAdminFeeRQ(adminFeeRQ);

		setAppIndicatorEnum(trackInfoDTO);

		PaymentBaseSessionStore paymentBaseSessionStore = getPaymentBaseSessionStore(adminFeeRQ.getTransactionId());

		paymentBaseSessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		AdministrationFeeRS adminFeeRS = new AdministrationFeeRS();

		AncillaryIntegrateTO ancillaryIntegrateTO = ancillaryService.getAncillaryIntegrateTO(trackInfoDTO, adminFeeRQ);

		BigDecimal balanceToPay;
		try {
			balanceToPay = ancillaryService.getPrePaymentTO(trackInfoDTO, adminFeeRQ).getBalanceDue();
		} catch (Exception e) {
			log.error("payment.api.anci.modification.amount.empty", e);
			throw new ModuleException("payment.api.anci.modification.amount.empty");

		}

		BigDecimal serviceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		composeAdminFeeRS(balanceToPay, balanceToPay, paymentBaseSessionStore, adminFeeRS, false, adminFeeRQ.getTransactionId(),
				false, adminFeeRQ, trackInfoDTO);

		for (AncillaryIntegratePassengerTO anciPassenger : ancillaryIntegrateTO.getReservation().getPassengers()) {
			for (LCCClientExternalChgDTO externalChg : anciPassenger.getExternalCharges()) {
				if (externalChg.getExternalCharges().equals(ReservationInternalConstants.EXTERNAL_CHARGES.SERVICE_TAX)) {
					serviceTaxAmount = AccelAeroCalculator.add(serviceTaxAmount, externalChg.getAmount());
				}
			}
		}

		// BigDecimal serviceTaxAmount = applyServiceTax(adminFeeRQ.getTransactionId(), adminFeeRQ, trackInfoDTO);

		adminFeeRS.setTotalServiceTaxes(serviceTaxAmount.doubleValue());
		adminFeeRS.setServiceTaxForAdminFee(paymentBaseSessionStore.getServiceTaxForTransactionFee().doubleValue());

		return adminFeeRS;
	}

	@Override
	public PaymentOptionsRS getBinPromotionRS(BinPromotionRQ binPromotionRQ, TrackInfoDTO trackInfoDTO) throws ModuleException {
		setAppIndicatorEnum(trackInfoDTO);

		validationService.validateBinPromotionRQ(binPromotionRQ);

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(binPromotionRQ.getTransactionId());

		PaymentOptionsRS paymentOptionsRS = new PaymentOptionsRS();
		paymentOptionsRS.setTransactionId(binPromotionRQ.getTransactionId());
		paymentOptionsRS.setHasPromoOption(AppSysParamsUtil.isPromoCodeEnabled());
		BinPromotionDetails binPromotionDetails = new BinPromotionDetails();

		// for BIN promotion (maintain it separately in the session)
		DiscountedFareDetails binDiscountedFareDetails = new DiscountedFareDetails();

		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		if (!isAdminFeeRegulated()) {
			paymentSessionStore.removePaymentCharge(EXTERNAL_CHARGES.CREDIT_CARD);
			paymentSessionStore.removePaymentCharge(EXTERNAL_CHARGES.JN_OTHER);
		}

		int paymentGatewayId = binPromotionRQ.getPaymentGatewayId();
		String cardNumber = binPromotionRQ.getCardNo();
		int cardType = binPromotionRQ.getCardType();
		// remove BIN-promotion in the session if previous [cardNumber + PG] combo doesn't hold
		if (paymentSessionStore.getBinPromoDiscountInfo() != null) {
			if (paymentGatewayId != paymentSessionStore.getBinPromoDiscountInfo().getPaymentGatewayId()
					|| !cardNumber.contentEquals(paymentSessionStore.getBinPromoDiscountInfo().getCardNumber())) {
				removeBinPromotion(paymentSessionStore);
			}
		}

		PreferenceInfo preferenceInfo = paymentSessionStore.getPreferencesForPayment();
		String searchSystemStr = preferenceInfo.getSelectedSystem();
		TravellerQuantity travellerQuantity = paymentSessionStore.getTravellerQuantityForPayment();
		String returnDate = getReturnDate(paymentSessionStore.getSelectedSegmentsForPayment());

		SearchParamsForPromoCheck searchParamsForPromoCheck = buildSearchParams(
				Integer.valueOf(binPromotionRQ.getCardNo().substring(0, 6)), returnDate, searchSystemStr, travellerQuantity);

		OndBookingClassInfo bookingClassAllocInfo = paymentUtils
				.getOndBookingClassInfo(paymentSessionStore.getFareSegChargeTOForPayment());
		List<FlightSegmentTO> flightSegments = paymentUtils
				.adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());
		SYSTEM searchSystem = SYSTEM.getEnum(searchSystemStr);
		String preferredLanguage = getPreferredLanguageFromLocale();

		PromoSelectionCriteria promoSelectionCriteria = buildPromotionCriteria(flightSegments,
				bookingClassAllocInfo.getBookingClasses(), bookingClassAllocInfo.getOndCodes(),
				bookingClassAllocInfo.getLogicalCabinClasses(), binPromotionRQ.getPromoCode(), searchParamsForPromoCheck,
				preferredLanguage, trackInfoDTO);

		AirproxyReservationQueryBD airproxySearchAndQuoteBD = ModuleServiceLocator.getAirproxySearchAndQuoteBD();

		// get applicable promotion
		ApplicablePromotionDetailsTO applicablePromotionDetailsTO = airproxySearchAndQuoteBD
				.pickApplicablePromotions(promoSelectionCriteria, searchSystem, trackInfoDTO);

		// get existing discountedFareDetails of BIN and other promotions
		DiscountedFareDetails existingDiscountedFareDetails = paymentSessionStore.getDiscountedFareDetails();
		DiscountedFareDetails existingBinDiscountedFareDetails = null;
		BigDecimal existingBinPromotionDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paymentSessionStore.getBinPromoDiscountInfo() != null) {
			existingBinDiscountedFareDetails = paymentSessionStore.getBinPromoDiscountInfo().getDiscountedFareDetails();
			existingBinPromotionDiscount = paymentSessionStore.getBinPromoDiscountInfo().getDiscountAmount();
		}

		boolean hasExistingPromotion = false;
		boolean hasExistingPromotionWithSamePromoCode = false;

		// applicable promotion exists for the given code and is a DISCOUNT (not BUYNGET nor FREE SERVICE)
		if (applicablePromotionDetailsTO != null && applicablePromotionDetailsTO.getPromoType()
				.equals(PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT)) {

			long promoId = applicablePromotionDetailsTO.getPromoCriteriaId().longValue();
			double existingDiscountValue = 0;

			// TODO separate bin and non-bin promotions as both has different requirements at payment page
			// for instance user could change BIN applied payment option and then return to enter the same code
			// need to calculate and send the information again for BIN even at same code already exists; not
			// necessarily for others
			if (existingDiscountedFareDetails != null || existingBinDiscountedFareDetails != null) {
				hasExistingPromotion = true;
				binPromotionDetails.setHasExistingPromotion(true);

				if ((existingDiscountedFareDetails != null
						&& existingDiscountedFareDetails.getPromotionId().longValue() == promoId)
						|| (existingBinDiscountedFareDetails != null
								&& existingBinDiscountedFareDetails.getPromotionId().longValue() == promoId)) {
					hasExistingPromotionWithSamePromoCode = true;
				}

				if (existingBinDiscountedFareDetails != null) {
					binPromotionDetails.setExistingPromotionAmount(existingBinPromotionDiscount);
					existingDiscountValue = existingBinPromotionDiscount.doubleValue();
				} else if (existingDiscountedFareDetails != null) {
					binPromotionDetails.setExistingPromotionAmount(paymentSessionStore.getDiscountAmount());
					existingDiscountValue = paymentSessionStore.getDiscountAmount().doubleValue();

				}
			}

			/* calculate ReservationDiscountDTO */

			List<PassengerTypeQuantityTO> paxQtyList = paymentUtils
					.getPaxQtyList(paymentSessionStore.getTravellerQuantityForPayment());

			setFareDiscountWith(paxQtyList, applicablePromotionDetailsTO, binDiscountedFareDetails);

			QuotedFareRebuildDTO quotedFareRebuildDTO = getQuotedFareRebuildDTO(paymentSessionStore, binDiscountedFareDetails);

			DiscountRQ discountRQ = buildDiscountRQ(null, binDiscountedFareDetails, quotedFareRebuildDTO, true,
					paymentSessionStore, binPromotionRQ.getTransactionId());

			ReservationDiscountDTO resDiscountDTO = ReservationModuleUtils.getAirproxyReservationBD()
					.calculatePromotionDiscount(discountRQ, trackInfoDTO);

			// Discount Amount
			BigDecimal binDiscountAmount = resDiscountDTO.getTotalDiscount();
			boolean discountAsCredits = DiscountApplyAsTypes.CREDIT.contentEquals(binDiscountedFareDetails.getDiscountAs());
			binPromotionDetails.setDiscountAmount(binDiscountAmount);
			binPromotionDetails.setDiscountAsCredit(discountAsCredits);

			// check whether to set to the Session
			boolean applyPromoAndUpdateSession = false;

			if (hasExistingPromotionWithSamePromoCode
					&& Math.abs(existingDiscountValue - binDiscountAmount.doubleValue()) > 0.01) {

				// not expected to be mismatched
				log.error("Promotion amount mismatch");

				if (binPromotionRQ.isOverrideExistingPromotion()) {
					log.warn("Promotion amount mismatch, ovveriding with the new value");
					applyPromoAndUpdateSession = true;
				}

			} else if (hasExistingPromotionWithSamePromoCode) {
				binPromotionDetails.setAlreadyApplied(true);

			} else if (!hasExistingPromotion || binPromotionRQ.isOverrideExistingPromotion()) {
				applyPromoAndUpdateSession = true;
			}

			if (applyPromoAndUpdateSession) {

				LmsCredits binLmsCredits = null;
				BigDecimal binLmsRedeemedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				// build BinPromotionDiscountInfo
				BinPromoDiscountInfo binPromoDiscountInfo = new BinPromoDiscountInfo();
				binPromoDiscountInfo.setDiscountedFareDetails(binDiscountedFareDetails);
				binPromoDiscountInfo.setCardNumber(cardNumber);
				binPromoDiscountInfo.setCardType(cardType);
				binPromoDiscountInfo.setPaymentGatewayId(paymentGatewayId);
				binPromoDiscountInfo.setDiscountAmount(binDiscountAmount);
				binPromoDiscountInfo.setDiscountAsCredit(discountAsCredits);
				// TODO: remove reservationDiscountDTO from the session (bin-discount and the other one)
				binPromoDiscountInfo.setReservationDiscountDTO(resDiscountDTO);

				// set promotion details to the session (keep BIN separately)
				paymentSessionStore.storeBinPromoDiscountInfo(binPromoDiscountInfo);

				// revert existing LMS redemption
				// build paymentOptions with BIN Promotion
				// also pack existing paymentOptions for roll back purposes
				String contactEmail = binPromotionRQ.getReservationContact().getEmailAddress();
				List<Passenger> paxInfo = binPromotionRQ.getPaxInfo();
				String memberAccountId = getMemberAccountId();
				LoyaltyInformation currentLoyaltyInformation = paymentSessionStore.getLoyaltyInformation();
				BigDecimal totalWithAnciCharges = paymentSessionStore.getTotalPaymentInformation().getTotalWithAncillaryCharges();

				double currentRedeemedAmount = 0.0;
				String lmsStatusMessage = null;

				boolean handleExistingLMSRedemption = false;

				// if LMS redemption exists
				if (!StringUtil.isNullOrEmpty(memberAccountId)) {
					handleExistingLMSRedemption = true;
					if (currentLoyaltyInformation != null && currentLoyaltyInformation.isRedeemLoyaltyPoints()
							&& currentLoyaltyInformation.getTotalRedeemedAmount().doubleValue() > 0) {
						currentRedeemedAmount = currentLoyaltyInformation.getTotalRedeemedAmount().doubleValue();
						revertLoyaltyRedemption(paymentSessionStore, paymentSessionStore.getPnr());
					}

				}

				// build discountRQ with existing promotion (if any) and
				// build existing paymentOptions (as in the initial payment page load)
				discountRQ = buildDiscountRQForLMSRedemption(paxInfo, paymentSessionStore.getDiscountedFareDetails(),
						paymentSessionStore, binPromotionRQ.getTransactionId());

				ComposePaymentDTO paymentDTO = (new ComposePaymentDTO()).setPnr(null).setLmsCredits(null)
						.setPaymentSessionStore(paymentSessionStore).setContactEmail(contactEmail).setPaxInfo(paxInfo)
						.setDiscountRQ(discountRQ).setTrackInfoDTO(trackInfoDTO)
						.setTransactionId(binPromotionRQ.getTransactionId()).setPnr(null);

				paymentOptionsRS = composePaymentOptionsResponse(paymentDTO);

				// TODO: think of removing the LMS adjusting part for simplicity and for the LMS total case
				if (handleExistingLMSRedemption) {
					LoyaltyInformation binPromoLoyaltyInformation = new LoyaltyInformation();
					binPromoLoyaltyInformation.setBinPromotion(true);
					paymentSessionStore.storeLoyaltyInformation(binPromoLoyaltyInformation);

					// get new LMS credits criteria with BIN promotion applied
					DiscountRQ binDiscountRQ = buildDiscountRQForLMSRedemption(paxInfo,
							paymentSessionStore.getBinPromoDiscountInfo().getDiscountedFareDetails(), paymentSessionStore,
							binPromotionRQ.getTransactionId());
					binLmsCredits = getLMSCredits(paymentSessionStore, paxInfo, binDiscountRQ, trackInfoDTO,
							binPromotionRQ.getTransactionId());

					// redeem only if there was an existing redemption
					// and only proceed if it cannot be an LMS total case
					// if LMS total possibility case update LMS unredeemed
					// with new maxRedeemable adjust accordingly

					if (currentRedeemedAmount > 0
							&& (discountAsCredits && currentRedeemedAmount < totalWithAnciCharges.doubleValue())
							|| (!discountAsCredits && currentRedeemedAmount < AccelAeroCalculator
									.add(totalWithAnciCharges, binDiscountAmount.negate()).doubleValue())) {
						// prepare for redemption
						binLmsCredits.setRedeemRequestAmount(BigDecimal.valueOf(currentRedeemedAmount));

						// redeem with BIN promotion
						DiscountRQ binPromoDiscountRQ = buildDiscountRQForLMSRedemption(paxInfo, binDiscountedFareDetails,
								paymentSessionStore, binPromotionRQ.getTransactionId());

						boolean binRedeemStatus = redeemLoyaltyPoints(binLmsCredits, null, paymentSessionStore,
								totalWithAnciCharges, null, paxInfo, binPromoDiscountRQ, trackInfoDTO, null,
								binPromoLoyaltyInformation, binPromotionRQ.getTransactionId());

						// get adjusted redeemedAmount
						if (binRedeemStatus) {
							binLmsRedeemedAmount = binPromoLoyaltyInformation.getTotalRedeemedAmount();
							binLmsCredits.setRedeemedAmount(binLmsRedeemedAmount);
							binLmsCredits.setAirRewardId(memberAccountId);
							binLmsCredits.setAvailablePoints(BigDecimal.valueOf(getLMSPointBalance(memberAccountId)));
							binLmsCredits.setRedeemed(true);

						}

						// message added within the LMS scope
						if (currentRedeemedAmount > 0
								&& Math.abs(currentRedeemedAmount - binLmsRedeemedAmount.doubleValue()) > 0.01) {
							lmsStatusMessage = "Previous LMS redemption " + currentRedeemedAmount + " is adjusted to "
									+ binLmsRedeemedAmount;
						}
					}

				}

				// lmsApplicableAmount
				BigDecimal lmsApplicableAmount = totalWithAnciCharges;
				if (!discountAsCredits) {
					lmsApplicableAmount = AccelAeroCalculator.add(lmsApplicableAmount, binDiscountAmount.negate());
				}
				// effective balance via deducting lms redemption
				BigDecimal effectiveBalance = AccelAeroCalculator.add(lmsApplicableAmount, binLmsRedeemedAmount.negate());
				BigDecimal totalTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

				// calculate transaction fee
				if (isAdminFeeRegulated()) {

					if (isAdminFeeRegulatedAndAppliedForChannel()) {

						paymentSessionStore.removePaymentCharge(EXTERNAL_CHARGES.CREDIT_CARD);
						paymentSessionStore.removePaymentCharge(EXTERNAL_CHARGES.JN_OTHER);

						ExternalChgDTO ccChgDTO = deriveCCChgDTO(paymentSessionStore, 0, lmsApplicableAmount);
						paymentSessionStore.addPaymentCharge(ccChgDTO);
						ExternalChgDTO ccFeeTaxChgDTO = deriveCCFeeChgDTO(paymentSessionStore);
						if (ccFeeTaxChgDTO != null) {
							paymentSessionStore.addPaymentCharge(ccFeeTaxChgDTO);
						}
						paymentSessionStore.setAdminFeeInitiated();
						totalTransactionFee = calculateTransactionFee(ccChgDTO, ccFeeTaxChgDTO);

					}

				} else {

					totalTransactionFee = deriveCCChgsAndCalculateTotalTransactionFee(paymentSessionStore, paymentGatewayId,
							effectiveBalance, null, null);

				}

				// calculate effective payable amount
				BigDecimal effectivePaymentAmount = AccelAeroCalculator.add(effectiveBalance, totalTransactionFee);

				// build BinPromotionDetails
				binPromotionDetails.setPromotionApplied(true);
				binPromotionDetails.setCardNo(cardNumber);
				binPromotionDetails.setCardType(cardType);
				binPromotionDetails.setPaymentGatewayId(paymentGatewayId);
				// BinDiscount already set, passing null to skip
				buildBinPromotionDetails(binPromotionDetails, paymentGatewayId, binLmsCredits, null, lmsApplicableAmount,
						totalTransactionFee, effectivePaymentAmount);

				// add informative messages (for the front-end developer)
				List<String> messages = new ArrayList<String>();

				messages.add(errorMessageSource.getMessage("payment.bin.promotion.lms.total.warning", null,
						LocaleContextHolder.getLocale()));

				if (!StringUtil.isNullOrEmpty(lmsStatusMessage)) {
					messages.add(lmsStatusMessage);
				}

				binPromotionDetails.setMessages(messages);

			}
		} else {

			// if not 'DISCOUNT' promotion available for the promotion code and other criteria
			binPromotionDetails.setNoPromotion(true);
		}

		paymentOptionsRS.setBinPromotionDetails(binPromotionDetails);
		paymentOptionsRS.setSuccess(true);

		return paymentOptionsRS;
	}

	@Override
	public PaymentOptionsRS getPaymentOptionsRS(PaymentOptionsRQ paymentOptionsRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validatePaymentOptionsRQ(paymentOptionsRQ);

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(paymentOptionsRQ.getTransactionId());

		if (StringUtil.isNullOrEmpty(paymentSessionStore.getPnr())) {
			List<FlightSegmentTO> flightSegments = paymentUtils
					.adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());

			String pnr = ModuleServiceLocator.getAirproxyReservationBD().generateNewPnr(flightSegments, trackInfoDTO);
			BookingSessionStore bookingSessionStore = getBookingSessionStore(paymentOptionsRQ.getTransactionId());
			bookingSessionStore.storePnr(pnr);
		}

		setAppIndicatorEnum(trackInfoDTO);

		setOriginCountryCodeToSessionForTestSystem(paymentOptionsRQ.getOriginCountryCode(), paymentSessionStore);

		PaymentOptionsRS paymentOptionsRS;
		BinPromotionDetails binPromotionDetails = null;

		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		paymentSessionStore.addAvailabilityFlexiChargesToFinancialStore(paymentOptionsRQ.getPaxInfo());

		if(!paymentOptionsRQ.isVoucherPaymentInProcess()){
			revertLoyaltyRedemption(paymentSessionStore, paymentSessionStore.getPnr());
		}
		if (!isAdminFeeRegulated()) {
			paymentSessionStore.clearPaymentInformation();
		}

		// differentiate booking, balance case using totalAVailableBalance
		paymentSessionStore.storeTotalAvailableBalance(null);

		String emailAddress = getContactEmailAddress(paymentOptionsRQ.getReservationContact());

		if (paymentOptionsRQ.isBinPromotion()) {
			// as this is paymentOptions, no LMS would be available, could skip LMS adjustment part
			int paymentGatewayId = paymentSessionStore.getBinPromoDiscountInfo().getPaymentGatewayId();

			// get LMS credit criteria with BIN promotion
			DiscountRQ discountRQ = buildDiscountRQForLMSRedemption(paymentOptionsRQ.getPaxInfo(),
					paymentSessionStore.getBinPromoDiscountInfo().getDiscountedFareDetails(), paymentSessionStore,
					paymentOptionsRQ.getTransactionId());

			LmsCredits binPromtionLmsCredits = getLMSCredits(paymentSessionStore, paymentOptionsRQ.getPaxInfo(), discountRQ,
					trackInfoDTO, paymentOptionsRQ.getTransactionId());

			// calculate lmsApplicableAmount (total-promotions) and effectiveBalance (total - BIN discount-lmsPayment)
			BigDecimal totalWithAnciCharges = paymentSessionStore.getTotalPaymentInformation().getTotalWithAncillaryCharges();
			BigDecimal binDiscountAmount = paymentSessionStore.getBinPromoDiscountInfo().getDiscountAmount();
			BigDecimal lmsApplicableAmount = totalWithAnciCharges;

			if (!paymentSessionStore.getBinPromoDiscountInfo().isDiscountAsCredit()) {
				lmsApplicableAmount = AccelAeroCalculator.add(lmsApplicableAmount, binDiscountAmount.negate());
			}
			BigDecimal effectiveBalance = lmsApplicableAmount; // no LMS payment

			BigDecimal totalTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

			// calculate transaction fee
			if (isAdminFeeRegulated()) {
				if (isAdminFeeRegulatedAndAppliedForChannel()) {

					totalTransactionFee = deriveAndValidateAdminFee(paymentSessionStore,
							applyPromotionsIfAny(totalWithAnciCharges, paymentSessionStore));
				}
			} else {

				totalTransactionFee = deriveCCChgsAndCalculateTotalTransactionFee(paymentSessionStore, paymentGatewayId,
						effectiveBalance, null, null);

			}

			// calculate effective payable amount
			BigDecimal effectivePaymentAmount = AccelAeroCalculator.add(effectiveBalance, totalTransactionFee);

			// BuildBinPromotionDetails

			binPromotionDetails = buildBinPromotionDetails(binPromotionDetails, paymentGatewayId, binPromtionLmsCredits,
					binDiscountAmount, lmsApplicableAmount, totalTransactionFee, effectivePaymentAmount);
			binPromotionDetails.setPromotionApplied(true);
			binPromotionDetails.setCardType(paymentSessionStore.getBinPromoDiscountInfo().getCardType());
			binPromotionDetails.setCardNo(paymentSessionStore.getBinPromoDiscountInfo().getCardNumber());
			binPromotionDetails.setPaymentGatewayId(paymentGatewayId);

		} else {
			// revert binPromotion if any
			removeBinPromotion(paymentSessionStore);
		}

		DiscountRQ discountRQ = null;
		if (paymentSessionStore.getDiscountedFareDetails() != null) {
			discountRQ = buildDiscountRQForLMSRedemption(paymentOptionsRQ.getPaxInfo(),
					paymentSessionStore.getDiscountedFareDetails(), paymentSessionStore, paymentOptionsRQ.getTransactionId());
		}

		// apply service taxes
		// applyServiceTax(paymentOptionsRQ.getTransactionId(), paymentOptionsRQ, trackInfoDTO);
		ServiceTaxRQAdaptor taxAdaptor = new ServiceTaxRQAdaptor();
		ComposePaymentDTO paymentDTO = (new ComposePaymentDTO()).setPnr(paymentOptionsRQ.getPnr()).setLmsCredits(null)
				.setPnr(paymentOptionsRQ.getPnr()).setPaymentSessionStore(paymentSessionStore).setContactEmail(emailAddress)
				.setPaxInfo(paymentOptionsRQ.getPaxInfo()).setDiscountRQ(discountRQ).setTrackInfoDTO(trackInfoDTO)
				.setTransactionId(paymentOptionsRQ.getTransactionId()).setServiceTaxRQ(taxAdaptor.adapt(paymentOptionsRQ));

		paymentOptionsRS = composePaymentOptionsResponse(paymentDTO);

		paymentOptionsRS.setBinPromotionDetails(binPromotionDetails);

		return paymentOptionsRS;
	}

	@Override
	public PaymentOptionsRS getBalancePaymentOptionsRS(PaymentOptionsRQ paymentOptionsRQ, TrackInfoDTO trackInfoDTO,
			String bookingTransactionId) throws ModuleException {

		validationService.validateBalancePaymentOptionsRQ(paymentOptionsRQ, bookingTransactionId);

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(bookingTransactionId);
		BookingSessionStore bookingSessionStore = getBookingSessionStore(bookingTransactionId);

		setAppIndicatorEnum(trackInfoDTO);

		setOriginCountryCodeToSessionForTestSystem(paymentOptionsRQ.getOriginCountryCode(), paymentSessionStore);

		if(!paymentOptionsRQ.isVoucherPaymentInProcess()){
			revertLoyaltyRedemption(paymentSessionStore, paymentSessionStore.getPnr());
		}

		paymentSessionStore.clearPaymentInformation();

		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		String operatingCarrier = "";
		String sellingCarrier = trackInfoDTO.getCarrierCode();
		String pnr = paymentOptionsRQ.getPnr();

		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.balance.payment.pnr.invalid");
		}

		boolean isGroupPnr = deriveIsGroupPNR(trackInfoDTO, pnr);

		boolean isRegisteredUser = isRegisteredUser();

		LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPnr, isRegisteredUser, operatingCarrier,
				sellingCarrier, trackInfoDTO);

		if (reservation == null) {

			throw new ModuleException("payment.api.balance.payment.pnr.invalid");

		}
		// Store pax List in payment Session Store for Service Taxes
		if (paymentSessionStore.getResInfo() == null
				|| (paymentSessionStore.getResInfo() != null && paymentSessionStore.getResInfo().isDummyInfo())) {
			paymentSessionStore.storeResInfo(new ReservationInfo());
		}
		if (paymentSessionStore.getResInfo().getPaxList() == null) {
			List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>();
			ReservationPaxTO pax;
			for (LCCClientReservationPax resPax : reservation.getPassengers()) {
				pax = new ReservationPaxTO();
				pax.setIsParent(resPax.getInfants() != null && !resPax.getInfants().isEmpty() ? true : false);
				pax.setPaxType(resPax.getPaxType());
				pax.setTitle(resPax.getTitle());
				pax.setFirstName(resPax.getFirstName());
				pax.setLastName(resPax.getLastName());
				pax.setSeqNumber(resPax.getPaxSequence());
				pax.setTravelerRefNumber(resPax.getTravelerRefNumber());
				paxList.add(pax);
			}
			paymentSessionStore.getResInfo().setPaxList(paxList);

		}
		if (paymentOptionsRQ.getCountryCode() == null || paymentOptionsRQ.getStateCode() == null
				|| paymentOptionsRQ.getTaxRegNo() == null) {
			paymentOptionsRQ.setCountryCode(reservation.getContactInfo().getCountryCode());
			paymentOptionsRQ.setStateCode(reservation.getContactInfo().getState());
			paymentOptionsRQ.setTaxRegNo(reservation.getContactInfo().getTaxRegNo());
			paymentOptionsRQ.setTransactionId(bookingTransactionId);
		}

		bookingSessionStore.storePnr(pnr);

		String emailAddress = reservation.getContactInfo().getEmail();
		initiateBalancePaymentInformation(paymentSessionStore, reservation);
		if (reservation.getTotalAvailableBalance() != null && reservation.getTotalAvailableBalance().doubleValue() > 0) {
			paymentSessionStore.storeTotalAvailableBalance(reservation.getTotalAvailableBalance());
		}

		ComposePaymentDTO paymentDTO = (new ComposePaymentDTO()).setPnr(pnr).setLmsCredits(null)
				.setPaymentSessionStore(paymentSessionStore).setContactEmail(emailAddress)
				.setPaxInfo(getPaxInfo(reservation.getPassengers())).setDiscountRQ(null).setTrackInfoDTO(trackInfoDTO)
				.setTransactionId(paymentOptionsRQ.getTransactionId()).setPaymentOptionsRQ(paymentOptionsRQ)
				.setBalancePayment(true);

		return composePaymentOptionsResponse(paymentDTO);
	}

	@Override
	public PaymentOptionsRS getRequotePaymentOptionsRS(PaymentOptionsRQ requotePaymentOptionsRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validateRequotePaymentOptions(requotePaymentOptionsRQ);

		setAppIndicatorEnum(trackInfoDTO);

		PaymentRequoteSessionStore paymentRequoteSessionStore = getRequotePaymentSessionStore(
				requotePaymentOptionsRQ.getTransactionId());

		setOriginCountryCodeToSessionForTestSystem(requotePaymentOptionsRQ.getOriginCountryCode(), paymentRequoteSessionStore);

		paymentRequoteSessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		if(!requotePaymentOptionsRQ.isVoucherPaymentInProcess()){
			revertLoyaltyRedemption(paymentRequoteSessionStore, paymentRequoteSessionStore.getResInfo().getPnr());
		}

		if (!isAdminFeeRegulated()) {
			paymentRequoteSessionStore.clearPaymentInformation();
		}

		String pnr = requotePaymentOptionsRQ.getPnr();
		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.requote.payment.pnr.invalid");
		}

		List<Passenger> paxInfo = null;
		paxInfo = getPaxFromPNR(trackInfoDTO, paxInfo, pnr, paymentRequoteSessionStore.isGroupPNR());

		ComposeRequotePaymentDTO paymentDTO = (new ComposeRequotePaymentDTO()).setPnr(pnr)
				.setPaymentRequoteSessionStore(paymentRequoteSessionStore).setLmsBlocking(false).setPaxInfo(paxInfo)
				.setTrackInfoDTO(trackInfoDTO).setTransactionId(requotePaymentOptionsRQ.getTransactionId());

		PaymentOptionsRS paymentOptionsRS = composeRequotePaymentOptionsResponse(paymentDTO);

		paymentOptionsRS.setTransactionId(requotePaymentOptionsRQ.getTransactionId());
		return paymentOptionsRS;
	}

	@Override
	public PaymentOptionsRS getAnciModificationPaymentOptionsRS(PaymentOptionsRQ paymentOptionsRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validateAnciModificationPaymentOptionsRQ(paymentOptionsRQ);

		setAppIndicatorEnum(trackInfoDTO);

		PaymentAncillarySessionStore paymentAncillarySessionStore = getPaymentAncillarySessionStore(
				paymentOptionsRQ.getTransactionId());

		setOriginCountryCodeToSessionForTestSystem(paymentOptionsRQ.getOriginCountryCode(), paymentAncillarySessionStore);

		paymentAncillarySessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		String pnr = paymentOptionsRQ.getPnr();

		if(!paymentOptionsRQ.isVoucherPaymentInProcess()){
			revertLoyaltyRedemption(paymentAncillarySessionStore, pnr);
		}

		if (!isAdminFeeRegulated()) {
			paymentAncillarySessionStore.clearPaymentInformation();
		}

		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.ancillary.modification.payment.pnr.invalid");
		}

		List<Passenger> paxInfo = null;

		paxInfo = getPaxFromPNR(trackInfoDTO, paxInfo, pnr, paymentAncillarySessionStore.isGroupPNR());

		return composeAnciModificationPaymentOptionsResponse(paymentOptionsRQ.getPnr(), null, paymentAncillarySessionStore, false,
				paxInfo, paymentOptionsRQ, trackInfoDTO, paymentOptionsRQ.getTransactionId());

	}

	@Override
	public PaymentOptionsRS getEffectivePaymentRS(EffectivePaymentRQ effectivePaymentRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validateEffectivePaymentRQ(effectivePaymentRQ, getCustomerSessionStore());
		String pnr = null;
		BookingSessionStore bookingSessionStore = getBookingSessionStore(effectivePaymentRQ.getTransactionId());
		if (bookingSessionStore != null) {
			pnr = bookingSessionStore.getPnr();
		}

		PaymentOptionsRS effectivePaymentOptionsRS = new PaymentOptionsRS();

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(effectivePaymentRQ.getTransactionId());

		setAppIndicatorEnum(trackInfoDTO);

		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		BigDecimal totalWithAnciCharges = paymentSessionStore.getTotalPaymentInformation().getTotalWithAncillaryCharges();
		List<Passenger> paxInfo = effectivePaymentRQ.getPaxInfo();
		DiscountedFareDetails discountedFareDetails = null;
		LoyaltyInformation loyaltyInformation = paymentSessionStore.getLoyaltyInformation();
		PayByVoucherInfo payByVoucherInfo = paymentSessionStore.getPayByVoucherInfo();
		LmsCredits lmsCreditsToRdeem = effectivePaymentRQ.getPaymentOptions().getLmsCredits();
		VoucherOption voucherOption = effectivePaymentRQ.getPaymentOptions().getVoucherOption();
		ServiceTaxRQAdaptor taxAdaptor = new ServiceTaxRQAdaptor();

		voidRedeemVoucher(voucherOption, effectivePaymentRQ.getTransactionId());
		// has an applied BIN promotion
		if (effectivePaymentRQ.isBinPromotion()) {

			BigDecimal binDiscountAmount = paymentSessionStore.getBinPromoDiscountInfo().getDiscountAmount();

			// handle LMS total case with BIN
			checkLMSRedemptionLeavesANonZeroAmountForCardPayment(paymentSessionStore, totalWithAnciCharges, lmsCreditsToRdeem,
					binDiscountAmount);

			revertLoyaltyRedemption(paymentSessionStore, paymentSessionStore.getPnr());

			int paymentGatewayId = paymentSessionStore.getBinPromoDiscountInfo().getPaymentGatewayId();

			// Redeem LMS for BIN promotion case
			LmsCredits binLmsCredits = null;
			if (effectivePaymentRQ.getPaymentOptions().getLmsCredits().getRedeemRequestAmount().doubleValue() > 0) {
				DiscountRQ binDiscountRQ = buildDiscountRQForLMSRedemption(effectivePaymentRQ.getPaxInfo(),
						paymentSessionStore.getBinPromoDiscountInfo().getDiscountedFareDetails(), paymentSessionStore,
						effectivePaymentRQ.getTransactionId());
				loyaltyInformation.setBinPromotion(true);
				boolean binRedeemStatus = redeemLoyaltyPoints(lmsCreditsToRdeem, pnr, paymentSessionStore, totalWithAnciCharges,
						null, paxInfo, binDiscountRQ, trackInfoDTO, null, loyaltyInformation,
						effectivePaymentRQ.getTransactionId());

				if (binRedeemStatus) {
					binLmsCredits = new LmsCredits();
					binLmsCredits.setRedeemedAmount(loyaltyInformation.getTotalRedeemedAmount());
					binLmsCredits.setAirRewardId(getMemberAccountId());
					binLmsCredits.setRedeemed(true);
					binLmsCredits.setAvailablePoints(BigDecimal.valueOf(getLMSPointBalance(getCustomerSessionStore().getFFID())));
				}
			}

			// calculate effective payment
			BigDecimal lmsRedeemedAmount = loyaltyInformation.getTotalRedeemedAmount();

			BigDecimal lmsApplicableAmount = totalWithAnciCharges;
			if (!paymentSessionStore.getBinPromoDiscountInfo().isDiscountAsCredit()) {
				lmsApplicableAmount = AccelAeroCalculator.add(lmsApplicableAmount, binDiscountAmount.negate());
			}

			BigDecimal effectiveBalance = lmsApplicableAmount;

			if (lmsRedeemedAmount != null && lmsRedeemedAmount.doubleValue() > 0) {
				effectiveBalance = AccelAeroCalculator.add(effectiveBalance, lmsRedeemedAmount.negate());
			}

			BigDecimal voucherRedeemAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			
			if (payByVoucherInfo != null) {
				voucherRedeemAmount = payByVoucherInfo.getRedeemedTotal();
			}
			
			if (voucherRedeemAmount != null && voucherRedeemAmount.doubleValue() > 0) {
				effectiveBalance = AccelAeroCalculator.add(effectiveBalance, voucherRedeemAmount.negate());
			}
			
			BigDecimal totalTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

			// calculate transaction fee
			if (isAdminFeeRegulated()) {
				if (isAdminFeeRegulatedAndAppliedForChannel()) {
					totalTransactionFee = deriveAndValidateAdminFee(paymentSessionStore,
							applyPromotionsIfAny(totalWithAnciCharges, paymentSessionStore));
				}
			} else {

				totalTransactionFee = deriveCCChgsAndCalculateTotalTransactionFee(paymentSessionStore, paymentGatewayId,
						effectiveBalance, null, null);

			}

			// calculate effectivePayment
			BigDecimal effectivePaymentAmount = AccelAeroCalculator.add(effectiveBalance, totalTransactionFee);

			// build BinPromotionDetails
			BinPromotionDetails binPromotionDetails = buildBinPromotionDetails(null, paymentGatewayId, binLmsCredits,
					binDiscountAmount, lmsApplicableAmount, totalTransactionFee, effectivePaymentAmount);

			// Get fresh paymentOptions barring BIN promotion (for roll-back)
			DiscountRQ discountRQ = buildDiscountRQForLMSRedemption(effectivePaymentRQ.getPaxInfo(),
					paymentSessionStore.getDiscountedFareDetails(), paymentSessionStore, effectivePaymentRQ.getTransactionId());

			ComposePaymentDTO paymentDTO = (new ComposePaymentDTO()).setPnr(effectivePaymentRQ.getPnr()).setLmsCredits(null)
					.setPaymentSessionStore(paymentSessionStore).setContactEmail(null).setPaxInfo(paxInfo)
					.setDiscountRQ(discountRQ).setTrackInfoDTO(trackInfoDTO)
					.setTransactionId(effectivePaymentRQ.getTransactionId())
					.setServiceTaxRQ(taxAdaptor.adapt(effectivePaymentRQ));

			effectivePaymentOptionsRS = composePaymentOptionsResponse(paymentDTO);

			effectivePaymentOptionsRS.setBinPromotionDetails(binPromotionDetails);

			return effectivePaymentOptionsRS;

		}

		removeBinPromotion(paymentSessionStore);

		discountedFareDetails = paymentSessionStore.getDiscountedFareDetails();

		DiscountRQ discountRQ = null;
		if (discountedFareDetails != null) {
			discountRQ = buildDiscountRQForLMSRedemption(effectivePaymentRQ.getPaxInfo(), discountedFareDetails,
					paymentSessionStore, effectivePaymentRQ.getTransactionId());
		}
		if (effectivePaymentRQ.getPaymentOptions().getLmsCredits().getRedeemRequestAmount().doubleValue() > 0) {

			redeemLoyaltyPoints(lmsCreditsToRdeem, pnr, paymentSessionStore, totalWithAnciCharges, null, paxInfo, discountRQ,
					trackInfoDTO, null, loyaltyInformation, effectivePaymentRQ.getTransactionId());
		}

		ComposePaymentDTO paymentDTO = (new ComposePaymentDTO()).setPnr(effectivePaymentRQ.getPnr())
				.setLmsCredits(lmsCreditsToRdeem).setPaymentSessionStore(paymentSessionStore).setContactEmail(null)
				.setPaxInfo(paxInfo).setDiscountRQ(discountRQ).setTrackInfoDTO(trackInfoDTO)
				.setTransactionId(effectivePaymentRQ.getTransactionId()).setServiceTaxRQ(effectivePaymentRQ);

		effectivePaymentOptionsRS = composePaymentOptionsResponse(paymentDTO);

		return effectivePaymentOptionsRS;

	}

	@Override
	public PaymentOptionsRS getEffectiveBalancePaymentRS(EffectivePaymentRQ effectiveBalancePaymentRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validateEffectiveBalancePaymentRQ(effectiveBalancePaymentRQ, getCustomerSessionStore());

		setAppIndicatorEnum(trackInfoDTO);

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(effectiveBalancePaymentRQ.getTransactionId());

		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		String operatingCarrier = "";
		String sellingCarrier = trackInfoDTO.getCarrierCode();
		boolean isGroupPnr = deriveIsGroupPNR(trackInfoDTO, effectiveBalancePaymentRQ.getPnr());
		boolean isRegisteredUser = isRegisteredUser();
		LCCClientReservation reservation = loadProxyReservation(effectiveBalancePaymentRQ.getPnr(), isGroupPnr, isRegisteredUser,
				operatingCarrier, sellingCarrier, trackInfoDTO);

		initiateBalancePaymentInformation(paymentSessionStore, reservation);
		if (reservation.getTotalAvailableBalance() != null && reservation.getTotalAvailableBalance().doubleValue() > 0) {
			paymentSessionStore.storeTotalAvailableBalance(reservation.getTotalAvailableBalance());
		}

		BigDecimal totalWithAnciCharges = paymentSessionStore.getTotalAvailableBalance();

		List<Passenger> paxInfo = null;

		String pnr = effectiveBalancePaymentRQ.getPnr();

		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.balance.payment.pnr.invalid");
		}
		paxInfo = getPaxFromPNR(trackInfoDTO, paxInfo, pnr, paymentSessionStore.isGroupPNR());

		String memberAccountId = getMemberAccountId();

		Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount = ModuleServiceLocator
				.getAirproxyReservationBD().getPaxProduDueAmount(pnr, false, false, getRemaningLoyaltyPoints(memberAccountId));

		voidRedeemVoucher(effectiveBalancePaymentRQ.getPaymentOptions().getVoucherOption(),
				effectiveBalancePaymentRQ.getTransactionId());

		LmsCredits lmsCredits = effectiveBalancePaymentRQ.getPaymentOptions().getLmsCredits();

		if (effectiveBalancePaymentRQ.getPaymentOptions().getLmsCredits().getRedeemRequestAmount().doubleValue() > 0) {
			redeemLoyaltyPoints(lmsCredits, effectiveBalancePaymentRQ.getPnr(), paymentSessionStore, totalWithAnciCharges,
					paxCarrierProductDueAmount, getPaxInfo(reservation.getPassengers()), null, trackInfoDTO, null,
					paymentSessionStore.getLoyaltyInformation(), effectiveBalancePaymentRQ.getTransactionId());
		}

		ComposePaymentDTO paymentDTO = (new ComposePaymentDTO()).setPnr(pnr).setLmsCredits(lmsCredits).setVoucherOption(null)
				.setPaymentSessionStore(paymentSessionStore).setContactEmail(null).setPaxInfo(paxInfo).setDiscountRQ(null)
				.setTrackInfoDTO(trackInfoDTO).setTransactionId(effectiveBalancePaymentRQ.getTransactionId()).setPnr(null);

		// same response except for LMS operations

		return composePaymentOptionsResponse(paymentDTO);

	}

	@Override
	public PaymentOptionsRS getEffectiveRequotePaymentRS(EffectivePaymentRQ effectiveRequotePaymentRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		validationService.validateRequoteEffectivePaymentRQ(effectiveRequotePaymentRQ, getCustomerSessionStore());

		setAppIndicatorEnum(trackInfoDTO);

		List<Passenger> paxInfo = null;

		PaymentRequoteSessionStore paymentRequoteSessionStore = getRequotePaymentSessionStore(
				effectiveRequotePaymentRQ.getTransactionId());

		paymentRequoteSessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		String pnr = effectiveRequotePaymentRQ.getPnr();

		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.requote.payment.pnr.invalid");
		}

		paxInfo = getPaxFromPNR(trackInfoDTO, paxInfo, pnr, paymentRequoteSessionStore.isGroupPNR());

		// set paxInfo used at redeemLMS
		effectiveRequotePaymentRQ.setPaxInfo(paxInfo);

		BigDecimal balanceToPay = paymentRequoteSessionStore.getBalanceToPayAmount();

		Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount = populatePaxProductDueAmounts(
				paymentRequoteSessionStore.getPassengerSummaryList());

		voidRedeemVoucher(effectiveRequotePaymentRQ.getPaymentOptions().getVoucherOption(),
				effectiveRequotePaymentRQ.getTransactionId());

		LmsCredits lmsCredits = effectiveRequotePaymentRQ.getPaymentOptions().getLmsCredits();

		if (effectiveRequotePaymentRQ.getPaymentOptions().getLmsCredits().getRedeemRequestAmount().doubleValue() > 0) {
			redeemLoyaltyPoints(lmsCredits, effectiveRequotePaymentRQ.getPnr(), paymentRequoteSessionStore, balanceToPay,
					paxCarrierProductDueAmount, effectiveRequotePaymentRQ.getPaxInfo(), null, trackInfoDTO, null,
					paymentRequoteSessionStore.getLoyaltyInformation(), effectiveRequotePaymentRQ.getTransactionId());
		}

		ComposeRequotePaymentDTO paymentDTO = (new ComposeRequotePaymentDTO()).setPnr(pnr).setLmsCredits(lmsCredits)
				.setPaymentRequoteSessionStore(paymentRequoteSessionStore).setLmsBlocking(true).setPaxInfo(paxInfo)
				.setTrackInfoDTO(trackInfoDTO).setTransactionId(effectiveRequotePaymentRQ.getTransactionId());

		// same response except for LMS operations
		return composeRequotePaymentOptionsResponse(paymentDTO);

	}

	@Override
	public PaymentOptionsRS getEffectiveAnciModificationPaymentRS(EffectivePaymentRQ effectiveAnciModificationPaymentRQ,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		validationService.validateEffectiveAnciModificationRQ(effectiveAnciModificationPaymentRQ, getCustomerSessionStore());

		setAppIndicatorEnum(trackInfoDTO);

		List<Passenger> paxInfo = null;

		PaymentAncillarySessionStore paymentAncillarySessionStore = getPaymentAncillarySessionStore(
				effectiveAnciModificationPaymentRQ.getTransactionId());

		paymentAncillarySessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		String pnr = effectiveAnciModificationPaymentRQ.getPnr();

		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.ancillary.modification.payment.pnr.invalid");
		}

		paxInfo = getPaxFromPNR(trackInfoDTO, paxInfo, pnr, paymentAncillarySessionStore.isGroupPNR());

		BigDecimal balanceToPay = getBalanceAmountForAnciModification(effectiveAnciModificationPaymentRQ, trackInfoDTO);

		BigDecimal removedCharges = getRemovedCharges(effectiveAnciModificationPaymentRQ, trackInfoDTO);

		voidRedeemVoucher(effectiveAnciModificationPaymentRQ.getPaymentOptions().getVoucherOption(),
				effectiveAnciModificationPaymentRQ.getTransactionId());

		LmsCredits lmsCredits = effectiveAnciModificationPaymentRQ.getPaymentOptions().getLmsCredits();

		if (effectiveAnciModificationPaymentRQ.getPaymentOptions().getLmsCredits().getRedeemRequestAmount().doubleValue() > 0) {
			redeemLoyaltyPoints(lmsCredits, effectiveAnciModificationPaymentRQ.getPnr(), paymentAncillarySessionStore,
					balanceToPay, null, paxInfo, null, trackInfoDTO, removedCharges,
					paymentAncillarySessionStore.getLoyaltyInformation(), effectiveAnciModificationPaymentRQ.getTransactionId());
		}

		return composeAnciModificationPaymentOptionsResponse(pnr, lmsCredits, paymentAncillarySessionStore, false, paxInfo,
				effectiveAnciModificationPaymentRQ, trackInfoDTO, effectiveAnciModificationPaymentRQ.getTransactionId());

	}

	@Override
	public MakePaymentRS processCreateBookingPayment(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException {

		validationService.preValidateCommonPaymentRQ(makePaymentRQ);

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(makePaymentRQ.getTransactionId());

		if (!paymentSessionStore.getVoucherInformation().isTotalAmountPaidFromVoucher()
				&& !paymentSessionStore.getVoucherInformation().isTotalPaidFromVoucherLMS()) {
			validationService.validateCreateBookingPaymentRQ(makePaymentRQ, paymentSessionStore);
		}

		setAppIndicatorEnum(trackInfoDTO);

		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		TotalPaymentInfo totalPaymentInfo = paymentSessionStore.getTotalPaymentInformation();
		BigDecimal totalWithoutAnci = AccelAeroCalculator.scaleValueDefault(totalPaymentInfo.getTotalWithoutAncillaryCharges());
		BigDecimal totalWithAnci = AccelAeroCalculator.scaleValueDefault(totalPaymentInfo.getTotalWithAncillaryCharges());
		BigDecimal effectiveTotal = totalWithAnci;
		boolean isOnHoldPnrCreated = paymentSessionStore.isOnHoldCreatedForPayment();

		// if onhold (IBE cash) payment clear session
		if (makePaymentRQ.getBookingType() != null && makePaymentRQ.getBookingType() == BookingType.ONHOLD) {
			paymentSessionStore.setDiscountedFareDetails(null);
			paymentSessionStore.storeReservationDiscountDTO(null);
			paymentSessionStore.storeBinPromoDiscountInfo(null);
			paymentSessionStore.setDiscountAmount(null);
		}

		// promotions only to be checked at create flow
		// move initial BIN promotion related checks to request validation
		else {
			if (makePaymentRQ.isBinPromotion()) {
				String cardNumber = null;
				int paymentGatewayId = 0;

				BinPromoDiscountInfo binPromoDiscountInfo = paymentSessionStore.getBinPromoDiscountInfo();

				if (binPromoDiscountInfo != null) {
					cardNumber = binPromoDiscountInfo.getCardNumber();
					paymentGatewayId = binPromoDiscountInfo.getPaymentGatewayId();
					// check card type (but not required as per the admin fee only depends on PG)
				} else {
					// throw exception
					// move this into validation
				}

				// move this into validation
				PaymentOptions paymentOptions = makePaymentRQ.getPaymentOptions();

				if (paymentOptions != null) {

					List<PaymentGateway> paymentGateways = paymentOptions.getPaymentGateways();

					// for now multiple PG not supported
					if (paymentGateways != null && !paymentGateways.isEmpty() && paymentGateways.size() == 1) {

						List<Card> cards = paymentGateways.get(0).getCards();

						// for now multiple cards not supported
						if (cards != null && !cards.isEmpty() && cards.size() == 1) {

							if (cardNumber.contentEquals(cards.get(0).getCardNo())
									&& paymentGatewayId == paymentGateways.get(0).getGatewayId()) {

								BigDecimal binDiscount = binPromoDiscountInfo.getDiscountAmount();

								// binDiscount cannot be null
								// only deduct if the discount is applied as money not credit
								if (binDiscount != null && !binPromoDiscountInfo.isDiscountAsCredit()) {
									// adjust totals with BIN discounts
									effectiveTotal = AccelAeroCalculator.add(totalWithAnci, binDiscount.negate());
									totalWithoutAnci = AccelAeroCalculator.add(totalWithoutAnci, binDiscount.negate());
								}
							}
						}
					}

				}

				// Override existing discountedFareDetails with BIN promotion DiscountedFareDetails
				// if payment fails for any reason then payment page should be stored back to the initial loading state
				paymentSessionStore.setDiscountedFareDetails(binPromoDiscountInfo.getDiscountedFareDetails());
				paymentSessionStore.setDiscountAmount(paymentSessionStore.getBinPromoDiscountInfo().getDiscountAmount());

				// clean BIN promotion discountedFareDetails --NOT (bookingController is using)
				// paymentSessionStore.storeBinPromoDiscountInfo(null);

			} else {

				// revert LMS if that's done with BIN promotion
				if (paymentSessionStore.getLoyaltyInformation().isBinPromotion()) {
					revertLoyaltyRedemption(paymentSessionStore, paymentSessionStore.getPnr());
				}

				// remove BIN promotion
				removeBinPromotion(paymentSessionStore);

				// adjust totals with other promotions (NOT BIN)
				effectiveTotal = applyPromotionsIfAny(totalWithAnci, paymentSessionStore);
				totalWithoutAnci = applyPromotionsIfAny(totalWithoutAnci, paymentSessionStore);
			}
		}

		return processPaymentRequest(makePaymentRQ, paymentSessionStore, trackInfoDTO, clientInfoDTO, false, totalWithoutAnci,
				effectiveTotal, isOnHoldPnrCreated,
				isCsOcFlightAvailable(getBookingSessionStore(makePaymentRQ.getTransactionId()).getSegments()), totalPaymentInfo.getTotalTicketPrice());

	}

	@Override
	public TransactionalBaseRS processBalancePayment(MakePaymentRQ balancePaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException {

		validationService.preValidateCommonPaymentRQ(balancePaymentRQ);

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(balancePaymentRQ.getTransactionId());

		validationService.validateProcessBalancePayment(balancePaymentRQ, paymentSessionStore);

		checkReservationDataIntergrity(balancePaymentRQ);

		setAppIndicatorEnum(trackInfoDTO);

		String pnr = balancePaymentRQ.getPnr();

		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);

		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.balance.payment.pnr.invalid");
		}

		String operatingCarrier = "";
		String sellingCarrier = trackInfoDTO.getCarrierCode();
		boolean isGroupPnr = deriveIsGroupPNR(trackInfoDTO, pnr);
		boolean isRegisteredUser = isRegisteredUser();
		LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPnr, isRegisteredUser, operatingCarrier,
				sellingCarrier, trackInfoDTO);

		if (balancePaymentRQ.getContactInfo() == null) {
			ReservationContactResAdaptor reservationContactResAdaptor = new ReservationContactResAdaptor();
			balancePaymentRQ.setContactInfo(reservationContactResAdaptor.adapt(reservation.getContactInfo()));
		}

		BigDecimal balanceToPay = paymentSessionStore.getTotalAvailableBalance();
		// NO ancillary added therefore totalWithoutAnci & totalWithAnci same
		MakePaymentRS makePaymentRS = processPaymentRequest(balancePaymentRQ, paymentSessionStore, trackInfoDTO, clientInfoDTO,
				true, balanceToPay, balanceToPay, false, false, balanceToPay);

		BookingRS bookingRS = (BookingRS) buildBookingRSForBalancePayment(makePaymentRS, balancePaymentRQ.getPnr(),
				balancePaymentRQ.getTransactionId(), trackInfoDTO);
		if (BookingRS.PaymentStatus.PENDING.equals(makePaymentRS.getPaymentStatus()) && makePaymentRS.isOfflinePayment()) {
			Date onholdReleaseZuluTimeStamp = makePaymentRS.getOnHoldReleaseTime();
			String onholdReleaseTime = getOnholdReleaseDuration(onholdReleaseZuluTimeStamp);
			bookingRS.setOnholdReleaseDuration(onholdReleaseTime);
		}

		bookingRS.setFirstDepartureDate(BookingUtil.getFirstDeparture(reservation));
		bookingRS.setContactLastName(BookingUtil.getContactLastName(reservation));
		return bookingRS;

	}

	@Override
	public MakePaymentRS processModifyBookingPayment(MakePaymentRQ requotePaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException {

		validationService.preValidateCommonPaymentRQ(requotePaymentRQ);

		PaymentRequoteSessionStore paymentRequoteSessionStore = getRequotePaymentSessionStore(
				requotePaymentRQ.getTransactionId());

		if (!paymentRequoteSessionStore.getVoucherInformation().isTotalAmountPaidFromVoucher()) {
			validationService.validateProcessRequotePayment(requotePaymentRQ, paymentRequoteSessionStore);
		}

		setAppIndicatorEnum(trackInfoDTO);
		paymentRequoteSessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		String pnr = requotePaymentRQ.getPnr();

		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.requote.payment.pnr.invalid");
		}

		String operatingCarrier = "";
		String sellingCarrier = trackInfoDTO.getCarrierCode();
		boolean isGroupPnr = paymentRequoteSessionStore.isGroupPNR();
		boolean isRegisteredUser = isRegisteredUser();
		LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPnr, isRegisteredUser, operatingCarrier,
				sellingCarrier, trackInfoDTO);

		if (requotePaymentRQ.getContactInfo() == null) {
			ReservationContactResAdaptor reservationContactResAdaptor = new ReservationContactResAdaptor();
			requotePaymentRQ.setContactInfo(reservationContactResAdaptor.adapt(reservation.getContactInfo()));
		}

		BigDecimal balanceToPay = paymentRequoteSessionStore.getBalanceToPayAmount();
		BigDecimal adminFee = AccelAeroCalculator.getDefaultBigDecimalZero();

		BigDecimal serviceTaxForTransactionFee = paymentRequoteSessionStore.getServiceTaxForTransactionFee();
		
		ExternalChgDTO ccChgDTO = deriveCCChgDTO(paymentRequoteSessionStore, 0, balanceToPay);
		if (ccChgDTO != null) {
			adminFee = ccChgDTO.getAmount();
		}
		
		if (serviceTaxForTransactionFee != null && serviceTaxForTransactionFee.doubleValue() > 0) {
			paymentRequoteSessionStore.removePaymentPaxCharge(EXTERNAL_CHARGES.SERVICE_TAX);

			if (ccChgDTO != null) {
				ServiceTaxRQAdaptor serviceTxAdaptor = new ServiceTaxRQAdaptor();
				calculateServiceTaxForCCcharge(serviceTxAdaptor.adapt(requotePaymentRQ), ccChgDTO, trackInfoDTO, true);
			}
		}
		// adding admin fee related service tax to the total
		balanceToPay = AccelAeroCalculator.add(balanceToPay, paymentRequoteSessionStore.getServiceTaxForTransactionFee());

		BigDecimal balanceToPayWithoutExtChgs = balanceToPay;

		List<EXTERNAL_CHARGES> skipCharges = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
		skipCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
		skipCharges.add(EXTERNAL_CHARGES.JN_OTHER);

		balanceToPayWithoutExtChgs = AccelAeroCalculator.subtract(balanceToPayWithoutExtChgs,
				BookingUtil.getTotalExtCharge(paymentRequoteSessionStore.getExternalCharges(), skipCharges));

		BigDecimal totalAllInclusiveAmount = AccelAeroCalculator.add(balanceToPay, adminFee);
		
		MakePaymentRS makePaymentRS = processPaymentRequest(requotePaymentRQ, paymentRequoteSessionStore, trackInfoDTO,
				clientInfoDTO, false, balanceToPayWithoutExtChgs, balanceToPay, false, false, totalAllInclusiveAmount);

		// BookingRS response = new BookingRS();
		makePaymentRS.setPnr(pnr);
		makePaymentRS.setFirstDepartureDate(paymentRequoteSessionStore.getSegments().get(0).getDepartureDateTime());
		makePaymentRS.setContactLastName(paymentRequoteSessionStore.getResInfo().getContactInfo().getLastName());
		if (makePaymentRS != null && makePaymentRS.isSuccess()) {
			log.debug("requote payment processed");
			log.debug("payment status : " + makePaymentRS.getPaymentStatus() + " actions: " + makePaymentRS.getActionStatus());
			if (PaymentStatus.SUCCESS.equals(makePaymentRS.getPaymentStatus())) {
				PostPaymentDTO postPaymentDTO = paymentRequoteSessionStore.getPostPaymentInformation();
				CustomerUtil.saveCustomerAlias(trackInfoDTO.getCustomerId(), postPaymentDTO);	
				try {

					RequoteSessionStore requoteSessionStore = getRequoteSessionStore(requotePaymentRQ.getTransactionId());

					List<ReservationPaxTO> anciIntegratedPaxList = getPaxListWithSelectedAncillaryDTOs(
							requoteSessionStore.getRequoteBalanceReqiredParams().isModifySegment(), requotePaymentRQ,
							trackInfoDTO);

					List<LCCClientReservationInsurance> lccClientReservationInsurances = getReservationInsurances(
							requoteSessionStore.getRequoteBalanceReqiredParams().isModifySegment(), requotePaymentRQ,
							trackInfoDTO);
					RequoteModifyRQ requoteModifyRQ = RequoteConfirmAdaptor.getRequoteModifyRQ(requoteSessionStore, trackInfoDTO,
							lccClientReservationInsurances, anciIntegratedPaxList);
					ModuleServiceLocator.getAirproxyReservationBD().requoteModifySegmentsWithAutoRefundForIBE(requoteModifyRQ,
							trackInfoDTO);
					reservation = bookingService.loadProxyReservation(requoteModifyRQ.getPnr(),
							requoteModifyRQ.getGroupPnr() != null, trackInfoDTO, true, false, null, null,
							AppSysParamsUtil.isPromoCodeEnabled());
					modificationService.modifyFFIDInfo(requoteSessionStore.getFFIDChangeRQ(), reservation, trackInfoDTO);

					makePaymentRS.setActionStatus(BookingRS.ActionStatus.BOOKING_MODIFICATION_SUCCESS);

					CommonServiceUtil commonServices = new CommonServiceUtil();
					commonServices.updateBlockedLmsCreditStatusForModifyReservation(requoteSessionStore);

					requoteSessionStore.clearRequoteSessionTansaction();
					log.debug("completed...........................!");

					// send medical ssr email
					SSRServicesUtil.sendMedicalSsrEmail(reservation.getPNR(), reservation.getPassengers(), anciIntegratedPaxList,
							reservation.getContactInfo());

				} catch (Exception e) {
					log.error("error while saving requote payment", e);
					makePaymentRS.setActionStatus(BookingRS.ActionStatus.BOOKING_MODIFICATION_FAILED);
				}
			} else if (makePaymentRS.isSwitchToExternal()) {
				log.debug("waiting for external party re-direction");
				makePaymentRS.setPaymentStatus(BookingRS.PaymentStatus.PENDING);
				makePaymentRS.setActionStatus(BookingRS.ActionStatus.REDIRET_EXTERNAL);
				// response.setExternalPGDetails(makePaymentRS.getExternalPGDetails());
			} else if (PaymentStatus.ERROR.equals(makePaymentRS.getPaymentStatus())) {
				makePaymentRS.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
				log.debug("error requote payment failed");
			}
		} else {
			log.debug("error requote payment failed");
			makePaymentRS.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
		}

		makePaymentRS.setTransactionId(requotePaymentRQ.getTransactionId());

		return makePaymentRS;
	}

	@Override
	public MakePaymentRS processModifyAnciPayment(MakePaymentRQ anciModificationPaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException {

		validationService.preValidateCommonPaymentRQ(anciModificationPaymentRQ);

		PaymentAncillarySessionStore paymentAncillarySessionStore = getPaymentAncillarySessionStore(
				anciModificationPaymentRQ.getTransactionId());

		if (!paymentAncillarySessionStore.getVoucherInformation().isTotalAmountPaidFromVoucher()) {
			validationService.validateProcessAnciModificationPayment(anciModificationPaymentRQ, paymentAncillarySessionStore);
		}

		setAppIndicatorEnum(trackInfoDTO);

		paymentAncillarySessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		String pnr = anciModificationPaymentRQ.getPnr();

		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("payment.api.ancillary.modification.payment.pnr.invalid");
		}

		String operatingCarrier = "";
		String sellingCarrier = trackInfoDTO.getCarrierCode();
		boolean isGroupPnr = paymentAncillarySessionStore.isGroupPNR();
		boolean isRegisteredUser = isRegisteredUser();
		LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPnr, isRegisteredUser, operatingCarrier,
				sellingCarrier, trackInfoDTO);

		if (anciModificationPaymentRQ.getContactInfo() == null) {
			ReservationContactResAdaptor reservationContactResAdaptor = new ReservationContactResAdaptor();
			anciModificationPaymentRQ.setContactInfo(reservationContactResAdaptor.adapt(reservation.getContactInfo()));
		}

		BigDecimal balanceToPay = null;
		BigDecimal balanceToPayWithoutAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal adminFee = AccelAeroCalculator.getDefaultBigDecimalZero();

		try {
			BigDecimal serviceTaxForTransactionFee = paymentAncillarySessionStore.getServiceTaxForTransactionFee();
			BigDecimal totalExternalCharges = AccelAeroCalculator.add(
					paymentAncillarySessionStore.getFinancialStore().getTotalAnicillaryCharges(), serviceTaxForTransactionFee);

			balanceToPay = ancillaryService.getPrePaymentTO(trackInfoDTO, anciModificationPaymentRQ).getBalanceDue();
			balanceToPay = AccelAeroCalculator.add(balanceToPay, serviceTaxForTransactionFee);

			if (serviceTaxForTransactionFee != null && serviceTaxForTransactionFee.doubleValue() > 0) {
				paymentAncillarySessionStore.removePaymentExternalChargesOnly(EXTERNAL_CHARGES.SERVICE_TAX);

				ExternalChgDTO ccChgDTO = deriveCCChgDTO(paymentAncillarySessionStore, 0, AccelAeroCalculator
						.subtract(balanceToPay, paymentAncillarySessionStore.getServiceTaxForTransactionFee()));
				if (ccChgDTO != null) {
					adminFee = ccChgDTO.getAmount();
				}
				ServiceTaxRQAdaptor serviceTxAdaptor = new ServiceTaxRQAdaptor();
				calculateServiceTaxForCCcharge(serviceTxAdaptor.adapt(anciModificationPaymentRQ), ccChgDTO, trackInfoDTO, true);
			}

			if (AccelAeroCalculator.isGreaterThan(balanceToPay, totalExternalCharges)) {
				balanceToPayWithoutAnci = AccelAeroCalculator.subtract(balanceToPay, totalExternalCharges);
			}
		} catch (Exception e) {
			throw new ModuleException("payment.api.anci.modification.amount.empty");
		}
		
		BigDecimal totalAllInclusiveAmount = AccelAeroCalculator.add(balanceToPay, adminFee);

		MakePaymentRS makePaymentRS = processPaymentRequest(anciModificationPaymentRQ, paymentAncillarySessionStore, trackInfoDTO,
				clientInfoDTO, false, balanceToPayWithoutAnci, balanceToPay, false, false, totalAllInclusiveAmount);

		makePaymentRS.setPnr(pnr);
		makePaymentRS.setFirstDepartureDate(BookingUtil.getFirstDeparture(reservation));
		makePaymentRS.setContactLastName(BookingUtil.getContactLastName(reservation));

		if (makePaymentRS != null && makePaymentRS.isSuccess()) {
			log.debug("anci modification payment processed");
			log.debug("payment status : " + makePaymentRS.getPaymentStatus() + " actions: " + makePaymentRS.getActionStatus());

			if (PaymentStatus.SUCCESS.equals(makePaymentRS.getPaymentStatus())) {
				PostPaymentDTO postPaymentDTO = paymentAncillarySessionStore.getPostPaymentInformation();
				CustomerUtil.saveCustomerAlias(trackInfoDTO.getCustomerId(), postPaymentDTO);
				makePaymentRS.setActionStatus(BookingRS.ActionStatus.BOOKING_MODIFICATION_SUCCESS);

				log.debug("completed...........................!");

			} else if (makePaymentRS.isSwitchToExternal()) {
				log.debug("waiting for external party re-direction");
				makePaymentRS.setPaymentStatus(BookingRS.PaymentStatus.PENDING);
				makePaymentRS.setActionStatus(BookingRS.ActionStatus.REDIRET_EXTERNAL);
				// response.setExternalPGDetails(makePaymentRS.getExternalPGDetails());
			} else if (PaymentStatus.ERROR.equals(makePaymentRS.getPaymentStatus())) {
				makePaymentRS.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
				log.debug("error requote payment failed");
			}
		} else {
			log.debug("error requote payment failed");
			makePaymentRS.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
		}

		if (makePaymentRS.isSuccess() && (makePaymentRS.getPaymentStatus().equals(PaymentStatus.SUCCESS)
				|| makePaymentRS.getPaymentStatus().equals(PaymentStatus.NO_PAYMENT))) {
			try {
				TransactionalBaseRS baseRS = ancillaryService.updateAncillary(trackInfoDTO, anciModificationPaymentRQ);
				// append any message/error/warning to the makePaymentRS
				makePaymentRS.getMessages().addAll(baseRS.getMessages());
				makePaymentRS.getErrors().addAll(baseRS.getErrors());
				makePaymentRS.getWarnings().addAll(baseRS.getWarnings());
			} catch (Exception e) {
				throw new ModuleException("payment.api.anci.modification.update.failed");

			}

			CommonServiceUtil commonServices = new CommonServiceUtil();
			commonServices.updateBlockedLmsCreditStatusForModifyAncillary(paymentAncillarySessionStore);
		}

		return makePaymentRS;
	}

	private String getPreferredLanguageFromLocale() {
		return LocaleContextHolder.getLocale().getLanguage();
	}

	@Override
	public Date calculateOnHoldReleaseTime(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfo) throws ModuleException {
		// TODO validation

		String transactionID = makePaymentRQ.getTransactionId();
		setAppIndicatorEnum(trackInfo);

		Date pnrReleaseTimeZulu = null;
		OnHold onHold = null;

		PaymentBaseSessionStore paymentBaseSessionStore = getPaymentBaseSessionStore(transactionID);
		List<FlightSegmentTO> flightSegments = paymentUtils
				.adaptFlightSegment(paymentBaseSessionStore.getSelectedSegmentsForPayment());
		Date depDateTimeZulu = paymentUtils.getDepartureTimeZulu(flightSegments);

		Collection<OndFareSegChargeTO> ondFareSegChargeTOs = paymentBaseSessionStore.getFareSegChargeTOForPayment()
				.getOndFareSegChargeTOs();
		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = getOnHoldReleaseTimeDTO(flightSegments, ondFareSegChargeTOs);

		if (makePaymentRQ != null && makePaymentRQ.getPaymentOptions() != null
				&& makePaymentRQ.getPaymentOptions().getPaymentGateways() != null
				&& !makePaymentRQ.getPaymentOptions().getPaymentGateways().isEmpty()) {

			int paymentgatewayId = makePaymentRQ.getPaymentOptions().getPaymentGateways().get(0).getGatewayId();
			IPGPaymentOptionDTO pgOptionsDto = paymentUtils.getPaymentGatewayOptions(paymentgatewayId);

			if (pgOptionsDto == null) {
				// payment gateway is in-active/invalid
				log.error("invalid payment gateway selected");
				throw new ModuleException("payment.api.invalid.payment.gateway.selected");
			}

			String baseCurrencyCode = pgOptionsDto.getBaseCurrency();

			Properties ipgProps = paymentUtils.getIPGProperties(paymentgatewayId, baseCurrencyCode);
			boolean isSupportOfflinePayment = Boolean
					.parseBoolean(ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_SUPPORT_OFFLINE_PAYMENT));

			if (isSupportOfflinePayment) {
				onHold = OnHold.IBEOFFLINEPAYMENT;
			} else {
				onHold = OnHold.IBEPAYMENT;
			}

			int onholdReleaseTime = pgOptionsDto.getOnholdReleaseTime();

			pnrReleaseTimeZulu = getOnHoldReleaseTime(onHold, depDateTimeZulu, onHoldReleaseTimeDTO);

			if (isSupportOfflinePayment && onholdReleaseTime > 0) {
				Date zuluLocalTime = CalendarUtil.getCurrentZuluDateTime();
				long longZuluLocalTime = zuluLocalTime.getTime();
				pnrReleaseTimeZulu = new Date(longZuluLocalTime + (onholdReleaseTime * 60000));
			}

		} else if (PaymentConsts.BookingType.ONHOLD.equals(makePaymentRQ.getBookingType())) {
			onHold = OnHold.IBE;
			pnrReleaseTimeZulu = getOnHoldReleaseTime(onHold, depDateTimeZulu, onHoldReleaseTimeDTO);
		}

		return pnrReleaseTimeZulu;
	}

	private void removeBinPromotion(PaymentSessionStore paymentSessionStore) {
		paymentSessionStore.storeBinPromoDiscountInfo(null);

	}

	private DiscountRQ buildDiscountRQForLMSRedemption(List<Passenger> paxInfo, DiscountedFareDetails discountedFareDetails,
			PaymentSessionStore paymentSessionStore, String transactionId) {

		DiscountRQ discountRQ = null;
		Collection<PassengerTypeQuantityTO> paxQtyList = null;
		Collection<PaxChargesTO> paxChargesList = null;

		if (discountedFareDetails != null) {

			if (paxInfo == null) {

				paxQtyList = paymentUtils.getPaxQtyList(paymentSessionStore.getTravellerQuantityForPayment());

			}

			else {

				Collection<ReservationPaxTO> reservationPaxTOCollection = new ArrayList<ReservationPaxTO>();

				if (paymentSessionStore.getPaxAncillaryExternalCharges() != null) {
					List<PassengerChargeTo<LCCClientExternalChgDTO>> anciSessionPaxList = paymentSessionStore
							.getPaxAncillaryExternalCharges();

					AdaptorUtils.adaptCollection(anciSessionPaxList, reservationPaxTOCollection, new AnciPassengerAdaptor(null));
				}

				paxChargesList = transformPaxList(reservationPaxTOCollection, paxInfo);
			}

			String selSystem = paymentSessionStore.getPreferencesForPayment().getSelectedSystem();

			DISCOUNT_METHOD discountMethod = null;
			if (discountedFareDetails.getPromotionId() == null) {
				if (discountedFareDetails.getDomesticSegmentCodeList().size() > 0
						&& AppSysParamsUtil.isDomesticFareDiscountEnabled()) {
					discountMethod = DISCOUNT_METHOD.DOM_FARE_DISCOUNT;

				} else if (discountedFareDetails.getFarePercentage() > 0) {
					discountMethod = DISCOUNT_METHOD.FARE_DISCOUNT;

				}
			} else {
				discountMethod = DISCOUNT_METHOD.PROMOTION;
			}

			discountRQ = new DiscountRQ(discountMethod);
			discountRQ.setDiscountInfoDTO(discountedFareDetails);
			discountRQ.setPaxChargesList(paxChargesList);
			discountRQ.setPaxQtyList(paxQtyList);
			discountRQ.setSystem(ProxyConstants.SYSTEM.getEnum(selSystem));
			discountRQ.setTransactionIdentifier(transactionId);

		}

		return discountRQ;
	}

	private DiscountRQ buildDiscountRQ(List<Passenger> paxInfo, DiscountedFareDetails discountedFareDetails,
			QuotedFareRebuildDTO quotedFareRebuildDTO, boolean calculateTotalOnly, PaymentSessionStore paymentSessionStore,
			String transactionId) {

		DiscountRQ discountRQ = null;

		if (discountedFareDetails != null) {

			discountRQ = buildDiscountRQForLMSRedemption(paxInfo, discountedFareDetails, paymentSessionStore, transactionId);
			discountRQ.setQuotedFareRebuildDTO(quotedFareRebuildDTO);
			discountRQ.setCalculateTotalOnly(calculateTotalOnly);

		}

		return discountRQ;
	}

	private PaymentMethodsRS adaptPaymentMethodsResponse(PaymentMethodsRQ paymentMethodsRequest, Set<String> paymentMethods) {
		PaymentMethodsRS paymentMethodsResponse = new PaymentMethodsRS();
		paymentMethodsResponse.setPaymentMethods(new ArrayList<String>(paymentMethods));
		paymentMethodsResponse.setSuccess(true);
		return paymentMethodsResponse;
	}

	@SuppressWarnings("unchecked")
	private Collection<IPGPaymentOptionDTO> getIPGPaymentGateways(IPGPaymentOptionDTO ipgPaymentOptionDTO)
			throws ModuleException {
		Map<String, Object> paymentMap = ModuleServiceLocator.getPaymentBrokerBD().getPaymentOptions(ipgPaymentOptionDTO);
		return (Collection<IPGPaymentOptionDTO>) paymentMap.get("PAYMENT_GATEWAY");
	}

	@SuppressWarnings("unchecked")
	private Collection<CountryPaymentCardBehaviourDTO> getCountryPaymentCardBehavior(String countryCode,
			Collection<Integer> paymentGatewayIds) throws ModuleException {
		return ModuleServiceLocator.getPaymentBrokerBD().getContryPaymentCardBehavior(countryCode, paymentGatewayIds);
	}

	private Collection<CountryPaymentCardBehaviourDTO> getONDWiseCountryCardBehavior(String countryCode,
			Collection<Integer> paymentGatewayIds) throws ModuleException {
		return ModuleServiceLocator.getPaymentBrokerBD().getONDWiseCountryCardBehavior(countryCode, paymentGatewayIds);
	}

	private SearchParamsForPromoCheck buildSearchParams(int bankId, String returnDate, String searchSystemStr,
			TravellerQuantity travellerQuantity) {
		SearchParamsForPromoCheck searchParamsForPromoCheck = new SearchParamsForPromoCheck();
		searchParamsForPromoCheck.setAdultCount(travellerQuantity.getAdultCount());
		searchParamsForPromoCheck.setChildCount(travellerQuantity.getChildCount());
		searchParamsForPromoCheck.setInfantCount(travellerQuantity.getInfantCount());
		searchParamsForPromoCheck.setReturnDate(returnDate);
		searchParamsForPromoCheck.setSearchSystem(searchSystemStr);
		searchParamsForPromoCheck.setBankId(Integer.valueOf(bankId));
		return searchParamsForPromoCheck;
	}

	private MakePaymentRS processPaymentRequest(MakePaymentRQ makePaymentRequest, PaymentBaseSessionStore paymentBaseSessionStore,
			TrackInfoDTO trackInfoDto, ClientCommonInfoDTO clientInfoDTO, boolean isBalancePayment, BigDecimal totalWithoutAnci,
			BigDecimal totalWithAnci, boolean isOnHoldPnrCreated, boolean isCsOcFlightAvailable, BigDecimal totalTicketPrice) throws ModuleException {

		if (isBalancePayment) {
			preValidationForExistingPayfort(makePaymentRequest, trackInfoDto);
		}

		boolean adminFeeConsumedByVoucher = false;
		
		MakePaymentRS makePaymentRS = new MakePaymentRS();
		DateFormat dateFormat = new SimpleDateFormat(PATTERN1);
		makePaymentRS.setTransactionId(makePaymentRequest.getTransactionId());

		// prevent from re-attempts
		if (paymentBaseSessionStore.getPaymentStatus() != null
				&& PaymentStatus.SUCCESS == paymentBaseSessionStore.getPaymentStatus()) {
			log.info("processPaymentRequest :: payment already completed");
			throw new ModuleException("payment.api.payment.already.completed");
		}

		// "payable-within-credit" case
		if (totalWithAnci.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
			makePaymentRS.setPaymentStatus(PaymentStatus.NO_PAYMENT);
			makePaymentRS.setSuccess(true);
			return makePaymentRS;
		}

		List<String> messages = new ArrayList<String>();
		PostPaymentDTO postPaymentDTO = new PostPaymentDTO();
		paymentBaseSessionStore.storePostPaymentInformation(postPaymentDTO);

		String strRequestData = null;
		boolean paymentProcessed = false;
		boolean alreadyCancelledPreviousTransaction = false;

		try {

			ExternalChgDTO ccChgDTO = null;
			ExternalChgDTO ccFeeTaxChgDTO = null;
			// calculate CC Charges
			BigDecimal totalTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (!isBalancePayment && isAdminFeeRegulatedAndAppliedForChannel()) {

				totalWithAnci = AccelAeroCalculator.subtract(totalWithAnci,
						paymentBaseSessionStore.getServiceTaxForTransactionFee());
				// if invalid exception thrown
				totalTransactionFee = deriveAndValidateAdminFee(paymentBaseSessionStore, totalWithAnci);

				ccChgDTO = deriveCCChgDTO(paymentBaseSessionStore, 0, totalWithAnci);
				// Removing Service Tax from PaymentSessionStore in payment external charge
				// paymentBaseSessionStore.removePaymentPaxChargesOnly(EXTERNAL_CHARGES.SERVICE_TAX);

				// update adminfee related service tax
				ServiceTaxRQAdaptor serviceTxAdaptor = new ServiceTaxRQAdaptor();
				BigDecimal ccFeeServiceTaxAmount = calculateServiceTaxForCCcharge(serviceTxAdaptor.adapt(makePaymentRequest),
						ccChgDTO, trackInfoDto, false);
				totalTransactionFee = AccelAeroCalculator.add(totalTransactionFee, ccFeeServiceTaxAmount);

				ccFeeTaxChgDTO = deriveCCFeeChgDTO(ccChgDTO, paymentBaseSessionStore);

				if (ccChgDTO != null) {
					paymentBaseSessionStore.getPostPaymentInformation().addExternalCharge(ccChgDTO);
				}

				if (ccFeeTaxChgDTO != null) {
					paymentBaseSessionStore.getPostPaymentInformation().addExternalCharge(ccFeeTaxChgDTO);
				}
			}

			BookingType bookingType = makePaymentRequest.getBookingType();

			// ONHOLD case: direct forward, no need to process payment details
			if (bookingType != null && bookingType == BookingType.ONHOLD) {

				// TODO clean below postPayment external charge usage
				if (isAdminFeeRegulatedAndAppliedForChannel()) {

					// if invalid exception thrown
					deriveAndValidateAdminFee(paymentBaseSessionStore, totalWithAnci);

					ccChgDTO = deriveCCChgDTO(paymentBaseSessionStore, 0, totalWithAnci);
					ccFeeTaxChgDTO = deriveCCFeeChgDTO(ccChgDTO, paymentBaseSessionStore);

					if (ccChgDTO != null) {
						paymentBaseSessionStore.getPostPaymentInformation().addExternalCharge(ccChgDTO);
					}

					if (ccFeeTaxChgDTO != null) {
						paymentBaseSessionStore.getPostPaymentInformation().addExternalCharge(ccFeeTaxChgDTO);
					}
				}

				log.info("direct forward, no need to process payment details");
				makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
				makePaymentRS.setCashPayment(true);
				makePaymentRS.setSuccess(true);
			}

			else {

				if (isBalancePayment) {
					postPaymentDTO.setBalancePayment(true);
				}

				// check LMS redemption, deduct, check LMS total case (success but for BIN)
				LoyaltyInformation loyaltyInformation = paymentBaseSessionStore.getLoyaltyInformation();

				if (loyaltyInformation != null && loyaltyInformation.isRedeemLoyaltyPoints()) {

					// adjust totals
					totalWithoutAnci = AccelAeroCalculator.subtract(totalWithoutAnci,
							paymentBaseSessionStore.getLoyaltyInformation().getTotalRedeemedAmount());

					totalWithAnci = AccelAeroCalculator.subtract(totalWithAnci,
							paymentBaseSessionStore.getLoyaltyInformation().getTotalRedeemedAmount());
					
					totalTicketPrice = AccelAeroCalculator.subtract(totalTicketPrice,
							paymentBaseSessionStore.getLoyaltyInformation().getTotalRedeemedAmount());

					// handle LMS TOTAL case
					boolean isLMSTotal = false;

					if (totalWithAnci.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
						// totalWithAnci non-zero before LMS deduction (NO_PAYMENT)
						isLMSTotal = true;
					}

					LoyaltyPaymentOption loyaltyPaymentOption = loyaltyInformation.getLoyaltyPaymentOption();

					// FIXME can remove carrierWiseLoyaltyPaymentInfo from postPaymentDTO, can directly access from the
					// session
					postPaymentDTO.setCarrierWiseLoyaltyPaymentInfo(
							paymentBaseSessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo());

					if (LoyaltyPaymentOption.TOTAL == loyaltyPaymentOption) {
						log.info("only loyalty payment skip other payment options");
						isLMSTotal = true;
					} else if (LoyaltyPaymentOption.PART == loyaltyPaymentOption) {
						log.info("partial loyalty payment");
					}

					// handle LMS total (SUCCESS if it's not BIN)
					if (isLMSTotal) {

						if (makePaymentRequest.isBinPromotion()) {
							makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);
							makePaymentRS.setMessages(Arrays.asList(errorMessageSource.getMessage(
									"payment.bin.promotion.lms.total.warning", null, LocaleContextHolder.getLocale())));

						} else {
							makePaymentRS.setPaymentStatus(PaymentStatus.SUCCESS);
						}

						makePaymentRS.setSuccess(true);
						// removed return of makePaymentRS statement as full loyalty payment needs payment gateway to
						// pay admin fee
					}

				}

				if (AppSysParamsUtil.isVoucherEnabled()) {

					if (paymentBaseSessionStore.getVoucherInformation().isTotalAmountPaidFromVoucher()) {
						log.info("Only voucher payment. Skip other payments");
						makePaymentRS.setPaymentStatus(PaymentStatus.SUCCESS);
						makePaymentRS.setSuccess(true);
						return makePaymentRS;
					}

					BigDecimal redeemedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (paymentBaseSessionStore.getPayByVoucherInfo() != null) {
						redeemedAmount = paymentBaseSessionStore.getPayByVoucherInfo().getRedeemedTotal();
					}

					totalWithoutAnci = AccelAeroCalculator.subtract(totalWithoutAnci, redeemedAmount);

					if (totalTicketPrice.compareTo(redeemedAmount) <= 0) {
						totalWithAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
						totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {
						if (totalWithAnci.compareTo(redeemedAmount) < 0) {
							adminFeeConsumedByVoucher = true;
						}
						totalWithAnci = AccelAeroCalculator.subtract(totalWithAnci, redeemedAmount);
						totalTicketPrice = AccelAeroCalculator.subtract(totalTicketPrice, redeemedAmount);
					}
				}

				// PNR in modify flow and when on-hold booking is done prior to payment
				String pnr = makePaymentRequest.getPnr();

				if( AccelAeroCalculator.getDefaultBigDecimalZero().equals(totalTicketPrice)){
					log.info("Payments has completed with LMS/VOUCHER  ::: PNR >> " + pnr);
					paymentBaseSessionStore.getVoucherInformation().setTotalPaidFromVoucherLMS(true);
					makePaymentRS.setPaymentStatus(PaymentStatus.SUCCESS);
					makePaymentRS.setSuccess(true);
					return makePaymentRS;
				}

				// build paymentOption (PG and Card details)
				PaymentOptions paymentOptions = makePaymentRequest.getPaymentOptions();
				CustomerUtil.overrideAndInjectAlias(trackInfoDto.getCustomerId(), paymentOptions);
				
				List<PaymentGateway> paymentGateways = paymentOptions.getPaymentGateways();

				for (PaymentGateway paymentGateway : paymentGateways) {

					int paymentgatewayId = paymentGateway.getGatewayId();

					IPGPaymentOptionDTO pgOptionsDto = paymentUtils.getPaymentGatewayOptions(paymentgatewayId);

					if (pgOptionsDto == null) {
						// payment gateway is in-active/invalid
						log.error("invalid payment gateway selected");
						throw new ModuleException("payment.api.invalid.payment.gateway.selected");
					}

					// multiple cards not supported (for now)
					List<Card> selectedCards = paymentGateway.getCards();
					if (selectedCards != null && selectedCards.size() > 1) {
						throw new ModuleException("payment.api.invalid.multiple.card.options.error");
					}

					// get paymentGateway and populate necessary fields
					String baseCurrencyCode = pgOptionsDto.getBaseCurrency();
					Properties ipgProps = paymentUtils.getIPGProperties(paymentgatewayId, baseCurrencyCode);
					paymentGateway = adaptPaymentGateway(pgOptionsDto, ipgProps);
					String providerName = paymentGateway.getProviderName();
					String brokerType = paymentGateway.getBrokerType();
					Boolean isSwitchToExternalURL = paymentGateway.isSwitchToExternalUrl();
					Boolean viewPaymentInIFrame = paymentGateway.isViewPaymentInIframe();
					boolean isOfflinePGW = PaymentConsts.OFFLINE_PG_OPTIONS.contains(providerName);
					boolean isBrokerTypeInternalExternal = PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL
							.equals(brokerType);
					String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);
					String requestMethod = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_REQUEST_METHOD);

					makePaymentRS.setSuccess(true);
					makePaymentRS.setSwitchToExternal(isSwitchToExternalURL);

					// start to build externalPGDetails & set to makePaymentResponse
					ExternalPGDetails externalPGDetails = new ExternalPGDetails();
					makePaymentRS.setExternalPGDetails(externalPGDetails);
					externalPGDetails.setViewPaymentInIframe(viewPaymentInIFrame);
					externalPGDetails.setBrokerType(brokerType);
					externalPGDetails.setRequestMethod(requestMethod);
					

					BigDecimal totalPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					
					if (AppSysParamsUtil.isVoucherEnabled() && adminFeeConsumedByVoucher) { 
						totalWithAnci = totalTicketPrice;
						totalPaymentAmount = totalTicketPrice;
						totalTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {
						if (!isAdminFeeRegulated()) {

							paymentBaseSessionStore.removePaymentCharge(EXTERNAL_CHARGES.CREDIT_CARD);
							paymentBaseSessionStore.removePaymentCharge(EXTERNAL_CHARGES.JN_OTHER);

							ccChgDTO = deriveCCChgDTO(paymentBaseSessionStore, paymentgatewayId, totalWithAnci);

							if (ccChgDTO != null && BigDecimal.ZERO.compareTo(ccChgDTO.getAmount()) < 0) {
								// add cardFee to post payment info and session
								postPaymentDTO.addExternalCharge(ccChgDTO);
								paymentBaseSessionStore.addPaymentCharge(ccChgDTO);

								ccFeeTaxChgDTO = deriveCCFeeChgDTO(paymentBaseSessionStore);

								if (ccFeeTaxChgDTO != null) {
									postPaymentDTO.addExternalCharge(ccFeeTaxChgDTO);
									paymentBaseSessionStore.addPaymentCharge(ccFeeTaxChgDTO);
								}

								totalTransactionFee = calculateTransactionFee(ccChgDTO, ccFeeTaxChgDTO);
							}

						}

						// TODO clean up postPayment external charge usage
						if (ccChgDTO != null && ccChgDTO.getAmount().doubleValue() > 0) {
							paymentBaseSessionStore.getPostPaymentInformation().addExternalCharge(ccChgDTO);
							if (!isAdminFeeRegulatedAndAppliedForChannel()) {
								boolean addToPaymentStore = (isOnHoldPnrCreated || isBalancePayment
										|| paymentBaseSessionStore instanceof RequoteTransaction) ? true : false;
								ServiceTaxRQAdaptor serviceTxAdaptor = new ServiceTaxRQAdaptor();
								BigDecimal ccFeeServiceTaxAmount = calculateServiceTaxForCCcharge(
										serviceTxAdaptor.adapt(makePaymentRequest), ccChgDTO, trackInfoDto,
										addToPaymentStore);
								totalTransactionFee = AccelAeroCalculator.add(totalTransactionFee,
										ccFeeServiceTaxAmount);

							}
						}
						if (ccFeeTaxChgDTO != null) {
							paymentBaseSessionStore.getPostPaymentInformation().addExternalCharge(ccFeeTaxChgDTO);
						}
					}
					// set effective payable amount
					
					ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

					CurrencyExchangeRate currencyExrate = exchangeRateProxy.getCurrencyExchangeRate(
							paymentGateway.getPaymentCurrency(), CommonServiceUtil.getApplicationEngine(appIndicator));

					totalPaymentAmount = AccelAeroCalculator.add(totalWithAnci, totalTransactionFee);

					
					LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();

					paymentGateway.setEffectivePaymentAmount(getAmountObj(totalPaymentAmount, currencyExrate, baseCurrencyCode));

					PayCurrencyDTO payCurrencyDTO = getPayCurrencyDTO(currencyExrate, baseCurrencyCode, trackInfoDto);
					postPaymentDTO.setPayCurrencyDTO(payCurrencyDTO);

					// build card details
					if (brokerType != null && selectedCards != null && !selectedCards.isEmpty()) {

						for (Card card : selectedCards) {

							boolean isGroupPnr = paymentBaseSessionStore.isGroupPNR();

							// TODO required for payment retry?
							postPaymentDTO.setGroupPnr(isGroupPnr);

							// if card not valid
							if (!isSelectedPaymentOptionValid(card.getCardType(), paymentgatewayId, true, pnr, isGroupPnr,
									trackInfoDto, paymentBaseSessionStore)) {
								log.info("selected payment card not applicable for search criteria");
								throw new ModuleException("payment.api.invalid.payment.gateway.selected");
							}

							String selCardType = "";
							String txtCardNo = "";
							String txtName = "";
							String txtSCode = "";
							String strExpiry = "";

							selCardType = card.getCardType() + "";

							// add card details if it's internal external
							if (brokerType != null && isBrokerTypeInternalExternal) {
								txtCardNo = card.getCardNo();
								txtName = card.getCardHoldersName();
								txtSCode = card.getCardCvv();
								strExpiry = dateFormat.format(card.getExpiryDate());

							}

							// verify requested payable against the calculated one
							if (totalPaymentAmount.compareTo(card.getPaymentAmount()) != 0) {
								log.info("found payment amount miss-match :actual payment amt:" + totalPaymentAmount
										+ "submitted amt:" + card.getPaymentAmount());
								// throw new ModuleException("payment.api.invalid.payment.amount.error");
							}

							if (AccelAeroCalculator.subtract(card.getPaymentAmount(), totalPaymentAmount).abs()
									.compareTo(BigDecimal.valueOf(0.01)) > 0) {
								totalPaymentAmount = card.getPaymentAmount();
							}

							// add commonCreditCardPaymentInfo (type of LccClientPaymentInfo) to
							// LccClientPaymentAssembler
							IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(
									paymentgatewayId, baseCurrencyCode);

							lccClientPaymentAssembler.addAllExternalCharges(paymentBaseSessionStore.getExternalCharges());

							if (isBrokerTypeInternalExternal) {
								lccClientPaymentAssembler.addInternalCardPayment(Integer.parseInt(selCardType), strExpiry,
										txtCardNo, txtName, null, txtSCode, totalWithoutAnci, appIndicator, TnxModeEnum.SECURE_3D,
										ipgIdentificationParamsDTO, payCurrencyDTO, new Date(), txtName, "", null, null, null);
							} else {
								lccClientPaymentAssembler.addExternalCardPayment(Integer.parseInt(selCardType), "",
										totalWithoutAnci, appIndicator, TnxModeEnum.SECURE_3D, null, ipgIdentificationParamsDTO,
										payCurrencyDTO, new Date(), null, null, isOfflinePGW, null);
							}

							// build contact information
							ReservationContactRQAdaptor contactInfoTransformer = new ReservationContactRQAdaptor(null);
							ReservationContact contactInfo = makePaymentRequest.getContactInfo();
							CommonReservationContactInfo reservationContactInfo = contactInfoTransformer.adapt(contactInfo);

							SYSTEM selectedSystem = SYSTEM.AA;
							if (paymentBaseSessionStore.getPreferencesForPayment() != null && !StringUtil
									.isNullOrEmpty(paymentBaseSessionStore.getPreferencesForPayment().getSelectedSystem())) {
								selectedSystem = SYSTEM
										.getEnum(paymentBaseSessionStore.getPreferencesForPayment().getSelectedSystem());
							}
							// get temporary payment map
							// isCsOcFlightAvailable///***

							// Grvty Impl Moving PNR Generation
							if (StringUtil.isNullOrEmpty(pnr)) {
								pnr = getPaymentSessionStore(makePaymentRequest.getTransactionId()).getPnr();
							}

							Map<Integer, CommonCreditCardPaymentInfo> tempPayMap = ModuleServiceLocator.getAirproxyReservationBD()
									.makeTemporyPaymentEntry(pnr, reservationContactInfo, lccClientPaymentAssembler, true,
											selectedSystem, trackInfoDto.getCarrierCode(), trackInfoDto,
											false /* isCsOcFlightAvailable */);
							int temporyPayId = paymentUtils.getTmpPayIdFromTmpPayMap(tempPayMap);

							// save LMS blocked credit details
							if (!AppSysParamsUtil.isEnableSingleStepRedeemingProcess()) {
								saveLMSBlockCreditDetails(loyaltyInformation, temporyPayId, totalPaymentAmount, pnr,
										trackInfoDto);
							} else {
								updateExistingBlockedCreditInfo(loyaltyInformation, totalPaymentAmount, pnr, temporyPayId);
							}

							String ipgPNR = null;
							if (pnr == null || pnr.isEmpty()) {
								ipgPNR = paymentUtils.getPNRFromLccTmpPayMap(tempPayMap);
							} else {
								ipgPNR = pnr;
							}

							// build PG specific IPGRequestDTO
							AdditionalDetails additionalDetails = paymentOptions.getAdditionalDetails();
							IPGRequestDTO ipgRequestDTO = composeIPGRequestDTO(card, ipgIdentificationParamsDTO, ipgPNR,
									temporyPayId, additionalDetails,
									AccelAeroRounderPolicy
											.convertAndRound(lccClientPaymentAssembler.getTotalPayAmount(), payCurrencyDTO)
											.toString(),
									contactInfo, trackInfoDto, clientInfoDTO, isBrokerTypeInternalExternal,
									paymentBaseSessionStore.getSelectedSegmentsForPayment(),
									paymentBaseSessionStore.getOriginCountryCodeForTest(), makePaymentRequest.isMobile(),
									payCurrencyDTO.getDecimalPlaces());
							ipgRequestDTO.setIntExtPaymentGateway(isBrokerTypeInternalExternal);
							ipgRequestDTO.setPaymentGateWayName(paymentGateway.getProviderName());
							ipgRequestDTO
									.setSelectedSystem(paymentBaseSessionStore.getPreferencesForPayment().getSelectedSystem());
							postPaymentDTO.setPaymentGateWay(paymentGateway);
							ipgRequestDTO.setServiceAppFlow(true);

							long onholdexpireIntervalinSec = 0;
							if (paymentBaseSessionStore.getOperationType() == OperationTypes.MAKE_ONLY && !isBalancePayment) {
								Date onholdExpireDate = calculateOnHoldReleaseTime(makePaymentRequest, trackInfoDto);
								Date currentDateTime = Calendar.getInstance().getTime();

								if (onholdExpireDate != null) {
									onholdexpireIntervalinSec = (onholdExpireDate.getTime() - currentDateTime.getTime()) / 1000;
								}
							}
							ipgRequestDTO.setExpireMTCOffLinePeriod(onholdexpireIntervalinSec);

							String customerId = getCustomerSessionStore().getLoggedInCustomerID();
							if (customerId != null) {
								if (card.isSaveCreditCard()) {
									ipgRequestDTO.setSaveCreditCard(card.isSaveCreditCard());
									ipgRequestDTO.setExpDate(card.getExpiryDate());
									ipgRequestDTO.setCardName(card.getCardName());
								}
								ipgRequestDTO.setCustomerId(customerId);
								ipgRequestDTO.setAlias(card.getAlias());
							}
							ipgRequestDTO.setCardNo(card.getCardNo());
							ipgRequestDTO.setSecureCode(card.getCardCvv());
							ipgRequestDTO.setHolderName(card.getCardHoldersName());
							ipgRequestDTO
									.setExpiryDate(card.getExpiryDate() != null ? dateFormat.format(card.getExpiryDate()) : null);
							PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
							IPGRequestResultsDTO ipgRequestResultsDTO;

							// get PG specific ipgRequestResultsDTO
							// handle all internal-external payment gateways (PayPal etc)
							if (isBrokerTypeInternalExternal) {

								List<CardDetailConfigDTO> cardDetailConfigData = paymentBrokerBD
										.getPaymentGatewayCardConfigData(ipgIdentificationParamsDTO.getIpgId().toString());

								if (PaymentConsts.PAYPAL_PG.equalsIgnoreCase(paymentGateway.getProviderCode())) {
									// TODO: internal logic & PG needs to be checked
									// paypalResponse = new PAYPALResponse();
									// setPPRequestDTO(ipgRequestDTO, payCurrencyDTO, ipgProps);// set pp data
									// userInputDTO.setCreditCardType(getStandardCardType(Integer.parseInt(card.getCardType())));
									// paypalResponse.setCreditCardType(card.getCardType());
									// ipgRequestDTO.setIntExtPaymentGateway(isIntExtPaymentGateway);
									// if (isIntExtPaymentGateway) {
									// setPPCreditCardInfo(ipgRequestDTO);
									// paypalResponse.setCVV2(card.getCardCVV());

									ipgRequestDTO.setUserInputDTO(new UserInputDTO());

									ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO, cardDetailConfigData);
								}

								// other internal external PGs
								else {
									ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO, cardDetailConfigData);
								}
							}

							// handle external PGs, Payfort, QIWI and the rest
							else if (PaymentConsts.PAYFORT_PAY_AT_STORE_PG.equalsIgnoreCase(providerName)) {
								// TODO: internal logic & PG needs to be checked
								ipgRequestDTO.setContactMobileNumber(additionalDetails.getParam2());
								ipgRequestDTO.setContactEmail(additionalDetails.getParam1());

								postPaymentDTO.setPnr(ipgPNR);
								postPaymentDTO.setPayFortStatus(true);
								postPaymentDTO.setPayFortStatusVoucher(false);
								postPaymentDTO.setOnHoldCreated(isOnHoldPnrCreated);

								ipgRequestDTO.setInvoiceExpirationTime(setupOnholdOfflinePaymentTime(ipgPNR,
										paymentBaseSessionStore.isGroupPNR(), trackInfoDto));

								ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);

								if (ipgRequestResultsDTO.getRequestData().equals("")) {
									log.info("PayFort Voucher Creation Failed");
									makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);
									makePaymentRS.setVoucherGeneration(VoucherStatus.VOUCHER_FAILED);
								} else {
									if (!isOnHoldPnrCreated) {
										int releaseTimeInMinutes = 0;// pgw.getOnholdReleaseTime(); // get pg
																		// ohd-release time
										// TODO check
										Date zuluLocalTime = CalendarUtil.getCurrentZuluDateTime();
										long longZuluLocalTime = zuluLocalTime.getTime();
										Date afterAddingPGWonholdTime = new Date(
												longZuluLocalTime + (releaseTimeInMinutes * 60000));
										String versionNo = "0";
										// if (version != null && !"".equals(version)) {
										// versionNo = version;
										// }
										// if (releaseTimeInMinutes > 0) {
										// trackInfoDTO.setMarketingUserId("WEB-USER");
										// ModuleServiceLocator.getAirproxyReservationBD().extendOnHold(ipgPNR,
										// afterAddingPGWonholdTime,
										// versionNo, groupPNR, getClientInfoDTO(), trackInfoDTO);
										// }
									}
									postPaymentDTO.setPayFortStatusVoucher(true);

									String waitingMsg = null;
									if (isOnHoldPnrCreated) {
										// TODO: need to get from master data
										waitingMsg = "<b>You can view the pay@store voucher details below. It will take several minutes to fully load the voucher.</b><br/>Once the voucher is displayed please click the continue button for further processing. When the voucher is displayed make sure to note down the necessary details or you can either email or sms the important details using the relavant buttons.<br/>PNR for your Reservation is <b>"
												+ pnr + "</b>.";

									} else {
										// TODO: need to get from master data
										waitingMsg = "<b>You can view the pay@store voucher details below. It will take several minutes to fully load the voucher.</b><br/>Once the voucher is displayed please click the continue button for further processing. When the voucher is displayed make sure to note down the necessary details or you can either email or sms the important details using the relavant buttons.<br/>PNR for your Reservation is <b>"
												+ pnr + "</b>.";
									}
									// TODO: need to get from master data
									// waitingMsg = waitingMsg.replace("{0}", pnr);

									messages.add(waitingMsg);
									makePaymentRS.setMessages(messages);
									makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
									makePaymentRS.setOfflinePayment(true);
									makePaymentRS.setVoucherGeneration(VoucherStatus.VOUCHER_SUCCESS);

									String scriptUrl = ipgProps.getProperty("payAtstoreScriptUrl");
									externalPGDetails.setScriptUrl(scriptUrl);
									externalPGDetails.setRequestId((String) ipgRequestResultsDTO.getPostDataMap()
											.get(Pay_AT_StoreResponseDTO.REQUEST_ID));
									externalPGDetails.setVoucherId((String) ipgRequestResultsDTO.getPostDataMap()
											.get(Pay_AT_StoreResponseDTO.VOUCHER_ID));
								}
								paymentProcessed = true;
							} else if (PaymentConsts.TAP_OFFLINE_PG.equalsIgnoreCase(providerName)) {

								ipgRequestDTO.setContactMobileNumber(additionalDetails.getParam2());
								ipgRequestDTO.setContactEmail(additionalDetails.getParam1());

								if (isBalancePayment) {
									cancelPreviousOfflineTransaction(ipgPNR);
									alreadyCancelledPreviousTransaction = true;
								}

								postPaymentDTO.setOnHoldCreated(isOnHoldPnrCreated);
								Date onHoldReleaseTime = setupOnholdOfflinePaymentTime(ipgPNR,
										paymentBaseSessionStore.isGroupPNR(), trackInfoDto);
								ipgRequestDTO.setInvoiceExpirationTime(onHoldReleaseTime);
								makePaymentRS.setOnHoldReleaseTime(onHoldReleaseTime);
								ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);

								if (ipgRequestResultsDTO.getRequestData() == null
										|| ipgRequestResultsDTO.getRequestData().equals("")) {
									log.info("Tap Offline PG Failed");
									makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);
									makePaymentRS.setErrors(Arrays.asList(ipgRequestResultsDTO.getErrorCode()));
								} else {
									// If needed in future
									String waitingMsg = "<b>Your payment reference ID from TAP Offline Payment Gateway : "
											+ ipgRequestResultsDTO.getPostDataMap().get(TapOfflineResponse.TAP_REF_ID)
											+ ".<br/>Payment details are sent to your email and mobile number.Reservation will be confirmed once the TAP payment gateway collect the money.</b>.";
									messages.add(waitingMsg);
									makePaymentRS.setMessages(messages);
									makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
									makePaymentRS.setOfflinePayment(true);
									externalPGDetails.setVoucherId(
											(String) ipgRequestResultsDTO.getPostDataMap().get(TapOfflineResponse.TAP_REF_ID));
									externalPGDetails.setDisplayOfflinePaymentMessage(true);
								}
								paymentProcessed = true;

							} else if (PaymentConsts.CMI_FATOURATI.equalsIgnoreCase(providerName)) {

								if (isBalancePayment) {
									cancelPreviousOfflineTransaction(ipgPNR);
									alreadyCancelledPreviousTransaction = true;
								}

								postPaymentDTO.setOnHoldCreated(isOnHoldPnrCreated);
								Date onHoldReleaseTime = setupOnholdOfflinePaymentTime(ipgPNR,
										paymentBaseSessionStore.isGroupPNR(), trackInfoDto);
								ipgRequestDTO.setInvoiceExpirationTime(onHoldReleaseTime);
								makePaymentRS.setOnHoldReleaseTime(onHoldReleaseTime);
								ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);

								if (ipgRequestResultsDTO.getRequestData() == null
										|| ipgRequestResultsDTO.getRequestData().equals("")) {
									log.info("Cmi Offline PG Failed");
									makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);
									makePaymentRS.setErrors(Arrays.asList(ipgRequestResultsDTO.getErrorCode()));
								} else {
									// If needed in future
									String waitingMsg = "<b>Your payment reference ID from CMI fatourati Offline Payment Gateway : "
											+ ipgRequestResultsDTO.getPostDataMap().get("RefFatourati")
											+ ".<br/>Payment details are sent to your email and mobile number.Reservation will be confirmed once the CMI Fatourati payment gateway collect the money.</b>.";
									messages.add(waitingMsg);
									makePaymentRS.setMessages(messages);
									makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
									makePaymentRS.setOfflinePayment(true);
									externalPGDetails
											.setVoucherId((String) ipgRequestResultsDTO.getPostDataMap().get("RefFatourati"));
									externalPGDetails.setDisplayOfflinePaymentMessage(true);
									PaymentEmailUtil.sendFatouratiEmail(getClientReservationDto(externalPGDetails.getVoucherId(),
											makePaymentRequest, ipgRequestDTO, paymentBaseSessionStore), null, null);
								}
								paymentProcessed = true;

							}

							else if (PaymentConsts.PAY_FORT_OFFLINE.equalsIgnoreCase(providerName)) {
								if (isBalancePayment) {
									cancelPreviousOfflineTransaction(ipgPNR);
									alreadyCancelledPreviousTransaction = true;
								}

								postPaymentDTO.setOnHoldCreated(isOnHoldPnrCreated);
								Date onHoldReleaseTime = setupOnholdOfflinePaymentTime(ipgPNR,
										paymentBaseSessionStore.isGroupPNR(), trackInfoDto);
								ipgRequestDTO.setExpiryDate(
										new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(onHoldReleaseTime));
								ipgRequestDTO.setInvoiceExpirationTime(onHoldReleaseTime);
								makePaymentRS.setOnHoldReleaseTime(onHoldReleaseTime);
								ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);

								if (StringUtils.isEmpty(ipgRequestResultsDTO.getRequestData())) {
									log.info("PayFort Offline PG Failed");
									makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);
									makePaymentRS.setErrors(Arrays.asList(ipgRequestResultsDTO.getErrorCode()));
								} else {
									makePaymentRS.setMessages(messages);
									makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
									makePaymentRS.setOfflinePayment(true);
									externalPGDetails.setPayFortOfflineBillNumber(ipgRequestResultsDTO.getBillNumber());

								}
								paymentProcessed = true;
							} else if (PaymentConsts.QIWI.equalsIgnoreCase(providerName)) {
								// TODO check expiration time
								ipgRequestDTO.setInvoiceExpirationTime(new Date());

								ipgRequestDTO.setContactMobileNumber(additionalDetails.getParam2());

								postPaymentDTO.setPnr(ipgPNR);

								postPaymentDTO.setOnHoldCreated(isOnHoldPnrCreated);

								ipgRequestDTO.setInvoiceExpirationTime(setupOnholdOfflinePaymentTime(ipgPNR,
										paymentBaseSessionStore.isGroupPNR(), trackInfoDto));

								ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);

								QiwiPResponse invoiceCreationResponse = ipgRequestResultsDTO.getQiwiResponse();

								isSwitchToExternalURL = !invoiceCreationResponse.isOfflineMode();

								postPaymentDTO.setTimeToSpare(invoiceCreationResponse.getTimeToSpare());
								makePaymentRS.setSwitchToExternal(isSwitchToExternalURL);
								postPaymentDTO.setInvoiceStatus(invoiceCreationResponse.getStatus());
								postPaymentDTO.setBillId(invoiceCreationResponse.getBillId());

								if (invoiceCreationResponse.getStatus().equals(QiwiPRequest.FAIL)) {

									log.debug("[PaymentServiceAdaper::Qiwi Invoice creation Failed]");

									postPaymentDTO.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(
											temporyPayId, ipgRequestResultsDTO.getPaymentBrokerRefNo(), tempPayMap));

									postPaymentDTO.setErrorCode(invoiceCreationResponse.getError());

									// TODO: recheck
									makePaymentRS.setErrors(
											Arrays.asList(QiwiPRequest.QIWI_ERROR_CODE, ipgRequestResultsDTO.getErrorCode()));

									makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);

									// makePaymentResponse.setActionStatus(ActionStatus.BOOKING_CREATION_FAILED);

								}

								// offline payment mode
								else if (!isSwitchToExternalURL
										&& invoiceCreationResponse.getStatus().equals(QiwiPRequest.WAITING)) {

									log.debug("[QIWI Offline Mode]");

									if (postPaymentDTO.getInvoiceStatus() != null
											&& postPaymentDTO.getInvoiceStatus().equals(QiwiPRequest.WAITING)
											&& (postPaymentDTO.getBillId() != null || postPaymentDTO.getBillId() != "")) {
										long timeToSpare = postPaymentDTO.getTimeToSpare();
										String billId = postPaymentDTO.getBillId();

										String waitingMsg = null;
										// FIXME
										if (!postPaymentDTO.isOnHoldCreated()) {
											waitingMsg = "<b>QIWI dummy waiting message (onhold created)<b>" + "<b>" + pnr
													+ "</b>.";
										} else {
											waitingMsg = "<b>QIWI dummy waiting message<b>" + "<b>" + pnr + "</b>.";

										}
										waitingMsg = waitingMsg.replace("{0}", paymentUtils.timeDiffInStr(timeToSpare));
										waitingMsg = waitingMsg.replace("{1}", billId);
										waitingMsg = waitingMsg.replace("{2}", pnr);

										log.debug("[PaymentServiceAdaper::Qiwi Offline Mode] Redirecting to Offline Info Page");

										postPaymentDTO.setInvoiceStatus(null);

										messages.add(waitingMsg);
										makePaymentRS.setMessages(messages);
										makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
										makePaymentRS.setOfflinePayment(true);
									}

								}

								paymentProcessed = true;

							}
							// rest of the external PGs
							else {
								if (PaymentConsts.PAY_FORT_ONLINE.equalsIgnoreCase(providerName)) {
									ipgRequestDTO.setContactEmail(additionalDetails.getParam1());
								}
								ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);
							}

							strRequestData = ipgRequestResultsDTO.getRequestData();
							postPaymentDTO.setIpgRequestDTO(ipgRequestDTO);
							postPaymentDTO.setPaymentGateWay(paymentGateway);
							postPaymentDTO.setPnr(ipgPNR);
							postPaymentDTO.setIpgRefenceNo(ipgRequestResultsDTO.getAccelAeroTransactionRef());
							postPaymentDTO.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(temporyPayId,
									ipgRequestResultsDTO.getPaymentBrokerRefNo(), tempPayMap));

							// For Canceling Tap Offline previous payment details
							if (isBalancePayment && !alreadyCancelledPreviousTransaction) {
								cancelPreviousOfflineTransaction(postPaymentDTO.getPnr());
							}

							// AmeriaBank handling
							if (ipgRequestResultsDTO.getAmeriaBankSessionInfo() != null) {
								if (ipgRequestResultsDTO.getAmeriaBankSessionInfo().getError() != null) {
									log.error(ipgRequestResultsDTO.getAmeriaBankSessionInfo().getError());
									ModuleException me = new ModuleException("payment.api.ameriabank.payment.failed");
									me.setErrorType(ErrorType.PAYMENT_ERROR);
									throw me;
								}

								postPaymentDTO.setAmeriaBankSessionInfo(ipgRequestResultsDTO.getAmeriaBankSessionInfo());
							}
							// if PG return XML response
							if (responseTypeXML != null && !responseTypeXML.equals("") && responseTypeXML.equals("true")) {

								log.debug("Execute the payment and get the response for XML response type PGWs");

								XMLResponseDTO response = paymentBrokerBD.getXMLResponse(ipgIdentificationParamsDTO,
										ipgRequestResultsDTO.getPostDataMap());

								log.debug("Recieved XMLresponse");
								List<Integer> tptIds = new ArrayList<Integer>();
								tptIds.add(temporyPayId);

								// if 3D Secure (status 46), confirmation to be handled at ConfirmPaymentController
								if (response != null && Integer.valueOf(response.getStatus()) == 46) {

									log.debug("3D Secure payment: PNR:" + ipgPNR + "SessionID :");

									makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
									makePaymentRS.setSwitchToExternal(true);

									externalPGDetails.setPostInputData(response.getHtml());
									externalPGDetails.setRedirectionURL(null);
									postPaymentDTO.setSecurePayment3D(true);

								}
								// if MOTO and XML response exists build confirmation response
								else if (response != null && !response.getStatus().equals("")) {
									IPGHandlerRQ ipgHandlerRequest = new IPGHandlerRQ();
									ipgHandlerRequest.setIpgResponse(response.toString());
									ipgHandlerRequest.setPostPaymentDTO(postPaymentDTO);
									ipgHandlerRequest.setOperationType(paymentBaseSessionStore.getOperationType());
									ipgHandlerRequest.setAlias(ipgRequestDTO.getAlias());
									ipgHandlerRequest.setCardToBeSaved(ipgRequestDTO.isSaveCreditCard());
									postPaymentDTO.setXmlResponse(response);

									PaymentConfirmationRS ipgHandlerResponse = confirmPaymentService.processPaymentReceipt(
											makePaymentRequest.getTransactionId(), ipgHandlerRequest, trackInfoDto);

									tempPayMap.get(temporyPayId).setPaymentSuccess(true);
									adaptMakePaymentResponse(makePaymentRS, ipgHandlerResponse);
									makePaymentRS.setExternalPGDetails(null);

								} else {

									log.info("PAYMENT ERROR");
									makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);

								}
								paymentProcessed = true;
							} else {
								log.info("RESPONSE TYPE IS NOT XML");
							}

							// limited to single card, break at the end of the first iteration
							break;
						}

						if (!paymentProcessed) {

							if (PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL.equals(brokerType)) {
								log.info("BROKER_TYPE_EXTERNAL ");
								makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
								makePaymentRS.setSwitchToExternal(true);

								addPostParamData(requestMethod, externalPGDetails, strRequestData);

							} else if (PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_3DEXTERNAL
									.equals(brokerType)) {

								log.info("BROKER_TYPE_INTERNAL_3DEXTERNAL ");
								makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
								makePaymentRS.setSwitchToExternal(true);

								if (viewPaymentInIFrame && !isSwitchToExternalURL) {
									externalPGDetails.setDisplayBanners(true);
								}

								addPostParamData(requestMethod, externalPGDetails, strRequestData);

							}
							log.debug("Interline paymentgateway External Rediraction Info:: RequestData :" + strRequestData
									+ " Transaction ID:" + makePaymentRequest.getTransactionId() + " Session ID:"
									+ trackInfoDto.getSessionId());
						}

						// limited to single PG, break at the end of the first iteration
						break;
					}
				}

			}

		} catch (ModuleException me) {
			revertLoyaltyRedemption(paymentBaseSessionStore, paymentBaseSessionStore.getPostPaymentInformation().getPnr());
			log.error("ModuleException @ processPaymentRequest " + me.getExceptionCode());
			ModuleException modEx = new ModuleException(me, me.getExceptionCode());
			if (ErrorType.PAYMENT_ERROR == me.getErrorType()) {
				modEx.setErrorType(ErrorType.PAYMENT_ERROR);
			}
			throw modEx;
		} catch (Exception e) {
			revertLoyaltyRedemption(paymentBaseSessionStore, paymentBaseSessionStore.getPostPaymentInformation().getPnr());
			log.error("Exception @ processPaymentRequest " + e.getMessage());
			throw new ModuleException(e, "error while processing payment");
		}

		return makePaymentRS;
	}

	private LCCClientReservation getClientReservationDto(String voucherId, MakePaymentRQ makePaymentRequest,
			IPGRequestDTO ipgRequestDTO, PaymentBaseSessionStore paymentBaseSessionStore) {
		LCCClientReservation dto = new LCCClientReservation();
		dto.setFatourati(voucherId);
		CommonReservationContactInfo contactInfo = new CommonReservationContactInfo();
		contactInfo.setFirstName(makePaymentRequest.getContactInfo().getFirstName());
		contactInfo.setLastName(makePaymentRequest.getContactInfo().getLastName());
		contactInfo.setEmail(makePaymentRequest.getContactInfo().getEmailAddress());
		dto.setContactInfo(contactInfo);
		if (ipgRequestDTO.getTravelDTO() != null && ipgRequestDTO.getTravelDTO().getTravelSegments() != null) {
			for (TravelSegmentDTO segmentInfo : ipgRequestDTO.getTravelDTO().getTravelSegments()) {
				dto.addSegment(getSegmentInfo(segmentInfo));
			}
		}
		dto.setPNR(makePaymentRequest.getPnr());
		dto.setTotalTicketPrice(new BigDecimal(ipgRequestDTO.getAmount()));
		if (paymentBaseSessionStore.getPostPaymentInformation() != null
				&& paymentBaseSessionStore.getPostPaymentInformation().getBookingRQ() != null) {
			for (Passenger passenger : paymentBaseSessionStore.getPostPaymentInformation().getBookingRQ().getPaxInfo()) {
				dto.addPassenger(getReservationPax(passenger));
			}
		}
		dto.setZuluBookingTimestamp(new Date());
		return dto;
	}

	private LCCClientReservationPax getReservationPax(Passenger passenger) {
		LCCClientReservationPax pax = new LCCClientReservationPax();
		pax.setFirstName(passenger.getFirstName());
		pax.setLastName(passenger.getLastName());
		pax.setTitle(passenger.getTitle());
		return pax;
	}

	private LCCClientReservationSegment getSegmentInfo(TravelSegmentDTO segmentInfo) {
		LCCClientReservationSegment segment = new LCCClientReservationSegment();
		segment.setSegmentCode(segmentInfo.getDepartureAirportCode() + "/" + segmentInfo.getArrivalAirportCode());
		segment.setDepartureDate(segmentInfo.getFlightDate());
		return segment;
	}

	private void preValidationForExistingPayfort(MakePaymentRQ makePaymentRequest, TrackInfoDTO trackInfoDto)
			throws ModuleException {

		if (StringUtil.isNullOrEmpty(makePaymentRequest.getPnr())) {

			Iterator<PaymentGateway> pgIterator = makePaymentRequest.getPaymentOptions().getPaymentGateways().iterator();

			while (pgIterator.hasNext()) {
				PaymentGateway paymentGateway = pgIterator.next();
				if (PaymentConsts.PAYFORT_PAY_AT_STORE_PG.contentEquals(paymentGateway.getProviderName())
						|| PaymentConsts.PAYFORT_PAY_AT_HOME_PG.contentEquals(paymentGateway.getProviderName())) {

					String tnxVal = String.valueOf(paymentGateway.getPaymentAmount().doubleValue());
					PayfortCheckRS payfortCheckRS = buildPayfortCheckRS(makePaymentRequest.getPnr(), tnxVal, trackInfoDto);
					if (payfortCheckRS != null) {
						if (payfortCheckRS.isVoucherAvailable()) {
							throw new ModuleException("pay.gateway.error.payfort.voucher.error.message");
						}
						if (payfortCheckRS.isTrackingNumberAvailable()) {
							throw new ModuleException("pay.gateway.error.payfort.payAtHome.TrackNum.error.message ");
						}
					}

				}
			}

		}

	}

	private PaymentGateway adaptPaymentGateway(IPGPaymentOptionDTO tmpIPGPaymentOptionDTO, Properties ipgProps)
			throws ModuleException {
		PaymentGatewayAdaptor paymentGatewayAdapter = new PaymentGatewayAdaptor(ipgProps);
		return paymentGatewayAdapter.adapt(tmpIPGPaymentOptionDTO);

	}

	private List<CountryPaymentCardBehaviourDTO> getPGActiveCards(int paymentGatewayId,
			Collection<CountryPaymentCardBehaviourDTO> cardsInfo) {

		List<CountryPaymentCardBehaviourDTO> filteredCardsInfo = new ArrayList<CountryPaymentCardBehaviourDTO>();

		if (cardsInfo != null && !cardsInfo.isEmpty()) {
			for (CountryPaymentCardBehaviourDTO cardInfo : cardsInfo) {
				if (paymentGatewayId == cardInfo.getPaymentGateWayID()) {

					filteredCardsInfo.add(cardInfo);
				}

			}
		}

		return filteredCardsInfo;
	}

	private PaymentOptionsRS composePaymentOptionsResponse(ComposePaymentDTO paymentDTO) throws ModuleException {
		PaymentSessionStore paymentSessionStore = paymentDTO.getPaymentSessionStore();

		String pnr = paymentDTO.getPnr();
		TrackInfoDTO trackInfoDTO = paymentDTO.getTrackInfoDTO();
		LmsCredits lmsCredits = paymentDTO.getLmsCredits();
		String transactionId = paymentDTO.getTransactionId();
		List<Passenger> paxInfo = paymentDTO.getPaxInfo();
		String contactEmail = paymentDTO.getContactEmail();
		DiscountRQ discountRQ = paymentDTO.getDiscountRQ();
		boolean isBalancePayment = paymentDTO.isBalancePayment();
		ServiceTaxRQ serviceTaxRQ = paymentDTO.getServiceTaxRQ();
		PayByVoucherInfo payByVoucherInfo = paymentSessionStore.getPayByVoucherInfo();

		boolean isOnHoldPnr = false;
		if (!StringUtil.isNullOrEmpty(pnr)) {
			boolean isGroupPnr = paymentSessionStore.isGroupPNR();
			String reservationStatus = getReservationStatus(pnr, isGroupPnr, trackInfoDTO);
			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)) {
				isOnHoldPnr = true;
			}
		}

		PaymentOptionsRS paymentOptionsRS = new PaymentOptionsRS();
		PaymentOptions paymentOptions = new PaymentOptions();
		LoyaltyInformation loyaltyInformation = paymentSessionStore.getLoyaltyInformation();
		boolean offlinePaymentEnable = false;
		boolean isLmsBlocking = lmsCredits != null;
		boolean isPaidByVouchers = payByVoucherInfo != null;

		BigDecimal totalWithAnciCharges = null;
		BigDecimal payableAmount;
		BigDecimal totalPayableAmount;

		
		if (paymentSessionStore.getTotalAvailableBalance() != null) { // balance payment case
			payableAmount = AccelAeroCalculator.scaleValueDefault(paymentSessionStore.getTotalAvailableBalance());
			totalPayableAmount = payableAmount;
		}

		else { // booking case
			paymentOptionsRS.setHasPromoOption(AppSysParamsUtil.isPromoCodeEnabled());
			TotalPaymentInfo totalPaymentInfo = paymentSessionStore.getTotalPaymentInformation();
			totalPayableAmount = totalPaymentInfo.getTotalTicketPrice();
			totalWithAnciCharges = AccelAeroCalculator.scaleValueDefault(totalPaymentInfo.getTotalWithAncillaryCharges());

			payableAmount = AccelAeroCalculator.scaleValueDefault(calculateAndApplyPromotionsIfAny(totalWithAnciCharges, paxInfo,
					paymentSessionStore, transactionId, trackInfoDTO));

			boolean isAllowOnHold = paymentUtils.isCabinClassAllowOnHold(paymentSessionStore.getFareSegChargeTOForPayment());
			// assuming all the offline PGW doesn't allow partial payments
			offlinePaymentEnable = setOfflinePaymentEnable(paymentSessionStore, isLmsBlocking, isAllowOnHold);

			// cash (only available in booking case) and isLmsBlocking true for effective case
			if (!isLmsBlocking && !isOnHoldPnr && isAllowOnHold && !isPaidByVouchers) {
				paymentOptions.setCash(buildCashOption(paymentSessionStore, contactEmail, paxInfo, trackInfoDTO, serviceTaxRQ));
			}
		}

		BigDecimal cardPayableAmount = payableAmount;

		if (loyaltyInformation != null && loyaltyInformation.isRedeemLoyaltyPoints()) {
			cardPayableAmount = AccelAeroCalculator.subtract(cardPayableAmount, loyaltyInformation.getTotalRedeemedAmount());
			totalPayableAmount = AccelAeroCalculator.subtract(totalPayableAmount, loyaltyInformation.getTotalRedeemedAmount());
		}


		if (isAdminFeeRegulatedAndAppliedForChannel() && !isOnHoldPnr && paymentSessionStore.getTotalAvailableBalance() == null) {
			cardPayableAmount = AccelAeroCalculator.subtract(cardPayableAmount,
					paymentSessionStore.getServiceTaxForTransactionFee());
			payableAmount = AccelAeroCalculator.subtract(payableAmount, paymentSessionStore.getServiceTaxForTransactionFee());
			cardPayableAmount = AccelAeroCalculator.add(cardPayableAmount, deriveCCChgsAndCalculateTotalTransactionFee(
					paymentSessionStore, 0, payableAmount, trackInfoDTO, serviceTaxRQ));
			// update cash amount (already set)

		}

		refreshVoucherRedeemedAmount(transactionId, totalPayableAmount);

		if (payByVoucherInfo != null) {
			cardPayableAmount = AccelAeroCalculator.subtract(cardPayableAmount, payByVoucherInfo.getRedeemedTotal());
			if (payByVoucherInfo.getRedeemedTotal().compareTo(totalPayableAmount) >= 0) {
				paymentSessionStore.getVoucherInformation().setIsTotalAmountPaidFromVoucher(true);
			}
		}

		List<PaymentGateway> paymentGateways = null;
		
		
		if (payByVoucherInfo != null
				&& payByVoucherInfo.getRedeemedTotal().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0
				&& loyaltyInformation.isRedeemLoyaltyPoints()
				&& cardPayableAmount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) <= 0) {
			paymentSessionStore.getVoucherInformation().setTotalPaidFromVoucherLMS(true);
		} else {
			paymentSessionStore.getVoucherInformation().setTotalPaidFromVoucherLMS(false);
		}

		if (!isLmsTotalCase(cardPayableAmount) || payByVoucherInfo != null) {

			BuildGatewaysDTO buildGatewaysDTO = (new BuildGatewaysDTO()).setCardPayableAmount(cardPayableAmount)
					.setOfflinePaymentEnable(offlinePaymentEnable).setPaymentBaseSessionStore(paymentSessionStore)
					.setTrackInfoDTO(trackInfoDTO).setOnHoldPnr(isOnHoldPnr).setPnr(pnr).setServiceTaxRQ(serviceTaxRQ)
					.setBalancePayment(isBalancePayment);

			paymentGateways = buildPaymentGatewayOptions(buildGatewaysDTO);

		}
		
		paymentOptions.setPaymentGateways(paymentGateways);

		// LMS
		String memberAccountId = getMemberAccountId();
		if (!StringUtil.isNullOrEmpty(memberAccountId)) {
			PreferenceInfo preferenceInfo = paymentSessionStore.getPreferencesForPayment();
			SYSTEM searchSystem = SYSTEM.getEnum(preferenceInfo.getSelectedSystem());
			LmsCredits lmsCreditsRedeemed = null;

			if (lmsCredits == null) {
				List<FlightSegmentTO> flightSegments = paymentUtils
						.adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());

				String pnrForLMS = !StringUtil.isNullOrEmpty(pnr) ? pnr : paymentSessionStore.getPnr();
				RedeemCalculateRQ redeemCalculateRQ = null;
				
				try {
					
					redeemCalculateRQ = getRedeemCalculateRQ(totalWithAnciCharges, searchSystem, memberAccountId,
						paymentSessionStore.getFareSegChargeTOForPayment(), paymentSessionStore.getTravellerQuantityForPayment(),
						preferenceInfo.getSeatType(), paymentSessionStore.getPaxAncillaryExternalCharges(), paxInfo,
						flightSegments, pnrForLMS, null, transactionId);
					
				} catch(Exception exception) {
					log.error("redeemCalculateRQ is Null==> LMS", exception);
				}

				if (discountRQ != null && redeemCalculateRQ != null) {
					redeemCalculateRQ.setDiscountRQ(discountRQ);
				}

				if (redeemCalculateRQ != null && (paymentSessionStore.getPaxAncillaryExternalCharges() == null || 
												  paymentSessionStore.getPaxAncillaryExternalCharges().isEmpty())) {
					
					redeemCalculateRQ.setPaxCarrierProductDueAmount(
							ModuleServiceLocator.getAirproxyReservationBD().getPaxProduDueAmount(pnr,
									paymentSessionStore.isGroupPNR(), false, getMemberAvailablePoints(memberAccountId)));
				}

				lmsCreditsRedeemed = getLMSCreditInfo(redeemCalculateRQ, trackInfoDTO);

				if (paymentSessionStore.getTotalAvailableBalance() != null) { // balance payment case
					// in XBE force confirm Ancillary modification (with free services applied) and then
					// IBE balance payment fails to pass discountRQ and calculates maxRedeemable > payable amount
					// this is could only come for balance payment case (LMS criteria request only)
					BigDecimal effectiveMaxRedeemableAmount = (lmsCreditsRedeemed.getMaxRedeemableAmount()
							.doubleValue() > payableAmount.doubleValue())
									? payableAmount
									: lmsCreditsRedeemed.getMaxRedeemableAmount();
					lmsCreditsRedeemed.setMaxRedeemableAmount(effectiveMaxRedeemableAmount);
				}

			} else if (loyaltyInformation != null && loyaltyInformation.isRedeemLoyaltyPoints()) {
				lmsCreditsRedeemed = new LmsCredits();
				lmsCreditsRedeemed.setAvailablePoints(BigDecimal.valueOf(getLMSPointBalance(memberAccountId)));
				lmsCreditsRedeemed.setRedeemedAmount(lmsCredits.getRedeemRequestAmount());
				lmsCreditsRedeemed.setAirRewardId(memberAccountId);
				lmsCreditsRedeemed.setRedeemed(true);
			}

			paymentOptions.setLmsCredits(lmsCreditsRedeemed);
		}

		/* VOUCHER DETAILS */
		paymentOptions.setVoucherOption(getRedeemedVoucherOption(transactionId));

		paymentOptionsRS.setPaymentOptions(paymentOptions);
		paymentOptionsRS.setSuccess(true);

		paymentOptionsRS.setTransactionId(transactionId);

		if (lmsCredits != null && lmsCredits.getRedeemRequestAmount() != null
				&& lmsCredits.getRedeemRequestAmount().doubleValue() < 0.00001) {
			paymentOptionsRS.setMessages(Arrays.asList("requested redeem amount is zero"));
		}

		return paymentOptionsRS;

	}

	private boolean isLmsTotalCase(BigDecimal cardPayableAmount) {
		return cardPayableAmount.doubleValue() <= 0;
	}

	private Cash buildCashOption(PaymentSessionStore paymentSessionStore, String contactEmail, List<Passenger> paxInfo,
			TrackInfoDTO trackInfoDTO, ServiceTaxRQ serviceTaxRQ) throws ModuleException {

		BigDecimal totalWithAnciCharges = paymentSessionStore.getTotalPaymentInformation().getTotalWithAncillaryCharges();
		Cash cash = null;

		if (paxInfo != null && !paxInfo.isEmpty()) {

			String cabinClass = getCabinClass(paymentSessionStore.getSelectedSegmentsForPayment());

			ReservationPaxAdaptor reservationPaxAdaptor = new ReservationPaxAdaptor();

			List<ReservationPax> reservationPaxList = new ArrayList<ReservationPax>();

			AdaptorUtils.adaptCollection(paxInfo, reservationPaxList, reservationPaxAdaptor);

			if (reservationPaxList != null && !reservationPaxList.isEmpty()) { // contact email optional
				cash = getOnHoldDetail(cabinClass, reservationPaxList, contactEmail,
						SYSTEM.getEnum(paymentSessionStore.getPreferencesForPayment().getSelectedSystem()),
						ResOnholdValidationDTO.COMBINE_VALIDATION, paymentSessionStore, trackInfoDTO);

			}

			if (cash != null) {

				if (isAdminFeeRegulatedAndAppliedForChannel()) {
					// deducting service tax for admin fee
					totalWithAnciCharges = AccelAeroCalculator.subtract(totalWithAnciCharges,
							paymentSessionStore.getServiceTaxForTransactionFee());
					totalWithAnciCharges = AccelAeroCalculator.add(totalWithAnciCharges,
							deriveCCChgsAndCalculateTotalTransactionFee(paymentSessionStore, 0, totalWithAnciCharges,
									trackInfoDTO, serviceTaxRQ));
				}
				cash.setPayableAmount(totalWithAnciCharges);
			}

		}

		return cash;
	}

	private boolean setOfflinePaymentEnable(PaymentSessionStore paymentSessionStore, boolean isLmsBlocking, boolean isAllowOnHold)
			throws ModuleException {
		long offlinePaymentOnholdTimeInsec;
		if (isAllowOnHold && !isLmsBlocking) {
			if (isOfflinePaymentEnable(paymentSessionStore.getOperationType())) {
				offlinePaymentOnholdTimeInsec = calculateOnholdExpiryPeriod(paymentSessionStore);
				if (offlinePaymentOnholdTimeInsec > 0) {
					return true;
				}
			}
		}
		return false;
	}

	private Double getLMSPointBalance(String memberAccountId) {

		String memberExternalId = getMemberExternalAccountId();
		Double lmsPointBalance = null;
		if (memberAccountId != null) {
			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			LmsMember lmsMember = null;
			try {
				lmsMember = lmsMemberDelegate.getLmsMember(memberAccountId);
			} catch (ModuleException me) {
				log.error("PaymentServiceAdapter.getLMSPonts ==> getLmsMember", me);
			}
			if (lmsMember.getEmailConfirmed() == 'Y') {
				LoyaltyPointDetailsDTO loyaltyPointDetailsDTO;
				try {
					loyaltyPointDetailsDTO = loyaltyManagementBD.getLoyaltyMemberPointBalances(memberAccountId, memberExternalId);
					lmsPointBalance = loyaltyPointDetailsDTO.getMemberPointsAvailable();
				} catch (ModuleException me) {
					log.error("PaymentServiceAdapter.getLMSPonts ==> getLoyaltyMemberPointBalances", me);
				}
			}
		}
		return lmsPointBalance;
	}

	private PaymentOptionsRS composeRequotePaymentOptionsResponse(ComposeRequotePaymentDTO paymentDTO) throws ModuleException {
		PaymentRequoteSessionStore paymentRequoteSessionStore = paymentDTO.getPaymentRequoteSessionStore();
		TrackInfoDTO trackInfoDTO = paymentDTO.getTrackInfoDTO();
		LmsCredits lmsCredits = paymentDTO.getLmsCredits();
		String transactionId = paymentDTO.getTransactionId();
		// get total, card payable
		BigDecimal balanceToPay = paymentRequoteSessionStore.getBalanceToPayAmount();
		ReservationInfo resInfo = paymentRequoteSessionStore.getResInfo();
		BigDecimal totalPayableAmount = balanceToPay;
		BigDecimal cardPayableAmount = balanceToPay;
		
		if (paymentRequoteSessionStore.getLoyaltyInformation() != null
				&& paymentRequoteSessionStore.getLoyaltyInformation().isRedeemLoyaltyPoints()) {
			cardPayableAmount = AccelAeroCalculator.subtract(balanceToPay,
					paymentRequoteSessionStore.getLoyaltyInformation().getTotalRedeemedAmount());
			totalPayableAmount = AccelAeroCalculator.subtract(totalPayableAmount, paymentRequoteSessionStore
					.getLoyaltyInformation().getTotalRedeemedAmount());
		}

		String memberAccountId = getMemberAccountId();

		boolean offlinePaymentEnable = false;

		PaymentOptions paymentOptions = new PaymentOptions();

		ServiceTaxRQ serviceTaxRq = new ServiceTaxRQ();
		serviceTaxRq.setCountryCode(resInfo.getContactInfo().getCountryCode());
		serviceTaxRq.setStateCode(resInfo.getContactInfo().getState());
		serviceTaxRq.setTransactionId(transactionId);
		serviceTaxRq.setTaxRegNo(resInfo.getContactInfo().getTaxRegNo());

		if (isAdminFeeRegulatedAndAppliedForChannel()) {
			BigDecimal adminFee = deriveCCChgsAndCalculateTotalTransactionFee(
					paymentRequoteSessionStore, 0, balanceToPay, trackInfoDTO, serviceTaxRq);
			cardPayableAmount = AccelAeroCalculator.add(cardPayableAmount, adminFee);
			totalPayableAmount = AccelAeroCalculator.add(totalPayableAmount, adminFee);
		}
		
		refreshVoucherRedeemedAmount(transactionId, totalPayableAmount);

		if (paymentRequoteSessionStore.getPayByVoucherInfo() != null) {
			cardPayableAmount = AccelAeroCalculator
					.subtract(cardPayableAmount, paymentRequoteSessionStore.getPayByVoucherInfo().getRedeemedTotal());
		}
		
		BuildGatewaysDTO buildGatewaysDTO = (new BuildGatewaysDTO()).setCardPayableAmount(cardPayableAmount)
				.setPaymentBaseSessionStore(paymentRequoteSessionStore).setOfflinePaymentEnable(offlinePaymentEnable)
				.setTrackInfoDTO(trackInfoDTO).setOnHoldPnr(false).setPnr(null).setServiceTaxRQ(serviceTaxRq);

		// Build Payment Gateway Options

		List<PaymentGateway> paymentGateways = buildPaymentGatewayOptions(buildGatewaysDTO);

		paymentOptions.setPaymentGateways(paymentGateways);

		PreferenceInfo preferenceInfo = paymentRequoteSessionStore.getPreferencesForPayment();

		SYSTEM searchSystem = SYSTEM.getEnum(preferenceInfo.getSelectedSystem());

		LmsCredits lmsCreditsRedeemed = null;
		if (!StringUtil.isNullOrEmpty(memberAccountId)) {
			LoyaltyInformation loyaltyInformation = paymentRequoteSessionStore.getLoyaltyInformation();
			if (lmsCredits == null) {
				List<FlightSegmentTO> flightSegments = paymentUtils
						.adaptFlightSegment(paymentRequoteSessionStore.getSelectedSegmentsForPayment());

				RedeemCalculateRQ redeemCalculateRQ = getRedeemCalculateRQ(balanceToPay, searchSystem, memberAccountId,
						paymentRequoteSessionStore.getFareSegChargeTOForPayment(),
						paymentRequoteSessionStore.getTravellerQuantityForPayment(), preferenceInfo.getSeatType(),
						paymentRequoteSessionStore.getPaxAncillaryExternalCharges(), paymentDTO.getPaxInfo(), flightSegments,
						paymentDTO.getPnr(), null, paymentDTO.getTransactionId());

				setPaxCarrierExternalCharges(redeemCalculateRQ, paymentRequoteSessionStore.getOperationType(),
						paymentRequoteSessionStore.getPassengerSummaryList());

				lmsCreditsRedeemed = getLMSCreditInfo(redeemCalculateRQ, trackInfoDTO);

			} else if (loyaltyInformation != null && loyaltyInformation.isRedeemLoyaltyPoints()) {
				lmsCreditsRedeemed = new LmsCredits();
				lmsCreditsRedeemed.setRedeemedAmount(lmsCredits.getRedeemRequestAmount());
				lmsCreditsRedeemed.setAirRewardId(memberAccountId);
				lmsCreditsRedeemed.setRedeemed(true);
			}
		}
		
		/* VOUCHER DETAILS */
		paymentOptions.setVoucherOption(getRedeemedVoucherOption(transactionId));
		
		paymentOptions.setLmsCredits(lmsCreditsRedeemed);

		PaymentOptionsRS paymentOptionsRS = new PaymentOptionsRS();
		paymentOptionsRS.setSuccess(true);
		paymentOptionsRS.setPaymentOptions(paymentOptions);

		paymentOptionsRS.setTransactionId(paymentDTO.getTransactionId());
		if (lmsCredits != null && lmsCredits.getRedeemRequestAmount() != null
				&& lmsCredits.getRedeemRequestAmount().doubleValue() < 0.00001) {
			paymentOptionsRS.setMessages(Arrays.asList("requested redeem amount is zero"));
		}

		return paymentOptionsRS;
	}

	private PaymentOptionsRS composeAnciModificationPaymentOptionsResponse(String pnr, LmsCredits lmsCredits,
			PaymentAncillarySessionStore paymentAncillarySessionStore, boolean isLmsBlocking, List<Passenger> paxInfo,
			TransactionalBaseRQ baseRQ, TrackInfoDTO trackInfoDTO, String transactionId) throws ModuleException {

		BigDecimal balanceToPay = getBalanceAmountForAnciModification(baseRQ, trackInfoDTO);

		BigDecimal removedCharges = getRemovedCharges(baseRQ, trackInfoDTO);

		BigDecimal totalPayableAmount = balanceToPay;
		
		BigDecimal cardPayableAmount = balanceToPay;


		if (paymentAncillarySessionStore.getLoyaltyInformation() != null
				&& paymentAncillarySessionStore.getLoyaltyInformation().isRedeemLoyaltyPoints()) {
			cardPayableAmount = AccelAeroCalculator.subtract(balanceToPay,
					paymentAncillarySessionStore.getLoyaltyInformation().getTotalRedeemedAmount());
			totalPayableAmount = AccelAeroCalculator.subtract(totalPayableAmount, paymentAncillarySessionStore
					.getLoyaltyInformation().getTotalRedeemedAmount());

		}

		String memberAccountId = getMemberAccountId();

		boolean offlinePaymentEnable = false;

		PaymentOptions paymentOptions = new PaymentOptions();

		CommonReservationContactInfo contactInfo = paymentAncillarySessionStore.getResInfo().getContactInfo();

		ServiceTaxRQ serviceTaxRQ = new ServiceTaxRQ();
		serviceTaxRQ.setTransactionId(transactionId);
		serviceTaxRQ.setCountryCode(contactInfo.getCountryCode());
		serviceTaxRQ.setStateCode(contactInfo.getState());
		serviceTaxRQ.setTaxRegNo(contactInfo.getTaxRegNo());

		if (isAdminFeeRegulatedAndAppliedForChannel()) {
			BigDecimal adminFee = deriveCCChgsAndCalculateTotalTransactionFee(paymentAncillarySessionStore, 0, balanceToPay, trackInfoDTO,
					serviceTaxRQ);
			cardPayableAmount = AccelAeroCalculator.add(cardPayableAmount, adminFee);
			totalPayableAmount = AccelAeroCalculator.add(totalPayableAmount, adminFee);

		}
		
		refreshVoucherRedeemedAmount(transactionId, totalPayableAmount);
		
		if (paymentAncillarySessionStore.getPayByVoucherInfo() != null) {
			cardPayableAmount = AccelAeroCalculator
					.subtract(cardPayableAmount, paymentAncillarySessionStore.getPayByVoucherInfo().getRedeemedTotal());
			if (paymentAncillarySessionStore.getPayByVoucherInfo().getRedeemedTotal().compareTo(balanceToPay) >= 0) {
				paymentAncillarySessionStore.getVoucherInformation().setIsTotalAmountPaidFromVoucher(true);
			}
		}

		BuildGatewaysDTO buildGatewaysDTO = (new BuildGatewaysDTO()).setCardPayableAmount(cardPayableAmount)
				.setPaymentBaseSessionStore(paymentAncillarySessionStore).setOfflinePaymentEnable(offlinePaymentEnable)
				.setTrackInfoDTO(trackInfoDTO).setOnHoldPnr(false).setPnr(null).setServiceTaxRQ(serviceTaxRQ);

		// Build Payment Gateway Options
		List<PaymentGateway> paymentGateways = buildPaymentGatewayOptions(buildGatewaysDTO);

		paymentOptions.setPaymentGateways(paymentGateways);
		PreferenceInfo preferenceInfo = paymentAncillarySessionStore.getPreferencesForPayment();

		SYSTEM searchSystem = SYSTEM.getEnum(preferenceInfo.getSelectedSystem());

		LmsCredits lmsCreditsRedeemed = null;
		if (!StringUtil.isNullOrEmpty(memberAccountId)) {
			LoyaltyInformation loyaltyInformation = paymentAncillarySessionStore.getLoyaltyInformation();
			if (lmsCredits == null) {
				List<FlightSegmentTO> flightSegments = paymentUtils
						.adaptFlightSegment(paymentAncillarySessionStore.getSelectedSegmentsForPayment());

				RedeemCalculateRQ redeemCalculateRQ = getRedeemCalculateRQ(balanceToPay, searchSystem, memberAccountId,
						paymentAncillarySessionStore.getFareSegChargeTOForPayment(),
						paymentAncillarySessionStore.getTravellerQuantityForPayment(), preferenceInfo.getSeatType(),
						paymentAncillarySessionStore.getPaxAncillaryExternalCharges(), paxInfo, flightSegments, pnr,
						removedCharges, transactionId);

				lmsCreditsRedeemed = getLMSCreditInfo(redeemCalculateRQ, trackInfoDTO);

			} else if (loyaltyInformation != null && loyaltyInformation.isRedeemLoyaltyPoints()) {
				lmsCreditsRedeemed = new LmsCredits();
				lmsCreditsRedeemed.setRedeemedAmount(lmsCredits.getRedeemRequestAmount());
				lmsCreditsRedeemed.setAirRewardId(memberAccountId);
				lmsCreditsRedeemed.setRedeemed(true);
			}
		}
		/* VOUCHER DETAILS */
		paymentOptions.setVoucherOption(getRedeemedVoucherOption(transactionId));

		paymentOptions.setLmsCredits(lmsCreditsRedeemed);

		PaymentOptionsRS paymentOptionsRS = new PaymentOptionsRS();
		paymentOptionsRS.setSuccess(true);
		paymentOptionsRS.setPaymentOptions(paymentOptions);

		paymentOptionsRS.setTransactionId(transactionId);

		if (lmsCredits != null && lmsCredits.getRedeemRequestAmount() != null
				&& lmsCredits.getRedeemRequestAmount().doubleValue() < 0.00001) {
			paymentOptionsRS.setMessages(Arrays.asList("requested redeem amount is zero"));
		}

		return paymentOptionsRS;
	}

	private BigDecimal applyPromotionsIfAny(BigDecimal applicableTotal, PaymentSessionStore paymentSessionStore) {

		// this wont deduct BIN promotion discounts, it's handled separately as it's (card-number + PG) specific
		DiscountedFareDetails discountedFareDetails = paymentSessionStore.getDiscountedFareDetails();
		BigDecimal discountAmount = null;
		if (paymentSessionStore.getReservationDiscountDTO() != null) {

			discountAmount = paymentSessionStore.getReservationDiscountDTO().getTotalDiscount();
		}

		if (discountedFareDetails == null || discountAmount == null || discountAmount.doubleValue() <= 0) {

			return applicableTotal;
		}

		return getDiscountedTotal(applicableTotal, discountedFareDetails, discountAmount);

	}

	private BigDecimal calculateAndApplyPromotionsIfAny(BigDecimal applicableTotal, List<Passenger> paxInfo,
			PaymentSessionStore paymentSessionStore, String transactionId, TrackInfoDTO trackInfoDTO) throws ModuleException {

		// this wont deduct BIN promotion discounts, it's handled separately as it's (card-number + PG) specific
		DiscountedFareDetails discountedFareDetails = paymentSessionStore.getDiscountedFareDetails();

		if (discountedFareDetails == null || paxInfo == null) {
			return applicableTotal;
		}

		ReservationDiscountDTO reservationDiscountDTO = buildReservationDiscountDTO(paxInfo, paymentSessionStore, trackInfoDTO,
				discountedFareDetails, transactionId);

		// store reservationDiscountDTO to the session
		// booking controller could use this (or should it calculate again?), revisit
		paymentSessionStore.storeReservationDiscountDTO(reservationDiscountDTO);

		return getDiscountedTotal(applicableTotal, discountedFareDetails, reservationDiscountDTO.getTotalDiscount());
	}

	private ReservationDiscountDTO buildReservationDiscountDTO(List<Passenger> paxInfo, PaymentSessionStore paymentSessionStore,
			TrackInfoDTO trackInfoDTO, DiscountedFareDetails discountedFareDetails, String transactionId) throws ModuleException {

		ReservationDiscountDTO reservationDiscountDTO = null;

		QuotedFareRebuildDTO quotedFareRebuildDTO = getQuotedFareRebuildDTO(paymentSessionStore, discountedFareDetails);

		DiscountRQ discountRQ = buildDiscountRQ(paxInfo, discountedFareDetails, quotedFareRebuildDTO, false, paymentSessionStore,
				transactionId);

		if (discountRQ != null) {
			reservationDiscountDTO = ReservationModuleUtils.getAirproxyReservationBD().calculatePromotionDiscount(discountRQ,
					trackInfoDTO);
		}

		return reservationDiscountDTO;
	}

	private QuotedFareRebuildDTO getQuotedFareRebuildDTO(PaymentBaseSessionStore paymentBaseSessionStore,
			DiscountedFareDetails discountedFareDetails) {

		List<FareSegChargeTO> fareSegChargeTOs = new ArrayList<FareSegChargeTO>();

		fareSegChargeTOs.add(paymentBaseSessionStore.getFareSegChargeTOForPayment());

		IPaxCountAssembler paxAssm = paymentUtils.getIPaxCountAssembler(paymentBaseSessionStore.getTravellerQuantityForPayment());

		QuotedFareRebuildDTO quotedFareRebuildDTO = new QuotedFareRebuildDTO(fareSegChargeTOs, paxAssm, discountedFareDetails,
				paymentBaseSessionStore.getPreferencesForPayment().getSeatType());
		return quotedFareRebuildDTO;
	}

	private BigDecimal getDiscountedTotal(BigDecimal applicableTotal, DiscountedFareDetails discountedFareDetails,
			BigDecimal discountAmount) {

		if (discountedFareDetails != null && discountAmount != null && discountAmount.doubleValue() > 0
				&& PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY.equals(discountedFareDetails.getDiscountAs())) {

			applicableTotal = AccelAeroCalculator.subtract(applicableTotal, discountAmount);
		}

		return applicableTotal;
	}

	private List<PaymentGateway> buildPaymentGatewayOptions(BuildGatewaysDTO buildGatewaysDTO) throws ModuleException {

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		PaymentBaseSessionStore sessionStore = buildGatewaysDTO.getPaymentBaseSessionStore();
		String pnr = buildGatewaysDTO.getPnr();
		ServiceTaxRQ serviceTaxRQ = buildGatewaysDTO.getServiceTaxRQ();
		boolean isBalancePayment = buildGatewaysDTO.isBalancePayment();
		String moduleCode = CommonServiceUtil.getApplicationEngine(buildGatewaysDTO.getTrackInfoDTO().getAppIndicator())
				.toString();

		SessionFlightSegment firstFlightSeg = buildGatewaysDTO.getPaymentBaseSessionStore().getSelectedSegmentsForPayment()
				.get(0);
		String firstSegOndCode = (firstFlightSeg == null) ? null : firstFlightSeg.getSegmentCode();
		boolean isOnHoldPnr = buildGatewaysDTO.isOnHoldPnr();
		TrackInfoDTO trackInfoDTO = buildGatewaysDTO.getTrackInfoDTO();
		BigDecimal cardPayableAmount = buildGatewaysDTO.getCardPayableAmount();

		int payablePaxCount = paymentUtils.getPayablePaxCount(sessionStore.getTravellerQuantityForPayment());
		int segmentCount = paymentUtils.getSegmentCount(sessionStore.getSelectedSegmentsForPayment());
		int operationType = sessionStore.getOperationType().getCode();

		IPGPaymentOptionDTO paymentOptionDTO = (new IPGPaymentOptionDTO()).setSelCurrency(AppSysParamsUtil.getDefaultPGCurrency())
				.setOfflinePayment(buildGatewaysDTO.isOfflinePaymentEnable()).setModuleCode(moduleCode)
				.setFirstSegmentONDCode(firstSegOndCode);

		Collection<IPGPaymentOptionDTO> ipgPaymentGateways = getIPGPaymentGateways(paymentOptionDTO);

		if (isOnHoldPnr && AppSysParamsUtil.isOnHoldEnable(OnHold.IBEOFFLINEPAYMENT)) {
			addOnholdCaseIPGs(ipgPaymentGateways, AppSysParamsUtil.getDefaultPGCurrency(), trackInfoDTO.getAppIndicator(), pnr,
					cardPayableAmount, isBalancePayment);
		}

		Collection<Integer> paymentGatewayIds = paymentUtils.getPaymentGatewayIds(ipgPaymentGateways);

		String countryCode = getCountryCode(buildGatewaysDTO.getTrackInfoDTO().getIpAddress(),
				sessionStore.getOriginCountryCodeForTest());

		boolean isOndWisePayment = AppSysParamsUtil.getIsONDWisePayment();

		Collection<CountryPaymentCardBehaviourDTO> cardsInfo = isOndWisePayment
				? getONDWiseCountryCardBehavior(firstSegOndCode, paymentGatewayIds)
				: getCountryPaymentCardBehavior(countryCode, paymentGatewayIds);

		List<PaymentGateway> paymentGateways = new ArrayList<PaymentGateway>();

		if (!isAdminFeeRegulated()) {
			sessionStore.removePaymentCharge(EXTERNAL_CHARGES.CREDIT_CARD);
			sessionStore.removePaymentCharge(EXTERNAL_CHARGES.JN_OTHER);
		}

		// per PGW, add card list, calculate PGW fees
		for (IPGPaymentOptionDTO tmpIPGPaymentOptionDTO : ipgPaymentGateways) {

			String baseCurrency = tmpIPGPaymentOptionDTO.getBaseCurrency();
			int paymentGatewayId = tmpIPGPaymentOptionDTO.getPaymentGateway();

			Properties ipgProps = paymentUtils.getIPGProperties(paymentGatewayId, baseCurrency);
			PaymentGateway paymentGateway = adaptPaymentGateway(tmpIPGPaymentOptionDTO, ipgProps);
			if (paymentGateway == null) {
				continue;
			}

			List<CountryPaymentCardBehaviourDTO> pgCards = getPGActiveCards(paymentGatewayId, cardsInfo);
			if (pgCards == null || pgCards.isEmpty()) {
				continue;
			}

			// FIXME hack, in old IBE QIWI offline is removed in front end.
			// as the availability at modify flow is set at the the PG level at the backend, couldn't determine card
			// wise availablility, TODO backend should be refactored to flag flow based card availability
			if (PaymentConsts.QIWI.contentEquals(tmpIPGPaymentOptionDTO.getProviderName())
					&& OperationTypes.MAKE_ONLY != sessionStore.getOperationType()) {
				removeQIWIOfflineInNonCreateFlows(pgCards);
			}

			CardAdaptor reservationPaxAdaptor = new CardAdaptor();
			List<Card> cards = new ArrayList<Card>();
			AdaptorUtils.adaptCollection(pgCards, cards, reservationPaxAdaptor);
			paymentGateway.setCards(cards);

			CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(baseCurrency,
					CommonServiceUtil.getApplicationEngine(appIndicator));

			BigDecimal effectivePaymentAmount = buildGatewaysDTO.getCardPayableAmount();

			if (!isAdminFeeRegulated()) {
				BigDecimal ccFeeServiceTax = AccelAeroCalculator.getDefaultBigDecimalZero();
				// balanceToPay amount is used to calculate transaction fee
				ExternalChgDTO ccChgDTO = deriveCCChgDTO(sessionStore, paymentGatewayId, cardPayableAmount);
				ccFeeServiceTax = calculateServiceTaxForCCcharge(serviceTaxRQ, ccChgDTO, trackInfoDTO, false);

				ExternalChgDTO ccFeeTaxChgDTO = deriveCCFeeChgDTO(ccChgDTO, sessionStore);

				BigDecimal totalTransactionFee = calculateTransactionFee(ccChgDTO, ccFeeTaxChgDTO);
				totalTransactionFee = AccelAeroCalculator.add(totalTransactionFee, ccFeeServiceTax);

				paymentGateway.setTransactionFee(getAmountObj(totalTransactionFee, currencyExchangeRate, baseCurrency));

				effectivePaymentAmount = AccelAeroCalculator.add(cardPayableAmount, totalTransactionFee);

			}

			paymentGateway.setEffectivePaymentAmount(getAmountObj(effectivePaymentAmount, currencyExchangeRate, baseCurrency));

			paymentGateways.add(paymentGateway);

		}

		return orderByPaymentMethod(paymentGateways, cardsInfo);
	}

	private List<PaymentGateway> orderByPaymentMethod(List<PaymentGateway> paymentGateways,
			Collection<CountryPaymentCardBehaviourDTO> cardsInfo) {

		List<PaymentGateway> paymentGatewaysOrderByPaymentMethod = new ArrayList<>();
		Set<Integer> addedList = new HashSet<>();

		for (CountryPaymentCardBehaviourDTO cardBehaviourDTO : cardsInfo) {

			for (PaymentGateway paymentGateway : paymentGateways) {

				if (addedList.contains(cardBehaviourDTO.getPaymentGateWayID())) {
					break;
				}

				if (cardBehaviourDTO.getPaymentGateWayID() == paymentGateway.getGatewayId()) {
					paymentGatewaysOrderByPaymentMethod.add(paymentGateway);
					addedList.add(paymentGateway.getGatewayId());
					break;
				}
			}
		}

		return paymentGatewaysOrderByPaymentMethod;
	}

	/*
	 * private List<PaymentGateway> buildPaymentGatewayOptions(BigDecimal cardPayableAmount, PaymentBaseSessionStore
	 * paymentBaseSessionStore, boolean offlinePaymentEnable, TrackInfoDTO trackInfoDTO, boolean isOnHoldPnr, String
	 * pnr, ServiceTaxRQ serviceTaxRQ) throws ModuleException { return buildPaymentGatewayOptions(cardPayableAmount,
	 * paymentBaseSessionStore, offlinePaymentEnable, trackInfoDTO, isOnHoldPnr, pnr, serviceTaxRQ, false); }
	 */

	private String getReservationStatus(String pnr, boolean isGroupPnr, TrackInfoDTO trackInfoDTO) throws ModuleException {
		String operatingCarrier = "";
		String sellingCarrier = trackInfoDTO.getCarrierCode();
		boolean isRegisteredUser = isRegisteredUser();
		LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPnr, isRegisteredUser, operatingCarrier,
				sellingCarrier, trackInfoDTO);

		return reservation.getStatus();

	}

	private void addOnholdCaseIPGs(Collection<IPGPaymentOptionDTO> ipgPaymentGateways, String paymentCurrency,
			AppIndicatorEnum appIndicator, String pnr, BigDecimal paymentAmount, boolean isBalancePayment) {

		List<String> pgProviderNames = isBalancePayment
				? PaymentConsts.OFFLINE_PG_OPTIONS_FOR_BALANCE_PAYMENT
				: PaymentConsts.OFFLINE_PG_OPTIONS;

		for (String pgProviderName : pgProviderNames) {
			addToPGList(pgProviderName, ipgPaymentGateways, paymentCurrency, appIndicator, pnr, paymentAmount);
		}

	}

	private void addToPGList(String pgProvider, Collection<IPGPaymentOptionDTO> ipgPaymentGateways, String paymentCurrency,
			AppIndicatorEnum appIndicator, String pnr, BigDecimal paymentAmount) {
		List<IPGPaymentOptionDTO> pgwListForOffline = ModuleServiceLocator.getPaymentBrokerBD()
				.getActivePaymentGatewayByProviderName(pgProvider);
		if (pgwListForOffline.size() > 0) {
			IPGPaymentOptionDTO offlinePgwConfigs;
			if (pgwListForOffline.size() > 1) {
				offlinePgwConfigs = pgwListForOffline.stream()
						.filter(pg -> pg.getModuleCode().equals(CommonServiceUtil.getApplicationEngine(appIndicator).toString()))
						.findFirst().orElse(null);
			} else {
				offlinePgwConfigs = pgwListForOffline.get(0);
			}
			// if (pnr != null && !isPGSelectedBeforeForOfflinePayment(pnr, offlinePgwConfigs.getPaymentGateway(),
			// paymentAmount)) {
			if (pnr != null && offlinePgwConfigs != null) {
				if (!StringUtil.isNullOrEmpty(paymentCurrency)) {
					offlinePgwConfigs.setSelCurrency(paymentCurrency);
				} else {
					offlinePgwConfigs.setSelCurrency(offlinePgwConfigs.getBaseCurrency());
				}
				ipgPaymentGateways.add(offlinePgwConfigs);
			}
		}
	}

	private boolean isPGSelectedBeforeForOfflinePayment(String pnr, int ipgId, BigDecimal paymentAmount) {
		try {
			CreditCardTransaction ccTransaction = ModuleServiceLocator.getPaymentBrokerBD()
					.loadOfflineTransactionByReference(pnr);
			if (ccTransaction != null) {
				String pgName = ccTransaction.getPaymentGatewayConfigurationName();
				String pgId = pgName.split("_")[0];
				if (Integer.parseInt(pgId) == ipgId) {
					BigDecimal previousAmount = ModuleServiceLocator.getPaymentBrokerBD()
							.getPaymentAmountFromTPTId(ccTransaction.getTemporyPaymentId());
					if (paymentAmount == null || previousAmount.compareTo(paymentAmount) == 0) {
						return true;
					}

				}
			}
		} catch (ModuleException e) {
			log.error("Calling remote PaymentBrokerBD error ");
			return false;
		}
		return false;
	}

	private void cancelPreviousOfflineTransaction(String pnr) {
		try {
			CreditCardTransaction ccTransaction = ModuleServiceLocator.getPaymentBrokerBD()
					.loadOfflineTransactionByReference(pnr);
			if (ccTransaction != null) {
				String ipgName = ccTransaction.getPaymentGatewayConfigurationName();
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgName);
				CreditCardPayment creditCardPayment = new CreditCardPayment();
				creditCardPayment.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
				creditCardPayment.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
				creditCardPayment.setTemporyPaymentId(ccTransaction.getTemporyPaymentId());
				creditCardPayment.setPnr(pnr);
				creditCardPayment.setOfflinePaymentCancel(true);
				ModuleServiceLocator.getPaymentBrokerBD().reverse(creditCardPayment, pnr, appIndicator, TnxModeEnum.SECURE_3D);
			}

		} catch (ModuleException e) {
			log.error("Calling remote PaymentBrokerBD error ");

		}
	}

	private BigDecimal getMaxRedeemAmount(Map<Integer, Map<String, BigDecimal>> paxWiseProductAmounts) {

		BigDecimal maxRedeemAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (paxWiseProductAmounts != null) {
			for (Entry<Integer, Map<String, BigDecimal>> paxEntry : paxWiseProductAmounts.entrySet()) {
				Map<String, BigDecimal> productAmounts = paxEntry.getValue();
				for (BigDecimal amount : productAmounts.values()) {
					if (amount != null) {
						maxRedeemAmount = AccelAeroCalculator.add(maxRedeemAmount, amount);
					}
				}
			}

		}
		return maxRedeemAmount;
	}

	@SuppressWarnings("unchecked")
	private boolean redeemLoyaltyPoints(BigDecimal redeemRequestAmount, PaymentBaseSessionStore paymentBaseSessionStore,
			List<Passenger> paxInfo, TrackInfoDTO trackInfoDTO, String pnr, BigDecimal totalWithAncillaryCharges,
			Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount, DiscountRQ discountRQ,
			BigDecimal removedCharges, LoyaltyInformation loyaltyInformation, String transactionId) throws ModuleException {

		boolean response = false;

		PreferenceInfo preferenceInfo = paymentBaseSessionStore.getPreferencesForPayment();

		SYSTEM searchSystem = SYSTEM.getEnum(preferenceInfo.getSelectedSystem());

		String memberAccountId = getMemberAccountId();
		String memberEnrollingCarrier = getMemberEnrollingCarrier();
		String memberExternalId = getMemberExternalAccountId();

		if (!StringUtil.isNullOrEmpty(memberAccountId) && redeemRequestAmount != null && redeemRequestAmount.doubleValue() > 0) {
			List<FlightSegmentTO> flightSegments = paymentUtils
					.adaptFlightSegment(paymentBaseSessionStore.getSelectedSegmentsForPayment());

			RedeemCalculateRQ redeemCalculateRQ = getRedeemCalculateRQ(redeemRequestAmount, searchSystem, memberAccountId,
					paymentBaseSessionStore.getFareSegChargeTOForPayment(),
					paymentBaseSessionStore.getTravellerQuantityForPayment(), preferenceInfo.getSeatType(),
					paymentBaseSessionStore.getPaxAncillaryExternalCharges(), paxInfo, flightSegments, pnr, removedCharges,
					transactionId);

			if (paxCarrierProductDueAmount != null) {
				redeemCalculateRQ.setPaxCarrierProductDueAmount(paxCarrierProductDueAmount);
				redeemCalculateRQ.setPaxCarrierExternalCharges(null);
			}

			if (OperationTypes.MODIFY_ONLY == paymentBaseSessionStore.getOperationType()
					&& !(paymentBaseSessionStore instanceof AncillaryTransaction)) {
				redeemCalculateRQ.setPaxCarrierExternalCharges(null);
				redeemCalculateRQ.setFareSegChargeTo(null);
			}

			if (discountRQ != null) {
				redeemCalculateRQ.setDiscountRQ(discountRQ);
			}

			redeemCalculateRQ.setMemberEnrollingCarrier(memberEnrollingCarrier);
			redeemCalculateRQ.setMemberExternalId(memberExternalId);

			// first time effective information is called
			if (loyaltyInformation == null) {
				loyaltyInformation = new LoyaltyInformation();
				paymentBaseSessionStore.storeLoyaltyInformation(loyaltyInformation);
			}

			LoyaltyPaymentOption loyaltyOption = LoyaltyPaymentOption.NONE;

			ServiceResponce serviceResponce = calculateLoyaltyPoints(redeemCalculateRQ, trackInfoDTO);

			if (serviceResponce != null && serviceResponce.isSuccess()) {

				Map<Integer, Map<String, Double>> paxWiseProductPoints = (Map<Integer, Map<String, Double>>) serviceResponce
						.getResponseParam(ResponceCodes.ResponseParams.PAX_PRODUCT_POINTS);

				Map<Integer, Map<String, BigDecimal>> paxWiseProductAmount = (Map<Integer, Map<String, BigDecimal>>) serviceResponce
						.getResponseParam(ResponceCodes.ResponseParams.PAX_PRODUCT_AMOUNT);

				Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo = new HashMap<String, LoyaltyPaymentInfo>();

				if (paxWiseProductPoints != null && !paxWiseProductPoints.isEmpty()) {

					if (SYSTEM.AA == searchSystem) {
						Map<String, Double> productTotalRedeemPoints = WebplatformUtil
								.getProductTotalRedeemPoints(paxWiseProductPoints);
						serviceResponce = ModuleServiceLocator.getAirproxyReservationBD().issueLoyaltyRewards(pnr,
								memberAccountId, productTotalRedeemPoints, searchSystem, appIndicator, memberEnrollingCarrier,
								memberExternalId);

					}

					if (serviceResponce.isSuccess()) {

						LoyaltyPaymentInfo loyaltyPaymentInfo = new LoyaltyPaymentInfo();

						BigDecimal totalRedeemedLoyaltyAmount = AirProxyReservationUtil
								.getTotalRedeemedPoints(paxWiseProductAmount);
						loyaltyPaymentInfo.setTotalPayment(totalRedeemedLoyaltyAmount);
						loyaltyPaymentInfo.setPaxProductPaymentBreakdown(paxWiseProductAmount);
						loyaltyInformation.setRedeemLoyaltyPoints(true);
						loyaltyInformation.setTotalRedeemedAmount(totalRedeemedLoyaltyAmount);

						BigDecimal balancePayment = AccelAeroCalculator.subtract(totalWithAncillaryCharges,
								totalRedeemedLoyaltyAmount);
						if (BigDecimal.ZERO.compareTo(balancePayment) >= 0) {
							loyaltyOption = LoyaltyPaymentOption.TOTAL;
						} else {
							loyaltyOption = LoyaltyPaymentOption.PART;
						}

						if (SYSTEM.AA == searchSystem) {
							String[] loyaltyRewardIds = (String[]) serviceResponce
									.getResponseParam(ResponceCodes.ResponseParams.LOYALTY_REWARD_IDS);
							loyaltyPaymentInfo.setLoyaltyRewardIds(loyaltyRewardIds);
							loyaltyPaymentInfo.setMemberAccountId(memberAccountId);
							carrierWiseLoyaltyPaymentInfo.put(trackInfoDTO.getCarrierCode(), loyaltyPaymentInfo);
						} else {
							carrierWiseLoyaltyPaymentInfo = (Map<String, LoyaltyPaymentInfo>) serviceResponce
									.getResponseParam(ResponceCodes.ResponseParams.CARRIER_LOYALTY_PAYMENTS);
						}

						loyaltyInformation.setCarrierWiseLoyaltyPaymentInfo(carrierWiseLoyaltyPaymentInfo);

						loyaltyInformation.setLoyaltyPaymentOption(loyaltyOption);

						addBlockCreditInfoIssueRewards(loyaltyInformation, pnr, trackInfoDTO);

						response = true;

					} else {
						log.error("Error in redeem loyalty points for member: " + memberAccountId);
					}
				}

			} else {
				log.error("Error in calculating redeemable amount for member: " + memberAccountId);
			}

		}

		return response;

	}

	private RedeemCalculateRQ getRedeemCalculateRQ(BigDecimal redeemRequestAmount, SYSTEM selectedType, String memeberAccountId,
			FareSegChargeTO fareSegChargeTO, TravellerQuantity travellerQuantity, String seatType,
			List<PassengerChargeTo<LCCClientExternalChgDTO>> paxAncillaryExternalCharges, List<Passenger> paxInfo,
			List<FlightSegmentTO> flightSegments, String pnr, BigDecimal removedCharges, String transactionId)
			throws ModuleException {

		RedeemCalculateRQ redeemCalculateRQ = null;

		if (!StringUtil.isNullOrEmpty(memeberAccountId) && paxInfo != null && flightSegments != null) {
			FlightPriceRQ flightPriceRQ = createFlightPriceRQ(travellerQuantity, seatType);

			Map<String, List<String>> carrierWiseFlightRPH = getCarrierWiseFlightRPH(flightSegments);

			Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges = paymentUtils
					.populatePaxExternalCharges(paxAncillaryExternalCharges, paxInfo, removedCharges);

			redeemCalculateRQ = new RedeemCalculateRQ();
			redeemCalculateRQ.setFlightPriceRQ(flightPriceRQ);
			redeemCalculateRQ.setPaxCarrierExternalCharges(paxCarrierExternalCharges);
			redeemCalculateRQ.setCarrierWiseFlightRPH(carrierWiseFlightRPH);
			redeemCalculateRQ.setFareSegChargeTo(fareSegChargeTO);
			redeemCalculateRQ.setFlexiQuote(getFlexiQuote(paxAncillaryExternalCharges));
			if (paxCarrierExternalCharges != null) {
				redeemCalculateRQ.setIssueRewardIds(true);
				redeemCalculateRQ.setPayForOHD(false);
			} else if (redeemRequestAmount != null
					&& AccelAeroCalculator.isGreaterThan(redeemRequestAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				redeemCalculateRQ.setPayForOHD(true);
				redeemCalculateRQ.setIssueRewardIds(true);
			} else {
				redeemCalculateRQ.setIssueRewardIds(false);
				redeemCalculateRQ.setPayForOHD(true);
			}
			redeemCalculateRQ.setMemberAccountID(memeberAccountId);
			Double remainingLoyaltyPoints = getMemberAvailablePoints(memeberAccountId);
			if (remainingLoyaltyPoints != null && remainingLoyaltyPoints > 0.0) {
				redeemCalculateRQ.setRedeemRequestAmount(redeemRequestAmount);
			} else {
				redeemCalculateRQ.setRedeemRequestAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			}
			redeemCalculateRQ.setRemainingPoint(remainingLoyaltyPoints);
			redeemCalculateRQ.setPnr(pnr);
			redeemCalculateRQ.setSystem(selectedType);
			redeemCalculateRQ.setTxnIdntifier(transactionId);
		}

		return redeemCalculateRQ;
	}

	private double getRemaningLoyaltyPoints(String loyaltyFFID) throws ModuleException {
		Double availablePoints = null;
		String memberExternalId = getMemberExternalAccountId();

		if (loyaltyFFID != null && !"".equals(loyaltyFFID)) {
			LoyaltyPointDetailsDTO loyaltyPointDetailsDTO = ModuleServiceLocator.getLoyaltyManagementBD()
					.getLoyaltyMemberPointBalances(loyaltyFFID, memberExternalId);

			if (loyaltyPointDetailsDTO != null) {
				availablePoints = loyaltyPointDetailsDTO.getMemberPointsAvailable();
			}
		}

		return availablePoints;
	}

	private void revertLoyaltyRedemption(PaymentBaseSessionStore paymentBaseSessionStore, String pnr) throws ModuleException {

		LoyaltyInformation existingLoyaltyInformation = null;

		if (paymentBaseSessionStore != null) {
			existingLoyaltyInformation = paymentBaseSessionStore.getLoyaltyInformation();
		}

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo = null;

		if (existingLoyaltyInformation != null) {
			carrierWiseLoyaltyPaymentInfo = paymentBaseSessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo();
		}

		String memberAccountId = getMemberAccountId();

		if (memberAccountId != null && !memberAccountId.isEmpty() && carrierWiseLoyaltyPaymentInfo != null
				&& carrierWiseLoyaltyPaymentInfo.values() != null && !carrierWiseLoyaltyPaymentInfo.values().isEmpty()) {
			for (LoyaltyPaymentInfo carrierLoyaltyPaymentInfo : carrierWiseLoyaltyPaymentInfo.values()) {
				if (carrierLoyaltyPaymentInfo != null && carrierLoyaltyPaymentInfo.getLoyaltyRewardIds() != null
						&& carrierLoyaltyPaymentInfo.getLoyaltyRewardIds().length > 0) {
					ModuleServiceLocator.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(pnr,
							carrierLoyaltyPaymentInfo.getLoyaltyRewardIds(), memberAccountId);
				}
			}

			existingLoyaltyInformation.setCarrierWiseLoyaltyPaymentInfo(null);
			existingLoyaltyInformation.setLoyaltyPaymentOption(LoyaltyPaymentOption.NONE);
			existingLoyaltyInformation.setTotalRedeemedAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			existingLoyaltyInformation.setRedeemLoyaltyPoints(false);

		}

	}

	private IPGRequestDTO composeIPGRequestDTO(Card card, IPGIdentificationParamsDTO ipgIdentificationParamsDTO, String ipgPNR,
			int temporyPayId, AdditionalDetails additionalDetails, String paymentAmount, ReservationContact contactInfo,
			TrackInfoDTO trackInfoDto, ClientCommonInfoDTO clientInfoDto, boolean isIntExt,
			List<SessionFlightSegment> sessionFlightSegments, String originCountryCodeForTest, boolean isMobile,
			Integer payCurrencyISODecimalPlaces) throws ModuleException {

		GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
		DateFormat dateFormat = new SimpleDateFormat(PATTERN1);
		IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
		ipgRequestDTO.setDefaultCarrierName(globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME));

		ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);

		ipgRequestDTO.setPaymentMethod(card.getCardName());

		ipgRequestDTO.setAmount(paymentAmount);

		ipgRequestDTO.setCardType(String.valueOf(card.getCardType()));
		ipgRequestDTO.setCardName(card.getCardName());
		ipgRequestDTO.setPayCurrencyISODecimalPlaces(payCurrencyISODecimalPlaces);

		if (isIntExt) {
			ipgRequestDTO.setCardNo(card.getCardNo());
			ipgRequestDTO.setSecureCode(card.getCardCvv());
			ipgRequestDTO.setHolderName(card.getCardHoldersName());
			ipgRequestDTO.setExpiryDate(dateFormat.format(card.getExpiryDate()));
		}

		ipgRequestDTO.setApplicationTransactionId(temporyPayId);

		String RETURN_URL = null;
		if (isMobile) {
			RETURN_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_MOBILE_URL_SUFFIX;
		} else {
			RETURN_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_URL_SUFFIX;

		}
		final String RECEIPT_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + PaymentConsts.RECEIPT_URL_SUFFIX;
		final String STATUS_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + PaymentConsts.STATUS_URL_SUFFIX;
		final String OFFER_URL = globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, trackInfoDto.getCarrierCode());

		ipgRequestDTO.setReturnUrl(RETURN_URL);
		ipgRequestDTO.setStatusUrl(STATUS_URL);
		ipgRequestDTO.setReceiptUrl(RECEIPT_URL);
		ipgRequestDTO.setOfferUrl(OFFER_URL);

		ipgRequestDTO.setApplicationIndicator(trackInfoDto.getAppIndicator());
		ipgRequestDTO.setPnr(ipgPNR);

		// As in current IBE (hard-coded to 2)
		ipgRequestDTO.setNoOfDecimals(2);
		ipgRequestDTO.setRequestedCarrierCode(trackInfoDto.getCarrierCode());
		ipgRequestDTO.setSessionID(trackInfoDto.getSessionId());
		ipgRequestDTO.setContactFirstName(StringUtils.trimToEmpty(contactInfo.getFirstName()));
		ipgRequestDTO.setContactLastName(StringUtils.trimToEmpty(contactInfo.getLastName()));

		if (contactInfo.getAddress() != null) {

			ipgRequestDTO.setContactAddressLine1(StringUtils.trimToEmpty(contactInfo.getAddress().getStreetAddress1()));

			ipgRequestDTO.setContactAddressLine2(StringUtils.trimToEmpty(contactInfo.getAddress().getStreetAddress2()));

			ipgRequestDTO.setContactCity(StringUtils.trimToEmpty(contactInfo.getAddress().getCity()));

			// no state information in the ContactInformation
			ipgRequestDTO.setContactState(StringUtils.trimToEmpty(null));

			ipgRequestDTO.setContactPostalCode(StringUtils.trimToEmpty(contactInfo.getAddress().getZipCode()));
		}

		ipgRequestDTO.setContactCountryCode(StringUtils.trimToEmpty(contactInfo.getCountry()));
		ipgRequestDTO.setContactCountryName(StringUtils.trimToEmpty(contactInfo.getCountry()));

		if (additionalDetails != null) {
			ipgRequestDTO.setContactMobileNumber(StringUtils.trimToEmpty(additionalDetails.getParam2()));
			ipgRequestDTO.setContactPhoneNumber(StringUtils.trimToEmpty(additionalDetails.getParam2()));
		}

		// contact info expected to be not null
		else if (contactInfo.getTelephoneNumber() != null) {
			StringBuilder sb = new StringBuilder();
			sb.append(StringUtils.trimToEmpty(contactInfo.getTelephoneNumber().getCountryCode()));
			sb.append(StringUtils.trimToEmpty(contactInfo.getTelephoneNumber().getAreaCode()));
			sb.append(StringUtils.trimToEmpty(contactInfo.getTelephoneNumber().getNumber()));

			ipgRequestDTO.setContactMobileNumber(sb.toString());
			ipgRequestDTO.setContactPhoneNumber(sb.toString());
		}

		if (additionalDetails != null) {
			ipgRequestDTO.setEmail(StringUtils.trimToEmpty(additionalDetails.getParam1()));
		} else {
			ipgRequestDTO.setEmail(StringUtils.trimToEmpty(contactInfo.getEmailAddress()));
		}

		ipgRequestDTO.setSessionLanguageCode(getPreferredLanguageFromLocale());

		ipgRequestDTO.setUserAgent(clientInfoDto.getBrowser());

		ipgRequestDTO.setUserIPAddress(trackInfoDto.getIpAddress());

		ipgRequestDTO.setIpCountryCode(getCountryCode(trackInfoDto.getIpAddress(), originCountryCodeForTest));

		addTravelData(ipgRequestDTO, sessionFlightSegments);

		return ipgRequestDTO;
	}

	private Double getMemberAvailablePoints(String memberAccountId) throws ModuleException {
		
		Double memberPointsAvailable = AccelAeroCalculator.getDefaultBigDecimalZero().doubleValue();
		String memberExternalId = getMemberExternalAccountId();
		
		if (!StringUtil.isNullOrEmpty(memberAccountId)) {
			
			LoyaltyPointDetailsDTO  loyaltyPointDetailsDTO = ModuleServiceLocator.getLoyaltyManagementBD()
						.getLoyaltyMemberPointBalances(memberAccountId, memberExternalId);			

			if (loyaltyPointDetailsDTO != null) {
				memberPointsAvailable = loyaltyPointDetailsDTO.getMemberPointsAvailable();
			}
		}
		return memberPointsAvailable;
	}

	private void addTravelData(IPGRequestDTO ipgRequestDTO, List<SessionFlightSegment> flightSegments) {

		if (flightSegments != null && ipgRequestDTO != null) {
			List<TravelSegmentDTO> segmentDTOs = new ArrayList<TravelSegmentDTO>();
			TravelSegmentDTO segmentDTO = null;
			Map mapAirports = MapGenerator.getAirportMap();
			String depSegmentCode = null;
			String arrivalSegmentCode = null;
			Date SysteDate = Calendar.getInstance().getTime();
			for (SessionFlightSegment flightSegment : flightSegments) {
				if (SysteDate.compareTo(flightSegment.getDepartureDateTimeZulu()) < 0) {
					segmentDTO = new TravelSegmentDTO();
					segmentDTO.setFlightNumber(Util.extractFlightNumber(flightSegment.getFlightDesignator()));
					segmentDTO.setCarrierCode(AppSysParamsUtil.extractCarrierCode(flightSegment.getFlightDesignator()));
					segmentDTO.setFlightDate(flightSegment.getDepartureDateTime());
					depSegmentCode = Util.getDepartureAirportCode(flightSegment.getSegmentCode());
					segmentDTO.setDepartureAirportCode(depSegmentCode);
					segmentDTO.setDepartureAirportName(String.valueOf(mapAirports.get(depSegmentCode)));
					arrivalSegmentCode = Util.getAirvalAirportCode(flightSegment.getSegmentCode());
					segmentDTO.setArrivalAirportCode(arrivalSegmentCode);
					segmentDTO.setArrivalAirportName(String.valueOf(mapAirports.get(arrivalSegmentCode)));
					segmentDTO.setNoOfStopvers(flightSegment.getSegmentCode().split("/").length - 2);
					segmentDTO.setClassOfService(flightSegment.getCabinClass());
					segmentDTOs.add(segmentDTO);
				}
			}
			TravelDTO travelDTO = new TravelDTO();
			travelDTO.setTravelSegments(segmentDTOs);
			ipgRequestDTO.setTravelDTO(travelDTO);
		}
	}

	private ServiceResponce calculateLoyaltyPoints(RedeemCalculateRQ redeemCalculateRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		ServiceResponce serviceResponce = ModuleServiceLocator.getAirproxyReservationBD()
				.calculateLoyaltyPointRedeemables(redeemCalculateRQ, trackInfoDTO);
		return serviceResponce;
	}

	private BigDecimal convertToPaymentCurrency(BigDecimal effectivePaymentAmount, CurrencyExchangeRate currencyExrate) {
		Currency pgCurrency = currencyExrate.getCurrency();
		return AccelAeroRounderPolicy.convertAndRound(effectivePaymentAmount, currencyExrate.getMultiplyingExchangeRate(),
				pgCurrency.getBoundryValue(), pgCurrency.getBreakPoint());
	}

	private OnHoldReleaseTimeDTO getOnHoldReleaseTimeDTO(List<FlightSegmentTO> flightSegmentTOs,
			Collection<OndFareSegChargeTO> ondFareSegChargeTOs) throws ModuleException {
		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = new OnHoldReleaseTimeDTO();
		onHoldReleaseTimeDTO.setBookingDate(new Date());
		Integer flightSegId = null;
		if (flightSegmentTOs != null && flightSegmentTOs.size() > 0) {
			Collections.sort(flightSegmentTOs);
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (flightSegmentTO != null) {
					String segmentCode = flightSegmentTO.getSegmentCode();
					flightSegId = flightSegmentTO.getFlightSegId();
					if (segmentCode != null) {
						onHoldReleaseTimeDTO.setOndCode(segmentCode);
						onHoldReleaseTimeDTO.setFlightDepartureDate(flightSegmentTO.getDepartureDateTimeZulu());
						onHoldReleaseTimeDTO.setDomestic(flightSegmentTO.isDomesticFlight());
						if (flightSegmentTO.getCabinClassCode() != null) {
							onHoldReleaseTimeDTO.setCabinClass(flightSegmentTO.getCabinClassCode());
						}
						break;
					}
				}
			}

			String bookingClass = OnHoldReleaseTimeDTO.ONHOLD_ANY;
			int fareRuleId = -1;

			if (flightSegId != null && ondFareSegChargeTOs != null && !ondFareSegChargeTOs.isEmpty()) {
				for (OndFareSegChargeTO ondFareSegChargeTO : ondFareSegChargeTOs) {
					fareRuleId = ondFareSegChargeTO.getFareRuleId();
					LinkedList<BookingClassAlloc> bookingClassAllocList = ondFareSegChargeTO.getFsegBCAlloc().get(flightSegId);
					if (bookingClassAllocList != null && !bookingClassAllocList.isEmpty()) {
						for (BookingClassAlloc bookingClassAlloc : bookingClassAllocList) {
							bookingClass = bookingClassAlloc.getBookingClassCode();
							break;
						}
					}
				}
			}

			onHoldReleaseTimeDTO.setBookingClass(bookingClass);
			onHoldReleaseTimeDTO.setFareRuleId(fareRuleId);
		}

		return onHoldReleaseTimeDTO;
	}

	private Date getOnHoldReleaseTime(OnHold appOnHold, Date depDate, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO)
			throws ModuleException {

		if (onHoldReleaseTimeDTO == null) {
			onHoldReleaseTimeDTO = new OnHoldReleaseTimeDTO();
		}
		AppIndicatorEnum appIndicatorEnum = appIndicator;

		boolean enforcePrivilegeCheck = false;
		if (appOnHold == OnHold.KIOSK) {
			appIndicatorEnum = AppIndicatorEnum.APP_KIOSK;
		} else if (appOnHold == OnHold.IBEPAYMENT) {
			appIndicatorEnum = AppIndicatorEnum.APP_IBEPAYMENT;
		} else if (appOnHold == OnHold.IBEOFFLINEPAYMENT) {
			enforcePrivilegeCheck = true;
			appIndicatorEnum = AppIndicatorEnum.APP_OFFLINEPAYMENT;
		} else if (appOnHold == OnHold.IBE) {
			appIndicatorEnum = AppIndicatorEnum.APP_IBE;
		} else if (appOnHold == OnHold.IOS) {
			appIndicatorEnum = AppIndicatorEnum.APP_IOS;
		} else if (appOnHold == OnHold.ANDROID) {
			appIndicatorEnum = AppIndicatorEnum.APP_ANDROID;
		}

		onHoldReleaseTimeDTO.setModuleCode(appIndicatorEnum.toString());

		return ReleaseTimeUtil.getReleaseTime(null, depDate, enforcePrivilegeCheck, null, appIndicatorEnum.toString(),
				onHoldReleaseTimeDTO);
	}

	private long calculateOnholdExpiryPeriod(PaymentSessionStore paymentSessionStore) throws ModuleException {
		long ohdIntervalinSeconds = 0;

		List<FlightSegmentTO> flightSegments = paymentUtils
				.adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());
		Collection<OndFareSegChargeTO> ondFareSegChargeTOs = paymentSessionStore.getFareSegChargeTOForPayment()
				.getOndFareSegChargeTOs();

		if (isOfflinePaymentEnable(paymentSessionStore.getOperationType())) {
			OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = getOnHoldReleaseTimeDTO(flightSegments, ondFareSegChargeTOs);
			Date depDateTimeZulu = paymentUtils.getDepartureTimeZulu(flightSegments);

			Date pnrReleaseTimeZulu = getOnHoldReleaseTime(OnHold.IBEOFFLINEPAYMENT, depDateTimeZulu, onHoldReleaseTimeDTO);

			Date currentDateTime = Calendar.getInstance().getTime();

			if (pnrReleaseTimeZulu != null) {
				ohdIntervalinSeconds = (pnrReleaseTimeZulu.getTime() - currentDateTime.getTime()) / 1000;
			}

		}
		return ohdIntervalinSeconds;
	}

	private boolean isOfflinePaymentEnable(OperationTypes operationTypes) {
		boolean enable = false;
		if (AppSysParamsUtil.isOnHoldEnable(OnHold.IBEOFFLINEPAYMENT) && operationTypes == OperationTypes.MAKE_ONLY) {
			enable = true;
		}

		return enable;
	}

	private PromoSelectionCriteria buildPromotionCriteria(List<FlightSegmentTO> flightSegmentTOs, List<String> bookingClasses,
			List<String> ondList, List<String> logicalCabinClasses, String promoCode, SearchParamsForPromoCheck searchParams,
			String preferedLang, TrackInfoDTO trackInfo) {

		PromoSelectionCriteria promoSelectionCriteria = new PromoSelectionCriteria();
		promoSelectionCriteria.setReservationDate(CalendarUtil.getCurrentSystemTimeInZulu());
		promoSelectionCriteria.setAdultCount(searchParams.getAdultCount());
		promoSelectionCriteria.setChildCount(searchParams.getChildCount());
		promoSelectionCriteria.setInfantCount(searchParams.getInfantCount());
		if (promoCode != null && !promoCode.isEmpty()) {
			promoSelectionCriteria.setPromoCode(promoCode);
		}

		if (searchParams != null && searchParams.getBankId() > 0) {
			promoSelectionCriteria.setBankIdNo(searchParams.getBankId());
		}

		promoSelectionCriteria.setSalesChannel(trackInfo.getOriginChannelId());
		promoSelectionCriteria.setPreferredLanguage(preferedLang);

		if (searchParams.getReturnDate() != null && !searchParams.getReturnDate().isEmpty()) {
			promoSelectionCriteria.setJourneyType(JourneyType.ROUNDTRIP);
		} else {
			promoSelectionCriteria.setJourneyType(JourneyType.SINGLE_SECTOR);
		}

		if (flightSegmentTOs != null) {
			for (FlightSegmentTO flightSegment : flightSegmentTOs) {
				promoSelectionCriteria.getFlights().add(flightSegment.getFlightNumber());
				promoSelectionCriteria.getFlightSegIds().add(flightSegment.getFlightSegId());
				promoSelectionCriteria.getCabinClasses().add(flightSegment.getCabinClassCode());
				promoSelectionCriteria.getFlightSegWiseLogicalCabinClass().put(flightSegment.getFlightSegId(),
						flightSegment.getLogicalCabinClassCode());
			}

			promoSelectionCriteria.setOndFlightDates(PromotionsUtils.getOndFlightDates(flightSegmentTOs));
		}

		promoSelectionCriteria.getBookingClasses().addAll(bookingClasses);

		promoSelectionCriteria.getOndList().addAll(ondList);

		promoSelectionCriteria.getLogicalCabinClasses().addAll(logicalCabinClasses);

		promoSelectionCriteria
				.setDryOperatingAirline(PromotionsUtils.getDryOperatingAirline(flightSegmentTOs, searchParams.getSearchSystem()));

		return promoSelectionCriteria;
	}

	private void setFareDiscountWith(List<PassengerTypeQuantityTO> paxQtyList, ApplicablePromotionDetailsTO promoDetails,
			DiscountedFareDetails discountedFareDetails) {

		if (promoDetails != null) {
			discountedFareDetails.setPromotionId(promoDetails.getPromoCriteriaId());
			discountedFareDetails.setPromotionType(promoDetails.getPromoType());
			discountedFareDetails.setPromoCode(promoDetails.getPromoCode());
			discountedFareDetails.setDescription(promoDetails.getDescription());
			discountedFareDetails.setSystemGenerated(promoDetails.isSystemGenerated());
			discountedFareDetails.setNotes(promoDetails.getPromoName());
			discountedFareDetails.setFarePercentage(promoDetails.getDiscountValue());
			discountedFareDetails.setDiscountType(promoDetails.getDiscountType());
			discountedFareDetails.setDiscountApplyTo(promoDetails.getApplyTo());
			discountedFareDetails.setDiscountAs(promoDetails.getDiscountAs());
			discountedFareDetails.addApplicablePaxCount(PaxTypeTO.ADULT, promoDetails.getApplicableAdultCount());
			discountedFareDetails.addApplicablePaxCount(PaxTypeTO.CHILD, promoDetails.getApplicableChildCount());
			discountedFareDetails.addApplicablePaxCount(PaxTypeTO.INFANT, promoDetails.getApplicableInfantCount());
			discountedFareDetails.setApplicableAncillaries(promoDetails.getApplicableAncillaries());
			discountedFareDetails.setApplicableBINs(promoDetails.getApplicableBINs());
			discountedFareDetails.setApplicableOnds(promoDetails.getApplicableOnds());
			discountedFareDetails.setAllowSamePaxOnly(promoDetails.isAllowSamePaxOnly());
			discountedFareDetails.setOriginPnr(promoDetails.getOriginPnr());
			discountedFareDetails.setApplicableForOneway(promoDetails.isApplicableForOneWay());
			discountedFareDetails.setApplicableForReturn(promoDetails.isApplicableForReturn());
			discountedFareDetails.setApplicability(promoDetails.getDiscountApplicability());

		} else {
			removeFareDiscount(discountedFareDetails);
		}
	}

	private void removeFareDiscount(DiscountedFareDetails discountInfo) {
		discountInfo = null;

	}

	private AmountObject getAmountObj(BigDecimal paymentAmount, CurrencyExchangeRate currencyExrate, String baseCurrency) {
		AmountObject paymentAmountObj = new AmountObject();
		if (paymentAmount != null && paymentAmount.doubleValue() > 0 && currencyExrate != null) {
			if (!AppSysParamsUtil.getBaseCurrency().equals(baseCurrency)) {
				paymentAmountObj.setBaseCurrencyValue(paymentAmount);
				paymentAmountObj.setPaymentCurrencyValue(convertToPaymentCurrency(paymentAmount, currencyExrate));
			} else {
				paymentAmountObj.setBaseCurrencyValue(paymentAmount);
				paymentAmountObj.setPaymentCurrencyValue(paymentAmount);
			}

		}

		return paymentAmountObj;
	}

	private MakePaymentRS adaptMakePaymentResponse(MakePaymentRS makePaymentResponse, PaymentConfirmationRS ipgHandlerResponse) {
		makePaymentResponse.setPaymentStatus(ipgHandlerResponse.getPaymentStatus());
		makePaymentResponse.setActionStatus(ipgHandlerResponse.getActionStatus());

		return makePaymentResponse;
	}

	private void addPostParamData(String requestMethod, ExternalPGDetails externalPGDetails, String strRequestData) {
		if (REQUEST_METHODS.POST.toString().equals(requestMethod)) {
			externalPGDetails.setPostInputData(strRequestData);
		} else {
			externalPGDetails.setRedirectionURL(strRequestData);
		}
	}

	private PayCurrencyDTO getPayCurrencyDTO(CurrencyExchangeRate currencyExrate, String baseCurrencyCode,
			TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		Currency currency = null;
		PayCurrencyDTO payCurrencyDTO = null;
		CommonMasterBD commonMasterBD = ModuleServiceLocator.getCommoMasterBD();

		if (currencyExrate != null) {
			currency = commonMasterBD.getCurrency(baseCurrencyCode);
			if (currency.getCardPaymentVisibility() == 1
					&& CommonServiceUtil.getDefaultPGId(currency, trackInfoDTO.getAppIndicator()) != null) {
				payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, currencyExrate.getMultiplyingExchangeRate(),
						currency.getBoundryValue(), currency.getBreakPoint(), currency.getDecimalPlaces());
			}
		}

		if (payCurrencyDTO == null) {

			log.debug("Interline paymentgateway checking for selected currency does not have pg getting the default ");

			currency = commonMasterBD.getCurrency(baseCurrencyCode);
			payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, BigDecimal.valueOf(1), currency.getBoundryValue(),
					currency.getBreakPoint());
		}

		return payCurrencyDTO;
	}

	private String getCountryCode(String ipAddress, String originCountryCodeForTest) throws ModuleException {
		String countryCode = "OT";

		if (AppSysParamsUtil.isTestStstem() && originCountryCodeForTest != null && originCountryCodeForTest.trim().length() > 0) {
			countryCode = originCountryCodeForTest;
		} else if (!StringUtil.isNullOrEmpty(ipAddress) && !AppSysParamsUtil.getIsONDWisePayment()) {
			countryCode = ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(ipAddress);
		}

		return countryCode;
	}

	private PaymentBaseSessionStore getPaymentBaseSessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);

	}

	private PaymentSessionStore getPaymentSessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);

	}

	private GiftVoucherSessionStore getGiftVoucherSessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);

	}

	private CustomerSessionStore getCustomerSessionStore() {
		return transactionFactory.getSessionStore(CustomerSession.SESSION_KEY);

	}

	private PaymentRequoteSessionStore getRequotePaymentSessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);
	}

	private BookingSessionStore getBookingSessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);
	}

	private RequoteSessionStore getRequoteSessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);
	}

	private PaymentAncillarySessionStore getPaymentAncillarySessionStore(String transactionId) {
		return transactionFactory.getTransactionStore(transactionId);
	}

	private Cash getOnHoldDetail(String cabinClass, List<ReservationPax> reservationPaxList, String contactEmail,
			SYSTEM searchSystem, int validationType, PaymentBaseSessionStore paymentBaseSessionStore, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		Date depTimeZulu = null;
		OnHold onHoldApp = OnHold.DEFAULT;
		Date pnrReleaseTime = null;
		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = null;
		boolean onHoldEnable = false;

		Cash cash = null;

		List<FlightSegmentTO> flightSegmentTOs = paymentUtils
				.adaptFlightSegment(paymentBaseSessionStore.getSelectedSegmentsForPayment());
		Collection<OndFareSegChargeTO> ondFareSegChargeTOs = paymentBaseSessionStore.getFareSegChargeTOForPayment()
				.getOndFareSegChargeTOs();

		onHoldReleaseTimeDTO = getOnHoldReleaseTimeDTO(flightSegmentTOs, ondFareSegChargeTOs);

		onHoldReleaseTimeDTO.setCabinClass(cabinClass);

		boolean isKioskApplication = false;

		if (AppIndicatorEnum.APP_KIOSK.equals(trackInfoDTO.getAppIndicator())) {
			isKioskApplication = true;
			onHoldApp = OnHold.KIOSK;
		} else if (AppIndicatorEnum.APP_IBE.equals(trackInfoDTO.getAppIndicator())) {
			onHoldApp = OnHold.IBE;
		} else if (AppIndicatorEnum.APP_IOS.equals(trackInfoDTO.getAppIndicator())) {
			onHoldApp = OnHold.IBE;
		} else if (AppIndicatorEnum.APP_ANDROID.equals(trackInfoDTO.getAppIndicator())) {
			onHoldApp = OnHold.ANDROID;
		}

		boolean oNDImageCapcha = false;
		String onHoldDisplayTime = "";

		// Create flow
		if (isKioskApplication && AppSysParamsUtil.isOnHoldEnable(onHoldApp)) {
			depTimeZulu = paymentUtils.getDepartureTimeZulu(flightSegmentTOs);
			pnrReleaseTime = getOnHoldReleaseTime(onHoldApp, depTimeZulu, onHoldReleaseTimeDTO);
			onHoldEnable = isOnHoldEnabled(pnrReleaseTime);

		} else if (AppSysParamsUtil.isOnHoldEnable(onHoldApp)
				&& paymentUtils.isCabinClassAllowOnHold(paymentBaseSessionStore.getFareSegChargeTOForPayment())) {
			oNDImageCapcha = AppSysParamsUtil.isOHDImageCapcha();
			depTimeZulu = paymentUtils.getDepartureTimeZulu(flightSegmentTOs);
			pnrReleaseTime = getOnHoldReleaseTime(onHoldApp, depTimeZulu, onHoldReleaseTimeDTO);

			onHoldEnable = isOnHoldEnabled(pnrReleaseTime);
			if (onHoldEnable) {
				onHoldEnable = onHoldEnable && ModuleServiceLocator.getAirproxySearchAndQuoteBD().isOnholdEnabled(
						flightSegmentTOs, reservationPaxList, contactEmail, validationType, trackInfoDTO.getOriginChannelId(),
						searchSystem, trackInfoDTO.getIpAddress(), trackInfoDTO);
			}

		} else {
			onHoldEnable = false;
		}

		if (onHoldEnable) {
			onHoldDisplayTime = paymentUtils.wrapWithDDHHMM(paymentUtils.getOnHoldDisplayTime(onHoldApp, pnrReleaseTime));

			cash = new Cash();
			cash.setOnholdRelease(pnrReleaseTime);
			cash.setReleaseTimeDisplay(onHoldDisplayTime);
			cash.setImageCapcha(oNDImageCapcha);

		}
		return cash;
	}

	private boolean isOnHoldEnabled(Date pnrReleaseTime) {
		if (pnrReleaseTime == null) {
			return false;
		} else {
			Date currentDateTime = Calendar.getInstance().getTime();
			if (pnrReleaseTime.getTime() > currentDateTime.getTime()) {
				return true;
			}
		}
		return false;
	}

	private LCCClientReservation loadProxyReservation(String pnr, boolean isGroupPNR, boolean isRegisteredUser,
			String operatingCarrier, String sellingCarrier, TrackInfoDTO trackInfoDTO) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr, isGroupPNR, operatingCarrier, sellingCarrier);

		ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
		modificationParamRQInfo.setAppIndicator(appIndicator);
		modificationParamRQInfo.setIsRegisteredUser(isRegisteredUser);

		return ModuleServiceLocator.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO, modificationParamRQInfo,
				trackInfoDTO);
	}

	private LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, String operatingCarrier, String sellingCarrier) {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(false);

		if (StringUtils.isNotEmpty(sellingCarrier)) {
			if (sellingCarrier.trim().length() == 2) {
				pnrModesDTO.setMarketingAirlineCode(sellingCarrier);
			}
		}

		if (StringUtils.isNotEmpty(operatingCarrier)) {
			if (operatingCarrier.trim().length() == 2) {
				pnrModesDTO.setAirlineCode(operatingCarrier);
			}
		}
		return pnrModesDTO;
	}

	private boolean isSelectedPaymentOptionValid(int cardType, int paymentGatewayId, boolean offlinePaymentEnable, String pnr,
			boolean isGroupPnr, TrackInfoDTO trackInfoDTO, PaymentBaseSessionStore paymentBaseSessionStore)
			throws ModuleException {

		String originCountryCodeForTest = paymentBaseSessionStore.getOriginCountryCodeForTest();
		String moduleCode = CommonServiceUtil.getApplicationEngine(trackInfoDTO.getAppIndicator()).toString();

		SessionFlightSegment firstFlightSeg = paymentBaseSessionStore.getSelectedSegmentsForPayment().get(0);
		String firstSegOndCode = firstFlightSeg == null ? null : firstFlightSeg.getSegmentCode();

		IPGPaymentOptionDTO paymentOptionDTO = (new IPGPaymentOptionDTO()).setSelCurrency(AppSysParamsUtil.getDefaultPGCurrency())
				.setOfflinePayment(offlinePaymentEnable).setModuleCode(moduleCode).setFirstSegmentONDCode(firstSegOndCode);

		Collection<IPGPaymentOptionDTO> ipgPaymentGateways = getIPGPaymentGateways(paymentOptionDTO);

		if (!StringUtil.isNullOrEmpty(pnr)) {

			String reservationStatus = getReservationStatus(pnr, isGroupPnr, trackInfoDTO);

			// verify allow Offline payment on balance payment only when OHD -> CNF
			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)) {
				addOnholdCaseIPGs(ipgPaymentGateways, AppSysParamsUtil.getDefaultPGCurrency(), trackInfoDTO.getAppIndicator(),
						pnr, null, false);
			}
		}

		Collection<Integer> paymentGatewayIds = paymentUtils.getPaymentGatewayIds(ipgPaymentGateways);

		String countryCode = getCountryCode(trackInfoDTO.getIpAddress(), originCountryCodeForTest);

		boolean isOndWisePayment = AppSysParamsUtil.getIsONDWisePayment();

		Collection<CountryPaymentCardBehaviourDTO> cardsInfo = isOndWisePayment
				? getONDWiseCountryCardBehavior(firstSegOndCode, paymentGatewayIds)
				: getCountryPaymentCardBehavior(countryCode, paymentGatewayIds);

		if (cardsInfo != null && !cardsInfo.isEmpty()) {
			for (CountryPaymentCardBehaviourDTO cardInfo : cardsInfo) {
				if (cardInfo.getPaymentGateWayID() == paymentGatewayId && cardInfo.getCardType() == cardType) {
					return true;
				}
			}
		}

		return false;
	}

	private String getContactEmailAddress(ReservationContact reservationContact) {

		if (reservationContact != null && !StringUtil.isNullOrEmpty(reservationContact.getEmailAddress())) {
			return reservationContact.getEmailAddress();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private LmsCredits getLMSCreditInfo(RedeemCalculateRQ redeemCalculateRQ, TrackInfoDTO trackInfoDTO) throws ModuleException {
		LmsCredits lmsCreditsRedeemed = null;
		BigDecimal maxRedeemAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (redeemCalculateRQ != null) {
			String memberAccountId = getMemberAccountId();
			String memberExternalId = getMemberExternalAccountId();

			lmsCreditsRedeemed = new LmsCredits();
			Double remainingLoyaltyPoints = getMemberAvailablePoints(memberAccountId);
			redeemCalculateRQ.setRemainingPoint(remainingLoyaltyPoints);
			redeemCalculateRQ.setMemberExternalId(memberExternalId);
			redeemCalculateRQ.setIssueRewardIds(false);
			ServiceResponce serviceResponce = calculateLoyaltyPoints(redeemCalculateRQ, trackInfoDTO);

			Map<Integer, Map<String, BigDecimal>> paxWiseProductAmounts = (Map<Integer, Map<String, BigDecimal>>) serviceResponce
					.getResponseParam(ResponceCodes.ResponseParams.PAX_PRODUCT_AMOUNT);

			if (paxWiseProductAmounts != null && !paxWiseProductAmounts.isEmpty()) {
				maxRedeemAmount = getMaxRedeemAmount(paxWiseProductAmounts);
			}

			Double memberAvailablePoints = (Double) serviceResponce
					.getResponseParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS);

			BigDecimal memberAvailablePointsAmount = (BigDecimal) serviceResponce
					.getResponseParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS_AMOUNT);

			lmsCreditsRedeemed.setAirRewardId(memberAccountId);
			lmsCreditsRedeemed.setMaxRedeemableAmount(maxRedeemAmount); // max redeem-able amount
			lmsCreditsRedeemed.setMinRedeemableAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			lmsCreditsRedeemed.setPaymentCurrency(AppSysParamsUtil.getBaseCurrency());
			lmsCreditsRedeemed.setAvailablePoints(BigDecimal.valueOf(memberAvailablePoints)); // available points
			lmsCreditsRedeemed.setAvailablePointsAmount(memberAvailablePointsAmount); // available points amount

		}
		return lmsCreditsRedeemed;
	}

	private String getMemberAccountId() {
		String memberAccountId = null;
		CustomerSessionStore customerSessionStore = getCustomerSessionStore();
		if (customerSessionStore != null) {
			memberAccountId = customerSessionStore.getFFID();
		}

		return memberAccountId;
	}

	private String getMemberExternalAccountId() {
		String externalId = null;
		CustomerSessionStore customerSessionStore = getCustomerSessionStore();
		if (customerSessionStore != null) {
			externalId = customerSessionStore.getLMSRemoteId();
		}
		return externalId;
	}

	private String getMemberEnrollingCarrier() {
		String enrollinCarrier = null;
		CustomerSessionStore customerSessionStore = getCustomerSessionStore();
		if (customerSessionStore != null) {
			enrollinCarrier = customerSessionStore.getMemberEnrollingCarrier();
		}

		return enrollinCarrier;
	}

	private Map<String, List<String>> getCarrierWiseFlightRPH(List<FlightSegmentTO> flightSegments) throws ModuleException {

		Map<String, List<String>> carrierWiseFlightRPH = new HashMap<String, List<String>>();
		Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightBD().getBusAirCarrierCodes();
		if (flightSegments != null) {
			for (FlightSegmentTO flightSegmentTO : flightSegments) {
				String operatingCarrier = FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber());
				if (busAirCarrierCodes.containsKey(operatingCarrier)) {
					operatingCarrier = busAirCarrierCodes.get(operatingCarrier);
				}

				if (operatingCarrier != null && carrierWiseFlightRPH.containsKey(operatingCarrier)) {
					carrierWiseFlightRPH.get(operatingCarrier).add(flightSegmentTO.getFlightRefNumber());
				} else {
					List<String> flightRefNumList = new ArrayList<String>();
					flightRefNumList.add(flightSegmentTO.getFlightRefNumber());
					carrierWiseFlightRPH.put(operatingCarrier, flightRefNumList);
				}
			}

		}

		return carrierWiseFlightRPH;
	}

	private void initiateBalancePaymentInformation(PaymentSessionStore paymentSessionStore, LCCClientReservation reservation) {

		TravellerQuantity travellerQuantity = new TravellerQuantity();
		travellerQuantity.setAdultCount(reservation.getTotalPaxAdultCount());
		travellerQuantity.setChildCount(reservation.getTotalPaxChildCount());
		travellerQuantity.setInfantCount(reservation.getTotalPaxInfantCount());

		PreferenceInfo prefInfo = new PreferenceInfo();
		// prefInfo.setBookingCode(bookingCode);
		// prefInfo.setSeatType(seatType);
		prefInfo.setSelectedSystem(reservation.isGroupPNR() ? SYSTEM.INT.toString() : SYSTEM.AA.toString());

		List<ServiceTaxContainer> serviceTaxContainers = new ArrayList<ServiceTaxContainer>(
				reservation.getApplicableServiceTaxes());

		paymentSessionStore.storeBalancePaymentRequiredInfo(getFlightSegments(reservation.getSegments()), prefInfo,
				serviceTaxContainers, travellerQuantity);

	}

	private List<FlightSegmentTO> getFlightSegments(Set<LCCClientReservationSegment> segments) {
		List<FlightSegmentTO> flightSegmentList = new ArrayList<FlightSegmentTO>();
		for (LCCClientReservationSegment resSeg : segments) {
			FlightSegmentTO fltSegTO = new FlightSegmentTO();
			fltSegTO.setFlightNumber(resSeg.getFlightNo());
			fltSegTO.setDepartureDateTime(resSeg.getDepartureDate());
			fltSegTO.setDepartureDateTimeZulu(resSeg.getDepartureDateZulu());
			fltSegTO.setFlightDuration(resSeg.getFlightDuration());
			fltSegTO.setFlightModelNumber(resSeg.getFlightModelNumber());
			fltSegTO.setFlightRefNumber(resSeg.getFlightSegmentRefNumber());
			fltSegTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber()));
			fltSegTO.setPnrSegId(resSeg.getPnrSegID().toString());
			fltSegTO.setReturnFlag(resSeg.getReturnFlag().equals("Y") ? true : false);
			fltSegTO.setRouteRefNumber(resSeg.getRouteRefNumber());
			fltSegTO.setSegmentCode(resSeg.getSegmentCode());

			flightSegmentList.add(fltSegTO);
		}

		return flightSegmentList;
	}

	private List<Passenger> getPaxInfo(Set<LCCClientReservationPax> lccPassengers) {
		PassengerResAdaptor passengerResAdaptor = new PassengerResAdaptor();
		List<Passenger> paxInfo = new ArrayList<Passenger>();
		AdaptorUtils.adaptCollection(lccPassengers, paxInfo, passengerResAdaptor);
		return paxInfo;
	}

	private void setPaxCarrierExternalCharges(RedeemCalculateRQ redeemCalculateRQ, OperationTypes operationType,
			Collection<LCCClientPassengerSummaryTO> passengerSummaryList) throws ModuleException {
		if (redeemCalculateRQ != null) {
			if (OperationTypes.MODIFY_ONLY == operationType) {
				redeemCalculateRQ.setFareSegChargeTo(null);
				redeemCalculateRQ.setPaxCarrierProductDueAmount(populatePaxProductDueAmounts(passengerSummaryList));
				redeemCalculateRQ.setPaxCarrierExternalCharges(null);
			} else if (OperationTypes.MAKE_ONLY == operationType && redeemCalculateRQ.isPayForOHD()) {
				redeemCalculateRQ.setPaxCarrierProductDueAmount(ModuleServiceLocator.getAirproxyReservationBD()
						.getPaxProduDueAmount(redeemCalculateRQ.getPnr(), false, false, redeemCalculateRQ.getRemainingPoint()));
				redeemCalculateRQ.setPaxCarrierExternalCharges(null);
			}

		}

	}

	private Map<Integer, Map<String, Map<String, BigDecimal>>>
			populatePaxProductDueAmounts(Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {
		Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount = new HashMap<Integer, Map<String, Map<String, BigDecimal>>>();
		if (passengerSummaryList != null && !passengerSummaryList.isEmpty()) {
			for (LCCClientPassengerSummaryTO paxSummary : passengerSummaryList) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber());
				paxCarrierProductDueAmount.put(paxSeq, paxSummary.getCarrierProductCodeWiseAmountDue());
			}
		}

		return paxCarrierProductDueAmount;
	}

	private Date setupOnholdOfflinePaymentTime(String pnr, boolean isGroupPnr, TrackInfoDTO trackInfoDTO) throws ModuleException {

		if (pnr != null) {
			String operatingCarrier = "";
			String sellingCarrier = trackInfoDTO.getCarrierCode();
			boolean isRegisteredUser = isRegisteredUser();
			LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPnr, isRegisteredUser, operatingCarrier,
					sellingCarrier, trackInfoDTO);
			return reservation.getZuluReleaseTimeStamp();
		}

		else {
			Date offlineTempOnhold = new Date(AppSysParamsUtil.getOnholdOfflinePaymentTimeInMillis());
			offlineTempOnhold = new Date(new Date().getTime() + offlineTempOnhold.getTime());
			return offlineTempOnhold;
		}
	}

	private List<Passenger> getPaxFromPNR(TrackInfoDTO trackInfoDTO, List<Passenger> paxInfo, String pnr, boolean isGroupPnr)
			throws ModuleException {
		if (!StringUtil.isNullOrEmpty(pnr)) {

			String operatingCarrier = "";
			String sellingCarrier = trackInfoDTO.getCarrierCode();
			boolean isRegisteredUser = isRegisteredUser();
			LCCClientReservation reservation = loadProxyReservation(pnr, isGroupPnr, isRegisteredUser, operatingCarrier,
					sellingCarrier, trackInfoDTO);
			paxInfo = getPaxInfo(reservation.getPassengers());

		}
		return paxInfo;
	}

	private boolean redeemLoyaltyPoints(LmsCredits lmsCredits, String pnr, PaymentBaseSessionStore paymentBaseSessionStore,
			BigDecimal totalWithAncillaryCharges, Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount,
			List<Passenger> paxInfo, DiscountRQ discountRQ, TrackInfoDTO trackInfoDTO, BigDecimal removedCharges,
			LoyaltyInformation loyaltyInformation, String transactionId) throws ModuleException {
		boolean lmsRedeemStatus = false;
		try {

			revertLoyaltyRedemption(paymentBaseSessionStore, pnr);
			if (lmsCredits != null && lmsCredits.getRedeemRequestAmount().doubleValue() > 0) {
				lmsRedeemStatus = redeemLoyaltyPoints(lmsCredits.getRedeemRequestAmount(), paymentBaseSessionStore, paxInfo,
						trackInfoDTO, pnr, totalWithAncillaryCharges, paxCarrierProductDueAmount, discountRQ, removedCharges,
						loyaltyInformation, transactionId);
			}

			log.debug("LMS redeem points service response status:" + lmsRedeemStatus);

		} catch (ModuleException exception) {
			log.error("getEffectivePaymentResponse :: Error @ redeemLoyaltyPoints : ");
			throw new ModuleException("payment.api.loyalty.redemption.failed");
		}

		return lmsRedeemStatus;
	}

	private BigDecimal getRemovedCharges(TransactionalBaseRQ baseRQ, TrackInfoDTO trackInfoDTO) throws ModuleException {
		AncillaryIntegrateTO ancillaryIntegrateTO = null;
		BigDecimal removedCharges = null;
		try {
			ancillaryIntegrateTO = ancillaryService.getAncillaryIntegrateTO(trackInfoDTO, baseRQ);
		} catch (Exception e) {
			throw new ModuleException("payment.api.anci.modification.integrate.failed");
		}

		removedCharges = AnciCommon.getRemoveAncillaryTotal(ancillaryIntegrateTO.getReservation().getPassengers());
		return removedCharges;
	}

	private BigDecimal getBalanceAmountForAnciModification(TransactionalBaseRQ baseRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		BigDecimal balanceToPay = null;
		try {
			balanceToPay = ancillaryService.getPrePaymentTO(trackInfoDTO, baseRQ).getBalanceDue();
		} catch (Exception e) {
			throw new ModuleException("payment.api.anci.modification.amount.empty");
		}
		return balanceToPay;
	}

	private FlightPriceRQ createFlightPriceRQ(TravellerQuantity travellerQuantity, String seatType) {
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		TravelerInfoSummaryTO travelerInfoSummaryTO = flightPriceRQ.getTravelerInfoSummary();
		travelerInfoSummaryTO.getPassengerTypeQuantityList().addAll(paymentUtils.getPaxQtyList(travellerQuantity));
		flightPriceRQ.getTravelPreferences().setBookingType(seatType);
		// flightPriceRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(false);
		return flightPriceRQ;
	}

	private boolean isRegisteredUser() {
		CustomerSessionStore customerSession = getCustomerSessionStore();
		if (customerSession != null) {
			if (!StringUtil.isNullOrEmpty(customerSession.getLoggedInCustomerID())) {
				return true;
			}
		}
		return false;
	}

	private String getReturnDate(List<SessionFlightSegment> segments) {

		if (isReturnFlight(segments)) {
			// JourneyType.ROUNDTRIP is set if return date is not empty
			return segments.get(segments.size() - 1).getDepartureDateTime().toString();
		}
		return "";
	}

	private boolean isReturnFlight(List<SessionFlightSegment> segments) {

		if (segments.size() > 1) {

			SessionFlightSegment firstSegment = segments.get(0);
			SessionFlightSegment lastSegment = segments.get(segments.size() - 1);

			if (firstSegment.getOndSequence() == 0 && lastSegment.getOndSequence() == 1) {

				if (firstSegment.getSegmentCode().substring(0, 3).contentEquals(lastSegment.getSegmentCode().substring(3))) {
					return true;
				}

			}

		}

		return false;
	}

	private Collection<PaxChargesTO> transformPaxList(Collection<ReservationPaxTO> reservationPaxTOCollection,
			List<Passenger> paxInfo) {

		Collection<PaxChargesTO> paxExtChargesList = new ArrayList<PaxChargesTO>();
		Collection<LCCClientExternalChgDTO> paxExternalCharges = new ArrayList<LCCClientExternalChgDTO>();

		if (paxInfo != null && !paxInfo.isEmpty()) {
			for (Passenger passenger : paxInfo) {
				PaxChargesTO paxExternalChargesTO = new PaxChargesTO();
				String paxType = passenger.getPaxType();
				boolean isParent = BookingUtil.isParent(paxInfo, passenger.getPaxSequence());
				if (passenger.getTravelWith() > 0) {
					continue;
				}

				if (PaxTypeTO.PARENT.equals(paxType)) {
					paxType = PaxTypeTO.ADULT;
				}

				paxExternalChargesTO.setPaxTypeCode(paxType);
				paxExternalChargesTO.setPaxSequence(passenger.getPaxSequence());

				// populating the pax external charges, from this implementation even there are no external charges
				// assigned to pax, pax external charge object will be created with emptry external charge list
				if (reservationPaxTOCollection != null && !reservationPaxTOCollection.isEmpty()) {
					paxExternalCharges = new ArrayList<LCCClientExternalChgDTO>();
					for (ReservationPaxTO resPaxTO : reservationPaxTOCollection) {
						if (passenger.getPaxSequence() == resPaxTO.getSeqNumber()) {
							paxExternalCharges = resPaxTO.getExternalCharges();
						}
					}
					paxExternalChargesTO.setExtChgList((List<LCCClientExternalChgDTO>) paxExternalCharges);

				} else {
					paxExternalChargesTO.setExtChgList(new ArrayList<LCCClientExternalChgDTO>());
				}

				paxExternalChargesTO.setParent(isParent);

				paxExtChargesList.add(paxExternalChargesTO);
			}
		}

		return paxExtChargesList;
	}

	/* PAYPAL addition */
	// public void setPPCreditCardInfo(IPGRequestDTO ipgRequestDTO, Card card, UserInputDTO userInputDTO) {
	// userInputDTO.setCreditCardNumber(card.getCardNo());
	// userInputDTO.setCVV2(String.valueOf(card.getCardCvv()));
	// userInputDTO.setExpMonth(Integer.parseInt(ipgRequestDTO.getExpiryDate().substring(2, 4)));
	// userInputDTO.setExpYear(2000 + Integer.parseInt(ipgRequestDTO.getExpiryDate().substring(0, 2)));
	// userInputDTO.setBuyerAddress1(card.getBillingAddress1());
	// userInputDTO.setBuyerAddress2(card.getBillingAddress2());
	// userInputDTO.setBuyerCity(card.getCity());
	// userInputDTO.setPostalCode(card.getPostalCode());
	// userInputDTO.setBuyerState(card.getState());
	// }
	//
	// public void setPPRequestDTO(IPGRequestDTO ipgRequestDTO, PayCurrencyDTO payCurrencyDTO, Properties ipgProps,
	// UserInputDTO userInputDTO) throws ModuleException {
	// userInputDTO.setAmount(ipgRequestDTO.getAmount());
	// userInputDTO.setBuyerFirstName(ipgRequestDTO.getContactFirstName());
	// userInputDTO.setBuyerLastName(ipgRequestDTO.getContactLastName());
	// userInputDTO.setCountryCodeType(CountryCodeType.fromString(ipgRequestDTO.getContactCountryCode()));
	// userInputDTO.setCurrencyCodeType(CurrencyCodeType.fromString(payCurrencyDTO.getPayCurrencyCode()));
	// userInputDTO.setInvoiceId(ipgRequestDTO.getPnr());
	// userInputDTO.setIPAddress(getClientInfoDTO().getIpAddress());
	// userInputDTO.setNoShipping(PAYPALPaymentUtils.NoShipping);
	// userInputDTO.setPayerCountry(StringUtils.trimToEmpty(ipgRequestDTO.getContactCountryCode()));
	// userInputDTO.setPaymentAction(PaymentActionCodeType.Sale);
	// userInputDTO.setReturnUrl(RETURN_URL);
	// userInputDTO.setTypeOfGoods(PAYPALPaymentUtils.TypeOfGoods);
	// userInputDTO.setUserId(ipgRequestDTO.getSessionID());// use to store something like sessions
	// }

	private LmsCredits getLMSCredits(PaymentSessionStore paymentSessionStore, List<Passenger> paxInfo, DiscountRQ discountRQ,
			TrackInfoDTO trackInfoDTO, String transactionId) throws ModuleException {

		BigDecimal totalWithAnciCharges = paymentSessionStore.getTotalPaymentInformation().getTotalWithAncillaryCharges();
		PreferenceInfo preferenceInfo = paymentSessionStore.getPreferencesForPayment();
		SYSTEM searchSystem = SYSTEM.getEnum(preferenceInfo.getSelectedSystem());
		String memberAccountId = getMemberAccountId();
		List<FlightSegmentTO> flightSegments = paymentUtils
				.adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());

		RedeemCalculateRQ redeemCalculateRQ = getRedeemCalculateRQ(totalWithAnciCharges, searchSystem, memberAccountId,
				paymentSessionStore.getFareSegChargeTOForPayment(), paymentSessionStore.getTravellerQuantityForPayment(),
				preferenceInfo.getSeatType(), paymentSessionStore.getPaxAncillaryExternalCharges(), paxInfo, flightSegments, null,
				null, transactionId);

		LmsCredits binPromoLmsCredits = getLMSCreditInfo(redeemCalculateRQ, trackInfoDTO);

		return binPromoLmsCredits;
	}

	// old IBE takes from search params
	private boolean getFlexiQuote(List<PassengerChargeTo<LCCClientExternalChgDTO>> paxAncillaryExternalCharges) {

		if (paxAncillaryExternalCharges != null && !paxAncillaryExternalCharges.isEmpty()) {
			for (PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo : paxAncillaryExternalCharges) {

				for (LCCClientExternalChgDTO lccClientExternalDTO : passengerChargeTo.getGetPassengerCharges()) {
					if (lccClientExternalDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
						return true;
					}
				}
			}
		}

		return false;
	}

	private void setOriginCountryCodeToSessionForTestSystem(String originCountryCodeForTest,
			PaymentBaseSessionStore paymentBaseSessionStore) {
		if (AppSysParamsUtil.isTestStstem()) {
			paymentBaseSessionStore.setOriginCountryCodeForTest(originCountryCodeForTest);
		}
	}

	private PromoSummary buildPromoInfo(DiscountedFareDetails discountedFareDetails,
			ReservationDiscountDTO reservationDiscountDTO) {
		PromoSummary promoSummary = new PromoSummary();
		promoSummary.setCode(discountedFareDetails.getCode());
		promoSummary.setType(discountedFareDetails.getPromotionType());
		promoSummary.setNotes(discountedFareDetails.getNotes());
		promoSummary.setDiscountType(discountedFareDetails.getDiscountType());
		promoSummary.setDiscountAmount(reservationDiscountDTO.getTotalDiscount());
		promoSummary.setDiscountAs(discountedFareDetails.getDiscountAs());
		promoSummary.setDiscountCode(reservationDiscountDTO.getDiscountCode());
		return promoSummary;
	}

	private void checkLMSRedemptionLeavesANonZeroAmountForCardPayment(PaymentSessionStore paymentSessionStore,
			BigDecimal totalWithAnciCharges, LmsCredits lmsCreditsToRdeem, BigDecimal binDiscountAmount) throws ModuleException {
		BigDecimal redeemRequestAmount = lmsCreditsToRdeem.getRedeemRequestAmount();
		BigDecimal binPromoEffectiveTotal = totalWithAnciCharges;

		if (!paymentSessionStore.getBinPromoDiscountInfo().isDiscountAsCredit()) {
			binPromoEffectiveTotal = AccelAeroCalculator.add(totalWithAnciCharges, binDiscountAmount.negate());
		}

		if (binPromoEffectiveTotal.doubleValue() <= redeemRequestAmount.doubleValue()) {

			throw new ModuleException("payment.api.bin.promotion.lms.redemption.invalid");

		}
	}

	private String getCabinClass(List<SessionFlightSegment> selectedSegmentsForPayment) {
		List<FlightSegmentTO> flightSegments = paymentUtils.adaptFlightSegment(selectedSegmentsForPayment);
		return flightSegments.get(0).getCabinClassCode();

	}

	public PayfortCheckRS buildPayfortCheckRS(String pnr, String transactionFee, TrackInfoDTO trackInfoDTO) {

		PayfortCheckRS payfortCheckRS = new PayfortCheckRS();
		boolean groupPNR = deriveIsGroupPNR(trackInfoDTO, pnr);

		if (!"".equals(pnr) && !"".equals(transactionFee)) {
			CreditCardTransaction creditCardTransaction = null;
			try {
				creditCardTransaction = ModuleServiceLocator.getPaymentBrokerBD().loadTransactionByReference(pnr);
			} catch (ModuleException e1) {
				log.info("HandleInterlinePayFortAction!checkPayFort : Loading Tempary Trasaction Failed", e1);
			}
			if (creditCardTransaction != null) {

				TempPaymentTnx tempPaymentTnx = null;
				BigDecimal ccAmount = null;
				BigDecimal resAmount = null;
				Reservation reservation = null;
				LCCClientReservation commonReservation = null;
				BigDecimal tnxValAmount = new BigDecimal(transactionFee);
				try {
					if (groupPNR) {

						LCCClientPnrModesDTO pnrModesDTO = paymentUtils.getPnrModesDTO(pnr, groupPNR, false, "", "", true,
								CommonServiceUtil.getApplicationEngine(appIndicator));
						commonReservation = AirproxyModuleUtils.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO,
								null, trackInfoDTO);
						tempPaymentTnx = ReservationModuleUtils.getReservationBD()
								.loadTempPayment(creditCardTransaction.getTemporyPaymentId());
						ccAmount = tempPaymentTnx.getAmount();
						resAmount = AccelAeroCalculator.scaleDefaultRoundingDown(
								AccelAeroCalculator.add(commonReservation.getTotalChargeAmount(), tnxValAmount));

					} else {

						reservation = ReservationModuleUtils.getReservationBD().getReservation(pnr, true);
						tempPaymentTnx = ReservationModuleUtils.getReservationBD()
								.loadTempPayment(creditCardTransaction.getTemporyPaymentId());
						ccAmount = tempPaymentTnx.getAmount();
						resAmount = AccelAeroCalculator.add(reservation.getTotalChargeAmount(), tnxValAmount);

					}
				} catch (CommonsDataAccessException e) {
					log.error("Reservation Load Fails for check already exists payment entries beacause: " + e.getStackTrace());
					return payfortCheckRS;

				} catch (ModuleException e) {
					log.error("Reservation Load Fails for check already exists payment entries beacause: " + e.getStackTrace());
					return payfortCheckRS;
				}
				String requestText = creditCardTransaction.getRequestText();

				if ((reservation != null || commonReservation != null) && ccAmount != null
						&& (ccAmount.equals(resAmount) || (-0.5 < AccelAeroCalculator.subtract(ccAmount, resAmount).doubleValue()
								&& AccelAeroCalculator.subtract(ccAmount, resAmount).doubleValue() < 0.5))) {
					if (requestText.contains(Pay_AT_StoreResponseDTO.VOUCHER_ID)
							&& requestText.contains(Pay_AT_StoreResponseDTO.REQUEST_ID)) {
						List<String> sellItems = Arrays.asList(requestText.split(","));
						String strRequestText = "";
						for (int i = 0; i < sellItems.size() - 1; i++) {
							strRequestText += sellItems.get(i) + ",";
						}
						strRequestText = strRequestText.substring(0, strRequestText.length() - 1);
						Map<String, String> params = Splitter.on(",").withKeyValueSeparator(":").split(strRequestText);
						payfortCheckRS.setVoucherID(params.get("voucherID ").trim());
						payfortCheckRS.setRequestID(params.get("requestID ").trim());
						payfortCheckRS.setVoucherAvailable(true);
						payfortCheckRS.setPopupMessage(errorMessageSource.getMessage(
								"pay.gateway.error.payfort.voucher.popup.message", null, LocaleContextHolder.getLocale()));

					} else if (requestText.contains("orderReference") && requestText.contains("trackingNumber")) {

						List<String> sellItems = Arrays.asList(requestText.split(","));
						String strRequestText = "";
						for (int i = 0; i < sellItems.size() - 1; i++) {
							strRequestText += sellItems.get(i) + ",";
						}
						strRequestText = strRequestText.substring(0, strRequestText.length() - 1);
						Map<String, String> params = Splitter.on(",").withKeyValueSeparator(":").split(strRequestText);
						payfortCheckRS.setTrackingNumberAvailable(true);
						String popUpMsg = errorMessageSource.getMessage(
								"pay.gateway.error.payfort.payAtHome.TrackNum.popup.message", null,
								LocaleContextHolder.getLocale());
						popUpMsg = popUpMsg.replace("{0}", params.get("trackingNumber ").trim());
						payfortCheckRS.setPopupMessage(popUpMsg);
					} else if (requestText.contains("orderReference")) {
						payfortCheckRS.setTrackingNumberAvailable(true);
						payfortCheckRS.setPopupMessage(null);
					}
				}
			}
		}
		payfortCheckRS.setSuccess(true);
		return payfortCheckRS;
	}

	private BinPromotionDetails buildBinPromotionDetails(BinPromotionDetails binPromotionDetails, int paymentGatewayId,
			LmsCredits binLmsCredits, BigDecimal binDiscount, BigDecimal lmsApplicableAmount, BigDecimal totalTransactionFee,
			BigDecimal effectivePaymentAmount) throws ModuleException {

		if (binPromotionDetails == null) {
			binPromotionDetails = new BinPromotionDetails();
			binPromotionDetails.setDiscountAmount(binDiscount);
		}

		binPromotionDetails.setLmsApplicableAmount(lmsApplicableAmount);
		binPromotionDetails.setLmsCredits(binLmsCredits);

		IPGPaymentOptionDTO pgOptionsDto = paymentUtils.getPaymentGatewayOptions(paymentGatewayId);
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		String baseCurrencyCode = pgOptionsDto.getBaseCurrency();
		CurrencyExchangeRate currencyExrate = exchangeRateProxy.getCurrencyExchangeRate(baseCurrencyCode,
				CommonServiceUtil.getApplicationEngine(appIndicator));

		binPromotionDetails.setTransactionFee(getAmountObj(totalTransactionFee, currencyExrate, baseCurrencyCode));

		binPromotionDetails.setEffectivePaymentAmount(getAmountObj(effectivePaymentAmount, currencyExrate, baseCurrencyCode));

		return binPromotionDetails;
	}

	void setAppIndicatorEnum(TrackInfoDTO trackInfoDTO) {
		this.appIndicator = trackInfoDTO.getAppIndicator();
	}

	private boolean deriveIsGroupPNR(TrackInfoDTO trackInfoDTO, String pnr) {
		LoadReservationRQ loadReservationRQ = new LoadReservationRQ();
		loadReservationRQ.setPnr(pnr);
		LoadReservationValidateRes loadReservationValidateRes = bookingService.validateLoadReservationRQ(loadReservationRQ,
				trackInfoDTO, getCustomerSessionStore().getLoggedInCustomerID());

		boolean isGroupPnr = false;
		if (loadReservationValidateRes != null) {
			isGroupPnr = loadReservationValidateRes.isGroupPnr();
		}
		return isGroupPnr;
	}

	private void removeQIWIOfflineInNonCreateFlows(List<CountryPaymentCardBehaviourDTO> pgCards) {
		Iterator<CountryPaymentCardBehaviourDTO> pgCardsIterator = pgCards.iterator();
		while (pgCardsIterator.hasNext()) {
			CountryPaymentCardBehaviourDTO countryPaymentCardBehaviourDTO = pgCardsIterator.next();
			if (PaymentConsts.QIWI_OFFLINE.contentEquals(countryPaymentCardBehaviourDTO.getCardDisplayName())) {
				pgCardsIterator.remove();
			}
		}

	}

	private List<LCCClientReservationInsurance> getReservationInsurances(boolean isModifySegment, TransactionalBaseRQ baseRQ,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		List<LCCClientReservationInsurance> reservationInsurances = null;
		if (isModifySegment) {
			if (ancillaryService != null) {
				AncillaryIntegrateTO ancillaryIntegrateTo = ancillaryService.getAncillaryIntegrateTO(trackInfoDTO, baseRQ);
				if (ancillaryIntegrateTo != null && ancillaryIntegrateTo.getReservation() != null) {
					reservationInsurances = ancillaryIntegrateTo.getReservation().getReservationInsurances();
				}
			}
		}
		return reservationInsurances;
	}

	private List<ReservationPaxTO> getPaxListWithSelectedAncillaryDTOs(boolean isModifySegment, TransactionalBaseRQ baseRQ,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		List<ReservationPaxTO> paxList = null;
		if (isModifySegment) {
			if (ancillaryService != null) {
				AncillaryIntegrateTO ancillaryIntegrateTo = ancillaryService.getAncillaryIntegrateTO(trackInfoDTO, baseRQ);
				if (ancillaryIntegrateTo != null && ancillaryIntegrateTo.getReservation() != null) {
					paxList = new ArrayList<ReservationPaxTO>();
					List<AncillaryIntegratePassengerTO> anciPassengers = ancillaryIntegrateTo.getReservation().getPassengers();
					for (AncillaryIntegratePassengerTO anciPax : anciPassengers) {
						ReservationPaxTO resPax = new ReservationPaxTO();
						resPax.setSeqNumber(Integer.parseInt(anciPax.getPassengerRph()));
						for (LCCSelectedSegmentAncillaryDTO anciDTO : anciPax.getSelectedAncillaries()) {
							resPax.addSelectedAncillaries(anciDTO);
						}
						paxList.add(resPax);
					}
				}
			}
		}
		return paxList;
	}

	private void checkReservationDataIntergrity(MakePaymentRQ makePaymentRequest) throws ModuleException {
		if (getPaymentSessionStore(makePaymentRequest.getTransactionId()).getFareSegChargeTOForPayment() != null) {
			throw new ModuleException("msg.multiple.invalid.flight");
		}
	}

	private TransactionalBaseRS buildBookingRSForBalancePayment(MakePaymentRS makePaymentRS, String pnr, String transactionId,
			TrackInfoDTO trackInfoDTO) {
		BookingSessionStore bookingSessionStore = getBookingSessionStore(transactionId);

		BookingRS bookingRS = new BookingRS();
		bookingRS.setPnr(pnr);
		if (makePaymentRS != null && makePaymentRS.isSuccess()) {
			bookingRS.setSuccess(true);
			makePaymentRS.setTransactionId(transactionId);
			log.debug("balance payment processed");
			log.debug("payment status : " + makePaymentRS.getPaymentStatus() + " actions: " + makePaymentRS.getActionStatus());
			if (PaymentStatus.SUCCESS.equals(makePaymentRS.getPaymentStatus())) {
				try {
					CustomerSessionStore customerSessionStore = getCustomerSessionStore();
					String customerID = customerSessionStore.getLoggedInCustomerID();
					bookingService.balancePayment(bookingSessionStore, trackInfoDTO, customerID);

					bookingRS.setActionStatus(BookingRS.ActionStatus.BOOKING_CREATION_SUCCESS);

					CommonServiceUtil commonServices = new CommonServiceUtil();
					commonServices.updateBlockedLmsCreditStatusForCreateReservation(bookingRS, bookingSessionStore);

					bookingSessionStore.clearBookingDetails();
					log.debug("completed...........................!");
				} catch (Exception e) {
					bookingRS.setActionStatus(BookingRS.ActionStatus.ONHOLD_CREATED_PAYMENT_FAILED);
					log.error("error while saving balance payment", e);
				}
				bookingRS.setPaymentStatus(BookingRS.PaymentStatus.SUCCESS);
				bookingRS.setSuccess(true);

			} else if (BookingRS.PaymentStatus.PENDING.equals(makePaymentRS.getPaymentStatus())
					&& makePaymentRS.isOfflinePayment()) {
				bookingRS.setPaymentStatus(BookingRS.PaymentStatus.PENDING);
				bookingRS.setActionStatus(BookingRS.ActionStatus.ONHOLD_CREATED_OFFLINE);
				bookingRS.setExternalPGDetails(makePaymentRS.getExternalPGDetails());
				bookingRS.setMessages(makePaymentRS.getMessages());
				bookingRS.setSuccess(true);

			} else if (makePaymentRS.isSwitchToExternal()) {
				log.debug("waiting for external party re-direction");
				bookingRS.setPaymentStatus(BookingRS.PaymentStatus.PENDING);
				bookingRS.setActionStatus(BookingRS.ActionStatus.REDIRET_EXTERNAL);
				bookingRS.setExternalPGDetails(makePaymentRS.getExternalPGDetails());
				bookingRS.setSuccess(true);
			} else if (PaymentStatus.ERROR.equals(makePaymentRS.getPaymentStatus())) {
				bookingRS.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
				log.debug("error balance payment failed");
			}
		} else {
			log.debug("error balance payment failed");
			bookingRS.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
		}

		setBookingIPGTransactionResult(bookingRS, transactionId);
		bookingRS.setTransactionId(transactionId);

		return bookingRS;
	}

	private void setBookingIPGTransactionResult(BookingRS bookingRS, String transactionID) {
		PaymentSessionStore sessionStore = getPaymentSessionStore(transactionID);
		if (sessionStore.getPostPaymentInformation() != null
				&& sessionStore.getPostPaymentInformation().getIpgResponseDTO() != null
				&& sessionStore.getPostPaymentInformation().getIpgResponseDTO().getIpgTransactionResultDTO() != null) {
			IPGTransactionRSAdaptor ipgTransactionRSAdaptor = new IPGTransactionRSAdaptor();
			bookingRS.setIpgTransactionResult(ipgTransactionRSAdaptor
					.adapt(sessionStore.getPostPaymentInformation().getIpgResponseDTO().getIpgTransactionResultDTO()));
		}
	}

	private boolean checkOnholdable(PaymentBaseSessionStore paymentBaseSessionStore, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		String cabinClass = getCabinClass(paymentBaseSessionStore.getSelectedSegmentsForPayment());

		Cash cash = getOnHoldDetail(cabinClass, new ArrayList<ReservationPax>(), null,
				SYSTEM.getEnum(paymentBaseSessionStore.getPreferencesForPayment().getSelectedSystem()),
				ResOnholdValidationDTO.OTHER_VALIDATION, paymentBaseSessionStore, trackInfoDTO);

		return cash != null;
	}

	public static boolean isCsOcFlightAvailable(List<SessionFlightSegment> flightSegments) {
		boolean isCsOcFlightAvailable = false;
		for (SessionFlightSegment flightSegment : flightSegments) {
			if (flightSegment.getCsOcCarrierCode() != null) {
				isCsOcFlightAvailable = true;
			}
		}
		return isCsOcFlightAvailable;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ExternalChgDTO deriveCCChgDTO(PaymentBaseSessionStore paymentBaseSessionStore, int paymentGatewayId,
			BigDecimal amountForAdminFeeCalculation) throws ModuleException {

		int payablePaxCount = paymentUtils.getPayablePaxCount(paymentBaseSessionStore.getTravellerQuantityForPayment());
		int segmentCount = paymentUtils.getSegmentCount(paymentBaseSessionStore.getSelectedSegmentsForPayment());
		int rateApplicableType = paymentBaseSessionStore.getOperationType().getCode();

		ExternalChgDTO externalChgDTO = null;

		if (paymentGatewayId > 0 || isAdminFeeRegulatedAndAppliedForChannel()) {

			Collection externalChargesCollection = new ArrayList();
			externalChargesCollection.add(EXTERNAL_CHARGES.CREDIT_CARD);

			Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = ModuleServiceLocator.getReservationBD()
					.getQuotedExternalCharges(externalChargesCollection, null, rateApplicableType);

			externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);

			// Check for Payment gateway wise charges and override if exists.
			PaymentGatewayWiseCharges pgwCharge;

			pgwCharge = ModuleServiceLocator.getChargeBD().getPaymentGatewayWiseCharge(paymentGatewayId);

			if (pgwCharge != null && pgwCharge.getChargeId() > 0) {
				// valid charge is defined for the selected PGW
				if (pgwCharge.getValuePercentageFlag() != PaymentConsts.CHARGE_BY_VALUE) {
					externalChgDTO.setRatioValueInPercentage(true);
					externalChgDTO.setRatioValue(BigDecimal.valueOf(pgwCharge.getChargeValuePercentage()));
				} else {
					externalChgDTO.setRatioValueInPercentage(false);
					externalChgDTO.setRatioValue(BigDecimal.valueOf(pgwCharge.getValueInDefaultCurrency()));
				}
			}

			if (externalChgDTO.isRatioValueInPercentage()) {
				externalChgDTO.calculateAmount(amountForAdminFeeCalculation);
			} else {
				externalChgDTO.calculateAmount(payablePaxCount, segmentCount);
			}
		}

		return externalChgDTO;
	}

	private BigDecimal getExternalChargeTotalAmount(ExternalChgDTO ccChgDTO) {
		if (ccChgDTO != null && ccChgDTO.getAmount() != null) {
			return ccChgDTO.getAmount();
		}
		return BigDecimal.valueOf(0);
	}

	private BigDecimal deriveCCChgsAndCalculateTotalTransactionFee(PaymentBaseSessionStore paymentBaseSessionStore,
			int paymentGatewayId, BigDecimal amountForAdminFeeCalculation, TrackInfoDTO trackInfoDTO, ServiceTaxRQ serviceTaxRQ)
			throws ModuleException {

		ExternalChgDTO ccChgDTO = deriveCCChgDTO(paymentBaseSessionStore, paymentGatewayId, amountForAdminFeeCalculation);
		BigDecimal serviceTaxForAdminFee = calculateServiceTaxForCCcharge(serviceTaxRQ, ccChgDTO, trackInfoDTO, false);

		ExternalChgDTO ccFeeTaxChgDTO = deriveCCFeeChgDTO(paymentBaseSessionStore);

		return AccelAeroCalculator.add(calculateTransactionFee(ccChgDTO, ccFeeTaxChgDTO), serviceTaxForAdminFee);

	}

	private BigDecimal calculateTransactionFee(ExternalChgDTO ccChgDTO, ExternalChgDTO ccFeeTaxChgDTO) throws ModuleException {

		BigDecimal ccFee = getExternalChargeTotalAmount(ccChgDTO);
		BigDecimal ccFeeTax = getExternalChargeTotalAmount(ccFeeTaxChgDTO);

		return AccelAeroCalculator.add(ccFee, ccFeeTax);
	}

	private ExternalChgDTO deriveCCFeeChgDTO(PaymentBaseSessionStore paymentBaseSessionStore) throws ModuleException {

		List<LCCClientExternalChgDTO> externalCharges = new ArrayList<>();
		// store ccChgDTO (transactionFee) to the session before calling this method
		externalCharges.addAll(paymentBaseSessionStore.getExternalCharges());

		return paymentUtils.getApplicableServiceTax(EXTERNAL_CHARGES.JN_OTHER,
				paymentBaseSessionStore.getOperationType().getCode(), externalCharges,
				paymentBaseSessionStore.getApplicableServiceTaxes());

	}

	private ExternalChgDTO deriveCCFeeChgDTO(ExternalChgDTO ccChgDTO, PaymentBaseSessionStore paymentBaseSessionStore)
			throws ModuleException {

		if (ccChgDTO != null && BigDecimal.ZERO.compareTo(ccChgDTO.getAmount()) < 0) {

			List<LCCClientExternalChgDTO> externalCharges = new ArrayList<>();
			externalCharges.addAll(paymentBaseSessionStore.getExternalCharges());
			// add ccChgDTO
			externalCharges.add(new LCCClientExternalChgDTO(ccChgDTO));

			return paymentUtils.getApplicableServiceTax(EXTERNAL_CHARGES.JN_OTHER,
					paymentBaseSessionStore.getOperationType().getCode(), externalCharges,
					paymentBaseSessionStore.getApplicableServiceTaxes());
		}
		return null;
	}

	private boolean isAdminFeeRegulated() {
		return AppSysParamsUtil.isAdminFeeRegulationEnabled();
	}

	private boolean isAdminFeeRegulatedForChannel() {
		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appIndicator)) {
			appIndicator = AppIndicatorEnum.APP_IBE;
		}
		return AppSysParamsUtil.isAdminFeeChannelWiseConfigured(CommonServiceUtil.getSysParamKeyApplicationEngine(appIndicator),
				null);
	}

	private boolean isAdminFeeRegulatedAndAppliedForChannel() {
		return isAdminFeeRegulated() && isAdminFeeRegulatedForChannel();

	}

	private void composeAdminFeeRS(BigDecimal amountForAdminFeeCalculation, BigDecimal totalAllInclusive,
			PaymentBaseSessionStore paymentBaseSessionStore, AdministrationFeeRS adminFeeRS, boolean balanceCase,
			String transactionId, boolean isBinPromo, ServiceTaxRQ serviceTaxRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		composeAdminFeeRS(amountForAdminFeeCalculation, totalAllInclusive, paymentBaseSessionStore, adminFeeRS, balanceCase,
				transactionId, isBinPromo, serviceTaxRQ, true, trackInfoDTO);

	}

	private void composeAdminFeeRS(BigDecimal amountForAdminFeeCalculation, BigDecimal totalAllInclusive,
			PaymentBaseSessionStore paymentBaseSessionStore, AdministrationFeeRS adminFeeRS, boolean balanceCase,
			String transactionId, boolean isBinPromo, ServiceTaxRQ serviceTaxRQ, boolean storeInSession,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		paymentBaseSessionStore.removePaymentCharge(EXTERNAL_CHARGES.CREDIT_CARD);
		paymentBaseSessionStore.removePaymentCharge(EXTERNAL_CHARGES.JN_OTHER);

		adminFeeRS.setTransactionId(transactionId);
		adminFeeRS.setCurrency(AppSysParamsUtil.getBaseCurrency());

		if (isAdminFeeRegulatedAndAppliedForChannel() && amountForAdminFeeCalculation != null && !balanceCase) {

			BigDecimal administrationFee;

			// Added to session when BIN promotion is applied
			if (isBinPromo) {
				administrationFee = deriveAndValidateAdminFee(paymentBaseSessionStore, amountForAdminFeeCalculation);
			} else {
				ExternalChgDTO ccChgDTO = deriveCCChgDTO(paymentBaseSessionStore, 0, amountForAdminFeeCalculation);
				paymentBaseSessionStore.addPaymentCharge(ccChgDTO);
				paymentBaseSessionStore.setServiceTaxForTransactionFee(
						calculateServiceTaxForCCcharge(serviceTaxRQ, ccChgDTO, trackInfoDTO, true));
				ExternalChgDTO ccFeeTaxChgDTO = deriveCCFeeChgDTO(paymentBaseSessionStore);
				if (ccFeeTaxChgDTO != null) {
					paymentBaseSessionStore.addPaymentCharge(ccFeeTaxChgDTO);
				}
				paymentBaseSessionStore.setAdminFeeInitiated();
				administrationFee = calculateTransactionFee(ccChgDTO, ccFeeTaxChgDTO);
			}

			adminFeeRS.setAdministrationFee(AccelAeroCalculator
					.add(administrationFee, paymentBaseSessionStore.getServiceTaxForTransactionFee()).doubleValue());
			adminFeeRS.setDisplayAdministrationFee(true);
			adminFeeRS.setTotalAllInclusive(AccelAeroCalculator
					.add(totalAllInclusive, administrationFee, paymentBaseSessionStore.getServiceTaxForTransactionFee())
					.doubleValue());
		} else {
			adminFeeRS.setTotalAllInclusive(totalAllInclusive.doubleValue());
		}

		adminFeeRS.setServiceTaxForAdminFee(paymentBaseSessionStore.getServiceTaxForTransactionFee().doubleValue());
		adminFeeRS.setSuccess(true);
	}

	private boolean externalChargeSessionCheck(String externalChgName, ExternalChgDTO externalChgDTO,
			Collection<LCCClientExternalChgDTO> externalChgDTOs) {

		if (StringUtil.isNullOrEmpty(externalChgName)) {
			return false;
		}

		LCCClientExternalChgDTO ccExternalChgDto = null;

		if (externalChgDTO != null) {
			ccExternalChgDto = new LCCClientExternalChgDTO(externalChgDTO);

		}

		Iterator<LCCClientExternalChgDTO> itr = externalChgDTOs.iterator();

		boolean valid = false;

		while (itr.hasNext()) {
			LCCClientExternalChgDTO itrExternalChgDTO = itr.next();

			if (externalChgDTO == null) {
				valid = true;
				if (externalChgName.contentEquals(itrExternalChgDTO.getExternalCharges().name())) {
					return false;
				}

			} else if (ccExternalChgDto.getExternalCharges().name()
					.contentEquals(itrExternalChgDTO.getExternalCharges().name())) {
				valid = true;
				if (ccExternalChgDto.getAmount().compareTo(itrExternalChgDTO.getAmount()) != 0) {
					valid = false;
				}
			}
		}
		return valid;
	}

	private BigDecimal deriveAndValidateAdminFee(PaymentBaseSessionStore paymentBaseSessionStore,
			BigDecimal amountForAdminFeeCalculation) throws ModuleException {

		BigDecimal totalTransactionFee;
		ExternalChgDTO ccChgDTO = deriveCCChgDTO(paymentBaseSessionStore, 0, amountForAdminFeeCalculation);

		if (!externalChargeSessionCheck(EXTERNAL_CHARGES.CREDIT_CARD.toString(), ccChgDTO,
				paymentBaseSessionStore.getExternalCharges())) {
			throw new ModuleException("payment.api.admin.fee.mismatch");
		}

		ExternalChgDTO ccFeeTaxChgDTO = deriveCCFeeChgDTO(paymentBaseSessionStore);

		if (!externalChargeSessionCheck(EXTERNAL_CHARGES.JN_OTHER.toString(), ccFeeTaxChgDTO,
				paymentBaseSessionStore.getExternalCharges())) {
			throw new ModuleException("payment.api.admin.fee.mismatch");
		}

		totalTransactionFee = calculateTransactionFee(ccChgDTO, ccFeeTaxChgDTO);
		return totalTransactionFee;
	}

	private List<ReservationChargeDetail> composeReservationChargeDetails(PaymentSessionStore paymentSessionStore)
			throws ModuleException {

		ChargeDetailsBuilder chargeDetailsBuilder = new ChargeDetailsBuilder(paymentSessionStore);
		List<ReservationChargeDetail> chargeDetails = chargeDetailsBuilder.buildCreateFlowReservationChargeDetails();

		return chargeDetails;

	}

	@Override
	public ReservationChargesRS getReservationChargeDetailsRS(ReservationChargesRQ reservationChargesRQ,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		setAppIndicatorEnum(trackInfoDTO);

		PaymentRequoteSessionStore paymentRequoteSessionStore = getRequotePaymentSessionStore(
				reservationChargesRQ.getTransactionId());
		paymentRequoteSessionStore.storeOperationType(OperationTypes.MODIFY_ONLY);

		ReservationChargesRS reservationChargesRS = new ReservationChargesRS();
		reservationChargesRS.setTransactionId(reservationChargesRQ.getTransactionId());
		reservationChargesRS.setCurrency(AppSysParamsUtil.getBaseCurrency());

		reservationChargesRS
				.setReservationChargeDetails(composeReservationNewChargeDetailsForRequote(paymentRequoteSessionStore));
		reservationChargesRS.setSuccess(true);

		return reservationChargesRS;

	}

	private List<ReservationChargeDetail> composeReservationNewChargeDetailsForRequote(
			PaymentRequoteSessionStore paymentRequoteSessionStore) throws ModuleException {

		ChargeDetailsBuilder chargeDetailsBuilder = new ChargeDetailsBuilder(paymentRequoteSessionStore);
		List<ReservationChargeDetail> chargeDetails = chargeDetailsBuilder.buildRequoteFlowReservationChargeDetails();

		return chargeDetails;
	}

	private String getOnholdReleaseDuration(Date onholdReleaseZuluTimeStamp) {
		if (onholdReleaseZuluTimeStamp != null) {
			LocalDateTime onholdReleaseDate = onholdReleaseZuluTimeStamp.toInstant().atZone(ZoneId.systemDefault())
					.toLocalDateTime();
			LocalDateTime now = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			Long durationInMinutes = ChronoUnit.MINUTES.between(now, onholdReleaseDate);
			Long durationInDays = ChronoUnit.DAYS.between(now, onholdReleaseDate);
			StringBuilder ohdTime = new StringBuilder();
			if (durationInDays > 0) {
				ohdTime.append(durationInDays + " Day(s) : ");
			}
			ohdTime.append(durationInMinutes / 60 + " Hour(s) : ");
			ohdTime.append(durationInMinutes % 60 + " Minute(s) ");
			return ohdTime.toString();
		}
		return null;
	}

	public void applyServiceTax(String transactionId, ServiceTaxRQ serviceTaxRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>();
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();

		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(transactionId);
		BaseAvailRQ baseAvailRQ = paymentUtils.adaptBaseAvailRQ(
				paymentUtils.getPaxQtyList(paymentSessionStore.getTravellerQuantityForPayment()),
				paymentSessionStore.getPreferencesForPayment(),
				CommonServiceUtil.getApplicationEngine(trackInfoDTO.getAppIndicator()));
		List<FlightSegmentTO> flightSegments = paymentUtils
				.adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());

		// Removing Service Tax from PaymentSessionStore in payment external charge
		paymentSessionStore.removePaymentPaxCharge(EXTERNAL_CHARGES.SERVICE_TAX);

		// populate Service tax related Info
		populateServiceTaxRelatedInfo(paxWiseExternalCharges, paxWisePaxTypes, paxList, transactionId, null, trackInfoDTO);

		ServiceTaxCalculatorUtil.printQuotedExternalChargesPaxWise(paxWiseExternalCharges);

		// Constructing ServiceTaxQuoteCriteriaDTO obj
		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = paymentUtils.adaptServiceTaxQuoteCriteriaDTO(baseAvailRQ,
				paymentSessionStore.getFareSegChargeTOForPayment(), flightSegments, serviceTaxRQ, paxWiseExternalCharges,
				paxWisePaxTypes);

		// Calling Service Tax Call
		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator.getAirproxyReservationBD()
				.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfoDTO);

		// Applying Service Tax RS to Pax wise Charges
		ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(paxList, serviceTaxQuoteRS, false, ApplicationEngine.IBE, false);

		// Store Service Taxes
		storeServiceTaxChargesToSession(transactionId, paxList);
	}

	public BigDecimal calculateServiceTaxForCCcharge(ServiceTaxRQ serviceTaxRQ, ExternalChgDTO extChgDTO,
			TrackInfoDTO trackInfoDTO, boolean storeInSession) throws ModuleException {
		BigDecimal serviceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (serviceTaxRQ != null) {
			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
			List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>();

			PaymentBaseSessionStore paymentSessionStore = getPaymentBaseSessionStore(serviceTaxRQ.getTransactionId());
			BaseAvailRQ baseAvailRQ = paymentUtils.adaptBaseAvailRQ(
					paymentUtils.getPaxQtyList(paymentSessionStore.getTravellerQuantityForPayment()),
					paymentSessionStore.getPreferencesForPayment(),
					CommonServiceUtil.getApplicationEngine(trackInfoDTO.getAppIndicator()));
			List<FlightSegmentTO> flightSegments = paymentUtils
					.adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());

			// populate Service tax related Info
			populateServiceTaxRelatedInfo(paxWiseExternalCharges, paxWisePaxTypes, paxList, serviceTaxRQ.getTransactionId(),
					extChgDTO, trackInfoDTO);

			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = paymentUtils.adaptServiceTaxQuoteCriteriaDTO(
					baseAvailRQ, null, flightSegments, serviceTaxRQ, paxWiseExternalCharges, paxWisePaxTypes);
			// for CC fee only we populate this flag
			serviceTaxQuoteCriteriaDTO.setCalculateOnlyForExternalCharges(true);
			ServiceTaxCalculatorUtil.printQuotedExternalChargesPaxWise(paxWiseExternalCharges);

			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator
					.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfoDTO);

			if (storeInSession) {
				ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(paxList, serviceTaxQuoteRS, false, ApplicationEngine.IBE, false);
				for (ReservationPaxTO pax : paxList) {
					for (LCCClientExternalChgDTO extCharge : pax.getExternalCharges()) {
						serviceTaxAmount = AccelAeroCalculator.add(serviceTaxAmount, extCharge.getAmount());
					}
					if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
						ReservationPaxTO parent = paymentUtils.findParent(pax, paxList);
						if (parent != null && parent.getInfantExternalCharges() != null) {
							for (LCCClientExternalChgDTO extCharge : parent.getInfantExternalCharges()) {
								serviceTaxAmount = AccelAeroCalculator.add(serviceTaxAmount, extCharge.getAmount());
							}
						}
					}
				}

				// Store Service Taxes
				storeServiceTaxChargesToSession(serviceTaxRQ.getTransactionId(), paxList);

				if (!isAdminFeeRegulatedAndAppliedForChannel()) {
					Map<Integer, List<LCCClientExternalChgDTO>> paxWiseServiceTaxFeeForPostPayment = paymentSessionStore
							.getPostPaymentInformation().getPaxWiseServiceTaxCCFee();
					for (ReservationPaxTO pax : paxList) {
						if (!paxWiseServiceTaxFeeForPostPayment.containsKey(pax.getSeqNumber())) {
							paxWiseServiceTaxFeeForPostPayment.put(pax.getSeqNumber(), new ArrayList<LCCClientExternalChgDTO>());
						}
						paxWiseServiceTaxFeeForPostPayment.get(pax.getSeqNumber()).addAll(pax.getExternalCharges());

						if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
							ReservationPaxTO parent = paymentUtils.findParent(pax, paxList);
							if (parent != null && parent.getInfantExternalCharges() != null) {
								paxWiseServiceTaxFeeForPostPayment.get(pax.getSeqNumber())
										.addAll(parent.getInfantExternalCharges());
							}
						}
					}
					// to avoid dry/interline quoting service taxes from own side
					storeServiceTaxExtCharges(serviceTaxQuoteRS, serviceTaxRQ.getTransactionId());
				}
			} else {
				serviceTaxAmount = ServiceTaxCalculatorUtil.getTotalServiceTaxAmount(paxList,
						paymentUtils.getPaxQtyList(paymentSessionStore.getTravellerQuantityForPayment()), serviceTaxQuoteRS,
						ApplicationEngine.IBE);

			}
		}

		return serviceTaxAmount;
	}

	// if extChgDTO param is set to null it will take all the external charges excluding JN_OTHER
	private void populateServiceTaxRelatedInfo(Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges,
			Map<Integer, String> paxWisePaxTypes, List<ReservationPaxTO> paxList, String transactionId, ExternalChgDTO extChgDTO,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		ReservationPaxTO resPaxTO = null;

		PaymentBaseSessionStore paymentSessionStore = getPaymentBaseSessionStore(transactionId);
		ReservationInfo resInfo = paymentSessionStore.getResInfo();
		int rateApplicableType = paymentSessionStore.getOperationType().getCode();

		if (resInfo != null && !resInfo.getPaxList().isEmpty()) {
			for (ReservationPaxTO reservationPaxTO : resInfo.getPaxList()) {

				resPaxTO = new ReservationPaxTO();
				resPaxTO.setSeqNumber(reservationPaxTO.getSeqNumber());
				resPaxTO.setIsParent(reservationPaxTO.getIsParent());
				resPaxTO.setPaxType(reservationPaxTO.getPaxType());
				resPaxTO.setInfantWith(reservationPaxTO.getInfantWith());
				paxList.add(resPaxTO);

				paxWisePaxTypes.put(reservationPaxTO.getSeqNumber(), reservationPaxTO.getPaxType());
			}
		}
		// using the same method to get all external charges or desired external charges
		if (extChgDTO == null) {
			paymentUtils.perPaxExternalCharges(paxWiseExternalCharges, paymentSessionStore.getFinancialStore(),
					paymentSessionStore.getSelectedSegmentsForPayment(), paymentSessionStore.getTravellerQuantityForPayment(),
					paymentSessionStore.getSessionFlexiDetailForPayment(), paxList, rateApplicableType);
		} else {
			paymentUtils.constructPaxWiseCCChargeMap(rateApplicableType, paymentSessionStore.getSelectedSegmentsForPayment(),
					paxList, paxWiseExternalCharges, extChgDTO.getAmount());

		}
	}

	private void storeServiceTaxChargesToSession(String transactionId, List<ReservationPaxTO> paxList) {
		PassengerChargeTo<LCCClientExternalChgDTO> passengerChargeTo;

		PaymentBaseSessionStore paymentSessionStore = getPaymentBaseSessionStore(transactionId);
		ReservationChargeTo<LCCClientExternalChgDTO> reservationChargeTo = paymentSessionStore.getFinancialStore()
				.getPaymentExternalCharges();

		// Adding newly quoted service taxes pax wise into payment session store
		if (reservationChargeTo.getPassengers() == null || reservationChargeTo.getPassengers().isEmpty()) {
			reservationChargeTo.setPassengers(new ArrayList<PassengerChargeTo<LCCClientExternalChgDTO>>());
			for (ReservationPaxTO reservationPaxTO : paxList) {
				passengerChargeTo = new PassengerChargeTo<>();
				passengerChargeTo.setPassengerRph(reservationPaxTO.getSeqNumber().toString());
				passengerChargeTo.setGetPassengerCharges(reservationPaxTO.getExternalCharges());
				reservationChargeTo.getPassengers().add(passengerChargeTo);

			}
		} else {
			for (ReservationPaxTO reservationPaxTO : paxList) {
				boolean paxExists = false;
				for (PassengerChargeTo<LCCClientExternalChgDTO> existingPassengerChargeTo : reservationChargeTo.getPassengers()) {
					if (existingPassengerChargeTo.getPassengerRph().equals(reservationPaxTO.getSeqNumber().toString())) {
						paxExists = true;
						for (LCCClientExternalChgDTO modifiedLCCClientExternalChgDTO : reservationPaxTO.getExternalCharges()) {
							if (modifiedLCCClientExternalChgDTO.getExternalCharges()
									.equals(ReservationInternalConstants.EXTERNAL_CHARGES.SERVICE_TAX)) {
								existingPassengerChargeTo.getGetPassengerCharges().add(modifiedLCCClientExternalChgDTO);
							}
						}

					}
				}
				if (!paxExists) {
					passengerChargeTo = new PassengerChargeTo<>();
					passengerChargeTo.setPassengerRph(reservationPaxTO.getSeqNumber().toString());
					passengerChargeTo.setGetPassengerCharges(reservationPaxTO.getExternalCharges());
					reservationChargeTo.getPassengers().add(passengerChargeTo);
				}
			}
		}
		// update infant charges added to parent
		for (PassengerChargeTo<LCCClientExternalChgDTO> sessionPax : reservationChargeTo.getPassengers()) {
			for (ReservationPaxTO pax : paxList) {
				if (PaxTypeTO.INFANT.equals(pax.getPaxType())
						&& pax.getSeqNumber().equals(Integer.parseInt(sessionPax.getPassengerRph()))) {
					ReservationPaxTO parent = paymentUtils.findParent(pax, paxList);
					if (parent != null && parent.getInfantExternalCharges() != null) {
						sessionPax.getGetPassengerCharges().addAll(parent.getInfantExternalCharges());
					}
				}
			}
		}
	}

	private void storeServiceTaxExtCharges(Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS,
			String transactionId) {
		if (serviceTaxQuoteRS != null) {
			PaymentBaseSessionStore paymentSessionStore = getPaymentBaseSessionStore(transactionId);
			Map<String, ServiceTaxExtChgDTO> serviceTaxExtChargeMap = paymentSessionStore.getPostPaymentInformation()
					.getChargeCodeWiseServiceTaxExtCharges();
			for (String carrier : serviceTaxQuoteRS.keySet()) {
				ServiceTaxQuoteForTicketingRevenueResTO serviceTaxTktRs = serviceTaxQuoteRS.get(carrier);
				if (serviceTaxTktRs.getPaxTypeWiseServiceTaxes() != null
						&& !serviceTaxTktRs.getPaxTypeWiseServiceTaxes().isEmpty()) {
					for (String paxType : serviceTaxTktRs.getPaxTypeWiseServiceTaxes().keySet()) {
						for (ServiceTaxTO serviceTaxTo : serviceTaxTktRs.getPaxTypeWiseServiceTaxes().get(paxType)) {
							if (!serviceTaxExtChargeMap.containsKey(carrier + "|" + serviceTaxTo.getChargeCode())) {
								ServiceTaxExtChgDTO serviceExtCharge = new ServiceTaxExtChgDTO();
								serviceExtCharge.setChargeCode(serviceTaxTo.getChargeCode());
								serviceExtCharge.setChgGrpCode(serviceTaxTo.getChargeGroupCode());
								serviceExtCharge.setChgRateId(serviceTaxTo.getChargeRateId());
								serviceTaxExtChargeMap.put(carrier + "|" + serviceTaxTo.getChargeCode(), serviceExtCharge);
							}
						}
					}
				}
				if (serviceTaxTktRs.getPaxWiseServiceTaxes() != null && !serviceTaxTktRs.getPaxWiseServiceTaxes().isEmpty()) {
					for (Integer paxSeq : serviceTaxTktRs.getPaxWiseServiceTaxes().keySet()) {
						for (ServiceTaxTO serviceTaxTo : serviceTaxTktRs.getPaxWiseServiceTaxes().get(paxSeq)) {
							if (!serviceTaxExtChargeMap.containsKey(carrier + "|" + serviceTaxTo.getChargeCode())) {
								ServiceTaxExtChgDTO serviceExtCharge = new ServiceTaxExtChgDTO();
								serviceExtCharge.setChargeCode(serviceTaxTo.getChargeCode());
								serviceExtCharge.setChgGrpCode(serviceTaxTo.getChargeGroupCode());
								serviceExtCharge.setChgRateId(serviceTaxTo.getChargeRateId());
								serviceExtCharge
										.setExternalChargesEnum(ReservationInternalConstants.EXTERNAL_CHARGES.SERVICE_TAX);
								serviceTaxExtChargeMap.put(carrier + "|" + serviceTaxTo.getChargeCode(), serviceExtCharge);
							}
						}
					}
				}
			}
		}
	}

	private void updateExistingBlockedCreditInfo(LoyaltyInformation loyaltyInformation, BigDecimal totalPaymentAmount, String pnr,
			Integer temporyPayId) throws ModuleException {
		try {

			if (AppSysParamsUtil.isLMSEnabled() 
					&& AppSysParamsUtil.isEnableSingleStepRedeemingProcess()
					&& loyaltyInformation != null
					&& loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo() != null
					&& !loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().isEmpty()) {

				LoyaltyPaymentInfo loyaltyPaymentInfo = loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo()
						.get(AppSysParamsUtil.getDefaultCarrierCode());

				BigDecimal totalBlockedCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (String key : loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().keySet()) {
					LoyaltyPaymentInfo loyaltyPayment = loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().get(key);
					if (loyaltyPayment != null) {
						if (loyaltyPaymentInfo == null) {
							loyaltyPaymentInfo = loyaltyPayment;
						}
						totalBlockedCredit = AccelAeroCalculator.add(totalBlockedCredit, loyaltyPayment.getTotalPayment());
					}
				}

				if (loyaltyPaymentInfo != null && loyaltyPaymentInfo.getTotalPayment().compareTo(BigDecimal.ZERO) > 0) {
					log.info("PREPARING UPDATE LMS BLOCKED CREDIT PAYMENT INFO ");
					LmsBlockedCredit lmsBlockedCredit = null;
					lmsBlockedCredit = ModuleServiceLocator.getReservationBD()
							.getLmsBlockCreditInfoByRewardId(loyaltyPaymentInfo.getLoyaltyRewardIds());
					if (lmsBlockedCredit != null) {
						lmsBlockedCredit.setPaymentTimeStamp(CalendarUtil.getCurrentZuluDateTime());
						lmsBlockedCredit.setMemberId(loyaltyPaymentInfo.getMemberAccountId());
						lmsBlockedCredit.setBlockedCreditAmount(totalBlockedCredit);
						lmsBlockedCredit.setTempTnxId(temporyPayId);
						lmsBlockedCredit.setTotalPaymentAmount(totalPaymentAmount);
						lmsBlockedCredit.setCreditUtilizedStatus(LmsBlockedCredit.PENDING);
						log.info("BEORE SAVING LMS BLOCKED CREDIT PAYMENT INFO " + lmsBlockedCredit.toString());
						ModuleServiceLocator.getReservationBD().saveLmsBlockCreditInfo(lmsBlockedCredit);
						log.info("COMPLETED SAVE LMS BLOCKED CREDIT PAYMENT INFO ");
					}
				}

			}
		} catch (Exception e) {
			log.error("ERROR @ SAVING LMS BLOCKED CREDIT PAYMENT INFO ");
			throw new ModuleException("payment.api.lms.save.block.credit.failed");
		}

	}

	private void addBlockCreditInfoIssueRewards(LoyaltyInformation loyaltyInformation, String pnr, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		try {

			if (AppSysParamsUtil.isLMSEnabled() 
					&& AppSysParamsUtil.isEnableSingleStepRedeemingProcess()
					&& loyaltyInformation != null
					&& loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo() != null
					&& !loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().isEmpty()) {

				LoyaltyPaymentInfo loyaltyPaymentInfo = loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo()
						.get(AppSysParamsUtil.getDefaultCarrierCode());

				BigDecimal totalBlockedCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (String key : loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().keySet()) {
					LoyaltyPaymentInfo loyaltyPayment = loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().get(key);
					if (loyaltyPayment != null) {
						if (loyaltyPaymentInfo == null) {
							loyaltyPaymentInfo = loyaltyPayment;
						}
						totalBlockedCredit = AccelAeroCalculator.add(totalBlockedCredit, loyaltyPayment.getTotalPayment());
					}
				}

				if (loyaltyPaymentInfo != null && loyaltyPaymentInfo.getTotalPayment().compareTo(BigDecimal.ZERO) > 0) {
					log.info("PREPARING SAVING LMS BLOCKED CREDIT PAYMENT INFO ");
					LmsBlockedCredit lmsBlockedCredit = new LmsBlockedCredit();
					lmsBlockedCredit.setPaymentTimeStamp(CalendarUtil.getCurrentZuluDateTime());
					lmsBlockedCredit.setMemberId(loyaltyPaymentInfo.getMemberAccountId());
					lmsBlockedCredit.setLmsCancelAttempts(0);
					lmsBlockedCredit.setBlockedCreditAmount(totalBlockedCredit);
					lmsBlockedCredit.setTotalPaymentAmount(loyaltyPaymentInfo.getTotalPayment());
					lmsBlockedCredit.setSalesChannelCode(trackInfoDTO.getOriginChannelId());
					lmsBlockedCredit.setLmsCreditPaymentInfo(com.isa.thinair.commons.core.util.XMLStreamer
							.compose(loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo()));
					lmsBlockedCredit.setPnr(pnr);
					lmsBlockedCredit.setCreditUtilizedStatus(LmsBlockedCredit.ISSUE);

					if (loyaltyPaymentInfo.getLoyaltyRewardIds() != null && loyaltyPaymentInfo.getLoyaltyRewardIds().length > 0) {
						lmsBlockedCredit.setLmsRewardIds(
								Arrays.stream(loyaltyPaymentInfo.getLoyaltyRewardIds()).collect(Collectors.toSet()));
					}
					log.info("BEORE SAVING LMS BLOCKED CREDIT PAYMENT INFO " + lmsBlockedCredit.toString());
					ModuleServiceLocator.getReservationBD().saveLmsBlockCreditInfo(lmsBlockedCredit);
					log.info("COMPLETED SAVE LMS BLOCKED CREDIT PAYMENT INFO ");
				}

			}
		} catch (Exception e) {
			log.error("ERROR @ SAVING LMS BLOCKED CREDIT PAYMENT INFO ");
			throw new ModuleException("payment.api.lms.save.block.credit.failed");
		}
	}

	private void saveLMSBlockCreditDetails(LoyaltyInformation loyaltyInformation, int temporyPayId, BigDecimal totalPaymentAmount,
			String pnr, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {

			if (AppSysParamsUtil.isLMSEnabled()
					&& !AppSysParamsUtil.isEnableSingleStepRedeemingProcess()
					&& loyaltyInformation != null
					&& loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo() != null
					&& !loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().isEmpty()) {

				LoyaltyPaymentInfo loyaltyPaymentInfo = loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo()
						.get(AppSysParamsUtil.getDefaultCarrierCode());

				BigDecimal totalBlockedCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (String key : loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().keySet()) {
					LoyaltyPaymentInfo loyaltyPayment = loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().get(key);
					if (loyaltyPayment != null) {
						if (loyaltyPaymentInfo == null) {
							loyaltyPaymentInfo = loyaltyPayment;
						}
						totalBlockedCredit = AccelAeroCalculator.add(totalBlockedCredit, loyaltyPayment.getTotalPayment());
					}
				}

				if (loyaltyPaymentInfo != null && loyaltyPaymentInfo.getTotalPayment().compareTo(BigDecimal.ZERO) > 0) {
					log.info("PREPARING SAVING LMS BLOCKED CREDIT PAYMENT INFO ");
					LmsBlockedCredit lmsBlockedCredit = new LmsBlockedCredit();
					lmsBlockedCredit.setPaymentTimeStamp(CalendarUtil.getCurrentZuluDateTime());// need
					lmsBlockedCredit.setMemberId(loyaltyPaymentInfo.getMemberAccountId());// need
					lmsBlockedCredit.setTempTnxId(temporyPayId);
					lmsBlockedCredit.setLmsCancelAttempts(0);
					lmsBlockedCredit.setBlockedCreditAmount(totalBlockedCredit);// it is possible
					lmsBlockedCredit.setTotalPaymentAmount(totalPaymentAmount);
					lmsBlockedCredit.setSalesChannelCode(trackInfoDTO.getOriginChannelId());
					lmsBlockedCredit.setLmsCreditPaymentInfo(com.isa.thinair.commons.core.util.XMLStreamer
							.compose(loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo()));
					lmsBlockedCredit.setCreditUtilizedStatus(LmsBlockedCredit.PENDING);

					if (loyaltyPaymentInfo.getLoyaltyRewardIds() != null && loyaltyPaymentInfo.getLoyaltyRewardIds().length > 0) {
						lmsBlockedCredit.setLmsRewardIds(
								Arrays.stream(loyaltyPaymentInfo.getLoyaltyRewardIds()).collect(Collectors.toSet()));
					}
					log.info("BEORE SAVING LMS BLOCKED CREDIT PAYMENT INFO " + lmsBlockedCredit.toString());
					ModuleServiceLocator.getReservationBD().saveLmsBlockCreditInfo(lmsBlockedCredit);
					log.info("COMPLETED SAVE LMS BLOCKED CREDIT PAYMENT INFO ");
				}

			}
		} catch (Exception e) {
			log.error("ERROR @ SAVING LMS BLOCKED CREDIT PAYMENT INFO ");
			throw new ModuleException("payment.api.lms.save.block.credit.failed");
		}
	}

	@Override
	public MakePaymentRS processVoucherPayment(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException {
		PaymentSessionStore paymentSessionStore = getPaymentSessionStore(makePaymentRQ.getTransactionId());
		paymentSessionStore.storeOperationType(OperationTypes.MAKE_ONLY);
		BigDecimal totalWithoutAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal effectiveTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		setAppIndicatorEnum(trackInfoDTO);
		validationService.preValidateCommonPaymentRQ(makePaymentRQ);
		validationService.validateCreateBookingPaymentRQ(makePaymentRQ, paymentSessionStore);
		return processPaymentRequest(makePaymentRQ, paymentSessionStore, trackInfoDTO, clientInfoDTO, false, totalWithoutAnci,
				effectiveTotal, false, false, effectiveTotal);
	}

	@Override
	public PaymentOptionsRS getIssueVoucherPaymentOptionsRS(TrackInfoDTO trackInfoDTO, PaymentOptionsRQ paymentOptionsRQ) throws ModuleException {
		setAppIndicatorEnum(trackInfoDTO);
		PaymentOptionsRS paymentOptionsRS;
		ComposePaymentDTO paymentDTO = (new ComposePaymentDTO()).setTrackInfoDTO(trackInfoDTO);
		paymentOptionsRS = composePaymentOptionsResponseForIssueVoucher(paymentDTO, paymentOptionsRQ);
		setPaymentGatewayWiseChargeValuePercentage(paymentOptionsRS);
		return paymentOptionsRS;
	}

	private void setPaymentGatewayWiseChargeValuePercentage(PaymentOptionsRS paymentOptionsRS) throws ModuleException {
		if (paymentOptionsRS != null && paymentOptionsRS.getPaymentOptions() != null
				& paymentOptionsRS.getPaymentOptions().getPaymentGateways() != null) {
			ExternalChgDTO externalChgDTO = GiftVoucherUtils.getCCFeeExternalChargeDTOFORVoucherPurchase();

			for (PaymentGateway paymentGateway : paymentOptionsRS.getPaymentOptions().getPaymentGateways()) {

				PaymentGatewayWiseCharges pgwCharge = ModuleServiceLocator.getChargeBD()
						.getPaymentGatewayWiseCharge(paymentGateway.getGatewayId());

				if (pgwCharge != null && pgwCharge.getChargeId() > 0) {
					// valid charge is defined for the selected PGW
					paymentGateway.setIsTransactionFeeInPercentage(
							!PaymentConsts.CHARGE_BY_VALUE.equals(pgwCharge.getValuePercentageFlag()));
					paymentGateway.setTransactionFeeValue(BigDecimal.valueOf(pgwCharge.getChargeValuePercentage()));
				} else if (externalChgDTO != null) {
					paymentGateway.setTransactionFeeValue(externalChgDTO.getRatioValue());
					paymentGateway.setIsTransactionFeeInPercentage(externalChgDTO.isRatioValueInPercentage());
				}
			}
		}
	}

	private List<PaymentGateway> buildPaymentGatewayOptionsForIssueVouhcher(BuildGatewaysDTO buildGatewaysDTO,
			PaymentOptionsRQ paymentOptionsRQ)
			throws ModuleException {
		String moduleCode = CommonServiceUtil.getApplicationEngine(buildGatewaysDTO.getTrackInfoDTO().getAppIndicator())
				.toString();
		String firstSegOndCode = null;
		IPGPaymentOptionDTO paymentOptionDTO = (new IPGPaymentOptionDTO()).setSelCurrency(AppSysParamsUtil.getDefaultPGCurrency())
				.setOfflinePayment(buildGatewaysDTO.isOfflinePaymentEnable()).setModuleCode(moduleCode)
				.setFirstSegmentONDCode(firstSegOndCode);

		Collection<IPGPaymentOptionDTO> ipgPaymentGateways = getIPGPaymentGateways(paymentOptionDTO);
		Collection<Integer> paymentGatewayIds = paymentUtils.getPaymentGatewayIds(ipgPaymentGateways);
		String countryCode = getCountryCode(buildGatewaysDTO.getTrackInfoDTO().getIpAddress(),
				paymentOptionsRQ.getOriginCountryCode());
		
		// As there is no ond code, we can not use ONDWisePayment
		Collection<CountryPaymentCardBehaviourDTO> cardsInfo = getCountryPaymentCardBehavior(countryCode, paymentGatewayIds);
		List<PaymentGateway> paymentGateways = new ArrayList<PaymentGateway>();

		// per PGW, add card list, calculate PGW fees
		for (IPGPaymentOptionDTO tmpIPGPaymentOptionDTO : ipgPaymentGateways) {
			String baseCurrency = tmpIPGPaymentOptionDTO.getBaseCurrency();
			int paymentGatewayId = tmpIPGPaymentOptionDTO.getPaymentGateway();
			Properties ipgProps = paymentUtils.getIPGProperties(paymentGatewayId, baseCurrency);
			PaymentGateway paymentGateway = adaptPaymentGateway(tmpIPGPaymentOptionDTO, ipgProps);
			if (paymentGateway == null) {
				continue;
			}
			List<CountryPaymentCardBehaviourDTO> pgCards = getPGActiveCards(paymentGatewayId, cardsInfo);
			if (pgCards == null || pgCards.isEmpty()) {
				continue;
			}
			CardAdaptor reservationPaxAdaptor = new CardAdaptor();
			List<Card> cards = new ArrayList<Card>();
			AdaptorUtils.adaptCollection(pgCards, cards, reservationPaxAdaptor);
			paymentGateway.setCards(cards);
			paymentGateways.add(paymentGateway);
		}

		return orderByPaymentMethod(paymentGateways, cardsInfo);
	}

	private PaymentOptionsRS composePaymentOptionsResponseForIssueVoucher(ComposePaymentDTO paymentDTO,
			PaymentOptionsRQ paymentOptionsRQ) throws ModuleException {
		TrackInfoDTO trackInfoDTO = paymentDTO.getTrackInfoDTO();
		String transactionId = paymentDTO.getTransactionId();
		PaymentOptionsRS paymentOptionsRS = new PaymentOptionsRS();
		PaymentOptions paymentOptions = new PaymentOptions();
		boolean offlinePaymentEnable = false;
		List<PaymentGateway> paymentGateways = null;
		BuildGatewaysDTO buildGatewaysDTO = (new BuildGatewaysDTO()).setOfflinePaymentEnable(offlinePaymentEnable)
				.setTrackInfoDTO(trackInfoDTO);
		paymentGateways = buildPaymentGatewayOptionsForIssueVouhcher(buildGatewaysDTO, paymentOptionsRQ);
		paymentOptions.setPaymentGateways(paymentGateways);
		paymentOptionsRS.setPaymentOptions(paymentOptions);
		paymentOptionsRS.setSuccess(true);
		paymentOptionsRS.setTransactionId(transactionId);

		return paymentOptionsRS;

	}

	@Override
	public VoucherRedeemRS redeemVouchers(VoucherRedeemRQ voucherRedeemRQ, TrackInfoDTO trackInfoDTO) throws ModuleException {
		VoucherRedeemRS voucherRedeemRS = new VoucherRedeemRS();
		AirproxyVoucherBD airproxyVoucherBD = ModuleServiceLocator.getAirproxyVoucherBD();

		Map<String, String> voucherOTPMap = getPaymentBaseSessionStore(voucherRedeemRQ.getTransactionId()).getVoucherOTPMap();
		processVoucherRedeemRequest(voucherRedeemRQ, trackInfoDTO, voucherRedeemRS, airproxyVoucherBD, voucherOTPMap);

		storeVoucherRedeemData(voucherRedeemRQ.getTransactionId(), voucherRedeemRS.getPayByVoucherInfo());
		voucherRedeemRS.setTransactionId(voucherRedeemRQ.getTransactionId());
		return voucherRedeemRS;
	}

	private void storeVoucherRedeemData(String transactionId, PayByVoucherInfo payByVoucherInfo) {
		PaymentBaseSessionStore paymentSessionStore = getPaymentBaseSessionStore(transactionId);
		PayByVoucherInfo sessionPayByVoucherInfo = getUnifiedVoucherSessionData(payByVoucherInfo,
				paymentSessionStore.getPayByVoucherInfo());
		paymentSessionStore.setPayByVoucherInfo(sessionPayByVoucherInfo);
	}

	private void processVoucherRedeemRequest(VoucherRedeemRQ voucherRedeemRQ, TrackInfoDTO trackInfoDTO,
			VoucherRedeemRS voucherRedeemRS, AirproxyVoucherBD airproxyVoucherBD, Map<String, String> voucherOTPMap)
			throws ModuleException {
		VoucherRedeemResponse voucherRedeemResponse;
		List<String> messages = new ArrayList<String>();
		if (AppSysParamsUtil.isVoucherEnabled()) {
			if (voucherRedeemRQ.getVoucherRedeemRequest().getUnsuccessfulAttempts() < 2) {

				List<String> voucherIDList = voucherRedeemRQ.getVoucherRedeemRequest().getVoucherIDList();

				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
				Voucher voucher = voucherDelegate.getVoucher(voucherIDList.get(0));

				try {
					if (voucher.getTemplateId() != null && AppSysParamsUtil.isOTPEnabled()) {
						if (StringUtil.isNullOrEmpty(voucherRedeemRQ.getVoucherRedeemRequest().getOTP())) {
							voucherRedeemResponse = airproxyVoucherBD
									.getVoucherRedeemResponse(voucherRedeemRQ.getVoucherRedeemRequest());
							if (voucherRedeemResponse == null || StringUtil.isNullOrEmpty(voucherRedeemResponse.getOtp())) {
								messages.add(errorMessageSource.getMessage("gift.voucher.redeem.failed", null,
										LocaleContextHolder.getLocale()));
								voucherRedeemRS.setSuccess(false);
							} else {
								voucherOTPMap.put(voucherRedeemRQ.getVoucherRedeemRequest().getVoucherIDList().get(0),
										voucherRedeemResponse.getOtp());
								messages.add(errorMessageSource.getMessage("gift.voucher.redemption.otp.confirmation.succed",
										null, LocaleContextHolder.getLocale()));
								voucherRedeemRS.setSuccess(true);
							}
						} else {
							if (voucherRedeemRQ.getVoucherRedeemRequest().getOTP()
									.equals(voucherOTPMap.get(voucherIDList.get(0)))) {
								processVoucherRedeem(voucherRedeemRQ.getVoucherRedeemRequest(), voucherRedeemRS,
										airproxyVoucherBD, messages, trackInfoDTO, voucherRedeemRQ.getTransactionId());
							} else {
								messages.add(errorMessageSource.getMessage("gift.voucher.redeem.otp.invalid", null,
										LocaleContextHolder.getLocale()));
								log.error("OTP not matched try again");
								voucherRedeemRS.setSuccess(false);
							}
						}

					} else {
						processVoucherRedeem(voucherRedeemRQ.getVoucherRedeemRequest(), voucherRedeemRS, airproxyVoucherBD,
								messages, trackInfoDTO, voucherRedeemRQ.getTransactionId());
					}
				} catch (Exception e) {
					log.error("OTP not sent to customer");
					messages.add(errorMessageSource.getMessage("gift.voucher.invalid", null,
							LocaleContextHolder.getLocale()));
					voucherRedeemRS.setSuccess(false);
				}
			} else {
				log.error("No of unsuccessful attempt exceeded the threshold limit");
				messages.add(errorMessageSource.getMessage("gift.voucher.redeem.unsuccessful.threshold", null,
						LocaleContextHolder.getLocale()));
				voucherRedeemRS.setSuccess(false);
			}
		} else {
			log.error("Voucher functionality not enabled");
			messages.add(errorMessageSource.getMessage("gift.voucher.disabled", null, LocaleContextHolder.getLocale()));
			voucherRedeemRS.setSuccess(false);
		}
		voucherRedeemRS.setMessages(messages);
	}

	private void processVoucherRedeem(VoucherRedeemRequest voucherRedeemRequest, VoucherRedeemRS voucherRedeemRS,
			AirproxyVoucherBD airproxyVoucherBD, List<String> messages, TrackInfoDTO trackInfoDTO, String transactionId)
			throws ModuleException {

		try {
			VoucherRedeemResponse voucherRedeemResponse = redeemVouchers(voucherRedeemRequest, airproxyVoucherBD, trackInfoDTO,
					transactionId);
			PaybyVoucherInfoAdapter payByVoucherInfoAdapter = new PaybyVoucherInfoAdapter();
			PayByVoucherInfo payByVoucherInfo = payByVoucherInfoAdapter.adapt(voucherRedeemResponse);
			voucherRedeemRS.setPayByVoucherInfo(payByVoucherInfo);
			voucherRedeemRS.setSuccess(true);

		} catch (ModuleException me) {
			messages.add(errorMessageSource.getMessage("gift.voucher.redemption.failed", null, LocaleContextHolder.getLocale()));
			voucherRedeemRS.setMessages(messages);
			voucherRedeemRS.setSuccess(false);
			me.setCustumValidateMessage("Voucher Redemption Failed.");
			log.error("ReedemVoucher ==> Voucher Redemption Failed.", me);
		} catch (Exception e) {
			messages.add(errorMessageSource.getMessage("gift.voucher.redemption.failed", null, LocaleContextHolder.getLocale()));
			voucherRedeemRS.setMessages(messages);
			voucherRedeemRS.setSuccess(false);
			log.error("Voucher Redemption Failed", e);
		}
	}

	private VoucherRedeemResponse redeemVouchers(VoucherRedeemRequest voucherRedeemRequest, AirproxyVoucherBD airproxyVoucherBD,
			TrackInfoDTO trackInfoDTO, String transactionId) throws Exception {

		VoucherRedeemResponse voucherRedeemResponse;
		MasterDataService masterDataService = new MasterDataServiceAdaptor();
		BigDecimal balanceToPay = new BigDecimal(voucherRedeemRequest.getBalanceToPay());
		String baseCurrency = CommonServiceUtil.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY);
		CurrencyExchangeRate currencyExchangeRate = masterDataService.getCurrencyExchangeRate(baseCurrency);
		Currency currency = currencyExchangeRate.getCurrency();
		PaymentBaseSessionStore paymentSessionStore = getPaymentBaseSessionStore(transactionId);

		balanceToPay = AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), balanceToPay,
				currency.getBoundryValue(), currency.getBreakPoint());

		voucherRedeemRequest.setBalanceToPay(balanceToPay.toString());
		voucherRedeemResponse = airproxyVoucherBD.getVoucherRedeemResponse(voucherRedeemRequest);

		BigDecimal voucherTotal = new BigDecimal(voucherRedeemResponse.getVouchersTotal());

		if (balanceToPay.compareTo(voucherTotal) > -1) {
			voucherRedeemResponse
					.setBalTotalPay(AccelAeroCalculator.multiplyDefaultScale(currencyExchangeRate.getMultiplyingExchangeRate(),
							AccelAeroCalculator.subtract(AccelAeroCalculator.scaleValueDefault(balanceToPay),
									AccelAeroCalculator.scaleValueDefault(voucherTotal)))
							.toString());
		} else {
			voucherRedeemResponse.setBalTotalPay(AccelAeroCalculator.getDefaultBigDecimalZero().toString());
			paymentSessionStore.getVoucherInformation().setIsTotalAmountPaidFromVoucher(true);
		}
		return voucherRedeemResponse;
	}

	private PayByVoucherInfo getUnifiedVoucherSessionData(PayByVoucherInfo payByVoucherInfo,
			PayByVoucherInfo sessionPayByVoucherInfo) {
		if (payByVoucherInfo != null) {
			if (sessionPayByVoucherInfo != null) {
				sessionPayByVoucherInfo.setRedeemedTotal(
						AccelAeroCalculator.add(sessionPayByVoucherInfo.getRedeemedTotal(), payByVoucherInfo.getRedeemedTotal()));
				sessionPayByVoucherInfo
						.setVouchersTotal(AccelAeroCalculator.add(new BigDecimal(sessionPayByVoucherInfo.getVouchersTotal()),
								new BigDecimal(payByVoucherInfo.getVouchersTotal())).toString());
				sessionPayByVoucherInfo.setVoucherRedeemDateTime(payByVoucherInfo.getVoucherRedeemDateTime());
				sessionPayByVoucherInfo.setBalTotalPay(payByVoucherInfo.getBalTotalPay());
				sessionPayByVoucherInfo.setUnsuccessfulAttempts(payByVoucherInfo.getUnsuccessfulAttempts());
				sessionPayByVoucherInfo.getVoucherDTOList().addAll(payByVoucherInfo.getVoucherDTOList());
			} else {
				sessionPayByVoucherInfo = payByVoucherInfo;

			}
		}
		return sessionPayByVoucherInfo;
	}

	@Override
	public void voidRedeemVoucher(VoucherOption voucherOption, String transactionId) throws ModuleException {
		PayByVoucherInfo sessionPayByVoucherInfo = getPaymentBaseSessionStore(transactionId).getPayByVoucherInfo();
		getPaymentBaseSessionStore(transactionId).setVoucherOTP(null);
		if (voucherOption != null && voucherOption.isVoidRedemption() && sessionPayByVoucherInfo != null) {
			ModuleServiceLocator.getVoucherBD().unblockRedeemedVouchers(sessionPayByVoucherInfo.getVoucherIDList());
			getPaymentBaseSessionStore(transactionId).setPayByVoucherInfo(null);
			getPaymentBaseSessionStore(transactionId).getVoucherInformation().setIsTotalAmountPaidFromVoucher(false);
		}
	}

	private VoucherOption getRedeemedVoucherOption(String transactionId) {
		PayByVoucherInfo sessionPayByVoucherInfo = getPaymentBaseSessionStore(transactionId).getPayByVoucherInfo();
		VoucherOption sessionVoucherOption = null;
		if (sessionPayByVoucherInfo != null) {
			sessionVoucherOption = new VoucherOption();
			if (sessionPayByVoucherInfo.getVoucherIDList() != null && !sessionPayByVoucherInfo.getVoucherIDList().isEmpty()) {
				sessionVoucherOption.setLastRedeemedVoucherId(
						sessionPayByVoucherInfo.getVoucherIDList().get(sessionPayByVoucherInfo.getVoucherIDList().size() - 1));
			}
			
			sessionVoucherOption.setTotalRedeemedAmount(sessionPayByVoucherInfo.getRedeemedTotal());

			sessionVoucherOption.setTotalAmountRedeem(
					getPaymentBaseSessionStore(transactionId).getVoucherInformation().isTotalAmountPaidFromVoucher());
		}
		return sessionVoucherOption;
	}

	private void refreshVoucherRedeemedAmount(String transactionId, BigDecimal payableAmount) {
		PayByVoucherInfo sessionPayByVoucherInfo = getPaymentBaseSessionStore(transactionId).getPayByVoucherInfo();
		if (sessionPayByVoucherInfo != null) {
			BigDecimal sessionVouchersTotal = new BigDecimal(sessionPayByVoucherInfo.getVouchersTotal());
			if (payableAmount.compareTo(sessionVouchersTotal) < 0) {
				sessionPayByVoucherInfo.setRedeemedTotal(payableAmount);
				getPaymentBaseSessionStore(transactionId).getVoucherInformation().setIsTotalAmountPaidFromVoucher(true);
			} else {
				sessionPayByVoucherInfo.setRedeemedTotal(sessionVouchersTotal);
				getPaymentBaseSessionStore(transactionId).getVoucherInformation()
						.setIsTotalAmountPaidFromVoucher(false);
			}
		}
	}

	@Override
	public MakePaymentRS createAlias(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO) throws ModuleException {
		MakePaymentRS makePaymentRS = new MakePaymentRS();

		boolean success = false;

		validationService.validateCreateAliasMakePaymentRQ(makePaymentRQ, trackInfoDTO.getCustomerId());

		PaymentGateway paymentGateway = extractPaymentGateway(makePaymentRQ);
		Integer paymentgatewayId = paymentGateway.getGatewayId();
		Card cardInfo = getCreditCardInformation(paymentGateway);

		String generatedAlias = CustomerAliasGenerator.generateAlias(cardInfo.getCardNo());

		IPGRequestDTO ipgRequestDTO = composeIPGRequestDTOForAlias(paymentgatewayId, cardInfo, trackInfoDTO, generatedAlias);

		IPGResponseDTO paymentResponse = processGatewayPayment(ipgRequestDTO, trackInfoDTO);

		processGatewayRefund(ipgRequestDTO, paymentResponse, trackInfoDTO);

		success = updateCustomerAlias(ipgRequestDTO, generatedAlias, paymentResponse);

		propagateAliasPaymentResponse(makePaymentRS, success);

		return makePaymentRS;
	}
	
	
	private void propagateAliasPaymentResponse(MakePaymentRS makePaymentRS, boolean success) {
		if (success) {
			makePaymentRS.setPaymentStatus(PaymentStatus.SUCCESS);
			makePaymentRS.setSuccess(true);
			makePaymentRS.addMessage(PaymentConstants.PARAM_SUCCESSFULL_REFUND);
		} else {
			makePaymentRS.addMessage(PaymentConstants.SERVICE_RESPONSE_ERROR_SERVER);
			makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);
			makePaymentRS.setSuccess(false);
		}
	}

	private PaymentGateway extractPaymentGateway(MakePaymentRQ makePaymentRQ) throws ModuleException {
		PaymentGateway paymentGateway = null;
		if (makePaymentRQ.getPaymentOptions() != null) {
			List<PaymentGateway> paymentGateways = makePaymentRQ.getPaymentOptions().getPaymentGateways();
			if (!CollectionUtils.isEmpty(paymentGateways)) {
				paymentGateway = BeanUtils.getFirstElement(paymentGateways);
			}
		}
		return paymentGateway;
	}

	private Integer getPaymentGatewayId(MakePaymentRQ makePaymentRQ) {
		Integer paymentgatewayId = null;
		if (makePaymentRQ.getPaymentOptions() != null) {
			List<PaymentGateway> paymentGateways = makePaymentRQ.getPaymentOptions().getPaymentGateways();
			if (!CollectionUtils.isEmpty(paymentGateways)) {
				PaymentGateway paymentGateway = BeanUtils.getFirstElement(paymentGateways);
				if (paymentGateway != null) {
					paymentgatewayId = paymentGateway.getGatewayId();
				}
			}
		}
		return paymentgatewayId;
	}

	private Card getCreditCardInformation(PaymentGateway paymentGateway) {
		Card cardInfo = null;
		List<Card> cards = paymentGateway.getCards();
		if (!CollectionUtils.isEmpty(cards)) {
			cardInfo = BeanUtils.getFirstElement(cards);
		}
		return cardInfo;
	}

	private void initiateTemporalPayment(IPGRequestDTO ipgRequestDTO, TrackInfoDTO trackInfoDTO, boolean isCredit) throws ModuleException {
		TemporyPaymentTO temporaryPaymentTo = getPopulatedTemporyPaymentTO(ipgRequestDTO, isCredit);
		ReservationBD reservationBD = ModuleServiceLocator.getReservationBD();
		TempPaymentTnx tempPaymentTnx = reservationBD.recordTemporyPaymentEntry(temporaryPaymentTo, trackInfoDTO);
		Integer tptId = tempPaymentTnx.getTnxId();
		ipgRequestDTO.setApplicationTransactionId(tptId);
	}

	/**
	 * 1. Initialize temporary payment transactions (t_temp_payment_tnx)
	 * 2. Perform the payment gateway payment and related credit card transactions (t_ccard_payment_status)
	 * 3. Update temporary payment transaction with response
	 */
	private IPGResponseDTO processGatewayPayment(IPGRequestDTO ipgRequestDTO, TrackInfoDTO trackInfoDTO) throws ModuleException {

		IPGResponseDTO paymentResponse = null;

		initiateTemporalPayment(ipgRequestDTO, trackInfoDTO, true);
		Integer paymentTptId = ipgRequestDTO.getApplicationTransactionId();

		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = ipgRequestDTO.getIpgIdentificationParamsDTO();
		Integer paymentgatewayId = ipgIdentificationParamsDTO.getIpgId();
		String paymentGatewayCurrency = ipgIdentificationParamsDTO.getPaymentCurrencyCode();

		if (isXmlBasedResponse(paymentgatewayId, paymentGatewayCurrency)) {
			IPGRequestResultsDTO iPGRequestResultsDTO = paymentBrokerBD.getRequestDataForAliasPayment(ipgRequestDTO);
			XMLResponseDTO xMLResponseDTO = paymentBrokerBD.getXMLResponse(ipgRequestDTO.getIpgIdentificationParamsDTO(),
					iPGRequestResultsDTO.getPostDataMap());

			if (xMLResponseDTO == null) {
				throw new ModuleException("payment.rejected");
			}

			if (externalNavigationRequired(xMLResponseDTO)) {
				// TODO handle 3D secure cases
				log.error("3D payment resdirection required for tpt_id" + paymentTptId);
			} else {
				int paymentBrokerRefNo = iPGRequestResultsDTO.getPaymentBrokerRefNo();
				IPGIdentificationParamsDTO ipgIdentificationDTO = ipgRequestDTO.getIpgIdentificationParamsDTO();
				log.debug("Attempting Alias payment for the tpt_id : " + paymentTptId);
				paymentResponse = getIPGResponseData(xMLResponseDTO, paymentTptId, paymentBrokerRefNo, ipgIdentificationDTO);
				
			}
		}

		if (paymentResponse != null && paymentTptId != null) {
			log.debug("Alias payment response for the tpt_id :" + paymentTptId + ", is success:" + paymentResponse.isSuccess());
			updateTemporalPaymentWithResponse(paymentTptId, paymentResponse.isSuccess());
			updateTemporalPaymentForProduct(paymentTptId, paymentResponse.isSuccess(), paymentResponse.getAlias());
		}

		return paymentResponse;

	}

	private void updateTemporalPaymentWithResponse(Integer tptId, boolean success) throws ModuleException {
		String status = ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE;
		if (success) {
			status = ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS;
		}
		updateTemporalPayment(tptId, status, null);
	}

	private void updateTemporalPaymentForProduct(Integer tptId, boolean success, String alias) throws ModuleException {
		String status = ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE;
		if (success) {
			status = ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS;
		}
		updateTemporalPayment(tptId, status, alias);
	}

	private void updateTemporalPayment(Integer tptId, String status, String alias) throws ModuleException {
		if (tptId != null) {
			ReservationBD reservationBD = ModuleServiceLocator.getReservationBD();
			List<Integer> temporalPaymentId = new ArrayList<Integer>();
			temporalPaymentId.add(tptId);
			reservationBD.updateTempPaymentEntry(temporalPaymentId, status, null, null, null, alias);
		}
	}

	private IPGRequestDTO composeIPGRequestDTOForAlias(int paymentgatewayId, Card cardInfo, TrackInfoDTO trackInfoDTO,
			String alias) throws ModuleException {
		IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
		IPGPaymentOptionDTO ipgPaymentOptionDTO = paymentUtils.getPaymentGatewayOptions(paymentgatewayId);
		String payCurrencyCode = ipgPaymentOptionDTO.getBaseCurrency();

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = paymentUtils.validateAndPrepareIPGConfigurationParamsDTO(
				paymentgatewayId, payCurrencyCode);

		ipgRequestDTO.setCardName(cardInfo.getCardName());
		ipgRequestDTO.setCardNo(cardInfo.getCardNo());
		ipgRequestDTO.setCardType(String.valueOf(cardInfo.getCardType()));
		String formattedAmount = AccelAeroCalculator.formatAsDecimal(cardInfo.getPaymentAmount());
		ipgRequestDTO.setAmount(formattedAmount);
		DateFormat dateFormat = new SimpleDateFormat(PATTERN1);
		ipgRequestDTO.setExpiryDate(dateFormat.format(cardInfo.getExpiryDate()));
		ipgRequestDTO.setExpDate(cardInfo.getExpiryDate());
		ipgRequestDTO.setHolderName(cardInfo.getCardHoldersName());
		ipgRequestDTO.setSecureCode(cardInfo.getCardCvv());
		ipgRequestDTO.setUserIPAddress(trackInfoDTO.getIpAddress());
		ipgRequestDTO.setNoOfDecimals(2);
		ipgRequestDTO.setCustomerId(trackInfoDTO.getCustomerId());
		ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		ipgRequestDTO.setApplicationIndicator(trackInfoDTO.getAppIndicator());
		ipgRequestDTO.setAlias(alias);

		return ipgRequestDTO;
	}

	private IPGResponseDTO getIPGResponseData(XMLResponseDTO xMLResponseDTO, Integer tempPayId, int paymentBrokerRefNo,
			IPGIdentificationParamsDTO ipgIdentificationDTO) throws ModuleException {
		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();

		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
		Map<String, String> receiptyMap = getReceiptMap(xMLResponseDTO);

		Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);

		ipgResponseDTO.setRequestTimsStamp(requestTime);
		ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
		ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
		ipgResponseDTO.setTemporyPaymentId(tempPayId);

		ipgResponseDTO = paymentBrokerBD.getReponseData(receiptyMap, ipgResponseDTO, ipgIdentificationDTO);
		return ipgResponseDTO;
	}
	
	/**
	 * 1. Initialize temporary payment transactions (t_temp_payment_tnx)
	 * 2. Perform the payment gateway refund and related credit card transactions (t_ccard_payment_status)
	 * 3. Update temporary payment transaction with response
	 */
	private void processGatewayRefund(IPGRequestDTO ipgRequestDTO, IPGResponseDTO ipgResponseDTO, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
		if (ipgResponseDTO != null && ipgResponseDTO.isSuccess()) {
			if (ipgResponseDTO.isSuccess()) {

				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = ipgRequestDTO.getIpgIdentificationParamsDTO();
				String strAmount = ipgRequestDTO.getAmount();
				AppIndicatorEnum appIndicator = trackInfoDTO.getAppIndicator();

				Integer paymentTptId = ipgRequestDTO.getApplicationTransactionId();
				initiateTemporalPayment(ipgRequestDTO, trackInfoDTO, false);
				Integer refundTptId = ipgRequestDTO.getApplicationTransactionId();

				String refundAudit = "Alias Refund request initiated for the payment tpt_id:" + paymentTptId + ",refund tpt_id:"
						+ refundTptId;
				log.debug(refundAudit);

				Integer paymentBrokerRefNo = ipgResponseDTO.getPaymentBrokerRefNo();

				PreviousCreditCardPayment oPrevCCPayment = new PreviousCreditCardPayment();
				oPrevCCPayment.setPaymentBrokerRefNo(paymentBrokerRefNo);
				// oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

				CreditCardPayment oCCPayment = new CreditCardPayment();
				oCCPayment.setPreviousCCPayment(oPrevCCPayment);
				oCCPayment.setAmount(strAmount);
				oCCPayment.setAppIndicator(appIndicator);
				oCCPayment.setPaymentBrokerRefNo(paymentBrokerRefNo);
				oCCPayment.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
				oCCPayment.setPnr(null);
				oCCPayment.setTemporyPaymentId(ipgRequestDTO.getApplicationTransactionId());

				ServiceResponce serviceResponce = paymentBrokerBD.refund(oCCPayment, null, appIndicator, null);

				if (serviceResponce != null) {
					log.debug(refundAudit + ", refund status success=:" + serviceResponce.isSuccess());
					updateTemporalPaymentWithResponse(refundTptId, serviceResponce.isSuccess());
					updateTemporalPaymentForProduct(refundTptId, serviceResponce.isSuccess(), null);
				} else {
					log.debug(refundAudit + ", refund status failed");
				}

			}
		}
	}

	private Map<String, String> getReceiptMap(XMLResponseDTO xmlResponse) {	
		Map<String, String> fields = new LinkedHashMap<String, String>();

		fields.put(PaymentConstants.XML_RESPONSE.ORDERID.getValue(), StringUtil.getNotNullString(xmlResponse.getOrderId()));
		fields.put(PaymentConstants.XML_RESPONSE.PAYID.getValue(), StringUtil.getNotNullString(xmlResponse.getPayId()));
		fields.put(PaymentConstants.XML_RESPONSE.NCSTATUS.getValue(), StringUtil.getNotNullString(xmlResponse.getNcStatus()));
		fields.put(PaymentConstants.XML_RESPONSE.NCERROR.getValue(), StringUtil.getNotNullString(xmlResponse.getNcError()));
		fields.put(PaymentConstants.XML_RESPONSE.NCERRORPLUS.getValue(),
				StringUtil.getNotNullString(xmlResponse.getNcErrorPlus()));
		fields.put(PaymentConstants.XML_RESPONSE.ACCEPTANCE.getValue(), StringUtil.getNotNullString(xmlResponse.getAcceptance()));
		fields.put(PaymentConstants.XML_RESPONSE.STATUS.getValue(), StringUtil.getNotNullString(xmlResponse.getStatus()));
		fields.put(PaymentConstants.XML_RESPONSE.ECI.getValue(), StringUtil.getNotNullString(xmlResponse.getEci()));
		fields.put(PaymentConstants.XML_RESPONSE.AMOUNT.getValue(), StringUtil.getNotNullString(xmlResponse.getAmount()));
		fields.put(PaymentConstants.XML_RESPONSE.CURRENCY.getValue(), StringUtil.getNotNullString(xmlResponse.getCurrency()));
		fields.put(PaymentConstants.XML_RESPONSE.PAYMENT_METHOD.getValue(),
				StringUtil.getNotNullString(xmlResponse.getPaymentMethod()));
		fields.put(PaymentConstants.XML_RESPONSE.BRAND.getValue(), StringUtil.getNotNullString(xmlResponse.getBrand()));
		fields.put(PaymentConstants.XML_RESPONSE.ALIAS.getValue(), StringUtil.getNotNullString(xmlResponse.getAlias()));
		fields.put(PaymentConstants.XML_RESPONSE.SHAREQUIRED.getValue(), "N");

		fields.put(PaymentConstants.IPG_SESSION_ID, "");

		return fields;
	}

	private boolean updateCustomerAlias(IPGRequestDTO ipgRequestDTO, String generatedAlias, IPGResponseDTO ipgResponseDTO)
			throws ModuleException {

		boolean succcess = false;
		if (isAliasVerfied(ipgRequestDTO, generatedAlias, ipgResponseDTO)) {
			AirCustomerServiceBD airCustomerDelegate = ModuleServiceLocator.getCustomerBD();
			CustomerAliasAdaptor customerAliasAdaptor = new CustomerAliasAdaptor();
			CustomerAlias customerAlias = customerAliasAdaptor.adapt(ipgRequestDTO);
			airCustomerDelegate.save(customerAlias);
			succcess = true;
		}

		return succcess;

	}

	private boolean isAliasVerfied(IPGRequestDTO ipgRequestDTO, String generatedAlias, IPGResponseDTO ipgResponseDTO) {
		boolean valid = false;
		if (ipgResponseDTO != null && ipgResponseDTO.isSuccess()) {
			String paymentGatewayAlias = ipgResponseDTO.getAlias();
			valid = !StringUtil.isNullOrEmpty(paymentGatewayAlias) && paymentGatewayAlias.equals(generatedAlias);
			if (!valid) {
				log.error("Customer alias is not updated due to alias mismatch. Generated Alias:" + generatedAlias
						+ ", PG Alias:" + paymentGatewayAlias);
			}

		}
		return valid;
	}

	private boolean isXmlBasedResponse(int paymentgatewayId, String gatewayCurrenyCode) throws ModuleException {
		Properties ipgProps = paymentUtils.getIPGProperties(paymentgatewayId, gatewayCurrenyCode);
		String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);
		return !StringUtil.isNullOrEmpty(responseTypeXML) && responseTypeXML.equals("true");
	}

	private TemporyPaymentTO getPopulatedTemporyPaymentTO(IPGRequestDTO ipgRequestDTO, boolean isCredit) throws ModuleException {
		TemporyPaymentTO temporaryPaymentTo = new TemporyPaymentTO();

		String strAmount = ipgRequestDTO.getAmount();
		BigDecimal baseCurrencyAmount = AccelAeroCalculator.scaleValueDefault(new BigDecimal(strAmount));
		temporaryPaymentTo.setAmount(baseCurrencyAmount);
		temporaryPaymentTo.setCredit(isCredit);

		String paymentCurrencyCode = ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		BigDecimal paymentCurrencyAmount = exchangeRateProxy.convert(paymentCurrencyCode, baseCurrencyAmount);
		temporaryPaymentTo.setPaymentCurrencyCode(paymentCurrencyCode);
		temporaryPaymentTo.setPaymentCurrencyAmount(paymentCurrencyAmount);

		temporaryPaymentTo.setContactFullName(ipgRequestDTO.getHolderName());

		temporaryPaymentTo.setCcNo(ipgRequestDTO.getCardNo());
		temporaryPaymentTo.setProductType(CustomAliasConstants.PRODUCT_TYPE);

		return temporaryPaymentTo;
	}


	private boolean externalNavigationRequired(XMLResponseDTO response) {
		return (!StringUtil.isNullOrEmpty(response.getStatus()) && Integer.valueOf(response.getStatus()) == 46);
	}

	// WHY THIS NEED TO REFACTOR
	private List<VoucherDTO> prepareVoucherDtoList(List<GiftVoucherDTO> giftVoucherList, VoucherDTO voucherDTOSampler) {
		List<VoucherDTO> voucherList = new ArrayList<>();
		VoucherDTOAdapter voucherDTOAdapter = new VoucherDTOAdapter();
		giftVoucherList.forEach(vdto -> {
			for (int i = 0; i < vdto.getQuantity(); i++) {
				voucherList.add(voucherDTOAdapter.adapt(vdto));
			}
		});
		return voucherList;
	}


	public MakePaymentRS processGiftVoucherPayment(TrackInfoDTO trackInfoDto,
			IssueVoucherRQ voucherRequest, ClientCommonInfoDTO clientCommonInfoDTO) throws ModuleException {

		setAppIndicatorEnum(trackInfoDto);

		MakePaymentRS makePaymentRS = new MakePaymentRS();
		DateFormat dateFormat = new SimpleDateFormat(PATTERN1);
		makePaymentRS.setTransactionId(voucherRequest.getTransactionId());
		GiftVoucherSessionStore giftVoucherSessionStore = getGiftVoucherSessionStore(voucherRequest.getTransactionId());
		giftVoucherSessionStore
				.setCardHolderName(voucherRequest.getVoucherDTOList().get(0).getVoucherPaymentDTO().getCardHolderName());
		List<VoucherDTO> preparedVoucherList = new ArrayList<>();

		// WHY THIS NEED TO REFACTOR
		List<VoucherDTO> voucherList = prepareVoucherDtoList(voucherRequest.getVoucherDTOList(), voucherRequest.getVoucherDTOList().get(0));

		PaymentGateway pgw = voucherRequest.getPaymentGateways().get(0);
		/**
		 * NEEDS TO HANDLE THIS SCENARIO LATER
		 */

		BigDecimal totalForVouchers = GiftVoucherUtils.getTotalValueOfVouchersInBase(voucherList);

		// prevent from re-attempts
		if (giftVoucherSessionStore.getPaymentStatus() != null && PaymentStatus.SUCCESS == giftVoucherSessionStore
				.getPaymentStatus()) {
			log.info("processPaymentRequest :: payment already completed");
			throw new ModuleException("payment.api.payment.already.completed");
		}

		// "payable-within-credit" case
		if (totalForVouchers.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
			makePaymentRS.setPaymentStatus(PaymentStatus.NO_PAYMENT);
			makePaymentRS.setSuccess(true);
			return makePaymentRS;
		}

		List<String> messages = new ArrayList<>();
		PostPaymentDTO postPaymentDTO = new PostPaymentDTO();
		giftVoucherSessionStore.storePostPaymentInformation(postPaymentDTO);

		String strRequestData = null;
		boolean paymentProcessed = false;

		try {

			ExternalChgDTO ccChgDTO = null;
			ExternalChgDTO ccFeeTaxChgDTO = null;
			// calculate CC Charges

			//			if (isAdminFeeRegulatedAndAppliedForChannel()) {
			//			}

			int paymentgatewayId = pgw.getGatewayId();

			IPGPaymentOptionDTO pgOptionsDto = paymentUtils.getPaymentGatewayOptions(paymentgatewayId);

			if (pgOptionsDto == null) {
				log.error("invalid payment gateway selected");
				throw new ModuleException("payment.api.invalid.payment.gateway.selected");
			}

			String baseCurrencyCode = pgOptionsDto.getBaseCurrency();
			Properties ipgProps = paymentUtils.getIPGProperties(paymentgatewayId, baseCurrencyCode);
			PaymentGateway paymentGateway = adaptPaymentGateway(pgOptionsDto, ipgProps);

			String providerName = paymentGateway.getProviderName();
			String brokerType = paymentGateway.getBrokerType();
			Boolean isSwitchToExternalURL = paymentGateway.isSwitchToExternalUrl();
			Boolean viewPaymentInIFrame = paymentGateway.isViewPaymentInIframe();
			boolean isOfflinePGW = PaymentConsts.OFFLINE_PG_OPTIONS.contains(providerName);
			boolean isBrokerTypeInternalExternal = PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL
					.equals(brokerType);
			String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);
			String requestMethod = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_REQUEST_METHOD);

			makePaymentRS.setSuccess(true);
			makePaymentRS.setSwitchToExternal(isSwitchToExternalURL);

			// start to build externalPGDetails & set to makePaymentResponse
			ExternalPGDetails externalPGDetails = new ExternalPGDetails();
			makePaymentRS.setExternalPGDetails(externalPGDetails);
			externalPGDetails.setViewPaymentInIframe(viewPaymentInIFrame);
			externalPGDetails.setBrokerType(brokerType);
			externalPGDetails.setRequestMethod(requestMethod);


			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			CurrencyExchangeRate currencyExrate = exchangeRateProxy.getCurrencyExchangeRate(paymentGateway.getPaymentCurrency(),
					CommonServiceUtil.getApplicationEngine(appIndicator));

			populateCreditCardTnxFeeForVouchers(voucherList, paymentgatewayId, totalForVouchers);
			BigDecimal totalPaymentAmount = GiftVoucherUtils.getTotalPaymentAmountForVouchersInBase(voucherList);

			LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();

			paymentGateway.setEffectivePaymentAmount(getAmountObj(totalPaymentAmount, currencyExrate, baseCurrencyCode));

			PayCurrencyDTO payCurrencyDTO = getPayCurrencyDTO(currencyExrate, baseCurrencyCode, trackInfoDto);
			postPaymentDTO.setPayCurrencyDTO(payCurrencyDTO);

			try {
				preparedVoucherList = ModuleServiceLocator.getVoucherBD().prepareVouchersForIBEPayment(
						voucherList, baseCurrencyCode,
						String.valueOf(paymentGateway.getEffectivePaymentAmount().getPaymentCurrencyValue()), totalPaymentAmount,
						trackInfoDto);

			} catch (ModuleException exc) {
				log.error("PaymentServiceAdapter ==> getVoucher() generating voucher failed", exc);
				makePaymentRS.setSuccess(false);
				makePaymentRS.setMessages(Collections.singletonList(
						errorMessageSource.getMessage("gift.voucher.notissued", null, LocaleContextHolder.getLocale())));
				return makePaymentRS;
			}

			for (VoucherDTO voucherDTO : preparedVoucherList) {
				VoucherPaymentDTO voucherPaymentDTO = voucherDTO.getVoucherPaymentDTO();
				voucherPaymentDTO.setAppIndicator(AppIndicatorEnum.APP_IBE);
				voucherPaymentDTO.setTnxModeCode(TnxModeEnum.MAIL_TP_ORDER.getCode());
				voucherPaymentDTO.setUserIp(trackInfoDto.getIpAddress());
			}




			// build card details
			//if (brokerType != null && selectedCards != null && !selectedCards.isEmpty()) {

			//for (Card card : selectedCards) {

			//							boolean isGroupPnr = paymentBaseSessionStore.isGroupPNR();
			//
			//							// TODO required for payment retry?
			//							postPaymentDTO.setGroupPnr(voucherRequest.g);

			// if card not valid
//	//		if (!isSelectedPaymentOptionValid(NumberUtils.toInt(voucherDTOForPayment.getCardType()), paymentgatewayId, true,
//	//				"GROUP_ID", false, trackInfoDto, paymentBaseSessionStore)) {
//	//			log.info("selected payment card not applicable for search criteria");
//	//			throw new ModuleException("payment.api.invalid.payment.gateway.selected");
//  //			}

			String selCardType = "";
			String txtCardNo = "";
			String txtName = "";
			String txtSCode = "";
			String strExpiry = "";

			Card selectedCard = pgw.getCards().get(0);

			selCardType = selectedCard.getCardType() + "";

			// add card details if it's internal external
			if (brokerType != null && isBrokerTypeInternalExternal) {
				txtCardNo = selectedCard.getCardNo();
				txtName = selectedCard.getCardHoldersName();
				txtSCode = selectedCard.getCardCvv();
				strExpiry = dateFormat.format(selectedCard.getExpiryDate());

			}

			// verify requested payable against the calculated one
			//							if (totalPaymentAmount.compareTo(card.getPaymentAmount()) != 0) {
			//								log.info("found payment amount miss-match :actual payment amt:" + totalPaymentAmount
			//										+ "submitted amt:" + card.getPaymentAmount());
			//								// throw new ModuleException("payment.api.invalid.payment.amount.error");
			//							}

			//							if (AccelAeroCalculator.subtract(card.getPaymentAmount(), totalPaymentAmount).abs()
			//									.compareTo(BigDecimal.valueOf(0.01)) > 0) {
			//								totalPaymentAmount = card.getPaymentAmount();
			//							}

			// add commonCreditCardPaymentInfo (type of LccClientPaymentInfo) to
			// LccClientPaymentAssembler
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(paymentgatewayId,
					baseCurrencyCode);

			// //lccClientPaymentAssembler.addAllExternalCharges(paymentBaseSessionStore.getExternalCharges());

			if (isBrokerTypeInternalExternal) {
				lccClientPaymentAssembler
						.addInternalCardPayment(Integer.parseInt(selCardType), strExpiry, txtCardNo, txtName, null, txtSCode,
								totalPaymentAmount, appIndicator, TnxModeEnum.SECURE_3D, ipgIdentificationParamsDTO,
								payCurrencyDTO, new Date(), txtName, "", null, null, null);
			} else {
				lccClientPaymentAssembler
						.addExternalCardPayment(Integer.parseInt(selCardType), "", totalPaymentAmount, appIndicator,
								TnxModeEnum.SECURE_3D, null, ipgIdentificationParamsDTO, payCurrencyDTO, new Date(), null, null,
								isOfflinePGW, null);
			}

			SYSTEM selectedSystem = SYSTEM.AA;

			Map<Integer, CommonCreditCardPaymentInfo> tempPayMap = new HashMap<>();
			int temporyPayId = preparedVoucherList.get(0).getVoucherPaymentDTO().getTempPaymentID();
			tempPayMap.put(temporyPayId, (CommonCreditCardPaymentInfo) lccClientPaymentAssembler.getPayments().iterator().next());


			String ipgPNR = preparedVoucherList.get(0).getVoucherGroupId();

			// build PG specific IPGRequestDTO
			//AdditionalDetails additionalDetails = paymentOptions.getAdditionalDetails();
			IPGRequestDTO ipgRequestDTO = composeIPGRequestDTO(selectedCard, ipgIdentificationParamsDTO,
					ipgPNR, temporyPayId, AccelAeroRounderPolicy.convertAndRound(lccClientPaymentAssembler.getTotalPayAmount(), payCurrencyDTO)
							.toString(), trackInfoDto, isBrokerTypeInternalExternal,
					null,
					false,
					payCurrencyDTO.getDecimalPlaces(), preparedVoucherList, clientCommonInfoDTO);

			ipgRequestDTO.setIntExtPaymentGateway(isBrokerTypeInternalExternal);
			ipgRequestDTO.setPaymentGateWayName(paymentGateway.getProviderName());
			ipgRequestDTO.setSelectedSystem(selectedSystem.toString());
			postPaymentDTO.setPaymentGateWay(paymentGateway);
			ipgRequestDTO.setServiceAppFlow(true);

			String customerId = getCustomerSessionStore().getLoggedInCustomerID();
			if (customerId != null) {
//				if (card.isSaveCreditCard()) {
//				}
				//ipgRequestDTO.setSaveCreditCard(card.isSaveCreditCard());
				//ipgRequestDTO.setExpDate(voucherDTOForPayment.getCardExpiry());
				ipgRequestDTO.setCardName(String.valueOf(selectedCard.getCardType()));
				ipgRequestDTO.setCustomerId(customerId);
				//ipgRequestDTO.setAlias(card.getAlias());
			}
			ipgRequestDTO.setCardNo(selectedCard.getCardNo());
			ipgRequestDTO.setSecureCode(selectedCard.getCardCvv());
			ipgRequestDTO.setHolderName(selectedCard.getCardHoldersName());
			ipgRequestDTO.setExpiryDate(selectedCard.getExpiryDate() != null ?
					dateFormat.format(selectedCard.getExpiryDate()) : null);
			PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
			IPGRequestResultsDTO ipgRequestResultsDTO;

			// get PG specific ipgRequestResultsDTO
			// handle all internal-external payment gateways (PayPal etc)
			if (isBrokerTypeInternalExternal) {

				List<CardDetailConfigDTO> cardDetailConfigData = paymentBrokerBD
						.getPaymentGatewayCardConfigData(ipgIdentificationParamsDTO.getIpgId().toString());

				if (PaymentConsts.PAYPAL_PG.equalsIgnoreCase(paymentGateway.getProviderCode())) {
					// TODO: internal logic & PG needs to be checked
					// paypalResponse = new PAYPALResponse();
					// setPPRequestDTO(ipgRequestDTO, payCurrencyDTO, ipgProps);// set pp data
					// userInputDTO.setCreditCardType(getStandardCardType(Integer.parseInt(card.getCardType())));
					// paypalResponse.setCreditCardType(card.getCardType());
					// ipgRequestDTO.setIntExtPaymentGateway(isIntExtPaymentGateway);
					// if (isIntExtPaymentGateway) {
					// setPPCreditCardInfo(ipgRequestDTO);
					// paypalResponse.setCVV2(card.getCardCVV());

					ipgRequestDTO.setUserInputDTO(new UserInputDTO());

					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO, cardDetailConfigData);
				}

				// other internal external PGs
				else {
					ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO, cardDetailConfigData);
				}
			} else if (PaymentConsts.QIWI.equalsIgnoreCase(providerName)) {
				// TODO check expiration time
				ipgRequestDTO.setInvoiceExpirationTime(new Date());

				//ipgRequestDTO.setContactMobileNumber(additionalDetails.getParam2());

				postPaymentDTO.setPnr(ipgPNR);

				ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);

				QiwiPResponse invoiceCreationResponse = ipgRequestResultsDTO.getQiwiResponse();

				isSwitchToExternalURL = !invoiceCreationResponse.isOfflineMode();

				makePaymentRS.setSwitchToExternal(isSwitchToExternalURL);
				postPaymentDTO.setInvoiceStatus(invoiceCreationResponse.getStatus());
				postPaymentDTO.setBillId(invoiceCreationResponse.getBillId());

				if (invoiceCreationResponse.getStatus().equals(QiwiPRequest.FAIL)) {

					log.debug("[PaymentServiceAdaper::Qiwi Invoice creation Failed]");

					postPaymentDTO.setTemporyPaymentMap(LCCClientApiUtils
							.updateLccTemporyPaymentReferences(temporyPayId, ipgRequestResultsDTO.getPaymentBrokerRefNo(),
									tempPayMap));

					postPaymentDTO.setErrorCode(invoiceCreationResponse.getError());

					// TODO: recheck
					makePaymentRS.setErrors(Arrays.asList(QiwiPRequest.QIWI_ERROR_CODE, ipgRequestResultsDTO.getErrorCode()));

					makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);

					//makePaymentResponse.setActionStatus(ActionStatus.BOOKING_CREATION_FAILED);

				}
				paymentProcessed = true;

			}
			// rest of the external PGs
			else {
//				if (PaymentConsts.PAY_FORT_ONLINE.equalsIgnoreCase(providerName)) {
//					ipgRequestDTO.setContactEmail(additionalDetails.getParam1());
//				}
				ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);
			}

			strRequestData = ipgRequestResultsDTO.getRequestData();
			postPaymentDTO.setIpgRequestDTO(ipgRequestDTO);
			postPaymentDTO.setPaymentGateWay(paymentGateway);
			postPaymentDTO.setPnr(ipgPNR);
			postPaymentDTO.setIpgRefenceNo(ipgRequestResultsDTO.getAccelAeroTransactionRef());
			postPaymentDTO.setTemporyPaymentMap(LCCClientApiUtils
					.updateLccTemporyPaymentReferences(temporyPayId, ipgRequestResultsDTO.getPaymentBrokerRefNo(), tempPayMap));
			giftVoucherSessionStore.storeGiftVouchers(preparedVoucherList);

			// AmeriaBank handling
			if (ipgRequestResultsDTO.getAmeriaBankSessionInfo() != null) {
				if (ipgRequestResultsDTO.getAmeriaBankSessionInfo().getError() != null) {
					log.error(ipgRequestResultsDTO.getAmeriaBankSessionInfo().getError());
					ModuleException me = new ModuleException("payment.api.ameriabank.payment.failed");
					me.setErrorType(ErrorType.PAYMENT_ERROR);
					throw me;
				}

				postPaymentDTO.setAmeriaBankSessionInfo(ipgRequestResultsDTO.getAmeriaBankSessionInfo());
			}
			// if PG return XML response
			if (responseTypeXML != null && !responseTypeXML.equals("") && responseTypeXML.equals("true")) {

				log.debug("Execute the payment and get the response for XML response type PGWs");

				XMLResponseDTO response = paymentBrokerBD
						.getXMLResponse(ipgIdentificationParamsDTO, ipgRequestResultsDTO.getPostDataMap());

				log.debug("Recieved XMLresponse");
				List<Integer> tptIds = new ArrayList<Integer>();
				tptIds.add(temporyPayId);

				// if 3D Secure (status 46), confirmation to be handled at ConfirmPaymentController
				if (response != null && Integer.valueOf(response.getStatus()) == 46) {

					log.debug("3D Secure payment: PNR:" + ipgPNR + "SessionID :");

					makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
					makePaymentRS.setSwitchToExternal(true);

					externalPGDetails.setPostInputData(response.getHtml());
					externalPGDetails.setRedirectionURL(null);
					postPaymentDTO.setSecurePayment3D(true);

				}
				// if MOTO and XML response exists build confirmation response
				else if (response != null && !response.getStatus().equals("")) {
					IPGHandlerRQ ipgHandlerRequest = new IPGHandlerRQ();
					ipgHandlerRequest.setIpgResponse(response.toString());
					ipgHandlerRequest.setPostPaymentDTO(postPaymentDTO);
					ipgHandlerRequest.setAlias(ipgRequestDTO.getAlias());
					ipgHandlerRequest.setCardToBeSaved(ipgRequestDTO.isSaveCreditCard());
					postPaymentDTO.setXmlResponse(response);

					PaymentConfirmationRS ipgHandlerResponse = confirmPaymentService
							.processPaymentReceiptForGiftVoucher(voucherRequest.getTransactionId(), ipgHandlerRequest,
									trackInfoDto);

					tempPayMap.get(temporyPayId).setPaymentSuccess(true);
					adaptMakePaymentResponse(makePaymentRS, ipgHandlerResponse);
					makePaymentRS.setExternalPGDetails(null);

				} else {

					log.info("PAYMENT ERROR");
					makePaymentRS.setPaymentStatus(PaymentStatus.ERROR);

				}
				paymentProcessed = true;
			} else {
				log.info("RESPONSE TYPE IS NOT XML");
			}

			if (!paymentProcessed) {
				if (PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL.equals(brokerType)) {
					log.info("BROKER_TYPE_EXTERNAL ");
					makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
					makePaymentRS.setSwitchToExternal(true);

					addPostParamData(requestMethod, externalPGDetails, strRequestData);

				} else if (PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_3DEXTERNAL
						.equals(brokerType)) {

					log.info("BROKER_TYPE_INTERNAL_3DEXTERNAL ");
					makePaymentRS.setPaymentStatus(PaymentStatus.PENDING);
					makePaymentRS.setSwitchToExternal(true);

					if (viewPaymentInIFrame && !isSwitchToExternalURL) {
						externalPGDetails.setDisplayBanners(true);
					}

					addPostParamData(requestMethod, externalPGDetails, strRequestData);

				}
				log.debug(
						"Interline paymentgateway External Rediraction Info:: RequestData :" + strRequestData + " Transaction ID:"
								+ voucherRequest.getTransactionId() + " Session ID:" + trackInfoDto.getSessionId());
			}

		} catch (ModuleException me) {
			log.error("ModuleException @ processPaymentRequest " + me.getExceptionCode());
			ModuleException modEx = new ModuleException(me, me.getExceptionCode());
			if (ErrorType.PAYMENT_ERROR == me.getErrorType()) {
				modEx.setErrorType(ErrorType.PAYMENT_ERROR);
			}
			throw modEx;
		} catch (Exception e) {
			log.error("Exception @ processPaymentRequest " + e.getMessage());
			throw new ModuleException(e, "error while processing payment");
		}

		boolean isPaymentSuccess = handleSuccessPaymentResponseForVouchers(makePaymentRS);

		if (isPaymentSuccess) {
			ModuleServiceLocator.getVoucherBD().issueGiftVouchersFromIBE(preparedVoucherList);
			messages.add(errorMessageSource
					.getMessage("gift.voucher.issued.payment.success", null, LocaleContextHolder.getLocale()));
			makePaymentRS.setVoucherGroupId(preparedVoucherList.get(0).getVoucherGroupId());
		}


		return makePaymentRS;
	}

	private void populateCreditCardTnxFeeForVouchers(List<VoucherDTO> vouchers, int paymentGatewayId,
			BigDecimal totalValueForVouchersInBase) throws ModuleException {
		ExternalChgDTO externalChgDTO = getExternalChargeDTOForVoucherPayment(paymentGatewayId);
		GiftVoucherUtils.updateCCFeeForGiftVouchers(vouchers, totalValueForVouchersInBase, externalChgDTO);
	}


	private ExternalChgDTO getExternalChargeDTOForVoucherPayment(int paymentGatewayId) throws ModuleException {

		ExternalChgDTO externalChgDTO = GiftVoucherUtils.getCCFeeExternalChargeDTOFORVoucherPurchase();

		PaymentGatewayWiseCharges pgwCharge = ModuleServiceLocator.
				getChargeBD().getPaymentGatewayWiseCharge(paymentGatewayId);

		if (pgwCharge != null && pgwCharge.getChargeId() > 0) {
			externalChgDTO.setRatioValueInPercentage(PaymentConsts.CHARGE_BY_VALUE.equals(pgwCharge.getValuePercentageFlag()));
			externalChgDTO.setRatioValue(BigDecimal.valueOf(pgwCharge.getChargeValuePercentage()));
			// valid charge is defined for the selected PGW
		}
		return externalChgDTO;
	}

	private boolean handleSuccessPaymentResponseForVouchers(MakePaymentRS makePaymentResponse) {
		boolean paymentSuccess = false;
		if (BookingRS.PaymentStatus.SUCCESS.equals(makePaymentResponse.getPaymentStatus())) {
			makePaymentResponse.setPaymentStatus(BookingRS.PaymentStatus.SUCCESS);
			paymentSuccess = true;
		}
		// IF CASH OPTION AND OFFLINE PAYMENT OPTIONS ADDED THIS NEEDS TO BE EXTENDED
		else if (makePaymentResponse.isSwitchToExternal()) {
			makePaymentResponse.setPaymentStatus(BookingRS.PaymentStatus.PENDING);
			makePaymentResponse.setActionStatus(BookingRS.ActionStatus.REDIRET_EXTERNAL);
		} else if (BookingRS.PaymentStatus.ERROR.equals(makePaymentResponse.getPaymentStatus())) {
			makePaymentResponse.setPaymentStatus(BookingRS.PaymentStatus.ERROR);
		}
		return paymentSuccess;
	}

	private IPGRequestDTO composeIPGRequestDTO(Card card, IPGIdentificationParamsDTO ipgIdentificationParamsDTO,
			String groupId,	int temporyPayId, String paymentAmount,
			TrackInfoDTO trackInfoDto, boolean isIntExt,
			String originCountryCodeForTest, boolean isMobile,
			Integer payCurrencyISODecimalPlaces, List<VoucherDTO> giftVouchers,
			ClientCommonInfoDTO clientCommonInfoDTO) throws ModuleException {

		GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
		DateFormat dateFormat = new SimpleDateFormat(PATTERN1);
		IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
		ipgRequestDTO.setDefaultCarrierName(globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME));

		ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);

		//ipgRequestDTO.setPaymentMethod(card.getPaymentMethod());

		ipgRequestDTO.setAmount(paymentAmount);

		ipgRequestDTO.setCardType(String.valueOf(card.getCardType()));
		ipgRequestDTO.setPayCurrencyISODecimalPlaces(payCurrencyISODecimalPlaces);

		if (isIntExt) {
			ipgRequestDTO.setCardNo(card.getCardNo());
			ipgRequestDTO.setSecureCode(card.getCardCvv());
			ipgRequestDTO.setHolderName(card.getCardHoldersName());
			ipgRequestDTO.setExpiryDate(dateFormat.format(card.getExpiryDate()));
		}

		ipgRequestDTO.setApplicationTransactionId(temporyPayId);

		String RETURN_URL = null;
		if (isMobile) {
			RETURN_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() +
					PaymentAPIConsts.REDIRECT_VOUCHER_PURCHASE_CONTROLLER_MOBILE_URL_SUFFIX;
		} else {
			RETURN_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() +
					PaymentAPIConsts.REDIRECT_VOUCHER_PURCHASE_CONTROLLER_URL_SUFFIX;

		}
		//final String RECEIPT_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + PaymentConsts.RECEIPT_URL_SUFFIX;
		//final String STATUS_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + PaymentConsts.STATUS_URL_SUFFIX;
		//final String OFFER_URL = globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, trackInfoDto.getCarrierCode());

		ipgRequestDTO.setReturnUrl(RETURN_URL);
//		ipgRequestDTO.setStatusUrl(STATUS_URL);
//		ipgRequestDTO.setReceiptUrl(RECEIPT_URL);
//		ipgRequestDTO.setOfferUrl(OFFER_URL);

		ipgRequestDTO.setApplicationIndicator(trackInfoDto.getAppIndicator());
		ipgRequestDTO.setPnr(groupId);

		// As in current IBE (hard-coded to 2)
		ipgRequestDTO.setNoOfDecimals(2);
		ipgRequestDTO.setRequestedCarrierCode(trackInfoDto.getCarrierCode());
		ipgRequestDTO.setSessionID(trackInfoDto.getSessionId());
		ipgRequestDTO.setContactFirstName(giftVouchers.get(0).getPaxFirstName());
		ipgRequestDTO.setContactLastName(StringUtils.trimToEmpty(giftVouchers.get(0).getPaxLastName()));
		ipgRequestDTO.setContactMobileNumber(StringUtils.trimToEmpty(giftVouchers.get(0).getMobileNumber()));
		ipgRequestDTO.setContactPhoneNumber(StringUtils.trimToEmpty(giftVouchers.get(0).getMobileNumber()));
		ipgRequestDTO.setEmail(StringUtils.trimToEmpty(giftVouchers.get(0).getEmail()));
		ipgRequestDTO.setCardName(card.getCardName());

		ipgRequestDTO.setSessionLanguageCode(getPreferredLanguageFromLocale());

		ipgRequestDTO.setUserIPAddress(trackInfoDto.getIpAddress());
		ipgRequestDTO.setUserAgent(clientCommonInfoDTO.getBrowser());

		ipgRequestDTO.setIpCountryCode(getCountryCode(trackInfoDto.getIpAddress(), originCountryCodeForTest));

		return ipgRequestDTO;
	}

	public void setCreditCardFeesToVoucherTemplates(List<VoucherTemplateTo> voucherList) throws ModuleException {
		Collection<ReservationInternalConstants.EXTERNAL_CHARGES> colExternalCharges = new ArrayList<>();
		try {
			colExternalCharges.add(ReservationInternalConstants.EXTERNAL_CHARGES.CREDIT_CARD);

			Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> externalCharges = ModuleServiceLocator
					.getReservationBD().getQuotedExternalCharges(colExternalCharges, null,
							CommonsConstants.ChargeRateOperationType.ANY);
			ExternalChgDTO externalChgDTO = externalCharges.get(ReservationInternalConstants.EXTERNAL_CHARGES.CREDIT_CARD);

			for (VoucherTemplateTo voucherTemplateTo : voucherList) {
				externalChgDTO.calculateAmount(new BigDecimal(voucherTemplateTo.getAmount()));
				voucherTemplateTo.setCcTnxFee(AccelAeroCalculator.scaleValueDefault(externalChgDTO.getAmount()).toString());
			}
		} catch (Exception e) {
			log.error("Exception @ setting CC fee " + e.getMessage());
		}
	}

	public GiftVoucherSessionStore getGiftVoucherStore (String transactionId){
		return getGiftVoucherSessionStore(transactionId);
	}
}