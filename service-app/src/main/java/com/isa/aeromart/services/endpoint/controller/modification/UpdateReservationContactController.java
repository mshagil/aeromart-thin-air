package com.isa.aeromart.services.endpoint.controller.modification;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationContactRQAdaptor;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.modification.UpdateReservationContactDetailsRQ;
import com.isa.aeromart.services.endpoint.dto.session.CustomerSession;
import com.isa.aeromart.services.endpoint.dto.session.store.CustomerSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

@Controller
@RequestMapping("modification")
public class UpdateReservationContactController extends StatefulController {

	

	@ResponseBody
	@RequestMapping(value = "/updateContact", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			TransactionalBaseRS updateReservationContactDetails(
					@Validated @RequestBody UpdateReservationContactDetailsRQ updateReservationContactDetailsReq)
					throws ModuleException {

		TransactionalBaseRS response = new TransactionalBaseRS();
		
		String intCustomerID = getCustomerSession().getLoggedInCustomerID();
		
		ReservationContactRQAdaptor reservationContactRQTransformer = new ReservationContactRQAdaptor(intCustomerID);
		CommonReservationContactInfo resContactInfo = reservationContactRQTransformer.adapt(updateReservationContactDetailsReq
				.getContactInfo());

		CommonReservationContactInfo oldContactInfo = getContactInfo(ModuleServiceLocator.getReservationQueryBD()
				.getReservationContactInfo(updateReservationContactDetailsReq.getPnr()));

		ModuleServiceLocator.getAirproxyReservationBD().modifyContactInfo(updateReservationContactDetailsReq.getPnr(), updateReservationContactDetailsReq.getVersion(), resContactInfo, oldContactInfo,
				updateReservationContactDetailsReq.isGroupPnr(), getTrackInfo().getAppIndicator().toString(), getTrackInfo());
		response.setSuccess(true);

		return response;

	}

	private CommonReservationContactInfo getContactInfo(ReservationContactInfo contactInfo) {

		CommonReservationContactInfo ccontactInfo = new CommonReservationContactInfo();

		ccontactInfo.setTitle(contactInfo.getTitle());
		ccontactInfo.setFirstName(contactInfo.getFirstName());
		ccontactInfo.setLastName(contactInfo.getLastName());
		ccontactInfo.setTitle(contactInfo.getTitle());
		ccontactInfo.setCity(contactInfo.getCity());
		ccontactInfo.setCountryCode(contactInfo.getCountryCode());
		ccontactInfo.setCustomerId(contactInfo.getCustomerId());
		ccontactInfo.setEmail(contactInfo.getEmail());
		ccontactInfo.setState(contactInfo.getState());
		ccontactInfo.setFax(contactInfo.getFax());
		ccontactInfo.setMobileNo(contactInfo.getMobileNo());
		ccontactInfo.setPhoneNo(contactInfo.getPhoneNo());
		if (contactInfo.getNationalityCode() != null) {
			ccontactInfo.setNationalityCode(Integer.parseInt(contactInfo.getNationalityCode()));
		}
		ccontactInfo.setStreetAddress1(contactInfo.getStreetAddress1());
		ccontactInfo.setStreetAddress2(contactInfo.getStreetAddress2());
		ccontactInfo.setPreferredLanguage(contactInfo.getPreferredLanguage());
		ccontactInfo.setZipCode(contactInfo.getZipCode());
		ccontactInfo.setState(contactInfo.getState());
		ccontactInfo.setTaxRegNo(contactInfo.getTaxRegNo());

		ccontactInfo.setEmgnTitle(contactInfo.getEmgnTitle());
		ccontactInfo.setEmgnFirstName(contactInfo.getEmgnFirstName());
		ccontactInfo.setEmgnLastName(contactInfo.getEmgnLastName());
		ccontactInfo.setEmgnPhoneNo(contactInfo.getEmgnPhoneNo());
		ccontactInfo.setEmgnEmail(contactInfo.getEmgnEmail());
		ccontactInfo.setSendPromoEmail(contactInfo.getSendPromoEmail());

		return ccontactInfo;
	}
	
	private CustomerSessionStore getCustomerSession() {
		return getSessionStore(CustomerSession.SESSION_KEY);
	}

}
