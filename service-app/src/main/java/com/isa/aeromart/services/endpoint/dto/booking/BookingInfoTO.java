package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;

public class BookingInfoTO {

	private String pnr;

	private String status;

	private boolean groupPnr;

	private String agent;

	private Date bookingDate;

	private String prefferedLanguage;

	private Date releaseDate;

	private int expiryDays;

	private TravellerQuantity travellerQuantity;

	private String version;
	
	private boolean balanceAvailable = false;

	private String policyCode;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getPrefferedLanguage() {
		return prefferedLanguage;
	}

	public void setPrefferedLanguage(String prefferedLanguage) {
		this.prefferedLanguage = prefferedLanguage;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getExpiryDays() {
		return expiryDays;
	}

	public void setExpiryDays(int expiryDays) {
		this.expiryDays = expiryDays;
	}

	public TravellerQuantity getTravelerQuantity() {
		return travellerQuantity;
	}

	public void setTravelerQuantity(TravellerQuantity travelerQuantity) {
		this.travellerQuantity = travelerQuantity;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public boolean isBalanceAvailable() {
		return balanceAvailable;
	}

	public void setBalanceAvailable(boolean balanceAvailable) {
		this.balanceAvailable = balanceAvailable;
	}
}
