package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.Date;
import java.util.Set;

public class LccReservationInfo {

	private int totalPaxAdultCount;
	private int totalChildPaxCount;
	private int totalInfantPaxCount;
	private Date lastFareQuoteDate;
	private Date ticketValiedityLastDate;
	
	private Set<LccResSegmentInfo> lccSegmentInfos;
	
	
	public int getTotalPaxAdultCount() {
		return totalPaxAdultCount;
	}
	public void setTotalPaxAdultCount(int totalPaxAuditCount) {
		this.totalPaxAdultCount = totalPaxAuditCount;
	}
	
	public int getTotalChildPaxCount() {
		return totalChildPaxCount;
	}
	public void setTotalChildPaxCount(int totalChildPaxCount) {
		this.totalChildPaxCount = totalChildPaxCount;
	}
	
	public int getTotalInfantPaxCount() {
		return totalInfantPaxCount;
	}
	public void setTotalInfantPaxCount(int totalInfantPaxCount) {
		this.totalInfantPaxCount = totalInfantPaxCount;
	}
	
	public Date getLastFareQuoteDate() {
		return lastFareQuoteDate;
	}
	public void setLastFareQuoteDate(Date lastFareQuoteDate) {
		this.lastFareQuoteDate = lastFareQuoteDate;
	}
	
	public Date getTicketValiedityLastDate() {
		return ticketValiedityLastDate;
	}
	public void setTicketValiedityLastDate(Date ticketValiedityLastDate) {
		this.ticketValiedityLastDate = ticketValiedityLastDate;
	}
	public Set<LccResSegmentInfo> getLccSegmentInfos() {
		return lccSegmentInfos;
	}
	public void setLccSegmentInfos(Set<LccResSegmentInfo> lccSegmentInfos) {
		this.lccSegmentInfos = lccSegmentInfos;
	}

	
	
}
