package com.isa.aeromart.services.endpoint.adaptor.impls.availability.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.FareClassAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.FareClassFromOndInfoAdaptor;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.aeromart.services.endpoint.dto.availability.FareClass;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.core.util.CommonsServices;

public class CommonRSAdaptor {

	private static Log log = LogFactory.getLog(CommonRSAdaptor.class);

	public static List<FareClass> getFareClasses(BaseAvailRS source) {
		Map<String, FareClass> fareClassMap = new HashMap<String, FareClass>();
		List<OriginDestinationInformationTO> originDestinationInformationTOs = new ArrayList<OriginDestinationInformationTO>();
		FareClassFromOndInfoAdaptor fareClassFromAvailTrans = new FareClassFromOndInfoAdaptor();
		if (source.getSelectedPriceFlightInfo() != null
				&& source.getSelectedPriceFlightInfo().getAvailableLogicalCCList() != null) {
			List<OndClassOfServiceSummeryTO> ondClassOfServiceSummeryTOs = source.getSelectedPriceFlightInfo()
					.getAvailableLogicalCCList();
			FareClassAdaptor fareClassTrans = new FareClassAdaptor();
			for (OndClassOfServiceSummeryTO cabinClassSummary : ondClassOfServiceSummeryTOs) {
				for (LogicalCabinClassInfoTO logicalCabinClassInfo : cabinClassSummary.getAvailableLogicalCCList()) {
					FareClass fareClass = fareClassTrans.adapt(logicalCabinClassInfo);
					fareClassMap.put(fareClass.getFareClassCode(), fareClass);
				}
			}
		}
		if (source.getOriginDestinationInformationList() != null) {
			originDestinationInformationTOs.addAll(source.getOriginDestinationInformationList());
			for (OriginDestinationInformationTO originDestinationInformationTO : originDestinationInformationTOs) {
				if (originDestinationInformationTO.getOrignDestinationOptions() != null) {
					for (OriginDestinationOptionTO originDestinationOptionTO : originDestinationInformationTO
							.getOrignDestinationOptions()) {
						if (originDestinationOptionTO.getFlightFareSummaryList() != null) {
							for (FlightFareSummaryTO flightFareSummaryTO : originDestinationOptionTO.getFlightFareSummaryList()) {
								FareClass fareClass = fareClassFromAvailTrans.adapt(flightFareSummaryTO);
								fareClassMap.put(fareClass.getFareClassCode(), fareClass);
							}
						}
					}
				}
			}
		}
		List<FareClass> fareClasses = new ArrayList<FareClass>(fareClassMap.values());
		updateWithTranslations(fareClasses);
		return fareClasses;
	}

	public static void updateWithTranslations(List<FareClass> fareClasses) {
		if (!"en".equals(LocaleContextHolder.getLocale().toString())) {
			Map<String, com.isa.thinair.commons.api.dto.LogicalCabinClassDTO> cachedLccMap = CommonsServices.getGlobalConfig()
					.getAvailableLogicalCCMap(LocaleContextHolder.getLocale().toString());
			if (cachedLccMap != null && !fareClasses.isEmpty()) {
				Iterator<FareClass> it = fareClasses.iterator();
				while (it.hasNext()) {
					FareClass fareClass = it.next();
					for (Entry<String, LogicalCabinClassDTO> entry : cachedLccMap.entrySet()) {
						LogicalCabinClassDTO cachedLcc = entry.getValue();
						if (cachedLcc.getLogicalCCCode().equals(fareClass.getLogicalCabinClass())) {
							if (FareClassType.FLEXI.equals(fareClass.getFareClassType())) {
								fareClass.setDescription(cachedLcc.getFlexiDescription());
							} else if (FareClassType.DEFAULT.equals(fareClass.getFareClassType())) {
								fareClass.setDescription(cachedLcc.getDescription());
							}
						}
					}
				}
			}

		}
	}

}
