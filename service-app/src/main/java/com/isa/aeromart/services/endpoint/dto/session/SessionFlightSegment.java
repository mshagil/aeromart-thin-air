package com.isa.aeromart.services.endpoint.dto.session;

import java.util.Date;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class SessionFlightSegment {
	private FlightSegmentTO flightSegment;

	public SessionFlightSegment(FlightSegmentTO flightSegment) {
		this.flightSegment = flightSegment;
	}

	public String getSegmentCode() {
		return flightSegment.getSegmentCode();
	}

	public Date getDepartureDateTime() {
		return flightSegment.getDepartureDateTime();
	}

	public Date getArrivalDateTime() {
		return flightSegment.getArrivalDateTime();
	}

	public String getFlightDesignator() {
		return flightSegment.getFlightNumber();
	}

	public String getOperatingAirline() {
		return flightSegment.getOperatingAirline();
	}

	public String getLCCFlightReference() {
		return flightSegment.getFlightRefNumber();
	}

	public String getCabinClass() {
		return flightSegment.getCabinClassCode();
	}

	public String getLogicalCabinClass() {
		return flightSegment.getLogicalCabinClassCode();
	}

	public Date getDepartureDateTimeZulu() {
		return flightSegment.getDepartureDateTimeZulu();
	}

	public Date getArrivalDateTimeZulu() {
		return flightSegment.getArrivalDateTimeZulu();
	}

	public Integer getFlightSegmentId() {
		return flightSegment.getFlightSegId();
	}

	public Integer getFlightId() {
		return flightSegment.getFlightId();
	}

	public String getSubStationShortName() {
		return flightSegment.getSubStationShortName();
	}

	public String getRouteRefNumber() {
		return flightSegment.getRouteRefNumber();
	}

	public boolean isSurfaceSegment() {
		return flightSegment.isSurfaceSegment();
	}

	public int getSegmentSequence() {
		return flightSegment.getSegmentSequence();
	}

	public int getOndSequence() {
		return flightSegment.getOndSequence();
	}

	public String getBookingType() {
		return flightSegment.getBookingType();
	}

	public boolean isDomesticFlight() {
		return flightSegment.isDomesticFlight();
	}

	public String getFlightSegmentStatus() {
		return flightSegment.getFlightSegmentStatus();
	}

	public boolean isWaitListed() {
		return flightSegment.isWaitListed();
	}

	public String getBookingClass() {
		return flightSegment.getBookingClass();
	}

	public Integer getFlexiId() {
		return flightSegment.getFlexiID();
	}

	public String getBaggageONDGroupId() {
		return flightSegment.getBaggageONDGroupId();
	}
	
	public void setBaggageONDGroupId(String baggageOndId) {
		flightSegment.setBaggageONDGroupId(baggageOndId);
	}
	
	public String getPnrSegId() {
		return flightSegment.getPnrSegId();
	}
	
	public String getFlightRefNumber() {
		return flightSegment.getFlightRefNumber();
	}
	
	public String getFareBasisCode() {
		return flightSegment.getFareBasisCode();
	}
	
	public String getSectorONDCode() {
		return flightSegment.getSectorONDCode();
	}

	public String getCsOcCarrierCode() {
		return flightSegment.getCsOcCarrierCode();
	}
	
	public boolean getReturnFlag() {
		return flightSegment.isReturnFlag();
	}

	public FlightSegmentTO getFlightSegment() {
		return flightSegment;
	}
}
