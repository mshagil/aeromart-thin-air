package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerContact;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerDetails;
import com.isa.aeromart.services.endpoint.dto.customer.CustomerQuestionaireMap;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerQuestionaire;
import com.isa.thinair.commons.api.exception.ModuleException;


public class CustomerReqAdaptor implements Adaptor<CustomerDetails , Customer>{

	@Override
	public Customer adapt(CustomerDetails customerDetails) {
		
		Customer customer = null;
		
		if(customerDetails.getCustomerID() != null && !customerDetails.getCustomerID().equals("") ){
			try {
				customer = ModuleServiceLocator.getCustomerBD().getCustomer(Integer.valueOf(customerDetails.getCustomerID()));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ModuleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			customer = new Customer();
			customer.setStatus(customerDetails.getStatus());
		}
		
		customer.setTitle(customerDetails.getTitle());
		customer.setFirstName(customerDetails.getFirstName());
		customer.setLastName(customerDetails.getLastName());
		if(customerDetails.getCustomerID() != null){
			customer.setCustomerId(Integer.valueOf(customerDetails.getCustomerID()));
		}
		customer.setPassword(customerDetails.getPassword());
		customer.setGender(customerDetails.getGender());
		customer.setDateOfBirth(customerDetails.getDateOfBirth());
		customer.setCountryCode(customerDetails.getCountryCode());
		customer.setNationalityCode(customerDetails.getNationalityCode());
		
		customer.setSecretQuestion(customerDetails.getSecretQuestion());
		customer.setSecretAnswer(customerDetails.getSecretAnswer());
		
		if(customerDetails.getRegistrationDate() != null){
			customer.setRegistration_date(DateToCalendar(customerDetails.getRegistrationDate()));
		}
		
		if(customerDetails.getConfirmationDate() != null){
			customer.setConfirmation_date(DateToCalendar(customerDetails.getConfirmationDate()));
		}
		
		if(customerDetails.getCustomerQuestionaire() != null){
			CustomerQuestionaireReqAdaptor customerQuestionaireReqTransformer = new CustomerQuestionaireReqAdaptor();
			ArrayList<CustomerQuestionaire> customerQuestionaireList = new ArrayList<CustomerQuestionaire>();
			
			for(CustomerQuestionaireMap customerQuestionaire : customerDetails.getCustomerQuestionaire()){
				customerQuestionaireList.add(customerQuestionaireReqTransformer.adapt(customerQuestionaire));
			}
			
			customer.setCustomerQuestionaire(new HashSet<CustomerQuestionaire>(customerQuestionaireList));
		}
				
		CustomerContact customerContact = customerDetails.getCustomerContact();
		PhoneNumberReqAdaptor phoneNumberReqTransformer = new PhoneNumberReqAdaptor();
		
		
		customer.setMobile(phoneNumberReqTransformer.adapt(customerContact.getMobileNumber()));
		customer.setTelephone(phoneNumberReqTransformer.adapt(customerContact.getTelephoneNumber()));
		customer.setFax(phoneNumberReqTransformer.adapt(customerContact.getFax()));
		

		if(customerContact.getAddress() != null){
			customer.setAddressLine(customerContact.getAddress().getStreetAddress1());
			customer.setAddressStreet(customerContact.getAddress().getStreetAddress2());
			customer.setZipCode(customerContact.getAddress().getZipCode());
		}else{
			customer.setAddressLine("");
			customer.setAddressStreet("");
		}
		
		customer.setEmailId(customerContact.getEmailAddress());
		customer.setAlternativeEmailId(customerContact.getAlternativeEmailId());
		
		if(customerContact.getEmergencyContact() != null){
			customer.setEmgnTitle(customerContact.getEmergencyContact().getEmgnTitle());
			customer.setEmgnFirstName(customerContact.getEmergencyContact().getEmgnFirstName());
			customer.setEmgnLastName(customerContact.getEmergencyContact().getEmgnLastName());
			customer.setEmgnEmail(customerContact.getEmergencyContact().getEmgnEmail());
			customer.setEmgnPhoneNo(phoneNumberReqTransformer.adapt(customerContact.getEmergencyContact().getEmgnPhoneNumber()));
		}
		
		customer.setSendPromoEmail(customerDetails.getSendPromoEmail());
		
		//TODO add social customer details
		return customer;
		
	}
	
	public static Calendar DateToCalendar(Date date){ 
		  Calendar cal = Calendar.getInstance();
		  cal.setTime(date);
		  return cal;
	}

}



