package com.isa.aeromart.services.endpoint.adaptor.impls.common;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.PhoneNumber;

public class PhoneNumberResTransformer implements Adaptor<String, PhoneNumber> {

	@Override
	public PhoneNumber adapt(String number) {
		PhoneNumber phoneNumber = new PhoneNumber();
		if (number != null) {
			String[] phoneNumberArray = number.split("-");
			if (phoneNumberArray.length == 3) {
				//phoneNumber = new PhoneNumber();
				phoneNumber.setCountryCode(phoneNumberArray[0]);
				phoneNumber.setAreaCode(phoneNumberArray[1]);
				phoneNumber.setNumber(phoneNumberArray[2]);
			}
		}
		return phoneNumber;
	}

}
