package com.isa.aeromart.services.endpoint.dto.payment;

public class PaymentPreferences {

	private String currencyCode;

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}
