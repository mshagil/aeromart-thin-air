package com.isa.aeromart.services.endpoint.controller.modification;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.FlightSegmentAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.ReservationInfoAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.ModificationAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.RequoteBalanceAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.PassengerChargeAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.modification.BalanceSummaryRQ;
import com.isa.aeromart.services.endpoint.dto.modification.BalanceSummaryRS;
import com.isa.aeromart.services.endpoint.dto.modification.RequoteBalanceInfo;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.FareInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.AvailabilityRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.RequoteSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

@Controller
@RequestMapping("balanceSummary")
public class BalanceSummaryController extends StatefulController {

	@Autowired
	private BookingService bookingService;

	@ResponseBody
	@RequestMapping(value = "/segmentModification", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BalanceSummaryRS getBalanceSummary(@RequestBody TransactionalBaseRQ baseRQ) throws ModuleException, ParseException {
		return createCommonBalanceSummaryResponse(baseRQ.getTransactionId());
	}

	@ResponseBody
	@RequestMapping(value = "/anciModification", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BalanceSummaryRS getBalanceSummaryAfterAnciModification(@RequestBody TransactionalBaseRQ baseRQ) throws ModuleException, ParseException {
		getRequoteSessionStore(baseRQ.getTransactionId()).getRequoteBalanceReqiredParams().setAnciModification(true);
		List<ReservationPaxTO> resPaxList = getPaxDetailsWithAnciModification(baseRQ.getTransactionId());
		ReservationInfo sessionResInfo = getRequoteSessionStore(baseRQ.getTransactionId()).getResInfo();
		resPaxList = ModificationUtils.includePassangerExternalCharges(sessionResInfo.getPaxList(), resPaxList);		
		sessionResInfo.setPaxList(resPaxList);
		getRequoteSessionStore(baseRQ.getTransactionId()).storeResInfo(sessionResInfo);
		return createCommonBalanceSummaryResponse(baseRQ.getTransactionId());
	}

	@ResponseBody
	@RequestMapping(value = "/cancelReservation", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public
			BalanceSummaryRS getBalanceSummaryForCancelReservation(@RequestBody BalanceSummaryRQ balanceSummaryReq)
					throws ModuleException {

		LCCClientResAlterQueryModesTO lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO
				.composeCancelReservationQueryRequest(balanceSummaryReq.getPnr(), balanceSummaryReq.getVersion(),balanceSummaryReq.isGroupPnr());
		ReservationBalanceTO lCCClientReservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD()
				.getResAlterationBalanceSummary(lCCClientResAlterQueryModesTO, getTrackInfo());

		if (lCCClientReservationBalanceTO.getVersion() != null) {
			lCCClientResAlterQueryModesTO.setVersion(lCCClientReservationBalanceTO.getVersion());
		}

		Map<String, BigDecimal> pnrPaxCreditMap = getPaxCreditMap(lCCClientReservationBalanceTO.getPassengerSummaryList());
		BalanceSummaryRS response = ModificationAdaptor.getCancelBalanceSummary(lCCClientReservationBalanceTO, pnrPaxCreditMap,  getResInfo(balanceSummaryReq));
		return response;
	}

	private BalanceSummaryRS createCommonBalanceSummaryResponse(String transactionId) throws ModuleException {
		BalanceSummaryRS response = new BalanceSummaryRS();
		RequoteBalanceQueryRQ balanceQueryTO = ModificationAdaptor.getBalanceQueryRQ(getRequoteSessionStore(transactionId));

		// to store remaining loyalty points of the customer in the session
		Double remainingLMSPoints = balanceQueryTO.getRemainingLoyaltyPoints();
		TrackInfoDTO trackInfo = getTrackInfo();
		ReservationBalanceTO lCCClientReservationBalanceTO = getReservationBalanceTO(balanceQueryTO, trackInfo);
		//update taxes with effective charges
		ModificationUtils.updateEffectiveTax(getRequoteSessionStore(transactionId).getFinancialStore().getAncillaryExternalCharges().getPassengers(), lCCClientReservationBalanceTO.getPaxEffectiveTax());
		response = ModificationAdaptor.createBalanceSummaryReseponse(getRequoteSessionStore(transactionId),
				lCCClientReservationBalanceTO);
		response.setTransactionId(transactionId);
		initiateTransaction(transactionId, response, lCCClientReservationBalanceTO, remainingLMSPoints);
		return response;

	}

	private void initiateTransaction(String transactionId, BalanceSummaryRS balanceSummaryResponse,
			ReservationBalanceTO balanceTo, Double remainingLMSPoints) {
		if (balanceTo != null) {
			RequoteSessionStore sessionStore = getRequoteSessionStore(transactionId);
			
			//sessionStore.storeBalanceInfo(balanceSummaryResponse.getModifiedBalanceInfo());
			sessionStore.storeBalanceInfo(getBalanceInfo(sessionStore,balanceTo));
			
			FareInfo sessionFareInfo = sessionStore.getFareInfo();
			sessionFareInfo.setCreditableAmount(AccelAeroCalculator.formatAsDecimal(balanceTo.getSegmentSummary()
					.getCurrentRefunds()));
			sessionFareInfo.setModifyCharge(AccelAeroCalculator.formatAsDecimal(balanceTo.getSegmentSummary().getNewModAmount()));
			sessionStore.storeFareInfo(sessionFareInfo);
			sessionStore.storeRemainingAvailableLoyaltyPoints(remainingLMSPoints);
			if (!sessionStore.getRequoteBalanceReqiredParams().isAnciModification()) {
				List<ReservationPaxTO> resPaxList = getPaxListWithFlexi(sessionStore);
				List<PassengerChargeTo<LCCClientExternalChgDTO>> paxChargeTOs = new ArrayList<PassengerChargeTo<LCCClientExternalChgDTO>>();
				AdaptorUtils.adaptCollection(resPaxList, paxChargeTOs, new PassengerChargeAdaptor());
				sessionStore.getFinancialStore().getAncillaryExternalCharges().setPassengers(paxChargeTOs);
			}
		}
	}

	private RequoteBalanceInfo getBalanceInfo(RequoteSessionStore session, ReservationBalanceTO reservationBalanceTo){
		RequoteBalanceAdaptor requoteBalanceAdaptor = new RequoteBalanceAdaptor(session.getResInfo().getPaxCreditMap(
				session.getResInfo().getPnr()));
		return requoteBalanceAdaptor.adapt(reservationBalanceTo);
	}
	
	private RequoteSessionStore getRequoteSessionStore(String transactionId) {
		return getTrasactionStore(transactionId);
	}

	private ReservationBalanceTO getReservationBalanceTO(RequoteBalanceQueryRQ balanceQueryTO, TrackInfoDTO trackInfo)
			throws ModuleException {
		return ModuleServiceLocator.getAirproxyReservationBD().getRequoteBalanceSummary(balanceQueryTO, trackInfo);
	}

	private List<ReservationPaxTO> getPaxDetailsWithAnciModification(String transactionId) throws ModuleException {

		FinancialStore financialStore = getRequoteSessionStore(transactionId).getFinancialStore();
		List<ReservationPaxTO> resPaxList = new ArrayList<ReservationPaxTO>();
		resPaxList = ModificationUtils.getPaxDetailsWithAnciModification(financialStore, getRequoteSessionStore(transactionId)
				.getRequoteBalanceInfo().getPassengerSummaryList());
		return resPaxList;
	}
	
	private Map<String, BigDecimal> getPaxCreditMap (Collection<LCCClientPassengerSummaryTO> paxSummaryList) {
		 Map<String, BigDecimal> paxCreditMap = new HashMap<String, BigDecimal>();
		 for (LCCClientPassengerSummaryTO paxSummary : paxSummaryList) {
			 paxCreditMap.put(paxSummary.getTravelerRefNumber(), paxSummary.getTotalCreditAmount());
		 }		
		 return paxCreditMap;		
	}
	
	private List<ReservationPaxTO> getPaxListWithFlexi(RequoteSessionStore sessionStore) {
		List<ReservationPaxTO> paxList = sessionStore.getResInfo().getPaxList();
		Map<Integer, Boolean> ondWiseFlexiMap = sessionStore.getFareInfo().getOndSelectedFlexi();
		List<PerPaxPriceInfoTO> paxWiseCharges = sessionStore.getFareInfo().getPerPaxPriceInfo();
		List<SessionFlightSegment> sessionFlightSegmentTOs = sessionStore.getFlightSegments();
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		AdaptorUtils.adaptCollection(sessionFlightSegmentTOs, flightSegmentTOs, new FlightSegmentAdaptor());
		if (paxList != null) {
			for (ReservationPaxTO reservationPaxTO : paxList) {
				for (PerPaxPriceInfoTO paxPriceInfoTO : paxWiseCharges) {
					if ((paxPriceInfoTO.getPassengerType().equals(reservationPaxTO.getPaxType()) || (paxPriceInfoTO
							.getPassengerType().equals(PaxTypeTO.ADULT) && reservationPaxTO.getIsParent()))
							&& !reservationPaxTO.getPaxType().equals(PaxTypeTO.INFANT)) {
						for (Entry<Integer, Boolean> entry : ondWiseFlexiMap.entrySet()) {
							int ondSequence = entry.getKey();
							boolean ondFlexi = entry.getValue();
							if (ondFlexi) {
								reservationPaxTO.addExternalCharges(ReservationUtil.transform(paxPriceInfoTO.getPassengerPrice()
										.getOndExternalCharge(ondSequence).getExternalCharges(), flightSegmentTOs));
							}
						}
						break;
					}
				}
			}
		}
		return paxList;
	}
	
	private String initiateTransaction(String transactionId) {
		
		if (transactionId == null) {
			transactionId = initTransaction(AvailabilityRequoteSessionStore.SESSION_KEY, transactionId);
		}
		return transactionId;
	}
	
	private ReservationInfo getResInfo(BalanceSummaryRQ balanceSummaryReq) throws ModuleException{
		LCCClientReservation reservation = loadProxyReservation(balanceSummaryReq.getPnr(), balanceSummaryReq.isGroupPnr());
		ReservationInfoAdaptor reservationInfoAdaptor = new ReservationInfoAdaptor(balanceSummaryReq.getTransactionId());	
		ReservationInfo sessionResInfo = reservationInfoAdaptor.adapt(reservation);
		
		return sessionResInfo;
	}

	private LCCClientReservation loadProxyReservation(String pnr, boolean isGroupPNR) throws ModuleException {
		return bookingService.loadProxyReservation(pnr, isGroupPNR, getTrackInfo(), false, false, null, getTrackInfo()
				.getCarrierCode(), false);
	}
}
