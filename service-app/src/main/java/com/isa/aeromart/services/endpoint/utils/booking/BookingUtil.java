package com.isa.aeromart.services.endpoint.utils.booking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.PaymentAssemblerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.modification.ReservationPassengerAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.PaxAdditionalInfoAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.BookingType;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaxPaymentUtil;
import com.isa.thinair.webplatform.api.v2.util.PaymentAssemblerComposer;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;

public class BookingUtil {

	private static Log log = LogFactory.getLog(BookingUtil.class);

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, boolean recordAudit, String airlineCode,
			String marketingAirlineCode, boolean skipPromoAdjustment) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		boolean loadLocalTimes;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		boolean loadFares = true; // Set True
		boolean loadLastUserNote = false;
		boolean loadOndChargesView = true;
		boolean loadPaxAvaBalance = true;
		boolean loadSegView = true;

		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegView(loadSegView);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(ApplicationEngine.IBE);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setSkipPromoAdjustment(skipPromoAdjustment);
		pnrModesDTO.setLoadAirportTransferInfo(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);

		if (StringUtils.isNotEmpty(marketingAirlineCode) && marketingAirlineCode.trim().length() == 2) {
			pnrModesDTO.setMarketingAirlineCode(marketingAirlineCode);
		}

		if (StringUtils.isNotEmpty(airlineCode) && airlineCode.trim().length() == 2) {
			pnrModesDTO.setAirlineCode(airlineCode);
		}

		return pnrModesDTO;
	}

	public static void populatePaxInformation(BookingRQ bookingRQ, CommonReservationAssembler reservationAssembler,
			AncillaryIntegrateTO ancillaryIntegrateTo, BookingSessionStore bookingSessionStore) {

		PaxAdditionalInfoAdaptor paxAdditionalInfoTransformer = new PaxAdditionalInfoAdaptor();
		if (reservationAssembler.getLccreservation() != null
				&& reservationAssembler.getLccreservation().getPassengers() != null) {
			reservationAssembler.getLccreservation().getPassengers().clear();
		}
		List<AncillaryIntegratePassengerTO> anciIntegratePassengerTos = ancillaryIntegrateTo.getReservation().getPassengers();

		boolean isOnhold;
		List<List<LCCClientExternalChgDTO>> paxwisePaymentExternalCharges = new ArrayList<>();
		Map<Integer, List<LCCClientExternalChgDTO>> passengerWisePaymentExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		if (bookingRQ.getMakePaymentRQ() == null || bookingRQ.isCreateOnhold()) {
			isOnhold = true;
		} else {
			isOnhold = false;
		}

		if (!isOnhold || AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			Collection<LCCClientExternalChgDTO> paymentExtCharges = bookingSessionStore.getPaymentExternalCharges();
			paxwisePaymentExternalCharges = getPaxwisePaymentExternalCharges(paymentExtCharges,
					getPayablePaxCount(bookingRQ.getPaxInfo()));
			if (paymentExtCharges == null) {
				paymentExtCharges = new ArrayList<>();
			}
		}

		if (bookingSessionStore.getPaxWisePaymentExternalCharges() != null
				&& !bookingSessionStore.getPaxWisePaymentExternalCharges().isEmpty()) {
			for (PassengerChargeTo<LCCClientExternalChgDTO> paymentPassneger : bookingSessionStore
					.getPaxWisePaymentExternalCharges()) {
				passengerWisePaymentExternalCharges.put(Integer.parseInt(paymentPassneger.getPassengerRph()),
						paymentPassneger.getGetPassengerCharges());
			}
		}

		Map<Integer, List<LCCClientFlexiExternalChgDTO>> paxWiseFlexiExternalCharges = FlexiUtil
				.getPaxWiseSegmentWiseFlexiExternalCharges(bookingSessionStore.getSegments(),
						bookingSessionStore.getTravellerQuantity(), bookingSessionStore.getSessionFlexiDetails(),
						bookingRQ.getPaxInfo());
		Iterator<List<LCCClientExternalChgDTO>> paxpaxwisePaymentExternalChargesIter = paxwisePaymentExternalCharges.iterator();

		PayByVoucherInfo sessionPayByVoucherInfo = null;
		if (bookingSessionStore.getPayByVoucherInfo() != null) {
			sessionPayByVoucherInfo = bookingSessionStore.getPayByVoucherInfo().clone();
		}
		for (Passenger passenger : bookingRQ.getPaxInfo()) {
			PaxAdditionalInfoDTO paxAdditionalInfoDTO = null;

			if (passenger.getAdditionalInfo() != null) {
				paxAdditionalInfoDTO = paxAdditionalInfoTransformer.adapt(passenger.getAdditionalInfo());
			}

			AncillaryIntegratePassengerTO ancillaryIntegratePassenger = getAncillaryIntergratePax(anciIntegratePassengerTos,
					passenger);

			boolean isParent = isParent(bookingRQ.getPaxInfo(), passenger.getPaxSequence());

			List<LCCClientExternalChgDTO> paymentExternalCharges;
			if (paxpaxwisePaymentExternalChargesIter == null || paxwisePaymentExternalCharges.isEmpty()
					|| !paxpaxwisePaymentExternalChargesIter.hasNext()) {
				paymentExternalCharges = new ArrayList<>();
			} else {
				paymentExternalCharges = paxpaxwisePaymentExternalChargesIter.next();
			}

			if (!passengerWisePaymentExternalCharges.isEmpty()) {
				if (passengerWisePaymentExternalCharges.containsKey(passenger.getPaxSequence())) {
					paymentExternalCharges.addAll(passengerWisePaymentExternalCharges.get(passenger.getPaxSequence()));
				}
			}

			LCCClientPaymentAssembler lccClientPaymentAssembler = getPaymentAssembler(passenger, bookingRQ,
					ancillaryIntegratePassenger, paymentExternalCharges, isOnhold, bookingSessionStore,
					paxWiseFlexiExternalCharges.get(passenger.getPaxSequence()), sessionPayByVoucherInfo);

			if (passenger.getPaxType().equals(PaxTypeTO.ADULT) && isParent) {
				lccClientPaymentAssembler.setPaxType(PaxTypeTO.ADULT);
				reservationAssembler.addParent(passenger.getFirstName(), passenger.getLastName(), passenger.getTitle(),
						passenger.getDateOfBirth(), passenger.getNationality(), passenger.getPaxSequence(),
						getInfantSeq(bookingRQ.getPaxInfo(), passenger.getPaxSequence()), null, paxAdditionalInfoDTO, "A",
						lccClientPaymentAssembler, null, null, null, null, null, null, null, null, null);
			} else if (passenger.getPaxType().equals(PaxTypeTO.ADULT)) {
				lccClientPaymentAssembler.setPaxType(PaxTypeTO.ADULT);
				reservationAssembler.addSingle(passenger.getFirstName(), passenger.getLastName(), passenger.getTitle(),
						passenger.getDateOfBirth(), passenger.getNationality(), passenger.getPaxSequence(), null,
						paxAdditionalInfoDTO, "A", lccClientPaymentAssembler, null, null, null, null, null, null, null, null,
						null, null);
			} else if (passenger.getPaxType().equals(PaxTypeTO.CHILD)) {
				lccClientPaymentAssembler.setPaxType(PaxTypeTO.CHILD);
				reservationAssembler.addChild(passenger.getFirstName(), passenger.getLastName(), passenger.getTitle(),
						passenger.getDateOfBirth(), passenger.getNationality(), passenger.getPaxSequence(), null,
						paxAdditionalInfoDTO, "A", lccClientPaymentAssembler, null, null, null, null, null, null, null, null,
						null, null);
			} else if (passenger.getPaxType().equals(PaxTypeTO.INFANT)) {
				reservationAssembler.addInfant(passenger.getFirstName(), passenger.getLastName(), passenger.getTitle(),
						passenger.getDateOfBirth(), passenger.getNationality(), passenger.getPaxSequence(),
						passenger.getTravelWith(), null, paxAdditionalInfoDTO, "A", null, null, null, null, null, null, null,
						null, null, lccClientPaymentAssembler);
			}
		}
	}

	private static AncillaryIntegratePassengerTO
			getAncillaryIntergratePax(List<AncillaryIntegratePassengerTO> anciIntegratePassengerTos, Passenger passenger) {
		AncillaryIntegratePassengerTO ancillaryIntegratePassenger = null;

		for (AncillaryIntegratePassengerTO ancillaryIntegratePassengerTo : anciIntegratePassengerTos) {
			if (String.valueOf(passenger.getPaxSequence()).equals(ancillaryIntegratePassengerTo.getPassengerRph())) {
				ancillaryIntegratePassenger = ancillaryIntegratePassengerTo;
				break;
			}
		}
		return ancillaryIntegratePassenger;
	}

	private static LCCClientPaymentAssembler getPaymentAssembler(Passenger passenger, BookingRQ bookingRQ,
			AncillaryIntegratePassengerTO ancillaryIntegratePassenger, List<LCCClientExternalChgDTO> paymentExternalCharges,
			boolean isOnhold, BookingSessionStore bookingSessionStore, List<LCCClientFlexiExternalChgDTO> paxFlexiExternalCharges,
			PayByVoucherInfo sessionPayByVoucherInfo) {

		BigDecimal lmsMemberPayment = getLmsMemberPayment(bookingSessionStore, passenger);

		List<LCCClientExternalChgDTO> allExternalCharges = mergeExternalCharges(paymentExternalCharges,
				ancillaryIntegratePassenger, paxFlexiExternalCharges, bookingRQ);

		// BigDecimal infantAmount = getAttachedInfantAmount(bookingRQ, passenger, bookingSessionStore);

		BigDecimal totalExternalCharge = getTotalExtCharge(allExternalCharges);

		BigDecimal totalWithAnci = AccelAeroCalculator.add(
				getPaxTypeWiseTotalCharge(bookingSessionStore).get(passenger.getPaxType()),
				totalExternalCharge/* , infantAmount */);

		if (!BookingType.ONHOLD.equals(bookingRQ.getMakePaymentRQ().getBookingType())) {
			BigDecimal paxPromoDiscountTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
			ReservationDiscountDTO resDiscountDTO = bookingSessionStore
					.getReservationDiscountDTOForBooking(bookingRQ.getMakePaymentRQ().isBinPromotion());
			DiscountedFareDetails discountedFareDetails = bookingSessionStore
					.getDiscountedFareDetailsForBooking(bookingRQ.getMakePaymentRQ().isBinPromotion());
			if (resDiscountDTO != null
					&& !PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discountedFareDetails.getDiscountAs())) {
				PaxDiscountDetailTO paxDiscountDetailTO = resDiscountDTO.getPaxDiscountDetail(passenger.getPaxSequence());
				if (paxDiscountDetailTO != null) {
					paxPromoDiscountTotal = paxDiscountDetailTO.getTotalDiscount();
				}
			}

			totalWithAnci = AccelAeroCalculator.subtract(totalWithAnci, paxPromoDiscountTotal);
		}

		PaxPaymentUtil paxPayUtil = null;
		BigDecimal redeemedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (bookingSessionStore.getPayByVoucherInfo() != null) {
			redeemedAmount = bookingSessionStore.getPayByVoucherInfo().getRedeemedTotal();
		}

		paxPayUtil = new PaxPaymentUtil(totalWithAnci, totalExternalCharge, AccelAeroCalculator.getDefaultBigDecimalZero(),
				lmsMemberPayment, redeemedAmount, AccelAeroCalculator.getDefaultBigDecimalZero());

		PaymentAssemblerAdaptor paymentAssemblerAdaptor = new PaymentAssemblerAdaptor(bookingSessionStore, paxPayUtil, passenger,
				allExternalCharges, isOnhold, sessionPayByVoucherInfo);

		return paymentAssemblerAdaptor.adapt(bookingRQ.getMakePaymentRQ());
	}

	private static BigDecimal getLmsMemberPayment(BookingSessionStore bookingSessionStore, Passenger passenger) {

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments;
		BigDecimal lmsMemberPayment = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (bookingSessionStore.getLoyaltyInformation() != null) {
			carrierWiseLoyaltyPayments = bookingSessionStore.getLoyaltyInformation().getCarrierWiseLoyaltyPaymentInfo();
			if (carrierWiseLoyaltyPayments != null) {
				Map<Integer, BigDecimal> paxWiseRedeemedAmounts = AirProxyReservationUtil
						.getPaxRedeemedTotalMap(carrierWiseLoyaltyPayments);

				lmsMemberPayment = paxWiseRedeemedAmounts.get(passenger.getPaxSequence());
				if (lmsMemberPayment != null) {
					lmsMemberPayment = AccelAeroCalculator.scaleValueDefault(lmsMemberPayment);
				}
			}
		}
		return lmsMemberPayment;
	}

	private static List<LCCClientExternalChgDTO> mergeExternalCharges(List<LCCClientExternalChgDTO> paymentExternalCharges,
			AncillaryIntegratePassengerTO ancillaryIntegratePassenger, List<LCCClientFlexiExternalChgDTO> paxFlexiExternalCharges,
			BookingRQ bookingRQ) {

		List<LCCClientExternalChgDTO> allExternalCharges = new ArrayList<>();
		if (paymentExternalCharges != null && !paymentExternalCharges.isEmpty()) {
			for (LCCClientExternalChgDTO paymentExternalCharge : paymentExternalCharges) {
				if (paymentExternalCharge.getExternalCharges().equals(EXTERNAL_CHARGES.CREDIT_CARD)
						|| paymentExternalCharge.getExternalCharges().equals(EXTERNAL_CHARGES.JN_OTHER)) {
					if (!bookingRQ.isCreateOnhold() || AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
						allExternalCharges.add(paymentExternalCharge);
					}
				} else {
					allExternalCharges.add(paymentExternalCharge);
				}
			}
		}

		if (ancillaryIntegratePassenger != null && ancillaryIntegratePassenger.getExternalCharges() != null) {
			allExternalCharges.addAll(ancillaryIntegratePassenger.getExternalCharges());
		}

		if (paxFlexiExternalCharges != null && !paxFlexiExternalCharges.isEmpty()) {
			allExternalCharges.addAll(paxFlexiExternalCharges);
		}

		return allExternalCharges;
	}

	private static BigDecimal getAttachedInfantAmount(BookingRQ bookingRQ, Passenger passenger,
			BookingSessionStore bookingSessionStore) {
		BigDecimal infantAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (isParent(bookingRQ.getPaxInfo(), passenger.getPaxSequence())) {
			infantAmount = getPaxTypeWiseTotalCharge(bookingSessionStore).get(PaxTypeTO.INFANT);
		}
		return infantAmount;
	}

	private static Map<String, BigDecimal> getPaxTypeWiseTotalCharge(BookingSessionStore bookingSessionStore) {
		FareSegChargeTO fareSegChargeTO = bookingSessionStore.getFarePricingInformation();
		Map<String, Double> paxWiseTotalChargesMap = fareSegChargeTO.getPaxWiseTotalCharges();
		Map<String, Double> totalJourneyFareMap = fareSegChargeTO.getTotalJourneyFare();
		Map<String, BigDecimal> paxTypeWiseTotalCharges = new HashMap<>();
		paxTypeWiseTotalCharges.put(PaxTypeTO.ADULT,
				AccelAeroCalculator.add(AccelAeroCalculator.parseBigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.ADULT)),
						AccelAeroCalculator.parseBigDecimal(totalJourneyFareMap.get(PaxTypeTO.ADULT))));

		if (paxWiseTotalChargesMap.get(PaxTypeTO.CHILD) != null) {
			paxTypeWiseTotalCharges.put(PaxTypeTO.CHILD,
					AccelAeroCalculator.add(AccelAeroCalculator.parseBigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.CHILD)),
							AccelAeroCalculator.parseBigDecimal(totalJourneyFareMap.get(PaxTypeTO.CHILD))));
		}

		if (paxWiseTotalChargesMap.get(PaxTypeTO.INFANT) != null) {
			paxTypeWiseTotalCharges.put(PaxTypeTO.INFANT,
					AccelAeroCalculator.add(AccelAeroCalculator.parseBigDecimal(paxWiseTotalChargesMap.get(PaxTypeTO.INFANT)),
							AccelAeroCalculator.parseBigDecimal(totalJourneyFareMap.get(PaxTypeTO.INFANT))));
		}

		return paxTypeWiseTotalCharges;
	}

	private static List<List<LCCClientExternalChgDTO>>
			getPaxwisePaymentExternalCharges(Collection<LCCClientExternalChgDTO> paymentExtCharges, int adultChildCount) {

		List<List<LCCClientExternalChgDTO>> typeWisePaxWiseExtChgs = new ArrayList<>();

		for (LCCClientExternalChgDTO externalCharge : paymentExtCharges) {
			typeWisePaxWiseExtChgs.add(composeExternalCharges(externalCharge, adultChildCount));
		}

		return getPaxWiseTypeWiseExtChgs(typeWisePaxWiseExtChgs);
	}

	private static List<List<LCCClientExternalChgDTO>>
			getPaxWiseTypeWiseExtChgs(List<List<LCCClientExternalChgDTO>> typeWisePaxWiseExtChgs) {
		List<List<LCCClientExternalChgDTO>> ret = new ArrayList<>();
		if (typeWisePaxWiseExtChgs != null && !typeWisePaxWiseExtChgs.isEmpty() && typeWisePaxWiseExtChgs.get(0) != null) {
			final int size = typeWisePaxWiseExtChgs.get(0).size();
			for (int i = 0; i < size; i++) {
				List<LCCClientExternalChgDTO> col = new ArrayList<>();
				for (List<LCCClientExternalChgDTO> row : typeWisePaxWiseExtChgs) {
					col.add(row.get(i));
				}
				ret.add(col);
			}
		}
		return ret;
	}

	private static List<LCCClientExternalChgDTO> composeExternalCharges(LCCClientExternalChgDTO extChgDTO, int noOfAdults) {
		List<LCCClientExternalChgDTO> colExternalChgDTO = new ArrayList<>();
		if (extChgDTO != null) {
			LCCClientExternalChgDTO extChgDTOClone = extChgDTO.clone();
			extChgDTO.setAmountConsumedForPayment(false);

			if (extChgDTO.getAmount().doubleValue() > 0) {
				colExternalChgDTO.addAll(getPerPaxExternalCharges(extChgDTOClone, noOfAdults));
			}
		}

		return colExternalChgDTO;
	}

	public static Collection<LCCClientExternalChgDTO> getPerPaxExternalCharges(LCCClientExternalChgDTO externalChgDTO,
			int numberOfConfirmedAdults) {

		BigDecimal[] bdEffectiveChgAmount = AccelAeroCalculator.roundAndSplit(externalChgDTO.getAmount(),
				numberOfConfirmedAdults);
		Collection<LCCClientExternalChgDTO> colExternalChgDTO = new ArrayList<>();

		LCCClientExternalChgDTO tmpExternalChgDTO;
		for (int i = 0; i < numberOfConfirmedAdults; i++) {
			tmpExternalChgDTO = externalChgDTO.clone();
			tmpExternalChgDTO.setAmount(bdEffectiveChgAmount[i]);

			colExternalChgDTO.add(tmpExternalChgDTO);
		}

		return colExternalChgDTO;
	}

	public static LCCClientBalancePayment getBalancePayment(IPGIdentificationParamsDTO ipgIdentificationParamsDTO,
			LCCClientReservation reservation, BookingSessionStore bookingSessionStore,
			Map<Integer, CommonCreditCardPaymentInfo> mapTnxIds) throws ModuleException {

		ExternalChgDTO externalChgDTO = null;
		ExternalChgDTO jnExternalChgDTO = null;
		int adultChildCount;
		ExternalChargesMediator externalChargesMediator;
		LinkedList<ExternalChgDTO> perPaxExternalCharges = null;
		LCCClientBalancePayment lccClientBalancePayment = new LCCClientBalancePayment();
		PostPaymentDTO postPaymentDTO = bookingSessionStore.getPostPaymentInformation();
		LoyaltyInformation loyaltyInformation = bookingSessionStore.getLoyaltyInformation();
		PayCurrencyDTO cardPayCurrencyDTO = postPaymentDTO.getPayCurrencyDTO();

		IPGResponseDTO ipgResponseDTO = postPaymentDTO.getIpgResponseDTO();

		lccClientBalancePayment.setContactInfo(reservation.getContactInfo());
		if (postPaymentDTO.getPnr() != null) {
			lccClientBalancePayment.setGroupPNR(postPaymentDTO.getPnr());
		} else {
			lccClientBalancePayment.setGroupPNR(bookingSessionStore.getPnr());
		}
		lccClientBalancePayment.setReservationStatus(reservation.getStatus());
		lccClientBalancePayment.setPnrSegments(reservation.getSegments());
		lccClientBalancePayment.setPassengers(reservation.getPassengers());
		lccClientBalancePayment.setTemporyPaymentMap(mapTnxIds);
		if (loyaltyInformation != null && loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo() != null) {
			lccClientBalancePayment.setLoyaltyPaymentInfo(
					loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo().get(AppSysParamsUtil.getDefaultCarrierCode()));
		}

		@SuppressWarnings("unchecked")
		// TODO CP: Refactor this method
		Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap = postPaymentDTO.getTemporyPaymentMap();
		CommonCreditCardPaymentInfo creditInfo = getCommonCreditCardPaymentInfo(temporyPaymentMap,
				ipgResponseDTO == null ? false : ipgResponseDTO.isSuccess());

		CommonCreditCardPaymentInfo creditCardPaymentInfo = null;
		BigDecimal totalLoyaltyPayAmount = BigDecimal.ZERO;
		Date currentDatetime = new Date();

		BigDecimal totalAmountAlreadyPaidAtIPG = AccelAeroCalculator.getDefaultBigDecimalZero();

		// TODO CP: Add Mashreq amount when there is MASHREQ_LOYALTY payment

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments = loyaltyInformation.getCarrierWiseLoyaltyPaymentInfo();
		Map<Integer, BigDecimal> paxWiseRedeemedAmounts = AirProxyReservationUtil
				.getPaxRedeemedTotalMap(carrierWiseLoyaltyPayments);

		String loyaltyMemberAccountId;
		String[] rewardIDs;

		BigDecimal totalLMSMemberPayment = AccelAeroCalculator.getDefaultBigDecimalZero();

		Collection<LCCClientReservationPax> passengers = reservation.getPassengers();
		boolean isInfantPaymentSeparated = reservation.isInfantPaymentSeparated();
		if (!LoyaltyPaymentOption.TOTAL.equals(loyaltyInformation.getLoyaltyPaymentOption()) && temporyPaymentMap != null) {
			creditCardPaymentInfo = (CommonCreditCardPaymentInfo) BeanUtils.getFirstElement(temporyPaymentMap.values());
			Map<EXTERNAL_CHARGES, ExternalChgDTO> payExtChgs = postPaymentDTO.getSelectedExternalCharges();
			// TODO CP: verify ----> externalChargesAdaptor.adapt(bookingSessionStore.getPaymentExternalCharges());
			if (payExtChgs != null) {
				externalChgDTO = payExtChgs.get(EXTERNAL_CHARGES.CREDIT_CARD);
				jnExternalChgDTO = payExtChgs.get(EXTERNAL_CHARGES.JN_OTHER);
			}
			// Credit card payment adult count
			int[] payablePaxCount = getPaxCountForCreditCardPayment(passengers, totalLoyaltyPayAmount, paxWiseRedeemedAmounts,
					isInfantPaymentSeparated);

			if (payablePaxCount[0] + payablePaxCount[1] > 0) {
				// Fix me: validate & review
				// IBE balance payment only credit card payment
				if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
					Map<EXTERNAL_CHARGES, ExternalChgDTO> creditCard = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
					creditCard.put(EXTERNAL_CHARGES.CREDIT_CARD, externalChgDTO);
					creditCard.put(EXTERNAL_CHARGES.JN_OTHER, jnExternalChgDTO);
					lccClientBalancePayment.setExternalChargesMap(creditCard);
					externalChargesMediator = new ExternalChargesMediator(createPaxList(reservation.getPassengers()), creditCard,
							true, false);
					externalChargesMediator.setCalculateJNTaxForCCCharge(true);
					perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(payablePaxCount[0],
							payablePaxCount[1]);
				}
				totalAmountAlreadyPaidAtIPG = creditCardPaymentInfo.getPayCurrencyAmount();
			}
		}

		ArrayList<String> redeemedVoucherList = new ArrayList<String>();

		BigDecimal remainingVoucherRedeemedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal voucherPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		PayByVoucherInfo sessionPayByVoucherInfo = null;
		if (bookingSessionStore.getPayByVoucherInfo() != null) {
			sessionPayByVoucherInfo = bookingSessionStore.getPayByVoucherInfo().clone();
			remainingVoucherRedeemedAmount = sessionPayByVoucherInfo.getRedeemedTotal();
			voucherPaymentAmount = sessionPayByVoucherInfo.getRedeemedTotal();
			redeemedVoucherList.addAll(sessionPayByVoucherInfo.getVoucherIDList());
		}

		for (LCCClientReservationPax pax : passengers) {

			Integer paxSequence = pax.getPaxSequence();
			BigDecimal amountDue = pax.getTotalAvailableBalance();
			List<LCCClientExternalChgDTO> perPaxCCServiceCharges = new ArrayList<LCCClientExternalChgDTO>();

			// add service Tax amount for CC pax wise
			if (bookingSessionStore.getPostPaymentInformation().getPaxWiseServiceTaxCCFee() != null
					&& !bookingSessionStore.getPostPaymentInformation().getPaxWiseServiceTaxCCFee().isEmpty()) {

				boolean paxWiseServiceTaxForCCFeeExists = false;

				Map<Integer, List<LCCClientExternalChgDTO>> paxWiseServiceTaxCCFee = bookingSessionStore
						.getPostPaymentInformation().getPaxWiseServiceTaxCCFee();
				if (paxWiseServiceTaxCCFee.containsKey(pax.getPaxSequence())
						&& !paxWiseServiceTaxCCFee.get(pax.getPaxSequence()).isEmpty()) {
					for (LCCClientExternalChgDTO paxServiceTaxCharge : paxWiseServiceTaxCCFee.get(pax.getPaxSequence())) {
						perPaxCCServiceCharges.add(paxServiceTaxCharge);
						paxWiseServiceTaxForCCFeeExists = true;
					}
				}

				if (paxWiseServiceTaxForCCFeeExists) {
					if (!bookingSessionStore.getPostPaymentInformation().getChargeCodeWiseServiceTaxExtCharges().isEmpty()) {
						ServiceTaxExtCharges serviceTaxExtCharges = new ServiceTaxExtCharges();
						serviceTaxExtCharges.setChgGrpCode(ReservationInternalConstants.ChargeGroup.TAX);
						serviceTaxExtCharges.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);
						for (String chargeCode : bookingSessionStore.getPostPaymentInformation()
								.getChargeCodeWiseServiceTaxExtCharges().keySet()) {
							serviceTaxExtCharges.addServiceTaxes(bookingSessionStore.getPostPaymentInformation()
									.getChargeCodeWiseServiceTaxExtCharges().get(chargeCode));

						}
						lccClientBalancePayment.getExternalChargesMap().put(EXTERNAL_CHARGES.SERVICE_TAX, serviceTaxExtCharges);
					}
				}
			}

			if ((isInfantPaymentSeparated || !pax.getPaxType().equals(PaxTypeTO.INFANT))
					&& amountDue.compareTo(BigDecimal.ZERO) >= 0) {

				Collection<ExternalChgDTO> ppec = (perPaxExternalCharges != null && !perPaxExternalCharges.isEmpty())
						? (Collection<ExternalChgDTO>) perPaxExternalCharges.pop()
						: null;
				List<LCCClientExternalChgDTO> extCharges = converToProxyExtCharges(ppec);
				if (!perPaxCCServiceCharges.isEmpty()) {
					extCharges.addAll(perPaxCCServiceCharges);
				}
				PaymentAssemblerComposer paymentComposer = new PaymentAssemblerComposer(extCharges);

				BigDecimal lmsMemberPayment = BigDecimal.ZERO;
				if (reconcileLmsPayAmount(paxWiseRedeemedAmounts.get(paxSequence), amountDue) != null) {
					lmsMemberPayment = reconcileLmsPayAmount(paxWiseRedeemedAmounts.get(paxSequence), amountDue);
				}

				BigDecimal totalExtChgs = getTotalExtCharge(extCharges);

				if (totalLoyaltyPayAmount == null) {
					totalLoyaltyPayAmount = BigDecimal.ZERO;
				}
				PaxPaymentUtil paxPayUtil = null;

				paxPayUtil = new PaxPaymentUtil(AccelAeroCalculator.add(amountDue, totalExtChgs), totalExtChgs,
						totalLoyaltyPayAmount, lmsMemberPayment, remainingVoucherRedeemedAmount,
						AccelAeroCalculator.getDefaultBigDecimalZero());

				totalLoyaltyPayAmount = paxPayUtil.getRemainingLoyalty();
				remainingVoucherRedeemedAmount = paxPayUtil.getRemainingVoucherAmount();

				while (paxPayUtil.hasConsumablePayments()) {
					PaymentMethod payMethod = paxPayUtil.getNextPaymentMethod();
					BigDecimal paxPayAmt = paxPayUtil.getPaymentAmount(payMethod);
					if (payMethod == PaymentMethod.LMS) {
						for (Entry<String, LoyaltyPaymentInfo> loyaltyPaymentInfoEntry : carrierWiseLoyaltyPayments.entrySet()) {
							LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = loyaltyPaymentInfoEntry.getValue();
							Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
									.getPaxProductPaymentBreakdown();
							BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil
									.getPaxRedeemedTotal(carrierPaxProductRedeemed, paxSequence);

							loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
							rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
							totalLMSMemberPayment = AccelAeroCalculator.add(totalLMSMemberPayment, carrierLmsMemberPayment);
							if (cardPayCurrencyDTO != null) {
								cardPayCurrencyDTO.setTotalPayCurrencyAmount(carrierLmsMemberPayment);
							}
							if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
								paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs, carrierPaxProductRedeemed,
										carrierLmsMemberPayment, cardPayCurrencyDTO, currentDatetime,
										loyaltyPaymentInfoEntry.getKey());
							}
						}
					} else if (payMethod == PaymentMethod.MASHREQ_LOYALTY) {
						// TODO CP: Fill this for MASHREQ_LOYALTY
					} else if (payMethod == PaymentMethod.VOUCHER) {
						if (sessionPayByVoucherInfo != null) {
							paymentComposer.addVoucherPayment(sessionPayByVoucherInfo, paxPayAmt,
								cardPayCurrencyDTO, currentDatetime);
						}
					} else {
						paymentComposer.addCreditCardPaymentInternal(paxPayAmt, ipgResponseDTO, ipgIdentificationParamsDTO,
								cardPayCurrencyDTO, currentDatetime, creditInfo.getNo(), creditInfo.geteDate(),
								creditInfo.getSecurityCode(), creditInfo.getName(), creditCardPaymentInfo.getPayReference(),
								creditCardPaymentInfo.getActualPaymentMethod(), null);
					}

				}

				LCCClientPaymentAssembler paymentAssembler = paymentComposer.getPaymentAssembler();
				lccClientBalancePayment.addPassengerPayments(pax.getPaxSequence(), paymentAssembler);
			}
		}

		// This is because AARESAA-8570. Currently we don't do a match between the IPG payment amount with amount due.
		// Due to damm loyalty, we can't rely on the IPG amount to do the payment. So a check is enforced at the end
		// to verify these amounts tally or not.
		if (lccClientBalancePayment.getPaxSeqWisePayAssemblers() != null
				&& lccClientBalancePayment.getPaxSeqWisePayAssemblers().size() > 0) {

			BigDecimal totalAmountAboutToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (LCCClientPaymentAssembler paymentAssembler : lccClientBalancePayment.getPaxSeqWisePayAssemblers().values()) {
				totalAmountAboutToPay = AccelAeroCalculator.add(totalAmountAboutToPay, paymentAssembler.getTotalPayAmount());
			}

			// TODO CP: Add Mashreq amount when there is MASHREQ_LOYALTY payment
			BigDecimal loyaltyCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

			BigDecimal totalAmountAlreadyPaid = AccelAeroCalculator.add(totalAmountAlreadyPaidAtIPG, loyaltyCredit,
					totalLMSMemberPayment, voucherPaymentAmount);
			if (AccelAeroCalculator.subtract(totalAmountAboutToPay, totalAmountAlreadyPaid).abs().doubleValue() > 0.3) {
				throw new ModuleException("airreservations.balance.payment.failed");
			}
		}

		return lccClientBalancePayment;

	}

	public static BigDecimal getTotalExtCharge(List<LCCClientExternalChgDTO> extChgList) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	private static Integer getInfantSeq(List<Passenger> passengers, int paxSequenceID) {
		Integer sequence = null;
		for (Passenger passenger : passengers) {
			if (passenger.getTravelWith() == paxSequenceID) {
				sequence = new Integer(passenger.getPaxSequence());
			}
		}
		return sequence;
	}

	private static List<ReservationPaxTO> createPaxList(Set<LCCClientReservationPax> resPassengers) {
		ReservationPassengerAdaptor reservationPassengerAdaptor = new ReservationPassengerAdaptor();
		List<ReservationPaxTO> reservationPaxList = new ArrayList<>();
		AdaptorUtils.adaptCollection(resPassengers, reservationPaxList, reservationPassengerAdaptor);
		return reservationPaxList;
	}

	private static CommonCreditCardPaymentInfo
			getCommonCreditCardPaymentInfo(Map<Integer, CommonCreditCardPaymentInfo> tempPayMap, boolean isPaymentSuccess) {
		CommonCreditCardPaymentInfo cardPaymentInfo = null;

		if (tempPayMap != null && !tempPayMap.isEmpty()) {
			Set<Integer> tmpKeySet = tempPayMap.keySet();

			for (Integer key : tmpKeySet) {
				if (key != null) {
					cardPaymentInfo = tempPayMap.get(key);
					cardPaymentInfo.setPaymentSuccess(isPaymentSuccess);
					if (cardPaymentInfo != null) {
						break;
					}
				}
			}
		}
		return cardPaymentInfo;
	}

	private static int[] getPaxCountForCreditCardPayment(Collection<LCCClientReservationPax> passengers,
			BigDecimal totalLoyaltyPayAmount, Map<Integer, BigDecimal> paxWiseRedeemedAmounts, boolean isInfantPaymentSeparated) {
		int adultCount = 0;
		int infantCount = 0;
		if (passengers != null) {
			for (LCCClientReservationPax pax : passengers) {
				if (!PaxTypeTO.INFANT.equals(pax.getPaxType()) || isInfantPaymentSeparated) {
					BigDecimal amountDue = pax.getTotalAvailableBalance();
					BigDecimal lmsMemberPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (paxWiseRedeemedAmounts.get(pax.getPaxSequence()) != null) {
						lmsMemberPayment = paxWiseRedeemedAmounts.get(pax.getPaxSequence());
					}

					if (amountDue.compareTo(lmsMemberPayment) > 0) {
						amountDue = AccelAeroCalculator.subtract(amountDue, lmsMemberPayment);
						if (totalLoyaltyPayAmount == null) {
							totalLoyaltyPayAmount = BigDecimal.ZERO;
						}
						if (AccelAeroCalculator.subtract(amountDue, totalLoyaltyPayAmount).compareTo(BigDecimal.ZERO) < 0) {
							totalLoyaltyPayAmount = AccelAeroCalculator.subtract(totalLoyaltyPayAmount, amountDue);
						} else {
							totalLoyaltyPayAmount = BigDecimal.ZERO;
							if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
								adultCount++;
							} else {
								infantCount++;
							}

						}
					}

				}
			}
		}
		int[] payablePaxCount = { adultCount, infantCount };
		return payablePaxCount;
	}

	private static BigDecimal reconcileLmsPayAmount(BigDecimal lmsPayAmount, BigDecimal amountDue) {
		if ((lmsPayAmount != null && amountDue != null) && (lmsPayAmount.equals(amountDue) || AccelAeroCalculator
				.subtract(amountDue, lmsPayAmount).abs().compareTo(AccelAeroRounderPolicy.LMS_THREASHOLD_AMOUNT) <= 0)) {
			return amountDue;
		}
		return lmsPayAmount;
	}

	public static List<LCCClientExternalChgDTO> converToProxyExtCharges(Collection<ExternalChgDTO> extChgs) {
		List<LCCClientExternalChgDTO> extChgList = new ArrayList<>();
		if (extChgs != null) {
			for (ExternalChgDTO chg : extChgs) {
				extChgList.add(new LCCClientExternalChgDTO(chg));
			}
		}
		return extChgList;
	}

	public static boolean isParent(List<Passenger> passengers, int paxSequenceID) {
		boolean isParent = false;
		for (Passenger passenger : passengers) {
			if (passenger.getTravelWith() == paxSequenceID) {
				isParent = true;
				break;
			}
		}
		return isParent;
	}

	public static int getPayablePaxCount(Collection<Passenger> passengers) {
		int totalPaxCount = 0;
		for (Passenger passenger : passengers) {
			if (passenger.getPaxType().equals(PaxTypeTO.ADULT) || passenger.getPaxType().equals(PaxTypeTO.CHILD)
					|| passenger.getPaxType().equals(PaxTypeTO.PARENT) || passenger.getPaxType().equals(PaxTypeTO.INFANT)) {
				totalPaxCount++;
			}
		}
		return totalPaxCount;
	}

	public static void validatePassengerFlightDetails(List<SessionFlightSegment> flightSegments, FareSegChargeTO fareSegChargeTO)
			throws ModuleException {
		boolean isSuccess = true;

		if (flightSegments != null && fareSegChargeTO != null) {

			Collection<OndFareSegChargeTO> ondFareSegChargeTOs = fareSegChargeTO.getOndFareSegChargeTOs();
			if (ondFareSegChargeTOs == null) {
				isSuccess = false;
			}

			Collection<Integer> flightSegIdList = getFlightSegmentIdList(flightSegments);
			for (OndFareSegChargeTO ondFareSegChargeTO : ondFareSegChargeTOs) {
				Set<Integer> keySet = ondFareSegChargeTO.getFsegBCAlloc().keySet();
				for (Integer key : keySet) {
					if (!flightSegIdList.contains(key)) {
						isSuccess = false;
						break;
					}
				}
			}

			if (!isSuccess) {
				throw new ModuleException("msg.multiple.invalid.flight");
			}
		}

	}

	private static Collection<Integer> getFlightSegmentIdList(List<SessionFlightSegment> flightSegments) {
		Collection<Integer> flightSegList = new ArrayList<>();

		for (SessionFlightSegment flightSegment : flightSegments) {
			flightSegList.add(flightSegment.getFlightSegmentId());
		}

		return flightSegList;
	}

	public static void populateResPaxWithSelAnciDtls(List<AncillaryIntegratePassengerTO> ancillaryIntegratePassengerTos,
			CommonReservationAssembler lcclientReservationAssembler) {

		Set<LCCClientReservationPax> reservationPaxs = lcclientReservationAssembler.getLccreservation().getPassengers();
		for (LCCClientReservationPax lccClientReservationPax : reservationPaxs) {
			if (ancillaryIntegratePassengerTos != null) {
				populatePaxAnci(ancillaryIntegratePassengerTos, lccClientReservationPax);
			}
		}
	}

	private static void populatePaxAnci(List<AncillaryIntegratePassengerTO> ancillaryIntegratePassengerTos,
			LCCClientReservationPax lccClientReservationPax) {
		for (AncillaryIntegratePassengerTO ancillaryIntegratePassengerTo : ancillaryIntegratePassengerTos) {
			if (ancillaryIntegratePassengerTo.getPassengerRph().equals(lccClientReservationPax.getPaxSequence().toString())) {
				if (ancillaryIntegratePassengerTo.getSelectedAncillaries() != null) {
					lccClientReservationPax.addSelectedAncillaries(ancillaryIntegratePassengerTo.getSelectedAncillaries());
				}
				break;
			}
		}
	}

	/*
	 * Method use to set segment sequence for ssr external charge DTO objects
	 */
	public static void setSegmentSeqForSSR(List<AncillaryIntegratePassengerTO> paxList, List<FlightSegmentTO> flightSegmentTOs,
			Integer startFltSegSeq) {

		SegmentUtil.setFltSegSequence(flightSegmentTOs, startFltSegSeq);

		for (AncillaryIntegratePassengerTO reservationPaxTO : paxList) {
			setextChgSequence(reservationPaxTO.getExternalCharges(), flightSegmentTOs);
			setSelectedAnciFltSegSeq(reservationPaxTO.getSelectedAncillaries(), flightSegmentTOs);
		}
	}

	private static void setSelectedAnciFltSegSeq(List<LCCSelectedSegmentAncillaryDTO> selAnciList,
			List<FlightSegmentTO> flightSegmentTOs) {
		if (selAnciList != null && selAnciList.size() > 0) {
			for (LCCSelectedSegmentAncillaryDTO selAnciDTO : selAnciList) {
				if (selAnciDTO.getFlightSegmentTO() != null && flightSegmentTOs != null) {
					selAnciDTO.getFlightSegmentTO().setSegmentSequence(
							getSegmentSeqFromSegments(flightSegmentTOs, selAnciDTO.getFlightSegmentTO().getFlightRefNumber()));
				}
			}
		}
	}

	private static void setextChgSequence(Collection<LCCClientExternalChgDTO> externalChgDTOs,
			List<FlightSegmentTO> flightSegmentTOs) {
		if (externalChgDTOs != null) {
			for (LCCClientExternalChgDTO extChgDTO : externalChgDTOs) {
				if (extChgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
					extChgDTO.setSegmentSequence(getSegmentSeqFromSegments(flightSegmentTOs, extChgDTO.getFlightRefNumber()));
				} else if (extChgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_TRANSFER) {
					extChgDTO.setSegmentSequence(getSegmentSeqFromSegments(flightSegmentTOs, extChgDTO.getFlightRefNumber()));
				} else if (extChgDTO.getExternalCharges() == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
					extChgDTO.setSegmentSequence(getSegmentSeqFromSegments(flightSegmentTOs, extChgDTO.getFlightRefNumber()));
				}
			}
		}
	}

	private static int getSegmentSeqFromSegments(List<FlightSegmentTO> flightSegmentTOs, String fltRefNo) {
		int seq = 0;
		for (FlightSegmentTO fltSegment : flightSegmentTOs) {
			if (fltSegment.getFlightRefNumber().equals(fltRefNo)) {
				seq = fltSegment.getSegmentSequence();
				break;
			}
		}

		return seq;
	}

	public static Date getFirstDeparture(LCCClientReservation reservation) {
		if (reservation.getSegments() != null) {
			List<LCCClientReservationSegment> segmentList = reservation.getSegments().stream()
					.filter(seg -> !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(seg.getStatus()))
					.collect(Collectors.toList());

			if (!segmentList.isEmpty()) {
				Collections.sort(segmentList);
				return segmentList.get(0).getDepartureDate();
			}
		}
		return null;
	}

	public static String getContactLastName(LCCClientReservation reservation) {
		return reservation.getContactInfo().getLastName();
	}

	public static BigDecimal getTotalExtCharge(Collection<LCCClientExternalChgDTO> extChgList,
			List<EXTERNAL_CHARGES> skipCharges) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			if (skipCharges == null || (skipCharges != null && !skipCharges.contains(chg.getExternalCharges()))) {
				total = AccelAeroCalculator.add(total, chg.getAmount());
			}
		}
		return total;
	}
}
