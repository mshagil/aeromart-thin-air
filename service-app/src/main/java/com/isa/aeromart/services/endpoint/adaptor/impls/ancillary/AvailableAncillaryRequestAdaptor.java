package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.Ancillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.BasicReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.AvailableAncillaryRQ;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

public class AvailableAncillaryRequestAdaptor extends
		TransactionAwareAdaptor<AvailableAncillaryRQ, Map<AncillariesConstants.AncillaryType, Object>> {

	public Map<AncillariesConstants.AncillaryType, Object> adapt(AvailableAncillaryRQ source) {
		Map<AncillariesConstants.AncillaryType, Object> transformedMap = new HashMap<>();

		AncillaryRequestAdaptor transformer;
		AncillariesConstants.AncillaryType ancillaryType;
		ReservationDiscountDTO resDiscountDTO = null;

		List<InventoryWrapper> flightSegments = getTransactionalAncillaryService().getFlightSegments(getTransactionId());
		List<InventoryWrapper> allFlightSegments = getTransactionalAncillaryService().getAllFlightSegments(getTransactionId());
		FareSegChargeTO pricingInformation = getTransactionalAncillaryService().getPricingInformation(getTransactionId());
		TravellerQuantity travellerQuantity = getTransactionalAncillaryService().getTravellerQuantityFromSession(
				getTransactionId());
		PreferenceInfo preferenceInfo = getTransactionalAncillaryService().getPreferencesForAncillary(getTransactionId());
		SYSTEM system = ProxyConstants.SYSTEM.getEnum(preferenceInfo.getSelectedSystem());

		String pnr = getTransactionalAncillaryService().getReservationInfo(getTransactionId()) == null ? ""
				: getTransactionalAncillaryService().getReservationInfo(getTransactionId()).getPnr();
		List<Ancillary> ancillaries = source.getAncillaries();
		boolean isModifyAnci = getTransactionalAncillaryService().isAncillaryTransaction(getTransactionId());
		Map<String, Integer> serviceCount = getTransactionalAncillaryService().getServiceCount(getTransactionId());
		

		SessionBasicReservation sessionReservation = getTransactionalAncillaryService().getReservationData(getTransactionId());
		BasicReservation basicReservation = AnciCommon.toBasicReservation(sessionReservation);
		DiscountedFareDetails discountedFareDetails = getTransactionalAncillaryService().getDiscountedFareDetails(
				getTransactionId());
		if (discountedFareDetails != null) {
			if (PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY.equals(discountedFareDetails.getDiscountAs())
					&& !PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(discountedFareDetails
							.getPromotionType())) {
				DiscountRQ discountRQ = AnciCommon.createDiscountRQ(discountedFareDetails, travellerQuantity, getTransactionId(),
						pricingInformation, preferenceInfo);
				try {
					resDiscountDTO = ReservationModuleUtils.getAirproxyReservationBD().calculatePromotionDiscount(discountRQ,
							getTrackInfoDTO());

				} catch (ModuleException e) {
					throw new ValidationException(ValidationException.Code.ANCI_AVAILABLE_GENERIC_ERROR);
				}
			}
		}

		// set track info
		for (Ancillary ancillary : ancillaries) {
			ancillaryType = AncillariesConstants.AncillaryType.resolveAncillaryType(ancillary.getType());
			transformer = AncillaryAdaptorFactory.getRequestAdaptor(ancillary.getType());
			transformedMap.put(ancillaryType, transformer.toAvailableAncillaries(getTrackInfoDTO(), basicReservation,
					source.getMetaData(), flightSegments, allFlightSegments, pricingInformation, travellerQuantity, system,
					source.getTransactionId(), pnr, isModifyAnci, resDiscountDTO, serviceCount));
		}

		return transformedMap;
	}
}
