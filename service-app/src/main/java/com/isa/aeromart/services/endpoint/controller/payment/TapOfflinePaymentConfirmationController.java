package com.isa.aeromart.services.endpoint.controller.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.constant.TapPaymentConfirmConstants;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.payment.ConfirmPaymentService;
import com.isa.aeromart.services.endpoint.dto.payment.tapOffline.TapOfflinePaymentConfirmRequest;
import com.isa.aeromart.services.endpoint.dto.payment.tapOffline.TapOfflinePaymentConfirmResponse;
import com.isa.thinair.commons.api.exception.ModuleException;

@Controller
@RequestMapping(value = "tapOffline", method = RequestMethod.GET)
public class TapOfflinePaymentConfirmationController extends StatefulController {
	
	@Autowired
	private ConfirmPaymentService confirmPaymentService;
	
	@ResponseBody
	@RequestMapping(value = "/payment/confirm", method = RequestMethod.GET)
	public String confirmPayment(@RequestParam(TapPaymentConfirmConstants.ORDER_NUMBER) String orderNumber,
			@RequestParam(TapPaymentConfirmConstants.PAY_MODE) String payMode,
			@RequestParam(TapPaymentConfirmConstants.PAY_REFERENCE_ID) String payRefId,
			@RequestParam(TapPaymentConfirmConstants.RESPONSE_CODE) String resCode,
			@RequestParam(TapPaymentConfirmConstants.RESPONSE_MESSAGE) String resMessage,
			@RequestParam(TapPaymentConfirmConstants.TAP_REFERENCE_ID) String tapRefId) throws ModuleException {

		TapOfflinePaymentConfirmRequest confirmRequest = getConfirmRequest(orderNumber, resCode, resMessage, payMode, payRefId,
				tapRefId);
		
		TapOfflinePaymentConfirmResponse confirmResponse = confirmPaymentService.confirmTapOfflinePayment(confirmRequest, getTrackInfo());

		return confirmResponse.getMessage();
	}

	private TapOfflinePaymentConfirmRequest getConfirmRequest(String ordNo, String resCode, String resMsg, String payMode,
			String payRefID, String tapRefID) {
		TapOfflinePaymentConfirmRequest request = new TapOfflinePaymentConfirmRequest();
		request.setOrderNumber(ordNo);
		request.setResCode(resCode);
		request.setResMessage(resMsg);
		request.setPayMode(payMode);
		request.setPayRefId(payRefID);
		request.setTapRefId(tapRefID);
		return request;
	}
}
