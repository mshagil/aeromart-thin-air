package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.BalanceSummary;
import com.isa.aeromart.services.endpoint.dto.booking.ChargeTemplate;
import com.isa.aeromart.services.endpoint.dto.booking.ChargeTemplate.Category;
import com.isa.aeromart.services.endpoint.dto.booking.ChargeTemplate.CurrencyType;
import com.isa.aeromart.services.endpoint.dto.booking.PaxCredits;
import com.isa.aeromart.services.endpoint.dto.booking.PaxPayments;
import com.isa.aeromart.services.endpoint.utils.common.LableServiceUtil;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;

public class BalanceSummaryAdaptor implements Adaptor<LCCClientReservation, BalanceSummary> {

	private static final String CREDIT_BALANCE = "creditBalance";
	private static final String BALANCE_TO_PAY = "balanceToPay";

	CurrencyExchangeRate currencyExchangeRate;

	public BalanceSummaryAdaptor(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	@Override
	public BalanceSummary adapt(LCCClientReservation reservation) {
		BalanceSummary balanceSummary = new BalanceSummary();

		PaxCreditAdaptor paxCreditAdaptor = new PaxCreditAdaptor();
		PaxPaymentAdaptor paxPaymentAdaptor = new PaxPaymentAdaptor();

		BigDecimal creditBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<PaxCredits> colCreditInfo = new ArrayList<>();
		Collection<PaxPayments> colPaxPayments = new ArrayList<>();

		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			creditBalance = AccelAeroCalculator.add(creditBalance, pax.getTotalAvailableBalance());
		}

		AdaptorUtils.adaptCollection(reservation.getPassengers(), colCreditInfo, paxCreditAdaptor);
		AdaptorUtils.adaptCollection(reservation.getPassengers(), colPaxPayments, paxPaymentAdaptor);

		balanceSummary.setChargeBreakdown(getReservationBalanceSummay(reservation));
		balanceSummary.setTotalPayment(reservation.getTotalPaidAmount().toString());
		balanceSummary.setTotalCharges(reservation.getTotalChargeAmount().toString());
		balanceSummary.setTotalCredits(creditBalance.toString());
		balanceSummary.setPaxWisePayments(colPaxPayments);
		balanceSummary.setPaxWiseCredits(colCreditInfo);
		return balanceSummary;
	}

	public List<ChargeTemplate> getReservationBalanceSummay(LCCClientReservation lccClientReservation) {
		List<ChargeTemplate> chargeTemplates = new ArrayList<>();

		String language = LocaleContextHolder.getLocale().toString();
		
		if(language == null || language.trim().isEmpty()){
			language ="en";
		}

		if (lccClientReservation != null) {
			Map<String, BigDecimal> paxChargers = getPaxCharges(lccClientReservation.getPassengers());
			Map<EXTERNAL_CHARGES, BigDecimal> externalSurcharges = lccClientReservation.getExternalChargersSummary();

			BigDecimal seatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal mealCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal baggageCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal insuranceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal crediCardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal airportServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal airportTransferCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal autoCheckinCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal anciPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (externalSurcharges != null) {
				seatCharge = externalSurcharges.get(EXTERNAL_CHARGES.SEAT_MAP) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.SEAT_MAP)
						: seatCharge;
				mealCharge = externalSurcharges.get(EXTERNAL_CHARGES.MEAL) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.MEAL)
						: mealCharge;
				baggageCharge = externalSurcharges.get(EXTERNAL_CHARGES.BAGGAGE) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.BAGGAGE)
						: baggageCharge;
				airportServiceCharge = externalSurcharges.get(EXTERNAL_CHARGES.AIRPORT_SERVICE) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.AIRPORT_SERVICE)
						: airportServiceCharge;
				airportTransferCharge = externalSurcharges.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER)
						: airportTransferCharge;
				autoCheckinCharge = externalSurcharges.get(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN)
						: autoCheckinCharge;
				insuranceCharge = externalSurcharges.get(EXTERNAL_CHARGES.INSURANCE) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.INSURANCE)
						: insuranceCharge;
				flexiCharge = externalSurcharges.get(EXTERNAL_CHARGES.FLEXI_CHARGES) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.FLEXI_CHARGES)
						: flexiCharge;
				crediCardCharge = externalSurcharges.get(EXTERNAL_CHARGES.CREDIT_CARD) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.CREDIT_CARD)
						: crediCardCharge;
				anciPenalty = externalSurcharges.get(EXTERNAL_CHARGES.ANCI_PENALTY) != null
						? externalSurcharges.get(EXTERNAL_CHARGES.ANCI_PENALTY)
						: anciPenalty;
			}

			BigDecimal totalListSurchages = AccelAeroCalculator.add(seatCharge, mealCharge, insuranceCharge, flexiCharge,
					crediCardCharge, baggageCharge, airportServiceCharge, airportTransferCharge, autoCheckinCharge);

			addChargeTemplate(chargeTemplates, language, lccClientReservation.getTotalTicketFare(),
					"PgPriceBreakDown_lblAirFare", lccClientReservation.getLastCurrencyCode(), Category.FARE);
			addChargeTemplate(chargeTemplates, language, lccClientReservation.getTotalTicketTaxCharge(),
					"PgPriceBreakDown_lblTax", lccClientReservation.getLastCurrencyCode(), Category.TAX);
			addChargeTemplate(chargeTemplates, language,
					AccelAeroCalculator.subtract(AccelAeroCalculator.add(lccClientReservation.getTotalTicketSurCharge(),
							lccClientReservation.getTotalTicketAdjustmentCharge()), totalListSurchages),
					"PgPriceBreakDown_lblSurcharges", lccClientReservation.getLastCurrencyCode(), Category.SURCHRGE);

			addChargeTemplate(chargeTemplates, language, seatCharge, "PgPriceBreakDown_lblSeatSelection",
					lccClientReservation.getLastCurrencyCode(), Category.ANCILLARY);
			addChargeTemplate(chargeTemplates, language, mealCharge, "PgPriceBreakDown_lblMealSelection",
					lccClientReservation.getLastCurrencyCode(), Category.ANCILLARY);
			addChargeTemplate(chargeTemplates, language, baggageCharge, "PgPriceBreakDown_lblBaggageSelection",
					lccClientReservation.getLastCurrencyCode(), Category.ANCILLARY);
			addChargeTemplate(chargeTemplates, language, autoCheckinCharge, "PgPriceBreakDown_lblAutoCheckinSelection",
					lccClientReservation.getLastCurrencyCode(), Category.ANCILLARY);
			addChargeTemplate(chargeTemplates, language, airportServiceCharge, "PgPriceBreakDown_lblHalaSelection",
					lccClientReservation.getLastCurrencyCode(), Category.ANCILLARY);
			addChargeTemplate(chargeTemplates, language, airportTransferCharge, "PgPriceBreakDown_lblAptSelection",
					lccClientReservation.getLastCurrencyCode(), Category.ANCILLARY);
			addChargeTemplate(chargeTemplates, language, flexiCharge, "PgPriceBreakDown_lblFlexiChages",
					lccClientReservation.getLastCurrencyCode(), Category.ANCILLARY);
			addChargeTemplate(chargeTemplates, language, insuranceCharge, "PgPriceBreakDown_lblInsurance",
					lccClientReservation.getLastCurrencyCode(), Category.ANCILLARY);
			addChargeTemplate(chargeTemplates, language, lccClientReservation.getTotalTicketModificationCharge(),
					"PgPriceBreakDown_lblmodificationCharge", lccClientReservation.getLastCurrencyCode(), Category.MODIFICATION_CHARGE);
			addChargeTemplate(chargeTemplates, language, lccClientReservation.getTotalTicketCancelCharge(),
					"PgPriceBreakDown_lblCancelCharge", lccClientReservation.getLastCurrencyCode(), Category.CANCELLATION_CHARGE);
			addChargeTemplate(chargeTemplates, language, crediCardCharge, "PgPriceBreakDown_lblTranFree",
					lccClientReservation.getLastCurrencyCode(), Category.TRANSACTION_FEE);
			addChargeTemplate(chargeTemplates, language, anciPenalty, "PgPriceBreakDown_lblAnciPenalty",
					lccClientReservation.getLastCurrencyCode(), Category.PENALTY);

			LCCPromotionInfoTO lccPromotionInfoTO = lccClientReservation.getLccPromotionInfoTO();
			String discountLabel = "";
			BigDecimal discountAmount = BigDecimal.ZERO;

			if (lccPromotionInfoTO != null) {
				discountLabel = "PgPriceBreakDown_lblDiscount";
				if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
					discountLabel = "PgPriceBreakDown_lblDiscountCredit";
				}
				discountAmount = getDiscountAmount(lccClientReservation, lccPromotionInfoTO);
			}

			if (lccPromotionInfoTO != null && DiscountApplyAsTypes.MONEY.equals(lccPromotionInfoTO.getDiscountAs())) {
				addChargeTemplate(chargeTemplates, language, discountAmount, discountLabel,
						lccClientReservation.getLastCurrencyCode(), Category.DISCOUNT);
			}

			if (lccClientReservation.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) != 0) {
				addChargeTemplate(chargeTemplates, language, paxChargers.get(CREDIT_BALANCE),
						"PgPriceBreakDown_lblCreditBalance", lccClientReservation.getLastCurrencyCode(), Category.CREDIT_BALANCE);
				addChargeTemplate(chargeTemplates, language, paxChargers.get(BALANCE_TO_PAY), "PgPriceBreakDown_lblBalanceToPay",
						lccClientReservation.getLastCurrencyCode(), Category.BALANCE_TO_PAY);
			}

			if (lccPromotionInfoTO != null && DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
				addChargeTemplate(chargeTemplates, language, discountAmount, discountLabel,
						lccClientReservation.getLastCurrencyCode(), Category.DISCOUNT);
			}

			addChargeTemplate(chargeTemplates, language, lccClientReservation.getTotalPaidAmount(),
					"PgPayment_lblTotalPaid", AppSysParamsUtil.getBaseCurrency(), Category.TOTAl_PAID_IN_BASE_CURRENCY);

			if (!AppSysParamsUtil.getBaseCurrency().equals(lccClientReservation.getLastCurrencyCode())
					&& lccClientReservation.getLastCurrencyCode() != null) {
				addChargeTemplate(chargeTemplates, language, lccClientReservation.getTotalTicketPrice(),
					"PgPriceBreakDown_lblTotalChargeInSelectedCurrency", lccClientReservation.getLastCurrencyCode(), Category.TOTAl_TICKET_PRICE_IN_SELECTED_CURRENCY);
			}

		}

		return chargeTemplates;
	}

	private BigDecimal getPaidCurrencyAmount(String currencyCode, BigDecimal amount) {
		BigDecimal amountInGivenCurrency = amount;
		if (!AppSysParamsUtil.getBaseCurrency().equals(currencyCode)) {
			String selectedCurrency = currencyCode;

			if (selectedCurrency != null && (!"".equals(selectedCurrency)) && currencyExchangeRate != null
					&& selectedCurrency.equals(currencyExchangeRate.getCurrency().getCurrencyCode())) {
				Currency pgCurrency = currencyExchangeRate.getCurrency();
				amountInGivenCurrency = AccelAeroRounderPolicy.convertAndRound(amount,
						currencyExchangeRate.getMultiplyingExchangeRate(), pgCurrency.getBoundryValue(),
						pgCurrency.getBreakPoint());
			}
		}
		return amountInGivenCurrency;
	}

	private static BigDecimal getDiscountAmount(LCCClientReservation lccClientReservation,
			LCCPromotionInfoTO lccPromotionInfoTO) {
		BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (lccClientReservation.getTotalDiscount() != null
				&& !AccelAeroCalculator.isEqual(lccClientReservation.getTotalDiscount(),
						AccelAeroCalculator.getDefaultBigDecimalZero())) {
			BigDecimal dispTotalDiscount = lccClientReservation.getTotalDiscount();
			if (AccelAeroCalculator.isLessThan(dispTotalDiscount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				dispTotalDiscount = dispTotalDiscount.negate();
			}

			if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
				discount = dispTotalDiscount;
			} else {
				discount = dispTotalDiscount.negate();
			}
		} else if (DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())
				&& lccPromotionInfoTO.getCreditDiscountAmount() != null
				&& !lccPromotionInfoTO.getCreditDiscountAmount().equals(BigDecimal.ZERO)) {

			BigDecimal dispTotalDiscount = lccPromotionInfoTO.getCreditDiscountAmount();
			if (AccelAeroCalculator.isLessThan(dispTotalDiscount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				dispTotalDiscount = dispTotalDiscount.negate();
			}

			discount = dispTotalDiscount;
		}
		return discount;
	}

	private void addChargeTemplate(List<ChargeTemplate> chargeTemplates, String language, BigDecimal chargeAmount,
			String labelName, String currencyCode, Category category) {
		if (!AccelAeroCalculator.isEqual(chargeAmount.abs(), AccelAeroCalculator.getDefaultBigDecimalZero().abs())) {
			ChargeTemplate chargeTemplate = new ChargeTemplate();
			chargeTemplate.setType(LableServiceUtil.getBalanceSummaryDescription(labelName, language));
			chargeTemplate.setAmount(new StringBuffer()
					.append(AccelAeroCalculator.formatAsDecimal(getPaidCurrencyAmount(currencyCode, chargeAmount))).toString());
			if (!AppSysParamsUtil.getBaseCurrency().equals(currencyCode) && currencyCode != null) {
				chargeTemplate.setCurrencyType(CurrencyType.PAID);
			}
			chargeTemplate.setCategory(category);
			chargeTemplates.add(chargeTemplate);
		}
	}

	private static Map<String, BigDecimal> getPaxCharges(Collection<LCCClientReservationPax> passengers) {
		Map<String, BigDecimal> ancilaryChargesMap = new HashMap<>();
		BigDecimal seatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal mealCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal baggageCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal airportServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal airportTranferCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (passengers != null) {
			for (LCCClientReservationPax pax : passengers) {
				// Pax Balance
				if (pax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) > 0) {
					balanceToPay = AccelAeroCalculator.add(balanceToPay, pax.getTotalAvailableBalance());
				} else {
					creditBalance = AccelAeroCalculator.add(creditBalance, pax.getTotalAvailableBalance());
				}
			}
		}

		ancilaryChargesMap.put("seat", seatCharge);
		ancilaryChargesMap.put("meal", mealCharge);
		ancilaryChargesMap.put("baggage", baggageCharge);
		ancilaryChargesMap.put("airportService", airportServiceCharge);
		ancilaryChargesMap.put("airportTransfer", airportTranferCharge);
		ancilaryChargesMap.put("creditBalance", creditBalance);
		ancilaryChargesMap.put("balanceToPay", balanceToPay);

		return ancilaryChargesMap;
	}

}
