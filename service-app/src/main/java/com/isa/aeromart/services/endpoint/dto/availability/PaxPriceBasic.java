package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;

public class PaxPriceBasic {

	// type of pax eg:- AD,CH,IN
	private String paxType;

	// total pax price
	private BigDecimal total = BigDecimal.ZERO;

	public PaxPriceBasic() {

	}

	public PaxPriceBasic(String paxType) {
		this.paxType = paxType;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
