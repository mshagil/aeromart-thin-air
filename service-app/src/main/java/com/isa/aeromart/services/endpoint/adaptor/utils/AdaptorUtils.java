package com.isa.aeromart.services.endpoint.adaptor.utils;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AdaptorUtils {

	private static final String CARRIER_SEPERATOR = "|";
	private static final String VERSION_SEPERATOR = "-";

	public static <S, T> void adaptCollection(Collection<S> source, Collection<T> destination, Adaptor<S, T> adaptor) {
		for (S sourceItem : source) {
			T destinationItem = adaptor.adapt(sourceItem);
			if (destinationItem != null) {
				destination.add(destinationItem);
			}
		}
	}

	private static boolean isCorrectCarrierVersion(String version) {
		String[] versions = StringUtils.split(version, CARRIER_SEPERATOR);
		for (String versionNumber : versions) {
			int indexOfAirlineCodeSeparator = versionNumber.lastIndexOf(VERSION_SEPERATOR);
			if (indexOfAirlineCodeSeparator == -1) {
				return false;
			}
		}
		return true;
	}

	public static String getCorrectInterlineVersion(String inVersion) {
		if (!isCorrectCarrierVersion(inVersion)) {
			return (AppSysParamsUtil.getDefaultCarrierCode() + VERSION_SEPERATOR + inVersion);
		}
		return inVersion;
	}

	public static boolean isReservationCabinClassOnholdable(BaseAvailRS flightAvailRS) {
		boolean allowOnhold = false;
		PriceInfoTO priceInfoTO = flightAvailRS.getSelectedPriceFlightInfo();
		if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {
			allowOnhold = !priceInfoTO.getFareTypeTO().isOnHoldRestricted();
		}
		return allowOnhold;
	}
}
