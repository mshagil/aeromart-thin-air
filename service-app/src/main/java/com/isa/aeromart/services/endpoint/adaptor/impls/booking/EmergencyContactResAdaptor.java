package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.common.PhoneNumberResTransformer;
import com.isa.aeromart.services.endpoint.dto.common.EmergencyContact;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;

public class EmergencyContactResAdaptor implements Adaptor<CommonReservationContactInfo, EmergencyContact> {

	@Override
	public EmergencyContact adapt(CommonReservationContactInfo commonReservationContactInfo) {
		EmergencyContact emergencyContact = new EmergencyContact();

		PhoneNumberResTransformer phoneNumberResTransformer = new PhoneNumberResTransformer();

		emergencyContact.setEmgnEmail(commonReservationContactInfo.getEmgnEmail());
		emergencyContact.setEmgnFirstName(commonReservationContactInfo.getEmgnFirstName());
		emergencyContact.setEmgnLastName(commonReservationContactInfo.getEmgnLastName());
		emergencyContact.setEmgnPhoneNumber(phoneNumberResTransformer.adapt(commonReservationContactInfo.getEmgnPhoneNo()));
		emergencyContact.setEmgnTitle(commonReservationContactInfo.getEmgnTitle());

		return emergencyContact;

	}

}
