package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class CustomerLoginCheckRS extends TransactionalBaseRS {

	private boolean customerLoggedIn = false;

	public boolean getCustomerLoggedIn() {
		return customerLoggedIn;
	}

	public void setCustomerLoggedIn(boolean customerLoggedIn) {
		this.customerLoggedIn = customerLoggedIn;
	}

}
