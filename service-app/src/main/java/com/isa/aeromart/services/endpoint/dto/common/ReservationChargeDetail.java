package com.isa.aeromart.services.endpoint.dto.common;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ReservationChargeDetail {

	private String description;

	private String chargeGroup;

	private String chargeCode;

	@CurrencyValue
	private BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getChargeGroup() {
		return chargeGroup;
	}

	public void setChargeGroup(String chargeGroup) {
		this.chargeGroup = chargeGroup;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}
	
	public void addChargeAmount(BigDecimal chargeAmount) {		
		this.chargeAmount = AccelAeroCalculator.add(this.chargeAmount,chargeAmount);
	}

}
