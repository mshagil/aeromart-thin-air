package com.isa.aeromart.services.endpoint.dto.ancillary.type.meal;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItem;

public class MealItem extends AncillaryItem {
	
	private String mealCategoryId;
	
	private String mealCategoryCode;

	private String mealImagePath;
	
	private String mealThumbnailImagePath;
	
	private String popularity;

	private Integer available;
	
	private boolean isCategoryRestricted;

	private Integer allocated;

	public String getMealCategoryId() {
		return mealCategoryId;
	}

	public void setMealCategoryId(String mealCategoryId) {
		this.mealCategoryId = mealCategoryId;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getMealImagePath() {
		return mealImagePath;
	}

	public void setMealImagePath(String mealImagePath) {
		this.mealImagePath = mealImagePath;
	}

	public String getMealThumbnailImagePath() {
		return mealThumbnailImagePath;
	}

	public void setMealThumbnailImagePath(String mealThumbnailImagePath) {
		this.mealThumbnailImagePath = mealThumbnailImagePath;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public String getPopularity() {
		return popularity;
	}

	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}

	public boolean isCategoryRestricted() {
		return isCategoryRestricted;
	}

	public void setCategoryRestricted(boolean isCategoryRestricted) {
		this.isCategoryRestricted = isCategoryRestricted;
	}

	public Integer getAllocated() {
		return allocated;
	}

	public void setAllocated(Integer allocated) {
		this.allocated = allocated;
	}
}
