package com.isa.aeromart.services.endpoint.delegate.ancillary;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.BasicReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherInformation;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillaryQuotation;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.AncillarySelection;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionBasicReservation;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.ReservationChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;

public interface TransactionalAncillaryService {

	void clearSession(String transactionId);

	List<InventoryWrapper> getFlightSegments(String transactionId);

	RPHGenerator getRphGenerator(String transactionId);

	void storeReservationData(String transactionId, BasicReservation basicReservation);

	SessionBasicReservation getReservationData(String transactionId);

	void addBlockedAncillaries(String transactionId, MetaData metaData, List<SelectedAncillary> ancillaries);

	void storeAncillaryPreferences(String transactionId, MetaData metaData, List<SelectedAncillary> ancillaries);

	void storeAncillaryQuotations(String transactionId, MetaData metaData, List<AncillaryType> ancillaryQuotations);

	AncillaryQuotation getAncillaryQuotation(String transactionId);

	AncillarySelection getAncillarySelectionsFromSession(String transactionId);

	FareSegChargeTO getPricingInformation(String transactionId);

	TravellerQuantity getTravellerQuantityFromSession(String transactionId);

	ReservationInfo getReservationInfo(String transactionId);

	PreferenceInfo getPreferencesForAncillary(String transactionId);

	void storeAncillaryCharges(String transactionId, ReservationChargeTo<LCCClientExternalChgDTO> reservationCharges);

	List<ModifiedFlight> getModifications(String transactionId);

	Map<String, String> getResSegRphToFlightSegMap(String transactionId);

	void storeReservationInfo(String transactionId, ReservationInfo resInfo);

	PostPaymentDTO getPaymentData(String transactionId);

    LoyaltyInformation getLoyaltyInformation(String transactionId);

	void storeRPHGenerator(String transactionId, RPHGenerator rphGenerator);

	void storePreferenceInfo(String transactionId, PreferenceInfo preferenceInfo);

	void storeSessionSegments(String transactionId, List<FlightSegmentTO> segments);

	void storePricingInformtation(String transactionId, FareSegChargeTO pricingInformation);

	public String initializeAnciModifyTransaction(String transactionId);
	
	List<SessionFlexiDetail> getSessionFlexiDetail(String transactionId);
	
	boolean isAncillaryTransaction(String transactionId);
	
	public boolean isRequoteTransaction(String transactionId);
	
	DiscountedFareDetails getDiscountedFareDetails(String transactionId);
	
	List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxAncillaryExternalCharges(String transactionId);
	
	List<SessionFlightSegment> getSelectedSegments(String transactionId);
	
	void storeUpdatedSessionFlightSegment(String transactionId, List<SessionFlightSegment> segments);
	
	void setDiscounAmount(String transactionId, BigDecimal discountAmount);
	
	void storeServiceCount(String transactionId, Map<String, Integer> serviceCount);
	
	void storeApplicableServiceTaxes(String transactionId, Set<ServiceTaxContainer> applicableServiceTaxes);
	
	Map<String, Integer> getServiceCount(String transactionId);
	
	public boolean isRegUser();
	
	boolean isOnHoldCreated(String transactionId);

	PaymentBaseSessionStore getPaymentBaseSessionStore(String transactionId);

	List<InventoryWrapper> getAllFlightSegments(String transactionId);

	public PayByVoucherInfo getPayByVoucherInfo(String transactionId);

	public VoucherInformation getVoucherInformation(String transactionId);
	
}
