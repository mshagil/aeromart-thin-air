package com.isa.aeromart.services.endpoint.dto.common;

public class Promotion {

	// promotion type. eg: DISCOUNT
	private String type;

	// promotion code. eg: D25
	private String code;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
