package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.SelectedFlightSegmentAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.TravelerQuantityAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.availability.RequoteAddedOND;
import com.isa.aeromart.services.endpoint.dto.availability.RequoteRQ;
import com.isa.aeromart.services.endpoint.dto.common.SpecificFlight;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccResSegmentInfo;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccReservationInfo;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class FareReqouteAdaptor implements Adaptor<LccReservationInfo, FlightPriceRQ> {

	private static Log log = LogFactory.getLog(FareReqouteAdaptor.class);
	private RequoteRQ requoteRequest;
	private RPHGenerator rphGenerator;
	private TrackInfoDTO trackInfo;
	private Map<Date, Boolean> ondQuoteFlexiInitialMap = new HashMap<Date, Boolean>();
	private Map<Date, Boolean> ondFlexiSelectedInitialMap = new HashMap<Date, Boolean>();
	private SYSTEM system;
	private Map<String, List<LccResSegmentInfo>> ondWiseResSegMap;
	private String transactionIdentifier;
	private boolean isCancelSegment;
	private boolean isModifySegment;

	public FareReqouteAdaptor(RequoteRQ request, Map<String, List<LccResSegmentInfo>> ondWiseResSegMap,
			RPHGenerator rphGenerator, TrackInfoDTO trackInfo, String transactionIdentifier, SYSTEM system) {
		this.requoteRequest = request;
		this.rphGenerator = rphGenerator;
		this.trackInfo = trackInfo;
		this.system = system;
		this.ondWiseResSegMap = ondWiseResSegMap;
		this.transactionIdentifier = transactionIdentifier;
		initializeRequestTypes();
	}

	@Override
	public FlightPriceRQ adapt(LccReservationInfo reservation) {
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();

		List<LccResSegmentInfo> ondResSegList;
		int departureVariance = 0;
		Map<String, String> busSegFltSegRPHMap = getGroundSegmentByFlightSegmentRPHMap(reservation.getLccSegmentInfos());

		for (String key : ondWiseResSegMap.keySet()) {

			ondResSegList = getOndWiseSegmentListBy(key);

			List<String> resSegRphList = new ArrayList<String>();
			populateSegRphList(resSegRphList, ondResSegList);

			if (isCancelledOND(resSegRphList))
				continue;
			if (!busSegFltSegRPHMap.isEmpty() && isModifyOrCancelBusSegAttachedFlightSeg(busSegFltSegRPHMap, resSegRphList)) {
				continue;
			}

			ModifiedFlight modifiedData = getModifiedONDData(resSegRphList);
			boolean isModifiedOnd = modifiedData == null ? false : true;

			List<SpecificFlight> newFltSegList = null;
			if (isModifiedOnd) {
				newFltSegList = getModifiedFligtSegData(modifiedData.getToFlightSegRPH());
			}

			OriginDestinationInformationTO ondInfoTO = flightPriceRQ.addNewOriginDestinationInformation();
			OriginDestinationInfoAdaptor adaptor = new OriginDestinationInfoAdaptor(rphGenerator, modifiedData, newFltSegList,
					isModifiedOnd, ondResSegList, reservation.getLccSegmentInfos());
			adaptor.adapt(ondInfoTO);

			populateDateWiseChargeAvailability(ondResSegList, ondInfoTO);
		}

		populateNewlyAddedONDs(flightPriceRQ, departureVariance);
		Collections.sort(flightPriceRQ.getOrderedOriginDestinationInformationList());
		setTravellerQuantity(flightPriceRQ, reservation);
		setAvailablePreferences(flightPriceRQ, reservation);
		flightPriceRQ.setTransactionIdentifier(transactionIdentifier);
		return flightPriceRQ;
	}

	private void populateSegRphList(List<String> resSegRphList, List<LccResSegmentInfo> ondResSegList) {
		for (LccResSegmentInfo resSeg : ondResSegList) {
			resSegRphList.add((ModificationUtils.createResSegRph(resSeg.getCarrierCode(), resSeg.getSegmentSeq())));
		}
	}

	private List<LccResSegmentInfo> getOndWiseSegmentListBy(String key) {
		return ondWiseResSegMap.get(key);
	}

	private void setAvailablePreferences(FlightPriceRQ flightPriceRQ, LccReservationInfo reservation) {
		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		availPref.setLastFareQuotedDate(reservation.getLastFareQuoteDate());
		availPref.setTicketValidTill(reservation.getTicketValiedityLastDate());
		availPref.setRequoteFlightSearch(true);
		availPref.setFromNameChange(false);

		if (requoteRequest.isGroupPnr() || SYSTEM.INT.equals(system)) {
			availPref.setSearchSystem(SYSTEM.INT);
		} else {
			availPref.setSearchSystem(SYSTEM.AA);
		}
		availPref.setAppIndicator(CommonUtil.getAppIndicator(trackInfo.getAppIndicator()));
		availPref.setPnr(requoteRequest.getModificationInfo().getPnr());
		availPref.setQuoteOndFlexi(getOndQuoteFlexi(flightPriceRQ.getOrderedOriginDestinationInformationList()));
		availPref.setOndFlexiSelected(getOndFlexiSelection(flightPriceRQ.getOrderedOriginDestinationInformationList()));
		availPref.setModifyBooking(true);
		availPref.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);

		try {
			availPref.setPointOfSale(ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(trackInfo.getIpAddress()));
		} catch (ModuleException e) {
			log.error("retrieve country code error : FareRequoteAdaptor ==> adapt ");
		}

	}

	private void setTravellerQuantity(FlightPriceRQ flightPriceRQ, LccReservationInfo reservation) {
		TravelerQuantityAdaptor travelerQuantityAdaptor = new TravelerQuantityAdaptor(flightPriceRQ.getTravelerInfoSummary());
		TravellerQuantity travelerQuantity = new TravellerQuantity();
		travelerQuantity.setAdultCount(reservation.getTotalPaxAdultCount());
		travelerQuantity.setChildCount(reservation.getTotalChildPaxCount());
		travelerQuantity.setInfantCount(reservation.getTotalInfantPaxCount());
		travelerQuantityAdaptor.adapt(travelerQuantity);
	}

	private Map<Integer, Boolean> getOndQuoteFlexi(List<OriginDestinationInformationTO> orderedOriginDestinationInformationList) {
		Map<Integer, Boolean> ondQuoteFlexi = new HashMap<Integer, Boolean>();
		int ondSeq = 0;
		for (OriginDestinationInformationTO ond : orderedOriginDestinationInformationList) {
			ondQuoteFlexi.put(ondSeq, ondQuoteFlexiInitialMap.get(ond.getPreferredDate()));
			ondSeq++;
		}
		return ondQuoteFlexi;
	}

	private Map<Integer, Boolean> getOndFlexiSelection(
			List<OriginDestinationInformationTO> orderedOriginDestinationInformationList) {
		Map<Integer, Boolean> ondFlexiSelect = new HashMap<Integer, Boolean>();
		int ondSeq = 0;
		for (OriginDestinationInformationTO ond : orderedOriginDestinationInformationList) {
			ondFlexiSelect.put(ondSeq, ondFlexiSelectedInitialMap.get(ond.getPreferredDate()));
			ondSeq++;
		}
		return ondFlexiSelect;
	}

	private List<OriginDestinationInformationTO> createNewlyAddedOND(int departureVariance) {

		List<OriginDestinationInformationTO> newOndINfoList = new ArrayList<OriginDestinationInformationTO>();

		for (RequoteAddedOND addedONDData : requoteRequest.getModificationInfo().getAddedFlights()) {

			OriginDestinationInformationTO newOriginDestinationINfo = new OriginDestinationInformationTO();

			List<SpecificFlight> flightList = new ArrayList<SpecificFlight>();

			for (String flightRph : addedONDData.getToFlightSegRPH()) {
				for (SpecificFlight specFlt : requoteRequest.getFlightSegments()) {
					if (flightRph.equals(specFlt.getFlightSegmentRPH())) {
						flightList.add(specFlt);
					}
				}
			}

			Collections.sort(flightList);

			List<FlightSegmentTO> flightSegementTOList = new ArrayList<FlightSegmentTO>();

			SpecificFlight firstSegment = flightList.get(0);
			SpecificFlight lastSegment = flightList.get(flightList.size() - 1);

			String origin = SegmentUtil.getFromAirport(firstSegment.getSegmentCode());
			String destination = SegmentUtil.getToAirport(lastSegment.getSegmentCode());

			Date depatureDate = flightList.get(0).getDepartureDateTime();
			Date depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(CalendarUtil.getStartTimeOfDate(depatureDate),
					departureVariance * -1);
			Date depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(CalendarUtil.getEndTimeOfDate(depatureDate),
					departureVariance);

			newOriginDestinationINfo.setOrigin(origin);
			newOriginDestinationINfo.setDestination(destination);
			newOriginDestinationINfo.setPreferredDate(depatureDate);
			newOriginDestinationINfo.setDepartureDateTimeStart(depatureDateTimeStart);
			newOriginDestinationINfo.setDepartureDateTimeEnd(depatureDateTimeEnd);

			newOriginDestinationINfo.setPreferredLogicalCabin(addedONDData.getPreferences().getLogicalCabinClass());
			newOriginDestinationINfo.setPreferredClassOfService(addedONDData.getPreferences().getLogicalCabinClass());
			newOriginDestinationINfo.setPreferredBookingClass(addedONDData.getPreferences().getBookingCode());

			if (addedONDData.getPreferences().getAdditionalPreferences().getFareClassType().equals(FareClassType.BUNDLE)) {
				newOriginDestinationINfo.setPreferredBundleFarePeriodId(addedONDData.getPreferences().getAdditionalPreferences()
						.getFareClassId());
			}

			newOriginDestinationINfo.setPreferredBookingType(addedONDData.getPreferences().getSeatType());

			flightSegementTOList.addAll(getFlightSegmentTOList(flightList));

			OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
			depOndOptionTO.getFlightSegmentList().addAll(flightSegementTOList);

			newOriginDestinationINfo.setFlownOnd(false);
			newOriginDestinationINfo.getOrignDestinationOptions().add(depOndOptionTO);
			newOndINfoList.add(newOriginDestinationINfo);
		}

		return newOndINfoList;
	}

	private List<SpecificFlight> getModifiedFligtSegData(List<String> toFlightSegRPH) {

		List<SpecificFlight> fltList = new ArrayList<SpecificFlight>();

		for (SpecificFlight flt : requoteRequest.getFlightSegments()) {
			if (toFlightSegRPH.contains(flt.getFlightSegmentRPH())) {
				fltList.add(flt);
			}
		}
		Collections.sort(fltList);
		return fltList;
	}

	private ModifiedFlight getModifiedONDData(List<String> resSegRphList) {
		if (requoteRequest.getModificationInfo().getModifiedFlights() != null) {
			for (ModifiedFlight flt : requoteRequest.getModificationInfo().getModifiedFlights()) {
				List<String> modifiedResSegRPHList = flt.getFromResSegRPH();
				if (modifiedResSegRPHList.containsAll(resSegRphList)) {
					return flt;
				}
			}
		}
		return null;
	}

	private boolean isCancelledOND(List<String> resSegRphList) {
		if (requoteRequest.getModificationInfo().getCnxFlights() != null) {
			return requoteRequest.getModificationInfo().getCnxFlights().containsAll(resSegRphList);
		}
		return false;
	}

	private List<FlightSegmentTO> getFlightSegmentTOList(List<SpecificFlight> reqSpecFltList) {
		List<FlightSegmentTO> fltSegToLst = new ArrayList<FlightSegmentTO>();
		SelectedFlightSegmentAdaptor adaptor = new SelectedFlightSegmentAdaptor(rphGenerator);
		AdaptorUtils.adaptCollection(reqSpecFltList, fltSegToLst, adaptor);
		return fltSegToLst;
	}

	private Map<String, String> getGroundSegmentByFlightSegmentRPHMap(Set<LccResSegmentInfo> pnrSegments) {
		Map<String, String> busSegFltSegRPHMap = new HashMap<String, String>();
		for (LccResSegmentInfo resSegment : pnrSegments) {
			if (resSegment.getGroundStationPnrSegmentID() != null) {
				for (LccResSegmentInfo resSegmentInner : pnrSegments) {
					if (resSegment.getGroundStationPnrSegmentID().equals(resSegmentInner.getPnrSegID())) {
						busSegFltSegRPHMap.put(
								ModificationUtils.createResSegRph(resSegmentInner.getCarrierCode(),
										resSegmentInner.getSegmentSeq()),
								ModificationUtils.createResSegRph(resSegment.getCarrierCode(), resSegment.getSegmentSeq()));
					}
				}
			}
		}
		return busSegFltSegRPHMap;
	}

	private boolean isModifyOrCancelBusSegAttachedFlightSeg(Map<String, String> busSegFltSegRPHMap, List<String> resSegRPHList) {
		String fromResSegRPH;
		for (String resSegRPH : resSegRPHList) {
			fromResSegRPH = busSegFltSegRPHMap.get(resSegRPH);
			if (fromResSegRPH != null) {
				if (requoteRequest.isModifySegment()) {
					for (ModifiedFlight modifiedFlt : requoteRequest.getModificationInfo().getModifiedFlights()) {
						if (modifiedFlt.getFromResSegRPH().contains(fromResSegRPH)) {
							return true;
						}
					}
				}

				if (requoteRequest.isCancelSegment()
						&& requoteRequest.getModificationInfo().getCnxFlights().contains(fromResSegRPH)) {
					return true;
				}
			}
		}

		return false;
	}

	private void populateDateWiseChargeAvailability(List<LccResSegmentInfo> ondResSegList,
			OriginDestinationInformationTO ondInfoTO) {
		boolean isQuoteFlexi = isQuoteFlexi(ondResSegList);
		ondQuoteFlexiInitialMap.put(ondInfoTO.getPreferredDate(), isQuoteFlexi);
		ondFlexiSelectedInitialMap.put(ondInfoTO.getPreferredDate(), ondInfoTO.isFlexiSelected());
	}

	private void populateNewlyAddedONDs(FlightPriceRQ flightPriceRQ, int departureVariance) {
		if (!isCancelSegment && !isModifySegment) {
			flightPriceRQ.getOrderedOriginDestinationInformationList().addAll(createNewlyAddedOND(departureVariance));
		}
	}

	private boolean isQuoteFlexi(List<LccResSegmentInfo> ondResSegList) {
		return ondResSegList.get(0).getFlexiRuleID() != null ? false : true;
	}

	private void initializeRequestTypes() {
		this.isCancelSegment = requoteRequest.isCancelSegment();
		this.isModifySegment = requoteRequest.isModifySegment();
	}

}
