package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.customer.FamilyMemberDTO;
import com.isa.thinair.aircustomer.api.model.FamilyMember;

public class FamilyMemberRQAdaptor implements Adaptor<FamilyMemberDTO, FamilyMember> {

	private RPHGenerator familyMemberIDHolder;

	public FamilyMemberRQAdaptor(RPHGenerator familyMemberIDHolder) {
		this.familyMemberIDHolder = familyMemberIDHolder;
	}

	@Override
	public FamilyMember adapt(FamilyMemberDTO familyMemberDTO) {
		FamilyMember familyMember = new FamilyMember();
		if (familyMemberIDHolder.getUniqueIdentifier(familyMemberDTO.getFamilyMemberId()) != null) {
			familyMember.setFamilyMemberId(
					Integer.valueOf(familyMemberIDHolder.getUniqueIdentifier(familyMemberDTO.getFamilyMemberId())));
		}
		familyMember.setTitle(familyMemberDTO.getTitle());
		familyMember.setFirstName(familyMemberDTO.getFirstName().toLowerCase());
		familyMember.setLastName(familyMemberDTO.getLastName().toLowerCase());
		familyMember.setNationalityCode(familyMemberDTO.getNationalityCode());
		familyMember.setRelationshipId(familyMemberDTO.getRelationshipId());
		familyMember.setDateOfBirth(familyMemberDTO.getDateOfBirth());
		return familyMember;
	}

}
