package com.isa.aeromart.services.endpoint.utils.common;

import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.IBEAppParameterDTO;
import com.isa.aeromart.services.endpoint.utils.masterdata.IBEParamKeys;

/**
 * 
 * @author rajitha
 *
 */
public class IBEParamKeyUtil {
	
	List<IBEAppParameterDTO> paramList = new ArrayList<IBEAppParameterDTO>();

	public List<IBEAppParameterDTO> prepareIbeParamKeys() {
		for (IBEParamKeys appParam : IBEParamKeys.values()) {

			IBEAppParameterDTO paramKeyValue = new IBEAppParameterDTO();
			paramKeyValue.setKey(appParam.getKey());
			paramKeyValue.setValue(appParam.getValue());;
			paramList.add(paramKeyValue);

		}
		return paramList;
	}

}
