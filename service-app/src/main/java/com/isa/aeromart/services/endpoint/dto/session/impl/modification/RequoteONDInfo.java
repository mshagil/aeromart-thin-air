package com.isa.aeromart.services.endpoint.dto.session.impl.modification;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;

public class RequoteONDInfo {

	private List<String> flightSegIdList;
	
	private List<String> flightSegRPHlist;

	private List<String> existingResSegRPHList;

	private List<String> modifiedResSegList;

	private List<FareTO> oldPerPaxFareTOList = null;

	private String status = null;	

	public List<String> getExistingResSegRPHList() {
		if (existingResSegRPHList == null) {
			return new ArrayList<String>();
		}
		return existingResSegRPHList;
	}

	public void setExistingResSegRPHList(List<String> existingPnrSegRPHList) {
		this.existingResSegRPHList = existingPnrSegRPHList;
	}

	public List<String> getModifiedResSegList() {
		if (modifiedResSegList == null) {
			return new ArrayList<String>();
		}
		return modifiedResSegList;
	}

	public void setModifiedResSegList(List<String> modifiedResSegList) {
		this.modifiedResSegList = modifiedResSegList;
	}

	public List<FareTO> getOldPerPaxFareTOList() {
		if (oldPerPaxFareTOList == null) {
			return new ArrayList<FareTO>();
		}
		return oldPerPaxFareTOList;
	}

	public void setOldPerPaxFareTOList(List<FareTO> oldPerPaxFareTOList) {
		this.oldPerPaxFareTOList = oldPerPaxFareTOList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getFlightSegIdList() {
		if (flightSegIdList == null) {
			return new ArrayList<String>();
		}
		return flightSegIdList;
	}

	public void setFlightSegIdList(List<String> flightSegIdList) {
		this.flightSegIdList = flightSegIdList;
	}

	public List<String> getFlightSegRPHlist() {
		if (flightSegRPHlist == null) {
			return new ArrayList<String>();
		}
		return flightSegRPHlist;
	}

	public void setFlightSegRPHlist(List<String> flightSegRPHlist) {
		this.flightSegRPHlist = flightSegRPHlist;
	}

}
