package com.isa.aeromart.services.endpoint.dto.masterdata;

public class CurrencyExchangeRate {

	private String currencyCode;

	private double baseToCurrExRate; // this is the exchange rate from the base currency to the actual currency. Should
										// divide the base
										// currency from this amount to get amount
	private int decimalPlaces;

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public double getBaseToCurrExRate() {
		return baseToCurrExRate;
	}

	public void setBaseToCurrExRate(double baseToCurrExRate) {
		this.baseToCurrExRate = baseToCurrExRate;
	}

	public int getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(int decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}
}
