package com.isa.aeromart.services.endpoint.utils.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.dto.common.ReservationChargeDetail;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.ReservationChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.FareInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentRequoteSessionStore;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentSessionStore;
import com.isa.aeromart.services.endpoint.utils.common.LableServiceUtil;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class ChargeDetailsBuilder {

	private PaymentUtils paymentUtils = new PaymentUtils();
	private PaymentBaseSessionStore paymentBaseSessionStore = null;

	public ChargeDetailsBuilder(PaymentBaseSessionStore paymentBaseSessionStore) {
		this.paymentBaseSessionStore = paymentBaseSessionStore;
	}

	public List<ReservationChargeDetail> buildCreateFlowReservationChargeDetails() throws ModuleException {

		List<ReservationChargeDetail> chargeDetails = null;
		if (AppSysParamsUtil.isShowIBEV3ChargesBreakdownInPaymentPage() && this.paymentBaseSessionStore != null) {
			PaymentSessionStore paymentSessionStore = (PaymentSessionStore) this.paymentBaseSessionStore;
			QuotedFareRebuildDTO quotedFareRBuilder = getQuotedFareRebuildDTO(paymentSessionStore, null);
			PreferenceInfo preferenceInfo = paymentSessionStore.getPreferencesForPayment();

			if (SYSTEM.getEnum(preferenceInfo.getSelectedSystem()) == SYSTEM.AA) {
				Collection<OndFareDTO> collFares = AirproxyModuleUtils.getAirReservationQueryBD()
						.recreateFareSegCharges(quotedFareRBuilder.getFirstOndRebuildCriteria());

				chargeDetails = reservationChargesTotalByChargeCode(collFares,
						paymentSessionStore.getTravellerQuantityForPayment());

				if (paymentSessionStore.getTotalPaymentInformation() != null
						&& paymentSessionStore.getTotalPaymentInformation().getFinancialStore() != null) {

					List<ReservationChargeDetail> externalCharges = getReservationExternalCharges(
							paymentSessionStore.getTotalPaymentInformation().getFinancialStore());
					if (externalCharges != null && !externalCharges.isEmpty()) {
						if (chargeDetails == null)
							chargeDetails = new ArrayList<>();

						chargeDetails.addAll(new ArrayList<>(externalCharges));
					}

				}

			}
		}
		return chargeDetails;

	}

	public List<ReservationChargeDetail> buildRequoteFlowReservationChargeDetails() throws ModuleException {

		List<ReservationChargeDetail> chargeDetails = null;
		if (AppSysParamsUtil.isShowIBEV3ChargesBreakdownInPaymentPage() && this.paymentBaseSessionStore != null) {
			PaymentRequoteSessionStore paymentRequoteSessionStore = (PaymentRequoteSessionStore) this.paymentBaseSessionStore;
			QuotedFareRebuildDTO quotedFareRBuilder = getQuotedFareRebuildDTO(paymentRequoteSessionStore, null);
			PreferenceInfo preferenceInfo = paymentRequoteSessionStore.getPreferencesForPayment();

			if (SYSTEM.getEnum(preferenceInfo.getSelectedSystem()) == SYSTEM.AA) {
				Collection<OndFareDTO> collFares = AirproxyModuleUtils.getAirReservationQueryBD()
						.recreateFareSegCharges(quotedFareRBuilder.getFirstOndRebuildCriteria());

				chargeDetails = reservationChargesTotalByChargeCode(collFares,
						paymentRequoteSessionStore.getTravellerQuantityForPayment());

				includeRequotedOtherExtraCharges(paymentRequoteSessionStore.getFareInfo(), chargeDetails);

				if (paymentRequoteSessionStore.getFinancialStore() != null) {

					List<ReservationChargeDetail> externalCharges = getReservationExternalCharges(
							paymentRequoteSessionStore.getFinancialStore());
					if (externalCharges != null && !externalCharges.isEmpty()) {

						if (chargeDetails == null)
							chargeDetails = new ArrayList<>();

						chargeDetails.addAll(new ArrayList<>(externalCharges));
					}

				}

			}
		}

		return chargeDetails;
	}

	private QuotedFareRebuildDTO getQuotedFareRebuildDTO(PaymentBaseSessionStore paymentBaseSessionStore,
			DiscountedFareDetails discountedFareDetails) {

		List<FareSegChargeTO> fareSegChargeTOs = new ArrayList<FareSegChargeTO>();

		fareSegChargeTOs.add(paymentBaseSessionStore.getFareSegChargeTOForPayment());

		IPaxCountAssembler paxAssm = paymentUtils.getIPaxCountAssembler(paymentBaseSessionStore.getTravellerQuantityForPayment());

		QuotedFareRebuildDTO quotedFareRebuildDTO = new QuotedFareRebuildDTO(fareSegChargeTOs, paxAssm, discountedFareDetails,
				paymentBaseSessionStore.getPreferencesForPayment().getSeatType());
		return quotedFareRebuildDTO;
	}

	private List<ReservationChargeDetail> reservationChargesTotalByChargeCode(Collection<OndFareDTO> collFares,
			TravellerQuantity travellerQuantity) throws ModuleException {

		boolean hasAdult = false;
		boolean hasChild = false;
		boolean hasInfant = false;
		int adultCount = 0;
		int childCount = 0;
		int infantCount = 0;
		if (travellerQuantity != null) {
			adultCount = travellerQuantity.getAdultCount();
			childCount = travellerQuantity.getChildCount();
			infantCount = travellerQuantity.getInfantCount();
			if (adultCount > 0) {
				hasAdult = true;
			}
			if (childCount > 0) {
				hasChild = true;
			}

			if (infantCount > 0) {
				hasInfant = true;
			}

		}

		Map<String, ReservationChargeDetail> reservationChargesMap = new HashMap<>();
		if (collFares != null && !collFares.isEmpty()) {
			for (OndFareDTO OndFareDTO : collFares) {
				Collection<String> validPaxType = new ArrayList<>();
				Collection<QuotedChargeDTO> allCharges = OndFareDTO.getAllCharges();

				BigDecimal adultFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal childFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal infantFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (hasAdult) {
					adultFareAmount = AccelAeroCalculator
							.parseBigDecimal(OndFareDTO.getFareSummaryDTO().getFareAmount(PassengerType.ADULT));
					adultFareAmount = AccelAeroCalculator.multiply(adultFareAmount, adultCount);
				}

				if (hasChild) {
					childFareAmount = AccelAeroCalculator
							.parseBigDecimal(OndFareDTO.getFareSummaryDTO().getFareAmount(PassengerType.CHILD));
					childFareAmount = AccelAeroCalculator.multiply(childFareAmount, childCount);
				}

				if (hasInfant) {
					infantFareAmount = AccelAeroCalculator
							.parseBigDecimal(OndFareDTO.getFareSummaryDTO().getFareAmount(PassengerType.INFANT));
					infantFareAmount = AccelAeroCalculator.multiply(infantFareAmount, infantCount);
				}

				ReservationChargeDetail fareDetail = null;

				if (reservationChargesMap.get(PricingConstants.ChargeGroups.FARE) == null) {
					fareDetail = new ReservationChargeDetail();
				} else {
					fareDetail = reservationChargesMap.get(PricingConstants.ChargeGroups.FARE);
				}

				BigDecimal totalFareAmount = AccelAeroCalculator.add(adultFareAmount, childFareAmount, infantFareAmount);

				fareDetail.addChargeAmount(totalFareAmount);
				fareDetail.setChargeCode(PricingConstants.ChargeGroups.FARE);
				fareDetail.setChargeGroup(PricingConstants.ChargeGroups.FARE);
				fareDetail.setDescription("Fare");

				reservationChargesMap.put(PricingConstants.ChargeGroups.FARE, fareDetail);

				if (allCharges != null) {

					Iterator<QuotedChargeDTO> allChargesItr = allCharges.iterator();
					while (allChargesItr.hasNext()) {
						QuotedChargeDTO quotedChargeDTO = allChargesItr.next();

						if (hasAdult) {
							if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
									|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
									|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
									|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {

								validPaxType.add(PassengerType.ADULT);

							}
						}
						if (hasChild) {
							if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY
									|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
									|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {

								validPaxType.add(PassengerType.CHILD);
							}
						}

						if (hasInfant) {
							if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
									|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
									|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT) {

								validPaxType.add(PassengerType.INFANT);

							}
						}

						if (validPaxType == null || validPaxType.isEmpty()) {
							// this charge is not applicable for selected PAX Type.
							continue;
						}
						ReservationChargeDetail chargeDetail = null;

						if (reservationChargesMap.get(quotedChargeDTO.getChargeCode()) == null) {
							chargeDetail = new ReservationChargeDetail();
						} else {
							chargeDetail = reservationChargesMap.get(quotedChargeDTO.getChargeCode());
						}

						BigDecimal adultChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						BigDecimal childChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						BigDecimal infantChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

						if (validPaxType.contains(PassengerType.ADULT)) {
							adultChargeAmount = ReservationApiUtils.getApplicableChargeAmount(PassengerType.ADULT,
									quotedChargeDTO);
							adultChargeAmount = AccelAeroCalculator.multiply(adultChargeAmount,
									travellerQuantity.getAdultCount());
						}

						if (validPaxType.contains(PassengerType.CHILD)) {
							childChargeAmount = ReservationApiUtils.getApplicableChargeAmount(PassengerType.CHILD,
									quotedChargeDTO);
							childChargeAmount = AccelAeroCalculator.multiply(childChargeAmount,
									travellerQuantity.getChildCount());
						}

						if (validPaxType.contains(PassengerType.INFANT)) {
							infantChargeAmount = ReservationApiUtils.getApplicableChargeAmount(PassengerType.INFANT,
									quotedChargeDTO);
							infantChargeAmount = AccelAeroCalculator.multiply(infantChargeAmount,
									travellerQuantity.getInfantCount());
						}
						BigDecimal totalChargeAmount = AccelAeroCalculator.add(adultChargeAmount, childChargeAmount,
								infantChargeAmount);
						if (totalChargeAmount.doubleValue() > 0) {
							chargeDetail.addChargeAmount(totalChargeAmount);
							chargeDetail.setChargeCode(quotedChargeDTO.getChargeCode());
							chargeDetail.setChargeGroup(quotedChargeDTO.getChargeGroupCode());
							chargeDetail.setDescription(quotedChargeDTO.getChargeDescription());

							reservationChargesMap.put(quotedChargeDTO.getChargeCode(), chargeDetail);

						}

					}

				}

			}
		}

		if (reservationChargesMap.values() != null) {
			return new ArrayList<ReservationChargeDetail>(reservationChargesMap.values());
		}

		return null;
	}

	private void addReservationExternalCharges(ReservationChargeTo<LCCClientExternalChgDTO> ancillaryExternalCharges,
			Map<String, ReservationChargeDetail> reservationChargesMap) {

		if (ancillaryExternalCharges != null) {
			List<PassengerChargeTo<LCCClientExternalChgDTO>> passengers = ancillaryExternalCharges.getPassengers();

			if (passengers != null && !passengers.isEmpty()) {
				for (PassengerChargeTo<LCCClientExternalChgDTO> passenger : passengers) {
					List<LCCClientExternalChgDTO> passengerCharges = passenger.getGetPassengerCharges();
					for (LCCClientExternalChgDTO charge : passengerCharges) {
						addChargeDetailsToMap(charge, reservationChargesMap);
					}

				}
			}

			List<LCCClientExternalChgDTO> reservationCharges = ancillaryExternalCharges.getReservationCharges();
			if (reservationCharges != null && !reservationCharges.isEmpty()) {
				for (LCCClientExternalChgDTO charge : reservationCharges) {

					addChargeDetailsToMap(charge, reservationChargesMap);
				}
			}

		}

	}

	private List<ReservationChargeDetail> getReservationExternalCharges(FinancialStore financialStore) {
		Collection<ReservationChargeDetail> externalCharges = null;
		if (financialStore != null) {

			Map<String, ReservationChargeDetail> reservationChargesMap = new HashMap<>();
			addReservationExternalCharges(financialStore.getAncillaryExternalCharges(), reservationChargesMap);
			addReservationExternalCharges(financialStore.getAvailabilityExternalCharges(), reservationChargesMap);

			updateExternalChargeDescription(reservationChargesMap);

			externalCharges = reservationChargesMap.values();

		}

		return new ArrayList<ReservationChargeDetail>(externalCharges);
	}

	private void updateExternalChargeDescription(Map<String, ReservationChargeDetail> reservationChargesMap) {

		if (reservationChargesMap != null && !reservationChargesMap.isEmpty()) {
			String language = LocaleContextHolder.getLocale().getLanguage();
			for (String chargeCode : reservationChargesMap.keySet()) {
				ReservationChargeDetail chargeDetail = reservationChargesMap.get(chargeCode);

				if (chargeDetail != null) {
					String description = chargeCode;
					if (EXTERNAL_CHARGES.SEAT_MAP.toString().equalsIgnoreCase(chargeCode)) {
						description = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblSeatSelection",
								language);
					}
					if (EXTERNAL_CHARGES.MEAL.toString().equalsIgnoreCase(chargeCode)) {
						description = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblMealSelection",
								language);
					}
					if (EXTERNAL_CHARGES.BAGGAGE.toString().equalsIgnoreCase(chargeCode)) {
						description = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblBaggageSelection",
								language);
					}

					if (EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString().equalsIgnoreCase(chargeCode)) {
						description = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblSSRSelection", language);

					}
					if (EXTERNAL_CHARGES.AIRPORT_SERVICE.toString().equalsIgnoreCase(chargeCode)) {
						description = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblHalaSelection",
								language);
					}
					if (EXTERNAL_CHARGES.AIRPORT_TRANSFER.toString().equalsIgnoreCase(chargeCode)) {
						description = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblAirportTransfer",
								language);
					}

					if (EXTERNAL_CHARGES.INSURANCE.toString().equalsIgnoreCase(chargeCode)) {
						description = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblInsurance", language);
					}
					
					if (EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.toString().equalsIgnoreCase(chargeCode)) {
						description = LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblAutoCheckinSelection",
								language);
					}

					chargeDetail.setDescription(description);
				}
			}

		}
	}

	private void addChargeDetailsToMap(LCCClientExternalChgDTO charge,
			Map<String, ReservationChargeDetail> reservationChargesMap) {
		if (charge.getAmount() != null && charge.getAmount().doubleValue() > 0) {
			String chargeCode = charge.getExternalCharges().toString();
			ReservationChargeDetail chargeDetail = null;

			if (reservationChargesMap.get(chargeCode) == null) {
				chargeDetail = new ReservationChargeDetail();
			} else {
				chargeDetail = reservationChargesMap.get(chargeCode);
			}

			chargeDetail.addChargeAmount(charge.getAmount());
			chargeDetail.setChargeCode(chargeCode);
			chargeDetail.setChargeGroup(PricingConstants.ChargeGroups.SURCHARGE);
			chargeDetail.setDescription(chargeCode);

			reservationChargesMap.put(chargeCode, chargeDetail);
		}
	}

	private void includeRequotedOtherExtraCharges(FareInfo fareInfo, List<ReservationChargeDetail> chargeDetails) {

		if (fareInfo != null) {
			if (chargeDetails == null) {
				chargeDetails = new ArrayList<>();
			}
			String modificationCharge = fareInfo.getModifyCharge();
			BigDecimal creditCardFee = fareInfo.getCreditCardFee();
			String cancelReservationCharge = fareInfo.getCancelReservationCharge();

			if (!StringUtil.isNullOrEmpty(modificationCharge)) {

				BigDecimal modChargeAmount = AccelAeroCalculator.getTwoScaledBigDecimalFromString(modificationCharge);

				if (modChargeAmount != null && modChargeAmount.doubleValue() > 0) {
					ReservationChargeDetail chargeDetail = new ReservationChargeDetail();

					chargeDetail.setChargeAmount(modChargeAmount);
					chargeDetail.setChargeCode(PricingConstants.ChargeGroups.MODIFICATION_CHARGE);
					chargeDetail.setChargeGroup(PricingConstants.ChargeGroups.MODIFICATION_CHARGE);
					chargeDetail.setDescription("Modification Charge");
					chargeDetails.add(chargeDetail);
				}

			}

			if (!StringUtil.isNullOrEmpty(cancelReservationCharge)) {

				BigDecimal cancelChargeAmount = AccelAeroCalculator.getTwoScaledBigDecimalFromString(cancelReservationCharge);

				if (cancelChargeAmount != null && cancelChargeAmount.doubleValue() > 0) {
					ReservationChargeDetail chargeDetail = new ReservationChargeDetail();

					chargeDetail.setChargeAmount(cancelChargeAmount);
					chargeDetail.setChargeCode(PricingConstants.ChargeGroups.CANCELLATION_CHARGE);
					chargeDetail.setChargeGroup(PricingConstants.ChargeGroups.CANCELLATION_CHARGE);
					chargeDetail.setDescription("Cancel Reservation Charge");
					chargeDetails.add(chargeDetail);
				}

			}

			if (creditCardFee != null && creditCardFee.doubleValue() > 0) {

				BigDecimal cancelChargeAmount = AccelAeroCalculator.getTwoScaledBigDecimalFromString(cancelReservationCharge);

				if (cancelChargeAmount.doubleValue() > 0) {
					ReservationChargeDetail chargeDetail = new ReservationChargeDetail();

					chargeDetail.setChargeAmount(creditCardFee);
					chargeDetail.setChargeCode(PricingConstants.ChargeGroups.THIRDPARTY_SURCHARGES);
					chargeDetail.setChargeGroup("Credit Card");
					chargeDetail.setDescription("Credit Card Fee");
					chargeDetails.add(chargeDetail);
				}

			}

		}

	}

}
