package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Collection;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedAncillaryType;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;

public class LoadReservationRS extends TransactionalBaseRS {

	private BalanceSummary balanceSummary;

	// Basic reservation information, a summary of the reservation
	private BookingInfoTO bookingInfoTO;

	// passenger details of the created reservation
	private List<Passenger> passengers;

	// Reservation modification parameters can modify/cancel etc
	private ModificationParams modificationParams;

	// Reservation segments of the reservation
	private List<ReservationSegment> reservationSegments;

	private List<FlexiInfo> flexiInfoList;

	// contact details of the reservation
	private ReservationContact contactInfo;

	// The alerts of types FLEXI ,GROUND_SERVICE , TRANSFER_ALERT, OPEN_RETURN
	private List<ReservationAlert> reservationAlerts;
	
	//selected ancillaries with price Quotations for whole reservation
	private List<PricedAncillaryType> reservationWiseSelectedAncillary;
	
	// pax ticket coupon information
	private Collection<PaxTicketCoupons> paxTicketCoupons;

	// Applied Promotion information
	private PromoInfoRS promoInfo;

	private PaymentDetails paymentDetails;

	private List<Advertisement> advertisements;

	private MetaData metaData;
	
	private boolean successfullRefund;
	
	private boolean isTaxInvoiceGenerated;
	
	private String reservationType;

	public BalanceSummary getBalanceSummary() {
		return balanceSummary;
	}

	public void setBalanceSummary(BalanceSummary balanceSummary) {
		this.balanceSummary = balanceSummary;
	}

	public BookingInfoTO getBookingInfoTO() {
		return bookingInfoTO;
	}

	public void setBookingInfoTO(BookingInfoTO bookingInfoTO) {
		this.bookingInfoTO = bookingInfoTO;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	public ModificationParams getModificationParams() {
		return modificationParams;
	}

	public void setModificationParams(ModificationParams modificationParams) {
		this.modificationParams = modificationParams;
	}

	public List<FlexiInfo> getFlexiInfoList() {
		return flexiInfoList;
	}

	public void setFlexiInfoList(List<FlexiInfo> flexiInfoList) {
		this.flexiInfoList = flexiInfoList;
	}

	public ReservationContact getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ReservationContact contactInfo) {
		this.contactInfo = contactInfo;
	}

	public List<ReservationAlert> getReservationAlerts() {
		return reservationAlerts;
	}

	public void setReservationAlerts(List<ReservationAlert> reservationAlerts) {
		this.reservationAlerts = reservationAlerts;
	}

	public List<ReservationSegment> getReservationSegments() {
		return reservationSegments;
	}

	public void setReservationSegments(List<ReservationSegment> reservationSegments) {
		this.reservationSegments = reservationSegments;
	}

	public List<PricedAncillaryType> getReservationWiseSelectedAncillary() {
		return reservationWiseSelectedAncillary;
	}

	public void setReservationWiseSelectedAncillary(List<PricedAncillaryType> reservationWiseSelectedAncillary) {
		this.reservationWiseSelectedAncillary = reservationWiseSelectedAncillary;
	}

	public MetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}

	public Collection<PaxTicketCoupons> getPaxTicketCoupons() {
		return paxTicketCoupons;
	}

	public void setPaxTicketCoupons(Collection<PaxTicketCoupons> paxTicketCoupons) {
		this.paxTicketCoupons = paxTicketCoupons;
	}

	public boolean isSuccessfullRefund() {
		return successfullRefund;
	}

	public void setSuccessfullRefund(boolean successfullRefund) {
		this.successfullRefund = successfullRefund;
	}

	public List<Advertisement> getAdvertisements() {
		return advertisements;
	}

	public void setAdvertisements(List<Advertisement> advertisements) {
		this.advertisements = advertisements;
	}

	public PromoInfoRS getPromoInfo() {
		return promoInfo;
	}

	public void setPromoInfo(PromoInfoRS promoInfo) {
		this.promoInfo = promoInfo;
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public boolean isTaxInvoiceGenerated() {
		return isTaxInvoiceGenerated;
	}

	public void setTaxInvoiceGenerated(boolean isTaxInvoiceGenerated) {
		this.isTaxInvoiceGenerated = isTaxInvoiceGenerated;
	}

	public String getReservationType() {
		return reservationType;
	}

	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}
}
