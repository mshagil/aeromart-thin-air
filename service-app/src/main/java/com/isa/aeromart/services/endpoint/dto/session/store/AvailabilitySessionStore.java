package com.isa.aeromart.services.endpoint.dto.session.store;

import java.util.List;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.BookingTransaction;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;

public interface AvailabilitySessionStore {

	public static String SESSION_KEY = BookingTransaction.SESSION_KEY;

	public void storePriceInventoryInfo(FareSegChargeTO pricingInformation);

	public void storeSelectedSegments(List<FlightSegmentTO> segments);

	public void storePreferences(PreferenceInfo preferenceInfo);

	public RPHGenerator getRPHGenerator();

	public void storeTravellerQuantity(TravellerQuantity travellerQuantity);

	public void storeDiscountDetails(DiscountedFareDetails discountDetails);

	public void storeReservationDiscountDTO(ReservationDiscountDTO reservationDiscountDTO);

	public void storeApplicableServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes);

	public void storeSessionFlexiDetails(List<SessionFlexiDetail> sessionFlexiDetailList);

	public List<SessionFlexiDetail> getSessionFlexiDetails();

	public void storeResInfo(ReservationInfo resInfo);
}
