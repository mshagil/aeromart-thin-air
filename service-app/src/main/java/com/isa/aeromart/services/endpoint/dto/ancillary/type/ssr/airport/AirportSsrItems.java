package com.isa.aeromart.services.endpoint.dto.ancillary.type.ssr.airport;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryItemLeaf;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class AirportSsrItems extends AncillaryItemLeaf {

	 private List<AirportSsrItem> items;

    public AirportSsrItems() {
        setType(AncillariesConstants.Type.SSR_AIRPORT);
    }

    public List<AirportSsrItem> getItems() {
		return items;
	}

	public void setItems(List<AirportSsrItem> items) {
		this.items = items;
	}
	
}
