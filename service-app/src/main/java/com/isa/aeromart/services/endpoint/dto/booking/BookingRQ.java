package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Date;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.ServiceTaxRQ;

public class BookingRQ extends ServiceTaxRQ {

	private List<Passenger> paxInfo;

	private ReservationContact reservationContact;

	private MakePaymentRQ makePaymentRQ;

	private List<String> flightSegmentRPHList;

	private boolean createOnhold;

	private Date onholdReleaseZuluTimeStamp;
	
	private String captcha;

	public List<Passenger> getPaxInfo() {
		return paxInfo;
	}

	public void setPaxInfo(List<Passenger> paxInfo) {
		this.paxInfo = paxInfo;
	}

	public ReservationContact getReservationContact() {
		return reservationContact;
	}

	public void setReservationContact(ReservationContact reservationContact) {
		this.reservationContact = reservationContact;
	}

	public List<String> getFlightSegmentRPHList() {
		return flightSegmentRPHList;
	}

	public void setFlightSegmentRPHList(List<String> flightSegmentRPHList) {
		this.flightSegmentRPHList = flightSegmentRPHList;
	}

	public MakePaymentRQ getMakePaymentRQ() {
		return makePaymentRQ;
	}

	public void setMakePaymentRQ(MakePaymentRQ makePaymentRQ) {
		this.makePaymentRQ = makePaymentRQ;
	}

	public boolean isCreateOnhold() {
		return createOnhold;
	}

	public Date getOnholdReleaseZuluTimeStamp() {
		return onholdReleaseZuluTimeStamp;
	}

	public void setCreateOnhold(boolean createOnhold) {
		this.createOnhold = createOnhold;
	}

	public void setOnholdReleaseZuluTimeStamp(Date onholdReleaseZuluTimeStamp) {
		this.onholdReleaseZuluTimeStamp = onholdReleaseZuluTimeStamp;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

}
