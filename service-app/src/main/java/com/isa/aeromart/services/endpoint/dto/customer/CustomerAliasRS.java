package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;

public class CustomerAliasRS extends TransactionalBaseRS {

	private List<CustomerAlias> customerAliasList;

	private String message;

	public List<CustomerAlias> getCustomerAliasList() {
		return customerAliasList;
	}

	public void setCustomerAliasList(List<CustomerAlias> customerAliasList) {
		this.customerAliasList = customerAliasList;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
