package com.isa.aeromart.services.endpoint.service;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.isa.thinair.platform.core.controller.ModuleFramework;

public class StartupServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		ModuleFramework.startup();
	}

}
