package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;

/** only used in paymentSummaryRes */
public class AdditionalServices {

	private BigDecimal seat;
	private BigDecimal meal;
	private BigDecimal baggage;
	private BigDecimal insurance;
	private BigDecimal inflightService;
	private BigDecimal airportService;
	private BigDecimal flexi;

	public BigDecimal getSeat() {
		return seat;
	}

	public void setSeat(BigDecimal seat) {
		this.seat = seat;
	}

	public BigDecimal getMeal() {
		return meal;
	}

	public void setMeal(BigDecimal meal) {
		this.meal = meal;
	}

	public BigDecimal getBaggage() {
		return baggage;
	}

	public void setBaggage(BigDecimal baggage) {
		this.baggage = baggage;
	}

	public BigDecimal getInsurance() {
		return insurance;
	}

	public void setInsurance(BigDecimal insurance) {
		this.insurance = insurance;
	}

	public BigDecimal getInflightService() {
		return inflightService;
	}

	public void setInflightService(BigDecimal inflightService) {
		this.inflightService = inflightService;
	}

	public BigDecimal getAirportService() {
		return airportService;
	}

	public void setAirportService(BigDecimal airportService) {
		this.airportService = airportService;
	}

	public BigDecimal getFlexi() {
		return flexi;
	}

	public void setFlexi(BigDecimal flexi) {
		this.flexi = flexi;
	}

}
