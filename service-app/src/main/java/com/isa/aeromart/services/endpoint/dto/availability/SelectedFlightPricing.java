package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.PromoInfo;
import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class SelectedFlightPricing {

	private List<FareRule> fareRules;

	@CurrencyValue
	private Total total;

	private List<OndOWPricing> ondOWPricing;

	private List<Surcharge> surcharges;

	private List<Tax> taxes;

	private BigDecimal serviceTaxAmountForSegmentFares = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal serviceTaxAmountForAdminFee = AccelAeroCalculator.getDefaultBigDecimalZero();

	private List<PromoInfo> promoInfo;
	
	@CurrencyValue
	private BigDecimal administrationFee;

	public List<FareRule> getFareRules() {
		if (fareRules == null) {
			fareRules = new ArrayList<FareRule>();
		}
		return fareRules;
	}

	public void setFareRules(List<FareRule> fareRules) {
		this.fareRules = fareRules;
	}

	public Total getTotal() {
		return total;
	}

	public void setTotal(Total total) {
		this.total = total;
	}

	public List<OndOWPricing> getOndOWPricing() {
		if (ondOWPricing == null) {
			ondOWPricing = new ArrayList<OndOWPricing>();
		}
		return ondOWPricing;
	}

	public void setOndOWPricing(List<OndOWPricing> ondOWPricing) {
		this.ondOWPricing = ondOWPricing;
	}

	public List<Surcharge> getSurcharges() {
		if (surcharges == null) {
			surcharges = new ArrayList<Surcharge>();
		}
		return surcharges;
	}

	public void setSurcharges(List<Surcharge> surcharges) {
		this.surcharges = surcharges;
	}

	public List<Tax> getTaxes() {
		if (taxes == null) {
			taxes = new ArrayList<Tax>();
		}
		return taxes;
	}

	public void setTaxes(List<Tax> taxes) {
		this.taxes = taxes;
	}

	public List<PromoInfo> getPromoInfo() {
		if (promoInfo == null) {
			promoInfo = new ArrayList<PromoInfo>();
		}
		return promoInfo;
	}

	public void setPromoInfo(List<PromoInfo> promoInfo) {
		this.promoInfo = promoInfo;
	}

	public BigDecimal getServiceTaxAmountForSegmentFares() {
		return serviceTaxAmountForSegmentFares;
	}

	public void setServiceTaxAmountForSegmentFares(BigDecimal serviceTaxAmount) {
		this.serviceTaxAmountForSegmentFares = serviceTaxAmount;
	}

	public BigDecimal getAdministrationFee() {
		return administrationFee;
	}

	public void setAdministrationFee(BigDecimal administrationFee) {
		this.administrationFee = administrationFee;
	}

	public BigDecimal getServiceTaxAmountForAdminFee() {
		return serviceTaxAmountForAdminFee;
	}

	public void setServiceTaxAmountForAdminFee(BigDecimal serviceTaxAmountForAdminFee) {
		this.serviceTaxAmountForAdminFee = serviceTaxAmountForAdminFee;
	}

}
