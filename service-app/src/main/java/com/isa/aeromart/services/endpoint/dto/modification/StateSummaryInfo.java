package com.isa.aeromart.services.endpoint.dto.modification;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class StateSummaryInfo {

	private BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal taxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal surchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal adjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal Discount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private ChargeSummaryInfo chargeInfo;

	private List<ChargeBreakdownTemplate> chargeBreakDown;

	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getSurchargeAmount() {
		return surchargeAmount;
	}

	public void setSurchargeAmount(BigDecimal surchargeAmount) {
		this.surchargeAmount = surchargeAmount;
	}

	public BigDecimal getAdjAmount() {
		return adjAmount;
	}

	public void setAdjAmount(BigDecimal adjAmount) {
		this.adjAmount = adjAmount;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getDiscount() {
		return Discount;
	}

	public void setDiscount(BigDecimal discount) {
		Discount = discount;
	}

	public ChargeSummaryInfo getChargeInfo() {
		return chargeInfo;
	}

	public void setChargeInfo(ChargeSummaryInfo chargeInfo) {
		this.chargeInfo = chargeInfo;
	}

	public List<ChargeBreakdownTemplate> getChargeBreakDown() {
		if(chargeBreakDown == null){
			chargeBreakDown = new ArrayList<>();
		}
		return chargeBreakDown;
	}

	public void setChargeBreakDown(List<ChargeBreakdownTemplate> chargeBrakDown) {
		this.chargeBreakDown = chargeBrakDown;
	}
}
