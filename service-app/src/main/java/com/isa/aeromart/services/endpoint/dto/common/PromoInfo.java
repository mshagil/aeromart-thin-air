package com.isa.aeromart.services.endpoint.dto.common;

import java.util.List;
import java.util.Set;

public class PromoInfo extends PromoSummary {

	private TravellerQuantity applyTo;

	private List<String> applicableAncillaries;

	private Set<String> applicableOnds;

	public TravellerQuantity getApplyTo() {
		return applyTo;
	}

	public void setApplyTo(TravellerQuantity applyTo) {
		this.applyTo = applyTo;
	}

	public List<String> getApplicableAncillaries() {
		return applicableAncillaries;
	}

	public void setApplicableAncillaries(List<String> applicableAncillaries) {
		this.applicableAncillaries = applicableAncillaries;
	}

	public Set<String> getApplicableOnds() {
		return applicableOnds;
	}

	public void setApplicableOnds(Set<String> applicableOnds) {
		this.applicableOnds = applicableOnds;
	}

}
