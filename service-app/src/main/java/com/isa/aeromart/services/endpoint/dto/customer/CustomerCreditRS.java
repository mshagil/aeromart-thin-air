package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Set;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;


public class CustomerCreditRS extends TransactionalBaseRS{

	private Set<CustomerCredit> customerCreditList;

	public Set<CustomerCredit> getCustomerCreditList() {
		return customerCreditList;
	}

	public void setCustomerCreditList(Set<CustomerCredit> customerCreditList) {
		this.customerCreditList = customerCreditList;
	}
	
}
