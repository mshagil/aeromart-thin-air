package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.IPGTransactionResult;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;

public class IPGTransactionRSAdaptor implements Adaptor<IPGTransactionResultDTO, IPGTransactionResult> {

	@Override
	public IPGTransactionResult adapt(IPGTransactionResultDTO transactionResult) {
		IPGTransactionResult ipgTransactionResult = null;
		if (transactionResult.isShowTransactionDetail()) {
			ipgTransactionResult = new IPGTransactionResult();
			ipgTransactionResult.setAmount(transactionResult.getAmount());
			ipgTransactionResult.setCurrency(transactionResult.getCurrency());
			ipgTransactionResult.setMerchantTrackID(transactionResult.getMerchantTrackID());
			ipgTransactionResult.setReferenceID(transactionResult.getReferenceID());
			ipgTransactionResult.setTransactionStatus(transactionResult.getTransactionStatus());
			ipgTransactionResult.setTransactionTime(transactionResult.getTransactionTime());
		}
		return ipgTransactionResult;
	}

}
