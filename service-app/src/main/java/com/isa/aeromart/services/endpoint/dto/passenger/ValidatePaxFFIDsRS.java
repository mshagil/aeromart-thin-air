package com.isa.aeromart.services.endpoint.dto.passenger;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class ValidatePaxFFIDsRS extends TransactionalBaseRS {

	private List<PaxValidationDetails> paxValidationList;

	public List<PaxValidationDetails> getPaxValidationList() {
		return paxValidationList;
	}

	public void setPaxValidationList(List<PaxValidationDetails> paxValidationList) {
		this.paxValidationList = paxValidationList;
	}

}
