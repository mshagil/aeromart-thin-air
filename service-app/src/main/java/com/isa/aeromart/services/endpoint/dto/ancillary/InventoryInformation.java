package com.isa.aeromart.services.endpoint.dto.ancillary;

public class InventoryInformation {
 
	private boolean isAvailable;

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
}
