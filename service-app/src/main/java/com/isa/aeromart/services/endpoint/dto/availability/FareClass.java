package com.isa.aeromart.services.endpoint.dto.availability;

import java.math.BigDecimal;
import java.util.Set;

import com.isa.aeromart.services.endpoint.constant.CommonConstants.FareClassType;
import com.isa.aeromart.services.endpoint.utils.common.CurrencyValue;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;

public class FareClass {

	// code generated to identify fare class type
	private String fareClassCode;

	private Integer fareClassId;

	private Integer rank;

	private String logicalCabinClass;

	private Set<String> freeServices;

	private String imageUrl;

	private String description;

	private String comment;

	private FareClassType fareClassType; // FLEXY,BUNDLE,NO_FLEXY

	private BundleFareDescriptionTemplateDTO templateDTO;

	@CurrencyValue
	private BigDecimal additionalFareFee = BigDecimal.ZERO;

	public String getFareClassCode() {
		return fareClassCode;
	}

	public void setFareClassCode(String fareClassCode) {
		this.fareClassCode = fareClassCode;
	}

	public Integer getFareClassId() {
		return fareClassId;
	}

	public void setFareClassId(Integer fareClassId) {
		this.fareClassId = fareClassId;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public Set<String> getFreeServices() {
		return freeServices;
	}

	public void setFreeServices(Set<String> freeServices) {
		this.freeServices = freeServices;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public FareClassType getFareClassType() {
		return fareClassType;
	}

	public void setFareClassType(FareClassType fareClassType) {
		this.fareClassType = fareClassType;
	}

	public BigDecimal getAdditionalFareFee() {
		return additionalFareFee;
	}

	public void setAdditionalFareFee(BigDecimal bundleFareFree) {
		this.additionalFareFee = bundleFareFree;
	}

	public BundleFareDescriptionTemplateDTO getTemplateDTO() {
		return templateDTO;
	}

	public void setTemplateDTO(BundleFareDescriptionTemplateDTO templateDTO) {
		this.templateDTO = templateDTO;
	}
}
