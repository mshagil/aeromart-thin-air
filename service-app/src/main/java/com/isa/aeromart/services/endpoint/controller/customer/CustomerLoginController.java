package com.isa.aeromart.services.endpoint.controller.customer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.isa.aeromart.services.endpoint.controller.common.StatefulController;

@Controller
@RequestMapping("customer")
public class CustomerLoginController extends StatefulController {

	// private static Log log = LogFactory.getLog(CustomerLoginController.class);
	// @ResponseBody
	// @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE },
	// produces = { MediaType.APPLICATION_JSON_VALUE })
	// public
	// CustomerLoginRes loginCustomer(@RequestBody CustomerLoginReq ajaxLoginReq) {
	// CustomerLoginRes ajaxLoginRes = new CustomerLoginRes();
	// ajaxLoginRes.setSuccess(true);
	// boolean loginSuccess = false;
	// AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
	// Customer customer = null;
	// try {
	// customer = customerDelegate.authenticate(ajaxLoginReq.getUserID(), ajaxLoginReq.getPassword());
	// } catch (ModuleException e) {
	// loginSuccess = false;
	// ajaxLoginRes.setSuccess(false);
	// log.error("LoginController ==> loginCustomer", e);
	// } catch (Exception e) {
	// loginSuccess = false;
	// ajaxLoginRes.setSuccess(false);
	// log.error("LoginController ==> loginCustomer", e);
	// }
	//
	// if (customer != null) {
	// loginSuccess = true;
	// LoggedInCustomerResAdaptor customerTransformer = new LoggedInCustomerResAdaptor();
	// LMSDetailsResAdaptor lmsDetailsResTransformer = new LMSDetailsResAdaptor();
	// ajaxLoginRes.setCustomer(customerTransformer.adapt(customer));
	// if (customer.getLMSMemberDetails() != null) {
	// ajaxLoginRes.setLmsDetails(lmsDetailsResTransformer.adapt(customer.getLMSMemberDetails()));
	// }
	// ajaxLoginRes.setSuccess(true);
	// } else {
	// loginSuccess = false;
	// ajaxLoginRes.setSuccess(true);
	// }
	// ajaxLoginRes.setLoginSuccess(loginSuccess);
	// return ajaxLoginRes;
	// }

}
