package com.isa.aeromart.services.endpoint.dto.masterdata;

public class IBELanguage {

	private String languageCode;
	
	private String language;

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
}
