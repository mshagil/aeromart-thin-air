package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class CustomerLoginRQ extends TransactionalBaseRQ {

	@NotNull(code = ValidationException.Code.NULL_LOGIN_REQUEST)
	private String customerID;
	@NotNull(code = ValidationException.Code.NULL_FORGOTPASS_REQUEST)
	private String password;
	
	private String userToken;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}
	
}
