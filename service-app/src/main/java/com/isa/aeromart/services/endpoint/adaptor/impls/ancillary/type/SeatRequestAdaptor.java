package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.SelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Selection;
import com.isa.aeromart.services.endpoint.dto.ancillary.assignee.PassengerAssignee;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.ancillary.scope.SegmentScope;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;

public class SeatRequestAdaptor extends AncillaryRequestAdaptor {

	public String getLCCAncillaryType() {
		return LCCAncillaryAvailabilityInDTO.LCCAncillaryType.SEAT_MAP;
	}

	public Object toAvailableAncillaries(TrackInfoDTO trackInfo, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount) {

		AvailableSeatRequestWrapper wrapper = new AvailableSeatRequestWrapper();

		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);
		flightSegmentTOs = AnciCommon.populateAnciOfferData(flightSegmentTOs, pricingInformation, travellerQuantity);

		LCCSeatMapDTO seatMapDTO = new LCCSeatMapDTO();
		seatMapDTO.setFlightDetails(flightSegmentTOs);
		seatMapDTO.setTransactionIdentifier(isModifyAnci? null : lccTransactionId != null ? lccTransactionId : "");
		seatMapDTO.setQueryingSystem(system); 
		seatMapDTO.setBundledFareDTOs(pricingInformation.getOndBundledFareDTOs()); 
		seatMapDTO.setPnr(pnr); 

		wrapper.setLccSeatMapDTO(seatMapDTO);
		wrapper.setSelectedLocale(LocaleContextHolder.getLocale().toString());
		wrapper.setTrackInfo(trackInfo);
		wrapper.setAppEngine(CommonServiceUtil.getApplicationEngine(trackInfo.getAppIndicator()));

		return wrapper;
	}

	public boolean blockSupported() {
		return true;
	}

	public Object toBlockAncillaries(List<SessionPassenger> passengers, RPHGenerator rphGenerator,
			List<InventoryWrapper> inventory, MetaData metaData, List<Preference> preferences, SYSTEM system,
			String lccTransactionId, TrackInfoDTO trackInfo) {
		OccupancySeatRequestWrapper wrapper = new OccupancySeatRequestWrapper();
		SegmentScope segmentScope = new SegmentScope();
		String flightSegmentRph;
		FlightSegmentTO flightSegmentTO;
		LCCSegmentSeatDTO lccSegmentSeatDTO;
		LCCAirSeatDTO lccAirSeatDTO;
		Map<String, LCCSegmentSeatDTO> segmentMap = new HashMap<String, LCCSegmentSeatDTO>();

		// setting seat dtos
		List<LCCSegmentSeatDTO> lccSegmentSeatDTOs = new ArrayList<LCCSegmentSeatDTO>();
		if (preferences != null) {
			for (Preference preference : preferences) {
				PassengerAssignee passenger = (PassengerAssignee) preference.getAssignee();
				for (Selection selection : preference.getSelections()) {

					segmentScope = (SegmentScope) selection.getScope();
					flightSegmentRph = rphGenerator.getUniqueIdentifier(segmentScope.getFlightSegmentRPH());

					if (!segmentMap.containsKey(flightSegmentRph)) {
						flightSegmentTO = AnciCommon.resolveInventory(inventory, flightSegmentRph).getFlightSegmentTO();
						lccSegmentSeatDTO = new LCCSegmentSeatDTO();
						lccSegmentSeatDTO.setFlightSegmentTO(flightSegmentTO);
						lccSegmentSeatDTO.setAirSeatDTOs(new ArrayList<LCCAirSeatDTO>());
						segmentMap.put(flightSegmentRph, lccSegmentSeatDTO);
					}

					lccSegmentSeatDTO = segmentMap.get(flightSegmentRph);

					for (SelectedItem selectedItem : selection.getSelectedItems()) {
						lccAirSeatDTO = new LCCAirSeatDTO();
						lccAirSeatDTO.setSeatNumber(selectedItem.getId());
						lccAirSeatDTO.setBookedPassengerType(AnciCommon.resolvePassengerType(passengers,
								passenger.getPassengerRph()));
						lccSegmentSeatDTO.getAirSeatDTOs().add(lccAirSeatDTO);
					}
				}
			}
		}

		for (LCCSegmentSeatDTO lccSegmentSeatDTORef : segmentMap.values()) {
			lccSegmentSeatDTOs.add(lccSegmentSeatDTORef);
		}

		wrapper.setBlockSeatDTOs(lccSegmentSeatDTOs);
		wrapper.setSystem(system);
		wrapper.setTrackInfo(trackInfo);
		wrapper.setTransactionIdentifier(lccTransactionId);

		return wrapper;
	}

	public Object toReleaseAncillaries(List<SessionPassenger> passengers, RPHGenerator rphGenerator, List<InventoryWrapper> inventory, MetaData metaData, List<Preference> preferences, SYSTEM system, String lccTransactionId, TrackInfoDTO trackInfo) {
		return toBlockAncillaries(passengers, rphGenerator, inventory, metaData, preferences, system, lccTransactionId, trackInfo);
	}

	public Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData) {

		List<SelectedItem> selectedItems;
		SegmentScope segmentScope;
		String flightSegmentRPH;

		LCCAirSeatDTO selectedSeat;
		List<LCCAirSeatDTO> selectedSeats;
		Map<String, List<LCCAirSeatDTO>> selectedSeatsMapping = new HashMap<>();

		for (Preference preference : preferences) {
			List<Selection> selections = preference.getSelections();
			for (Selection selection : selections) {
				segmentScope = (SegmentScope) selection.getScope();
				flightSegmentRPH = segmentScope.getFlightSegmentRPH();

				if (!selectedSeatsMapping.containsKey(flightSegmentRPH)) {
					selectedSeatsMapping.put(flightSegmentRPH, new ArrayList<>());
				}

				selectedSeats = selectedSeatsMapping.get(flightSegmentRPH);

				selectedItems = selection.getSelectedItems();
				for (SelectedItem selectedItem : selectedItems) {
					selectedSeat = new LCCAirSeatDTO();
					selectedSeat.setSeatNumber(selectedItem.getId());
					selectedSeats.add(selectedSeat);
				}

			}
		}

		return selectedSeatsMapping;
	}

	public Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData) {
		return null;
	}

	public Object toSaveAncillaries(AncillaryReservation ancillaryReservation) {
		return null;
	}

	public class AvailableSeatRequestWrapper {
		private LCCSeatMapDTO lccSeatMapDTO;
		private String selectedLocale;
		private BasicTrackInfo trackInfo;
		private ApplicationEngine appEngine;

		public LCCSeatMapDTO getLccSeatMapDTO() {
			return lccSeatMapDTO;
		}

		public void setLccSeatMapDTO(LCCSeatMapDTO lccSeatMapDTO) {
			this.lccSeatMapDTO = lccSeatMapDTO;
		}

		public String getSelectedLocale() {
			return selectedLocale;
		}

		public void setSelectedLocale(String selectedLocale) {
			this.selectedLocale = selectedLocale;
		}

		public BasicTrackInfo getTrackInfo() {
			return trackInfo;
		}

		public void setTrackInfo(BasicTrackInfo trackInfo) {
			this.trackInfo = trackInfo;
		}

		public ApplicationEngine getAppEngine() {
			return appEngine;
		}

		public void setAppEngine(ApplicationEngine appEngine) {
			this.appEngine = appEngine;
		}
	}

	public class OccupancySeatRequestWrapper {
		private List<LCCSegmentSeatDTO> blockSeatDTOs;
		private String transactionIdentifier;
		private ProxyConstants.SYSTEM system;
		private BasicTrackInfo trackInfo;

		public List<LCCSegmentSeatDTO> getBlockSeatDTOs() {
			return blockSeatDTOs;
		}

		public void setBlockSeatDTOs(List<LCCSegmentSeatDTO> blockSeatDTOs) {
			this.blockSeatDTOs = blockSeatDTOs;
		}

		public String getTransactionIdentifier() {
			return transactionIdentifier;
		}

		public void setTransactionIdentifier(String transactionIdentifier) {
			this.transactionIdentifier = transactionIdentifier;
		}

		public ProxyConstants.SYSTEM getSystem() {
			return system;
		}

		public void setSystem(ProxyConstants.SYSTEM system) {
			this.system = system;
		}

		public BasicTrackInfo getTrackInfo() {
			return trackInfo;
		}

		public void setTrackInfo(BasicTrackInfo trackInfo) {
			this.trackInfo = trackInfo;
		}
	}
}
