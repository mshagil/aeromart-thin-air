package com.isa.aeromart.services.endpoint.dto.ancillary.ancillaryinput;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryInput;
import com.isa.thinair.commons.api.constants.AncillariesConstants;

public class InFlightSsrInput extends AncillaryInput {

    private String comment;

    public InFlightSsrInput() {
        setAncillaryType(AncillariesConstants.Type.SSR_IN_FLIGHT);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
