package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

public class MultipleSelections extends BasicRuleValidation {

    private boolean multipleSelect;

    public MultipleSelections() {
        setRuleCode(RuleCode.MULTIPLE_SELECTIONS);
    }

    public boolean isMultipleSelect() {
        return multipleSelect;
    }

    public void setMultipleSelect(boolean multipleSelect) {
        this.multipleSelect = multipleSelect;
    }
}
