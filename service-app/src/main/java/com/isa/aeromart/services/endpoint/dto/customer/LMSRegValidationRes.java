package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class LMSRegValidationRes extends BaseRS {

	private boolean registeredCustomer = false;

	private boolean lmsNameMatch = false;

	private boolean validHeadFFID = false;

	private boolean validRefferdFFID = false;

	public boolean isRegisteredCustomer() {
		return registeredCustomer;
	}

	public void setRegisteredCustomer(boolean registeredCustomer) {
		this.registeredCustomer = registeredCustomer;
	}

	public boolean isLmsNameMatch() {
		return lmsNameMatch;
	}

	public void setLmsNameMatch(boolean lmsNameMatch) {
		this.lmsNameMatch = lmsNameMatch;
	}

	public boolean isValidFamilyHeadEmail() {
		return validHeadFFID;
	}

	public void setValidFamilyHeadEmail(boolean validHeadFFID) {
		this.validHeadFFID = validHeadFFID;
	}

	public boolean isValidRefferdEmail() {
		return validRefferdFFID;
	}

	public void setValidRefferdEmail(boolean validRefferdFFID) {
		this.validRefferdFFID = validRefferdFFID;
	}

}
