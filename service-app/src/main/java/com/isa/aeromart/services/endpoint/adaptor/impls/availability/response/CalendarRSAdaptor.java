package com.isa.aeromart.services.endpoint.adaptor.impls.availability.response;

import static com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils.adaptCollection;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.OriginDestinationRSAdaptor;
import com.isa.aeromart.services.endpoint.dto.availability.FlightCalendarRS;
import com.isa.aeromart.services.endpoint.dto.availability.OriginDestinationRS;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class CalendarRSAdaptor implements Adaptor<BaseAvailRS, FlightCalendarRS> {

	private boolean withFare;
	private boolean calculateReturnDiscount;
	private RPHGenerator rphGenerator;
	private TravellerQuantity travellerQuantity;

	public CalendarRSAdaptor(boolean withFare, boolean calculateReturnDiscount, RPHGenerator rphGenerator,
			TravellerQuantity travellerQuantity) {
		this.withFare = withFare;
		this.calculateReturnDiscount = calculateReturnDiscount;
		this.rphGenerator = rphGenerator;
		this.travellerQuantity = travellerQuantity;
	}

	@Override
	public FlightCalendarRS adapt(BaseAvailRS source) {
		FlightCalendarRS response = new FlightCalendarRS();
		if (source != null) {
			replaceReturnFlexiFares(source);
			response.getOriginDestinationResponse().addAll(
					adaptOriginDestinationResponse(source.getOriginDestinationInformationList(),
							calculateInboundPerAdultFlexiCharge(source)));
			response.setTransactionId(source.getTransactionIdentifier());
			response.setSuccess(true);
		}
		return response;
	}

	private List<OriginDestinationRS> adaptOriginDestinationResponse(
			List<OriginDestinationInformationTO> originDestinationInfoList, BigDecimal inboundAdultFlexiCharge) {
		List<OriginDestinationRS> originDestinationResponses = new ArrayList<OriginDestinationRS>();
		adaptCollection(originDestinationInfoList, originDestinationResponses,
				getAdaptor(inboundAdultFlexiCharge, travellerQuantity));
		return originDestinationResponses;
	}

	private Adaptor<OriginDestinationInformationTO, OriginDestinationRS> getAdaptor(BigDecimal inboundAdultFlexiCharge,
			TravellerQuantity travellerQuantity) {
		if (withFare) {
			if (calculateReturnDiscount) {
				return OriginDestinationRSAdaptor.getAdaptorForReturnFareResponse(rphGenerator, inboundAdultFlexiCharge,
						travellerQuantity);
			} else {
				return OriginDestinationRSAdaptor.getAdaptorForFareResponse(rphGenerator);
			}
		} else {
			return OriginDestinationRSAdaptor.getAdaptorForFlightResponse(rphGenerator);
		}
	}

	public static CalendarRSAdaptor getAdaptorForRetunFareResponse(BaseAvailRS availRS, RPHGenerator rphGenerator,
			TravellerQuantity travellerQuantity) {
		return new CalendarRSAdaptor(true, availRS != null && availRS.getOriginDestinationInformationList().size() == 2,
				rphGenerator, travellerQuantity);
	}

	public static CalendarRSAdaptor getAdaptorForFareResponse(RPHGenerator rphGenerator) {
		return new CalendarRSAdaptor(true, false, rphGenerator, null);
	}

	public static CalendarRSAdaptor getAdaptorForFlightResponse(RPHGenerator rphGenerator) {
		return new CalendarRSAdaptor(false, false, rphGenerator, null);
	}

	public static BigDecimal calculateInboundPerAdultFlexiCharge(BaseAvailRS source) {
		BigDecimal perPaxFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (source != null && source.getSelectedPriceFlightInfo() != null
				&& source.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.ADULT) != null) {
			PerPaxPriceInfoTO perPaxPrice = source.getSelectedPriceFlightInfo().getPerPaxPriceInfoTO(PaxTypeTO.ADULT);
			if (perPaxPrice.getPassengerPrice() != null
					&& perPaxPrice.getPassengerPrice().getOndExternalCharge(OndSequence.IN_BOUND) != null) {
				ONDExternalChargeTO outBoundExternalCharge = perPaxPrice.getPassengerPrice().getOndExternalCharge(
						OndSequence.IN_BOUND);
				for (ExternalChargeTO externalCharge : outBoundExternalCharge.getExternalCharges()) {
					if (externalCharge.getType().equals(ReservationInternalConstants.EXTERNAL_CHARGES.FLEXI_CHARGES)) {
						perPaxFlexiCharge = AccelAeroCalculator.add(perPaxFlexiCharge, externalCharge.getAmount());
					}
				}
			}
		}
		return perPaxFlexiCharge;
	}

	public static void replaceReturnFlexiFares(BaseAvailRS source) {
		if (isReturnSearch(source.getOriginDestinationInformationList())) {
			if (source.getSelectedPriceFlightInfo() != null
					&& source.getSelectedPriceFlightInfo().getAvailableLogicalCCList() != null) {
				List<OndClassOfServiceSummeryTO> availableLogicalCCList = source.getSelectedPriceFlightInfo()
						.getAvailableLogicalCCList();
				boolean hasOutboundFlexi = false;
				boolean hasInboundFlexi = false;
				if (availableLogicalCCList != null && availableLogicalCCList.size() == OndSequence.RETURN_OND_SIZE) {
					hasOutboundFlexi = isOndFlexiFareAvailable(availableLogicalCCList.get(OndSequence.OUT_BOUND));
					hasInboundFlexi = isOndFlexiFareAvailable(availableLogicalCCList.get(OndSequence.IN_BOUND));
					removeSegOptionUnAvailFlexi(source.getOriginDestinationInformationList().get(OndSequence.OUT_BOUND),
							hasOutboundFlexi);
					removeSegOptionUnAvailFlexi(source.getOriginDestinationInformationList().get(OndSequence.IN_BOUND),
							hasInboundFlexi);
				}
			}
		}
	}

	public static boolean isReturnSearch(List<OriginDestinationInformationTO> ondList) {
		boolean isReturnSearch = false;
		if (ondList != null && ondList.size() == OndSequence.RETURN_OND_SIZE) {
			OriginDestinationInformationTO outBoundOnd = ondList.get(OndSequence.OUT_BOUND);
			OriginDestinationInformationTO inboundOnd = ondList.get(OndSequence.IN_BOUND);
			if (outBoundOnd.getOrigin().equals(inboundOnd.getDestination())
					&& outBoundOnd.getDestination().equals(inboundOnd.getOrigin())) {
				isReturnSearch = true;
			}
		}
		return isReturnSearch;
	}

	public static boolean isOndFlexiFareAvailable(OndClassOfServiceSummeryTO ondFareList) {
		boolean isFlexiAvail = false;
		if (ondFareList != null && ondFareList.getAvailableLogicalCCList() != null) {
			for (LogicalCabinClassInfoTO logicalCCInfoTO : ondFareList.getAvailableLogicalCCList()) {
				if (logicalCCInfoTO.isWithFlexi() && logicalCCInfoTO.getBundledFarePeriodId() == null) {
					isFlexiAvail = true;
					break;
				}
			}
		}
		return isFlexiAvail;
	}

	public static void removeSegOptionUnAvailFlexi(OriginDestinationInformationTO ondInfo, boolean hasFlexi) {
		if (!ondInfo.getOriginDestinationSegFareOptions().isEmpty() && !hasFlexi) {
			for (OriginDestinationOptionTO ondSegOpt : ondInfo.getOriginDestinationSegFareOptions()) {
				if (ondSegOpt.getFlightFareSummaryList() != null && !ondSegOpt.getFlightFareSummaryList().isEmpty()) {
					for (FlightFareSummaryTO fareSummary : ondSegOpt.getFlightFareSummaryList()) {
						if (fareSummary.getBundledFarePeriodId() == null && fareSummary.isWithFlexi()) {
							ondSegOpt.getFlightFareSummaryList().remove(fareSummary);
							break;
						}
					}
				}
			}
		}
	}
}
