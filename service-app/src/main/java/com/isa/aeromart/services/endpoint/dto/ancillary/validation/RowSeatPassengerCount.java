package com.isa.aeromart.services.endpoint.dto.ancillary.validation;

import java.util.Map;

public class RowSeatPassengerCount extends BasicRuleValidation {
	
	private Integer seatRowId;
	
	private Map<String, Integer> rowSeatPassengetCountMap;
	
	public RowSeatPassengerCount() {
		setApplicability(ApplicabilityCode.SEAT_ROW_WISE);
        setRuleCode(RuleCode.PAX_TYPE_COUNT_FOR_ROW_SEATS);
    }

	public Map<String, Integer> getRowSeatPassengetCountMap() {
		return rowSeatPassengetCountMap;
	}

	public void setRowSeatPassengetCountMap(Map<String, Integer> rowSeatPassengetCountMap) {
		this.rowSeatPassengetCountMap = rowSeatPassengetCountMap;
	}

	public Integer getSeatRowId() {
		return seatRowId;
	}

	public void setSeatRowId(Integer seatRowId) {
		this.seatRowId = seatRowId;
	}

}
