package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.availability.Preferences;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;

public class TravelerPreferenceAdaptor implements Adaptor<Preferences, TravelPreferencesTO> {

	TravelPreferencesTO travelPreferencesTO;

	public TravelerPreferenceAdaptor(TravelPreferencesTO travelPreferencesTO) {
		this.travelPreferencesTO = travelPreferencesTO;
	}

	@Override
	public TravelPreferencesTO adapt(Preferences source) {
		travelPreferencesTO.setBookingClassCode(source.getBookingCode());
		travelPreferencesTO.setBookingType(source.getSeatType());
		return travelPreferencesTO;
	}

}
