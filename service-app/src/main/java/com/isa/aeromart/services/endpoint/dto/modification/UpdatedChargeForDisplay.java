package com.isa.aeromart.services.endpoint.dto.modification;

import java.math.BigDecimal;
import java.util.List;

public class UpdatedChargeForDisplay {
	
	private BigDecimal creditToDisplay;
	
	private BigDecimal dueAmountToDisplay;
	
	private List<ChargeDescriptionInfo> chargeListToDisplay;

	/**
	 * @return the creditToDisplay
	 */
	public BigDecimal getCreditToDisplay() {
		return creditToDisplay;
	}

	/**
	 * @param creditToDisplay the creditToDisplay to set
	 */
	public void setCreditToDisplay(BigDecimal creditToDisplay) {
		this.creditToDisplay = creditToDisplay;
	}

	/**
	 * @return the dueAmountToDisplay
	 */
	public BigDecimal getDueAmountToDisplay() {
		return dueAmountToDisplay;
	}

	/**
	 * @param dueAmountToDisplay the dueAmountToDisplay to set
	 */
	public void setDueAmountToDisplay(BigDecimal dueAmountToDisplay) {
		this.dueAmountToDisplay = dueAmountToDisplay;
	}

	/**
	 * @return the chargeListToDisplay
	 */
	public List<ChargeDescriptionInfo> getChargeListToDisplay() {
		return chargeListToDisplay;
	}

	/**
	 * @param chargeListToDisplay the chargeListToDisplay to set
	 */
	public void setChargeListToDisplay(List<ChargeDescriptionInfo> chargeListToDisplay) {
		this.chargeListToDisplay = chargeListToDisplay;
	}
	
}
