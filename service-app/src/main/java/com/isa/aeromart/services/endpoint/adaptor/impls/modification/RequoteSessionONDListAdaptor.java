package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.availability.ModifiedFlight;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.RequoteONDInfo;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;

public class RequoteSessionONDListAdaptor implements Adaptor<List<OriginDestinationInformationTO>, List<RequoteONDInfo>> {

	private List<ModifiedFlight> modifiedFlights;
	private RPHGenerator rphGenerator;
	private LCCClientReservation reservation;

	public RequoteSessionONDListAdaptor(List<ModifiedFlight> modifiedFlights, RPHGenerator rphGenerator, LCCClientReservation reservation) {
		this.modifiedFlights = modifiedFlights;
		this.rphGenerator = rphGenerator;
		this.reservation = reservation;
		
	}

	@Override
	public List<RequoteONDInfo> adapt(List<OriginDestinationInformationTO> source) {

		List<RequoteONDInfo> requoteSessionONDs = new ArrayList<RequoteONDInfo>();
		for (OriginDestinationInformationTO ondInfo : source) {
			RequoteONDInfo requoteOND = new RequoteONDInfo();
			List<FareTO> oldPerPaxFareTOList = new ArrayList<FareTO>();
			if (ondInfo.getOldPerPaxFareTOList() != null) {
				oldPerPaxFareTOList.addAll(ondInfo.getOldPerPaxFareTOList());
				requoteOND.setOldPerPaxFareTOList(oldPerPaxFareTOList);
			}
			
			List<String> existingResSegRPHList = new ArrayList<String>();
			existingResSegRPHList.addAll(ondInfo.getExistingPnrSegRPHs());
			requoteOND.setExistingResSegRPHList(existingResSegRPHList);

			List<String> flightSegIDList = new ArrayList<String>();
			List<String> flightSegRPHlist = new ArrayList<String>();
			if (ondInfo.getOrignDestinationOptions() != null && !ondInfo.getOrignDestinationOptions().isEmpty()) {
				for (OriginDestinationOptionTO ondInfoTo : ondInfo.getOrignDestinationOptions()) {
					if (ondInfoTo.getFlightSegmentList() != null && !ondInfoTo.getFlightSegmentList().isEmpty()) {
						for (FlightSegmentTO fltSegTo : ondInfoTo.getFlightSegmentList()) {
							String[] fltSegData = fltSegTo.getFlightRefNumber().split("\\$");
							if (fltSegData.length > 2) {
								flightSegIDList.add(fltSegData[2]);
							}
							flightSegRPHlist.add(fltSegTo.getFlightRefNumber());
						}
					}
				}
			}
			requoteOND.setFlightSegIdList(flightSegIDList);
			requoteOND.setFlightSegRPHlist(flightSegRPHlist);
			requoteOND.setModifiedResSegList(getModifiedResRPHList(flightSegRPHlist));
			requoteSessionONDs.add(requoteOND);
		}
		return requoteSessionONDs;
	}

	private List<String> getModifiedResRPHList(List<String> flightSegRPHList) {
		List<String> modifiedResRPHList = new ArrayList<String>();
		if (modifiedFlights != null && !modifiedFlights.isEmpty()) {
			List<String> convertedRPHlist = new ArrayList<String>();
			for (ModifiedFlight modifiedFlight : modifiedFlights) {
				for (String uniqueFlightSegId : modifiedFlight.getToFlightSegRPH()) {
					convertedRPHlist.add(rphGenerator.getUniqueIdentifier(uniqueFlightSegId));
				}
				if (CollectionUtils.containsAny(flightSegRPHList, convertedRPHlist)) {
					modifiedResRPHList.addAll(ModificationUtils.getResSegUniqueIdentifierList(modifiedFlight.getFromResSegRPH(),
							reservation.getSegments()));
				}
			}
		}
		return modifiedResRPHList;
	}

}
