package com.isa.aeromart.services.endpoint.dto.session.store;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.payment.LoyaltyInformation;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.BookingTransaction;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.TotalPaymentInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.PassengerChargeTo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.PreferenceInfo;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;

public interface BookingSessionStore {
	public static String SESSION_KEY = BookingTransaction.SESSION_KEY;

	public List<SessionFlightSegment> getSegments();

	public FareSegChargeTO getFarePricingInformation();

	public TotalPaymentInfo getTotalPaymentInformation();

	public Collection<LCCClientExternalChgDTO> getPaymentExternalCharges();

	public ReservationDiscountDTO getReservationDiscountDTOForBooking(boolean isBinPromotion);

	public void storeBookingRQ(BookingRQ bookingRQ);

	public void storeOnHoldCreated(boolean flag);

	public boolean isOnHoldCreated();

	public PostPaymentDTO getPostPaymentInformation();

	public void storePnr(String pnr);

	public String getPnr();

	public void clearBookingDetails();

	public PreferenceInfo getPreferenceInfoForBooking();

	public void storeCaptchaText(String captcha);

	public String getCaptchaText();

	public LoyaltyInformation getLoyaltyInformation();

	public DiscountedFareDetails getDiscountedFareDetailsForBooking(boolean isBinPromotion);

	public BigDecimal getDiscountAmountForBooking(boolean isBinPromotion);

	public TravellerQuantity getTravellerQuantity();

	public List<SessionFlexiDetail> getSessionFlexiDetails();

	public List<String> getInvolvingCarrierCodes();

	public void storeOnholdReleaseTime(String onholdReleaseTime);

	public String getOnholdReleaseTime();

	public void storeLastName(String lastName);

	public String getLastName();

	public void storeFirstDepatureDate(Date firstDepatureDate);

	public Date getFirstDepatureDate();

	public List<PassengerChargeTo<LCCClientExternalChgDTO>> getPaxWisePaymentExternalCharges();

	public PayByVoucherInfo getPayByVoucherInfo();

	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo);

	public Map<String, String> getVoucherOTPMap();

}
