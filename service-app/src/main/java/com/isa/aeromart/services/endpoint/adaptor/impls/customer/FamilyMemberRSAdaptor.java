package com.isa.aeromart.services.endpoint.adaptor.impls.customer;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.dto.customer.FamilyMemberDTO;
import com.isa.thinair.aircustomer.api.model.FamilyMember;

public class FamilyMemberRSAdaptor implements Adaptor<FamilyMember, FamilyMemberDTO> {

	private RPHGenerator familyMemberIDHolder;

	public FamilyMemberRSAdaptor(RPHGenerator familyMemberIDHolder) {
		this.familyMemberIDHolder = familyMemberIDHolder;
	}

	@Override
	public FamilyMemberDTO adapt(FamilyMember familyMember) {
		FamilyMemberDTO familyMemberDTO = new FamilyMemberDTO();
		String firstName = familyMember.getFirstName();
		String lastName = familyMember.getLastName();
		familyMemberDTO.setFamilyMemberId(familyMemberIDHolder.getRPH(String.valueOf(familyMember.getFamilyMemberId())));
		familyMemberDTO.setTitle(familyMember.getTitle());
		familyMemberDTO.setFirstName(firstName.substring(0, 1).toUpperCase() + firstName.substring(1));
		familyMemberDTO.setLastName(lastName.substring(0, 1).toUpperCase() + lastName.substring(1));
		familyMemberDTO.setNationalityCode(familyMember.getNationalityCode());
		familyMemberDTO.setRelationshipId(familyMember.getRelationshipId());

		if (familyMember.getDateOfBirth() != null) {
			familyMemberDTO.setDateOfBirth(familyMember.getDateOfBirth());
		}

		return familyMemberDTO;
	}

}
