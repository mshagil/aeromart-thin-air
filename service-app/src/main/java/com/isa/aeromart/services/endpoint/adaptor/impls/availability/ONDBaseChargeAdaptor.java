package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import com.isa.aeromart.services.endpoint.dto.availability.OndOWPricing;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BasicChargeTO;

abstract class ONDBaseChargeAdaptor {
	private ONDPriceAdaptor ondAdaptor = null;

	public ONDBaseChargeAdaptor(ONDPriceAdaptor ondAdaptor) {
		this.ondAdaptor = ondAdaptor;
	}

	public PaxPrice getPaxPrice(String paxType, BasicChargeTO basicCharge) {
		return ondAdaptor.getPaxPrice(paxType, basicCharge);
	}

	public OndOWPricing getOndPrice(int ondSequence) {
		return ondAdaptor.getOndPrice(ondSequence);
	}

	public PaxPrice getPaxPrice(String paxType, BaseFareTO baseFare) {
		return ondAdaptor.getPaxPrice(paxType, baseFare);
	}

}