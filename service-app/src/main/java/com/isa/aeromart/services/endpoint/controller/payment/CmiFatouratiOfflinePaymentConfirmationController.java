	package com.isa.aeromart.services.endpoint.controller.payment;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;

import com.isa.aeromart.services.endpoint.constant.TapPaymentConfirmConstants;
import com.isa.aeromart.services.endpoint.controller.common.StatefulController;
import com.isa.aeromart.services.endpoint.delegate.payment.ConfirmPaymentService;
import com.isa.aeromart.services.endpoint.dto.payment.cmiFatourati.CmiFatouratiOfflinePaymentConfirmationRequest;
import com.isa.aeromart.services.endpoint.dto.payment.cmiFatourati.CmiFatouratiOfflinePaymentConfirmationResponse;
import com.isa.aeromart.services.endpoint.dto.payment.tapOffline.TapOfflinePaymentConfirmRequest;
import com.isa.aeromart.services.endpoint.dto.payment.tapOffline.TapOfflinePaymentConfirmResponse;
import com.isa.thinair.commons.api.exception.ModuleException;

	@Controller
	@RequestMapping(value = "cmifatourati", method = RequestMethod.POST)
	public class CmiFatouratiOfflinePaymentConfirmationController extends StatefulController {
		
		@Autowired
		private ConfirmPaymentService confirmPaymentService;
		@ResponseBody
		@RequestMapping(value = "/payment/confirm", method = RequestMethod.POST)
		public CmiFatouratiOfflinePaymentConfirmationResponse confirmPayment(@RequestBody CmiFatouratiOfflinePaymentConfirmationRequest confirmRequest){

			
			CmiFatouratiOfflinePaymentConfirmationResponse confirmResponse = confirmPaymentService.confirmCmiFatouratiOfflinePayment(confirmRequest, getTrackInfo());

			return confirmResponse;
		}

		

}
