package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class ExternalAgentLoginRQ extends TransactionalBaseRQ {

	@NotNull(code = ValidationException.Code.NULL_LOGIN_REQUEST)
	private String username;
	@NotNull(code = ValidationException.Code.NULL_FORGOTPASS_REQUEST)
	private String password;

	private String merchantId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

}
