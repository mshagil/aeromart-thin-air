package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.dto.common.BaseRS;

public class BlockRS extends BaseRS {

	private String blockReferenceId;

	private PaymentInfo paymentInfo;

	private PaymentInfo paymentInfoInAgentCurrency;

	public void setBlockReferenceId(String blockReferenceId) {
		this.blockReferenceId = blockReferenceId;
	}

	public String getBlockReferenceId() {
		return blockReferenceId;
	}

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public PaymentInfo getPaymentInfoInAgentCurrency() {
		return paymentInfoInAgentCurrency;
	}

	public void setPaymentInfoInAgentCurrency(PaymentInfo paymentInfoInAgentCurrency) {
		this.paymentInfoInAgentCurrency = paymentInfoInAgentCurrency;
	}

}
