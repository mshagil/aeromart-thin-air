package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationModificationParams;
import com.isa.aeromart.services.endpoint.dto.booking.SegmentModificationParams;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.PNRModificationAuthorizationUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;

public class ResModParamsAdaptor implements Adaptor<LCCClientReservation, ReservationModificationParams> {

	private boolean isRegUser = false;

	private boolean isGroupPnr;

	private boolean singleSegExist;
	
	private boolean cancellable;

	private boolean unCancelledUNsegExist;

	List<SegmentModificationParams> segmentModificationParams;

	public ResModParamsAdaptor(boolean isGroupPnr, boolean isRegUser, List<SegmentModificationParams> segmentModificationParams,
			boolean singleSegExist, boolean cancellable, boolean unCancelledUNsegExist) {
		this.isGroupPnr = isGroupPnr;
		this.isRegUser = isRegUser;
		this.segmentModificationParams = segmentModificationParams;
		this.singleSegExist = singleSegExist;
		this.cancellable = cancellable;
		this.unCancelledUNsegExist =unCancelledUNsegExist;
	}
	@Override
	public ReservationModificationParams adapt(LCCClientReservation reservation) {
		ReservationModificationParams modificationParams = new ReservationModificationParams();
		boolean isPartiallyFlown = ModificationUtils.isPartiallyFlownReservation(reservation.getSegments());
		boolean modifiable = reservation.isModifiableReservation();
		if (isGroupPnr) {
			cancellable = cancellable
					&& reservation.getInterlineModificationParams().contains(ModifcationParamTypes.CANCEL_ALLOW);
		} else {
			cancellable = cancellable && (isAllowCancelReservation(reservation) || (unCancelledUNsegExist && !isPartiallyFlown));
			if (isRegUser) {
				cancellable = cancellable && AppSysParamsUtil.isAllowIBECancellationServicesReservation();
			} else {
				cancellable = cancellable && AppSysParamsUtil.isUnregisterdUserCanCancelSegment();
				modifiable = modifiable && AppSysParamsUtil.isUnregisterdUserCanMofidyReservation();
			}
			if (cancellable) {
				cancellable = PNRModificationAuthorizationUtils.isSegmentModifiableAsPerETicketStatus(reservation, null);
			}
		}

		modificationParams.setCancellable(cancellable && modifiable);
		modificationParams.setModifiable(modifiable);
		return modificationParams;
	}

	private boolean isAllowCancelReservation(LCCClientReservation reservation) {
		boolean allowCancelReservation = true;
		List<String> cancelSegmentList =getCancelledSegmentRphList(reservation.getSegments());
		if (!singleSegExist) {
			for (SegmentModificationParams segModParam : segmentModificationParams) {
				if (!segModParam.isCancellable() && !cancelSegmentList.contains(segModParam.getReservationSegmentRPH())) {
					allowCancelReservation = false;
				}
			}
		} else {
			if (reservation.getLccPromotionInfoTO() != null) {
				allowCancelReservation = reservation.getLccPromotionInfoTO().isCancellationAllowed()
						&& !reservation.getSegments().iterator().next().isFlownSegment();
			}
		}
		return allowCancelReservation;
	}

	private List<String> getCancelledSegmentRphList(Set<LCCClientReservationSegment> segList){
		List<String> cancelSegmentList = new ArrayList<String>();
		for(LCCClientReservationSegment seg : segList){
			if(ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(seg.getStatus())){
				cancelSegmentList.add(ModificationUtils.createResSegRph(seg.getCarrierCode(),seg.getSegmentSeq()));
			}
		}
		return cancelSegmentList;
	}

}
