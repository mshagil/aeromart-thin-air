package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Date;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.payment.ExternalPGDetails;

public class BookingRS extends TransactionalBaseRS {

	public enum PaymentStatus {
		SUCCESS, PENDING, ERROR, NO_PAYMENT, ALREADY_COMPLETED
	}

	public enum ActionStatus {
		REDIRET_EXTERNAL, ONHOLD_CREATED_PAYMENT_FAILED, ONHOLD_CREATED_CASH, ONHOLD_CREATED_OFFLINE, BOOKING_CREATION_SUCCESS, BOOKING_CREATION_FAILED, BOOKING_MODIFICATION_SUCCESS, BOOKING_MODIFICATION_FAILED
	}

	private PaymentStatus paymentStatus;

	private ActionStatus actionStatus;

	private String pnr;

	private ExternalPGDetails externalPGDetails;

	private String onholdReleaseDuration;

	private IPGTransactionResult ipgTransactionResult;

	private Date firstDepartureDate;

	private String contactLastName;

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public ExternalPGDetails getExternalPGDetails() {
		return externalPGDetails;
	}

	public void setExternalPGDetails(ExternalPGDetails externalPGDetails) {
		this.externalPGDetails = externalPGDetails;
	}

	public String getOnholdReleaseDuration() {
		return onholdReleaseDuration;
	}

	public void setOnholdReleaseDuration(String onholdReleaseDuration) {
		this.onholdReleaseDuration = onholdReleaseDuration;
	}

	public IPGTransactionResult getIpgTransactionResult() {
		return ipgTransactionResult;
	}

	public void setIpgTransactionResult(IPGTransactionResult ipgTransactionResult) {
		this.ipgTransactionResult = ipgTransactionResult;
	}

	public Date getFirstDepartureDate() {
		return firstDepartureDate;
	}

	public void setFirstDepartureDate(Date firstDepartureDate) {
		this.firstDepartureDate = firstDepartureDate;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

}
