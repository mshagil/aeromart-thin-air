package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillaryRequestAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.BasicReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.ContactInformation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.Passenger;
import com.isa.aeromart.services.endpoint.dto.ancillary.Preference;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationData;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.TotalPaymentInfo;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAddressDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;

public class InsuranseRequestAdaptor extends AncillaryRequestAdaptor {
	@Override
	public String getLCCAncillaryType() {
		return LCCAncillaryAvailabilityInDTO.LCCAncillaryType.INSURANCE;
	}

	@Override
	public Object toAvailableAncillaries(TrackInfoDTO trackInfo, ReservationData reservationData, MetaData metaData,
			List<InventoryWrapper> inventory, List<InventoryWrapper> allInventory, FareSegChargeTO pricingInformation,
			TravellerQuantity travellerQuantity, SYSTEM system, String lccTransactionId, String pnr, boolean isModifyAnci,
			ReservationDiscountDTO resDiscountDTO, Map<String, Integer> serviceCount) {

		AvailableInsuranceRequestWrapper wrapper = new AvailableInsuranceRequestWrapper();
		BigDecimal totalTicketPriceWithoutExternal = null;
		List<FlightSegmentTO> filteredFlightSegmentTOs = new ArrayList<>();
		
		BasicReservation basicReservation = (BasicReservation) reservationData;

		List<Passenger> passengersList = basicReservation.getPassengers();
        if (passengersList == null) return null;

		ContactInformation contactInformation = basicReservation.getContactInfo();

		Collection<Passenger> adultList = new ArrayList<Passenger>();

		Collection<Passenger> childList = new ArrayList<Passenger>();

		Collection<Passenger> infantList = new ArrayList<Passenger>();

		Collection<Passenger> paxAdultChildList = new ArrayList<Passenger>();

		for (Passenger passenger : passengersList) {
			if (passenger.getType().equalsIgnoreCase(PaxTypeTO.ADULT)) {
				adultList.add(passenger);
			}
			if (passenger.getType().equalsIgnoreCase(PaxTypeTO.CHILD)) {
				childList.add(passenger);
			}
			if (passenger.getType().equalsIgnoreCase(PaxTypeTO.INFANT)) {
				infantList.add(passenger);
			}
		}
		paxAdultChildList.addAll(adultList);
		paxAdultChildList.addAll(childList);

		List<LCCInsuredPassengerDTO> insuredPassengerDTO = compileInsuredPaxDTOList(paxAdultChildList, contactInformation);

		if (AppSysParamsUtil.allowAddInsurnaceForInfants() && infantList != null && !infantList.isEmpty()) {
			insuredPassengerDTO.addAll(compileInsuredPaxDTOList(infantList, contactInformation));
		}
		List<FlightSegmentTO> flightSegmentTOs = AnciCommon.toFlightSegmentTOs(allInventory);
		flightSegmentTOs = AnciCommon.populateAnciOfferData(flightSegmentTOs, pricingInformation, travellerQuantity);

		InsuranceFltSegBuilder insuranceFltSegBuilder = null;
		try {
			insuranceFltSegBuilder = new InsuranceFltSegBuilder(flightSegmentTOs);
			for(FlightSegmentTO flightSegmentTO : flightSegmentTOs){
				if(!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())){
					filteredFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		} catch (ModuleException e) {
			throw new ValidationException(ValidationException.Code.ANCI_AVAILABLE_GENERIC_ERROR, e);
		}

		LCCInsuredJourneyDTO insuredJourneyDTO = AncillaryDTOUtil.getInsuranceJourneyDetails(
				insuranceFltSegBuilder.getInsurableFlightSegments(), insuranceFltSegBuilder.isReturnJourney(),
				CommonServiceUtil.getChannelId(trackInfo.getAppIndicator()));

		LCCInsuredContactInfoDTO insurContactInfo = getLCCInsuranceContactInfo(contactInformation);

		insuredJourneyDTO.setSelectedLanguage(LocaleContextHolder.getLocale().toString());
		ClientCommonInfoDTO clientCommonInfoDTO = new ClientCommonInfoDTO();
		clientCommonInfoDTO.setIpAddress(trackInfo.getIpAddress());
		clientCommonInfoDTO.setCarrierCode(trackInfo.getCarrierCode());
		
		// setting totalTicketPriceWithoutExternal
		if (!isModifyAnci) {
			TotalPaymentInfo TotalPaymentInfo = new TotalPaymentInfo(pricingInformation, null, travellerQuantity);
			totalTicketPriceWithoutExternal = TotalPaymentInfo.getTotalTicketPrice();
			if (resDiscountDTO != null) {
				totalTicketPriceWithoutExternal = AccelAeroCalculator.subtract(totalTicketPriceWithoutExternal,
						resDiscountDTO.getTotalDiscount());
			}
		}

		wrapper.setFlightSegmentTOs(filteredFlightSegmentTOs);
		wrapper.setInsuredPassengerDTOs(insuredPassengerDTO);
		wrapper.setInsuredJourneyDTO(insuredJourneyDTO);
		wrapper.setTransactionIdentifier(isModifyAnci? null : lccTransactionId != null ? lccTransactionId : "");
		wrapper.setSystem(system);
		wrapper.setPnr(pnr); 
		wrapper.setClientInfoDto(clientCommonInfoDTO);
		wrapper.setTotalTicketPriceWithoutExternal(totalTicketPriceWithoutExternal);
		wrapper.setTrackInfo(trackInfo);
		wrapper.setContactInfo(insurContactInfo);
		wrapper.setAppEngine(CommonServiceUtil.getApplicationEngine(trackInfo.getAppIndicator()));

		return wrapper;
	}

	private LCCInsuredContactInfoDTO getLCCInsuranceContactInfo(ContactInformation contactInformation) {
		LCCInsuredContactInfoDTO lccInsuranceContactInfo = new LCCInsuredContactInfoDTO();
		lccInsuranceContactInfo.setContactPerson(contactInformation.getFirstName() + " " + contactInformation.getLastName());
		lccInsuranceContactInfo.setAddress1(contactInformation.getAddress().getStreetAddress1() + contactInformation.getAddress().getStreetAddress2());
		lccInsuranceContactInfo.setAddress2(contactInformation.getAddress().getCity());
		lccInsuranceContactInfo.setHomePhoneNum(contactInformation.getLandNumber().getCountryCode() + "-" + contactInformation.getLandNumber().getAreaCode() + "-"
				+ contactInformation.getLandNumber().getNumber());
		lccInsuranceContactInfo.setMobilePhoneNum(contactInformation.getMobileNumber().getCountryCode() + "-" + contactInformation.getMobileNumber().getAreaCode() + "-"
				+ contactInformation.getMobileNumber().getNumber());
		lccInsuranceContactInfo.setCity(contactInformation.getAddress().getCity());
		lccInsuranceContactInfo.setCountry(contactInformation.getCountry());
		lccInsuranceContactInfo.setEmailAddress(contactInformation.getEmailAddress());
		lccInsuranceContactInfo.setPrefLanguage(LocaleContextHolder.getLocale().toString());

		return lccInsuranceContactInfo;
	}

	public Object toPriceQuoteAncillaries(List<Preference> preferences, MetaData metaData) {
		return null;

	}

	public Object toAllocateAncillaries(List<Preference> preferences, MetaData metaData) {
		return null;

	}

	public Object toSaveAncillaries(AncillaryReservation ancillaryReservation) {
		return null;

	}

	private List<LCCInsuredPassengerDTO> compileInsuredPaxDTOList(Collection<Passenger> paxAdultChildList,
			ContactInformation contactInfo) {
		List<LCCInsuredPassengerDTO> paxList = new ArrayList<LCCInsuredPassengerDTO>();
		for (Passenger p : paxAdultChildList) {
			String fn = p.getFirstName();
			String lastName = p.getLastName();
			String title = p.getTitle();
			String nationality = p.getNationality();

			NameDTO nameDto = new NameDTO();
			nameDto.setFirstName(fn);
			nameDto.setLastName(lastName);
			nameDto.setPaxTitle(title);

			LCCAddressDTO address = new LCCAddressDTO();
			address.setCellPhoneNo(contactInfo.getMobileNumber().getCountryCode()+ "-" + contactInfo.getMobileNumber().getAreaCode() + "-" + contactInfo.getMobileNumber().getNumber());
			address.setCityName(contactInfo.getAddress().getCity());
			address.setCountryName(contactInfo.getCountry());
			address.setEmailAddr(contactInfo.getEmailAddress());
			address.setHomePhoneNo(contactInfo.getLandNumber().getCountryCode() + "-" + contactInfo.getLandNumber().getAreaCode() + "-" + contactInfo.getLandNumber().getNumber());
			address.setStateName(contactInfo.getState());

			LCCInsuredPassengerDTO pax = new LCCInsuredPassengerDTO();
			pax.setBirthDate(p.getDateOfBirth());
			pax.setAddressDTO(address);
			pax.setEmail(contactInfo.getEmailAddress());
			pax.setHomePhoneNumber(address.getHomePhoneNo());
			pax.setNameDTO(nameDto);
			pax.setNationality(nationality);
			paxList.add(pax);
		}
		return paxList;
	}

	public class AvailableInsuranceRequestWrapper {

		private List<FlightSegmentTO> flightSegmentTOs;
		private List<LCCInsuredPassengerDTO> insuredPassengerDTOs;
		private LCCInsuredJourneyDTO insuredJourneyDTO;
		private String transactionIdentifier;
		private SYSTEM system;
		private String pnr;
		private ClientCommonInfoDTO clientInfoDto;
		private BigDecimal totalTicketPriceWithoutExternal;
		private BasicTrackInfo trackInfo;
		private ApplicationEngine appEngine;
		private LCCInsuredContactInfoDTO contactInfo;

		public List<FlightSegmentTO> getFlightSegmentTOs() {
			return flightSegmentTOs;
		}

		public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
			this.flightSegmentTOs = flightSegmentTOs;
		}

		public List<LCCInsuredPassengerDTO> getInsuredPassengerDTOs() {
			return insuredPassengerDTOs;
		}

		public void setInsuredPassengerDTOs(List<LCCInsuredPassengerDTO> insuredPassengerDTOs) {
			this.insuredPassengerDTOs = insuredPassengerDTOs;
		}

		public LCCInsuredJourneyDTO getInsuredJourneyDTO() {
			return insuredJourneyDTO;
		}

		public void setInsuredJourneyDTO(LCCInsuredJourneyDTO insuredJourneyDTO) {
			this.insuredJourneyDTO = insuredJourneyDTO;
		}

		public String getTransactionIdentifier() {
			return transactionIdentifier;
		}

		public void setTransactionIdentifier(String transactionIdentifier) {
			this.transactionIdentifier = transactionIdentifier;
		}

		public SYSTEM getSystem() {
			return system;
		}

		public void setSystem(SYSTEM system) {
			this.system = system;
		}

		public String getPnr() {
			return pnr;
		}

		public void setPnr(String pnr) {
			this.pnr = pnr;
		}

		public ClientCommonInfoDTO getClientInfoDto() {
			return clientInfoDto;
		}

		public void setClientInfoDto(ClientCommonInfoDTO clientInfoDto) {
			this.clientInfoDto = clientInfoDto;
		}

		public BigDecimal getTotalTicketPriceWithoutExternal() {
			return totalTicketPriceWithoutExternal;
		}

		public void setTotalTicketPriceWithoutExternal(BigDecimal totalTicketPriceWithoutExternal) {
			this.totalTicketPriceWithoutExternal = totalTicketPriceWithoutExternal;
		}

		public BasicTrackInfo getTrackInfo() {
			return trackInfo;
		}

		public void setTrackInfo(BasicTrackInfo trackInfo) {
			this.trackInfo = trackInfo;
		}

		public ApplicationEngine getAppEngine() {
			return appEngine;
		}

		public void setAppEngine(ApplicationEngine appEngine) {
			this.appEngine = appEngine;
		}

		public LCCInsuredContactInfoDTO getContactInfo() {
			return contactInfo;
		}

		public void setContactInfo(LCCInsuredContactInfoDTO contactInfo) {
			this.contactInfo = contactInfo;
		}

	}

}
