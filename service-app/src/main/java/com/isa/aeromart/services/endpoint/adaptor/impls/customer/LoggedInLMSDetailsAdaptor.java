package com.isa.aeromart.services.endpoint.adaptor.impls.customer;


import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.customer.LoggedInLmsDetails;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;



public class LoggedInLMSDetailsAdaptor implements Adaptor<LmsMember , LoggedInLmsDetails >{

	@Override
	public LoggedInLmsDetails adapt(LmsMember lmsDetails) {
		
		LoggedInLmsDetails loggedInLMSDetails = new LoggedInLmsDetails();
		
		if(lmsDetails.getEmailConfirmed()=='Y'){
	    	LoyaltyManagementBD lmsManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
	    	try {
				LoyaltyPointDetailsDTO existingMember = lmsManagementBD.getLoyaltyMemberPointBalances(lmsDetails.getFfid(), lmsDetails.getMemberExternalId());
				loggedInLMSDetails.setAvailablePoints(existingMember.getMemberPointsAvailable());
			} catch (ModuleException e) {
				//TODO handle errors
			}
	    }

		loggedInLMSDetails.setEmailConfirmed((lmsDetails.getEmailConfirmed() == 'Y')? true:false);
		loggedInLMSDetails.setFfid(lmsDetails.getFfid());
		
		return loggedInLMSDetails;
	}

}
