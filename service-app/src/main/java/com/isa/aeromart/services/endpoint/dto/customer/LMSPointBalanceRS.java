package com.isa.aeromart.services.endpoint.dto.customer;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class LMSPointBalanceRS extends TransactionalBaseRS {

	private Double pointBalance;

	public void setPointBalance(Double pointBalance) {
		this.pointBalance = pointBalance;

	}

	public Double getPointBalance() {
		return pointBalance;
	}

}
