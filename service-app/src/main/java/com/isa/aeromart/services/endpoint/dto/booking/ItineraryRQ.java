package com.isa.aeromart.services.endpoint.dto.booking;

public class ItineraryRQ {
	
	private String pnr;

	private boolean groupPnr;

	private String itineraryLanguage;

	private String operationType;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getItineraryLanguage() {
		return itineraryLanguage;
	}

	public void setItineraryLanguage(String itineraryLanguage) {
		this.itineraryLanguage = itineraryLanguage;
	}
}
