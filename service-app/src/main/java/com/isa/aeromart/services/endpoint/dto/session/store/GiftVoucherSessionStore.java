package com.isa.aeromart.services.endpoint.dto.session.store;

import com.isa.aeromart.services.endpoint.dto.booking.BookingRS;
import com.isa.aeromart.services.endpoint.dto.payment.PostPaymentDTO;
import com.isa.aeromart.services.endpoint.dto.session.GiftVoucherTransaction;
import com.isa.thinair.commons.api.dto.VoucherDTO;

import java.util.List;

public interface GiftVoucherSessionStore {

	public static String SESSION_KEY = GiftVoucherTransaction.SESSION_KEY;

	public List<VoucherDTO> getGiftVouchers();

	public void storeGiftVouchers(List<VoucherDTO> giftVoucherDTOs);

	public void setPaymentStatus(BookingRS.PaymentStatus status);

	public BookingRS.PaymentStatus getPaymentStatus();

	public void storePostPaymentInformation(PostPaymentDTO postPaymentDTO);

	public PostPaymentDTO getPostPaymentInformation();

	public void clearSessionStore();

	public String getCardHolderName();

	public void setCardHolderName(String cardHolderName);
}
