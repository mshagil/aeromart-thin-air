package com.isa.aeromart.services.endpoint.delegate.passenger;

import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRQ;
import com.isa.aeromart.services.endpoint.dto.passenger.ValidatePaxFFIDsRS;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface PassengerService {

	public boolean checkDuplicates(TrackInfoDTO trackInfoDTO, DuplicateValidatorAssembler duplicateValidatorAssembler);

	public ValidatePaxFFIDsRS validateLMSPax(ValidatePaxFFIDsRQ validatePaxFFIDsRQ) throws ModuleException;
}
