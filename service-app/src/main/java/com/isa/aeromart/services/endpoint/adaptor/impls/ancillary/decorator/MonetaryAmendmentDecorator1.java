package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.decorator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.DecoratedAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.adaptor.impls.booking.FlightSegmentAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.ancillary.internal.InventoryWrapper;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.BookingTransaction;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.ancillary.SessionPassenger;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.aeromart.services.endpoint.utils.booking.FlexiUtil;
import com.isa.aeromart.services.endpoint.utils.common.CommonServiceUtil;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

/**
 * Use MonetaryAmendmentDecorator instead New target is PriceQuoteAncillaryRS instead of AncillaryIntegrateTO
 */
@Deprecated
public class MonetaryAmendmentDecorator1<S>
		extends DecoratedAdaptor<MonetaryAmendmentDecorator1.MonetaryAmendmentWrapper, S, AncillaryIntegrateTO> {

	private AncillaryIntegrateTO ancillaryIntegrateTO;

	public AncillaryIntegrateTO adapt(S source) {

		ancillaryIntegrateTO = getBaseAdaptor().adapt(source);
		setJnTax(getDecorate().getTaxApplicability(), getDecorate().getLccClientReservation());
		setPenaltyForAnciModification(getDecorate().isApplyAnciPenalty());
		try {
			if (getTransactionalAncillaryService().isAncillaryTransaction(getTransactionId())) {
				applyServiceTax();
			}
		} catch (ModuleException e) {
//			TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ancillaryIntegrateTO;
	}

	private void setJnTax(AnciAvailabilityRS taxApplicability, LCCClientReservation lccClientReservation) {

		List<InventoryWrapper> inventory = getTransactionalAncillaryService().getFlightSegments(getTransactionId());
		InventoryWrapper firstDepartingSegment = AnciCommon.getFirstDepartingSegment(inventory);
		ReservationPaxTO reservationPaxTO;
		List<FlightSegmentTO> flightSegmentTOs;
		String paxType;
		Map<Integer, Map<ReservationInternalConstants.EXTERNAL_CHARGES, BigDecimal>> paxWiseModSegAnciTotal;

		ReservationInfo reservationInfo = getTransactionalAncillaryService().getReservationInfo(getTransactionId());

		paxWiseModSegAnciTotal = new HashMap<>();
		List<AncillaryIntegratePassengerTO> anciPassengerList = ancillaryIntegrateTO.getReservation().getPassengers();
		Map<Integer, List<LCCClientFlexiExternalChgDTO>> paxWiseFlexiExternalChargeMap = AvailabilitySelectedFlexiDetails();

		if (getTransactionalAncillaryService().isRequoteTransaction(getTransactionId())
				&& !ReservationInternalConstants.ReservationStatus.ON_HOLD
						.equals(getTransactionalAncillaryService().getReservationInfo(getTransactionId()).getReservationStatus())
				&& lccClientReservation != null) {
			Collection<LCCClientReservationPax> colPax = lccClientReservation.getPassengers();
			flightSegmentTOs = AnciCommon.toFlightSegmentTOs(inventory);
			List<String> modSegList = AnciCommon.getModifiedFltSegRefList(
					getTransactionalAncillaryService().getRphGenerator(getTransactionId()),
					getTransactionalAncillaryService().getModifications(getTransactionId()),
					getTransactionalAncillaryService().getResSegRphToFlightSegMap(getTransactionId()));
			paxWiseModSegAnciTotal = ExternalChargeUtil.getPaxWiseModifyingSegmentAnciTotal(flightSegmentTOs, modSegList, colPax);
		}

		if (taxApplicability != null && taxApplicability.isJnTaxApplicable() && anciPassengerList != null) {

			for (AncillaryIntegratePassengerTO anciPassengerTO : anciPassengerList) {

				List<LCCClientExternalChgDTO> paxExternalChargeList = new ArrayList<LCCClientExternalChgDTO>();
				reservationPaxTO = new ReservationPaxTO();

				reservationPaxTO.setSeqNumber(Integer.valueOf(anciPassengerTO.getPassengerRph()));
				for (LCCClientExternalChgDTO lccClientExternalChgDTO : anciPassengerTO.getExternalCharges()) {
					paxExternalChargeList.add(lccClientExternalChgDTO.clone());
				}
				// update availability selected flexi chargers to calculate jn tax
				if (paxWiseFlexiExternalChargeMap.containsKey(Integer.valueOf(anciPassengerTO.getPassengerRph()))) {
					paxExternalChargeList
							.addAll(paxWiseFlexiExternalChargeMap.get(Integer.valueOf(anciPassengerTO.getPassengerRph())));
				}

				for (LCCSelectedSegmentAncillaryDTO segment : anciPassengerTO.getSelectedAncillaries()) {
					reservationPaxTO.addSelectedAncillaries(segment);
				}
				for (LCCSelectedSegmentAncillaryDTO segment : anciPassengerTO.getAncillariesToRemove()) {
					reservationPaxTO.addAncillariesToRemove(segment);
				}

				reservationPaxTO.addExternalCharges(paxExternalChargeList);
				for (ReservationPaxTO pax : reservationInfo.getPaxList()) {
					if (anciPassengerTO.getPassengerRph().equals(String.valueOf(pax.getSeqNumber()))) {
						paxType = pax.getPaxType();

						if (!PaxTypeTO.INFANT.equals(paxType)) {
							ExternalChargeUtil.setPaxEffectiveExternalChargeTaxAmount(reservationPaxTO,
									taxApplicability.getTaxRatio(),
									firstDepartingSegment.getFlightSegmentTO().getFlightRefNumber(), paxWiseModSegAnciTotal);
						}
					}
				}

				for (LCCClientExternalChgDTO externalChg : reservationPaxTO.getExternalCharges()) {
					if (externalChg.getExternalCharges() == ReservationInternalConstants.EXTERNAL_CHARGES.JN_ANCI) {
						anciPassengerTO.getExternalCharges().add(externalChg);
						anciPassengerTO.setNetAmount(anciPassengerTO.getNetAmount() != null
								? AccelAeroCalculator.add(anciPassengerTO.getNetAmount(), externalChg.getAmount())
								: externalChg.getAmount());
					}
				}

			}

			// populate JN_ANCI tax objects for ancillaryIntegrateTO
			if (ancillaryIntegrateTO.getReservation() != null) {
				ancillaryIntegrateTO.getReservation().setJnTaxApplicable(taxApplicability.isJnTaxApplicable());
				ancillaryIntegrateTO.getReservation().setTaxRatio(taxApplicability.getTaxRatio());
			}
		}
	}

	private void setPenaltyForAnciModification(boolean applyAnciPenalty) {

		List<InventoryWrapper> inventory = getTransactionalAncillaryService().getFlightSegments(getTransactionId());
		InventoryWrapper firstDepartingSegment = AnciCommon.getSegmentForAnciPenalty(inventory);
		ReservationPaxTO reservationPaxTO;
		String paxType;
		boolean anciModifyPenaltyForSystem;
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseAnciPenaltyExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();

		ReservationInfo reservationInfo = getTransactionalAncillaryService().getReservationInfo(getTransactionId());

		if (ProxyConstants.SYSTEM.AA.equals(ProxyConstants.SYSTEM.getEnum(
				getTransactionalAncillaryService().getPreferencesForAncillary(getTransactionId()).getSelectedSystem()))) {
			anciModifyPenaltyForSystem = !AppSysParamsUtil.isRefundIBEAnciModifyCredit();
		} else {
			anciModifyPenaltyForSystem = true;
		}

		if (applyAnciPenalty && anciModifyPenaltyForSystem && ancillaryIntegrateTO.getReservation().getPassengers() != null) {

			for (AncillaryIntegratePassengerTO anciPassengerTO : ancillaryIntegrateTO.getReservation().getPassengers()) {
				reservationPaxTO = new ReservationPaxTO();
				reservationPaxTO.setSeqNumber(Integer.valueOf(anciPassengerTO.getPassengerRph()));
				reservationPaxTO.addExternalCharges(anciPassengerTO.getExternalCharges());
				for (LCCSelectedSegmentAncillaryDTO segment : anciPassengerTO.getSelectedAncillaries()) {
					reservationPaxTO.addSelectedAncillaries(segment);
				}
				for (LCCSelectedSegmentAncillaryDTO segment : anciPassengerTO.getAncillariesToRemove()) {
					reservationPaxTO.addAncillariesToRemove(segment);
				}

				for (ReservationPaxTO pax : reservationInfo.getPaxList()) {
					if (anciPassengerTO.getPassengerRph().equals(String.valueOf(pax.getSeqNumber()))) {
						paxType = pax.getPaxType();
						reservationPaxTO.setPaxType(paxType);
						paxWisePaxTypes.put(pax.getSeqNumber(), paxType);
					}
				}
				//populate paxwise penalty
				AnciCommon.applyAncillaryModifyPenalty(reservationPaxTO, firstDepartingSegment.getFlightSegmentTO());
				
				//penalty related service tax call
				for (LCCClientExternalChgDTO externalChg : reservationPaxTO.getExternalCharges()) {
					if (externalChg.getExternalCharges() == ReservationInternalConstants.EXTERNAL_CHARGES.ANCI_PENALTY) {
						anciPassengerTO.getExternalCharges().add(externalChg);
						anciPassengerTO.setNetAmount(anciPassengerTO.getNetAmount() != null
								? AccelAeroCalculator.add(anciPassengerTO.getNetAmount(), externalChg.getAmount())
								: externalChg.getAmount());
						if(!paxWiseAnciPenaltyExternalCharges.containsKey(Integer.valueOf(anciPassengerTO.getPassengerRph()))){
							paxWiseAnciPenaltyExternalCharges.put(Integer.valueOf(anciPassengerTO.getPassengerRph()), new ArrayList<LCCClientExternalChgDTO>());
						}
						paxWiseAnciPenaltyExternalCharges.get(Integer.valueOf(anciPassengerTO.getPassengerRph())).add(externalChg);
					}
				}

			}
			// For Non Ticketing Service Taxes point of embarkation is required and for IBE's It is not declared and
			// because of that it will not not effect the penalty amount, If Service Taxes are calculated regardless of
			// this scenarios can remove the if condition 
			if (false) {
				ServiceTaxQuoteForNonTicketingRevenueRS nonTicketingServiceTaxRS = null;
				ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
				serviceTaxQuoteCriteriaDTO
						.setFareSegChargeTO(getTransactionalAncillaryService().getPricingInformation(getTransactionId()));
				serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(AnciCommon.toBaseAvailRQ(
						AnciCommon.getPaxQtyList(
								getTransactionalAncillaryService().getTravellerQuantityFromSession(getTransactionId())),
						getTransactionalAncillaryService().getPreferencesForAncillary(getTransactionId()),
						CommonServiceUtil.getApplicationEngine(getTrackInfoDTO().getAppIndicator())));
				serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(
						AnciCommon.toFlightSegmentTOs(getTransactionalAncillaryService().getFlightSegments(getTransactionId())));
				serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(getTransactionId());
				serviceTaxQuoteCriteriaDTO.setPaxCountryCode(getTransactionalAncillaryService()
						.getReservationInfo(getTransactionId()).getContactInfo().getCountryCode());
				serviceTaxQuoteCriteriaDTO.setPaxState(
						getTransactionalAncillaryService().getReservationInfo(getTransactionId()).getContactInfo().getState());
				String taxRegNo = getTransactionalAncillaryService().getReservationInfo(getTransactionId()).getContactInfo()
						.getTaxRegNo();
				serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered((taxRegNo != null && !taxRegNo.isEmpty()) ? true : false);
				serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
				serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseAnciPenaltyExternalCharges);
				// TODO expose a method in Airproxy to call the non ticketing revenue
				// try {
				//
				// nonTicketingServiceTaxRS = ModuleServiceLocator.getAirproxyReservationBD()
				// .quoteServiceTaxForNonTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfoDTO());
				// } catch (ModuleException e) {
				// // TODO implement a log for decorator related logics
				// }

				if (nonTicketingServiceTaxRS != null && nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes() != null
						&& !nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().isEmpty()) {
					nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().forEach((paxSeq, paxServiceTax) -> {
						ancillaryIntegrateTO.getReservation().getPassengers().forEach(ancillaryIntegratePassengerTO -> {
							if (paxSeq == Integer.valueOf(ancillaryIntegratePassengerTO.getPassengerRph())) {
								ancillaryIntegratePassengerTO.getExternalCharges().forEach(paxExternalCharge -> {
									if (paxExternalCharge
											.getExternalCharges() == ReservationInternalConstants.EXTERNAL_CHARGES.ANCI_PENALTY) {
										paxExternalCharge.setAmount(AccelAeroCalculator.subtract(paxExternalCharge.getAmount(),
												paxServiceTax.getEffectiveCnxModPenChargeAmount()));
										if (!paxWiseAnciPenaltyExternalCharges
												.containsKey(Integer.valueOf(ancillaryIntegratePassengerTO.getPassengerRph()))) {
											paxWiseAnciPenaltyExternalCharges.put(
													Integer.valueOf(ancillaryIntegratePassengerTO.getPassengerRph()),
													new ArrayList<LCCClientExternalChgDTO>());
										}
										paxServiceTax.getCnxModPenServiceTaxes().forEach(serviceTaxDTO -> {
											if (AccelAeroCalculator.isLessThan(serviceTaxDTO.getAmount(),
													AccelAeroCalculator.getDefaultBigDecimalZero())) {
												LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
												chgDTO.setExternalCharges(EXTERNAL_CHARGES.SERVICE_TAX);
												chgDTO.setAmount(serviceTaxDTO.getAmount());
												chgDTO.setFlightRefNumber(serviceTaxDTO.getFlightRefNumber());
												chgDTO.setCarrierCode(serviceTaxDTO.getCarrierCode());
												chgDTO.setNonTaxableAmount(serviceTaxDTO.getNonTaxableAmount());
												chgDTO.setTaxableAmount(serviceTaxDTO.getTaxableAmount());
												chgDTO.setCode(serviceTaxDTO.getChargeCode());
												paxWiseAnciPenaltyExternalCharges
														.get(Integer.valueOf(ancillaryIntegratePassengerTO.getPassengerRph()))
														.add(chgDTO);
											}

										});
									}
								});
							}
						});
					});
				}
			}
		}
	}

	private Map<Integer, List<LCCClientFlexiExternalChgDTO>> AvailabilitySelectedFlexiDetails() {
		
		List<SessionFlexiDetail> sessionFlexiDetails = getTransactionalAncillaryService().getSessionFlexiDetail(
				getTransactionId());
		TravellerQuantity travellerQuantity = getTransactionalAncillaryService().getTravellerQuantityFromSession(
				getTransactionId());
		List<SessionFlightSegment> sessionFlightSegments = getTransactionalAncillaryService().getSelectedSegments(
				getTransactionId());
		List<Passenger> passengers = AnciCommon.sessionPassengerToCommomPassenger((List<SessionPassenger>) (getTransactionalAncillaryService()
				.getReservationData(getTransactionId()) != null ? getTransactionalAncillaryService()
				.getReservationData(getTransactionId()).getPassengers(): new ArrayList<SessionPassenger>()));

		return FlexiUtil.getPaxWiseSegmentWiseFlexiExternalCharges(sessionFlightSegments, travellerQuantity, sessionFlexiDetails,
				passengers);
	}
	
	private void applyServiceTax() throws ModuleException {
		
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
		ReservationInfo resInfo = getTransactionalAncillaryService().getReservationInfo(getTransactionId());

		PaymentBaseSessionStore paymentSessionStore = getTransactionalAncillaryService().getPaymentBaseSessionStore(getTransactionId());
		
		if(paymentSessionStore instanceof BookingTransaction){
			return;
		}
		
		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
		baseAvailRQ.getAvailPreferences().setSearchSystem(ProxyConstants.SYSTEM.getEnum(paymentSessionStore.getPreferencesForPayment().getSelectedSystem()));
		
		List<FlightSegmentTO> flightSegments = adaptFlightSegment(paymentSessionStore.getSelectedSegmentsForPayment());

		// Removing Service Tax from PaymentSessionStore in payment external charge
		paymentSessionStore.removePaymentPaxCharge(EXTERNAL_CHARGES.SERVICE_TAX);
		
		for (AncillaryIntegratePassengerTO anciPassenger : ancillaryIntegrateTO.getReservation().getPassengers()) {
			Map<String, LCCClientExternalChgDTO> addedChgs = getChgCodeFltRefWiseAddedChargeDTOs(anciPassenger);
			Map<String, BigDecimal> removedChgAmts = getChgCodeFltRefWiseRemovedChgAmts(anciPassenger);
			setEffectiveChgs(addedChgs, removedChgAmts);
			List<LCCClientExternalChgDTO> charges = new ArrayList<>();
			for (LCCClientExternalChgDTO extChg : addedChgs.values()) {
				charges.add(extChg);
			}
			paxWiseExternalCharges.put(Integer.parseInt(anciPassenger.getPassengerRph()), charges);
		}
		
		for (ReservationPaxTO reservationPaxTO : resInfo.getPaxList()) {
			paxWisePaxTypes.put(reservationPaxTO.getSeqNumber(), reservationPaxTO.getPaxType());
		}
		
		// Constructing ServiceTaxQuoteCriteriaDTO obj
		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = createServiceTaxQuoteCriteriaForTktDTO(baseAvailRQ,null,
											flightSegments, resInfo.getContactInfo(), paxWiseExternalCharges,paxWisePaxTypes,getTransactionId());
		serviceTaxQuoteCriteriaDTO.setCalculateOnlyForExternalCharges(
				getTransactionalAncillaryService().isAncillaryTransaction(getTransactionId()));
		// Calling Service Tax Call
		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator.getAirproxyReservationBD()
				.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfoDTO());

		// Applying Service Tax RS to Pax wise Charges
		ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(resInfo.getPaxList(), serviceTaxQuoteRS, false, ApplicationEngine.IBE, false);
		
		updateAncillaryIntegrateTO(resInfo.getPaxList());
		
	}

	private List<FlightSegmentTO> adaptFlightSegment(List<SessionFlightSegment> segments) {
		FlightSegmentAdaptor flightSegmentAdaptor = new FlightSegmentAdaptor();
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		AdaptorUtils.adaptCollection(segments, flightSegmentTOs, flightSegmentAdaptor);
		return flightSegmentTOs;
	}
	
	private ServiceTaxQuoteCriteriaForTktDTO createServiceTaxQuoteCriteriaForTktDTO(BaseAvailRQ baseAvailRQ, FareSegChargeTO fareSegChargeTO,
			List<FlightSegmentTO> flightSegments, CommonReservationContactInfo contactInfo,
			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges, Map<Integer, String> paxWisePaxTypes,String transactionId) {

		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
		
		if (baseAvailRQ.getAvailPreferences() != null && baseAvailRQ.getAvailPreferences().getAppIndicator() == null) {
			baseAvailRQ.getAvailPreferences()
					.setAppIndicator(CommonServiceUtil.getApplicationEngine(getTrackInfoDTO().getAppIndicator()));
		}		
		serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
		serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(fareSegChargeTO);
		serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegments);
		serviceTaxQuoteCriteriaDTO.setPaxState(contactInfo.getState());
		serviceTaxQuoteCriteriaDTO.setPaxCountryCode(contactInfo.getCountryCode());
		serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered((contactInfo.getTaxRegNo() != null && !"".equals(contactInfo
				.getTaxRegNo())) ? true : false);
		serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
		serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
		serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(transactionId);

		return serviceTaxQuoteCriteriaDTO;
	}
	
	private void updateAncillaryIntegrateTO(List<ReservationPaxTO> paxList) {
		for (AncillaryIntegratePassengerTO anciPassenger : ancillaryIntegrateTO.getReservation().getPassengers()) {
			for (ReservationPaxTO reservationPaxTO : paxList) {
				if (anciPassenger.getPassengerRph().equals(String.valueOf(reservationPaxTO.getSeqNumber()))) {
					for (LCCClientExternalChgDTO externalChg : reservationPaxTO.getExternalCharges()) {
						if (externalChg.getExternalCharges() == ReservationInternalConstants.EXTERNAL_CHARGES.SERVICE_TAX) {
							anciPassenger.getExternalCharges().add(externalChg);
							anciPassenger.setNetAmount(anciPassenger.getNetAmount() != null
									? AccelAeroCalculator.add(anciPassenger.getNetAmount(), externalChg.getAmount())
									: externalChg.getAmount());
						}
					}
					// to avoid charges getting duplicated in AncillaryIntegratePassengerTO & ReservationPaxTO
					reservationPaxTO.removeSelectedExternalCharge(EXTERNAL_CHARGES.SERVICE_TAX);
				}
			}
		}
	}
	
	private static Map<String, LCCClientExternalChgDTO> getChgCodeFltRefWiseAddedChargeDTOs(AncillaryIntegratePassengerTO anciPassenger) {
		Map<String, LCCClientExternalChgDTO> addedChgs = new HashMap<>();
		for (LCCClientExternalChgDTO extChg : anciPassenger.getExternalCharges()) {
			String key = extChg.getFlightRefNumber() + "|" + extChg.getExternalCharges().toString();

			if (!addedChgs.containsKey(key)) {
				addedChgs.put(key, extChg.clone());
			} else {
				LCCClientExternalChgDTO existinChg = addedChgs.get(key);
				existinChg.setAmount(AccelAeroCalculator.add(extChg.getAmount(), existinChg.getAmount()));
			}
		}
		return addedChgs;
	}
	
	private static Map<String, BigDecimal> getChgCodeFltRefWiseRemovedChgAmts(AncillaryIntegratePassengerTO anciPassenger) {
		Map<String, BigDecimal> removedChgAmts = new HashMap<>();
		for (LCCSelectedSegmentAncillaryDTO segAnci : anciPassenger.getAncillariesToRemove()) {
			
			if (segAnci.getAirSeatDTO() != null && segAnci.getAirSeatDTO().getSeatNumber() != null
					&& !segAnci.getAirSeatDTO().getSeatNumber().trim().equals("")) {
				BigDecimal charge = segAnci.getAirSeatDTO().getSeatCharge();
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.SEAT_MAP.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getMealDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCMealDTO meal : segAnci.getMealDTOs()) {
					charge = AccelAeroCalculator.add(charge,
							AccelAeroCalculator.multiply(meal.getMealCharge(), new BigDecimal(meal.getAllocatedMeals())));

				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.MEAL.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getBaggageDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCBaggageDTO baggage : segAnci.getBaggageDTOs()) {
					charge = AccelAeroCalculator.add(charge, baggage.getBaggageCharge());
				}

				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.BAGGAGE.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getSpecialServiceRequestDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCSpecialServiceRequestDTO ssr : segAnci.getSpecialServiceRequestDTOs()) {
					charge = AccelAeroCalculator.add(charge, ssr.getCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getAirportServiceDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCAirportServiceDTO airportService : segAnci.getAirportServiceDTOs()) {
					charge = AccelAeroCalculator.add(charge, airportService.getServiceCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.AIRPORT_SERVICE.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getAirportTransferDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCAirportServiceDTO airportTrasnsfer : segAnci.getAirportTransferDTOs()) {
					charge = AccelAeroCalculator.add(charge, airportTrasnsfer.getServiceCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.AIRPORT_TRANSFER.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getAutomaticCheckinDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCAutomaticCheckinDTO autoCheckin : segAnci.getAutomaticCheckinDTOs()) {
					charge = AccelAeroCalculator.add(charge, autoCheckin.getAutomaticCheckinCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
		}
		return removedChgAmts;
	}
	
	private static void setEffectiveChgs(Map<String, LCCClientExternalChgDTO> addedChgs, Map<String, BigDecimal> removedChgAmts) {
		Iterator<Entry<String, LCCClientExternalChgDTO>> itr = addedChgs.entrySet().iterator();
		while (itr.hasNext()) {
			String key = itr.next().getKey();
			if (removedChgAmts.containsKey(key)) {
				LCCClientExternalChgDTO chg = addedChgs.get(key);
				if (chg.getAmount().compareTo(removedChgAmts.get(key)) <= 0) {
					itr.remove();
				} else {
					chg.setAmount(AccelAeroCalculator.subtract(chg.getAmount(), removedChgAmts.get(key)));
				}
			}
		}
	}
	
	public static class MonetaryAmendmentWrapper {

		private AnciAvailabilityRS taxApplicability;

		private boolean applyAnciPenalty;

		private LCCClientReservation lccClientReservation;

		public AnciAvailabilityRS getTaxApplicability() {
			return taxApplicability;
		}

		public void setTaxApplicability(AnciAvailabilityRS taxApplicability) {
			this.taxApplicability = taxApplicability;
		}

		public boolean isApplyAnciPenalty() {
			return applyAnciPenalty;
		}

		public void setApplyAnciPenalty(boolean applyAnciPenalty) {
			this.applyAnciPenalty = applyAnciPenalty;
		}

		public LCCClientReservation getLccClientReservation() {
			return lccClientReservation;
		}

		public void setLccClientReservation(LCCClientReservation lccClientReservation) {
			this.lccClientReservation = lccClientReservation;
		}

	}

}
