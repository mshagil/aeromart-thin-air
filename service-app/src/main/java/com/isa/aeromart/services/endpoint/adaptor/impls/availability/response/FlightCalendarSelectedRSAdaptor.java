package com.isa.aeromart.services.endpoint.adaptor.impls.availability.response;

import static com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.CommonRSAdaptor.getFareClasses;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.SelectedFlightPricingAdaptor;
import com.isa.aeromart.services.endpoint.dto.availability.AvailabilitySearchRQ;
import com.isa.aeromart.services.endpoint.dto.availability.FlightCalendarSelectedRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

public class FlightCalendarSelectedRSAdaptor implements Adaptor<FlightAvailRS, FlightCalendarSelectedRS> {

	private AvailabilitySearchRQ availabilitySearchReq;
	private RPHGenerator rphGenerator;
	private TrackInfoDTO trackInfo;
	private SYSTEM searchSystem;

	public FlightCalendarSelectedRSAdaptor(AvailabilitySearchRQ availabilitySearchReq, RPHGenerator rphGenerator, TrackInfoDTO trackInfo, SYSTEM searchSystem) {
		this.availabilitySearchReq = availabilitySearchReq;
		this.rphGenerator = rphGenerator;
		this.trackInfo = trackInfo;
		this.searchSystem = searchSystem;
	}

	@Override
	public FlightCalendarSelectedRS adapt(FlightAvailRS source) {
		FlightCalendarSelectedRS response = new FlightCalendarSelectedRS();
		if (source != null) {
			CalendarRSAdaptor flightCalendarAdaptor = CalendarRSAdaptor.getAdaptorForFlightResponse(rphGenerator);
			response = new FlightCalendarSelectedRS(flightCalendarAdaptor.adapt(source));
			if (source.getSelectedPriceFlightInfo() != null) {
				SelectedFlightPricingAdaptor selFlightPriceTrans = new SelectedFlightPricingAdaptor(
						availabilitySearchReq.getTravellerQuantity(), source.getSelectedSegmentFlightPriceInfo(), searchSystem,
						trackInfo, source.getTransactionIdentifier());
				response.setSelectedFlightPricing(selFlightPriceTrans.adapt(source.getSelectedPriceFlightInfo()));
			}
			response.setCurrency(availabilitySearchReq.getPreferences().getCurrency());
			response.setFareClasses(getFareClasses(source));
			response.setSuccess(true);
		}
		return response;
	}
}
