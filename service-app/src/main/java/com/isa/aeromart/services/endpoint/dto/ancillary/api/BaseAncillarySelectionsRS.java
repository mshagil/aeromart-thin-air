package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryReservation;
import com.isa.aeromart.services.endpoint.dto.ancillary.MetaData;
import com.isa.aeromart.services.endpoint.dto.ancillary.ReservationDescriptor;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class BaseAncillarySelectionsRS extends TransactionalBaseRS {

    private ReservationDescriptor reservationData;

    private MetaData metaData;

    private AncillaryReservation ancillaryReservation;

    public ReservationDescriptor getReservationData() {
        return reservationData;
    }

    public void setReservationData(ReservationDescriptor reservationData) {
        this.reservationData = reservationData;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public AncillaryReservation getAncillaryReservation() {
        return ancillaryReservation;
    }

    public void setAncillaryReservation(AncillaryReservation ancillaryReservation) {
        this.ancillaryReservation = ancillaryReservation;
    }

}
