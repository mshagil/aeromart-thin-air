package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class ReservationInfoAdaptor implements Adaptor<LCCClientReservation, ReservationInfo> {

	private String transactionIdentifier;

	public ReservationInfoAdaptor(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	@Override
	public ReservationInfo adapt(LCCClientReservation reservation) {

		ReservationInfo reservationInfo = new ReservationInfo();
		reservationInfo.setPnr(reservation.getPNR());
		reservationInfo.setGroupPNR(reservation.isGroupPNR());
		reservationInfo.setVersion(reservation.getVersion());
		if (reservation.getContactInfo().getCustomerId() != null) {
			reservationInfo.setCustomerId(reservation.getContactInfo().getCustomerId().toString());
		}
		if (reservation.getPreferrenceInfo() != null) {
			reservationInfo.setLanguage(reservation.getPreferrenceInfo().getPreferredLanguage());
		}
		reservationInfo.setSelectedCurrency(reservation.getLastCurrencyCode());
		TravellerQuantity travellerQuantity = new TravellerQuantity();
		travellerQuantity.setAdultCount(reservation.getTotalPaxAdultCount());
		travellerQuantity.setChildCount(reservation.getTotalPaxChildCount());
		travellerQuantity.setInfantCount(reservation.getTotalPaxInfantCount());
		reservationInfo.setPaxTypeWiseCount(travellerQuantity);
		Map<String, Map<String, BigDecimal>> pnrPaxCreditMap = new HashMap<>();
		pnrPaxCreditMap.put(reservation.getPNR(), getPaxCreditMap(reservation.getPassengers()));
		reservationInfo.setPnrPaxCreditMap(pnrPaxCreditMap);

		reservationInfo.setPaxList(createPaxList(reservation.getPassengers()));
		reservationInfo.setContactInfo(reservation.getContactInfo());
		reservationInfo.setReservationStatus(reservation.getStatus());
		reservationInfo.setExistingAllLccSegments(reservation.getSegments());
		reservationInfo.setTransactionId(transactionIdentifier);
		reservationInfo.setInfantPaymentSeparated(reservation.isInfantPaymentSeparated());
		return reservationInfo;
	}

	private List<ReservationPaxTO> createPaxList(Set<LCCClientReservationPax> lccPaxList) {
		List<ReservationPaxTO> paxList = new ArrayList<>();
		for (LCCClientReservationPax lccPax : lccPaxList) {
			ReservationPaxTO pax = new ReservationPaxTO();
			pax.setTravelerRefNumber(lccPax.getTravelerRefNumber());
			pax.setSeqNumber(lccPax.getPaxSequence());
			pax.setPaxType(lccPax.getPaxType());
			pax.setIsParent(isParent(lccPax.getPaxSequence(), lccPaxList));
			pax = setInfantWith(lccPax, pax);
			paxList.add(pax);
		}
		return paxList;
	}

	private boolean isParent(Integer paxSequence, Set<LCCClientReservationPax> lccPaxList) {
		boolean isParent = false;
		for (LCCClientReservationPax lccPax : lccPaxList) {
			if (PaxTypeTO.INFANT.equals(lccPax.getPaxType()) && lccPax.getParent() != null
					&& paxSequence.equals(lccPax.getParent().getPaxSequence())) {
				isParent = true;
				break;
			}
		}
		return isParent;
	}

	private static Map<String, BigDecimal> getPaxCreditMap(Set<LCCClientReservationPax> passengers) {
		Map<String, BigDecimal> paxCreditMap = new HashMap<>();

		for (LCCClientReservationPax pax : passengers) {
			paxCreditMap.put(pax.getPaxSequence().toString(), pax.getTotalAvailableBalance());
		}

		return paxCreditMap;
	}

	private ReservationPaxTO setInfantWith(LCCClientReservationPax lccPax, ReservationPaxTO pax) {
		if (PaxTypeTO.INFANT.equals(lccPax.getPaxType()) && lccPax.getParent() != null) {
			pax.setInfantWith(lccPax.getParent().getPaxSequence());
			return pax;
		}
		return pax;
	}

}
