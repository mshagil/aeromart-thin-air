package com.isa.aeromart.services.endpoint.controller.common;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.isa.aeromart.services.endpoint.errorhandling.validation.GenericValidator;

public abstract class StatefulRequestAwareController {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@Autowired
	private ServletContext servletContext;

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	protected HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	protected ServletContext getServletContext() {
		return servletContext;
	}

	protected void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(new GenericValidator());
	}

}
