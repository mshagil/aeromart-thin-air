package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.ancillary.api.PriceQuoteAncillaryRS;

public abstract class DecoratedAdaptor<D, S, T> extends TransactionAwareAdaptor<S, T> {

	private Adaptor<S, T> baseAdaptor;

	private D decorate;
	
	private PriceQuoteAncillaryRS removeAncillary;
	
	private PriceQuoteAncillaryRS addedAncillary;

	public Adaptor<S, T> getBaseAdaptor() {
		return baseAdaptor;
	}

	public void setBaseAdaptor(Adaptor<S, T> baseAdaptor) {
		this.baseAdaptor = baseAdaptor;
	}

	public D getDecorate() {
		return decorate;
	}

	public void setDecorate(D decorate) {
		this.decorate = decorate;
	}

	public PriceQuoteAncillaryRS getRemoveAncillary() {
		return removeAncillary;
	}

	public void setRemoveAncillary(PriceQuoteAncillaryRS removeAncillary) {
		this.removeAncillary = removeAncillary;
	}

	public PriceQuoteAncillaryRS getAddedAncillary() {
		return addedAncillary;
	}

	public void setAddedAncillary(PriceQuoteAncillaryRS addedAncillary) {
		this.addedAncillary = addedAncillary;
	}
	
}
