package com.isa.aeromart.services.endpoint.dto.availability;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotEmpty;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class AvailabilitySearchRQ extends TransactionalBaseRQ {

	@NotEmpty
	private List<OriginDestinationInfo> journeyInfo;

	@NotNull
	private TravellerQuantity travellerQuantity;

	private Preferences preferences;

	private boolean groupPnr;
	
	private String pnr;
	
	private String selectedFlightSegmentSequence;

	public List<OriginDestinationInfo> getJourneyInfo() {
		return journeyInfo;
	}

	public void setJourneyInfo(List<OriginDestinationInfo> journeyInfo) {
		this.journeyInfo = journeyInfo;
	}

	public Preferences getPreferences() {
		if (preferences == null) {
			return new Preferences();
		}
		return preferences;
	}

	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}

	public TravellerQuantity getTravellerQuantity() {
		return travellerQuantity;
	}

	public void setTravellerQuantity(TravellerQuantity travellerQuantity) {
		this.travellerQuantity = travellerQuantity;
	}

	public boolean isGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getSelectedFlightSegmentSequence() {
		return selectedFlightSegmentSequence;
	}

	public void setSelectedFlightSegmentSequence(String selectedFlightSegmentSequence) {
		this.selectedFlightSegmentSequence = selectedFlightSegmentSequence;
	}
}
