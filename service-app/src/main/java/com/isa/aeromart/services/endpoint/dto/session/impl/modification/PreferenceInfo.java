package com.isa.aeromart.services.endpoint.dto.session.impl.modification;

public class PreferenceInfo {
	
	private String seatType;

	private String bookingCode;
	
	private String selectedSystem;

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(String selectedSystem) {
		this.selectedSystem = selectedSystem;
	}	
	
}
