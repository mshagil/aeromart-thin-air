package com.isa.aeromart.services.endpoint.dto.payment;

import java.math.BigDecimal;

public class EffectiveMultiplePaymentRQ {

	private PaymentOptions paymentOptions;
	private BigDecimal totalPaymentAmount;

	public BigDecimal getTotalPaymentAmount() {
		return totalPaymentAmount;
	}

	public void setTotalPaymentAmount(BigDecimal totalPaymentAmount) {
		this.totalPaymentAmount = totalPaymentAmount;
	}

	public PaymentOptions getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(PaymentOptions paymentOptions) {
		this.paymentOptions = paymentOptions;
	}

}
