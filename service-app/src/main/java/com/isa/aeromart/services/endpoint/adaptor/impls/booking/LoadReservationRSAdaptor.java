package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.adaptor.impls.passenger.PassengerResAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.booking.FlexiInfo;
import com.isa.aeromart.services.endpoint.dto.booking.LoadReservationRS;
import com.isa.aeromart.services.endpoint.dto.booking.PaxTicketCoupons;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationAlert;
import com.isa.aeromart.services.endpoint.dto.booking.ReservationSegment;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;
import com.isa.thinair.commons.api.exception.ModuleException;

public class LoadReservationRSAdaptor extends LoadReservationAdaptor<LoadReservationRS> {

	private RPHGenerator rphGenerator;

	private String carURL;

	private String customerID;

	private boolean isGroupPnr;

	private Map<String, Boolean> groundSegRef;

	CurrencyExchangeRate currencyExchangeRate;
	
	private final static String YES = "Y";
	
	private final static String NO = "N";

	public LoadReservationRSAdaptor(boolean isGroupPnr, String carURL, String customerID, Map<String, Boolean> groundSegRef,
			CurrencyExchangeRate currencyExchangeRate) {
		super();
		this.rphGenerator = new RPHGenerator();
		this.carURL = carURL;
		this.customerID = customerID;
		this.isGroupPnr = isGroupPnr;
		this.groundSegRef = groundSegRef;
		this.currencyExchangeRate = currencyExchangeRate;
	}

	@Override
	public LoadReservationRS adapt(LCCClientReservation reservation) {
		LoadReservationRS loadReservationRS = new LoadReservationRS();
		Set<LCCClientReservationPax> lccClientReservationPassengers = reservation.getPassengers();

		PassengerResAdaptor passengerResAdaptor = new PassengerResAdaptor();
		ReservationContactResAdaptor reservationContactResAdaptor = new ReservationContactResAdaptor();
		BookingInfoResAdaptor bookingInfoResAdaptor = new BookingInfoResAdaptor();
		PromoInfoAdaptor promoInfoAdaptor = new PromoInfoAdaptor(reservation);
		ModificationParamAdaptor modificationParamsAdaptor = new ModificationParamAdaptor(isGroupPnr, customerID != null,
				groundSegRef);
		BalanceSummaryAdaptor balanceSummaryAdaptor = new BalanceSummaryAdaptor(currencyExchangeRate);
		ReservationSegmentResAdaptor reservationSegmentResAdaptor = new ReservationSegmentResAdaptor(rphGenerator);
		PaxTicketCouponAdaptor paxTicketCouponAdaptor = new PaxTicketCouponAdaptor(rphGenerator);
		PaymentDetailsAdaptor paymentDetailsAdaptor = new PaymentDetailsAdaptor();
		
		List<Passenger> passengers = new ArrayList<>();
		List<ReservationSegment> reservationSegments = new ArrayList<>();
		List<ReservationAlert> reservationAlertInfo = new ArrayList<>();
		List<PaxTicketCoupons> paxTicketCoupons = new ArrayList<>();
		Map<String, String> resSegIdToResSegPHPMap = new HashMap<String, String>();

		reservationAlertInfo.addAll(getAlertInfo(reservation.getAlertInfoTOs(), null));
		reservationAlertInfo.addAll(getAlertInfo(reservation.getFlexiAlertInfoTOs(), LCCClientAlertInfoTO.alertType.FLEXI));
		reservationAlertInfo.addAll(getAlertInfo(reservation.getSegmentInfoTOs(), LCCClientAlertInfoTO.alertType.GROUND_SERVICE));
		reservationAlertInfo
				.addAll(getAlertInfo(reservation.getOpenReturnSegmentInfoTOs(), LCCClientAlertInfoTO.alertType.OPEN_RETURN));
		
		
		AdaptorUtils.adaptCollection(lccClientReservationPassengers, passengers, passengerResAdaptor);
		List<LCCClientReservationSegment> sortedResSegList = new ArrayList<>(reservation.getSegments());
		Collections.sort(sortedResSegList);
		setReturnFlagForSegments(sortedResSegList);
		for (LCCClientReservationSegment segment : sortedResSegList) {
			if (segment.getSubStatus() == null || !segment.getSubStatus().equals(ReservationSegmentSubStatus.EXCHANGED)) {
				ReservationSegment resSeg = reservationSegmentResAdaptor.adapt(segment);
				resSegIdToResSegPHPMap.put(segment.getBookingFlightSegmentRefNumber(), resSeg.getReservationSegmentRPH());
				reservationSegments.add(resSeg);
			}
		}
		Collections.sort(reservationSegments);

		AdaptorUtils.adaptCollection(lccClientReservationPassengers, paxTicketCoupons, paxTicketCouponAdaptor);
		
		if (reservation.getLccPromotionInfoTO() != null) {
			loadReservationRS.setPromoInfo(promoInfoAdaptor.adapt(reservation.getLccPromotionInfoTO()));
		}

		if(reservation.getFlexiAlertInfoTOs()!=null){
			FlexiInfoAdaptor flexiInfoAdaptor = new FlexiInfoAdaptor(resSegIdToResSegPHPMap, reservationSegments);
			List<FlexiInfo> flexiInfoList = new ArrayList<FlexiInfo>();
				AdaptorUtils.adaptCollection(reservation.getFlexiAlertInfoTOs(), flexiInfoList, flexiInfoAdaptor);
			loadReservationRS.setFlexiInfoList(flexiInfoList);
		}

		loadReservationRS.setPaymentDetails(paymentDetailsAdaptor.adapt(reservation));
		loadReservationRS.setContactInfo(reservationContactResAdaptor.adapt(reservation.getContactInfo()));
		loadReservationRS.setReservationAlerts(reservationAlertInfo);
		
		loadReservationRS.setPassengers(passengers);
		
		loadReservationRS.setReservationSegments(reservationSegments);
		loadReservationRS.setBookingInfoTO(bookingInfoResAdaptor.adapt(reservation));
		loadReservationRS.setModificationParams(modificationParamsAdaptor.adapt(reservation));
		loadReservationRS.setBalanceSummary(balanceSummaryAdaptor.adapt(reservation));
		loadReservationRS.setPaxTicketCoupons(paxTicketCoupons);
		loadReservationRS.setAdvertisements(getAdvertisements(carURL, null));

		PriceQuoteAncillaryAdaptor priceQuoteAncillaryAdaptor = new PriceQuoteAncillaryAdaptor(loadReservationRS, rphGenerator);
		loadReservationRS = priceQuoteAncillaryAdaptor.adapt(reservation);

		Collections.sort(loadReservationRS.getPassengers());

		loadReservationRS.setSuccessfullRefund(reservation.isSuccessfulRefund());
		loadReservationRS.setTaxInvoiceGenerated(isTaxInvoiceAvailable(reservation.getPNR()));
		loadReservationRS.setReservationType(ReservationApiUtils.getReservationType(sortedResSegList));

		return loadReservationRS;
	}

	private List<ReservationAlert> getAlertInfo(List<LCCClientAlertInfoTO> alertInfoTOs, String alertType) {
		ReservationAlertAdaptor reservationAlertAdaptor = new ReservationAlertAdaptor(alertType);
		List<ReservationAlert> reservationAlertInfo = new ArrayList<>();
		if (alertInfoTOs != null && !alertInfoTOs.isEmpty()) {
			AdaptorUtils.adaptCollection(alertInfoTOs, reservationAlertInfo, reservationAlertAdaptor);
		}
		return reservationAlertInfo;
	}
	
	private boolean isTaxInvoiceAvailable(String pnr) {
		boolean isTaxInvoiceAvailable = false;
		List<TaxInvoice> taxInvoices = null;
		try {
			taxInvoices = ModuleServiceLocator.getReservationBD().getTaxInvoiceForPrintInvoice(pnr);
		} catch (ModuleException e) {
			e.printStackTrace();
		}
		if (taxInvoices != null && !taxInvoices.isEmpty()) {
			isTaxInvoiceAvailable = true;
		}
		return isTaxInvoiceAvailable;
	}
	
	private void setReturnFlagForSegments(List<LCCClientReservationSegment> resSegmentList) {
		Map<String, String> segmentReturnMap = ReservationApiUtils.getSegmentReturnMap(resSegmentList);
		if (segmentReturnMap != null && !segmentReturnMap.isEmpty()) {
			for (LCCClientReservationSegment resSegment : resSegmentList) {
				String returnFlag = ReservationApiUtils.getReturnFlag(segmentReturnMap, resSegment);
				resSegment.setReturnFlag(returnFlag);
			}
		}
	}

}
