package com.isa.aeromart.services.endpoint.dto.ancillary;

//import java.util.List;

public class Ancillary {

	private String type;

//TODO implement with concrete class
//	private List<AvailabilityCustomization> customizations;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

//	public List<AvailabilityCustomization> getCustomizations() {
//		return customizations;
//	}
//
//	public void setCustomizations(List<AvailabilityCustomization> customizations) {
//		this.customizations = customizations;
//	}
}
