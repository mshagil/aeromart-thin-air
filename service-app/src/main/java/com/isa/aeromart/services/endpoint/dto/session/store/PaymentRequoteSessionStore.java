package com.isa.aeromart.services.endpoint.dto.session.store;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.financial.FinancialStore;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.FareInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;

public interface PaymentRequoteSessionStore extends PaymentBaseSessionStore {
	public static String SESSION_KEY = RequoteSessionStore.SESSION_KEY;

	public BigDecimal getBalanceToPayAmount();

	public Collection<LCCClientPassengerSummaryTO> getPassengerSummaryList();

	public ReservationInfo getResInfo();

	public List<SessionFlightSegment> getSegments();
	
	public FinancialStore getFinancialStore();
	
	public FareInfo getFareInfo();

}
