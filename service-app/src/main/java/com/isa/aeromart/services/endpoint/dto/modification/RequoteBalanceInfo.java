package com.isa.aeromart.services.endpoint.dto.modification;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class RequoteBalanceInfo {
	
	private BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal modificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal totalCreditBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal cancelationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal totalReservationCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap;
	
	private Collection<LCCClientPassengerSummaryTO> passengerSummaryList;	

	public BigDecimal getBalanceToPay() {
		return balanceToPay;
	}

	public void setBalanceToPay(BigDecimal balanceToPay) {
		this.balanceToPay = balanceToPay;
	}

	public BigDecimal getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(BigDecimal modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	public BigDecimal getTotalCreditBalance() {
		return totalCreditBalance;
	}

	public void setTotalCreditBalance(BigDecimal totalCreditBalance) {
		this.totalCreditBalance = totalCreditBalance;
	}

	public BigDecimal getTotalReservationCredit() {
		return totalReservationCredit;
	}

	public void setTotalReservationCredit(BigDecimal totalReservationCredit) {
		this.totalReservationCredit = totalReservationCredit;
	}

	public BigDecimal getCancelationCharge() {
		return cancelationCharge;
	}

	public void setCancelationCharge(BigDecimal cancelationCharge) {
		this.cancelationCharge = cancelationCharge;
	}

	public Map<Integer, BigDecimal> getPaxWiseAdjustmentAmountMap() {
		if(paxWiseAdjustmentAmountMap == null){
			return new HashMap<Integer, BigDecimal>();
		}
		return paxWiseAdjustmentAmountMap;
	}

	public void setPaxWiseAdjustmentAmountMap(Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap) {
		this.paxWiseAdjustmentAmountMap = paxWiseAdjustmentAmountMap;
	}

	public Collection<LCCClientPassengerSummaryTO> getPassengerSummaryList() {
		if(passengerSummaryList == null){
			return new ArrayList<LCCClientPassengerSummaryTO>();
		}
		return passengerSummaryList;
	}

	public void setPassengerSummaryList(Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {
		this.passengerSummaryList = passengerSummaryList;
	}	
	
	
	
}
