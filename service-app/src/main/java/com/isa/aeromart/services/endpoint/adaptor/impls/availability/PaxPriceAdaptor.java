package com.isa.aeromart.services.endpoint.adaptor.impls.availability;

import java.math.BigDecimal;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.availability.PaxPrice;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class PaxPriceAdaptor implements Adaptor<PerPaxPriceInfoTO, PaxPrice> {

	private TravellerQuantity travellerQuantity;

	public PaxPriceAdaptor(TravellerQuantity travellerQuantity) {
		this.travellerQuantity = travellerQuantity;
	}
	@Override
	public PaxPrice adapt(PerPaxPriceInfoTO source) {
		PaxPrice target = new PaxPrice();
		target.setPaxType(source.getPassengerType());
		target.setFare(source.getPassengerPrice().getTotalBaseFare());
		target.setSurcharge(source.getPassengerPrice().getTotalSurcharges());
		target.setTax(source.getPassengerPrice().getTotalTaxes());
		target.setTotal(source.getPassengerPrice().getTotalPriceWithInfant());
		if (target.getPaxType().equalsIgnoreCase(PaxTypeTO.ADULT)) {
			target.setNoOfPax(travellerQuantity.getAdultCount());
		} else if (target.getPaxType().equalsIgnoreCase(PaxTypeTO.CHILD)) {
			target.setNoOfPax(travellerQuantity.getChildCount());
		} else if (target.getPaxType().equalsIgnoreCase(PaxTypeTO.INFANT)) {
			target.setNoOfPax(travellerQuantity.getInfantCount());
		}
		target.setTotalFare(target.getFare().multiply(new BigDecimal(target.getNoOfPax())));

		return target;
	}

}
