package com.isa.aeromart.services.endpoint.dto.ancillary.integrate;

import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;

public class AncillaryUpdateRequestTO {
    private CommonAncillaryModifyAssembler anciAssembler;
    private boolean enableFraudCheck;
    private CommonReservationContactInfo contactInfo;
    private boolean isInterlinePaymentAllowed;
    private Map<String, Set<String>> flightSegWiseSelectedMeals;

    public CommonAncillaryModifyAssembler getAnciAssembler() {
        return anciAssembler;
    }

    public void setAnciAssembler(CommonAncillaryModifyAssembler anciAssembler) {
        this.anciAssembler = anciAssembler;
    }

    public boolean isEnableFraudCheck() {
        return enableFraudCheck;
    }

    public void setEnableFraudCheck(boolean enableFraudCheck) {
        this.enableFraudCheck = enableFraudCheck;
    }

    public CommonReservationContactInfo getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(CommonReservationContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    public boolean isInterlinePaymentAllowed() {
        return isInterlinePaymentAllowed;
    }

    public void setInterlinePaymentAllowed(boolean isInterlinePaymentAllowed) {
        this.isInterlinePaymentAllowed = isInterlinePaymentAllowed;
    }

    public Map<String, Set<String>> getFlightSegWiseSelectedMeals() {
        return flightSegWiseSelectedMeals;
    }

    public void setFlightSegWiseSelectedMeals(Map<String, Set<String>> flightSegWiseSelectedMeals) {
        this.flightSegWiseSelectedMeals = flightSegWiseSelectedMeals;
    }
}
