package com.isa.aeromart.services.endpoint.dto.customer;

import java.util.Collection;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;


public class TravelHistoryRS extends TransactionalBaseRS{
	
	private Collection<ReservationDetails> ReservationDetails = null;


	public Collection<ReservationDetails> getReservationDetails() {
		return ReservationDetails;
	}

	public void setReservationDetails(Collection<ReservationDetails> reservationDetails) {
		ReservationDetails = reservationDetails;
	}

}
