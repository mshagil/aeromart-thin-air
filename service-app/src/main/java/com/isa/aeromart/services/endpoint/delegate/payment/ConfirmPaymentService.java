package com.isa.aeromart.services.endpoint.delegate.payment;

import com.isa.aeromart.services.endpoint.dto.payment.IPGHandlerRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentConfirmationRS;
import com.isa.aeromart.services.endpoint.dto.payment.cmiFatourati.CmiFatouratiOfflinePaymentConfirmationRequest;
import com.isa.aeromart.services.endpoint.dto.payment.cmiFatourati.CmiFatouratiOfflinePaymentConfirmationResponse;
import com.isa.aeromart.services.endpoint.dto.payment.tapOffline.TapOfflinePaymentConfirmRequest;
import com.isa.aeromart.services.endpoint.dto.payment.tapOffline.TapOfflinePaymentConfirmResponse;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

import java.util.Map;

public interface ConfirmPaymentService {

	PaymentConfirmationRS processPaymentReceipt(String transactionId, IPGHandlerRQ ipgHandlerRequest, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	TapOfflinePaymentConfirmResponse confirmTapOfflinePayment(TapOfflinePaymentConfirmRequest request, TrackInfoDTO trackInfoDTO);

	Map<String, String> confirmPayFortOfflinePayment(Map<String, String> requestData);

	CmiFatouratiOfflinePaymentConfirmationResponse confirmCmiFatouratiOfflinePayment(
			CmiFatouratiOfflinePaymentConfirmationRequest confirmRequest,
			TrackInfoDTO trackInfo);


	public PaymentConfirmationRS processPaymentReceiptForGiftVoucher(String transactionId, IPGHandlerRQ ipgHandlerRequest,
			TrackInfoDTO trackInfoDTO) throws ModuleException;
}
