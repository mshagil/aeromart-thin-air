package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.common.AddressResTransformer;
import com.isa.aeromart.services.endpoint.adaptor.impls.common.PhoneNumberResTransformer;
import com.isa.aeromart.services.endpoint.dto.common.ReservationContact;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;

public class ReservationContactResAdaptor implements Adaptor<CommonReservationContactInfo, ReservationContact> {

	@Override
	public ReservationContact adapt(CommonReservationContactInfo commonReservationContactInfo) {
		ReservationContact reservationContact = new ReservationContact();

		AddressResTransformer addressResTransformer = new AddressResTransformer();
		PhoneNumberResTransformer phoneNumberResTransformer = new PhoneNumberResTransformer();
		EmergencyContactResAdaptor emergencyContactResTransformer = new EmergencyContactResAdaptor();

		reservationContact.setAddress(addressResTransformer.adapt(commonReservationContactInfo));
		reservationContact.setCountry(commonReservationContactInfo.getCountryCode());
		reservationContact.setEmailAddress(commonReservationContactInfo.getEmail());
		reservationContact.setEmergencyContact(emergencyContactResTransformer.adapt(commonReservationContactInfo));
		reservationContact.setFax(phoneNumberResTransformer.adapt(commonReservationContactInfo.getFax()));
		reservationContact.setFirstName(commonReservationContactInfo.getFirstName());
		reservationContact.setLastName(commonReservationContactInfo.getLastName());
		reservationContact.setMobileNumber(phoneNumberResTransformer.adapt(commonReservationContactInfo.getMobileNo()));
		if (commonReservationContactInfo.getNationalityCode() != null) {
			reservationContact.setNationality(commonReservationContactInfo.getNationalityCode());
		}
		reservationContact.setPreferredLangauge(commonReservationContactInfo.getPreferredLanguage());
		reservationContact.setTelephoneNumber(phoneNumberResTransformer.adapt(commonReservationContactInfo.getPhoneNo()));
		reservationContact.setTitle(commonReservationContactInfo.getTitle());
		reservationContact.setZipCode(commonReservationContactInfo.getZipCode());
		reservationContact.setState(commonReservationContactInfo.getState());
		reservationContact.setTaxRegNo(commonReservationContactInfo.getTaxRegNo());
		reservationContact.setSendPromoEmail(commonReservationContactInfo.getSendPromoEmail());

		return reservationContact;
	}

}
