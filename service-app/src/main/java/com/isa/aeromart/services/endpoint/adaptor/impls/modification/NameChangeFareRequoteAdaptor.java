package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.TravelerQuantityAdaptor;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.masterdata.LccResSegmentInfo;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class NameChangeFareRequoteAdaptor implements Adaptor<LCCClientReservation, FlightPriceRQ> {

	private SYSTEM system;

	public NameChangeFareRequoteAdaptor(SYSTEM system) {
		this.system = system;
	}

	@Override
	public FlightPriceRQ adapt(LCCClientReservation reservation) {

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		Map<String, List<LccResSegmentInfo>> ondWiseSegMap = ModificationUtils.getOndWiseSegmentMap(reservation
				.getSegments());
		Map<Integer, Boolean> quoteOndFlexi = new HashMap<Integer, Boolean>();
		
		int ondSeq = 0;
		List<LccResSegmentInfo> ondResSegList;
		for (String key : ondWiseSegMap.keySet()) {
			OriginDestinationInformationTO ondInfoTO = flightPriceRQ.addNewOriginDestinationInformation();
			ondResSegList = ondWiseSegMap.get(key);

			LccResSegmentInfo firstSegment = ondResSegList.get(0);
			LccResSegmentInfo lastSegment = ondResSegList.get(ondResSegList.size() - 1);
			String origin = SegmentUtil.getFromAirport(firstSegment.getSegmentCode());
			String destination = SegmentUtil.getToAirport(lastSegment.getSegmentCode());
			ondInfoTO.setOrigin(origin);
			ondInfoTO.setDestination(destination);

			int departureVariance = 0; // AppSysParamsUtil.getMaxDepartureReturnVarience(AppIndicatorEnum.APP_IBE);
			Date depatureDate = firstSegment.getDepartureDate();
			Date depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(CalendarUtil.getStartTimeOfDate(depatureDate),
					departureVariance * -1);
			Date depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(CalendarUtil.getEndTimeOfDate(depatureDate),
					departureVariance);

			ondInfoTO.setPreferredDate(depatureDate);
			ondInfoTO.setDepartureDateTimeStart(depatureDateTimeStart);
			ondInfoTO.setDepartureDateTimeEnd(depatureDateTimeEnd);
			ondInfoTO.setPreferredLogicalCabin(firstSegment.getLogicalCabinClass());
			ondInfoTO.setPreferredClassOfService(firstSegment.getCabinClass());
			ondInfoTO.setPreferredBookingType(firstSegment.getBookingType());
			ondInfoTO.setPreferredBundleFarePeriodId(firstSegment.getBundleFarePeriodId());
			ondInfoTO.setSameFlightModification(true);

			List<FareTO> perPaxFareToList = new ArrayList<FareTO>();
			List<String> flightRphList = new ArrayList<String>();
			List<FlightSegmentTO> fltSegToLst = new ArrayList<FlightSegmentTO>();
			List<Integer> flightSegmentIds = new ArrayList<Integer>();
			List<String> resSegRPHList = new ArrayList<String>();
			
			boolean isFlown = false;

			for (LccResSegmentInfo resSeg : ondResSegList) {

				FlightSegmentTO flightSegementTO = new FlightSegmentTO();
				flightSegementTO.setFlightRefNumber(resSeg.getFlightSegmentRefNumber());
				flightSegementTO.setSegmentCode(resSeg.getSegmentCode());
				flightSegementTO.setFlightNumber(resSeg.getFlightNo());
				flightSegementTO.setRouteRefNumber(resSeg.getRouteRefNumber());
				fltSegToLst.add(flightSegementTO);

				perPaxFareToList.add(resSeg.getFareTO());
				flightRphList.add(resSeg.getFlightSegmentRefNumber());
				resSegRPHList.add(ModificationUtils.createResSegUnqIdatifier(resSeg.getFlightSegmentRefNumber(),
						resSeg.getPnrSegID()));
				flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber()));

				
				if (resSeg.isFlownSegment()) {
					isFlown = true;
				}
			}
			
			if (firstSegment.getBundleFarePeriodId() != null) {
				ondInfoTO.setPreferredBundleFarePeriodId(firstSegment.getBundleFarePeriodId());
			}

			if (AppSysParamsUtil.isSameFareModificationEnabled() || AppSysParamsUtil.isEnforceSameHigher()) {
				ondInfoTO.setOldPerPaxFareTOList(perPaxFareToList);
				ondInfoTO.setEligibleToSameBCMod(false);
			}

			//isFlexi = firstSegment.getFlexiRuleID() != null ? true : false;
			quoteOndFlexi.put(ondSeq, true);
			ondSeq++;
			
			OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
			depOndOptionTO.setFlightSegmentList(fltSegToLst);
			ondInfoTO.getExistingFlightSegIds().addAll(flightSegmentIds);
			ondInfoTO.getExistingPnrSegRPHs().addAll(resSegRPHList);
			ondInfoTO.setFlownOnd(isFlown);
			ondInfoTO.getOrignDestinationOptions().add(depOndOptionTO);
		}

		TravelerQuantityAdaptor travelerQuantityAdaptor = new TravelerQuantityAdaptor(flightPriceRQ.getTravelerInfoSummary());
		TravellerQuantity travelerQuantity = new TravellerQuantity();
		travelerQuantity.setAdultCount(reservation.getTotalPaxAdultCount());
		travelerQuantity.setChildCount(reservation.getTotalPaxChildCount());
		travelerQuantity.setInfantCount(reservation.getTotalPaxInfantCount());
		travelerQuantityAdaptor.adapt(travelerQuantity);

		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		availPref.setTicketValidTill(reservation.getTicketValidTill());
		availPref.setRequoteFlightSearch(true);
		availPref.setQuoteOndFlexi(quoteOndFlexi);
		availPref.setFromNameChange(true);
		availPref.setSearchSystem(system);
		availPref.setAppIndicator(ApplicationEngine.IBE);
		availPref.setPnr(reservation.getPNR());
		return flightPriceRQ;
	}
}