package com.isa.aeromart.services.endpoint.dto.session.store;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.session.BookingTransaction;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;

public interface PassengerSessionStore {
	public static String SESSION_KEY = BookingTransaction.SESSION_KEY;

	public List<SessionFlightSegment> getSegments();
}
