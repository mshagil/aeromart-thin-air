package com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.AncillariesResponseAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciAvailability;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.RPHGenerator;
import com.isa.aeromart.services.endpoint.delegate.ancillary.TransactionalAncillaryService;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryAvailability;
import com.isa.aeromart.services.endpoint.dto.ancillary.AncillaryType;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability;
import com.isa.aeromart.services.endpoint.dto.ancillary.Applicability.ApplicableType;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillary;
import com.isa.aeromart.services.endpoint.dto.ancillary.AvailableAncillaryUnit;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge;
import com.isa.aeromart.services.endpoint.dto.ancillary.Charge.ChargeBasis;
import com.isa.aeromart.services.endpoint.dto.ancillary.PricedSelectedItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.Provider;
import com.isa.aeromart.services.endpoint.dto.ancillary.Rule;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope;
import com.isa.aeromart.services.endpoint.dto.ancillary.Scope.ScopeType;
import com.isa.aeromart.services.endpoint.dto.ancillary.provider.ThirdPartyProvider;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItem;
import com.isa.aeromart.services.endpoint.dto.ancillary.type.insurance.InsuranceItems;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.BasicRuleValidation;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.InsurancePlansCombinations;
import com.isa.aeromart.services.endpoint.dto.ancillary.validation.InsurancePlansGroup;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class InsuranseResponseAdaptor extends AncillariesResponseAdaptor {
	@Override
	public AncillaryAvailability toAncillariesAvailability(List<LCCAncillaryAvailabilityOutDTO> availability) {
		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();

		boolean isAvailable = AnciAvailability.isAnciActive(availability,
				LCCAncillaryAvailabilityInDTO.LCCAncillaryType.INSURANCE);
		ancillaryAvailability.setAvailable(isAvailable);

		return ancillaryAvailability;
	}

	@Override
	public AncillaryType
			toAvailableAncillaryItems(Object ancillaries, RPHGenerator rphGenerator, boolean anciModify, MessageSource errorMessageSource) {

		Map<INSURANCEPROVIDER, Provider> insuranceProviderMap = new HashMap<INSURANCEPROVIDER, Provider>();
		AncillaryType ancillaryType = new AncillaryType();
		Applicability applicability = new Applicability();
		Scope scope = new Scope();

		ThirdPartyProvider provider;
		AvailableAncillary availableAncillary;
		AvailableAncillaryUnit availableAncillaryUnit;
		InsuranceItems insuranceItems;
		List<Charge> charges;
		Charge charge;
		InsuranceItem insuranceItem;

		InsurancePlansCombinations insurancePlansCombinations;
		InsurancePlansGroup insurancePlansGroup;
		Map<String, InsurancePlansGroup> insurancePlansGroupMap;

		int ruleId = 1;

		ancillaryType.setProviders(new ArrayList<Provider>());
		ancillaryType.setAncillaryType(AncillariesConstants.AncillaryType.INSURANCE.name());
		applicability.setApplicableType(ApplicableType.ALL_PASSENGERS.name());
		scope.setScopeType(ScopeType.ALL_SEGMENTS);

		List<LCCInsuranceQuotationDTO> source = (List<LCCInsuranceQuotationDTO>) ancillaries;

		// support multiple insurance
		// can remove when back end support with provider id
		List<INSURANCEPROVIDER> insuranceProviders = AppSysParamsUtil.getInsuranceProvidersList();
		if (insuranceProviders != null && !insuranceProviders.isEmpty()) {
			for (INSURANCEPROVIDER insuranceProvider : insuranceProviders) {

				provider = new ThirdPartyProvider();
				availableAncillaryUnit = new AvailableAncillaryUnit();
				insuranceItems = new InsuranceItems();

				provider.setProviderType(Provider.ProviderType.THIRD_PARTY);
				provider.setAvailableAncillaries(new AvailableAncillary());
				provider.setProviderDescription(insuranceProvider.name());
				// provider.setProviderId();
				// provider.setProviderDescription();
				provider.getAvailableAncillaries().setAvailableUnits(new ArrayList<AvailableAncillaryUnit>());
				availableAncillaryUnit.setApplicability(applicability);
				availableAncillaryUnit.setScope(scope);
				insuranceItems.setItems(new ArrayList<InsuranceItem>());
				availableAncillaryUnit.setItemsGroup(insuranceItems);

				// Since all the insurance scopes are 'ALL_SEGMENTS', only AvailableAncillaryUnit for whole reservation
				// for each provider
				provider.getAvailableAncillaries().getAvailableUnits().add(availableAncillaryUnit);
				insuranceProviderMap.put(insuranceProvider, provider);
			}
		}

		insurancePlansCombinations = new InsurancePlansCombinations();
		insurancePlansCombinations.setInsurancePlanGroups(new ArrayList<>());
		insurancePlansCombinations.setRuleValidationId(ruleId++);
		insurancePlansGroupMap = new HashMap<>();
		if (!source.isEmpty()) {
			for (LCCInsuranceQuotationDTO lccInsuranceQuotationDTO : source) {

				insuranceItem = new InsuranceItem();
				insuranceItem.setItemId(lccInsuranceQuotationDTO.getInsuranceRefNumber());
				insuranceItem.setInsuranceRefNumber(lccInsuranceQuotationDTO.getInsuranceRefNumber());
				insuranceItem.setFeeCode(lccInsuranceQuotationDTO.getSsrFeeCode());
				// insuranceItem.setOperatingAirline(lccInsuranceQuotationDTO.getOperatingAirline());
				insuranceItem.setPlanCode(lccInsuranceQuotationDTO.getPlanCode());
				// insuranceItem.setQuotedCurrencyCode(lccInsuranceQuotationDTO.getQuotedCurrencyCode());
				insuranceItem.setItemName(lccInsuranceQuotationDTO.getInsuranceExternalContent().get(
						CommonsConstants.InsuranceExternalContenKey.PLAN_TITEL.name()));
				insuranceItem.setDescription(lccInsuranceQuotationDTO.getInsuranceExternalContent().get(
						CommonsConstants.InsuranceExternalContenKey.PLAN_DESC.name()));
				insuranceItem.setPlanCover(lccInsuranceQuotationDTO.getInsuranceExternalContent().get(
						CommonsConstants.InsuranceExternalContenKey.PLAN_COVERED_INFOMATION_DESCRIPTION.name()));
				insuranceItem.setTermsAndConditions(lccInsuranceQuotationDTO.getInsuranceExternalContent().get(
						CommonsConstants.InsuranceExternalContenKey.PLAN_TERMS_AND_CONDITIONS.name()));

				charges = new ArrayList<>();
				charge = new Charge();
				charge.setChargeBasis(ChargeBasis.ALL_PASSENGERS.name());
				charge.setAmount(lccInsuranceQuotationDTO.getQuotedTotalPremiumAmount());
				charges.add(charge);
				insuranceItem.setCharges(charges);
				insuranceItem.setDefaultSelection(lccInsuranceQuotationDTO.isEuropianCountry() == true ? false : true);

				if (!insurancePlansGroupMap.containsKey(String.valueOf(lccInsuranceQuotationDTO.getGroupID()))) {

					insurancePlansGroup = new InsurancePlansGroup();
					insurancePlansGroup.setItemIds(new ArrayList<>());
					insurancePlansGroup.setMinSelections(0);
					insurancePlansGroup.setMaxSelections(1);
					insurancePlansCombinations.getInsurancePlanGroups().add(insurancePlansGroup);
					insurancePlansCombinations.setRuleMessage(errorMessageSource.getMessage(
							BasicRuleValidation.RuleCode.INSURANCE_PLANS_COMBINATIONS_DESCRIPTION, null,
							LocaleContextHolder.getLocale()));
					insurancePlansGroupMap.put(String.valueOf(lccInsuranceQuotationDTO.getGroupID()), insurancePlansGroup);
				}
				insurancePlansGroup = insurancePlansGroupMap.get(String.valueOf(lccInsuranceQuotationDTO.getGroupID()));
				insurancePlansGroup.getItemIds().add(lccInsuranceQuotationDTO.getInsuranceRefNumber());

				insuranceItems = (InsuranceItems) insuranceProviderMap.get(INSURANCEPROVIDER.TUNE).getAvailableAncillaries()
						.getAvailableUnits().get(0).getItemsGroup();
				insuranceItems.getItems().add(insuranceItem);
				insuranceProviderMap.get(INSURANCEPROVIDER.TUNE).getAvailableAncillaries().getAvailableUnits().get(0)
						.setItemsGroup(insuranceItems);

			}
		} else {
			return ancillaryType;
		}

		availableAncillary = insuranceProviderMap.get(INSURANCEPROVIDER.TUNE).getAvailableAncillaries();
		List<Rule> rules = new ArrayList<>();
		rules.add(insurancePlansCombinations);
		availableAncillary.setValidationDefinitions(rules);

		availableAncillaryUnit = availableAncillary.getAvailableUnits().get(0);

		List<Integer> validations = new ArrayList<>();
		validations.add(insurancePlansCombinations.getRuleValidationId());
		availableAncillaryUnit.setValidations(validations);

		for (Provider providerRef : insuranceProviderMap.values()) {
			ancillaryType.getProviders().add(providerRef);
		}

		return ancillaryType;

	}

	public Object toAncillaryItemModel(Scope scope, PricedSelectedItem item) {

		LCCInsuranceQuotationDTO lccInsuranceQuoteDto = new LCCInsuranceQuotationDTO();
		lccInsuranceQuoteDto.setPlanCode(item.getId());
		lccInsuranceQuoteDto.setDisplayTotalPremiumAmount(item.getAmount());
		return lccInsuranceQuoteDto;

	}

	@Override
	public void updateSessionData(Object ancillaries, TransactionalAncillaryService transactionalAncillaryService,
			String transactionId) {

	}
}
