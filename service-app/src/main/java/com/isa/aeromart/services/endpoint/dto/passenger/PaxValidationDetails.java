package com.isa.aeromart.services.endpoint.dto.passenger;

public class PaxValidationDetails {

	private int paxSequence;

	private boolean validFFID;

	private boolean validName;

	private boolean validPaxType;

	private boolean emailConfirmed;

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public boolean isValidFFID() {
		return validFFID;
	}

	public void setValidFFID(boolean validFFID) {
		this.validFFID = validFFID;
	}

	public boolean isValidName() {
		return validName;
	}

	public void setValidName(boolean validName) {
		this.validName = validName;
	}

	public boolean isValidPaxType() {
		return validPaxType;
	}

	public void setValidPaxType(boolean validPaxType) {
		this.validPaxType = validPaxType;
	}

	public boolean isEmailConfirmed() {
		return emailConfirmed;
	}

	public void setEmailConfirmed(boolean emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}


}
