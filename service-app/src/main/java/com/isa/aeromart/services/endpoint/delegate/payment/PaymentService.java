package com.isa.aeromart.services.endpoint.delegate.payment;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.ClientCommonInfoDTO;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRQ;
import com.isa.aeromart.services.endpoint.dto.payment.AdministrationFeeRS;
import com.isa.aeromart.services.endpoint.dto.payment.BinPromotionRQ;
import com.isa.aeromart.services.endpoint.dto.payment.EffectivePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRQ;
import com.isa.aeromart.services.endpoint.dto.payment.MakePaymentRS;
import com.isa.aeromart.services.endpoint.dto.payment.PayfortCheckRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PayfortCheckRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentMethodsRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentOptionsRS;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRQ;
import com.isa.aeromart.services.endpoint.dto.payment.PaymentSummaryRS;
import com.isa.aeromart.services.endpoint.dto.payment.ReservationChargesRQ;
import com.isa.aeromart.services.endpoint.dto.payment.ReservationChargesRS;
import com.isa.aeromart.services.endpoint.dto.payment.ServiceTaxRQ;
import com.isa.aeromart.services.endpoint.dto.payment.VoucherOption;
import com.isa.aeromart.services.endpoint.dto.session.store.GiftVoucherSessionStore;
import com.isa.aeromart.services.endpoint.dto.voucher.IssueVoucherRQ;
import com.isa.aeromart.services.endpoint.dto.voucher.VoucherRedeemRQ;
import com.isa.aeromart.services.endpoint.dto.voucher.VoucherRedeemRS;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;

public interface PaymentService {

	// payment methods
	PaymentMethodsRS getPaymentMethodsRS(PaymentMethodsRQ paymentMethodsRQ, TrackInfoDTO trackInfoDTO) throws ModuleException;

	AdministrationFeeRS getAdminFeeRS(BigDecimal amount, AdministrationFeeRQ adminFeeRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	// return payment breakdown for create flow
	PaymentSummaryRS getPaymentSummaryRS(PaymentSummaryRQ paymentSummaryRQ, TrackInfoDTO trackInfo) throws ModuleException;

	// Administration Fee service (only useful when ADMIN_FEE_REGULATION_ENABLED = "RES_209" is enabled, else zero
	// response and display set to false)
	AdministrationFeeRS getBalanceAdminFee(AdministrationFeeRQ adminFeeRQ, TrackInfoDTO trackInfo, String transactionId)
			throws ModuleException;

	AdministrationFeeRS getRequoteAdminFee(AdministrationFeeRQ adminFeeRQ, TrackInfoDTO trackInfo) throws ModuleException;

	AdministrationFeeRS getAnciModificationAdminFee(AdministrationFeeRQ adminFeeRQ, TrackInfoDTO trackInfo)
			throws ModuleException;

	// check PayFort in balance payment cases
	PayfortCheckRS checkPayfort(PayfortCheckRQ payfortCheckRQ, TrackInfoDTO trackInfo);

	// BIN promotion
	PaymentOptionsRS getBinPromotionRS(BinPromotionRQ binPromotionRQ, TrackInfoDTO trackInfoDTO) throws ModuleException;

	// on-hold release time
	Date calculateOnHoldReleaseTime(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfo) throws ModuleException;

	// payment options cases
	PaymentOptionsRS getPaymentOptionsRS(PaymentOptionsRQ paymentOptionsRQ, TrackInfoDTO trackInfoDTO) throws ModuleException;

	PaymentOptionsRS getBalancePaymentOptionsRS(PaymentOptionsRQ paymentOptionsRQ, TrackInfoDTO trackInfoDTO,
			String bookingTransactionId) throws ModuleException;

	PaymentOptionsRS getRequotePaymentOptionsRS(PaymentOptionsRQ paymentOptionsRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	PaymentOptionsRS getAnciModificationPaymentOptionsRS(PaymentOptionsRQ paymentOptionsRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	// effective payment options cases
	PaymentOptionsRS getEffectivePaymentRS(EffectivePaymentRQ effectivePaymentRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	PaymentOptionsRS getEffectiveBalancePaymentRS(EffectivePaymentRQ effectivePaymentRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	PaymentOptionsRS getEffectiveRequotePaymentRS(EffectivePaymentRQ effectivePaymentRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	PaymentOptionsRS getEffectiveAnciModificationPaymentRS(EffectivePaymentRQ effectivePaymentRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	// process payment cases
	MakePaymentRS processCreateBookingPayment(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException;

	TransactionalBaseRS processBalancePayment(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException;

	MakePaymentRS processModifyBookingPayment(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException;

	MakePaymentRS processModifyAnciPayment(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO,
			ClientCommonInfoDTO clientInfoDTO) throws ModuleException;

	void applyServiceTax(String transactionId, ServiceTaxRQ serviceTaxRQ, TrackInfoDTO trackInfoDTO) throws ModuleException;

	// return reservation charge break down breakdown for requote flow
	ReservationChargesRS getReservationChargeDetailsRS(ReservationChargesRQ reservationChargesRQ, TrackInfoDTO trackInfo)
			throws ModuleException;

	// for voucher payment through Voucher Controller
	MakePaymentRS processVoucherPayment(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO, ClientCommonInfoDTO clientInfoDTO)
			throws ModuleException;

	MakePaymentRS createAlias(MakePaymentRQ makePaymentRQ, TrackInfoDTO trackInfoDTO) throws ModuleException;

	PaymentOptionsRS getIssueVoucherPaymentOptionsRS(TrackInfoDTO trackInfo, PaymentOptionsRQ paymentOptionsRQ) throws ModuleException;

	VoucherRedeemRS redeemVouchers(VoucherRedeemRQ voucherRedeemRQ, TrackInfoDTO trackInfoDTO) throws ModuleException;

	void voidRedeemVoucher(VoucherOption voucherOption, String transactionId) throws ModuleException;

	MakePaymentRS processGiftVoucherPayment(TrackInfoDTO trackInfoDto,
			IssueVoucherRQ voucherRequest, ClientCommonInfoDTO clientCommonInfoDTO) throws ModuleException;

	public void setCreditCardFeesToVoucherTemplates(List<VoucherTemplateTo> voucherList) throws ModuleException;

	public GiftVoucherSessionStore getGiftVoucherStore (String transactionId);

}
