package com.isa.aeromart.services.endpoint.dto.ping;

import java.util.Date;

public class PingResponse {

	private String echoToken;

	private Integer counter;
	
	private Date responseTime;

	public String getEchoToken() {
		return echoToken;
	}

	public void setEchoToken(String echoToken) {
		this.echoToken = echoToken;
	}

	public Integer getCounter() {
		return counter;
	}

	public void setCounter(Integer counter) {
		this.counter = counter;
	}

	public Date getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Date responseTinme) {
		this.responseTime = responseTinme;
	}
	
}
