package com.isa.aeromart.services.endpoint.adaptor.impls.availability.response;

import static com.isa.aeromart.services.endpoint.adaptor.impls.availability.response.CommonRSAdaptor.getFareClasses;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.impls.availability.SelectedFlightPricingAdaptor;
import com.isa.aeromart.services.endpoint.dto.availability.FareQuoteRS;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.errorhandling.ValidationException.Code;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

public class FareQuoteRSAdaptor implements Adaptor<FlightPriceRS, FareQuoteRS> {

	private TravellerQuantity travellerQuantity;
	private TrackInfoDTO trackInfo;
	private SYSTEM searchSystem;
	private RPHGenerator rphGenerator;
	private Map<Integer, Boolean> ondFlexiSelected;
	private String currency;
	private String transactionId;
	private boolean isRequote;

	public FareQuoteRSAdaptor(TravellerQuantity travellerQuantity, TrackInfoDTO trackInfo, SYSTEM searchSystem,
			RPHGenerator rphGenerator, Map<Integer, Boolean> ondFlexiSelected, String currency, String transactionId,
			boolean isRequote) {
		this.travellerQuantity = travellerQuantity;
		this.trackInfo = trackInfo;
		this.searchSystem = searchSystem;
		this.rphGenerator = rphGenerator;
		this.ondFlexiSelected = ondFlexiSelected;
		this.currency = currency;
		this.transactionId = transactionId;
		this.isRequote = isRequote;
	}

	@Override
	public FareQuoteRS adapt(FlightPriceRS source) {
		FareQuoteRS response = new FareQuoteRS();
		if (source != null) {
			CalendarRSAdaptor flightCalendarAdaptor = CalendarRSAdaptor.getAdaptorForFareResponse(rphGenerator);
			response = new FareQuoteRS(flightCalendarAdaptor.adapt(source));
			if (source.getSelectedPriceFlightInfo() != null) {
				SelectedFlightPricingAdaptor selFlightPriceTrans = new SelectedFlightPricingAdaptor(travellerQuantity,
						source.getSelectedSegmentFlightPriceInfo(), searchSystem, trackInfo, source.getTransactionIdentifier());
				response.setSelectedFlightPricing(selFlightPriceTrans.adapt(source.getSelectedPriceFlightInfo()));
				response.setCurrency(currency);
				if (source.getSelectedPriceFlightInfo().getAvailableLogicalCCList() != null && !isRequote) {
					validateFlexiAvailability(ondFlexiSelected, source.getSelectedPriceFlightInfo().getAvailableLogicalCCList());
				}
			} else {
				throw new ValidationException(Code.SELECTED_FLIGHT_FARES_NOT_AVAILABLE);
			}
			response.setFareClasses(getFareClasses(source));
			// set transaction id to response
			if (source.getTransactionIdentifier() != null) {
				response.setTransactionId(source.getTransactionIdentifier());
			} else {
				response.setTransactionId(transactionId);
			}
			response.setSuccess(true);
		} else {
			response.setSuccess(false);
		}
		return response;

	}

	public void validateFlexiAvailability(Map<Integer, Boolean> quoteOndFlexiMap,
			List<OndClassOfServiceSummeryTO> availableLogicalCCList) throws ValidationException {
		if (quoteOndFlexiMap != null && quoteOndFlexiMap.size() > 0) {
			for (Entry<Integer, Boolean> entry : quoteOndFlexiMap.entrySet()) {
				if (availableLogicalCCList.size() > entry.getKey()) {
					OndClassOfServiceSummeryTO ondClassSummary = availableLogicalCCList.get(entry.getKey());
					if (entry.getValue() && ondClassSummary != null) {
						boolean hasFlexi = false;
						boolean isBundledServiceSelected = false;
						for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : ondClassSummary.getAvailableLogicalCCList()) {
							if (logicalCabinClassInfoTO.isWithFlexi() && logicalCabinClassInfoTO.getBundledFarePeriodId() == null) {
								hasFlexi = true;
							}
							if (logicalCabinClassInfoTO.getBundledFarePeriodId() != null && logicalCabinClassInfoTO.isSelected()) {
								isBundledServiceSelected = true;
							}
						}
						// Flexi validation should skip if bundled fare is selected
						if (!isBundledServiceSelected && !hasFlexi) {
							throw new ValidationException(ValidationException.Code.FLEXI_NOT_AVAILABLE);
						}
					}
				}
			}
		}
	}

}
