package com.isa.aeromart.services.endpoint.utils.booking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.aeromart.services.endpoint.adaptor.impls.ancillary.replicate.AnciCommon;
import com.isa.aeromart.services.endpoint.dto.common.Passenger;
import com.isa.aeromart.services.endpoint.dto.common.TravellerQuantity;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionExternalChargeSummary;
import com.isa.aeromart.services.endpoint.dto.session.impl.availability.SessionFlexiDetail;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.commons.api.constants.AncillariesConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class FlexiUtil {

	public static Map<Integer, List<LCCClientFlexiExternalChgDTO>> getPaxWiseSegmentWiseFlexiExternalCharges(
			List<SessionFlightSegment> sessionFlightSegments, TravellerQuantity travellerQuantity,
			List<SessionFlexiDetail> sessionFlexiDetailList, List<Passenger> passengers) {
		Map<Integer, List<LCCClientFlexiExternalChgDTO>> flexiExtChgMap = new HashMap<>();

		int nonInfants = travellerQuantity.getAdultCount() + travellerQuantity.getChildCount();

		for (Passenger pax : passengers) {
			if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				List<LCCClientFlexiExternalChgDTO> externalChgDTOList = new ArrayList<>();
				if (sessionFlexiDetailList != null && !sessionFlexiDetailList.isEmpty()) {
					for (SessionFlexiDetail sessionFlexiDetail : sessionFlexiDetailList) {
						externalChgDTOList.addAll(getPaxFlexiExternalCharges(sessionFlightSegments, nonInfants, pax,
								sessionFlexiDetail));
					}
				}
				flexiExtChgMap.put(pax.getPaxSequence(), externalChgDTOList);
			}
		}

		return flexiExtChgMap;

	}

	private static List<LCCClientFlexiExternalChgDTO> getPaxFlexiExternalCharges(List<SessionFlightSegment> sessionFlightSegments,
			int nonInfants, Passenger pax, SessionFlexiDetail sessionFlexiDetail) {
		List<LCCClientFlexiExternalChgDTO> externalChgDTOList = new ArrayList<>();
		if (sessionFlexiDetail.isFlexiAvailable() && sessionFlexiDetail.isFlexiSelected()) {
			for (SessionExternalChargeSummary sessionExternalChargeSummary : sessionFlexiDetail.getExternalChargeSummary()) {
				List<String> fltSegRefList = getFlightSegmentRPHList(sessionExternalChargeSummary.getSegmentCode(),
						sessionFlightSegments);
				int segmentCount = fltSegRefList.size();
				BigDecimal[] flexiPassengerQuota = AccelAeroCalculator
						.roundAndSplit(sessionExternalChargeSummary.getFlexiCharge(), nonInfants);
				BigDecimal[] flexiSegmentQuota = AccelAeroCalculator.roundAndSplit(flexiPassengerQuota[pax.getPaxSequence() - 1],
						segmentCount);

				populateSegmentWiseFlexiExtCharges(pax, externalChgDTOList, sessionExternalChargeSummary, fltSegRefList,
						flexiSegmentQuota);
			}
		}
		return externalChgDTOList;
	}

	private static void populateSegmentWiseFlexiExtCharges(Passenger pax, List<LCCClientFlexiExternalChgDTO> externalChgDTOList,
			SessionExternalChargeSummary sessionExternalChargeSummary, List<String> fltSegRefList,
			BigDecimal[] flexiSegmentQuota) {
		int segmentIndex = 0;
		for (String flightRefNumber : fltSegRefList) {
			if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				LCCClientFlexiExternalChgDTO tmpChgDTO = new LCCClientFlexiExternalChgDTO();
				tmpChgDTO
						.setExternalCharges(AnciCommon.getExternalCharge(AncillariesConstants.AncillaryType.FLEXIBILITY));
				tmpChgDTO.setCarrierCode(sessionExternalChargeSummary.getCarrierCode());
				tmpChgDTO.setCode(sessionExternalChargeSummary.getSurchargeCode());
				tmpChgDTO.setFlightRefNumber(flightRefNumber);
				tmpChgDTO.setSegmentCode(sessionExternalChargeSummary.getSegmentCode());
				tmpChgDTO.setAmount(flexiSegmentQuota[segmentIndex]);
				tmpChgDTO.setFlexiBilities(sessionExternalChargeSummary.getAdditionalDetails());
				externalChgDTOList.add(tmpChgDTO);
			}
			segmentIndex++;
		}
	}

	private static List<String> getFlightSegmentRPHList(String segmentCode, List<SessionFlightSegment> sessionFlightSegments) {
		List<String> fltSegRefList = new ArrayList<>();
		for (SessionFlightSegment flightSegment : sessionFlightSegments) {
			if (segmentCode.contains(flightSegment.getSegmentCode())) {
				fltSegRefList.add(flightSegment.getFlightRefNumber());
			}
		}
		return fltSegRefList;
	}

}
