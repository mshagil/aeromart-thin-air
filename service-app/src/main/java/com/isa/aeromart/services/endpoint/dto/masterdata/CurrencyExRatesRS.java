package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class CurrencyExRatesRS extends TransactionalBaseRS {

	private List<CurrencyExchangeRate> currencyExRates;

	public List<CurrencyExchangeRate> getCurrencyExRates() {
		return currencyExRates;
	}

	public void setCurrencyExRates(List<CurrencyExchangeRate> currencyExRates) {
		this.currencyExRates = currencyExRates;
	}
	
}
