package com.isa.aeromart.services.endpoint.dto.externalagent;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;

public class ExternalAgentLogOutRQ extends TransactionalBaseRQ{
	
	private String token;
	
	private String merchantID;
	
	private String hashvalue;

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getHashvalue() {
		return hashvalue;
	}

	public void setHashvalue(String hashvalue) {
		this.hashvalue = hashvalue;
	}
	
}
