package com.isa.aeromart.services.endpoint.dto.passenger;

import java.util.List;

public class PaxDOBValidationResponse {

	private List<PaxDOBValidationOp> paxDOBResp;

	public List getPaxDOBResp() {
		return paxDOBResp;
	}

	public void setPaxDOBResp(List<PaxDOBValidationOp> paxDOBResp) {
		this.paxDOBResp = paxDOBResp;
	}
	
}
