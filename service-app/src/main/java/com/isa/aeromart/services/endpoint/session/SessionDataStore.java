package com.isa.aeromart.services.endpoint.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionDataStore implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, Object> dataMap = new HashMap<String, Object>();

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	@SuppressWarnings("unchecked")
	public <T> T getData(String key) {
		return (T) dataMap.get(key);
	}

	public boolean containSessionKey(String key) {
		return dataMap.containsKey(key);
	}

	public <T> void putData(String key, T t) {
		dataMap.put(key, t);
	}

	public void remove(String key) {
		dataMap.remove(key);
	}
}
