package com.isa.aeromart.services.endpoint.adaptor.impls.modification;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.dto.common.BalanceTo;
import com.isa.aeromart.services.endpoint.dto.modification.ChargeDescriptionInfo;
import com.isa.aeromart.services.endpoint.dto.modification.UpdatedChargeForDisplay;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationExternalChargeInfo;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.utils.common.LableServiceUtil;
import com.isa.aeromart.services.endpoint.utils.modification.ModificationUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.BigDecimalWrapper;

public class UpdatedChargesAdaptor implements Adaptor<ReservationBalanceTO, UpdatedChargeForDisplay> {

	private ReservationInfo resInfo;
	private ReservationExternalChargeInfo extCharge;
	private BigDecimal ccFee;

	public UpdatedChargesAdaptor(ReservationInfo resInfo, ReservationExternalChargeInfo extCharge, BigDecimal ccFee) {
		this.resInfo = resInfo;
		this.extCharge = extCharge;
		this.ccFee = ccFee;
	}

	@Override
	public UpdatedChargeForDisplay adapt(ReservationBalanceTO source) {
	
		UpdatedChargeForDisplay updatedCharges = new UpdatedChargeForDisplay();
		String language = LocaleContextHolder.getLocale().getLanguage();
		if (source != null) {
			BigDecimal creditCardFee = AccelAeroCalculator.getDefaultBigDecimalZero();

			updatedCharges.setCreditToDisplay(source.getTotalCreditAmount());
			updatedCharges.setDueAmountToDisplay(source.getTotalAmountDue());

			List<ChargeDescriptionInfo> collection = new ArrayList<ChargeDescriptionInfo>();
			LCCClientSegmentSummaryTO lCCClientSegmentSummaryTO = source.getSegmentSummary();

			ChargeDescriptionInfo chargeInfo = new ChargeDescriptionInfo();
			chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblFlexiChages",
					language));
			BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (lCCClientSegmentSummaryTO.getOutBoundExternalCharge().compareTo(BigDecimal.ZERO) > 0) {
				flexiCharge = lCCClientSegmentSummaryTO.getOutBoundExternalCharge();
			} else if (lCCClientSegmentSummaryTO.getInBoundExternalCharge().compareTo(BigDecimal.ZERO) > 0) {
				flexiCharge = lCCClientSegmentSummaryTO.getInBoundExternalCharge();
			}
			if (flexiCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				chargeInfo
						.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.getDefaultBigDecimalZero()));
				chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(flexiCharge));
				chargeInfo.setEnableDisplay(false);
				collection.add(chargeInfo);
			}

			BigDecimal newFareWithFlexi = AccelAeroCalculator.add(lCCClientSegmentSummaryTO.getNewFareAmount(), flexiCharge);
			chargeInfo = new ChargeDescriptionInfo();
			chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentFareAmount()));
			chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(newFareWithFlexi));
			chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblAirFare",
					language));

			collection.add(chargeInfo);

			chargeInfo = new ChargeDescriptionInfo();
			chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getCurrentTaxAmount(), lCCClientSegmentSummaryTO.getCurrentSurchargeAmount(),
					lCCClientSegmentSummaryTO.getCurrentAdjAmount())));
			chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getNewTaxAmount(), lCCClientSegmentSummaryTO.getNewSurchargeAmount(),
					lCCClientSegmentSummaryTO.getNewExtraFeeAmount())));
			chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblTaxNSurcharges",
					language));
			collection.add(chargeInfo);

			// Add Ancillary
			List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
			if (resInfo.getPaxList() != null) {
				for (ReservationPaxTO pax : resInfo.getPaxList()) {
					extChgList.addAll(pax.getExternalCharges());
				}
			}

			BigDecimal seatChg = BigDecimal.ZERO;
			BigDecimal mealChg = BigDecimal.ZERO;
			BigDecimal baggageChg = BigDecimal.ZERO;
			BigDecimal ssrChg = BigDecimal.ZERO;
			BigDecimal airportServiceChg = BigDecimal.ZERO;
			BigDecimal airportTransferChg = BigDecimal.ZERO;
			BigDecimal insuranceChg = BigDecimal.ZERO;
			BigDecimal autoCheckinChg = BigDecimal.ZERO;
			BigDecimal totalAncillaryCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

			for (LCCClientExternalChgDTO chgDTO : extChgList) {
				if (chgDTO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
					if (!chgDTO.isAmountConsumedForPayment()) {
						totalAncillaryCharges = AccelAeroCalculator.add(totalAncillaryCharges, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.SEAT_MAP) {
						seatChg = AccelAeroCalculator.add(seatChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.MEAL) {
						mealChg = AccelAeroCalculator.add(mealChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.BAGGAGE) {
						baggageChg = AccelAeroCalculator.add(baggageChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
						ssrChg = AccelAeroCalculator.add(ssrChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
						airportServiceChg = AccelAeroCalculator.add(airportServiceChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_TRANSFER) {
						airportTransferChg = AccelAeroCalculator.add(airportTransferChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.INSURANCE) {
						insuranceChg = AccelAeroCalculator.add(insuranceChg, chgDTO.getAmount());
					}
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.AUTOMATIC_CHECKIN) {
						autoCheckinChg = AccelAeroCalculator.add(autoCheckinChg, chgDTO.getAmount());
					}
				}
			}

			if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
				if (seatChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentSeatAmount().compareTo(BigDecimal.ZERO) != 0) {
					chargeInfo = new ChargeDescriptionInfo();
					chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentSeatAmount()));
					chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(seatChg));
					chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
							"PgPriceBreakDown_lblSeatSelection", language));
					collection.add(chargeInfo);
				}

				if (mealChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentMealAmount().compareTo(BigDecimal.ZERO) != 0) {
					chargeInfo = new ChargeDescriptionInfo();
					chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentMealAmount()));
					chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(mealChg));
					chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
							"PgPriceBreakDown_lblMealSelection", language));
					collection.add(chargeInfo);
				}

				if (baggageChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentBaggageAmount().compareTo(BigDecimal.ZERO) != 0) {
					chargeInfo = new ChargeDescriptionInfo();
					chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentBaggageAmount()));
					chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(baggageChg));
					chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
							"PgPriceBreakDown_lblBaggageSelection", language));
					collection.add(chargeInfo);
				}

				if (ssrChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentSSRAmount().compareTo(BigDecimal.ZERO) != 0) {
					chargeInfo = new ChargeDescriptionInfo();
					chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentSSRAmount()));
					chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(ssrChg));
					chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
							"PgPriceBreakDown_lblSSRSelection", language));
					collection.add(chargeInfo);
				}

				if (airportServiceChg.compareTo(BigDecimal.ZERO) > 0) {
					chargeInfo = new ChargeDescriptionInfo();
					chargeInfo.setDisplayOldCharge("");
					chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(airportServiceChg));
					chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
							"PgPriceBreakDown_lblHalaSelection", language));
					collection.add(chargeInfo);
				}

				if (airportTransferChg.compareTo(BigDecimal.ZERO) > 0) {
					chargeInfo = new ChargeDescriptionInfo();
					chargeInfo.setDisplayOldCharge("");
					chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(airportTransferChg));
					chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
							"PgPriceBreakDown_lblAirportTransfer", language));
					collection.add(chargeInfo);
				}

				if (insuranceChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentInsuranceAmount().compareTo(BigDecimal.ZERO) != 0) {
					chargeInfo = new ChargeDescriptionInfo();
					chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentInsuranceAmount()));
					chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(insuranceChg));
					chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
							"PgPriceBreakDown_lblInsurance", language));
					collection.add(chargeInfo);
				}
				
				if (autoCheckinChg.compareTo(BigDecimal.ZERO) != 0
						|| lCCClientSegmentSummaryTO.getCurrentAutoCheckinAmount().compareTo(BigDecimal.ZERO) != 0) {
					chargeInfo = new ChargeDescriptionInfo();
					chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
							.getCurrentAutoCheckinAmount()));
					chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(autoCheckinChg));
					chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
							"PgPriceBreakDown_lblAutoCheckinSelection", language));
					collection.add(chargeInfo);
				}
			}

			chargeInfo = new ChargeDescriptionInfo();
			chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentModAmount()));
			chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getNewModAmount()));
			chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
					"PgPriceBreakDown_lblmodificationCharge", language));
			collection.add(chargeInfo);

			chargeInfo = new ChargeDescriptionInfo();
			chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentCnxAmount()));
			chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getNewCnxAmount()));
			chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblCancelCharge",
					language));
			collection.add(chargeInfo);

			if (lCCClientSegmentSummaryTO.getModificationPenalty() != null) {
				chargeInfo = new ChargeDescriptionInfo();
				chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentModificationPenatly()));
				chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getModificationPenalty()));
				chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblPenalties",
						language));
				collection.add(chargeInfo);
			}

			if ((lCCClientSegmentSummaryTO.getCurrentAdjAmount() != null && lCCClientSegmentSummaryTO.getNewAdjAmount() != null)
					&& (lCCClientSegmentSummaryTO.getCurrentAdjAmount().doubleValue() != 0 || lCCClientSegmentSummaryTO
							.getNewAdjAmount().doubleValue() != 0)) {
				chargeInfo = new ChargeDescriptionInfo();
				chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentAdjAmount()));
				chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getNewAdjAmount()));
				chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
						"PgPriceBreakDown_lblAdjustmentCharge", language));
				collection.add(chargeInfo);
			}

			if (ccFee != null) {
				creditCardFee = ccFee;
				BigDecimal ccTax = extCharge.getServiceChargeTax(EXTERNAL_CHARGES.JN_OTHER);
				creditCardFee = AccelAeroCalculator.add(creditCardFee, ccTax);
			}

			if (creditCardFee.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				chargeInfo = new ChargeDescriptionInfo();
				chargeInfo.setDisplayOldCharge(new BigDecimalWrapper().getDisplayValue());
				chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(creditCardFee));
				chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblTranFree",
						language));
				collection.add(chargeInfo);
			}

			// Total Current + New Charges
			chargeInfo = new ChargeDescriptionInfo();
			chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getCurrentFareAmount(), lCCClientSegmentSummaryTO.getCurrentTaxAmount(),
					lCCClientSegmentSummaryTO.getCurrentSurchargeAmount(), lCCClientSegmentSummaryTO.getCurrentCnxAmount(),
					lCCClientSegmentSummaryTO.getCurrentModAmount(), lCCClientSegmentSummaryTO.getCurrentAdjAmount(),
					lCCClientSegmentSummaryTO.getCurrentModificationPenatly(),
					lCCClientSegmentSummaryTO.getCurrentBaggageAmount(), lCCClientSegmentSummaryTO.getCurrentSeatAmount(),
					lCCClientSegmentSummaryTO.getCurrentMealAmount(), lCCClientSegmentSummaryTO.getCurrentSSRAmount(),
					lCCClientSegmentSummaryTO.getCurrentInsuranceAmount(), lCCClientSegmentSummaryTO.getCurrentAutoCheckinAmount())));
			BigDecimal newtotalWithAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal modificationPenalty = lCCClientSegmentSummaryTO.getModificationPenalty() != null ? lCCClientSegmentSummaryTO
					.getModificationPenalty() : AccelAeroCalculator.getDefaultBigDecimalZero();

			newtotalWithAnci = AccelAeroCalculator.add(newFareWithFlexi, lCCClientSegmentSummaryTO.getNewTaxAmount(),
					lCCClientSegmentSummaryTO.getNewSurchargeAmount(), lCCClientSegmentSummaryTO.getNewCnxAmount(),
					lCCClientSegmentSummaryTO.getNewModAmount(), lCCClientSegmentSummaryTO.getNewAdjAmount(),
					(AccelAeroCalculator.getDefaultBigDecimalZero()), creditCardFee, modificationPenalty,
					lCCClientSegmentSummaryTO.getNewExtraFeeAmount());

			chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(newtotalWithAnci));
			chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription("PgPriceBreakDown_lblTotalCharge",
					language));
			collection.add(chargeInfo);

			chargeInfo = new ChargeDescriptionInfo();
			chargeInfo.setDisplayOldCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentNonRefunds()));
			chargeInfo.setDisplayNewCharge(new BigDecimalWrapper().getDisplayValue());
			chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
					"PgPriceBreakDown_lblNonRefundAmount", language));
			collection.add(chargeInfo);

			chargeInfo = new ChargeDescriptionInfo();
			chargeInfo.setDisplayOldCharge("");
			if (lCCClientSegmentSummaryTO.getCurrentRefunds().compareTo(BigDecimal.ZERO) > 0) {
				chargeInfo
						.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentRefunds()));
			} else {
				chargeInfo
						.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO.getCurrentRefunds()));
			}
			chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
					"PgPriceBreakDown_lblCreditableAmount", language));
			collection.add(chargeInfo);

			// Reservation Credit
			BigDecimal totalPaxCredit = ModificationUtils.getTotalPaxCredit(resInfo.getPaxCreditMap(resInfo.getPnr())).negate();
			if (totalPaxCredit.compareTo(BigDecimal.ZERO) > 0) {
				chargeInfo = new ChargeDescriptionInfo();
				chargeInfo.setDisplayOldCharge("");
				chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(ModificationUtils.getTotalPaxCredit(
						resInfo.getPaxCreditMap(resInfo.getPnr())).negate()));
				chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
						"PgPriceBreakDown_lblReservationCredit", language));
				collection.add(chargeInfo);
			}

			BalanceTo balanceTo = ModificationUtils.getTotalBalanceToPayAmount(source, null);
			BigDecimal balanceToPay = balanceTo.getBalanceToPay();
			balanceToPay = AccelAeroCalculator.add(balanceToPay, creditCardFee);
			if (balanceTo.getTotalCreditBalance().compareTo(BigDecimal.ZERO) < 0) {
				chargeInfo = new ChargeDescriptionInfo();
				chargeInfo.setDisplayOldCharge("");
				chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(balanceTo.getTotalCreditBalance().negate()));
				chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
						"PgPriceBreakDown_lblCreditBalance", language));
				collection.add(chargeInfo);
			}
			if (balanceToPay.compareTo(BigDecimal.ZERO) >= 0) {
				chargeInfo = new ChargeDescriptionInfo();
				chargeInfo.setDisplayOldCharge("");
				chargeInfo.setDisplayNewCharge(AccelAeroCalculator.formatAsDecimal(balanceToPay));
				chargeInfo.setDisplayDescription(LableServiceUtil.getBalanceSummaryDescription(
						"PgPriceBreakDown_lblBalanceToPay", language));
				collection.add(chargeInfo);
			}
			updatedCharges.setChargeListToDisplay(collection);
		}
		return updatedCharges;
	}

}
