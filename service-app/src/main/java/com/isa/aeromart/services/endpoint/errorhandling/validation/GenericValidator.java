package com.isa.aeromart.services.endpoint.errorhandling.validation;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.isa.aeromart.services.endpoint.errorhandling.ValidationException;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotEmpty;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class GenericValidator implements Validator {

	private static final String PACKAGE_NAME = "com.isa.aeromart.services.endpoint.dto";

	@Override
	public boolean supports(Class<?> any) {
		return true;
	}

	@Override
	public void validate(Object object, Errors errors) {
		if (object != null) {
			Class<?> clazz = object.getClass();

			FieldsVisitor fieldsVisitor = new FieldsVisitor();
			List<Field> fields = fieldsVisitor.getFields(clazz);
			try {
				for (Field field : fields) {
					if (field.isAnnotationPresent(NotNull.class)) {
						validateNotNull(object, field);

					}
					if (field.isAnnotationPresent(NotEmpty.class)) {
						validateNotEmpty(object, field);

					}
					traverseFieldObject(object, errors, field);
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new ValidationException(ValidationException.Code.GENERIC_SYSTEM_ERROR, e);
			}
		}
	}

	private void validateNotNull(Object object, Field field) throws IllegalAccessException {
		field.setAccessible(true);
		Object fieldValueObject = getFieldValue(object, field);
		if (fieldValueObject == null) {
			Throwable throwable = new Throwable(
					"Field :" + field.getName() + " of Class :" + object.getClass().getName() + " cannot be null");
			throw new ValidationException(ValidationException.Code.NULL_ERROR, throwable);
		}
	}

	private void validateNotEmpty(Object object, Field field) throws IllegalAccessException {
		field.setAccessible(true);
		Object fieldValueObject = getFieldValue(object, field);
		boolean error = false;
		if (fieldValueObject != null) {
			if (fieldValueObject instanceof Collection) {
				Collection<?> coll = (Collection<?>) fieldValueObject;
				if (coll.size() == 0) {
					error = true;
				}
			} else if (fieldValueObject instanceof String) {
				String strVal = (String) fieldValueObject;
				if (strVal.length() == 0) {
					error = true;
				}
			}
		} else {
			error = true;
		}
		if (error) {
			Throwable throwable = new Throwable(
					"Field :" + field.getName() + " of Class :" + object.getClass().getName() + " cannot be empty");
			throw new ValidationException(ValidationException.Code.NULL_ERROR, throwable);
		}
	}

	private void traverseFieldObject(Object object, Errors errors, Field field) throws IllegalAccessException {
		Object fieldValueObject = getFieldValue(object, field);
		if (fieldValueObject != null) {
			Class<?> fieldType = fieldValueObject.getClass();
			if (isValidatorApplicableClass(fieldType)) {
				validate(fieldValueObject, errors);
			} else if (fieldType.isArray()) {
				validateArray(errors, fieldType);
			} else if (Collection.class.isAssignableFrom(fieldType)) {
				validateCollection(fieldValueObject, errors);
			} else if (Map.class.isAssignableFrom(fieldType)) {
				validateMap(fieldValueObject, errors);
			}
		}
	}

	private Object getFieldValue(Object object, Field field) {
		try {
			if (field.isAccessible()) {
				return field.get(object);

			} else {
				Class<?> clazz = object.getClass();
				Method m = clazz.getMethod(getGetter(field));
				if (m != null) {
					return m.invoke(object);
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchMethodException | SecurityException
				| InvocationTargetException e) {
		}
		return null;
	}

	private String getGetter(Field field) {
		return "get" + field.getName().substring(1, 2).toUpperCase() + field.getName().substring(2);
	}

	private void validateMap(Object fieldValueObject, Errors errors) {
		Map<?, ?> valueMap = (Map<?, ?>) fieldValueObject;
		if (valueMap.size() > 0) {
			for (Entry<?, ?> entryItem : valueMap.entrySet()) {
				Object keyItem = entryItem.getKey();
				Object valueItem = entryItem.getValue();
				if (keyItem != null && isValidatorApplicableClass(keyItem.getClass())) {
					validate(keyItem, errors);
				}
				if (valueItem != null && isValidatorApplicableClass(valueItem.getClass())) {
					validate(valueItem, errors);
				}
			}
		}
	}

	private void validateCollection(Object fieldValueObject, Errors errors) {
		Collection<?> valueList = (Collection<?>) fieldValueObject;
		if (valueList.size() > 0) {
			for (Object listItem : valueList) {
				if (isValidatorApplicableClass(listItem.getClass())) {
					validate(listItem, errors);
				}
			}
		}
	}

	private void validateArray(Errors errors, Class<?> fieldType) {
		if (Array.getLength(fieldType) > 0) {
			Class<?> arrayType = fieldType.getComponentType();
			if (isValidatorApplicableClass(arrayType)) {
				for (int index = 0; index < Array.getLength(fieldType); index++) {
					Object arrayObject = Array.get(fieldType, 0);
					if (arrayObject != null) {
						validate(arrayObject, errors);
					}
				}
			}
		}
	}

	private boolean isValidatorApplicableClass(Class<?> clazzName) {
		return clazzName != null && !clazzName.isPrimitive() && clazzName.getPackage() != null
				&& clazzName.getPackage().getName().startsWith(PACKAGE_NAME);
	}

	private class FieldsVisitor {
		public List<Field> getFields(Class<?> clazz) {
			List<Field> fields = new ArrayList<>();

			Field[] visibleFields;

			if (clazz != null && !(clazz.equals(Object.class))) {
				visibleFields = clazz.getDeclaredFields();

				fields.addAll(getFields(clazz.getSuperclass()));
				fields.addAll(Arrays.asList(visibleFields));

			}

			return fields;
		}
	}
}
