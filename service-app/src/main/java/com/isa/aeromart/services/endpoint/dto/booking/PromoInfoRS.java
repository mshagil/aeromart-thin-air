package com.isa.aeromart.services.endpoint.dto.booking;

import java.util.Set;

public class PromoInfoRS {

	private String promoType;

	private String discountType;

	private Float value;

	private Float appliedTotal;

	private boolean cancellable = false;

	private boolean modifiable = false;

	private String appliedTo;

	private int appliedAdults;

	private int appliedChilds;

	private Set<String> appliedAncis;

	private Set<Integer> applicableBINs;

	public int getAppliedAdults() {
		return appliedAdults;
	}

	public void setAppliedAdults(int appliedAdults) {
		this.appliedAdults = appliedAdults;
	}

	public int getAppliedChilds() {
		return appliedChilds;
	}

	public void setAppliedChilds(int appliedChilds) {
		this.appliedChilds = appliedChilds;
	}

	public int getAppliedInfants() {
		return appliedInfants;
	}

	public void setAppliedInfants(int appliedInfants) {
		this.appliedInfants = appliedInfants;
	}

	private int appliedInfants;

	private boolean creditPromo = false;

	private boolean moneyPromo = false;

	public String getPromoType() {
		return promoType;
	}

	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public boolean getCancellable() {
		return cancellable;
	}

	public void setCancellable(boolean cancellable) {
		this.cancellable = cancellable;
	}

	public boolean getModifiable() {
		return modifiable;
	}

	public void setModifiable(boolean modifiable) {
		this.modifiable = modifiable;
	}

	public String getAppliedTo() {
		return appliedTo;
	}

	public void setAppliedTo(String appliedTo) {
		this.appliedTo = appliedTo;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public boolean getMoneyPromo() {
		return moneyPromo;
	}

	public void setMoneyPromo(boolean moneyPromo) {
		this.moneyPromo = moneyPromo;
	}

	public boolean getCreditPromo() {
		return creditPromo;
	}

	public void setCreditPromo(boolean creditPromo) {
		this.creditPromo = creditPromo;
	}

	public Set<String> getAppliedAncis() {
		return appliedAncis;
	}

	public void setAppliedAncis(Set<String> appliedAncis) {
		this.appliedAncis = appliedAncis;
	}

	public Set<Integer> getApplicableBINs() {
		return applicableBINs;
	}

	public void setApplicableBINs(Set<Integer> applicableBINs) {
		this.applicableBINs = applicableBINs;
	}

	public Float getAppliedTotal() {
		return appliedTotal;
	}

	public void setAppliedTotal(Float appliedTotal) {
		this.appliedTotal = appliedTotal;
	}

}
