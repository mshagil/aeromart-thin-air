package com.isa.aeromart.services.endpoint.adaptor.impls.booking;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.i18n.LocaleContextHolder;

import com.isa.aeromart.services.endpoint.adaptor.Adaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts.BookingType;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegrateTO;
import com.isa.aeromart.services.endpoint.dto.booking.BookingRQ;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.store.BookingSessionStore;
import com.isa.aeromart.services.endpoint.utils.booking.BookingUtil;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationPreferrenceInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class ReservationAssemblerAdaptor implements Adaptor<BookingRQ, CommonReservationAssembler> {

	private BookingSessionStore bookingSessionStore;

	private AncillaryIntegrateTO ancillaryIntegrateTo;

	private TrackInfoDTO trackInfoDTO;

	private String customerID;

	public ReservationAssemblerAdaptor(TrackInfoDTO trackInfoDTO, BookingSessionStore bookingSessionStore,
			AncillaryIntegrateTO ancillaryIntegrateTo, String customerID) {
		this.bookingSessionStore = bookingSessionStore;
		this.ancillaryIntegrateTo = ancillaryIntegrateTo;
		this.trackInfoDTO = trackInfoDTO;
		this.customerID = customerID;
	}

	@Override
	public CommonReservationAssembler adapt(BookingRQ bookingRQ) {
		CommonReservationAssembler reservationAssembler = new CommonReservationAssembler();
		ReservationContactRQAdaptor contactInfoTransformer = new ReservationContactRQAdaptor(customerID);
		LCCClientReservation lccreservation = new LCCClientReservation();

		boolean isBinPromotion = bookingRQ.getMakePaymentRQ().isBinPromotion();

		CommonReservationPreferrenceInfo preferrenceInfo = new CommonReservationPreferrenceInfo();
		preferrenceInfo.setPreferredLanguage(LocaleContextHolder.getLocale().toString());
		lccreservation.setPreferrenceInfo(preferrenceInfo);
		lccreservation.setZuluBookingTimestamp(new Date());
		if (bookingSessionStore.getPostPaymentInformation() != null) {
			lccreservation.setPNR(bookingSessionStore.getPostPaymentInformation().getPnr());
			reservationAssembler.setTemporaryPaymentMap(bookingSessionStore.getPostPaymentInformation().getTemporyPaymentMap());
		}else{
			lccreservation.setPNR(bookingSessionStore.getPnr());
		}
		setSegmentInfoFromSession(lccreservation);

		reservationAssembler.setLccreservation(lccreservation);
		reservationAssembler.setAppIndicator(trackInfoDTO.getAppIndicator());
		reservationAssembler.setTargetSystem(ProxyConstants.SYSTEM.getEnum(bookingSessionStore.getPreferenceInfoForBooking()
				.getSelectedSystem()));
		reservationAssembler.setSelectedCurrency(AppSysParamsUtil.getBaseCurrency());
		String currency = bookingRQ.getReservationContact().getPreferredCurrency();
		if(currency!=null && !currency.isEmpty()) {
			reservationAssembler.setSelectedCurrency(currency);
		}
		reservationAssembler.setPnrZuluReleaseTimeStamp(bookingRQ.getOnholdReleaseZuluTimeStamp());
		reservationAssembler.setSelectedFareSegChargeTO(bookingSessionStore.getFarePricingInformation());

		if (!BookingType.ONHOLD.equals(bookingRQ.getMakePaymentRQ().getBookingType())) {
			reservationAssembler.setDiscountedFareDetails(bookingSessionStore.getDiscountedFareDetailsForBooking(isBinPromotion));
			reservationAssembler.setDiscountAmount(bookingSessionStore.getDiscountAmountForBooking(isBinPromotion));
		}
		if (bookingSessionStore.getLoyaltyInformation() != null) {
			reservationAssembler.setCarrierWiseLoyaltyPaymentMap(bookingSessionStore.getLoyaltyInformation()
					.getCarrierWiseLoyaltyPaymentInfo());
		}
		reservationAssembler.addContactInfo("", contactInfoTransformer.adapt(bookingRQ.getReservationContact()), null);

		if (ancillaryIntegrateTo.getReservation().getReservationInsurances() != null) {
			lccreservation.setReservationInsurances(ancillaryIntegrateTo.getReservation().getReservationInsurances());
		}

		BookingUtil.populatePaxInformation(bookingRQ, reservationAssembler, ancillaryIntegrateTo, bookingSessionStore);
		BookingUtil.populateResPaxWithSelAnciDtls(ancillaryIntegrateTo.getReservation().getPassengers(), reservationAssembler);

		List<FlightSegmentTO> fltSegTOs = new ArrayList<FlightSegmentTO>();
		AdaptorUtils.adaptCollection(bookingSessionStore.getSegments(), fltSegTOs, new FlightSegmentAdaptor());
		if (fltSegTOs != null || fltSegTOs.size() != 0) {
			BookingUtil.setSegmentSeqForSSR(ancillaryIntegrateTo.getReservation().getPassengers(), fltSegTOs, null);
		}

		if (bookingRQ.isCreateOnhold()) {
			reservationAssembler.setOnHoldBooking(true);
			reservationAssembler.setPnrZuluReleaseTimeStamp(bookingRQ.getOnholdReleaseZuluTimeStamp());
		} else {
			reservationAssembler.setOnHoldBooking(false);
		}

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		PassengerTypeQuantityAdaptor passengerTypeQuantityAdaptor = new PassengerTypeQuantityAdaptor();
		flightPriceRQ.getTravelerInfoSummary().getPassengerTypeQuantityList()
				.addAll(passengerTypeQuantityAdaptor.adapt(bookingRQ.getPaxInfo()));
		reservationAssembler.setLccTransactionIdentifier(bookingRQ.getTransactionId());
		reservationAssembler.setFlightPriceRQ(flightPriceRQ);
		// populate JN_ANCI tax ratio

		if(ancillaryIntegrateTo.getReservation().isJnTaxApplicable()){
			reservationAssembler.setServiceTaxRatio(ancillaryIntegrateTo.getReservation().getTaxRatio());
		}

		if (bookingSessionStore.getPayByVoucherInfo() != null) {
			reservationAssembler.setPayByVoucherInfo(bookingSessionStore.getPayByVoucherInfo());
		}
		reservationAssembler.setChargesApplicability(ChargeRateOperationType.MAKE_ONLY);
		return reservationAssembler;
	}

	

	private void setSegmentInfoFromSession(LCCClientReservation lccreservation) {

		List<SessionFlightSegment> sessionFlightSegments = bookingSessionStore.getSegments();

		SessionSegmentAdaptor segmentAdaptor = new SessionSegmentAdaptor();
		Integer segmentSequence = 1;
		for (SessionFlightSegment sessionFlightSegment : sessionFlightSegments) {
			segmentAdaptor.setSegmentSequence(segmentSequence++);
			lccreservation.addSegment(segmentAdaptor.adapt(sessionFlightSegment));
		}
	}

}
