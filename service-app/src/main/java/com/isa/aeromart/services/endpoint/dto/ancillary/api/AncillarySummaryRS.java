package com.isa.aeromart.services.endpoint.dto.ancillary.api;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.ancillary.AncillarySummary;
import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRS;

public class AncillarySummaryRS extends TransactionalBaseRS {
	
	private List<AncillarySummary> ancillarySummeryList;

	public List<AncillarySummary> getAncillarySummeryList() {
		return ancillarySummeryList;
	}

	public void setAncillarySummeryList(List<AncillarySummary> ancillarySummeryList) {
		this.ancillarySummeryList = ancillarySummeryList;
	}
	
}
