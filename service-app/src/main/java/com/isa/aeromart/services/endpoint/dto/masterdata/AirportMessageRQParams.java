package com.isa.aeromart.services.endpoint.dto.masterdata;

public class AirportMessageRQParams {
	private String salesChanel;
	private String stage;
	private String languageCode;

	public AirportMessageRQParams() {

	}

	public AirportMessageRQParams(String salesChanel, String stage, String languageCode) {
		this.salesChanel = salesChanel;
		this.stage = stage;
		this.languageCode = languageCode;
	}

	public String getSalesChanel() {
		return salesChanel;
	}

	public void setSalesChanel(String salesChanel) {
		this.salesChanel = salesChanel;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

}
