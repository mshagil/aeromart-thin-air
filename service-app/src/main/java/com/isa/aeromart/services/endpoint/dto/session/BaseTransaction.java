package com.isa.aeromart.services.endpoint.dto.session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class BaseTransaction {
	List<BaseHookCommand> initHooks = new ArrayList<BaseHookCommand>();
	List<BaseHookCommand> cleanupHooks = new ArrayList<BaseHookCommand>();

	final protected void init() {
		executeCommands(initHooks);
	}

	final public void cleanup() {
		executeCommands(cleanupHooks);
	}

	private void executeCommands(List<BaseHookCommand> commands) {
		for (Iterator<BaseHookCommand> itr = commands.iterator(); itr.hasNext();) {
			itr.next().execute();
		}
	}

	protected void addInitHook(BaseHookCommand hook) {
		initHooks.add(hook);
	}

	public void addCleanupHook(BaseHookCommand hook) {
		cleanupHooks.add(hook);
	}
}
