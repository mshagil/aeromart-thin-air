package com.isa.aeromart.services.endpoint.dto.modification;

public class CancelBalanceSummaryDTO {
	private String airFare;

	private String tax;

	private String discount;

	private String creditableAmount;

	private String cancelChrage;

	private String perPaxCancelCharge;

	private String availableCredit;

	private String finalCredit;

	public String getAirFare() {
		return airFare;
	}

	public void setAirFare(String airFare) {
		this.airFare = airFare;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getCreditableAmount() {
		return creditableAmount;
	}

	public void setCreditableAmount(String creditableAmount) {
		this.creditableAmount = creditableAmount;
	}

	public String getCancelChrage() {
		return cancelChrage;
	}

	public void setCancelChrage(String cancelChrage) {
		this.cancelChrage = cancelChrage;
	}

	public String getAvailableCredit() {
		return availableCredit;
	}

	public void setAvailableCredit(String availableCredit) {
		this.availableCredit = availableCredit;
	}

	public String getFinalCredit() {
		return finalCredit;
	}

	public void setFinalCredit(String finalCredit) {
		this.finalCredit = finalCredit;
	}

	public String getPerPaxCancelCharge() {
		return perPaxCancelCharge;
	}

	public void setPerPaxCancelCharge(String perPaxCancelCharge) {
		this.perPaxCancelCharge = perPaxCancelCharge;
	}

}
