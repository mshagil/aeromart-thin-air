package com.isa.aeromart.services.endpoint.dto.session.store;

import com.isa.aeromart.services.endpoint.constant.CommonConstants;
import com.isa.aeromart.services.endpoint.dto.session.AncillaryTransaction;

public interface AncillaryFlowSessionStore {

    String SESSION_KEY = AncillaryTransaction.SESSION_KEY;

    void setReservationFlow(CommonConstants.ReservationFlow reservationFlow);

    CommonConstants.ReservationFlow getReservationFlow();

}
