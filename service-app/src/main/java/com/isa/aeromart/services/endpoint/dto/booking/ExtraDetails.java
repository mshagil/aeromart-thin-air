package com.isa.aeromart.services.endpoint.dto.booking;

public class ExtraDetails {

	private String flightSegmentCode;
	
	private String flightDesignator;
	
	private String seat = "";
	
	private String meal = "";
	
	private String baggage = "";

	private String specialServices = "";

	private String airprotServices = "";
	
	private String airprotTransfer = "";

	/**
	 * This is for automatic checkin ancillary
	 */
	private String automaticCheckin = "";

	public String getFlightSegmentCode() {
		return flightSegmentCode;
	}

	public void setFlightSegmentCode(String flightSegmentCode) {
		this.flightSegmentCode = flightSegmentCode;
	}

	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getMeal() {
		return meal;
	}

	public void setMeal(String meal) {
		this.meal = meal;
	}

	public String getBaggage() {
		return baggage;
	}

	public void setBaggage(String baggage) {
		this.baggage = baggage;
	}

	public String getSpecialServices() {
		return specialServices;
	}

	public void setSpecialServices(String ssr) {
		this.specialServices = ssr;
	}

	public String getAirprotServices() {
		return airprotServices;
	}

	public void setAirprotServices(String airprotServices) {
		this.airprotServices = airprotServices;
	}

	public String getAirprotTransfer() {
		return airprotTransfer;
	}

	public void setAirprotTransfer(String airprotTransfer) {
		this.airprotTransfer = airprotTransfer;
	}

	/**
	 * @return the autoCheckinSeatPreference
	 */
	public String getAutomaticCheckin() {
		return automaticCheckin;
	}

	/**
	 * @param autoCheckinSeatPreference
	 *            the autoCheckinSeatPreference to set
	 */
	public void setAutomaticCheckin(String autoCheckinSeatPreference) {
		this.automaticCheckin = autoCheckinSeatPreference;
	}
	
}
