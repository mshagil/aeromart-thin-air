package com.isa.aeromart.services.endpoint.dto.masterdata;

import java.util.List;

import com.isa.aeromart.services.endpoint.dto.common.TransactionalBaseRQ;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotEmpty;
import com.isa.aeromart.services.endpoint.errorhandling.validation.annotations.NotNull;

public class AirportMessagesRQ extends TransactionalBaseRQ {

	@NotEmpty
	private List<AirportMessageFlightSegmentInfo> flightSegInfo;

	@NotNull
	private String stage;

	private String languageCode;

	public List<AirportMessageFlightSegmentInfo> getFlightSegInfo() {
		return flightSegInfo;
	}

	public void setFlightSegInfo(List<AirportMessageFlightSegmentInfo> flightSegInfo) {
		this.flightSegInfo = flightSegInfo;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

}
