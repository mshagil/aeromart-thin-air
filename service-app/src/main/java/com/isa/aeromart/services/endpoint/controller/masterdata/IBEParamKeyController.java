package com.isa.aeromart.services.endpoint.controller.masterdata;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.aeromart.services.endpoint.dto.common.IBEParamKeyResponse;
import com.isa.thinair.commons.api.exception.ModuleException;


/**
 * 
 * @author rajitha
 *
 */
@Controller
@RequestMapping(value = "parameters", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
public class IBEParamKeyController {
	
	private static Log log = LogFactory.getLog(IBEParamKeyController.class);
	
	@ResponseBody
	@RequestMapping(value = "/ibe")
	public IBEParamKeyResponse parameterRequest()
			throws ModuleException {
		IBEParamKeyResponse paramKeyResponse = new IBEParamKeyResponse();
		paramKeyResponse.setSuccess(true);
		return paramKeyResponse;
	}
	
	

}
