package com.isa.aeromart.services.endpoint.dto.booking;

public class PaxModificationParams {

	private int paxSequence;

	private boolean nameEditable;

	private boolean ffidEditable;

	private boolean isNameChangeFfid;

	public boolean isNameEditable() {
		return nameEditable;
	}

	public void setNameEditable(boolean nameEditable) {
		this.nameEditable = nameEditable;
	}

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public boolean isFfidEditable() {
		return ffidEditable;
	}

	public void setFfidEditable(boolean ffidEditable) {
		this.ffidEditable = ffidEditable;
	}

	public boolean isNameChangeFfid() {
		return isNameChangeFfid;
	}

	public void setNameChangeFfid(boolean isNameChangeFfid) {
		this.isNameChangeFfid = isNameChangeFfid;
	}
}
