package com.isa.aeromart.services.endpoint.controller.payment;

import com.efawateer.*;
import com.isa.aeromart.services.endpoint.constant.PaymentConsts;
import com.isa.aeromart.services.endpoint.delegate.booking.BookingService;
import com.isa.aeromart.services.endpoint.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.core.*;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.*;
import com.isa.thinair.paymentbroker.api.constants.EFawateerConstants;
import com.isa.thinair.paymentbroker.api.dto.*;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller @RequestMapping(value = "efawateer") public class EFawateerRequestController {
	@Autowired BookingService bookingService;
	private static final Log log = LogFactory.getLog(EFawateerRequestController.class);
	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	@ResponseBody @RequestMapping(value = "/payment/query/BillPull", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE }) public MFEP handleBillPul(@RequestParam("GUID") String guID,
			@RequestBody MFEP request) throws Exception {
		MFEP eFawateerResponse = null;
		log.info(getRequestData(request));
		try {
				if (validateRequest(request)) {
					if (EFawateerConstants.TRANSPORTATION.equalsIgnoreCase(request.getMsgBody().getServiceType())) {
						IPGIdentificationParamsDTO ipgIdentificationParamsDTO = getIpgIdentificationParamsDTO();
						if (ipgIdentificationParamsDTO != null) {
							if (AppSysParamsUtil.isTestStstem()
									&& request.getMsgBody().getIsVerificationRequired() != null) {
								String pnr = request.getMsgBody().getAcctInfo().getBillNo();
								LCCClientReservation reservation = loadReservation(pnr);
								eFawateerResponse = getEFawateerResponse(reservation, ipgIdentificationParamsDTO,
										request, guID);
								IPGResponseDTO responseDTO = ModuleServiceLocator.getPaymentBrokerBD()
										.getReponseData(null, getIPGResponseDTO(eFawateerResponse),
												ipgIdentificationParamsDTO);
								setSignatureToResponse(eFawateerResponse, responseDTO);
								recordCCPaymentStatus(request, eFawateerResponse, ipgIdentificationParamsDTO, pnr,
										reservation);
							} else {
								IPGRequestResultsDTO resultsDTO = ModuleServiceLocator.getPaymentBrokerBD()
										.getRequestData(getIpgRequestDTO(request, ipgIdentificationParamsDTO));
								String merchantID = resultsDTO.getRequestData();
								String sdrCode = request.getMsgHeader().getTrsInf().getSdrCode();
								if (EFawateerConstants.SDR_CODE.equalsIgnoreCase(sdrCode) && merchantID
										.equalsIgnoreCase(request.getMsgHeader().getTrsInf().getRcvCode())) {
									if (resultsDTO.isSignatureVerified()) {
										String pnr = request.getMsgBody().getAcctInfo().getBillNo();
										LCCClientReservation reservation = loadReservation(pnr);

										eFawateerResponse = getEFawateerResponse(reservation,
												ipgIdentificationParamsDTO, request, guID);
										IPGResponseDTO responseDTO = ModuleServiceLocator.getPaymentBrokerBD()
												.getReponseData(null, getIPGResponseDTO(eFawateerResponse),
														ipgIdentificationParamsDTO);
										setSignatureToResponse(eFawateerResponse, responseDTO);
										recordCCPaymentStatus(request, eFawateerResponse, ipgIdentificationParamsDTO,
												pnr, reservation);
									} else {
										eFawateerResponse = getErrorResponse(EFawateerConstants.SIGNATURE_ERROR_CODE,
												EFawateerConstants.SIGNATURE_ERROR_DESC,
												EFawateerConstants.ERROR_SEVERITY, request, guID);
									}
								} else {
									log.debug("Expected SDR Code or RCV code not received");
									throw new Exception("Invalid SDR code or RCV code");
								}
							}
						}
					} else {
						log.debug("Expected an EFawateer Transportation service type, but received " + request
								.getMsgBody().getServiceType());
						throw new Exception("Service type is invalid");
					}
				}
		} catch (Exception ex) {
			log.error(
					"Error occurred when processing the EFawateer Bill Pull request. Could not generate the response ",
					ex);
			if (StringUtil.isNullOrEmpty(ex.getLocalizedMessage())) {
				eFawateerResponse = getErrorResponse(EFawateerConstants.INTERNAL_ERROR_CODE,
						EFawateerConstants.INTERNAL_ERROR_DESC, EFawateerConstants.ERROR_SEVERITY, request, guID);
			} else {
				eFawateerResponse = getErrorResponse(EFawateerConstants.INTERNAL_ERROR_CODE, ex.getLocalizedMessage(),
						EFawateerConstants.ERROR_SEVERITY, request, guID);
			}
		}
		return eFawateerResponse;
	}

	@ResponseBody @RequestMapping(value = "/payment/confirm/ReceivePaymentNotification", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE }) public MFEP handlePaymentNotification(@RequestParam("guid") String guID,
			@RequestBody MFEP request) throws Exception {
		MFEP eFawateerResponse = null;
		log.info(getRequestData(request));
		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
		try {
				if (validateRequest(request)) {
					if (EFawateerConstants.TRANSPORTATION.equalsIgnoreCase(
							request.getMsgBody().getTransactions().getTrxInf().getServiceTypeDetails()
									.getServiceType())) {
						IPGIdentificationParamsDTO ipgIdentificationParamsDTO = getIpgIdentificationParamsDTO();
						if (ipgIdentificationParamsDTO != null) {
							if (AppSysParamsUtil.isTestStstem()
									&& request.getMsgBody().getIsVerificationRequired() != null) {
								Map<String, String> reqMap = getRequestMap(request, ipgResponseDTO,
										ipgIdentificationParamsDTO);
								ModuleServiceLocator.getPaymentBrokerBD()
										.handleDeferredResponse(reqMap, ipgResponseDTO, ipgIdentificationParamsDTO);
								eFawateerResponse = createPaymentNotfnResponse(request, guID);
								IPGResponseDTO responseDTO = ModuleServiceLocator.getPaymentBrokerBD()
										.getReponseData(null, getIPGResponseDTO(eFawateerResponse),
												ipgIdentificationParamsDTO);
								setSignatureToResponse(eFawateerResponse, responseDTO);
							} else {
								IPGRequestResultsDTO resultsDTO = ModuleServiceLocator.getPaymentBrokerBD()
										.getRequestData(getIpgRequestDTO(request, ipgIdentificationParamsDTO));
								String merchantID = resultsDTO.getRequestData();
								String sdrCode = request.getMsgHeader().getTrsInf().getSdrCode();
								if (EFawateerConstants.SDR_CODE.equalsIgnoreCase(sdrCode) && merchantID
										.equalsIgnoreCase(request.getMsgHeader().getTrsInf().getRcvCode())) {
									if (resultsDTO.isSignatureVerified()) {
										Map<String, String> reqMap = getRequestMap(request, ipgResponseDTO,
												ipgIdentificationParamsDTO);
										ModuleServiceLocator.getPaymentBrokerBD()
												.handleDeferredResponse(reqMap, ipgResponseDTO,
														ipgIdentificationParamsDTO);
										eFawateerResponse = createPaymentNotfnResponse(request, guID);
										IPGResponseDTO responseDTO = ModuleServiceLocator.getPaymentBrokerBD()
												.getReponseData(null, getIPGResponseDTO(eFawateerResponse),
														ipgIdentificationParamsDTO);
										setSignatureToResponse(eFawateerResponse, responseDTO);
									} else {
										eFawateerResponse = getErrorResponse(EFawateerConstants.SIGNATURE_ERROR_CODE,
												EFawateerConstants.SIGNATURE_ERROR_DESC,
												EFawateerConstants.ERROR_SEVERITY, request, guID);
									}
								} else {
									log.debug("Expected SDR Code or RCV code not received");
									throw new Exception("Invalid SDR code or RCV code");
								}
							}
						}
					} else {
						log.debug("Expected an EFawateer Transportation service type, but received " + request
								.getMsgBody().getServiceType());
						throw new Exception("Service type is invalid");
					}
				}
		} catch (ModuleException ex) {
			log.error(
					"Error occurred when processing the EFawateer payment notification request. Could not generate the response ",
					ex);
			if (StringUtil.isNullOrEmpty(ex.getExceptionCode())) {
				eFawateerResponse = getErrorResponse(EFawateerConstants.INTERNAL_ERROR_CODE,
						EFawateerConstants.INTERNAL_ERROR_DESC, EFawateerConstants.ERROR_SEVERITY, request, guID);
			} else {
				eFawateerResponse = getErrorResponse(EFawateerConstants.INTERNAL_ERROR_CODE, ex.getExceptionCode(),
						EFawateerConstants.ERROR_SEVERITY, request, guID);
			}
		} catch (Exception e) {
			log.error(
					"Error occurred when processing the EFawateer payment notification request. Could not generate the response ",
					e);

			if (StringUtil.isNullOrEmpty(e.getLocalizedMessage())) {
				eFawateerResponse = getErrorResponse(EFawateerConstants.INTERNAL_ERROR_CODE,
						EFawateerConstants.INTERNAL_ERROR_DESC, EFawateerConstants.ERROR_SEVERITY, request, guID);
			} else {
				eFawateerResponse = getErrorResponse(EFawateerConstants.INTERNAL_ERROR_CODE, e.getLocalizedMessage(),
						EFawateerConstants.ERROR_SEVERITY, request, guID);
			}
		}
		return eFawateerResponse;
	}

	private void setSignatureToResponse(MFEP eFawateerResponse, IPGResponseDTO ipgResponseDTO) {
		if (ipgResponseDTO.getSignature() != null) {
			eFawateerResponse.setMsgFooter(new MsgFooter());
			eFawateerResponse.getMsgFooter().setSecurity(new Security());
			eFawateerResponse.getMsgFooter().getSecurity().setSignature(ipgResponseDTO.getSignature());
		}
	}

	private Map<String, String> getRequestMap(MFEP request, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws Exception {
		ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
		String pnr = request.getMsgBody().getTransactions().getTrxInf().getAcctInfo().getBillNo();
		String pmtStatus = request.getMsgBody().getTransactions().getTrxInf().getPmtStatus();
		String joebppsTrx = request.getMsgBody().getTransactions().getTrxInf().getJOEBPPSTrx();
		String bankTrxID = request.getMsgBody().getTransactions().getTrxInf().getBankTrxID();
		double paidAmount = request.getMsgBody().getTransactions().getTrxInf().getPaidAmt();
		double dueAmount = request.getMsgBody().getTransactions().getTrxInf().getDueAmt();
		if (Double.compare(dueAmount, paidAmount) == 0) {
			String requestData = getRequestData(request);
			Map<String, String> reqMap = new HashMap<String, String>();
			reqMap.put(CommandParamNames.PNR, pnr);
			reqMap.put(PaymentConstants.STATUS, pmtStatus);
			reqMap.put(EFawateerConstants.JOEBPPSTRX, joebppsTrx);
			reqMap.put(EFawateerConstants.DUE_AMOUNT, String.valueOf(dueAmount));
			reqMap.put(EFawateerConstants.REQUEST, requestData);
			reqMap.put( EFawateerConstants.BANKTRXID, bankTrxID);
			return reqMap;
		} else {
			throw new Exception("Paid amount and Due amount do not match");
		}

	}

	private MFEP createPaymentNotfnResponse(MFEP request, String guID) {
		String processDate = request.getMsgBody().getTransactions().getTrxInf().getProcessDate();
		String STMTDate = request.getMsgBody().getTransactions().getTrxInf().getSTMTDate();
		String rcvCode = request.getMsgHeader().getTrsInf().getRcvCode();
		String jOEBTrx = request.getMsgBody().getTransactions().getTrxInf().getJOEBPPSTrx();
		MFEP eFawateerResponse = new MFEP();
		eFawateerResponse.setMsgBody(new MsgBody());
		eFawateerResponse.setMsgHeader(new MsgHeader());
		eFawateerResponse.getMsgBody().setTransactions(new Transactions());
		eFawateerResponse.getMsgBody().getTransactions().setTrxInf(new TrxInf());
		eFawateerResponse.getMsgHeader().setTrsInf(new TrsInf());
		eFawateerResponse.getMsgHeader().getTrsInf().setSdrCode(rcvCode);
		eFawateerResponse.getMsgHeader().getTrsInf().setResTyp(EFawateerConstants.PAYMENT_NOTIFICATION_RESPONSE);
		eFawateerResponse.getMsgBody().getTransactions().getTrxInf().setJOEBPPSTrx(jOEBTrx);
		eFawateerResponse.getMsgBody().getTransactions().getTrxInf().setProcessDate(processDate);
		eFawateerResponse.getMsgBody().getTransactions().getTrxInf().setSTMTDate(STMTDate);
		eFawateerResponse.getMsgHeader().setTmStp(setDateFormat(CalendarUtil.getCurrentSystemTimeInZulu()));
		eFawateerResponse.getMsgHeader().setGUID(guID);
		eFawateerResponse.getMsgHeader().setResult(new Result());
		eFawateerResponse.getMsgHeader().getResult().setErrorCode(EFawateerConstants.SUCCESS_ERROR_CODE);
		eFawateerResponse.getMsgHeader().getResult().setErrorDesc(EFawateerConstants.SUCCESS_ERROR_DESC);
		eFawateerResponse.getMsgHeader().getResult().setSeverity(EFawateerConstants.SUCCESS_SEVERITY);
		eFawateerResponse.getMsgBody().getTransactions().getTrxInf().setResult(new Result());
		eFawateerResponse.getMsgBody().getTransactions().getTrxInf().getResult().setErrorCode(
				EFawateerConstants.SUCCESS_ERROR_CODE);
		eFawateerResponse.getMsgBody().getTransactions().getTrxInf().getResult().setErrorDesc(
				EFawateerConstants.SUCCESS_ERROR_DESC);
		eFawateerResponse.getMsgBody().getTransactions().getTrxInf().getResult().setSeverity(
				EFawateerConstants.SUCCESS_SEVERITY);
		return eFawateerResponse;
	}

	private void recordCCPaymentStatus(MFEP request, MFEP eFawateerResponse,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, String pnr, LCCClientReservation reservation)
			throws Exception {
		OfflinePaymentInfo offlinePaymentInfo = getOfflinePaymentInfo(reservation, ipgIdentificationParamsDTO);
		CredentialsDTO credentialsDTO = new CredentialsDTO();
		credentialsDTO.setSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC);
		CreditCardPaymentStatusDTO creditCardPaymentStatusDTO = new CreditCardPaymentStatusDTO();
		creditCardPaymentStatusDTO.setPnr(pnr);
		creditCardPaymentStatusDTO.setServiceType(PaymentConstants.ServiceType.AUTHORIZE);
		creditCardPaymentStatusDTO.setStrRequest(getRequestData(request));
		creditCardPaymentStatusDTO.setStrResponse(getRequestData(eFawateerResponse));

		ReservationModuleUtils.getReservationBD().handleBalanceAmountRequestForOfflinePGW(
				AirProxyReservationUtil.transformContactInfo(reservation.getContactInfo()), offlinePaymentInfo,
				credentialsDTO, true, true, creditCardPaymentStatusDTO, ipgIdentificationParamsDTO);
	}

	private LCCClientReservation loadReservation(String pnr) throws Exception {
		LCCClientReservation reservation = new LCCClientReservation();
		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(pnr);
		reservationSearchDTO.setSearchAll(true);
		TrackInfoDTO trackInfo = new TrackInfoDTO();
		Collection<ReservationListTO> reservations = ModuleServiceLocator.getAirproxyReservationBD()
				.searchReservations(reservationSearchDTO, null, trackInfo);
		if (reservations.isEmpty()) {
			throw new Exception("No reservation found for the given PNR");
		} else {

			if (reservations.size() > 1) {
				throw new Exception("More than one reservation for the given PNR");
			} else {
				ReservationListTO reservationItem = BeanUtils.getFirstElement(reservations);
				String groupPnr = extractGroupPNR(reservationItem);
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				if (StringUtil.isNullOrEmpty(groupPnr)) {
					pnrModesDTO.setPnr(pnr);
				} else {
					pnrModesDTO.setGroupPNR(pnr);
				}
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadOndChargesView(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
				pnrModesDTO.setLoadSegViewBookingTypes(true);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
				ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
				paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_IBE);
				reservation = ModuleServiceLocator.getAirproxyReservationBD()
						.searchReservationByPNR(pnrModesDTO, null, trackInfo);
			}
		}
		return reservation;
	}

	private String extractGroupPNR(ReservationListTO reservationItem) {
		String groupPNR = "";
		groupPNR = reservationItem.getOriginatorPnr();
		if (StringUtils.isEmpty(groupPNR)) {
			if (StringUtils.isNotEmpty(reservationItem.getAirlineCode()) && StringUtils
					.isNotEmpty(reservationItem.getMarketingAirlineCode())) {
				groupPNR = reservationItem.getPnrNo();
			}
		}
		return groupPNR;
	}

	private MFEP getErrorResponse(String errorCode, String errorDesc, String severity, MFEP request, String guID)
			throws Exception {

		MFEP errorResponse = new MFEP();
		errorResponse.setMsgHeader(new MsgHeader());
		errorResponse.getMsgHeader().setResult(new Result());
		errorResponse.getMsgHeader().setTrsInf(new TrsInf());
		errorResponse.getMsgHeader().getResult().setErrorCode(errorCode);
		errorResponse.getMsgHeader().getResult().setErrorDesc(errorDesc);
		errorResponse.getMsgHeader().getResult().setSeverity(severity);
		errorResponse.getMsgHeader().setTmStp(setDateFormat(CalendarUtil.getCurrentSystemTimeInZulu()));
		errorResponse.getMsgHeader().setGUID(guID);
		errorResponse.getMsgHeader().getTrsInf().setSdrCode(request.getMsgHeader().getTrsInf().getRcvCode());
		if (EFawateerConstants.BILLPULL_REQUEST
				.equalsIgnoreCase(BeanUtils.nullHandler(request.getMsgHeader().getTrsInf().getReqTyp()))) {
			errorResponse.getMsgHeader().getTrsInf().setResTyp(EFawateerConstants.BILLPULL_RESPONSE);
		} else if (EFawateerConstants.PAYMENT_NOTIFICATION_REQUEST
				.equalsIgnoreCase(BeanUtils.nullHandler(request.getMsgHeader().getTrsInf().getReqTyp()))) {
			errorResponse.getMsgHeader().getTrsInf().setResTyp(EFawateerConstants.PAYMENT_NOTIFICATION_RESPONSE);
		}
		errorResponse.setMsgBody(new MsgBody());
		errorResponse.getMsgBody().setRecCount(EFawateerConstants.REC_COUNT_ERROR);
		IPGResponseDTO responseDTO = ModuleServiceLocator.getPaymentBrokerBD()
				.getReponseData(null, getIPGResponseDTO(errorResponse), getIpgIdentificationParamsDTO());
		setSignatureToResponse(errorResponse, responseDTO);
		return errorResponse;
	}

	private IPGRequestDTO getIpgRequestDTO(MFEP request, IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws Exception {
		String plainText = getPlainTextForVerification(request.getMsgBody());
		String signature = request.getMsgFooter().getSecurity().getSignature();
		IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
		ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		ipgRequestDTO.setSignature(signature);
		ipgRequestDTO.setRequestData(plainText);
		return ipgRequestDTO;
	}

	private IPGResponseDTO getIPGResponseDTO(MFEP response) throws Exception {
		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
		ipgResponseDTO.setResponseData(getPlainTextForVerification(response.getMsgBody()));
		return ipgResponseDTO;
	}

	private IPGIdentificationParamsDTO getIpgIdentificationParamsDTO() throws Exception {
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD()
				.getActivePaymentGatewayByProviderName(PaymentConsts.EFAWATEER_OFFLINE);
		if (!CollectionUtils.isEmpty(pgwList)) {
			IPGPaymentOptionDTO ipgPaymentOptionDTO = pgwList.get(0);
			if (ipgPaymentOptionDTO != null) {
				ipgIdentificationParamsDTO = WebplatformUtil
						.validateAndPrepareIPGConfigurationParamsDTO(ipgPaymentOptionDTO.getPaymentGateway(),
								ipgPaymentOptionDTO.getBaseCurrency());
			}
		} else {
			throw new Exception("No valid payment gateways");
		}
		return ipgIdentificationParamsDTO;
	}

	private PayCurrencyDTO getPayCurrencyDTO(CurrencyExchangeRate currencyExchangeRate, String baseCurrencyCode)
			throws ModuleException {
		com.isa.thinair.airmaster.api.model.Currency currency;
		PayCurrencyDTO payCurrencyDTO = null;
		CommonMasterBD commonMasterBD = ModuleServiceLocator.getCommoMasterBD();
		if (currencyExchangeRate != null) {
			currency = commonMasterBD.getCurrency(baseCurrencyCode);
			payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, currencyExchangeRate.getMultiplyingExchangeRate(),
					currency.getBoundryValue(), currency.getBreakPoint(), currency.getDecimalPlaces());
		} else {
			log.debug("Currency exchange rate not given ");
		}
		return payCurrencyDTO;
	}

	private OfflinePaymentInfo getOfflinePaymentInfo(LCCClientReservation reservation,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws Exception {
		OfflinePaymentInfo offlinePaymentInfo = new OfflinePaymentInfo();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy
				.getCurrencyExchangeRate(ipgIdentificationParamsDTO.getPaymentCurrencyCode(),
						ApplicationEngine.XBE);
		PayCurrencyDTO payCurrencyDTO = getPayCurrencyDTO(currencyExchangeRate,
				ipgIdentificationParamsDTO.getPaymentCurrencyCode());
		payCurrencyDTO.setTotalPayCurrencyAmount(exchangeRateProxy
				.convert(ipgIdentificationParamsDTO.getPaymentCurrencyCode(), getTotalAvailableBalance(reservation),
						true));
		payCurrencyDTO.setPayCurrencyExchangeRateId(currencyExchangeRate.getExchangeRateId());
		offlinePaymentInfo.setCarrierCode(reservation.getMarketingCarrier());
		offlinePaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		offlinePaymentInfo.setPnr(reservation.getPNR());
		offlinePaymentInfo.setTotalAmount(getTotalAvailableBalance(reservation));
		offlinePaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		return offlinePaymentInfo;
	}

	private String setDateFormat(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone(EFawateerConstants.TIME_ZONE));
		sdf.applyPattern(DATE_FORMAT);
		return sdf.format(date);
	}


	private MFEP getEFawateerResponse(LCCClientReservation reservation,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, MFEP request, String guID) throws Exception {
		MFEP eFawateerResponse = new MFEP();
		eFawateerResponse.setMsgBody(new MsgBody());
		eFawateerResponse.setMsgHeader(new MsgHeader());
		eFawateerResponse.getMsgHeader().setResult(new Result());
		eFawateerResponse.getMsgHeader().setTrsInf(new TrsInf());
		eFawateerResponse.getMsgBody().setBillsRec(new BillsRec());
		eFawateerResponse.getMsgBody().getBillsRec().setBillRec(new BillRec());
		eFawateerResponse.getMsgBody().getBillsRec().getBillRec().setAcctInfo(new AcctInfo());
		eFawateerResponse.getMsgBody().getBillsRec().getBillRec().getAcctInfo().setBillingNo(reservation.getPNR());
		eFawateerResponse.getMsgBody().getBillsRec().getBillRec().getAcctInfo().setBillNo(reservation.getPNR());
		eFawateerResponse.getMsgHeader().setTmStp(setDateFormat(CalendarUtil.getCurrentZuluDateTime()));
		eFawateerResponse.getMsgHeader().setGUID(guID);
		eFawateerResponse.getMsgBody().setRecCount(EFawateerConstants.REC_COUNT);
		eFawateerResponse.getMsgHeader().getResult().setErrorCode(EFawateerConstants.SUCCESS_ERROR_CODE);
		eFawateerResponse.getMsgHeader().getResult().setErrorDesc(EFawateerConstants.SUCCESS_ERROR_DESC);
		eFawateerResponse.getMsgHeader().getResult().setSeverity(EFawateerConstants.SUCCESS_SEVERITY);
		eFawateerResponse.getMsgHeader().getTrsInf().setResTyp(EFawateerConstants.BILLPULL_RESPONSE);
		eFawateerResponse.getMsgHeader().getTrsInf().setSdrCode(request.getMsgHeader().getTrsInf().getRcvCode());
		eFawateerResponse.getMsgBody().getBillsRec().getBillRec().setBillStatus(EFawateerConstants.BILL_NEW);

		eFawateerResponse.getMsgBody().getBillsRec().getBillRec()
				.setDueDate(setDateFormat(reservation.getZuluBookingTimestamp()));
		eFawateerResponse.getMsgBody().getBillsRec().getBillRec()
				.setIssueDate(setDateFormat(reservation.getZuluBookingTimestamp()));
		eFawateerResponse.getMsgBody().getBillsRec().getBillRec()
				.setDueAmount(getConvertedAmount(getTotalAvailableBalance(reservation), ipgIdentificationParamsDTO));
		eFawateerResponse.getMsgBody().getBillsRec().getBillRec().setServiceType(request.getMsgBody().getServiceType());
		if (EFawateerConstants.STATUS_OHD.equalsIgnoreCase(reservation.getStatus()))
		{
			eFawateerResponse.getMsgBody().getBillsRec().getBillRec()
					.setCloseDate(setDateFormat(reservation.getZuluReleaseTimeStamp()));
		}
		return eFawateerResponse;
	}

	private BigDecimal getTotalAvailableBalance(LCCClientReservation reservation) throws Exception {
		Set<LCCClientReservationPax> passengers = reservation.getPassengers();
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (LCCClientReservationPax pax : passengers) {
			BigDecimal amount = pax.getTotalAvailableBalance();
			if (AccelAeroCalculator.isGreaterThan(amount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				totalAmount = AccelAeroCalculator.add(totalAmount, amount);
			}
		}
		if (totalAmount.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
			throw new Exception("Total due amount is 0.00. No ba"
					+ "lance to pay.");
		} else {
			return totalAmount;
		}

	}

	private String getConvertedAmount(BigDecimal totalAmount, IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		try {
			return exchangeRateProxy.convert(ipgIdentificationParamsDTO.getPaymentCurrencyCode(), totalAmount, true)
					.toPlainString();
		} catch (ModuleException e) {
			log.error("Exception occurred when tried to convert to " + ipgIdentificationParamsDTO
					.getPaymentCurrencyCode(), e);
			return null;
		}
	}

	private String getPlainTextForVerification(MsgBody requestResponse) throws Exception {

		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(MsgBody.class);
		Marshaller marshaller = context.createMarshaller();
		JAXBElement<MsgBody> root = new JAXBElement<>(new QName("", EFawateerConstants.MESSAGE_BODY), MsgBody.class,
				requestResponse);
		marshaller.marshal(root, writer);
		String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		String plainText = output.substring(output.indexOf(">") + 1);
		return plainText;
	}

	private String getRequestData(MFEP request) throws Exception {
		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(MFEP.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.marshal(request, writer);
		String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		String plainText = output.substring(output.indexOf(">") + 1);
		return plainText;
	}

	private boolean validateMsgHeaderAndFooter(MFEP request) {
		if (request.getMsgFooter() != null && request.getMsgHeader() != null) {
			MsgHeader msgHeader = request.getMsgHeader();
			MsgFooter msgFooter = request.getMsgFooter();
			if (msgHeader.getTmStp() != null && msgHeader.getTrsInf() != null
					&& msgHeader.getTrsInf().getReqTyp() != null && msgHeader.getTrsInf().getRcvCode() != null
					&& msgHeader.getTrsInf().getSdrCode() != null && msgFooter.getSecurity() != null
					&& msgFooter.getSecurity().getSignature() != null) {
				return true;
			}
		}
		return false;
	}

	private boolean validateRequest(MFEP request) throws Exception {
		if (validateMsgHeaderAndFooter(request) && request.getMsgBody() != null) {
			MsgBody msgBody = request.getMsgBody();
			if (EFawateerConstants.BILLPULL_REQUEST
					.equalsIgnoreCase(BeanUtils.nullHandler(request.getMsgHeader().getTrsInf().getReqTyp()))) {
				if (msgBody.getAcctInfo() != null && msgBody.getServiceType() != null
						&& msgBody.getAcctInfo().getBillingNo() != null && msgBody.getAcctInfo().getBillNo() != null) {
					return true;
				} else {
					log.debug("Request is invalid. It lacks some mandatory data (MsgBody)");
					throw new Exception("Request is invalid. It lacks some mandatory fields in the Message body.");
				}

			} else if (EFawateerConstants.PAYMENT_NOTIFICATION_REQUEST
					.equalsIgnoreCase(BeanUtils.nullHandler(request.getMsgHeader().getTrsInf().getReqTyp()))) {
				if (msgBody.getTransactions() != null && msgBody.getTransactions().getTrxInf().getJOEBPPSTrx() != null
						&& msgBody.getTransactions().getTrxInf().getAcctInfo() != null
						&& msgBody.getTransactions().getTrxInf().getAcctInfo().getBillNo() != null
						&& msgBody.getTransactions().getTrxInf().getSTMTDate() != null
						&& msgBody.getTransactions().getTrxInf().getDueAmt() != null
						&& msgBody.getTransactions().getTrxInf().getPaidAmt() != null
						&& msgBody.getTransactions().getTrxInf().getFeesAmt() != null
						&& msgBody.getTransactions().getTrxInf().getBankTrxID() != null
						&& msgBody.getTransactions().getTrxInf().getBankCode() != null
						&& msgBody.getTransactions().getTrxInf().getPmtStatus() != null
						&& msgBody.getTransactions().getTrxInf().getAccessChannel() != null
						&& msgBody.getTransactions().getTrxInf().getPaymentMethod() != null
						&& msgBody.getTransactions().getTrxInf().getFeesOnBiller() != null
						&& msgBody.getTransactions().getTrxInf().getServiceTypeDetails() != null
						&& msgBody.getTransactions().getTrxInf().getServiceTypeDetails().getServiceType() != null
						&& msgBody.getTransactions().getTrxInf().getProcessDate() != null) {
					return true;
				} else {
					log.debug("Request is invalid. It lacks some mandatory data (MsgBody)");
					throw new Exception("Request is invalid. It lacks some mandatory fields in the Message body.");
				}
			} else {
				log.debug("Request type is invalid or missing. ");
				throw new Exception("Request type is invalid or missing");

			}
		} else {
			log.debug("Request is invalid. It lacks some mandatory data (MsgHeader or MsgFooter)");
			throw new Exception("Request is invalid. It lacks some mandatory fields in the Message header or footer.");

		}

	}


}


