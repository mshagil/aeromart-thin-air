package com.isa.aeromart.services.endpoint.dto.externalagent;

public class CreditLimitRS {

	private String paymentType;
	private String currencyCode;
	private String availabilityLimit;

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setCurrenyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setAvailabilityLimit(String availabilityLimit) {
		this.availabilityLimit = availabilityLimit;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getAvailabilityLimit() {
		return availabilityLimit;
	}

}
