'use strict';

angular.module('myApp')
    .controller('LandingCtrl', ['landingService', '$location', '$window', function (landingService, $location, $window) {
        var self = this;

        var urlParams = $location.search();

        var params = {
            timeStamp: urlParams.timeStamp,
            returnUrl: urlParams.returnUrl,
            merchantID: urlParams.merchantID,
            secureHash: urlParams.secureHash
        };

        landingService.setReturnUrl(urlParams.returnUrl);

        landingService.checkTokenValidity(params,
            function success(res) {
                if (res.success) {
                    navigateToLogin();
                } else {
                    navigateToError();
                }
            },
            function error() {
                navigateToError();
            }
        );

        function navigateToError(){
            $window.location.href = urlParams.returnUrl + '#error';
        }

        function navigateToLogin(){
            $location.path('/login')
        }

    }])

    .service('landingService', ['$http', function landingService($http) {
        var self = this;
        self.returnUrl = "";

        self.getHash = function (successFn, errorFn) {
            var path = '/service-app/controller/externalagent/generateHashTS';
            var timestamp = moment().subtract(2, 'minute').format('YYYY-MM-DDTHH:mm:ss');
            var params = {
                token: timestamp
            };

            $http.post(path, params)
                .then(
                    function success(res) {
                        successFn(res);
                    },
                    function error(err) {
                        errorFn(err)
                    }
                )
        };

        self.checkTokenValidity = function (params, successFn, errorFn) {
            var path = '/service-app/controller/externalagent/login';

            $http.post(path, null, {
                    params: params
                })
                .then(
                    function success(res) {
                        successFn(res.data);
                    },
                    function error(err) {
                        errorFn(err.data)
                    }
                )
        };

        self.setReturnUrl = function(returnUrl){
            if(returnUrl){
                self.returnUrl = returnUrl;
            }
        };

        self.getReturnUrl = function(){
            return self.returnUrl;
        };

    }]);