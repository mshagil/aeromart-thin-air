/*
 * Created on Jul 5, 2005
 *
 */
package com.isa.thinair.platform.api;

import junit.framework.TestCase;

import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * @author Nasly
 *
 */
public class TestDefaultModule extends TestCase {
	public TestDefaultModule() {
		super();
	}
	
	protected void setUp(){
//		System.setProperty("log4j.configuration", "log4j-config.xml");
		ModuleFramework.startup("moduleconfigs");
	}
	
	public void testGetModule(){
		LookupService lookupService = LookupServiceFactory.getInstance();
		IModule module = lookupService.getModule("exampleModule");
		assertNotNull(module);	
	}
	
	public void testGetBD(){
		LookupService lookupService = LookupServiceFactory.getInstance();
		IModule module = lookupService.getModule("exampleModule");
		assertNotNull(module);
		assertNotNull(module.getServiceBD("mock.bd.remote"));
		assertNotNull(module.getServiceBD("mock.bd.local"));
	}
}
