/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.platform.api;

import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Lasantha Pambagoda
 */
public class TestSystemProperties extends PlatformTestCase{

	protected void setUp() throws Exception{
		super.setUp();
	}
	
	public void testGetSystemProps() {
		
		LookupService lookup = LookupServiceFactory.getInstance();
		GlobalConfig config = (GlobalConfig)lookup.getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);		
		String value = config.getSystemParam(SystemParamKeys.OFFSET_FROM_THE_STANDED_FIRST_DAY_OF_WEEK);
		assertNotNull(value);
		assertEquals("6", value);
	}
}
