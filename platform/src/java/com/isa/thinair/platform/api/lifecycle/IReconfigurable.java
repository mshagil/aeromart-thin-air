package com.isa.thinair.platform.api.lifecycle;

public interface IReconfigurable {

	/**
	 * Called to reconfigure a module. If the module is <code>running</code> then it will be stopped first and then
	 * reconfigured.
	 * 
	 * @throws Exception
	 *             if an failure occurs while reconfiguring.
	 */
	void reconfigure() throws Exception;

}
