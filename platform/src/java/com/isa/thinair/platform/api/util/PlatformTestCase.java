/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * PlatformTestCase.java
 *
 * ===============================================================================
 *
 */
package com.isa.thinair.platform.api.util;

import junit.framework.TestCase;

import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * All the module test cases should extend this class
 * 
 * @author Lasantha Pambagoda
 * 
 */
public class PlatformTestCase extends TestCase {

	private String relativeConfigRoot = null;

	public PlatformTestCase() {
	}

	public PlatformTestCase(String relativeConfRoot) {
		relativeConfigRoot = relativeConfRoot;
	}

	protected void setUp() throws Exception {
		super.setUp();
		// FileSystemResource resource = new FileSystemResource("c:/isalogs/unittest/config/log4j-unittest.xml");
		// if (resource.exists()){
		// System.setProperty("log4j.configuration","c:/isalogs/unittest/config/log4j-unittest.xml");
		// DOMConfigurator.configure(System.getProperty("log4j.configuration"));
		// }
		if (relativeConfigRoot == null) {
			ModuleFramework.startup();
		} else {
			ModuleFramework.startup(relativeConfigRoot);
		}
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
