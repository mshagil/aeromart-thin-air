/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.platform.api.lifecycle;

public interface IDisposable {

	/**
	 * Called just before the Module is removed from the system. This method is called after the stop method.
	 * 
	 * @throws Exception
	 *             if <code>dispose</code> fails.
	 */
	void dispose() throws Exception;

}
