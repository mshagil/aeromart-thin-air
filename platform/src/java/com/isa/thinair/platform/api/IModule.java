/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.platform.api;

/**
 * @author Nasly Default top level service interface of a module
 */
public interface IModule {

	/**
	 * Retrieves a business delegate by specifying the name its configured with
	 * 
	 * @param delegateName
	 *            the key configured for the required BD
	 * @return business delegate object
	 */
	public IModule getModule();

	public IServiceDelegate getServiceBD(String fullBDKey);

	public DefaultModuleConfig getModuleConfig();

	public Object getLocalBean(String beanId);

	public IModule lookupModule(String moduleName);

	public IServiceDelegate lookupServiceBD(String moduleName, String bdKeyWithoutLocationSuffix);

	public IServiceDelegate lookupServiceBDSpecifyingFullBDKey(String moduleName, String fullBDKey);

	public Object lookupBean(String moduleName, String beanId);
}
