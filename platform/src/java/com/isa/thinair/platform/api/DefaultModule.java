/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * DefaultModule.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.api;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.core.bean.UnifiedBeanFactory;
import com.isa.thinair.platform.core.commons.exception.PlatformRuntimeException;

/**
 * @author Nasly Default implementation of module service interface. Custom module service implementations MUST
 *         subclasses this class.
 */
public class DefaultModule implements IModule {
	private final Log log = LogFactory.getLog(getClass());
	private static final String BEAN_URI_PREFIX = UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules";
	private Map delegatesMap;
	private DefaultModuleConfig moduleConfig;
	private String moduleName;

	public DefaultModule() {
		super();
	}

	// public IServiceDelegate getServiceDelegate(String callerModule, String delegateName) {
	// return getServiceDelegate(getSuffixedDelegateKey(callerModule, delegateName));
	// }

	public IServiceDelegate getServiceBD(String fullBDKey) {
		if (fullBDKey == null || !(fullBDKey.endsWith(".remote") || fullBDKey.endsWith(".local"))) {
			log.error("Invalid full BD key. BD key should be prefixed either .remote or .local [fullBDKey = " + fullBDKey + "]");
			throw new PlatformRuntimeException("platform.arg.bd.key.invalid", "platform.desc");
		}
		String bdImplClass = (String) delegatesMap.get(fullBDKey);
		if ((bdImplClass == null) || (bdImplClass.length() == 0)) {
			log.error("No valid configuration found for BD key = " + fullBDKey + " in " + getModuleName()
					+ " module configurations");
			throw new PlatformRuntimeException("platform.config.bd.notfound", "platform.desc");
		}
		IServiceDelegate serviceDelegate = null;
		try {
			// disabling BD caching - 04May2005
			// serviceDelegate = BDCahche.getServiceDelegate(bdImplClass);
			if (serviceDelegate == null) {
				if (log.isErrorEnabled())
					log.debug(":::::::::::::::::::: Instantiating BD [bdKey = " + fullBDKey + "] [bdImplClass =" + bdImplClass
							+ "]");
				Class clazz = Class.forName(bdImplClass);
				serviceDelegate = (IServiceDelegate) clazz.newInstance();
				// BDCahche.addServiceDelegate(bdImplClass, serviceDelegate);
			} else {
				if (log.isErrorEnabled())
					log.debug(":::::::::::::::::::: BD found in the cache [bdKey = " + fullBDKey + "] [bdImplClass ="
							+ bdImplClass + "]");
			}
		} catch (ClassNotFoundException e) {
			if (log.isErrorEnabled())
				log.error("Invalid BD impl class configuration [BDKey = " + fullBDKey + "] in " + getModuleName()
						+ " module configurations", e);
			throw new PlatformRuntimeException(e, "platform.module.bd.impl.invalid", "platform.desc");
		} catch (InstantiationException e) {
			if (log.isErrorEnabled())
				log.error("Could not instantiate the BD impl [BDKey = " + fullBDKey + "] specified in " + getModuleName()
						+ " module configurations", e);
			throw new PlatformRuntimeException(e, "platform.module.bd.impl.invalid", "platform.desc");
		} catch (IllegalAccessException e) {
			if (log.isErrorEnabled())
				log.error("Could not instantiate the BD impl [BDKey = " + fullBDKey + "] specified in " + getModuleName()
						+ " module configurations", e);
			throw new PlatformRuntimeException(e, "platform.module.bd.impl.invalid", "platform.desc");
		}
		return serviceDelegate;
	}

	public IModule getModule() {
		return getLookupService().getModule(getModuleName());
	}

	public Object getLocalBean(String beanId) {
		return getLookupService().getBean(BEAN_URI_PREFIX + "/" + getModuleName() + "?id=" + beanId);
	}

	public IModule lookupModule(String moduleName) {
		return getLookupService().getModule(moduleName);
	}

	public IServiceDelegate lookupServiceBD(String serviceProviderModuleName, String bdKeyWithoutLocationSuffix) {
		String fullBDKey = getSuffixedBDKey(serviceProviderModuleName, bdKeyWithoutLocationSuffix);
		return getLookupService().getServiceBD(serviceProviderModuleName, fullBDKey);
	}

	public IServiceDelegate lookupServiceBDSpecifyingFullBDKey(String serviceProviderModuleName, String fullBDKey) {
		return getLookupService().getServiceBD(serviceProviderModuleName, fullBDKey);
	}

	public Object lookupBean(String moduleName, String beanId) {
		return getLookupService().getBean(BEAN_URI_PREFIX + "/" + moduleName + "?id=" + beanId);
	}

	public void setDelegatesMap(Map delegatesMap) {
		this.delegatesMap = delegatesMap;
	}

	public DefaultModuleConfig getModuleConfig() {
		return moduleConfig;
	}

	public void setModuleConfig(DefaultModuleConfig moduleConfig) {
		this.moduleConfig = moduleConfig;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	private LookupService getLookupService() {
		return LookupServiceFactory.getInstance();
	}

	private String getSuffixedBDKey(String targetModuleName, String BDKey) {
		if (BDKey.endsWith(".local") || BDKey.endsWith(".remote")) {
			log.error("Unqualified BD key should not have suffix .local or .remote [BDkey = " + BDKey + "]");
			throw new PlatformRuntimeException("platform.arg.bd.key.invalid", "platform.desc");
		}

		String key = (String) getModuleConfig().getModuleDependencyMap().get(targetModuleName);

		if (key == null || key.equals("")) {
			log.error("Requested module's locality configurtion not found in " + getModuleName()
					+ " module configurations [BDKey = " + BDKey + "] [moduleName = " + targetModuleName + "]");
			throw new PlatformRuntimeException("platform.config.dependencymap.invalid", "platform.desc");
		}

		if (key.equalsIgnoreCase("local")) {
			return BDKey + ".local";
		} else if (key.equalsIgnoreCase("remote")) {
			return BDKey + ".remote";
		} else {
			log.error("Invalid module dependency configuration in " + getModuleName()
					+ " module configuratins. Values allowed are local|remote");
			throw new PlatformRuntimeException("platform.config.dependencymap.invalid", "platform.desc");
		}
	}
}
