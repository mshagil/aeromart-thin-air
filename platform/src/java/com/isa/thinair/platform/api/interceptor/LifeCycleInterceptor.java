package com.isa.thinair.platform.api.interceptor;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import EDU.oswego.cs.dl.util.concurrent.ReadWriteLock;
import EDU.oswego.cs.dl.util.concurrent.ReentrantWriterPreferenceReadWriteLock;

import com.isa.thinair.platform.api.lifecycle.IDisposable;
import com.isa.thinair.platform.api.lifecycle.IInitializable;
import com.isa.thinair.platform.api.lifecycle.ILifeCycle;
import com.isa.thinair.platform.api.lifecycle.ILifeCycleState;
import com.isa.thinair.platform.api.lifecycle.IReconfigurable;
import com.isa.thinair.platform.api.lifecycle.IStartable;
import com.isa.thinair.platform.api.lifecycle.ISuspendable;

/**
 * LifeCycleInterceptor
 */
public class LifeCycleInterceptor implements MethodInterceptor {

	private Log _log = LogFactory.getLog(getClass());

	private int _lifeCycleState = ILifeCycleState.CREATING;

	// TODO: should this be moved to the target object?
	private final ReadWriteLock _lifeCycleRWLock = new ReentrantWriterPreferenceReadWriteLock();

	private final Object _suspendedLock = new Object();

	private static final Method _SUSPEND_METHOD;
	private static final Method _START_METHOD;

	private static String[] _operationNames;
	private static int[] _validEntryStates;
	private static int[] _validExitStates;

	// TODO: make this a configurable property
	private static final long _BLOCK_UNTIL_RESUME_TIMEOUT = 5000;

	private static int _INITIALIZE_METHOD_IDX = 0;
	private static int _START_METHOD_IDX = 1;
	private static int _STOP_METHOD_IDX = 2;
	private static int _SUSPEND_METHOD_IDX = 3;
	private static int _RESUME_METHOD_IDX = 4;
	private static int _RECONFIGURE_METHOD_IDX = 5;
	private static int _DISPOSE_METHOD_IDX = 6;

	/**
	 * invoke method of the MethodInterceptor
	 */
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Object ret = null;
		Class declaringClass = invocation.getMethod().getDeclaringClass();
		// True if either an ILifeCycle method or a super interface method
		if (declaringClass.isAssignableFrom(ILifeCycle.class)) {
			// handle a life cycle method
			invokeLifeCycleMethod(invocation, declaringClass);
		} else {
			ret = invokeBusinessMethod(invocation);
		}
		return ret;
	}

	protected Object invokeBusinessMethod(MethodInvocation invocation) throws Throwable {
		_lifeCycleRWLock.readLock().acquire();
		try {
			// handle a non LifeCycle method
			int state = getLifeCycleState();
			if (state != ILifeCycleState.RUNNING) {
				if (state == ILifeCycleState.SUSPENDED || state == ILifeCycleState.RESUMING) {
					waitForResume();
				} else {
					throw new LifeCycleException("module is unavailable.");
				}
			}
			return invocation.proceed();
		} finally {
			_lifeCycleRWLock.readLock().release();
		}
	}

	protected void invokeLifeCycleMethod(MethodInvocation invocation, Class declaringClass) throws Throwable {

		startLifeCycleMethod();
		try {
			if (declaringClass.isAssignableFrom(IReconfigurable.class)) {
				doReconfigure(invocation);
			} else if (declaringClass.isAssignableFrom(IStartable.class)) {
				if (invocation.getMethod().equals(_START_METHOD))
					doStart(invocation);
				else
					doStop(invocation);
			} else if (declaringClass.isAssignableFrom(ISuspendable.class)) {
				if (invocation.getMethod().equals(_SUSPEND_METHOD))
					doSuspend(invocation);
				else
					doResume(invocation);
			} else if (declaringClass.isAssignableFrom(IInitializable.class)) {
				doInitialize(invocation);
			} else if (declaringClass.isAssignableFrom(IDisposable.class)) {
				doDispose(invocation);
			}
		} finally {
			stopLifeCycleMethod();
		}
	}

	/**
	 * LifeCycle interface methods.
	 */

	/**
	 * Initialize the Module. Called immediately after the Modules's constructor.
	 * 
	 */
	protected void doInitialize(MethodInvocation invocation) {
		final String opName = "initialize";
		int entryState = getLifeCycleState();
		try {
			checkEntryState(entryState, _INITIALIZE_METHOD_IDX, opName);

			setLifeCycleState(ILifeCycleState.INITIALIZING);
			if (invocation.getThis() instanceof IInitializable) {
				invocation.proceed();
			}
			setLifeCycleState(ILifeCycleState.STOPPED);
		} catch (Throwable t) {
			handleException("module " + opName + " operation  failed", t);
		} finally {
			if (getLifeCycleState() == ILifeCycleState.INITIALIZING) {
				setLifeCycleState(entryState);
			}
		}
	}

	protected void checkEntryState(int state, int operation, String operationName) {
		if ((state & _validEntryStates[operation]) == 0) {
			throw new LifeCycleException("invalid state for \"" + operationName + "\" operation");
		}
	}

	/**
	 * Reconfigure the Module. If the module in the RUNNING state, it will be suspended before reconfiguration.
	 * 
	 */
	protected void doReconfigure(MethodInvocation invocation) throws Throwable {
		final String opName = "reconfigure";
		int entryState = getLifeCycleState();
		int lastGoodState = entryState;
		try {
			checkEntryState(entryState, _RECONFIGURE_METHOD_IDX, opName);

			if (entryState == ILifeCycleState.RUNNING) {
				doSuspend(invocation);
				lastGoodState = getLifeCycleState();
			}

			setLifeCycleState(ILifeCycleState.RECONFIGURING);

			if (invocation.getThis() instanceof IReconfigurable) {
				invocation.proceed();
			}

			if (entryState == ILifeCycleState.RUNNING) {
				doResume(invocation);
			} else {
				setLifeCycleState(entryState);
			}
		} catch (Throwable t) {
			handleException("module " + opName + " operation  failed", t);
		} finally {
			int exitState = getLifeCycleState();
			if (exitState != ILifeCycleState.RUNNING && exitState != ILifeCycleState.SUSPENDED
					&& exitState != ILifeCycleState.STOPPED) {
				// TODO: is this enough or should we kill the module
				setLifeCycleState(lastGoodState);
			}
		}
	}

	/**
	 * Start the Module. If successful, the Module will be ready to handle client requests.
	 */
	protected void doStart(MethodInvocation invocation) throws Throwable {
		final String opName = "start";
		int entryState = getLifeCycleState();
		try {
			checkEntryState(entryState, _START_METHOD_IDX, opName);

			setLifeCycleState(ILifeCycleState.STARTING);
			if (invocation.getThis() instanceof IStartable) {
				invocation.proceed();
			}
			setLifeCycleState(ILifeCycleState.RUNNING);
		} catch (Throwable t) {
			handleException("module " + opName + " operation  failed", t);
		} finally {
			if (getLifeCycleState() == ILifeCycleState.STARTING) {
				setLifeCycleState(entryState);
			}
		}
	}

	/**
	 * Stop the Module.
	 * 
	 */
	protected void doStop(MethodInvocation invocation) throws Throwable {
		final String opName = "stop";
		int entryState = getLifeCycleState();
		try {
			checkEntryState(entryState, _STOP_METHOD_IDX, opName);

			setLifeCycleState(ILifeCycleState.STOPPING);
			if (invocation.getThis() instanceof IStartable) {
				invocation.proceed();
			}
			setLifeCycleState(ILifeCycleState.STOPPED);
		} catch (Throwable t) {
			handleException("module " + opName + " operation  failed", t);
		} finally {
			if (getLifeCycleState() == ILifeCycleState.STOPPING) {
				setLifeCycleState(entryState);
			}
		}
	}

	/**
	 * Suspends the Module.
	 * 
	 */
	protected void doSuspend(MethodInvocation invocation) {
		final String opName = "suspend";
		int entryState = getLifeCycleState();
		try {
			checkEntryState(entryState, _SUSPEND_METHOD_IDX, opName);

			setLifeCycleState(ILifeCycleState.SUSPENDING);
			if (invocation.getThis() instanceof IStartable) {
				invocation.proceed();
			}
			setLifeCycleState(ILifeCycleState.SUSPENDED);
		} catch (Throwable t) {
			handleException("module " + opName + " operation  failed", t);
		} finally {
			if (getLifeCycleState() == ILifeCycleState.SUSPENDING) {
				setLifeCycleState(entryState);
			}
		}
	}

	/**
	 * Resume the Module.
	 * 
	 * @param invocation
	 */
	protected void doResume(MethodInvocation invocation) {
		final String opName = "resume";
		int entryState = getLifeCycleState();
		try {
			checkEntryState(entryState, _RESUME_METHOD_IDX, opName);

			setLifeCycleState(ILifeCycleState.RESUMING);
			if (invocation.getThis() instanceof IStartable) {
				invocation.proceed();
			}
			setLifeCycleState(ILifeCycleState.RUNNING);

			synchronized (_suspendedLock) {
				_suspendedLock.notifyAll();
			}
		} catch (Throwable t) {
			handleException("module " + opName + " operation  failed", t);
		} finally {
			if (getLifeCycleState() == ILifeCycleState.RESUMING) {
				setLifeCycleState(entryState);
			}
		}
	}

	/**
	 * Dispose the Module. Called when the Module is already Stopped.
	 * 
	 */
	protected void doDispose(MethodInvocation invocation) throws Throwable {
		final String opName = "dispose";

		int entryState = getLifeCycleState();
		if (entryState == ILifeCycleState.DESTROYED) {
			return;
		}
		try {
			checkEntryState(entryState, _DISPOSE_METHOD_IDX, opName);

			if (entryState == ILifeCycleState.RUNNING) {
				doStop(invocation);
			}

			setLifeCycleState(ILifeCycleState.DESTROYING);
			if (invocation.getThis() instanceof IDisposable) {
				invocation.proceed();
			}
		} catch (Throwable t) {
			handleException("module " + opName + " operation  failed", t);
		} finally {
			setLifeCycleState(ILifeCycleState.DESTROYED);
		}
	}

	protected synchronized void setLifeCycleState(int newState) {
		_lifeCycleState = newState;
	}

	protected synchronized int getLifeCycleState() {
		return _lifeCycleState;
	}

	protected void startLifeCycleMethod() {
		try {
			_lifeCycleRWLock.writeLock().attempt(1000);
		} catch (InterruptedException e) {
			String msg = "Could not acquire lifecycle write lock";
			_log.error(msg, e);
			throw new LifeCycleException(msg);
		}
	}

	protected void stopLifeCycleMethod() {
		_lifeCycleRWLock.writeLock().release();
	}

	protected void handleException(String msg, Throwable t) {
		_log.error(msg, t);
		if (t instanceof LifeCycleException) {
			throw (LifeCycleException) t;
		} else {
			throw new LifeCycleException(msg, t);
		}
	}

	protected void waitForResume() {
		try {
			_lifeCycleRWLock.readLock().release();

			synchronized (_suspendedLock) {
				if (getLifeCycleState() == ILifeCycleState.SUSPENDED) {
					_suspendedLock.wait(_BLOCK_UNTIL_RESUME_TIMEOUT);
				}
			}
		} catch (InterruptedException e) {
			_log.warn("interrupted while waiting for module resume.", e);
		} finally {
			try {
				_lifeCycleRWLock.readLock().acquire();
			} catch (InterruptedException e) {
				String errMsg = "could not acquire lifecycle read lock";
				_log.error(errMsg, e);
				throw new LifeCycleException(errMsg, e);
			}
			if (getLifeCycleState() != ILifeCycleState.RUNNING) {
				String msg = "timed out waiting for module to resume";
				_log.warn(msg);
				throw new LifeCycleException(msg);
			}
		}

	}

	protected void takeOutOfService() {
		// TODO: add capability to generate events when life cycle
		// state changes happen. The container will listen for
		// these events and handle them appropriately. In this
		// situation, the container should invalidate
		// the BeanFactory.
		setLifeCycleState(ILifeCycleState.DESTROYED);
	}

	static {
		try {
			_START_METHOD = IStartable.class.getMethod("start", new Class[] {});
			_SUSPEND_METHOD = ISuspendable.class.getMethod("suspend", new Class[] {});
		} catch (NoSuchMethodException e) {
			throw new RuntimeException("initialization of LifeCycleInterceptor failed.", e);
		}
		// Each life cycle operation has a set of valid entry states.
		_validEntryStates = new int[32];
		_validEntryStates[_INITIALIZE_METHOD_IDX] = ILifeCycleState.CREATING;
		_validEntryStates[_START_METHOD_IDX] = ILifeCycleState.STOPPED;
		_validEntryStates[_STOP_METHOD_IDX] = ILifeCycleState.RUNNING | ILifeCycleState.SUSPENDED;
		_validEntryStates[_SUSPEND_METHOD_IDX] = ILifeCycleState.RUNNING;
		_validEntryStates[_RESUME_METHOD_IDX] = ILifeCycleState.SUSPENDED;
		_validEntryStates[_RECONFIGURE_METHOD_IDX] = ILifeCycleState.RUNNING | ILifeCycleState.SUSPENDED
				| ILifeCycleState.STOPPED;
		// all entry states a valid for dispose
		_validEntryStates[_DISPOSE_METHOD_IDX] = 0xffffffff;

	}
}
