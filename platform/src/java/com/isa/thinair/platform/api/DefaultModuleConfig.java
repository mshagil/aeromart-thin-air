/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 6, 2005
 * 
 * DefaultModuleConfig.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.api;

import java.util.Map;
import java.util.Properties;

/**
 * @author Nasly Default configuration implemetation for a module
 */
public class DefaultModuleConfig {
	private Map moduleDependencyMap;
	private Properties jndiProperties;

	public Map getModuleDependencyMap() {
		return moduleDependencyMap;
	}

	public void setModuleDependencyMap(Map moduleDependencyMap) {
		this.moduleDependencyMap = moduleDependencyMap;
	}

	public Properties getJndiProperties() {
		return jndiProperties;
	}

	public void setJndiProperties(Properties jndiProperties) {
		this.jndiProperties = jndiProperties;
	}
}
