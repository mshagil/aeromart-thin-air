/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 6, 2005
 * 
 * ModuleDefinitionImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.controller;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IModuleDefinition;

/**
 * @author Nasly
 * 
 */
public class ModuleDefinitionImpl implements IModuleDefinition {

	private String moduleName;
	private IModule implementation;
	private IModule proxy;
	private String description;

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public void setImplementation(IModule moduleImpl) {
		this.implementation = moduleImpl;
	}

	public void setProxy(IModule moduleProxy) {
		this.proxy = moduleProxy;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModuleName() {
		return this.moduleName;
	}

	public IModule getImplementation() {
		return this.implementation;
	}

	public IModule getProxy() {
		return this.proxy;
	}

	public String getDescription() {
		return this.description;
	}

}
