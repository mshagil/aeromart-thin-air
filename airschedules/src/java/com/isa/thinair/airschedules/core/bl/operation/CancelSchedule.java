/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentResInvSummaryDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.bl.email.FlightScheduleEmail;
import com.isa.thinair.airschedules.core.bl.noncmd.ReservationsForFlights;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.OverlapDetailUtil;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for cancel flight schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="cancelSchedule"
 */
public class CancelSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;
	private FlightDAO flightDAO;
	private FlightSegementDAO flightSegmentDAO;

	// BD's
	private FlightInventoryResBD flightInventryResBD;
	private FlightInventoryBD flightInventryBD;
	private SegmentBD segmentBD;
	private AirportBD airportBD;
	private MealBD mealBD;
	private BaggageBusinessDelegate baggageBD;
	private SeatMapBD seatBD;
	private ReservationBD reservationBD;

	// Helpers
	private ReservationsForFlights reservation4Flights;
	private LocalZuluTimeAdder timeAdder;
	private boolean isScheduleMessageProcess;

	/**
	 * constructor of the CancelSchedule command
	 */
	public CancelSchedule() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
		flightSegmentDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);

		// looking up bd's
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
		flightInventryResBD = AirSchedulesUtil.getFlightInventoryResBD();
		segmentBD = AirSchedulesUtil.getSegmentBD();
		airportBD = AirSchedulesUtil.getAirportBD();
		mealBD = AirSchedulesUtil.getMealBD();
		baggageBD = AirSchedulesUtil.getBaggageBD();
		seatBD = AirSchedulesUtil.getSeatMapBD();
		timeAdder = new LocalZuluTimeAdder(airportBD);
		reservation4Flights = new ReservationsForFlights(flightDAO, flightSegmentDAO, flightInventryBD, segmentBD);
		reservationBD = AirSchedulesUtil.getReservationBD();
		isScheduleMessageProcess = false;
	}

	/**
	 * execute method of the CancelSchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Integer scheduleId = (Integer) this.getParameter(CommandParamNames.SCHEDULE_ID);
		Boolean force = (Boolean) this.getParameter(CommandParamNames.FORCE);
		FlightAlertDTO flightAlertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);
		isScheduleMessageProcess = getParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, Boolean.FALSE,
				Boolean.class);
		Collection<FlightReservationAlertDTO> flightReservationAlertDTOList = new ArrayList<FlightReservationAlertDTO>();
		HashMap<Integer, Collection<Integer>> buildScheduleFlightIds = new HashMap<Integer, Collection<Integer>>();

		// checking params
		this.checkParams(scheduleId, force);

		// Collecting Flights to Publish
		Collection<Flight> flightsToPublish = new ArrayList<Flight>();

		// load the corresponding flight schedule
		FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
		// if no schedule found for id throw exception
		if (schedule == null)
			throw new ModuleException("airschedules.logic.bl.schedule.noschedule");
		schedule.setModifiedDate(new Date(System.currentTimeMillis()));

		FlightSchedule overlappingSched = null;

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// if schedule is not overlapping
		if (!schedule.isOverlapping()) {

			if (schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())) {
				flightSchedleDAO.removeFlightSchedule(scheduleId.intValue());
			} else {
				// if NO reservations exist
				Collection<Flight> flights = flightSchedleDAO.getFlightsForSchedule(schedule.getScheduleId().intValue());
				List<Integer> flihgtIdList = new ArrayList<Integer>();
				Collection<Integer> flightsHasRes = new ArrayList<Integer>();
				Collection<Integer> flightSegIds = new ArrayList<Integer>();

				if (flights.size() > 0) {
					// flihgtIdList = ScheduledFlightUtil.getFlightIds(flights);
					flights = filterFlownFlights(flights);
					if (flights != null && !flights.isEmpty()) {
						flihgtIdList.addAll(ScheduledFlightUtil.getFlightIds(flights));
						Collection<Integer> cancelFlightIds = new ArrayList<Integer>();
						cancelFlightIds.addAll(flihgtIdList);
						buildScheduleFlightIds.put(schedule.getScheduleId(), cancelFlightIds);
						flightsHasRes = flightInventryResBD.filterFlightsHavingReservations(flihgtIdList);
					}
				}

				if (flightsHasRes.size() == 0) {

					// if schedule is linked with gds then dont delete it
					if (schedule.getGdsIds() != null && schedule.getGdsIds().size() != 0) {

						this.cancelCompletedSchedule(schedule, flights, flightAlertDTO, flightsToPublish, flightsHasRes);

						// if schedule has no gds linked, then delete all schedule info
					} else {

						// remove inventory
						flightInventryBD.deleteFlightInventories(flihgtIdList);

						flightSegIds = ScheduledFlightUtil.getFlightSegmentIDs(flights);
						// remove the meal templates which are assigned to a flight.
						mealBD.deleteFlightMeals(flightSegIds);
						// remove baggage charge templates
						baggageBD.deleteFlightBaggages(flightSegIds);
						// remove seat charges
						seatBD.deleteFlightSeats(flightSegIds);
						// remove flights

						for (Flight flight : flights) {
							flight.setStatus(FlightStatusEnum.CANCELLED.getCode());
							flightDAO.removeFlight(flight.getFlightId().intValue());

							// Add the flights to the list which holds the publish flight list
							flightsToPublish.add(flight);
						}

						// remove flight schedule
						flightSchedleDAO.removeFlightSchedule(scheduleId.intValue());
					}

				}
				// if reservations exist, needs force=true for actual cancellation
				else {
					if ((force == null || force.booleanValue() == false) && flightsHasRes.size() > 0) {

						HashMap<FlightSummaryDTO, Collection<FlightSegmentResInvSummaryDTO>> flightDetailsWithResMap = reservation4Flights
								.getFlightDetailsWithResMap(flightsHasRes, null);

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE);
						responce.addResponceParam(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE, flightDetailsWithResMap);

					} else {

						this.cancelCompletedSchedule(schedule, flights, flightAlertDTO, flightsToPublish, flightsHasRes);
						flights = timeAdder.addLocalTimeDetailsToFlights(flights);

						flightReservationAlertDTOList.addAll(AlertUtil.prepareFlightAlertInfo(flights));
					}
				}

			}

			// if schedule is overlapping
		} else {

			overlappingSched = flightSchedleDAO.getFlightSchedule(schedule.getOverlapingScheduleId().intValue());

			if (schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())
					&& overlappingSched.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())) {

				schedule = OverlapDetailUtil.removeOverlapDetailsFromSchedule(schedule);
				overlappingSched = OverlapDetailUtil.removeOverlapDetailsFromSchedule(overlappingSched);

				flightSchedleDAO.saveFlightSchedule(schedule);
				flightSchedleDAO.saveFlightSchedule(overlappingSched);

				flightSchedleDAO.removeFlightSchedule(scheduleId.intValue());
				flightSchedleDAO.removeFlightSchedule(overlappingSched.getScheduleId().intValue());

			} else {

				Collection<Flight> flights = flightSchedleDAO.getFlightsForSchedule(schedule.getScheduleId().intValue());
				Collection<Flight> overlapFlights = flightSchedleDAO.getFlightsForSchedule(overlappingSched.getScheduleId()
						.intValue());
				Collection<Integer> flightsHasRes = new ArrayList<Integer>();
				List<Integer> flihgtIdList = new ArrayList<Integer>();

				if (flights.size() > 0 || overlapFlights.size() > 0) {

					// flihgtIdList = ScheduledFlightUtil.getFlightIds(flights);
					flihgtIdList.addAll(ScheduledFlightUtil.getFlightIds(flights));

					Collection<Integer> cancelFlightIds = new ArrayList<Integer>();
					cancelFlightIds.addAll(flihgtIdList);
					buildScheduleFlightIds.put(schedule.getScheduleId(), cancelFlightIds);

					List<Integer> overlapFlihgtIdList = ScheduledFlightUtil.getFlightIds(overlapFlights);

					Collection<Integer> overlapCancelFlightIds = new ArrayList<Integer>();
					overlapCancelFlightIds.addAll(overlapFlihgtIdList);
					buildScheduleFlightIds.put(overlappingSched.getScheduleId(), overlapCancelFlightIds);

					flihgtIdList.addAll(overlapFlihgtIdList);
					flightsHasRes = flightInventryResBD.filterFlightsHavingReservations(flihgtIdList);
				}
				if (flightsHasRes.size() == 0) {

					// if schedule is not linked with gds then delete all
					if ((schedule.getGdsIds() == null || schedule.getGdsIds().size() == 0)
							&& (overlappingSched.getGdsIds() == null || overlappingSched.getGdsIds().size() == 0)) {

						// remove inventory
						flightInventryBD.deleteFlightInventories(flihgtIdList);
						// issues could exist in inventory

						Collection<Flight> flightsToRemove = new ArrayList<Flight>();

						// remove overlap from flights and save
						Iterator<Integer> flightIdIt = flihgtIdList.iterator();
						while (flightIdIt.hasNext()) {
							Integer flightId = (Integer) flightIdIt.next();
							Flight flight = flightDAO.getFlight(flightId.intValue());
							flight = OverlapDetailUtil.removeOverlapDetailsFromFlight(flight);
							flightDAO.saveFlight(flight);
							flightsToRemove.add(flight);
						}

						// remove flights
						for (Flight flight : flightsToRemove) {

							flight.setStatus(FlightStatusEnum.CANCELLED.getCode());
							flightDAO.removeFlight(flight.getFlightId().intValue());

							// Add the flights to the list which holds the publish flight list
							flightsToPublish.add(flight);
						}

						// remove overlap dependency from both
						schedule = OverlapDetailUtil.removeOverlapDetailsFromSchedule(schedule);
						overlappingSched = OverlapDetailUtil.removeOverlapDetailsFromSchedule(overlappingSched);

						flightSchedleDAO.saveFlightSchedule(schedule);
						flightSchedleDAO.saveFlightSchedule(overlappingSched);

						// remove flight both schedules
						flightSchedleDAO.removeFlightSchedule(scheduleId.intValue());
						flightSchedleDAO.removeFlightSchedule(overlappingSched.getScheduleId().intValue());

					} else {

						this.cancelCompletedSchedule(schedule, flights, flightAlertDTO, flightsToPublish, flightsHasRes);
						this.cancelCompletedSchedule(overlappingSched, overlapFlights, flightAlertDTO, flightsToPublish,
								flightsHasRes);
					}

				} else {
					if ((force == null || force.booleanValue() == false) && flightsHasRes.size() > 0) {

						HashMap<FlightSummaryDTO, Collection<FlightSegmentResInvSummaryDTO>> flightDetailsWithResMap = reservation4Flights
								.getFlightDetailsWithResMap(flightsHasRes, null);

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE);
						responce.addResponceParam(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE, flightDetailsWithResMap);

					} else {

						this.cancelCompletedSchedule(schedule, flights, flightAlertDTO, flightsToPublish, flightsHasRes);
						this.cancelCompletedSchedule(overlappingSched, overlapFlights, flightAlertDTO, flightsToPublish,
								flightsHasRes);
						flights = timeAdder.addLocalTimeDetailsToFlights(flights);
						overlapFlights = timeAdder.addLocalTimeDetailsToFlights(overlapFlights);

						flightReservationAlertDTOList.addAll(AlertUtil.prepareFlightAlertInfo(flights));
						flightReservationAlertDTOList.addAll(AlertUtil.prepareFlightAlertInfo(overlapFlights));
					}
				}
			}
		}

//		sendScheduleCancellationOfficerNotificationEmail(schedule, flightsToPublish.size(), true);
		// return command responce
		if (responce.isSuccess()) {
			responce.setResponseCode(ResponceCodes.CANCEL_SCHEDULE_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_EMAIL_DTO_COLLECTION, flightReservationAlertDTOList);
			if (overlappingSched != null)
				responce.addResponceParam(CommandParamNames.OVERLAPPING_SCHEDULE, overlappingSched);
			responce.addResponceParam(CommandParamNames.FLIGHT_SCHEDULE, schedule);
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			responce.addResponceParam(CommandParamNames.BUILDED_SCHEDULE_FLIGHT_IDS, buildScheduleFlightIds);
			responce.addResponceParam(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
		}
		return responce;
	}

	private void cancelAirportTransfers(Collection<Flight> flights, Collection<Integer> flightIdsHavingReservations)
			throws ModuleException {
		List<Integer> fltSegIDs = new ArrayList<Integer>();
		for (Flight flight : flights) {
			if (flightIdsHavingReservations.contains(flight.getFlightId())) {
				for (FlightSegement fltSeg : flight.getFlightSegements()) {
					fltSegIDs.add(fltSeg.getFltSegId());
				}
			}
		}

		if (fltSegIDs != null && fltSegIDs.size() > 0) {

			Collection<Integer> pnrSegmentIds = new ArrayList<Integer>();
			Map<Integer, Collection<PaxAirportTransferTO>> cancelledAptMap = null;
			ArrayList<Reservation> reservationList = reservationBD.loadReservationsHavingAirportTransfers(fltSegIDs);

			for (Reservation reservation : reservationList) {
				cancelledAptMap = new HashMap<Integer, Collection<PaxAirportTransferTO>>();
				for (PaxAirportTransferTO apt : reservation.getAirportTransfers()) {
					if (cancelledAptMap.containsKey(apt.getPnrPaxId())) {
						cancelledAptMap.get(apt.getPnrPaxId()).add(apt);
					} else {
						Collection<PaxAirportTransferTO> cancelledApts = new ArrayList<PaxAirportTransferTO>();
						cancelledApts.add(apt);
						cancelledAptMap.put(apt.getPnrPaxId(), cancelledApts);
						pnrSegmentIds.add(apt.getPnrSegId());
					}
				}
				if (!cancelledAptMap.isEmpty()) {
					segmentBD.cancelAirportTransferRequests(reservation, pnrSegmentIds, cancelledAptMap, null);
				}
			}
		}
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Integer scheduleId, Boolean force) throws ModuleException {

		if (scheduleId == null || force == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}

	/**
	 * private method to
	 * 
	 * @param schedule
	 * @param flights
	 * @param flightsHasRes
	 *            TODO
	 * @throws ModuleException
	 */
	private void cancelCompletedSchedule(FlightSchedule schedule, Collection<Flight> flights, FlightAlertDTO flightAlertDTO,
			Collection<Flight> flightsToPublish, Collection<Integer> flightsHasRes) throws ModuleException {

		List<Integer> flightSegmentIds = new ArrayList<Integer>();
		Iterator<Flight> flightsIt = flights.iterator();
		while (flightsIt.hasNext()) {

			Flight flight = (Flight) flightsIt.next();

			flightSegmentIds.addAll(SegmentUtil.getFlightSegIds(flight.getFlightSegements()));

			// cencel flight and save it
			flight.setStatus(FlightStatusEnum.CANCELLED.getCode());
			flightDAO.saveFlight(flight);

			// Add the flights to the list which holds the publish flight list
			flightsToPublish.add(flight);
		}
		if (flightsHasRes.size() > 0) {
			cancelAirportTransfers(flights, flightsHasRes);
		}

		// [AirReservation integration] cancel reservations
		// segmentBD.cancelSegments(flightSegmentIds, cancelCharge, null);

		// List flightIds = ScheduledFlightUtil.getFlightIds(flights);
		// [NOTE] Do not call this method to invalidate inventories of cancelled flights
		// flightInventryBD.invalidateFlightInventories(flightIds);

		// cancel the schedule and save it
		schedule.setStatusCode(ScheduleStatusEnum.CANCELLED.getCode());
		schedule.setCancelMsgWaiting(false);
		schedule.setHoldCancelMsgTill(null);
		flightSchedleDAO.saveFlightSchedule(schedule);
	}

	private boolean isSkipFlightUpdate(Date departureDate) {
		boolean isSkip = false;
		if (isScheduleMessageProcess && departureDate.before(CalendarUtil.getCurrentZuluDateTime())) {
			isSkip = true;
		}
		return isSkip;
	}
	
	private Collection<Flight> filterFlownFlights(Collection<Flight> allFlights) {
		if (!isScheduleMessageProcess) {
			return allFlights;
		} else {
			Collection<Flight> filteredFlights = new ArrayList<Flight>();
			Iterator<Flight> it = allFlights.iterator();
			while (it.hasNext()) {
				Flight flight = (Flight) it.next();
				if (!isSkipFlightUpdate(flight.getDepartureDate())) {
					filteredFlights.add(flight);
				} else {
					flight.setScheduleId(null);
					flightDAO.saveFlight(flight);
				}
			}
			return filteredFlights;
		}
	}
	
//	private void sendScheduleCancellationOfficerNotificationEmail(FlightSchedule schedule, int numberOfFlights, boolean success)
//			throws ModuleException {
//		if (AppSysParamsUtil.isEnableOfficerEmailAlertAtFlightCnx()) {
//			FlightScheduleEmail.sendScheduleCancellationOfficerNotification(schedule, numberOfFlights, success);
//		}
//	}

}
