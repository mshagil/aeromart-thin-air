/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.OverlappedScheduleDTO;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for validate conflicting schedules for a given schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkConflictingSchedulesForSchedule"
 */
public class CheckConflictingSchedulesForSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;
	
	private AirportBD airportBD;

	/**
	 * Constructor of the ValidateConflictingSchedules command
	 */
	public CheckConflictingSchedulesForSchedule() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
		airportBD = AirSchedulesUtil.getAirportBD();
	}

	/**
	 * execute method of the validate conflicting schedules command
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		Collection<AffectedPeriod> periodList = (Collection) this.getParameter(CommandParamNames.CREATE_PERIOD_LIST);

		// checking params
		this.checkParams(schedule);

		// create the responce to return
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// if affected create periods or empty null construct it
		if (periodList == null || periodList.size() == 0) {
			AffectedPeriod period = new AffectedPeriod(schedule.getStartDate(), schedule.getStopDate(), schedule.getFrequency());
			periodList = new ArrayList<AffectedPeriod>();
			periodList.add(period);
		}

		LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
		schedule = timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		// check all affected periods
		Iterator<AffectedPeriod> periodListIt = periodList.iterator();
		while (periodListIt.hasNext()) {

			AffectedPeriod period = (AffectedPeriod) periodListIt.next();

			// gets the conflicting schedules
			List<FlightSchedule> conflictList = flightSchedleDAO.getConflictingSchedules(schedule, period);
			// if conflicts exists
			if (conflictList != null && conflictList.size() > 0) {				

				// collection to keep conflict schedule details
				Collection<OverlappedScheduleDTO> collection = new ArrayList<OverlappedScheduleDTO>();

				FlightSchedule fls;
				OverlappedScheduleDTO overlapped;

				Iterator<FlightSchedule> it = conflictList.iterator();

				while (it.hasNext()) {

					fls = (FlightSchedule) it.next();
					fls = timeAdder.addLocalTimeDetailsToFlightSchedule(fls, false);
					
					//Check to stop same local day schedules	

					boolean hasCommonDays = hasCommonDays(fls, schedule);

					if (hasCommonDays) {
						// construct the overlapping DTO
						overlapped = new OverlappedScheduleDTO();
						overlapped.setFlightNumber(fls.getFlightNumber());
						overlapped.setStartDate(fls.getStartDate());
						overlapped.setOperationType(fls.getOperationTypeId() + "");

						// add DTO to the list
						collection.add(overlapped);
					}
					
				}
				
				if(collection.size() > 0){
					// conficts found change the responce accordingly
					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.SCHEDULE_SCHEDULE_CONFLICT);
					responce.addResponceParam(ResponceCodes.SCHEDULE_SCHEDULE_CONFLICT, collection);
					break;
				}
				
			}
		}

		return responce;
	}

	private boolean hasCommonDays(FlightSchedule existingSchedule, FlightSchedule newSchedule) throws ModuleException {

		boolean hasCommonDays = false;

		Date commonStartDate = existingSchedule.getStartDate().after(newSchedule.getStartDate())
				? existingSchedule.getStartDate()
				: newSchedule.getStartDate();

		Date commonStopDate = existingSchedule.getStopDate().before(newSchedule.getStopDate())
				? existingSchedule.getStopDate()
				: newSchedule.getStopDate();

		AffectedPeriod existingScheduleAffectedPeriod = new AffectedPeriod(commonStartDate, commonStopDate,
				existingSchedule.getFrequency());

		AffectedPeriod newScheduleAffectedPeriod = new AffectedPeriod(commonStartDate, commonStopDate,
				newSchedule.getFrequency());

		Collection<Date> existingScheduleFlightDates = ScheduledFlightUtil
				.getFlightDatesForSchedule(existingScheduleAffectedPeriod);
		Collection<Date> newScheduleFlightDates = ScheduledFlightUtil.getFlightDatesForSchedule(newScheduleAffectedPeriod);

		if (!existingScheduleFlightDates.isEmpty() && !newScheduleFlightDates.isEmpty()) {
			for (Date existingScheduleFlightDate : existingScheduleFlightDates) {
				for (Date newScheduleFlightDate : newScheduleFlightDates) {
					if (newScheduleFlightDate.equals(existingScheduleFlightDate)) {
						hasCommonDays = true;
						break;
					}
				}
			}
		}

		return hasCommonDays;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null || schedule.getStartDate() == null || schedule.getStopDate() == null
				|| schedule.getFrequency() == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
	
	
	private boolean conflicingLocalDepartureDates(FlightSchedule  schedule, FlightSchedule fls) throws ModuleException {
		
		boolean   blnCoplicying = false;
		Frequency checkingFrequency = schedule.getFrequencyLocal();
		Frequency conflictFrequency = fls.getFrequencyLocal();
		
		if((checkingFrequency.getDay0() && conflictFrequency.getDay0())
				||(checkingFrequency.getDay1() && conflictFrequency.getDay1())
				||(checkingFrequency.getDay2() && conflictFrequency.getDay2())
				||(checkingFrequency.getDay3() && conflictFrequency.getDay3())
				||(checkingFrequency.getDay4() && conflictFrequency.getDay4())
				||(checkingFrequency.getDay5() && conflictFrequency.getDay5())
				||(checkingFrequency.getDay6() && conflictFrequency.getDay6())){
			
			blnCoplicying =  true;
						
		}
		
		return blnCoplicying;
	}
}
