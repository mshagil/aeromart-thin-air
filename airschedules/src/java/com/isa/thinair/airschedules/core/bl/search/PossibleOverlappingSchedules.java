/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.search;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to search possible overlapping schedules
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="possibleOverlappingSchedules"
 */
public class PossibleOverlappingSchedules extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	/**
	 * constructor of the PossibleOverlappingSchedules command
	 */
	public PossibleOverlappingSchedules() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
	}

	/**
	 * execute method of the PossibleOverlappingSchedules command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);

		// checking params
		this.checkParams(schedule);

		Collection<FlightSchedule> later = flightSchedleDAO.getPossibleLaterOverlappingSchedules(schedule);
		Collection<FlightSchedule> earliar = flightSchedleDAO.getPossibleEarliarOverlappingSchedules(schedule);

		Collection<FlightSchedule> posibles = new ArrayList<FlightSchedule>();
		posibles = ScheduledFlightUtil.appendWithoutDuplicates(later, earliar);

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.addResponceParam(CommandParamNames.FLIGHT_SCHEDULE_LIST, posibles);
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
