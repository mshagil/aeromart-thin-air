package com.isa.thinair.airschedules.core.bl.parse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to parse flights to publish
 * 
 * @author Manoj Dhanushka
 * @isa.module.command name="parseFlightsToPublish"
 */
public class ParseFlightsToPublish extends DefaultBaseCommand {

	// BDs
	private AirportBD airportBD;

	/**
	 * constructor of the ParseFlightsToPublish command
	 */
	public ParseFlightsToPublish() {
		airportBD = AirSchedulesUtil.getAirportBD();
	}

	/**
	 * execute method of the ParseFlightsToPublish command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {
		Collection<Flight> flightList = (Collection<Flight>) this.getParameter(CommandParamNames.FLIGHT_LIST);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		Collection<DisplayFlightDTO> parsedFlightList = null;

		// if flight list has flights
		if (flightList != null && flightList.size() > 0) {
			parsedFlightList = new ArrayList<DisplayFlightDTO>();
			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
			Iterator<Flight> itFlights = flightList.iterator();

			while (itFlights.hasNext()) {

				Flight flight = (Flight) itFlights.next();
				flight = timeAdder.addLocalTimeDetailsToFlight(flight);
				DisplayFlightDTO flightDTO = new DisplayFlightDTO(flight);
				parsedFlightList.add(flightDTO);
			}
			responce.addResponceParam(CommandParamNames.FLIGHT_INFO_LIST, parsedFlightList);

		} else {
			responce.addResponceParam(CommandParamNames.FLIGHT_INFO_LIST, parsedFlightList);
		}

		// return command responce
		return responce;
	}

}
