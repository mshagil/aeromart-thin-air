package com.isa.thinair.airschedules.core.bl.operation;

import java.util.Map;

import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for split bulk schedule
 * 
 * @isa.module.command name="splitBulkSchedule"
 */
public class SplitBulkSchedule  extends DefaultBaseCommand{

	private FlightScheduleDAO flightSchedleDAO;
	private FlightDAO flightDAO;
	
	public SplitBulkSchedule(){
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
	}
	@Override
	public ServiceResponce execute() throws ModuleException {
		// TODO Auto-generated method stub
		String aircraftCode = (String) this.getParameter(CommandParamNames.AIRCRAFT_CODE);
		String newAircraftCode = (String) this.getParameter(CommandParamNames.NEW_AIRCRAFT_CODE);
		Map<Integer, Integer> scheduleIdsMap = flightSchedleDAO.insertSpliitedSchedules(aircraftCode);
		flightSchedleDAO.updatedExistingSchedules(aircraftCode, newAircraftCode, scheduleIdsMap);
		flightDAO.updatedFlights(aircraftCode, newAircraftCode, scheduleIdsMap);
		return new DefaultServiceResponse(true);
	}
	

}
