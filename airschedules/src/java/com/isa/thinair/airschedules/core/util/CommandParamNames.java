/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.util;

/**
 * class to hold all the command parameter names
 * 
 * @author Lasantha
 */
public abstract class CommandParamNames {

	public static final String SEARCH_OUT_BOUND_FLIGHTS = "searchOutBoundFlights";

	public static String FLIGHT_SCHEDULE = "flightSchedule";

	public static String OVERLAPPING_SCHEDULE = "overlappingSchedule";

	public static String FLIGHT_SCHEDULE_LIST = "flightScheduleList";

	public static String FLIGHT_SCHEDULE_INFO_LIST = "flightScheduleInfoList";

	public static String LOADED_EXISTING_SCHEDULE = "loadedExistingSchedule";

	public static String LOADED_EXISTING_OVERLAPPING_SCHEDULE = "loadedExistingOverlappingSchedule";

	public static String AFFECTED_PERIOD = "affectedSplitPeriod";

	public static String CREATE_PERIOD_LIST = "createPeriodList";

	public static String UPDATE_PERIOD_LIST = "updatePeriodList";

	public static String CANCEL_PERIOD_LIST = "cancelPeriodList";

	public static String SCHEDULE_IDS = "scheduleIds";

	public static String USER_NAME = "username";

	public static String SCHEDULE_ID = "scheduleId";

	public static String FORCE = "force";

	public static String START_DATE = "startDate";

	public static String END_DATE = "endDate";

	public static String FLIGHT = "flight";

	public static String OVERLAPPING_FLIGHT = "overlappingFlight";

	public static String EXISTING_FLIGHT = "existingFlight";

	public static String EXISTING_OVERLAPPING_FLIGHT = "existingOverlappingFlight";

	public static String FLIGHT_LIST = "flightList";

	public static String FLIGHT_INFO_LIST = "flightInfoList";

	public static String IDS = "ids";

	public static String ALLERT_TYPE = "allertType";

	public static String DEPARTURE_DATE = "departureDate";

	public static String FLIGHT_ID = "flightId";

	public static String PAGE = "page";

	public static String ADHOC_FLIGHT_PAGE = "adhocFlightpage";

	public static String DISPLAY = "display";

	public static String AVILABLE_FLIGHT_SEARCH_DTO = "AvailableFlightSearchDTO";

	public static String CALENDAR_SEARCH_REQUEST = "CALENDAR_SEARCH_REQUEST";

	public static String FLIGHT_ALLOW_ACTION_DTO = "FlightAllowActionDTO";

	public static String FLIGHT_ALERT_DTO = "FlightAlertDTO";

	public static String FLIGHT_DOWNGRADE_TO_ANY_MODEL = "FlightDowngradeToAny";

	public static String WARNING = "warning";

	public static String AIRPORT_CODE = "airportCode";

	public static String AIRPORT = "airport";

	public static String AIRPORT_DST = "airportDst ";

	public static String IS_DATE_IN_LOCAL = "isDatesInLocal";

	public static String CRITERIA_LIST = "criteriaList";
	
	public static String IS_ADHOC_FLIGHT_DATA_ENABLED = "isAdhocFlightDataEnabled";

	public static String FLIGHT_FLIGHTSEGMENT_CRITERIA_LIST = "flightFlightSegmentCriteriaList";

	public static String START_INDEX = "startIndex";

	public static String PAGE_SIZE = "pageSize";

	public static String FLIGHT_OVERWRITE_FLAG = "overwriteFlag";

	public static String FIRST_LEG_DEP_OFFSET_CHANGED_FLAG = "firstLegDepOffsetChangedFlag";

	public static String IS_UPDATING_LEGS = "isUpdatingLegs";
	
	public static String FIRST_LEG_DAY_DIFFERENCE = "firstLegDayDiff";

	public static String USER_PRINCIPAL = "userPrincipal";

	public static String COMMAND_NAME = "commandName";

	public static String FLIGHT_SEGMENT_EMAIL_DTO_COLLECTION = "flightSegmentEmailDTOCollection";

	public static String LAST_RESPONSE = "lastResponse";

	public static String CHECK_RESERVATIONS_ONLY = "checkReservationsOnly";

	public static String GDS_PARAM_COLLECTOR = "gdsParamCollector";

	public static String BUILDED_SCHEDULE_FLIGHT_IDS = "buildedScheduleFlightIds";

	public static String GDS_IDS = "gdsIds";

	public static String VERSION_ID = "versionID";

	public static String IS_FLIGHT_MANIFEST = "isFlightManifest";
	
	public static String IS_REPORTING = "isReporting";

	public static String ROLL_FORWARD_FLIGHT_SEARCH = "rollForwardFlightSearch";

	public static String RES_FLIGHT_SEGMENT_DTO = "reservationFlightSegmentDTO";

	public static String ROLL_FORWARD_FLIGHT_LIST = "rollForwardFlightList";
	
	public static String FLIGHTS_TO_PUBLISH = "flightsToPublish";

	public static String AIRCRAFT_CODE = "aircraftCode";
	
	public static String NEW_AIRCRAFT_CODE = "newAircraftCode";
	
	public static String ACTIVATE_PAST_FLIGHT = "ActivatePastFlights";
	
	public static String PNL_DEPARTURE_GAP = "PnlDepartureGap";
	
	public static String GDS_ID = "gdsID";
	
	public static String SCHEDULE_START_DATE = "scheduleStartDate";
	
	public static String SCHEDULE_END_DATE = "scheduleEndDate";
	
	public static String IS_SELECTED_SCHEDULES = "isSelectedSchedules";
	
	public static String EMAIL_ADDRESS = "emailAddress";

	public static String SSIM_RECORD_DTO = "ssimRecordDTO";	
	
	public static String EXTERNAL_SSM_SENDING = "externalSSMSending";
	
	public static String EXTERNAL_SITA_ADDRESS = "externalSitaAddress";
	
	public static String EXTERNAL_EMAIL_ADDRESS = "externalEmailAddress";

	public static String NEW_SCHEDULE_IDS_FROM_SPLIT = "newSheduleIDsFromSplit";

	public static String IS_REVISE_FREQUENCY = "isReviseFrequency";

	public static String EXISITNG_SSM_SUB_SCHEDULES = "existingSubSchedules";
	
	public static String NEW_SSM_SUB_SCHEDULES = "newSubSchedules";	
	
	public static String IS_SCHEDULE_PERIOD_CHANGE = "isPeriodChange";
	
	public static String IS_SCHEDULE_SPLIT = "isScheduleSplit";

	public static String IS_TRANSACTIONAL = "isTransactional";

	public static String IS_SHEDULE_MESSAGE_PROCESS = "isScheduleMessageProcessing";
	
	public static String IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL = "isEnableAttachDefaultAnciTemplate";
	
	public static String HOLD_CANCEL_MSG_TILL = "holdCancelMsgTill";
	
	public static String AGENT_APPLICABLE_ONDS = "agentApplicableOnds";
	
	public static String SCHEDULE_REVISION_DTO = "scheduleRevisionTO";
	
	public static String CREATED_FLIGHT_DATES = "createdFlightDates";

	public static String CANCELED_FLIGHT_DATES = "canceledFlightDates";

	public static String BUILD_DEFAULT_INVENTORY = "buildDefaultInventory";
}
