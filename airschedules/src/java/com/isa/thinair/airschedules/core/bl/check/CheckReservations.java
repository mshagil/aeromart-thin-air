/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.FlightSegmentResInvSummaryDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.core.bl.noncmd.FlightsForAffectedPeriod;
import com.isa.thinair.airschedules.core.bl.noncmd.ReservationsForFlights;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.PeriodsUtil;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for check reservations
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkReservations"
 */
public class CheckReservations extends DefaultBaseCommand {

	private static final int CHECK_FOR_FLIGHTS = 0;
	private static final int CHECK_FOR_PERIODS = 1;
	private static final int CHECK_FOR_SCHEDULES = 2;

	// Dao's
	private FlightDAO flightDAO;
	private FlightSegementDAO flightSegmentDAO;
	private FlightScheduleDAO flightSchedleDAO;

	// BD's
	private FlightInventoryResBD flightInventryResBD;
	private FlightInventoryBD flightInventryBD;
	private SegmentBD segmentBD;

	// helpers
	private FlightsForAffectedPeriod fltsForAffectedPeriod;
	private ReservationsForFlights reservation4Flights;

	/**
	 * constructor of the CheckReservations command
	 */
	public CheckReservations() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		flightSegmentDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// looking up bd's
		flightInventryResBD = AirSchedulesUtil.getFlightInventoryResBD();
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();

		segmentBD = AirSchedulesUtil.getSegmentBD();

		// helpers
		fltsForAffectedPeriod = new FlightsForAffectedPeriod(flightDAO);
		reservation4Flights = new ReservationsForFlights(flightDAO, flightSegmentDAO, flightInventryBD, segmentBD);
	}

	/**
	 * execute method of the CheckReservations command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Collection<AffectedPeriod> cancelPeriods = (Collection<AffectedPeriod>) this.getParameter(CommandParamNames.CANCEL_PERIOD_LIST);
		Integer flightId = (Integer) this.getParameter(CommandParamNames.FLIGHT_ID);
		Boolean forceCancel = (Boolean) this.getParameter(CommandParamNames.FORCE);
		FlightSchedule existing = (FlightSchedule) this.getParameter(CommandParamNames.LOADED_EXISTING_SCHEDULE);
		Boolean isOverwrite = (Boolean) this.getParameter(CommandParamNames.FLIGHT_OVERWRITE_FLAG);
		FlightSchedule existingOverlapping = (FlightSchedule) this
				.getParameter(CommandParamNames.LOADED_EXISTING_OVERLAPPING_SCHEDULE);
		Integer scheduleId = (Integer) this.getParameter(CommandParamNames.SCHEDULE_ID);
		Boolean checkReservationsOnly = (Boolean) this.getParameter(CommandParamNames.CHECK_RESERVATIONS_ONLY);

		// checking params
		int type = this.checkParams(cancelPeriods, flightId, scheduleId);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// if not force cancel then check for reservations
		if (!forceCancel.booleanValue()) {

			// if check reservations for periods for schedule
			if (type == CheckReservations.CHECK_FOR_PERIODS) {

				boolean bothPending = existing.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode());
				if (existingOverlapping != null)
					bothPending = bothPending
							&& existingOverlapping.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode());

				if (!bothPending) {

					Iterator<AffectedPeriod> itCancelPeriods = cancelPeriods.iterator();

					Collection<Integer> flightsHasRes = new ArrayList<Integer>();
					List<Integer> flihgtIdList = new ArrayList<Integer>();

					while (itCancelPeriods.hasNext()) {

						AffectedPeriod period = (AffectedPeriod) itCancelPeriods.next();
						List<Integer> flightIds = fltsForAffectedPeriod.getFlightIdsForCancelPeriod(period, existing.getScheduleId()
								.intValue(), isOverwrite.booleanValue());

						if (flightIds.size() > 0) {
							flihgtIdList.addAll(flightIds);
						}
					}

					if (existing.isOverlapping() && existingOverlapping != null) {

						int millsDiff = (int) (existingOverlapping.getStartDate().getTime() - existing.getStartDate().getTime());
						int dayDiff = CalendarUtil.daysUntil(existing.getStartDate(), existingOverlapping.getStartDate());

						Iterator<AffectedPeriod> itCancelPeriodsOverlap = cancelPeriods.iterator();

						while (itCancelPeriodsOverlap.hasNext()) {

							AffectedPeriod period = (AffectedPeriod) itCancelPeriodsOverlap.next();
							AffectedPeriod overlapPeriod = PeriodsUtil.getPeriodForOverlapping(period, millsDiff, dayDiff);

							FlightsForAffectedPeriod fltsForAffectedPeriod = new FlightsForAffectedPeriod(flightDAO);
							List<Integer> flightIds = fltsForAffectedPeriod.getFlightIdsForCancelPeriod(overlapPeriod, existing
									.getOverlapingScheduleId().intValue(), isOverwrite.booleanValue());

							if (flightIds.size() > 0) {
								flihgtIdList.addAll(flightIds);
							}
						}
					}

					if (flihgtIdList.size() > 0) {

						flightsHasRes = flightInventryResBD.filterFlightsHavingReservations(flihgtIdList);
					}

					if (flightsHasRes.size() > 0) {

						HashMap<FlightSummaryDTO, Collection<FlightSegmentResInvSummaryDTO>> flightDetailsWithResMap = reservation4Flights.getFlightDetailsWithResMap(flightsHasRes, null);

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE);
						responce.addResponceParam(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE, flightDetailsWithResMap);

					} else if (!flihgtIdList.isEmpty()) {

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.RESERVATIONS_NOT_FOUND_FOR_SCHEDULE);
					}
				}
				// if check reservations for flights
			} else if (type == CheckReservations.CHECK_FOR_FLIGHTS) {

				Collection<Integer> flightsHasRes = new ArrayList<Integer>();
				List<Integer> flightIds = new ArrayList<Integer>();
				flightIds.add(flightId);

				Flight flight = flightDAO.getFlight(flightId.intValue());

				if (flight.isOverlapping()) {

					Integer overlapId = flight.getOverlapingFlightId();
					flightIds.add(overlapId);
				}

				if (flightIds.size() > 0) {

					flightsHasRes = flightInventryResBD.filterFlightsHavingReservations(flightIds);
				}

				if (flightsHasRes.size() > 0) {

					HashMap<FlightSummaryDTO,Collection<FlightSegmentResInvSummaryDTO>> flightDetailsWithResMap = reservation4Flights.getFlightDetailsWithResMap(flightsHasRes, null);

					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_FLIGHT);
					responce.addResponceParam(ResponceCodes.RESERVATIONS_FOUND_FOR_FLIGHT, flightDetailsWithResMap);

				} else if (checkReservationsOnly != null && checkReservationsOnly.booleanValue()) {

					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.RESERVATIONS_NOT_FOUND_FOR_FLIGHT);

				} else {

					responce.setSuccess(true);
				}

			} else if (type == CheckReservations.CHECK_FOR_SCHEDULES) {

				// load the corresponding flight schedule
				FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());

				// if no schedule found for id throw exception
				if (schedule == null)
					throw new ModuleException("airschedules.logic.bl.schedule.noschedule");

				if (!schedule.isOverlapping()) {

					if (!schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())) {

						Collection<Flight> flights = flightSchedleDAO.getFlightsForSchedule(schedule.getScheduleId().intValue());
						List<Integer> flihgtIdList = new ArrayList<Integer>();
						Collection<Integer> flightsHasRes = new ArrayList<Integer>();

						if (flights.size() > 0) {
							// flihgtIdList = ScheduledFlightUtil.getFlightIds(flights);
							flihgtIdList.addAll(ScheduledFlightUtil.getFlightIds(flights));
							flightsHasRes = flightInventryResBD.filterFlightsHavingReservations(flihgtIdList);
						}

						if (flightsHasRes.size() > 0) {

							HashMap<FlightSummaryDTO, Collection<FlightSegmentResInvSummaryDTO>> flightDetailsWithResMap = reservation4Flights.getFlightDetailsWithResMap(flightsHasRes, null);

							responce.setSuccess(false);
							responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE);
							responce.addResponceParam(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE, flightDetailsWithResMap);

						} else if (checkReservationsOnly != null && checkReservationsOnly.booleanValue()) {

							responce.setSuccess(false);
							responce.setResponseCode(ResponceCodes.RESERVATIONS_NOT_FOUND_FOR_SCHEDULE);

						} else {

							responce.setSuccess(true);
						}

					}

				} else {
					// if schedule is overlapping
					FlightSchedule overlappingSched = flightSchedleDAO.getFlightSchedule(schedule.getOverlapingScheduleId()
							.intValue());

					if (!schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())
							|| !overlappingSched.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())) {

						Collection<Flight> flights = flightSchedleDAO.getFlightsForSchedule(schedule.getScheduleId().intValue());
						Collection<Flight> overlapFlights = flightSchedleDAO.getFlightsForSchedule(overlappingSched.getScheduleId()
								.intValue());
						Collection<Integer> flightsHasRes = new ArrayList<Integer>();
						List<Integer> flihgtIdList = new ArrayList<Integer>();

						if (flights.size() > 0 || overlapFlights.size() > 0) {

							// flihgtIdList = ScheduledFlightUtil.getFlightIds(flights);
							flihgtIdList.addAll(ScheduledFlightUtil.getFlightIds(flights));
							List<Integer> overlapFlihgtIdList = ScheduledFlightUtil.getFlightIds(overlapFlights);
							flihgtIdList.addAll(overlapFlihgtIdList);
							flightsHasRes = flightInventryResBD.filterFlightsHavingReservations(flihgtIdList);
						}

						if (flightsHasRes.size() > 0) {

							HashMap<FlightSummaryDTO, Collection<FlightSegmentResInvSummaryDTO>> flightDetailsWithResMap = reservation4Flights.getFlightDetailsWithResMap(flightsHasRes, null);

							responce.setSuccess(false);
							responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE);
							responce.addResponceParam(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE, flightDetailsWithResMap);

						} else if (checkReservationsOnly != null && checkReservationsOnly.booleanValue()) {

							responce.setSuccess(false);
							responce.setResponseCode(ResponceCodes.RESERVATIONS_NOT_FOUND_FOR_SCHEDULE);

						} else {

							responce.setSuccess(true);
						}

					}
				}

			} // end of CheckReservations.CHECK_FOR_SCHEDULES

		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private int checkParams(Collection<AffectedPeriod> cancelPeriods, Integer flightId, Integer scheduleId) throws ModuleException {

		int validationType = -1;

		if (flightId != null) {

			validationType = CheckReservations.CHECK_FOR_FLIGHTS;

		} else if (cancelPeriods != null) {

			validationType = CheckReservations.CHECK_FOR_PERIODS;

		} else if (scheduleId != null) {

			validationType = CheckReservations.CHECK_FOR_SCHEDULES;

		} else {
			// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
		}

		return validationType;
	}
}
