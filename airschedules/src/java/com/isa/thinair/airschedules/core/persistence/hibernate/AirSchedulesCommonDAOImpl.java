/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.hibernate;

import java.util.Collection;

import com.isa.thinair.airschedules.api.model.OperationType;
import com.isa.thinair.airschedules.api.model.ScheduleBuildStatus;
import com.isa.thinair.airschedules.api.model.ScheduleStatus;
import com.isa.thinair.airschedules.core.persistence.dao.AirSchedulesCommonDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author Lasantha pambagoda
 * @isa.module.dao-impl dao-name="airSchedulesCommonDAO"
 */
public class AirSchedulesCommonDAOImpl extends PlatformHibernateDaoSupport implements AirSchedulesCommonDAO {

	/**
	 * gets the operation type for given ID
	 */
	public OperationType getOperationType(int id) {
		return (OperationType) get(OperationType.class, new Integer(id));
	}

	/**
	 * gets all Operation Types
	 */
	public Collection<OperationType> getAllOpertationTypes() {
		return find("from OperationType", OperationType.class);
	}

	/**
	 * gets all schedule build status
	 */
	public Collection<ScheduleBuildStatus> getAllScheduleBuildStatus() {
		return find("from ScheduleBuildStatus", ScheduleBuildStatus.class);
	}

	/**
	 * gets all schedule status
	 */
	public Collection<ScheduleStatus> getAllScheduleStatus() {
		return find("from ScheduleStatus", ScheduleStatus.class);
	}

}
