/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Lasantha
 * @isa.module.command name="checkValidSegments"
 */
public class CheckValidSegments extends DefaultBaseCommand {

	/**
	 * constructor of the CheckValidSegments command
	 */
	public CheckValidSegments() {

	}

	/**
	 * execute method of the CheckValidSegments command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);

		// checking params
		this.checkParams(schedule, flight);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		boolean validSegFound = false;

		if (schedule != null) {

			Set<FlightScheduleSegment> segments = schedule.getFlightScheduleSegments();
			Iterator<FlightScheduleSegment> itSegments = segments.iterator();

			while (itSegments.hasNext()) {
				FlightScheduleSegment segment = (FlightScheduleSegment) itSegments.next();
				validSegFound = segment.getValidFlag();
				if (validSegFound)
					break;
			}
		}

		if (flight != null) {

			Set<FlightSegement> segments = flight.getFlightSegements();
			Iterator<FlightSegement> itSegments = segments.iterator();

			while (itSegments.hasNext()) {
				FlightSegement segment = (FlightSegement) itSegments.next();
				validSegFound = segment.getValidFlag();
				if (validSegFound)
					break;
			}
		}

		if (!validSegFound) {
			responce.setSuccess(false);
			responce.setResponseCode(ResponceCodes.ALL_SEGMENTS_INVALID);
			return responce;
		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule, Flight flight) throws ModuleException {

		if (schedule == null && flight == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
