package com.isa.thinair.airschedules.core.bl.noncmd;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airreservation.api.dto.FlightReservationSummaryDTO;
import com.isa.thinair.airreservation.api.dto.FlightSegmentReservationDTO;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airschedules.api.dto.FlightSegmentResInvSummaryDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * utility class to work with reservations
 * 
 * @author Chandana Kithalagama
 */
public class ReservationsForFlights {

	// Dao's
	private FlightDAO flightDAO;
	private FlightSegementDAO flightSegmentDAO;

	// BD's
	private FlightInventoryBD flightInventryBD;
	private SegmentBD segmentBD;

	/**
	 * constructor of the ReservationsForFlights command
	 */
	public ReservationsForFlights(FlightDAO flightDAO, FlightSegementDAO flightSegmentDAO, FlightInventoryBD flightInventryBD,
			SegmentBD segmentBD) {

		this.flightDAO = flightDAO;
		this.flightSegmentDAO = flightSegmentDAO;
		this.flightInventryBD = flightInventryBD;
		this.segmentBD = segmentBD;
	}

	/**
	 * private method to get the flight details with the reservation
	 * 
	 * @param flightsHasRes
	 * @return
	 * @throws ModuleException
	 */
	public LinkedHashMap<FlightSummaryDTO,Collection<FlightSegmentResInvSummaryDTO>> getFlightDetailsWithResMap(Collection<Integer> flightsHasRes, 
			Collection<Integer> flightSegmentsHasRes)
			throws ModuleException {
		LinkedHashMap<FlightSummaryDTO,Collection<FlightSegmentResInvSummaryDTO>> map = new LinkedHashMap<FlightSummaryDTO,Collection<FlightSegmentResInvSummaryDTO>>();
		List<FlightSummaryDTO> summaryList = flightDAO.getFlightSummary(flightsHasRes);
		Collection<FlightInventorySummaryDTO> invSummList = flightInventryBD.getFlightSegmentInventoriesSummary(flightsHasRes);
		Collection<FlightReservationSummaryDTO> resSummList = (Collection) segmentBD.getFlightReservationsSummary(flightsHasRes);
		Iterator<FlightSummaryDTO> itSum = summaryList.iterator();
		boolean resFound = false;
		while (itSum.hasNext()) {
			FlightSummaryDTO summary = (FlightSummaryDTO) itSum.next();
			Iterator<FlightInventorySummaryDTO> itInvSum = invSummList.iterator();
			while (itInvSum.hasNext()) {
				FlightInventorySummaryDTO invSummaryDTO = (FlightInventorySummaryDTO) itInvSum.next();
				if ((summary != null) && (invSummaryDTO != null) && (summary.getFlightId() == invSummaryDTO.getFlightId())) {
					Collection<FlightSegmentResInvSummaryDTO> flightResInv = new ArrayList<FlightSegmentResInvSummaryDTO>();
					Collection<FCCSegInvSummaryDTO> segInv = invSummaryDTO.getSegmentInventories();
					Iterator<FCCSegInvSummaryDTO> itSegInv = segInv.iterator();

					int flightId = invSummaryDTO.getFlightId();

					while (itSegInv.hasNext()) {
						FCCSegInvSummaryDTO invSegDTO = (FCCSegInvSummaryDTO) itSegInv.next();
						// added to find the segment ID for flight id and segment code
						String segmentCode = invSegDTO.getSegmentCode();
						FlightSegement segment = flightSegmentDAO.getFlightSegement(flightId, segmentCode);
						if (segment != null) {
							int segmentId = segment.getFltSegId().intValue();
							if ((invSegDTO != null) && (invSegDTO.getSegmentCode() != null)) {
								resFound = false;
								Iterator<FlightReservationSummaryDTO> itResSum = resSummList.iterator();
								while (itResSum.hasNext()) {
									FlightReservationSummaryDTO resSummaryDTO = (FlightReservationSummaryDTO) itResSum.next();
									if ((resSummaryDTO != null)
											&& (invSummaryDTO.getFlightId() == (resSummaryDTO.getFlightId()).intValue())) {
										Collection<FlightSegmentReservationDTO> segRes = resSummaryDTO.getSegmentReservations();
										Iterator<FlightSegmentReservationDTO> itSegRes = segRes.iterator();
										while (itSegRes.hasNext()) {
											FlightSegmentReservationDTO resSegDTO = (FlightSegmentReservationDTO) itSegRes.next();
											if ((resSegDTO != null)
													&& (resSegDTO.getSegmentCode() != null)
													&& (resSegDTO.getSegmentCode().trim().equalsIgnoreCase(invSegDTO
															.getSegmentCode().trim()))
													&& (flightSegmentsHasRes == null || (flightSegmentsHasRes != null && flightSegmentsHasRes
															.contains(new Integer(segmentId))))) {
												FlightSegmentResInvSummaryDTO flgSegResInvDTO = new FlightSegmentResInvSummaryDTO(
														invSegDTO.getSegmentCode().trim(), segmentId, invSegDTO.getAdultSold(),
														invSegDTO.getInfantSold(), resSegDTO.getConfirmedPnrs(),
														invSegDTO.getAdultOnhold(), invSegDTO.getInfantOnhold(),
														resSegDTO.getOnholdPnrs());
												flightResInv.add(flgSegResInvDTO);
												resFound = true;
												break;
											}
										}
									}
									if (resFound) {
										break;
									}
								}
							}
							if (!flightResInv.isEmpty()) {
								map.put(summary, flightResInv);
							}
						}
					}
				}
			}
		}
		return map;
	}
}
