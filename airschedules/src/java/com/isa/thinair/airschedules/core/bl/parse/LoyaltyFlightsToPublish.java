package com.isa.thinair.airschedules.core.bl.parse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author rumesh
 * @isa.module.command name="flightForPublishForLoyalty"
 */
public class LoyaltyFlightsToPublish extends DefaultBaseCommand {

	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		Collection<Flight> flightList = (Collection<Flight>) this.getParameter(CommandParamNames.FLIGHT_LIST);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		Collection<DisplayFlightDTO> parsedFlightList = null;

		Map<String, Flight> flightMap = new HashMap<String, Flight>();
		// if flight list has flights
		if (flightList != null && flightList.size() > 0) {
			parsedFlightList = new ArrayList<DisplayFlightDTO>();
			for (Flight flight : flightList) {
				if (!flightMap.containsKey(flight.getFlightNumber())) {
					flightMap.put(flight.getFlightNumber(), flight);
				}
			}

			if (!flightMap.isEmpty()) {
				for (Flight flight : flightMap.values()) {
					DisplayFlightDTO flightDTO = new DisplayFlightDTO(flight);
					parsedFlightList.add(flightDTO);
				}

			}


			responce.addResponceParam(CommandParamNames.FLIGHT_INFO_LIST, parsedFlightList);

		} else {
			responce.addResponceParam(CommandParamNames.FLIGHT_INFO_LIST, parsedFlightList);
		}

		// return command responce
		return responce;
	}

}
