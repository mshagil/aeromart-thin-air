/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.GDSFlightEventCollector;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="integrateAmmendSchedule"
 */
public class IntegrateAmmendSchedule extends DefaultBaseCommand {

	// BDs
	private GDSPublishingBD gdsPublishingBD;

	/**
	 * constructor of the ValidateCreateFlights command
	 */
	public IntegrateAmmendSchedule() {

		// looking up BDs
		gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
	}

	/**
	 * execute method of the ValidateCreateFlights command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			GDSScheduleEventCollector sGdsEvents = (GDSScheduleEventCollector) this
					.getParameter(CommandParamNames.GDS_PARAM_COLLECTOR);
			FlightSchedule overlapingUpdatedschedule = (FlightSchedule) this.getParameter(CommandParamNames.OVERLAPPING_SCHEDULE);

			if (overlapingUpdatedschedule != null) {
				sGdsEvents.setUpdatedOverlapSchedule(ScheduledFlightUtil.getCopyOfSchedule(overlapingUpdatedschedule));
				if (sGdsEvents.getExistingOverlapSchedule() != null) {
					sGdsEvents.getUpdatedOverlapSchedule().setScheduleId(sGdsEvents.getExistingOverlapSchedule().getScheduleId());
				}
			}

			String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
					.getServiceTypeForScheduleFlight();

			sGdsEvents.setServiceType(serviceType);

			Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_AMMEND_SCHEDULE);
			env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, sGdsEvents);

			gdsPublishingBD.publishMessage(env);
			
			Collection<Flight> flightsToPublish = (Collection<Flight>) this.getParameter(CommandParamNames.FLIGHTS_TO_PUBLISH);
			if (flightsToPublish != null && !flightsToPublish.isEmpty()
					&& sGdsEvents.containsAction(GDSFlightEventCollector.MODEL_CHANGED)) {

				Collection<Integer> flightIds = new ArrayList<Integer>();
				for (Flight flight : flightsToPublish) {
					flightIds.add(flight.getFlightId());
				}

				NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
				NotifyFlightEventRQ newFlightEvent = new NotifyFlightEventRQ();

				Set<Integer> gDSIds = sGdsEvents.getExistingSchedule().getGdsIds();

				if (gDSIds != null && !gDSIds.isEmpty()) {
					newFlightEvent.setGdsIdsAdded(gDSIds);
					newFlightEvent.setFlightEventCode(FlightEventCode.FLIGHT_GDS_PUBLISHED_CHANGED);
					newFlightEvent.setFlightIds(flightIds);

					notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEvent);
					AirSchedulesUtil.getFlightInventoryBD().notifyFlightEvent(notifyFlightEventsRQ);
				}

			}
		}
		// return command responce
		return lastResponse;
	}
}
