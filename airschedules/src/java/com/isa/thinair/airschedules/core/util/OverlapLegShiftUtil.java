package com.isa.thinair.airschedules.core.util;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import com.isa.thinair.airschedules.api.dto.OverlapLegDeltaDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;

public class OverlapLegShiftUtil {
	/**
	 * private method to find time difference(s) (delta) of overlapping leg(s) of two schedules populates a
	 * LinkedHashMap of OverlapLegDeltaDTO(s)
	 * 
	 * @param updated
	 * @param overlapping
	 * @return
	 */
	private static LinkedHashMap<String, OverlapLegDeltaDTO>  getTimeDeltaForOverlappingSegments(FlightSchedule updated, FlightSchedule existing) {

		LinkedHashMap<String, OverlapLegDeltaDTO> legDeltaMap = new LinkedHashMap<String, OverlapLegDeltaDTO> ();

		Collection<FlightScheduleLeg> updLegs = updated.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> itUpdLegs = updLegs.iterator();

		while (itUpdLegs.hasNext()) {

			FlightScheduleLeg updLeg = (FlightScheduleLeg) itUpdLegs.next();
			Collection<FlightScheduleLeg> extLegs = existing.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> itExtLeg = extLegs.iterator();

			while (itExtLeg.hasNext()) {

				FlightScheduleLeg extLeg = (FlightScheduleLeg) itExtLeg.next();
				if (updLeg.getOrigin().equals(extLeg.getOrigin()) && updLeg.getDestination().equals(extLeg.getDestination())) {

					Date updLegDepartureTime = CalendarUtil.getOfssetAddedTime(updLeg.getEstDepartureTimeZulu(),
							updLeg.getEstDepartureDayOffset());
					Date extLegDepartureTime = CalendarUtil.getOfssetAddedTime(extLeg.getEstDepartureTimeZulu(),
							extLeg.getEstDepartureDayOffset());
					Date updLegArrivalTime = CalendarUtil.getOfssetAddedTime(updLeg.getEstArrivalTimeZulu(),
							updLeg.getEstArrivalDayOffset());
					Date extLegArrivalTime = CalendarUtil.getOfssetAddedTime(extLeg.getEstArrivalTimeZulu(),
							extLeg.getEstArrivalDayOffset());
					long depDelta = updLegDepartureTime.getTime() - extLegDepartureTime.getTime();
					long arrDelta = updLegArrivalTime.getTime() - extLegArrivalTime.getTime();

					if (depDelta != 0 || arrDelta != 0) {

						OverlapLegDeltaDTO delta = new OverlapLegDeltaDTO();
						delta.setDepartureDeltaInMillis(depDelta);
						delta.setArrivalDeltaInMillis(arrDelta);
						String mapKey = updLeg.getOrigin() + "/" + updLeg.getDestination();

						legDeltaMap.put(mapKey, delta);
					}
				}
			}
		}
		return legDeltaMap;
	}

	/**
	 * private method to get time difference(s) of ovelapping leg(s) of two flights returns a LinkedHashMap of
	 * OverlapLegDeltaDTOs
	 * 
	 * @param updated
	 * @param overlapping
	 * @return
	 */
	private static LinkedHashMap<String, OverlapLegDeltaDTO> getTimeDeltaForOverlappingSegments(Flight updated, Flight existing) {

		LinkedHashMap<String, OverlapLegDeltaDTO>  legDeltaMap = new LinkedHashMap<String, OverlapLegDeltaDTO> ();

		Set<FlightLeg> updLegs = updated.getFlightLegs();
		Iterator<FlightLeg> itUpdLeg = updLegs.iterator();

		while (itUpdLeg.hasNext()) {

			FlightLeg updleg = (FlightLeg) itUpdLeg.next();
			Set<FlightLeg> extLegs = existing.getFlightLegs();
			Iterator<FlightLeg> itExtLeg = extLegs.iterator();

			while (itExtLeg.hasNext()) {

				FlightLeg extleg = (FlightLeg) itExtLeg.next();

				// get the corresponding legs
				if (updleg.getOrigin().equals(extleg.getOrigin()) && updleg.getDestination().equals(extleg.getDestination())) {

					Date updLegDepartureTime = CalendarUtil.getOfssetAndTimeAddedDate(updated.getDepartureDate(),
							updleg.getEstDepartureTimeZulu(), updleg.getEstDepartureDayOffset());
					Date extLegDepartureTime = CalendarUtil.getOfssetAndTimeAddedDate(existing.getDepartureDate(),
							extleg.getEstDepartureTimeZulu(), extleg.getEstDepartureDayOffset());
					Date updLegArrivalTime = CalendarUtil.getOfssetAndTimeAddedDate(updated.getDepartureDate(),
							updleg.getEstArrivalTimeZulu(), updleg.getEstArrivalDayOffset());
					Date extLegArrivalTime = CalendarUtil.getOfssetAndTimeAddedDate(existing.getDepartureDate(),
							extleg.getEstArrivalTimeZulu(), extleg.getEstArrivalDayOffset());

					long depDelta = updLegDepartureTime.getTime() - extLegDepartureTime.getTime();
					long arrDelta = updLegArrivalTime.getTime() - extLegArrivalTime.getTime();

					if (depDelta != 0 || arrDelta != 0) {

						OverlapLegDeltaDTO delta = new OverlapLegDeltaDTO();
						delta.setDepartureDeltaInMillis(depDelta);
						delta.setArrivalDeltaInMillis(arrDelta);
						String mapKey = updleg.getOrigin() + "/" + updleg.getDestination();

						legDeltaMap.put(mapKey, delta);
					}
				}
			}
		}

		return legDeltaMap;
	}

	/**
	 * private method to get time difference(s) of the overlapping leg(s) between an updated schedule and a given flight
	 * of the ovelapping schedule returns a LinkedHashMap of OverlapLegDeltaDTOs
	 * 
	 * @param updatedSchedule
	 * @param overlappingFlight
	 * @return
	 */
	private static LinkedHashMap<String, OverlapLegDeltaDTO> getTimeDeltaForOverlappingSegments(FlightSchedule updatedSchedule, Flight existingFlight) {

		LinkedHashMap<String, OverlapLegDeltaDTO> legDeltaMap = new LinkedHashMap<String, OverlapLegDeltaDTO>();

		Collection<FlightScheduleLeg> updScheduleLegs = updatedSchedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> itUpdSchLegs = updScheduleLegs.iterator();

		while (itUpdSchLegs.hasNext()) {

			FlightScheduleLeg updScheduleLeg = (FlightScheduleLeg) itUpdSchLegs.next();
			Collection<FlightLeg> ovelapLegs = existingFlight.getFlightLegs();
			Iterator<FlightLeg> itOvelapLeg = ovelapLegs.iterator();

			while (itOvelapLeg.hasNext()) {

				FlightLeg overlapFlightLeg = (FlightLeg) itOvelapLeg.next();
				if (updScheduleLeg.getOrigin().equals(overlapFlightLeg.getOrigin())
						&& updScheduleLeg.getDestination().equals(overlapFlightLeg.getDestination())) {

					Date updLegDepartureTime = CalendarUtil.getOfssetAddedTime(updScheduleLeg.getEstDepartureTimeZulu(),
							updScheduleLeg.getEstDepartureDayOffset());
					Date extLegDepartureTime = CalendarUtil.getOfssetAddedTime(overlapFlightLeg.getEstDepartureTimeZulu(),
							overlapFlightLeg.getEstDepartureDayOffset());
					Date updLegArrivalTime = CalendarUtil.getOfssetAddedTime(updScheduleLeg.getEstArrivalTimeZulu(),
							updScheduleLeg.getEstArrivalDayOffset());
					Date extLegArrivalTime = CalendarUtil.getOfssetAddedTime(overlapFlightLeg.getEstArrivalTimeZulu(),
							overlapFlightLeg.getEstArrivalDayOffset());
					long depDelta = updLegDepartureTime.getTime() - extLegDepartureTime.getTime();
					long arrDelta = updLegArrivalTime.getTime() - extLegArrivalTime.getTime();

					if (depDelta != 0 || arrDelta != 0) {

						OverlapLegDeltaDTO delta = new OverlapLegDeltaDTO();
						delta.setDepartureDeltaInMillis(depDelta);
						delta.setArrivalDeltaInMillis(arrDelta);
						String mapKey = updScheduleLeg.getOrigin() + "/" + updScheduleLeg.getDestination();

						legDeltaMap.put(mapKey, delta);
					}
				}
			}
		}
		return legDeltaMap;
	}

	/**
	 * private method to update the ovelapping segment timings (overlapping schedule) to match with updated schedule
	 * overlapping segment timings.
	 * 
	 * @param overlapping
	 * @param updated
	 * @param existing
	 * @return
	 */

	public static FlightSchedule updateOverlappingFromUpdated(FlightSchedule overlappingSche, FlightSchedule updatedSche,
			FlightSchedule existingSche) throws ModuleException {

		// get overlap leg time change (update and existing)
		LinkedHashMap<String, OverlapLegDeltaDTO> legDeltaMap = OverlapLegShiftUtil.getTimeDeltaForOverlappingSegments(updatedSche, existingSche);
		boolean overlapLegTimeChanged = false;

		if (legDeltaMap != null && legDeltaMap.size() > 0) {

			// find the overlapping leg(s) of overlapping schedule
			Set<FlightScheduleLeg> updatedLegs = updatedSche.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> itUpdatedLegs = updatedLegs.iterator();

			while (itUpdatedLegs.hasNext()) {

				FlightScheduleLeg updatedLeg = (FlightScheduleLeg) itUpdatedLegs.next();
				Set<FlightScheduleLeg> overlapLegs = overlappingSche.getFlightScheduleLegs();
				Iterator<FlightScheduleLeg> itOverlapLegs = overlapLegs.iterator();

				while (itOverlapLegs.hasNext()) {

					FlightScheduleLeg overlapLeg = (FlightScheduleLeg) itOverlapLegs.next();

					if (updatedLeg.getOrigin().equals(overlapLeg.getOrigin())
							&& updatedLeg.getDestination().equals(overlapLeg.getDestination())) {

						String mapKey = updatedLeg.getOrigin() + "/" + updatedLeg.getDestination();

						OverlapLegDeltaDTO legDelta = (OverlapLegDeltaDTO) legDeltaMap.get(mapKey);

						if (legDelta != null) {

							overlapLegTimeChanged = true;

							// calculate dep/arr time deltas
							Date adjustedDepTime = CalendarUtil.getOfssetInMilisecondAddedDate(
									overlapLeg.getEstDepartureTimeZulu(), (int) legDelta.getDepartureDeltaInMillis());
							int dayOffDep = CalendarUtil.daysUntil(overlapLeg.getEstDepartureTimeZulu(), adjustedDepTime);

							Date adjustedArrTime = CalendarUtil.getOfssetInMilisecondAddedDate(
									overlapLeg.getEstArrivalTimeZulu(), (int) legDelta.getArrivalDeltaInMillis());
							int dayOffArr = CalendarUtil.daysUntil(overlapLeg.getEstArrivalTimeZulu(), adjustedArrTime);

							// adjust dep/arr times
							overlapLeg.setEstDepartureTimeZulu(CalendarUtil.getOnlyTime(adjustedDepTime));
							overlapLeg.setEstArrivalTimeZulu(CalendarUtil.getOnlyTime(adjustedArrTime));

							// TODO if a flight exist for the start date, adjust start date
							// adjust first leg origin time - could affect schedule start date
							if (overlapLeg.getLegNumber() == 1) {
								Date firstLegTime = CalendarUtil.getOnlyTime(overlapLeg.getEstDepartureTimeZulu());

								Date newStartDate = CalendarUtil.getOfssetAndTimeAddedDate(
										CalendarUtil.getStartTimeOfDate(overlappingSche.getStartDate()), firstLegTime, dayOffDep);
								Date newStopDate = CalendarUtil.getOfssetAndTimeAddedDate(
										CalendarUtil.getStartTimeOfDate(overlappingSche.getStopDate()), firstLegTime, dayOffDep);

								overlappingSche.setStartDate(newStartDate);
								overlappingSche.setStopDate(newStopDate);

								Frequency frq = overlappingSche.getFrequency();
								frq = FrequencyUtil.shiftFrequencyBy(frq, dayOffDep);
								overlappingSche.setFrequency(frq);

								// reset the dep/arr day offsets
								overlapLeg.setEstDepartureDayOffset(0);
								overlapLeg.setEstArrivalDayOffset(overlapLeg.getEstArrivalDayOffset() + dayOffArr - dayOffDep);

							} else {
								overlapLeg.setEstArrivalDayOffset(overlapLeg.getEstArrivalDayOffset() + dayOffArr);
								overlapLeg.setEstDepartureDayOffset(overlapLeg.getEstDepartureDayOffset() + dayOffDep);
							}
						}
					}
				}
			}
		}

		// set schedule status to ERR
		if (overlapLegTimeChanged) {

			overlappingSche.setStatusCode(ScheduleStatusEnum.ERROR.getCode());

		}

		return overlappingSche;
	}

	/**
	 * private method to update overlapping flight with the overlapping leg timings of the updated flight
	 * 
	 * @param overlapping
	 * @param updated
	 * @param existing
	 * @return
	 */
	public static Flight updateOverlappingFromUpdated(Flight overlappingFlt, Flight updatedFlt, Flight existingFlt)
			throws ModuleException {

		boolean overlapLegTimeChanged = false;

		// get overlap leg time change (update and existing)
		LinkedHashMap<String, OverlapLegDeltaDTO> legDeltaMap = OverlapLegShiftUtil.getTimeDeltaForOverlappingSegments(updatedFlt, existingFlt);

		if (legDeltaMap != null && legDeltaMap.size() > 0) {
			// find the overlapping leg(s) of overlapping flight
			Set<FlightLeg> updatedLegs = updatedFlt.getFlightLegs();
			Iterator<FlightLeg> itUpdatedLegs = updatedLegs.iterator();

			while (itUpdatedLegs.hasNext()) {

				FlightLeg updatedLeg = (FlightLeg) itUpdatedLegs.next();
				Set<FlightLeg> overlapingLegs = overlappingFlt.getFlightLegs();
				Iterator<FlightLeg> itOverlapLegs = overlapingLegs.iterator();

				while (itOverlapLegs.hasNext()) {

					FlightLeg overlapLeg = (FlightLeg) itOverlapLegs.next();
					if (updatedLeg.getOrigin().equals(overlapLeg.getOrigin())
							&& updatedLeg.getDestination().equals(overlapLeg.getDestination())) {

						String mapKey = updatedLeg.getOrigin() + "/" + updatedLeg.getDestination();

						OverlapLegDeltaDTO legDelta = (OverlapLegDeltaDTO) legDeltaMap.get(mapKey);
						if (legDelta != null) {

							overlapLegTimeChanged = true;

							// calculate dep/arr time deltas
							Date adjustedDepTime = CalendarUtil.getOfssetInMilisecondAddedDate(
									overlapLeg.getEstDepartureTimeZulu(), (int) legDelta.getDepartureDeltaInMillis());
							int dayOffDep = CalendarUtil.daysUntil(overlapLeg.getEstDepartureTimeZulu(), adjustedDepTime);

							Date adjustedArrTime = CalendarUtil.getOfssetInMilisecondAddedDate(
									overlapLeg.getEstArrivalTimeZulu(), (int) legDelta.getArrivalDeltaInMillis());
							int dayOffArr = CalendarUtil.daysUntil(overlapLeg.getEstArrivalTimeZulu(), adjustedArrTime);

							// adjust dep/arr times
							overlapLeg.setEstDepartureTimeZulu(CalendarUtil.getOnlyTime(adjustedDepTime));
							overlapLeg.setEstArrivalTimeZulu(CalendarUtil.getOnlyTime(adjustedArrTime));

							// adjust flight dep date if first leg dep has a day offset
							if (overlapLeg.getLegNumber() == 1) {

								Date firstLegTime = CalendarUtil.getOnlyTime(overlapLeg.getEstDepartureTimeZulu());
								Date newDepartureDate = CalendarUtil.getOfssetAndTimeAddedDate(
										CalendarUtil.getStartTimeOfDate(overlappingFlt.getDepartureDate()), firstLegTime,
										dayOffDep);
								overlappingFlt.setDepartureDate(newDepartureDate);

								overlapLeg.setEstDepartureDayOffset(0);
								overlapLeg.setEstArrivalDayOffset(overlapLeg.getEstArrivalDayOffset() + dayOffArr - dayOffDep);
							} else {
								overlapLeg.setEstArrivalDayOffset(overlapLeg.getEstArrivalDayOffset() + dayOffArr);
								overlapLeg.setEstDepartureDayOffset(overlapLeg.getEstDepartureDayOffset() + dayOffDep);
							}
						}
					}
				}
			}
		}

		if (overlapLegTimeChanged) {

			// set flight status to CLOSED
			overlappingFlt.setStatus(FlightStatusEnum.CLOSED.getCode());
		}

		return overlappingFlt;
	}

	/**
	 * private method to update the ovelapping segment (overlapping flight) timings to match with updated schedule
	 * overlapping segment timings.
	 * 
	 * @param overlapping
	 * @param updated
	 * @param existing
	 * @return
	 */

	public static Flight updateOverlappingFromUpdated(Flight overlappingFlt, FlightLeg overlapFlightLeg,
			FlightSchedule updatedSche, Flight existingFlt, FlightSchedule existingSche) throws ModuleException {

		// get overlap leg time change (update and existing)
		LinkedHashMap<String, OverlapLegDeltaDTO> legDeltaMap = null;
		if (existingFlt != null)
			legDeltaMap = OverlapLegShiftUtil.getTimeDeltaForOverlappingSegments(updatedSche, existingFlt);
		else
			legDeltaMap = OverlapLegShiftUtil.getTimeDeltaForOverlappingSegments(updatedSche, existingSche);

		boolean overlapLegTimeChanged = false;

		if (legDeltaMap != null && legDeltaMap.size() > 0) {

			String mapKey = overlapFlightLeg.getOrigin() + "/" + overlapFlightLeg.getDestination();

			OverlapLegDeltaDTO legDelta = (OverlapLegDeltaDTO) legDeltaMap.get(mapKey);

			if (legDelta != null) {

				overlapLegTimeChanged = true;

				// calculate dep/arr time deltas
				Date adjustedDepTime = CalendarUtil.getOfssetInMilisecondAddedDate(overlapFlightLeg.getEstDepartureTimeZulu(),
						(int) legDelta.getDepartureDeltaInMillis());
				int dayOffDep = CalendarUtil.daysUntil(overlapFlightLeg.getEstDepartureTimeZulu(), adjustedDepTime);

				Date adjustedArrTime = CalendarUtil.getOfssetInMilisecondAddedDate(overlapFlightLeg.getEstArrivalTimeZulu(),
						(int) legDelta.getArrivalDeltaInMillis());
				int dayOffArr = CalendarUtil.daysUntil(overlapFlightLeg.getEstArrivalTimeZulu(), adjustedArrTime);

				// adjust dep/arr times
				overlapFlightLeg.setEstDepartureTimeZulu(CalendarUtil.getOnlyTime(adjustedDepTime));
				overlapFlightLeg.setEstArrivalTimeZulu(CalendarUtil.getOnlyTime(adjustedArrTime));

				// adjust flight dep date if first leg dep has a day offset
				if (overlapFlightLeg.getLegNumber() == 1) {

					Date firstLegTime = CalendarUtil.getOnlyTime(overlapFlightLeg.getEstDepartureTimeZulu());
					Date newDepartureDate = CalendarUtil.getOfssetAndTimeAddedDate(
							CalendarUtil.getStartTimeOfDate(overlappingFlt.getDepartureDate()), firstLegTime, dayOffDep);
					overlappingFlt.setDepartureDate(newDepartureDate);

					overlapFlightLeg.setEstDepartureDayOffset(0);
					overlapFlightLeg.setEstArrivalDayOffset(overlapFlightLeg.getEstArrivalDayOffset() + dayOffArr - dayOffDep);
				} else {
					overlapFlightLeg.setEstArrivalDayOffset(overlapFlightLeg.getEstArrivalDayOffset() + dayOffArr);
					overlapFlightLeg.setEstDepartureDayOffset(overlapFlightLeg.getEstDepartureDayOffset() + dayOffDep);
				}
			}
		}

		if (overlapLegTimeChanged) {

			// set flight status to CLOSED
			overlappingFlt.setStatus(FlightStatusEnum.CLOSED.getCode());
		}

		return overlappingFlt;
	}
}
