/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import java.util.HashMap;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="integrateCancelSchedule"
 */
public class IntegrateCancelSchedule extends DefaultBaseCommand {

	// BDs
	private GDSPublishingBD gdsPublishingBD;

	/**
	 * constructor of the ValidateCreateFlights command
	 */
	public IntegrateCancelSchedule() {

		// looking up BDs
		gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
	}

	/**
	 * execute method of the ValidateCreateFlights command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
			if (schedule != null && isScheduleHasValidOperationDays(schedule)) {
				FlightSchedule overlappingSchedule = (FlightSchedule) this.getParameter(CommandParamNames.OVERLAPPING_SCHEDULE);
				HashMap buildFlightIds = (HashMap) this.getParameter(CommandParamNames.BUILDED_SCHEDULE_FLIGHT_IDS);

				Boolean force = (Boolean) this.getParameter(GDSSchedConstants.ParamNames.FORCE);

				String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
						.getServiceTypeForScheduleFlight();

				Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_SSM_CANCEL);

				env.addParam(GDSSchedConstants.ParamNames.SCHEDULE, schedule);
				env.addParam(GDSSchedConstants.ParamNames.OVERLAPPING_SCHEDULE, overlappingSchedule);
				env.addParam(GDSSchedConstants.ParamNames.SERVICE_TYPE, serviceType);
				env.addParam(GDSSchedConstants.ParamNames.FLIGHT_IDS_MAP_FOR_SCHEDULES, buildFlightIds);

				gdsPublishingBD.publishMessage(env);
			}

		}
		// return last command response
		return lastResponse;
	}

	/**
	 * private method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */

	private boolean isScheduleHasValidOperationDays(FlightSchedule schedule) throws ModuleException {
		// validate schedule period & day of operation
		Frequency validOperationDays = CalendarUtil.getValidFrequencyMatchesDateRange(schedule.getStartDate(),
				schedule.getStopDate(), schedule.getFrequency());

		if (validOperationDays.getDayCount(true) > 0) {
			return true;
		}
		return false;
	}

}
