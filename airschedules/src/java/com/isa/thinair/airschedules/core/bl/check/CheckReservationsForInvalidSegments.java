package com.isa.thinair.airschedules.core.bl.check;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentResInvSummaryDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlySegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.bl.noncmd.ReservationsForFlights;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to check reservations for invalid segments of schedules and flights
 * 
 * @author Chandana Kithalagama
 * @isa.module.command name="checkReservationsForInvalidSegments"
 */
public class CheckReservationsForInvalidSegments extends DefaultBaseCommand {

	private static final int CHECK_FOR_FLIGHT = 0;
	private static final int CHECK_FOR_PERIODS = 1;

	// DAO's
	private final FlightDAO flightDAO;
	private final FlightSegementDAO flightSegmentDAO;

	// BD's
	private final FlightInventoryBD flightInventryBD;
	private final FlightInventoryResBD flightInventryResBD;
	private final SegmentBD segmentBD;

	// Helpers
	private final ReservationsForFlights reservation4Flights;

	/**
	 * constructor of the CheckReservationsForInvalidSegments command
	 */
	public CheckReservationsForInvalidSegments() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSegmentDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);

		// looking up bd's
		flightInventryResBD = AirSchedulesUtil.getFlightInventoryResBD();
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
		segmentBD = AirSchedulesUtil.getSegmentBD();

		// helpers
		reservation4Flights = new ReservationsForFlights(flightDAO, flightSegmentDAO, flightInventryBD, segmentBD);
	}

	/**
	 * execute method of the CheckReservationsForInvalidSegments command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		// ------- schedule params ----------------
		Collection<AffectedPeriod> updatePeriods = (Collection<AffectedPeriod>) this
				.getParameter(CommandParamNames.UPDATE_PERIOD_LIST);
		FlightSchedule updated = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		FlightSchedule existing = (FlightSchedule) this.getParameter(CommandParamNames.LOADED_EXISTING_SCHEDULE);
		Boolean forceCancel = (Boolean) this.getParameter(CommandParamNames.FORCE);
		// -------- flight params ------------------
		Flight updatedFlight = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight existingFlight = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);
		FlightAllowActionDTO allowedActions = (FlightAllowActionDTO) this.getParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO);

		// checking params
		int type = this.checkParams(updatePeriods, updatedFlight);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		List<Integer> fltSegIDsList = new ArrayList<Integer>();
		Collection<Integer> flightsHasRes = new ArrayList<Integer>();
		Collection<Integer> flightSegmentIdsHasRes = new ArrayList<Integer>();

		// if check reservations for periods for schedule
		if (type == CheckReservationsForInvalidSegments.CHECK_FOR_PERIODS && !forceCancel.booleanValue()) {

			if (!existing.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())) {

				// check segment valid invalid segments
				List<FlySegement> invScheduleSegIDs = SegmentUtil.getInvalidatedSegments(existing.getFlightScheduleSegments(),
						updated.getFlightScheduleSegments());
				for (FlySegement invalidSegment : invScheduleSegIDs) {
					List<Integer> invFlightSegIDs = flightDAO.getFlightSegmentIDs(updated.getScheduleId().intValue(),
							invalidSegment.getSegmentCode());
					if (invFlightSegIDs != null) {
						fltSegIDsList.addAll(invFlightSegIDs);
					}
				}

				// [NOTE] overlapping schedule is not affected by invalidating a segment in existing schedule

				// check reservations for the invalidated segments of the updated flight(s)
				if (fltSegIDsList.size() > 0) {

					flightSegmentIdsHasRes = flightInventryResBD.filterSegmentsHavingReservations(fltSegIDsList);
				}

				if (flightSegmentIdsHasRes.size() > 0) {

					flightsHasRes = flightSegmentDAO.getFlightIds(flightSegmentIdsHasRes);
					HashMap<FlightSummaryDTO, Collection<FlightSegmentResInvSummaryDTO>> flightDetailsWithResMap = reservation4Flights
							.getFlightDetailsWithResMap(flightsHasRes, flightSegmentIdsHasRes);

					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE);
					responce.addResponceParam(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE, flightDetailsWithResMap);
					return responce;

				} else {

					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.RESERVATIONS_NOT_FOUND_FOR_SCHEDULE);
					return responce;
				}
			}
		} else if (type == CheckReservationsForInvalidSegments.CHECK_FOR_FLIGHT && !allowedActions.isAllowSegmentV2I()) {

			if (!existingFlight.getStatus().equals(FlightStatusEnum.CREATED.getCode())) {

				// check segment valid invalid segments
				List<FlySegement> invScheduleSegIDs = SegmentUtil.getInvalidatedSegments(existingFlight.getFlightSegements(),
						updatedFlight.getFlightSegements());
				for (FlySegement invalidSegment : invScheduleSegIDs) {
					List<Integer> invFlightSegIDs = flightDAO.getFlightSegmentIDs(updated.getScheduleId().intValue(),
							invalidSegment.getSegmentCode());
					if (invFlightSegIDs != null) {
						fltSegIDsList.addAll(invFlightSegIDs);
					}
				}

				// [NOTE] overlapping flight is not affected by invalidating a segment in existing flight

				// check reservations for the invalidated segments of the updated flight(s)
				if (fltSegIDsList.size() > 0) {

					flightSegmentIdsHasRes = flightInventryResBD.filterSegmentsHavingReservations(fltSegIDsList);

					if (flightSegmentIdsHasRes.size() > 0) {

						flightsHasRes.add(existingFlight.getFlightId());
						HashMap<FlightSummaryDTO, Collection<FlightSegmentResInvSummaryDTO>> flightDetailsWithResMap = reservation4Flights
								.getFlightDetailsWithResMap(flightsHasRes, flightSegmentIdsHasRes);

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_FLIGHT);
						responce.addResponceParam(ResponceCodes.RESERVATIONS_FOUND_FOR_FLIGHT, flightDetailsWithResMap);
						return responce;

					} else {

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.RESERVATIONS_NOT_FOUND_FOR_FLIGHT);
						return responce;
					}
				}
			}
		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private int checkParams(Collection<AffectedPeriod> updatePeriods, Flight updated) throws ModuleException {

		int validationType = -1;

		if (updated != null) {

			validationType = CheckReservationsForInvalidSegments.CHECK_FOR_FLIGHT;

		} else if (updatePeriods != null) {

			validationType = CheckReservationsForInvalidSegments.CHECK_FOR_PERIODS;

		} else {
			// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
		}

		return validationType;
	}
}
