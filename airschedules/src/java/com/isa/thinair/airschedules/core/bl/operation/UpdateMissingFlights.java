/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ScheduleRevisionTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * This command is for SSM REV actions, when given period has multiple schedule splits or ADHOC flights intended REV
 * action operation more complex therefore to keep the final result consistent following manual flight
 * cancellation/creation needed.
 * 
 * command for cancel & create missing flights within a given date period.
 * 
 * 
 * @author M.Rikaz
 * @isa.module.command name="updateMissingFlights"
 */
public class UpdateMissingFlights extends DefaultBaseCommand {

	// Dao's
	private final FlightScheduleDAO flightSchedleDAO;

	private final Log log = LogFactory.getLog(getClass());

	private boolean isLocalTimeMode = false;

	/**
	 * constructor of the BuildSchedule command
	 */
	public UpdateMissingFlights() {

		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

	}

	/**
	 * execute method of the UpdateMissingFlights command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		ScheduleRevisionTO scheduleRevisionTO = (ScheduleRevisionTO) this.getParameter(CommandParamNames.SCHEDULE_REVISION_DTO);
		FlightAlertDTO flightAlertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);
		FlightAllowActionDTO flightAllowActionDTO = (FlightAllowActionDTO) this
				.getParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO);

		Collection<Date> createdFlightDates = null;
		Collection<Date> canceledFlightDates = null;

		String flightNumber = scheduleRevisionTO.getFlightNumber();

		Date existingStartDate = scheduleRevisionTO.getStartDate();
		Date existingEndDate = scheduleRevisionTO.getEndDate();
		Frequency existingFrequency = scheduleRevisionTO.getFrequency();

		Date newStartDate = scheduleRevisionTO.getUpdatedStartDate();
		Date newEndDate = scheduleRevisionTO.getUpdatedEndDate();
		Frequency newFrequency = scheduleRevisionTO.getUpdatedFrequency();
		this.isLocalTimeMode = scheduleRevisionTO.isLocalTimeMode();
		boolean isEnableAttachDefaultAnciTemplate = scheduleRevisionTO.isEnableAttachDefaultAnciTemplate();

		Collection<Flight> flightColl = getAvailableNonCancelledFlights(flightNumber, existingStartDate, existingEndDate);
		Flight flight = null;
		if (flightColl == null || flightColl.isEmpty()) {

			ScheduleSearchDTO scheduleSearchDTO = new ScheduleSearchDTO();
			scheduleSearchDTO.setStartDate(existingStartDate);
			scheduleSearchDTO.setEndDate(existingEndDate);
			scheduleSearchDTO.setFlightNumber(flightNumber);
			scheduleSearchDTO.setLocalTime(isLocalTimeMode);

			List<String> statusFilters = new ArrayList<String>();
			statusFilters.add(FlightStatusEnum.CANCELLED.toString());
			scheduleSearchDTO.setStatusFilter(statusFilters);

			flightColl = flightSchedleDAO.getAvailableAdhocFlights(scheduleSearchDTO);
		}

		if (flightColl != null && !flightColl.isEmpty()) {
			Iterator<Flight> flightCollItr = flightColl.iterator();

			while (flightCollItr.hasNext()) {
				flight = flightCollItr.next();

				if (flight != null)
					break;
			}

			AffectedPeriod period = new AffectedPeriod(newStartDate, newEndDate, newFrequency);
			Collection<Date> validNewFlightDates = ScheduledFlightUtil.getFlightDatesForSchedule(period);

			createdFlightDates = createMissingFlights(validNewFlightDates, flight.getFlightNumber(), flight.getFlightId(),
					isEnableAttachDefaultAnciTemplate, flightAllowActionDTO);

			Collection<Date> missedCnxFlightDates = getCancelledFlightDatesForGivenPeriod(existingStartDate, existingEndDate,
					existingFrequency, newStartDate, newEndDate, newFrequency);

			canceledFlightDates = cancelMissingFlights(missedCnxFlightDates, flightNumber, flightAlertDTO);

		}

		// return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (responce.isSuccess()) {
			responce.addResponceParam(CommandParamNames.CREATED_FLIGHT_DATES, createdFlightDates);
			responce.addResponceParam(CommandParamNames.CANCELED_FLIGHT_DATES, canceledFlightDates);
			responce.addResponceParam(CommandParamNames.FLIGHT, flight);

		}
		return responce;

	}

	private Collection<Date> getCancelledFlightDatesForGivenPeriod(Date existingStartDate, Date existingEndDate,
			Frequency existingFrequency, Date newStartDate, Date newEndDate, Frequency newFrequency) throws ModuleException {

		Collection<Date> missedCnxFlightDates = new ArrayList<>();

		Frequency cnxOperationDays = FrequencyUtil.getRestOfTheFrequency(existingFrequency, newFrequency);

		if (cnxOperationDays.getDayCount(true) > 0) {
			AffectedPeriod canceledPeriod = new AffectedPeriod(existingStartDate, existingEndDate, cnxOperationDays);

			Collection<Date> validCnxFlightDates = ScheduledFlightUtil.getFlightDatesForSchedule(canceledPeriod);
			if (validCnxFlightDates != null && !validCnxFlightDates.isEmpty()) {
				missedCnxFlightDates.addAll(validCnxFlightDates);
			}
		}

		if (existingFrequency.getDayCount(true) > 0) {
			// schedule original start date trimmed, all flight within that period needs to be cancelled
			if (!CalendarUtil.isSameDay(newStartDate, existingStartDate)
					&& CalendarUtil.isGreaterThan(newStartDate, existingStartDate)) {

				AffectedPeriod scheduleStartDateTrimedPeriod = new AffectedPeriod(existingStartDate,
						CalendarUtil.addDateVarience(newStartDate, -1), existingFrequency);
				Collection<Date> cnxFlightDatesPart1 = ScheduledFlightUtil
						.getFlightDatesForSchedule(scheduleStartDateTrimedPeriod);
				if (cnxFlightDatesPart1 != null && !cnxFlightDatesPart1.isEmpty()) {
					missedCnxFlightDates.addAll(cnxFlightDatesPart1);
				}

			}

			// schedule original end date trimmed, all flight within that period needs to be cancelled
			if (!CalendarUtil.isSameDay(newEndDate, existingEndDate) && CalendarUtil.isGreaterThan(existingEndDate, newEndDate)) {
				AffectedPeriod scheduleEndDateTrimedPeriod = new AffectedPeriod(CalendarUtil.addDateVarience(newEndDate, 1),
						existingEndDate, existingFrequency);
				Collection<Date> cnxFlightDatesPart2 = ScheduledFlightUtil.getFlightDatesForSchedule(scheduleEndDateTrimedPeriod);
				if (cnxFlightDatesPart2 != null && !cnxFlightDatesPart2.isEmpty()) {
					missedCnxFlightDates.addAll(cnxFlightDatesPart2);
				}
			}
		}

		return missedCnxFlightDates;
	}

	private Collection<Flight> getAvailableNonCancelledFlights(String flightNumber, Date startDate, Date endDate)
			throws ModuleException {

		ScheduleSearchDTO flightSearchDTO = new ScheduleSearchDTO();
		flightSearchDTO.setStartDate(CalendarUtil.getStartTimeOfDate(startDate));
		flightSearchDTO.setFlightNumber(flightNumber);
		flightSearchDTO.setEndDate(CalendarUtil.getEndTimeOfDate(endDate));
		flightSearchDTO.setLocalTime(this.isLocalTimeMode);

		List<String> flightStatusFilters = new ArrayList<String>();
		flightStatusFilters.add(FlightStatusEnum.CREATED.toString());
		flightStatusFilters.add(FlightStatusEnum.ACTIVE.toString());
		flightStatusFilters.add(FlightStatusEnum.CLOSED.toString());
		flightStatusFilters.add(FlightStatusEnum.CLOSED_FOR_RESERVATION.toString());
		flightSearchDTO.setStatusFilter(flightStatusFilters);

		return flightSchedleDAO.getAvailableAdhocFlights(flightSearchDTO);
	}

	private Collection<Date> cancelMissingFlights(Collection<Date> missedCnxFlightDates, String flightNumber,
			FlightAlertDTO flightAlertDTO) throws ModuleException {
		Collection<Date> canceledFlightDates = new ArrayList<>();
		if (missedCnxFlightDates != null && !missedCnxFlightDates.isEmpty()) {
			for (Date departureDate : missedCnxFlightDates) {
				Collection<Flight> availableFlights = getAvailableNonCancelledFlights(flightNumber, departureDate, departureDate);

				if (availableFlights != null && !availableFlights.isEmpty()) {
					for (Flight cancelFlight : availableFlights) {
						try {
							log.debug("FLIGHT STILL EXIST ON - " + CalendarUtil.formatDate(departureDate, CalendarUtil.PATTERN1));
							AirSchedulesUtil.getFlightBD().cancelFlight(cancelFlight.getFlightId(), true, flightAlertDTO);
							canceledFlightDates.add(departureDate);
						} catch (Exception e) {
							log.error("ERROR @ UPDATE_MISSING_FLIGHT CANCELLATION - "
									+ CalendarUtil.formatDate(departureDate, CalendarUtil.PATTERN1));
						}
					}

				} else {
					log.debug("FLIGHT ALL READY CANCELLED/NOT EXIST ON - "
							+ CalendarUtil.formatDate(departureDate, CalendarUtil.PATTERN1));
				}
			}
		}
		return canceledFlightDates;
	}

	private Collection<Date> createMissingFlights(Collection<Date> validNewFlightDates, String flightNumber, Integer flightId,
			boolean isEnableAttachDefaultAnciTemplate, FlightAllowActionDTO flightAllowActionDTO) throws ModuleException {
		Collection<Date> createdFlightDates = new ArrayList<>();
		if (validNewFlightDates != null && !validNewFlightDates.isEmpty()) {
			for (Date departureDate : validNewFlightDates) {

				Collection<Flight> availableFlights = getAvailableNonCancelledFlights(flightNumber, departureDate, departureDate);

				if (availableFlights == null || availableFlights.isEmpty()) {
					try {
						log.debug("FLIGHT NOT EXIST ON - " + CalendarUtil.formatDate(departureDate, CalendarUtil.PATTERN1));

						ServiceResponce serviceResponce = AirSchedulesUtil.getFlightBD().prepareCopyFlight(flightId,
								departureDate, this.isLocalTimeMode);

						if (serviceResponce != null && serviceResponce.isSuccess()) {

							Flight copyedFlight = (Flight) serviceResponce.getResponseParam(CommandParamNames.FLIGHT);

							if (copyedFlight != null) {
								AirSchedulesUtil.getFlightBD().createFlight(flightAllowActionDTO, copyedFlight,
										isEnableAttachDefaultAnciTemplate);
								createdFlightDates.add(departureDate);
							}

						}

					} catch (Exception e) {
						log.error("ERROR @ UPDATE_MISSING_FLIGHT CREATION - "
								+ CalendarUtil.formatDate(departureDate, CalendarUtil.PATTERN1));
					}
				} else {
					log.debug("FLIGHT ALL READY EXIST ON - " + CalendarUtil.formatDate(departureDate, CalendarUtil.PATTERN1));
				}

			}
		}
		return createdFlightDates;
	}
}
