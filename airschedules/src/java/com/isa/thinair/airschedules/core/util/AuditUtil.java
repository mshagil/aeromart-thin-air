package com.isa.thinair.airschedules.core.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.ExternalConstants.ScheduleTimeFormat;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.bl.audit.AuditParams;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class AuditUtil {

	/**
	 * public method to format the frequency object (format: MO TU FR SA)
	 * 
	 * @param frequency
	 * @return
	 */
	public static String getFrequencyAuditInfo(Frequency frequency) {

		StringBuffer frequencyDetails = new StringBuffer();

		if (frequency.getDay0())
			frequencyDetails.append(CalendarUtil.getDayFromDayNumber(0).toString().substring(0, 2) + " ");
		if (frequency.getDay1())
			frequencyDetails.append(CalendarUtil.getDayFromDayNumber(1).toString().substring(0, 2) + " ");
		if (frequency.getDay2())
			frequencyDetails.append(CalendarUtil.getDayFromDayNumber(2).toString().substring(0, 2) + " ");
		if (frequency.getDay3())
			frequencyDetails.append(CalendarUtil.getDayFromDayNumber(3).toString().substring(0, 2) + " ");
		if (frequency.getDay4())
			frequencyDetails.append(CalendarUtil.getDayFromDayNumber(4).toString().substring(0, 2) + " ");
		if (frequency.getDay5())
			frequencyDetails.append(CalendarUtil.getDayFromDayNumber(5).toString().substring(0, 2) + " ");
		if (frequency.getDay6())
			frequencyDetails.append(CalendarUtil.getDayFromDayNumber(6).toString().substring(0, 2) + " ");

		return frequencyDetails.toString();
	}

	/**
	 * public method to add flight details to audit content hash map
	 * 
	 * @param flight
	 * @return
	 */
	public static LinkedHashMap<String, Object> addFlightInfo(Flight flight) {

		LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		contents.put(AuditParams.FLIGHT_ID, flight.getFlightId());
		contents.put(AuditParams.FLIGHT_NO, flight.getFlightNumber());
		contents.put(AuditParams.ORIGIN_AIRPORT, flight.getOriginAptCode());
		contents.put(AuditParams.DESTINATION_AIRPORT, flight.getDestinationAptCode());
		contents.put(AuditParams.DEPARTURE_DATE, dateFormat.format(flight.getDepartureDate()));
		Calendar flightClosureTimeZulu = new GregorianCalendar();
		flightClosureTimeZulu.setTime(flight.getDepartureDate());
		flightClosureTimeZulu.add(Calendar.MINUTE, -AppSysParamsUtil.getFlightClosureGapInMins());
		contents.put(AuditParams.STOP_BOOKING_TIME, flightClosureTimeZulu.getTime());
		contents.put(AuditParams.OP_TYPE, new Integer(flight.getOperationTypeId()));
		contents.put(AuditParams.AIRCRAFT_MODEL_NO, flight.getModelNumber());
		contents.put(AuditParams.FLIGHT_TYPE, flight.getFlightType());
		if (flight.getOldModelNumber() != null && !flight.getOldModelNumber().equals("")
				&& !flight.getOldModelNumber().equals(flight.getFlightNumber())) {
			contents.put("Old aircraft model", flight.getOldModelNumber());
		}
		if (flight.getSeatChargeTemplateId() != null && flight.getSeatChargeTemplateId() > 0) {
			contents.put("Seat template", "Attached");
		} else {
			contents.put("Seat template", "Not Attached");
		}
	
		contents.put(AuditParams.LEG_DETAILS, LegUtil.getLegInfo(flight.getFlightLegs(), ScheduleTimeFormat.Zulu));
		contents.put(AuditParams.SEGMENT_DETAILS, SegmentUtil.getSegmentAuditInfo(flight.getFlightSegements()));
		if (!("").equals(flight.getUserNotes())) {
			contents.put(AuditParams.USER_NOTES, flight.getUserNotes());
		}
		if (flight.getScheduleId() != null)
			contents.put(AuditParams.SCHEDULE_ID, flight.getScheduleId());
		if (flight.getOverlapingFlightId() != null)
			contents.put(AuditParams.OVERLAPPING_FLIGHT_ID, flight.getOverlapingFlightId());
		
		if(!("").equals(flight.getStatus())){
			contents.put(AuditParams.STATUS, flight.getStatus());
		}
		return contents;
	}

	/**
	 * public method to get schedule details add to audit content hash map
	 * 
	 * @param schedule
	 * @return
	 */
	public static LinkedHashMap<String, Object> addScheduleInfo(FlightSchedule schedule) {

		LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		contents.put(AuditParams.SCHEDULE_ID, schedule.getScheduleId());
		contents.put(AuditParams.FLIGHT_NO, schedule.getFlightNumber());
		contents.put(AuditParams.ORIGIN_AIRPORT, schedule.getDepartureStnCode());
		contents.put(AuditParams.DESTINATION_AIRPORT, schedule.getArrivalStnCode());
		contents.put(AuditParams.SCHEDULE_START_DATE, dateFormat.format(schedule.getStartDate()));
		contents.put(AuditParams.SCHEDULE_STOP_DATE, dateFormat.format(schedule.getStopDate()));
		contents.put(AuditParams.OP_TYPE, new Integer(schedule.getOperationTypeId()));
		contents.put(AuditParams.AIRCRAFT_MODEL_NO, schedule.getModelNumber());
		contents.put(AuditParams.FREQUENCY, AuditUtil.getFrequencyAuditInfo(schedule.getFrequency()));
		contents.put(AuditParams.LEG_DETAILS, LegUtil.getLegInfo(schedule.getFlightScheduleLegs(), ScheduleTimeFormat.Zulu));
		contents.put(AuditParams.SEGMENT_DETAILS, SegmentUtil.getSegmentAuditInfo(schedule.getFlightScheduleSegments()));

		if (schedule.getOverlapingScheduleId() != null)
			contents.put(AuditParams.OVERLAPPING_SCHEDULE_ID, schedule.getOverlapingScheduleId());

		return contents;
	}
	
	public static String getScheduleIdsString(Collection<Integer> scheduleIds) {

		StringBuilder schedIds = new StringBuilder();
		if (scheduleIds != null) {
			Iterator<Integer> it = scheduleIds.iterator();
			while (it.hasNext()) {
				schedIds.append(((Integer) it.next()).toString() + ", ");
			}

			if (schedIds.toString().lastIndexOf(',') > 0) {
				schedIds = new StringBuilder(schedIds.substring(0, schedIds.toString().lastIndexOf(',')));
			}
		}
		return schedIds.toString();
	}
}
