/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:19:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.model.FlightSegNotificationEvent;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;

import java.sql.Blob;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * BHive Commons: FlightSegement module
 * 
 * @author Lasantha Pambagoda FlightSegementDAO is business delegate interface for the flightsegement service apis
 * 
 */

public interface FlightSegementDAO {

	public List<FlightSegement> getFlightSegements();

	public FlightSegement getFlightSegement(int FltSegId);

	public FlightSegement getFlightSegement(int flightId, String segmentCode);

	public void saveFlightSegement(FlightSegement flightsegement);

	public void removeFlightSegement(int FltSegId);

	public Collection<FlightSegement> getFlightSegments(int flightId);

	public Collection<FlightSegement> getDepartureFlightForStation(String stationCode, Date gmtDstEffectiveFrom,
			Date gmtDstEffectiveTo);

	public Collection<FlightSegement> getArrivalFlightForStation(String stationCode, Date gmtDstEffectiveFrom,
			Date gmtDstEffectiveTo);

	public void updateDepartureFlightForStation(String stationCode, Date gmtDstEffectiveFrom, Date gmtDstEffectiveTo);

	public void updateArrivalFlightForStation(String stationCode, Date gmtDstEffectiveFrom, Date gmtDstEffectiveTo);

	public Collection<FlightSegmentDTO> getFlightSegments(Collection<Integer> flightSegmentIds);

	public Collection<Integer> getFlightIds(Collection<Integer> flightSegmentIds);

	public Collection<FlightSegement> getFlightSegmentsForLocalDate(String originAirportCode, String destAirportCode,
			String flightNo, Date departureDateLocal, boolean dateOnly);

	public Collection<FlightSegmentDTO> getFilledFlightSegmentDTOs(Collection<FlightSegmentDTO> flightSegmentDTOs)
			throws ModuleException;

	public Collection<Integer> getFlightSegIdsforSchedule(int scheduleId) throws ModuleException;

	public Collection<FlightSeatsDTO> getFlightSegIdsforFlight(Collection<Integer> colFlightId) throws ModuleException;

	public Collection<FlightMealDTO> getFlightSegIdsforFlightforMeal(Collection<Integer> colFlightId) throws ModuleException;

	public Integer saveFlightSegNotificationEventStatus(Integer flightSegmentId, Integer flightMsgEventId, String newStatus,
			String failureReason, Integer notificationCount, String notificationType);

	public FlightSegNotificationEvent saveFlightSegNotificationEventStatus(Integer flightSegmentId, Integer flightMsgEventId,
			String newStatus, String failureReason, Integer notificationCount, String notificationType, String insFirstResponse,
			Blob insSecResponse);

	public Integer getFlightSegNotificationAttempts(Integer flightMsgEventId);

	public Collection<FlightBaggageDTO> getFlightSegIdsforFlightforBaggage(Collection<Integer> colFlightId)
			throws ModuleException;

	public Collection<FlightSegmentDTO> getFlightSegments(Collection<Integer> flightIds, String segmentCode);

	public Collection<String> getBusCarrierCode();

	public Map<String, String> getBusAirCarrierCodes();

	public FlightSegement getSpecificFlightSegment(int pnrSegId);

	public Collection<FlightSegement> getFlightSegmentList(String segmentCode, Date departureDate);

	public List<FlightSegement>
			getSuggestedSegmentListForADate(String flightNumber, String departureDateLocal, String segmentCode);

	public int getFlightSegmentCount(String flightNumber, Date departureDateLocal, Date arrivalDateLocal, String segmentCode);

	public boolean areSameDaySameFlights(int flightSegID1, int flightSegID2);

	public boolean isSeatMapAvailableForFlightSegement(Integer flightSegId) throws ModuleException;

	public Map<String, List<FlightSegmentTO>> searchFlightsWildcardBased(WildcardFlightSearchDto flightSearchDto)
			throws ModuleException;

	public Map<String, List<FlightSegmentTO>> searchCodeShareFlightsWildcardBased(WildcardFlightSearchDto flightSearchDto)
			throws ModuleException;

	public Map<Integer, Boolean> isAtLeastOneSegmentHavingCSOCFlight(Collection<Integer> colFlightSegIds) throws ModuleException;

	public Collection<Integer> getFlightSegIdsforFlight(int flightId) throws ModuleException;

	public Collection<Integer> getPaxFareSegETIds(String flightId) throws ParseException;
}
