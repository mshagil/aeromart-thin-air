/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.warning;

import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.bl.noncmd.DurationCalculator;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to warn flight leg duration changes
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="warnFlightLegDuration"
 */
public class WarnFlightLegDuration extends DefaultBaseCommand {

	// BD's
	private CommonMasterBD commonMasterBD;

	// helpers
	DurationCalculator durationCalculator;

	/**
	 * constructor of the WarnFlightLegDuration command
	 */
	public WarnFlightLegDuration() {

		// looking up daos
		commonMasterBD = AirSchedulesUtil.getCommonMasterBD();

		// helpers
		durationCalculator = new DurationCalculator();
	}

	/**
	 * execute method of the WarnFlightLegDuration command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);

		FlightAllowActionDTO allowDTO = (FlightAllowActionDTO) this.getParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO);

		DefaultServiceResponse warning = (DefaultServiceResponse) this.getParameter(CommandParamNames.WARNING);

		// checking params
		this.checkParams(flight, allowDTO);

		// constructing responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		if (flight.getOperationTypeId() != AirScheduleCustomConstants.OperationTypes.OPERATIONS) {

			Collection<FlightLeg> legs = flight.getFlightLegs();
			Iterator<FlightLeg> itLegs = legs.iterator();
			boolean tolaranceErrorFound = false;

			while (itLegs.hasNext()) {

				FlightLeg fleg = (FlightLeg) itLegs.next();

				// set the route info
				fleg.setModelRouteId(fleg.getOrigin() + "/" + fleg.getDestination());

				RouteInfo routeInfo = commonMasterBD.getRouteInfo(fleg.getModelRouteId());
				int allowedDureation = (routeInfo != null) ? routeInfo.getDuration() : 0;
				double errorPercentage = (routeInfo != null) ? routeInfo.getDurationTolerance().doubleValue() : 0;

				if (allowedDureation == 0) {

					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.AllOWED_DURATION_ZERO);
					responce.addResponceParam(ResponceCodes.AllOWED_DURATION_ZERO, fleg);
					return responce;
				}
				
				boolean skipDurationValidation = false;
				if (flight.getViaScheduleMessaging() && AppSysParamsUtil.isSkipDurationToleranceForShedMsgs()) {
					skipDurationValidation = true;
				}
				
				if (!skipDurationValidation) {
					boolean hssError = durationCalculator.calculeteLegDuration(fleg, allowedDureation, errorPercentage);
					if (hssError)
						tolaranceErrorFound = hssError;
				}
			}

			if (tolaranceErrorFound && !allowDTO.isAllowAnyDuration()) {
				if (warning == null)
					warning = new DefaultServiceResponse(true);
				warning.setSuccess(false);
				warning.setResponseCode(ResponceCodes.WARNING_FOR_FLIGHT_UPDATE);
				warning.addResponceParam(ResponceCodes.INVALID_LEG_DURATION, flight);
			}
		}

		if (warning != null)
			responce.addResponceParam(CommandParamNames.WARNING, warning);
		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight, FlightAllowActionDTO allowDTO) throws ModuleException {

		if (flight == null || allowDTO == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
