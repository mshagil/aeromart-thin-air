/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.prepare;

import java.util.Collection;

import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.PeriodsUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for prepare ammend flight schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="prepareAmmendSchedule"
 */
public class PrepareAmmendSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	/**
	 * constructor of the PrepareAmmendSchedule command
	 */
	public PrepareAmmendSchedule() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

	}

	/**
	 * execute method of the PrepareAmmendSchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule updated = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		int firstLegDayDiff = 0;
		if (this.getParameter(CommandParamNames.FIRST_LEG_DAY_DIFFERENCE) != null) {
			firstLegDayDiff = (Integer) this.getParameter(CommandParamNames.FIRST_LEG_DAY_DIFFERENCE);
		}

		// checking params
		this.checkParams(updated);

		FlightSchedule existing = flightSchedleDAO.getFlightSchedule(updated.getScheduleId().intValue());
		// if no flight found for the ammending flight schedule
		if (existing == null) // throw exception
			throw new ModuleException("airschedules.logic.bl.schedule.noschedule");

		FlightSchedule overlapping = null;

		if (existing.isOverlapping()) {

			overlapping = flightSchedleDAO.getFlightSchedule(existing.getOverlapingScheduleId().intValue());
			// if no flight found for the ammending flight schedule
			if (overlapping == null) // throw exception
				throw new ModuleException("airschedules.logic.bl.schedule.noschedule");
		}

		// gets the create, update, and cancel periods for the schedule
		Collection<AffectedPeriod> createPeriods = PeriodsUtil.getCreatePeriods(existing, updated, firstLegDayDiff);
		Collection<AffectedPeriod> updatePeriods = PeriodsUtil.getUpdatePeriods(existing, updated, firstLegDayDiff);
		Collection<AffectedPeriod> cancelPeriods = PeriodsUtil.getCancelPeriods(existing, updated, firstLegDayDiff);

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// set the response params
		responce.addResponceParam(CommandParamNames.LOADED_EXISTING_SCHEDULE, existing);
		responce.addResponceParam(CommandParamNames.LOADED_EXISTING_OVERLAPPING_SCHEDULE, overlapping);
		responce.addResponceParam(CommandParamNames.CREATE_PERIOD_LIST, createPeriods);
		responce.addResponceParam(CommandParamNames.CANCEL_PERIOD_LIST, cancelPeriods);
		responce.addResponceParam(CommandParamNames.UPDATE_PERIOD_LIST, updatePeriods);

		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null || schedule.getBuildStatusCode() == null)
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
