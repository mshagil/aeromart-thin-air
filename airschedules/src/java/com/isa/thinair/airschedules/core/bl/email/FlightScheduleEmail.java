package com.isa.thinair.airschedules.core.bl.email;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airschedules.api.dto.EmailFlightSchedulesDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertInfoDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

/**
 * Extract and send flight schedules (temporary class)
 * 
 * @author Duminda G
 */
public class FlightScheduleEmail {

	private static final Log log = LogFactory.getLog(FlightScheduleEmail.class);

	/**
	 * Send flight schedules
	 * 
	 * @param colEmailFlightSchedulesDTO
	 * @return
	 * @throws ModuleException
	 */
	public static void sendSchedules(Collection<EmailFlightSchedulesDTO> colEmailFlightSchedulesDTO) throws ModuleException {
		compileMail(extractMessege(colEmailFlightSchedulesDTO));
	}

	// TODO get from commons
	public static String[] getDateRangeZulu() {
		long day = 5 * 1000 * 60 * 60 * 24;
		long st = getCurrentDateTime().getTime() - day;
		long ed = getCurrentDateTime().getTime() + day;
		Date StartDate = new Date(st);
		Date EndDate = new Date(ed);

		return new String[] { formatDate(StartDate, "dd/MM/yyyy HH:mm"), formatDate(EndDate, "dd/MM/yyyy HH:mm") };
	}

	/**
	 * get current system date
	 * 
	 * @return
	 */
	private static Date getCurrentDateTime() {
		TimeZone timeZone = null;
		String strTimeZone = "GMT";
		Calendar cal = Calendar.getInstance();

		if (strTimeZone == null || "".equals(strTimeZone)) {
			timeZone = TimeZone.getDefault();
		} else {
			TimeZone.getTimeZone(strTimeZone);
		}

		cal.setTimeZone(timeZone);
		return cal.getTime();
	}

	/**
	 * format date
	 * 
	 * @param utilDate
	 * @param fmt
	 * @return
	 */
	private static String formatDate(Date utilDate, String fmt) {
		String formatedDate = "";

		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			formatedDate = sdFmt.format(utilDate);
		}

		return formatedDate;
	}

	/**
	 * Extract message
	 * 
	 * @param colEmailFlightSchedulesDTO
	 * @return
	 */
	private static String extractMessege(Collection<EmailFlightSchedulesDTO> colEmailFlightSchedulesDTO) {
		StringBuilder strMessege = new StringBuilder();
		boolean addSeperator = false;
		for (Iterator<EmailFlightSchedulesDTO> iter = colEmailFlightSchedulesDTO.iterator(); iter.hasNext();) {
			EmailFlightSchedulesDTO emailFlightSchedulesDTO = (EmailFlightSchedulesDTO) iter.next();
			if (addSeperator) {
				strMessege.append("|");
			}
			strMessege.append(emailFlightSchedulesDTO.getFlight_id()).append(",").append(emailFlightSchedulesDTO.getFlt_seg_id())
					.append(",").append(emailFlightSchedulesDTO.getA_city_code()).append(",")
					.append(emailFlightSchedulesDTO.getArrival_airport()).append(",")
					.append(emailFlightSchedulesDTO.getD_city_code()).append(",")
					.append(emailFlightSchedulesDTO.getDeparture_airport()).append(",")
					.append(emailFlightSchedulesDTO.getEst_time_arrival_zulu()).append(",")
					.append(emailFlightSchedulesDTO.getEst_time_departure_zulu()).append(",")
					.append(emailFlightSchedulesDTO.getFlight_number()).append(",")
					.append(emailFlightSchedulesDTO.getModel_number()).append(",").append(emailFlightSchedulesDTO.getStatus());
			addSeperator = true;
		}

		return strMessege.toString();
	}

	/**
	 * Compile mail
	 * 
	 * @param messege
	 * @throws ModuleException
	 */
	private static void compileMail(String messege) throws ModuleException {
		log.warn("FIXME - implement emailing");
		// FIXME
		// TODO move to config
		// EmailMessage msg = new EmailMessage();
		// msg.setFromAddress("res@accelaero.com");
		// msg.setToAddress("dcs@accelaero.com");
		// msg.setSubject("FLIGHTSCHEDULE");
		//
		// msg.setBody(messege);
		//
		// msg.setMailServerConfigurationName("testdcs");
		// EmailBroadcaster emailBroadcaster = new EmailBroadcaster();
		// List messagesList = new ArrayList();
		// messagesList.add(msg);
		// emailBroadcaster.sendMessages(messagesList);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void sentFlightCancellationOfficerNotification(Collection<Flight> cancelledFlights, boolean success) throws ModuleException {

		if (cancelledFlights != null && !cancelledFlights.isEmpty()) {
			AirportBD airportBD = AirSchedulesUtil.getAirportBD();
			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
			cancelledFlights = timeAdder.addLocalTimeDetailsToFlights(cancelledFlights);
			Collection<FlightReservationAlertDTO> flightReservationAlertDTOList = AlertUtil
					.prepareFlightAlertInfo(cancelledFlights);

			ReservationBD reservationBD = AirSchedulesUtil.getReservationBD();
			List<String> officerEmilList = (List<String>) reservationBD.getOfficersEmailList();

			if (!success){
				officerEmilList.addAll(AppSysParamsUtil.getScheduleMsgProcessErrorEmailRecipients());
			}

			MessageProfile profile;
			List messageList = new ArrayList();

			if (officerEmilList != null && !officerEmilList.isEmpty() && flightReservationAlertDTOList != null
					&& !flightReservationAlertDTOList.isEmpty()) {

				for (String emailAddress : officerEmilList) {

					UserMessaging user = new UserMessaging();
					user.setToAddres(emailAddress);
					messageList.add(user);
				}

				List messageProfiles = new ArrayList();
				FlightReservationAlertDTO flightReservationAlertDTO;

				Iterator itColFlightReservationAlertDTO = flightReservationAlertDTOList.iterator();
				while (itColFlightReservationAlertDTO.hasNext()) {

					flightReservationAlertDTO = (FlightReservationAlertDTO) itColFlightReservationAlertDTO.next();

					profile = new MessageProfile();
					profile.setUserMessagings(messageList);

					Topic topic = new Topic();
					HashMap map = new HashMap();

					String emailSubject = (success)? "FLIGHT CANCELLATION  "  : "FLIGHT CANCELLATION FAILURE   "
							+ flightReservationAlertDTO.getOldFlightAlertInfoDTO().getFlightNumber() + " : "
							+ flightReservationAlertDTO.getOldFlightAlertInfoDTO().getDepartureStringDate("dd/MMM/yyyy");					

					map.put("subject", emailSubject);
					map.put("description", "");

					map.put("alertTemplateEnum", flightReservationAlertDTO.getAlertTemplateIds());
					map.put("flightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());

					topic.setLocale(Locale.ENGLISH);
					topic.setTopicParams(map);
					topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_EMAIL_FOR_AN_OFFICIAL);
					profile.setTopic(topic);
					messageProfiles.add(profile);
				}

				if (messageProfiles.size() > 0) {
					MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
					messagingServiceBD.sendMessages(messageProfiles);
				}
			}

		}

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void sendScheduleCancellationOfficerNotification(FlightSchedule schedule, int numberOfFlights, boolean success) throws ModuleException {

		if (schedule != null && (numberOfFlights > 0 || !success)) {
			AirportBD airportBD = AirSchedulesUtil.getAirportBD();
			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);

			schedule = timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);

			FlightAlertInfoDTO fltAlertInfoDTO = AlertUtil.prepareScheduleAlertInfo(schedule);
			fltAlertInfoDTO.setNumberOfFlights(numberOfFlights);
			

			ReservationBD reservationBD = AirSchedulesUtil.getReservationBD();
			List<String> officerEmilList = (List<String>) reservationBD.getOfficersEmailList();
			
			if (!success){
				officerEmilList.addAll(AppSysParamsUtil.getScheduleMsgProcessErrorEmailRecipients());
			}
			MessageProfile profile;
			List messageList = new ArrayList();

			if (officerEmilList != null && !officerEmilList.isEmpty() && fltAlertInfoDTO != null) {

				for (String emailAddress : officerEmilList) {

					UserMessaging user = new UserMessaging();
					user.setToAddres(emailAddress);
					messageList.add(user);
				}

				List messageProfiles = new ArrayList();

				profile = new MessageProfile();
				profile.setUserMessagings(messageList);

				Topic topic = new Topic();
				HashMap map = new HashMap();

				String emailSubject = (success)? "SCHEDULE CANCELLATION   " : "SCHEDULE CANCELLATION FAILURE   "
						+ fltAlertInfoDTO.getFlightNumber() + " : "
						+ fltAlertInfoDTO.getStartDateFormatted("dd/MMM/yyyy") + " - "
						+ fltAlertInfoDTO.getEndDateFormatted("dd/MMM/yyyy");				

				map.put("subject", emailSubject);
				map.put("description", "");				
				map.put("flightAlertInfoDTO", fltAlertInfoDTO);

				topic.setLocale(Locale.ENGLISH);
				topic.setTopicParams(map);
				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.SCHEDULE_CANCEL_EMAIL_FOR_AN_OFFICIAL);
				profile.setTopic(topic);
				messageProfiles.add(profile);

				if (messageProfiles.size() > 0) {
					MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
					messagingServiceBD.sendMessages(messageProfiles);
				}
			}

		}

	}
}
