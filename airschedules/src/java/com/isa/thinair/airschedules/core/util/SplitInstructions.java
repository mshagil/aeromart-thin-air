/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.util;

import java.util.Date;

import com.isa.thinair.commons.api.dto.Frequency;

/**
 * @author Lasantha Pambagoda
 */
public class SplitInstructions {

	public static int LEFT_SPLIT = 0;

	public static int RIGHT_SPLIT = 1;

	public static int FREQUENCY_SPLIT = 2;

	private int operation;

	private Date date;

	private Frequency frequency;

	/**
	 * @return Returns the date.
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            The date to set.
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return Returns the operation.
	 */
	public int getOperation() {
		return operation;
	}

	/**
	 * @param operation
	 *            The operation to set.
	 */
	public void setOperation(int operation) {
		this.operation = operation;
	}

	/**
	 * @return Returns the frequency.
	 */
	public Frequency getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency
	 *            The frequency to set.
	 */
	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}
}
