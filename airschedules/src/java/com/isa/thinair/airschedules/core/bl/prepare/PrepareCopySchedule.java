/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.prepare;

import java.util.Date;
import java.util.HashSet;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for copy schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="prepareCopySchedule"
 */
public class PrepareCopySchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	// BDs
	private AirportBD airportBD;

	// helpers
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the CopySchedule command
	 */
	public PrepareCopySchedule() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// BDs
		airportBD = AirSchedulesUtil.getAirportBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the CopySchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Integer scheduleId = (Integer) this.getParameter(CommandParamNames.SCHEDULE_ID);
		Date fromDate = (Date) this.getParameter(CommandParamNames.START_DATE);
		Date toDate = (Date) this.getParameter(CommandParamNames.END_DATE);
		Boolean datesInLocal = (Boolean) this.getParameter(CommandParamNames.IS_DATE_IN_LOCAL);

		// checking params
		checkParams(scheduleId, fromDate, toDate, datesInLocal);

		// load the corresponding schedule
		FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
		// if no schedule found for the id
		if (schedule == null) // throw exception
			throw new ModuleException("airschedules.logic.bl.schedule.noschedule");

		// get the copy of flight schedule
		FlightSchedule copyedSchedule = ScheduledFlightUtil.getCopyOfSchedule(schedule);
		
		if (copyedSchedule.getGdsIds() != null) {
			// when copying schedule new schedule needs to be published manually to the respective GDS.
			copyedSchedule.getGdsIds().clear();
		}		
		
		if (copyedSchedule.getSsmSplitFlightSchedule()!= null) {
			// when copying schedule new schedule needs to be split seperate.
			copyedSchedule.getSsmSplitFlightSchedule().clear();
		}		

		// set the fields not comming from the coping schedule
		copyedSchedule.setBuildStatusCode(ScheduleBuildStatusEnum.BUILT.getCode());
		copyedSchedule.setStatusCode(ScheduleStatusEnum.ACTIVE.getCode());
		copyedSchedule.setNumberOfDepartures(null);
		copyedSchedule.setAvailableSeatKilometers(null);
		copyedSchedule.setGdsIds(new HashSet<Integer>());

		FlightScheduleLeg copyedFirstLeg = LegUtil.getFirstScheduleLeg(copyedSchedule.getFlightScheduleLegs());

		if (datesInLocal.booleanValue()) {

			String origin = schedule.getDepartureStnCode();
			Date effectiveDate = schedule.getStartDate();

			Date scheduleLocalStartTime = timeAdder.getLocalDateTimeAsEffective(origin, effectiveDate,
					copyedFirstLeg.getEstDepartureTimeZulu());
			Date copyStartDate = timeAdder.getZuluDateTimeAsEffective(origin, effectiveDate,
					CalendarUtil.getTimeAddedDate(fromDate, scheduleLocalStartTime));
			Date copyToDate = timeAdder.getZuluDateTimeAsEffective(origin, effectiveDate,
					CalendarUtil.getTimeAddedDate(toDate, scheduleLocalStartTime));

			copyedSchedule.setStartDate(copyStartDate);
			copyedSchedule.setStopDate(copyToDate);

		} else {

			Date copyStartDate = CalendarUtil.getTimeAddedDate(fromDate, copyedFirstLeg.getEstDepartureTimeZulu());
			Date copyToDate = CalendarUtil.getTimeAddedDate(toDate, copyedFirstLeg.getEstDepartureTimeZulu());

			copyedSchedule.setStartDate(copyStartDate);
			copyedSchedule.setStopDate(copyToDate);
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.addResponceParam(CommandParamNames.FLIGHT_SCHEDULE, copyedSchedule);
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Integer scheduleId, Date startDate, Date endDate, Boolean datesInLocal) throws ModuleException {

		if (scheduleId == null || startDate == null || endDate == null || datesInLocal == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}

}
