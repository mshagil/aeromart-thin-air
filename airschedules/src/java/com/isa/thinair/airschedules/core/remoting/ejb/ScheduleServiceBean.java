/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.remoting.ejb;

import com.isa.thinair.airmaster.api.model.AirportDSTHistory;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.OperationType;
import com.isa.thinair.airschedules.api.model.ScheduleBuildStatus;
import com.isa.thinair.airschedules.api.model.ScheduleStatus;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.airschedules.core.bl.email.FlightScheduleEmail;
import com.isa.thinair.airschedules.core.persistence.dao.AirSchedulesCommonDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleJdbcDAO;
import com.isa.thinair.airschedules.core.service.bd.ScheduleBDImpl;
import com.isa.thinair.airschedules.core.service.bd.ScheduleBDLocalImpl;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ScheduleRevisionTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.ServiceResponce;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

/**
 * Session bean to provide Flight Schedule Service realted functionalities
 * 
 * @author Lasantha Pambagoda
 */
@Stateless
@RemoteBinding(jndiBinding = "ScheduleService.remote")
@LocalBinding(jndiBinding = "ScheduleService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ScheduleServiceBean extends PlatformBaseSessionBean implements ScheduleBDImpl, ScheduleBDLocalImpl {

	/**
	 * method to get flight schedule for given flight schedule id
	 * 
	 */
	public FlightSchedule getFlightSchedule(int id) throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getFlightSchedule(id);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * method to search flights according to given criteria
	 * 
	 */
	public Page searchFlightSchedules(List<ModuleCriterion> criteria, int startIndex, int pageSize) throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).searchFlightSchedules(criteria, startIndex, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to search flight schedules for display
	 * 
	 */
	public Page searchFlightSchedulesForDisplay(List<ModuleCriterion> criteria, int startIndex, int pageSize,
			boolean isDatesInLocal) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.SEARCH_SCHEDULES_FOR_DISPLAY_MACRO);
		command.setParameter(CommandParamNames.CRITERIA_LIST, criteria);
		command.setParameter(CommandParamNames.START_INDEX, new Integer(startIndex));
		command.setParameter(CommandParamNames.PAGE_SIZE, new Integer(pageSize));
		command.setParameter(CommandParamNames.IS_DATE_IN_LOCAL, new Boolean(isDatesInLocal));
		command.setParameter((CommandParamNames.IS_ADHOC_FLIGHT_DATA_ENABLED), false);
		DefaultServiceResponse responce = (DefaultServiceResponse) command.execute();

		return (Page) responce.getResponseParam(CommandParamNames.PAGE);
	}

	/**
	 * method to search flight schedules for display
	 * 
	 */
	public Page[] searchAndReassembleSchedules(List<ModuleCriterion> criteria, int startIndex, int pageSize,
			boolean isDatesInLocal, boolean isAdhocFlightDataEnabled) throws ModuleException {
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.SEARCH_AND_REASSEMBLE_SCHEDULES);
		command.setParameter(CommandParamNames.CRITERIA_LIST, criteria);
		command.setParameter(CommandParamNames.START_INDEX, new Integer(startIndex));
		command.setParameter(CommandParamNames.PAGE_SIZE, new Integer(pageSize));
		command.setParameter(CommandParamNames.IS_DATE_IN_LOCAL, new Boolean(isDatesInLocal));
		command.setParameter(CommandParamNames.IS_ADHOC_FLIGHT_DATA_ENABLED, new Boolean(isAdhocFlightDataEnabled));
		DefaultServiceResponse responce = (DefaultServiceResponse) command.execute();

		Page[] pages = { (Page) responce.getResponseParam(CommandParamNames.PAGE),
				(Page) responce.getResponseParam(CommandParamNames.ADHOC_FLIGHT_PAGE) };
		return pages;
	}

	/**
	 * mehtod to get all the flight schedules
	 * 
	 */
	public Page getAllFlightSchedules(int startIndex, int pageSize) throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getAllFlightSchedules(startIndex, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to get number of flights for schedule
	 * 
	 */
	public int getNoOfFlightsForSchedule(int scheduleId) throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getNoOfFlightsForSchedule(scheduleId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to get flights
	 * 
	 */
	public Collection<Flight> getFlights(int scheduleId) throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getFlightsForSchedule(scheduleId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Collection<Flight> getFutureFlightsOfSchedule(int scheduleId, Date fromDate, Date toDate) throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getFutureFlightsForSchedule(scheduleId, fromDate,
					toDate);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to get all operration types
	 * 
	 */
	public Collection<OperationType> getAllOperationTypes() throws ModuleException {
		try {
			return ((AirSchedulesCommonDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.AIR_SCHEDULES_COMMON_DAO)).getAllOpertationTypes();
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to get all schedule build statuses
	 * 
	 */
	public Collection<ScheduleBuildStatus> getAllScheduleBuildStatus() throws ModuleException {
		try {
			return ((AirSchedulesCommonDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.AIR_SCHEDULES_COMMON_DAO)).getAllScheduleBuildStatus();
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to get all schedule statuses
	 * 
	 */
	public Collection<ScheduleStatus> getAllScheduleStatus() throws ModuleException {
		try {
			return ((AirSchedulesCommonDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.AIR_SCHEDULES_COMMON_DAO)).getAllScheduleStatus();
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to get flight numbers schedule
	 * 
	 */
	public Collection<String> getFlightNumbers() throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getFlightNumbers();
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to save flights
	 */
	public void rollForwardUserNote(Collection<Integer> ids, String userNote, String shedId) throws ModuleException {

		try {
			((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO))
					.rollForwardUserNote(ids, userNote, shedId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * method to get Flight Schedules to GDS Publish
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightScheduleInfoDTO> getFlightSchedulesToGDSPublish(Date lastPublishTime, String codeShareCarrier) throws ModuleException {
		try {
			Collection<FlightSchedule> scheduleList = ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getFlightSchedulesToGDSPublish(lastPublishTime, codeShareCarrier);

			Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.SCHEDULES_FOR_PUBLISH_MACRO);
			command.setParameter(CommandParamNames.FLIGHT_SCHEDULE_LIST, scheduleList);
			ServiceResponce responce = command.execute();

			return ((Collection<FlightScheduleInfoDTO>) responce.getResponseParam(CommandParamNames.FLIGHT_SCHEDULE_INFO_LIST));

		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to get possible overlapping ids
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightSchedule> getPossibleOverlappingSchedules(FlightSchedule schedule) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance()
				.getLocalBean(CommandNames.POSSIBLE_OVERLAPPING_SCHEDULES_MACRO);
		command.setParameter(CommandParamNames.FLIGHT_SCHEDULE, schedule);
		ServiceResponce responce = command.execute();

		return ((Collection<FlightSchedule>) responce.getResponseParam(CommandParamNames.FLIGHT_SCHEDULE_LIST));
	}

	/**
	 * method to create schedule
	 * 
	 */
	public ServiceResponce createSchedule(FlightSchedule flightSchedule, boolean isReviseFrequency,
			boolean isEnableAttachDefaultAnciTemplate) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.CREATE_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.CREATE_SCHEDULE_MACRO);
		flightSchedule = (FlightSchedule)setUserDetails(flightSchedule);
		command.setParameter(CommandParamNames.FLIGHT_SCHEDULE, flightSchedule);
		command.setParameter(CommandParamNames.IS_REVISE_FREQUENCY, isReviseFrequency);
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		command.setParameter(CommandParamNames.IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL, isEnableAttachDefaultAnciTemplate);
		return this.execute(command);
	}

	/**
	 * method to build schedule
	 */
	@TransactionTimeout(900)
	public ServiceResponce buildSchedule(Collection<Integer> scheduleIds, boolean isScheduleMessageProcess)
			throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.BUILD_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.BUILD_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.SCHEDULE_IDS, scheduleIds);
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		command.setParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, isScheduleMessageProcess);
		return this.execute(command);
	}

	/**
	 * method to build schedule
	 */
	@TransactionTimeout(900)
	public ServiceResponce buildSchedule(Integer scheduleId) throws ModuleException {

		Collection<Integer> scheduleIds = new ArrayList<Integer>();
		scheduleIds.add(scheduleId);

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.BUILD_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.BUILD_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.SCHEDULE_IDS, scheduleIds);
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		return this.execute(command);
	}

	/**
	 * Method to cancel schedule
	 * 
	 * @param scheduleId
	 * @param forceCancel
	 * @param flightAlertDTO
	 * @param overwriteFlag
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	public ServiceResponce cancelSchedule(int scheduleId, boolean forceCancel, FlightAlertDTO flightAlertDTO,
			boolean overwriteFlag, boolean isScheduleMessageProcess) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.CANCEL_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.CANCEL_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.SCHEDULE_ID, new Integer(scheduleId));
		command.setParameter(CommandParamNames.FORCE, new Boolean(forceCancel));
		command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);
		command.setParameter(CommandParamNames.FLIGHT_OVERWRITE_FLAG, new Boolean(overwriteFlag));
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		command.setParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, isScheduleMessageProcess);
		return this.execute(command);
	}

	/**
	 * method to copy schedule
	 * 
	 */
	public ServiceResponce copySchedule(int scheduleId, Date fromDate, Date toDate, boolean isDatesInLocal)
			throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.COPY_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.COPY_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.SCHEDULE_ID, new Integer(scheduleId));
		command.setParameter(CommandParamNames.START_DATE, fromDate);
		command.setParameter(CommandParamNames.END_DATE, toDate);
		command.setParameter(CommandParamNames.IS_DATE_IN_LOCAL, new Boolean(isDatesInLocal));
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		return this.execute(command);
	}

	/**
	 * method to split schedule
	 * 
	 */
	public ServiceResponce splitSchedule(int scheduleId, Date fromDate, Date toDate, Frequency days, boolean isDatesInLocal,
			boolean isReviseFrequency, boolean isScheduleMessageProcess) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.SPLIT_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.SPLIT_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.SCHEDULE_ID, new Integer(scheduleId));
		command.setParameter(CommandParamNames.AFFECTED_PERIOD, new AffectedPeriod(fromDate, toDate, days));
		command.setParameter(CommandParamNames.IS_DATE_IN_LOCAL, new Boolean(isDatesInLocal));
		command.setParameter(CommandParamNames.IS_REVISE_FREQUENCY, new Boolean(isReviseFrequency));
		command.setParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, isScheduleMessageProcess);
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		return this.execute(command);
	}

	/**
	 * method to split schedule
	 * 
	 */
	public ServiceResponce splitBulkSchedule(String aircraftCode, String newAircraftCode) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.SPLIT_BULK_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.SPLIT_BULK_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.AIRCRAFT_CODE, aircraftCode);
		command.setParameter(CommandParamNames.NEW_AIRCRAFT_CODE, newAircraftCode);

		return this.execute(command);
	}

	/**
	 * method to update schedule details
	 * 
	 * @param flightSchedule
	 * @param forceCancel
	 * @param flightAlertDTO
	 * @return
	 * @throws ModuleException
	 * 
	 */
	public ServiceResponce updateSchedule(FlightSchedule flightSchedule, boolean forceCancel, FlightAlertDTO flightAlertDTO,
			boolean overwriteFlag, boolean downgradeToAnyModel, boolean isScheduleMessageProcessing,
			boolean isEnableAttachDefaultAnciTemplate) throws ModuleException {
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.AMMEND_SCHEDULE_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.AMMEND_SCHEDULE_MACRO);
		flightSchedule =(FlightSchedule)setUserDetails(flightSchedule);
		command.setParameter(CommandParamNames.FLIGHT_SCHEDULE, flightSchedule);
		command.setParameter(CommandParamNames.FORCE, new Boolean(forceCancel));
		command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);
		command.setParameter(CommandParamNames.FLIGHT_OVERWRITE_FLAG, new Boolean(overwriteFlag));
		command.setParameter(CommandParamNames.IS_UPDATING_LEGS, new Boolean(false));
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		command.setParameter(CommandParamNames.FLIGHT_DOWNGRADE_TO_ANY_MODEL, new Boolean(downgradeToAnyModel));
		command.setParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, isScheduleMessageProcessing);
		command.setParameter(CommandParamNames.IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL, isEnableAttachDefaultAnciTemplate);
		return this.execute(command);
	}

	/**
	 * method to update schedule leg details
	 * 
	 * @param flightSchedule
	 * @param forceCancel
	 * @param flightAlertDTO
	 * @return
	 * @throws ModuleException
	 * 
	 */
	public ServiceResponce updateScheduleLegs(FlightSchedule flightSchedule, boolean forceCancel, FlightAlertDTO flightAlertDTO,
			boolean overwriteFlag, int firstLegDayDiff) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.AMMEND_SCHEDULE_LEGS_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.AMMEND_SCHEDULE_LEGS_MACRO);
		command.setParameter(CommandParamNames.FLIGHT_SCHEDULE, flightSchedule);
		command.setParameter(CommandParamNames.FORCE, new Boolean(forceCancel));
		command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);
		command.setParameter(CommandParamNames.FLIGHT_OVERWRITE_FLAG, new Boolean(overwriteFlag));
		command.setParameter(CommandParamNames.IS_UPDATING_LEGS, new Boolean(true));
		command.setParameter(CommandParamNames.FIRST_LEG_DAY_DIFFERENCE, new Integer(firstLegDayDiff));
		command.setParameter(CommandParamNames.FLIGHT_DOWNGRADE_TO_ANY_MODEL, new Boolean(false));
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		return this.execute(command);
	}

	/**
	 * public method to check whether reservations exist for a cancelled schedule
	 * 
	 * @param scheduleId
	 * @param forceCancel
	 * @return HashMap of flight details with reservation info
	 * @throws ModuleException
	 * 
	 */
	public ServiceResponce getFlightReservationsForCancel(int scheduleId, boolean forceCancel) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.CHECK_RESERVATIONS);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.CHECK_RESERVATIONS);
		command.setParameter(CommandParamNames.CHECK_RESERVATIONS_ONLY, new Boolean(true));
		command.setParameter(CommandParamNames.SCHEDULE_ID, new Integer(scheduleId));
		command.setParameter(CommandParamNames.FORCE, new Boolean(forceCancel));
		return this.execute(command);
	}

	/**
	 * method to get flight schedules
	 * 
	 */
	public void sendFlightSchedules() throws ModuleException {
		try {
			String[] dateRange = FlightScheduleEmail.getDateRangeZulu();
			FlightScheduleEmail.sendSchedules(lookupFlightScheduleJdbcDAO().PublishFlightSchedule(dateRange[0], dateRange[1]));
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Method to Lookup the FlightScheduleJdbcDAO FlightScheduleJdbcDAO
	 * 
	 * @return
	 */
	private FlightScheduleJdbcDAO lookupFlightScheduleJdbcDAO() {

		FlightScheduleJdbcDAO dao = null;
		try {
			dao = (FlightScheduleJdbcDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_JDBC_DAO);
		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}
		return dao;
	}

	/**
	 * publish the schedule to GDSes
	 * 
	 * @param scheduleId
	 * @param gdsIds
	 * @return
	 * @throws ModuleException
	 * 
	 */
	public ServiceResponce publishScheduleToGDS(Integer scheduleId, Collection<Integer> gdsIds, Integer version)
			throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.ADD_REOMVE_SCHEDULE_G_D_S_MACRO);
		command.setParameter(CommandParamNames.SCHEDULE_ID, scheduleId);
		command.setParameter(CommandParamNames.GDS_IDS, gdsIds);
		command.setParameter(CommandParamNames.VERSION_ID, new Integer(version));
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		
		return this.execute(command);
	}

	/**
	 * publish the schedule to GDSes
	 * 
	 * @param scheduleId
	 * @param gdsIds
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void publishAllSchedulesToGDS(List<Integer> gdsIds) throws ModuleException {

		try {
			ScheduleSearchDTO dto = new ScheduleSearchDTO();
			dto.setStartDate(new Date());
			GregorianCalendar currentDate = new GregorianCalendar();
			// Schedules for next 3 years
			currentDate.set(GregorianCalendar.YEAR, currentDate.get(GregorianCalendar.YEAR) + 3);
			dto.setEndDate(currentDate.getTime());
			dto.setScheduleStatus(ScheduleStatusEnum.ACTIVE.getCode());
			dto.setBuildStatus(ScheduleBuildStatusEnum.BUILT.getCode());
			Collection<FlightScheduleInfoDTO> schedules = ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getAvailableFlightSchedules(dto);
			for (FlightScheduleInfoDTO schedule : schedules) {
				if (schedule.getScheduleId() != null) {
					publishScheduleToGDS(schedule.getScheduleId(), gdsIds, (int) schedule.getVersion());
				}
			}
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			throw new ModuleException(ex.getMessage());
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce publishSchedulesToGDS(int gdsID, Date scheduleStartDate, Date scheduleEndDate, boolean isSelectedSchedules,
			String emailAddress) throws ModuleException{
		
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.PUBLISH_S_S_M_RECAP_MACRO);
		command.setParameter(CommandParamNames.GDS_ID, gdsID);
		command.setParameter(CommandParamNames.SCHEDULE_START_DATE, scheduleStartDate);
		command.setParameter(CommandParamNames.SCHEDULE_END_DATE, scheduleEndDate);
		command.setParameter(CommandParamNames.IS_SELECTED_SCHEDULES, isSelectedSchedules);
		command.setParameter(CommandParamNames.EMAIL_ADDRESS, emailAddress);
		command.setParameter(CommandParamNames.EXTERNAL_SSM_SENDING, false);
		return this.execute(command);
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce publishSchedulesToExternal(Date scheduleStartDate, Date scheduleEndDate, boolean isSelectedSchedules,
			String emailAddress, String sitaAddress) throws ModuleException {
		
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.PUBLISH_S_S_M_RECAP_MACRO);
		command.setParameter(CommandParamNames.SCHEDULE_START_DATE, scheduleStartDate);
		command.setParameter(CommandParamNames.SCHEDULE_END_DATE, scheduleEndDate);
		command.setParameter(CommandParamNames.IS_SELECTED_SCHEDULES, isSelectedSchedules);
		command.setParameter(CommandParamNames.EXTERNAL_EMAIL_ADDRESS, emailAddress);
		command.setParameter(CommandParamNames.EXTERNAL_SITA_ADDRESS, sitaAddress);
		command.setParameter(CommandParamNames.EXTERNAL_SSM_SENDING, true);
		return this.execute(command);
	}

	public Collection<FlightScheduleInfoDTO> getFlightSchedulesInfomation(ScheduleSearchDTO scheduleSearchDTO)
			throws ModuleException {

		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getAvailableFlightSchedules(scheduleSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * private method to execute the command
	 * 
	 * @param command
	 * @return response
	 * @throws ModuleException
	 */
	public ServiceResponce execute(Command command) throws ModuleException {

		try {

			return command.execute();

		} catch (ModuleException ex) {

			this.sessionContext.setRollbackOnly();
			throw ex;

		} catch (CommonsDataAccessException cdaex) {

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		} catch (Exception ex) {

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	public void scheduleJob(Map<String,String> jobDataMap) throws ModuleException {
		AirSchedulesUtil.getSchedularBD().scheduleJob(jobDataMap);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection<FlightSchedule> getDSTApplicableFutureSchedules(AirportDSTHistory airportDST)
			throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getDSTApplicableFutureSchedules(airportDST);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	
	public void scheduleSSMRecap(Map<String, String> jobDataMap) throws ModuleException{
		AirSchedulesUtil.getSchedularBD().scheduleJob(jobDataMap);		
	}	

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce splitSubScheduleForSSM(FlightSchedule schedule) throws ModuleException {
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.SPLIT_SUB_SCHEDULE);
		command.setParameter(CommandParamNames.FLIGHT_SCHEDULE, schedule);

		return this.execute(command);

	}

	@Override
	public void updateDelayCancelFlightSchedule(Collection<Integer> scheduleIds, boolean status, Date holdTill, String remarks)
			throws ModuleException {
		try {
			((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO))
					.updateDelayCancelFlightSchedule(scheduleIds, status, holdTill, remarks);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}
	
	public void resumeSsmAsmWaitingCancelActions() throws ModuleException {
		try {
			Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.RESUME_CANCEL_SCHEDULE);
			// command.setParameter(CommandParamNames.HOLD_CANCEL_MSG_TILL, null);

			this.execute(command);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Collection<FlightSchedule> getPossibleFlightDesignatorChange(Date startDate, Date stopDate, String segmentCode,
			boolean isLocalTime) throws ModuleException {
		try {
			return ((FlightScheduleDAO) AirSchedulesUtil.getInstance()
					.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO))
							.getPossibleFlightDesignatorChange(startDate, stopDate, segmentCode, isLocalTime);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	/**
	 * Method to cancel schedule
	 * 
	 * @param scheduleId
	 * @param forceCancel
	 * @param flightAlertDTO
	 * @param overwriteFlag
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce cancelScheduleWithRequiresNew(int scheduleId, boolean forceCancel, FlightAlertDTO flightAlertDTO,
			boolean overwriteFlag, boolean isScheduleMessageProcess) throws ModuleException {
		return cancelSchedule(scheduleId, forceCancel, flightAlertDTO, overwriteFlag, isScheduleMessageProcess);
	}

	@Override
	public void removeGdsAutopublishRoutes(String gdsId) throws ModuleException {
		lookupFlightScheduleJdbcDAO().removeGdsAutopublishRoutes(gdsId);
	}

	@Override
	public ServiceResponce updateMissingFlightsWithinPeriod(ScheduleRevisionTO scheduleRevisionTO, FlightAlertDTO flightAlertDTO,
			FlightAllowActionDTO flightAllowActionDTO) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.UPDATE_MISSING_FLIGHTS);
		command.setParameter(CommandParamNames.SCHEDULE_REVISION_DTO, scheduleRevisionTO);
		command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);
		command.setParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO, flightAllowActionDTO);
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());

		return this.execute(command);
	}
}
