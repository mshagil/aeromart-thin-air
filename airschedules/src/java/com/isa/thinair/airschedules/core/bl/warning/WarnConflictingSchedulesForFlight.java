/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.warning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.OverlappedScheduleDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for validate conflicting schedules with given flight
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="warnConflictingSchedulesForFlight"
 */
public class WarnConflictingSchedulesForFlight extends DefaultBaseCommand {

	// Dao's
	private FlightDAO flightDAO;

	/**
	 * constructor of the ValidateConflictingSchedulesForFlight command
	 */
	public WarnConflictingSchedulesForFlight() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	/**
	 * execute method of the ValidateConflictingSchedulesForFlight command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);

		FlightAllowActionDTO allowDTO = (FlightAllowActionDTO) this.getParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO);

		DefaultServiceResponse warning = (DefaultServiceResponse) this.getParameter(CommandParamNames.WARNING);

		// checking params
		this.checkParams(flight, allowDTO);

		if (!allowDTO.isAllowConflicts()) {

			// get the conflicting scheules with this schedule
			Collection<FlightSchedule> conflictingSchedules = flightDAO.getConflictingSchedules(flight.getDayNumber(), flight.getFlightNumber(),
					flight.getDepartureDate(), flight.getScheduleId());

			if (conflictingSchedules.size() > 0) {

				if (warning == null)
					warning = new DefaultServiceResponse(false);

				warning.setResponseCode(ResponceCodes.WARNING_FOR_FLIGHT_UPDATE);

				ArrayList<OverlappedScheduleDTO> list = new ArrayList<OverlappedScheduleDTO>();
				Iterator<FlightSchedule> itCon = conflictingSchedules.iterator();

				while (itCon.hasNext()) {

					FlightSchedule overlapping = (FlightSchedule) itCon.next();

					OverlappedScheduleDTO dto = new OverlappedScheduleDTO();
					dto.setFlightNumber(overlapping.getFlightNumber());
					dto.setStartDate(overlapping.getStartDate());
					dto.setOperationType(overlapping.getOperationTypeId() + "");
					dto.setScheduleId(overlapping.getScheduleId());
					list.add(dto);
				}

				warning.addResponceParam(ResponceCodes.FLIGHT_SCHEDULE_CONFLICT, list);
			}
		}

		// constructing responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (warning != null)
			responce.addResponceParam(CommandParamNames.WARNING, warning);
		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight, FlightAllowActionDTO allowDTO) throws ModuleException {

		if (flight == null || allowDTO == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
