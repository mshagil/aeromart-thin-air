/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airschedules.core.bl.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AlertTypeEnum;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for the RecalculateLocalTime
 * 
 * @author byorn/chandana
 * @isa.module.command name="recalculateLocalTime"
 */
public class RecalculateLocalTime extends DefaultBaseCommand {

	// Dao's
	private FlightSegementDAO flightSegementDAO;
	private FlightDAO flightDAO;
	private AirportBD airportBD;

	private LocalZuluTimeAdder timeAdder;

	public static final String GMT_OFFSET_ACTION_MINUS = "-";

	/**
	 * constructor of the RecalculateLocalTime command
	 */
	public RecalculateLocalTime() {

		// look up DAOs
		flightSegementDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		airportBD = AirSchedulesUtil.getAirportBD();

		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the RecalculateLocalTime command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		boolean flightHasBeenUpdated = false;
		Date startDate = (Date) this.getParameter(CommandParamNames.START_DATE);
		Date endDate = (Date) this.getParameter(CommandParamNames.END_DATE);
		Airport airport = (Airport) this.getParameter(CommandParamNames.AIRPORT);
		Collection<AirportDST> dsts = (Collection<AirportDST>) this.getParameter(CommandParamNames.AIRPORT_DST);
		FlightAlertDTO alertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);

		// checking params
		this.checkParams(startDate, endDate, airport, dsts, alertDTO);

		String airportCode = airport.getAirportCode();

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// prepare alert/email DTO for flight
		Collection<FlightReservationAlertDTO> flightReservationAlertDTOList = new ArrayList<FlightReservationAlertDTO>();

		// list for sending alerts
		List<Integer> flightSegIds = new ArrayList<Integer>();
		
		// Flight Id list to publish
		List<Integer> flightIds = new ArrayList<Integer>();

		// arrival time updation.
		Collection<FlightSegement> flightSegments = flightSegementDAO.getArrivalFlightForStation(airportCode, startDate, endDate);

		Iterator<FlightSegement> flightSegmentsIterator = flightSegments.iterator();
		while (flightSegmentsIterator.hasNext()) {

			FlightSegement flightSegement = (FlightSegement) flightSegmentsIterator.next();

			// load flight for alerting
			Flight existing = null;
			Collection<FlightSegement> oldSegments = new ArrayList<FlightSegement>();

			if ((alertDTO != null)
					&& (alertDTO.isEmailForRescheduledFlight() || alertDTO.isAlertForRescheduledFlight()
							|| alertDTO.isSendEmailsForRescheduledFlight() || alertDTO.isSendSMSForRescheduledFlight())) {

				existing = flightDAO.getFlight(flightSegement.getFlightId().intValue());
				existing = timeAdder.addLocalTimeDetailsToFlight(existing);
				oldSegments.add(flightSegement);
				oldSegments = SegmentUtil.cloneSegments(oldSegments);
			}

			int gmtOffsetInMins = airport.getGmtOffsetHours(); // is actually in minutes.
			int dstInMins = getEffectiveDSTValue(flightSegement.getEstTimeArrivalZulu(), dsts);

			Date updatedTime = CalendarUtil.getLocalDate(flightSegement.getEstTimeArrivalZulu(), gmtOffsetInMins, dstInMins);

			if (!CalendarUtil.isDateEqual(flightSegement.getEstTimeArrivalLocal(), updatedTime)) {
				flightHasBeenUpdated = true;
				flightSegement.setEstTimeArrivalLocal(updatedTime);

				flightSegIds.add(flightSegement.getFltSegId());
				flightSegementDAO.saveFlightSegement(flightSegement);
				flightIds.add(flightSegement.getFlightId());

				// prepare alert/email DTO
				if (AlertUtil.shouldRescheduleNotified(alertDTO)) {
					Collection<FlightSegement> newSegments = new ArrayList<FlightSegement>();
					newSegments.add(flightSegement);
					flightReservationAlertDTOList.add(AlertUtil.prepareFlightAlertInfo(oldSegments, newSegments, existing));
				}
			}
		}

		// departure s time updation.
		Collection<FlightSegement> flightSegments1 = flightSegementDAO.getDepartureFlightForStation(airportCode, startDate, endDate);

		Iterator<FlightSegement> flightSegmentsIterator1 = flightSegments1.iterator();
		while (flightSegmentsIterator1.hasNext()) {

			FlightSegement flightSegement1 = (FlightSegement) flightSegmentsIterator1.next();

			// load flight for alerting
			Flight existing = null;
			Collection<FlightSegement> oldSegments = new ArrayList<FlightSegement>();

			if ((alertDTO != null)
					&& (alertDTO.isEmailForRescheduledFlight() || alertDTO.isAlertForRescheduledFlight()
							|| alertDTO.isSendEmailsForRescheduledFlight() || alertDTO.isSendSMSForRescheduledFlight())) {

				existing = flightDAO.getFlight(flightSegement1.getFlightId().intValue());
				existing = timeAdder.addLocalTimeDetailsToFlight(existing);
				oldSegments.add(flightSegement1);
				oldSegments = SegmentUtil.cloneSegments(oldSegments);
			}

			int gmtOffsetInMins = airport.getGmtOffsetHours(); // is actually in minutes.
			int dstInMins = getEffectiveDSTValue(flightSegement1.getEstTimeArrivalZulu(), dsts);

			Date updatedTime = CalendarUtil.getLocalDate(flightSegement1.getEstTimeDepatureZulu(), gmtOffsetInMins, dstInMins);

			// if (!flightSegement1.getEstTimeDepatureLocal().equals(updatedTime)){
			if (!CalendarUtil.isDateEqual(flightSegement1.getEstTimeDepatureLocal(), updatedTime)) {
				flightHasBeenUpdated = true;
				flightSegement1.setEstTimeDepatureLocal(updatedTime);

				if (!(flightSegIds.contains(flightSegement1.getFltSegId()))) {
					flightSegIds.add(flightSegement1.getFltSegId());
				}
				flightSegementDAO.saveFlightSegement(flightSegement1);
				flightIds.add(flightSegement1.getFlightId());

				// prepare alert/email DTO
				if (AlertUtil.shouldRescheduleNotified(alertDTO)) {
					Collection<FlightSegement> newSegments = new ArrayList<FlightSegement>();
					newSegments.add(flightSegement1);
					flightReservationAlertDTOList.add(AlertUtil.prepareFlightAlertInfo(oldSegments, newSegments, existing));
				}
			}
		}

		if (AlertUtil.shouldRescheduleNotified(alertDTO) && flightHasBeenUpdated) {

			// return command response
			if (responce.isSuccess()) {
				responce.setResponseCode(ResponceCodes.RECALCULATE_LOCAL_TIME_SUCCESS);
				responce.addResponceParam(CommandParamNames.IDS, flightSegIds);
				responce.addResponceParam(CommandParamNames.ALLERT_TYPE, AlertTypeEnum.ALERT_DST_CHANGED);
				responce.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_EMAIL_DTO_COLLECTION, flightReservationAlertDTOList);
				responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			}
		} else {
			responce.setSuccess(false);
		}
		
		
		//Add the flight list to publish,check the parameter because there is no point of doing DB calls if this
		//feature is not enabled
		if(AppSysParamsUtil.isFlightDetailsPublishingEnabled()){
			if(!flightIds.isEmpty()){
				Collection<Flight> flightsToPublish = new ArrayList<Flight>();
				
				Flight flight = null;
				for(Integer flightId:flightIds){
					flight = flightDAO.getFlight(flightId.intValue());
					
					if(flight != null){
						flightsToPublish.add(flight);
					}
				}
				responce.addResponceParam(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
			}
		}
		
		return responce;
	}

	private int getEffectiveDSTValue(Date gmtDate, Collection<AirportDST> dstTOs) throws ModuleException {
		if (dstTOs == null || dstTOs.size() == 0) {
			return 0;
		}

		Iterator<AirportDST> iterator = dstTOs.iterator();
		while (iterator.hasNext()) {
			AirportDST airportDST = (AirportDST) iterator.next();
			Date startDate = airportDST.getDstStartDateTime();
			Date endDate = airportDST.getDstEndDateTime();
			if (CalendarUtil.isBetween(gmtDate, startDate, endDate)) {
				return airportDST.getDstAdjustTime();
			}
		}
		return 0;

	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Date startDate, Date endDate, Airport airport, Collection<AirportDST> dstTos, FlightAlertDTO alertDTO)
			throws ModuleException {

		if (startDate == null || endDate == null || airport == null || dstTos == null || alertDTO == null)// throw
																											// exception
			throw new ModuleException("airschedules.arg.invalid.null");

	}

}
