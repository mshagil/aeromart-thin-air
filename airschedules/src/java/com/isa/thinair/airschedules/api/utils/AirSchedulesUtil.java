package com.isa.thinair.airschedules.api.utils;

import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.InventoryTemplateBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.CommonAncillaryServiceBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.msgbroker.api.service.MessageBrokerServiceBD;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.scheduler.api.service.SchedulerBD;
import com.isa.thinair.scheduler.api.utils.SchedulerConstants;

public class AirSchedulesUtil {
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(InternalConstants.MODULE_NAME);
	}

	/**
	 * Returns EJB2 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupEJB2ServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getInstance().getModuleConfig(),
				AirschedulesConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	/**
	 * Returns EJB3 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getInstance().getModuleConfig(),
				AirschedulesConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	public static AuditorBD getAuditorBD() {
		return (AuditorBD) lookupEJB2ServiceBD(AuditorConstants.MODULE_NAME, AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	public static AlertingBD getAlertingBD() {
		return (AlertingBD) lookupEJB2ServiceBD(AlertingConstants.MODULE_NAME, AlertingConstants.BDKeys.ALERTING_SERVICE);
	}

	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryResBD.SERVICE_NAME);
	}

	public static GDSPublishingBD getGDSPublishingBD() {
		return (GDSPublishingBD) lookupEJB2ServiceBD(GdsservicesConstants.MODULE_NAME,
				GdsservicesConstants.BDKeys.GDSSERVICES_GDSPUBLISHING);
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static AircraftBD getAircraftBD() {
		return (AircraftBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AircraftBD.SERVICE_NAME);
	}

	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}

	public static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	public static SegmentBD getSegmentBD() {
		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	public static MealBD getMealBD() {
		return (MealBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, MealBD.SERVICE_NAME);
	}

	public static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}

	public static LogicalCabinClassBD getLogicalCabinClassBD() {
		return (LogicalCabinClassBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, LogicalCabinClassBD.SERVICE_NAME);
	}
	
	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}
	
	public static ReservationQueryBD getReservationQueryBD() {
		return (ReservationQueryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}
	
	public static BaggageBusinessDelegate getBaggageBD(){
		return (BaggageBusinessDelegate) lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);
	}

	public static ScheduleBD getScheduleBD() {
		return (ScheduleBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}

	public static SchedulerBD getSchedularBD() {
		return (SchedulerBD) lookupEJB3Service(SchedulerConstants.MODULE_NAME, SchedulerBD.SERVICE_NAME);
	}
	
	public static GDSServicesBD getGDSServicesBD() {
		return (GDSServicesBD) lookupEJB3Service(GdsservicesConstants.MODULE_NAME, GDSServicesBD.SERVICE_NAME);
	}
	
	public static Object getBean(String beanName) {
		return LookupServiceFactory.getInstance().getModule(AirschedulesConstants.MODULE_NAME).getLocalBean(beanName);
	}
	
	public static MessageBrokerServiceBD getMessageBrokerServiceBD() {
		return (MessageBrokerServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, MessageBrokerServiceBD.SERVICE_NAME);
	}

	public static CommonAncillaryServiceBD getCommonAncillaryServiceBD() {
		return (CommonAncillaryServiceBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME,
				CommonAncillaryServiceBD.SERVICE_NAME);
	} 
	
	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public static InventoryTemplateBD getInventoryTemplateBD() {
		return (InventoryTemplateBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, InventoryTemplateBD.SERVICE_NAME);
	}

}
