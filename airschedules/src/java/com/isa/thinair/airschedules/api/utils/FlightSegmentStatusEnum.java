/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

/**
 * @author Lasantha
 * 
 */
public class FlightSegmentStatusEnum {

	private String code;
	private String description;

	private FlightSegmentStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String toString() {
		return this.code;
	}

	public boolean equals(FlightSegmentStatusEnum status) {
		return status.toString().equals(this.code);
	}

	public String getDescription() {
		return description;
	}

	public String getCode() {
		return code;
	}

	public static FlightSegmentStatusEnum getBuildStatus(String code) {

		FlightSegmentStatusEnum statusEnum = null;
		code = (code == null) ? "" : code;

		if (code.equals(OPEN.toString())) {
			statusEnum = OPEN;
		} else if (code.equals(CLOSED.toString())) {
			statusEnum = CLOSED;
		}

		return statusEnum;
	}

	public static final FlightSegmentStatusEnum OPEN = new FlightSegmentStatusEnum("OPN", "Opean");
	public static final FlightSegmentStatusEnum CLOSED = new FlightSegmentStatusEnum("CLS", "Closed");
}
