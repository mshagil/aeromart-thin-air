/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:18:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

/**
 * FlightScheduleLeg is the entity class to repesent a FlightScheduleLeg model
 * 
 * @author Code Generation Tool
 * @version Ver 1.0.0
 * @hibernate.class table = "T_FLIGHT_SCHEDULE_LEG"
 */
public class FlightScheduleLeg extends Leg {

	private static final long serialVersionUID = 5845610563061477671L;

	// private fields
	private Integer id;

	private Integer scheduleId;

	/**
	 * returns the id
	 * 
	 * @return Returns the id.
	 * @hibernate.id column = "FSL_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_SCHEDULE_LEG"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * sets the id
	 * 
	 * @param fslId
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * returns the scheduleId
	 * 
	 * @return scheduleId
	 * @hibernate.property column = "SCHEDULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_SCHEDULE_LEG"
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * sets the scheduleId
	 * 
	 * @param scheduleId
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}
}
