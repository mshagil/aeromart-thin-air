package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class FlightScheduleSegmentInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String SegemntCode;

	private Collection<FlightScheduleInfoDTO> flightSchedules;

	public String getSegemntCode() {
		return SegemntCode;
	}

	public void setSegemntCode(String segemntCode) {
		SegemntCode = segemntCode;
	}

	public Collection<FlightScheduleInfoDTO> getFlightSchedules() {
		if (flightSchedules == null) {
			flightSchedules = new ArrayList<FlightScheduleInfoDTO>();
		}
		return flightSchedules;
	}
}
