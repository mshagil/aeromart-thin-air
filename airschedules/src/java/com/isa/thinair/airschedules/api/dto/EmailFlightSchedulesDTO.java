/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

/**
 * data transfer object to hold the flight schedule details
 * 
 * @author Duminda G
 */
public class EmailFlightSchedulesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8831842676413611972L;
	private int flight_id;
	private int flt_seg_id;
	private String flight_number;
	private String status;
	private String departure_airport;
	private String arrival_airport;
	private String est_time_departure_zulu;
	private String est_time_arrival_zulu;
	private String model_number;
	private String d_city_code;
	private String a_city_code;

	/**
	 * @return Returns the a_city_code.
	 */
	public String getA_city_code() {
		return a_city_code;
	}

	/**
	 * @param a_city_code
	 *            The a_city_code to set.
	 */
	public void setA_city_code(String a_city_code) {
		this.a_city_code = a_city_code;
	}

	/**
	 * @return Returns the d_city_code.
	 */
	public String getD_city_code() {
		return d_city_code;
	}

	/**
	 * @param d_city_code
	 *            The d_city_code to set.
	 */
	public void setD_city_code(String d_city_code) {
		this.d_city_code = d_city_code;
	}

	/**
	 * @return Returns the departure_airport.
	 */
	public String getDeparture_airport() {
		return departure_airport;
	}

	/**
	 * @param departure_airport
	 *            The departure_airport to set.
	 */
	public void setDeparture_airport(String departure_airport) {
		this.departure_airport = departure_airport;
	}

	/**
	 * @return Returns the est_time_arrival_zulu.
	 */
	public String getEst_time_arrival_zulu() {
		return est_time_arrival_zulu;
	}

	/**
	 * @param est_time_arrival_zulu
	 *            The est_time_arrival_zulu to set.
	 */
	public void setEst_time_arrival_zulu(String est_time_arrival_zulu) {
		this.est_time_arrival_zulu = est_time_arrival_zulu;
	}

	/**
	 * @return Returns the est_time_departure_zulu.
	 */
	public String getEst_time_departure_zulu() {
		return est_time_departure_zulu;
	}

	/**
	 * @param est_time_departure_zulu
	 *            The est_time_departure_zulu to set.
	 */
	public void setEst_time_departure_zulu(String est_time_departure_zulu) {
		this.est_time_departure_zulu = est_time_departure_zulu;
	}

	/**
	 * @return Returns the flight_id.
	 */
	public int getFlight_id() {
		return flight_id;
	}

	/**
	 * @param flight_id
	 *            The flight_id to set.
	 */
	public void setFlight_id(int flight_id) {
		this.flight_id = flight_id;
	}

	/**
	 * @return Returns the flight_number.
	 */
	public String getFlight_number() {
		return flight_number;
	}

	/**
	 * @param flight_number
	 *            The flight_number to set.
	 */
	public void setFlight_number(String flight_number) {
		this.flight_number = flight_number;
	}

	/**
	 * @return Returns the model_number.
	 */
	public String getModel_number() {
		return model_number;
	}

	/**
	 * @param model_number
	 *            The model_number to set.
	 */
	public void setModel_number(String model_number) {
		this.model_number = model_number;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the flt_seg_id.
	 */
	public int getFlt_seg_id() {
		return flt_seg_id;
	}

	/**
	 * @param flt_seg_id
	 *            The flt_seg_id to set.
	 */
	public void setFlt_seg_id(int flt_seg_id) {
		this.flt_seg_id = flt_seg_id;
	}

	/**
	 * @return Returns the arrival_airport.
	 */
	public String getArrival_airport() {
		return arrival_airport;
	}

	/**
	 * @param arrival_airport
	 *            The arrival_airport to set.
	 */
	public void setArrival_airport(String arrival_airport) {
		this.arrival_airport = arrival_airport;
	}

}
