package com.isa.thinair.airschedules.api.model;

import java.io.Serializable;

/**
 * 
 * @author manoj
 * @hibernate.class table = "T_CS_FLIGHT"
 */
public class CodeShareMCFlight implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer csFlightId;
	
	private Integer flightId;

	private String csMCFlightNumber;
	
	private String csMCCarrierCode;
	
	/**
	 * @return Returns the csFlightId.
	 * 
	 * @hibernate.id column = "CS_FLIGHT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CS_FLIGHT"
	 */
	public Integer getCsFlightId() {
		return csFlightId;
	}

	/**
	 * returns the flightId
	 * 
	 * @return flightId
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * returns the csMCFlightNumber
	 * 
	 * @return csMCFlightNumber
	 * @hibernate.property column = "CS_FLIGHT_NUMBER"
	 */
	public String getCsMCFlightNumber() {
		return csMCFlightNumber;
	}

	/**
	 * returns the csMCCarrierCode
	 * 
	 * @return csMCCarrierCode
	 * @hibernate.property column = "CS_CARRIER_CODE"
	 */
	public String getCsMCCarrierCode() {
		return csMCCarrierCode;
	}
	
	public void setCsFlightId(Integer csFlightId) {
		this.csFlightId = csFlightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public void setCsMCFlightNumber(String csMCFlightNumber) {
		if (csMCFlightNumber != null) {
			this.csMCFlightNumber = csMCFlightNumber.toUpperCase();
		}
	}

	public void setCsMCCarrierCode(String csMCCarrierCode) {
		this.csMCCarrierCode = csMCCarrierCode;
	}

}
