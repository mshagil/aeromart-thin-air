package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class FlightInformationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 95869129828175421L;
	private Collection<Collection<FlightScheduleInfoDTO>> flightInfoCol;
	private Collection<Collection<FlightScheduleInfoDTO>> flightInfoReturnCol;

	/**
	 * @return the flightInfoReturnCol
	 */
	public Collection getFlightInfoReturnCol() {
		if (flightInfoReturnCol == null)
			flightInfoReturnCol = new ArrayList<Collection<FlightScheduleInfoDTO>>();
		return flightInfoReturnCol;
	}

	/**
	 * @param flightInfoReturnCol
	 *            the flightInfoReturnCol to set
	 */
	public void setFlightInfoReturnCol(Collection<Collection<FlightScheduleInfoDTO>> flightInfoReturnCol) {
		this.flightInfoReturnCol = flightInfoReturnCol;
	}

	/**
	 * @return the flightInfoList
	 */
	public Collection getFlightInfoList() {
		if (flightInfoCol == null)
			flightInfoCol = new ArrayList<Collection<FlightScheduleInfoDTO>>();
		return flightInfoCol;
	}

	/**
	 * @param flightInfoList
	 *            the flightInfoList to set
	 */
	public void setFlightInfoList(Collection flightInfoCol) {
		this.flightInfoCol = flightInfoCol;
	}

	public FlightInformationDTO() {
	}

}
