package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class AvailableFlightSearchCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date minDate;
	private Date maxDate;
	private Date maxDateZulu;
	private String origin;
	private String destination;
	private int adults;
	private int infants;
	private int childs;
	private String cabinClass;
	private String logicalCabinClass;
	private int availabilityRestriction;
	private boolean includeOnlyMarketingCarrierSegs;
	private boolean isOnlyInterline;
	private boolean isInterline;
	private Collection<String> externalBookingClasses;
	private Date arrMinDate;
	private Date arrMaxDate;
	private boolean isInboundFlight;
	private long cutOffTimeForInternational;
	private long cutOffTimeForDomestic;
	private boolean allowFlightSearchAfterCutOffTime;
	private boolean multiCitySearch;
	private boolean allowDoOverbookAfterCutOffTime;
	private boolean allowDoOverbookBeforeCutOffTime;
	private String appIndicator;
	private boolean isModifyBooking;
	private int channelCode;
	private boolean isSourceFlightInCutOffTime;
	private boolean isOpenReturn;
	private String agentCode;

	private int ondSequence;

	public Date getMinDate() {
		return minDate;
	}

	private void setMinDate(Date minDate) {
		this.minDate = minDate;
	}

	public Date getMaxDate() {
		return maxDate;
	}

	private void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	public String getOrigin() {
		return origin;
	}

	private void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	private void setDestination(String destination) {
		this.destination = destination;
	}

	public int getAdults() {
		return adults;
	}

	public void setAdults(int adults) {
		this.adults = adults;
	}

	public int getChilds() {
		return childs;
	}

	public void setChilds(int childs) {
		this.childs = childs;
	}

	public int getInfants() {
		return infants;
	}

	public void setInfants(int infants) {
		this.infants = infants;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	private void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	private void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public int getAvailabilityRestriction() {
		return availabilityRestriction;
	}

	public void setAvailabilityRestriction(int availabilityRestriction) {
		this.availabilityRestriction = availabilityRestriction;
	}

	public boolean isIncludeOnlyMarketingCarrierSegs() {
		return includeOnlyMarketingCarrierSegs;
	}

	public void setIncludeOnlyMarketingCarrierSegs(boolean includeOnlyMarketingCarrierSegs) {
		this.includeOnlyMarketingCarrierSegs = includeOnlyMarketingCarrierSegs;
	}

	public boolean isOnlyInterline() {
		return isOnlyInterline;
	}

	public void setOnlyInterline(boolean isOnlyInterline) {
		this.isOnlyInterline = isOnlyInterline;
	}

	public boolean isInterline() {
		return isInterline;
	}

	public void setInterline(boolean isInterline) {
		this.isInterline = isInterline;
	}

	public Collection<String> getExternalBookingClasses() {
		return externalBookingClasses;
	}

	public void setExternalBookingClasses(Collection<String> externalBookingClasses) {
		this.externalBookingClasses = externalBookingClasses;
	}

	public Date getArrMinDate() {
		return arrMinDate;
	}

	private void setArrMinDate(Date arrMinDate) {
		this.arrMinDate = arrMinDate;
	}

	public Date getArrMaxDate() {
		return arrMaxDate;
	}

	private void setArrMaxDate(Date arrMaxDate) {
		this.arrMaxDate = arrMaxDate;
	}

	public boolean isInboundFlight() {
		return isInboundFlight;
	}

	public void setInboundFlight(boolean isInboundFlight) {
		this.isInboundFlight = isInboundFlight;
	}

	public long getCutOffTimeForInternational() {
		return cutOffTimeForInternational;
	}

	public void setCutOffTimeForInternational(long cutOffTimeForInternational) {
		this.cutOffTimeForInternational = cutOffTimeForInternational;
	}

	public long getCutOffTimeForDomestic() {
		return cutOffTimeForDomestic;
	}

	public void setCutOffTimeForDomestic(long cutOffTimeForDomestic) {
		this.cutOffTimeForDomestic = cutOffTimeForDomestic;
	}

	public void setOriginDestination(String fromAirport, String toAirport) {
		setOrigin(fromAirport);
		setDestination(toAirport);
	}

	public void setDates(Date depatureDateTimeStart, Date depatureDateTimeEnd, Date arrivalDateTimeStart,
			Date arrivalDateTimeEnd, Date maxDateZulu) {
		setMinDate(depatureDateTimeStart);
		setMaxDate(depatureDateTimeEnd);
		setArrMinDate(arrivalDateTimeStart);
		setArrMaxDate(arrivalDateTimeEnd);
		setMaxDateZulu(maxDateZulu);
	}

	public void setCabins(String cabinClassCode, String logicalCabinClassCode) {
		setCabinClass(cabinClassCode);
		setLogicalCabinClass(logicalCabinClassCode);
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public boolean isAllowFlightSearchAfterCutOffTime() {
		return allowFlightSearchAfterCutOffTime;
	}

	public void setAllowFlightSearchAfterCutOffTime(boolean allowFlightSearchAfterCutOffTime) {
		this.allowFlightSearchAfterCutOffTime = allowFlightSearchAfterCutOffTime;
	}

	public boolean isMultiCitySearch() {
		return multiCitySearch;
	}

	public void setMultiCitySearch(boolean multiCitySearch) {
		this.multiCitySearch = multiCitySearch;
	}

	public Date getMaxDateZulu() {
		return maxDateZulu;
	}

	public void setMaxDateZulu(Date maxDateZulu) {
		this.maxDateZulu = maxDateZulu;
	}

	public boolean isAllowDoOverbookAfterCutOffTime() {
		return allowDoOverbookAfterCutOffTime;
	}

	public void setAllowDoOverbookAfterCutOffTime(boolean allowDoOverbookAfterCutOffTime) {
		this.allowDoOverbookAfterCutOffTime = allowDoOverbookAfterCutOffTime;
	}

	public boolean isAllowDoOverbookBeforeCutOffTime() {
		return allowDoOverbookBeforeCutOffTime;
	}

	public void setAllowDoOverbookBeforeCutOffTime(boolean allowDoOverbookBeforeCutOffTime) {
		this.allowDoOverbookBeforeCutOffTime = allowDoOverbookBeforeCutOffTime;
	}

	public String getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(String appIndicator) {
		this.appIndicator = appIndicator;
	}

	public boolean isModifyBooking() {
		return isModifyBooking;
	}

	public void setModifyBooking(boolean isModifyBooking) {
		this.isModifyBooking = isModifyBooking;
	}

	public int getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}

	public boolean isSourceFlightInCutOffTime() {
		return isSourceFlightInCutOffTime;
	}

	public void setSourceFlightInCutOffTime(boolean isSourceFlightInCutOffTime) {
		this.isSourceFlightInCutOffTime = isSourceFlightInCutOffTime;
	}

	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	public void setOpenReturn(boolean isOpenReturn) {
		this.isOpenReturn = isOpenReturn;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	
	
}
