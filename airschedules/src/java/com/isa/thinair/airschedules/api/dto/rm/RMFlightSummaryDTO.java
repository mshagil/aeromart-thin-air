/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto.rm;

import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airschedules.api.dto.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;

/**
 * @author byorn
 * 
 */
public class RMFlightSummaryDTO extends FlightSummaryDTO {

	/** hold the connected segements **/
	private Collection<FlightSegmentDTO> connectedSegments;

	/** holds the overlappilng flight id **/
	// if no overlapping flight id is found it is zero
	private int overlappling_flightId;

	public RMFlightSummaryDTO(int flightId, Date depatureDate, String flightNumber) {
		super(flightId, depatureDate, flightNumber);
	}

	/**
	 * @return the connectedSegments
	 */
	public Collection<FlightSegmentDTO> getConnectedSegments() {
		return connectedSegments;
	}

	/**
	 * @param connectedSegments
	 *            the connectedSegments to set
	 */
	public void setConnectedSegments(Collection<FlightSegmentDTO> connectedSegments) {
		this.connectedSegments = connectedSegments;
	}

	/**
	 * @return the overlappling_flightId
	 */
	public int getOverlappling_flightId() {
		return overlappling_flightId;
	}

	/**
	 * @param overlappling_flightId
	 *            the overlappling_flightId to set
	 */
	public void setOverlappling_flightId(int overlappling_flightId) {
		this.overlappling_flightId = overlappling_flightId;
	}

}
