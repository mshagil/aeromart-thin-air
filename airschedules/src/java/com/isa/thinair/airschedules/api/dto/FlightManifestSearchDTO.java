package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airschedules.api.criteria.FlightFlightSegmentSearchCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;

public class FlightManifestSearchDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5279217301682001701L;

	private List<ModuleCriterion> criteria;

	private int startIndex;

	private int pageSize;

	private boolean isDatesInLocal;

	private FlightFlightSegmentSearchCriteria flightCriteria;

	private boolean isFlightManifest;

	private boolean isInterline;

	private String carrierCode;
		
	private boolean dataFromReporting;
	
	List<String> agentApplicableOnds;

	public List<ModuleCriterion> getCriteria() {
		return criteria;
	}

	public void setCriteria(List<ModuleCriterion> criteria) {
		this.criteria = criteria;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isDatesInLocal() {
		return isDatesInLocal;
	}

	public void setDatesInLocal(boolean isDatesInLocal) {
		this.isDatesInLocal = isDatesInLocal;
	}

	public FlightFlightSegmentSearchCriteria getFlightCriteria() {
		return flightCriteria;
	}

	public void setFlightCriteria(FlightFlightSegmentSearchCriteria flightCriteria) {
		this.flightCriteria = flightCriteria;
	}

	public boolean isFlightManifest() {
		return isFlightManifest;
	}

	public void setFlightManifest(boolean isFlightManifest) {
		this.isFlightManifest = isFlightManifest;
	}

	public boolean isInterline() {
		return isInterline;
	}

	public void setInterline(boolean isInterline) {
		this.isInterline = isInterline;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public boolean isDataFromReporting() {
		return dataFromReporting;
	}

	public void setDataFromReporting(boolean dataFromReporting) {
		this.dataFromReporting = dataFromReporting;
	}

	public List<String> getAgentApplicableOnds() {
		return agentApplicableOnds;
	}

	public void setAgentApplicableOnds(List<String> agentApplicableOnds) {
		this.agentApplicableOnds = agentApplicableOnds;
	}
	
	
}
