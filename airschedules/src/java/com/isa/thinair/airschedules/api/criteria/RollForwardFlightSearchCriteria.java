package com.isa.thinair.airschedules.api.criteria;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public class RollForwardFlightSearchCriteria implements Serializable {
	private static final long serialVersionUID = 1L;

	private int flightSegId;
	private int fareId;
	private int fareRuleId;
	private String segmentCode;
	private int segmentSeq;
	private String cabinClassCode;
	private String bookingClassCode;
	private String startDate;
	private String endDate;
	private String returnFlag;
	private boolean openRerurn;
	private String agentCode;

	public boolean isOpenRerurn() {
		return openRerurn;
	}

	public void setOpenRerurn(boolean openRerurn) {
		this.openRerurn = openRerurn;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	public int getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getSegmentSeq() {
		return segmentSeq;
	}

	public void setSegmentSeq(int segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}	
}
