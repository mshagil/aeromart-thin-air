package com.isa.thinair.airschedules.api.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class SegmentInvAvailability {
	private FlightSegmentTO flightSegment;
	private List<LogicalCabinAvailability> availablities;
	private Map<String, LogicalCabinAvailability> availabilityByLogicalCabin = null;

	public FlightSegmentTO getFlightSegment() {
		return flightSegment;
	}

	public void setFlightSegment(FlightSegmentTO flightSegment) {
		this.flightSegment = flightSegment;
	}

	public List<LogicalCabinAvailability> getAvailablities() {
		return availablities;
	}

	public void setAvailablities(List<LogicalCabinAvailability> availablities) {
		this.availablities = availablities;
	}

	public LogicalCabinAvailability getAvailabilityFor(String logicalCabinClass) {
		if (logicalCabinClass != null) {
			if (availabilityByLogicalCabin == null) {
				availabilityByLogicalCabin = new HashMap<String, LogicalCabinAvailability>();
				for (LogicalCabinAvailability lccAvail : availablities) {
					availabilityByLogicalCabin.put(lccAvail.getLogicalCabinClass(), lccAvail);
				}
			}
			return availabilityByLogicalCabin.get(logicalCabinClass);
		}
		return null;
	}
}
