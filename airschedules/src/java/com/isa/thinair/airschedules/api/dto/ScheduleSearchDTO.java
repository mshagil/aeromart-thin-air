/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Lasantha Pambagoda
 */
public class ScheduleSearchDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4572831973576536094L;

	private Date startDate;

	private Date endDate;

	private String scheduleStatus;

	private String buildStatus;

	private Integer opType;

	private String flightNumber;

	private String fromAirport;

	private String toAirport;

	private List<String> flightNumers;

	private String flightType;
	
	private String csOCCarrierCode;
	
	private String csOCFlightNumber;
	
	private boolean isLocalTime;
	
	private List<String> statusFilter;

	private boolean isAdHocFlightsOnly;

	private boolean enableWildcard;

	/**
	 * @return Returns the buildStatus.
	 */
	public String getBuildStatus() {
		return buildStatus;
	}

	/**
	 * @param buildStatus
	 *            The buildStatus to set.
	 */
	public void setBuildStatus(String buildStatus) {
		this.buildStatus = buildStatus;
	}

	/**
	 * @return Returns the endDate.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            The endDate to set.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the fromAirport.
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param fromAirport
	 *            The fromAirport to set.
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the opType.
	 */
	public Integer getOpType() {
		return opType;
	}

	/**
	 * @param opType
	 *            The opType to set.
	 */
	public void setOpType(Integer opType) {
		this.opType = opType;
	}

	/**
	 * @return Returns the scheduleStatus.
	 */
	public String getScheduleStatus() {
		return scheduleStatus;
	}

	/**
	 * @param scheduleStatus
	 *            The scheduleStatus to set.
	 */
	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}

	/**
	 * @return Returns the startDate.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return Returns the toAirport.
	 */
	public String getToAirport() {
		return toAirport;
	}

	/**
	 * @param toAirport
	 *            The toAirport to set.
	 */
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public List<String> getFlightNumers() {
		if (flightNumers == null) {
			flightNumers = new ArrayList<String>();
		}
		return flightNumers;
	}

	public void setFlightNumers(List<String> flightNumers) {
		this.flightNumers = flightNumers;
	}

	/**
	 * @return the flightType
	 */
	public String getFlightType() {
		return flightType;
	}

	/**
	 * @param flightType
	 *            the flightType to set
	 */
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getCsOCCarrierCode() {
		return csOCCarrierCode;
	}

	public void setCsOCCarrierCode(String csOCCarrierCode) {
		this.csOCCarrierCode = csOCCarrierCode;
	}

	public String getCsOCFlightNumber() {
		return csOCFlightNumber;
	}

	public void setCsOCFlightNumber(String csOCFlightNumber) {
		this.csOCFlightNumber = csOCFlightNumber;
	}

	public boolean isLocalTime() {
		return isLocalTime;
	}

	public void setLocalTime(boolean isLocalTime) {
		this.isLocalTime = isLocalTime;
	}

	public List<String> getStatusFilter() {
		if (statusFilter == null) {
			statusFilter = new ArrayList<String>();
		}
		return statusFilter;
	}

	public void setStatusFilter(List<String> statusFilter) {
		this.statusFilter = statusFilter;
	}

	public boolean isAdHocFlightsOnly() {
		return isAdHocFlightsOnly;
	}

	public void setAdHocFlightsOnly(boolean isAdHocFlightsOnly) {
		this.isAdHocFlightsOnly = isAdHocFlightsOnly;
	}

	public boolean isEnableWildcard() {
		return enableWildcard;
	}

	public void setEnableWildcard(boolean enableWildcard) {
		this.enableWildcard = enableWildcard;
	}
}
