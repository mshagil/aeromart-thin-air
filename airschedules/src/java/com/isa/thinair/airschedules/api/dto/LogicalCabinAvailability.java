package com.isa.thinair.airschedules.api.dto;

public class LogicalCabinAvailability {
	private String logicalCabinClass;
	private int avaialbleAdultSeats;
	private int availableInfantSeats;
	private int availableFixedAdultSeats;
	private double loadFactor;

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public int getAvaialbleAdultSeats() {
		return avaialbleAdultSeats;
	}

	public void setAvaialbleAdultSeats(int avaialbleAdultSeats) {
		this.avaialbleAdultSeats = avaialbleAdultSeats;
	}

	public int getAvailableInfantSeats() {
		return availableInfantSeats;
	}

	public void setAvailableInfantSeats(int availableInfantSeats) {
		this.availableInfantSeats = availableInfantSeats;
	}

	public int getAvailableFixedAdultSeats() {
		return availableFixedAdultSeats;
	}

	public void setAvailableFixedAdultSeats(int availableFixedAdultSeats) {
		this.availableFixedAdultSeats = availableFixedAdultSeats;
	}

	public void setLoadFactor(double loadFactor) {
		this.loadFactor = loadFactor;
	}

	public double getLoadFactor() {
		return this.loadFactor;
	}

}
