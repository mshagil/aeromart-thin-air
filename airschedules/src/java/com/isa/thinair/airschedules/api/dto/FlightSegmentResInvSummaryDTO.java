/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

/**
 *
 */
public class FlightSegmentResInvSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7495323725202397149L;

	private String segmentCode;

	private int segmentId;

	private int soldSeatsAdult;

	private int soldSeatsInfant;

	private int confirmedPnrs;

	private int onholdSeatsAdult;

	private int onholdSeatsInfant;

	private int onholdPnrs;

	public FlightSegmentResInvSummaryDTO() {

	}

	public FlightSegmentResInvSummaryDTO(String segmentCode, int segmentId, int soldSeatsAdult, int soldSeatsInfant,
			int confirmedPnrs, int onholdSeatsAdult, int onholdSeatsInfant, int onholdPnrs) {
		this.segmentCode = segmentCode;
		this.segmentId = segmentId;
		this.soldSeatsAdult = soldSeatsAdult;
		this.soldSeatsInfant = soldSeatsInfant;
		this.confirmedPnrs = confirmedPnrs;
		this.onholdSeatsAdult = onholdSeatsAdult;
		this.onholdSeatsInfant = onholdSeatsInfant;
		this.onholdPnrs = onholdPnrs;

	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(int segmentId) {
		this.segmentId = segmentId;
	}

	public int getSoldSeatsAdult() {
		return soldSeatsAdult;
	}

	public void setSoldSeatsAdult(int soldSeatsAdult) {
		this.soldSeatsAdult = soldSeatsAdult;
	}

	public int getSoldSeatsInfant() {
		return soldSeatsInfant;
	}

	public void setSoldSeatsInfant(int soldSeatsInfant) {
		this.soldSeatsInfant = soldSeatsInfant;
	}

	public int getConfirmedPnrs() {
		return confirmedPnrs;
	}

	public void setConfirmedPnrs(int confirmedPnrs) {
		this.confirmedPnrs = confirmedPnrs;
	}

	public int getOnholdSeatsAdult() {
		return onholdSeatsAdult;
	}

	public void setOnholdSeatsAdult(int onholdSeatsAdult) {
		this.onholdSeatsAdult = onholdSeatsAdult;
	}

	public int getOnholdSeatsInfant() {
		return onholdSeatsInfant;
	}

	public void setOnholdSeatsInfant(int onholdSeatsInfant) {
		this.onholdSeatsInfant = onholdSeatsInfant;
	}

	public int getOnholdPnrs() {
		return onholdPnrs;
	}

	public void setOnholdPnrs(int onholdPnrs) {
		this.onholdPnrs = onholdPnrs;
	}

}
