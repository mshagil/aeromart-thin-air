/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

/**
 * @author M.Rikaz
 */
public class AnciDefaultTemplStatusEnum {

	private String code;
	private String description;

	private AnciDefaultTemplStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String toString() {
		return this.code;
	}

	public boolean equals(AnciDefaultTemplStatusEnum status) {
		return status.toString().equals(this.code);
	}

	public String getDescription() {
		return description;
	}

	public String getCode() {
		return code;
	}

	public static final AnciDefaultTemplStatusEnum SCHEDULED = new AnciDefaultTemplStatusEnum("SCH", "Scheduled");
	public static final AnciDefaultTemplStatusEnum IN_PROGRESS = new AnciDefaultTemplStatusEnum("INP", "In progress");
	public static final AnciDefaultTemplStatusEnum CREATED = new AnciDefaultTemplStatusEnum("CRE", "Successfully created");
	public static final AnciDefaultTemplStatusEnum CREATED_W_ERRORS = new AnciDefaultTemplStatusEnum("CRE_ERROR",
			"Created with errors");

}
