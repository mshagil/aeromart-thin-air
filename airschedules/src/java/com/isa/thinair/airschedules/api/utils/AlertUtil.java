package com.isa.thinair.airschedules.api.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAlertDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.ExternalConstants.ScheduleTimeFormat;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * utility class for alerts
 * 
 * @author Chandana Kithalagama
 */
public class AlertUtil {

	/**
	 * 
	 * @param oldFlightAlertInfoDTO
	 * @param newFlightAlertInfoDTO
	 * @param transportMethod
	 *            TODO
	 * @return
	 */
	public static HashMap<AlertTemplateEnum, HashMap<String, Object>> prepareFlightSegmentAlertDto(
			FlightAlertInfoDTO oldFlightAlertInfoDTO, FlightAlertInfoDTO newFlightAlertInfoDTO) {

		HashMap<AlertTemplateEnum, HashMap<String, Object>> alertDetails = new HashMap<AlertTemplateEnum, HashMap<String, Object>>();
		HashMap<String, Object> alertParams = new HashMap<String, Object>();

		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_FLIGHT_NO, oldFlightAlertInfoDTO.getFlightNumber());
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_DEPARTURE_DATE, oldFlightAlertInfoDTO.getDepartureDate());
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_LEG_DETAILS, oldFlightAlertInfoDTO.getLegDetails());
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_ROUTE, oldFlightAlertInfoDTO.getRoute());
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_SEGMENT_DETAILS, oldFlightAlertInfoDTO.getSegmentDetails());

		// flight updation
		if (newFlightAlertInfoDTO != null) {

			alertParams.put(AlertTemplateEnum.TemplateParams.NEW_FLIGHT_NO, newFlightAlertInfoDTO.getFlightNumber());
			alertParams.put(AlertTemplateEnum.TemplateParams.NEW_DEPARTURE_DATE, newFlightAlertInfoDTO.getDepartureDate());
			alertParams.put(AlertTemplateEnum.TemplateParams.NEW_LEG_DETAILS, newFlightAlertInfoDTO.getLegDetails());
			alertParams.put(AlertTemplateEnum.TemplateParams.NEW_ROUTE, newFlightAlertInfoDTO.getRoute());
			alertParams.put(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_DETAILS, newFlightAlertInfoDTO.getSegmentDetails());

			if (newFlightAlertInfoDTO.getFlightId().compareTo(oldFlightAlertInfoDTO.getFlightId()) != 0) {
				alertDetails.put(AlertTemplateEnum.TEMPLATE_TRANSFER_PAX, alertParams);
			} else { // If the same flight

				if (!oldFlightAlertInfoDTO.getFlightNumber().equals(newFlightAlertInfoDTO.getFlightNumber()))
					alertDetails.put(AlertTemplateEnum.TEMPLATE_CHANGE_FLIGHT_NUMBER, alertParams);

				if ((newFlightAlertInfoDTO.getDepartureDate().getTime() != oldFlightAlertInfoDTO.getDepartureDate().getTime())
						|| oldFlightAlertInfoDTO.getLegDetails() != null
						&& !oldFlightAlertInfoDTO.getLegDetails().equals(newFlightAlertInfoDTO.getLegDetails())) {
					if (CalendarUtil.getTimeDifferenceInHours(oldFlightAlertInfoDTO.getDepartureDate(),
							newFlightAlertInfoDTO.getDepartureDate()) > 24)
						alertDetails.put(AlertTemplateEnum.TEMPLATE_CHANGE_DATE_TIME, alertParams);
					else
						alertDetails.put(AlertTemplateEnum.TEMPLATE_FLIGHT_DELAY, alertParams);
				}

			}

			if (oldFlightAlertInfoDTO.getRoute() != null && newFlightAlertInfoDTO.getRoute() != null) {
				if (!oldFlightAlertInfoDTO.getRoute().equals(newFlightAlertInfoDTO.getRoute()))
					alertDetails.put(AlertTemplateEnum.TEMPLATE_CHANGE_ROUTE, alertParams);
			}

			if (oldFlightAlertInfoDTO.getSegmentDetails() != null
					&& !oldFlightAlertInfoDTO.getSegmentDetails().equals(newFlightAlertInfoDTO.getSegmentDetails()))
				alertDetails.put(AlertTemplateEnum.DAY_LIGHT_SAVING_TIME_CHANGED, alertParams);
			// flight cancellation
		} else {
			if (oldFlightAlertInfoDTO.getAction() != null) {
				if (oldFlightAlertInfoDTO.getAction().equals(AlertTemplateEnum.TemplateParams.RE_PROTECT)) {
					alertDetails.put(AlertTemplateEnum.TEMPLATE_FLIGHT_REPROTECT_MSG, alertParams);
				}

			} else {
				alertDetails.put(AlertTemplateEnum.TEMPLATE_CANCELLATION, alertParams);
			}
		}

		return alertDetails;
	}

	/**
	 * public method to create flight segment alert details
	 * 
	 * @param flightSegments
	 * @param oldSegments
	 * @param newSegments
	 * @return HashMap of flightsegmentid as key and another HashMap of segment details (segment code, old dep time, old
	 *         arrival time new dep time and new arrival time) as valus
	 */
	public static HashMap<Integer,HashMap<String, Object>> getFlightSegmentAlertDetails(Collection<FlightSegement> flightSegments, 
			Collection<FlightSegement> oldSegments, Collection<FlightSegement> newSegments) {

		HashMap<Integer,HashMap<String, Object>> flightSegmentMap = new HashMap<Integer,HashMap<String, Object>>();

		Iterator<FlightSegement> itFlightSegments = flightSegments.iterator();

		while (itFlightSegments.hasNext()) {
			FlightSegement flightSegment = (FlightSegement) itFlightSegments.next();

			HashMap<String, Object> flightSegmentAlertDetailsMap = new HashMap<String, Object>();
			flightSegmentAlertDetailsMap.put(AlertTemplateEnum.TemplateParams.SEGMENT, flightSegment.getSegmentCode());

			if (oldSegments != null) {
				Iterator<FlightSegement> itOldSegments = oldSegments.iterator();
				while (itOldSegments.hasNext()) {
					FlightSegement oldSegment = (FlightSegement) itOldSegments.next();
					if (oldSegment.getFltSegId().intValue() == flightSegment.getFltSegId().intValue()) {
						flightSegmentAlertDetailsMap.put(AlertTemplateEnum.TemplateParams.OLD_SEGMENT_DEPARTURE,
								oldSegment.getEstTimeDepatureLocal());
						flightSegmentAlertDetailsMap.put(AlertTemplateEnum.TemplateParams.OLD_SEGMENT_ARRIVAL,
								oldSegment.getEstTimeArrivalLocal());
						break;
					}
				}
			}

			if (newSegments != null) {
				Iterator<FlightSegement> itNewSegments = newSegments.iterator();
				while (itNewSegments.hasNext()) {
					FlightSegement newSegment = (FlightSegement) itNewSegments.next();
					if (newSegment.getFltSegId().intValue() == flightSegment.getFltSegId().intValue()) {
						flightSegmentAlertDetailsMap.put(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_DEPARTURE,
								newSegment.getEstTimeDepatureLocal());
						flightSegmentAlertDetailsMap.put(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_ARRIVAL,
								newSegment.getEstTimeArrivalLocal());
						break;
					}
				}
			}

			flightSegmentMap.put(flightSegment.getFltSegId(), flightSegmentAlertDetailsMap);
		}

		return flightSegmentMap;
	}

	/**
	 * 
	 * @param oldFlightAlertInfoDTO
	 * @param newFlightAlertInfoDTO
	 * @return
	 */
	public static List<FlightSegement> getAffectedFlightSegments(FlightAlertInfoDTO oldFlightAlertInfoDTO,
			FlightAlertInfoDTO newFlightAlertInfoDTO) {

		List<FlightSegement> affectedsegments = new ArrayList<FlightSegement>();
		boolean onlyLegDetailsChanged = true;

		// flight updation
		if (newFlightAlertInfoDTO != null) {

			if (!oldFlightAlertInfoDTO.getFlightNumber().equals(newFlightAlertInfoDTO.getFlightNumber()))
				onlyLegDetailsChanged = false;

			if (newFlightAlertInfoDTO.getDepartureDate().getTime() != oldFlightAlertInfoDTO.getDepartureDate().getTime())
				onlyLegDetailsChanged = false;

			if (oldFlightAlertInfoDTO.getRoute() != null
					&& !oldFlightAlertInfoDTO.getRoute().equals(newFlightAlertInfoDTO.getRoute()))
				onlyLegDetailsChanged = false;

			if (oldFlightAlertInfoDTO.getSegmentDetails() != null
					&& !oldFlightAlertInfoDTO.getSegmentDetails().equals(newFlightAlertInfoDTO.getSegmentDetails()))
				onlyLegDetailsChanged = false;

			if (oldFlightAlertInfoDTO.getSegments() != null && !oldFlightAlertInfoDTO.getSegments().isEmpty()
					&& newFlightAlertInfoDTO.getSegments() != null && !newFlightAlertInfoDTO.getSegments().isEmpty()) {
				if (!oldFlightAlertInfoDTO.getLegDetails().equals(newFlightAlertInfoDTO.getLegDetails()) && onlyLegDetailsChanged)
					affectedsegments.addAll(SegmentUtil.getAffectedFlightSegments(oldFlightAlertInfoDTO.getSegments(),
							newFlightAlertInfoDTO.getSegments()));
			}
			
			
		}

		if ((!onlyLegDetailsChanged || newFlightAlertInfoDTO == null) && oldFlightAlertInfoDTO.getSegments() != null
				&& !oldFlightAlertInfoDTO.getSegments().isEmpty()) {
			affectedsegments.addAll(oldFlightAlertInfoDTO.getSegments());
		}			

		return affectedsegments;
	}

	/**
	 * 
	 * @param flight
	 * @return
	 */
	public static FlightAlertInfoDTO copyFlightDetailsToFlightAlertInfoDTO(Flight flight) {

		String scheduleTimeFormat = ScheduleTimeFormat.Local;
		FlightAlertInfoDTO flightAlertInfoDTO = new FlightAlertInfoDTO();

		flightAlertInfoDTO.setFlightId(new Integer(flight.getFlightId().intValue()));
		flightAlertInfoDTO.setFlightNumber(new String(flight.getFlightNumber()));
		if (scheduleTimeFormat.equals(ScheduleTimeFormat.Local))
			flightAlertInfoDTO.setDepartureDate(new Date(flight.getDepartureDateLocal().getTime()));
		else
			flightAlertInfoDTO.setDepartureDate(new Date(flight.getDepartureDate().getTime()));

		flightAlertInfoDTO.setDestinationAptCode(new String(flight.getDestinationAptCode()));
		flightAlertInfoDTO.setOriginAptCode(new String(flight.getOriginAptCode()));
		flightAlertInfoDTO.setModelNumber(new String(flight.getModelNumber()));
		if (flight.getScheduleId() != null)
			flightAlertInfoDTO.setScheduleId(new Integer(flight.getScheduleId().intValue()));
		flightAlertInfoDTO.setStatus(new String(flight.getStatus()));
		flightAlertInfoDTO.setLegDetails(new String(LegUtil.getLegInfo(flight.getFlightLegs(), scheduleTimeFormat)));
		// flightAlertInfoDTO.setSegmentDetails(SegmentUtil.getSegmentAlertInfo(flight.getFlightSegements()));
		if (flight.getFlightSegements() != null && !flight.getFlightSegements().isEmpty()) {
			flightAlertInfoDTO.setRoute((SegmentUtil.getDetailsOfLongestSegment(flight.getFlightSegements())).getSegmentCode());
			flightAlertInfoDTO.setSegments(SegmentUtil.cloneSegments(flight.getFlightSegements()));
		}	

		return flightAlertInfoDTO;
	}

	public static FlightAlertInfoDTO copySegmentDetailsToFlightAlertInfoDTO(FlightAlertInfoDTO flightAlertInfoDTO,
			Collection<FlightSegement> segments) {

		flightAlertInfoDTO.setSegmentDetails(SegmentUtil.getSegmentAlertInfo(segments));
		return flightAlertInfoDTO;
	}

	/**
	 * used for fligth alerts caused by fligth cancellation
	 * 
	 * @param flights
	 * @param transportMethod
	 *            TODO
	 * @return collection of FlightReservationAlertDTO(s)
	 */
	public static Collection<FlightReservationAlertDTO> prepareFlightAlertInfo(Collection<Flight> flights) {

		Collection<FlightReservationAlertDTO> flightReservationAlertDTOList = new ArrayList<FlightReservationAlertDTO>();
		Iterator<Flight> itFlight = flights.iterator();

		while (itFlight.hasNext()) {

			Flight flight = (Flight) itFlight.next();
			FlightAlertInfoDTO oldFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(flight);

			FlightReservationAlertDTO flightReservationAlertDTO = new FlightReservationAlertDTO();
			flightReservationAlertDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
			flightReservationAlertDTO.setFlightSegments(AlertUtil.getFlightSegmentAlertDetails(flight.getFlightSegements(), null,
					null));
			flightReservationAlertDTO.setReservationAlertType(ReservationInternalConstants.ReservationAlertTypes.CANCEL_FLIGHT);
			flightReservationAlertDTO.setAlertDetails(AlertUtil.prepareFlightSegmentAlertDto(oldFlightAlertInfoDTO, null));

			// flightSegmentEmailDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
			// flightSegmentEmailDTO.setFlightSegmentIds(segmentIds);
			// flightSegmentEmailDTO.setFlightSegments(flight.getFlightSegements());
			// [NOTE] alert template ids are set in the command - see SendAlert command
			// flightSegmentEmailDTO.setTopic(EmailTemplateEnum.GENERAL_CANCEL_RESCHEDULE_TEMPLATE);

			flightReservationAlertDTOList.add(flightReservationAlertDTO);
		}

		return flightReservationAlertDTOList;
	}

	private static Set<FlightSegmentDTO> getChangedFlightSegments(Collection<FlightSegement> oldFlightSegs,
			Collection<FlightSegement> newFlightSegs) {
		Map<String, FlightSegement> oldSegs = new HashMap<String, FlightSegement>();
		SimpleDateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm");
		Set<FlightSegmentDTO> changedSegs = new HashSet<FlightSegmentDTO>();

		for (FlightSegement oldSeg : oldFlightSegs) {
			oldSegs.put(oldSeg.getSegmentCode(), oldSeg);
		}
		for (FlightSegement seg : newFlightSegs) {
			if (seg.getSegmentCode().length() == 7) {
				FlightSegement fltSeg = oldSegs.get(seg.getSegmentCode());
				FlightSegmentDTO changedSeg = new FlightSegmentDTO();
				boolean isChangedFlight = false;
				if (fltSeg != null && seg != null) {
					if (!CalendarUtil.isDateEqual(fltSeg.getEstTimeDepatureLocal(), seg.getEstTimeDepatureLocal())) {
						isChangedFlight = true;
						changedSeg.setDepartureTimeLocal(df.format(seg.getEstTimeDepatureLocal()));

					}
					if (!CalendarUtil.isDateEqual(fltSeg.getEstTimeArrivalLocal(), seg.getEstTimeArrivalLocal())) {
						isChangedFlight = true;
						changedSeg.setArrivalTimeLocal(df.format(seg.getEstTimeArrivalLocal()));
					}
				}

				if (isChangedFlight) {
					changedSeg.setSegmentCode(seg.getSegmentCode());
					changedSegs.add(changedSeg);
				}
			}
		}
		return changedSegs;
	}

	/**
	 * public method to create FlightReservationAlertDTO used for flight alerts caused by update to flight details
	 * 
	 * @param oldFlight
	 * @param newFlight
	 * @param transportMethod
	 *            TODO
	 * @return collection of FlightReservationAlertDTO
	 */
	public static FlightReservationAlertDTO prepareFlightAlertInfo(Flight oldFlight, Flight newFlight) {

		FlightReservationAlertDTO flightReservationAlertDTO = null;

		FlightAlertInfoDTO oldFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(oldFlight);
		FlightAlertInfoDTO newFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(newFlight);

		newFlightAlertInfoDTO.setChangedSegments(getChangedFlightSegments(oldFlight.getFlightSegements(),
				newFlight.getFlightSegements()));

		// check for flight info changes
		HashMap<AlertTemplateEnum, HashMap<String, Object>> alertDetails = AlertUtil.prepareFlightSegmentAlertDto(oldFlightAlertInfoDTO, newFlightAlertInfoDTO);

		if (alertDetails.size() > 0) {

			flightReservationAlertDTO = new FlightReservationAlertDTO();
			flightReservationAlertDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
			flightReservationAlertDTO.setNewFlightAlertInfoDTO(newFlightAlertInfoDTO);

			flightReservationAlertDTO
					.setReservationAlertType(ReservationInternalConstants.ReservationAlertTypes.RESCHEDULE_FLIGHT);
			flightReservationAlertDTO.setAlertDetails(AlertUtil.prepareFlightSegmentAlertDto(oldFlightAlertInfoDTO,
					newFlightAlertInfoDTO));
			/*
			 * flightReservationAlertDTO.setFlightSegments(SegmentUtil .getFlightSegIdsAndCodes(AlertUtil
			 * .getAffectedFlightSegments(oldFlightAlertInfoDTO, newFlightAlertInfoDTO)));
			 */
			flightReservationAlertDTO.setFlightSegments(AlertUtil.getFlightSegmentAlertDetails(
					AlertUtil.getAffectedFlightSegments(oldFlightAlertInfoDTO, newFlightAlertInfoDTO),
					oldFlightAlertInfoDTO.getSegments(), newFlightAlertInfoDTO.getSegments()));
			// flightSegmentEmailDTO.setFlightSegmentIds(SegmentUtil
			// .getFlightSegIds(oldFlight.getFlightSegements()));
			// flightSegmentEmailDTO.setFlightSegments(oldFlight.getFlightSegements());
			// flightSegmentEmailDTO.setTopic(EmailTemplateEnum.GENERAL_CANCEL_RESCHEDULE_TEMPLATE);
			// [NOTE] alert template ids are set in the command - see SendAlert command
		}

		return flightReservationAlertDTO;
	}

	/**
	 * used for flight alerts generated by a airport DST change
	 * 
	 * @param oldFlightSegment
	 * @param newFlightSegment
	 * @param oldFlight
	 * @return
	 */
	public static FlightReservationAlertDTO prepareFlightAlertInfo(Collection<FlightSegement> oldFlightSegments, 
			Collection<FlightSegement> newFlightSegments, Flight oldFlight) {

		FlightReservationAlertDTO flightReservationAlertDTO = new FlightReservationAlertDTO();

		FlightAlertInfoDTO oldFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(oldFlight);
		FlightAlertInfoDTO newFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(oldFlight);

		oldFlightAlertInfoDTO = AlertUtil.copySegmentDetailsToFlightAlertInfoDTO(oldFlightAlertInfoDTO, oldFlightSegments);
		newFlightAlertInfoDTO = AlertUtil.copySegmentDetailsToFlightAlertInfoDTO(newFlightAlertInfoDTO, newFlightSegments);

		flightReservationAlertDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
		flightReservationAlertDTO.setNewFlightAlertInfoDTO(newFlightAlertInfoDTO);
		flightReservationAlertDTO.setFlightSegments(AlertUtil.getFlightSegmentAlertDetails(newFlightSegments, oldFlightSegments,
				newFlightSegments));
		flightReservationAlertDTO.setReservationAlertType(ReservationInternalConstants.ReservationAlertTypes.RESCHEDULE_FLIGHT);
		flightReservationAlertDTO.setAlertDetails(AlertUtil.prepareFlightSegmentAlertDto(oldFlightAlertInfoDTO,
				newFlightAlertInfoDTO));

		// flightSegmentEmailDTO.setFlightSegmentIds(SegmentUtil
		// .getFlightSegIds(newFlightSegments));
		// flightSegmentEmailDTO.setFlightSegments(newFlightSegments);
		// flightSegmentEmailDTO.setTopic(EmailTemplateEnum.GENERAL_CANCEL_RESCHEDULE_TEMPLATE);

		return flightReservationAlertDTO;

	}

	/**
	 * 
	 * @param flightReservationAlertDTOList
	 * @param flightAlertDTO
	 * @return
	 */
	public static ReservationAlertDTO prepareReservationAlertInfo(Collection<FlightReservationAlertDTO> flightReservationAlertDTOList,
			FlightAlertDTO flightAlertDTO) {

		ReservationAlertDTO reservationAlertDTO = new ReservationAlertDTO();

		reservationAlertDTO.setFlightAlertDTO(flightAlertDTO);
		reservationAlertDTO.setColFlightReservationAlertDTO(flightReservationAlertDTOList);

		return reservationAlertDTO;
	}

	public static boolean shouldRescheduleNotified(FlightAlertDTO flightAlertDTO) {
		if (flightAlertDTO != null
				&& (flightAlertDTO.isAlertForRescheduledFlight() || flightAlertDTO.isSendEmailsForRescheduledFlight()
						|| flightAlertDTO.isSendSMSForRescheduledFlight() || flightAlertDTO.isSendSMSForFlightAlterations()
						|| flightAlertDTO.isSendEmailsForFlightAlterations() || flightAlertDTO.isEmailForRescheduledFlight())) {
			return true;
		}
		return false;
	}

	public static boolean shouldRescheduleNotifiedAlert(FlightAlertDTO flightAlertDTO) {
		if (flightAlertDTO != null && flightAlertDTO.isAlertForRescheduledFlight()) {
			return true;
		}
		return false;
	}

	public static boolean shouldRescheduleNotifiedEmail(FlightAlertDTO flightAlertDTO) {
		if (flightAlertDTO != null
				&& (flightAlertDTO.isSendEmailsForRescheduledFlight() || flightAlertDTO.isSendEmailsForFlightAlterations())) {
			return true;
		}
		return false;
	}

	public static boolean shouldRescheduleNotifiedSMS(FlightAlertDTO flightAlertDTO) {
		if (flightAlertDTO != null
				&& (flightAlertDTO.isSendSMSForRescheduledFlight() || flightAlertDTO.isSendSMSForFlightAlterations())) {
			return true;
		}
		return false;
	}

	public static boolean isNotifyAnyReservationAffections(FlightAlertDTO flightAlertDTO) {
		if (flightAlertDTO != null
				&& (flightAlertDTO.isAlertForRescheduledFlight() || flightAlertDTO.isSendEmailsForRescheduledFlight()
						|| flightAlertDTO.isSendSMSForRescheduledFlight() || flightAlertDTO.isSendSMSForFlightAlterations()
						|| flightAlertDTO.isSendEmailsForFlightAlterations() || flightAlertDTO.isEmailForCancelledFlight()
						|| flightAlertDTO.isEmailForRescheduledFlight() || flightAlertDTO.isEmailForCancelledFlight()
						|| flightAlertDTO.isEmailForRescheduledFlight() || flightAlertDTO.isAlertForCancelledFlight() || flightAlertDTO
						.isAlertForRescheduledFlight())) {
			return true;
		}
		return false;
	}
	
	public static FlightAlertInfoDTO prepareScheduleAlertInfo(FlightSchedule schedule) {

		FlightAlertInfoDTO flightAlertInfoDTO = new FlightAlertInfoDTO();

		flightAlertInfoDTO.setFlightNumber(new String(schedule.getFlightNumber()));
		flightAlertInfoDTO.setStartDate(schedule.getStartDateLocal());
		flightAlertInfoDTO.setEndDate(schedule.getStopDateLocal());
		flightAlertInfoDTO.setDestinationAptCode(schedule.getArrivalStnCode());
		flightAlertInfoDTO.setOriginAptCode(schedule.getDepartureStnCode());
		flightAlertInfoDTO.setModelNumber(schedule.getModelNumber());
		flightAlertInfoDTO.setScheduleId(schedule.getScheduleId());		
		flightAlertInfoDTO.setOperationDays(getDaysOfOperationAsString(schedule.getFrequencyLocal()));		
		flightAlertInfoDTO.setStatus(schedule.getStatusCode());
		flightAlertInfoDTO.setLegDetails(LegUtil.getLegInfo(schedule.getFlightScheduleLegs(), ScheduleTimeFormat.Local));
		flightAlertInfoDTO.setRoute((SegmentUtil.getDetailsOfLongestFlightScheduleSegment(schedule.getFlightScheduleSegments())== null) ? "-" : SegmentUtil
				.getDetailsOfLongestFlightScheduleSegment(schedule.getFlightScheduleSegments()).getSegmentCode());

		return flightAlertInfoDTO;
	}

	private static String getDaysOfOperationAsString(Frequency freq) {
		String daysOfOperation = null;
		if (freq != null) {
			StringBuilder sb = new StringBuilder();

			if (freq.getDay0()) {
				sb.append("Mon,");
			}

			if (freq.getDay1()) {
				sb.append("Tue,");
			}

			if (freq.getDay2()) {
				sb.append("Wed,");
			}

			if (freq.getDay3()) {
				sb.append("Thu,");
			}

			if (freq.getDay4()) {
				sb.append("Fri,");
			}

			if (freq.getDay5()) {
				sb.append("Sat,");
			}

			if (freq.getDay6()) {
				sb.append("Sun");
			}
			daysOfOperation = sb.toString();
			
			if (daysOfOperation.endsWith(",")) {
				daysOfOperation = daysOfOperation.substring(0, daysOfOperation.length() - 1);
			}

		}
		return daysOfOperation;
	}
	
	public static FlightAlertDTO generateFlightAlertDTO(boolean isScheduleMsgProcessing) {
		FlightAlertDTO fltAltDTO = new FlightAlertDTO();

		if (!isScheduleMsgProcessing) {
			GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

			boolean blnEmailAlert = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS)
					.equalsIgnoreCase("Y") ? true : false;
			boolean blnSmsAlert = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS).equalsIgnoreCase("Y")
					? true
					: false;

			if (blnSmsAlert) {

				boolean blnSmsCnf = globalConfig.getBizParam(SystemParamKeys.SMS_FOR_CONFIRMED_BOOKINGS).equalsIgnoreCase("Y")
						? true
						: false;
				boolean blnSmsOnH = globalConfig.getBizParam(SystemParamKeys.SMS_FOR_ON_HOLD_BOOKINGS).equalsIgnoreCase("Y")
						? true
						: false;
				if (blnSmsCnf || blnSmsOnH) {
					fltAltDTO.setSendSMSForFlightAlterations(blnSmsAlert);
					fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
					fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);
				}
			}

			fltAltDTO.setAlertForRescheduledFlight(true);
			fltAltDTO.setAlertForCancelledFlight(true);
			fltAltDTO.setSendEmailsForFlightAlterations(blnEmailAlert);
		} else {
			fltAltDTO = generateFlightAlertDTOForSsmAsmProcessing(false);
		}

		return fltAltDTO;
	}
	
	public static FlightAlertDTO generateFlightAlertDTOForSsmAsmProcessing(boolean isCancelAction) {

		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

		boolean blnEmailAlert = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS).equalsIgnoreCase("Y")
				? true
				: false;
		boolean blnSmsAlert = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS).equalsIgnoreCase("Y")
				? true
				: false;
		
		boolean blnEmailAlertForScheduleMsg = AppSysParamsUtil.sendEmailForFlightAlterationByScheduleMessages();

		boolean blnSmsAlertForScheduleMsg = AppSysParamsUtil.sendSMSForFlightAlterationByScheduleMessages();

		FlightAlertDTO fltAltDTO = new FlightAlertDTO();

		if (blnSmsAlert && blnSmsAlertForScheduleMsg) {

			boolean blnSmsCnf = globalConfig.getBizParam(SystemParamKeys.SMS_FOR_CONFIRMED_BOOKINGS).equalsIgnoreCase("Y") ? true
					: false;
			boolean blnSmsOnH = globalConfig.getBizParam(SystemParamKeys.SMS_FOR_ON_HOLD_BOOKINGS).equalsIgnoreCase("Y") ? true
					: false;
			if (blnSmsCnf || blnSmsOnH) {
				fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
				fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);
				if (isCancelAction) {
					fltAltDTO.setSendSMSForRescheduledFlight(true);
				} else {
					fltAltDTO.setSendSMSForFlightAlterations(true);
				}
			}
		}

		fltAltDTO.setAlertForRescheduledFlight(true);
		fltAltDTO.setAlertForCancelledFlight(true);

		if (blnEmailAlert && blnEmailAlertForScheduleMsg) {
			fltAltDTO.setSendEmailsForFlightAlterations(true);
			if (isCancelAction) {
				fltAltDTO.setEmailForCancelledFlight(true);
			} else {
				fltAltDTO.setEmailForRescheduledFlight(true);
			}
		}

		return fltAltDTO;
	}
}
