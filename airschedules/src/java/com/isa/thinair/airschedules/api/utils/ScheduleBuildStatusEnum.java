/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

/**
 * @author Lasantha Pambagoda
 */
public class ScheduleBuildStatusEnum {

	private String code;
	private String description;

	private ScheduleBuildStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String toString() {
		return this.code;
	}

	public boolean equals(ScheduleBuildStatusEnum status) {
		return status.toString().equals(this.code);
	}

	public String getDescription() {
		return description;
	}

	public String getCode() {
		return code;
	}

	public static ScheduleBuildStatusEnum getBuildStatus(String code) {

		ScheduleBuildStatusEnum statusEnum = null;
		code = (code == null) ? "" : code;

		if (code.equals(PENDING.toString())) {
			statusEnum = PENDING;
		} else if (code.equals(BUILT.toString())) {
			statusEnum = BUILT;
		}
		return statusEnum;
	}

	public static final ScheduleBuildStatusEnum PENDING = new ScheduleBuildStatusEnum("PND", "Pending");
	public static final ScheduleBuildStatusEnum BUILT = new ScheduleBuildStatusEnum("BLT", "Built");
}
