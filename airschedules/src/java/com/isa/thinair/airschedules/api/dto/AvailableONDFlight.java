package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;

public class AvailableONDFlight implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<FlightSegmentTO> flightSegments;
	private List<SegmentInvAvailability> inventoryAvailability;
	private int ondSequence;
	private String ondCode;

	private List<String> subSegmentCodes = null;

	public List<FlightSegmentTO> getFlightSegments() {
		return flightSegments;
	}

	public void setFlightSegments(List<FlightSegmentTO> flightSegments) {
		this.flightSegments = flightSegments;
		Collections.sort(this.flightSegments);
		this.ondCode = null;
	}

	public List<SegmentInvAvailability> getInventoryAvailability() {
		return inventoryAvailability;
	}

	public void setInventoryAvailability(List<SegmentInvAvailability> inventoryAvailability) {
		this.inventoryAvailability = inventoryAvailability;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public FlightSegmentTO getFirstFlightSegment() {
		if (this.flightSegments == null) {
			return null;
		}
		return this.flightSegments.get(0);
	}

	public SegmentInvAvailability getSegmentInventoryAvailability(FlightSegmentTO segment) {
		for (SegmentInvAvailability segInv : getInventoryAvailability()) {
			if (segInv.getFlightSegment().equals(segment)) {
				return segInv;
			}
		}
		return null;
	}

	public FlightSegmentTO getFlightSegmentFor(String segmentCode) {
		if (isDirectOnd()) {
			if (getFirstFlightSegment().getSegmentCode().equals(segmentCode)) {
				return getFirstFlightSegment();
			}
		} else {
			if (subSegmentCodes.contains(segmentCode)) {
				for (FlightSegmentTO fltSegment : getFlightSegments()) {
					if (fltSegment.getSegmentCode().equals(segmentCode)) {
						return fltSegment;
					}
				}
			}
		}
		return null;
	}

	public String getOndCode() {
		if (this.ondCode == null) {
			String ondCode = null;
			if (this.flightSegments != null && this.flightSegments.size() > 1) {
				this.subSegmentCodes = new ArrayList<String>();
			}
			for (IFlightSegment fltSeg : this.flightSegments) {
				if (ondCode == null) {
					ondCode = fltSeg.getSegmentCode();
				} else {
					String depAirportCode = fltSeg.getSegmentCode().substring(0, fltSeg.getSegmentCode().indexOf("/"));
					if (ondCode.endsWith(depAirportCode)) {
						ondCode = ondCode.substring(0, ondCode.lastIndexOf("/") + 1) + fltSeg.getSegmentCode();
					} else {
						throw new RuntimeException("Invalid segment sequence");
					}
				}
				if (subSegmentCodes != null) {
					subSegmentCodes.add(fltSeg.getSegmentCode());
				}
			}
			this.ondCode = ondCode;
		}
		return this.ondCode;
	}

	public boolean isDirectOnd() {
		return (this.flightSegments != null && this.flightSegments.size() == 1);
	}

	public List<String> getSubONDCodes() {
		return this.subSegmentCodes;
	}

	public Set<String> getDistinctOndCodes() {
		Set<String> ondCodes = new HashSet<String>();
		ondCodes.add(getOndCode());
		if (!isDirectOnd()) {
			ondCodes.addAll(getSubONDCodes());
		}
		return ondCodes;
	}
}