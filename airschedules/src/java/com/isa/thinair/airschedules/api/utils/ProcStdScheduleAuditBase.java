package com.isa.thinair.airschedules.api.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ProcStdScheduleAuditBase {

	public static enum Status {
		FAILD, SUCCESS
	}

	public static enum Operation {
		CANCEL, UPDATE, CREATE, BUILD, SPLIT
	}

	protected static String getStatus(Status status, ProcStdScheduleAuditBase.Operation operation) {
		StringBuilder statusBuilder = new StringBuilder();
		statusBuilder.append(operation.toString());
		statusBuilder.append(" ");
		statusBuilder.append(status.toString());
		return statusBuilder.toString();
	}

	protected static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	protected static String formatDate(Date date) {
		if (date != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			return dateFormat.format(date);
		}
		return null;
	}
}