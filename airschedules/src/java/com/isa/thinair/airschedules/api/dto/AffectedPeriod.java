/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.api.dto.Frequency;

/**
 * class to hold affected period
 * 
 * @author Lasantha Pambagoda
 */
public class AffectedPeriod implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9110633001389086365L;

	private Date startDate;

	private Date endDate;

	private Frequency frequency;

	private boolean middlePeriod = false;

	/**
	 * Constructor
	 */
	public AffectedPeriod() {
	}

	/**
	 * Constructor with arguments
	 */
	public AffectedPeriod(Date startDate, Date endDate, Frequency frequency) {

		this.startDate = startDate;
		this.endDate = endDate;
		this.frequency = frequency;
	}

	/**
	 * Constructor with arguments
	 */
	public AffectedPeriod(Date startDate, Date endDate, Frequency frequency, boolean middlePeriod) {

		this.startDate = startDate;
		this.endDate = endDate;
		this.frequency = frequency;
		this.middlePeriod = middlePeriod;
	}

	/**
	 * @return Returns the endDate.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            The endDate to set.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return Returns the startDate.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return Returns the frequency.
	 */
	public Frequency getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency
	 *            The frequency to set.
	 */
	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	/**
	 * @return Returns the middlePeriod.
	 */
	public boolean isMiddlePeriod() {
		return middlePeriod;
	}

	/**
	 * @param middlePeriod
	 *            The middlePeriod to set.
	 */
	public void setMiddlePeriod(boolean middlePeriod) {
		this.middlePeriod = middlePeriod;
	}

	@Override
	public String toString() {
		return "AffectedPeriod [startDate=" + startDate + ", endDate="
				+ endDate + ", middlePeriod=" + middlePeriod + ", frequency="
				+ frequency + "]";
	}
	
	
}
