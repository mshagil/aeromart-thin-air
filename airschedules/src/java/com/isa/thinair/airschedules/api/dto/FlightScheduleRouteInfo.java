package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author eric
 * 
 */
public class FlightScheduleRouteInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String route;

	private Collection<FlightScheduleSegmentInfo> flightScheduleSegmentInfo;

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public Collection<FlightScheduleSegmentInfo> getFlightScheduleSegmentInfo() {
		if (flightScheduleSegmentInfo == null) {
			flightScheduleSegmentInfo = new ArrayList<FlightScheduleSegmentInfo>();
		}
		return flightScheduleSegmentInfo;
	}

}
