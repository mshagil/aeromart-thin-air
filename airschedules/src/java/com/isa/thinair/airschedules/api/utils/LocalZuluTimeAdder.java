/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.AirportDSTHistory;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * local zulu time detail adder
 * 
 * @author Lasantha Pambagoda
 */
public class LocalZuluTimeAdder {

	private AirportBD airportBD;

	public static final String GMT_OFFSET_ACTION_MINUS = "-";

	public LocalZuluTimeAdder(AirportBD airportBD) {
		this.airportBD = airportBD;
	}

	// //////////////////////////////////////////////
	// PUBLIC METHODS OF LocalZuluTimeAdder
	// /////////////////////////////////////////////

	/**
	 * add zulu and local date/time details to segments. In this situation flight should contain date time details in
	 * zulu time
	 */
	public Flight addOnlySegementTimeDetails(Flight flight) throws ModuleException {

		Set<FlightLeg> legList = flight.getFlightLegs();
		Set<FlightSegement> segList = flight.getFlightSegements();
		Date depatureDate = flight.getDepartureDate();

		Iterator<FlightLeg> it = legList.iterator();
		while (it.hasNext()) {

			Date depLocal = null;
			Date arrLocal = null;

			FlightLeg leg = (FlightLeg) it.next();

			String origin = leg.getOrigin();
			String destination = leg.getDestination();
			Date depZulu = CalendarUtil.getOfssetAndTimeAddedDate(depatureDate, leg.getEstDepartureTimeZulu(),
					leg.getEstDepartureDayOffset());
			Date arrZulu = CalendarUtil.getOfssetAndTimeAddedDate(depatureDate, leg.getEstArrivalTimeZulu(),
					leg.getEstArrivalDayOffset());

			// set the arrival, departure time
			depLocal = this.getLocalDateTime(origin, depZulu);
			arrLocal = this.getLocalDateTime(destination, arrZulu);

			addDateDetailsToSegment(origin, depLocal, depZulu, destination, arrLocal, arrZulu, segList);
		}

		return flight;
	}

	/**
	 * method to add the zulu time details to the give filght which has time details in local time
	 */
	public Flight addZuluTimeDetailsforFlight(Flight flight) throws ModuleException {

		// fields to find out
		Date departureDateZulu = null;

		FlightLeg firstLeg = LegUtil.getFirstFlightLeg(flight.getFlightLegs());

		String origin = firstLeg.getOrigin();
		Date departureDateLocal = CalendarUtil.getTimeAddedDate(flight.getDepartureDateLocal(),
				firstLeg.getEstDepartureTimeLocal());

		// calculate the local date, here EFFECTIVE DATE for estDepartureTimeZulu
		// is the same date (that is departureDateLocal)
		departureDateZulu = this.getZuluDateTime(origin, departureDateLocal);

		// setting found out fields
		flight.setDepartureDate(departureDateZulu);

		// add Zulu time details to legs
		this.addZuluTimeDetailsToFlightLegs(departureDateZulu, departureDateLocal, flight);

		return flight;
	}

	/**
	 * method to add local time details to the flight which already has time detials in zulu time
	 */
	public Flight addLocalTimeDetailsToFlight(Flight flight) throws ModuleException {

		Date departureDateLocal = null;
		Date departureDateZulu = flight.getDepartureDate();

		String origin = flight.getOriginAptCode();
		
		if(StringUtil.isNullOrEmpty(origin)){
			FlightLeg firstLeg = LegUtil.getFirstFlightLeg(flight.getFlightLegs());
			origin = firstLeg.getOrigin();
		}			

		// calculate the local date
		departureDateLocal = this.getLocalDateTime(origin, departureDateZulu);
		flight.setDepartureDateLocal(departureDateLocal);

		if (flight.getFlightLegs() != null)
			this.addLocalTimeToFlightLegs(departureDateLocal, departureDateZulu, flight);

		return flight;
	}

	/**
	 * method to add local time details to flights which already has time detials in zulu time
	 */
	public Collection<Flight> addLocalTimeDetailsToFlights(Collection<Flight> flights) throws ModuleException {

		Collection<Flight> flightsInLocal = new ArrayList<Flight>();
		Iterator<Flight> itFlights = flights.iterator();

		while (itFlights.hasNext()) {

			Flight flight = (Flight) itFlights.next();
			flight = this.addLocalTimeDetailsToFlight(flight);
			flightsInLocal.add(flight);
		}
		return flightsInLocal;
	}

	/**
	 * method to recalculete the zulu time details to the give filght schedule which has time details in zulu time
	 */
	public FlightSchedule recalculateZuluTimeDetailsToSchedule(FlightSchedule schedule) throws ModuleException {

		// fields to recalculate out
		Date startDateZulu = null;
		Date stopDateZulu = null;
		Date startDate = schedule.getStartDate();
		Date stopDate = schedule.getStopDate();

		FlightScheduleLeg firstLeg = LegUtil.getFirstScheduleLeg(schedule.getFlightScheduleLegs());
		if (firstLeg.getEstDepartureDayOffset() != 0) {
			int offsetVarient = firstLeg.getEstDepartureDayOffset();
			LegUtil.changeLegOffsetZulu(schedule.getFlightScheduleLegs(), offsetVarient);
			// Shifting start / end dates
			startDate = CalendarUtil.addDateVarience(startDate, offsetVarient);
			stopDate = CalendarUtil.addDateVarience(stopDate, offsetVarient);
			// Shifting frequency
			Frequency frequencyZulu = FrequencyUtil.getCopyOfFrequency(schedule.getFrequency());
			frequencyZulu = FrequencyUtil.shiftFrequencyBy(frequencyZulu, offsetVarient);
			schedule.setFrequency(frequencyZulu);
		}

		startDateZulu = CalendarUtil.getTimeAddedDate(startDate, firstLeg.getEstDepartureTimeZulu());
		stopDateZulu = CalendarUtil.getTimeAddedDate(stopDate, firstLeg.getEstDepartureTimeZulu());

		// setting recalculated fields
		schedule.setStartDate(startDateZulu);
		schedule.setStopDate(stopDateZulu);

		return schedule;
	}

	/**
	 * method to add the zulu time details to the give flight schedule which has time details in local time
	 */
	public FlightSchedule addZuluTimeDetailsToSchedule(FlightSchedule schedule) throws ModuleException {

		// fields to find out
		Date startDateZulu = null;
		Date stopDateZulu = null;
		Frequency frequencyZulu = null;
		Date startDateLocal = schedule.getStartDateLocal();
		Date stopDateLocal = schedule.getStopDateLocal();

		FlightScheduleLeg firstLeg = LegUtil.getFirstScheduleLeg(schedule.getFlightScheduleLegs());
		String origin = firstLeg.getOrigin();
		if (firstLeg.getEstDepartureDayOffsetLocal() != 0) {
			int offsetVarient = firstLeg.getEstDepartureDayOffsetLocal();
			LegUtil.changeLegOffsetLocal(schedule.getFlightScheduleLegs(), offsetVarient);
			// Shifting start / end dates
			startDateLocal = CalendarUtil.addDateVarience(startDateLocal, offsetVarient);
			stopDateLocal = CalendarUtil.addDateVarience(stopDateLocal, offsetVarient);
			// Shifting frequency
			Frequency frequencyLocal = FrequencyUtil.getCopyOfFrequency(schedule.getFrequencyLocal());
			frequencyLocal = FrequencyUtil.shiftFrequencyBy(frequencyLocal, offsetVarient);
			schedule.setFrequencyLocal(frequencyLocal);
		}

		startDateLocal = CalendarUtil.getTimeAddedDate(startDateLocal, firstLeg.getEstDepartureTimeLocal());
		stopDateLocal = CalendarUtil.getTimeAddedDate(stopDateLocal, firstLeg.getEstDepartureTimeLocal());

		// get the EFFECTIVE DATE for the time conversion
		Date effectiveDate = this.getZuluDateTime(origin, startDateLocal);

		// calculate the local date
		startDateZulu = effectiveDate;
		stopDateZulu = this.getZuluDateTimeAsEffective(origin, effectiveDate, stopDateLocal);

		int dif = CalendarUtil.daysUntil(startDateLocal, startDateZulu);
		frequencyZulu = FrequencyUtil.getCopyOfFrequency(schedule.getFrequencyLocal());
		frequencyZulu = FrequencyUtil.shiftFrequencyBy(frequencyZulu, dif);

		// setting found out fields
		schedule.setStartDate(startDateZulu);
		schedule.setStopDate(stopDateZulu);
		schedule.setFrequency(frequencyZulu);

		// add Zulu time details to legs
		this.addZuluTimeDetailsToFlightScheduleLeg(startDateZulu, startDateLocal, schedule);

		return schedule;
	}

	/**
	 * method to add local time details to the flight schedule which already has time detials in zulu time
	 * 
	 * @param skipLocalDateCalculation
	 *            
	 */
	public FlightSchedule addLocalTimeDetailsToFlightSchedule(FlightSchedule schedule, boolean skipLocalDateCalculation)
			throws ModuleException {

		// fields to find out
		Date startDateLocal = null;
		Date stopDateLocal = null;
		Frequency frequencyLocal = null;

		String origin = schedule.getDepartureStnCode();
		Date startDateZulu = schedule.getStartDate();
		Date stopDateZulu = schedule.getStopDate();

		// EFFECTIVE DATE for the time conversion
		Date effectiveDate = startDateZulu;

		if (!skipLocalDateCalculation || (schedule.getStartDateLocal() == null || schedule.getStopDateLocal() == null)) {
			// calculate the local date
			startDateLocal = this.getLocalDateTimeAsEffective(origin, effectiveDate, startDateZulu);
			stopDateLocal = this.getLocalDateTimeAsEffective(origin, effectiveDate, stopDateZulu);
		} else {
			startDateLocal = schedule.getStartDateLocal();
			stopDateLocal = schedule.getStopDateLocal();
		}
		

		int dif = CalendarUtil.daysUntil(startDateZulu, startDateLocal);
		frequencyLocal = FrequencyUtil.getCopyOfFrequency(schedule.getFrequency());
		frequencyLocal = FrequencyUtil.shiftFrequencyBy(frequencyLocal, dif);

		// setting found out fields
		schedule.setStartDateLocal(startDateLocal);
		schedule.setStopDateLocal(stopDateLocal);
		schedule.setFrequencyLocal(frequencyLocal);

		// add Local time details to legs
		if (schedule.getFlightScheduleLegs() != null && schedule.getFlightScheduleLegs().size() > 0)
			this.addLocalTimeDetailsToFlightScheduleLeg(startDateLocal, startDateZulu, schedule);

		return schedule;
	}

	/**
	 * method to get converted zulu time/date as local time/date for the given station
	 */
	public Date getLocalDateTime(String airport, Date zuluTime) throws ModuleException {

		AirportDST aptDST = airportBD.getEffectiveAirportDST(airport, zuluTime);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		Date localTime = CalendarUtil.getLocalDate(zuluTime, gmtOffset, dstOffset);

		return localTime;
	}

	/**
	 * method to get converted zulu time/date as local time/date for the given station and for a given effective date
	 */
	public Date getLocalDateTimeAsEffective(String airport, Date effectiveDate, Date zuluTime) throws ModuleException {

		AirportDST aptDST = airportBD.getEffectiveAirportDST(airport, effectiveDate);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		Date localTime = CalendarUtil.getLocalDate(zuluTime, gmtOffset, dstOffset);

		return localTime;
	}

	/**
	 * method to get converted local time/date as zulu time/date for the given station
	 */
	public Date getZuluDateTime(String airport, Date localDate) throws ModuleException {

		int dstOffset = 0;

		// find GMT for the airport
		int gmtOffset = getGMTOffset(airport);

		// calculate the zulu date without DST
		Date zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		// find DST with zulu date
		AirportDST aptDST = airportBD.getEffectiveAirportDST(airport, zuluDate);

		dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		// calculate the DSt adjusted zulu date
		zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		AirportDST aptDSTZulu = airportBD.getEffectiveAirportDST(airport, zuluDate);

		if ((aptDSTZulu != null && aptDST != null && aptDSTZulu.getDstCode() != aptDST.getDstCode())
				|| (aptDSTZulu != null && aptDST == null)) {
			throw new ModuleException("airschedules.arg.invalid.cannot.calculate.zulutime");
		} else if (aptDSTZulu == null && aptDST != null) {
			// given local time does not exists due to DST time changes
			zuluDate = CalendarUtil.getZuluDate(CalendarUtil.addMinutes(localDate, dstOffset), gmtOffset, dstOffset);
			aptDSTZulu = airportBD.getEffectiveAirportDST(airport, zuluDate);
			if ((aptDSTZulu != null && aptDST != null && aptDSTZulu.getDstCode() != aptDST.getDstCode())
					|| (aptDSTZulu != null && aptDST == null) || (aptDSTZulu == null && aptDST != null))
				throw new ModuleException("airschedules.arg.invalid.cannot.calculate.zulutime");
		}
		return zuluDate;

	}

	/**
	 * method to get converted local time/date as zulu time/date for the given station and given effective date
	 */
	public Date getZuluDateTimeAsEffective(String airport, Date effectiveDate, Date localDate) throws ModuleException {

		// add local time details to flight
		AirportDST aptDST = airportBD.getEffectiveAirportDST(airport, effectiveDate);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		// calculate the local date
		Date zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		return zuluDate;
	}

	// ///////////////////////////////////////////////
	// PRIVATE MEHTODS
	// //////////////////////////////////////////////

	/**
	 * private mehtod to add the zulu and local date/time details to given segment
	 */
	private void addDateDetailsToSegment(String ognCode, Date ognLocalDate, Date ognZuluDate, String destCode,
			Date destLocalDate, Date destZuluDate, Set<FlightSegement> segmentList) {

		Iterator<FlightSegement> itSegs = segmentList.iterator();
		while (itSegs.hasNext()) {

			FlightSegement segement = (FlightSegement) itSegs.next();
			// if segment start with ogn add depature time details
			if (segement.getSegmentCode().startsWith(ognCode + "/" + destCode)) {

				segement.setEstTimeDepatureLocal(ognLocalDate);
				segement.setEstTimeDepatureZulu(ognZuluDate);
			}

			// if segment start with ogn add depature time details
			if (segement.getSegmentCode().endsWith(ognCode + "/" + destCode)) {

				segement.setEstTimeArrivalLocal(destLocalDate);
				segement.setEstTimeArrivalZulu(destZuluDate);
			}
		}
	}

	/**
	 * private method to add time detils to the flight leg
	 */
	private void addZuluTimeDetailsToFlightLegs(Date depatureDateZulu, Date depatureDateLocal, Flight flight)
			throws ModuleException {

		Set<FlightLeg> legList = flight.getFlightLegs();
		Set<FlightSegement> segList = flight.getFlightSegements();

		Iterator<FlightLeg> itLegs = legList.iterator();
		while (itLegs.hasNext()) {

			FlightLeg leg = (FlightLeg) itLegs.next();

			// fields to find out
			Date estDepartureTimeZulu = null;
			Date estArrivalTimeZulu = null;
			int estDepartureDayOffset = 0;
			int estArrivalDayOffset = 0;

			String origin = leg.getOrigin();
			String destination = leg.getDestination();
			Date estDepartureTimeLocal = CalendarUtil.getOfssetAndTimeAddedDate(depatureDateLocal,
					leg.getEstDepartureTimeLocal(), leg.getEstDepartureDayOffsetLocal());
			Date estArrivalTimeLocal = CalendarUtil.getOfssetAndTimeAddedDate(depatureDateLocal, leg.getEstArrivalTimeLocal(),
					leg.getEstArrivalDayOffsetLocal());

			// here effective date for estDepartureTimeLocal is the same date
			estDepartureTimeZulu = this.getZuluDateTime(origin, estDepartureTimeLocal);
			estDepartureDayOffset = CalendarUtil.daysUntil(depatureDateZulu, estDepartureTimeZulu);

			// here effective date for estArrivalTimeLocal is the same date
			estArrivalTimeZulu = this.getZuluDateTime(destination, estArrivalTimeLocal);
			estArrivalDayOffset = CalendarUtil.daysUntil(depatureDateZulu, estArrivalTimeZulu);

			// setting found out fields
			leg.setEstDepartureTimeZulu(CalendarUtil.getOnlyTime(estDepartureTimeZulu));
			leg.setEstDepartureDayOffset(estDepartureDayOffset);
			leg.setEstArrivalTimeZulu(CalendarUtil.getOnlyTime(estArrivalTimeZulu));
			leg.setEstArrivalDayOffset(estArrivalDayOffset);

			// add time details to segment
			this.addDateDetailsToSegment(origin, estDepartureTimeLocal, estDepartureTimeZulu, destination, estArrivalTimeLocal,
					estArrivalTimeZulu, segList);

		}
	}

	/**
	 * private method to add time detils to the flight leg
	 */
	private void addLocalTimeToFlightLegs(Date depatureDateLocal, Date depatureDateZulu, Flight flight) throws ModuleException {

		Set<FlightLeg> flightLegs = flight.getFlightLegs();
		Iterator<FlightLeg> itLegs = flightLegs.iterator();

		while (itLegs.hasNext()) {

			FlightLeg leg = (FlightLeg) itLegs.next();

			// fields to find out
			Date estDepartureTimeLocal = null;
			Date estArrivalTimeLocal = null;
			int estDepartureDayOffsetLocal = 0;
			int estArrivalDayOffsetLocal = 0;

			String origin = leg.getOrigin();
			String destination = leg.getDestination();
			Date estDepartureTimeZulu = CalendarUtil.getOfssetAndTimeAddedDate(depatureDateZulu, leg.getEstDepartureTimeZulu(),
					leg.getEstDepartureDayOffset());
			Date estArrivalTimeZulu = CalendarUtil.getOfssetAndTimeAddedDate(depatureDateZulu, leg.getEstArrivalTimeZulu(),
					leg.getEstArrivalDayOffset());

			// here effective date for estDepartureTimeZulu is the same date
			estDepartureTimeLocal = this.getLocalDateTime(origin, estDepartureTimeZulu);
			estDepartureDayOffsetLocal = CalendarUtil.daysUntil(depatureDateLocal, estDepartureTimeLocal);

			// here effective date for estArrivalTimeZulu is the same date
			estArrivalTimeLocal = this.getLocalDateTime(destination, estArrivalTimeZulu);
			estArrivalDayOffsetLocal = CalendarUtil.daysUntil(depatureDateLocal, estArrivalTimeLocal);

			// setting found out fields
			leg.setEstDepartureTimeLocal(CalendarUtil.getOnlyTime(estDepartureTimeLocal));
			leg.setEstArrivalTimeLocal(CalendarUtil.getOnlyTime(estArrivalTimeLocal));
			leg.setEstArrivalDayOffsetLocal(estArrivalDayOffsetLocal);
			leg.setEstDepartureDayOffsetLocal(estDepartureDayOffsetLocal);
		}
	}

	/**
	 * private method to add time detils to the flight schedule leg
	 */
	private void addZuluTimeDetailsToFlightScheduleLeg(Date startDateZulu, Date startDateLocal, FlightSchedule schedule)
			throws ModuleException {

		Set<FlightScheduleLeg> legList = schedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> itLegs = legList.iterator();

		while (itLegs.hasNext()) {

			FlightScheduleLeg leg = (FlightScheduleLeg) itLegs.next();

			// fields to find out
			Date estDepartureTimeZulu = null;
			Date estArrivalTimeZulu = null;
			int estDepartureDayOffset = 0;
			int estArrivalDayOffset = 0;

			String origin = leg.getOrigin();
			String destination = leg.getDestination();
			Date estDepartureTimeLocal = CalendarUtil.getOfssetAndTimeAddedDate(startDateLocal, leg.getEstDepartureTimeLocal(),
					leg.getEstDepartureDayOffsetLocal());
			Date estArrivalTimeLocal = CalendarUtil.getOfssetAndTimeAddedDate(startDateLocal, leg.getEstArrivalTimeLocal(),
					leg.getEstArrivalDayOffsetLocal());

			// here effective date for estDepartureTimeLocal is the same date
			estDepartureTimeZulu = this.getZuluDateTime(origin, estDepartureTimeLocal);
			estDepartureDayOffset = CalendarUtil.daysUntil(startDateZulu, estDepartureTimeZulu);

			// here effective date for estArrivalTimeLocal is the same date
			estArrivalTimeZulu = this.getZuluDateTime(destination, estArrivalTimeLocal);
			estArrivalDayOffset = CalendarUtil.daysUntil(startDateZulu, estArrivalTimeZulu);

			// setting found out fields
			leg.setEstDepartureTimeZulu(CalendarUtil.getOnlyTime(estDepartureTimeZulu));
			leg.setEstDepartureDayOffset(estDepartureDayOffset);
			leg.setEstArrivalTimeZulu(CalendarUtil.getOnlyTime(estArrivalTimeZulu));
			leg.setEstArrivalDayOffset(estArrivalDayOffset);
		}
	}

	/**
	 * private method to add time detils to the flight schedule leg
	 */
	private void addLocalTimeDetailsToFlightScheduleLeg(Date startDateLocal, Date startDateZulu, FlightSchedule schedule)
			throws ModuleException {

		Set<FlightScheduleLeg> schedLegs = schedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> itLegs = schedLegs.iterator();

		while (itLegs.hasNext()) {

			FlightScheduleLeg leg = (FlightScheduleLeg) itLegs.next();

			// fields to find out
			Date estDepartureTimeLocal = null;
			Date estArrivalTimeLocal = null;
			int estDepartureDayOffsetLocal = 0;
			int estArrivalDayOffsetLocal = 0;

			String origin = leg.getOrigin();
			String destination = leg.getDestination();
			Date estDepartureTimeZulu = CalendarUtil.getOfssetAndTimeAddedDate(startDateZulu, leg.getEstDepartureTimeZulu(),
					leg.getEstDepartureDayOffset());
			Date estArrivalTimeZulu = CalendarUtil.getOfssetAndTimeAddedDate(startDateZulu, leg.getEstArrivalTimeZulu(),
					leg.getEstArrivalDayOffset());

			// here effective date for estDepartureTimeZulu is the same date
			estDepartureTimeLocal = this.getLocalDateTime(origin, estDepartureTimeZulu);
			estDepartureDayOffsetLocal = CalendarUtil.daysUntil(startDateLocal, estDepartureTimeLocal);

			// here effective date for estArrivalTimeZulu is the same date
			estArrivalTimeLocal = this.getLocalDateTime(destination, estArrivalTimeZulu);
			estArrivalDayOffsetLocal = CalendarUtil.daysUntil(startDateLocal, estArrivalTimeLocal);

			// setting found out fields
			leg.setEstDepartureTimeLocal(CalendarUtil.getOnlyTime(estDepartureTimeLocal));
			leg.setEstArrivalTimeLocal(CalendarUtil.getOnlyTime(estArrivalTimeLocal));
			leg.setEstDepartureDayOffsetLocal(estDepartureDayOffsetLocal);
			leg.setEstArrivalDayOffsetLocal(estArrivalDayOffsetLocal);
		}
	}

	private int getGMTOffset(String airport) throws ModuleException {

		Airport apt = airportBD.getAirport(airport);
		int gmtOffset = 0;
		if (apt != null) {

			gmtOffset = (apt.getGmtOffsetAction().equals(LocalZuluTimeAdder.GMT_OFFSET_ACTION_MINUS)) ? (-1 * apt
					.getGmtOffsetHours()) : apt.getGmtOffsetHours();
		}

		return gmtOffset;
	}
	
	public FlightSchedule addLocalTimeDetailsToSubSchedule(FlightSchedule schedule, SSMSplitFlightSchedule subSchedule)
			throws ModuleException {

		if (subSchedule != null && subSchedule.getStartDate() != null && subSchedule.getStopDate() != null) {
			schedule.setStartDate(CalendarUtil.getTimeAddedDate(subSchedule.getStartDate(),
					LegUtil.getFirstScheduleLeg(schedule.getFlightScheduleLegs()).getEstDepartureTimeZulu()));
			schedule.setStopDate(CalendarUtil.getTimeAddedDate(subSchedule.getStopDate(),
					LegUtil.getFirstScheduleLeg(schedule.getFlightScheduleLegs()).getEstDepartureTimeZulu()));
			schedule.setStartDateLocal(subSchedule.getStartDateLocal());
			schedule.setStopDateLocal(subSchedule.getStopDateLocal());
		}

		return this.addLocalTimeDetailsToFlightSchedule(schedule, true);
	}
	
	
	private Date getLocalDateTimePreviousDST(String airport, Date zuluTime,int dstOffset) throws ModuleException {
		int gmtOffset = getGMTOffset(airport);
		Date localTime = CalendarUtil.getLocalDate(zuluTime, gmtOffset, dstOffset);
		return localTime;
	}

	
	public void addLocalTimeDetailsToFlightScheduleLegOld(Date startDateLocal, Date startDateZulu, FlightSchedule schedule,AirportDSTHistory aptDST)
			throws ModuleException {

		Set<FlightScheduleLeg> schedLegs = schedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> itLegs = schedLegs.iterator();

		while (itLegs.hasNext()) {

			FlightScheduleLeg leg = (FlightScheduleLeg) itLegs.next();

			// fields to find out
			Date estDepartureTimeLocal = null;
			Date estArrivalTimeLocal = null;
			int estDepartureDayOffsetLocal = 0;
			int estArrivalDayOffsetLocal = 0;

			String origin = leg.getOrigin();
			String destination = leg.getDestination();
			Date estDepartureTimeZulu = CalendarUtil.getOfssetAndTimeAddedDate(startDateZulu, leg.getEstDepartureTimeZulu(),
					leg.getEstDepartureDayOffset());
			Date estArrivalTimeZulu = CalendarUtil.getOfssetAndTimeAddedDate(startDateZulu, leg.getEstArrivalTimeZulu(),
					leg.getEstArrivalDayOffset());
			
			// here effective date for estDepartureTimeZulu is the same date
			if (aptDST != null && origin.equals(aptDST.getAirportCode())) {
				estDepartureTimeLocal = this.getLocalDateTimePreviousDST(origin, estDepartureTimeZulu,aptDST.getDstAdjustTimeOld());
			} else {
				estDepartureTimeLocal = this.getLocalDateTime(origin, estDepartureTimeZulu);
			}
			estDepartureDayOffsetLocal = CalendarUtil.daysUntil(startDateLocal, estDepartureTimeLocal);

			// here effective date for estArrivalTimeZulu is the same date
			if (aptDST != null && destination.equals(aptDST.getAirportCode())) {
				estArrivalTimeLocal = this.getLocalDateTimePreviousDST(destination, estArrivalTimeZulu,aptDST.getDstAdjustTimeOld());
			} else {
				estArrivalTimeLocal = this.getLocalDateTime(destination, estArrivalTimeZulu);
			}
			estArrivalDayOffsetLocal = CalendarUtil.daysUntil(startDateLocal, estArrivalTimeLocal);

			// setting found out fields
			leg.setEstDepartureTimeLocal(CalendarUtil.getOnlyTime(estDepartureTimeLocal));
			leg.setEstArrivalTimeLocal(CalendarUtil.getOnlyTime(estArrivalTimeLocal));
			leg.setEstDepartureDayOffsetLocal(estDepartureDayOffsetLocal);
			leg.setEstArrivalDayOffsetLocal(estArrivalDayOffsetLocal);
		}
	}
	
	public FlightSchedule addLocalTimeDetailsToExistingFlightSchedule(FlightSchedule schedule, AirportDSTHistory aptDST)
			throws ModuleException {

		// fields to find out
		Date startDateLocal = null;
		Date stopDateLocal = null;
		Frequency frequencyLocal = null;

		String origin = schedule.getDepartureStnCode();
		Date startDateZulu = schedule.getStartDate();
		Date stopDateZulu = schedule.getStopDate();

		// calculate the local date
		if (aptDST != null && origin.equals(aptDST.getAirportCode())) {
			startDateLocal = this.getLocalDateTimePreviousDST(origin, startDateZulu, aptDST.getDstAdjustTimeOld());
		}

		if (aptDST != null && origin.equals(aptDST.getAirportCode())) {
			stopDateLocal = this.getLocalDateTimePreviousDST(origin, stopDateZulu, aptDST.getDstAdjustTimeOld());
		}

		int dif = CalendarUtil.daysUntil(startDateZulu, startDateLocal);
		frequencyLocal = FrequencyUtil.getCopyOfFrequency(schedule.getFrequency());
		frequencyLocal = FrequencyUtil.shiftFrequencyBy(frequencyLocal, dif);

		// setting found out fields
		schedule.setStartDateLocal(startDateLocal);
		schedule.setStopDateLocal(stopDateLocal);
		schedule.setFrequencyLocal(frequencyLocal);

		// add Local time details to legs
		if (schedule.getFlightScheduleLegs() != null && schedule.getFlightScheduleLegs().size() > 0) {
			if (aptDST != null) {
				this.addLocalTimeDetailsToFlightScheduleLegOld(startDateLocal, startDateZulu, schedule, aptDST);
			}
		}

		return schedule;
	}
}
