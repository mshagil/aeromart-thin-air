/**
 * 
 */
package com.isa.thinair.airschedules.api.model;

import java.io.Serializable;
import java.sql.Clob;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * @author nafly
 * @version Ver 1.0.0
 * @hibernate.class table = "T_FLIGHTS_TO_PUBLISH"
 *
 */
public class FlightsToPublish extends Persistent implements Serializable{

	private static final long serialVersionUID = 1663061642456565796L;
	
	public static final String SUCCESS = "S";
	public static final String PENDING = "P";
	public static final String ERROR   = "E";
	public static final int MAX_BULK_SIZE = 100;
	
	private Integer flightsToPublishId;
	
	private Clob flightDetails;
	
	private Date flightEditTime;
	
	private Date processedTime;
	
	private String processStatus;
	
	private Integer flightId;
	
	private String strFlightDetails;
	

	/**
	 * @return the flightsToPublishId
	 * @hibernate.id column = "FLIGHTS_TO_PUBLISH_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHTS_TO_PUBLISH"
	 */
	public Integer getFlightsToPublishId() {
		return flightsToPublishId;
	}

	/**
	 * @param flightsToPublishId the flightsToPublishId to set
	 */
	public void setFlightsToPublishId(Integer flightsToPublishId) {
		this.flightsToPublishId = flightsToPublishId;
	}

	/**
	 * @return the flightDetails
	 * @hibernate.property column = "FLIGHT_DETAILS"
	 */
	public Clob getFlightDetails() {
		return flightDetails;
	}

	/**
	 * @param flightDetails the flightDetails to set
	 */
	public void setFlightDetails(Clob flightDetails) {
		this.flightDetails = flightDetails;
		setStrFlightDetails(StringUtil.clobToString(this.flightDetails));
	}

	/**
	 * @return the flightEditTime
	 * @hibernate.property column = "FLIGHT_EDIT_TIME"
	 */
	public Date getFlightEditTime() {
		return flightEditTime;
	}

	/**
	 * @param flightEditTime the flightEditTime to set
	 */
	public void setFlightEditTime(Date flightEditTime) {
		this.flightEditTime = flightEditTime;
	}

	/**
	 * @return the processedTime
	 * @hibernate.property column = "PROCESSED_TIME"
	 */
	public Date getProcessedTime() {
		return processedTime;
	}

	/**
	 * @param processedTime the processedTime to set
	 */
	public void setProcessedTime(Date processedTime) {
		this.processedTime = processedTime;
	}

	/**
	 * @return the processStatus
	 * @hibernate.property column = "PROCESSING_STATUS"
	 */
	public String getProcessStatus() {
		return processStatus;
	}

	/**
	 * @param processStatus the processStatus to set
	 */
	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	/**
	 * @return the flightId
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId the flightId to set
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return the strFlightDetails
	 */
	public String getStrFlightDetails() {
		return strFlightDetails;
	}

	/**
	 * @param strFlightDetails the strFlightDetails to set
	 */
	public void setStrFlightDetails(String strFlightDetails) {
		this.strFlightDetails = strFlightDetails;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((flightEditTime == null) ? 0 : flightEditTime.hashCode());
		result = prime * result + ((flightId == null) ? 0 : flightId.hashCode());
		result = prime * result + ((flightsToPublishId == null) ? 0 : flightsToPublishId.hashCode());
		result = prime * result + ((processStatus == null) ? 0 : processStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightsToPublish other = (FlightsToPublish) obj;
		if (flightEditTime == null) {
			if (other.flightEditTime != null)
				return false;
		} else if (!flightEditTime.equals(other.flightEditTime))
			return false;
		if (flightId == null) {
			if (other.flightId != null)
				return false;
		} else if (!flightId.equals(other.flightId))
			return false;
		if (flightsToPublishId == null) {
			if (other.flightsToPublishId != null)
				return false;
		} else if (!flightsToPublishId.equals(other.flightsToPublishId))
			return false;
		if (processStatus == null) {
			if (other.processStatus != null)
				return false;
		} else if (!processStatus.equals(other.processStatus))
			return false;
		return true;
	}
	
	
	
	
}
