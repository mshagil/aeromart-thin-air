package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class WildcardFlightSearchRespDto implements Serializable {
	private FlightSegmentTO bestMatch;
	private boolean flightNumberAmbiguous;
	private boolean departureTimeAmbiguous;

	public FlightSegmentTO getBestMatch() {
		return bestMatch;
	}

	public void setBestMatch(FlightSegmentTO bestMatch) {
		this.bestMatch = bestMatch;
	}

	public boolean isFlightNumberAmbiguous() {
		return flightNumberAmbiguous;
	}

	public void setFlightNumberAmbiguous(boolean flightNumberAmbiguous) {
		this.flightNumberAmbiguous = flightNumberAmbiguous;
	}

	public boolean isDepartureTimeAmbiguous() {
		return departureTimeAmbiguous;
	}

	public void setDepartureTimeAmbiguous(boolean departureTimeAmbiguous) {
		this.departureTimeAmbiguous = departureTimeAmbiguous;
	}
}
