package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.commons.api.dto.Frequency;

public class TransferRollForwardFlightsSearchCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6250809518198611696L;
	private Flight flight;
	private Date fromDate;
	private Date toDate;
	private Frequency frequency;
	private boolean includeClsFlights;
	private boolean isSource;

	public boolean isSource() {
		return isSource;
	}

	public void setSource(boolean isSource) {
		this.isSource = isSource;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return Returns the includeClsFlights.
	 */
	public boolean isIncludeClsFlights() {
		return includeClsFlights;
	}

	/**
	 * @param includeClsFlights
	 *            The includeClsFlights to set.
	 */
	public void setIncludeClsFlights(boolean includeClsFlights) {
		this.includeClsFlights = includeClsFlights;
	}

}
