/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

/**
 * @author Lasantha Pambagoda
 */
public class ScheduleStatusEnum {

	private String code;
	private String description;

	private ScheduleStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String toString() {
		return this.code;
	}

	public boolean equals(ScheduleStatusEnum status) {
		return status.toString().equals(this.code);
	}

	public String getDescription() {
		return description;
	}

	public String getCode() {
		return code;
	}

	public static ScheduleStatusEnum getBuildStatus(String code) {

		ScheduleStatusEnum statusEnum = null;
		code = (code == null) ? "" : code;

		if (code.equals(ACTIVE.toString())) {
			statusEnum = ACTIVE;
		} else if (code.equals(CANCELLED.toString())) {
			statusEnum = CANCELLED;
		} else if (code.equals(CLOSED.toString())) {
			statusEnum = CLOSED;
		} else if (code.equals(ERROR.toString())) {
			statusEnum = ERROR;
		}

		return statusEnum;
	}

	public static final ScheduleStatusEnum ACTIVE = new ScheduleStatusEnum("ACT", "Active");
	public static final ScheduleStatusEnum CANCELLED = new ScheduleStatusEnum("CNX", "Cancelled");
	public static final ScheduleStatusEnum CLOSED = new ScheduleStatusEnum("CLS", "Closed");
	public static final ScheduleStatusEnum ERROR = new ScheduleStatusEnum("ERR", "Error");
}
