/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 15, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case to test FlightScheduleLegDAOImpl
 * 
 * @author Lasantha Pambagoda
 */
public class TestFlightScheduleLegDAOImpl extends PlatformTestCase {

	// flight schedule leg dao to be test
	private FlightScheduleLegDAO flightScheduleLegDAO;

	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		super.setUp();
		flightScheduleLegDAO = (FlightScheduleLegDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_LEG_DAO);
	}

	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * method to test getFlightScheduleLegs
	 */
	public void testGetFlightScheduleLegs() {
		List list = flightScheduleLegDAO.getFlightScheduleLegs();
		assertNotNull("returned list from the getFlightScheduleLegs was null",
				list);
		assertTrue(
				"database has records of flight schedule, " +
				"but the returned list from the getFlightScheduleLegs method has zero elements",
				(list.size() > 0));
	}

	/**
	 * method to test getFlightScheduleLeg
	 */
	public void testGetFlightScheduleLeg() {
		FlightScheduleLeg fliScheLeg = flightScheduleLegDAO
				.getFlightScheduleLeg(10000);
		assertNotNull("database has record with id=10000, "
				+ "but returned flight schedule leg for the id was null",
				fliScheLeg);
	}

	/**
	 * method to test,
	 * 		save new flight schedule leg
	 * 		update existing flight schedule leg
	 * 		remove existing flight schedule leg
	 */
	public void testSaveUpdateRemoveFlightScheduleLeg() {
		
		FlightScheduleLeg fliScheLeg = UtilityForTesting.createFlightScheduleLeg();
		fliScheLeg.setScheduleId(new Integer(10000));
		
		//testing save new flight schedule leg
		saveFlightScheduleLegTesing(fliScheLeg);

		//testing updaing flight schedule leg
		updateFlightScheduleLegTesting(fliScheLeg.getId().intValue());

		//testing remove flight schedule leg
		removeFlightScheduleLegTesting(fliScheLeg.getId().intValue());
	}	
	
	/**
	 * method to test save flight schedule leg
	 */
	public void saveFlightScheduleLegTesing(FlightScheduleLeg fliScheLeg) {
		try {

			flightScheduleLegDAO.saveFlightScheduleLeg(fliScheLeg);

		} catch (Exception ex) {

			fail("Failed in the saving new flightc schedule leg, error = "
					+ ex.getMessage());
		}
	}

	/**
	 * method to test update flight schedule
	 */
	public void updateFlightScheduleLegTesting(int id) {
		try {

			FlightScheduleLeg fliScheLeg= flightScheduleLegDAO.getFlightScheduleLeg(id);
			int duration = fliScheLeg.getDuration();
			fliScheLeg.setDuration(20);
			flightScheduleLegDAO.saveFlightScheduleLeg(fliScheLeg);

			FlightScheduleLeg fltScheLegUpdated = flightScheduleLegDAO.getFlightScheduleLeg(id);

			assertFalse("Flight leg is not updated", duration == fltScheLegUpdated
					.getDuration());
			assertEquals("Version number of the updated flight leg is wrong",
					1, fltScheLegUpdated.getVersion());

		} catch (Exception ex) {
			fail("Failed in the updating flight leg, error = "
					+ ex.getMessage());
		}		
	}
	
	/**
	 * mehtod to test remove flight schedule leg
	 */
	public void removeFlightScheduleLegTesting(int id) {
		try {

			flightScheduleLegDAO.removeFlightScheduleLeg(id);
			FlightScheduleLeg removedfscheleg = flightScheduleLegDAO.getFlightScheduleLeg(id);
			assertNull(
					"Flight schedule Leg has not removed by the removeFlightScheduleLeg method in the DAO",
					removedfscheleg);

		} catch (Exception ex) {

			fail("Failed in the removing flight schedule leg, error = "
					+ ex.getMessage());
		}
	}
}
