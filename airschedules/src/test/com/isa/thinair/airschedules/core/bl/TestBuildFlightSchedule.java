/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import java.util.ArrayList;

import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case to test the build flight schedule command
 * @author Lasantha Pambagoda
 */
public class TestBuildFlightSchedule extends PlatformTestCase {

	//buildFlightScheduleCommand to be test
	private Command buildFlightScheduleCommand;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		buildFlightScheduleCommand
			=  (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.BUILD_SCHEDULE_MACRO);
		
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the create flight schedule macro command
	 * @throws ModuleException 
	 */
	public void testExecute() throws ModuleException{
		
		ArrayList scheduleIds = new ArrayList();
		scheduleIds.add(new Integer(10002));
		
		buildFlightScheduleCommand.setParameter( CommandParamNames.SCHEDULE_IDS , scheduleIds);
		ServiceResponce responce =  (ServiceResponce)buildFlightScheduleCommand.execute();
		
		assertTrue("build schedule is not success full", responce.isSuccess());
		assertNull("Respond code is not null", responce.getResponseCode());
	}
	
}
