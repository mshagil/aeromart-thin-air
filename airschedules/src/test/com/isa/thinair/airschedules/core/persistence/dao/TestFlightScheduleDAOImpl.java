/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 15, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case to test FlightScheduleDAOImpl 
 * @author Lasantha Pambagoda
 */
public class TestFlightScheduleDAOImpl extends PlatformTestCase {
	
	//flight schedule dao to be test
	private FlightScheduleDAO flightScheduleDAO;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		super.setUp();
		flightScheduleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	//////////////////////////////////////////////////
	// Test mehtods of the Flight Schedule DAO
	////////////////////////////////////////////////
	/**
	 * method to test getFlightSchedules
	 */
	public void testGetAllFlightSchedules() {
		Page page = flightScheduleDAO.getAllFlightSchedules(0,10);
		assertNotNull("returned list from the getFlightSchedules was null", page);
		assertTrue(
				"database has records of flight schedules, "
						+ "but the returned list from the getFlightSchedules method has zero elements",
				(page.getPageData().size() > 0));
	}

	/**
	 * method to test getFlightSchedules
	 */
	public void testGetFlightSchedule() {
		FlightSchedule flightSchedule = flightScheduleDAO.getFlightSchedule(10000);
		assertNotNull("database has record with id=10000 for the flight schedule,"
				+ " but returned flight schedule for the id=10000 was null", flightSchedule);
	}

	/**
	 * method to test,
	 * 		save new flight schedule
	 * 		update existing flight schedule
	 * 		remove existing flight schedule
	 */
	public void testSaveUpdateRemoveFlightSchedule() {
		
		FlightSchedule flightSchedule = UtilityForTesting.cerateFlightSchedule();
		
		//testing save new flight schedule
		saveFlightScheduleTesting(flightSchedule);

		//testing updaing flight schedule
		updateFlightScheduleTesting(flightSchedule.getScheduleId().intValue());

		//testing remove flight schedule
		removeFlightScheduleTesting(flightSchedule.getScheduleId().intValue());
	}	
	
	/**
	 * mehtod to test conflicting schedules for flight schedule
	 */
	public void testGetConflictingSchedules(){
		
		FlightSchedule flightSchedule = UtilityForTesting.getConflictingFlightScheduleWithSchedules();	
		
		AffectedPeriod period = new AffectedPeriod(flightSchedule.getStartDate(), 
				flightSchedule.getStopDate(), flightSchedule.getFrequency());
		
		List list = flightScheduleDAO.getConflictingSchedules(flightSchedule, period);
		assertNotNull("returned list from the getOverlappingFlightsForFlightSchedules was null", list);
		assertTrue(
				"database has maching records for the senario, "
						+ "but the returned list from the getOverlappingFlightsForFlightSchedules " +
								"method has zero elements",
				(list.size() > 0));
	}
	
	/**
	 * mehtod to test conflicting flights for flight schedule
	 */
	public void testGetConflictingFlights() {
		
		FlightSchedule flightSchedule = UtilityForTesting.getConflictingFlightScheduleWithFlights();
		
		AffectedPeriod period = new AffectedPeriod(flightSchedule.getStartDate(), 
				flightSchedule.getStopDate(), flightSchedule.getFrequency());
		
		List list = flightScheduleDAO.getConflictingFlights(flightSchedule, period);
		assertNotNull("returned list from the getOverlappingFlightsForFlightSchedules was null", list);
		assertTrue(
				"database has maching records for the senario, "
						+ "but the returned list from the getOverlappingFlightsForFlightSchedules" +
								" method has zero elements",
				(list.size() > 0));	
		System.out.println(list.size());
	}
	
	/**
	 * method to test serch flight schedules
	 */
	public void testSearchFlightSchedulesForDisplay(){
		
		List criList = UtilityForTesting.createSerchCriteriaForSchedules();
		
		/*Page page = flightScheduleDAO.searchFlightSchedulesForDisplay(criList, 0, 10);
		assertNotNull("returned list from the searchFlightSchedulesForDisplay was null", page);
		assertTrue(
				"database has maching records of flight schedules, "
						+ "but the returned list from the searchFlightSchedulesForDisplay method has zero elements",
				(page.getPageData().size() > 0));
		*/
	}	
	
	/**
	 * method to test serch flight schedules
	 */
	public void testSearchFlightSchedules(){
		
		List criList = UtilityForTesting.createSerchCriteriaForSchedules();
		
		Page page = flightScheduleDAO.searchFlightSchedules(criList, 0 , 10);
		assertNotNull("returned list from the searchFlightSchedules was null", page);
		assertTrue(
				"database has maching records of flight schedules, "
						+ "but the returned list from the searchFlightSchedules method has zero elements",
				(page.getPageData().size() > 0));
	}
	
	/**
	 * mehtod to test getFlightNumbers
	 */
	public void testGetFlightNumbers(){
		
		Collection rerults = flightScheduleDAO.getFlightNumbers();
		assertNotNull("returned list from the getFlightNumbers was null", rerults);
		assertTrue(
				"database has maching records of flight numbers, "
						+ "but the returned list from the getFlightNumbers method has zero elements",
				(rerults.size() > 0));
	}
	
	/**
	 * test method for getNoOfFlightsForSchedule 
	 */
	public void testGetNoOfFlightsForSchedule(){
		
		int num = flightScheduleDAO.getNoOfFlightsForSchedule(10001);
		assertEquals("schedule 10001 has 6 flights", 6, num);
	}
	
	/**
	 * test method for testGetFlightsForSchedule
	 */
	public void testGetFlightsForSchedule(){
		
		Collection rerults = flightScheduleDAO.getFlightsForSchedule(10001);
		assertNotNull("returned list from the getFlightsForSchedule was null", rerults);
		assertTrue(
				"database has maching records of flights, "
						+ "but the returned list from the getFlightsForSchedule method has zero elements",
				(rerults.size() > 0));
		assertEquals("schedule 10001 has 6 flights", 6, rerults.size());
	}
	
	/**
	 * test method for testGetPossibleOverlappingScheduleIds
	 */
	public void testGetPossibleOverlappingSchedules(){
		
		FlightSchedule flightSchedule = UtilityForTesting.getOverLappingFlightSchedule();
		/*Collection collection = flightScheduleDAO.getPossibleOverlappingSchedules(flightSchedule);
		assertNotNull("returned list from the testGetPossibleOverlappingSchedules was null", collection);
		assertTrue(
				"database has maching records of overlapping flight schedules," +
				" but the returned list from the getPossibleOverlappingSchedules" +
				" method has zero elements",
				(collection.size() > 0));
		*/
	}
	
	///////////////////////////////////////////
	// private methods to help tests
	//////////////////////////////////////////
	/**
	 * method to test save new flight schedule
	 */
	private void saveFlightScheduleTesting(FlightSchedule flightSchedule) {
		
		try {

			flightScheduleDAO.saveFlightSchedule(flightSchedule);

		} catch (Exception ex) {

			fail("Failed in the saving new flight schedule, error = " + ex.getMessage());
		}
	}

	/**
	 * method to test update flight schedule
	 */
	private void updateFlightScheduleTesting(int id) {
		
		try {

			FlightSchedule flts = flightScheduleDAO.getFlightSchedule(id);
			Integer avaiKilo = flts.getAvailableSeatKilometers();
			flts.setAvailableSeatKilometers(new Integer(20));
			flightScheduleDAO.saveFlightSchedule(flts);

			FlightSchedule fsUpdated = flightScheduleDAO.getFlightSchedule(id);

			assertFalse("FlightSchedule is not updated", avaiKilo.intValue() == fsUpdated
					.getAvailableSeatKilometers().intValue());
			assertEquals("Version number of the updated flight schedule is wrong", 1,
					fsUpdated.getVersion());

		} catch (Exception ex) {
			fail("Failed in the updating flight schedule, error = " + ex.getMessage());
		}		
	}
	
	/**
	 * method to remove flight schedule
	 */
	private void removeFlightScheduleTesting(int id) {
		
		try {

			flightScheduleDAO.removeFlightSchedule(id);
			FlightSchedule removedFltSche = flightScheduleDAO.getFlightSchedule(id);
			assertNull(
					"FlightSchedule has not removed by the removeFlightSchedule method in the DAO",
					removedFltSche);

		} catch (Exception ex) {

			fail("Failed in the removing flight schedule, error = " + ex.getMessage());
		}
	}


}
