/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 15, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case to test FlightSegementDAOImpl 
 * @author Lasantha Pambagoda
 */
public class TestFlightSegementDAOImpl extends PlatformTestCase {

	//flight segment dao to be test
	private FlightSegementDAO flightSegementDAO;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		flightSegementDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);
		
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	/**
	 * method to test getFlightSegments
	 */
	/*public void testGetFlightSegements() {
		List list = flightSegementDAO.getFlightSegements();
		assertNotNull("returned list from the getFlightSegements was null", list);
		assertTrue(
				"database has records of flight segments, "
						+ "but the returned list from the getFlightSegements method has zero elements",
				(list.size() > 0));
	}*/

	/**
	 * method to test getFlightSegment
	 */
	/*public void testGetFlightSegement() {
		FlightSegement flightSegment = flightSegementDAO.getFlightSegement(10010);
		assertNotNull("database has record with id=10010 for the flight schedule,"
				+ " but returned flight schedule for the id=10010 was null", flightSegment);
	}*/

	/**
	 * method to test,
	 * 		save new flight segment
	 * 		update existing flight segment
	 * 		remove existing flight segment
	 */
	/*public void testSaveUpdateRemoveFlightSegment() {
		
		FlightSegement fliSeg = UtilityForTesting.createFlightSegment();

		//testing save new flight segmnet
		saveFlightSegmentTesting(fliSeg);

		//testing updaing flight segmnet
		updateFlightSegmentTesting(fliSeg.getFltSegId().intValue());

		//testing remove flight segmnet
		removeFlightSegmentTesting(fliSeg.getFltSegId().intValue());
	}*/
	
	/**
	 * method to test getFlightSegments
	 */
	public void testGetFlightSegmentsForLocalDate() {
		String airportCode = "SHJ";
		String flightNo = "G9663";
		Date departureDateLocal = new GregorianCalendar(2006,2,16).getTime();
		boolean dateOnly = true;
		Collection flightSegments = flightSegementDAO.getFlightSegmentsForLocalDate(airportCode, null
				, flightNo, departureDateLocal, dateOnly);
		assertNotNull("returned list from the getFlightSegmentsForLocalDate was null", flightSegments);
		assertTrue(
				"database has records of flight segments, "
						+ "but the returned list from the getFlightSegmentsForLocalDate method has zero elements",
				(flightSegments.size() > 0));
	}
	
	/**
	 * method to test save new flight segment
	 */
	private void saveFlightSegmentTesting( FlightSegement fs) {

		try {

			flightSegementDAO.saveFlightSegement(fs);

		} catch (Exception ex) {

			fail("Failed in the saving new flight segment, error = " + ex.getMessage());
		}
		
	}

	/**
	 * method to test update flight segment
	 */
	private void updateFlightSegmentTesting(int id) {

		try {

			FlightSegement fs = flightSegementDAO.getFlightSegement(id);
			boolean valid = fs.getValidFlag();
			fs.setValidFlag(false);
			flightSegementDAO.saveFlightSegement(fs);

			FlightSegement fsUpdated = flightSegementDAO.getFlightSegement(id);

			assertFalse("FlightSegment is not updated", valid == fsUpdated.getValidFlag());
			assertEquals("Version number of the updated flight segmnet is wrong", 1,
					fsUpdated.getVersion());

		} catch (Exception ex) {
			fail("Failed in the updating flight segmnet, error = " + ex.getMessage());
		}		

	}
	
	/**
	 * method to test remove flight segment
	 */
	private void removeFlightSegmentTesting(int id) {

		try {

			flightSegementDAO.removeFlightSegement(id);
			FlightSegement removedFltSeg = flightSegementDAO.getFlightSegement(id);
			assertNull(
					"FlightSegment has not removed by the removeFlightSegment method in the DAO",
					removedFltSeg);

		} catch (Exception ex) {

			fail("Failed in the removing flight segment, error = " + ex.getMessage());
		}

	}
}
