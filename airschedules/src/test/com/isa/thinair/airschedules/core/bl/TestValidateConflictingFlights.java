/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import java.util.Collection;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;

import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * testing the ValidateConflictingFlights command
 * @author Lasantha Pambagoda
 */
public class TestValidateConflictingFlights extends PlatformTestCase {
	
	// validate conflicting schedules command to be test
	private Command conflictFltCommand;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		conflictFltCommand = (Command) AirSchedulesUtil
				.getInstance().getLocalBean(CommandNames.CHECK_CONFLICTING_FLIGHTS_FOR_SCHEDULE);
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the validate conflicting schedules command
	 * @throws ModuleException 
	 */
	public void testExecute() throws ModuleException{
		
			FlightSchedule schedule = UtilityForTesting.getConflictingFlightScheduleWithFlights();
			
			conflictFltCommand.setParameter( CommandParamNames.FLIGHT_SCHEDULE , schedule);
			ServiceResponce responce =  (ServiceResponce)conflictFltCommand.execute();
			
			assertFalse("Conflicts should found but conflicts not found", responce.isSuccess());
			Object conflicts = responce.getResponseParam(responce.getResponseCode());
			assertNotNull("Conflict detail list is null", conflicts);
			assertTrue("Conflict detail list is empty", ((Collection)conflicts).size() > 0 );
	}

}
