/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules;

import com.isa.thinair.platform.api.util.PlatformTestCase;


/**
 * Test class to test the module initializationn
 * @author Lasantha Pambagoda
 */
public class TestModuleInitialization extends PlatformTestCase {
	
	public void testModuleInitialization(){
		try {
			super.setUp();
		}catch (Exception ex) {
			fail("module not initialized. erro message = " + ex.getMessage());
		}
	}
}
