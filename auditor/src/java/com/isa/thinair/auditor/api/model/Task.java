package com.isa.thinair.auditor.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author MN
 * @hibernate.class table="T_TASK"
 */
public class Task implements Serializable {

	private static final long serialVersionUID = -4446050897991935156L;
	private String taskCode;
	private String template;
	private TaskGroup taskGroup;
	private String severityLevel;

	public Task() {
	}

	public Task(String taskCode, TaskGroup taskGroup) {
		this.taskCode = taskCode;
		this.taskGroup = taskGroup;
	}

	public Task(String taskTypeCode, String template, TaskGroup taskGroup) {
		this.taskCode = taskTypeCode;
		this.template = template;
		this.taskGroup = taskGroup;
	}

	/**
	 * @hibernate.id generator-class="assigned" type="java.lang.String" column="TASK_CODE"
	 */
	public String getTaskCode() {
		return this.taskCode;
	}

	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	/**
	 * @hibernate.property column="TEMPLATE"
	 */
	public String getTemplate() {
		return this.template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * @hibernate.property column="SEVERITY_LEVEL"
	 */
	public String getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(String severityLevel) {
		this.severityLevel = severityLevel;
	}

	/**
	 * @hibernate.many-to-one not-null="true"
	 * @hibernate.column name="TASK_GROUP_CODE"
	 */
	public TaskGroup getTaskGroup() {
		return this.taskGroup;
	}

	public void setTaskGroup(TaskGroup taskGroup) {
		this.taskGroup = taskGroup;
	}

	public String toString() {
		return new ToStringBuilder(this).append("taskCode", getTaskCode()).toString();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Task))
			return false;
		Task castOther = (Task) other;
		return new EqualsBuilder().append(this.getTaskCode(), castOther.getTaskCode()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getTaskCode()).toHashCode();
	}
}
