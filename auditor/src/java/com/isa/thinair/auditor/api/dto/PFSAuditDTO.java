package com.isa.thinair.auditor.api.dto;

import org.apache.commons.lang.time.DateFormatUtils;

import com.isa.thinair.auditor.api.model.PFSAudit;

public class PFSAuditDTO {

	private long pfsID;
	// private Date timeStampZulu;
	private String timeStampZulu;
	private String action;
	private String userNote;
	private String systemNote;
	private String userName;
	private String ipAddress;

	public PFSAuditDTO() {

	}

	public PFSAuditDTO(String ipAddress, String userName, String userNote, String systemNote) {
		this();
		this.ipAddress = ipAddress;
		this.userName = userName;
		this.userNote = userNote;
		this.systemNote = systemNote;
	}

	public PFSAuditDTO(PFSAudit pfsAudit) {

		this.timeStampZulu = DateFormatUtils.format(pfsAudit.getTimeStampZulu(), "dd-MM-yyyy HH:mm"); // pfsAudit.getTimeStampZulu();
		this.pfsID = pfsAudit.getPfsID();
		this.action = pfsAudit.getAction();
		this.userNote = pfsAudit.getUserNote();
		this.systemNote = pfsAudit.getSystemNote();
		this.userName = pfsAudit.getUserName();
		this.ipAddress = pfsAudit.getIpAddress();
	}

	public long getPfsID() {
		return pfsID;
	}

	public void setPfsID(long pfsID) {
		this.pfsID = pfsID;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public String getSystemNote() {
		return systemNote;
	}

	public void setSystemNote(String systemNote) {
		this.systemNote = systemNote;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getTimeStampZulu() {
		return timeStampZulu;
	}

	public void setTimeStampZulu(String timeStampZulu) {
		this.timeStampZulu = timeStampZulu;
	}

}
