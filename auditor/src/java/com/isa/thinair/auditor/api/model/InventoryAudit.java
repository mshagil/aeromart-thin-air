/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:11:58
 * ===============================================================================
 */
package com.isa.thinair.auditor.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Byorn
 * 
 * @hibernate.class table = "T_INVENTORY_AUDIT"
 */
public class InventoryAudit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3376184110211057378L;
	private Long auditId;
	private Date auditTime;
	private Integer operationId;
	private Integer fccsbInvId;
	private String content;
	private String userId;
	private Integer salesChannelCode;
	private Integer fccSegInvId;
	// unmapped
	private String segmentCode;
	private String logicalCCCode;

	/**
	 * @hibernate.id column = "AUDIT_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_INVENTORY_AUDIT"
	 */
	public Long getAuditId() {
		return auditId;
	}

	/**
	 * @param auditId
	 *            the auditId to set
	 */
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	/**
	 * @return the auditTime
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getAuditTime() {
		return auditTime;
	}

	/**
	 * @param auditTime
	 *            the auditTime to set
	 */
	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	/**
	 * @hibernate.property column = "OPERATION_ID"
	 * @return the operationId
	 */
	public Integer getOperationId() {
		return operationId;
	}

	/**
	 * @param operationId
	 *            the operationId to set
	 */
	public void setOperationId(Integer operationId) {
		this.operationId = operationId;
	}

	/**
	 * @hibernate.property column = "FCCSBA_ID"
	 * @return the fccsbInvId
	 */
	public Integer getFccsbInvId() {
		return fccsbInvId;
	}

	/**
	 * @param fccsbInvId
	 *            the fccsbInvId to set
	 */
	public void setFccsbInvId(Integer fccsbInvId) {
		this.fccsbInvId = fccsbInvId;
	}

	/**
	 * @hibernate.property column = "CONTENT"
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @hibernate.property column = "USER_ID"
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 * @return the salesChannelCode
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            the salesChannelCode to set
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return the fccSegInvId
	 * @hibernate.property column = "FCCSA_ID"
	 */
	public Integer getFccSegInvId() {
		return fccSegInvId;
	}

	/**
	 * @param fccSegInvId
	 *            the fccSegInvId to set
	 */
	public void setFccSegInvId(Integer fccSegInvId) {
		this.fccSegInvId = fccSegInvId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}
}
