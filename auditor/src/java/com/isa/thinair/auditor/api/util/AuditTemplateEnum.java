package com.isa.thinair.auditor.api.util;

import java.io.Serializable;

/**
 * @author Thejaka UA
 */
public class AuditTemplateEnum implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4543374355688717342L;
	private String code;

	private AuditTemplateEnum(String code) {
		this.code = code;
	}

	public boolean equals(AuditTemplateEnum alertTemplate) {
		return (alertTemplate.code == this.code);
	}

	public String getCode() {
		return code;
	}

	public static final AuditTemplateEnum CHANGED_CONTACT_DETAILS = new AuditTemplateEnum("MODCNT");
	public static final AuditTemplateEnum CHANGED_PASSENGER_DETAILS = new AuditTemplateEnum("MODPAX");
	public static final AuditTemplateEnum CANCELED_RESERVATION = new AuditTemplateEnum("CNXRES");
	public static final AuditTemplateEnum SPLIT_RESERVATION = new AuditTemplateEnum("SPTRES");
	public static final AuditTemplateEnum REMOVED_PASSENGER = new AuditTemplateEnum("RMVPAX");
	public static final AuditTemplateEnum CANCELED_SEGMENT = new AuditTemplateEnum("CNXSEG");
	public static final AuditTemplateEnum ADDED_NEW_SEGMENT = new AuditTemplateEnum("ADDSEG");
	public static final AuditTemplateEnum MODIFIED_SEGMENT = new AuditTemplateEnum("MODSEG");
	public static final AuditTemplateEnum ADDED_NEW_PASSENGER = new AuditTemplateEnum("ADDPAX");
	public static final AuditTemplateEnum CREDIT_ADJUSTED = new AuditTemplateEnum("ADJCRD");
	public static final AuditTemplateEnum PAYMENT_MADE = new AuditTemplateEnum("PADRES");
	public static final AuditTemplateEnum ADDED_ONHOLD_RESERVATION = new AuditTemplateEnum("NEWONH");
	public static final AuditTemplateEnum ADDED_CONFIRMED_RESERVATION = new AuditTemplateEnum("NEWCNF");
	public static final AuditTemplateEnum MADE_REFUND = new AuditTemplateEnum("REFPAX");
	public static final AuditTemplateEnum OWNERSHIP_TRANSFERED = new AuditTemplateEnum("TNFOWN");
	public static final AuditTemplateEnum CLEARED_ALERT = new AuditTemplateEnum("CLRALT");
	public static final AuditTemplateEnum CREATED_ALERT = new AuditTemplateEnum("CREALT");
	public static final AuditTemplateEnum EXTENDED_ONHOLD = new AuditTemplateEnum("EXTOHD");
	public static final AuditTemplateEnum PAYMENT_ERROR = new AuditTemplateEnum("PAYERR");
	public static final AuditTemplateEnum VIEWED_RESERVATION = new AuditTemplateEnum("VIVRES");
	public static final AuditTemplateEnum TRANSFERED_SEGMENT = new AuditTemplateEnum("TNFSEG");
	public static final AuditTemplateEnum MIGRATION_MODIFICATION = new AuditTemplateEnum("MIGMOD");
	public static final AuditTemplateEnum ADDED_SPLIT_RESERVATION = new AuditTemplateEnum("NEWSPT");
	public static final AuditTemplateEnum ADDED_RESERVATION_FOR_REMOVED_PASSENGERS = new AuditTemplateEnum("NEWRMP");
	public static final AuditTemplateEnum CREDIT_CARRY_FORWARD = new AuditTemplateEnum("CRCFWD");
	public static final AuditTemplateEnum CREDIT_BROUGHT_FORWARD = new AuditTemplateEnum("CRBFWD");
	public static final AuditTemplateEnum EMAIL_ITINERARY = new AuditTemplateEnum("EMLPNR");
	public static final AuditTemplateEnum EMAIL_FLIGHT_ALTERATION = new AuditTemplateEnum("EMLFAL");
	public static final AuditTemplateEnum INTERLINED_SEGMENT_TRANSFER = new AuditTemplateEnum("INSEGT");
	public static final AuditTemplateEnum CHANGED_EXTERNAL_SEGMENTS = new AuditTemplateEnum("CEXSEG");
	public static final AuditTemplateEnum MODIFY_SEATS = new AuditTemplateEnum("MODSET");
	public static final AuditTemplateEnum CREDIT_REINSTATE = new AuditTemplateEnum("REINST");
	public static final AuditTemplateEnum CREDIT_EXTEND_EXPIRY = new AuditTemplateEnum("CRXTND");
	public static final AuditTemplateEnum NOTIFY_PNR = new AuditTemplateEnum("NFYPNR");
	public static final AuditTemplateEnum ADD_INSURANCE = new AuditTemplateEnum("ADDINS");
	public static final AuditTemplateEnum OPEN_RETURN_CONFIRM = new AuditTemplateEnum("OPRCNF");
	public static final AuditTemplateEnum MODIFY_MEALS = new AuditTemplateEnum("MODMEL");
	public static final AuditTemplateEnum MODIFY_AIRPORT_TRANSFERS = new AuditTemplateEnum("MODAPT");
	public static final AuditTemplateEnum EMAIL_REMINDER = new AuditTemplateEnum("EMLREM");
	public static final AuditTemplateEnum ADD_SSR = new AuditTemplateEnum("ADSSR");
	public static final AuditTemplateEnum MODIFY_SSR = new AuditTemplateEnum("MODSSR");
	public static final AuditTemplateEnum FLOWN_PAX = new AuditTemplateEnum("FLOWN_FLIGHT_PAX");
	public static final AuditTemplateEnum PAX_PFS_ENTRY = new AuditTemplateEnum("PFSENTRY");
	public static final AuditTemplateEnum PAX_ETL_ENTRY = new AuditTemplateEnum("ETLENTRY");
	public static final AuditTemplateEnum PAX_PRL_ENTRY = new AuditTemplateEnum("ETLENTRY");
	public static final AuditTemplateEnum NO_REC_PAX = new AuditTemplateEnum("NO_REC_PAX");
	public static final AuditTemplateEnum NO_SHOW_PAX = new AuditTemplateEnum("NO_SHOW_PAX");
	public static final AuditTemplateEnum GO_SHOW_PAX = new AuditTemplateEnum("GO_SHOW_PAX");
	public static final AuditTemplateEnum MODIFY_BAGGAGES = new AuditTemplateEnum("MODBAG");
	public static final AuditTemplateEnum OHD_ROLL_FORWARD = new AuditTemplateEnum("OHDRFWD");
	public static final AuditTemplateEnum UPDATED_ETICKET = new AuditTemplateEnum("MODETICK");
	public static final AuditTemplateEnum ONHOLD_EMAIL = new AuditTemplateEnum("EMLOHD");
	public static final AuditTemplateEnum RESERVATION_MODIFIED_EMAIL_AGENT = new AuditTemplateEnum("EMLRESMOD");
	public static final AuditTemplateEnum BANK_GUR_EXP_EMAIL = new AuditTemplateEnum("EMLBNKEXP");
	public static final AuditTemplateEnum CREDIT_LIMIT_UTILIZATION_EMAIL = new AuditTemplateEnum("EMLBNKCL");
	public static final AuditTemplateEnum BSP_CREDIT_LIMIT_UTILIZATION_EMAIL = new AuditTemplateEnum("EMLBSPCL");
	public static final AuditTemplateEnum PAX_AUTO_REFUND = new AuditTemplateEnum("REFPAXAUTO");
	public static final AuditTemplateEnum PAX_AGENT_AUTO_REFUND = new AuditTemplateEnum("REFAGNAUTO");
	public static final AuditTemplateEnum RES_PEN_CHARGES = new AuditTemplateEnum("PENCHG");
	public static final AuditTemplateEnum AUTO_CANCELLATION = new AuditTemplateEnum("AUTOCNX");
	public static final AuditTemplateEnum PROMOTION_NEXT_SEAT_FREE_REQUEST_LEVEL = new AuditTemplateEnum("PRONSFREQ");
	public static final AuditTemplateEnum PROMOTION_FLEXI_DATE_REQUEST_LEVEL = new AuditTemplateEnum("PROFXDREQ");
	public static final AuditTemplateEnum PROMOTION_FLEXI_DATE_SUBSCRIPTION = new AuditTemplateEnum("PROMOFDSUB");
	public static final AuditTemplateEnum CHANGED_WL_PRIORITY = new AuditTemplateEnum("CHGWLPRI");
	public static final AuditTemplateEnum OVERRIDE_CNX_CHARGE = new AuditTemplateEnum("OVRCNXCHG");
	public static final AuditTemplateEnum OVERRIDE_MOD_CHARGE = new AuditTemplateEnum("OVRMODCHG");
	public static final AuditTemplateEnum CHANGE_FLIGHT_TIME = new AuditTemplateEnum("CHGFLTTIME");
	public static final AuditTemplateEnum CNX_CHARGE_OVERIDE = new AuditTemplateEnum("CNXCRGOVRD");
	public static final AuditTemplateEnum OHD_PAY_REMINDER = new AuditTemplateEnum("EMLPAY");
	public static final AuditTemplateEnum ANCI_PENALTY_CHARGE = new AuditTemplateEnum("ANCIPENCHG");
	public static final AuditTemplateEnum EXTERNAL_TICKET_EXCHANGE = new AuditTemplateEnum("TKTEXCHG");
	public static final AuditTemplateEnum ADDED_NEW_OAL_SEGMENT = new AuditTemplateEnum("ADDOALSEG");
	public static final AuditTemplateEnum CHANGED_OAL_SEGMENTS = new AuditTemplateEnum("COALSEG");
	public static final AuditTemplateEnum ADD_USER_NOTE = new AuditTemplateEnum("ADDUNOTE");
	public static final AuditTemplateEnum PROC_SSM = new AuditTemplateEnum("PROCSSM");
	public static final AuditTemplateEnum PROC_ASM = new AuditTemplateEnum("PROCASM");
	public static final AuditTemplateEnum PROC_SISM = new AuditTemplateEnum("PROCSISM");
	public static final AuditTemplateEnum FFID_REMOVE_CUSTOMER_NAME_UPDATE = new AuditTemplateEnum("FFRECUNAUP");
	public static final AuditTemplateEnum CHARGES_REVERSED = new AuditTemplateEnum("CHRGREV");
	public static final AuditTemplateEnum NO_SHOW_TAX_REV = new AuditTemplateEnum("NSTAXREV");
	public static final AuditTemplateEnum GENERATE_TAX_INVOICE = new AuditTemplateEnum("GENTAXINV");

	public interface TemplateParams {
		public interface AddedNewSegment {
			public static final String SEGMENT_INFORMATION = "segmentInformation";
			public static final String FORCE_CONFIRM = "forceConfirm";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface CanceledSegment {
			public static final String SEGMENT_INFORMATION = "segmentInformation";
			public static final String VACATED_SEATS = "vacatedSeats";
			public static final String VACATED_MEALS = "vacatedMeals";
			public static final String FARE_CHANGES = "fareChanges";
			public static final String ORIGINAL_SEGMENT_TOTAL = "originalSegTotal";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface CreditCarryForward {
			public static final String CR_CARRY_FORWARD_INFORMATION = "creditCarryForwardInfo";
			public static final String IP_ADDDRESS = "ipAddress";
		}

		public interface CreditBroughtForward {
			public static final String CR_BROUGHT_FORWARD_INFORMATION = "creditBroughtForwardInfo";
			public static final String IP_ADDDRESS = "ipAddress";
		}

		public interface CanceledReservation {
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String CANCEL_INFO = "cancelInfo";
			public static final String TOTAL_CHARGES = "totalCharges";
			public static final String TOTAL_PAYMENTS = "totalPayments";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface AddedNewPassenger {
			public static final String PASSENGER_DETAILS = "passengerDetails";
			public static final String FORCE_CONFIRM = "forceConfirm";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface RemovedPassenger {
			public static final String PASSENGER_DETAILS = "passengerDetails";
			public static final String NEW_PNR = "newPnr";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface SplitReservation {
			public static final String NEW_PNR = "newPnr";
			public static final String SPLIT_PASSENGERS = "splitPassengers";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface AddedSplitReservation {
			public static final String OLD_PNR = "oldPnr";
			public static final String PASSENGER_DETAILS = "passengerDetails";
			public static final String BOOKING_TYPE = "bookingType";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface AddedReservationForRemovedPassengers {
			public static final String OLD_PNR = "oldPnr";
			public static final String PASSENGER_DETAILS = "passengerDetails";
			public static final String BOOKING_TYPE = "bookingType";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface OwnershipTransfered {
			public static final String CURRENT_OWNER = "currOwner";
			public static final String NEW_OWNER = "newOwner";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface ExtendedOnhold {
			public static final String NEW_DURATION = "newDuration";
			public static final String NEW_RELEASE_TIME = "newReleaseTime";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface ChangedWLPriority {
			public static final String NEW_PRIORITY = "newPriority";
			public static final String OLD_PRIORITY = "oldPriority";
			public static final String SEGMENT_INFORMATION = "segmentInformation";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface ChangedContactDetails {
			public static final String CONTACT_DETAILS = "contactDetails";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface UpdatedETicket {
			public static final String PNR_PAX_ID = "pnrPaxId";
			public static final String ETICKET_NUMBER = "eTicketNumber";
			public static final String FLIGHT_NUMBER = "flightNumber";
			public static final String FLIGHT_DEPARTURE_DATE = "flightDepartureDate";
			public static final String SEGMENT_CODE = "segmentCode";
			public static final String STATUS = "status";
			public static final String PREVIOUS_STATUS = "previousStatus";
			public static final String PASSENGER_NAME = "passengerName";
			public static final String PREVIOUS_PAX_STATUS = "previousPaxStatus";
			public static final String PAX_STATUS = "paxStatus";

		}

		public interface ChangedPassengerDetails {
			public static final String PASSENGER_DETAILS = "passengerDetails";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface CreditAdjusted {
			public static final String AMOUNT = "amount";
			public static final String NEW_PAX_BALANCE = "newPnrPaxBalance";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface ChargesReversed {
			public static final String CONTENT = "auditContent";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface ModifiedSegment {
			public static final String OLD_SEGMENT_INFORMATION = "oldSegmentInformation";
			public static final String NEW_SEGMENT_INFORMATION = "newSegmentInformation";
			public static final String FARE_CHANGES = "fareChanges";
			public static final String ORIGINAL_SEGMENT_TOTAL = "originalSegTotal";
			public static final String FORCE_CONFIRM = "forceConfirm";
			public static final String VACATED_SEATS = "vacatedSeats";
			public static final String VACATED_MEALS = "vacatedMeals";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface AddedConfirmedReservation {
			public static final String PASSENGER_DETAILS = "passengerDetails";

			public static final String FIRST_NAME = "contactFirstName";
			public static final String LAST_NAME = "contactLastName";
			public static final String STREET_ADDRESS_1 = "contactStreetAddress1";
			public static final String STREET_ADDRESS_2 = "contactStreetAddress2";
			public static final String CITY = "contactCity";
			public static final String STATE = "contactState";
			public static final String COUNTRY = "contactCountry";
			public static final String PHONE_NUMBER = "contactPhoneNumber";
			public static final String MOBILE_NUMBER = "contactMobileNumber";
			public static final String FAX = "contactFax";
			public static final String EMAIL = "contactEmail";
			public static final String NATIONALITY_CODE = "contactNationalityCode";

			public static final String TOTAL_CHARGES = "totalCharges";
			public static final String TOTAL_PAYMENTS = "totalPayments";

			public static final String FORCE_CONFIRM = "forceConfirm";
			public static final String BOOKING_TYPE = "bookingType";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String DATE_OF_BIRITH = "DOB";
			public static final String SSR = "SSR";
			public static final String SSR_REMARKS = "SSR Remarks";
			public static final String FLIGHT_SEGMENT_DETAILS = "flightSegmentDetails";
			public static final String ORIGIN_COUNTRY_OF_CALL = "originCountryOfCall";


		}

		public interface AddedOnHoldReservation {
			public static final String PASSENGER_DETAILS = "passengerDetails";

			public static final String FIRST_NAME = "contactFirstName";
			public static final String LAST_NAME = "contactLastName";
			public static final String STREET_ADDRESS_1 = "contactStreetAddress1";
			public static final String STREET_ADDRESS_2 = "contactStreetAddress2";
			public static final String CITY = "contactCity";
			public static final String STATE = "contactState";
			public static final String COUNTRY = "contactCountry";
			public static final String PHONE_NUMBER = "contactPhoneNumber";
			public static final String MOBILE_NUMBER = "contactMobileNumber";
			public static final String FAX = "contactFax";
			public static final String EMAIL = "contactEmail";
			public static final String NATIONALITY_CODE = "contactNationalityCode";
			public static final String RELEASE_TIME = "releaseTime";
			public static final String TOTAL_CHARGES = "totalCharges";
			public static final String BOOKING_TYPE = "bookingType";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String FLIGHT_SEGMENT_DETAILS = "flightSegmentDetails";
			public static final String ORIGIN_COUNTRY_OF_CALL = "originCountryOfCall";
		}

		public interface PaymentMade {
			public static final String PASSENGERS_PAYMENTS = "passengersPayments";
			public static final String FORCE_CONFIRM = "forceConfirm";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface MadeRefund {
			public static final String PASSENGER_DETAILS = "passengerDetails";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String REFUND_TYPE = "refundType";
			public static final String REFUND_TYPE_AUTO = "AUTO REFUND";
			public static final String REFUND_TYPE_MANUAL = "MANUAL REFUND";

		}

		public interface PaymentError {
			public static final String AMOUNT = "amount";
			public static final String ERROR_FUNCTION = "errorFunction";
			public static final String ERROR_DESCRIPTION = "errorDescription";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface MigrationModification {
			public static final String IP_ADDDRESS = "ipAddress";
		}

		public interface ClearedAlert {
			public static final String ALERT_DETAILS = "alertContent";
			public static final String IP_ADDDRESS = "ipAddress";
		}

		public interface CreatedAlert {
			public static final String ALERT_DETAILS = "alertContent";
			public static final String IP_ADDDRESS = "ipAddress";
		}

		public interface ViewedReservation {
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface TransferedSegment {
			public static final String OLD_FLIGHT_SEGMENT = "oldSegmentCode";
			public static final String OLD_FLIGHT_NUMBER = "oldFlightNo";
			public static final String OLD_FLIGHT_DATE = "oldFlightDate";
			public static final String NEW_FLIGHT_SEGMENT = "newSegmentCode";
			public static final String NEW_FLIGHT_NUMBER = "newFlightNo";
			public static final String NEW_FLIGHT_DATE = "newFlightDate";
			public static final String CANCELED_MEALS = "canceledMeals";
			public static final String CANCELED_SEATS = "canceledSeats";
		}

		public interface EmailItinerary {
			public static final String SENT_STATUS = "sentStatus";
			public static final String STATUS_REASONS = "statusReasons";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface EmailFlightAlterations {
			public static final String SENT_STATUS = "sentStatus";
			public static final String STATUS_REASONS = "statusReasons";
			public static final String FLIGHT_ALTERATIONS = "flightAlterations";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface TransferInterlinedSegment {
			public static final String TRANSFER_PNR = "transferPNR";
			public static final String SEGMENT_INFO = "segmentInfo";
			public static final String CHARGES_INFO = "chargesInfo";
			public static final String PAYMENTS_INFO = "paymentsInfo";
		}

		public interface Seats {
			public static final String SEATS = "seats";
			public static final String REMOVED = "removed";
			public static final String ADDED = "added";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface Meals {
			public static final String MEALS = "meals";
			public static final String REMOVED = "removed";
			public static final String ADDED = "added";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface AirportTransfers {
			public static final String AIRPORTTRANSFERS = "airportTransfers";
			public static final String REMOVED = "removed";
			public static final String ADDED = "added";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface AutomaticCheckins {
			public static final String AUTOMATICCHECKINS = "automaticCheckins";
			public static final String REMOVED = "removed";
			public static final String ADDED = "added";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface Baggages {
			public static final String BAGGAGES = "baggages";
			public static final String REMOVED = "removed";
			public static final String ADDED = "added";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface SSRs {
			public static final String SSRS = "inflight services";
			public static final String REMOVED = "removed";
			public static final String ADDED = "added";
			public static final String UPDATED = "updated";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface ReinstateCredit {
			public static final String PAX_REINSTATED_AMUONT = "reInstatedAmount";
			public static final String PAX_NEW_BALANCE = "newBalance";
			public static final String PAX_NEW_EXP_DATE = "paxNewExpDate";
			public static final String PAX_NAME = "paxName";
		}

		public interface ExtendCreditExpiry {
			public static final String PAX_NAME = "paxName";
			public static final String PAX_NEW_EXP_DATE = "paxNewExpDate";
		}

		public interface Insurance {
			public static final String INSURANCE = "insurance";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface ChangedExternalSegment {
			public static final String MODIFIED_SEGMENT_INFORMATION = "modifiedSegmentInfo";
			public static final String ADDED_SEGMENT_INFORMATION = "addedSegmentInfo";
			public static final String IP_ADDDRESS = "ipAddress";
		}

		public interface NotifyPnr {
			public static final String SENT_STATUS = "sentStatus";
			public static final String STATUS_REASONS = "statusReasons";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String NOTIFICATION_CODE = "notificationCode";
			public static final String NOTIFICATION_RECEPIENT = "recepient";
			public static final String NOTIFICATION_CHANNEL = "notificationChannel";
			public static final String NOTIFICATION_CONTENT = "notificationContent";
		}

		public interface OpenReturnConfirmation {
			public static final String SEGMENT_INFORMATION = "segmentInformation";
		}

		public interface FareDiscount {
			public static final String FARE_DISCOUNT_INFO = "fareDiscountInfo";
		}

		public interface EmailReminder {
			public static final String EMAIL_ADDRESS = "emailAddress";
			public static final String SENT_STATUS = "sentStatus";
			public static final String FAILURE_REASONS = "failureReason";
		}

		public interface Flexibilities {
			public static final String FLEXIBILITIES = "flexibilities";
			public static final String FLEXIBILITIES_INFO = "flexibilitiesInfo";
		}

		public interface ItineraryFareMask {
			public static final String ITINERARY_FARE_MAKS = "itineraryFareMask";
		}

		public interface PFSEntry {
			public static final String PAX_STATUS = "paxStatus";
			public static final String PAX_NAME = "paxName";
			public static final String E_TICKET_NO = "eticketNo";
			public static final String FLIGHT_NO = "flightNo";
			public static final String SEGMENT = "segment";
			public static final String BOOKING_CLASS = "bookingclass";
			public static final String TRAVEL_DATE = "travelDate";
			public static final String BOOKED_DATE = "bookedDate";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String REMOTE_USER = "remoteUser";
			public static final String PROCESS_ERROR = "processError";
			public static final String GOSHOW_AGENT = "goShowAgent";
			public static final String TOTAL_CHARGES = "totalCharges";
		}

		public interface ETLEntry {
			public static final String PAX_STATUS = "paxStatus";
			public static final String PAX_NAME = "paxName";
			public static final String E_TICKET_NO = "eticketNo";
			public static final String COUPON_ID = "couponId";
			public static final String FLIGHT_NO = "flightNo";
			public static final String SEGMENT = "segment";
			public static final String BOOKING_CLASS = "bookingclass";
			public static final String TRAVEL_DATE = "travelDate";
			public static final String BOOKED_DATE = "bookedDate";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String REMOTE_USER = "remoteUser";
			public static final String PROCESS_ERROR = "processError";
		}

		public interface PRLEntry {
			public static final String PAX_STATUS = "paxStatus";
			public static final String PAX_NAME = "paxName";
			public static final String E_TICKET_NO = "eticketNo";
			public static final String FLIGHT_NO = "flightNo";
			public static final String SEGMENT = "segment";
			public static final String BOOKING_CLASS = "bookingclass";
			public static final String TRAVEL_DATE = "travelDate";
			public static final String BOOKED_DATE = "bookedDate";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String REMOTE_USER = "remoteUser";
			public static final String PROCESS_ERROR = "processError";
		}

		public interface OHDRollForward {
			public static final String SOURCE_PNR = "sourcePnr";
			public static final String NEW_PNRS = "newPnrs";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String REMOTE_USER = "remoteUser";
		}

		public interface OnholdAlert {
			public static final String EMAIL_ADDRESS = "emailAddress";
			public static final String SENT_STATUS = "sentStatus";
			public static final String FAILURE_REASONS = "failureReason";
		}

		public interface BankGuaranteeExpireEmail {
			public static final String EMAIL_ADDRESS = "emailAddress";
			public static final String SENT_STATUS = "sentStatus";
			public static final String FAILURE_REASONS = "failureReason";
		}

		public interface CreditLimitUtilizationEmail {
			public static final String EMAIL_ADDRESS = "emailAddress";
			public static final String SENT_STATUS = "sentStatus";
			public static final String FAILURE_REASONS = "failureReason";
			public static final String UTILIZATION = "utilization";
		}

		public interface ReservationModifyEmailAgent {
			public static final String EMAIL_ADDRESS = "emailAddress";
			public static final String SENT_STATUS = "sentStatus";
			public static final String FAILURE_REASONS = "failureReason";
			public static final String ADDITIONAL_INFO = "additionalInfo";
		}

		public interface PaxCreditAutoRefund {
			public static final String PAX_DETAILS = "passengerDetails";
			public static final String AMOUNT = "amount";
			public static final String TXN_DETAILS = "txnDetails";
			public static final String TXN_STATUS = "status";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String TXN_SUCCESS = "Success";
			public static final String TXN_FAILED = "Failed";
		}

		public interface AutoCancellationEntry {
			public static final String AUTO_CNX_ID = "autoCnxId";
			public static final String EXPIRE_ON = "expireOn";
			public static final String CNX_TYPE = "cnxType";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String REMOTE_USER = "remoteUser";
		}

		public interface PromotionNextSeatFreeApproveRequest {
			public static final String ADDED_SEATS = "addedSeats";
			public static final String REMOVED_SEATS = "removedSeats";
			public static final String PNR = "pnr";
			public static final String APPROVAL_STATUS = "approvalStatus";
			public static final String NUMBEROFSEATS = "numberOfSeats";
			public static final String REFUND_AMOUNT = "refundAmount";
			public static final String PAX_NAME = "paxName";
			public static final String SEG_CODE = "segCode";
		}

		public interface PromotionFlexiDateApproveRequest {
			public static final String PNR = "pnr";
			public static final String APPROVAL_STATUS = "approvalStatus";
			public static final String ORIGINAL_FLIGHT_ID = "originalFlightNo";
			public static final String ORIGINAL_DEPARTURE_DATE = "originalDepartureDate";
			public static final String TRANSFERD_FLIGHT_ID = "transferdFlightNo";
			public static final String TRANSFERD_DEPARTURE_DATE = "transferdDepartureDate";
		}

		public interface PromotionFlexiDateSubsciption {
			public static final String DIRECTION = "direction";
			public static final String REQUEST_STATUS = "requestStatus";
			public static final String LOWER_BOUND = "lowerBound";
			public static final String UPPER_BOUND = "upperBound";
		}

		public interface UpdateFlightTime {
			public static final String FLIGHT_NO = "flightNo";
			public static final String SEGMENT_CODE = "segmentCode";
			public static final String DEPARTURE_DATE = "deptDate";
			public static final String ARRIVAL_DATE = "arrDate";
			public static final String USER_ID = "userId";
		}

		public interface OverrideCNX_MODCharge {
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String MODE = "strMode";
			public static final String ROUTE_TYPE = "routeType";
			public static final String OVERRIDE_BASIS = "overrideBasis";
			public static final String ADULT_CNX_CHARGE = "adultCnxCharge";
			public static final String Adult_CNX_DEFAULT_CHARGE = "adultCnxDef";
			public static final String ADULT_CNX_CHARGE_MIN = "adultCnxChargeMin";
			public static final String ADULT_CNX_CHARGE_MAX = "adultCnxChargeMAX";
			public static final String ADULT_CNX_CHARGE_PERCENTAGE = "adultCnxChargePercentage";
			public static final String CHILD_CNX_CHARGE = "childCnxCharge";
			public static final String CHILD_CNX_DEFAULT_CHARGE = "childCnxDef";
			public static final String CHILD_CNX_CHARGE_MIN = "childCnxChargeMin";
			public static final String CHILD_CNX_CHARGE_MAX = "childCnxChargeMAX";
			public static final String CHILD_CNX_CHARGE_PERCENTAGE = "childCnxChargePercentage";
			public static final String INFANT_CNX_CHARGE = "infantCnxCharge";
			public static final String INFANT_CNX_CHARGE_MIN = "infantCnxChargeMin";
			public static final String INFANT_CNX_CHARGE_MAX = "infantCnxChargeMAX";
			public static final String INFANT_CNX_CHARGE_PERCENTAGE = "infantCnxChargePercentage";

		}

		public interface AncillaryModificationPenalty {
			public static final String ANCILLARY_MODIFICATION_PENALTY = "ancillaryModificationPenalty";
			public static final String ANCILLARY_PENALTY_AMOUNT = "ancillaryPenaltyAmount";
		}

		public interface ExchangeSegment {
			public static final String SEGMENT_INFORMATION = "segmentInformation";
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
		}

		public interface RemoveFFIDFromUnflownPNRs {
			public static final String FFID = "airRewardsId";
			public static final String PAX_NAME = "paxName";
		}

		public interface AddUserNote {
			public static final String IP_ADDDRESS = "ipAddress";
			public static final String ORIGIN_CARRIER = "originCarrierCode";
			public static final String REMOTE_USER = "remoteUser";
		}

		public interface TaxInvoiceGenerated {
			public static final String TAX_INVOICE = "taxInvoice";
		}
	}
}
