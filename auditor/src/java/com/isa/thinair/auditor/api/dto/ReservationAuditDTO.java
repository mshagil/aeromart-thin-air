package com.isa.thinair.auditor.api.dto;

public class ReservationAuditDTO extends AuditDTO {

	private String pnr;

	private Long salesChannelCode;

	private Long customerId;

	public String getPnr() {
		return this.pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Long getSalesChannelCode() {
		return this.salesChannelCode;
	}

	public void setSalesChannelCode(Long salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
}
