package com.isa.thinair.auditor.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.model.AgentAudit;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.model.AuditTemplate;
import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.auditor.api.model.NameChangeChargeAudit;
import com.isa.thinair.auditor.api.model.PFSAudit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorModuleUtils;
import com.isa.thinair.auditor.core.persistence.dao.AuditorDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * 
 * @isa.module.dao-impl dao-name = "auditorDAO"
 */
public class AuditorDAOImpl extends PlatformBaseHibernateDaoSupport implements AuditorDAO {

	public void createAudit(Audit audit) {
		hibernateSave(audit);
	}

	public void createAudit(ReservationAudit reservationAudit) {
		hibernateSave(reservationAudit);
	}

	public void createAudit(InventoryAudit inventoryAudit) {
		save(inventoryAudit);
	}

	public Page getAudits(List criteria, int startIndex, int pageSize, List orderByFields) {
		return getPagedData(criteria, startIndex, pageSize, Audit.class, orderByFields);
	}

	public Page getReservationAudits(List criteria, int startIndex, int pageSize, List orderByFields) {
		return getPagedData(criteria, startIndex, pageSize, ReservationAudit.class, orderByFields);
	}

	public List getAudits(String pnr) {
		String hql = "select resAudit from ReservationAudit resAudit " + " where resAudit.pnr = ? ";
		return getSession().createQuery(hql).setString(0, pnr).list();
	}

	public AuditTemplate getAuditTemplate(String auditorCode) {
		return (AuditTemplate) get(AuditTemplate.class, auditorCode);
	}

	public Collection<InventoryAudit> getInventoryAudit(Integer flightId) {
		Collection<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(flightId);
		Integer ovlFltId = getOverlappingFltId(flightId);

		if (ovlFltId != null) {
			flightIds.add(ovlFltId);
		}

		Map<Integer, String[]> segInvIdWithSegCodeAndLogicalCCCodeMap = getFccSegIds(flightIds);

		if (segInvIdWithSegCodeAndLogicalCCCodeMap.keySet() != null && segInvIdWithSegCodeAndLogicalCCCodeMap.keySet().size() > 0) {

			String hqlSelect = "select invA from InventoryAudit invA " + "where invA.fccSegInvId in ("
					+ Util.buildIntegerInClauseContent(segInvIdWithSegCodeAndLogicalCCCodeMap.keySet())
					+ ") order by invA.auditTime desc";

			List<InventoryAudit> inventoryAudits = find(hqlSelect, InventoryAudit.class);
			// populate the segment codes
			if (inventoryAudits != null && inventoryAudits.size() > 0) {
				for (InventoryAudit inventoryAudit : inventoryAudits) {
					String[] segCodeAndLogicalCCCode = segInvIdWithSegCodeAndLogicalCCCodeMap
							.get(inventoryAudit.getFccSegInvId());
					inventoryAudit.setSegmentCode(segCodeAndLogicalCCCode[0]);
					inventoryAudit.setLogicalCCCode(segCodeAndLogicalCCCode[1]);
				}
			}
			return inventoryAudits;
		}
		return new ArrayList<InventoryAudit>();

	}

	public void save(InventoryAudit inventoryAudit) {

		hibernateSave(inventoryAudit);

	}

	private Integer getOverlappingFltId(Integer flightId) {

		DataSource ds = AuditorModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String sql = "SELECT f.overlap_flight_instance_id " + "  FROM  t_flight f " + " WHERE f.flight_id = " + flightId;

		return (Integer) templete.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Integer letsbeprofessional = null;
				while (rs.next()) {

					letsbeprofessional = new Integer(rs.getInt(1));

				}

				return letsbeprofessional;
			}
		});
	}

	@SuppressWarnings("unchecked")
	private Map<Integer, String[]> getFccSegIds(Collection<Integer> flightIds) {

		DataSource ds = AuditorModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String sql = "SELECT f.fccsa_id, f.segment_code, f.logical_cabin_class_code FROM t_fcc_seg_alloc f WHERE f.flight_id in ("
				+ Util.buildIntegerInClauseContent(flightIds) + ") ";

		return (Map<Integer, String[]>) templete.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String[]> segInvSegCodeWithLCCCode = new HashMap<Integer, String[]>();

				while (rs.next()) {
					String[] segWithLccCode = { rs.getString("segment_code"), rs.getString("logical_cabin_class_code") };
					segInvSegCodeWithLCCCode.put(new Integer(rs.getInt("fccsa_id")), segWithLccCode);
				}

				return segInvSegCodeWithLCCCode;
			}
		});
	}

	@Override
	public void createAudit(AgentAudit agentAudit) {
		hibernateSave(agentAudit);

	}

	@Override
	public void createAudit(PFSAudit pFSAudit) {
		hibernateSave(pFSAudit);
		// String s = pFSAudit.getUserName();
	}

	@Override
	public void deleteAudits(Collection<PFSAudit> pFSAudits) {
		deleteAll(pFSAudits);
	}

	@Override
	public void createAudit(NameChangeChargeAudit nameChangeChargeAudit) {
		hibernateSave(nameChangeChargeAudit);
		// String s = pFSAudit.getUserName();
	}

	@Override
	public Collection<PFSAuditDTO> retrievePFSAudits(long pfsID) {
		String hql = "select pfsAudit from PFSAudit pfsAudit " + " where pfsAudit.pfsID = ? "
				+ "order by pfsAudit.timeStampZulu desc";
		String strPFSID = String.valueOf(pfsID);
		Collection<PFSAudit> pfsAudits = new ArrayList<PFSAudit>();
		Collection<PFSAuditDTO> pfsAudtitDTOs = new ArrayList<PFSAuditDTO>();
		pfsAudits = getSession().createQuery(hql).setString(0, strPFSID).list();

		for (Iterator<PFSAudit> i = pfsAudits.iterator(); i.hasNext();) {

			PFSAudit pfsAudit = (PFSAudit) i.next();
			pfsAudtitDTOs.add(new PFSAuditDTO(pfsAudit));
		}
		return pfsAudtitDTOs;
	}

	@Override
	public Collection<PFSAudit> getPFSAudits(long pfsID) {
		String hql = "select pfsAudit from PFSAudit pfsAudit " + " where pfsAudit.pfsID = ? ";
		List<PFSAudit> pfsAuditCol = find(hql, new Object[] { new Long(pfsID) }, PFSAudit.class);
		return pfsAuditCol;
	}

	@Override
	public void updateReservationAudit(ReservationAudit reservationAudit) {
		hibernateSaveOrUpdate(reservationAudit);
	}

	@Override
	public ReservationAudit getLastReservationAudit(String pnr){
		
		ReservationAudit reservationAudit = null;
		
		List<ReservationAudit> list = null;

		String hql = "select reservationAudit from ReservationAudit as reservationAudit where reservationAudit.pnr=?"
						+ " and reservationAudit.userNote is not null order by reservationAudit.auditId desc";
		Object[] parmas = { pnr };

		list = find(hql, parmas, ReservationAudit.class);

		if (list.size() != 0) {
			reservationAudit = (ReservationAudit) list.get(0);
		}
		
		return reservationAudit;
	}
}
