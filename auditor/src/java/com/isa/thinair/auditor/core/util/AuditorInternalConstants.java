/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.auditor.core.util;

public abstract class AuditorInternalConstants {

	public static String MODULE_NAME = "auditor";

	public static interface DAOProxyNames {
		public static String AUDITOR_DAO = "auditorDAOProxy";
		public static String AUDITOR_JDBC_DAO = "auditorJdbcDAO";
	}

	public static interface UserNoteParams {
		public static String PUBLIC = "PUB";
		public static String PRIVATE = "PVT";
	}
}