package com.isa.thinair.auditor.core.bl;

import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.model.PFSAudit;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class PFSAuditBL {

	public static void preparePFSAudit(PFSAuditDTO pfsAuditDTO, long pfsID, String action, String systemNote)
			throws ModuleException {

		try {
			if (pfsID != 0 && pfsAuditDTO != null && action != null && !action.equals("")) {
				pfsAuditDTO.setPfsID(pfsID);
				pfsAuditDTO.setAction(action);

				PFSAudit pfsAudit = new PFSAudit();
				pfsAudit.setPfsID(pfsID);
				pfsAudit.setTimeStampZulu(CalendarUtil.getCurrentSystemTimeInZulu());
				pfsAudit.setAction(action);
				pfsAudit.setUserNote(pfsAuditDTO.getUserNote());
				pfsAudit.setUserName(pfsAuditDTO.getUserName());
				pfsAudit.setIpAddress(pfsAuditDTO.getIpAddress());
				pfsAudit.setSystemNote(systemNote);

				new AuditorBL().doAudit(pfsAudit);
			}

		} catch (ModuleException e) {
			throw e;
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

}
