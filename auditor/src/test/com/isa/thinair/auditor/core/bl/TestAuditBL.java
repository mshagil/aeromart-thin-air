package com.isa.thinair.auditor.core.bl;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Iterator;

import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.platform.api.util.PlatformTestCase;


public class TestAuditBL extends PlatformTestCase {
    
    private static final String PARAM_SEPARATOR = "||";
    
    private static final String VALUE_SEPARATOR = "::";
    
    private static final String PARAM_VALUE_SEPARATOR = ":=";
    
    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testDoAudit() throws Exception {
    	
    	AuditTemplateEnum auditTemplateEnum =  AuditTemplateEnum.REMOVED_PASSENGER;
    	
    	ReservationAudit reservationAudit = new ReservationAudit();
    	reservationAudit.setModificationType(auditTemplateEnum.getCode());
//    	reservationAudit.setAppCode("APP Code");
//    	reservationAudit.setCustomerId(new Long(31));
//    	reservationAudit.setPnr("9HDO1B");
//    	reservationAudit.setSalesChannelCode(new Long(4));
//    	reservationAudit.setTemplateCode(auditTemplateEnum.getCode());
//    	reservationAudit.setTimestamp(new Date());
//    	reservationAudit.setUserId("SYSTEM");    	
//    	reservationAudit.setUserNotes("User Notes HERE");
    	
    	LinkedHashMap contents = new LinkedHashMap();
    	contents.put(AuditTemplateEnum.TemplateParams.RemovedPassenger.IP_ADDDRESS,"10.200.2.300");
    	//contents.put(AuditTemplateEnum.TemplateParams.RemovedPassenger.NEW_PNR,"NEWPNR");
    	contents.put(AuditTemplateEnum.TemplateParams.RemovedPassenger.PASSENGER_DETAILS,"Thejaka UA");
    	//contents.put(AuditTemplateEnum.TemplateParams.RemovedPassenger.PASSENGER_TYPE,"PAssenger Type is here");
    	
    	AuditorBL auditorBL = new AuditorBL();

    	auditorBL.doAudit(reservationAudit, contents);
    	
    }

//    public void testConstructContent() {
//        String auditContent = "";
//
//        Object obj;
//        Iterator it;
//        String[] values; 
//        String paramKey;
//        String paramValue = "";
//        int noOfRecs = 0;
//        
//        LinkedHashMap contents = new LinkedHashMap();
//        
//        /*String[] contentSeg = new String[] {"new seg 1 <> new seg 2", "old seg 1 <> old seg 2"};
//        String[] contentPax = new String[] {"new name" , "old name"};
//        contents.put("Seg", contentSeg);
//        contents.put("Pax", contentPax);*/
//        
//        
//        /*
//        String[] contentSeg = new String[] {"new seg 1"};
//        String[] contentPax = new String[] {"new name"};
//        contents.put("Seg", contentSeg);
//        contents.put("Pax", contentPax);        
//        */
//        
//        /*
//        String[] contentSeg = new String[] {"new seg 1 <> new seg 2", "old seg 1 <> old seg 2"};
//        contents.put("Seg", contentSeg);
//        */        
//
//        /*
//        String[] contentSeg = new String[] {"new seg 1", "old seg 1"};
//        contents.put("Seg", contentSeg);
//        */   
//        
//        /*
//        String[] contentSeg = new String[] {"new seg 1"};
//        contents.put("Seg", contentSeg);
//        */ 
//        
//        
//        String singleContent = "value 1";
//        contents.put("Key1", singleContent);
//        contents.put("Key2", "value 2");
//         
//        if ((contents != null) && (!contents.isEmpty())) {
//            it = contents.keySet().iterator();
//            while (it.hasNext()) {
//                paramKey = (String) it.next();
//                paramValue = "";                
//                if ((paramKey != null) && (!paramKey.trim().equals(""))) {
//                    obj = contents.get(paramKey);
//                    if ((obj instanceof String)) {
//                        paramValue = (String) obj; 
//                    } else if (obj instanceof String[]) {
//                        values = (String[]) obj;
//                        if ((values != null) && (values.length > 0)) {
//                            noOfRecs = values.length;
//                            for (int i = (noOfRecs - 1); i >= 0; i--) {
//                                paramValue += values[i]; 
//                                if (i != 0) {                     
//                                    paramValue += VALUE_SEPARATOR;
//                                }
//                            }
//                        }
//                    }
//                    if ((paramValue != null) && (!paramValue.trim().equals(""))) {
//                        if ((auditContent != null) 
//                                && (!auditContent.trim().equalsIgnoreCase(""))) {
//                            auditContent += PARAM_SEPARATOR;
//                        }
//                        auditContent += 
//                            paramKey + PARAM_VALUE_SEPARATOR + paramValue;
//                    }
//                }
//                
//            }
//            
//        }
//        System.out.println(auditContent);
//    }
    
}
