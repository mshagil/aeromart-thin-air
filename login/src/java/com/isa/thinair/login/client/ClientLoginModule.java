package com.isa.thinair.login.client;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Mohamed Nasly
 */
public class ClientLoginModule {

	private static Log log = LogFactory.getLog(ClientLoginModule.class);

	/** JAAS Login configuration file */
	private static final String DEFAULT_LOGING_CONFIG = System.getProperty("repository.home")
			+ "/repository/modules/login/jass_client_login.config";

	private static final String DEFAULT_CONFIGURATION_NAME = "jbossClientLogin";

	private LoginContext lc;

	private String userName;

	public void login(String userName, String password, String loginConfigFile, String configurationName) throws ModuleException {
		this.userName = userName;

		if (loginConfigFile == null) {
			System.setProperty("java.security.auth.login.config", DEFAULT_LOGING_CONFIG);
		} else {
			System.setProperty("java.security.auth.login.config", loginConfigFile);
		}

		/** populate the callback object with username and password */
		ClientCallbackHandler handler = new ClientCallbackHandler(this.userName, password);

		try {
			lc = new LoginContext(configurationName == null ? DEFAULT_CONFIGURATION_NAME : configurationName, handler);
			lc.login();
			log.info("Client login SUCCESSFUL [username=" + this.userName + "]");
		} catch (LoginException le) {
			log.error("Client login FAILED [username=" + this.userName + "]", le);
			throw new ModuleException(le, "login.client.failed");
		}
	}

	public void logout() throws ModuleException {
		// When Multiple EJB Application invocations happens there will be multiple login context in place
		// In that case we need to logout the logincontext in order to do a correct authentication/authorization
		if (lc != null) {
			try {
				lc.logout();
				log.info("Client logout SUCCESSFUL [username=" + this.userName + "]");
			} catch (LoginException e) {
				log.error("Client login FAILED [username=" + this.userName + "]", e);
				throw new ModuleException(e, "login.client.failed");
			}
		}
	}
}
