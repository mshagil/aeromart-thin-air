/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.login.util;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Login Utilites which holds DST information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class LoginUtilities {
	/**
	 * Return agent local time
	 * 
	 * @param colUserDST
	 * @param zuluTime
	 * @return local time and agent station code
	 */
	public static Date getAgentLocalTime(Collection colUserDST, Date zuluTime) {
		Iterator itColUserDST = colUserDST.iterator();
		boolean matchFound = false;
		Date localDate = null;
		UserDST userDST;

		while (itColUserDST.hasNext()) {
			userDST = (UserDST) itColUserDST.next();

			if (compareZuluDates(userDST.getDstStartDateTime(), userDST.getDstEndDateTime(), zuluTime)) {
				localDate = CalendarUtil.getLocalDate(zuluTime, userDST.getGmtOffsetMinutes(), userDST.getAdjustMinutes());
				matchFound = true;
				break;
			}
		}

		// If match is not found
		if (matchFound == false) {
			itColUserDST = colUserDST.iterator();
			if (itColUserDST.hasNext()) {
				userDST = (UserDST) itColUserDST.next();
				localDate = CalendarUtil.getLocalDate(zuluTime, userDST.getGmtOffsetMinutes(), 0);
			}
		}

		return localDate;
	}

	/**
	 * Compare Zulu Dates
	 * 
	 * @param from
	 * @param to
	 * @param currentTime
	 * @return
	 */
	private static boolean compareZuluDates(Date from, Date to, Date currentTime) {

		if (from == null || to == null || currentTime == null) {
			return false;
		} else if (currentTime.after(from) && currentTime.before(to)) {
			return true;
		}

		return false;
	}
}
