package com.isa.thinair.login.util;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class LookupUtils {

	private static final String path = "isa:base://modules/login?id=LoginDAO";

	public static LoginDAO LookupDAO() {
		LookupService lookup = LookupServiceFactory.getInstance();
		LoginDAO loginDAO = (LoginDAO) lookup.getBean(path);
		return loginDAO;
	}

}
