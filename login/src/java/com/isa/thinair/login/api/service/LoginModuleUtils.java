package com.isa.thinair.login.api.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.login.core.config.LoginConfig;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class LoginModuleUtils {

	private static Log log = LogFactory.getLog(LoginModuleUtils.class);

	public static IModule lookupModule(String moduleName) {
		return LookupServiceFactory.getInstance().getModule(moduleName);
	}

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getLoginConfig(), "login",
				"login.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getLoginConfig(), "login",
				"login.config.dependencymap.invalid");
	}

	public static CryptoServiceBD getCryptoServiceBD() {
		return (CryptoServiceBD) lookupServiceBD(CryptoConstants.MODULE_NAME, CryptoConstants.BDKeys.CRYPTO_SERVICE);
	}

	public static LoginConfig getLoginConfig() {
		return (LoginConfig) LookupServiceFactory.getInstance().getBean("isa:base://modules/login?id=loginModuleConfig");
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

}
