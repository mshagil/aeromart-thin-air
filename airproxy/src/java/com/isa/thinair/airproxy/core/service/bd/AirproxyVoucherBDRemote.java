package com.isa.thinair.airproxy.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airproxy.api.service.AirproxyVoucherBD;

/**
 * @author chethiya
 *
 */
@Remote
public interface AirproxyVoucherBDRemote extends AirproxyVoucherBD {

}
