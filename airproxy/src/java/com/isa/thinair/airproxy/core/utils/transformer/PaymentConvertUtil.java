package com.isa.thinair.airproxy.core.utils.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonLoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonOfflinePaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCashPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientLMSPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientVoucherPaymentInfo;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServicePenaltyExtChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.AutoCheckinExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class PaymentConvertUtil {

	/**
	 * 
	 * @param paymentAssembler
	 * @param extExternalChgDTOMap
	 * @return
	 */
	public static IPayment populatePerPaxPayment(LCCClientPaymentAssembler paymentAssembler,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap) throws ModuleException {
		IPayment ipaymentPassenger = new PaymentAssembler();

		Collection<ExternalChgDTO> extChgList = new ArrayList<ExternalChgDTO>();

			if(extExternalChgDTOMap !=null){
			for (LCCClientExternalChgDTO paxExtChg : paymentAssembler.getPerPaxExternalCharges()) {

				ExternalChgDTO externalChgDTO = (ExternalChgDTO) extExternalChgDTOMap.get(paxExtChg.getExternalCharges());

				if (externalChgDTO != null) {
					externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
					externalChgDTO.setApplicableChargeRate(paxExtChg.getJourneyType());
					externalChgDTO.setAmountConsumedForPayment(paxExtChg.isAmountConsumedForPayment());
					externalChgDTO.setAmount(paxExtChg.getAmount());
					externalChgDTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
					
					if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()
							&& paxExtChg.getOperationMode() != null) {
						externalChgDTO.setOperationMode(paxExtChg.getOperationMode());
					}					
					
					if (externalChgDTO instanceof SMExternalChgDTO) {
						((SMExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg
								.getFlightRefNumber()));
						((SMExternalChgDTO) externalChgDTO).setSeatNumber(paxExtChg.getCode());
					} else if (externalChgDTO instanceof MealExternalChgDTO) {
						((MealExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg
								.getFlightRefNumber()));
						((MealExternalChgDTO) externalChgDTO).setMealCode(paxExtChg.getCode());
						((MealExternalChgDTO) externalChgDTO).setAnciOffer(paxExtChg.isAnciOffer());
						((MealExternalChgDTO) externalChgDTO).setOfferedTemplateID(paxExtChg.getOfferedAnciTemplateID());
					} else if (externalChgDTO instanceof BaggageExternalChgDTO) {
						((BaggageExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
						((BaggageExternalChgDTO) externalChgDTO).setBaggageCode(paxExtChg.getCode());
						((BaggageExternalChgDTO) externalChgDTO).setOndBaggageChargeGroupId(paxExtChg.getOndBaggageChargeGroupId());
						((BaggageExternalChgDTO) externalChgDTO).setOndBaggageGroupId(paxExtChg.getOndBaggageGroupId());
					} else if (externalChgDTO instanceof FlexiExternalChgDTO) {
						LCCClientFlexiExternalChgDTO paxFlexiChg = (LCCClientFlexiExternalChgDTO) paxExtChg;
						((FlexiExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(paxFlexiChg.getFlightRefNumber()));
						((FlexiExternalChgDTO) externalChgDTO).setChargeCode(paxFlexiChg.getCode());
						for (FlexiInfoTO flexibility : paxFlexiChg.getFlexiBilities()) {
							ReservationPaxOndFlexibilityDTO flexi = new ReservationPaxOndFlexibilityDTO();
							flexi.setAvailableCount(flexibility.getAvailableCount());
							flexi.setStatus(ReservationPaxOndFlexibility.STATUS_ACTIVE);
							flexi.setUtilizedCount(0);
							flexi.setFlexiRateId(flexibility.getFlexiRateId());
							flexi.setFlexibilityTypeId(flexibility.getFlexibilityTypeId());
							((FlexiExternalChgDTO) externalChgDTO).addToReservationFlexibilitiesList(flexi);
						}
					} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
						((SSRExternalChargeDTO) externalChgDTO).setAirportCode(paxExtChg.getAirportCode());
						((SSRExternalChargeDTO) externalChgDTO).setFlightRPH(paxExtChg.getFlightRefNumber());
						((SSRExternalChargeDTO) externalChgDTO).setSegmentSequece(paxExtChg.getSegmentSequence());
						((SSRExternalChargeDTO) externalChgDTO).setApplyOn((paxExtChg.getApplyOn() == null)
								? ReservationPaxSegmentSSR.APPLY_ON_SEGMENT
								: paxExtChg.getApplyOn());
						((SSRExternalChargeDTO) externalChgDTO).setSSRText(paxExtChg.getUserNote());

						Integer ssrId = SSRUtil.getSSRId(paxExtChg.getCode());
						((SSRExternalChargeDTO) externalChgDTO).setSSRId(ssrId);
					} else if (externalChgDTO instanceof AirportTransferExternalChgDTO) {
						((AirportTransferExternalChgDTO) externalChgDTO).setFlightRPH(paxExtChg.getFlightRefNumber());
						((AirportTransferExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
						((AirportTransferExternalChgDTO) externalChgDTO).setTransferDate(paxExtChg.getTransferDate());
						((AirportTransferExternalChgDTO) externalChgDTO).setTransferType(paxExtChg.getTransferType());
						((AirportTransferExternalChgDTO) externalChgDTO).setTransferAddress(paxExtChg.getTransferAddress());
						((AirportTransferExternalChgDTO) externalChgDTO).setTransferContact(paxExtChg.getTransferContact());
						((AirportTransferExternalChgDTO) externalChgDTO).setAirportTransferId(paxExtChg.getOfferedAnciTemplateID());
						((AirportTransferExternalChgDTO) externalChgDTO).setAirportCode(paxExtChg.getAirportCode());

					} else if (externalChgDTO instanceof ServiceTaxExtChgDTO) {
						((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(paxExtChg.getFlightRefNumber());
					} else if (externalChgDTO instanceof ServicePenaltyExtChgDTO) {
						((ServicePenaltyExtChgDTO) externalChgDTO).setFlightRefNumber(paxExtChg.getFlightRefNumber());
					} else if (externalChgDTO instanceof ServiceTaxExtCharges) {
						if (!StringUtil.isNullOrEmpty(paxExtChg.getCode())) {
							externalChgDTO = (ServiceTaxExtChgDTO) ((ServiceTaxExtCharges) extExternalChgDTOMap
									.get(paxExtChg.getExternalCharges())).getRespectiveServiceChgDTO(paxExtChg.getCode());							
							if (externalChgDTO != null) {										
								externalChgDTO = (ServiceTaxExtChgDTO) externalChgDTO.clone();								
								((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(paxExtChg.getFlightRefNumber());
								((ServiceTaxExtChgDTO) externalChgDTO).setChargeCode(paxExtChg.getCode());
								((ServiceTaxExtChgDTO) externalChgDTO).setTicketingRevenue(paxExtChg.isTicketingRevenue());
								((ServiceTaxExtChgDTO) externalChgDTO).setTaxableAmount(paxExtChg.getTaxableAmount());
								((ServiceTaxExtChgDTO) externalChgDTO).setNonTaxableAmount(paxExtChg.getNonTaxableAmount());
								((ServiceTaxExtChgDTO) externalChgDTO).setAmount(paxExtChg.getAmount());
								((ServiceTaxExtChgDTO) externalChgDTO).setTaxDepositCountryCode(paxExtChg.getTaxDepositCountryCode());
								((ServiceTaxExtChgDTO) externalChgDTO).setTaxDepositStateCode(paxExtChg.getTaxDepositStateCode());
								((ServiceTaxExtChgDTO) externalChgDTO).setServiceTaxAppliedToCCFee(paxExtChg.isServiceTaxAppliedToCCFee());
							}

						}
					} else if (externalChgDTO instanceof AutoCheckinExternalChgDTO) {
						((AutoCheckinExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
						((AutoCheckinExternalChgDTO) externalChgDTO).setAutoCheckinId(paxExtChg.getCode() != null ? Integer
								.valueOf(paxExtChg.getCode()) : 0);
						((AutoCheckinExternalChgDTO) externalChgDTO).setSeatTypePreference(paxExtChg.getSeatTypePreference());
						((AutoCheckinExternalChgDTO) externalChgDTO).setEmail(paxExtChg.getEmail());
						((AutoCheckinExternalChgDTO) externalChgDTO).setSeatCode(paxExtChg.getSeatCode());
					}
					extChgList.add(externalChgDTO);
				}
			}
		}
		ipaymentPassenger.addExternalCharges(extChgList);

		BigDecimal consumedExternalCharges = paymentAssembler.getTotalConsumedExternalCharges();
		for (LCCClientPaymentInfo payment : paymentAssembler.getPayments()) {

			BigDecimal totalWOExtCharges = BigDecimal.ZERO;
			if (payment.includeExternalCharges()) {
				totalWOExtCharges = AccelAeroCalculator.subtract(payment.getTotalAmount(), consumedExternalCharges);
			} else {
				totalWOExtCharges = payment.getTotalAmount();
			}

			Integer paymentTnxId = (payment.getPaymentTnxReference() != null && !payment.getPaymentTnxReference().isEmpty())
					? Integer.valueOf(payment.getPaymentTnxReference())
					: null;

			if (payment instanceof CommonLoyaltyPaymentInfo) {
				CommonLoyaltyPaymentInfo payOpt = (CommonLoyaltyPaymentInfo) payment;
				ipaymentPassenger.addLoyaltyCreditPayment(payOpt.getAgentCode(), totalWOExtCharges, payOpt.getLoyaltyAccount(),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);
			} else if (payment instanceof CommonCreditCardPaymentInfo) {
				CommonCreditCardPaymentInfo payOpt = (CommonCreditCardPaymentInfo) payment;
				ipaymentPassenger.addCardPayment(payOpt.getType(), payOpt.geteDate(), payOpt.getNo(), payOpt.getName(),
						payOpt.getAddress(), payOpt.getSecurityCode(), totalWOExtCharges, payOpt.getAppIndicator(),
						payOpt.getTnxMode(), payOpt.getOldCCRecordId(), payOpt.getIpgIdentificationParamsDTO(),
						payOpt.getPayReference(), payOpt.getActualPaymentMethod(), payOpt.getCarrierCode(),
						payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), payOpt.getAuthorizationId(),
						payOpt.getPaymentBrokerId(),
						payOpt.getUserInputDTO(), paymentTnxId);
			} else if (payment instanceof LCCClientCashPaymentInfo) {
				LCCClientCashPaymentInfo payOpt = (LCCClientCashPaymentInfo) payment;
				ipaymentPassenger.addCashPayment(totalWOExtCharges, payOpt.getPayReference(), payOpt.getActualPaymentMethod(),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);

			} else if (payment instanceof LCCClientOnAccountPaymentInfo) {
				LCCClientOnAccountPaymentInfo payOpt = (LCCClientOnAccountPaymentInfo) payment;
				ipaymentPassenger.addAgentCreditPayment(payOpt.getAgentCode(), totalWOExtCharges, payOpt.getPayReference(),
						payOpt.getActualPaymentMethod(), payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(),
						payOpt.getLccUniqueTnxId(), payOpt.getPaymentMethod(), paymentTnxId);

			} else if (payment instanceof LCCClientPaxCreditPaymentInfo) {
				LCCClientPaxCreditPaymentInfo payOpt = (LCCClientPaxCreditPaymentInfo) payment;
				// pass the carrier code and pnr to facilitate any carrier credit utilization
				ipaymentPassenger.addCreditPayment(totalWOExtCharges, String.valueOf(payOpt.getDebitPaxRefNo()),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), payOpt.getPaxSequence(),
						payOpt.getExpiryDate(), payOpt.getResidingPnr(), null, null, paymentTnxId);
			} else if (payment instanceof LCCClientLMSPaymentInfo) {
				LCCClientLMSPaymentInfo payOpt = (LCCClientLMSPaymentInfo) payment;
				ipaymentPassenger.addLMSPayment(payOpt.getLoyaltyMemberAccountId(), payOpt.getRewardIDs(), totalWOExtCharges,
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);
			} else if (payment instanceof CommonOfflinePaymentInfo) {
				CommonOfflinePaymentInfo payOpt = (CommonOfflinePaymentInfo) payment;
				ipaymentPassenger.addOfflinePayment(totalWOExtCharges, payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(),
						payOpt.getIpgIdentificationParamsDTO(), paymentTnxId);
			} else if (payment instanceof LCCClientVoucherPaymentInfo) {
				LCCClientVoucherPaymentInfo payOpt = (LCCClientVoucherPaymentInfo) payment;
				ipaymentPassenger.addVoucherPayment(payOpt.getVoucherDTO(), totalWOExtCharges, payOpt.getCarrierCode(),
						payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);
			}
		}

		return ipaymentPassenger;
	}

	/**
	 * Method is overloaded to identify onHold and ForceConfirm bookings
	 * 
	 * @param paymentAssembler
	 * @param extExternalChgDTOMap
	 * @param isOnHold
	 * @param isForceConfirmed
	 * @return
	 */
	public static IPayment populatePerPaxPayment(LCCClientPaymentAssembler paymentAssembler,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap, boolean isOnHold, boolean isForceConfirmed)
			throws ModuleException {
		IPayment ipaymentPassenger = new PaymentAssembler();

		Collection<ExternalChgDTO> extChgList = new ArrayList<ExternalChgDTO>();

		for (LCCClientExternalChgDTO paxExtChg : paymentAssembler.getPerPaxExternalCharges()) {

			ExternalChgDTO externalChgDTO = (ExternalChgDTO) extExternalChgDTOMap.get(paxExtChg.getExternalCharges());

			if (externalChgDTO != null) {
				externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
				externalChgDTO.setApplicableChargeRate(paxExtChg.getJourneyType());
				externalChgDTO.setAmountConsumedForPayment(paxExtChg.isAmountConsumedForPayment());
				externalChgDTO.setAmount(paxExtChg.getAmount());
				externalChgDTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
				
				if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()
						&& paxExtChg.getOperationMode() != null) {
					externalChgDTO.setOperationMode(paxExtChg.getOperationMode());
				}

				if (externalChgDTO instanceof SMExternalChgDTO) {
					((SMExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg
							.getFlightRefNumber()));
					((SMExternalChgDTO) externalChgDTO).setSeatNumber(paxExtChg.getCode());
				} else if (externalChgDTO instanceof MealExternalChgDTO) {
					((MealExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg
							.getFlightRefNumber()));
					((MealExternalChgDTO) externalChgDTO).setMealCode(paxExtChg.getCode());
					((MealExternalChgDTO) externalChgDTO).setAnciOffer(paxExtChg.isAnciOffer());
					((MealExternalChgDTO) externalChgDTO).setOfferedTemplateID(paxExtChg.getOfferedAnciTemplateID());
				} else if (externalChgDTO instanceof BaggageExternalChgDTO) {
					((BaggageExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
							.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
					((BaggageExternalChgDTO) externalChgDTO).setBaggageCode(paxExtChg.getCode());
					((BaggageExternalChgDTO) externalChgDTO).setOndBaggageChargeGroupId(paxExtChg.getOndBaggageChargeGroupId());
					((BaggageExternalChgDTO) externalChgDTO).setOndBaggageGroupId(paxExtChg.getOndBaggageGroupId());
				} else if (externalChgDTO instanceof FlexiExternalChgDTO) {
					LCCClientFlexiExternalChgDTO paxFlexiChg = (LCCClientFlexiExternalChgDTO) paxExtChg;
					((FlexiExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
							.getSegmentIdFromFlightRPH(paxFlexiChg.getFlightRefNumber()));
					((FlexiExternalChgDTO) externalChgDTO).setChargeCode(paxFlexiChg.getCode());
					for (FlexiInfoTO flexibility : paxFlexiChg.getFlexiBilities()) {
						ReservationPaxOndFlexibilityDTO flexi = new ReservationPaxOndFlexibilityDTO();
						flexi.setAvailableCount(flexibility.getAvailableCount());
						flexi.setStatus(ReservationPaxOndFlexibility.STATUS_ACTIVE);
						flexi.setUtilizedCount(0);
						flexi.setFlexiRateId(flexibility.getFlexiRateId());
						flexi.setFlexibilityTypeId(flexibility.getFlexibilityTypeId());
						((FlexiExternalChgDTO) externalChgDTO).addToReservationFlexibilitiesList(flexi);
					}
				} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
					((SSRExternalChargeDTO) externalChgDTO).setAirportCode(paxExtChg.getAirportCode());
					((SSRExternalChargeDTO) externalChgDTO).setFlightRPH(paxExtChg.getFlightRefNumber());
					((SSRExternalChargeDTO) externalChgDTO).setSegmentSequece(paxExtChg.getSegmentSequence());
					((SSRExternalChargeDTO) externalChgDTO).setApplyOn((paxExtChg.getApplyOn() == null)
							? ReservationPaxSegmentSSR.APPLY_ON_SEGMENT
							: paxExtChg.getApplyOn());
					((SSRExternalChargeDTO) externalChgDTO).setSSRText(paxExtChg.getUserNote());

					Integer ssrId = SSRUtil.getSSRId(paxExtChg.getCode());
					((SSRExternalChargeDTO) externalChgDTO).setSSRId(ssrId);
				} else if (externalChgDTO instanceof AirportTransferExternalChgDTO) {
					((AirportTransferExternalChgDTO) externalChgDTO).setFlightRPH(paxExtChg.getFlightRefNumber());
					((AirportTransferExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
							.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
					((AirportTransferExternalChgDTO) externalChgDTO).setTransferDate(paxExtChg.getTransferDate());
					((AirportTransferExternalChgDTO) externalChgDTO).setTransferType(paxExtChg.getTransferType());
					((AirportTransferExternalChgDTO) externalChgDTO).setTransferAddress(paxExtChg.getTransferAddress());
					((AirportTransferExternalChgDTO) externalChgDTO).setTransferContact(paxExtChg.getTransferContact());
					((AirportTransferExternalChgDTO) externalChgDTO).setAirportTransferId(paxExtChg.getOfferedAnciTemplateID());
					((AirportTransferExternalChgDTO) externalChgDTO).setAirportCode(paxExtChg.getAirportCode());

				} else if (externalChgDTO instanceof ServiceTaxExtChgDTO) {
					((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(paxExtChg.getFlightRefNumber());
				} else if (externalChgDTO instanceof ServicePenaltyExtChgDTO) {
					((ServicePenaltyExtChgDTO) externalChgDTO).setFlightRefNumber(paxExtChg.getFlightRefNumber());
				}

				if (((isOnHold || isForceConfirmed) && externalChgDTO.getExternalChargesEnum().equals(
						EXTERNAL_CHARGES.CREDIT_CARD))
						|| ((isOnHold || isForceConfirmed) && externalChgDTO.getExternalChargesEnum().equals(
								EXTERNAL_CHARGES.JN_OTHER))) {

				} else {

					extChgList.add(externalChgDTO);
				}

			}
		}

		ipaymentPassenger.addExternalCharges(extChgList);

		BigDecimal consumedExternalCharges = paymentAssembler.getTotalConsumedExternalCharges();
		for (LCCClientPaymentInfo payment : paymentAssembler.getPayments()) {

			BigDecimal totalWOExtCharges = BigDecimal.ZERO;
			if (payment.includeExternalCharges()) {
				totalWOExtCharges = AccelAeroCalculator.subtract(payment.getTotalAmount(), consumedExternalCharges);
			} else {
				totalWOExtCharges = payment.getTotalAmount();
			}

			Integer paymentTnxId = payment.getPaymentTnxReference() != null
					? Integer.valueOf(payment.getPaymentTnxReference())
					: null;

			if (payment instanceof CommonLoyaltyPaymentInfo) {
				CommonLoyaltyPaymentInfo payOpt = (CommonLoyaltyPaymentInfo) payment;
				ipaymentPassenger.addLoyaltyCreditPayment(payOpt.getAgentCode(), totalWOExtCharges, payOpt.getLoyaltyAccount(),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);
			} else if (payment instanceof CommonCreditCardPaymentInfo) {
				CommonCreditCardPaymentInfo payOpt = (CommonCreditCardPaymentInfo) payment;
				ipaymentPassenger.addCardPayment(payOpt.getType(), payOpt.geteDate(), payOpt.getNo(), payOpt.getName(),
						payOpt.getAddress(), payOpt.getSecurityCode(), totalWOExtCharges, payOpt.getAppIndicator(),
						payOpt.getTnxMode(), payOpt.getOldCCRecordId(), payOpt.getIpgIdentificationParamsDTO(),
						payOpt.getPayReference(), payOpt.getActualPaymentMethod(), payOpt.getCarrierCode(),
						payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), null, payOpt.getPaymentBrokerId(),
						payOpt.getUserInputDTO(), paymentTnxId);
			} else if (payment instanceof LCCClientCashPaymentInfo) {
				LCCClientCashPaymentInfo payOpt = (LCCClientCashPaymentInfo) payment;
				ipaymentPassenger.addCashPayment(totalWOExtCharges, payOpt.getPayReference(), payOpt.getActualPaymentMethod(),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);

			} else if (payment instanceof LCCClientOnAccountPaymentInfo) {
				LCCClientOnAccountPaymentInfo payOpt = (LCCClientOnAccountPaymentInfo) payment;
				ipaymentPassenger.addAgentCreditPayment(payOpt.getAgentCode(), totalWOExtCharges, payOpt.getPayReference(),
						payOpt.getActualPaymentMethod(), payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(),
						payOpt.getLccUniqueTnxId(), payOpt.getPaymentMethod(), paymentTnxId);

			} else if (payment instanceof LCCClientPaxCreditPaymentInfo) {
				LCCClientPaxCreditPaymentInfo payOpt = (LCCClientPaxCreditPaymentInfo) payment;
				// pass the carrier code and pnr to facilitate any carrier credit utilization
				ipaymentPassenger.addCreditPayment(totalWOExtCharges, String.valueOf(payOpt.getDebitPaxRefNo()),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), payOpt.getPaxSequence(),
						payOpt.getExpiryDate(), payOpt.getResidingPnr(), null, null, paymentTnxId);
			} else if (payment instanceof LCCClientLMSPaymentInfo) {
				LCCClientLMSPaymentInfo payOpt = (LCCClientLMSPaymentInfo) payment;
				ipaymentPassenger.addLMSPayment(payOpt.getLoyaltyMemberAccountId(), payOpt.getRewardIDs(), totalWOExtCharges,
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);
			} else if (payment instanceof LCCClientVoucherPaymentInfo) {
				LCCClientVoucherPaymentInfo payOpt = (LCCClientVoucherPaymentInfo) payment;
				ipaymentPassenger.addVoucherPayment(payOpt.getVoucherDTO(), totalWOExtCharges, payOpt.getCarrierCode(),
						payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);
			}
		}

		return ipaymentPassenger;
	}

	public static CardPaymentInfo convertCreditCardPayment(CommonCreditCardPaymentInfo commonInfo) {
		CardPaymentInfo cardPayInfo = new CardPaymentInfo();
		cardPayInfo.setAddress(commonInfo.getAddress());
		cardPayInfo.setAppIndicator(commonInfo.getAppIndicator());
		cardPayInfo.setAuthorizationId(commonInfo.getAuthorizationId());
		cardPayInfo.setEDate(commonInfo.geteDate());
		cardPayInfo.setIpgIdentificationParamsDTO(commonInfo.getIpgIdentificationParamsDTO());
		cardPayInfo.setName(commonInfo.getName());
		cardPayInfo.setNo(commonInfo.getNo());
		cardPayInfo.setNoFirstDigits(commonInfo.getNoFirstDigits());
		cardPayInfo.setNoLastDigits(commonInfo.getNoLastDigits());
		cardPayInfo.setPayCurrencyDTO(commonInfo.getPayCurrencyDTO());
		cardPayInfo.setPaymentBrokerRefNo(commonInfo.getPaymentBrokerId());
		cardPayInfo.setTemporyPaymentId(commonInfo.getTemporyPaymentId());
		cardPayInfo.setTnxMode(commonInfo.getTnxMode());
		cardPayInfo.setType(commonInfo.getType());
		cardPayInfo.setTotalAmount(commonInfo.getTotalAmount());
		cardPayInfo.setSecurityCode(commonInfo.getSecurityCode());
		cardPayInfo.setPnr(commonInfo.getGroupPNR());
		cardPayInfo.setSuccess(commonInfo.isPaymentSuccess());
		return cardPayInfo;

	}

	public static CommonReservationAssembler combineActualCreditPayments(
			Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers,
			CommonReservationAssembler lcclientReservationAssembler) {
		for (LCCClientReservationPax reservationPax : lcclientReservationAssembler.getLccreservation().getPassengers()) {
			LCCClientPaymentAssembler lccClientPaymentAssembler = paxWisePaymentAssemblers.get(reservationPax.getPaxSequence());
			if (lccClientPaymentAssembler != null) {
				reservationPax.getLccClientPaymentAssembler().getPayments().clear();
				// clear the list, because we have all the payments in new assembler payments list
				reservationPax.getLccClientPaymentAssembler().getPayments().addAll(lccClientPaymentAssembler.getPayments());
			}
		}
		return lcclientReservationAssembler;
	}

}
