package com.isa.thinair.airproxy.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airproxy.api.service.AirproxyAutomaticCheckinBD;

@Local
public interface AirproxyAutomaticCheckinBDLocal extends AirproxyAutomaticCheckinBD {

}
