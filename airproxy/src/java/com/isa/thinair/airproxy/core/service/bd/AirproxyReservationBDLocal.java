package com.isa.thinair.airproxy.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airproxy.api.service.AirproxyReservationAllBD;

@Local
public interface AirproxyReservationBDLocal extends AirproxyReservationAllBD {

}
