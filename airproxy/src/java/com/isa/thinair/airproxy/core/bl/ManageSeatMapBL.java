package com.isa.thinair.airproxy.core.bl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * BL to manage Seat Map block, release and related functionality
 * 
 * @author Dilan Anuruddha
 * 
 */
public class ManageSeatMapBL {

	private Log log = LogFactory.getLog(ManageSeatMapBL.class);

	private List<LCCSegmentSeatDTO> segmentSeatDTOs;
	private String transactionIdentifier;
	private SYSTEM system;
	private BasicTrackInfo trackInfo;

	public ManageSeatMapBL(List<LCCSegmentSeatDTO> segmentSeatDTOs, String transactionIdentifier, SYSTEM system,
			BasicTrackInfo trackInfo) {
		super();
		this.segmentSeatDTOs = segmentSeatDTOs;
		this.transactionIdentifier = transactionIdentifier;
		this.system = system;
		this.trackInfo = trackInfo;
	}

	/**
	 * Block seats on seat map
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public boolean blockSeats() throws ModuleException {
		if (system == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCAncillaryBD().blockSeats(segmentSeatDTOs, transactionIdentifier, trackInfo);
		} else {
			return blockOwnAirlineSeats();
		}
	}

	/**
	 * Block Own Airline Seats on seat amp
	 * 
	 * @return success boolean
	 */
	private boolean blockOwnAirlineSeats() {
		boolean blockSuccess = true;
		try {
			SeatMapBD seatMapBD = AirproxyModuleUtils.getSeatMapBD();
			Map<Integer, String> amSeatIdPaxTypeMap = new HashMap<Integer, String>();
			if (segmentSeatDTOs != null) {
				for (LCCSegmentSeatDTO segSeat : segmentSeatDTOs) {

					Integer fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(segSeat.getFlightSegmentTO()
							.getFlightRefNumber());
					for (LCCAirSeatDTO seat : segSeat.getAirSeatDTOs()) {
						Integer flightAMSeatId = seatMapBD.getFlightAmSeatID(fltSegId, seat.getSeatNumber());
						String paxType = seat.getBookedPassengerType();
						if (paxType.compareTo(PaxTypeTO.ADULT) == 0) {
							amSeatIdPaxTypeMap.put(flightAMSeatId, ReservationInternalConstants.PassengerType.ADULT);
						} else if (paxType.compareTo(PaxTypeTO.CHILD) == 0) {
							amSeatIdPaxTypeMap.put(flightAMSeatId, ReservationInternalConstants.PassengerType.CHILD);
						} else if (paxType.compareTo(PaxTypeTO.PARENT) == 0) {
							amSeatIdPaxTypeMap.put(flightAMSeatId, ReservationInternalConstants.PassengerType.PARENT);
						} else {
							throw new ModuleException("Invalid passenger type " + paxType);
						}
					}

				}
			}
			if (amSeatIdPaxTypeMap.size() > 0) {
				ServiceResponce serviceResponce = seatMapBD.blockSeats(amSeatIdPaxTypeMap);
				blockSuccess = serviceResponce.isSuccess();
				if (log.isDebugEnabled()) {
					log.debug("Seatmap blocking " + (serviceResponce.isSuccess() ? "Successful" : "Not Successful"));
				}
			}
		} catch (Exception e) {
			log.error("Air proxy Seat block failed ", e);
			blockSuccess = false;
		}

		return blockSuccess;
	}

	/**
	 * Release seat map
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public boolean releaseSeats() throws ModuleException {
		if (system == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCAncillaryBD().releaseSeats(segmentSeatDTOs, transactionIdentifier, trackInfo);
		} else {
			return releaseOwnAirlineSeats();
		}
	}

	/**
	 * Release own airline seat map
	 * 
	 * @return
	 */
	private boolean releaseOwnAirlineSeats() {
		boolean releaseSuccess = true;
		try {
			SeatMapBD seatMapBD = AirproxyModuleUtils.getSeatMapBD();
			Set<Integer> flightAMSeatIds = new HashSet<Integer>();
			for (LCCSegmentSeatDTO segSeat : segmentSeatDTOs) {
				Integer fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(segSeat.getFlightSegmentTO()
						.getFlightRefNumber());
				for (LCCAirSeatDTO seat : segSeat.getAirSeatDTOs()) {
					Integer flightAMSeatId = seatMapBD.getFlightAmSeatID(fltSegId, seat.getSeatNumber());
					flightAMSeatIds.add(flightAMSeatId);
				}
			}
			if (flightAMSeatIds.size() > 0) {
				ServiceResponce serviceResponce = seatMapBD.releaseBlockedSeats(flightAMSeatIds);
				releaseSuccess = serviceResponce.isSuccess();
				if (log.isDebugEnabled()) {
					log.debug("Seatmap releasing " + (serviceResponce.isSuccess() ? "Successful" : "Not Successful"));
				}
			}
		} catch (Exception e) {
			log.error("Air proxy Seat release failed ", e);
			releaseSuccess = false;
		}
		return releaseSuccess;
	}

}
