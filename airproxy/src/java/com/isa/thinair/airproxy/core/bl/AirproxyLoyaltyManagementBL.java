package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;
import com.isa.thinair.promotion.api.model.lms.FlownFile.PRODUCT_CODE;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateReq;
import com.isa.thinair.promotion.api.to.lms.RedeemLoyaltyPointsReq;

/**
 * 
 * @author rumesh
 * 
 */
public class AirproxyLoyaltyManagementBL {
	private static Log log = LogFactory.getLog(AirproxyLoyaltyManagementBL.class);

	private String memberAccountId;
	private BigDecimal redeemRequestedAmount;
	private Map<String, List<LCCClientExternalChgDTO>> paxExternalCharges;
	private Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges;
	private Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount;
	private FlightPriceRQ flightPriceRQ;
	private Map<String, List<String>> carrierWiseFlightRPH;
	private FareSegChargeTO fareSegChargeTo;
	private boolean isFlexiQuote;
	private SYSTEM system;
	private String txnIdntifier;
	private boolean issueRewardIds;
	private boolean isPayForOHD;
	private String pnr;
    private DiscountRQ discountRQ;
	private Double remainingPoint;
	private String memberExternalId;
	private String memberEnrollingCarrier;

	public Map<Integer, Map<String, Map<String, BigDecimal>>> getPaxProductWiseDueAmount(String pnr, boolean isGroupPNR,
			boolean isLoadLCCRes, double remainingLoyaltyPoints)
			throws ModuleException {

		Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount = new HashMap<Integer, Map<String, Map<String, BigDecimal>>>();

		LCCClientReservation lccClientReservation = null;

		if (isLoadLCCRes) {
			lccClientReservation = loadGroupReservation(pnr);
		}

		if (!isGroupPNR) {
			Map<Integer, Set<ChargesDetailDTO>> paxSeqWiseChargeDue = loadBalanceDueChargeBreakdown(pnr);

			Map<Integer, TreeMap<String, BigDecimal>> paxProductDueAmount = new HashMap<Integer, TreeMap<String, BigDecimal>>();
			List<LoyaltyProduct> loyaltyProducts = AirproxyModuleUtils.getLoyaltyManagementBD().getLoyaltyProducts();

			Map<String, String> externalChargeMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
			Collection<String> externalChargeCodes = externalChargeMap.values();

			for (LoyaltyProduct loyaltyProduct : loyaltyProducts) {
				if (loyaltyProduct.getThresholdValue() == null
						|| (loyaltyProduct.getThresholdValue() != null && loyaltyProduct.getThresholdValue().compareTo(
								remainingLoyaltyPoints) <= 0)) {
					for (Integer paxSeq : paxSeqWiseChargeDue.keySet()) {
						if (!paxProductDueAmount.containsKey(paxSeq)) {
							paxProductDueAmount.put(paxSeq, new TreeMap<String, BigDecimal>());
						}

						Map<String, BigDecimal> productDueAmount = paxProductDueAmount.get(paxSeq);

						BigDecimal productChargeTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
						Collection<ChargesDetailDTO> chargesDetailDTOs = paxSeqWiseChargeDue.get(paxSeq);
						for (ChargesDetailDTO chargesDetailDTO : chargesDetailDTOs) {
							Set<String> loyaltyChargeCodes = loyaltyProduct.getChargeCodes();

							if ((loyaltyChargeCodes == null || loyaltyChargeCodes.isEmpty())
									&& loyaltyProduct.getProductId().equals(PRODUCT_CODE.FLIGHT.toString())) {
								if (!externalChargeCodes.contains(chargesDetailDTO.getChargeCode())
										&& !chargesDetailDTO.isBundleFareFee()
										&& (chargesDetailDTO.getChargeGroupCode().equals(
												ReservationInternalConstants.ChargeGroup.FAR)
												|| chargesDetailDTO.getChargeGroupCode().equals(
														ReservationInternalConstants.ChargeGroup.SUR) || chargesDetailDTO
												.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.INF))) {
									productChargeTotal = AccelAeroCalculator.add(productChargeTotal,
											chargesDetailDTO.getChargeAmount());
								} else if (chargesDetailDTO.getChargeGroupCode().equals(
										ReservationInternalConstants.ChargeGroup.SUR)
										&& externalChargeMap != null
										&& chargesDetailDTO.getChargeCode() != null
										&& externalChargeMap.get(ReservationInternalConstants.EXTERNAL_CHARGES.NAME_CHANGE_CHARGE
												.toString()) != null
										&& externalChargeMap.get(
												ReservationInternalConstants.EXTERNAL_CHARGES.NAME_CHANGE_CHARGE.toString())
												.equals(chargesDetailDTO.getChargeCode())) {
									productChargeTotal = AccelAeroCalculator.add(productChargeTotal,
											chargesDetailDTO.getChargeAmount());

								}
							} else if (loyaltyChargeCodes.contains(chargesDetailDTO.getChargeCode())) {
								productChargeTotal = AccelAeroCalculator.add(productChargeTotal,
										chargesDetailDTO.getChargeAmount());
							}
						}

						if (AccelAeroCalculator.isGreaterThan(productChargeTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
							if (!productDueAmount.containsKey(loyaltyProduct.getProductId())) {
								productDueAmount.put(loyaltyProduct.getProductId(),
										AccelAeroCalculator.getDefaultBigDecimalZero());
							}

							BigDecimal dueAmount = productDueAmount.get(loyaltyProduct.getProductId());
							dueAmount = AccelAeroCalculator.add(dueAmount, productChargeTotal);
							productDueAmount.put(loyaltyProduct.getProductId(), dueAmount);
						}
					}
				}
			}

			if (lccClientReservation != null) {
				for (LCCClientReservationPax lccClientReservationPax : lccClientReservation.getPassengers()) {
					Integer paxSeq = lccClientReservationPax.getPaxSequence();
					if (paxSeq != null && paxProductDueAmount.get(paxSeq) != null && paxProductDueAmount.get(paxSeq).size() > 0) {
						TreeMap<String, BigDecimal> productDueAmounts = paxProductDueAmount.get(paxSeq);
						BigDecimal totalDueAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

						BigDecimal paxDueAmount = lccClientReservationPax.getTotalAvailableBalance();

						if (paxDueAmount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
							TreeMap<String, BigDecimal> updatedProductDueAmounts = new TreeMap<String, BigDecimal>();
							for (String productCode : productDueAmounts.keySet()) {
								BigDecimal productAmount = productDueAmounts.get(productCode);
								totalDueAmount = AccelAeroCalculator.add(totalDueAmount, productAmount);
								if (paxDueAmount.compareTo(totalDueAmount) > 0) {
									updatedProductDueAmounts.put(productCode, productAmount);
								} else if (totalDueAmount.compareTo(paxDueAmount) == 0) {
									updatedProductDueAmounts.put(productCode, productAmount);
									break;
								} else if (paxDueAmount.compareTo(totalDueAmount) < 0) {
									BigDecimal actualDueAmount = AccelAeroCalculator.subtract(paxDueAmount,
											AccelAeroCalculator.subtract(totalDueAmount, productAmount));
									updatedProductDueAmounts.put(productCode, actualDueAmount);
									break;
								}
							}
							paxProductDueAmount.put(paxSeq, updatedProductDueAmounts);
						} else {
							paxProductDueAmount.put(paxSeq, new TreeMap<String, BigDecimal>());
						}

					}
				}
			}

			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			for (Integer paxSeq : paxProductDueAmount.keySet()) {
				Map<String, Map<String, BigDecimal>> carrierProductDueAmount = new HashMap<String, Map<String, BigDecimal>>();
				carrierProductDueAmount.put(carrierCode, paxProductDueAmount.get(paxSeq));
				paxCarrierProductDueAmount.put(paxSeq, carrierProductDueAmount);
			}

		} else {
			// Interline calculation will handle in getLoyaltyRedeemableAmounts service call it self. Pass param
			// IsPayForOHD
		}
		return paxCarrierProductDueAmount;
	}


	public ServiceResponce calculateLoyaltyPointRedeemables(UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		if (system == SYSTEM.AA) {

			Collection<OndFareDTO> ondFareDTOs = null;

			if (flightPriceRQ != null && fareSegChargeTo != null) {
				OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(flightPriceRQ,
						fareSegChargeTo);
				ondFareDTOs = AirproxyModuleUtils.getAirReservationQueryBD().recreateFareSegCharges(ondRebuildCriteria);
			}

			CurrencyExchangeRate currencyExchangeRate = AirproxyModuleUtils.getCommonMasterBD().getExchangeRate(
					AppSysParamsUtil.getLoyaltyManagedCurrencyCode(), CalendarUtil.getCurrentSystemTimeInZulu());

			RedeemCalculateReq redeemCalculateReqTo = new RedeemCalculateReq();
			redeemCalculateReqTo.setMemberAccountId(memberAccountId);
			redeemCalculateReqTo.setRequestedLoyaltyPointsAmount(redeemRequestedAmount);
			redeemCalculateReqTo.setPaxCarrierExternalCharges(paxCarrierExternalCharges);
			redeemCalculateReqTo.setOndFareDTOs(ondFareDTOs);
			redeemCalculateReqTo.setPaxCarrierProductDueAmount(paxCarrierProductDueAmount);
			redeemCalculateReqTo.setLoyaltyPointsToCurrencyConversionRate(AppSysParamsUtil
					.getLoyaltyPointsToCurrencyConversionRate());
			redeemCalculateReqTo.setCurrencyToLoyaltyPointsConversionRate(AppSysParamsUtil
					.getCurrencyToLoyaltyPointsConversionRate());
			redeemCalculateReqTo.setCurrencyExRate(currencyExchangeRate);
			redeemCalculateReqTo.setDiscountRQ(discountRQ);
			redeemCalculateReqTo.setMemberExternalId(memberExternalId);
			
			return AirproxyModuleUtils.getLoyaltyManagementBD().calculatePaxLoyaltyRedeemableAmounts(redeemCalculateReqTo);
		} else {
			RedeemCalculateReq redeemCalculateReqTo = new RedeemCalculateReq();
			redeemCalculateReqTo.setMemberAccountId(memberAccountId);
			redeemCalculateReqTo.setPaxCarrierExternalCharges(paxCarrierExternalCharges);
			redeemCalculateReqTo.setTxnIdntifier(txnIdntifier);
			redeemCalculateReqTo.setPaxCarrierProductDueAmount(paxCarrierProductDueAmount);
			redeemCalculateReqTo.setCarrierWiseFlightRPH(carrierWiseFlightRPH);
			redeemCalculateReqTo.setRequestedLoyaltyPointsAmount(redeemRequestedAmount);
			redeemCalculateReqTo.setIssueRewardIds(issueRewardIds);
			redeemCalculateReqTo.setFlexiQuote(isFlexiQuote);
			redeemCalculateReqTo.setPayForOHD(isPayForOHD);
			redeemCalculateReqTo.setPnr(pnr);
			redeemCalculateReqTo.setDiscountRQ(discountRQ);
			redeemCalculateReqTo.setMemberEnrollingCarrier(memberEnrollingCarrier);
			redeemCalculateReqTo.setMemberExternalId(memberExternalId);
			redeemCalculateReqTo.setRemainingPoint(remainingPoint);
			return AirproxyModuleUtils.getLCCReservationBD().calculatePaxLoyaltyRedeemableAmounts(redeemCalculateReqTo,
					userPrincipal, trackInfoDTO);
		}
	}

	public ServiceResponce issueLoyaltyRewards(Map<String, Double> productPoints, AppIndicatorEnum appIndicator)
			throws ModuleException {
		if (system == SYSTEM.AA) {
			RedeemLoyaltyPointsReq redeemLoyaltyPointsReq = new RedeemLoyaltyPointsReq();
			redeemLoyaltyPointsReq.setMemberAccountId(memberAccountId);
			redeemLoyaltyPointsReq.setProductPoints(productPoints);
			redeemLoyaltyPointsReq.setAppIndicator(appIndicator);
			redeemLoyaltyPointsReq.setLoyaltyPointsToCurrencyConversionRate(AppSysParamsUtil
					.getLoyaltyPointsToCurrencyConversionRate());
			redeemLoyaltyPointsReq.setPnr(pnr);
			redeemLoyaltyPointsReq.setMemberEnrollingCarrier(memberEnrollingCarrier);
			redeemLoyaltyPointsReq.setMemberExternalId(memberExternalId);
			return AirproxyModuleUtils.getLoyaltyManagementBD().issueLoyaltyRewards(redeemLoyaltyPointsReq);
		} else {
			// TODO:RW Handle dry/interline flow
			return null;
		}
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	public void setRedeemRequestedAmount(BigDecimal redeemRequestedAmount) {
		this.redeemRequestedAmount = redeemRequestedAmount;
	}

	public void
			setPaxCarrierExternalCharges(Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges) {
		this.paxCarrierExternalCharges = paxCarrierExternalCharges;
	}

	public void setPaxCarrierProductDueAmount(Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount) {
		this.paxCarrierProductDueAmount = paxCarrierProductDueAmount;
	}

	public void setFlightPriceRQ(FlightPriceRQ flightPriceRQ) {
		this.flightPriceRQ = flightPriceRQ;
	}

	public void setFareSegChargeTo(FareSegChargeTO fareSegChargeTo) {
		this.fareSegChargeTo = fareSegChargeTo;
	}

	public void setFlexiQuote(boolean isFlexiQuote) {
		this.isFlexiQuote = isFlexiQuote;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public void setTxnIdntifier(String txnIdntifier) {
		this.txnIdntifier = txnIdntifier;
	}

	public void setIssueRewardIds(boolean issueRewardIds) {
		this.issueRewardIds = issueRewardIds;
	}

	public void setPayForOHD(boolean isPayForOHD) {
		this.isPayForOHD = isPayForOHD;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setCarrierWiseFlightRPH(Map<String, List<String>> carrierWiseFlightRPH) {
		this.carrierWiseFlightRPH = carrierWiseFlightRPH;
	}

	public void setRemainingPoint(Double remainingPoint) {
		this.remainingPoint = remainingPoint;
	}

	public void setMemberExternalId(String memberExternalId) {
		this.memberExternalId = memberExternalId;
	}

	public void setMemberEnrollingCarrier(String memberEnrollingCarrier) {
		this.memberEnrollingCarrier = memberEnrollingCarrier;
	}

	private Map<Integer, Set<ChargesDetailDTO>> loadBalanceDueChargeBreakdown(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);

		return AirproxyModuleUtils.getReservationBD().getPnrPaxSeqWiseBalanceDueCharges(pnrModesDTO);
	}

	private LCCClientReservation loadGroupReservation(String groupPNR) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(groupPNR);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setGroupPNR(groupPNR);

		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadExternalPaxPayments(true);

		ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
		paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_IBE);
		paramRQInfo.setIsRegisteredUser(true);
		TrackInfoDTO trackInfo = new TrackInfoDTO();

		LCCClientReservation lccClientReservation = AirproxyModuleUtils.getAirproxyReservationBD().searchReservationByPNR(
				pnrModesDTO, paramRQInfo, trackInfo);
		return lccClientReservation;
	}

	public void setDiscountRQ(DiscountRQ discountRQ) {
		this.discountRQ = discountRQ;
	}
	
}
