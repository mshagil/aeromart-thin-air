package com.isa.thinair.airproxy.core.utils.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.ReservationAssemblerConvertUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

public class SegmentAssemblerConvertUtil {

	public SegmentAssemblerConvertUtil() {

	}

	/**
	 * Given ondFareDTO,lCCClientSegmentAssembler return a segment Assembler
	 * 
	 * @param ondFareDTOs
	 * @param segmentAssembler
	 * @param oldReservation
	 * @param modifiedSegIds
	 * @param modifiedONDReturnGrp
	 * @return
	 * @throws ModuleException
	 */
	public ISegment getSegmentAssembler(Collection<OndFareDTO> ondFareDTOs, LCCClientSegmentAssembler segmentAssembler,
			Reservation oldReservation, Collection<Integer> modifiedSegIds, Integer modifiedONDReturnGrp) throws ModuleException {
		ISegment iSegment = new SegmentAssembler(ondFareDTOs, segmentAssembler.getLastFareQuotedDate());
		populatePaxInformation(iSegment, segmentAssembler);
		// setting up ground stations
		// TODO: Refactor--> Only the current "new" segment returned?? if so no need to put in a map
		Map<Integer, String> groundStationBySegmentMap = new LinkedHashMap<Integer, String>();
		Map<Integer, Integer> groundSementFlightByAirFlightMap = new LinkedHashMap<Integer, Integer>();
		CommonUtil.mapStationsBySegment(segmentAssembler.getSegmentsMap().values(), groundStationBySegmentMap,
				groundSementFlightByAirFlightMap);

		if (segmentAssembler.getDiscountedFareDetails() != null) {
			iSegment.setFareDiscountInfo(segmentAssembler.getDiscountedFareDetails());
			iSegment.setFareDiscount(segmentAssembler.getFareDiscountPercentage(), segmentAssembler.getDiscountedFareDetails()
					.getNotes());
		}

		iSegment.setApplicableAgentCommissions(segmentAssembler.getApplicableAgentCommissions());

		populateOndSegments(iSegment, ondFareDTOs, oldReservation, modifiedSegIds, modifiedONDReturnGrp, groundStationBySegmentMap,
				groundSementFlightByAirFlightMap);
		/** Added by Lalanthi - RAK Insurance change */
		populateInsurance(iSegment, segmentAssembler, oldReservation);
		// add the new release time stamp

		if (!hasPayment(segmentAssembler) && segmentAssembler.getPnrZuluReleaseTimeStamp() != null) {
			iSegment.overrideZuluReleaseTimeStamp(segmentAssembler.getPnrZuluReleaseTimeStamp());
		}
		// sets the last selected currency code
		iSegment.setLastCurrencyCode(segmentAssembler.getLastCurrencyCode());

		return iSegment;
	}

	public ISegment getSegmentAssembler(LCCClientSegmentAssembler lCCClientSegmentAssembler) throws ModuleException {
		ISegment iSegment = new SegmentAssembler(null);
		populatePaxInformation(iSegment, lCCClientSegmentAssembler);
		return iSegment;
	}

	/**
	 * 
	 * @param iSegment
	 * @param groundSementFlightByAirFlightMap
	 * @param selectedFlightDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private void populateOndSegments(ISegment iSegment, Collection<OndFareDTO> ondFareDTOs, Reservation oldReservation,
			Collection<Integer> modifiedSegIds, Integer modifiedONDReturnGrp, Map<Integer, String> groundStationBySegmentMap,
			Map<Integer, Integer> groundSementFlightByAirFlightMap)
			throws ModuleException {

		List<Collection<ReservationSegmentTO>> ondFlightSegments = ReservationAssemblerConvertUtil.getOndWiseFlights(ondFareDTOs,
				null);
		boolean isReturnBooking = OndFareUtil.isReturnBooking(ondFareDTOs);

		int segmentSeq = getNextSegSequanceId(oldReservation);
		int intOndGrp = getNextONDGroupId(oldReservation);
		int intReturnOndGrp = getNextONDReturnGroupId(oldReservation);

		Date openRTConfirmExpiry = OndFareUtil.getOpenReturnPeriods(ondFareDTOs).getConfirmBefore();
		boolean openRTSegExists = false;

		// FIXME a small hack until the full fix is done for OPENRT
		if (ondFlightSegments.size() == 2 && openRTConfirmExpiry != null) {
			openRTSegExists = true;
		}

		if (AppSysParamsUtil.isTicketValidityEnabled() && modifiedSegIds != null && modifiedSegIds.size() > 0) {
			Map<Collection<Integer>, Collection<Integer>> invSegMap = ReservationApiUtils.getInverseSegments(oldReservation,
					false);
			Map<Integer, Collection<Integer>> ondGrpWiseSegments = ReservationApiUtils.getOndGrpWiseSegments(oldReservation
					.getSegments());

			Set<Integer> invSegIds = new HashSet<Integer>();
			for (Integer modSegId : modifiedSegIds) {
				Collection<Integer> ondSegGrp = null;
				for (Collection<Integer> ondSegIds : ondGrpWiseSegments.values()) {
					if (ondSegIds.contains(modSegId)) {
						ondSegGrp = ondSegIds;
						break;
					}
				}
				if (ondSegGrp != null) {
					Collection<Integer> invGrpSegIds = invSegMap.get(ondSegGrp);
					if (invGrpSegIds != null) {
						invSegIds.addAll(invGrpSegIds);
					}
				}
			}

			if (invSegIds.size() > 0) {
				List<Long> ticketValidityColl = new ArrayList<Long>();
				List<Date> segmentDepDateColl = new ArrayList<Date>();
				for (ReservationSegmentDTO resSeg : oldReservation.getSegmentsView()) {
					if (invSegIds.contains(resSeg.getPnrSegId())) {

						long maxStayOverMins = ReservationApiUtils.getTotalStayOverMins(resSeg.getZuluDepartureDate(), resSeg
								.getFareTO().getMaximumStayOverMonths(), resSeg.getFareTO().getMaximumStayOver());

						if (maxStayOverMins > 0)
							ticketValidityColl.add(maxStayOverMins);

						segmentDepDateColl.add(resSeg.getZuluDepartureDate());
					}
				}
				Collections.sort(ticketValidityColl);
				Collections.sort(segmentDepDateColl);

				Date firstDepDate = null;
				Long mostRestrMaxStayOver = null;
				if (segmentDepDateColl.size() > 0)
					firstDepDate = segmentDepDateColl.get(0);

				if (ticketValidityColl.size() > 0)
					mostRestrMaxStayOver = ticketValidityColl.get(0);

				iSegment.setExistingValidityInfo(firstDepDate, mostRestrMaxStayOver);
			}
		}

		int ondSequence = 0;
		for (Collection<ReservationSegmentTO> perOndFlights : ondFlightSegments) {
			int lastFareTypeId = -1;
			for (ReservationSegmentTO segmentInfo : perOndFlights) {
				Date openRTConfirmBefore = null;
				boolean isReturnOND = false;

				if (openRTSegExists && ondSequence == 1) {
					openRTConfirmBefore = openRTConfirmExpiry;
				}

				if (isReturnBooking && ondSequence == 1) {
					isReturnOND = true;
				}

				iSegment.addOndSegment((ondSequence + 1), segmentSeq++, segmentInfo.getFlightSegId(), intOndGrp, intReturnOndGrp,
						openRTConfirmBefore, null, null, false, isReturnOND, segmentInfo.getSelectedBundledFarePeriodId(), null, null,
						segmentInfo.getCsOcCarrierCode(), segmentInfo.getCsOcFlightNumber());
				iSegment.addGroundStationDataToPNRSegment(groundStationBySegmentMap, groundSementFlightByAirFlightMap);
				int fareTypeId = segmentInfo.getOndFareType();
				if (fareTypeId == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE || fareTypeId == FareTypes.PER_FLIGHT_FARE) {
					intOndGrp++;
					intReturnOndGrp++;
				}
				lastFareTypeId = fareTypeId;
			}
			if (lastFareTypeId != FareTypes.PER_FLIGHT_FARE) {
				intOndGrp++;
			}
			if (lastFareTypeId == FareTypes.OND_FARE) {
				intReturnOndGrp++;
				intOndGrp++;
			}
			ondSequence++;

		}
	}

	/**
	 * 
	 * @param iReservation
	 * @param passengers
	 * @throws ModuleException
	 */
	private void populatePaxInformation(ISegment iSegment, LCCClientSegmentAssembler lCCClientSegmentAssembler)
			throws ModuleException {
		Map<Integer, LCCClientPaymentAssembler> paxPayments = lCCClientSegmentAssembler.getPassengerPaymentsMap();
		for (Integer pnrPaxId : paxPayments.keySet()) {
			iSegment.addPassengerPayments(
					pnrPaxId,
					PaymentConvertUtil.populatePerPaxPayment(paxPayments.get(pnrPaxId),
							lCCClientSegmentAssembler.getExternalChargesMap()));
		}
	}

	/**
	 * gets a next fare group id
	 * 
	 * @param oldReservation
	 * @return
	 */
	private static int getNextONDGroupId(Reservation oldReservation) {
		int nextONDGroupId = 0;

		Set colSegs = oldReservation.getSegments();

		for (Iterator iter = colSegs.iterator(); iter.hasNext();) {
			ReservationSegment reservationSegment = (ReservationSegment) iter.next();
			Integer ondGroupId = reservationSegment.getOndGroupId();
			if (ondGroupId != null && nextONDGroupId < ondGroupId.intValue()) {
				nextONDGroupId = reservationSegment.getOndGroupId().intValue();
			}

		}

		nextONDGroupId++;

		return nextONDGroupId;
	}

	/**
	 * gets the next segment sequence id
	 * 
	 * @param oldReservation
	 * @return
	 */
	private static int getNextSegSequanceId(Reservation oldReservation) {
		int iNextSegSequanceId = 0;

		Set colSegs = oldReservation.getSegments();

		for (Iterator iter = colSegs.iterator(); iter.hasNext();) {
			ReservationSegment reservationSegment = (ReservationSegment) iter.next();
			Integer segSeqId = reservationSegment.getSegmentSeq();
			if (segSeqId != null && iNextSegSequanceId < segSeqId.intValue()) {
				iNextSegSequanceId = segSeqId.intValue();
			}

		}

		iNextSegSequanceId++;

		return iNextSegSequanceId;
	}

	/**
	 * gets a next return ond group id
	 * 
	 * @param oldReservation
	 * @return
	 */
	private static int getNextONDReturnGroupId(Reservation oldReservation) {
		int nextReturnONDGroupId = 0;

		Set colSegs = oldReservation.getSegments();

		for (Iterator iter = colSegs.iterator(); iter.hasNext();) {
			ReservationSegment reservationSegment = (ReservationSegment) iter.next();
			Integer returnOndGroupId = reservationSegment.getReturnOndGroupId();
			if (returnOndGroupId != null && nextReturnONDGroupId < returnOndGroupId.intValue()) {
				nextReturnONDGroupId = reservationSegment.getReturnOndGroupId().intValue();
			}

		}

		nextReturnONDGroupId++;

		return nextReturnONDGroupId;
	}

	private static boolean isOutFlight(Reservation reservation, Collection<Integer> collOldSegmentIds) {
		boolean blnReturnFlight = true;

		Collection<ReservationSegmentDTO> colSegDTO = reservation.getSegmentsView();

		/*
		 * for (Iterator<ReservationSegmentDTO> iter = colSegDTO.iterator(); iter.hasNext();) { ReservationSegmentDTO
		 * reservationSegmentDTO = (ReservationSegmentDTO) iterSegments.next();
		 * 
		 * if (collOldSegmentIds != null) { for (Iterator<Integer> iterColOldSegId = collOldSegmentIds.iterator();
		 * iterColOldSegId.hasNext();) { Integer oldPNRSegId = (Integer) iterColOldSegId.next();
		 * 
		 * if (oldPNRSegId.equals(reservationSegmentDTO.getPnrSegId())) { return
		 * reservationSegmentDTO.getReturnFlag().equals(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE) ?
		 * true: false; } } } }
		 */

		if (colSegDTO != null) {
			for (ReservationSegmentDTO reservationSegmentDTO : colSegDTO) {
				if (collOldSegmentIds != null) {
					for (Integer integer : collOldSegmentIds) {
						Integer oldPNRSegId = integer;

						if (oldPNRSegId.equals(reservationSegmentDTO.getPnrSegId())) {
							// return reservationSegmentDTO.getReturnFlag().equals(
							// ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE) ? true : false;
							return reservationSegmentDTO.getJourneySeq().intValue() != 2 ? true : false;
						}
					}
				}
			}
		}

		return blnReturnFlight;
	}

	/**
	 * Populate temporary payment entries from common reservation assembler to the iSegment
	 * 
	 * @param iSegment
	 * @param temporaryPaymentMap
	 */
	public void populateTempPaymentEntries(ISegment iSegment, Map<Integer, CommonCreditCardPaymentInfo> temporaryPaymentMap) {
		if (temporaryPaymentMap != null && temporaryPaymentMap.size() > 0) {
			Map<Integer, CardPaymentInfo> cardPayInfoMap = new HashMap<Integer, CardPaymentInfo>();
			Set<Integer> tmpKeySet = temporaryPaymentMap.keySet();
			for (Integer key : tmpKeySet) {
				CommonCreditCardPaymentInfo commonInfo = temporaryPaymentMap.get(key);
				CardPaymentInfo cardPayInfo = PaymentConvertUtil.convertCreditCardPayment(commonInfo);
				cardPayInfoMap.put(key, cardPayInfo);
			}
			iSegment.addTemporyPaymentEntries(cardPayInfoMap);
		}
	}

	public void populateInsurance(ISegment iSegment, LCCClientSegmentAssembler lcclientSegmentAssembler, Reservation reservation) {
		IInsuranceRequest insurance = new InsuranceRequestAssembler();
		List<LCCClientReservationInsurance> lccInsurances = lcclientSegmentAssembler.getResInsurances();

		InsureSegment insSegment = new InsureSegment();
		if (lccInsurances != null && !lccInsurances.isEmpty()) {

			for (LCCClientReservationInsurance lccInsurance : lccInsurances) {
				LCCInsuredJourneyDTO insuredJourneyDTO = lccInsurance.getInsuredJourneyDTO();
				insSegment.setArrivalDate(insuredJourneyDTO.getJourneyEndDate());
				insSegment.setDepartureDate(insuredJourneyDTO.getJourneyStartDate());
				insSegment.setFromAirportCode(insuredJourneyDTO.getJourneyStartAirportCode());
				insSegment.setToAirportCode(insuredJourneyDTO.getJourneyEndAirportCode());
				insSegment.setRoundTrip(insuredJourneyDTO.isRoundTrip());
				insSegment.setSalesChanelCode(insuredJourneyDTO.getSalesChannel());

				insurance.setInsuranceId(new Integer(lccInsurance.getInsuranceQuoteRefNumber()));
				insurance.addFlightSegment(insSegment);
				insurance.setQuotedTotalPremiumAmount(lccInsurance.getQuotedTotalPremium());
				insurance.setInsuranceType(lccInsurance.getInsuranceType());
				insurance.setPlanCode(lccInsurance.getPlanCode());
				insurance.setSsrFeeCode(lccInsurance.getSsrFeeCode());
				BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(lccInsurance.getQuotedTotalPremium(),
						reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount());
				int i = 0;
				for (ReservationPax pax : reservation.getPassengers()) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
						InsurePassenger insurePassenger = new InsurePassenger();

						// adding AIG Pax info
						insurePassenger.setFirstName(pax.getFirstName());
						insurePassenger.setLastName(pax.getLastName());
						insurePassenger.setDateOfBirth(pax.getDateOfBirth());

						ReservationContactInfo contactInfo = reservation.getContactInfo();
						// Adding AIG Contact Info
						insurePassenger.setAddressLn1(contactInfo.getStreetAddress1());
						insurePassenger.setAddressLn2(contactInfo.getStreetAddress2());
						insurePassenger.setCity(contactInfo.getCity());
						insurePassenger.setEmail(contactInfo.getEmail());
						insurePassenger.setCountryOfAddress(contactInfo.getCountryCode());
						insurePassenger.setHomePhoneNumber(contactInfo.getPhoneNo());

						insurance.addPassenger(insurePassenger);
						insurance.addInsuranceCharge(pax.getPaxSequence(), insPaxWise[i++]);
					}
				}

				iSegment.addInsurance(insurance);
			}
		}

	}

	public void mapStationsBySegment(Collection<LCCClientReservationSegment> segments, Map<Integer, String> segmentMap)
			throws NumberFormatException, ModuleException {

		if (segmentMap == null) {
			segmentMap = new HashMap<Integer, String>();
		}
		for (LCCClientReservationSegment clientReservationSegment : segments) {
			Collection airports = AddModifySurfaceSegmentValidations.getAirportsFromSegmentCode(clientReservationSegment
					.getSegmentCode());
			if (clientReservationSegment.getSubStationShortName() != null
					|| AirproxyModuleUtils.getAirportBD().isContainGroundSegment(airports)) {
				if (clientReservationSegment.getSubStationShortName() != null) {
					segmentMap.put(Integer.parseInt(clientReservationSegment.getFlightSegmentRefNumber()),
							clientReservationSegment.getSubStationShortName());
				} else {
					segmentMap.put(Integer.parseInt(clientReservationSegment.getFlightSegmentRefNumber()),
							AddModifySurfaceSegmentValidations.returnGroundSegment(airports));
				}
			}

		}
	}

	private boolean hasPayment(LCCClientSegmentAssembler lccClientSegmentAssembler) {
		if (lccClientSegmentAssembler != null) {

			Map<Integer, LCCClientPaymentAssembler> paxPaymentsMap = lccClientSegmentAssembler.getPassengerPaymentsMap();

			if (paxPaymentsMap != null) {
				for (Integer pnrPaxId : paxPaymentsMap.keySet()) {
					LCCClientPaymentAssembler lccClientPaymentAssembler = paxPaymentsMap.get(pnrPaxId);
					if (lccClientPaymentAssembler != null && lccClientPaymentAssembler.getPayments() != null
							&& lccClientPaymentAssembler.getPayments().size() > 0) {
						return true;
					}
				}
			}

		}

		return false;
	}

}
