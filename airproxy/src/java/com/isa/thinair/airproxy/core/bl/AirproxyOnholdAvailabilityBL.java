package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * 
 * @author rumesh
 * 
 */
public class AirproxyOnholdAvailabilityBL {

	public static boolean checkOnholdAvailability(List<FlightSegmentTO> flightSegmentTOs, List<ReservationPax> paxList,
			String contactEmail, int validationType, int salesChannel, SYSTEM searchSystem, String ipAddress,
			BasicTrackInfo trackInfo, UserPrincipal userPrincipal) throws ModuleException {
		boolean onholdAvailable = false;
		String ip = ipAddress;
		if (ipAddress == null || ipAddress.trim().isEmpty())
			ip = userPrincipal.getIpAddress();
		if (searchSystem == SYSTEM.INT) {
			onholdAvailable = AirproxyModuleUtils.getLCCSearchAndQuoteBD().isReservationOnholdable(
					populateCarrierWiseOnholdDTOs(flightSegmentTOs, paxList, contactEmail, validationType, ip, salesChannel),
					trackInfo);
		} else {
			onholdAvailable = AirproxyModuleUtils.getAirReservationQueryBD().isReservationOnholdable(
					ReservationApiUtils.getOnHoldValidationDTO(populateFlightSegmentDtoList(flightSegmentTOs), paxList,
							contactEmail, validationType, ip, salesChannel));
		}

		return onholdAvailable;
	}

	private static Map<String, ResOnholdValidationDTO> populateCarrierWiseOnholdDTOs(List<FlightSegmentTO> flightSegmentTOs,
			List<ReservationPax> paxList, String contactEmail, int validationType, String ipAddress, int salesChannel) {
		Map<String, ResOnholdValidationDTO> carrierWiseOHD = new HashMap<String, ResOnholdValidationDTO>();

		ResOnholdValidationDTO ohdDto = null;
		for (FlightSegmentTO fltSeg : flightSegmentTOs) {
			if (carrierWiseOHD.containsKey(fltSeg.getOperatingAirline())) {
				ohdDto = carrierWiseOHD.get(fltSeg.getOperatingAirline());
				ohdDto.addFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltSeg.getFlightRefNumber()));
				ohdDto.addDepartureDate(fltSeg.getDepartureDateTimeZulu());
			} else {
				ohdDto = new ResOnholdValidationDTO();
				ohdDto.addFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltSeg.getFlightRefNumber()));
				ohdDto.addDepartureDate(fltSeg.getDepartureDateTimeZulu());
				ohdDto.setIpAddress(ipAddress);
				ohdDto.setValidationType(validationType);
				ohdDto.setPaxList(paxList);
				ohdDto.setContactEmail(contactEmail);
				ohdDto.setSalesChannel(salesChannel);
				carrierWiseOHD.put(fltSeg.getOperatingAirline(), ohdDto);
			}
		}

		return carrierWiseOHD;
	}

	private static List<FlightSegmentDTO> populateFlightSegmentDtoList(List<FlightSegmentTO> flightSegmentTOs) {
		List<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
			flightSegmentDTO.setSegmentId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegmentTO.getFlightRefNumber()));
			flightSegmentDTO.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
			flightSegmentDTOs.add(flightSegmentDTO);
		}

		return flightSegmentDTOs;
	}
}
