package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author aravinth.r
 */
public class AirproxyAutomaticCheckinBL {

	private Log log = LogFactory.getLog(AirproxyAutomaticCheckinBL.class);

	private SYSTEM system;
	private List<FlightSegmentTO> flightSegmentTOs;
	private AppIndicatorEnum appIndicator;
	private UserPrincipal userPrincipal;
	private Map<String, Integer> serviceCount;
	private boolean modifyAnci;

	public static final String APPLICABILITY_TYPE_PAX = "P";
	public static final String APPLICABILITY_TYPE_RESERVATION = "R";

	public Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO> execute() throws ModuleException {

		return getOwnAirlineAiportAutomaticCheckins();
	}

	/**
	 * This is to get automatic checkin availability in own airline
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO> getOwnAirlineAiportAutomaticCheckins()
			throws ModuleException {
		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = AncillaryCommonUtil.populateAiportWiseFltSegments(
				flightSegmentTOs, serviceCount, false);
		Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO> lccAiportWiseMap = new LinkedHashMap<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO>();

		if (AppSysParamsUtil.isAutomaticCheckinEnabled()) {
			Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> airportWiseResults = AirproxyModuleUtils.getAutomaticCheckinBD()
					.getAvailableAutomaticCheckins(airportWiseMap);

			flightSegmentTOs.forEach(flightSegmentTO -> {
				populateSegmentWiseAutoCheckin(airportWiseMap, lccAiportWiseMap, airportWiseResults, flightSegmentTO);
			});
		}
		return lccAiportWiseMap;
	}
	
	public static List<AutomaticCheckinDTO> getAutomaticCheckinDetails(String airportCode) throws ModuleException {
		return AirproxyModuleUtils.getAutomaticCheckinBD().getAutomaticCheckinDetails(airportCode);
	}

	/**
	 * This is to populate autocheckin details in segment wise
	 * 
	 * @param airportWiseMap
	 * @param lccAiportWiseMap
	 * @param airportWiseResults
	 * @param flightSegmentTO
	 */
	private void populateSegmentWiseAutoCheckin(Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap,
			Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO> lccAiportWiseMap,
			Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> airportWiseResults, FlightSegmentTO flightSegmentTO) {

		airportWiseResults.entrySet().forEach(
				airportWiseResultEntry -> {
					AirportServiceKeyTO keyTO = airportWiseResultEntry.getKey();
					FlightSegmentTO originalFltSeg = airportWiseMap.get(keyTO);
					if (flightSegmentTO.getSegmentCode().equals(originalFltSeg.getSegmentCode())
							&& flightSegmentTO.getDepartureDateTimeZulu().getTime() == keyTO.getDepartureDateTime()) {
						getActiveAutoCheckins(lccAiportWiseMap, flightSegmentTO, airportWiseResultEntry, keyTO);
					}
				});
	}

	/**
	 * 
	 * This is to get only active automatic checkin details
	 * 
	 * @param lccAiportWiseMap
	 * @param flightSegmentTO
	 * @param airportWiseResultEntry
	 * @param keyTO
	 */
	private void getActiveAutoCheckins(Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO> lccAiportWiseMap,
			FlightSegmentTO flightSegmentTO, Entry<AirportServiceKeyTO, List<AutomaticCheckinDTO>> airportWiseResultEntry,
			AirportServiceKeyTO keyTO) {
		List<LCCAutomaticCheckinDTO> lccAutomaticCheckinList = new ArrayList<LCCAutomaticCheckinDTO>();
		// Adding available active autocheckins in list
		airportWiseResultEntry.getValue().forEach(automaticCheckinDTO -> {
			if ("ACT".equalsIgnoreCase(automaticCheckinDTO.getStatus())) {
				lccAutomaticCheckinList.add(populateLCCAutomaticCheckinDTO(automaticCheckinDTO));
			}
		});
		LCCFlightSegmentAutomaticCheckinDTO lccFSegAutomaticCheckinDTO = new LCCFlightSegmentAutomaticCheckinDTO();
		lccFSegAutomaticCheckinDTO.setFlightSegmentTO(flightSegmentTO);
		lccFSegAutomaticCheckinDTO.setAutomcaticCheckin(lccAutomaticCheckinList);
		lccAiportWiseMap.put(keyTO, lccFSegAutomaticCheckinDTO);
	}

	private LCCAutomaticCheckinDTO populateLCCAutomaticCheckinDTO(AutomaticCheckinDTO automaticCheckinDTO) {
		LCCAutomaticCheckinDTO lccAutomaticCheckinDTO = new LCCAutomaticCheckinDTO();
		lccAutomaticCheckinDTO.setAirportCode(automaticCheckinDTO.getAirportCode());
		lccAutomaticCheckinDTO.setStatus(automaticCheckinDTO.getStatus());
		lccAutomaticCheckinDTO.setAutomaticCheckinCharge(automaticCheckinDTO.getAmount());
		lccAutomaticCheckinDTO.setAutoCheckinId(automaticCheckinDTO.getAutomaticCheckinTemplateId());
		return lccAutomaticCheckinDTO;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public Map<String, Integer> getServiceCount() {
		return serviceCount;
	}

	public boolean isModifyAnci() {
		return modifyAnci;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public void setServiceCount(Map<String, Integer> serviceCount) {
		this.serviceCount = serviceCount;
	}

	public void setModifyAnci(boolean modifyAnci) {
		this.modifyAnci = modifyAnci;
	}
}
