package com.isa.thinair.airproxy.core.remoting.ejb;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.automaticCheckin.AutomaticCheckinRequest;
import com.isa.thinair.airproxy.api.model.automaticCheckin.AutomaticCheckinResponse;
import com.isa.thinair.airproxy.core.bl.AirproxyAutomaticCheckinBL;
import com.isa.thinair.airproxy.core.service.bd.AirproxyAutomaticCheckinBDLocal;
import com.isa.thinair.airproxy.core.service.bd.AirproxyAutomaticCheckinBDRemote;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

@Stateless
@RemoteBinding(jndiBinding = "AirproxyAutomaticCheckinService.remote")
@LocalBinding(jndiBinding = "AirproxyAutomaticCheckinService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AirproxyAutomaticCheckinServiceBean extends PlatformBaseSessionBean implements AirproxyAutomaticCheckinBDLocal,
		AirproxyAutomaticCheckinBDRemote {

	@Override
	public AutomaticCheckinResponse getAutomaticCheckinResponse(AutomaticCheckinRequest automaticCheckinRequest)
			throws ModuleException {
		AutomaticCheckinResponse automaticCheckinResponse = new AutomaticCheckinResponse();
		try {
			List<AutomaticCheckinDTO> automaticCheckinDTO = AirproxyAutomaticCheckinBL
					.getAutomaticCheckinDetails(automaticCheckinRequest.getDepartAirPortCode());
			if (automaticCheckinDTO.size() > 0) {
				automaticCheckinResponse.getAutomaticCheckinDTO().add(automaticCheckinDTO.get(0));
			}
		} catch (ModuleException me) {
			log.error("Error occured while executing method getAutomaticCheckinResponse(AutomaticCheckinRequest)", me);
			throw me;
		} catch (Exception ex) {
			log.error("Error occured while executing method getAutomaticCheckinResponse(AutomaticCheckinRequest)", ex);
			throw new ModuleException(ex.getMessage());
		}
		return automaticCheckinResponse;
	}

}
