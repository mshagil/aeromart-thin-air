package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentBaggagesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airproxy.api.utils.AncillaryUtil;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.SeatFactorConditionApplyFor;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;

/**
 * @author mano
 * @since May 9, 2011
 */
public class AirproxyBaggageBusinessLayer {

	private SYSTEM system;
	private List<LCCBaggageRequestDTO> baggageRequestDTOs;
	private String transactionIdentifier;
	private String selectedLanguage;
	private boolean isModifyOperation;
	private boolean hasFinalCutOverPrivilege;
	private boolean isRequote;
	private ApplicationEngine appEngine;
	private LCCReservationBaggageSummaryTo baggageSummaryTo;
	private UserPrincipal userPrincipal;
	private List<BundledFareDTO> bundledFareDTOs;
	private String pnr;
	private BasicTrackInfo trackInfo;
	private Map<String, Map<String, Integer>> carrierWiseOndWiseBundlePeriodID;

	public LCCBaggageResponseDTO execute() throws ModuleException {
		if (selectedLanguage == null || selectedLanguage.equalsIgnoreCase(""))
			selectedLanguage = "en";

		LCCBaggageResponseDTO lccBaggageResponseDTO;

		if (system == SYSTEM.INT) {
			lccBaggageResponseDTO = AirproxyModuleUtils.getLCCAncillaryBD().getAvailableBaggages(baggageRequestDTOs,
					transactionIdentifier, selectedLanguage, baggageSummaryTo, pnr, trackInfo, isRequote,
					carrierWiseOndWiseBundlePeriodID);
		} else {
			lccBaggageResponseDTO = getOwnAirlineBaggages(selectedLanguage);
		}

		return handleBufferTimes(lccBaggageResponseDTO);
	}

	private LCCBaggageResponseDTO handleBufferTimes(LCCBaggageResponseDTO lccBaggageResponseDTO) {
		Set<String> bufferTimeViolatedOndGroupIds = new HashSet<String>();

		for (LCCFlightSegmentBaggagesDTO lccFlightSegmentBaggagesDTO : lccBaggageResponseDTO.getFlightSegmentBaggages()) {
			FlightSegmentTO flightSegmentTO = lccFlightSegmentBaggagesDTO.getFlightSegmentTO();
			boolean isStandBy = isStandby(flightSegmentTO);
			long millis = BaggageTimeWatcher.getBaggageCutOverTimeForXBEInMillis(isModifyOperation, hasFinalCutOverPrivilege,
					isStandBy);

			if (flightSegmentTO.getDepartureDateTimeZulu().getTime() - millis < new Date().getTime()) {
				bufferTimeViolatedOndGroupIds.add(flightSegmentTO.getBaggageONDGroupId());
			}
		}

		for (LCCFlightSegmentBaggagesDTO lccFlightSegmentBaggagesDTO : lccBaggageResponseDTO.getFlightSegmentBaggages()) {
			if (bufferTimeViolatedOndGroupIds.contains(lccFlightSegmentBaggagesDTO.getFlightSegmentTO().getBaggageONDGroupId())) {
				lccFlightSegmentBaggagesDTO.getBaggages().clear();
			}
		}

		return lccBaggageResponseDTO;
	}

	public LCCBaggageResponseDTO getOwnAirlineBaggages(String selectedLanguage) throws ModuleException {
		LCCBaggageResponseDTO baggageResponseDTO = new LCCBaggageResponseDTO();
		Map<Integer, FlightSegmentTO> flightSegmentMap = new HashMap<Integer, FlightSegmentTO>();
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		List<FlightSegmentTO> fSegsForOND = new ArrayList<FlightSegmentTO>();
		BaggageRq baggageRq;

		for (LCCBaggageRequestDTO baggageReq : baggageRequestDTOs) {
			Set<Integer> segIdList = new HashSet<Integer>();

			FlightSegmentTO flightSeg = baggageReq.getFlightSegment();
			fSegsForOND.add(flightSeg);
			Integer segmentId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSeg.getFlightRefNumber());
			segIdList.add(segmentId);
			flightSegmentMap.put(segmentId, flightSeg);
			flightSegIdWiseCos.put(
					segmentId,
					new ClassOfServiceDTO(flightSeg.getCabinClassCode(), flightSeg.getLogicalCabinClassCode(), flightSeg
							.getSegmentCode()));
		}

		List<LCCFlightSegmentBaggagesDTO> segmentBaggageList = new ArrayList<LCCFlightSegmentBaggagesDTO>();

		Map<Integer, List<FlightBaggageDTO>> baggageMap = null;

		// TODO
		// check if available baggage with translations
		// else default should be en
		if (selectedLanguage.equalsIgnoreCase("en")) {
			if (AppSysParamsUtil.isONDBaggaeEnabled()) {

				baggageRq = new BaggageRq();
				baggageRq.setFlightSegmentTOs(fSegsForOND);
				baggageRq.setBookingClasses(baggageSummaryTo.getBookingClasses());
				baggageRq.setClassesOfService(baggageSummaryTo.getClassesOfService());
				baggageRq.setLogicalCC(baggageSummaryTo.getLogicalCC());
				baggageRq.setCheckCutoverTime(true);
				baggageRq.setAgent(userPrincipal.getAgentCode());
				baggageRq.setRequote(isRequote);
				baggageRq.setOwnReservation(true);
				baggageRq.setSalesChannel(userPrincipal.getSalesChannel());
				baggageMap = AirproxyModuleUtils.getBaggageBusinessDelegate().getBaggage(baggageRq);

			} else {
				baggageMap = AirproxyModuleUtils.getBaggageBusinessDelegate().getBaggages(flightSegIdWiseCos, false, false);
			}

		} else {
			// Seems translation not handle for ond baggage
			// Tmp enable en
			if (AppSysParamsUtil.isONDBaggaeEnabled()) {

				baggageRq = new BaggageRq();
				baggageRq.setFlightSegmentTOs(fSegsForOND);
				baggageRq.setBookingClasses(baggageSummaryTo.getBookingClasses());
				baggageRq.setClassesOfService(baggageSummaryTo.getClassesOfService());
				baggageRq.setCheckCutoverTime(true);
				baggageRq.setAgent(userPrincipal.getAgentCode());
				baggageRq.setRequote(isRequote);
				baggageRq.setOwnReservation(true);
				baggageRq.setSalesChannel(userPrincipal.getSalesChannel());
				baggageMap = AirproxyModuleUtils.getBaggageBusinessDelegate().getBaggage(baggageRq);
			} else {
				baggageMap = AirproxyModuleUtils.getBaggageBusinessDelegate().getBaggagesWithTranslations(flightSegIdWiseCos,
						selectedLanguage);
			}
		}

		Map<Integer, Integer> ondTemplateIds = InventoryAPIUtil.getOndWiseSelectedTemplateId(bundledFareDTOs,
				EXTERNAL_CHARGES.BAGGAGE.toString());
		boolean isBundledFareSelected = CommonUtil.isAtleastOneBundledFareSelected(bundledFareDTOs);

		Set<Integer> segIdSet = baggageMap.keySet();
		Map<Integer, Map<Integer, BigDecimal>> ondWiseSeatFactorCriteria = getONDWiseSeatFactorCriteria(new ArrayList<FlightSegmentTO>(flightSegmentMap.values()));
		for (Integer segId : segIdSet) {

			LCCFlightSegmentBaggagesDTO segmentBaggage = new LCCFlightSegmentBaggagesDTO();
			FlightSegmentTO flightSegmentTO = flightSegmentMap.get(segId);
			Map<Integer, BigDecimal> seatFactorCriterias = ondWiseSeatFactorCriteria.get(flightSegmentTO.getOndSequence());
			
			if (isBundledFareSelected) {
				if (ondTemplateIds != null && ondTemplateIds.containsKey(flightSegmentTO.getOndSequence())
						&& ondTemplateIds.get(flightSegmentTO.getOndSequence()) != null && baggageMap.get(segId) != null
						&& !baggageMap.get(segId).isEmpty()) {
					Integer bundledFareTemplateId = ondTemplateIds.get(flightSegmentTO.getOndSequence());

					List<FlightBaggageDTO> bundledFareBaggages = AirproxyModuleUtils.getBaggageBusinessDelegate()
							.getBaggageFromTemplate(bundledFareTemplateId, selectedLanguage, flightSegmentTO.getCabinClassCode());

					removeInvalidBaggagesFromBundle(bundledFareBaggages,seatFactorCriterias);
					AncillaryUtil.injectBaggageONDGroupID(bundledFareBaggages, getBaggageONDGroupID(baggageMap.get(segId)));
					baggageMap.put(segId, bundledFareBaggages);

				}
			} else {
				// Anci Offer operation related variables
				List<FlightBaggageDTO> offerBaggages = new ArrayList<FlightBaggageDTO>();
				AnciOfferCriteriaTO availableOffer = null;

				if (appEngine != null
						&& (SalesChannelsUtil.isAppEngineWebOrMobile(appEngine))) {
					AnciOfferCriteriaSearchTO searchCriteria = AnciOfferUtil.createAnciOfferSearch(flightSegmentTO,
							AnciOfferType.BAGGAGE);
					availableOffer = AirproxyModuleUtils.getAnciOfferBD().getAvailableAnciOffer(searchCriteria);
				}

				if (availableOffer != null) {
					offerBaggages = AirproxyModuleUtils.getBaggageBusinessDelegate().getBaggageFromTemplate(
							availableOffer.getTemplateID(), selectedLanguage, null);

					AncillaryUtil.injectBaggageONDGroupID(offerBaggages, getBaggageONDGroupID(baggageMap.get(segId)));

					segmentBaggage.setAnciOffer(true);
					segmentBaggage.setOfferName(availableOffer.getNameTranslations().get(selectedLanguage));
					segmentBaggage.setOfferDescription(availableOffer.getDescriptionTranslations().get(selectedLanguage));
					segmentBaggage.setOfferedTemplateID(availableOffer.getTemplateID());

					baggageMap.put(segId, offerBaggages);
				}
			}

			Set<String> ondGroupIds = new HashSet<String>();

			for (FlightBaggageDTO baggage : baggageMap.get(segId)) {

				LCCBaggageDTO baggageDTO = new LCCBaggageDTO();
				String ucs = "";

				baggageDTO.setBaggageName(baggage.getBaggageName());

				if (baggage.getTranslatedBaggageDescription() == null
						|| StringUtil.getUnicode(baggage.getTranslatedBaggageDescription()).equalsIgnoreCase("null"))
					baggageDTO.setBaggageDescription(baggage.getBaggageDescription());
				else {
					ucs = StringUtil.getUnicode(baggage.getTranslatedBaggageDescription());
					ucs = ucs.replace("^", ",");
					baggageDTO.setBaggageDescription(ucs);
				}

				baggageDTO.setBaggageCharge(baggage.getAmount());
				baggageDTO.setSoldPieces(baggage.getSoldPieces());
				baggageDTO.setDefaultBaggage(baggage.getDefaultBaggage());
				baggageDTO.setInactiveSelectedBaggage(baggage.isInactiveSelectedBaggage());
				baggageDTO.setOndBaggageGroupId(baggage.getOndGroupId());
				baggageDTO.setOndBaggageChargeId(BeanUtils.nullHandler(baggage.getChargeId()));
				baggageDTO.setBaggageStatus(baggage.getBaggageStatus());
				baggageDTO.setAvailableWeight(baggage.getAvailableWeight());
				baggageDTO.setWeight(baggage.getBaggageWeight().doubleValue());

				if (baggage.getOndGroupId() != null) {
					ondGroupIds.add(baggage.getOndGroupId());
				}

				segmentBaggage.getBaggages().add(baggageDTO);
			}

			if (ondGroupIds.size() > 1) {
				throw new ModuleException("airinventory.baggage.ond.should.be.either.ond.or.segment");
			}
			String ondGroupId = BeanUtils.nullHandler(BeanUtils.getFirstElement(ondGroupIds));
			if (ondGroupId.length() > 0) {
				flightSegmentTO.setBaggageONDGroupId(ondGroupId);
			}

			segmentBaggage.setFlightSegmentTO(flightSegmentTO);
			segmentBaggageList.add(segmentBaggage);
		}

		for (LCCFlightSegmentBaggagesDTO segBaggagesDTO : segmentBaggageList) {
			Collections.sort(segBaggagesDTO.getBaggages());
		}
		baggageResponseDTO.getFlightSegmentBaggages().addAll(segmentBaggageList);
		return baggageResponseDTO;
	}
	
	private Map<Integer, Map<Integer, BigDecimal>> getONDWiseSeatFactorCriteria(List<FlightSegmentTO> flightSegmentTOs) {
		Map<Integer, List<FlightSegmentTO>> ondWiseFlightSegments = new HashMap<>();
		Map<Integer, Map<Integer, BigDecimal>> ondWiseSeatFactorCriteria = new HashMap<>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			List<FlightSegmentTO> flightSegments = ondWiseFlightSegments.get(flightSegmentTO.getOndSequence());
			if (flightSegments == null) {
				flightSegments = new ArrayList<>();
				ondWiseFlightSegments.put(flightSegmentTO.getOndSequence(), flightSegments);
			}
			flightSegments.add(flightSegmentTO);
		}
		
		for(Map.Entry<Integer, List<FlightSegmentTO>> entry : ondWiseFlightSegments.entrySet()) {
			ondWiseSeatFactorCriteria.put(entry.getKey(), getSeatFactorCriteria(entry.getValue()));
		}
		
		return ondWiseSeatFactorCriteria;
	}

	private Map<Integer, BigDecimal> getSeatFactorCriteria(List<FlightSegmentTO> flightSegmentTOs) {

		Map<Integer, BigDecimal> seatFactorCriterias = new HashMap<Integer, BigDecimal>();
		Map<Integer, BigDecimal> loadFactors = null;
		List<Integer> flightSegIds = new ArrayList<Integer>();
		for (FlightSegmentTO flightSeg : flightSegmentTOs) {
			flightSegIds.add(flightSeg.getFlightSegId());
		}
		loadFactors = AirInventoryModuleUtils.getLocalFlightInventoryBD().getFlightLoadFactor(flightSegIds);
		
		Integer flightSegmentWithMostRestrictiveSeatFactor =
		        Collections.max(loadFactors.entrySet(), new Comparator<Map.Entry<Integer, BigDecimal>> () {
		            public int compare(Map.Entry<Integer, BigDecimal> e1, Map.Entry<Integer, BigDecimal>  e2) {
		                return e1.getValue().compareTo(e2.getValue());
		            }
		        }).getKey();
		
		
		
		FlightSegmentTO firstSeg = getPreferedFlightSegmentBaggageRq(flightSegmentTOs, SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition());
		FlightSegmentTO lastSeg = getPreferedFlightSegmentBaggageRq(flightSegmentTOs, SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition());
		
		
		seatFactorCriterias.put(SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition(), loadFactors.get(flightSegmentWithMostRestrictiveSeatFactor));
		seatFactorCriterias.put(SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition(), loadFactors.get(firstSeg.getFlightSegId()));
		seatFactorCriterias.put(SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition(), loadFactors.get(lastSeg.getFlightSegId()));
		return seatFactorCriterias;

	}
	
	
	private static FlightSegmentTO getPreferedFlightSegmentBaggageRq(List<FlightSegmentTO>  flightSegments, int condition) {

		if (SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition() == condition) {
			Collections.sort(flightSegments, new Comparator<FlightSegmentTO>() {
				public int compare(FlightSegmentTO o1, FlightSegmentTO o2) {
					return o1.getDepartureDateTimeZulu()
							.compareTo(o2.getDepartureDateTimeZulu());
				}
			});
			return flightSegments.get(0);
		} else if (SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition() == condition) {
			Collections.sort(flightSegments, new Comparator<FlightSegmentTO>() {
				public int compare(FlightSegmentTO o1, FlightSegmentTO o2) {
					return o2.getDepartureDateTimeZulu()
							.compareTo(o1.getDepartureDateTimeZulu());
				}
			});
			return flightSegments.get(0);
		}

		return null;
	}
	
	private static void removeInvalidBaggagesFromBundle(List<FlightBaggageDTO> bundledFareBaggages, Map<Integer, BigDecimal> seatFactorCriterias){
		List<FlightBaggageDTO> invalidBaggages = new ArrayList<FlightBaggageDTO>();
		for(FlightBaggageDTO bundledFareBaggage : bundledFareBaggages){
			BigDecimal seatFactorCondition = seatFactorCriterias.get(bundledFareBaggage.getSeatFactorConditionApplyFor());
			if(seatFactorCondition !=null && seatFactorCondition.intValue() >= bundledFareBaggage.getSeatFactor()){
				invalidBaggages.add(bundledFareBaggage);
			}
		}
		
		if(!invalidBaggages.isEmpty()){
			bundledFareBaggages.removeAll(invalidBaggages);
		}
	}
	
	private boolean isStandby(FlightSegmentTO flightSegmentTO) {
		boolean isStandBy = true;
		if (flightSegmentTO != null) {
			if (!BookingClass.BookingClassType.STANDBY.equals(flightSegmentTO.getBookingType())) {
				isStandBy = false;
			}
		}
		return isStandBy;
	}

	private String getBaggageONDGroupID(Collection<FlightBaggageDTO> flightBaggageDTOs) {
		String groupID = null;

		for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			groupID = flightBaggageDTO.getOndGroupId();
			break;
		}

		return groupID;
	}

	/**
	 * @return the system
	 */
	public SYSTEM getSystem() {
		return system;
	}

	/**
	 * @param system
	 *            the system to set
	 */
	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	/**
	 * @return the baggageRequestDTOs
	 */
	public List<LCCBaggageRequestDTO> getBaggageRequestDTOs() {
		return baggageRequestDTOs;
	}

	/**
	 * @param baggageRequestDTOs
	 *            the baggageRequestDTOs to set
	 */
	public void setBaggageRequestDTOs(List<LCCBaggageRequestDTO> baggageRequestDTOs) {
		this.baggageRequestDTOs = baggageRequestDTOs;
	}

	/**
	 * @return the transactionIdentifier
	 */
	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	/**
	 * @param transactionIdentifier
	 *            the transactionIdentifier to set
	 */
	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	/**
	 * @return the isModifyOperation
	 */
	public boolean isModifyOperation() {
		return isModifyOperation;
	}

	/**
	 * @param isModifyOperation
	 *            the isModifyOperation to set
	 */
	public void setModifyOperation(boolean isModifyOperation) {
		this.isModifyOperation = isModifyOperation;
	}

	/**
	 * @return the hasFinalCutOverPrivilege
	 */
	public boolean isHasFinalCutOverPrivilege() {
		return hasFinalCutOverPrivilege;
	}

	/**
	 * @param hasFinalCutOverPrivilege
	 *            the hasFinalCutOverPrivilege to set
	 */
	public void setHasFinalCutOverPrivilege(boolean hasFinalCutOverPrivilege) {
		this.hasFinalCutOverPrivilege = hasFinalCutOverPrivilege;
	}

	public LCCReservationBaggageSummaryTo getBaggageSummaryTo() {
		return baggageSummaryTo;
	}

	public void setBaggageSummaryTo(LCCReservationBaggageSummaryTo baggageSummaryTo) {
		this.baggageSummaryTo = baggageSummaryTo;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public ApplicationEngine getAppEngine() {
		return appEngine;
	}

	public void setAppEngine(ApplicationEngine appEngine) {
		this.appEngine = appEngine;
	}

	public boolean isRequote() {
		return isRequote;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public List<BundledFareDTO> getBundledFareDTOs() {
		return bundledFareDTOs;
	}

	public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
		this.bundledFareDTOs = bundledFareDTOs;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public BasicTrackInfo getTrackInfo() {
		return trackInfo;
	}

	public void setTrackInfo(BasicTrackInfo trackInfo) {
		this.trackInfo = trackInfo;
	}

	public Map<String, Map<String, Integer>> getCarrierWiseOndWiseBundlePeriodID() {
		return carrierWiseOndWiseBundlePeriodID;
	}

	public void setCarrierWiseOndWiseBundlePeriodID(Map<String, Map<String, Integer>> carrierWiseOndWiseBundlePeriodID) {
		this.carrierWiseOndWiseBundlePeriodID = carrierWiseOndWiseBundlePeriodID;
	}
}
