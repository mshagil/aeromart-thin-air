package com.isa.thinair.airproxy.core.bl;

import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.ReportingUtil;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;
import com.isa.thinair.airschedules.api.dto.FlightManifestSearchDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class AirproxyFlightServiceBL {

	public static Page searchFlightsForDisplay(FlightManifestSearchDTO searchCriteria, BasicTrackInfo trackInfo)
			throws ModuleException {
		Page flightsPage = null;
		if (!searchCriteria.isInterline()) {
			flightsPage = AirproxyModuleUtils.getFlightBD().searchFlightsForDisplay(searchCriteria.getCriteria(),
					searchCriteria.getStartIndex(), searchCriteria.getPageSize(), searchCriteria.isDatesInLocal(),
					searchCriteria.getFlightCriteria(), searchCriteria.isFlightManifest(),
					searchCriteria.isDataFromReporting(), searchCriteria.getAgentApplicableOnds());
		} else {
			flightsPage = AirproxyModuleUtils.getLCCFlightServiceBD().searchFlightsForDisplay(searchCriteria, trackInfo);
		}
		return flightsPage;
	}

	public static String viewFlightManifest(FlightManifestOptionsDTO flightManifestOptionsDTO, BasicTrackInfo trackInfo)
			throws ModuleException {
		String executingCarrierCode = flightManifestOptionsDTO.getFlightNo().substring(0, 2);
		Set<String> ownCarrierCodes = CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodes();
		if (ownCarrierCodes.contains(executingCarrierCode)) {
			return AirproxyModuleUtils.getAirReservationQueryBD().viewFlightManifest(flightManifestOptionsDTO);
		} else {
			return (String) AirproxyModuleUtils.getLCCFlightServiceBD()
					.getFlightManifestData(flightManifestOptionsDTO, trackInfo);
		}
	}

	public static boolean sendFlightManifestAsEmail(FlightManifestOptionsDTO flightManifestOptionsDTO, BasicTrackInfo trackInfo)
			throws ModuleException {
		Set<String> ownCarrierCodes = CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodes();
		String executingCarrierCode = flightManifestOptionsDTO.getFlightNo().substring(0, 2);
		if (ownCarrierCodes.contains(executingCarrierCode)) {
			return AirproxyModuleUtils.getAirReservationQueryBD().sendFlightManifestAsEmail(flightManifestOptionsDTO);
		} else {
			return (Boolean) AirproxyModuleUtils.getLCCFlightServiceBD().getFlightManifestData(flightManifestOptionsDTO,
					trackInfo);

		}
	}

	public static List<FlightLoadReportDataDTO> getFlightLoadData(ReportsSearchCriteria search, BasicTrackInfo trackInfo)
			throws ModuleException {
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		String executingCarrierCode = search.getCarrierCode();
		if (carrierCode.equals(executingCarrierCode)) {
			return ReportingUtil.composeFlightLoadReportDataDTOs(AirproxyModuleUtils.getDataExtractionBD().getFlightLoad(search));
		} else {
			return AirproxyModuleUtils.getLCCFlightServiceBD().getFlightLoadData(search, trackInfo);
		}

	}
}
