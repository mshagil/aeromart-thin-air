package com.isa.thinair.airproxy.api.model.masterDataPublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;

/**
 * The Class AirportDSTPublishRQDataTO.
 */
public class AirportDSTPublishRQDataTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The airport. */
	private Airport airport;

	/** The airport ds ts. */
	private List<AirportDST> airportDSTs;

	private String countryCode;

	/**
	 * Gets the aiport.
	 * 
	 * @return the aiport
	 */
	public Airport getAiport() {
		return airport;
	}

	/**
	 * Sets the airport.
	 * 
	 * @param airport
	 *            the new airport
	 */
	public void setAirport(Airport airport) {
		this.airport = airport;
	}

	/**
	 * Gets the airport ds ts.
	 * 
	 * @return the airport ds ts
	 */
	public List<AirportDST> getAirportDSTs() {
		if (airportDSTs == null) {
			airportDSTs = new ArrayList<AirportDST>();
		}
		return airportDSTs;
	}

	/**
	 * Sets the airport ds ts.
	 * 
	 * @param airportDSTs
	 *            the new airport ds ts
	 */
	public void setAirportDSTs(List<AirportDST> airportDSTs) {
		this.airportDSTs = airportDSTs;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
