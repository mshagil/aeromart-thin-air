package com.isa.thinair.airproxy.api.model.userInfoPublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserInfoPublishRSDataTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<String> updatedUserIds;

	private List<String> updatedRoleIds;

	private List<String> updatedAgentCodes;

	private List<String> errors;

	public List<String> getUpdatedUserIds() {
		if (updatedUserIds == null) {
			updatedUserIds = new ArrayList<String>();
		}
		return updatedUserIds;
	}

	public void setUpdatedUserIds(List<String> updatedUserIds) {
		this.updatedUserIds = updatedUserIds;
	}

	public List<String> getUpdatedRoleIds() {
		if (updatedRoleIds == null) {
			updatedRoleIds = new ArrayList<String>();
		}
		return updatedRoleIds;
	}

	public void setUpdatedRoleIds(List<String> updatedRoleIds) {
		this.updatedRoleIds = updatedRoleIds;
	}

	public List<String> getUpdatedAgentCodes() {
		if (updatedAgentCodes == null) {
			updatedAgentCodes = new ArrayList<String>();
		}
		return updatedAgentCodes;
	}

	public void setUpdatedAgentCodes(List<String> updatedAgentCodes) {
		this.updatedAgentCodes = updatedAgentCodes;
	}

	public List<String> getErrors() {
		if (errors == null) {
			errors = new ArrayList<String>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

}
