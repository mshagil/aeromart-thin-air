package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

public class LCCOfflinePaymentDetails implements Serializable {

	private static final long serialVersionUID = 7725555514026176222L;

	private String paymentGatewayReferenceKey;
	private String paymentGatewayReference;

	public String getPaymentGatewayReferenceKey() {
		return paymentGatewayReferenceKey;
	}

	public void setPaymentGatewayReferenceKey(String paymentGatewayReferenceKey) {
		this.paymentGatewayReferenceKey = paymentGatewayReferenceKey;
	}

	public String getPaymentGatewayReference() {
		return paymentGatewayReference;
	}

	public void setPaymentGatewayReference(String paymentGatewayReference) {
		this.paymentGatewayReference = paymentGatewayReference;
	}
}
