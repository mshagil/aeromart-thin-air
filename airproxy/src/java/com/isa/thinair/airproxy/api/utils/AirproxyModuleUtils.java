package com.isa.thinair.airproxy.api.utils;

import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.AirportTransferBD;
import com.isa.thinair.airmaster.api.service.AutomaticCheckinBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airproxy.api.service.AirproxyReservationBD;
import com.isa.thinair.airproxy.core.config.AirproxyModuleConfig;
import com.isa.thinair.airreservation.api.service.BlacklistPAXBD;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.gdsservices.api.service.GdsRequiredBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.lccclient.api.service.LCCAlertingBD;
import com.isa.thinair.lccclient.api.service.LCCAncillaryBD;
import com.isa.thinair.lccclient.api.service.LCCFlightServiceBD;
import com.isa.thinair.lccclient.api.service.LCCPaxCreditBD;
import com.isa.thinair.lccclient.api.service.LCCReservationBD;
import com.isa.thinair.lccclient.api.service.LCCSearchAndQuoteBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.promotion.api.service.AnciOfferBD;
import com.isa.thinair.promotion.api.service.BunldedFareBD;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.service.PromotionCriteriaAdminBD;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.wsclient.api.service.InsuranceClientBD;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

public class AirproxyModuleUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(AirproxyConstants.MODULE_NAME);
	}

	public static AirproxyModuleConfig getAirproxyConfig() {
		return (AirproxyModuleConfig) getInstance().getModuleConfig();
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirproxyConfig(), "airproxy",
				"airproxy.config.dependencymap.invalid");
	}

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality,
				AirproxyModuleUtils.getAirproxyConfig(), "airproxy", "airproxy.config.dependencymap.invalid");
	}

	public static ReservationQueryBD getAirReservationQueryBD() {
		return (ReservationQueryBD) AirproxyModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationQueryBD.SERVICE_NAME);
	}

	public static LCCSearchAndQuoteBD getLCCSearchAndQuoteBD() {
		return (LCCSearchAndQuoteBD) AirproxyModuleUtils.lookupEJB3Service(LccclientConstants.MODULE_NAME,
				LCCSearchAndQuoteBD.SERVICE_NAME);
	}

	public static LCCAncillaryBD getLCCAncillaryBD() {
		return (LCCAncillaryBD) AirproxyModuleUtils
				.lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCAncillaryBD.SERVICE_NAME);
	}

	public static LCCPaxCreditBD getLCCPaxCreditBD() {
		return (LCCPaxCreditBD) AirproxyModuleUtils
				.lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCPaxCreditBD.SERVICE_NAME);
	}

	public static LCCReservationBD getLCCReservationBD() {
		return (LCCReservationBD) AirproxyModuleUtils.lookupEJB3Service(LccclientConstants.MODULE_NAME,
				LCCReservationBD.SERVICE_NAME);
	}

	public static ReservationBD getReservationBD() {
		return (ReservationBD) AirproxyModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) AirproxyModuleUtils.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
				TravelAgentBD.SERVICE_NAME);
	}

	public static AirportBD getAirportBD() {
		return (AirportBD) AirproxyModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}

	public final static SegmentBD getResSegmentBD() {
		return (SegmentBD) AirproxyModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public static FlightBD getFlightBD() {
		return (FlightBD) AirproxyModuleUtils.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static LogicalCabinClassBD getLogicalCabinClassBD() {
		return (LogicalCabinClassBD) AirproxyModuleUtils.lookupEJB3Service(AirschedulesConstants.MODULE_NAME,
				LogicalCabinClassBD.SERVICE_NAME);
	}

	public final static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) AirproxyModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}

	public static SsrBD getSsrServiceBD() {
		return (SsrBD) AirproxyModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, SsrBD.SERVICE_NAME);
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) AirproxyModuleUtils
				.lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public final static MealBD getMealBD() {

		return (MealBD) AirproxyModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, MealBD.SERVICE_NAME);
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public static WSClientBD getWSClientBD() {
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}
	
	public static InsuranceClientBD getInsuranceClientBD() {
		return (InsuranceClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, InsuranceClientBD.SERVICE_NAME);
	}

	public static PassengerBD getPassengerBD() {
		return (PassengerBD) AirproxyModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) AirproxyModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				FlightInventoryResBD.SERVICE_NAME);
	}

	public final static AlertingBD getAlertingBD() {
		return (AlertingBD) AirproxyModuleUtils.lookupServiceBD(AlertingConstants.MODULE_NAME,
				AlertingConstants.BDKeys.ALERTING_SERVICE);
	}

	public final static FareBD getFareBD() {
		return (FareBD) AirproxyModuleUtils.lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	public final static SecurityBD getSecurityBD() {
		return (SecurityBD) AirproxyModuleUtils.lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	public static ReservationAuxilliaryBD getReservationAuxilliaryBD() {
		return (ReservationAuxilliaryBD) AirproxyModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationAuxilliaryBD.SERVICE_NAME);
	}

	public static ScheduleBD getScheduleBD() {
		return (ScheduleBD) AirproxyModuleUtils.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}

	public static AircraftBD getAirCraftBD() {
		return (AircraftBD) AirproxyModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AircraftBD.SERVICE_NAME);
	}

	public static LCCAlertingBD getLCCAlertingBD() {
		return (LCCAlertingBD) AirproxyModuleUtils.lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCAlertingBD.SERVICE_NAME);
	}

	public static LCCFlightServiceBD getLCCFlightServiceBD() {
		return (LCCFlightServiceBD) AirproxyModuleUtils.lookupEJB3Service(LccclientConstants.MODULE_NAME,
				LCCFlightServiceBD.SERVICE_NAME);
	}

	public static DataExtractionBD getDataExtractionBD() {
		return (DataExtractionBD) AirproxyModuleUtils.lookupEJB3Service(ReportingConstants.MODULE_NAME,
				DataExtractionBD.SERVICE_NAME);
	}

	/**
	 * Baggage Delegate
	 * 
	 * @return
	 */
	public final static BaggageBusinessDelegate getBaggageBusinessDelegate() {

		return (BaggageBusinessDelegate) AirproxyModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);
	}
	
	public final static AirportTransferBD getAirportTransferBD() {
		return (AirportTransferBD) AirproxyModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				AirportTransferBD.SERVICE_NAME);
	}

	public final static AutomaticCheckinBD getAutomaticCheckinBD() {
		return (AutomaticCheckinBD) AirproxyModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				AutomaticCheckinBD.SERVICE_NAME);
	}

	public static LocationBD getLocationDB() {
		return (LocationBD) AirproxyModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	public static AirproxyReservationBD getAirproxyReservationBD() {
		return (AirproxyReservationBD) AirproxyModuleUtils.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyReservationBD.SERVICE_NAME);
	}

	public static PromotionCriteriaAdminBD getPromotionCriteriaAdminBD() {
		return (PromotionCriteriaAdminBD) AirproxyModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionCriteriaAdminBD.SERVICE_NAME);
	}

	public final static PromotionManagementBD getPromotionManagementBD() {
		return (PromotionManagementBD) AirproxyModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionManagementBD.SERVICE_NAME);
	}

	public static ChargeBD getChargeBD() {
		return (ChargeBD) AirproxyModuleUtils.lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	public final static AnciOfferBD getAnciOfferBD() {
		return (AnciOfferBD) AirproxyModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME, AnciOfferBD.SERVICE_NAME);
	}

	public static final BunldedFareBD getBundledFareBD() {
		return (BunldedFareBD) AirproxyModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME, BunldedFareBD.SERVICE_NAME);
	}

	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) AirproxyModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				LoyaltyManagementBD.SERVICE_NAME);
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}
	
	public final static GdsRequiredBD getGdsRequiredBD() {
		return (GdsRequiredBD) lookupEJB3Service(GdsservicesConstants.MODULE_NAME, GdsRequiredBD.SERVICE_NAME);
	}

	public final static BlacklistPAXBD getBlacklisPAXBD() {
		return (BlacklistPAXBD) AirproxyModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				BlacklistPAXBD.SERVICE_NAME);
	}

	public static VoucherBD getVoucherBD() {
		return (VoucherBD) AirproxyModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME, VoucherBD.SERVICE_NAME);
	}
}
