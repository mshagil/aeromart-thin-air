package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class LCCSegmentSeatMapDTO implements Serializable, Comparable<LCCSegmentSeatMapDTO> {

	private static final long serialVersionUID = 1L;
	private FlightSegmentTO flightSegmentDTO;
	private LCCSeatMapDetailDTO lccSeatMapDetailDTO;

	private String anciOfferName;

	private String anciOfferDescription;

	private boolean isAnciOffer;

	private Integer offeredAnciTemplateID;

	public FlightSegmentTO getFlightSegmentDTO() {
		return flightSegmentDTO;
	}

	public void setFlightSegmentDTO(FlightSegmentTO flightSegmentDTO) {
		this.flightSegmentDTO = flightSegmentDTO;
	}

	public LCCSeatMapDetailDTO getLccSeatMapDetailDTO() {
		return lccSeatMapDetailDTO;
	}

	public void setLccSeatMapDetailDTO(LCCSeatMapDetailDTO lccSeatMapDetailDTO) {
		this.lccSeatMapDetailDTO = lccSeatMapDetailDTO;
	}

	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	public String getAnciOfferDescription() {
		return anciOfferDescription;
	}

	public void setAnciOfferDescription(String anciOfferDescription) {
		this.anciOfferDescription = anciOfferDescription;
	}

	public String getAnciOfferName() {
		return anciOfferName;
	}

	public void setAnciOfferName(String anciOfferName) {
		this.anciOfferName = anciOfferName;
	}

	public Integer getOfferedAnciTemplateID() {
		return offeredAnciTemplateID;
	}

	public void setOfferedAnciTemplateID(Integer offeredAnciTemplateID) {
		this.offeredAnciTemplateID = offeredAnciTemplateID;
	}

	@Override
	public int compareTo(LCCSegmentSeatMapDTO o) {
		return this.flightSegmentDTO.getDepartureDateTime().compareTo(o.getFlightSegmentDTO().getDepartureDateTime());
	}

}
