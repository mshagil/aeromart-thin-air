package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airmaster.api.dto.ServiceProviderDTO;

public class LCCAirportServiceDTO implements Serializable {
	private static final long serialVersionUID = 2266802535767811692L;

	public static final String APPLICABILITY_TYPE_PAX = "P";
	public static final String APPLICABILITY_TYPE_RESERVATION = "R";

	private String ssrCode;
	private String ssrName;
	private String ssrDescription;
	private String status;
	private String applicabilityType;
	private BigDecimal adultAmount;
	private BigDecimal childAmount;
	private BigDecimal infantAmount;
	private BigDecimal reservationAmount;
	// This holds the actual charge for the service after applicability check
	private BigDecimal serviceCharge;
	private String airportCode;
	private String carrierCode;
	private String applyOn;
	private String ssrImagePath;
	private String ssrThumbnailImagePath;
	private boolean alertAutoCancellation;

	private Integer airportTransferId;
	private String tranferDate;
	private String transferAddress;
	private String transferContact;
	private String transferType;
	private ServiceProviderDTO provider;
	private boolean existing;
	
	private String serviceNumber = null;
	
	private Long ssrID;
	private boolean isFree = false;
	
	/**
	 * @return the ssrID
	 */
	public Long getSsrID() {
		return ssrID;
	}

	/**
	 * @param ssrID
	 *            the ssrID to set
	 */
	public void setSsrID(Long ssrID) {
		this.ssrID = ssrID;
	}
	
	/**
	 * @return the isFree
	 */
	public boolean getIsFree() {
		return isFree;
	}

	/**
	 * @param isFree
	 *            set if the service is free
	 */
	public void setIsFree(boolean isFree) {
		this.isFree = isFree;
	}	

	/**
	 * @return the ssrCode
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            the ssrCode to set
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return the ssrName
	 */
	public String getSsrName() {
		return ssrName;
	}

	/**
	 * @param ssrName
	 *            the ssrName to set
	 */
	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}

	/**
	 * @return the ssrDescription
	 */
	public String getSsrDescription() {
		return ssrDescription;
	}

	/**
	 * @param ssrDescription
	 *            the ssrDescription to set
	 */
	public void setSsrDescription(String ssrDescription) {
		this.ssrDescription = ssrDescription;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the applicabilityType
	 */
	public String getApplicabilityType() {
		return applicabilityType;
	}

	/**
	 * @param applicabilityType
	 *            the applicabilityType to set
	 */
	public void setApplicabilityType(String applicabilityType) {
		this.applicabilityType = applicabilityType;
	}

	/**
	 * @return the adultAmount
	 */
	public BigDecimal getAdultAmount() {
		return adultAmount;
	}

	/**
	 * @param adultAmount
	 *            the adultAmount to set
	 */
	public void setAdultAmount(BigDecimal adultAmount) {
		this.adultAmount = adultAmount;
	}

	/**
	 * @return the childAmount
	 */
	public BigDecimal getChildAmount() {
		return childAmount;
	}

	/**
	 * @param childAmount
	 *            the childAmount to set
	 */
	public void setChildAmount(BigDecimal childAmount) {
		this.childAmount = childAmount;
	}

	/**
	 * @return the infantAmount
	 */
	public BigDecimal getInfantAmount() {
		return infantAmount;
	}

	/**
	 * @param infantAmount
	 *            the infantAmount to set
	 */
	public void setInfantAmount(BigDecimal infantAmount) {
		this.infantAmount = infantAmount;
	}

	/**
	 * @return the reservationAmount
	 */
	public BigDecimal getReservationAmount() {
		return reservationAmount;
	}

	/**
	 * @param reservationAmount
	 *            the reservationAmount to set
	 */
	public void setReservationAmount(BigDecimal reservationAmount) {
		this.reservationAmount = reservationAmount;
	}

	/**
	 * @return the serviceCharge
	 */
	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}

	/**
	 * @param serviceCharge
	 *            the serviceCharge to set
	 */
	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the applyOn
	 */
	public String getApplyOn() {
		return applyOn;
	}

	/**
	 * @param applyOn
	 *            the applyOn to set
	 */
	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	/**
	 * @return the ssrImagePath
	 */
	public String getSsrImagePath() {
		return ssrImagePath;
	}

	/**
	 * @param ssrImagePath
	 *            the ssrImagePath to set
	 */
	public void setSsrImagePath(String ssrImagePath) {
		this.ssrImagePath = ssrImagePath;
	}

	/**
	 * @return the ssrThumbnailImagePath
	 */
	public String getSsrThumbnailImagePath() {
		return ssrThumbnailImagePath;
	}

	/**
	 * @param ssrThumbnailImagePath
	 *            the ssrThumbnailImagePath to set
	 */
	public void setSsrThumbnailImagePath(String ssrThumbnailImagePath) {
		this.ssrThumbnailImagePath = ssrThumbnailImagePath;
	}

	/**
	 * @return the alertAutoCancellation
	 */
	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	/**
	 * @param alertAutoCancellation
	 *            the alertAutoCancellation to set
	 */
	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	public String getTranferDate() {
		return tranferDate;
	}

	public void setTranferDate(String tranferDate) {
		this.tranferDate = tranferDate;
	}

	public String getTransferAddress() {
		return transferAddress;
	}

	public void setTransferAddress(String transferAddress) {
		this.transferAddress = transferAddress;
	}

	public String getTransferContact() {
		return transferContact;
	}

	public void setTransferContact(String transferContact) {
		this.transferContact = transferContact;
	}

	public ServiceProviderDTO getProvider() {
		return provider;
	}

	public void setProvider(ServiceProviderDTO provider) {
		this.provider = provider;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public Integer getAirportTransferId() {
		if (airportTransferId == null && provider != null && provider.getAirportTransferId() != null) {
			return provider.getAirportTransferId();
		}
		return airportTransferId;
	}

	public void setAirportTransferId(Integer airportTransferId) {
		this.airportTransferId = airportTransferId;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public boolean isExisting() {
		return existing;
	}

	public void setExisting(boolean existing) {
		this.existing = existing;
	}

}
