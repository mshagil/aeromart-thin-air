package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

public class GDSChargeAdjustmentRQ implements Serializable {

	private static final long serialVersionUID = -1379957412579860540L;

	private String pnr;
	private boolean groupPNR;
	private Map<Integer, GDSPaxChargesTO> gdsChargesByPax = null;
	private String version;
	private TrackInfoDTO trackInfo = null;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public Map<Integer, GDSPaxChargesTO> getGdsChargesByPax() {
		return gdsChargesByPax;
	}

	public void setGdsChargesByPax(Map<Integer, GDSPaxChargesTO> gdsChargesByPax) {
		this.gdsChargesByPax = gdsChargesByPax;
	}

	public void addGdsCharges(Integer paxId, GDSPaxChargesTO paxChargesTO) {
		if (gdsChargesByPax == null)
			gdsChargesByPax = new HashMap<Integer, GDSPaxChargesTO>();

		gdsChargesByPax.put(paxId, paxChargesTO);
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public TrackInfoDTO getTrackInfo() {
		return trackInfo;
	}

	public void setTrackInfo(TrackInfoDTO trackInfo) {
		this.trackInfo = trackInfo;
	}

}
