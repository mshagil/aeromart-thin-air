package com.isa.thinair.airproxy.api.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleSearchRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;

public interface AirproxyReservationQueryBD {

	public static final String SERVICE_NAME = "AirproxyReservationService";

	public FlightAvailRS searchAvailableFlights(FlightAvailRQ flightAvailRQ, TrackInfoDTO trackInfo, boolean withAllFares)
			throws ModuleException;

	public FlightAvailRS searchAvailableFlightsWithAllFares(FlightAvailRQ flightAvailRQ, TrackInfoDTO trackInfo)
			throws ModuleException;

	public FlightPriceRS quoteFlightPrice(FlightPriceRQ priceQuoteRQ, TrackInfoDTO trackInfo) throws ModuleException;

	public FareAvailRS searchFares(FareAvailRQ fareAvailRQ, BasicTrackInfo trackInfo) throws ModuleException;

	public FlightPriceRS changeFlightPrice(ChangeFareRQ changeFareRQ, BasicTrackInfo trackInfo) throws ModuleException;

	public Collection<?>
			priviledgeUserBlockSeat(FlightPriceRQ priceQuoteRQ, FareSegChargeTO fareSegChargeTO)
					throws ModuleException;

	public ScheduleAvailRS getSchedulesList(ScheduleAvailRQ scheduleAvailRQ, BasicTrackInfo trackInfo) throws ModuleException;

	public ScheduleSearchRS getSchedulesList(ScheduleSearchRQ scheduleSearchRQ) throws ModuleException;

	public boolean isOnholdEnabled(List<FlightSegmentTO> flightSegmentTOs, List<ReservationPax> paxList, String email,
			int validationType, int salesChannel, SYSTEM searchSystem, String ipAddress, BasicTrackInfo trackInfo)
			throws ModuleException;

	public List<RollForwardFlightAvailRS> searchAvailableFlightForRollForward(Collection<LCCClientReservationSegment> colsegs,
			Date startDate, Date endDate, Integer adultCount, Integer childCount, Integer infantCount, SYSTEM searchSystem,
			boolean isOverBook, String agentCode) throws ModuleException;

	public boolean isDuplicateNameExist(Collection<Integer> fltSegmentIds, String pnr) throws ModuleException;

	public Integer getAppliedFareDiscountPercentage(String pnr, boolean isGropPNR) throws ModuleException;

	public AnciAvailabilityRS getTaxApplicabilityForAncillaries(String carrierCode, String segmentCode, EXTERNAL_CHARGES extChg,
			BasicTrackInfo trackInfo) throws ModuleException;

	public ApplicablePromotionDetailsTO pickApplicablePromotions(PromoSelectionCriteria promoSelectionCriteria,
			SYSTEM prefSystem, BasicTrackInfo trackInfo) throws ModuleException;
}
