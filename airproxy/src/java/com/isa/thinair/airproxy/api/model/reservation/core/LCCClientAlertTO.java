package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

/**
 * The Class LCCClientAlertTO.
 */
public class LCCClientAlertTO implements Serializable {

	private static final long serialVersionUID = -7197178119446241379L;

	/** The content. */
	private String content;

	/** The alert id. */
	private Integer alertId;

	/** The IBE visible only flag. */
	private String ibeVisibleOnly;
	
	/** Has actioned using the IBE flag */
	private String ibeActioned;
	
	/** Has flight segments assigned to the alertId */
	private boolean hasFlightSegments;

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Gets the alert id.
	 * 
	 * @return the alert id
	 */
	public int getAlertId() {
		return alertId;
	}

	/**
	 * Sets the alert id.
	 * 
	 * @param alertId
	 *            the new alert id
	 */
	public void setAlertId(int alertId) {
		this.alertId = alertId;
	}

	/**
	 * Gets the IBE_visible_only flag.
	 * 
	 * @return the IBE_visible_only
	 */
	public String getIbeVisibleOnly() {
		return ibeVisibleOnly;
	}

	/**
	 * Sets the IBE_visible_only flag.
	 * 
	 * @param ibevisibleonly
	 *            the new IBE_visible_only
	 */
	public void setIbeVisibleOnly(String ibeVisibleOnly) {
		this.ibeVisibleOnly = ibeVisibleOnly;
	}

	/**
	 * @return the ibeActioned
	 */
	public String getIbeActioned() {
		return ibeActioned;
	}

	/**
	 * @param ibeActioned the ibeActioned to set
	 */
	public void setIbeActioned(String ibeActioned) {
		this.ibeActioned = ibeActioned;
	}

	/**
	 * @return the hasFlightSegments
	 */
	public boolean isHasFlightSegments() {
		return hasFlightSegments;
	}

	/**
	 * @param hasFlightSegments the hasFlightSegments to set
	 */
	public void setHasFlightSegments(boolean hasFlightSegments) {
		this.hasFlightSegments = hasFlightSegments;
	}

}
