package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class FareAvailRQ extends BaseAvailRQ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean excludeNonLowestPublicFares = false;

	private int salesChannelCode = -1;

	private String travelAgentCode = null;

	private String selectedOBFltRefNo;

	private String selectedIBFltRefNo;

	private List<FlightSegmentTO> outboundFlightSegments;

	private List<FlightSegmentTO> inboundFlightSegments;

	private boolean excludeOnewayPublicFaresFromReturnJourneys;

	public FareAvailRQ() {
	}

	public FareAvailRQ(BaseAvailRQ baseAvail) {
		super(baseAvail);
	}

	public boolean isExcludeNonLowestPublicFares() {
		return excludeNonLowestPublicFares;
	}

	public void setExcludeNonLowestPublicFares(boolean excludeNonLowestPublicFares) {
		this.excludeNonLowestPublicFares = excludeNonLowestPublicFares;
	}

	public List<FlightSegmentTO> getOutboundFlightSegments() {
		if (outboundFlightSegments == null) {
			outboundFlightSegments = new ArrayList<FlightSegmentTO>();
		}
		return outboundFlightSegments;
	}

	public void setOutboundFlightSegments(List<FlightSegmentTO> outboundFlightSegments) {
		this.outboundFlightSegments = outboundFlightSegments;
	}

	public List<FlightSegmentTO> getInboundFlightSegments() {
		if (inboundFlightSegments == null) {
			inboundFlightSegments = new ArrayList<FlightSegmentTO>();
		}
		return inboundFlightSegments;
	}

	public void setInboundFlightSegments(List<FlightSegmentTO> inboundFlightSegments) {
		this.inboundFlightSegments = inboundFlightSegments;
	}

	public int getSalesChannelCode() {
		return salesChannelCode;
	}

	public void setSalesChannelCode(int salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	public String getTravelAgentCode() {
		return travelAgentCode;
	}

	public void setTravelAgentCode(String travelAgentCode) {
		this.travelAgentCode = travelAgentCode;
	}

	public String getSelectedOBFltRefNo() {
		return selectedOBFltRefNo;
	}

	public void setSelectedOBFltRefNo(String selectedOBFltRefNo) {
		this.selectedOBFltRefNo = selectedOBFltRefNo;
	}

	public String getSelectedIBFltRefNo() {
		return selectedIBFltRefNo;
	}

	public void setSelectedIBFltRefNo(String selectedIBFltRefNo) {
		this.selectedIBFltRefNo = selectedIBFltRefNo;
	}

	public boolean isExcludeOnewayPublicFaresFromReturnJourneys() {
		return excludeOnewayPublicFaresFromReturnJourneys;
	}

	public void setExcludeOnewayPublicFaresFromReturnJourneys(boolean excludeOnewayPublicFaresFromReturnJourneys) {
		this.excludeOnewayPublicFaresFromReturnJourneys = excludeOnewayPublicFaresFromReturnJourneys;
	}

}
