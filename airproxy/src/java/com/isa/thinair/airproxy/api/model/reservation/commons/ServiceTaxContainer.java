package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author rumesh
 * 
 */
public class ServiceTaxContainer implements Serializable {

	private static final long serialVersionUID = -8448346902248994090L;

	private boolean taxApplicable;

	private BigDecimal taxRatio = AccelAeroCalculator.getDefaultBigDecimalZero();

	private EXTERNAL_CHARGES externalCharge;

	private Set<EXTERNAL_CHARGES> taxableExternalCharges = new HashSet<EXTERNAL_CHARGES>();

	private String taxApplyingFlightRefNumber;

	public ServiceTaxContainer() {

	}

	public ServiceTaxContainer(boolean taxApplicable, BigDecimal taxRatio, EXTERNAL_CHARGES externalCharge) {
		this.taxApplicable = taxApplicable;
		this.taxRatio = taxRatio;
		this.externalCharge = externalCharge;
	}

	public boolean isTaxApplicable() {
		return taxApplicable;
	}

	public void setTaxApplicable(boolean taxApplicable) {
		this.taxApplicable = taxApplicable;
	}

	public BigDecimal getTaxRatio() {
		return taxRatio;
	}

	public void setTaxRatio(BigDecimal taxRatio) {
		this.taxRatio = taxRatio;
	}

	public EXTERNAL_CHARGES getExternalCharge() {
		return externalCharge;
	}

	public void setExternalCharge(EXTERNAL_CHARGES externalCharge) {
		this.externalCharge = externalCharge;
	}

	public Set<EXTERNAL_CHARGES> getTaxableExternalCharges() {
		return taxableExternalCharges;
	}

	public void setTaxableExternalCharges(Set<EXTERNAL_CHARGES> taxableExternalCharges) {
		this.taxableExternalCharges = taxableExternalCharges;
	}

	public String getTaxApplyingFlightRefNumber() {
		return taxApplyingFlightRefNumber;
	}

	public void setTaxApplyingFlightRefNumber(String taxApplyingFlightRefNumber) {
		this.taxApplyingFlightRefNumber = taxApplyingFlightRefNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((externalCharge == null) ? 0 : externalCharge.hashCode());
		result = prime * result + (taxApplicable ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ServiceTaxContainer)) {
			return false;
		}
		ServiceTaxContainer other = (ServiceTaxContainer) obj;
		if (externalCharge != other.externalCharge) {
			return false;
		}
		if (taxApplicable != other.taxApplicable) {
			return false;
		}
		return true;
	}

}
