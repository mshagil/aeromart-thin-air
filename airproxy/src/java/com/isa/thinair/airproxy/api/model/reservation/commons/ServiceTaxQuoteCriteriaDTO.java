package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

public class ServiceTaxQuoteCriteriaDTO extends ServiceTaxBaseCriteriaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String lccTransactionIdentifier;

	public String getLccTransactionIdentifier() {
		return lccTransactionIdentifier;
	}

	public void setLccTransactionIdentifier(String lccTransactionIdentifier) {
		this.lccTransactionIdentifier = lccTransactionIdentifier;
	}

}
