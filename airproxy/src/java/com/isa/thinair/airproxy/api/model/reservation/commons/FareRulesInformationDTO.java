package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.List;

public class FareRulesInformationDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<FareRuleDTO> fareRules;
	private boolean adultApplicable;
	private boolean childApplicable;
	private boolean infantApplicable;
	private boolean hasComment;
	private boolean hasAgentInstructions;

	public List<FareRuleDTO> getFareRules() {
		return fareRules;
	}

	public void setFareRules(List<FareRuleDTO> fareRules) {
		this.fareRules = fareRules;
	}

	public boolean isAdultApplicable() {
		return adultApplicable;
	}

	public void setAdultApplicable(boolean adultApplicable) {
		this.adultApplicable = adultApplicable;
	}

	public boolean isChildApplicable() {
		return childApplicable;
	}

	public void setChildApplicable(boolean childApplicable) {
		this.childApplicable = childApplicable;
	}

	public boolean isInfantApplicable() {
		return infantApplicable;
	}

	public void setInfantApplicable(boolean infantApplicable) {
		this.infantApplicable = infantApplicable;
	}

	public boolean isHasComment() {
		return hasComment;
	}

	public void setHasComment(boolean hasComment) {
		this.hasComment = hasComment;
	}

	/**
	 * @return the hasAgentInstructions
	 */
	public boolean isHasAgentInstructions() {
		return hasAgentInstructions;
	}

	/**
	 * @param hasAgentInstructions
	 *            the hasAgentInstructions to set
	 */
	public void setHasAgentInstructions(boolean hasAgentInstructions) {
		this.hasAgentInstructions = hasAgentInstructions;
	}
}
