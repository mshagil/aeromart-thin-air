package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ClassOfServicePreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;

public class BaseAvailRQ implements Serializable {

	private static final long serialVersionUID = 1L;

	private TravelerInfoSummaryTO travelerInfoSummary;

	private TravelPreferencesTO travelPreferences;

	private AvailPreferencesTO availPreferences;

	private List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();

	private String transactionIdentifier;

	private ClassOfServicePreferencesTO classOfServicePrefTO;
	
	private String customerId;;

	public BaseAvailRQ() {

	}

	public BaseAvailRQ(BaseAvailRQ baseAvailIn) {
		setOriginDestinationInformationList(baseAvailIn.getOriginDestinationInformationList());
		TravelerInfoSummaryTO travellerInfo = getTravelerInfoSummary();
		AvailPreferencesTO availPref = getAvailPreferences();
		TravelPreferencesTO travelPref = getTravelPreferences();
		// ClassOfServicePreferencesTO cosPref = getClassOfServicePrefTO();

		AvailPreferencesTO bAvailPref = baseAvailIn.getAvailPreferences();
		TravelPreferencesTO bTravelPref = baseAvailIn.getTravelPreferences();
		// ClassOfServicePreferencesTO bCosPref = baseAvailIn.getClassOfServicePrefTO();

		travellerInfo.getPassengerTypeQuantityList().addAll(baseAvailIn.getTravelerInfoSummary().getPassengerTypeQuantityList());

		availPref.setAppIndicator(bAvailPref.getAppIndicator());
		availPref.setBookingPaxType(bAvailPref.getBookingPaxType());
		availPref.setExistingSegmentsList(bAvailPref.getExistingSegmentsList());
		availPref.setFareCategoryType(bAvailPref.getFareCategoryType());
		availPref.setFirstDepatureDate(bAvailPref.getFirstDepatureDate());
		availPref.setFlightsPerOndRestriction(bAvailPref.getFlightsPerOndRestriction());
		availPref.setForceHalfReturnSearch(bAvailPref.isForceHalfReturnSearch());
		availPref.setHalfReturnFareQuote(bAvailPref.isHalfReturnFareQuote());
		availPref.setRequoteFlightSearch(bAvailPref.isRequoteFlightSearch());
		availPref.setInboundFareModified(bAvailPref.isInboundFareModified());
		availPref.setInverseFareID(bAvailPref.getInverseFareID());
		availPref.setInverseSegmentsList(bAvailPref.getInverseSegmentsList());
		availPref.setLastArrivalDate(bAvailPref.getLastArrivalDate());
		availPref.setModifiedSegmentsList(bAvailPref.getModifiedSegmentsList());
		availPref.setModifyBooking(bAvailPref.isModifyBooking());
		availPref.setSourceFlightInCutOffTime(bAvailPref.isSourceFlightInCutOffTime());
		availPref.setOldFareAmount(bAvailPref.getOldFareAmount());
		availPref.setOldFareCarrierCode(bAvailPref.getOldFareCarrierCode());
		availPref.setOldFareID(bAvailPref.getOldFareID());
		availPref.setOldFareType(bAvailPref.getOldFareType());
		availPref.setOldflightSegmentList(bAvailPref.getOldflightSegmentList());
		availPref.setParticipatingCarriers(bAvailPref.getParticipatingCarriers());
		availPref.setQuoteOndFlexi(bAvailPref.getQuoteOndFlexi());
		availPref.setOndFlexiSelected(bAvailPref.getOndFlexiSelected());
		availPref.setRestrictionLevel(bAvailPref.getRestrictionLevel());
		availPref.setSearchSystem(bAvailPref.getSearchSystem());
		availPref.setStayOverTimeInMillis(bAvailPref.getStayOverTimeInMillis());
		availPref.setTravelAgentCode(bAvailPref.getTravelAgentCode());
		availPref.setTicketValidTill(bAvailPref.getTicketValidTill());
		availPref.setLastFareQuotedDate(bAvailPref.getLastFareQuotedDate());
		availPref.setSearchAllBC(bAvailPref.isSearchAllBC());
		availPref.setFixedFareAgent(bAvailPref.isFixedFareAgent());
		availPref.setGoshoFareAgent(bAvailPref.isGoshoFareAgent());
		availPref.setExcludedCharges(bAvailPref.getExcludedCharges());
		availPref.setPromoApplicable(bAvailPref.isPromoApplicable());
		availPref.setPromoCode(bAvailPref.getPromoCode());
		availPref.setBankIdentificationNumber(bAvailPref.getBankIdentificationNumber());
		availPref.setExistRetGropMap(bAvailPref.getExistRetGropMap());
		availPref.setPointOfSale(bAvailPref.getPointOfSale());
		availPref.setAllowOverrideSameOrHigherFareMod(bAvailPref.isAllowOverrideSameOrHigherFareMod());
		availPref.setFareCalendarSearch(bAvailPref.isFareCalendarSearch());
		availPref.setMultiCitySearch(bAvailPref.isMultiCitySearch());
		availPref.setPreserveOndOrder(bAvailPref.isPreserveOndOrder());
		availPref.setQuoteFares(bAvailPref.isQuoteFares());
		availPref.setIncludeHRTFares(bAvailPref.isIncludeHRTFares());
		if (bAvailPref.getPreferredLanguage() != null && !bAvailPref.getPreferredLanguage().isEmpty()) {
			availPref.setPreferredLanguage(bAvailPref.getPreferredLanguage());
		}

		travelPref.setBookingClassCode(bTravelPref.getBookingClassCode());
		travelPref.setBookingType(bTravelPref.getBookingType());
		travelPref.setOpenReturn(bTravelPref.isOpenReturn());
		travelPref.setOpenReturnConfirm(bTravelPref.isOpenReturnConfirm());
		travelPref.setValidityId(bTravelPref.getValidityId());
	}

	public OriginDestinationInformationTO addNewOriginDestinationInformation() {
		OriginDestinationInformationTO originDestination = new OriginDestinationInformationTO();
		originDestinationInformationList.add(originDestination);
		return originDestination;
	}

	public OriginDestinationInformationTO getOriginDestinationInformation(String origin, String destination) {
		for (OriginDestinationInformationTO originDestination : originDestinationInformationList) {
			if (originDestination.getOrigin().equals(origin) && originDestination.getDestination().equals(destination)) {
				return originDestination;
			}
		}
		return null;
	}

	public OriginDestinationInformationTO getOriginDestinationInformation(int index) {
		if (index < originDestinationInformationList.size()) {
			return originDestinationInformationList.get(index);
		}
		return null;
	}

	public List<OriginDestinationInformationTO> getOriginDestinationInformationList() {
		if (originDestinationInformationList == null) {
			originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
		}
		return originDestinationInformationList;
	}

	public List<OriginDestinationInformationTO> getOrderedOriginDestinationInformationList() {
		Collections.sort(this.originDestinationInformationList);
		return this.originDestinationInformationList;
	}

	public void setOriginDestinationInformationList(List<OriginDestinationInformationTO> originDestinationInformationList) {
		this.originDestinationInformationList = originDestinationInformationList;
	}

	public TravelerInfoSummaryTO getTravelerInfoSummary() {
		if (travelerInfoSummary == null) {
			travelerInfoSummary = new TravelerInfoSummaryTO();
		}
		return travelerInfoSummary;
	}

	public TravelPreferencesTO getTravelPreferences() {
		if (travelPreferences == null) {
			travelPreferences = new TravelPreferencesTO();
		}
		return travelPreferences;
	}

	public AvailPreferencesTO getAvailPreferences() {
		if (availPreferences == null) {
			availPreferences = new AvailPreferencesTO();
		}
		return availPreferences;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	/**
	 * @return the classOfServicePrefTO
	 */
	@Deprecated
	public ClassOfServicePreferencesTO getClassOfServicePrefTO() {
		if (this.classOfServicePrefTO == null) {
			this.classOfServicePrefTO = new ClassOfServicePreferencesTO();
		}
		return classOfServicePrefTO;
	}

	/**
	 * @param classOfServicePrefTO
	 *            the classOfServicePrefTO to set
	 */
	@Deprecated
	public void setClassOfServicePrefTO(ClassOfServicePreferencesTO classOfServicePrefTO) {
		this.classOfServicePrefTO = classOfServicePrefTO;
	}

	public Map<String, List<Integer>> getLogicalCabinClassSelection() {
		Map<String, List<Integer>> logicalCCSelection = new HashMap<String, List<Integer>>();
		List<OriginDestinationInformationTO> orderedOndList = getOrderedOriginDestinationInformationList();
		int ondSequence = 0;
		for (OriginDestinationInformationTO ondInfoTO : orderedOndList) {
			if (ondInfoTO.getPreferredLogicalCabin() != null && !"".equals(ondInfoTO.getPreferredLogicalCabin())) {
				if (!logicalCCSelection.containsKey(ondInfoTO.getPreferredLogicalCabin())) {
					logicalCCSelection.put(ondInfoTO.getPreferredLogicalCabin(), null);
				}
				if (ondInfoTO.getOrignDestinationOptions() != null) {
					if (logicalCCSelection.get(ondInfoTO.getPreferredLogicalCabin()) == null) {
						logicalCCSelection.put(ondInfoTO.getPreferredLogicalCabin(), new ArrayList<Integer>());
					}

					logicalCCSelection.get(ondInfoTO.getPreferredLogicalCabin()).add(ondSequence);

					// Add same logical cabin class selection as outbound, for inbound ond if open return available
					// search. This use to identify flexi fare selection in fare quote.
					if ((orderedOndList.size() > 0 && orderedOndList.size() <= 2) && travelPreferences.isOpenReturn()) {
						logicalCCSelection.get(ondInfoTO.getPreferredLogicalCabin()).add(OndSequence.IN_BOUND);
					}
				}
			}
			ondSequence++;
		}
		return logicalCCSelection;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	

}
