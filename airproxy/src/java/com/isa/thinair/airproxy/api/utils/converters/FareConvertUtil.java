package com.isa.thinair.airproxy.api.utils.converters;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChangeFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airinventory.api.util.FareRulesUtil;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.FlexiRuleFlexibilityDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareDiscountTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.utils.AgentCommissionMediator;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.assembler.ChargeMerger;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PreferenceAnalyzer;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class FareConvertUtil {
	/**
	 * 
	 * @param applicableTo
	 * @return
	 */
	public static Set<String> populatePaxTypes(int applicableTo) {
		Set<String> paxTypes = new HashSet<String>();

		if (applicableTo == Charge.APPLICABLE_TO_ADULT_ONLY || applicableTo == Charge.APPLICABLE_TO_ADULT_CHILD
				|| applicableTo == Charge.APPLICABLE_TO_ADULT_INFANT || applicableTo == Charge.APPLICABLE_TO_ALL) {
			paxTypes.add(PaxTypeTO.ADULT);
		}

		if (applicableTo == Charge.APPLICABLE_TO_CHILD_ONLY || applicableTo == Charge.APPLICABLE_TO_ADULT_CHILD
				|| applicableTo == Charge.APPLICABLE_TO_ALL) {
			paxTypes.add(PaxTypeTO.CHILD);
		}

		if (applicableTo == Charge.APPLICABLE_TO_INFANT_ONLY || applicableTo == Charge.APPLICABLE_TO_ADULT_INFANT
				|| applicableTo == Charge.APPLICABLE_TO_ALL) {
			paxTypes.add(PaxTypeTO.INFANT);
		}

		return paxTypes;
	}

	/**
	 * 
	 * @param paxFareTO
	 * @param ondFareDTO
	 * @param paxType
	 * @param ondSequence
	 * @param flightSearchGapInDays
	 *            TODO
	 * @param isFixedFare
	 * @throws ModuleException
	 */
	public static void populateCharges(FareTypeTO paxFareTO, OndFareDTO ondFareDTO, String paxType, int ondSequence)
			throws ModuleException {

		// Ond code
		String ondCode = ondFareDTO.getOndCode(false);

		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		// All Charges
		Collection<QuotedChargeDTO> chargesList = ondFareDTO.getAllCharges();

		// All Flexi Charges
		Collection<FlexiRuleDTO> flexiChargesList = ondFareDTO.getAllFlexiCharges();

		HashMap<String, FeeTO> paxFeeMap = new HashMap<String, FeeTO>();
		BigDecimal paxFeeTotal = paxFareTO.getTotalFees();

		HashMap<String, TaxTO> paxTaxMap = new HashMap<String, TaxTO>();
		BigDecimal paxTaxTotal = paxFareTO.getTotalTaxes();

		HashMap<String, SurchargeTO> paxSurchargeMap = new HashMap<String, SurchargeTO>();
		BigDecimal paxSurchargeTotal = paxFareTO.getTotalSurcharges();

		ONDExternalChargeTO ondExternalCharge = paxFareTO.getOndExternalCharge(ondSequence);
		ondExternalCharge.setOndSequence(ondSequence);

		if (chargesList != null) {
			for (QuotedChargeDTO quotedChargeDTO : chargesList) {

				Set<String> paxTypeSet = FareQuoteUtil.populatePaxTypes(quotedChargeDTO.getApplicableTo());

				if (paxType.compareTo(PaxTypeTO.ADULT) == 0 && !paxTypeSet.contains(PaxTypeTO.ADULT)) {
					continue;
				} else if (paxType.compareTo(PaxTypeTO.CHILD) == 0 && !paxTypeSet.contains(PaxTypeTO.CHILD)) {
					continue;
				} else if (paxType.compareTo(PaxTypeTO.INFANT) == 0 && !paxTypeSet.contains(PaxTypeTO.INFANT)) {
					continue;
				}

				BigDecimal paxCharge = getEffectiveCharge(quotedChargeDTO, paxType);
				// create a charge key based on the chargeCode and
				// selectedCurrency code(if charge is defined as a
				// selected currency charge - This enable us to add the selected
				// currency charge values directly
				BigDecimal paxSelCurrCharge = AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getValueInLocalCurrency());
				String chargeKey = quotedChargeDTO.getChargeCode()
						+ (quotedChargeDTO.getLocalCurrencyCode() == null ? "" : quotedChargeDTO.getLocalCurrencyCode());
				if (quotedChargeDTO.getChargeGroupCode().compareTo(ChargeGroups.TAX) == 0) {
					// per pax tax
					TaxTO tax = paxTaxMap.get(chargeKey);
					if (tax == null) {
						tax = createTaxTO(carrierCode, ondCode, ondSequence, new Date(), quotedChargeDTO, paxType);
						paxTaxMap.put(chargeKey, tax);
					}
					tax.setAmount(AccelAeroCalculator.add(tax.getAmount(), paxCharge));
					if (tax.isDefinedInLocal()) {
						tax.setLocalCurrencyAmount(AccelAeroCalculator.add(tax.getLocalCurrencyAmount(), paxSelCurrCharge));
					}

					paxTaxTotal = AccelAeroCalculator.add(paxTaxTotal, paxCharge);

				} else if (quotedChargeDTO.getChargeGroupCode().compareTo(ChargeGroups.SURCHARGE) == 0
						|| quotedChargeDTO.getChargeGroupCode().compareTo(ChargeGroups.INFANT_SURCHARGE) == 0) {
					// per pax surcharges
					SurchargeTO surcharge = paxSurchargeMap.get(chargeKey);
					if (surcharge == null) {
						surcharge = createSurchargeTO(carrierCode, ondCode, ondSequence, new Date(), quotedChargeDTO, paxType);
						paxSurchargeMap.put(chargeKey, surcharge);

					}
					surcharge.setAmount(AccelAeroCalculator.add(surcharge.getAmount(), paxCharge));
					if (surcharge.isDefinedInLocal()) {
						surcharge.setLocalCurrencyAmount(
								AccelAeroCalculator.add(surcharge.getLocalCurrencyAmount(), paxSelCurrCharge));
					}

					paxSurchargeTotal = AccelAeroCalculator.add(paxSurchargeTotal, paxCharge);

				} else {

					// per pax fees
					FeeTO fee = paxFeeMap.get(chargeKey);
					if (fee == null) {
						fee = createFeeTO(carrierCode, ondCode, ondSequence, new Date(), quotedChargeDTO, paxType);
						paxFeeMap.put(chargeKey, fee);
					}
					if (fee.isDefinedInLocal()) {
						fee.setLocalCurrencyAmount(AccelAeroCalculator.add(fee.getLocalCurrencyAmount(), paxSelCurrCharge));
					}
					fee.setAmount(AccelAeroCalculator.add(fee.getAmount(), paxCharge));

					paxSurchargeTotal = AccelAeroCalculator.add(paxSurchargeTotal, paxCharge);

				}

			}
		}

		if (flexiChargesList != null) {
			for (FlexiRuleDTO quotedFlexiChargeDTO : flexiChargesList) {
				ExternalChargeTO externalCharge = createExternalChargeTO(carrierCode, ondCode,
						CalendarUtil.getCurrentSystemTimeInZulu(), quotedFlexiChargeDTO, paxType);
				externalCharge.setOndSequence(ondSequence);
				ondExternalCharge.addExternalCharge(externalCharge);
			}
		}

		paxFareTO.getFees().addAll(paxFeeMap.values());
		paxFareTO.setTotalFees(paxFeeTotal);

		paxFareTO.getTaxes().addAll(paxTaxMap.values());
		paxFareTO.setTotalTaxes(paxTaxTotal);

		paxFareTO.getSurcharges().addAll(paxSurchargeMap.values());
		paxFareTO.setTotalSurcharges(paxSurchargeTotal);

	}

	/**
	 * 
	 * @param carrierCode
	 * @param ondCode
	 * @param ondSequence
	 * @param date
	 * @param quotedChargeDTO
	 * @param paxType
	 * @return
	 */
	public static SurchargeTO createSurchargeTO(String carrierCode, String ondCode, int ondSequence, Date date,
			QuotedChargeDTO quotedChargeDTO, String paxType) {
		SurchargeTO surcharge = new SurchargeTO();
		surcharge.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		surcharge.setCarrierCode(carrierCode);
		surcharge.setSegmentCode(ondCode);
		// Jira 6929
		if (quotedChargeDTO.getReportingChargeGroupCode() != null
				&& AppSysParamsUtil.isGroupChargesByReportingChargeGroupCodeEnabled()) {
			surcharge.setSurchargeCode(quotedChargeDTO.getReportingChargeGroupCode());
			surcharge.setReportingChargeGroupCode(quotedChargeDTO.getReportingChargeGroupCode());
		} else {
			surcharge.setSurchargeCode(quotedChargeDTO.getChargeCode());
		}
		surcharge.setSurchargeName(quotedChargeDTO.getChargeDescription());
		if (quotedChargeDTO.getLocalCurrencyCode() != null) {
			surcharge.setLocalCurrencyCode(quotedChargeDTO.getLocalCurrencyCode());
			surcharge.setLocalCurrencyAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}
		surcharge.setApplicableTime(date);
		surcharge.getApplicablePassengerTypeCode().add(paxType);
		surcharge.setOndSequence(ondSequence);

		return surcharge;
	}

	/**
	 * 
	 * @param carrierCode
	 * @param ondCode
	 * @param date
	 * @param flexiRuleDTO
	 * @param paxType
	 * @return
	 */
	public static ExternalChargeTO createExternalChargeTO(String carrierCode, String ondCode, Date date,
			FlexiRuleDTO flexiRuleDTO, String paxType) {
		Collection<FlexiInfoTO> flexiInfoTOs = new ArrayList<FlexiInfoTO>();
		for (FlexiRuleFlexibilityDTO flexiRuleFlexibilityDTO : flexiRuleDTO.getAvailableFlexibilities()) {
			FlexiInfoTO flexiInfoTO = new FlexiInfoTO();
			flexiInfoTO.setFlexiRateId(flexiRuleDTO.getFlexiRuleRateId());
			flexiInfoTO.setAvailableCount(flexiRuleFlexibilityDTO.getAvailableCount());
			flexiInfoTO.setCutOverBufferInMins(flexiRuleFlexibilityDTO.getCutOverBufferInMins());
			flexiInfoTO.setFlexibilityTypeId(flexiRuleFlexibilityDTO.getFlexibilityTypeId());
			flexiInfoTOs.add(flexiInfoTO);
		}
		ExternalChargeTO externalCharge = new ExternalChargeTO();
		externalCharge.setAmount(AccelAeroCalculator.parseBigDecimal(flexiRuleDTO.getEffectiveChargeAmount(paxType)));
		externalCharge.setCarrierCode(carrierCode);
		externalCharge.setSegmentCode(ondCode);
		externalCharge.setSurchargeCode(flexiRuleDTO.getChargeCode());
		externalCharge.setSurchargeName(flexiRuleDTO.getDescription());
		externalCharge.setApplicableTime(date);
		externalCharge.getApplicablePassengerTypeCode().add(paxType);
		externalCharge.setAdditionalDetails(flexiInfoTOs);
		externalCharge.setComments(flexiRuleDTO.getRuleComments());
		externalCharge.setType(EXTERNAL_CHARGES.FLEXI_CHARGES);
		return externalCharge;
	}

	/**
	 * 
	 * @param carrierCode
	 * @param ondCode
	 * @param ondSequence
	 * @param date
	 * @param quotedChargeDTO
	 * @param paxType
	 * @return
	 */
	public static FeeTO createFeeTO(String carrierCode, String ondCode, int ondSequence, Date date,
			QuotedChargeDTO quotedChargeDTO, String paxType) {
		FeeTO fee = new FeeTO();
		fee.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		fee.setCarrierCode(carrierCode);
		fee.setSegmentCode(ondCode);
		// Jira 6929
		if (quotedChargeDTO.getReportingChargeGroupCode() != null
				&& AppSysParamsUtil.isGroupChargesByReportingChargeGroupCodeEnabled()) {
			fee.setFeeCode(quotedChargeDTO.getReportingChargeGroupCode());
			fee.setReportingChargeGroupCode(quotedChargeDTO.getReportingChargeGroupCode());
		} else {
			fee.setFeeCode(quotedChargeDTO.getChargeCode());
		}
		fee.setFeeName(quotedChargeDTO.getChargeDescription());
		if (quotedChargeDTO.getLocalCurrencyCode() != null) {
			fee.setLocalCurrencyCode(quotedChargeDTO.getLocalCurrencyCode());
			fee.setLocalCurrencyAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}
		fee.setApplicableTime(date);
		fee.getApplicablePassengerTypeCode().add(paxType);
		fee.setOndSequence(ondSequence);

		return fee;
	}

	/**
	 * 
	 * @param carrierCode
	 * @param ondCode
	 * @param ondSequence
	 * @param date
	 * @param quotedChargeDTO
	 * @param paxType
	 * @return
	 */
	public static TaxTO createTaxTO(String carrierCode, String ondCode, int ondSequence, Date date,
			QuotedChargeDTO quotedChargeDTO, String paxType) {
		TaxTO tax = new TaxTO();
		tax.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		tax.setCarrierCode(carrierCode);
		tax.setSegmentCode(ondCode);
		// Jira 6929
		if (quotedChargeDTO.getReportingChargeGroupCode() != null
				&& AppSysParamsUtil.isGroupChargesByReportingChargeGroupCodeEnabled()) {
			tax.setTaxCode(quotedChargeDTO.getReportingChargeGroupCode());
			tax.setReportingChargeGroupCode(quotedChargeDTO.getReportingChargeGroupCode());
		} else {
			tax.setTaxCode(quotedChargeDTO.getChargeCode());
		}
		tax.setTaxName(quotedChargeDTO.getChargeDescription());
		if (quotedChargeDTO.getLocalCurrencyCode() != null) {
			tax.setLocalCurrencyCode(quotedChargeDTO.getLocalCurrencyCode());
			tax.setLocalCurrencyAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}
		tax.setApplicableTime(date);
		tax.getApplicablePassengerTypeCode().add(paxType);
		tax.setOndSequence(ondSequence);

		return tax;
	}

	/**
	 * 
	 * @param journeyOndFareDTOList
	 * @param paxType
	 * @param isFixedFare
	 * @return
	 * @throws ModuleException
	 */
	public static PerPaxPriceInfoTO createPerPaxPriceInfo(List<Collection<OndFareDTO>> journeyOndFareDTOList, String paxType,
			BaseAvailRQ baseAvailRQ) throws ModuleException {

		PerPaxPriceInfoTO paxPriceInfo = new PerPaxPriceInfoTO();
		FareTypeTO fareTypeTO = new FareTypeTO();
		fareTypeTO.setBaseFares(new ArrayList<BaseFareTO>());
		fareTypeTO.setFees(new ArrayList<FeeTO>());
		fareTypeTO.setTaxes(new ArrayList<TaxTO>());
		fareTypeTO.setSurcharges(new ArrayList<SurchargeTO>());

		fareTypeTO.setTotalBaseFare(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareTypeTO.setTotalFees(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareTypeTO.setTotalPrice(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareTypeTO.setTotalSurcharges(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareTypeTO.setTotalTaxes(AccelAeroCalculator.getDefaultBigDecimalZero());

		paxPriceInfo.setPassengerPrice(fareTypeTO);

		BigDecimal totalPaxFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal paxAgentCommission = AccelAeroCalculator.getDefaultBigDecimalZero(); // for
																						// whole
																						// reservation

		AgentCommissionDetails agentCommissions = new AgentCommissionDetails();
		PreferenceAnalyzer preferenceAnalyzer = new PreferenceAnalyzer(baseAvailRQ);
		AgentCommissionMediator agentCommissionMediator = new AgentCommissionMediator(preferenceAnalyzer);

		List<List<String>> journeyOndSummary = new ArrayList<List<String>>();
		boolean isAllUnTouchedOnDs = isAllUnTouchedOnDs(journeyOndFareDTOList);
		for (Collection<OndFareDTO> ondFareDTOs : journeyOndFareDTOList) {
			List<String> ondsInJoureney = getJourneyOndCodes(ondFareDTOs);
			journeyOndSummary.add(ondsInJoureney);
		}

		int ondSequence = 0;

		for (Collection<OndFareDTO> ondFareDTOs : journeyOndFareDTOList) {
			// adding this is mandatory otherwise ond sequence will be wrong for
			// flexi
			if (fareTypeTO.hasOndExternalCharge(ondSequence)) {
				fareTypeTO.addOndExternalCharge(ondSequence);
			}

			for (OndFareDTO ondFareDTO : ondFareDTOs) {

				populateCharges(paxPriceInfo.getPassengerPrice(), ondFareDTO, paxType, ondFareDTO.getOndSequence());

				FareSummaryDTO fareSummary = ondFareDTO.getFareSummaryDTO();
				if (fareSummary.getFareAmount(paxType) != null) {

					List<String> journeyOnds = getJourneyOndCodes(ondFareDTOs);
					if (agentCommissionMediator.applyAgentCommissionForOnd(ondFareDTO.getOndCode(), journeyOnds,
							journeyOndSummary)) {
						BigDecimal perOndPaxAgentCommission = ondFareDTO.getPaxOndAgentCommission(paxType);
						if (perOndPaxAgentCommission.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
							agentCommissions.addCommissionApplicableFareIds(ondFareDTO.getFareId());
							paxAgentCommission = paxAgentCommission.add(perOndPaxAgentCommission);
						}
					}

					BigDecimal ondFare = AccelAeroCalculator.parseBigDecimal(fareSummary.getFareAmount(paxType));
					BaseFareTO baseFare = new BaseFareTO();
					baseFare.setAmount(ondFare);
					baseFare.setSegmentCode(ondFareDTO.getOndCode());
					baseFare.getCarrierCode().add(AppSysParamsUtil.getDefaultCarrierCode());
					baseFare.setApplicableTime(new Date());
					baseFare.getApplicablePassengerTypeCode().add(paxType);
					baseFare.setOndSequence(ondFareDTO.getOndSequence());

					if (ondFareDTO.getSegmentsMap() != null) {
						List<String> rphList = new ArrayList<String>();
						for (Entry<Integer, FlightSegmentDTO> entry : ondFareDTO.getSegmentsMap().entrySet()) {
							FlightSegmentDTO flightSegmentDTO = entry.getValue();
							rphList.add(FlightRefNumberUtil.composeFlightRPH(flightSegmentDTO));
						}
						baseFare.setSegmentRPHList(rphList);
					}
					if (ondFareDTO.getSegmentsMap() != null
							&& !FareQuoteUtil.isBusSegmentExists(ondFareDTO.getSegmentsMap().values())) {
						if (ondFareDTO.getAllFlexiCharges() == null || ondFareDTO.getAllFlexiCharges().size() == 0) {

						}
					}

					paxPriceInfo.getPassengerPrice().getBaseFares().add(baseFare);

					totalPaxFare = AccelAeroCalculator.add(totalPaxFare, ondFare);

				}
			}
			ondSequence++;
		}

		Integer excluedFareIdsFromCommissionRemoval = agentCommissionMediator.getExcludedFareIdsFromCommissionRemoval();
		if (excluedFareIdsFromCommissionRemoval != null) {
			agentCommissions.addCommissionRemovalExcludedFareIds(excluedFareIdsFromCommissionRemoval);
		}

		FareTypeTO perPaxFareType = paxPriceInfo.getPassengerPrice();
		perPaxFareType.setTotalBaseFare(totalPaxFare);
		perPaxFareType.setApplicableAgentCommissions(agentCommissions);
		perPaxFareType.setPerPaxTotalAgentCommission(paxAgentCommission);

		perPaxFareType.setTotalPrice(AccelAeroCalculator.add(perPaxFareType.getTotalBaseFare(),
				perPaxFareType.getTotalSurcharges(), perPaxFareType.getTotalTaxes(), perPaxFareType.getTotalFees()));
		perPaxFareType.setTotalPriceWithInfant(perPaxFareType.getTotalPrice()); // default
																				// to
																				// the
																				// total
																				// price

		paxPriceInfo.setPassengerType(paxType);
		return paxPriceInfo;
	}

	/**
	 * 
	 * @param quotedChargeDTO
	 * @param paxType
	 * @return
	 */
	public static BigDecimal getEffectiveCharge(QuotedChargeDTO quotedChargeDTO, String paxType) {
		BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Zaki : This is to fix an issue caused by retrieving certain charges
		// which are saved
		// as percentages within the database. These charges once multiplied
		// give double values
		// with improper precisions. One example is TAX code [GT] which once
		// retrieved gives
		// [12.4500000000001] rather than [12.45].
		if (quotedChargeDTO.getEffectiveChargeAmount(paxType) != null) {
			if (AppSysParamsUtil.isRoundupChargesWhenCalculatedInBaseCurrency()) {
				chargeAmount = new BigDecimal(quotedChargeDTO.getEffectiveChargeAmount(paxType)).setScale(0, RoundingMode.UP);
			} else {
				chargeAmount = AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(paxType));
			}
		}

		return chargeAmount;
	}

	/**
	 * 
	 * @param perPaxPriceInfo
	 * @return
	 */
	public static PerPaxPriceInfoTO clonePerPaxPriceInfo(PerPaxPriceInfoTO perPaxPriceInfo) {
		PerPaxPriceInfoTO clone = new PerPaxPriceInfoTO();
		clone.setPassengerPrice(perPaxPriceInfo.getPassengerPrice());
		clone.setPassengerType(perPaxPriceInfo.getPassengerType());
		clone.setTravelerRefNumber(perPaxPriceInfo.getTravelerRefNumber());
		return clone;
	}

	/**
	 * 
	 * @param selectedFlightDTO
	 * @param paxAssm
	 * @param appIndicator
	 * @param isFareQuote
	 * @param logicalCabinClassSelection
	 * @param getSegPriceInfo
	 *            boolean denoting whether to get the Segment fare price infomration
	 * @return
	 * @throws ModuleException
	 */

	public static PriceInfoTO getSelectedPriceFltInfo(SelectedFlightDTO selectedFlightDTO, IPaxCountAssembler paxAssm,
			String agentCode, String appIndicator, boolean isFareQuote, Map<String, List<Integer>> logicalCabinClassSelection,
			boolean getSegPriceInfo, BaseAvailRQ baseAvailRQ) throws ModuleException {
		if (selectedFlightDTO == null || selectedFlightDTO.getSelectedOndFlights().size() == 0) {
			return null;
		}

		PriceInfoTO priceInfoTO = new PriceInfoTO();
		Collection<OndFareDTO> ondFareDTOs = new ArrayList<OndFareDTO>();
		List<Collection<OndFareDTO>> jouneyONDWiseFareONDs = new ArrayList<Collection<OndFareDTO>>();
		int ondSequence = 0;
		Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
		Map<Integer, Integer> ondWiseFareTypes = new HashMap<Integer, Integer>();
		Map<Integer, Boolean> ondFlexiSelection = new HashMap<Integer, Boolean>();
		Map<Integer, Integer> ondWiseFareIds = new HashMap<Integer, Integer>();
		List<BundledFareDTO> selectedOndBundledFares = new ArrayList<BundledFareDTO>();
		String selectedLanguage = baseAvailRQ.getAvailPreferences().getPreferredLanguage();

		// If segPrice is to be taken then get the segmentOndFlights
		List<AvailableIBOBFlightSegment> selectedOndFlights = getSegPriceInfo
				? selectedFlightDTO.getOndWiseMinimumSegFareFlights()
				: selectedFlightDTO.getSelectedOndFlights();

		if (selectedOndFlights == null || selectedOndFlights.isEmpty()) {
			return null;
		}

		Collections.sort(selectedOndFlights);

		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : selectedOndFlights) {

			String selectedLogicalCabinClass = availableIBOBFlightSegment.getSelectedLogicalCabinClass();
			boolean isOndFlexiSelected = ((baseAvailRQ.getAvailPreferences().getOndFlexiSelected()
					.get(availableIBOBFlightSegment.getOndSequence()) != null)
					&& availableIBOBFlightSegment.getSelectedBundledFare() == null)
							? baseAvailRQ.getAvailPreferences().getOndFlexiSelected()
									.get(availableIBOBFlightSegment.getOndSequence())
							: false;
			if (selectedLogicalCabinClass == null) {
				return null;
			}

			if (ondFlexiSelection.get(ondSequence) == null) {
				ondFlexiSelection.put(ondSequence, true);
			}

			List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses = new ArrayList<LogicalCabinClassInfoTO>();
			boolean isLogicalCCWithFlexiSelected = false;
			for (String availableLogicalCabinClass : availableIBOBFlightSegment.getAvailableLogicalCabinClasses()) {
				LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(availableLogicalCabinClass);

				Collection<OndFareDTO> colOndFareDTOForLogicalCC = availableIBOBFlightSegment
						.getFareChargesSeatsSegmentsDTOs(availableLogicalCabinClass);

				if (colOndFareDTOForLogicalCC == null) {
					continue;
				}

				/*
				 * With new flexi implementation, flexi will be offered as a new logical cabin class in the front-end.
				 * But, in the back-end same logical cabin class will be used with flexi flag. So, before sending
				 * available logical CC & fares to the front-end, dummy logical cabin class will populated based on the
				 * requirement.
				 * 
				 * Here, if the flexi is defined as a FOC service for a particular logical cc, only the 'logical cc with
				 * flexi' option will be added. Otherwise separate option will be added 'logical cc with flexi'
				 */

				boolean freeFlexiEnabled = logicalCabinClassDTO.isFreeFlexiEnabled();

				if (freeFlexiEnabled && FareQuoteUtil.isFlexiAvailable(colOndFareDTOForLogicalCC)) {
					LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
					isLogicalCCWithFlexiSelected |= createDummyLogicalCabinClassForFlexi(logicalCabinClassInfoTO,
							availableIBOBFlightSegment, logicalCabinClassDTO, selectedLogicalCabinClass,
							logicalCabinClassSelection, freeFlexiEnabled, isFareQuote, isOndFlexiSelected, selectedLanguage);
					segmentLogicalCabinClasses.add(logicalCabinClassInfoTO);
				} else {
					boolean hasFlexi = FareQuoteUtil.isFlexiAvailable(colOndFareDTOForLogicalCC);
					if (hasFlexi && AppSysParamsUtil.isDisplayFlexiAsSeparteBundle()) {
						LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
						isLogicalCCWithFlexiSelected |= createDummyLogicalCabinClassForFlexi(logicalCabinClassInfoTO,
								availableIBOBFlightSegment, logicalCabinClassDTO, selectedLogicalCabinClass,
								logicalCabinClassSelection, freeFlexiEnabled, isFareQuote, isOndFlexiSelected, selectedLanguage);

						logicalCabinClassInfoTO.setFlexiRuleID(FareQuoteUtil.getAvailableFlexiID(colOndFareDTOForLogicalCC));
						segmentLogicalCabinClasses.add(logicalCabinClassInfoTO);
					}

					LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
					logicalCabinClassInfoTO.setLogicalCCCode(availableLogicalCabinClass);
					logicalCabinClassInfoTO.setLogicalCCDesc(
							(logicalCabinClassDTO != null) ? logicalCabinClassDTO.getDescription() : availableLogicalCabinClass);

					if (!isLogicalCCWithFlexiSelected && selectedLogicalCabinClass.equals(availableLogicalCabinClass)
							&& availableIBOBFlightSegment.getSelectedBundledFare() == null) {
						logicalCabinClassInfoTO.setSelected(true);
					}

					logicalCabinClassInfoTO.setComment(logicalCabinClassDTO.getComment());
					logicalCabinClassInfoTO.setRank(logicalCabinClassDTO.getNestRank());
					logicalCabinClassInfoTO.setImageUrl(OndConvertUtil
							.generateLogicalCCImageUrl(logicalCabinClassDTO.getLogicalCCCode(), false, selectedLanguage));
					logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(logicalCabinClassDTO.getBundleFareDescriptionTemplateDTO());
					logicalCabinClassInfoTO.setFlexiAvailable(hasFlexi);

					segmentLogicalCabinClasses.add(logicalCabinClassInfoTO);
				}
			}

			addBundledFaresToLogicalCabinList(segmentLogicalCabinClasses, availableIBOBFlightSegment);

			if (availableIBOBFlightSegment.isFlown() && availableIBOBFlightSegment.getSelectedBundledFare() != null) {
				retainOnlyBundledServiceOption(segmentLogicalCabinClasses,
						availableIBOBFlightSegment.getSelectedBundledFare().getBundledFarePeriodId());
			}

			Collection<OndFareDTO> ondFares = availableIBOBFlightSegment
					.getFareChargesSeatsSegmentsDTOs(selectedLogicalCabinClass);

			setOndSelectedBundledFarePeriodId(availableIBOBFlightSegment.getSelectedBundledFare(), ondFareDTOs);
	
			if(availableIBOBFlightSegment.isSplitOperationForBusInvoked()){
				updateFareDTOsForSplitOperation(ondFares);
			}
			
			selectedOndBundledFares.add(availableIBOBFlightSegment.getSelectedBundledFare());
			ondFareDTOs.addAll(ondFares);
			if (jouneyONDWiseFareONDs.size() == ondSequence) {
				jouneyONDWiseFareONDs.add(new ArrayList<OndFareDTO>());
			}
			jouneyONDWiseFareONDs.get(ondSequence).addAll(ondFares);

			Collections.sort(segmentLogicalCabinClasses);
			priceInfoTO.addAvailableLogicalCC(availableIBOBFlightSegment.getOndCode(), ondSequence, segmentLogicalCabinClasses,
					availableIBOBFlightSegment.getRequestedOndSequence());

			ondFlexiSelection.put(ondSequence, ondFlexiSelection.get(ondSequence) && isLogicalCCWithFlexiSelected);
			ondWiseFareTypes.put(ondSequence, availableIBOBFlightSegment.getFareType(selectedLogicalCabinClass));

			if (availableIBOBFlightSegment.getFare(selectedLogicalCabinClass) != null) {
				ondWiseFareIds.put(ondSequence, availableIBOBFlightSegment.getFare(selectedLogicalCabinClass).getFareId());
			}

			ondSequence++;
		}
		priceInfoTO.setFareTypeTO(getFareTypeTO(jouneyONDWiseFareONDs, paxAssm, agentCode, appIndicator, baseAvailRQ));
		priceInfoTO.getFareTypeTO().setOndWiseFareType(ondWiseFareTypes);
		priceInfoTO.getFareTypeTO().setOndFlexiSelection(ondFlexiSelection);
		priceInfoTO.getFareTypeTO().setPromotionTO(selectedFlightDTO.getPromotionDetails());
		priceInfoTO.getFareTypeTO().setOndWiseFareId(ondWiseFareIds);
		priceInfoTO.getFareTypeTO().setOndBundledFareDTOs(selectedOndBundledFares);
		priceInfoTO.setLastFareQuotedDate(selectedFlightDTO.getLastFareQuotedDate());
		priceInfoTO.setFQWithinValidity(selectedFlightDTO.isFQWithinValidity());
		priceInfoTO.setFareSegChargeTO(getFareSegChargeTO(selectedFlightDTO.getFareType(), ondFareDTOs, paxAssm, null));
		priceInfoTO.getFareSegChargeTO().setOndBundledFareDTOs(selectedOndBundledFares);

		return priceInfoTO;
	}

	private static void retainOnlyBundledServiceOption(List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses,
			Integer selectedBundledFarePeriodId) {
		if (selectedBundledFarePeriodId != null) {
			Collection<LogicalCabinClassInfoTO> retainElements = new ArrayList<LogicalCabinClassInfoTO>();
			for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : segmentLogicalCabinClasses) {
				if (selectedBundledFarePeriodId.equals(logicalCabinClassInfoTO.getBundledFarePeriodId())) {
					retainElements.add(logicalCabinClassInfoTO);
				}
			}

			if (!retainElements.isEmpty()) {
				segmentLogicalCabinClasses.retainAll(retainElements);
				if (!segmentLogicalCabinClasses.isEmpty()) {
					segmentLogicalCabinClasses.iterator().next().setSelected(true);
				}
			}
		}
	}

	private static void setOndSelectedBundledFarePeriodId(BundledFareDTO bundledFareDTO, Collection<OndFareDTO> ondFares) {
		if (bundledFareDTO != null) {
			for (OndFareDTO ondFareDTO : ondFares) {
				if (!InventoryAPIUtil.isBusSegmentExists(ondFareDTO.getSegmentsMap().values())) {
					ondFareDTO.setSelectedBundledFarePeriodId(bundledFareDTO.getBundledFarePeriodId());
				}
			}
		}
	}

	private static void addBundledFaresToLogicalCabinList(List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses,
			AvailableIBOBFlightSegment availableIBOBFlightSegment) {

		Map<String, List<BundledFareLiteDTO>> availableBundledFares = availableIBOBFlightSegment.getAvailableBundledFares();

		Collections.sort(segmentLogicalCabinClasses);

		int lastRank = 0;
		LogicalCabinClassInfoTO selectedLogicalCabinClass = getSelectedLogicalCabinClass(segmentLogicalCabinClasses);

		if (segmentLogicalCabinClasses != null && !segmentLogicalCabinClasses.isEmpty()) {
			LogicalCabinClassInfoTO lastElement = segmentLogicalCabinClasses.get(segmentLogicalCabinClasses.size() - 1);
			lastRank = lastElement.getRank();
		}

		if (availableBundledFares != null
				&& availableBundledFares.containsKey(availableIBOBFlightSegment.getSelectedLogicalCabinClass())
				&& !availableIBOBFlightSegment.isFlown()) {

			List<BundledFareLiteDTO> bundledFareLiteDTOs = availableBundledFares
					.get(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
			if (bundledFareLiteDTOs != null && !bundledFareLiteDTOs.isEmpty()) {

				BundledFareDTO bundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();

				for (BundledFareLiteDTO bundledFareLiteDTO : bundledFareLiteDTOs) {
					LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
					logicalCabinClassInfoTO.setBundledFarePeriodId(bundledFareLiteDTO.getBundleFarePeriodId());
					logicalCabinClassInfoTO.setComment(bundledFareLiteDTO.getDescription());
					logicalCabinClassInfoTO.setLogicalCCCode(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
					logicalCabinClassInfoTO.setLogicalCCDesc(bundledFareLiteDTO.getBundledFareName());
					logicalCabinClassInfoTO.setRank(++lastRank);
					logicalCabinClassInfoTO.setWithFlexi(bundledFareLiteDTO.isFlexiIncluded() && FareQuoteUtil.isFlexiAvailable(
							availableIBOBFlightSegment, availableIBOBFlightSegment.getSelectedLogicalCabinClass()));
					logicalCabinClassInfoTO.setBookingClasses(bundledFareLiteDTO.getBookingClasses());
					logicalCabinClassInfoTO.setSegmentBookingClasses(bundledFareLiteDTO.getSegmentBookingClass());
					logicalCabinClassInfoTO.setBundledFareFreeServiceName(bundledFareLiteDTO.getFreeServices());
					logicalCabinClassInfoTO.setBundledFareFee(bundledFareLiteDTO.getPerPaxBundledFee());
					logicalCabinClassInfoTO.setImageUrl(bundledFareLiteDTO.getImageUrl());
					logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(bundledFareLiteDTO.getBundleFareDescriptionTemplateDTO());

					if (bundledFareDTO != null
							&& bundledFareDTO.getBundledFarePeriodId().equals(bundledFareLiteDTO.getBundleFarePeriodId())) {
						logicalCabinClassInfoTO.setSelected(true);
						selectedLogicalCabinClass = logicalCabinClassInfoTO;
					}

					segmentLogicalCabinClasses.add(logicalCabinClassInfoTO);
				}
			}
		} else if (availableIBOBFlightSegment.getSelectedBundledFare() != null
				&& (availableIBOBFlightSegment.isFlown() || availableIBOBFlightSegment.isUnTouchedOnd())) {
			BundledFareDTO selectedBundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();
			LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
			logicalCabinClassInfoTO.setBundledFarePeriodId(selectedBundledFareDTO.getBundledFarePeriodId());
			logicalCabinClassInfoTO.setComment(selectedBundledFareDTO.getBundledFareName());
			logicalCabinClassInfoTO.setLogicalCCCode(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
			logicalCabinClassInfoTO.setLogicalCCDesc(selectedBundledFareDTO.getBundledFareName());
			logicalCabinClassInfoTO.setRank(++lastRank);
			logicalCabinClassInfoTO
					.setWithFlexi(selectedBundledFareDTO.isServiceIncluded(EXTERNAL_CHARGES.FLEXI_CHARGES.toString())
							&& FareQuoteUtil.isFlexiAvailable(availableIBOBFlightSegment,
									availableIBOBFlightSegment.getSelectedLogicalCabinClass()));
			logicalCabinClassInfoTO.setBookingClasses(selectedBundledFareDTO.getBookingClasses());
			logicalCabinClassInfoTO.setBundledFareFreeServiceName(selectedBundledFareDTO.getApplicableServiceNames());
			logicalCabinClassInfoTO.setBundledFareFee(selectedBundledFareDTO.getPerPaxBundledFee());
			logicalCabinClassInfoTO.setImageUrl(selectedBundledFareDTO.getImageUrl());
			logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(selectedBundledFareDTO.getBundleFareDescriptionTemplateDTO());

			logicalCabinClassInfoTO.setSelected(true);
			selectedLogicalCabinClass = logicalCabinClassInfoTO;

			segmentLogicalCabinClasses.add(logicalCabinClassInfoTO);
		}

		if (selectedLogicalCabinClass != null) {
			populateBundleFareDelta(selectedLogicalCabinClass, segmentLogicalCabinClasses);
		}
	}

	private static void populateBundleFareDelta(LogicalCabinClassInfoTO selectedLogicalCabinClass,
			List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses) {

		for (LogicalCabinClassInfoTO logicalCabinClass : segmentLogicalCabinClasses) {
			if (!logicalCabinClass.isSelected()) {
				BigDecimal bundleFareDelta = AccelAeroCalculator.subtract(logicalCabinClass.getBundledFareFee(),
						selectedLogicalCabinClass.getBundledFareFee());
				logicalCabinClass.setBundledFareDelta(bundleFareDelta);
			}
		}
	}

	private static LogicalCabinClassInfoTO
			getSelectedLogicalCabinClass(List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses) {

		LogicalCabinClassInfoTO selectedLogicalCabinClass = null;
		for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : segmentLogicalCabinClasses) {
			if (logicalCabinClassInfoTO.isSelected()) {
				selectedLogicalCabinClass = logicalCabinClassInfoTO;
				break;
			}
		}
		return selectedLogicalCabinClass;
	}

	private static boolean createDummyLogicalCabinClassForFlexi(LogicalCabinClassInfoTO logicalCabinClassInfoTO,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, LogicalCabinClassDTO logicalCabinClassDTO,
			String selectedLogicalCabinClass, Map<String, List<Integer>> logicalCabinClassSelection, boolean freeFlexiEnabled,
			boolean isFareQuote, boolean isFlexiSelected, String preferredLanguage) {

		boolean isLogicalCCWithFlexiSelected = false;
		logicalCabinClassInfoTO.setLogicalCCCode(logicalCabinClassDTO.getLogicalCCCode());

		if (freeFlexiEnabled) {
			logicalCabinClassInfoTO.setLogicalCCDesc(logicalCabinClassDTO.getDescription());
		} else {
			logicalCabinClassInfoTO.setLogicalCCDesc(logicalCabinClassDTO.getFlexiDescription());
		}

		if (isFlexiSelected || freeFlexiEnabled) {
			isLogicalCCWithFlexiSelected = true;
		}

		logicalCabinClassInfoTO.setSelected(isLogicalCCWithFlexiSelected);
		logicalCabinClassInfoTO.setComment(logicalCabinClassDTO.getFlexiComment());
		logicalCabinClassInfoTO.setWithFlexi(true);
		logicalCabinClassInfoTO.setRank(logicalCabinClassDTO.getNestRank());
		logicalCabinClassInfoTO.setImageUrl(
				OndConvertUtil.generateLogicalCCImageUrl(logicalCabinClassDTO.getLogicalCCCode(), true, preferredLanguage));
		logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(logicalCabinClassDTO.getBundleFareDescriptionTemplateDTO());

		return isLogicalCCWithFlexiSelected;
	}

	/**
	 * 
	 * @param journeyOndFareDTOList
	 * @param paxAssm
	 * @param appIndicator
	 * @return
	 * @throws ModuleException
	 */
	public static FareTypeTO getFareTypeTO(List<Collection<OndFareDTO>> journeyOndFareDTOList, IPaxCountAssembler paxAssm,
			String agentCode, String appIndicator, BaseAvailRQ baseAvailRQ) throws ModuleException {
		List<BaseFareTO> baseFares = new ArrayList<BaseFareTO>();
		List<FeeTO> fees = new ArrayList<FeeTO>();
		List<SurchargeTO> surcharges = new ArrayList<SurchargeTO>();
		List<TaxTO> taxes = new ArrayList<TaxTO>();
		// List<ExternalChargeTO> outBoundExternalCharges = new
		// ArrayList<ExternalChargeTO>();
		// List<ExternalChargeTO> inBoundExternalCharges = new
		// ArrayList<ExternalChargeTO>();

		FareTypeTO fareType = new FareTypeTO();
		fareType.setBaseFares(baseFares);
		fareType.setFees(fees);
		fareType.setSurcharges(surcharges);
		fareType.setTaxes(taxes);
		// fareType.setOutBoundExternalCharges(outBoundExternalCharges);
		// fareType.setInBoundExternalCharges(inBoundExternalCharges);
		fareType.setTotalBaseFare(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareType.setTotalFees(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareType.setTotalSurcharges(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareType.setTotalTaxes(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareType.setTotalPrice(AccelAeroCalculator.getDefaultBigDecimalZero());
		fareType.setTotalPriceWithInfant(AccelAeroCalculator.getDefaultBigDecimalZero());
		// fareType.setTotalOutBoundExternalCharges(AccelAeroCalculator.getDefaultBigDecimalZero());
		// fareType.setTotalInBoundExternalCharges(AccelAeroCalculator.getDefaultBigDecimalZero());

		List<PerPaxPriceInfoTO> perPaxPriceInfo = new ArrayList<PerPaxPriceInfoTO>();
		BigDecimal infantTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		int refIncrementor = 0;

		ChargeMerger chargeMerger = new ChargeMerger();
		if (paxAssm.getInfantCount() > 0) {
			PerPaxPriceInfoTO pax = createPerPaxPriceInfo(journeyOndFareDTOList, PaxTypeTO.INFANT, baseAvailRQ);
			pax.setTravelerRefNumber(new Integer(refIncrementor++).toString());
			infantTotal = pax.getPassengerPrice().getTotalPrice();
			chargeMerger.addPax(pax, paxAssm.getInfantCount());
			perPaxPriceInfo.add(pax);
			for (int i = 0; i < paxAssm.getInfantCount() - 1; i++) {
				perPaxPriceInfo.add(clonePerPaxPriceInfo(pax));
			}
		}
		if (paxAssm.getChildCount() > 0) {
			PerPaxPriceInfoTO pax = createPerPaxPriceInfo(journeyOndFareDTOList, PaxTypeTO.CHILD, baseAvailRQ);
			pax.setTravelerRefNumber(new Integer(refIncrementor++).toString());
			perPaxPriceInfo.add(pax);
			chargeMerger.addPax(pax, paxAssm.getChildCount());
			for (int i = 0; i < paxAssm.getChildCount() - 1; i++) {
				perPaxPriceInfo.add(clonePerPaxPriceInfo(pax));
			}
		}
		if (paxAssm.getAdultCount() > 0) {
			PerPaxPriceInfoTO pax = createPerPaxPriceInfo(journeyOndFareDTOList, PaxTypeTO.ADULT, baseAvailRQ);
			pax.setTravelerRefNumber(new Integer(refIncrementor++).toString());
			pax.getPassengerPrice()
					.setTotalPriceWithInfant(AccelAeroCalculator.add(pax.getPassengerPrice().getTotalPrice(), infantTotal));
			chargeMerger.addPax(pax, paxAssm.getAdultCount());
			perPaxPriceInfo.add(pax);
			for (int i = 0; i < paxAssm.getAdultCount() - 1; i++) {
				perPaxPriceInfo.add(clonePerPaxPriceInfo(pax));
			}
		}

		fees.addAll(chargeMerger.getFees());
		surcharges.addAll(chargeMerger.getSurcharges());
		taxes.addAll(chargeMerger.getTaxes());
		baseFares.addAll(chargeMerger.getBaseFares());
		// outBoundExternalCharges.addAll(chargeMerger.getOutBoundExternalCharges());
		// inBoundExternalCharges.addAll(chargeMerger.getInBoundExternalCharges());

		fareType.setTotalBaseFare(chargeMerger.getTotalBaseFare());
		fareType.setTotalFees(chargeMerger.getTotalFee());
		fareType.setTotalSurcharges(chargeMerger.getTotalSurcharge());
		fareType.setTotalTaxes(chargeMerger.getTotalTax());
		fareType.setTotalPrice(chargeMerger.getTotalPrice());
		fareType.setTotalPriceWithInfant(chargeMerger.getTotalPriceWithInfant());
		// fareType.setTotalOutBoundExternalCharges(chargeMerger.getTotalOutBoundExternalCharge());
		// fareType.setTotalInBoundExternalCharges(chargeMerger.getTotalInBoundExternalCharge());
		fareType.setOndExternalCharges(chargeMerger.getONDExternalCharges());
		fareType.setTotAgentCommission(chargeMerger.getTotalAgentCommission());
		fareType.setApplicableAgentCommissions(chargeMerger.getApplicableAgentCommissions());
		fareType.setPerPaxPriceInfo(perPaxPriceInfo);

		Collection<OndFareDTO> ondFareDTOList = new ArrayList<OndFareDTO>();
		for (Collection<OndFareDTO> ondFareColl : journeyOndFareDTOList) {
			ondFareDTOList.addAll(ondFareColl);
		}

		// set on hold information
		boolean isOnHoldRestricted = isOnholdRestricted(ondFareDTOList);

		Date holdReleaseTimestamp = null;

		Collection<FlightSegmentDTO> flightDepInfo = getFlightDepartureInfo(ondFareDTOList);
		if (!isOnHoldRestricted) {
			// TODO Remove agentCode from getReleaseTimeStamp
			holdReleaseTimestamp = getReleaseTimeStamp(flightDepInfo, agentCode, appIndicator,
					getOnHoldReleaseTimeDTO(flightDepInfo, ondFareDTOList, appIndicator, agentCode));
			if (holdReleaseTimestamp != null) {
				fareType.setOnHoldReleaseTimeStamp(holdReleaseTimestamp);
			} else if (!SalesChannelsUtil.isAppIndicatorWebOrMobile(appIndicator)) {
				isOnHoldRestricted = true;
			}
		}
		fareType.setOnHoldRestricted(isOnHoldRestricted);

		Object[] arrObj = getRestrictedBufferTime(flightDepInfo);
		if (arrObj != null) {
			Date ohdBufferStart = (Date) arrObj[0];
			Date OhdBufferStop = (Date) arrObj[1];
			fareType.setOnHoldStartTime(ohdBufferStart);
			fareType.setOnHoldStopTime(OhdBufferStop);
		}

		fareType.setApplicableFareRules(getFareRuleDTOList(ondFareDTOList));
		fareType.setFareDiscountInfo(getFareDiscountTO(ondFareDTOList));
		return fareType;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static OnHoldReleaseTimeDTO getOnHoldReleaseTimeDTO(Collection<FlightSegmentDTO> flightDepInfo,
			Collection<OndFareDTO> ondFareDTOList, String applicationIndicator, String agentCode) throws ModuleException {
		OnHoldReleaseTimeDTO holdReleaseTimeDTO = new OnHoldReleaseTimeDTO();
		String bookingClass = OnHoldReleaseTimeDTO.ONHOLD_ANY;
		String cabinClass = OnHoldReleaseTimeDTO.ONHOLD_ANY;
		Integer flightID = null;
		int fareRuleId = -1;

		if (flightDepInfo != null) {
			Collections.sort((ArrayList) flightDepInfo);
			for (FlightSegmentDTO flightsegment : flightDepInfo) {
				// Commented by Nili Feb 14, 2012.
				// Let it be define for bus segment as well. So we don't have
				// ondcode nullpointer issues as well.
				// if(!AirproxyModuleUtils.getAirportBD().isBusSegment(flightsegment.getSegmentCode()))
				// {
				holdReleaseTimeDTO.setOndCode(flightsegment.getSegmentCode());
				holdReleaseTimeDTO.setFlightDepartureDate(flightsegment.getDepartureDateTimeZulu());
				holdReleaseTimeDTO.setDomestic(flightsegment.isDomesticFlight());
				flightID = flightsegment.getFlightId();
				break;
				// }
			}
		}
		if (ondFareDTOList != null) {
			for (OndFareDTO ondFareDTO : ondFareDTOList) {
				if (ondFareDTO != null) {
					FareSummaryDTO fareSummaryDTO = ondFareDTO.getFareSummaryDTO();
					if (fareSummaryDTO != null && fareSummaryDTO.getOndCode() != null) {
						if (holdReleaseTimeDTO.getOndCode().split("/")[0]
								.equalsIgnoreCase(fareSummaryDTO.getOndCode().split("/")[0])) {
							bookingClass = fareSummaryDTO.getBookingClassCode();
							fareRuleId = fareSummaryDTO.getFareRuleID();
							break;
						}
					}
				}
			}
			// Extract COS
			outerloop: for (OndFareDTO ondFareDTO : ondFareDTOList) {
				if (ondFareDTO != null) {
					Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = ondFareDTO.getSegmentSeatDistsDTO();
					if (segmentSeatDistsDTOs != null) {
						for (SegmentSeatDistsDTO segmentSeatDistsDTO : segmentSeatDistsDTOs) {
							if (segmentSeatDistsDTO != null) {
								if (segmentSeatDistsDTO.getFlightId() == flightID) {
									// TODO : Logical CC Change
									cabinClass = segmentSeatDistsDTO.getCabinClassCode();
									break outerloop;
								}
							}
						}
					}
				}
			}

		}
		holdReleaseTimeDTO.setAgentCode(agentCode);
		holdReleaseTimeDTO.setCabinClass(cabinClass);
		holdReleaseTimeDTO.setBookingDate(new Date());
		holdReleaseTimeDTO.setBookingClass(bookingClass);
		holdReleaseTimeDTO.setFareRuleId(fareRuleId);
		holdReleaseTimeDTO.setModuleCode(applicationIndicator);

		return holdReleaseTimeDTO;
	}

	/**
	 * 
	 * @param ondFareDTOList
	 * @return
	 */
	public static List<FareRuleDTO> getFareRuleDTOList(Collection<OndFareDTO> ondFareDTOList) {
		List<FareRuleDTO> fareRuleDTOs = new ArrayList<FareRuleDTO>();
		Map<String, String> bcToCoSMap = new HashMap<String, String>();
		for (OndFareDTO ondFareDTO : ondFareDTOList) {
			List<FareSummaryDTO> fareSumColl = new ArrayList<FareSummaryDTO>();
			if (ondFareDTO.getFareSummaryDTO() != null) {
				fareSumColl.add(ondFareDTO.getFareSummaryDTO());
			}
			try {
				if (fareSumColl.size() > 0) {
					FareRulesUtil.injectDepAirportCurrFares(fareSumColl);
				}
			} catch (ModuleException e1) {
				e1.printStackTrace();
			}
			fillBcToCoSMap(bcToCoSMap, ondFareDTO.getSegmentSeatDistsDTO());
			FareRuleDTO fareRule = getFareRuleDTOFromFareSummaryDTO(ondFareDTO.getFareSummaryDTO(), bcToCoSMap);
			fareRule.setOndSequence(ondFareDTO.getOndSequence());
			try {
				fareRule.setOrignNDest(ondFareDTO.getOndCode());
			} catch (ModuleException e) {
			}
			fareRuleDTOs.add(fareRule);
		}
		return fareRuleDTOs;
	}

	private static void fillBcToCoSMap(Map<String, String> bcToCoSMap, Collection<SegmentSeatDistsDTO> seatDistColl) {

		for (SegmentSeatDistsDTO seatDist : seatDistColl) {
			if (!bcToCoSMap.containsKey(seatDist.getBookingCode())) {
				bcToCoSMap.put(seatDist.getBookingCode(), seatDist.getCabinClassCode());
			}
		}
	}

	/**
	 * 
	 * @param fareSum
	 * @param bcToCoSMap
	 * @return
	 */
	private static FareRuleDTO getFareRuleDTOFromFareSummaryDTO(FareSummaryDTO fareSum, Map<String, String> bcToCoSMap) {
		FareRuleDTO fr = new FareRuleDTO();

		fr.setComments(fareSum.getFareRuleComment());
		fr.setAgentInstructions(fareSum.getAgentInstructions());
		fr.setOrignNDest(fareSum.getOndCode());

		// fr.setDescription(fareSum.getFareDescription());
		fr.setFareRuleCode(fareSum.getFareRuleCode());
		fr.setFareBasisCode(fareSum.getFareBasisCode());
		fr.setFareCategoryCode(fareSum.getFareCategoryCode());
		fr.setBookingClassCode(fareSum.getBookingClassCode());
		fr.setCabinClassCode(bcToCoSMap.get(fareSum.getBookingClassCode()));

		fr.setFareRuleCommentInSelectedLocal(fareSum.getFareRuleCommentSelectedLanguage());

		fr.setAdultFareApplicable(fareSum.isAdultApplicability());
		// fr.setAdultFareAmount(new BigDecimal(ondFare.getAdultFare()));
		fr.setAdultFareAmount(new BigDecimal(fareSum.getFareAmount(PaxTypeTO.ADULT)));
		fr.setAdultFareRefundable(fareSum.isAdultFareRefundable());

		fr.setChildFareApplicable(fareSum.isChildApplicability());
		// fr.setChildFareAmount(new BigDecimal(ondFare.getChildFare()));
		fr.setChildFareAmount(new BigDecimal(fareSum.getFareAmount(PaxTypeTO.CHILD)));
		fr.setChildFareRefundable(fareSum.isChildFareRefundable());
		fr.setChildFareType(fareSum.getChildFareType());

		fr.setInfantFareApplicable(fareSum.isInfantApplicability());
		// fr.setInfantFareAmount(new BigDecimal(ondFare.getInfantFare()));
		fr.setInfantFareAmount(new BigDecimal(fareSum.getFareAmount(PaxTypeTO.INFANT)));
		fr.setInfantFareRefundable(fareSum.isInfantFareRefundable());
		fr.setInfantFareType(fareSum.getInfantFareType());

		fr.setCurrencyCode(fareSum.getCurrenyCode());
		fr.setDepAPFareDTO(fareSum.getDepartureAPFare());
		fr.setFareRuleId(fareSum.getFareRuleID());

		return fr;
	}

	/**
	 * Composing the FareSegChargeTO from ondFareDTO collection which will be used to rebuild the ondFareDOT rather than
	 * re-quoting since rebuilding will be lighter than re-quoting the PQ. This is not handle for add infant.
	 * 
	 * @param journeyFareType
	 * @param ondFareDTOList
	 * @param paxAssm
	 * @param actualOndSequence
	 *            TODO
	 * @return
	 */
	public static FareSegChargeTO getFareSegChargeTO(int journeyFareType, Collection<OndFareDTO> ondFareDTOList,
			IPaxCountAssembler paxAssm, Map<Integer, Integer> actualOndSequence) {
		FareSegChargeTO fareSegChargeTO = new FareSegChargeTO();

		double totalAdultFare = 0;
		double totalChildFare = 0;
		double totalInfantFare = 0;

		double totalAdultCharge = 0;
		double totalChildCharge = 0;
		double totalInfantCharge = 0;

		boolean isOnHoldRestricted = false;

		// FIXME do we need to set the cabin class to the session.
		for (OndFareDTO ondFareDTO : ondFareDTOList) {

			totalAdultFare = totalAdultFare + ondFareDTO.getAdultFare();
			totalChildFare = totalChildFare + ondFareDTO.getChildFare();
			totalInfantFare = totalInfantFare + ondFareDTO.getInfantFare();

			double[] totalCharges = ondFareDTO.getTotalCharges();
			totalAdultCharge = totalAdultCharge + totalCharges[0];
			totalInfantCharge = totalInfantCharge + totalCharges[1];
			totalChildCharge = totalChildCharge + totalCharges[2];

			OndFareSegChargeTO ondFareSegChargeTO = new OndFareSegChargeTO();
			ondFareSegChargeTO.setFareID(ondFareDTO.getFareSummaryDTO().getFareId());
			ondFareSegChargeTO.setFareBasisCode(ondFareDTO.getFareSummaryDTO().getFareBasisCode());
			ondFareSegChargeTO.setFarePercentage(ondFareDTO.getFareSummaryDTO().getFarePercentage());
			ondFareSegChargeTO.setFareType(ondFareDTO.getFareType());
			ondFareSegChargeTO.setInbound(ondFareDTO.isInBoundOnd());
			ondFareSegChargeTO.setFareRuleId(ondFareDTO.getFareSummaryDTO().getFareRuleID());
			ondFareSegChargeTO.setOndCode(ondFareDTO.getFareSummaryDTO().getOndCode());
			int ondSequence = ondFareDTO.getOndSequence();
			if (actualOndSequence != null && actualOndSequence.containsKey(ondSequence)) {
				ondSequence = actualOndSequence.get(ondSequence);
			}
			ondFareSegChargeTO.setOndSequence(ondSequence);

			if (ondFareDTO.getRequestedOndSequence() != -1) {
				ondFareSegChargeTO.setRequestedOndSequence(ondFareDTO.getRequestedOndSequence());
			} else {
				ondFareSegChargeTO.setRequestedOndSequence(ondSequence);
			}

			ondFareDTO.setOndSequence(ondSequence);
			ondFareSegChargeTO.setModifyInbound(ondFareDTO.isModifyInbound());
			ondFareSegChargeTO.setOpenReturn(ondFareDTO.isOpenReturn());
			ondFareSegChargeTO.setSubJourneyGroup(ondFareDTO.getSubJourneyGroup());
			ondFareSegChargeTO.setSplitOperationForBusInvoked(ondFareDTO.isSplitOperationForBusInvoked());
			
			// adding segment distribution
			Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = ondFareDTO.getSegmentSeatDistsDTO();
			if (segmentSeatDistsDTOs != null) {
				for (SegmentSeatDistsDTO seatDistsDTO : segmentSeatDistsDTOs) {
					Collection<SeatDistribution> sDistributions = seatDistsDTO.getSeatDistribution();
					LinkedList<BookingClassAlloc> bookingClassAllocs = new LinkedList<BookingClassAlloc>();
					if (sDistributions != null) {
						for (SeatDistribution seatDistribution : sDistributions) {
							bookingClassAllocs.add(new BookingClassAlloc(seatDistribution.getBookingClassCode(),
									seatDistsDTO.getCabinClassCode(), seatDistsDTO.getLogicalCabinClass(),
									seatDistribution.getNoOfSeats(), seatDistribution.getBookingClassType(),
									seatDistribution.isOverbook(), seatDistribution.isBookedFlightCabinBeingSearched(),
									seatDistribution.isSameBookingClassAsExisting(), seatDistribution.isFlownOnd(),
									seatDistribution.isWaitListed(), seatDistribution.isUnTouchedOnd()));
							if (!isOnHoldRestricted && seatDistribution.isOnholdRestricted()) {
								isOnHoldRestricted = true;
							}
						}
					}
					ondFareSegChargeTO.addFsegBCAlloc(seatDistsDTO.getFlightSegId(), bookingClassAllocs);
				}
			}

			// adding charger id
			Collection<QuotedChargeDTO> chargeLsit = ondFareDTO.getAllCharges();
			if (chargeLsit != null) {
				for (QuotedChargeDTO chargeDTO : chargeLsit) {
					ondFareSegChargeTO.addChargeRateIds(chargeDTO.getChargeRateId());
				}
			}

			// adding flexi id
			Collection<FlexiRuleDTO> flexiRuleDTOs = ondFareDTO.getAllFlexiCharges();
			if (flexiRuleDTOs != null) {
				for (FlexiRuleDTO flexiInfoTO : flexiRuleDTOs) {
					ondFareSegChargeTO.addFlexiChargeId(flexiInfoTO.getFlexiRuleId());
				}
			}

			for (FlightSegmentDTO flightSegmentDTO : ondFareDTO.getSegmentsMap().values()) {
				if (flightSegmentDTO.getOperationTypeID() != null
						&& AirScheduleCustomConstants.OperationTypes.BUS_SERVICE == flightSegmentDTO.getOperationTypeID()) {
					ondFareSegChargeTO.setBusSegmentExists(true);
				}
			}

			fareSegChargeTO.addOndFareSegChargeTOs(ondFareSegChargeTO);

			fareSegChargeTO.addJourneyWiseTotalFareByPaxType(ondFareDTO.getSubJourneyGroup(), PaxTypeTO.ADULT,
					ondFareDTO.getAdultFare());
			fareSegChargeTO.addJourneyWiseTotalFareByPaxType(ondFareDTO.getSubJourneyGroup(), PaxTypeTO.CHILD,
					ondFareDTO.getChildFare());
			fareSegChargeTO.addJourneyWiseTotalFareByPaxType(ondFareDTO.getSubJourneyGroup(), PaxTypeTO.INFANT,
					ondFareDTO.getInfantFare());
		}

		fareSegChargeTO.addTotalJourneyFare(PaxTypeTO.ADULT, totalAdultFare);
		fareSegChargeTO.addTotalJourneyFare(PaxTypeTO.CHILD, totalChildFare);
		fareSegChargeTO.addTotalJourneyFare(PaxTypeTO.INFANT, totalInfantFare);

		fareSegChargeTO.addPaxWiseTotalCharges(PaxTypeTO.ADULT, totalAdultCharge);
		fareSegChargeTO.addPaxWiseTotalCharges(PaxTypeTO.CHILD, totalChildCharge);
		fareSegChargeTO.addPaxWiseTotalCharges(PaxTypeTO.INFANT, totalInfantCharge);

		if (!isOnHoldRestricted) {
			fareSegChargeTO.setAllowOnhold(true);
		}

		return fareSegChargeTO;
	}

	/**
	 * This is an overloaded method to set total service taxes amount
	 * 
	 * @param journeyFareType
	 * @param ondFareDTOList
	 * @param paxAssm
	 * @param actualOndSequence
	 * @param segments
	 * @return
	 */
	public static FareSegChargeTO getFareSegChargeTO(int journeyFareType, Collection<OndFareDTO> ondFareDTOList,
			IPaxCountAssembler paxAssm, Map<Integer, Integer> actualOndSequence, List<AvailableIBOBFlightSegment> segments) {
		FareSegChargeTO fareSegChargeTO = getFareSegChargeTO(journeyFareType, ondFareDTOList, paxAssm, actualOndSequence);
		double serviceTaxAmount = 0.0;
		if (!CollectionUtils.isEmpty(segments)) {
			Collection<QuotedChargeDTO> serviceTaxes = !CollectionUtils.isEmpty(segments.get(0).getServiceTaxes())
					? segments.get(0).getServiceTaxes().values()
					: null;
			if (!CollectionUtils.isEmpty(serviceTaxes)) {
				String selectedLogicalCabinClass = segments.get(0).getSelectedLogicalCabinClass();
				for (AvailableIBOBFlightSegment segment : segments) {
					for (Entry<String, QuotedChargeDTO> serviceTaxEntry : segment.getServiceTaxes().entrySet()) {
						for (QuotedChargeDTO charge : segment.getAllEffectiveCharges(selectedLogicalCabinClass)) {
							serviceTaxAmount += getServiceTaxAmountForCharges(paxAssm, segment, serviceTaxEntry, charge);
						}

					}
				}
				serviceTaxAmount += getServiceTaxAmountForTotalFare(paxAssm, fareSegChargeTO, serviceTaxes);
			}
		}
		fareSegChargeTO.setTotalServiceTaxAmount(AccelAeroCalculator.parseBigDecimal(serviceTaxAmount));
		return fareSegChargeTO;
	}

	private static double getServiceTaxAmountForTotalFare(IPaxCountAssembler paxAssm, FareSegChargeTO fareSegChargeTO,
			Collection<QuotedChargeDTO> serviceTaxes) {
		double serviceTaxAmount = 0.0;
		for (QuotedChargeDTO serviceTax : serviceTaxes) {
			if (serviceTax.isChargeValueInPercentage()) {
				serviceTaxAmount = serviceTaxAmount + ((serviceTax.getChargeValuePercentage() / 100)
						* (fareSegChargeTO.getTotalJourneyFare().get(PaxTypeTO.ADULT) * paxAssm.getAdultCount()
								+ fareSegChargeTO.getTotalJourneyFare().get(PaxTypeTO.INFANT) * paxAssm.getInfantCount()
								+ fareSegChargeTO.getTotalJourneyFare().get(PaxTypeTO.CHILD) * paxAssm.getChildCount()));
			} else {
				// This needs to be implemented
			}
		}
		return serviceTaxAmount;
	}

	private static double getServiceTaxAmountForCharges(IPaxCountAssembler paxAssm, AvailableIBOBFlightSegment segment,
			Entry<String, QuotedChargeDTO> serviceTaxEntry, QuotedChargeDTO charge) {
		double serviceTaxAmount = 0.0;
		if (CollectionUtils.isEmpty(segment.getServiceTaxExcludedCharges())
				|| CollectionUtils.isEmpty(segment.getServiceTaxExcludedCharges().get(serviceTaxEntry.getKey()))
				|| !segment.getServiceTaxExcludedCharges().get(serviceTaxEntry.getKey()).contains(charge.getChargeCode())) {
			if (serviceTaxEntry.getValue().isChargeValueInPercentage()) {
				double taxableAmount = 0.0;
				if (ChargeQuoteUtils.isChgApplicableForPaxType(charge, PaxTypeTO.ADULT)) {
					taxableAmount += charge.getEffectiveChargeAmount(PaxTypeTO.ADULT) * paxAssm.getAdultCount();
				}
				if (ChargeQuoteUtils.isChgApplicableForPaxType(charge, PaxTypeTO.CHILD)) {
					taxableAmount += charge.getEffectiveChargeAmount(PaxTypeTO.CHILD) * paxAssm.getChildCount();
				}
				if (ChargeQuoteUtils.isChgApplicableForPaxType(charge, PaxTypeTO.INFANT)) {
					taxableAmount += charge.getEffectiveChargeAmount(PaxTypeTO.INFANT) * paxAssm.getInfantCount();
				}
				serviceTaxAmount = (serviceTaxEntry.getValue().getChargeValuePercentage() / 100) * taxableAmount;
			} else {
				// This needs to be implemented
			}
		}
		return serviceTaxAmount;
	}

	/**
	 * 
	 * @param colFlightSegmentDTOs
	 * @return
	 * @throws ModuleException
	 */
	public static Object[] getRestrictedBufferTime(Collection<FlightSegmentDTO> colFlightSegmentDTOs) throws ModuleException {
		return ReleaseTimeUtil.getRestrictedBufferTime(colFlightSegmentDTOs);
	}

	/**
	 * 
	 * @param ondFareDTOs
	 * @return
	 */
	public static Collection<FlightSegmentDTO> getFlightDepartureInfo(Collection<OndFareDTO> ondFareDTOs) {
		return ReleaseTimeUtil.getFlightDepartureInfo(ondFareDTOs);
	}

	/**
	 * 
	 * @param colFlightSegmentDTOs
	 * @param appIndicator
	 * @return
	 * @throws ModuleException
	 */
	public static Date getReleaseTimeStamp(Collection<FlightSegmentDTO> colFlightSegmentDTOs, String agentCode,
			String appIndicator, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO) throws ModuleException {
		return ReleaseTimeUtil.getReleaseTimeStamp(colFlightSegmentDTOs, null, false, agentCode, appIndicator,
				onHoldReleaseTimeDTO);
	}

	/**
	 * 
	 * @param ondFareDTOs
	 * @return
	 */
	public static boolean isOnholdRestricted(Collection<OndFareDTO> ondFareDTOs) {
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			Collection<SegmentSeatDistsDTO> segCollection = ondFareDTO.getSegmentSeatDistsDTO();
			if (checkOnHoldRestrictions(segCollection)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param ondFareDTOs
	 * @return
	 */
	private static FareDiscountTO getFareDiscountTO(Collection<OndFareDTO> ondFareDTOs) {
		FareDiscountTO fareDiscountTO = new FareDiscountTO();
		Integer fareDiscountMin = null;
		Integer fareDiscountMax = null;
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			FareSummaryDTO fareSummaryDTO = ondFareDTO.getFareSummaryDTO();
			if (!fareSummaryDTO.hasFareDiscount()) {
				fareDiscountMin = null;
				fareDiscountMax = null;
				break;
			} else {
				// set the most restricted fare discount
				if (fareDiscountMin == null || fareDiscountMin.compareTo(fareSummaryDTO.getFareDiscountMin()) < 0) {
					fareDiscountMin = fareSummaryDTO.getFareDiscountMin();
				}

				if (fareDiscountMax == null || fareDiscountMax.compareTo(fareSummaryDTO.getFareDiscountMax()) > 0) {
					fareDiscountMax = fareSummaryDTO.getFareDiscountMax();
				}
			}
		}
		if (fareDiscountMin != null && fareDiscountMax != null && fareDiscountMax.compareTo(fareDiscountMin) >= 0) {
			fareDiscountTO.setFareDiscountMin(fareDiscountMin);
			fareDiscountTO.setFareDiscountMax(fareDiscountMax);
		}
		return fareDiscountTO;
	}

	/**
	 * 
	 * @param colsegSeatDto
	 * @return
	 */
	private static boolean checkOnHoldRestrictions(Collection<SegmentSeatDistsDTO> colsegSeatDto) {
		Collection<SeatDistribution> colSeatDist;
		Iterator<SegmentSeatDistsDTO> itersegSeats;
		Iterator<SeatDistribution> iterSeatDist;
		SegmentSeatDistsDTO segSeatDto;
		SeatDistribution seatDist;

		itersegSeats = colsegSeatDto.iterator();
		while (itersegSeats.hasNext()) {

			segSeatDto = itersegSeats.next();
			colSeatDist = segSeatDto.getEffectiveSeatDistribution();
			if (colSeatDist != null) {
				iterSeatDist = colSeatDist.iterator();
				while (iterSeatDist.hasNext()) {
					seatDist = iterSeatDist.next();
					if (seatDist.isOnholdRestricted()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @param fareSummary
	 * @param adultCount
	 * @param childCount
	 * @param infantCount
	 * @return
	 */
	public static BigDecimal calculateTotalFare(FareSummaryDTO fareSummary, int adultCount, int childCount, int infantCount) {

		BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (fareSummary != null) {
			BigDecimal totalAdultFare = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalChildFare = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalInfantFare = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (fareSummary.getFareAmount(PaxTypeTO.ADULT) != null) {
				totalAdultFare = AccelAeroCalculator.multiply(new BigDecimal(fareSummary.getFareAmount(PaxTypeTO.ADULT)),
						new BigDecimal(adultCount));
			}
			if (fareSummary.getFareAmount(PaxTypeTO.CHILD) != null) {
				totalChildFare = AccelAeroCalculator.multiply(new BigDecimal(fareSummary.getFareAmount(PaxTypeTO.CHILD)),
						new BigDecimal(childCount));
			}
			if (fareSummary.getFareAmount(PaxTypeTO.INFANT) != null) {
				totalInfantFare = AccelAeroCalculator.multiply(new BigDecimal(fareSummary.getFareAmount(PaxTypeTO.INFANT)),
						new BigDecimal(infantCount));
			}
			totalFare = AccelAeroCalculator.add(totalFare, totalAdultFare, totalChildFare, totalInfantFare);
		}
		return totalFare;
	}

	/**
	 * 
	 * @param paxAssm
	 * @param appIndicator
	 * @param quoteOndFlexi
	 * @param selectedFlightDTO
	 * @return
	 * @throws ModuleException
	 */
	public static PriceInfoTO getChangedPriceFltInfo(ChangeFaresDTO changeFaresDTO, IPaxCountAssembler paxAssm, String agentCode,
			String appIndicator, Map<Integer, Boolean> quoteOndFlexi, BaseAvailRQ priceQuoteRQ) throws ModuleException {
		if (changeFaresDTO == null) {
			return null;
		}

		// BigDecimal totalFare =
		// AccelAeroCalculator.getDefaultBigDecimalZero();
		Map<Integer, Integer> ondWiseFareType = new HashMap<Integer, Integer>();
		List<Collection<OndFareDTO>> journeyONDWiseFareONDs = new ArrayList<Collection<OndFareDTO>>();
		Map<Integer, Integer> ondWiseFareIds = new HashMap<Integer, Integer>();
		Collection<OndFareDTO> ondFareDTOList = null;
		List<BundledFareDTO> selectedBundledServices = new ArrayList<BundledFareDTO>();
		Map<Integer, BundledFareDTO> ondSelectedBundledServices = changeFaresDTO.getOndSelectedBundledFares();

		if (changeFaresDTO != null) {
			if (changeFaresDTO.getChangedFareType() != FareTypes.NO_FARE) {
				ondFareDTOList = changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs();

				List<OndFareDTO> ondFareDTOListOrderBySequence = new ArrayList<OndFareDTO>(ondFareDTOList);
				Collections.sort(ondFareDTOListOrderBySequence, new Comparator<OndFareDTO>() {
					public int compare(OndFareDTO p1, OndFareDTO p2) {
						return new Long(p1.getOndSequence()).compareTo(new Long(p2.getOndSequence()));
					}
				});

				for (OndFareDTO ondFareDTO : ondFareDTOListOrderBySequence) {
					if (journeyONDWiseFareONDs.size() == ondFareDTO.getOndSequence()) {
						journeyONDWiseFareONDs.add(new ArrayList<OndFareDTO>());
						selectedBundledServices.add(ondSelectedBundledServices.get(ondFareDTO.getOndSequence()));
					}
					journeyONDWiseFareONDs.get(ondFareDTO.getOndSequence()).add(ondFareDTO);

					ondWiseFareType.put(ondFareDTO.getOndSequence(), ondFareDTO.getFareType());
					ondWiseFareIds.put(ondFareDTO.getOndSequence(), ondFareDTO.getFareId());

				}
			}
		}

		PriceInfoTO priceInfoTO = new PriceInfoTO();
		priceInfoTO.setFareTypeTO(getFareTypeTO(journeyONDWiseFareONDs, paxAssm, agentCode, appIndicator, priceQuoteRQ));
		priceInfoTO.getFareTypeTO().setOndWiseFareType(ondWiseFareType);
		priceInfoTO.getFareTypeTO().setOndWiseFareId(ondWiseFareIds);
		priceInfoTO.getFareTypeTO().setOndBundledFareDTOs(selectedBundledServices);

		priceInfoTO.setFareSegChargeTO(getFareSegChargeTO(changeFaresDTO.getChangedFareType(), ondFareDTOList, paxAssm, null));
		priceInfoTO.getFareSegChargeTO().setOndBundledFareDTOs(selectedBundledServices);

		Map<Integer, Map<String, List<BundledFareLiteDTO>>> ondAvailableBundledFares = changeFaresDTO.getAvailableBundledFares();

		Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
		// Set available logical cabin class after fare changed
		for (OndFareDTO ondFareDTO : ondFareDTOList) {
			BundledFareDTO selBundledFare = (ondSelectedBundledServices != null && !ondSelectedBundledServices.isEmpty())
					? ondSelectedBundledServices.get(ondFareDTO.getRequestedOndSequence())
					: null;
			boolean isFlexiQuote = quoteOndFlexi.get(ondFareDTO.getRequestedOndSequence()) != null
					? quoteOndFlexi.get(ondFareDTO.getRequestedOndSequence())
					: false;
			boolean isFlexiSelected = priceQuoteRQ.getAvailPreferences().getOndFlexiSelected()
					.get(ondFareDTO.getOndSequence()) != null
							? priceQuoteRQ.getAvailPreferences().getOndFlexiSelected().get(ondFareDTO.getRequestedOndSequence())
							: false;
			List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses = new ArrayList<LogicalCabinClassInfoTO>();
			SegmentSeatDistsDTO segmentSeatDistsDTO = ondFareDTO.getSegmentSeatDistsDTO().iterator().next();
			LogicalCabinClassDTO logicalCC = cachedLogicalCCMap.get(segmentSeatDistsDTO.getLogicalCabinClass());

			LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
			logicalCabinClassInfoTO.setLogicalCCCode(logicalCC.getLogicalCCCode());
			logicalCabinClassInfoTO.setLogicalCCDesc((isFlexiSelected && !logicalCC.isFreeFlexiEnabled())
					? logicalCC.getFlexiDescription()
					: logicalCC.getDescription());
			if (selBundledFare == null) {
				logicalCabinClassInfoTO.setSelected(true);
			}
			logicalCabinClassInfoTO.setComment(isFlexiSelected ? logicalCC.getFlexiComment() : logicalCC.getComment());
			logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(logicalCC.getBundleFareDescriptionTemplateDTO());
			logicalCabinClassInfoTO.setRank(logicalCC.getNestRank());
			logicalCabinClassInfoTO.setWithFlexi(isFlexiSelected);
			segmentLogicalCabinClasses.add(logicalCabinClassInfoTO);

			if (ondAvailableBundledFares.containsKey(ondFareDTO.getOndSequence())) {
				Map<String, List<BundledFareLiteDTO>> logicalBundledFareDTOs = ondAvailableBundledFares
						.get(ondFareDTO.getOndSequence());
				if (logicalBundledFareDTOs != null && !logicalBundledFareDTOs.isEmpty()
						&& logicalBundledFareDTOs.containsKey(logicalCC.getLogicalCCCode())) {
					List<BundledFareLiteDTO> availableBundledFares = logicalBundledFareDTOs.get(logicalCC.getLogicalCCCode());
					if (availableBundledFares != null && !availableBundledFares.isEmpty()) {

						Collections.sort(segmentLogicalCabinClasses);
						LogicalCabinClassInfoTO lastElement = segmentLogicalCabinClasses
								.get(segmentLogicalCabinClasses.size() - 1);
						int lastRank = lastElement.getRank();

						for (BundledFareLiteDTO bundledFareLiteDTO : availableBundledFares) {
							logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
							logicalCabinClassInfoTO.setBundledFarePeriodId(bundledFareLiteDTO.getBundleFarePeriodId());
							logicalCabinClassInfoTO.setComment(bundledFareLiteDTO.getDescription());
							logicalCabinClassInfoTO.setLogicalCCCode(logicalCC.getLogicalCCCode());
							logicalCabinClassInfoTO.setLogicalCCDesc(bundledFareLiteDTO.getBundledFareName());
							logicalCabinClassInfoTO.setRank(++lastRank);
							logicalCabinClassInfoTO.setWithFlexi(bundledFareLiteDTO.isFlexiIncluded());
							logicalCabinClassInfoTO.setBookingClasses(bundledFareLiteDTO.getBookingClasses());
							logicalCabinClassInfoTO.setSegmentBookingClasses(bundledFareLiteDTO.getSegmentBookingClass());
							logicalCabinClassInfoTO.setBundledFareFreeServiceName(bundledFareLiteDTO.getFreeServices());
							logicalCabinClassInfoTO.setBundledFareFee(bundledFareLiteDTO.getPerPaxBundledFee());
							if (selBundledFare != null
									&& selBundledFare.getBundledFarePeriodId().equals(bundledFareLiteDTO.getBundleFarePeriodId())) {
								logicalCabinClassInfoTO.setSelected(true);
							}
							logicalCabinClassInfoTO.setImageUrl(bundledFareLiteDTO.getImageUrl());
							logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(bundledFareLiteDTO.getBundleFareDescriptionTemplateDTO());
							segmentLogicalCabinClasses.add(logicalCabinClassInfoTO);
						}

					}
				}
			}

			FlightSegmentDTO fltSeg = ondFareDTO.getFlightSegmentDTO(ondFareDTO.getFlightSegmentIds().iterator().next());

			priceInfoTO.addAvailableLogicalCC(ondFareDTO.getOndCode(), ondFareDTO.getOndSequence(), segmentLogicalCabinClasses,
					ondFareDTO.getRequestedOndSequence());
		}

		return priceInfoTO;
	}

	private static boolean isSelectedFlightAvailabilitySearch(Integer ondSequence,
			Map<String, List<Integer>> logicalCabinClassSelection) {
		boolean isAvailSearch = true;

		if (logicalCabinClassSelection != null) {
			String logicalCC = FareQuoteUtil.getClassOfserviceFromOndSequence(logicalCabinClassSelection, ondSequence);
			if (logicalCC != null) {
				isAvailSearch = false;
			}
		}

		return isAvailSearch;
	}

	private static List<String> getJourneyOndCodes(Collection<OndFareDTO> ondFareDTOs) throws ModuleException {
		List<String> journeyOnds = new ArrayList<String>();
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			String ondCode = ondFareDTO.getOndCode();
			if (ondCode != null && !ondCode.isEmpty()) {
				journeyOnds.add(ondCode);
			}

		}
		return journeyOnds;
	}

	private static boolean isAllUnTouchedOnDs(List<Collection<OndFareDTO>> journeyOndFareDTOList) throws ModuleException {
		for (Collection<OndFareDTO> ondFareDTOs : journeyOndFareDTOList) {
			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				if (!ondFareDTO.isUnTouchedOnd()) {
					return false;
				}
			}
		}
		return true;
	}
	
	private static void updateFareDTOsForSplitOperation(Collection<OndFareDTO> ondFares){
		for(OndFareDTO fare : ondFares){
			fare.setSplitOperationForBusInvoked(true);
		}
	}
}
