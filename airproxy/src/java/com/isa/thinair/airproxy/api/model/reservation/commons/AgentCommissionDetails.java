package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * META info related to Agent Commissions grant and removal
 * 
 */
public class AgentCommissionDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private Set<Integer> commissionApplicableFareIds = null; // For introducing onds
	private Set<Integer> commissionRemovalExcludedFareIds = null; // Already persisted onds: used in modify flow

	public AgentCommissionDetails() {
		commissionApplicableFareIds = new HashSet<Integer>();
		commissionRemovalExcludedFareIds = new HashSet<Integer>();
	}

	public Set<Integer> getCommissionApplicableFareIds() {
		return commissionApplicableFareIds;
	}

	public void setCommissionApplicableFareIds(Set<Integer> commissionApplicableFareIds) {
		this.commissionApplicableFareIds = commissionApplicableFareIds;
	}

	public Set<Integer> getCommissionRemovalExcludedFareIds() {
		return commissionRemovalExcludedFareIds;
	}

	public void setCommissionRemovalExcludedFareIds(Set<Integer> commissionRemovalExcludedFareIds) {
		this.commissionRemovalExcludedFareIds = commissionRemovalExcludedFareIds;
	}

	public void addCommissionApplicableFareIds(Integer fareId) {
		getCommissionApplicableFareIds().add(fareId);
	}

	public void addCommissionRemovalExcludedFareIds(Integer fareId) {
		getCommissionRemovalExcludedFareIds().add(fareId);
	}

}
