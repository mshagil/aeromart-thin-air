/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;

public class LCCClientBalancePayment implements Serializable {

	private static final long serialVersionUID = 2967459884599616097L;

	private String groupPNR;

	private String ownerAgent;

	private boolean isForceConfirm;

	private boolean splittedBooking = false;

	private CommonReservationContactInfo contactInfo;

	/*
	 * Holds whether the reservation use other carrier pax credit for payment If yes the reservation need to be saved as
	 * a interline reservation
	 */
	private boolean otherCarrierPaxCreditUser;

	private Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers = new HashMap<Integer, LCCClientPaymentAssembler>();

	private Map externalChargesMap;

	/**
	 * hold reservation status
	 */
	private String reservationStatus;

	private Collection<LCCClientReservationSegment> pnrSegments;

	private Collection<LCCClientReservationPax> passengers;

	private Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap;

	private boolean actualPayment;

	private boolean acceptPaymentsThanPayable;

	private LoyaltyPaymentInfo loyaltyPaymentInfo;

	private PayByVoucherInfo payByVoucherInfo;

	public LCCClientBalancePayment() {

	}

	public void addPassengerPayments(Integer pnrPaxId, LCCClientPaymentAssembler lccClientPaymentAssembler) {
		paxSeqWisePayAssemblers.put(pnrPaxId, lccClientPaymentAssembler);
	}

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param groupPNR
	 *            the groupPNR to set
	 */
	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the ownerAgent
	 */
	public String getOwnerAgent() {
		return ownerAgent;
	}

	/**
	 * @param ownerAgent
	 *            the ownerAgent to set
	 */
	public void setOwnerAgent(String ownerAgent) {
		this.ownerAgent = ownerAgent;
	}

	/**
	 * @return the contactInfo
	 */
	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the paxSeqWisePayAssemblers
	 */
	public Map<Integer, LCCClientPaymentAssembler> getPaxSeqWisePayAssemblers() {
		return paxSeqWisePayAssemblers;
	}

	public void setPaxSeqWisePayAssemblers(Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers) {
		this.paxSeqWisePayAssemblers = paxSeqWisePayAssemblers;
	}

	public boolean isForceConfirm() {
		return isForceConfirm;
	}

	public void setForceConfirm(boolean isForceConfirm) {
		this.isForceConfirm = isForceConfirm;
	}

	public void setExternalChargesMap(Map externalChargesMap) {
		this.externalChargesMap = externalChargesMap;
	}

	public Map getExternalChargesMap() {
		return externalChargesMap;
	}

	public boolean isSplittedBooking() {
		return splittedBooking;
	}

	public void setSplittedBooking(boolean splittedBooking) {
		this.splittedBooking = splittedBooking;
	}

	/**
	 * @return the useOtherCarrierPaxCredit
	 */
	public boolean isOtherCarrierPaxCreditUser() {
		return otherCarrierPaxCreditUser;
	}

	/**
	 * @param useOtherCarrierPaxCredit
	 *            the useOtherCarrierPaxCredit to set
	 */
	public void setOtherCarrierPaxCreditUser(boolean useOtherCarrierPaxCredit) {
		this.otherCarrierPaxCreditUser = useOtherCarrierPaxCredit;
	}

	/**
	 * @return
	 */
	public String getReservationStatus() {
		return reservationStatus;
	}

	/**
	 * @param reservationStatus
	 */
	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	/**
	 * @return
	 */
	public Collection<LCCClientReservationSegment> getPnrSegments() {
		return pnrSegments;
	}

	/**
	 * @param pnrSegments
	 */
	public void setPnrSegments(Collection<LCCClientReservationSegment> pnrSegments) {
		this.pnrSegments = pnrSegments;
	}

	public Collection<LCCClientReservationPax> getPassengers() {
		return passengers;
	}

	public void setPassengers(Collection<LCCClientReservationPax> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return the temporyPaymentMap
	 */
	public Map<Integer, CommonCreditCardPaymentInfo> getTemporyPaymentMap() {
		return temporyPaymentMap;
	}

	/**
	 * @param temporyPaymentMap
	 *            the temporyPaymentMap to set
	 */
	public void setTemporyPaymentMap(Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap) {
		this.temporyPaymentMap = temporyPaymentMap;
	}

	public boolean isActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(boolean actualPayment) {
		this.actualPayment = actualPayment;
	}

	public LoyaltyPaymentInfo getLoyaltyPaymentInfo() {
		return loyaltyPaymentInfo;
	}

	public void setLoyaltyPaymentInfo(LoyaltyPaymentInfo loyaltyPaymentInfo) {
		this.loyaltyPaymentInfo = loyaltyPaymentInfo;
	}

	public boolean isAcceptPaymentsThanPayable() {
		return acceptPaymentsThanPayable;
	}

	public void setAcceptPaymentsThanPayable(boolean acceptPaymentsThanPayable) {
		this.acceptPaymentsThanPayable = acceptPaymentsThanPayable;
	}

	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

}