package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LCCAirSeatDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String seatNumber;
	private String seatAvailability;
	private BigDecimal seatCharge;
	private String seatType;
	private String bookedPassengerType;
	private List<String> notAllowedPassengerType;
	private String seatMessage;
	private String logicalCabinClassCode;
	private boolean alertAutoCancellation;
	private Map<String, String> socialIdentity;
	private String paxName;
	private String seatVisibility;
	private String seatLocationType;

	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	public String getSeatAvailability() {
		return seatAvailability;
	}

	public void setSeatAvailability(String seatAvailability) {
		this.seatAvailability = seatAvailability;
	}

	public BigDecimal getSeatCharge() {
		return seatCharge;
	}

	public void setSeatCharge(BigDecimal seatCharge) {
		this.seatCharge = seatCharge;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public String getBookedPassengerType() {
		return bookedPassengerType;
	}

	public void setBookedPassengerType(String bookedPassengerType) {
		this.bookedPassengerType = bookedPassengerType;
	}

	public List<String> getNotAllowedPassengerType() {
		if (notAllowedPassengerType == null)
			notAllowedPassengerType = new ArrayList<String>();
		return notAllowedPassengerType;
	}

	public void setNotAllowedPassengerType(List<String> notAllowedPassengerType) {
		this.notAllowedPassengerType = notAllowedPassengerType;
	}

	public String getSeatMessage() {
		return seatMessage;
	}

	public void setSeatMessage(String seatMessage) {
		this.seatMessage = seatMessage;
	}

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public Map<String, String> getSocialIdentity() {
		if (socialIdentity == null) {
			socialIdentity = new HashMap<String, String>();
		}

		return socialIdentity;
	}

	public void setSocialIdentity(Map<String, String> socialIdentity) {
		this.socialIdentity = socialIdentity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCAirSeatDTO [bookedPassengerType=");
		builder.append(bookedPassengerType);
		builder.append(", notAllowedPassengerType=");
		builder.append(notAllowedPassengerType);
		builder.append(", seatAvailability=");
		builder.append(seatAvailability);
		builder.append(", seatCharge=");
		builder.append(seatCharge);
		builder.append(", seatNumber=");
		builder.append(seatNumber);
		builder.append(", seatType=");
		builder.append(seatType);
		builder.append(", seatMessage=");
		builder.append(seatMessage);
		builder.append(", logicalCabinClassCode=");
		builder.append(logicalCabinClassCode);
		builder.append(", alertAutoCancellation=");
		builder.append(alertAutoCancellation);
		builder.append(", paxName=");
		builder.append(paxName);
		builder.append("]");
		return builder.toString();
	}

	public String getSeatVisibility() {
		return seatVisibility;
	}

	public void setSeatVisibility(String seatVisibility) {
		this.seatVisibility = seatVisibility;
	}

	public void setSeatLocationType(String seatLocationType) {
		this.seatLocationType = seatLocationType;
	}

	public String getSeatLocationType() {
		return seatLocationType;
	}
}
