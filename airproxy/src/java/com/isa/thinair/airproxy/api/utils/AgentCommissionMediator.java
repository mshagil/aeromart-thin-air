package com.isa.thinair.airproxy.api.utils;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.utils.assembler.PreferenceAnalyzer;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Mediate the agent commission for defined strategies
 * 
 * 
 */
public class AgentCommissionMediator implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PreferenceAnalyzer preferenceAnalyzer = null;

	public AgentCommissionMediator(PreferenceAnalyzer preferenceAnalyzer) {
		this.preferenceAnalyzer = preferenceAnalyzer;
		preferenceAnalyzer.anlyze();
	}

	public boolean applyAgentCommissionForOnd(String ondCode, List<String> journeyOnds,
			List<List<String>> journeyOndSummary) {
		if (!AppSysParamsUtil.isAgentCommmissionEnabled()) {
			return false;
		}

		return AirproxyModuleUtils.getReservationBD().applyAgentCommissionForOnd(preferenceAnalyzer, ondCode,
				journeyOnds, journeyOndSummary);

	}

	/** Holds the fare ids to keep the commission in modify (OND)flow */
	public Integer getExcludedFareIdsFromCommissionRemoval() {
		PreferenceAnalyzer.MODE selectedMode = preferenceAnalyzer.getOperation();
		if (preferenceAnalyzer != null && selectedMode != null
				&& selectedMode.equals(PreferenceAnalyzer.MODE.MODIFY_OND)) {
			return preferenceAnalyzer.getOldFareId();
		}
		return null;
	}

}
