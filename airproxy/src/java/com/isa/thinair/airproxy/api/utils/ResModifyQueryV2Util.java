package com.isa.thinair.airproxy.api.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airproxy.api.dto.FareInfoTOV2;
import com.isa.thinair.airproxy.api.dto.PAXSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.ResBalancesSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.dto.SegmentSummaryTOV2;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.LccPaxPaymentTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author Nilindra Fernando
 */
public class ResModifyQueryV2Util {

	public static ResBalancesSummaryTOV2 getResBalancesForAlt(String alterationType, Reservation res,
			Collection<Integer> pnrSegIds, Collection<Integer> newFltSegIds, Collection<OndFareDTO> colOndFareDTOs,
			boolean addCurrentModCharges, CustomChargesTO customChargesTO,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap, boolean hasExternalCarrierPayments,
			float fareDiscountPercentage, Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap) throws ModuleException {
		Date quotedDateTime = new Date();

		ResBalancesSummaryTOV2 resBalancesSummaryTOV2 = new ResBalancesSummaryTOV2();

		// Pax charges for specified pnrSegments
		Collection<PnrChargesDTO> pnrPaxCharges = null;
		Collection<PnrChargesDTO> pnrSegPaxCharges = null;
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)) {
			pnrPaxCharges = AirproxyModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), true, res.getVersion(),
					customChargesTO, hasExternalCarrierPayments, false, null);
			pnrSegPaxCharges = pnrPaxCharges;
		} else if (ReservationConstants.AlterationType.ALT_VOID_RES.equals(alterationType)) {
			pnrPaxCharges = AirproxyModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), true, res.getVersion(),
					customChargesTO, hasExternalCarrierPayments, true, null);
			pnrSegPaxCharges = pnrPaxCharges;
		} else if (ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)) {
			pnrPaxCharges = AirproxyModuleUtils.getResSegmentBD().getOndChargesForCancellationApplyingModCharge(res.getPnr(),
					pnrSegIds, newFltSegIds, true, res.getVersion(), customChargesTO, hasExternalCarrierPayments, false);
			pnrSegPaxCharges = AirproxyModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), true, false, true,
					res.getVersion());

		} else if (ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
			pnrPaxCharges = AirproxyModuleUtils.getResSegmentBD().getOndChargesForCancellation(res.getPnr(), pnrSegIds, true,
					res.getVersion(), customChargesTO, hasExternalCarrierPayments, false);
			pnrSegPaxCharges = AirproxyModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), true, res.getVersion(),
					customChargesTO, false, false, null);

		} else if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			pnrPaxCharges = AirproxyModuleUtils.getResSegmentBD().getOndChargesForModification(res.getPnr(), pnrSegIds,
					newFltSegIds, true, false, res.getVersion(), customChargesTO, false);

			// For Rak Airways, set Insurance charge as refundable(Lalanthi Date:23/09/2010)
			if (AppSysParamsUtil.isRakEnabled()) {
				setModificationChargesForRakInsurance(pnrPaxCharges);
			}
			pnrSegPaxCharges = AirproxyModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), true, false, true,
					res.getVersion());
		} else if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
			pnrPaxCharges = AirproxyModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), true, false, false,
					res.getVersion());
			pnrSegPaxCharges = pnrPaxCharges;
		} else if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			pnrPaxCharges = AirproxyModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), true, false, false,
					res.getVersion());
			pnrSegPaxCharges = pnrPaxCharges;
		} else {
			throw new ModuleException("airreservation.modify.reservation.query.invalid.mod.type");
		}

		Map<Integer, PnrChargesDTO> pnrPaxChargesMap = AirProxyReservationUtil.indexOndChargesByPax(pnrPaxCharges);
		Map<Integer, PnrChargesDTO> pnrPaxAllSegChargesMap = AirProxyReservationUtil.indexOndChargesByPax(pnrSegPaxCharges);
		if (pnrPaxAllSegChargesMap.containsKey(null)) {
			pnrPaxAllSegChargesMap = pnrPaxChargesMap;
		}

		// Current and new OND balances
		// TODO to merge both the methods to a single method
		if (addCurrentModCharges) {
			resBalancesSummaryTOV2.setSegmentSummaryTOV2(getONDBalancesForAlt(res, pnrPaxChargesMap, colOndFareDTOs,
					alterationType, quotedDateTime, paxExtChargeMap, fareDiscountPercentage));
		} else {
			resBalancesSummaryTOV2.setSegmentSummaryTOV2(getONDBalancesForAltInterline(res, pnrPaxChargesMap, colOndFareDTOs,
					alterationType, quotedDateTime, paxExtChargeMap));
		}
		if (addCurrentModCharges) {
			resBalancesSummaryTOV2.setPaxSummaryTOV2(getPaxBalancesForAlt(res, colOndFareDTOs, pnrPaxAllSegChargesMap,
					alterationType, quotedDateTime, pnrPaxChargesMap, addCurrentModCharges, pnrSegIds, paxExtChargeMap,
					fareDiscountPercentage, quotedExtChgMap));
		} else {
			resBalancesSummaryTOV2.setPaxSummaryTOV2(getPaxBalancesForAltInterline(res, colOndFareDTOs, pnrPaxAllSegChargesMap,
					pnrPaxChargesMap, alterationType, quotedDateTime, pnrSegIds, paxExtChargeMap, quotedExtChgMap));
		}

		BigDecimal[] totalAmounts = getTotalAmounts(resBalancesSummaryTOV2.getPaxSummaryTOV2(), res.isInfantPaymentRecordedWithInfant());

		resBalancesSummaryTOV2.setTotalCnxChargeForCurrentAlt(totalAmounts[0]);
		resBalancesSummaryTOV2.setTotalModChargeForCurrentAlt(totalAmounts[1]);
		resBalancesSummaryTOV2.setTotalPrice(totalAmounts[2]);
		resBalancesSummaryTOV2.setTotalAmountDue(totalAmounts[3]);
		resBalancesSummaryTOV2.setTotalCreditAmount(totalAmounts[4]);
		resBalancesSummaryTOV2.setTotalFareDiscount(totalAmounts[5]);

		resBalancesSummaryTOV2.setColPayments(new ArrayList<ReservationTnx>());
		resBalancesSummaryTOV2.setColCredits(new ArrayList<ReservationTnx>());
		resBalancesSummaryTOV2.setColLccPaxPayments(new ArrayList<LccPaxPaymentTO>());

		return resBalancesSummaryTOV2;
	}

	/**
	 * For Rak insurance only the selected insurance charges should be act like refundable, so balances should be
	 * updated accordingly.
	 * 
	 * @param pnrPaxCharges
	 */
	private static void setModificationChargesForRakInsurance(Collection<PnrChargesDTO> pnrPaxCharges) {
		Collection<ChargeMetaTO> insuranceChargeMetaTOs = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newNonRefChargeMetaTOs = new ArrayList<ChargeMetaTO>();
		for (PnrChargesDTO pnrPaxCharge : pnrPaxCharges) {
			for (ChargeMetaTO chargeMetaTO : pnrPaxCharge.getNonRefundableChargeMetaAmounts()) {
				if (chargeMetaTO != null) {
					if (ReservationInternalConstants.SERVICE_CALLER.INSURANCE.equalsIgnoreCase(chargeMetaTO.getChargeCode())) {
						insuranceChargeMetaTOs.add(chargeMetaTO);
					} else {
						newNonRefChargeMetaTOs.add(chargeMetaTO);
					}
				}
			}
			pnrPaxCharge.setNonRefundableChargeMetaAmounts(newNonRefChargeMetaTOs);
			if (pnrPaxCharge.getRefundableChargeMetaAmounts() != null) {
				pnrPaxCharge.getRefundableChargeMetaAmounts().addAll(insuranceChargeMetaTOs);
			} else {
				pnrPaxCharge.setRefundableChargeMetaAmounts(insuranceChargeMetaTOs);
			}
		}
	}

	private static void composeCustomerPaymentsIfResIsLCC(Reservation res, String alterationType,
			ResBalancesSummaryTOV2 resBalancesSummaryTOV2) throws ModuleException {

		if (res.getAdminInfo().getOriginChannelId() == ReservationInternalConstants.SalesChannel.LCC
				&& !ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
			for (PAXSummaryTOV2 paxSummaryTOV2 : resBalancesSummaryTOV2.getPaxSummaryTOV2()) {
				paxSummaryTOV2.setColLccPaxPayments(new ArrayList<LccPaxPaymentTO>());
			}

			resBalancesSummaryTOV2.setColLccPaxPayments(new ArrayList<LccPaxPaymentTO>());
		}
	}

	// private static Map<Integer, Collection<LccPaxPaymentTO>> getPaxSeqAndLccPaxPaymentTO(List<LccResPaymentTO>
	// customerPayments) {
	// Map<Integer, Collection<LccPaxPaymentTO>> paxSeqAndLccPaxPaymentTOMap = new HashMap<Integer,
	// Collection<LccPaxPaymentTO>>();
	//
	// for (LccResPaymentTO lccResPaymentTO : customerPayments) {
	// for (LccPaxPaymentTO lccPaxPaymentTO : lccResPaymentTO.getColLCCPaxPaymentTO()) {
	// Collection<LccPaxPaymentTO> colLccPaxPaymentTO = paxSeqAndLccPaxPaymentTOMap
	// .get(lccPaxPaymentTO.getPaxSequence());
	//
	// if (colLccPaxPaymentTO == null) {
	// colLccPaxPaymentTO = new ArrayList<LccPaxPaymentTO>();
	// colLccPaxPaymentTO.add(lccPaxPaymentTO);
	//
	// paxSeqAndLccPaxPaymentTOMap.put(lccPaxPaymentTO.getPaxSequence(), colLccPaxPaymentTO);
	// } else {
	// colLccPaxPaymentTO.add(lccPaxPaymentTO);
	// }
	// }
	// }
	//
	// return paxSeqAndLccPaxPaymentTOMap;
	// }

	// @SuppressWarnings("unchecked")
	// private static Map<Integer, Integer> getPnrPaxIdAndPaxSeqMap(Reservation res) {
	// Map<Integer, Integer> pnrPaxIdAndPaxSeqMap = new HashMap<Integer, Integer>();
	//
	// for (ReservationPax reservationPax : (Collection<ReservationPax>) res.getPassengers()) {
	// pnrPaxIdAndPaxSeqMap.put(reservationPax.getPnrPaxId(), reservationPax.getPaxSequence());
	// }
	//
	// return pnrPaxIdAndPaxSeqMap;
	// }

	/**
	 * Returns the total amounts 0 - Total CNX Charge 1 - Total MOD Charge 2 - Total Price 3 - Total Available Balance
	 * 
	 * @param colPaxSummaryTOV2
	 * @param isInfantPaymentSeparated
	 * @return
	 */
	private static BigDecimal[] getTotalAmounts(Collection<PAXSummaryTOV2> colPaxSummaryTOV2, boolean isInfantPaymentSeparated) {
		BigDecimal totalCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (PAXSummaryTOV2 paxSummaryTOV2 : colPaxSummaryTOV2) {
			if (isInfantPaymentSeparated || !ReservationInternalConstants.PassengerType.INFANT.equals(paxSummaryTOV2.getPaxType())) {
				totalCnxCharge = AccelAeroCalculator.add(totalCnxCharge, paxSummaryTOV2.getTotalCnxChargeForCurrentAlt());
				totalModCharge = AccelAeroCalculator.add(totalModCharge, paxSummaryTOV2.getTotalModChargeForCurrentAlt());

				totalPrice = AccelAeroCalculator.add(totalPrice, paxSummaryTOV2.getTotalPrice());
				totalAmountDue = AccelAeroCalculator.add(totalAmountDue, (BigDecimal.ZERO.compareTo(paxSummaryTOV2
						.getTotalAmountDue()) > 0) ? BigDecimal.ZERO : paxSummaryTOV2.getTotalAmountDue());
				totalCreditAmount = AccelAeroCalculator.add(totalCreditAmount, paxSummaryTOV2.getTotalCreditAmount());
				totalFareDiscount = AccelAeroCalculator.add(totalFareDiscount, paxSummaryTOV2.getTotalPaxFareDiscount());
			}
		}

		return new BigDecimal[] { totalCnxCharge, totalModCharge, totalPrice, totalAmountDue, totalCreditAmount,
				totalFareDiscount };
	}

	/**
	 * Returns the total amounts 0 - Total CNX Charge 1 - Total MOD Charge 2 - Total Price 3 - Total Available Balance
	 * 
	 * @param colPaxSummaryTOV2
	 * @return
	 */
	// private static BigDecimal[] getTotalAmounts(Collection<PAXSummaryTOV2> colPaxSummaryTOV2, String alterationType)
	// {
	// BigDecimal totalCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	// BigDecimal totalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	// BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
	// BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
	// BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	//
	// for (PAXSummaryTOV2 paxSummaryTOV2 : colPaxSummaryTOV2) {
	// if (!ReservationInternalConstants.PassengerType.INFANT.equals(paxSummaryTOV2.getPaxType())) {
	// totalCnxCharge = AccelAeroCalculator.add(totalCnxCharge, paxSummaryTOV2.getTotalCnxChargeForCurrentAlt());
	// totalModCharge = AccelAeroCalculator.add(totalModCharge, paxSummaryTOV2.getTotalModChargeForCurrentAlt());
	// totalPrice = AccelAeroCalculator.add(totalPrice, paxSummaryTOV2.getTotalPrice());
	// totalAmountDue = AccelAeroCalculator.add(totalAmountDue, paxSummaryTOV2.getTotalAmountDue());
	// totalCreditAmount = AccelAeroCalculator.add(totalCreditAmount, paxSummaryTOV2.getTotalCreditAmount());
	// } else {
	// if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
	// totalPrice = AccelAeroCalculator.add(totalPrice, paxSummaryTOV2.getTotalPrice());
	// }
	// }
	// }
	// return new BigDecimal[] { totalCnxCharge, totalModCharge, totalPrice, totalAmountDue, totalCreditAmount };
	// }

	// /**
	// * Compose Payments
	// *
	// * @param pnrPaxCharges
	// * @return
	// */
	// private static Collection<ReservationTnx> composePayments(Collection<PnrChargesDTO> pnrPaxCharges) {
	// Collection<ReservationTnx> colReservationTnx = new ArrayList<ReservationTnx>();
	//
	// for (PnrChargesDTO pnrChargesDTO : pnrPaxCharges) {
	// colReservationTnx.addAll(pnrChargesDTO.getPaymentInfo());
	// }
	//
	// return colReservationTnx;
	// }

	/**
	 * Compose Credits
	 * 
	 * @param pnrPaxCharges
	 * @return
	 */
	// private static Collection<ReservationTnx> composeCredits(Collection<PnrChargesDTO> pnrPaxCharges) {
	// Collection<ReservationTnx> colReservationTnx = new ArrayList<ReservationTnx>();
	//
	// for (PnrChargesDTO pnrChargesDTO : pnrPaxCharges) {
	// colReservationTnx.addAll(pnrChargesDTO.getAvailableBalanceMetaAmounts());
	// }
	//
	// return colReservationTnx;
	// }

	public static boolean isRemoveInsurance(String alterationType, Reservation reservation) {
		if ((ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType) || ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE
					.equals(alterationType)) && reservation.getReservationInsurance() != null && !reservation.getReservationInsurance().isEmpty()) {
			for (ReservationInsurance reservationInsurance: reservation.getReservationInsurance()) {
				return ReservationInternalConstants.INSURANCE_STATES.OH.toString().equals(reservationInsurance.getState());
			}
		}
		return false;

	}

	/**
	 * Returns Balances for reservation alterations (Modifications/Cancellations/Additions) for specified OND(s).
	 * 
	 * @param res
	 * @param pnrPaxChargesMap2
	 * @param paxExtChargeMap
	 * @param quotedExtChgMap
	 * @param fareChargesAndSegmentDTOs
	 * @param type
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private static SegmentSummaryTOV2 getONDBalancesForAlt(Reservation res, Map<Integer, PnrChargesDTO> pnrPaxChargesMap,
			Collection<OndFareDTO> colOndFareDTOs, String alterationType, Date quotedDateTime,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap, float fareDiscountPercentage) throws ModuleException {

		SegmentSummaryTOV2 segmentSummaryTOV2 = new SegmentSummaryTOV2();

		BigDecimal identifiedTotCnx = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal identifiedTotMod = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal identifiedTotExtraFees = AccelAeroCalculator.getDefaultBigDecimalZero();

		// For existing OND
		Collection<ChargeMetaTO> currentActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> currentRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> currentNonRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();

		// For new OND
		Collection<ChargeMetaTO> newActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newNonRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();

		Collection<ChargeMetaTO> newActualOutBoundFlexiChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newActualInBoundFlexiChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

		Object[] faresAndCharges = null;
		Object[] outBoundFlexiCharges = null;
		Object[] inBoundFlexiCharges = null;

		boolean removeInsurance = isRemoveInsurance(alterationType, res);

		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_VOID_RES.equals(alterationType)) {

			for (Iterator<ReservationPax> paxIt = res.getPassengers().iterator(); paxIt.hasNext();) {
				ReservationPax pax = paxIt.next();
				
				if (ReservationApiUtils.isAdultType(pax) || ReservationApiUtils.isChildType(pax)
						|| (res.isInfantPaymentRecordedWithInfant() && ReservationApiUtils.isInfantType(pax))) {
					PnrChargesDTO paxOndCharges = pnrPaxChargesMap.get(pax.getPnrPaxId());
					if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)
							&& ReservationApiUtils.isInfantType(pax) && paxOndCharges == null) {
						continue;
					}
					if (!ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
						identifiedTotCnx = AccelAeroCalculator.add(identifiedTotCnx,
								paxOndCharges.getIdentifiedCancellationAmount()); // identified cnx charge for current
																					// operation
						identifiedTotMod = AccelAeroCalculator.add(identifiedTotMod,
								paxOndCharges.getIdentifiedModificationAmount());// identified mod charge for current
																					// modification
					} else {
						newActualChargeMetaAmounts.addAll(paxOndCharges.getActualChargeMetaAmounts());
						newRefundableMetaAmounts.addAll(paxOndCharges.getRefundableChargeMetaAmounts());
						newNonRefundableMetaAmounts.addAll(paxOndCharges.getNonRefundableChargeMetaAmounts());
					}

					currentActualChargeMetaAmounts.addAll(paxOndCharges.getActualChargeMetaAmounts());
					currentNonRefundableMetaAmounts.addAll(paxOndCharges.getNonRefundableChargeMetaAmounts());
					if (paxOndCharges.getPaymentInfo() != null && paxOndCharges.getPaymentInfo().size() > 0) {
						currentRefundableMetaAmounts.addAll(paxOndCharges.getRefundableChargeMetaAmounts());
					}

					identifiedTotExtraFees = AccelAeroCalculator.add(identifiedTotExtraFees,
							paxOndCharges.getIdentifiedExtraFeeAmount());

				}
			}
		}

		if (paxExtChargeMap != null) {
			for (Integer paxSeqId : paxExtChargeMap.keySet()) {
				if (paxExtChargeMap.containsKey(paxSeqId)) {
					for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(paxSeqId)) {
						ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
								ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null, null);
						newActualChargeMetaAmounts.add(anciMetaCharge);
					}
				}
			}
		}

		if (colOndFareDTOs != null) {
			faresAndCharges = getNewFaresAndCharges(colOndFareDTOs, quotedDateTime);
			outBoundFlexiCharges = getFlexiCharges(colOndFareDTOs, quotedDateTime, false);
			inBoundFlexiCharges = getFlexiCharges(colOndFareDTOs, quotedDateTime, true);
		}
		BigDecimal totalFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {

			Collection<ChargeMetaTO> adultMetaCharges = (Collection<ChargeMetaTO>) faresAndCharges[0];
			Collection<ChargeMetaTO> infantMetaCharges = (Collection<ChargeMetaTO>) faresAndCharges[1];
			Collection<ChargeMetaTO> childMetaCharges = (Collection<ChargeMetaTO>) faresAndCharges[2];

			Collection<ChargeMetaTO> adultOutBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
			Collection<ChargeMetaTO> infantOutBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
			Collection<ChargeMetaTO> childOutBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
			Collection<ChargeMetaTO> adultInBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
			Collection<ChargeMetaTO> infantInBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
			Collection<ChargeMetaTO> childInBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
			if (outBoundFlexiCharges != null) {
				adultOutBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) outBoundFlexiCharges[0];
				infantOutBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) outBoundFlexiCharges[1];
				childOutBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) outBoundFlexiCharges[2];
			}
			if (inBoundFlexiCharges != null) {
				adultInBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) inBoundFlexiCharges[0];
				infantInBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) inBoundFlexiCharges[1];
				childInBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) inBoundFlexiCharges[2];
			}

			// For Adults
			for (int i = 0; i < res.getTotalPaxAdultCount(); i++) {
				for (ChargeMetaTO chargeMetaTO : adultMetaCharges) {
					totalFareDiscount = AccelAeroCalculator.add(totalFareDiscount, AccelAeroCalculator.calculatePercentage(
							chargeMetaTO.getChargeAmount(), fareDiscountPercentage, RoundingMode.DOWN));
					newActualChargeMetaAmounts.add(chargeMetaTO.clone());
				}
				for (ChargeMetaTO chargeMetaTO : adultOutBoundFlexiMetaCharges) {
					newActualOutBoundFlexiChargeMetaAmounts.add(chargeMetaTO.clone());
				}
				for (ChargeMetaTO chargeMetaTO : adultInBoundFlexiMetaCharges) {
					newActualInBoundFlexiChargeMetaAmounts.add(chargeMetaTO.clone());
				}
			}

			// For infants
			for (int i = 0; i < res.getTotalPaxInfantCount(); i++) {
				for (ChargeMetaTO chargeMetaTO : infantMetaCharges) {
					// No Fare discounts for Children and infants
					// totalFareDiscount = AccelAeroCalculator.add(totalFareDiscount,
					// AccelAeroCalculator.calculatePercentage(
					// chargeMetaTO.getChargeAmount(), fareDiscountPercentage, RoundingMode.DOWN));
					newActualChargeMetaAmounts.add(chargeMetaTO.clone());
				}
				for (ChargeMetaTO chargeMetaTO : infantOutBoundFlexiMetaCharges) {
					newActualOutBoundFlexiChargeMetaAmounts.add(chargeMetaTO.clone());
				}
				for (ChargeMetaTO chargeMetaTO : infantInBoundFlexiMetaCharges) {
					newActualInBoundFlexiChargeMetaAmounts.add(chargeMetaTO.clone());
				}
			}

			// For Children
			for (int i = 0; i < res.getTotalPaxChildCount(); i++) {
				for (ChargeMetaTO chargeMetaTO : childMetaCharges) {
					// No Fare discounts for Children and infants
					// totalFareDiscount = AccelAeroCalculator.add(totalFareDiscount,
					// AccelAeroCalculator.calculatePercentage(
					// chargeMetaTO.getChargeAmount(), fareDiscountPercentage, RoundingMode.DOWN));
					newActualChargeMetaAmounts.add(chargeMetaTO.clone());
				}
				for (ChargeMetaTO chargeMetaTO : childOutBoundFlexiMetaCharges) {
					newActualOutBoundFlexiChargeMetaAmounts.add(chargeMetaTO.clone());
				}
				for (ChargeMetaTO chargeMetaTO : childInBoundFlexiMetaCharges) {
					newActualInBoundFlexiChargeMetaAmounts.add(chargeMetaTO.clone());
				}
			}

			// TODO - set refundable/non-refundable totals
		}

		if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			Collection<ChargeMetaTO> infantMetaCharges = (Collection<ChargeMetaTO>) faresAndCharges[1];
			for (ChargeMetaTO chargeMetaTO : infantMetaCharges) {
				newActualChargeMetaAmounts.add(chargeMetaTO.clone());
			}
		}
		BigDecimal newOutBoundFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newInBoundFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (ChargeMetaTO chargeMetaTO : newActualOutBoundFlexiChargeMetaAmounts) {
			newOutBoundFlexiCharge = AccelAeroCalculator.add(newOutBoundFlexiCharge, chargeMetaTO.getChargeAmount());
		}
		for (ChargeMetaTO chargeMetaTO : newActualInBoundFlexiChargeMetaAmounts) {
			newInBoundFlexiCharge = AccelAeroCalculator.add(newInBoundFlexiCharge, chargeMetaTO.getChargeAmount());
		}

		// Existing OND Fares & Charges
		segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(currentActualChargeMetaAmounts, null));
		segmentSummaryTOV2.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(currentNonRefundableMetaAmounts, null));
		segmentSummaryTOV2.setCurrentRefundableAmounts(SegmentSummaryTOV2.convert(currentRefundableMetaAmounts, null));

		// New OND Fare & Charges
		segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newActualChargeMetaAmounts, totalFareDiscount));
		segmentSummaryTOV2.setNewNonRefundableAmounts(SegmentSummaryTOV2.convert(newNonRefundableMetaAmounts, null));
		segmentSummaryTOV2.setNewRefundableAmounts(SegmentSummaryTOV2.convert(newRefundableMetaAmounts, null));
		segmentSummaryTOV2.setOutBoundFlexiCharge(newOutBoundFlexiCharge);
		segmentSummaryTOV2.setInBoundFlexiCharge(newInBoundFlexiCharge);

		// Identified total CNX/MOD charges
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
			segmentSummaryTOV2.setIdentifiedTotalCnxCharge(identifiedTotCnx);
		}
		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)) {
			segmentSummaryTOV2.setIdentifiedTotalModCharge(identifiedTotMod);
		}

		segmentSummaryTOV2.setIdentifiedExtraFeeAmount(identifiedTotExtraFees);

		return segmentSummaryTOV2;
	}

	@SuppressWarnings("unchecked")
	private static Collection<PAXSummaryTOV2> getPaxBalancesForAlt(Reservation reservation,
			Collection<OndFareDTO> fareChargesAndSegmentDTOs, Map<Integer, PnrChargesDTO> pnrPaxAllSegChargesMap,
			String alterationType, Date quotedDateTime, Map<Integer, PnrChargesDTO> pnrONDPaxChargesMap,
			boolean addCurrentModCharges, Collection<Integer> pnrSegIds,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap, float fareDiscountPercentage,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap) throws ModuleException {

		Map<Integer, PAXSummaryTOV2> newPaxSummaries = new HashMap<Integer, PAXSummaryTOV2>();
		Map<Integer, ReservationPax> reservationPaxMap = AirProxyReservationUtil.prepareReservationPaxMap(reservation);
		Collection<ChargeMetaTO> newAdultMetaCharges = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newInfantMetaCharges = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newChildMetaCharges = new ArrayList<ChargeMetaTO>();

		Collection<ChargeMetaTO> adultOutBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> infantOutBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> childOutBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();

		Collection<ChargeMetaTO> adultInBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> infantInBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> childInBoundFlexiMetaCharges = new ArrayList<ChargeMetaTO>();

		Object[] quotedFaresAndCharges = null;
		Object[] outBoundFlexiCharges = null;
		Object[] inBoundFlexiCharges = null;

		boolean removeInsurance = isRemoveInsurance(alterationType, reservation);

		if (fareChargesAndSegmentDTOs != null) {
			quotedFaresAndCharges = getNewFaresAndCharges(fareChargesAndSegmentDTOs, quotedDateTime);
			outBoundFlexiCharges = getFlexiCharges(fareChargesAndSegmentDTOs, quotedDateTime, false);
			inBoundFlexiCharges = getFlexiCharges(fareChargesAndSegmentDTOs, quotedDateTime, true);
			// This outbound inbound separation will be used for add segment
		}
		if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			newAdultMetaCharges = (Collection<ChargeMetaTO>) quotedFaresAndCharges[0];
			newInfantMetaCharges = (Collection<ChargeMetaTO>) quotedFaresAndCharges[1];
			newChildMetaCharges = (Collection<ChargeMetaTO>) quotedFaresAndCharges[2];

			if (outBoundFlexiCharges != null) {
				adultOutBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) outBoundFlexiCharges[0];
				infantOutBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) outBoundFlexiCharges[1];
				childOutBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) outBoundFlexiCharges[2];
			}
			if (inBoundFlexiCharges != null) {
				adultInBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) inBoundFlexiCharges[0];
				infantInBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) inBoundFlexiCharges[1];
				childInBoundFlexiMetaCharges = (Collection<ChargeMetaTO>) inBoundFlexiCharges[2];
			}
		}

		if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			newInfantMetaCharges = (Collection<ChargeMetaTO>) quotedFaresAndCharges[1];
		}

		SegmentSummaryTOV2 segmentSummaryTOV2;
		Collection<ChargeMetaTO> actualPaxChargeMetaAmounts;
		Collection<ChargeMetaTO> actualInfChargeMetaAmounts;
		Collection<ChargeMetaTO> refundablePaxChargeMetaAmounts;
		Collection<ChargeMetaTO> refundableInfChargeMetaAmounts;
		Collection<ChargeMetaTO> nonRefundablePaxChargeMetaAmounts;
		Collection<ChargeMetaTO> nonRefundableInfChargeMetaAmounts;
		Collection<ChargeMetaTO> newPaxChargeMetaAmounts;
		Collection<ChargeMetaTO> newInfChargeMetaAmounts;
		BigDecimal paxCancellationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal paxModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal infCancellationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal infModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal paxExtraFees = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal infExtraFees = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Current pax cnx and mod amount
		for (ReservationPax reservationPax : reservationPaxMap.values()) {
			PnrChargeDetailTO cnxChargeDetailTO = new PnrChargeDetailTO();
			PnrChargeDetailTO modChargeDetailTO = new PnrChargeDetailTO();
			PnrChargeDetailTO infCnxChargeDetailTO = new PnrChargeDetailTO();
			PnrChargeDetailTO infModChargeDetailTO = new PnrChargeDetailTO();
			Set<ReservationPaxFare> colResPaxFares = reservationPax.getPnrPaxFares();
			BigDecimal outBoundFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal inBoundFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal currentPayment = AccelAeroCalculator.subtract(reservationPax.getTotalAvailableBalance(),
					reservationPax.getTotalChargeAmount());
			BigDecimal currentCharge = reservationPax.getTotalChargeAmount();

			PnrChargesDTO pnrAllSegChargesDTO = pnrPaxAllSegChargesMap.get(reservationPax.getPnrPaxId());
			if (pnrAllSegChargesDTO == null) {
				pnrAllSegChargesDTO = new PnrChargesDTO();
			}
			if (!ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
				cnxChargeDetailTO = pnrAllSegChargesDTO.getCnxChargeDetailTO() != null ? pnrAllSegChargesDTO
						.getCnxChargeDetailTO() : null;
				modChargeDetailTO = pnrAllSegChargesDTO.getModChargeDetailTO() != null ? pnrAllSegChargesDTO
						.getModChargeDetailTO() : null;
			}

			if (ReservationApiUtils.isAdultType(reservationPax) || ReservationApiUtils.isChildType(reservationPax) || 
					(reservation.isInfantPaymentRecordedWithInfant() && ReservationApiUtils.isInfantType(reservationPax) && !ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType))) {

				actualPaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				actualInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				refundablePaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				refundableInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				nonRefundablePaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				nonRefundableInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

				newPaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				newInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

				Integer adultId = null;
				
				if(!reservation.isInfantPaymentRecordedWithInfant()){
					adultId = reservationPax.getAccompaniedPaxId();
				}

				paxExtraFees = pnrAllSegChargesDTO.getIdentifiedExtraFeeAmount();

				if (adultId != null) {
					PnrChargesDTO infPnrChargesDTO = pnrPaxAllSegChargesMap.get(adultId);
					infExtraFees = infPnrChargesDTO.getIdentifiedExtraFeeAmount();
				}

				// Deduct cancelled segments' total refundable amount
				if (!(ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType) || ReservationConstants.AlterationType.ALT_CANCEL_OND
							.equals(alterationType))) {
					// Parent
					if (adultId != null) {
						actualPaxChargeMetaAmounts.addAll(filterNonMatchingCharges(
								pnrAllSegChargesDTO.getActualChargeMetaAmounts(),
								ReservationInternalConstants.PassengerType.INFANT));

						PnrChargesDTO infPnrChargesDTO = pnrPaxAllSegChargesMap.get(adultId);
						actualInfChargeMetaAmounts.addAll(infPnrChargesDTO.getActualChargeMetaAmounts());
					} else {
						actualPaxChargeMetaAmounts.addAll(pnrAllSegChargesDTO.getActualChargeMetaAmounts());
					}
					if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
							|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)
							|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
						PnrChargesDTO pnrONDChargesDTO = pnrONDPaxChargesMap.get(reservationPax.getPnrPaxId());
						paxModificationCharge = pnrONDChargesDTO.getIdentifiedModificationAmount();

						if (adultId != null) {
							nonRefundablePaxChargeMetaAmounts.addAll(filterNonMatchingCharges(
									pnrONDChargesDTO.getNonRefundableChargeMetaAmounts(),
									ReservationInternalConstants.PassengerType.INFANT));

							PnrChargesDTO infPnrChargesDTO = pnrONDPaxChargesMap.get(adultId);

							nonRefundableInfChargeMetaAmounts.addAll(infPnrChargesDTO.getNonRefundableChargeMetaAmounts());
						} else {
							nonRefundablePaxChargeMetaAmounts.addAll(pnrONDChargesDTO.getNonRefundableChargeMetaAmounts());
						}
					} else if (!ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
						if (adultId != null) {
							if (!reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
								refundablePaxChargeMetaAmounts.addAll(filterNonMatchingCharges(
										pnrAllSegChargesDTO.getRefundableChargeMetaAmounts(),
										ReservationInternalConstants.PassengerType.INFANT));
							}
							nonRefundablePaxChargeMetaAmounts.addAll(filterNonMatchingCharges(
									pnrAllSegChargesDTO.getNonRefundableChargeMetaAmounts(),
									ReservationInternalConstants.PassengerType.INFANT));

							PnrChargesDTO infPnrChargesDTO = pnrPaxAllSegChargesMap.get(adultId);
							if (!reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
								refundableInfChargeMetaAmounts.addAll(infPnrChargesDTO.getRefundableChargeMetaAmounts());
							}
							nonRefundableInfChargeMetaAmounts.addAll(infPnrChargesDTO.getNonRefundableChargeMetaAmounts());
						} else {
							if (!reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
								refundablePaxChargeMetaAmounts.addAll(pnrAllSegChargesDTO.getRefundableChargeMetaAmounts());
							}
							nonRefundablePaxChargeMetaAmounts.addAll(pnrAllSegChargesDTO.getNonRefundableChargeMetaAmounts());
						}
					}
				}

				// Insurance amount to be deducted for add segment
				BigDecimal insAmtForAddOnd = AccelAeroCalculator.getDefaultBigDecimalZero();
				// Add cancellation or modification charges
				if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)) {

					paxCancellationCharge = pnrAllSegChargesDTO.getIdentifiedCancellationAmount();

					if (adultId != null) {
						PnrChargesDTO infPnrChargesDTO = pnrPaxAllSegChargesMap.get(adultId);
						infCancellationCharge = infPnrChargesDTO.getIdentifiedCancellationAmount();

						infCnxChargeDetailTO = infPnrChargesDTO.getCnxChargeDetailTO() != null ? infPnrChargesDTO
								.getCnxChargeDetailTO() : null;
						infModChargeDetailTO = infPnrChargesDTO.getCnxChargeDetailTO() != null ? infPnrChargesDTO
								.getCnxChargeDetailTO() : null;
					}

					if (pnrAllSegChargesDTO.getCnxChargeDetailTO() != null
							&& !pnrAllSegChargesDTO.getCnxChargeDetailTO().isCustomCharge()) {
						cnxChargeDetailTO = null;
						infCnxChargeDetailTO = null;
					}

					if (pnrAllSegChargesDTO.getModChargeDetailTO() != null
							&& !pnrAllSegChargesDTO.getModChargeDetailTO().isCustomCharge()) {
						modChargeDetailTO = null;
						infModChargeDetailTO = null;
					}

				} else if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)) {
					PnrChargesDTO pnrSegChargesDTO = pnrONDPaxChargesMap.get(reservationPax.getPnrPaxId());
					paxModificationCharge = pnrSegChargesDTO.getIdentifiedModificationAmount();

					if (adultId != null) {
						PnrChargesDTO infPnrChargesDTO = pnrONDPaxChargesMap.get(adultId);
						infModificationCharge = infPnrChargesDTO.getIdentifiedModificationAmount();
						infCnxChargeDetailTO = infPnrChargesDTO.getCnxChargeDetailTO() != null ? infPnrChargesDTO
								.getCnxChargeDetailTO() : null;
						infModChargeDetailTO = infPnrChargesDTO.getCnxChargeDetailTO() != null ? infPnrChargesDTO
								.getCnxChargeDetailTO() : null;
					}

					cnxChargeDetailTO = pnrSegChargesDTO.getCnxChargeDetailTO() != null
							? pnrSegChargesDTO.getCnxChargeDetailTO()
							: null;
					modChargeDetailTO = pnrSegChargesDTO.getModChargeDetailTO() != null
							? pnrSegChargesDTO.getModChargeDetailTO()
							: null;

					fillnewChagesMetaData(pnrAllSegChargesDTO, pnrSegChargesDTO, colResPaxFares,
							(ArrayList<ChargeMetaTO>) newPaxChargeMetaAmounts,
							(ArrayList<ChargeMetaTO>) refundablePaxChargeMetaAmounts,
							(ArrayList<ChargeMetaTO>) actualPaxChargeMetaAmounts, pnrSegIds,
							reservationPax.getAccompaniedPaxId(), reservation, currentPayment, currentCharge, removeInsurance,
							quotedExtChgMap);
				} else if (ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
					PnrChargesDTO pnrSegChargesDTO = pnrONDPaxChargesMap.get(reservationPax.getPnrPaxId());
					paxCancellationCharge = pnrSegChargesDTO.getIdentifiedCancellationAmount();

					if (adultId != null) {
						PnrChargesDTO infPnrChargesDTO = pnrONDPaxChargesMap.get(adultId);
						infCancellationCharge = infPnrChargesDTO.getIdentifiedCancellationAmount();
						infCnxChargeDetailTO = infPnrChargesDTO.getCnxChargeDetailTO() != null ? infPnrChargesDTO
								.getCnxChargeDetailTO() : null;
						infModChargeDetailTO = infPnrChargesDTO.getCnxChargeDetailTO() != null ? infPnrChargesDTO
								.getCnxChargeDetailTO() : null;
					}

					cnxChargeDetailTO = pnrSegChargesDTO.getCnxChargeDetailTO() != null
							? pnrSegChargesDTO.getCnxChargeDetailTO()
							: null;
					modChargeDetailTO = pnrSegChargesDTO.getModChargeDetailTO() != null
							? pnrSegChargesDTO.getModChargeDetailTO()
							: null;

					fillnewChagesMetaData(pnrAllSegChargesDTO, pnrSegChargesDTO, colResPaxFares,
							(ArrayList<ChargeMetaTO>) newPaxChargeMetaAmounts,
							(ArrayList<ChargeMetaTO>) refundablePaxChargeMetaAmounts,
							(ArrayList<ChargeMetaTO>) actualPaxChargeMetaAmounts, pnrSegIds,
							reservationPax.getAccompaniedPaxId(), reservation, currentPayment, currentCharge, removeInsurance,
							quotedExtChgMap);
				} else if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType) && removeInsurance) {
					Integer insChgRateId = quotedExtChgMap.get(EXTERNAL_CHARGES.INSURANCE).getChgRateId();
					for (ChargeMetaTO chgMetaTO : pnrAllSegChargesDTO.getRefundableChargeMetaAmounts()) {
						if (chgMetaTO.getChargeRateId() != null
								&& chgMetaTO.getChargeRateId().intValue() == insChgRateId.intValue()) {
							insAmtForAddOnd = AccelAeroCalculator.add(insAmtForAddOnd, chgMetaTO.getChargeAmount());
						}
					}
				}

				BigDecimal totalPaxFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totalInfFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
				// Add new quoted segments' prices
				if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
					Iterator<ChargeMetaTO> it = null;
					if (ReservationApiUtils.isAdultType(reservationPax)) {
						newPaxChargeMetaAmounts.addAll(newAdultMetaCharges);
						it = newAdultMetaCharges.iterator();
					} else if (ReservationApiUtils.isChildType(reservationPax)) {
						newPaxChargeMetaAmounts.addAll(newChildMetaCharges);
						it = newChildMetaCharges.iterator();
					}

					// Fare Discount will be applied only for adults AARESAA-8588
					if (ReservationApiUtils.isAdultType(reservationPax)) {
						while (it != null && it.hasNext()) {
							ChargeMetaTO chargeTO = it.next();
							if (ReservationInternalConstants.ChargeGroup.FAR.equals(chargeTO.getChargeGroupCode())) {
								totalPaxFareDiscount = AccelAeroCalculator.add(totalPaxFareDiscount, AccelAeroCalculator
										.calculatePercentage(chargeTO.getChargeAmount(), fareDiscountPercentage,
												RoundingMode.DOWN));
							}
						}
					}

					if (paxExtChargeMap != null && paxExtChargeMap.containsKey(reservationPax.getPaxSequence())) {
						for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(reservationPax.getPaxSequence())) {
							ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
									ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null,
									null);
							newPaxChargeMetaAmounts.add(anciMetaCharge);
						}
					}

					if (adultId != null) {
						newInfChargeMetaAmounts.addAll(newInfantMetaCharges);
						/*
						 * it = newInfantMetaCharges.iterator(); while (it != null && it.hasNext()) { ChargeMetaTO
						 * chargeTO = it.next(); if
						 * (ReservationInternalConstants.ChargeGroup.FAR.equals(chargeTO.getChargeGroupCode())) {
						 * totalPaxFareDiscount = AccelAeroCalculator.add(totalPaxFareDiscount, AccelAeroCalculator
						 * .calculatePercentage(chargeTO.getChargeAmount(), fareDiscountPercentage, RoundingMode.DOWN));
						 * 
						 * // No Fare discounts for Children and infants //totalInfFareDiscount =
						 * AccelAeroCalculator.add(totalInfFareDiscount, AccelAeroCalculator //
						 * .calculatePercentage(chargeTO.getChargeAmount(), fareDiscountPercentage, //
						 * RoundingMode.DOWN)); } }
						 */
					}
				} else if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)) {
					if (paxExtChargeMap != null && paxExtChargeMap.containsKey(reservationPax.getPaxSequence())) {
						for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(reservationPax.getPaxSequence())) {
							ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
									ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null,
									null);
							newPaxChargeMetaAmounts.add(anciMetaCharge);
						}
					}
				}

				// Adding extra fee
				if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)) {
					PnrChargesDTO pnrSegChargesDTO = pnrONDPaxChargesMap.get(reservationPax.getPnrPaxId());
					if (pnrSegChargesDTO.getExtraFeeChargeMetaAmounts() != null) {
						for (ChargeMetaTO chargeMetaTO : pnrSegChargesDTO.getExtraFeeChargeMetaAmounts()) {
							newPaxChargeMetaAmounts.add(chargeMetaTO);
						}
					}
				}

				// Add new infants prices
				if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
					BigDecimal[] addInfRefund = new BigDecimal[0]; // no refundable calculations needed for add infant
					pnrAllSegChargesDTO.setRefundableAmounts(addInfRefund);
					if (reservationPax.getInfants() != null && !reservationPax.getInfants().isEmpty()) {
						Set<ReservationPax> setInfants = reservationPax.getInfants();
						for (ReservationPax adInf : setInfants) {
							if (adInf.getPnrPaxId() == null) {								
								if (paxExtChargeMap != null && paxExtChargeMap.containsKey(reservationPax.getPaxSequence())) {
									for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(reservationPax.getPaxSequence())) {
										ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
												ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(),
												quotedDateTime, null, null);
										newInfantMetaCharges.add(anciMetaCharge);
									}
								}								
								newInfChargeMetaAmounts.addAll(newInfantMetaCharges);
								for (ChargeMetaTO chargeMetaTO : infantOutBoundFlexiMetaCharges) {
									outBoundFlexiCharge = AccelAeroCalculator.add(outBoundFlexiCharge,
											chargeMetaTO.getChargeAmount());
								}
								for (ChargeMetaTO chargeMetaTO : infantInBoundFlexiMetaCharges) {
									inBoundFlexiCharge = AccelAeroCalculator.add(inBoundFlexiCharge,
											chargeMetaTO.getChargeAmount());
								}
							}
						}
					}
				}

				if (ReservationApiUtils.isAdultType(reservationPax)) {
					for (ChargeMetaTO chargeMetaTO : adultOutBoundFlexiMetaCharges) {
						outBoundFlexiCharge = AccelAeroCalculator.add(outBoundFlexiCharge, chargeMetaTO.getChargeAmount());
					}
					for (ChargeMetaTO chargeMetaTO : adultInBoundFlexiMetaCharges) {
						inBoundFlexiCharge = AccelAeroCalculator.add(inBoundFlexiCharge, chargeMetaTO.getChargeAmount());
					}
				} else if (ReservationApiUtils.isChildType(reservationPax)) {
					for (ChargeMetaTO chargeMetaTO : childOutBoundFlexiMetaCharges) {
						outBoundFlexiCharge = AccelAeroCalculator.add(outBoundFlexiCharge, chargeMetaTO.getChargeAmount());
					}
					for (ChargeMetaTO chargeMetaTO : childInBoundFlexiMetaCharges) {
						inBoundFlexiCharge = AccelAeroCalculator.add(inBoundFlexiCharge, chargeMetaTO.getChargeAmount());
					}
				}

				// sets the pax segs details
				segmentSummaryTOV2 = new SegmentSummaryTOV2();
				segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(actualPaxChargeMetaAmounts, null));

				Collection<ChargeMetaTO> allNonRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
				allNonRefundableMetaAmounts.addAll(nonRefundablePaxChargeMetaAmounts);
				allNonRefundableMetaAmounts.addAll(nonRefundableInfChargeMetaAmounts);

				Collection<ChargeMetaTO> allRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
				allRefundableMetaAmounts.addAll(refundablePaxChargeMetaAmounts);
				allRefundableMetaAmounts.addAll(refundableInfChargeMetaAmounts);

				segmentSummaryTOV2.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(allNonRefundableMetaAmounts, null));
				segmentSummaryTOV2.setCurrentRefundableAmounts(SegmentSummaryTOV2.convert(allRefundableMetaAmounts, null));

				segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newPaxChargeMetaAmounts, totalPaxFareDiscount));
				segmentSummaryTOV2.setIdentifiedTotalCnxCharge(paxCancellationCharge);
				segmentSummaryTOV2.setIdentifiedTotalModCharge(paxModificationCharge);
				segmentSummaryTOV2.setIdentifiedExtraFeeAmount(paxExtraFees);
				segmentSummaryTOV2.setOutBoundFlexiCharge(outBoundFlexiCharge);
				segmentSummaryTOV2.setInBoundFlexiCharge(inBoundFlexiCharge);

				PAXSummaryTOV2 paxSummaryTOV2 = new PAXSummaryTOV2();
				paxSummaryTOV2.setColCredits(new ArrayList<ReservationTnx>());
				if (pnrAllSegChargesDTO != null && pnrAllSegChargesDTO.getPaymentInfo() != null
						&& pnrAllSegChargesDTO.getPaymentInfo().size() > 0) {
					paxSummaryTOV2.setColPayments(pnrAllSegChargesDTO.getPaymentInfo());
				} else {
					Collection<ReservationTnx> tempPayments;
					try {
						ArrayList<Integer> pnrPaxIdList = new ArrayList<Integer>();
						pnrPaxIdList.add(reservationPax.getPnrPaxId());
						Map<Integer, Collection<ReservationTnx>> mapPayments = AirproxyModuleUtils.getPassengerBD()
								.getPNRPaxPaymentsAndRefunds(pnrPaxIdList);
						tempPayments = mapPayments.get(reservationPax.getPnrPaxId());
						if (tempPayments == null) {
							tempPayments = new ArrayList<ReservationTnx>();
						}
					} catch (Exception ex) {
						tempPayments = new ArrayList<ReservationTnx>();
					}
					paxSummaryTOV2.setColPayments(tempPayments);
				}

				paxSummaryTOV2.setPnrPaxId(reservationPax.getPnrPaxId());
				paxSummaryTOV2.setPaxName(ReservationApiUtils.getPassengerName("", reservationPax.getTitle(),
						reservationPax.getFirstName(), reservationPax.getLastName(), false));
				paxSummaryTOV2.setPaxType(reservationPax.getPaxType());
				paxSummaryTOV2.setInfantId(adultId);
				paxSummaryTOV2.setSegmentSummaryTOV2(segmentSummaryTOV2);
				paxSummaryTOV2.setTotalCnxChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalCnxCharge());
				paxSummaryTOV2.setTotalModChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalModCharge());
				paxSummaryTOV2.setCnxChargeDetailTO(cnxChargeDetailTO);
				paxSummaryTOV2.setModChargeDetailTO(modChargeDetailTO);
				BigDecimal totalPaxNewPrice;
				BigDecimal newPaxBalanceAmount;
				// calculate the pax amounts
				if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
					BigDecimal resBalance = BigDecimal.ZERO;
					resBalance = pnrAllSegChargesDTO.getAvailableBalance().negate();

					if (resBalance.intValue() == 0) {
						Collection<ReservationTnx> tempBalances;
						try {
							ArrayList<Integer> pnrPaxIdList = new ArrayList<Integer>();
							pnrPaxIdList.add(reservationPax.getPnrPaxId());
							Map<Integer, Collection<ReservationTnx>> mapBalances = AirproxyModuleUtils.getPassengerBD()
									.getAllBalanceTransactions(pnrPaxIdList);
							tempBalances = mapBalances.get(reservationPax.getPnrPaxId());
							if (tempBalances == null) {
								tempBalances = new ArrayList<ReservationTnx>();
							}
						} catch (Exception ex) {
							tempBalances = new ArrayList<ReservationTnx>();
						}

						for (ReservationTnx reservationTnx : tempBalances) {
							resBalance = AccelAeroCalculator.add(resBalance, reservationTnx.getAmount());
						}
						resBalance = resBalance.negate();
					}
					FareInfoTOV2 infFareInfo = SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, null);
					newPaxBalanceAmount = AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalPrice(),
							infFareInfo.getTotalPrice(), resBalance.negate(), insAmtForAddOnd.negate());

					totalPaxNewPrice = AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalPrice(),
							infFareInfo.getTotalPrice(), currentCharge, insAmtForAddOnd.negate());

					// deducting fare discount amounts
					// totalPaxFareDiscount = AccelAeroCalculator.add(totalPaxFareDiscount,
					// segmentSummaryTOV2.getNewAmounts()
					// .getTotalFareDiscount(), infFareInfo.getTotalFareDiscount());

				} else if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
					FareInfoTOV2 infFareInfo = SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, null);
					totalPaxNewPrice = AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalPrice(),
							infFareInfo.getTotalPrice(), segmentSummaryTOV2.getIdentifiedTotalCnxCharge(),
							segmentSummaryTOV2.getIdentifiedTotalModCharge(),segmentSummaryTOV2.getIdentifiedExtraFeeAmount());
					BigDecimal currentPaxPayments = AccelAeroCalculator.subtract(reservationPax.getTotalAvailableBalance(),
							currentCharge);
					newPaxBalanceAmount = AccelAeroCalculator.add(totalPaxNewPrice, currentPaxPayments);

					// deducting fare discount amounts
					// totalPaxFareDiscount = AccelAeroCalculator.add(totalPaxFareDiscount,
					// segmentSummaryTOV2.getNewAmounts()
					// .getTotalFareDiscount(), infFareInfo.getTotalFareDiscount());

				} else {
					BigDecimal resBalance = BigDecimal.ZERO;
					BigDecimal paxRefundables = AccelAeroCalculator.add(pnrAllSegChargesDTO.getRefundableAmounts());
					if (!reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
						resBalance = pnrAllSegChargesDTO.getAvailableBalance().negate();

					}
					FareInfoTOV2 infFareInfo = SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, null);
					totalPaxNewPrice = AccelAeroCalculator.add((AccelAeroCalculator.subtract(
							AccelAeroCalculator.add(pnrAllSegChargesDTO.getActualAmounts()), paxRefundables)), // current
																												// charges
							segmentSummaryTOV2.getNewAmounts().getTotalPrice(), infFareInfo.getTotalPrice(), segmentSummaryTOV2
									.getIdentifiedTotalCnxCharge(), segmentSummaryTOV2.getIdentifiedTotalModCharge(), segmentSummaryTOV2.getIdentifiedExtraFeeAmount());
					// deducting fare discount amounts
					// totalPaxFareDiscount = AccelAeroCalculator.add(totalPaxFareDiscount,
					// segmentSummaryTOV2.getNewAmounts()
					// .getTotalFareDiscount(), infFareInfo.getTotalFareDiscount());

					if (!reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
						/*
						 * adding current mod cnx and adj charges
						 */
						newPaxBalanceAmount = AccelAeroCalculator.subtract(totalPaxNewPrice, AccelAeroCalculator.add(
								AccelAeroCalculator.add(pnrAllSegChargesDTO.getActualAmounts()), resBalance));
					} else {
						newPaxBalanceAmount = totalPaxNewPrice;
					}

				}

				BigDecimal totalAmountDue = BigDecimal.ZERO;
				if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
					totalAmountDue = AccelAeroCalculator.add(newPaxBalanceAmount, totalPaxFareDiscount.negate());
				} else {
					totalAmountDue = AccelAeroCalculator.add(newPaxBalanceAmount, totalPaxFareDiscount.negate()).doubleValue() > 0
							? AccelAeroCalculator.add(newPaxBalanceAmount, totalPaxFareDiscount.negate())
							: AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				BigDecimal totalCreditAmount = newPaxBalanceAmount.doubleValue() < 0
						? newPaxBalanceAmount.negate()
						: AccelAeroCalculator.getDefaultBigDecimalZero();

				paxSummaryTOV2.setTotalAmountDue(totalAmountDue);
				paxSummaryTOV2.setTotalFareDiscountAmount(totalPaxFareDiscount);
				paxSummaryTOV2.setTotalCreditAmount(totalCreditAmount);
				paxSummaryTOV2.setTotalPrice(totalPaxNewPrice);

				newPaxSummaries.put(reservationPax.getPnrPaxId(), paxSummaryTOV2);

				if (adultId != null) {
					ReservationPax infant = (ReservationPax) BeanUtils.getFirstElement(reservationPax.getInfants());

					if (pnrONDPaxChargesMap != null) {
						PnrChargesDTO pnrSegChargesDTO = pnrONDPaxChargesMap.get(infant.getPnrPaxId());
						infCnxChargeDetailTO = pnrSegChargesDTO.getCnxChargeDetailTO() != null ? pnrSegChargesDTO
								.getCnxChargeDetailTO() : infCnxChargeDetailTO;
						infModChargeDetailTO = pnrSegChargesDTO.getModChargeDetailTO() != null ? pnrSegChargesDTO
								.getModChargeDetailTO() : infModChargeDetailTO;
					}

					paxSummaryTOV2 = new PAXSummaryTOV2();
					paxSummaryTOV2.setColCredits(new ArrayList<ReservationTnx>());
					paxSummaryTOV2.setPnrPaxId(infant.getPnrPaxId());
					paxSummaryTOV2.setPaxName(ReservationApiUtils.getPassengerName("", infant.getTitle(), infant.getFirstName(),
							infant.getLastName(), false));
					paxSummaryTOV2.setPaxType(infant.getPaxType());

					segmentSummaryTOV2 = new SegmentSummaryTOV2();

					segmentSummaryTOV2.setIdentifiedTotalCnxCharge(infCancellationCharge);
					segmentSummaryTOV2.setIdentifiedTotalModCharge(infModificationCharge);
					segmentSummaryTOV2.setIdentifiedExtraFeeAmount(infExtraFees);

					segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(actualInfChargeMetaAmounts, null));
					segmentSummaryTOV2.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(
							nonRefundableInfChargeMetaAmounts, null));
					segmentSummaryTOV2.setCurrentRefundableAmounts(SegmentSummaryTOV2.convert(refundableInfChargeMetaAmounts,
							null));
					segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, totalInfFareDiscount));
					paxSummaryTOV2.setSegmentSummaryTOV2(segmentSummaryTOV2);

					paxSummaryTOV2.setTotalCnxChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalCnxCharge());
					paxSummaryTOV2.setTotalModChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalModCharge());

					PnrChargesDTO infPnrChargesDTO = pnrPaxAllSegChargesMap.get(adultId);
					if (infPnrChargesDTO == null) {
						infPnrChargesDTO = new PnrChargesDTO();
					}
					paxSummaryTOV2.setColPayments(infPnrChargesDTO.getPaymentInfo());
					BigDecimal totalInfNewPrice;
					BigDecimal newInFBalanceAmount;

					if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
						totalInfNewPrice = segmentSummaryTOV2.getNewAmounts().getTotalPrice();
						newInFBalanceAmount = totalInfNewPrice;
					} else {
						totalInfNewPrice = AccelAeroCalculator.add((AccelAeroCalculator.subtract(
								AccelAeroCalculator.add(infPnrChargesDTO.getActualAmounts()),
								AccelAeroCalculator.add(infPnrChargesDTO.getRefundableAmounts()))), // current charges
								segmentSummaryTOV2.getNewAmounts().getTotalPrice(), segmentSummaryTOV2
										.getIdentifiedTotalCnxCharge(), segmentSummaryTOV2.getIdentifiedTotalModCharge(), segmentSummaryTOV2.getIdentifiedExtraFeeAmount());

						newInFBalanceAmount = AccelAeroCalculator.subtract(totalInfNewPrice,
								AccelAeroCalculator.add(infPnrChargesDTO.getActualAmounts()));

					}
					BigDecimal totalInfAmountDue = AccelAeroCalculator.add(newInFBalanceAmount, totalInfFareDiscount.negate())
							.doubleValue() > 0
							? AccelAeroCalculator.add(newInFBalanceAmount, totalInfFareDiscount.negate())
							: AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal totalInfCreditAmount = newInFBalanceAmount.doubleValue() < 0
							? newInFBalanceAmount.negate()
							: AccelAeroCalculator.getDefaultBigDecimalZero();

					paxSummaryTOV2.setTotalAmountDue(totalInfAmountDue);
					paxSummaryTOV2.setTotalFareDiscountAmount(totalInfFareDiscount);
					paxSummaryTOV2.setTotalCreditAmount(totalInfCreditAmount);
					paxSummaryTOV2.setTotalPrice(totalInfNewPrice);
					paxSummaryTOV2.setCnxChargeDetailTO(infCnxChargeDetailTO);
					paxSummaryTOV2.setModChargeDetailTO(infModChargeDetailTO);

					newPaxSummaries.put(infant.getPnrPaxId(), paxSummaryTOV2);
				}
			}
		}
		return newPaxSummaries.values();
	}

	/**
	 * Fill the new amount using all the segments
	 * 
	 * @param allChargesDTO
	 *            has all charges when cancelling
	 * @param pnrONDChargesDTO
	 *            has all the charges when modifying
	 * @param newPaxChargeMetaAmounts
	 *            the filling charges array.
	 * @param removeInsurance
	 * @param quotedExtChgMap
	 */
	private static void fillnewChagesMetaData(PnrChargesDTO allChargesDTO, PnrChargesDTO pnrONDChargesDTO,
			Set<ReservationPaxFare> colResPaxFares, ArrayList<ChargeMetaTO> newPaxChargeMetaAmounts,
			ArrayList<ChargeMetaTO> refundablePaxChargeMetaAmounts, ArrayList<ChargeMetaTO> actualPaxChargeMetaAmounts,
			Collection<Integer> pnrSegIds, Integer infantId, Reservation reservation, BigDecimal currentPayment,
			BigDecimal totPaxCharges, boolean removeInsurance, Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap) {

		BigDecimal newFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotalCarryFoward = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal fareRefund = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal chargeRefund = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal canceledTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newRefund = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newNonRefund = AccelAeroCalculator.getDefaultBigDecimalZero();

		Integer insChgRateId = null;
		if (removeInsurance) {
			insChgRateId = quotedExtChgMap.get(EXTERNAL_CHARGES.INSURANCE).getChgRateId();
		}

		for (Iterator iterator = colResPaxFares.iterator(); iterator.hasNext();) {
			ReservationPaxFare reservationPaxFare = (ReservationPaxFare) iterator.next();
			ReservationPaxFare reservationInfantPaxFare = null;

			if (infantId != null) {
				Collection colValidPNRSegmentIds = getPNRSegmentIds(reservationPaxFare.getPaxFareSegments());
				Integer infantPnrPaxId = infantId;
				reservationInfantPaxFare = getReservationPaxFare(reservation, colValidPNRSegmentIds, infantPnrPaxId);
			}

			newFare = AccelAeroCalculator.add(newFare, reservationPaxFare.getTotalFare());
			newCharge = AccelAeroCalculator.add(newCharge, reservationPaxFare.getTotalTaxCharge(),
					reservationPaxFare.getTotalSurCharge());
			newCharge = AccelAeroCalculator.add(newCharge, reservationPaxFare.getTotalAdjustmentCharge(), reservationPaxFare
					.getTotalDiscount().negate());
			newCharge = AccelAeroCalculator.add(newCharge, reservationPaxFare.getTotalCancelCharge(),
					reservationPaxFare.getTotalModificationCharge());

			// for a parent add infant amounts
			if (reservationInfantPaxFare != null) {
				newFare = AccelAeroCalculator.add(newFare, reservationInfantPaxFare.getTotalFare());
				newCharge = AccelAeroCalculator.add(newCharge, reservationInfantPaxFare.getTotalTaxCharge(),
						reservationInfantPaxFare.getTotalSurCharge());
				newCharge = AccelAeroCalculator.add(newCharge, reservationInfantPaxFare.getTotalAdjustmentCharge(),
						reservationInfantPaxFare.getTotalDiscount().negate());
				newCharge = AccelAeroCalculator.add(newCharge, reservationInfantPaxFare.getTotalCancelCharge(),
						reservationInfantPaxFare.getTotalModificationCharge());
			}

			// for the modified segment
			if (checkONDSegments(reservationPaxFare, pnrSegIds)) {
				fareRefund = pnrONDChargesDTO.getRefundableFare().negate();
				BigDecimal refundableChg = pnrONDChargesDTO.getRefundableCharge().negate();
				chargeRefund = AccelAeroCalculator.add(chargeRefund, refundableChg);

				newRefund = AccelAeroCalculator.add(newRefund, fareRefund, chargeRefund);

				/*
				 * -ve compensating for the pro-rated value not being included in the
				 * reservationPaxFare.getTotalChargeAmount()
				 */
				BigDecimal fareProRateDiff = BigDecimal.ZERO;
				if (pnrONDChargesDTO.getRefundableFare().compareTo(reservationPaxFare.getTotalFare()) != 0) {
					// calculating the difference of total fare and pro-rated total fare per pax, per ond
					fareProRateDiff = AccelAeroCalculator.subtract(reservationPaxFare.getTotalFare(),
							pnrONDChargesDTO.getRefundableFare());
				}
				// canceledTotal = reservationPaxFare.getTotalChargeAmount();
				canceledTotal = AccelAeroCalculator.add(canceledTotal, reservationPaxFare.getTotalChargeAmount(),
						fareProRateDiff.negate());

				if (reservationInfantPaxFare != null) {
					// TotalChargeAmount includes fare and charges
					canceledTotal = AccelAeroCalculator.add(canceledTotal, reservationInfantPaxFare.getTotalChargeAmount());
				}
			} else if (removeInsurance) {
				// IF there is insurance that is removable in non modifying segments, remove them

				BigDecimal refInsAmt = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (ChargeMetaTO chgMetaTO : pnrONDChargesDTO.getRefundableChargeMetaAmounts()) {
					if (chgMetaTO.getChargeRateId() != null && chgMetaTO.getChargeRateId().intValue() == insChgRateId.intValue()) {
						refInsAmt = AccelAeroCalculator.add(refInsAmt, chgMetaTO.getChargeAmount());
					}
				}
				refInsAmt = refInsAmt.negate();
				chargeRefund = AccelAeroCalculator.add(chargeRefund, refInsAmt);
				newRefund = AccelAeroCalculator.add(newRefund, chargeRefund);
			}
		}
		newFare = AccelAeroCalculator.add(newFare, fareRefund);
		newCharge = AccelAeroCalculator.add(newCharge, chargeRefund);
		newNonRefund = AccelAeroCalculator.add(canceledTotal, newRefund);

		// Add the new Fare
		ChargeMetaTO currFareAmount = new ChargeMetaTO();
		currFareAmount.setChargeDate(new Date());
		currFareAmount.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.FAR);

		ChargeMetaTO currTaxAmount = new ChargeMetaTO();
		currTaxAmount.setChargeDate(new Date());
		currTaxAmount.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.TAX);

		ChargeMetaTO currSurAmount = new ChargeMetaTO();
		currSurAmount.setChargeDate(new Date());
		currSurAmount.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);

		currFareAmount.setChargeAmount(newFare);
		currSurAmount.setChargeAmount(newCharge);

		newPaxChargeMetaAmounts.add(currFareAmount);
		newPaxChargeMetaAmounts.add(currSurAmount);

		newRefund = (Math.abs(newRefund.doubleValue()) < Math.abs(currentPayment.doubleValue())) ? newRefund : currentPayment;
		newTotalCarryFoward = AccelAeroCalculator.add(currentPayment, newNonRefund);

		if (newTotalCarryFoward.doubleValue() > 0) {
			newTotalCarryFoward = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		// New Refunds
		ChargeMetaTO currRefundAmount = new ChargeMetaTO();
		currRefundAmount.setChargeDate(new Date());
		currRefundAmount.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.ADJ);

		currRefundAmount.setChargeAmount(newTotalCarryFoward.negate());
		refundablePaxChargeMetaAmounts.add(currRefundAmount);

		// setting actual Charges for Mod/CNX
		ChargeMetaTO currPaxChargeAmount = new ChargeMetaTO();
		currPaxChargeAmount.setChargeDate(new Date());
		currPaxChargeAmount.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.FAR);

		currPaxChargeAmount.setChargeAmount(totPaxCharges);
		actualPaxChargeMetaAmounts.add(currPaxChargeAmount);

	}

	private static Object[] getNewFaresAndCharges(Collection<OndFareDTO> fareChargesAndSegmentDTOs, Date chargeDate) {
		Collection<ChargeMetaTO> newAdultActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newChildActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newInfantActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

		for (Iterator<OndFareDTO> fareChargesAndSegmentDTOsIt = fareChargesAndSegmentDTOs.iterator(); fareChargesAndSegmentDTOsIt
				.hasNext();) {
			OndFareDTO fareChargesAndSegmentDTO = fareChargesAndSegmentDTOsIt.next();

			if (ReservationApiUtils.isFareExist(fareChargesAndSegmentDTO.getAdultFare())) {
				BigDecimal currentAdultFare = AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO.getAdultFare());
				newAdultActualChargeMetaAmounts.add(ChargeMetaTO.createBasicChargeMetaTO(
						ReservationInternalConstants.ChargeGroup.FAR, currentAdultFare, chargeDate, null, null));
			}

			if (ReservationApiUtils.isFareExist(fareChargesAndSegmentDTO.getInfantFare())) {
				BigDecimal currentInfantFare = AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO.getInfantFare());
				newInfantActualChargeMetaAmounts.add(ChargeMetaTO.createBasicChargeMetaTO(
						ReservationInternalConstants.ChargeGroup.FAR, currentInfantFare, chargeDate, null, null));
			}

			if (ReservationApiUtils.isFareExist(fareChargesAndSegmentDTO.getChildFare())) {
				BigDecimal currentChildFare = AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO.getChildFare());
				newChildActualChargeMetaAmounts.add(ChargeMetaTO.createBasicChargeMetaTO(
						ReservationInternalConstants.ChargeGroup.FAR, currentChildFare, chargeDate, null, null));
			}

			BigDecimal[] taxes = AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO.getTotalCharges(ChargeGroups.TAX));
			createMetaCharge(ReservationInternalConstants.ChargeGroup.TAX, taxes[0], chargeDate, newAdultActualChargeMetaAmounts);
			createMetaCharge(ReservationInternalConstants.ChargeGroup.TAX, taxes[1], chargeDate, newInfantActualChargeMetaAmounts);
			createMetaCharge(ReservationInternalConstants.ChargeGroup.TAX, taxes[2], chargeDate, newChildActualChargeMetaAmounts);

			BigDecimal[] surcharges = AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO
					.getTotalCharges(ChargeGroups.SURCHARGE));

			createMetaCharge(ReservationInternalConstants.ChargeGroup.SUR, surcharges[0], chargeDate,
					newAdultActualChargeMetaAmounts);
			createMetaCharge(ReservationInternalConstants.ChargeGroup.SUR, surcharges[1], chargeDate,
					newInfantActualChargeMetaAmounts);
			createMetaCharge(ReservationInternalConstants.ChargeGroup.SUR, surcharges[2], chargeDate,
					newChildActualChargeMetaAmounts);

			BigDecimal[] infantSurcharges = AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO
					.getTotalCharges(ChargeGroups.INFANT_SURCHARGE));
			createMetaCharge(ReservationInternalConstants.ChargeGroup.SUR, infantSurcharges[1], chargeDate,
					newInfantActualChargeMetaAmounts);
		}

		return new Object[] { newAdultActualChargeMetaAmounts, newInfantActualChargeMetaAmounts, newChildActualChargeMetaAmounts };
	}

	private static Object[] getFlexiCharges(Collection<OndFareDTO> fareChargesAndSegmentDTOs, Date chargeDate, boolean isInBound) {
		Collection<ChargeMetaTO> newAdultActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newChildActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newInfantActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

		for (Iterator<OndFareDTO> fareChargesAndSegmentDTOsIt = fareChargesAndSegmentDTOs.iterator(); fareChargesAndSegmentDTOsIt
				.hasNext();) {
			OndFareDTO fareChargesAndSegmentDTO = fareChargesAndSegmentDTOsIt.next();
			if ((fareChargesAndSegmentDTO.isInBoundOnd() && isInBound)
					|| (!fareChargesAndSegmentDTO.isInBoundOnd() && !isInBound)) {
				if (fareChargesAndSegmentDTO.getAllFlexiCharges() != null
						&& fareChargesAndSegmentDTO.getAllFlexiCharges().size() > 0) {
					for (Iterator<FlexiRuleDTO> iterator = fareChargesAndSegmentDTO.getAllFlexiCharges().iterator(); iterator
							.hasNext();) {
						// Assuming one ondfare linked to one flexi fare
						FlexiRuleDTO flexiRuleDTO = iterator.next();
						createMetaCharge(ReservationInternalConstants.ChargeGroup.SUR,
								new BigDecimal(flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT)), chargeDate,
								newAdultActualChargeMetaAmounts);
						createMetaCharge(ReservationInternalConstants.ChargeGroup.SUR,
								new BigDecimal(flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT)), chargeDate,
								newInfantActualChargeMetaAmounts);
						createMetaCharge(ReservationInternalConstants.ChargeGroup.SUR,
								new BigDecimal(flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD)), chargeDate,
								newChildActualChargeMetaAmounts);
						break;
					}
				}
			}
		}

		return new Object[] { newAdultActualChargeMetaAmounts, newInfantActualChargeMetaAmounts, newChildActualChargeMetaAmounts };
	}

	private static void createMetaCharge(String chargeGroupCode, BigDecimal chargeAmount, Date chargeDate,
			Collection<ChargeMetaTO> chargeMetaAmounts) {
		if (chargeAmount.doubleValue() > 0) {
			chargeMetaAmounts.add(ChargeMetaTO.createBasicChargeMetaTO(chargeGroupCode, chargeAmount, chargeDate, null, null));
		}
	}

	private static Collection<ChargeMetaTO> filterNonMatchingCharges(Collection<ChargeMetaTO> colChargeMetaTO, String paxType) {
		Collection<ChargeMetaTO> nonMatchingCharges = new ArrayList<ChargeMetaTO>();

		for (ChargeMetaTO chargeMetaTO : colChargeMetaTO) {
			if (!paxType.equals(chargeMetaTO.getPaxType())) {
				nonMatchingCharges.add(chargeMetaTO);
			}
		}

		return nonMatchingCharges;
	}

	public static ResBalancesSummaryTOV2 getBalancesForFreshAddOnd(String alterationType,
			Collection<PAXSummaryTOV2> colPAXSummaryTOV2, Collection<OndFareDTO> colOndFareDTOs,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap, int fareDiscPerc) {

		Date quotedDateTime = new Date();

		ResBalancesSummaryTOV2 resBalancesSummaryTOV2 = new ResBalancesSummaryTOV2();

		// Current and new OND balances
		resBalancesSummaryTOV2.setSegmentSummaryTOV2(getONDBalancesForAlt(colPAXSummaryTOV2, colOndFareDTOs, quotedDateTime,
				paxExtChargeMap, fareDiscPerc));

		// Pax balances
		resBalancesSummaryTOV2.setPaxSummaryTOV2(getPaxBalancesForAlt(colPAXSummaryTOV2, colOndFareDTOs, quotedDateTime,
				paxExtChargeMap));

		BigDecimal[] totalAmounts = getTotalAmounts(resBalancesSummaryTOV2.getPaxSummaryTOV2(), false);

		resBalancesSummaryTOV2.setTotalCnxChargeForCurrentAlt(totalAmounts[0]);
		resBalancesSummaryTOV2.setTotalModChargeForCurrentAlt(totalAmounts[1]);
		resBalancesSummaryTOV2.setTotalPrice(totalAmounts[2]);
		resBalancesSummaryTOV2.setTotalAmountDue(totalAmounts[3]);
		resBalancesSummaryTOV2.setTotalCreditAmount(totalAmounts[4]);

		resBalancesSummaryTOV2.setColPayments(new ArrayList<ReservationTnx>());
		resBalancesSummaryTOV2.setColCredits(new ArrayList<ReservationTnx>());
		resBalancesSummaryTOV2.setColLccPaxPayments(new ArrayList<LccPaxPaymentTO>());

		return resBalancesSummaryTOV2;
	}

	/**
	 * Returns Balances for reservation alterations (Modifications/Cancellations/Additions) for specified OND(s).
	 * 
	 * @param paxExtChargeMap
	 * @param fareDiscPerc
	 *            TODO
	 * @param res
	 * @param fareChargesAndSegmentDTOs
	 * @param type
	 * 
	 * @return
	 */
	private static SegmentSummaryTOV2 getONDBalancesForAlt(Collection<PAXSummaryTOV2> colPAXSummaryTOV2,
			Collection<OndFareDTO> colOndFareDTOs, Date quotedDateTime,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap, int fareDiscPerc) {

		int[] passengerCounts = getPaxTypeWiseCounts(colPAXSummaryTOV2);

		SegmentSummaryTOV2 segmentSummaryTOV2 = new SegmentSummaryTOV2();

		BigDecimal identifiedTotCnx = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal identifiedTotMod = AccelAeroCalculator.getDefaultBigDecimalZero();

		// For existing OND
		Collection<ChargeMetaTO> currentActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> currentRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> currentNonRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();

		// For new OND
		Collection<ChargeMetaTO> newActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newNonRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();

		Object[] faresAndCharges = getNewFaresAndCharges(colOndFareDTOs, quotedDateTime);
		Collection<ChargeMetaTO> adultMetaCharges = (Collection) faresAndCharges[0];
		Collection<ChargeMetaTO> infantMetaCharges = (Collection) faresAndCharges[1];
		Collection<ChargeMetaTO> childMetaCharges = (Collection) faresAndCharges[2];

		BigDecimal totalFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		// For Adults
		for (int i = 0; i < passengerCounts[0]; i++) {
			for (ChargeMetaTO chargeMetaTO : adultMetaCharges) {
				totalFareDiscount = AccelAeroCalculator.add(totalFareDiscount,
						AccelAeroCalculator.calculatePercentage(chargeMetaTO.getChargeAmount(), fareDiscPerc));
				newActualChargeMetaAmounts.add(chargeMetaTO.clone());
			}
		}

		// For infants
		for (int i = 0; i < passengerCounts[2]; i++) {
			for (ChargeMetaTO chargeMetaTO : infantMetaCharges) {
				newActualChargeMetaAmounts.add(chargeMetaTO.clone());
			}
		}

		// For Children
		for (int i = 0; i < passengerCounts[1]; i++) {
			for (ChargeMetaTO chargeMetaTO : childMetaCharges) {
				newActualChargeMetaAmounts.add(chargeMetaTO.clone());
			}
		}
		if (paxExtChargeMap != null) {
			for (Integer seq : paxExtChargeMap.keySet()) {
				for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(seq)) {
					ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
							ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null, null);
					newActualChargeMetaAmounts.add(anciMetaCharge);
				}
			}
		}

		// Existing OND Fares & Charges
		segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(currentActualChargeMetaAmounts, null));
		segmentSummaryTOV2.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(currentNonRefundableMetaAmounts, null));
		segmentSummaryTOV2.setCurrentRefundableAmounts(SegmentSummaryTOV2.convert(currentRefundableMetaAmounts, null));

		// New OND Fare & Charges
		segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newActualChargeMetaAmounts, null));
		segmentSummaryTOV2.setNewNonRefundableAmounts(SegmentSummaryTOV2.convert(newNonRefundableMetaAmounts, null));
		segmentSummaryTOV2.setNewRefundableAmounts(SegmentSummaryTOV2.convert(newRefundableMetaAmounts, null));

		segmentSummaryTOV2.setIdentifiedTotalCnxCharge(identifiedTotCnx);
		segmentSummaryTOV2.setIdentifiedTotalModCharge(identifiedTotMod);
		segmentSummaryTOV2.setIdentifiedExtraFeeAmount(AccelAeroCalculator.getDefaultBigDecimalZero());

		return segmentSummaryTOV2;
	}

	private static int[] getPaxTypeWiseCounts(Collection<PAXSummaryTOV2> colPAXSummaryTOV2) {
		int adult = 0;
		int child = 0;
		int infant = 0;

		for (PAXSummaryTOV2 summaryTOV2 : colPAXSummaryTOV2) {
			if (summaryTOV2.getPaxType().equals(PaxTypeTO.ADULT)) {
				adult++;
			} else if (summaryTOV2.getPaxType().equals(PaxTypeTO.CHILD)) {
				child++;
			} else if (summaryTOV2.getPaxType().equals(PaxTypeTO.INFANT)) {
				infant++;
			}
		}

		return new int[] { adult, child, infant };
	}

	private static Collection<PAXSummaryTOV2> getPaxBalancesForAlt(Collection<PAXSummaryTOV2> colPAXSummaryTOV2,
			Collection<OndFareDTO> ondFareDTOs, Date quotedDateTime, Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap) {

		Map<Integer, PAXSummaryTOV2> newPaxSummaries = new HashMap<Integer, PAXSummaryTOV2>();

		for (PAXSummaryTOV2 paxSummaryTOV2 : colPAXSummaryTOV2) {
			newPaxSummaries.put(paxSummaryTOV2.getPnrPaxId(), paxSummaryTOV2);
		}

		Object[] quotedFaresAndCharges = getNewFaresAndCharges(ondFareDTOs, quotedDateTime);

		Collection<ChargeMetaTO> newAdultMetaCharges = (Collection) quotedFaresAndCharges[0];
		Collection<ChargeMetaTO> newInfantMetaCharges = (Collection) quotedFaresAndCharges[1];
		Collection<ChargeMetaTO> newChildMetaCharges = (Collection) quotedFaresAndCharges[2];

		SegmentSummaryTOV2 segmentSummaryTOV2;
		Collection<ChargeMetaTO> newPaxChargeMetaAmounts;
		Collection<ChargeMetaTO> newInfChargeMetaAmounts;

		for (PAXSummaryTOV2 paxSummaryTOV2 : colPAXSummaryTOV2) {

			if (ReservationInternalConstants.PassengerType.ADULT.equals(paxSummaryTOV2.getPaxType())
					|| ReservationInternalConstants.PassengerType.CHILD.equals(paxSummaryTOV2.getPaxType())) {

				newPaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				newInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

				Integer infantId = paxSummaryTOV2.getInfantId();

				if (ReservationInternalConstants.PassengerType.ADULT.equals(paxSummaryTOV2.getPaxType())) {
					newPaxChargeMetaAmounts.addAll(newAdultMetaCharges);
				} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxSummaryTOV2.getPaxType())) {
					newPaxChargeMetaAmounts.addAll(newChildMetaCharges);
				}

				if (paxExtChargeMap != null && paxExtChargeMap.containsKey(paxSummaryTOV2.getPnrPaxId())) {
					for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(paxSummaryTOV2.getPnrPaxId())) {
						ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
								ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null, null);
						newPaxChargeMetaAmounts.add(anciMetaCharge);
					}
				}

				if (infantId != null) {
					newInfChargeMetaAmounts.addAll(newInfantMetaCharges);
				}

				segmentSummaryTOV2 = new SegmentSummaryTOV2();
				segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(new ArrayList<ChargeMetaTO>(), null));
				segmentSummaryTOV2
						.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(new ArrayList<ChargeMetaTO>(), null));
				segmentSummaryTOV2.setCurrentRefundableAmounts(SegmentSummaryTOV2.convert(new ArrayList<ChargeMetaTO>(), null));
				segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newPaxChargeMetaAmounts, null));
				segmentSummaryTOV2.setIdentifiedTotalCnxCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
				segmentSummaryTOV2.setIdentifiedTotalModCharge(AccelAeroCalculator.getDefaultBigDecimalZero());

				paxSummaryTOV2.setSegmentSummaryTOV2(segmentSummaryTOV2);
				paxSummaryTOV2.setTotalCnxChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalCnxCharge());
				paxSummaryTOV2.setTotalModChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalModCharge());
				paxSummaryTOV2.setColCredits(new ArrayList<ReservationTnx>());
				paxSummaryTOV2.setColPayments(new ArrayList<ReservationTnx>());

				BigDecimal totalPaxNewPrice = AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalPrice(),
						SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, null).getTotalPrice(),
						segmentSummaryTOV2.getIdentifiedTotalCnxCharge(), segmentSummaryTOV2.getIdentifiedTotalModCharge());

				BigDecimal newPaxBalanceAmount = totalPaxNewPrice;
				BigDecimal totalAmountDue = newPaxBalanceAmount.doubleValue() > 0 ? newPaxBalanceAmount : AccelAeroCalculator
						.getDefaultBigDecimalZero();
				BigDecimal totalCreditAmount = newPaxBalanceAmount.doubleValue() < 0
						? newPaxBalanceAmount.negate()
						: AccelAeroCalculator.getDefaultBigDecimalZero();

				paxSummaryTOV2.setTotalAmountDue(totalAmountDue);
				paxSummaryTOV2.setTotalCreditAmount(totalCreditAmount);
				paxSummaryTOV2.setTotalPrice(totalPaxNewPrice);

				if (infantId != null) {
					paxSummaryTOV2 = newPaxSummaries.get(infantId);

					segmentSummaryTOV2 = new SegmentSummaryTOV2();
					segmentSummaryTOV2.setIdentifiedTotalCnxCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
					segmentSummaryTOV2.setIdentifiedTotalModCharge(AccelAeroCalculator.getDefaultBigDecimalZero());

					segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(new ArrayList<ChargeMetaTO>(), null));
					segmentSummaryTOV2.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(new ArrayList<ChargeMetaTO>(),
							null));
					segmentSummaryTOV2.setCurrentRefundableAmounts(SegmentSummaryTOV2
							.convert(new ArrayList<ChargeMetaTO>(), null));
					segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, null));
					paxSummaryTOV2.setSegmentSummaryTOV2(segmentSummaryTOV2);

					paxSummaryTOV2.setTotalCnxChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalCnxCharge());
					paxSummaryTOV2.setTotalModChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalModCharge());

					paxSummaryTOV2.setColCredits(new ArrayList<ReservationTnx>());
					paxSummaryTOV2.setColPayments(new ArrayList<ReservationTnx>());

					BigDecimal totalInfNewPrice = AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalPrice(),
							segmentSummaryTOV2.getIdentifiedTotalCnxCharge(), segmentSummaryTOV2.getIdentifiedTotalModCharge());

					paxSummaryTOV2.setTotalAmountDue(totalAmountDue);
					paxSummaryTOV2.setTotalCreditAmount(totalCreditAmount);
					paxSummaryTOV2.setTotalPrice(totalInfNewPrice);
				}
			}
		}
		return newPaxSummaries.values();
	}

	public static ResBalancesSummaryTOV2 applyPaymentAwareness(String alterationType, Reservation reservation,
			ResBalancesSummaryTOV2 holderResBalancesSummaryTOV2) throws ModuleException {

		Map<Integer, PAXSummaryTOV2> paxSummaries = new HashMap<Integer, PAXSummaryTOV2>();
		for (PAXSummaryTOV2 paxSummaryTOV2 : holderResBalancesSummaryTOV2.getPaxSummaryTOV2()) {
			if (paxSummaryTOV2.getPnrPaxId() == null && ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
				continue;
			}
			paxSummaries.put(paxSummaryTOV2.getPnrPaxId(), paxSummaryTOV2);
		}

		Map<Integer, Collection<ReservationTnx>> mapBalances = AirproxyModuleUtils.getPassengerBD().getAllBalanceTransactions(
				new ArrayList<Integer>(paxSummaries.keySet()));
		Map<Integer, Collection<ReservationTnx>> mapPayments = AirproxyModuleUtils.getPassengerBD().getPNRPaxPaymentsAndRefunds(
				new ArrayList<Integer>(paxSummaries.keySet()));

		BigDecimal resTotalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal resTotalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ReservationTnx> colResPayments = new ArrayList<ReservationTnx>();
		Collection<ReservationTnx> colResBalances = new ArrayList<ReservationTnx>();

		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			if (!ReservationApiUtils.isInfantType(reservationPax) && reservation.isInfantPaymentRecordedWithInfant()) {
				PAXSummaryTOV2 paxSummaryTOV2 = paxSummaries.get(reservationPax.getPnrPaxId());

				BigDecimal compareValue = AccelAeroCalculator.add(paxSummaryTOV2.getTotalAmountDue(), paxSummaryTOV2
						.getTotalCreditAmount().negate(), reservationPax.getTotalAvailableBalance());

				BigDecimal totalAmountDue = compareValue.doubleValue() > 0 ? compareValue : AccelAeroCalculator
						.getDefaultBigDecimalZero();
				BigDecimal totalCreditAmount = compareValue.doubleValue() < 0 ? compareValue.negate() : AccelAeroCalculator
						.getDefaultBigDecimalZero();

				paxSummaryTOV2.setTotalAmountDue(totalAmountDue);
				paxSummaryTOV2.setTotalCreditAmount(totalCreditAmount);

				Collection<ReservationTnx> tempPayments = mapPayments.get(reservationPax.getPnrPaxId());
				Collection<ReservationTnx> tempBalances = mapBalances.get(reservationPax.getPnrPaxId());

				paxSummaryTOV2.setColPayments(tempPayments != null ? tempPayments : new ArrayList<ReservationTnx>());
				paxSummaryTOV2.setColCredits(tempBalances != null ? tempBalances : new ArrayList<ReservationTnx>());

				colResPayments.addAll(paxSummaryTOV2.getColPayments());
				colResBalances.addAll(paxSummaryTOV2.getColCredits());

				resTotalAmountDue = AccelAeroCalculator.add(resTotalAmountDue, totalAmountDue);
				resTotalCreditAmount = AccelAeroCalculator.add(resTotalCreditAmount, totalCreditAmount);

				Integer adultId = reservationPax.getAccompaniedPaxId();

				if (adultId != null && !reservation.isInfantPaymentRecordedWithInfant()) {
					PAXSummaryTOV2 infantPaxSummaryTOV2 = paxSummaries.get(adultId);

					infantPaxSummaryTOV2.setTotalAmountDue(totalAmountDue);
					infantPaxSummaryTOV2.setTotalCreditAmount(totalCreditAmount);
				}
			}
		}

		holderResBalancesSummaryTOV2.setTotalAmountDue(resTotalAmountDue);
		holderResBalancesSummaryTOV2.setTotalCreditAmount(resTotalCreditAmount);

		holderResBalancesSummaryTOV2.setColCredits(colResBalances);
		holderResBalancesSummaryTOV2.setColPayments(colResPayments);

		composeCustomerPaymentsIfResIsLCC(reservation, alterationType, holderResBalancesSummaryTOV2);

		return holderResBalancesSummaryTOV2;
	}

	/**
	 * Returns Balances for reservation alterations (Modifications/Cancellations/Additions) for specified OND(s).
	 * 
	 * @param res
	 * @param paxExtChargeMap
	 * @param fareChargesAndSegmentDTOs
	 * @param type
	 * @return
	 */
	private static SegmentSummaryTOV2 getONDBalancesForAltInterline(Reservation res,
			Map<Integer, PnrChargesDTO> pnrPaxChargesMap, Collection<OndFareDTO> colOndFareDTOs, String alterationType,
			Date quotedDateTime, Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap) {

		SegmentSummaryTOV2 segmentSummaryTOV2 = new SegmentSummaryTOV2();

		BigDecimal identifiedTotCnx = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal identifiedTotMod = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal identifiedTotExtraFees = AccelAeroCalculator.getDefaultBigDecimalZero();

		// For existing OND
		Collection<ChargeMetaTO> currentActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> currentRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> currentNonRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();

		// For new OND
		Collection<ChargeMetaTO> newActualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newNonRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();

		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {

			for (Iterator<ReservationPax> paxIt = res.getPassengers().iterator(); paxIt.hasNext();) {
				ReservationPax pax = paxIt.next();

				if (ReservationApiUtils.isAdultType(pax) || ReservationApiUtils.isChildType(pax)
						|| (res.isInfantPaymentRecordedWithInfant() && ReservationApiUtils.isInfantType(pax))) {
					PnrChargesDTO paxOndCharges = pnrPaxChargesMap.get(pax.getPnrPaxId());

					if (paxOndCharges != null) {
						if (!ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
							identifiedTotCnx = AccelAeroCalculator.add(identifiedTotCnx,
									paxOndCharges.getIdentifiedCancellationAmount()); // identified cnx charge for current
																						// operation
							identifiedTotMod = AccelAeroCalculator.add(identifiedTotMod,
									paxOndCharges.getIdentifiedModificationAmount());// identified mod charge for current
																						// modification
						} else {
							newActualChargeMetaAmounts.addAll(paxOndCharges.getActualChargeMetaAmounts());
							newRefundableMetaAmounts.addAll(paxOndCharges.getRefundableChargeMetaAmounts());
							newNonRefundableMetaAmounts.addAll(paxOndCharges.getNonRefundableChargeMetaAmounts());
						}
	
						currentActualChargeMetaAmounts.addAll(paxOndCharges.getActualChargeMetaAmounts());
						currentRefundableMetaAmounts.addAll(paxOndCharges.getRefundableChargeMetaAmounts());
						currentNonRefundableMetaAmounts.addAll(paxOndCharges.getNonRefundableChargeMetaAmounts());
	
						identifiedTotExtraFees = AccelAeroCalculator.add(identifiedTotExtraFees,
								paxOndCharges.getIdentifiedExtraFeeAmount());
					}
				}
			}
			
			
			if (paxExtChargeMap != null && !paxExtChargeMap.isEmpty()) {
				for (Integer paxSequence : paxExtChargeMap.keySet()) {
					List<LCCClientExternalChgDTO> paxExternalCharges = paxExtChargeMap.get(paxSequence);
					if (paxExternalCharges != null && !paxExternalCharges.isEmpty()) {
						for (LCCClientExternalChgDTO extCharge : paxExternalCharges) {
							ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
									ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null,
									null);
							newActualChargeMetaAmounts.add(anciMetaCharge);
						}
					}
				}
			}	
		}

		Object[] faresAndCharges = null;

		if (colOndFareDTOs != null) {
			faresAndCharges = getNewFaresAndCharges(colOndFareDTOs, quotedDateTime);
		}

		if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {

			// Object[] faresAndCharges = getNewFaresAndCharges(colOndFareDTOs, quotedDateTime);
			Collection<ChargeMetaTO> adultMetaCharges = (Collection) faresAndCharges[0];
			Collection<ChargeMetaTO> infantMetaCharges = (Collection) faresAndCharges[1];
			Collection<ChargeMetaTO> childMetaCharges = (Collection) faresAndCharges[2];

			// For Adults
			for (int i = 0; i < res.getTotalPaxAdultCount(); i++) {
				for (ChargeMetaTO chargeMetaTO : adultMetaCharges) {
					newActualChargeMetaAmounts.add(chargeMetaTO.clone());
				}
			}

			// For infants
			for (int i = 0; i < res.getTotalPaxInfantCount(); i++) {
				for (ChargeMetaTO chargeMetaTO : infantMetaCharges) {
					newActualChargeMetaAmounts.add(chargeMetaTO.clone());
				}
			}

			// For Children
			for (int i = 0; i < res.getTotalPaxChildCount(); i++) {
				for (ChargeMetaTO chargeMetaTO : childMetaCharges) {
					newActualChargeMetaAmounts.add(chargeMetaTO.clone());
				}
			}
			for (Iterator<ReservationPax> paxIt = res.getPassengers().iterator(); paxIt.hasNext();) {
				ReservationPax pax = paxIt.next();
				if (paxExtChargeMap != null && paxExtChargeMap.containsKey(pax.getPaxSequence())) {
					for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(pax.getPaxSequence())) {
						ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
								ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null, null);
						newActualChargeMetaAmounts.add(anciMetaCharge);
					}
				}
			}

			// TODO - set refundable/non-refundable totals
		}

		if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			Collection<ChargeMetaTO> infantMetaCharges = (Collection<ChargeMetaTO>) faresAndCharges[1];
			for (ChargeMetaTO chargeMetaTO : infantMetaCharges) {
				newActualChargeMetaAmounts.add(chargeMetaTO.clone());
			}
		}

		// Existing OND Fares & Charges
		segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(currentActualChargeMetaAmounts, null));
		segmentSummaryTOV2.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(currentNonRefundableMetaAmounts, null));
		segmentSummaryTOV2.setCurrentRefundableAmounts(SegmentSummaryTOV2.convert(currentRefundableMetaAmounts, null));

		// New OND Fare & Charges
		segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newActualChargeMetaAmounts, null));
		segmentSummaryTOV2.setNewNonRefundableAmounts(SegmentSummaryTOV2.convert(newNonRefundableMetaAmounts, null));
		segmentSummaryTOV2.setNewRefundableAmounts(SegmentSummaryTOV2.convert(newRefundableMetaAmounts, null));

		// Identified total CNX/MOD charges
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
			segmentSummaryTOV2.setIdentifiedTotalCnxCharge(identifiedTotCnx);
		}
		if (ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)) {
			segmentSummaryTOV2.setIdentifiedTotalModCharge(identifiedTotMod);
		}

		segmentSummaryTOV2.setIdentifiedExtraFeeAmount(identifiedTotExtraFees);

		return segmentSummaryTOV2;
	}

	private static Collection<PAXSummaryTOV2> getPaxBalancesForAltInterline(Reservation reservation,
			Collection<OndFareDTO> fareChargesAndSegmentDTOs, Map<Integer, PnrChargesDTO> pnrPaxAllSegChargesMap,
			Map<Integer, PnrChargesDTO> pnrPaxChargesMap, String alterationType, Date quotedDateTime,
			Collection<Integer> pnrSegIds, Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap) {

		Map<Integer, PAXSummaryTOV2> newPaxSummaries = new HashMap<Integer, PAXSummaryTOV2>();
		Map<Integer, ReservationPax> reservationPaxMap = AirProxyReservationUtil.prepareReservationPaxMap(reservation);
		Collection<ChargeMetaTO> newAdultMetaCharges = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newInfantMetaCharges = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> newChildMetaCharges = new ArrayList<ChargeMetaTO>();

		boolean removeInsurance = isRemoveInsurance(alterationType, reservation);

		Object[] quotedFaresAndCharges = null;

		if (fareChargesAndSegmentDTOs != null) {
			quotedFaresAndCharges = getNewFaresAndCharges(fareChargesAndSegmentDTOs, quotedDateTime);
		}

		if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
			newAdultMetaCharges = (Collection) quotedFaresAndCharges[0];
			newInfantMetaCharges = (Collection) quotedFaresAndCharges[1];
			newChildMetaCharges = (Collection) quotedFaresAndCharges[2];
		}

		if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			newInfantMetaCharges = (Collection<ChargeMetaTO>) quotedFaresAndCharges[1];
		}

		SegmentSummaryTOV2 segmentSummaryTOV2;
		Collection<ChargeMetaTO> actualPaxChargeMetaAmounts;
		Collection<ChargeMetaTO> actualInfChargeMetaAmounts;
		Collection<ChargeMetaTO> refundablePaxChargeMetaAmounts;
		Collection<ChargeMetaTO> refundableInfChargeMetaAmounts;
		Collection<ChargeMetaTO> nonRefundablePaxChargeMetaAmounts;
		Collection<ChargeMetaTO> nonRefundableInfChargeMetaAmounts;
		Collection<ChargeMetaTO> newPaxChargeMetaAmounts;
		Collection<ChargeMetaTO> newInfChargeMetaAmounts;
		BigDecimal paxCancellationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal paxModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal infCancellationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal infModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		Integer insChgRateId = null;
		if (removeInsurance) {
			insChgRateId = quotedExtChgMap.get(EXTERNAL_CHARGES.INSURANCE).getChgRateId();
		}
		boolean isInfantPaymentSeperate = reservation.isInfantPaymentRecordedWithInfant();
		for (ReservationPax reservationPax : reservationPaxMap.values()) {

			PnrChargesDTO pnrChargesDTO = pnrPaxChargesMap.get(reservationPax.getPnrPaxId());

			if (pnrChargesDTO == null) {
				pnrChargesDTO = new PnrChargesDTO();
			}

			if (ReservationApiUtils.isAdultType(reservationPax) || ReservationApiUtils.isChildType(reservationPax)
					|| (isInfantPaymentSeperate && !ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType))) {

				BigDecimal refInsAmt = AccelAeroCalculator.getDefaultBigDecimalZero();
				actualPaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				actualInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				refundablePaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				refundableInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				nonRefundablePaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				nonRefundableInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

				newPaxChargeMetaAmounts = new ArrayList<ChargeMetaTO>();
				newInfChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

				Integer adultId = reservationPax.getAccompaniedPaxId();
				if (isInfantPaymentSeperate && adultId != null) {
					adultId = null;
				}

				// Deduct cancelled segments' total refundable amount
				if (!ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
					// Parent
					if (adultId != null) {
						actualPaxChargeMetaAmounts.addAll(filterNonMatchingCharges(pnrChargesDTO.getActualChargeMetaAmounts(),
								ReservationInternalConstants.PassengerType.INFANT));

						PnrChargesDTO infPnrChargesDTO = pnrPaxChargesMap.get(adultId);
						actualInfChargeMetaAmounts.addAll(infPnrChargesDTO.getActualChargeMetaAmounts());
					} else {
						actualPaxChargeMetaAmounts.addAll(pnrChargesDTO.getActualChargeMetaAmounts());
					}

					if (adultId != null) {
						refundablePaxChargeMetaAmounts
								.addAll(filterNonMatchingCharges(pnrChargesDTO.getRefundableChargeMetaAmounts(),
										ReservationInternalConstants.PassengerType.INFANT));
						nonRefundablePaxChargeMetaAmounts
								.addAll(filterNonMatchingCharges(pnrChargesDTO.getNonRefundableChargeMetaAmounts(),
										ReservationInternalConstants.PassengerType.INFANT));

						PnrChargesDTO infPnrChargesDTO = pnrPaxChargesMap.get(adultId);
						refundableInfChargeMetaAmounts.addAll(infPnrChargesDTO.getRefundableChargeMetaAmounts());
						nonRefundableInfChargeMetaAmounts.addAll(infPnrChargesDTO.getNonRefundableChargeMetaAmounts());
					} else {
						refundablePaxChargeMetaAmounts.addAll(pnrChargesDTO.getRefundableChargeMetaAmounts());
						nonRefundablePaxChargeMetaAmounts.addAll(pnrChargesDTO.getNonRefundableChargeMetaAmounts());
					}

					// In case of modify segment in same carrier, since add ond will remove all insurance charges
					// make sure that that insurance charge is added here to counter it.
					// FIXME This is a hak! and it works :) refactor and get rid of this
					if (removeInsurance
							&& ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)) {
						for (ChargeMetaTO chgMetaTO : refundablePaxChargeMetaAmounts) {
							if (chgMetaTO.getChargeRateId() != null
									&& chgMetaTO.getChargeRateId().intValue() == insChgRateId.intValue()) {
								refInsAmt = AccelAeroCalculator.add(refInsAmt, chgMetaTO.getChargeAmount());
							}
						}
						refInsAmt = refInsAmt.negate();
					}
				} else if (removeInsurance) {
					PnrChargesDTO pnrAllChgDTO = pnrPaxAllSegChargesMap.get(reservationPax.getPnrPaxId());
					Collection<ChargeMetaTO> refMetaChgsCollection = new ArrayList<ChargeMetaTO>();
					Collection<ChargeMetaTO> refMetaChgsCollectionInf = new ArrayList<ChargeMetaTO>();
					if (adultId != null) {
						refMetaChgsCollection.addAll(filterNonMatchingCharges(pnrAllChgDTO.getRefundableChargeMetaAmounts(),
								ReservationInternalConstants.PassengerType.INFANT));

						PnrChargesDTO infPnrChargesDTO = pnrPaxAllSegChargesMap.get(adultId);
						refMetaChgsCollectionInf.addAll(infPnrChargesDTO.getRefundableChargeMetaAmounts());
					} else {
						refMetaChgsCollection.addAll(pnrAllChgDTO.getRefundableChargeMetaAmounts());
					}

					for (ChargeMetaTO chgMetaTO : refMetaChgsCollection) {
						if (chgMetaTO.getChargeRateId() != null && insChgRateId != null
								&& chgMetaTO.getChargeRateId().intValue() == insChgRateId.intValue()) {
							refInsAmt = AccelAeroCalculator.add(refInsAmt, chgMetaTO.getChargeAmount());

						}
					}
				}

				// Add cancellation or modification charges
				if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {

					paxCancellationCharge = pnrChargesDTO.getIdentifiedCancellationAmount();

					if (adultId != null) {
						PnrChargesDTO infPnrChargesDTO = pnrPaxChargesMap.get(adultId);
						infCancellationCharge = infPnrChargesDTO.getIdentifiedCancellationAmount();
					}

				} else if (ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)) {
					paxModificationCharge = pnrChargesDTO.getIdentifiedModificationAmount();

					if (adultId != null) {
						PnrChargesDTO infPnrChargesDTO = pnrPaxChargesMap.get(adultId);
						infModificationCharge = infPnrChargesDTO.getIdentifiedModificationAmount();
					}
				}

				// Add new quoted segments' prices
				if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {

					if (ReservationApiUtils.isAdultType(reservationPax)) {
						newPaxChargeMetaAmounts.addAll(newAdultMetaCharges);
					} else if (ReservationApiUtils.isChildType(reservationPax)) {
						newPaxChargeMetaAmounts.addAll(newChildMetaCharges);
					}

					if (paxExtChargeMap != null && paxExtChargeMap.containsKey(reservationPax.getPaxSequence())) {
						for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(reservationPax.getPaxSequence())) {
							ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
									ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null,
									null);
							newPaxChargeMetaAmounts.add(anciMetaCharge);
						}
					}

					if (adultId != null) {
						newInfChargeMetaAmounts.addAll(newInfantMetaCharges);
					}
				}

				// Adding extra fee
				if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)) {
					if (pnrChargesDTO.getExtraFeeChargeMetaAmounts() != null) {
						for (ChargeMetaTO chargeMetaTO : pnrChargesDTO.getExtraFeeChargeMetaAmounts()) {
							newPaxChargeMetaAmounts.add(chargeMetaTO);
						}
					}
				}

				// Add new infants prices
				if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
					BigDecimal[] addInfRefund = new BigDecimal[0]; // no refundable calculations needed for add infant
					pnrChargesDTO.setRefundableAmounts(addInfRefund);
					if (reservationPax.getInfants() != null && !reservationPax.getInfants().isEmpty()) {
						Set<ReservationPax> setInfants = reservationPax.getInfants();
						for (ReservationPax adInf : setInfants) {
							if (adInf.getPnrPaxId() == null) {
								if (paxExtChargeMap != null && paxExtChargeMap.containsKey(reservationPax.getPaxSequence())) {
									for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(reservationPax.getPaxSequence())) {
										ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
												ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(),
												quotedDateTime, null, null);
										newInfantMetaCharges.add(anciMetaCharge);
									}
								}	
								// newInfChargeMetaAmounts.addAll(newInfantMetaCharges);
								newPaxChargeMetaAmounts.addAll(newInfantMetaCharges);
							}
						}
					}
				} else if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)) {
					if (paxExtChargeMap != null && paxExtChargeMap.containsKey(reservationPax.getPaxSequence())) {
						for (LCCClientExternalChgDTO extCharge : paxExtChargeMap.get(reservationPax.getPaxSequence())) {
							ChargeMetaTO anciMetaCharge = ChargeMetaTO.createBasicChargeMetaTO(
									ReservationInternalConstants.ChargeGroup.SUR, extCharge.getAmount(), quotedDateTime, null,
									null);
							newPaxChargeMetaAmounts.add(anciMetaCharge);
						}
					}
				}

				segmentSummaryTOV2 = new SegmentSummaryTOV2();
				segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(actualPaxChargeMetaAmounts, null));

				Collection<ChargeMetaTO> allNonRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
				allNonRefundableMetaAmounts.addAll(nonRefundablePaxChargeMetaAmounts);
				allNonRefundableMetaAmounts.addAll(nonRefundableInfChargeMetaAmounts);

				Collection<ChargeMetaTO> allRefundableMetaAmounts = new ArrayList<ChargeMetaTO>();
				allRefundableMetaAmounts.addAll(refundablePaxChargeMetaAmounts);
				allRefundableMetaAmounts.addAll(refundableInfChargeMetaAmounts);

				segmentSummaryTOV2.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(allNonRefundableMetaAmounts, null));
				segmentSummaryTOV2.setCurrentRefundableAmounts(SegmentSummaryTOV2.convert(allRefundableMetaAmounts, null));
				segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newPaxChargeMetaAmounts, null));
				segmentSummaryTOV2.setIdentifiedTotalCnxCharge(paxCancellationCharge);
				segmentSummaryTOV2.setIdentifiedTotalModCharge(paxModificationCharge);

				PAXSummaryTOV2 paxSummaryTOV2 = new PAXSummaryTOV2();
				paxSummaryTOV2.setColCredits(new ArrayList<ReservationTnx>());

				if (pnrChargesDTO != null && pnrChargesDTO.getPaymentInfo() != null
						&& pnrChargesDTO.getPaymentInfo().size() > 0) {
					paxSummaryTOV2.setColPayments(pnrChargesDTO.getPaymentInfo());
				} else {
					Collection<ReservationTnx> tempPayments;
					try {
						ArrayList<Integer> pnrPaxIdList = new ArrayList<Integer>();
						pnrPaxIdList.add(reservationPax.getPnrPaxId());
						Map<Integer, Collection<ReservationTnx>> mapPayments = AirproxyModuleUtils.getPassengerBD()
								.getPNRPaxPaymentsAndRefunds(pnrPaxIdList);
						tempPayments = mapPayments.get(reservationPax.getPnrPaxId());
						if (tempPayments == null) {
							tempPayments = new ArrayList<ReservationTnx>();
						}
					} catch (Exception ex) {
						tempPayments = new ArrayList<ReservationTnx>();
					}
					paxSummaryTOV2.setColPayments(tempPayments);
				}
				paxSummaryTOV2.setPnrPaxId(reservationPax.getPnrPaxId());
				paxSummaryTOV2.setPaxName(ReservationApiUtils.getPassengerName("", reservationPax.getTitle(),
						reservationPax.getFirstName(), reservationPax.getLastName(), false));
				paxSummaryTOV2.setPaxType(reservationPax.getPaxType());
				paxSummaryTOV2.setInfantId(adultId);
				paxSummaryTOV2.setSegmentSummaryTOV2(segmentSummaryTOV2);
				paxSummaryTOV2.setTotalCnxChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalCnxCharge());
				paxSummaryTOV2.setTotalModChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalModCharge());

				BigDecimal totalPaxNewPrice;
				BigDecimal newPaxBalanceAmount;

				if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
					totalPaxNewPrice = AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalPrice(),
							SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, null).getTotalPrice());

					newPaxBalanceAmount = totalPaxNewPrice;

				} else {
					BigDecimal adj = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal[] actAmts = pnrChargesDTO.getActualAmounts();
					BigDecimal[] refAmts = pnrChargesDTO.getRefundableAmounts();
					if (refAmts.length >= 6 && refAmts[5] != null) {
						adj = refAmts[5];
					}

					if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
						totalPaxNewPrice = AccelAeroCalculator.add(
								(AccelAeroCalculator.subtract(AccelAeroCalculator.add(actAmts),
										AccelAeroCalculator.add(refAmts))), // current charges
								segmentSummaryTOV2.getNewAmounts().getTotalPrice());
					} else {
						totalPaxNewPrice = AccelAeroCalculator.add(
								(AccelAeroCalculator.subtract(AccelAeroCalculator.add(actAmts),
										AccelAeroCalculator.add(refAmts))), // current charges
								segmentSummaryTOV2.getNewAmounts().getTotalPrice(),
								SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, null).getTotalPrice(),
								segmentSummaryTOV2.getIdentifiedTotalCnxCharge(),
								// infCancellationCharge,
								segmentSummaryTOV2.getIdentifiedTotalModCharge());
						// ,infModificationCharge);
					}

					newPaxBalanceAmount = AccelAeroCalculator.subtract(totalPaxNewPrice, AccelAeroCalculator.add(actAmts));
					if ((ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
							|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)
							|| ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType))
							&& adj.compareTo(BigDecimal.ZERO) < 0) {
						newPaxBalanceAmount = AccelAeroCalculator.add(newPaxBalanceAmount, adj.negate());
					}
				}
				newPaxBalanceAmount = AccelAeroCalculator.subtract(newPaxBalanceAmount, refInsAmt);

				BigDecimal totalAmountDue = newPaxBalanceAmount.doubleValue() > 0 ? newPaxBalanceAmount
						: AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totalCreditAmount = newPaxBalanceAmount.doubleValue() < 0 ? newPaxBalanceAmount.negate()
						: AccelAeroCalculator.getDefaultBigDecimalZero();

				paxSummaryTOV2.setTotalAmountDue(totalAmountDue);
				paxSummaryTOV2.setTotalCreditAmount(totalCreditAmount);
				paxSummaryTOV2.setTotalPrice(totalPaxNewPrice);

				newPaxSummaries.put(reservationPax.getPnrPaxId(), paxSummaryTOV2);

				if (adultId != null) {
					ReservationPax infant = (ReservationPax) BeanUtils.getFirstElement(reservationPax.getInfants());

					paxSummaryTOV2 = new PAXSummaryTOV2();
					paxSummaryTOV2.setColCredits(new ArrayList<ReservationTnx>());

					PnrChargesDTO infPnrChargesDTO = pnrPaxChargesMap.get(adultId);
					if (infPnrChargesDTO == null) {
						infPnrChargesDTO = new PnrChargesDTO();
					}
					paxSummaryTOV2.setColPayments(infPnrChargesDTO.getPaymentInfo());

					paxSummaryTOV2.setPnrPaxId(infant.getPnrPaxId());
					paxSummaryTOV2.setPaxName(ReservationApiUtils.getPassengerName("", infant.getTitle(), infant.getFirstName(),
							infant.getLastName(), false));
					paxSummaryTOV2.setPaxType(infant.getPaxType());

					segmentSummaryTOV2 = new SegmentSummaryTOV2();
					segmentSummaryTOV2.setIdentifiedTotalCnxCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
					segmentSummaryTOV2.setIdentifiedTotalModCharge(AccelAeroCalculator.getDefaultBigDecimalZero());

					segmentSummaryTOV2.setCurrentAmounts(SegmentSummaryTOV2.convert(actualInfChargeMetaAmounts, null));
					segmentSummaryTOV2
							.setCurrentNonRefundableAmounts(SegmentSummaryTOV2.convert(nonRefundableInfChargeMetaAmounts, null));
					segmentSummaryTOV2
							.setCurrentRefundableAmounts(SegmentSummaryTOV2.convert(refundableInfChargeMetaAmounts, null));
					segmentSummaryTOV2.setNewAmounts(SegmentSummaryTOV2.convert(newInfChargeMetaAmounts, null));
					paxSummaryTOV2.setSegmentSummaryTOV2(segmentSummaryTOV2);

					paxSummaryTOV2.setTotalCnxChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalCnxCharge());
					paxSummaryTOV2.setTotalModChargeForCurrentAlt(segmentSummaryTOV2.getIdentifiedTotalModCharge());

					BigDecimal totalInfNewPrice;

					if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
							|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
						totalInfNewPrice = segmentSummaryTOV2.getNewAmounts().getTotalPrice();
					} else {
						totalInfNewPrice = AccelAeroCalculator.add(
								(AccelAeroCalculator.subtract(AccelAeroCalculator.add(infPnrChargesDTO.getActualAmounts()),
										AccelAeroCalculator.add(infPnrChargesDTO.getRefundableAmounts()))), // current
																											// charges
								segmentSummaryTOV2.getNewAmounts().getTotalPrice());

					}

					paxSummaryTOV2.setTotalAmountDue(totalAmountDue);
					paxSummaryTOV2.setTotalCreditAmount(totalCreditAmount);
					paxSummaryTOV2.setTotalPrice(totalInfNewPrice);

					newPaxSummaries.put(infant.getPnrPaxId(), paxSummaryTOV2);
				}
			}
		}
		return newPaxSummaries.values();
	}

	private static Collection getPNRSegmentIds(Set colReservationPaxFareSegment) {
		Collection<Integer> colSegmentIds = new ArrayList<Integer>();
		for (Iterator iter = colReservationPaxFareSegment.iterator(); iter.hasNext();) {
			ReservationPaxFareSegment reservationPaxFareSegment = (ReservationPaxFareSegment) iter.next();
			colSegmentIds.add(reservationPaxFareSegment.getSegment().getPnrSegId());
		}

		return colSegmentIds;
	}

	private static ReservationPaxFare getReservationPaxFare(Reservation reservation, Collection<Integer> colSegmentIds,
			Integer pnrPaxId) {
		Set<ReservationPax> paxs = reservation.getPassengers();

		for (Iterator<ReservationPax> iter = paxs.iterator(); iter.hasNext();) {
			ReservationPax reservationPax = iter.next();
			if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {
				Set<ReservationPaxFare> colResPaxFares = reservationPax.getPnrPaxFares();
				for (Iterator<ReservationPaxFare> iterator = colResPaxFares.iterator(); iterator.hasNext();) {
					ReservationPaxFare reservationPaxFare = iterator.next();
					if (checkONDSegments(reservationPaxFare, colSegmentIds)) {
						return reservationPaxFare;
					}
				}
			}
		}

		return null;
	}

	private static boolean checkONDSegments(ReservationPaxFare resPaxFare, Collection<Integer> colSegmentIds) {
		Set<ReservationPaxFareSegment> resPaxFareSegment = resPaxFare.getPaxFareSegments();
		ReservationPaxFareSegment reservationPaxFareSegment;

		for (Iterator<ReservationPaxFareSegment> iter = resPaxFareSegment.iterator(); iter.hasNext();) {
			reservationPaxFareSegment = iter.next();

			if (!colSegmentIds.contains(reservationPaxFareSegment.getPnrSegId())) {
				return false;
			}
		}

		return true;
	}

}