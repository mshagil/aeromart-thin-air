package com.isa.thinair.airproxy.api.service;

public interface AirproxyReservationAllBD extends AirproxyAncillaryBD, AirproxyReservationBD, AirproxyReservationQueryBD,
		AirproxySegmentBD, AirproxyPassengerBD, AirproxyAlertingBD, AirProxyFlightServiceBD {

}
