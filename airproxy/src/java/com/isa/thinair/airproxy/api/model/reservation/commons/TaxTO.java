package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class TaxTO extends BasicChargeTO implements Serializable, Comparable<TaxTO> {

	private static final long serialVersionUID = 1L;

	protected String taxCode;
	protected String taxName;
	protected String carrierCode;
	protected String segmentCode;
	protected List<String> applicablePassengerTypeCode;
	protected Date applicableTime;
	protected String chargeRateId;
	protected String chagrgeGroupCode;
	protected String chargeCode;
	protected String reportingChargeGroupCode;

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public List<String> getApplicablePassengerTypeCode() {
		if (applicablePassengerTypeCode == null)
			applicablePassengerTypeCode = new ArrayList<String>();
		return applicablePassengerTypeCode;
	}

	public void setApplicablePassengerTypeCode(List<String> applicablePassengerTypeCode) {
		this.applicablePassengerTypeCode = applicablePassengerTypeCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public Date getApplicableTime() {
		return applicableTime;
	}

	public void setApplicableTime(Date applicableTime) {
		this.applicableTime = applicableTime;
	}

	public String getChargeRateId() {
		return chargeRateId;
	}

	public void setChargeRateId(String chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	public String getChagrgeGroupCode() {
		return chagrgeGroupCode;
	}

	public void setChagrgeGroupCode(String chagrgeGroupCode) {
		this.chagrgeGroupCode = chagrgeGroupCode;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getApplicableToDisplay() {
		String applicableTo = "";
		if (applicablePassengerTypeCode != null) {
			for (String applicable : applicablePassengerTypeCode) {
				applicableTo = applicableTo + "," + PaxTypeTO.getPaxTypeDisplayForAAPaxType(applicable);
				;
			}
		}
		if (!applicableTo.equals("")) {
			applicableTo = applicableTo.substring(1);
		}
		return applicableTo;
	}

	@Override
	public TaxTO clone() {
		TaxTO clone = new TaxTO();
		clone.setCarrierCode(this.carrierCode);
		clone.setSegmentCode(this.getSegmentCode());
		clone.setTaxCode(this.getTaxCode());
		clone.setAmount(this.getAmount());
		clone.getApplicablePassengerTypeCode().addAll(this.getApplicablePassengerTypeCode());
		clone.setChagrgeGroupCode(this.getChagrgeGroupCode());
		clone.setChargeRateId(this.getChargeRateId());
		clone.setChargeCode(this.getChargeCode());
		clone.setLocalCurrencyAmount(this.getLocalCurrencyAmount());
		clone.setLocalCurrencyCode(this.getLocalCurrencyCode());
		clone.setReportingChargeGroupCode(this.getReportingChargeGroupCode());
		clone.setOndSequence(this.getOndSequence());
		clone.setTaxName(this.getTaxName());
		return clone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TaxTO [amount=");
		builder.append(amount);
		builder.append(", applicablePassengerTypeCode=");
		builder.append(applicablePassengerTypeCode);
		builder.append(", applicableTime=");
		builder.append(applicableTime);
		builder.append(", carrierCode=");
		builder.append(carrierCode);
		builder.append(", segmentCode=");
		builder.append(segmentCode);
		builder.append(", taxCode=");
		builder.append(taxCode);
		builder.append(", taxName=");
		builder.append(taxName);
		builder.append(", chargeRateId=");
		builder.append(chargeRateId);
		builder.append(", chargeCode=");
		builder.append(chargeCode);
		builder.append(", chagrgeGroupCode=");
		builder.append(chagrgeGroupCode);
		builder.append(", localCurrencyCode=");
		builder.append(localCurrencyCode);
		builder.append(", localCurrencyAmount=");
		builder.append(localCurrencyAmount);
		builder.append("]");
		builder.append(", reportingChargeGroupCode=");
		builder.append(reportingChargeGroupCode);
		builder.append("]");
		return builder.toString();
	}

	public String getReportingChargeGroupCode() {
		return reportingChargeGroupCode;
	}

	public void setReportingChargeGroupCode(String reportingChargeGroupCode) {
		this.reportingChargeGroupCode = reportingChargeGroupCode;
	}

	@Override
	public int compareTo(TaxTO o) {
		return o.amount.compareTo(this.amount);
	}

}
