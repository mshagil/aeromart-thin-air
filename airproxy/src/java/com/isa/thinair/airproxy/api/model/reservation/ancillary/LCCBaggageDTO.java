package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author mano
 */
public class LCCBaggageDTO implements Serializable, Comparable<LCCBaggageDTO> {

	private String baggageName;

	private String baggageDescription;

	private int soldPieces;

	private BigDecimal baggageCharge;

	private String defaultBaggage;

	private boolean alertAutoCancellation;

	private String ondBaggageChargeId;

	private String ondBaggageGroupId;

	private double weight;

	private boolean inactiveSelectedBaggage;

	private String status;

	private BigDecimal availableWeight;
	
	private String baggageStatus;
	
	
	/**
	 * 
	 * @return
	 */
	public String getBaggageStatus() {
		return baggageStatus;
	}
	
	/**
	 * 
	 * @param baggageStatus
	 */
	public void setBaggageStatus(String baggageStatus) {
		this.baggageStatus = baggageStatus;
	}
	/**
	 * 
	 * @return the baggage status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 
	 * @param status
	 * 			baggage status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the baggageName
	 */
	public String getBaggageName() {
		return baggageName;
	}

	/**
	 * @param baggageName
	 *            the baggageName to set
	 */
	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	/**
	 * @return the baggageDescription
	 */
	public String getBaggageDescription() {
		return baggageDescription;
	}

	/**
	 * @param baggageDescription
	 *            the baggageDescription to set
	 */
	public void setBaggageDescription(String baggageDescription) {
		this.baggageDescription = baggageDescription;
	}

	/**
	 * @return the soldPieces
	 */
	public int getSoldPieces() {
		return soldPieces;
	}

	/**
	 * @param soldPieces
	 *            the soldPieces to set
	 */
	public void setSoldPieces(int soldPieces) {
		this.soldPieces = soldPieces;
	}

	/**
	 * @return the baggageCharge
	 */
	public BigDecimal getBaggageCharge() {
		return baggageCharge;
	}

	/**
	 * @param baggageCharge
	 *            the baggageCharge to set
	 */
	public void setBaggageCharge(BigDecimal baggageCharge) {
		this.baggageCharge = baggageCharge;
	}

	public String getDefaultBaggage() {
		return defaultBaggage;
	}

	public void setDefaultBaggage(String defaultBaggage) {
		this.defaultBaggage = defaultBaggage;
	}

	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	@Override
	public int compareTo(LCCBaggageDTO o) {
		return this.baggageDescription.toLowerCase().compareTo(o.getBaggageDescription().toLowerCase());
	}

	/**
	 * @return the ondBaggageChargeId
	 */
	public String getOndBaggageChargeId() {
		return ondBaggageChargeId;
	}

	/**
	 * @param ondBaggageChargeId the ondBaggageChargeId to set
	 */
	public void setOndBaggageChargeId(String ondBaggageChargeId) {
		this.ondBaggageChargeId = ondBaggageChargeId;
	}

	/**
	 * @return the ondBaggageGroupId
	 */
	public String getOndBaggageGroupId() {
		return ondBaggageGroupId;
	}

	/**
	 * @param ondBaggageGroupId the ondBaggageGroupId to set
	 */
	public void setOndBaggageGroupId(String ondBaggageGroupId) {
		this.ondBaggageGroupId = ondBaggageGroupId;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	public void setInactiveSelectedBaggage(boolean inactiveSelectedBaggage) {
		this.inactiveSelectedBaggage = inactiveSelectedBaggage;
	}

	public boolean isInactiveSelectedBaggage() {
		return inactiveSelectedBaggage;
	}

	public BigDecimal getAvailableWeight() {
		return availableWeight;
	}

	public void setAvailableWeight(BigDecimal availableWeight) {
		this.availableWeight = availableWeight;
	}
}
