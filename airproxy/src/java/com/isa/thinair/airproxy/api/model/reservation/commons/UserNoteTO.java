package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class UserNoteTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<String> userNotes; // Holds last userNote

	private Date modifiedDate;
	private String action;
	private String systemNote;
	private String userName;
	private String userText;
	private String carrierCode;
    private String userNoteType;
    private String pnr;
	private boolean isGroupPNR;
    
	public ArrayList<String> getUserNotes() {
		return userNotes;
	}

	public void setUserNotes(ArrayList<String> userNotes) {
		this.userNotes = userNotes;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getSystemNote() {
		return systemNote;
	}

	public void setSystemNote(String systemNote) {
		this.systemNote = systemNote;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserText() {
		return userText;
	}

	public void setUserText(String userText) {
		this.userText = userText;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getUserNoteType() {
		return userNoteType;
	}

	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isGroupPNR() {
		return isGroupPNR;
	}

	public void setGroupPNR(boolean isGroupPNR) {
		this.isGroupPNR = isGroupPNR;
	}


}
