package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

public class LCCPassengerTypeQuantityDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String paxType;
	private int quantity;

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
