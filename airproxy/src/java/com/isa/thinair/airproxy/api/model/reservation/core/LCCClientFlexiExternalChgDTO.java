package com.isa.thinair.airproxy.api.model.reservation.core;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;

public class LCCClientFlexiExternalChgDTO extends LCCClientExternalChgDTO {

	private static final long serialVersionUID = 1L;

	private Collection<FlexiInfoTO> flexiBilities = new ArrayList<FlexiInfoTO>();

	public Collection<FlexiInfoTO> getFlexiBilities() {
		return flexiBilities;
	}

	public void setFlexiBilities(Collection<FlexiInfoTO> flexiBilities) {
		this.flexiBilities = flexiBilities;
	}

	public void addAdditionalDetail(FlexiInfoTO flexibiliy) {
		this.getFlexiBilities().add(flexibiliy);
	}

	public LCCClientFlexiExternalChgDTO cloneWithFlexibilities() {
		LCCClientFlexiExternalChgDTO clone = new LCCClientFlexiExternalChgDTO();
		clone.setExternalCharges(this.getExternalCharges());
		clone.setAmount(this.getAmount());
		clone.setCode(this.getCode());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setCarrierCode(this.getCarrierCode());
		clone.setFlightRefNumber(this.getFlightRefNumber());
		clone.setSegmentCode(this.getSegmentCode());
		clone.setSegmentSequence(this.getSegmentSequence());
		clone.setAirportCode(this.getAirportCode());
		clone.setJourneyType(this.getJourneyType());
		clone.setOndBaggageGroupId(this.getOndBaggageGroupId());
		clone.setOndBaggageChargeGroupId(this.getOndBaggageChargeGroupId());
		clone.setAnciOffer(this.isAnciOffer());
		clone.setOfferedAnciTemplateID(this.getOfferedAnciTemplateID());
		clone.setFlexiBilities(this.getFlexiBilities());

		return clone;
	}
}
