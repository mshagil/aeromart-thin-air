package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

/**
 * @author Nilindra Fernando
 */
public class LCCClientCCPaymentMetaInfo implements Serializable {

	private static final long serialVersionUID = -5422988022113579401L;

	private boolean isPaymentSuccessful;

	private Integer paymentBrokerId;

	private Integer temporyPaymentId;

	/**
	 * @return the isPaymentSuccessful
	 */
	public boolean isPaymentSuccessful() {
		return isPaymentSuccessful;
	}

	/**
	 * @param isPaymentSuccessful
	 *            the isPaymentSuccessful to set
	 */
	public void setPaymentSuccessful(boolean isPaymentSuccessful) {
		this.isPaymentSuccessful = isPaymentSuccessful;
	}

	/**
	 * @return the paymentBrokerId
	 */
	public Integer getPaymentBrokerId() {
		return paymentBrokerId;
	}

	/**
	 * @param paymentBrokerId
	 *            the paymentBrokerId to set
	 */
	public void setPaymentBrokerId(Integer paymentBrokerId) {
		this.paymentBrokerId = paymentBrokerId;
	}

	/**
	 * @return the temporyPaymentId
	 */
	public Integer getTemporyPaymentId() {
		return temporyPaymentId;
	}

	/**
	 * @param temporyPaymentId
	 *            the temporyPaymentId to set
	 */
	public void setTemporyPaymentId(Integer temporyPaymentId) {
		this.temporyPaymentId = temporyPaymentId;
	}

}
