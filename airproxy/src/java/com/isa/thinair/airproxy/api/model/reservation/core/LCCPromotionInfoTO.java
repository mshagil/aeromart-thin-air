package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

public class LCCPromotionInfoTO implements Serializable {

	private static final long serialVersionUID = 3123825719235851476L;

	private Long promoCodeId;

	private String promoType;

	private String discountType;

	private String discountApplyTo;

	private float discountValue;

	private String discountAs;

	private boolean modificationAllowed;

	private boolean cancellationAllowed;

	private boolean splitAllowed;
	
	private boolean removePaxAllowed;

	private int appliedAdults;

	private int appliedChilds;

	private int appliedInfants;

	private Set<String> appliedAncis;

	private Set<Integer> applicableBINs;

	private boolean systemGenerated;
	
	private BigDecimal creditDiscountAmount;


	public Long getPromoCodeId() {
		return promoCodeId;
	}

	public void setPromoCodeId(Long promoCodeId) {
		this.promoCodeId = promoCodeId;
	}

	public String getPromoType() {
		return promoType;
	}

	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDiscountApplyTo() {
		return discountApplyTo;
	}

	public void setDiscountApplyTo(String discountApplyTo) {
		this.discountApplyTo = discountApplyTo;
	}

	public float getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(float discountValue) {
		this.discountValue = discountValue;
	}

	public String getDiscountAs() {
		return discountAs;
	}

	public void setDiscountAs(String discountAs) {
		this.discountAs = discountAs;
	}

	public boolean isModificationAllowed() {
		return modificationAllowed;
	}

	public void setModificationAllowed(boolean modificationAllowed) {
		this.modificationAllowed = modificationAllowed;
	}

	public boolean isCancellationAllowed() {
		return cancellationAllowed;
	}

	public void setCancellationAllowed(boolean cancellationAllowed) {
		this.cancellationAllowed = cancellationAllowed;
	}

	public boolean isSplitAllowed() {
		return splitAllowed;
	}

	public void setSplitAllowed(boolean splitAllowed) {
		this.splitAllowed = splitAllowed;
	}

	public int getAppliedAdults() {
		return appliedAdults;
	}

	public void setAppliedAdults(int appliedAdults) {
		this.appliedAdults = appliedAdults;
	}

	public int getAppliedChilds() {
		return appliedChilds;
	}

	public void setAppliedChilds(int appliedChilds) {
		this.appliedChilds = appliedChilds;
	}

	public int getAppliedInfants() {
		return appliedInfants;
	}

	public void setAppliedInfants(int appliedInfants) {
		this.appliedInfants = appliedInfants;
	}

	public Set<String> getAppliedAncis() {
		return appliedAncis;
	}

	public void setAppliedAncis(Set<String> appliedAncis) {
		this.appliedAncis = appliedAncis;
	}

	public Set<Integer> getApplicableBINs() {
		return applicableBINs;
	}

	public void setApplicableBINs(Set<Integer> applicableBINs) {
		this.applicableBINs = applicableBINs;
	}

	public boolean isSystemGenerated() {
		return systemGenerated;
	}

	public void setSystemGenerated(boolean systemGenerated) {
		this.systemGenerated = systemGenerated;
	}

	public boolean isRemovePaxAllowed() {
		return removePaxAllowed;
	}

	public void setRemovePaxAllowed(boolean removePaxAllowed) {
		this.removePaxAllowed = removePaxAllowed;
	}
	
	public BigDecimal getCreditDiscountAmount() {
		return creditDiscountAmount;
	}

	public void setCreditDiscountAmount(BigDecimal creditDiscountAmount) {
		this.creditDiscountAmount = creditDiscountAmount;
	}
}
