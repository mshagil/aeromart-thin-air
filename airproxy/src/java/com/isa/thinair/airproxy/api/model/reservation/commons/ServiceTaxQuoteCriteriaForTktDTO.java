package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;

public class ServiceTaxQuoteCriteriaForTktDTO extends ServiceTaxQuoteCriteriaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private FareSegChargeTO fareSegChargeTO;

	private List<FlightSegmentTO> flightSegmentTOs;

	private boolean paxTaxRegistered;

	private BaseAvailRQ baseAvailRQ;

	private Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges;

	private Map<Integer, String> paxWisePaxTypes;
	
	protected boolean calculateOnlyForExternalCharges;

	public FareSegChargeTO getFareSegChargeTO() {
		return fareSegChargeTO;
	}

	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	public boolean isPaxTaxRegistered() {
		return paxTaxRegistered;
	}

	public Map<Integer, List<LCCClientExternalChgDTO>> getPaxWiseExternalCharges() {
		return paxWiseExternalCharges;
	}

	public Map<Integer, String> getPaxWisePaxTypes() {
		return paxWisePaxTypes;
	}

	public void setFareSegChargeTO(FareSegChargeTO fareSegChargeTO) {
		this.fareSegChargeTO = fareSegChargeTO;
	}

	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	public BaseAvailRQ getBaseAvailRQ() {
		return baseAvailRQ;
	}

	public void setBaseAvailRQ(BaseAvailRQ baseAvailRQ) {
		this.baseAvailRQ = baseAvailRQ;
	}

	public void setPaxWiseExternalCharges(Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges) {
		this.paxWiseExternalCharges = paxWiseExternalCharges;
	}

	public void setPaxWisePaxTypes(Map<Integer, String> paxWisePaxTypes) {
		this.paxWisePaxTypes = paxWisePaxTypes;
	}

	public boolean isCalculateOnlyForExternalCharges() {
		return calculateOnlyForExternalCharges;
	}

	public void setCalculateOnlyForExternalCharges(boolean calculateOnlyForExternalCharges) {
		this.calculateOnlyForExternalCharges = calculateOnlyForExternalCharges;
	}
	
}
