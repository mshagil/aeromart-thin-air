package com.isa.thinair.airproxy.api.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;

public class ReportingUtil {
	public static List<FlightLoadReportDataDTO> composeFlightLoadReportDataDTOs(ResultSet resultSet) {
		List<FlightLoadReportDataDTO> dtoList = new ArrayList<FlightLoadReportDataDTO>();

		try {
			while (resultSet.next()) {
				FlightLoadReportDataDTO reportDataDTO = new FlightLoadReportDataDTO();

				reportDataDTO.setConnChild(resultSet.getString("conn_child"));
				reportDataDTO.setConnectingFlightNumber(resultSet.getString("connecting_flight_number"));
				reportDataDTO.setConnectingSegment(resultSet.getString("connecting_segment"));
				reportDataDTO.setConnFemale(resultSet.getString("conn_female"));
				reportDataDTO.setConnInfant(resultSet.getString("conn_infant"));
				reportDataDTO.setConnMale(resultSet.getString("conn_male"));
				reportDataDTO.setConnOther(resultSet.getString("conn_other"));
				reportDataDTO.setConnTotal(resultSet.getString("conn_total"));
				reportDataDTO.setOriginChild(resultSet.getString("origin_child"));
				reportDataDTO.setOriginDate(resultSet.getString("origin_date"));
				reportDataDTO.setOriginFemale(resultSet.getString("origin_female"));
				reportDataDTO.setOriginFlightnumber(resultSet.getString("origin_flight_number"));
				reportDataDTO.setOriginInfant(resultSet.getString("origin_infant"));
				reportDataDTO.setOriginMale(resultSet.getString("origin_male"));
				reportDataDTO.setOriginOther(resultSet.getString("origin_other"));
				reportDataDTO.setOriginSegment(resultSet.getString("origin_segment"));
				reportDataDTO.setOriginTotal(resultSet.getString("origin_total"));

				dtoList.add(reportDataDTO);

			}
		} catch (SQLException e) {

		}

		return dtoList;
	}
}
