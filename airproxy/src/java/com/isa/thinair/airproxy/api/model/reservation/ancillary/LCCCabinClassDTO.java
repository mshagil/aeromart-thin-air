package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LCCCabinClassDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String cabinType;
	private List<LCCAirRowGroupDTO> airRowGroupDTOs;

	public String getCabinType() {
		return cabinType;
	}

	public void setCabinType(String cabinType) {
		this.cabinType = cabinType;
	}

	public List<LCCAirRowGroupDTO> getAirRowGroupDTOs() {
		if (airRowGroupDTOs == null)
			airRowGroupDTOs = new ArrayList<LCCAirRowGroupDTO>();
		return airRowGroupDTOs;
	}

	public void setAirRowGroupDTOs(List<LCCAirRowGroupDTO> airRowGroupDTOs) {
		this.airRowGroupDTOs = airRowGroupDTOs;
	}
}
