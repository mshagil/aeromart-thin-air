package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.math.BigDecimal;

public class BaggageRateUnit implements Serializable {

	private static final long serialVersionUID = 1L;

	private String baggageName;

	private String baggageDescription;

	private BigDecimal baggageCharge;

	private boolean defaultBggage;

	public String getBaggageName() {
		return baggageName;
	}

	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	public String getBaggageDescription() {
		return baggageDescription;
	}

	public void setBaggageDescription(String baggageDescription) {
		this.baggageDescription = baggageDescription;
	}

	public BigDecimal getBaggageCharge() {
		return baggageCharge;
	}

	public void setBaggageCharge(BigDecimal baggageCharge) {
		this.baggageCharge = baggageCharge;
	}

	public boolean getDefaultBggage() {
		return defaultBggage;
	}

	public void setDefaultBggage(boolean defaultBggage) {
		this.defaultBggage = defaultBggage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((baggageCharge == null) ? 0 : baggageCharge.hashCode());
		result = prime * result + ((baggageDescription == null) ? 0 : baggageDescription.hashCode());
		result = prime * result + ((baggageName == null) ? 0 : baggageName.hashCode());
		result = prime * result + (defaultBggage ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaggageRateUnit other = (BaggageRateUnit) obj;
		if (baggageCharge == null) {
			if (other.baggageCharge != null)
				return false;
		} else if (!baggageCharge.equals(other.baggageCharge))
			return false;
		if (baggageDescription == null) {
			if (other.baggageDescription != null)
				return false;
		} else if (!baggageDescription.equals(other.baggageDescription))
			return false;
		if (baggageName == null) {
			if (other.baggageName != null)
				return false;
		} else if (!baggageName.equals(other.baggageName))
			return false;
		if (defaultBggage != other.defaultBggage)
			return false;
		return true;
	}

}

