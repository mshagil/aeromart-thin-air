package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PaxCreditReinstateTO implements Serializable {

	private static final long serialVersionUID = 111223344L;

	public static final String EXTEND = "EXTEND";

	public static final String RE_INSTATE = "REINSTATE";

	private Integer paxID;

	private String pnr;

	private Date extendingDate;

	private String userNote;

	private Long creditId;

	private boolean blnGroupPnr = false;

	private BigDecimal creditAmount;
	
	/** operating carrier credit amount */
	private BigDecimal ocCreditAmount;

	private Integer transactionId;

	private String mode;
	
	private String carrierCode;

	public Integer getPaxID() {
		return paxID;
	}

	public void setPaxID(Integer paxID) {
		this.paxID = paxID;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Date getExtendingDate() {
		return extendingDate;
	}

	public void setExtendingDate(Date extendingDate) {
		this.extendingDate = extendingDate;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public Long getCreditId() {
		return creditId;
	}

	public void setCreditId(Long creditId) {
		this.creditId = creditId;
	}

	public boolean isBlnGroupPnr() {
		return blnGroupPnr;
	}

	public void setBlnGroupPnr(boolean blnGroupPnr) {
		this.blnGroupPnr = blnGroupPnr;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public BigDecimal getOcCreditAmount() {
		return ocCreditAmount;
	}

	public void setOcCreditAmount(BigDecimal ocCreditAmount) {
		this.ocCreditAmount = ocCreditAmount;
	}

}
