package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class BaseFareTO implements Serializable {

	private static final long serialVersionUID = 1L;

	protected BigDecimal amount;
	protected List<String> carrierCode;
	protected String segmentCode;
	protected List<String> applicablePassengerTypeCode;
	protected Date applicableTime;
	protected List<String> segmentRPHList;
	protected String logicalCCType;
	protected int ondSequence = -1;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public List<String> getApplicablePassengerTypeCode() {
		if (applicablePassengerTypeCode == null)
			applicablePassengerTypeCode = new ArrayList<String>();
		return applicablePassengerTypeCode;
	}

	public void setApplicablePassengerTypeCode(List<String> applicablePassengerTypeCode) {
		this.applicablePassengerTypeCode = applicablePassengerTypeCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public Date getApplicableTime() {
		return applicableTime;
	}

	public void setApplicableTime(Date applicableTime) {
		this.applicableTime = applicableTime;
	}

	public String getApplicableToDisplay() {
		String applicableTo = "";
		if (applicablePassengerTypeCode != null) {
			for (String applicable : applicablePassengerTypeCode) {
				applicableTo = applicableTo + "," + PaxTypeTO.getPaxTypeDisplayForAAPaxType(applicable);
				;
			}
		}
		if (!applicableTo.equals("")) {
			applicableTo = applicableTo.substring(1);
		}
		return applicableTo;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	@Override
	public BaseFareTO clone() {
		BaseFareTO clone = new BaseFareTO();
		clone.setCarrierCode(this.carrierCode);
		clone.setSegmentCode(this.getSegmentCode());
		clone.setAmount(this.getAmount());
		clone.getApplicablePassengerTypeCode().addAll(this.getApplicablePassengerTypeCode());
		clone.getSegmentRPHList().addAll(this.getSegmentRPHList());
		clone.setOndSequence(this.getOndSequence());
		return clone;
	}

	/**
	 * @return the carrierCode
	 */
	public List<String> getCarrierCode() {
		if (carrierCode == null)
			carrierCode = new ArrayList<String>();
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(List<String> carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the segmentRPHList
	 */
	public List<String> getSegmentRPHList() {
		if (segmentRPHList == null) {
			segmentRPHList = new ArrayList<String>();
		}
		return segmentRPHList;
	}

	/**
	 * @param segmentRPHList
	 *            the segmentRPHList to set
	 */
	public void setSegmentRPHList(List<String> segmentRPHList) {
		this.segmentRPHList = segmentRPHList;
	}

	/**
	 * @return the logicalCCType
	 */
	public String getLogicalCCType() {
		return logicalCCType;
	}

	/**
	 * @param logicalCCType
	 *            the logicalCCType to set
	 */
	public void setLogicalCCType(String logicalCCType) {
		this.logicalCCType = logicalCCType;
	}
}
