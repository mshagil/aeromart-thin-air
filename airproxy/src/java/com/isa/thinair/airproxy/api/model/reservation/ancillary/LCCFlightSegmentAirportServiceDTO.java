package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class LCCFlightSegmentAirportServiceDTO implements Serializable {
	private static final long serialVersionUID = -4536029206528945363L;

	private FlightSegmentTO flightSegmentTO;
	private List<LCCAirportServiceDTO> airportServices;

	/**
	 * @return the flightSegmentTO
	 */
	public FlightSegmentTO getFlightSegmentTO() {
		return flightSegmentTO;
	}

	/**
	 * @param flightSegmentTO
	 *            the flightSegmentTO to set
	 */
	public void setFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		this.flightSegmentTO = flightSegmentTO;
	}

	/**
	 * @return the lccAirportServiceDTOList
	 */
	public List<LCCAirportServiceDTO> getAirportServices() {
		if (airportServices == null) {
			airportServices = new ArrayList<LCCAirportServiceDTO>();
		}
		return airportServices;
	}

	/**
	 * @param airportServices
	 *            the airportServices to set
	 */
	public void setAirportServices(List<LCCAirportServiceDTO> airportServices) {
		this.airportServices = airportServices;
	}
}
