package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Map;

public class ServiceTaxQuoteCriteriaForNonTktDTO extends ServiceTaxQuoteCriteriaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private ProxyConstants.SYSTEM prefSystem;

	private Map<Integer, PaxCnxModPenChargeForServiceTaxDTO> paxWiseCharges;

	public ProxyConstants.SYSTEM getPrefSystem() {
		return prefSystem;
	}

	public void setPrefSystem(ProxyConstants.SYSTEM prefSystem) {
		this.prefSystem = prefSystem;
	}

	public Map<Integer, PaxCnxModPenChargeForServiceTaxDTO> getPaxWiseCharges() {
		return paxWiseCharges;
	}

	public void setPaxWiseCharges(Map<Integer, PaxCnxModPenChargeForServiceTaxDTO> paxWiseCharges) {
		this.paxWiseCharges = paxWiseCharges;
	}	
}
