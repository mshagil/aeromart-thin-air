package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.model.Reservation;

public class LCCClientOtherAirlineSegment implements Comparable<LCCClientOtherAirlineSegment>, Serializable {

	private Integer pnrOtherAirlineSegId;

	private String flightNumber;

	private String bookingCode;

	private String marketingFlightNumber;

	private String marketingBookingCode;

	private Date departureDateTimeLocal;

	private Date arrivalDateTimeLocal;

	private String status;

	private Integer flightSegId;

	private String segmentCode;

	private Reservation reservation;

	private String interlineGroupKey;

	private String cabinClassCode;
	
	public Integer getPnrOtherAirlineSegId() {
		return pnrOtherAirlineSegId;
	}

	public void setPnrOtherAirlineSegId(Integer pnrOtherAirlineSegId) {
		this.pnrOtherAirlineSegId = pnrOtherAirlineSegId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getMarketingFlightNumber() {
		return marketingFlightNumber;
	}

	public void setMarketingFlightNumber(String marketingFlightNumber) {
		this.marketingFlightNumber = marketingFlightNumber;
	}

	public String getMarketingBookingCode() {
		return marketingBookingCode;
	}

	public void setMarketingBookingCode(String marketingBookingCode) {
		this.marketingBookingCode = marketingBookingCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	@Override
	public int compareTo(LCCClientOtherAirlineSegment o) {
		return this.departureDateTimeLocal.compareTo(o.getDepartureDateTimeLocal());
	}

	public Date getDepartureDateTimeLocal() {
		return departureDateTimeLocal;
	}

	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	public Date getArrivalDateTimeLocal() {
		return arrivalDateTimeLocal;
	}

	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getInterlineGroupKey() {
		return interlineGroupKey;
	}

	public void setInterlineGroupKey(String interlineGroupKey) {
		this.interlineGroupKey = interlineGroupKey;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
}
