package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.DepAPFareDTO;
import com.isa.thinair.airpricing.api.model.FareRuleFee;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.model.InboundDepartureFrequency;
import com.isa.thinair.airpricing.api.model.OutboundDepartureFrequency;

public class FareRuleDTO implements Serializable, Comparable<FareRuleDTO> {

	private static final long serialVersionUID = 1L;

	private String orignNDest;

	private String comments;
	private String description;
	private String fareRuleCode;

	private Integer advanceBookingDays;

	private String status;

	private boolean returnFlag;

	private Long minimumStayoverMins;

	private Long minimumStayoverMonths;

	private Long maximumStayoverMins;

	private Long maximumStayoverMonths;

	private Date outboundDepatureTimeFrom;

	private Date outboundDepatureTimeTo;

	private Date inboundDepatureTimeFrom;

	private Date inboundDepatureTimeTo;

	private OutboundDepartureFrequency outboundDepatureFreqency;

	private InboundDepartureFrequency inboundDepatureFrequency;

	private double modificationChargeAmount;

	private String modificationChargeType;

	private double cancellationChargeAmount;

	private String cancellationChargeType;

	private String fareRuleDescription;

	private String fareRulesIdentifier;

	private String fareBasisCode;

	private Set<Integer> visibleChannelIds;

	private Set<String> visibileAgentCodes;

	private int fareRuleId;

	private String paxCategoryCode;

	private String fareCategoryCode;
	private String bookingClassCode;
	private int fareId;

	/** Adult fare characteristics */
	private boolean adultFareApplicable;
	private BigDecimal adultFareAmount;
	private boolean adultFareRefundable;

	/** Child fare characteristics */
	private boolean childFareApplicable;
	private BigDecimal childFareAmount;
	private boolean childFareRefundable;
	private String childFareType;

	/** Infant fare characteristics */
	private boolean infantFareApplicable;
	private BigDecimal infantFareAmount;
	private boolean infantFareRefundable;
	private String infantFareType;

	/** For sorting based on segment **/
	private long departureDateLong;
	private String visibleChannelNames;
	private String visibleAgents;

	private String currencyCode;

	private DepAPFareDTO depAPFareDTO;

	private DepAPFareDTO selectedCurrFareDTO;

	private Collection<FareRulePax> paxDetails;

	private String internationalModifyBufferTime;

	private String domesticModifyBufferTime;

	private String printExpDate;

	private boolean OpenReturnFlag;

	private Long openRetConfPeriod;

	private Long openRenConfPeriodMonths;

	private Set<Integer> flexiCodes;

	private Double minModificationAmount;

	private Double maximumModificationAmount;

	private Double minCancellationAmount;

	private Double maximumCancellationAmount;

	/* fare rule modifications and load factor */
	private String modifyByDate;

	private String modifyByRoute;

	private Integer loadFactorMin;

	private Integer loadFactorMax;

	private Integer fareDiscountMin;

	private Integer fareDiscountMax;

	private Collection<FareRuleFee> fees;

	private String agentInstructions;

	private String cabinClassCode;

	private int ondSequence = 0;

	private String applicableAgentCommission;

	private boolean feeTwlHrsCutOverApplied;

	private boolean halfRTFlag;

	private Double modChargeAmountInLocal = 0.0d;

	private Double cnxChargeAmountInLocal = 0.0d;

	private String localCurrencyCode;

	private boolean localCurrencySelected = false;

	private String agentCommissionType;

	private Double agentCommissionAmount;

	private Double agentCommissionAmountInLocal;
	
	private String bulkTicket;
	
	private String minBulkTicketSeatCount;

	private String fareRuleCommentInSelectedLocal;

	public String getOrignNDest() {
		return orignNDest;
	}

	public void setOrignNDest(String orignNDest) {
		this.orignNDest = orignNDest;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public String getFareCategoryCode() {
		return fareCategoryCode;
	}

	public void setFareCategoryCode(String fareCategoryCode) {
		this.fareCategoryCode = fareCategoryCode;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public boolean isAdultFareApplicable() {
		return adultFareApplicable;
	}

	public void setAdultFareApplicable(boolean adultFareApplicable) {
		this.adultFareApplicable = adultFareApplicable;
	}

	public BigDecimal getAdultFareAmount() {
		return adultFareAmount;
	}

	public void setAdultFareAmount(BigDecimal adultFareAmount) {
		this.adultFareAmount = adultFareAmount;
	}

	public boolean isAdultFareRefundable() {
		return adultFareRefundable;
	}

	public void setAdultFareRefundable(boolean adultFareRefundable) {
		this.adultFareRefundable = adultFareRefundable;
	}

	public boolean isChildFareApplicable() {
		return childFareApplicable;
	}

	public void setChildFareApplicable(boolean childFareApplicable) {
		this.childFareApplicable = childFareApplicable;
	}

	public BigDecimal getChildFareAmount() {
		return childFareAmount;
	}

	public void setChildFareAmount(BigDecimal childFareAmount) {
		this.childFareAmount = childFareAmount;
	}

	public boolean isChildFareRefundable() {
		return childFareRefundable;
	}

	public void setChildFareRefundable(boolean childFareRefundable) {
		this.childFareRefundable = childFareRefundable;
	}

	public String getChildFareType() {
		return childFareType;
	}

	public void setChildFareType(String childFareType) {
		this.childFareType = childFareType;
	}

	public boolean isInfantFareApplicable() {
		return infantFareApplicable;
	}

	public void setInfantFareApplicable(boolean infantFareApplicable) {
		this.infantFareApplicable = infantFareApplicable;
	}

	public BigDecimal getInfantFareAmount() {
		return infantFareAmount;
	}

	public void setInfantFareAmount(BigDecimal infantFareAmount) {
		this.infantFareAmount = infantFareAmount;
	}

	public boolean isInfantFareRefundable() {
		return infantFareRefundable;
	}

	public void setInfantFareRefundable(boolean infantFareRefundable) {
		this.infantFareRefundable = infantFareRefundable;
	}

	public String getInfantFareType() {
		return infantFareType;
	}

	public void setInfantFareType(String infantFareType) {
		this.infantFareType = infantFareType;
	}

	public long getDepartureDateLong() {
		return departureDateLong;
	}

	public void setDepartureDateLong(long departureDateLong) {
		this.departureDateLong = departureDateLong;
	}

	public String getVisibleAgents() {
		return visibleAgents;
	}

	public void setVisibleAgents(String visibleAgents) {
		this.visibleAgents = visibleAgents;
	}

	public String getVisibleChannelNames() {
		return visibleChannelNames;
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	public void setVisibleChannelNames(String visibleChannelNames) {
		this.visibleChannelNames = visibleChannelNames;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public DepAPFareDTO getDepAPFareDTO() {
		return depAPFareDTO;
	}

	public void setDepAPFareDTO(DepAPFareDTO depAPFareDTO) {
		this.depAPFareDTO = depAPFareDTO;
	}

	public DepAPFareDTO getSelectedCurrFareDTO() {
		return selectedCurrFareDTO;
	}

	public void setSelectedCurrFareDTO(DepAPFareDTO selectedCurrFareDTO) {
		this.selectedCurrFareDTO = selectedCurrFareDTO;
	}

	/**
	 * @return the advanceBookingDays
	 */
	public Integer getAdvanceBookingDays() {
		return advanceBookingDays;
	}

	/**
	 * @param advanceBookingDays
	 *            the advanceBookingDays to set
	 */
	public void setAdvanceBookingDays(Integer advanceBookingDays) {
		this.advanceBookingDays = advanceBookingDays;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the returnFlag
	 */
	public boolean getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            the returnFlag to set
	 */
	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return the minimumStayoverMins
	 */
	public Long getMinimumStayoverMins() {
		return minimumStayoverMins;
	}

	/**
	 * @param minimumStayoverMins
	 *            the minimumStayoverMins to set
	 */
	public void setMinimumStayoverMins(Long minimumStayoverMins) {
		this.minimumStayoverMins = minimumStayoverMins;
	}

	/**
	 * @return the minimumStayoverMonths
	 */
	public Long getMinimumStayoverMonths() {
		return minimumStayoverMonths;
	}

	/**
	 * @param minimumStayoverMonths
	 *            the minimumStayoverMonths to set
	 */
	public void setMinimumStayoverMonths(Long minimumStayoverMonths) {
		this.minimumStayoverMonths = minimumStayoverMonths;
	}

	/**
	 * @return the maximumStayoverMins
	 */
	public Long getMaximumStayoverMins() {
		return maximumStayoverMins;
	}

	/**
	 * @param maximumStayoverMins
	 *            the maximumStayoverMins to set
	 */
	public void setMaximumStayoverMins(Long maximumStayoverMins) {
		this.maximumStayoverMins = maximumStayoverMins;
	}

	/**
	 * @return the maximumStayoverMonths
	 */
	public Long getMaximumStayoverMonths() {
		return maximumStayoverMonths;
	}

	/**
	 * @param maximumStayoverMonths
	 *            the maximumStayoverMonths to set
	 */
	public void setMaximumStayoverMonths(Long maximumStayoverMonths) {
		this.maximumStayoverMonths = maximumStayoverMonths;
	}

	/**
	 * @return the outboundDepatureTimeFrom
	 */
	public Date getOutboundDepatureTimeFrom() {
		return outboundDepatureTimeFrom;
	}

	/**
	 * @param outboundDepatureTimeFrom
	 *            the outboundDepatureTimeFrom to set
	 */
	public void setOutboundDepatureTimeFrom(Date outboundDepatureTimeFrom) {
		this.outboundDepatureTimeFrom = outboundDepatureTimeFrom;
	}

	/**
	 * @return the outboundDepatureTimeTo
	 */
	public Date getOutboundDepatureTimeTo() {
		return outboundDepatureTimeTo;
	}

	/**
	 * @param outboundDepatureTimeTo
	 *            the outboundDepatureTimeTo to set
	 */
	public void setOutboundDepatureTimeTo(Date outboundDepatureTimeTo) {
		this.outboundDepatureTimeTo = outboundDepatureTimeTo;
	}

	/**
	 * @return the inboundDepatureTimeFrom
	 */
	public Date getInboundDepatureTimeFrom() {
		return inboundDepatureTimeFrom;
	}

	/**
	 * @param inboundDepatureTimeFrom
	 *            the inboundDepatureTimeFrom to set
	 */
	public void setInboundDepatureTimeFrom(Date inboundDepatureTimeFrom) {
		this.inboundDepatureTimeFrom = inboundDepatureTimeFrom;
	}

	/**
	 * @return the inboundDepatureTimeTo
	 */
	public Date getInboundDepatureTimeTo() {
		return inboundDepatureTimeTo;
	}

	/**
	 * @param inboundDepatureTimeTo
	 *            the inboundDepatureTimeTo to set
	 */
	public void setInboundDepatureTimeTo(Date inboundDepatureTimeTo) {
		this.inboundDepatureTimeTo = inboundDepatureTimeTo;
	}

	/**
	 * @return the outboundDepatureFreqency
	 */
	public OutboundDepartureFrequency getOutboundDepatureFreqency() {
		return outboundDepatureFreqency;
	}

	/**
	 * @param outboundDepatureFreqency
	 *            the outboundDepatureFreqency to set
	 */
	public void setOutboundDepatureFreqency(OutboundDepartureFrequency outboundDepatureFreqency) {
		this.outboundDepatureFreqency = outboundDepatureFreqency;
	}

	/**
	 * @return the inboundDepatureFrequency
	 */
	public InboundDepartureFrequency getInboundDepatureFrequency() {
		return inboundDepatureFrequency;
	}

	/**
	 * @param inboundDepatureFrequency
	 *            the inboundDepatureFrequency to set
	 */
	public void setInboundDepatureFrequency(InboundDepartureFrequency inboundDepatureFrequency) {
		this.inboundDepatureFrequency = inboundDepatureFrequency;
	}

	/**
	 * @return the modificationChargeAmount
	 */
	public double getModificationChargeAmount() {
		return modificationChargeAmount;
	}

	/**
	 * @param modificationChargeAmount
	 *            the modificationChargeAmount to set
	 */
	public void setModificationChargeAmount(double modificationChargeAmount) {
		this.modificationChargeAmount = modificationChargeAmount;
	}

	/**
	 * @return the modificationChargeType
	 */
	public String getModificationChargeType() {
		return modificationChargeType;
	}

	/**
	 * @param modificationChargeType
	 *            the modificationChargeType to set
	 */
	public void setModificationChargeType(String modificationChargeType) {
		this.modificationChargeType = modificationChargeType;
	}

	/**
	 * @return the cancellationChargeAmount
	 */
	public double getCancellationChargeAmount() {
		return cancellationChargeAmount;
	}

	/**
	 * @param cancellationChargeAmount
	 *            the cancellationChargeAmount to set
	 */
	public void setCancellationChargeAmount(double cancellationChargeAmount) {
		this.cancellationChargeAmount = cancellationChargeAmount;
	}

	/**
	 * @return the cancellationChargeType
	 */
	public String getCancellationChargeType() {
		return cancellationChargeType;
	}

	/**
	 * @param cancellationChargeType
	 *            the cancellationChargeType to set
	 */
	public void setCancellationChargeType(String cancellationChargeType) {
		this.cancellationChargeType = cancellationChargeType;
	}

	/**
	 * @return the fareRuleDescription
	 */
	public String getFareRuleDescription() {
		return fareRuleDescription;
	}

	/**
	 * @param fareRuleDescription
	 *            the fareRuleDescription to set
	 */
	public void setFareRuleDescription(String fareRuleDescription) {
		this.fareRuleDescription = fareRuleDescription;
	}

	/**
	 * @return the fareRulesIdentifier
	 */
	public String getFareRulesIdentifier() {
		return fareRulesIdentifier;
	}

	/**
	 * @param fareRulesIdentifier
	 *            the fareRulesIdentifier to set
	 */
	public void setFareRulesIdentifier(String fareRulesIdentifier) {
		this.fareRulesIdentifier = fareRulesIdentifier;
	}

	/**
	 * @return the visibleChannelIds
	 */
	public Set<Integer> getVisibleChannelIds() {
		return visibleChannelIds;
	}

	/**
	 * @param visibleChannelIds
	 *            the visibleChannelIds to set
	 */
	public void setVisibleChannelIds(Set<Integer> visibleChannelIds) {
		this.visibleChannelIds = visibleChannelIds;
	}

	/**
	 * @return the visibileAgentCodes
	 */
	public Set<String> getVisibileAgentCodes() {
		return visibileAgentCodes;
	}

	/**
	 * @param visibileAgentCodes
	 *            the visibileAgentCodes to set
	 */
	public void setVisibileAgentCodes(Set<String> visibileAgentCodes) {
		this.visibileAgentCodes = visibileAgentCodes;
	}

	/**
	 * @return the fareRuleId
	 */
	public int getFareRuleId() {
		return fareRuleId;
	}

	/**
	 * @param fareRuleId
	 *            the fareRuleId to set
	 */
	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	/**
	 * @return the paxCategoryCode
	 */
	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	/**
	 * @param paxCategoryCode
	 *            the paxCategoryCode to set
	 */
	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	/**
	 * @return the paxDetails
	 */
	public Collection<FareRulePax> getPaxDetails() {
		return paxDetails;
	}

	/**
	 * @param paxDetails
	 *            the paxDetails to set
	 */
	public void setPaxDetails(Collection<FareRulePax> paxDetails) {
		this.paxDetails = paxDetails;
	}

	/**
	 * @return the internationalModifyBufferTime
	 */
	public String getInternationalModifyBufferTime() {
		return internationalModifyBufferTime;
	}

	/**
	 * @param internationalModifyBufferTime
	 *            the internationalModifyBufferTime to set
	 */
	public void setInternationalModifyBufferTime(String internationalModifyBufferTime) {
		this.internationalModifyBufferTime = internationalModifyBufferTime;
	}

	/**
	 * @return the domesticModifyBufferTime
	 */
	public String getDomesticModifyBufferTime() {
		return domesticModifyBufferTime;
	}

	/**
	 * @param domesticModifyBufferTime
	 *            the domesticModifyBufferTime to set
	 */
	public void setDomesticModifyBufferTime(String domesticModifyBufferTime) {
		this.domesticModifyBufferTime = domesticModifyBufferTime;
	}

	/**
	 * @return the printExpDate
	 */
	public String getPrintExpDate() {
		return printExpDate;
	}

	/**
	 * @param printExpDate
	 *            the printExpDate to set
	 */
	public void setPrintExpDate(String printExpDate) {
		this.printExpDate = printExpDate;
	}

	/**
	 * @return the openReturnFlag
	 */
	public boolean getOpenReturnFlag() {
		return OpenReturnFlag;
	}

	/**
	 * @param openReturnFlag
	 *            the openReturnFlag to set
	 */
	public void setOpenReturnFlag(boolean openReturnFlag) {
		OpenReturnFlag = openReturnFlag;
	}

	/**
	 * @return the openRetConfPeriod
	 */
	public Long getOpenRetConfPeriod() {
		return openRetConfPeriod;
	}

	/**
	 * @param openRetConfPeriod
	 *            the openRetConfPeriod to set
	 */
	public void setOpenRetConfPeriod(Long openRetConfPeriod) {
		this.openRetConfPeriod = openRetConfPeriod;
	}

	/**
	 * @return the openRenConfPeriodMonths
	 */
	public Long getOpenRenConfPeriodMonths() {
		return openRenConfPeriodMonths;
	}

	/**
	 * @param openRenConfPeriodMonths
	 *            the openRenConfPeriodMonths to set
	 */
	public void setOpenRenConfPeriodMonths(Long openRenConfPeriodMonths) {
		this.openRenConfPeriodMonths = openRenConfPeriodMonths;
	}

	/**
	 * @return the flexiCodes
	 */
	public Set<Integer> getFlexiCodes() {
		return flexiCodes;
	}

	/**
	 * @param flexiCodes
	 *            the flexiCodes to set
	 */
	public void setFlexiCodes(Set<Integer> flexiCodes) {
		this.flexiCodes = flexiCodes;
	}

	/**
	 * @return the minModificationAmount
	 */
	public Double getMinModificationAmount() {
		return minModificationAmount;
	}

	/**
	 * @param minModificationAmount
	 *            the minModificationAmount to set
	 */
	public void setMinModificationAmount(Double minModificationAmount) {
		this.minModificationAmount = minModificationAmount;
	}

	/**
	 * @return the maximumModificationAmount
	 */
	public Double getMaximumModificationAmount() {
		return maximumModificationAmount;
	}

	/**
	 * @param maximumModificationAmount
	 *            the maximumModificationAmount to set
	 */
	public void setMaximumModificationAmount(Double maximumModificationAmount) {
		this.maximumModificationAmount = maximumModificationAmount;
	}

	/**
	 * @return the minCancellationAmount
	 */
	public Double getMinCancellationAmount() {
		return minCancellationAmount;
	}

	/**
	 * @param minCancellationAmount
	 *            the minCancellationAmount to set
	 */
	public void setMinCancellationAmount(Double minCancellationAmount) {
		this.minCancellationAmount = minCancellationAmount;
	}

	/**
	 * @return the maximumCancellationAmount
	 */
	public Double getMaximumCancellationAmount() {
		return maximumCancellationAmount;
	}

	/**
	 * @param maximumCancellationAmount
	 *            the maximumCancellationAmount to set
	 */
	public void setMaximumCancellationAmount(Double maximumCancellationAmount) {
		this.maximumCancellationAmount = maximumCancellationAmount;
	}

	/**
	 * @return the modifyByDate
	 */
	public String getModifyByDate() {
		return modifyByDate;
	}

	/**
	 * @param modifyByDate
	 *            the modifyByDate to set
	 */
	public void setModifyByDate(String modifyByDate) {
		this.modifyByDate = modifyByDate;
	}

	/**
	 * @return the modifyByRoute
	 */
	public String getModifyByRoute() {
		return modifyByRoute;
	}

	/**
	 * @param modifyByRoute
	 *            the modifyByRoute to set
	 */
	public void setModifyByRoute(String modifyByRoute) {
		this.modifyByRoute = modifyByRoute;
	}

	/**
	 * @return the loadFactorMin
	 */
	public Integer getLoadFactorMin() {
		return loadFactorMin;
	}

	/**
	 * @param loadFactorMin
	 *            the loadFactorMin to set
	 */
	public void setLoadFactorMin(Integer loadFactorMin) {
		this.loadFactorMin = loadFactorMin;
	}

	/**
	 * @return the loadFactorMax
	 */
	public Integer getLoadFactorMax() {
		return loadFactorMax;
	}

	/**
	 * @param loadFactorMax
	 *            the loadFactorMax to set
	 */
	public void setLoadFactorMax(Integer loadFactorMax) {
		this.loadFactorMax = loadFactorMax;
	}

	/**
	 * @return the fareDiscountMin
	 */
	public Integer getFareDiscountMin() {
		return fareDiscountMin;
	}

	/**
	 * @param fareDiscountMin
	 *            the fareDiscountMin to set
	 */
	public void setFareDiscountMin(Integer fareDiscountMin) {
		this.fareDiscountMin = fareDiscountMin;
	}

	/**
	 * @return the fareDiscountMax
	 */
	public Integer getFareDiscountMax() {
		return fareDiscountMax;
	}

	/**
	 * @param fareDiscountMax
	 *            the fareDiscountMax to set
	 */
	public void setFareDiscountMax(Integer fareDiscountMax) {
		this.fareDiscountMax = fareDiscountMax;
	}

	/**
	 * @return the fees
	 */
	public Collection<FareRuleFee> getFees() {
		return fees;
	}

	/**
	 * @param fees
	 *            the fees to set
	 */
	public void setFees(Collection<FareRuleFee> fees) {
		this.fees = fees;
	}

	/**
	 * @return the feeTwlHrsCutOverApplied
	 */
	public boolean getFeeTwlHrsCutOverApplied() {
		return feeTwlHrsCutOverApplied;
	}

	/**
	 * @param feeTwlHrsCutOverApplied
	 *            the feeTwlHrsCutOverApplied to set
	 */
	public void setFeeTwlHrsCutOverApplied(boolean feeTwlHrsCutOverApplied) {
		this.feeTwlHrsCutOverApplied = feeTwlHrsCutOverApplied;
	}

	/**
	 * @return the halfRTFlag
	 */
	public boolean getHalfRTFlag() {
		return halfRTFlag;
	}

	/**
	 * @param halfRTFlag
	 *            the halfRTFlag to set
	 */
	public void setHalfRTFlag(boolean halfRTFlag) {
		this.halfRTFlag = halfRTFlag;
	}

	/**
	 * @return the modChargeAmountInLocal
	 */
	public Double getModChargeAmountInLocal() {
		return modChargeAmountInLocal;
	}

	/**
	 * @param modChargeAmountInLocal
	 *            the modChargeAmountInLocal to set
	 */
	public void setModChargeAmountInLocal(Double modChargeAmountInLocal) {
		this.modChargeAmountInLocal = modChargeAmountInLocal;
	}

	/**
	 * @return the cnxChargeAmountInLocal
	 */
	public Double getCnxChargeAmountInLocal() {
		return cnxChargeAmountInLocal;
	}

	/**
	 * @param cnxChargeAmountInLocal
	 *            the cnxChargeAmountInLocal to set
	 */
	public void setCnxChargeAmountInLocal(Double cnxChargeAmountInLocal) {
		this.cnxChargeAmountInLocal = cnxChargeAmountInLocal;
	}

	/**
	 * @return the localCurrencyCode
	 */
	public String getLocalCurrencyCode() {
		return localCurrencyCode;
	}

	/**
	 * @param localCurrencyCode
	 *            the localCurrencyCode to set
	 */
	public void setLocalCurrencyCode(String localCurrencyCode) {
		this.localCurrencyCode = localCurrencyCode;
	}

	/**
	 * @return the localCurrencySelected
	 */
	public boolean isLocalCurrencySelected() {
		return localCurrencySelected;
	}

	/**
	 * @param localCurrencySelected
	 *            the localCurrencySelected to set
	 */
	public void setLocalCurrencySelected(boolean localCurrencySelected) {
		this.localCurrencySelected = localCurrencySelected;
	}

	/**
	 * @return the agentCommissionType
	 */
	public String getAgentCommissionType() {
		return agentCommissionType;
	}

	/**
	 * @param agentCommissionType
	 *            the agentCommissionType to set
	 */
	public void setAgentCommissionType(String agentCommissionType) {
		this.agentCommissionType = agentCommissionType;
	}

	/**
	 * @return the agentCommissionAmount
	 */
	public Double getAgentCommissionAmount() {
		return agentCommissionAmount;
	}

	/**
	 * @param agentCommissionAmount
	 *            the agentCommissionAmount to set
	 */
	public void setAgentCommissionAmount(Double agentCommissionAmount) {
		this.agentCommissionAmount = agentCommissionAmount;
	}

	/**
	 * @return the agentCommissionAmountInLocal
	 */
	public Double getAgentCommissionAmountInLocal() {
		return agentCommissionAmountInLocal;
	}

	/**
	 * @param agentCommissionAmountInLocal
	 *            the agentCommissionAmountInLocal to set
	 */
	public void setAgentCommissionAmountInLocal(Double agentCommissionAmountInLocal) {
		this.agentCommissionAmountInLocal = agentCommissionAmountInLocal;
	}

	public int compareTo(FareRuleDTO ruleDTO) {
		if (this.departureDateLong > ruleDTO.getDepartureDateLong()) {
			return 1; // After
		} else if (this.departureDateLong < ruleDTO.getDepartureDateLong()) {
			return -1; // Before
		} else {
			return 0; // Same Date
		}
	}

	/**
	 * @return the agentInstructions
	 */
	public String getAgentInstructions() {
		return agentInstructions;
	}

	/**
	 * @param agentInstructions
	 *            the agentInstructions to set
	 */
	public void setAgentInstructions(String agentInstructions) {
		this.agentInstructions = agentInstructions;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public String getApplicableAgentCommission() {
		return applicableAgentCommission;
	}

	public void setApplicableAgentCommission(String applicableAgentCommission) {
		this.applicableAgentCommission = applicableAgentCommission;
	}	

	public String getBulkTicket() {
		return bulkTicket;
	}

	public void setBulkTicket(String bulkTicket) {
		this.bulkTicket = bulkTicket;
	}

	public String getMinBulkTicketSeatCount() {
		return minBulkTicketSeatCount;
	}

	public void setMinBulkTicketSeatCount(String minBulkTicketSeatCount) {
		this.minBulkTicketSeatCount = minBulkTicketSeatCount;
	}

	public String getFareRuleCommentInSelectedLocal() {
		return fareRuleCommentInSelectedLocal;
	}

	public void setFareRuleCommentInSelectedLocal(String fareRuleCommentInSelectedLocal) {
		this.fareRuleCommentInSelectedLocal = fareRuleCommentInSelectedLocal;
	}

	@Override
	public String toString() {
		return "FareRuleDTO [orignNDest=" + orignNDest + ", comments=" + comments + ", description=" + description
				+ ", fareRuleCode=" + fareRuleCode + ", fareBasisCode=" + fareBasisCode + ", fareCategoryCode="
				+ fareCategoryCode + ", bookingClassCode=" + bookingClassCode + ", fareId=" + fareId + ", adultFareApplicable="
				+ adultFareApplicable + ", adultFareAmount=" + adultFareAmount + ", adultFareRefundable=" + adultFareRefundable
				+ ", childFareApplicable=" + childFareApplicable + ", childFareAmount=" + childFareAmount
				+ ", childFareRefundable=" + childFareRefundable + ", childFareType=" + childFareType + ", infantFareApplicable="
				+ infantFareApplicable + ", infantFareAmount=" + infantFareAmount + ", infantFareRefundable="
				+ infantFareRefundable + ", infantFareType=" + infantFareType + ", departureDateLong=" + departureDateLong
				+ ", visibleChannelNames=" + visibleChannelNames + ", visibleAgents=" + visibleAgents + ", currencyCode="
				+ currencyCode + ", depAPFareDTO=" + depAPFareDTO + ", selectedCurrFareDTO=" + selectedCurrFareDTO
				+ ", agentInstructions=" + agentInstructions + ", cabinClassCode=" + cabinClassCode + ", ondSequence="
				+ ondSequence + ", advanceBookingDays=" + advanceBookingDays + ", returnFlag=" + returnFlag
				+ ", minimumStayoverMins" + minimumStayoverMins + ", minimumStayoverMonths=" + minimumStayoverMonths
				+ ", maximumStayoverMins" + maximumStayoverMins + ", maximumStayoverMonths" + maximumStayoverMonths
				+ ", outboundDepatureTimeFrom" + outboundDepatureTimeFrom + ", outboundDepatureTimeTo=" + outboundDepatureTimeTo
				+ ", inboundDepatureTimeFrom=" + inboundDepatureTimeFrom + ", inboundDepatureTimeTo=" + inboundDepatureTimeTo
				+ ", modificationChargeAmount=" + modificationChargeAmount + ", modificationChargeType=" + modificationChargeType
				+ ", cancellationChargeAmount=" + cancellationChargeAmount + ", cancellationChargeType=" + cancellationChargeType
				+ ", fareRuleId=" + fareRuleId + ", bulkTicket=" + bulkTicket + ", minBulkTicketSeatCount=" + minBulkTicketSeatCount+"]";
	}


}
