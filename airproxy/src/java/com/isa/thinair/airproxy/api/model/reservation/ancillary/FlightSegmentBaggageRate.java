package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FlightSegmentBaggageRate implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<FlightSegmentLite> ondFlights;

	private Set<BaggageRateUnit> baggageRates;

	public List<FlightSegmentLite> getOndFlights() {
		if (ondFlights == null) {
			ondFlights = new ArrayList<>();
		}
		return ondFlights;
	}

	public void setOndFlights(List<FlightSegmentLite> ondFlights) {
		this.ondFlights = ondFlights;
	}

	public Set<BaggageRateUnit> getBaggageRates() {
		if (baggageRates == null) {
			baggageRates = new HashSet<>();
		}
		return baggageRates;
	}

	public void setBaggageRates(Set<BaggageRateUnit> baggageRates) {
		this.baggageRates = baggageRates;
	}
}