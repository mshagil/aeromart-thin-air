package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Hold LMS payment information
 * 
 * @author rumesh
 * 
 */
public class LCCClientLMSPaymentInfo implements LCCClientPaymentInfo, Serializable {

	private static final long serialVersionUID = 4970746951970273328L;

	private String loyaltyMemberAccountId;

	private String[] rewardIDs;

	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String totalAmountCurrencyCode;

	private PayCurrencyDTO payCurrencyDTO;

	private Integer paxSequence;

	private Date txnDateTime;

	private String carrierCode;

	private String lccUniqueTnxId;

	private String remarks;

	private BigDecimal payCurrencyAmount;

	private boolean includeExternalCharges;

	private String payReference;

	private Integer actualPaymentMethod;

	private String paymentMethod;

	private String originalPayReference;

	private String recieptNumber;

	private String carrierVisePayments;

	private String paymentTnxReference;

	private String originalPaymentUID;

	private Date paymentTnxDateTime;

	private Map<Integer, Map<String, BigDecimal>> paxProductRedeemedAmounts;

	private boolean offlinePayment = false;
	
	private boolean nonRefundable = false;	
	

	public String getLoyaltyMemberAccountId() {
		return loyaltyMemberAccountId;
	}

	public void setLoyaltyMemberAccountId(String loyaltyMemberAccountId) {
		this.loyaltyMemberAccountId = loyaltyMemberAccountId;
	}

	public String[] getRewardIDs() {
		return rewardIDs;
	}

	public void setRewardIDs(String[] rewardIDs) {
		this.rewardIDs = rewardIDs;
	}

	@Override
	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	@Override
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public PayCurrencyDTO getPayCurrencyDTO() {
		return this.payCurrencyDTO;
	}

	@Override
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	@Override
	public Integer getPaxSequence() {
		return this.paxSequence;
	}

	@Override
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	@Override
	public Date getTxnDateTime() {
		return this.txnDateTime;
	}

	@Override
	public void setTxnDateTime(Date date) {
		this.txnDateTime = date;
	}

	@Override
	public String getCarrierCode() {
		return this.carrierCode;
	}

	@Override
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	@Override
	public String getTotalAmountCurrencyCode() {
		return this.totalAmountCurrencyCode;
	}

	@Override
	public void setTotalAmountCurrencyCode(String totalAmountCurrencyCode) {
		this.totalAmountCurrencyCode = totalAmountCurrencyCode;
	}

	@Override
	public String getLccUniqueTnxId() {
		return this.lccUniqueTnxId;
	}

	@Override
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	@Override
	public String getRemarks() {
		return this.remarks;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;
	}

	@Override
	public BigDecimal getPayCurrencyAmount() {
		return this.payCurrencyAmount;
	}

	@Override
	public void setIncludeExternalCharges(boolean includeExternalCharges) {
		this.includeExternalCharges = includeExternalCharges;
	}

	@Override
	public boolean includeExternalCharges() {
		return this.includeExternalCharges;
	}

	@Override
	public String getPayReference() {
		return this.payReference;
	}

	@Override
	public void setPayReference(String payReference) {
		this.payReference = payReference;
	}

	@Override
	public Integer getActualPaymentMethod() {
		return this.actualPaymentMethod;
	}

	@Override
	public void setActualPaymentMethod(Integer actualPaymentMethod) {
		this.actualPaymentMethod = actualPaymentMethod;
	}

	@Override
	public String getOriginalPayReference() {
		return this.originalPayReference;
	}

	@Override
	public void setOriginalPayReference(String originalPayReference) {
		this.originalPayReference = originalPayReference;
	}

	@Override
	public String getRecieptNumber() {
		return this.recieptNumber;
	}

	@Override
	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	@Override
	public String getCarrierVisePayments() {
		return this.carrierVisePayments;
	}

	@Override
	public void setCarrierVisePayments(String carrierVisePayments) {
		this.carrierVisePayments = carrierVisePayments;
	}

	@Override
	public String getPaymentTnxReference() {
		return this.paymentTnxReference;
	}

	@Override
	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;
	}

	@Override
	public String getOriginalPaymentUID() {
		return this.originalPaymentUID;
	}

	@Override
	public void setOriginalPaymentUID(String originalPaymentUID) {
		this.originalPaymentUID = originalPaymentUID;
	}

	@Override
	public Date getPaymentTnxDateTime() {
		return this.paymentTnxDateTime;
	}

	@Override
	public void setPaymentTnxDateTime(Date paymentTnxDateTime) {
		this.paymentTnxDateTime = paymentTnxDateTime;
	}

	@Override
	public boolean isOfflinePayment() {
		return this.offlinePayment;
	}

	@Override
	public void setOfflinePayment(boolean isOfflinePayment) {
		this.offlinePayment = isOfflinePayment;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Map<Integer, Map<String, BigDecimal>> getPaxProductRedeemedAmounts() {
		return paxProductRedeemedAmounts;
	}

	public void setPaxProductRedeemedAmounts(Map<Integer, Map<String, BigDecimal>> paxProductRedeemedAmounts) {
		this.paxProductRedeemedAmounts = paxProductRedeemedAmounts;
	}

	@Override
	public boolean isNonRefundable() {
		return this.nonRefundable;
	}

	@Override
	public void setNonRefundable(boolean isNonRefundable) {
		this.nonRefundable = isNonRefundable;		
	}

}
