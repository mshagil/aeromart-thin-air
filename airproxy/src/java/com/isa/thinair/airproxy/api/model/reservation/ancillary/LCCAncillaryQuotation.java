package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;

public class LCCAncillaryQuotation implements Serializable {

	private List<LCCSelectedSegmentAncillaryDTO> segmentQuotations;

	private List<LCCInsuranceQuotationDTO> insuranceQuotations;
	
	private SYSTEM system;
	
	private String TransactionId;
	
	private String pnr;
	
	private boolean modifyAnci;

	public List<LCCSelectedSegmentAncillaryDTO> getSegmentQuotations() {
		return segmentQuotations;
	}

	public void setSegmentQuotations(List<LCCSelectedSegmentAncillaryDTO> segmentQuotations) {
		this.segmentQuotations = segmentQuotations;
	}

	public List<LCCInsuranceQuotationDTO> getInsuranceQuotations() {
		return insuranceQuotations;
	}

	public void setInsuranceQuotations(List<LCCInsuranceQuotationDTO> insuranceQuotations) {
		this.insuranceQuotations = insuranceQuotations;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public String getTransactionId() {
		return TransactionId;
	}

	public void setTransactionId(String transactionId) {
		TransactionId = transactionId;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isModifyAnci() {
		return modifyAnci;
	}

	public void setModifyAnci(boolean modifyAnci) {
		this.modifyAnci = modifyAnci;
	}
	
}
