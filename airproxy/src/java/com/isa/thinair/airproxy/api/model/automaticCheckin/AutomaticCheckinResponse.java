package com.isa.thinair.airproxy.api.model.automaticCheckin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;

public class AutomaticCheckinResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<AutomaticCheckinDTO> automaticCheckinDTO;

	public List<AutomaticCheckinDTO> getAutomaticCheckinDTO() {
		if (automaticCheckinDTO == null) {
			automaticCheckinDTO = new ArrayList<AutomaticCheckinDTO>();
		}
		return automaticCheckinDTO;
	}

	public void setAutomaticCheckinDto(List<AutomaticCheckinDTO> automaticCheckinDTO) {
		this.automaticCheckinDTO = automaticCheckinDTO;
	}

}
