/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold pax credit payment data transfer information
 */
public class LCCClientPaxCreditPaymentInfo implements Serializable, LCCClientPaymentInfo {

	private static final long serialVersionUID = 1L;

	/** Hold total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal paxCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String totalAmountCurrencyCode;

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	private Integer debitPaxRefNo;

	private String description;

	private Integer paxSequence;

	private Date txnDateTime;

	private String carrierCode;

	private String lccUniqueTnxId;

	private String remarks;

	private BigDecimal payCurrencyAmount;

	private boolean includeExternalCharges;

	private String payReference;

	private Integer actualPaymentMethod;

	private String originalPayReference;

	private String recieptNumber;

	private BigDecimal residingCarrierAmount;

	private Date expiryDate;

	private String paxCreditId;

	private String residingPnr;

	private String carrierVisePayments;

	private String paymentTnxReference;

	private String originalPaymentUID;

	private Date paymentTnxDateTime;

	private boolean offlinePayment = false;
	
	private boolean nonRefundable = false;	

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return the paxSequence
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            the paxSequence to set
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	public Date getTxnDateTime() {
		return this.txnDateTime;
	}

	public void setTxnDateTime(Date txnDateTime) {
		this.txnDateTime = txnDateTime;
	}

	@Override
	public LCCClientPaxCreditPaymentInfo clone() {
		LCCClientPaxCreditPaymentInfo lccClientPaxCreditPaymentInfo = new LCCClientPaxCreditPaymentInfo();
		lccClientPaxCreditPaymentInfo.setDebitPaxRefNo(this.getDebitPaxRefNo());
		lccClientPaxCreditPaymentInfo.setTotalAmount(this.getTotalAmount());
		lccClientPaxCreditPaymentInfo.setTotalAmountCurrencyCode(this.getTotalAmountCurrencyCode());
		lccClientPaxCreditPaymentInfo.setPayCurrencyDTO((PayCurrencyDTO) this.getPayCurrencyDTO().clone());
		lccClientPaxCreditPaymentInfo.setPaxSequence(this.getPaxSequence());
		lccClientPaxCreditPaymentInfo.setLccUniqueTnxId(this.getLccUniqueTnxId());
		lccClientPaxCreditPaymentInfo.setDescription(this.getDescription());
		lccClientPaxCreditPaymentInfo.setPaymentTnxReference(this.getPaymentTnxReference());
		lccClientPaxCreditPaymentInfo.setOriginalPaymentUID(this.getOriginalPaymentUID());
		lccClientPaxCreditPaymentInfo.setPaymentTnxDateTime(this.getPaymentTnxDateTime());

		return lccClientPaxCreditPaymentInfo;
	}

	/**
	 * @return the totalAmountCurrencyCode
	 */
	public String getTotalAmountCurrencyCode() {
		return totalAmountCurrencyCode;
	}

	/**
	 * @param totalAmountCurrencyCode
	 *            the totalAmountCurrencyCode to set
	 */
	public void setTotalAmountCurrencyCode(String totalAmountCurrencyCode) {
		this.totalAmountCurrencyCode = totalAmountCurrencyCode;
	}

	/**
	 * @return the lccUniqueTnxId
	 */
	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	/**
	 * @param lccUniqueTnxId
	 *            the lccUniqueTnxId to set
	 */
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	/**
	 * @return the debitPaxRefNo
	 */
	public Integer getDebitPaxRefNo() {
		return debitPaxRefNo;
	}

	/**
	 * @param debitPaxRefNo
	 *            the debitPaxRefNo to set
	 */
	public void setDebitPaxRefNo(Integer debitPaxRefNo) {
		this.debitPaxRefNo = debitPaxRefNo;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getRemarks() {
		return remarks;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;

	}

	@Override
	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	@Override
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;

	}

	@Override
	public boolean includeExternalCharges() {
		return includeExternalCharges;
	}

	@Override
	public void setIncludeExternalCharges(boolean includeExternalCharges) {
		this.includeExternalCharges = includeExternalCharges;
	}

	public String getPayReference() {
		return payReference;
	}

	public void setPayReference(String payReference) {
		this.payReference = payReference;
	}

	public String getOriginalPayReference() {
		return originalPayReference;
	}

	public Integer getActualPaymentMethod() {

		return actualPaymentMethod;
	}

	public void setActualPaymentMethod(Integer actualPaymentMethod) {

		this.actualPaymentMethod = actualPaymentMethod;
	}

	public void setOriginalPayReference(String originalPayReference) {
		this.originalPayReference = originalPayReference;
	}

	public String getRecieptNumber() {
		return recieptNumber;
	}

	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	public BigDecimal getResidingCarrierAmount() {
		return residingCarrierAmount;
	}

	public void setResidingCarrierAmount(BigDecimal residingCarrierAmount) {
		this.residingCarrierAmount = residingCarrierAmount;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPaxCreditId() {
		return paxCreditId;
	}

	public void setPaxCreditId(String paxCreditId) {
		this.paxCreditId = paxCreditId;
	}

	public String getResidingPnr() {
		return residingPnr;
	}

	public void setResidingPnr(String residingPnr) {
		this.residingPnr = residingPnr;
	}

	public void setPaxCreditAmount(BigDecimal paxCreditAmount) {
		this.paxCreditAmount = paxCreditAmount;
	}

	public BigDecimal getPaxCreditAmount() {
		return paxCreditAmount;
	}

	/**
	 * @return the carrierVisePayments
	 */
	public String getCarrierVisePayments() {
		return carrierVisePayments;
	}

	/**
	 * @param carrierVisePayments
	 *            the carrierVisePayments to set
	 */
	public void setCarrierVisePayments(String carrierVisePayments) {
		this.carrierVisePayments = carrierVisePayments;
	}

	public String getPaymentTnxReference() {
		return paymentTnxReference;
	}

	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;
	}

	public String getOriginalPaymentUID() {
		return originalPaymentUID;
	}

	public void setOriginalPaymentUID(String originalPaymentUID) {
		this.originalPaymentUID = originalPaymentUID;
	}

	public Date getPaymentTnxDateTime() {
		return this.paymentTnxDateTime;
	}

	public void setPaymentTnxDateTime(Date paymentTnxDateTime) {
		this.paymentTnxDateTime = paymentTnxDateTime;
	}

	public boolean isOfflinePayment() {
		return this.offlinePayment;
	}

	public void setOfflinePayment(boolean isOfflinePayment) {
		this.offlinePayment = isOfflinePayment;
	}

	@Override
	public boolean isNonRefundable() {
		return this.nonRefundable;
	}

	@Override
	public void setNonRefundable(boolean isNonRefundable) {
		this.nonRefundable = isNonRefundable;
	}
}