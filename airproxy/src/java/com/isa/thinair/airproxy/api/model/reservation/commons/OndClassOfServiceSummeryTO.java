package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author rumesh
 * 
 */
public class OndClassOfServiceSummeryTO implements Serializable, Comparable<OndClassOfServiceSummeryTO> {
	private static final long serialVersionUID = -3484057172917993870L;

	private String ondCode;
	private boolean isInbound;
	private Integer sequence;
	private List<LogicalCabinClassInfoTO> availableLogicalCCList;
	private Integer requestedOndSequence;

	public OndClassOfServiceSummeryTO() {

	}

	public OndClassOfServiceSummeryTO(String ondCode, boolean isInbound, int sequence, Integer requestedOndSequence,
			List<LogicalCabinClassInfoTO> availableLogicalCCList) {
		super();
		this.ondCode = ondCode;
		this.isInbound = isInbound;
		this.sequence = sequence;
		this.availableLogicalCCList = availableLogicalCCList;
		if (requestedOndSequence != null) {
			this.requestedOndSequence = requestedOndSequence;
		} else {
			this.requestedOndSequence = sequence;
		}
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public boolean isInbound() {
		return isInbound;
	}

	public void setInbound(boolean isInbound) {
		this.isInbound = isInbound;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Integer getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public void setRequestedOndSequence(Integer requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	public List<LogicalCabinClassInfoTO> getAvailableLogicalCCList() {
		return availableLogicalCCList;
	}

	public void setAvailableLogicalCCList(List<LogicalCabinClassInfoTO> availableLogicalCCList) {
		this.availableLogicalCCList = availableLogicalCCList;
	}

	@Override
	public int compareTo(OndClassOfServiceSummeryTO obj) {
		return this.sequence.compareTo(obj.getSequence());
	}
}
