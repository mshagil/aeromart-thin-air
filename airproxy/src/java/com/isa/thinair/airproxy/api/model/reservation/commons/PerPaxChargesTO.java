package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author nafly
 * 
 */
public class PerPaxChargesTO implements Serializable {

	private static final long serialVersionUID = 1578956423551456374L;

	public PerPaxChargesTO(String paxType) {
		this.passengerType = paxType;
	}

	// AccelAero Pax Type
	private String passengerType;

	private BigDecimal totalFare;

	private BigDecimal totalBaseFare;
	
	private BigDecimal totalTax;

	private BigDecimal totalCharges;
	
	private BigDecimal totalChargesInSelectedCur;

	/**
	 * @return the passengerType
	 */
	public String getPassengerType() {
		return passengerType;
	}

	/**
	 * @param passengerType
	 *            the passengerType to set
	 */
	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	/**
	 * @return the totalFare
	 */
	public BigDecimal getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            the totalFare to set
	 */
	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	/**
	 * @return the totalBaseFare
	 */
	public BigDecimal getTotalBaseFare() {
		return totalBaseFare;
	}

	/**
	 * @param totalBaseFare
	 *            the totalBaseFare to set
	 */
	public void setTotalBaseFare(BigDecimal totalBaseFare) {
		this.totalBaseFare = totalBaseFare;
	}

	/**
	 * @return the totalTax
	 */
	public BigDecimal getTotalTax() {
		return totalTax;
	}

	/**
	 * @param totalTax
	 *            the totalTax to set
	 */
	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	/**
	 * @return the totalCharges
	 */
	public BigDecimal getTotalCharges() {
		return totalCharges;
	}

	/**
	 * @param totalCharges
	 *            the totalCharges to set
	 */
	public void setTotalCharges(BigDecimal totalCharges) {
		this.totalCharges = totalCharges;
	}
	
	/**
	 * @return the totalChargesInSelectedCur
	 */
	public BigDecimal getTotalChargesInSelectedCur() {
		return totalChargesInSelectedCur;
	}

	/**
	 * @param totalChargesInSelectedCur
	 *            the totalChargesInSelectedCur to set
	 */
	public void setTotalChargesInSelectedCur(BigDecimal totalChargesInSelectedCur) {
		this.totalChargesInSelectedCur = totalChargesInSelectedCur;
	}

}
