package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DiscountedFareDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float farePercentage;
	private String notes;
	private String code;
	// for domestic fare discount
	private int domFareDiscountPercentage;
	private boolean isDomesticDiscountApplied;
	private ArrayList<String> domesticSegmentCodeList = new ArrayList<String>();
	private boolean oldFareDiscountApplied;

	// Promo Code related parameters
	private Long promotionId;
	private String promoCode;
	private String promotionType;
	private String description;
	private boolean systemGenerated;
	private boolean allowSamePaxOnly;
	private String originPnr;
	private String discountType;
	private int discountApplyTo;
	private String discountAs;
	private Map<String, Integer> applicablePaxCount;
	private List<String> applicableAncillaries;
	private Set<Integer> applicableBINs;
	private Set<String> applicableOnds;
	private boolean applicableForOneway;
	private boolean applicableForReturn;
	private String applicability;

	public float getFarePercentage() {
		return farePercentage;
	}

	public void setFarePercentage(float farePercentage) {
		this.farePercentage = farePercentage;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getDomFareDiscountPercentage() {
		return domFareDiscountPercentage;
	}

	public void setDomFareDiscountPercentage(int domFareDiscountPercentage) {
		this.domFareDiscountPercentage = domFareDiscountPercentage;
	}

	public boolean isDomesticDiscountApplied() {
		return isDomesticDiscountApplied;
	}

	public void setDomesticDiscountApplied(boolean isDomesticDiscountApplied) {
		this.isDomesticDiscountApplied = isDomesticDiscountApplied;
	}

	public ArrayList<String> getDomesticSegmentCodeList() {
		return domesticSegmentCodeList;
	}

	public void setDomesticSegmentCodeList(ArrayList<String> domesticSegmentCodeList) {
		this.domesticSegmentCodeList = domesticSegmentCodeList;
	}

	public boolean isOldFareDiscountApplied() {
		return oldFareDiscountApplied;
	}

	public void setOldFareDiscountApplied(boolean oldFareDiscountApplied) {
		this.oldFareDiscountApplied = oldFareDiscountApplied;
	}

	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	public String getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public boolean isSystemGenerated() {
		return systemGenerated;
	}

	public void setSystemGenerated(boolean systemGenerated) {
		this.systemGenerated = systemGenerated;
	}

	public boolean isAllowSamePaxOnly() {
		return allowSamePaxOnly;
	}

	public void setAllowSamePaxOnly(boolean allowSamePaxOnly) {
		this.allowSamePaxOnly = allowSamePaxOnly;
	}

	public String getOriginPnr() {
		return originPnr;
	}

	public void setOriginPnr(String originPnr) {
		this.originPnr = originPnr;
	}

	public int getDiscountApplyTo() {
		return discountApplyTo;
	}

	public void setDiscountApplyTo(int discountApplyTo) {
		this.discountApplyTo = discountApplyTo;
	}

	public String getDiscountAs() {
		return discountAs;
	}

	public void setDiscountAs(String discountAs) {
		this.discountAs = discountAs;
	}

	public Map<String, Integer> getApplicablePaxCount() {
		if (applicablePaxCount == null) {
			applicablePaxCount = new HashMap<String, Integer>();
		}
		return applicablePaxCount;
	}

	public void setApplicablePaxCount(Map<String, Integer> applicablePaxCount) {
		this.applicablePaxCount = applicablePaxCount;
	}

	public void addApplicablePaxCount(String paxType, Integer count) {
		getApplicablePaxCount().put(paxType, count);
	}

	public Integer getPaxCount(String paxType) {
		return getApplicablePaxCount().get(paxType);
	}

	public List<String> getApplicableAncillaries() {
		return applicableAncillaries;
	}

	public void setApplicableAncillaries(List<String> applicableAncillaries) {
		this.applicableAncillaries = applicableAncillaries;
	}

	public Set<Integer> getApplicableBINs() {
		return applicableBINs;
	}

	public void setApplicableBINs(Set<Integer> applicableBINs) {
		this.applicableBINs = applicableBINs;
	}

	public Set<String> getApplicableOnds() {
		return applicableOnds;
	}

	public void setApplicableOnds(Set<String> applicableOnds) {
		this.applicableOnds = applicableOnds;
	}

	public boolean isApplicableForOneway() {
		return applicableForOneway;
	}

	public void setApplicableForOneway(boolean applicableForOneway) {
		this.applicableForOneway = applicableForOneway;
	}

	public boolean isApplicableForReturn() {
		return applicableForReturn;
	}

	public void setApplicableForReturn(boolean applicableForReturn) {
		this.applicableForReturn = applicableForReturn;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

}
