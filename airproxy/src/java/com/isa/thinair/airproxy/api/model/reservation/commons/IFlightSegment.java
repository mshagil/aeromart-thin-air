package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.util.Date;

public interface IFlightSegment {

	public String getSegmentCode();

	public Date getDepartureDateTime();

	public Date getArrivalDateTime();

	public Date getDepartureDateTimeZulu();

	public Date getArrivalDateTimeZulu();

}