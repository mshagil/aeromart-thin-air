package com.isa.thinair.airproxy.api.model.automaticCheckin;

import java.io.Serializable;

public class AutomaticCheckinRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String departAirPortCode;

	public String getDepartAirPortCode() {
		return departAirPortCode;
	}

	public void setDepartAirPortCode(String departAirPortCode) {
		this.departAirPortCode = departAirPortCode;
	}

}
