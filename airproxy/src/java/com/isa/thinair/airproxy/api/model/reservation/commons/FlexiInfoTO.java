package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

public class FlexiInfoTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int availableCount;

	private int flexiRateId;

	private int flexibilityTypeId;

	private long cutOverBufferInMins;

	public int getAvailableCount() {
		return availableCount;
	}

	public void setAvailableCount(int availableCount) {
		this.availableCount = availableCount;
	}

	public int getFlexiRateId() {
		return flexiRateId;
	}

	public void setFlexiRateId(int flexiRateId) {
		this.flexiRateId = flexiRateId;
	}

	public int getFlexibilityTypeId() {
		return flexibilityTypeId;
	}

	public void setFlexibilityTypeId(int flexibilityTypeId) {
		this.flexibilityTypeId = flexibilityTypeId;
	}

	public long getCutOverBufferInMins() {
		return cutOverBufferInMins;
	}

	public void setCutOverBufferInMins(long cutOverBufferInMins) {
		this.cutOverBufferInMins = cutOverBufferInMins;
	}

	public FlexiInfoTO clone() {
		FlexiInfoTO clone = new FlexiInfoTO();
		clone.setAvailableCount(this.getAvailableCount());
		clone.setCutOverBufferInMins(this.getCutOverBufferInMins());
		clone.setFlexibilityTypeId(this.getFlexibilityTypeId());
		clone.setFlexiRateId(this.getFlexiRateId());
		return clone;
	}
}
