package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.util.ArrayList;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.VoucherPaymentOption;
import com.isa.thinair.commons.api.dto.VoucherDTO;

/**
 * @author chethiya
 *
 */
public class VoucherRedeemResponse implements Serializable{

	private static final long serialVersionUID = 6070648035418151374L;

	private ArrayList<VoucherDTO> redeemVoucherList = new ArrayList<VoucherDTO>();

	private String redeemedTotal;

	private String vouchersTotal;

	private String balTotalPay;

	private String balanceToPaySelectedCurrency;

	private VoucherPaymentOption voucherPaymentOption = VoucherPaymentOption.NONE;
	
	private String otp;

	/**
	 * @return the redeemedVoucherLists
	 */
	public ArrayList<VoucherDTO> getRedeemVoucherList() {
		return redeemVoucherList;
	}

	/**
	 * @param redeemedVoucherLists
	 *            the redeemedVoucherLists to set
	 */
	public void setRedeemVoucherList(ArrayList<VoucherDTO> redeemVoucherList) {
		this.redeemVoucherList = redeemVoucherList;
	}

	/**
	 * @return the redeemedTotal
	 */
	public String getRedeemedTotal() {
		return redeemedTotal;
	}

	/**
	 * @param redeemedTotal
	 *            the redeemedTotal to set
	 */
	public void setRedeemedTotal(String redeemedTotal) {
		this.redeemedTotal = redeemedTotal;
	}

	/**
	 * @return the vouchersTotal
	 */
	public String getVouchersTotal() {
		return vouchersTotal;
	}

	/**
	 * @param vouchersTotal
	 *            the vouchersTotal to set
	 */
	public void setVouchersTotal(String vouchersTotal) {
		this.vouchersTotal = vouchersTotal;
	}

	/**
	 * @return the balTotalPay
	 */
	public String getBalTotalPay() {
		return balTotalPay;
	}

	/**
	 * @param balTotalPay the balTotalPay to set
	 */
	public void setBalTotalPay(String balTotalPay) {
		this.balTotalPay = balTotalPay;
	}

	/**
	 * @return the voucherPaymentOption
	 */
	public VoucherPaymentOption getVoucherPaymentOption() {
		return voucherPaymentOption;
	}

	/**
	 * @param voucherPaymentOption the voucherPaymentOption to set
	 */
	public void setVoucherPaymentOption(VoucherPaymentOption voucherPaymentOption) {
		this.voucherPaymentOption = voucherPaymentOption;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getBalanceToPaySelectedCurrency() {
		return balanceToPaySelectedCurrency;
	}

	public void setBalanceToPaySelectedCurrency(String balanceToPaySelcur) {
		this.balanceToPaySelectedCurrency = balanceToPaySelcur;
	}

}
