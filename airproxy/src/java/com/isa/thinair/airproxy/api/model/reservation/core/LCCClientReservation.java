/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.dto.ChargeAdjustmentPrivilege;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airreservation.api.dto.RefundableChargeDetailDTO;
import com.isa.thinair.airreservation.api.dto.StationContactDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * LCCClientReservation is the entity class to represent a LCConnect Reservation model. This will keep track of
 * LCConnect main reservation information
 * 
 * @author Nilindra Fernando
 */
public class LCCClientReservation implements Serializable {

	private static final long serialVersionUID = 1L;

	// Reservation related
	private String pnr;
	/* indicate whether reservation pnr is a group pnr or not */
	private boolean groupPNR;
	private String marketingCarrier;

	private Map<String, String> carrierPNR;
	private Map<String, String> carrierName;

	/** Holds booking time stamp */
	private Date zuluBookingTimestamp;

	/** Holds release time stamp */
	private Date zuluReleaseTimeStamp;

	/** Holds reservation status */
	private String status;

	/** Holds reservation display status */
	private String displayStatus;

	// Contact and administrator related
	/** Holds contact information */
	private CommonReservationContactInfo contactInfo;
	/** Holds staff information */
	private CommonReservationAdminInfo adminInfo;

	private CommonReservationPreferrenceInfo preferrenceInfo;

	/** Hold the Last User Note */
	private String lastUserNote;

	/** Hold the Last User Note Type */
	private String UserNoteType;

	private String fatourati;

	// Segments related
	/** Holds a set of LCCReservationSegment */
	private Set<LCCClientReservationSegment> segments;

	private Set<LCCClientOtherAirlineSegment> othertAirlineSegments;

	// Passenger related
	/** Holds a set of ReservationPax */
	private Set<LCCClientReservationPax> passengers;

	// Count related
	/** Holds total passenger count */
	private int totalPaxAdultCount;
	/** Holds total child count */
	private int totalPaxChildCount;
	/** Holds total infant count */
	private int totalPaxInfantCount;

	// Ticket Amount related
	/** Holds total ticket fare */
	private BigDecimal totalTicketFare = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket sur-charge */
	private BigDecimal totalTicketSurCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket tax-charge */
	private BigDecimal totalTicketTaxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket cancel charge */
	private BigDecimal totalTicketCancelCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket modification charge */
	private BigDecimal totalTicketModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket adjustment charge */
	private BigDecimal totalTicketAdjustmentCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger paid amount */
	private BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger available credit or debit amount */
	private BigDecimal totalAvailableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket price */
	private BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Actual payments in paid currency (for display purposes only) */
	private LCCClientPaymentHolder lccClientPaymentHolder;

	/** Holds fare/ charges information */
	private List<TaxTO> taxes;
	private List<FeeTO> fees;
	private List<SurchargeTO> surcharges;

	private String version;

	private List<LCCClientCarrierOndGroup> carrierOndGrouping;

	private List<LCCClientReservationInsurance> reservationInsurances;

	/* Hold Alert infomation */
	private List<LCCClientAlertInfoTO> alertInfoTOs;

	/* Hold Flexi information */
	private List<LCCClientAlertInfoTO> flexiAlertInfoTOs;

	private String lastCurrencyCode;

	private Date lastModificationTimestamp;

	/** Hold general information that needs to be shown with the segment */
	private List<LCCClientAlertInfoTO> segmentInfoTOs;

	private List<LCCClientAlertInfoTO> openReturnSegmentInfoTOs;

	private Map<EXTERNAL_CHARGES, BigDecimal> externalChargesSummary;

	private List<String> interlineModificationParams;

	private boolean useOtherCarrierPaxCredit;

	private String paymentRef;// capture offline payment refrence number

	/**
	 * The flag indicating whether this reservation is a modifiable reservation after it's creation. This is
	 * specifically used to identify reservation made via external channels like GDSs which shouldn't without proper
	 * communication with the external systems.
	 */
	private boolean modifiableReservation;

	/** Holds the charge adjustment privileges for interline carriers associated with this reservation */
	private Map<String, List<ChargeAdjustmentPrivilege>> interlineChargeAdjustmentPrivileges;

	private String endorsementNote;

	private String bookingCategory;

	private String originCountryOfCall;

	private String itineraryFareMask;

	private Date ticketValidTill;

	private Date ticketValidFrom;

	private Date dateOfIssue = null;

	/**
	 * Property to denote a successful refund.
	 */
	private boolean successfulRefund;

	private boolean allowVoidReservation;

	private boolean infantPaymentSeparated;

	private AutoCancellationInfo autoCancellationInfo;

	private Integer gdsId;

	private Set<StationContactDTO> stationContacts;

	private long nameChangeCutoffTime;

	private String nameChangeThresholdTimePerStation;

	private LCCOfflinePaymentDetails lccOfflinePaymentDetails;

	/**
	 * Map containing refundable charge details for this reservation. Key -> pnrPaxID, Value -> collection of
	 * RefundableChargeDetailDTO containing the refundable charge amount data
	 */
	private Map<String, Collection<RefundableChargeDetailDTO>> refundableChargeDetails;

	/** Holds applied promotion information */
	private LCCPromotionInfoTO lccPromotionInfoTO;

	private long groupBookingRequestID;

	/** Holds ond wise bundled fare DTOs */
	private List<BundledFareDTO> bundledFareDTOs;

	// Holds service taxes applicable for OHD reservations
	private Set<ServiceTaxContainer> applicableServiceTaxes;

	private Integer nameChangeCount = 0;

	private String reasonForAllowBlPax;

	private String externalPos;

	private String externalRecordLocator;

	private String salesChannel;

	private BigDecimal totalGOQUOAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private List<TaxInvoice> taxInvoicesList;

	private boolean useEtsForReservation;
	/** true if self re-protection has done */
	private boolean actionedByIBE = false;

	/**
	 * Return total charge amounts ==> [0]fare [1]tax [2]surcharge [3]cancelcharge [4]modificationcharge
	 * [5]adjustmentcharge [6]discount
	 * 
	 * @return
	 */
	public BigDecimal[] getTotalTicketAmounts() {
		return new BigDecimal[] { this.getTotalTicketFare(), this.getTotalTicketTaxCharge(), this.getTotalTicketSurCharge(),
				this.getTotalTicketCancelCharge(), this.getTotalTicketModificationCharge(), this.getTotalTicketAdjustmentCharge(),
				this.getTotalDiscount() };
	}

	/**
	 * Reset total charges
	 */
	public void resetTotalTicketAmounts() {
		this.setTotalTicketAmounts(ReservationApiUtils.getChargeTypeAmounts());
	}

	/**
	 * Set total charge amounts
	 * 
	 * @param totalCharges
	 */
	public void setTotalTicketAmounts(BigDecimal[] totalChargeAmounts) {
		this.setTotalTicketFare(totalChargeAmounts[0]);
		this.setTotalTicketTaxCharge(totalChargeAmounts[1]);
		this.setTotalTicketSurCharge(totalChargeAmounts[2]);
		this.setTotalTicketCancelCharge(totalChargeAmounts[3]);
		this.setTotalTicketModificationCharge(totalChargeAmounts[4]);
		this.setTotalTicketAdjustmentCharge(totalChargeAmounts[5]);
	}

	/**
	 * Add reservation segment
	 * 
	 * @param reservationSegment
	 */
	public void addSegment(LCCClientReservationSegment lccReservationSegment) {
		if (this.getSegments() == null) {
			this.setSegments(new HashSet<LCCClientReservationSegment>());
		}
		// TODO Review
		// No Need to Presentation
		// lccReservationSegment.setLccReservation(this);
		this.getSegments().add(lccReservationSegment);
	}

	public void addOtherAirlineSegment(LCCClientOtherAirlineSegment lccOtherAirlineSegment) {
		if (this.getOthertAirlineSegments() == null) {
			this.setOthertAirlineSegments(new HashSet<LCCClientOtherAirlineSegment>());
		}
		this.getOthertAirlineSegments().add(lccOtherAirlineSegment);
	}

	/**
	 * Adds a passenger
	 * 
	 * @param reservationPax
	 */
	public void addPassenger(LCCClientReservationPax lccReservationPax) {
		if (this.getPassengers() == null) {
			this.setPassengers(new HashSet<LCCClientReservationPax>());
		}

		lccReservationPax.setLccReservation(this);
		this.getPassengers().add(lccReservationPax);
	}

	/**
	 * Overrided by default to support lazy loading
	 * 
	 * @return
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPNR()).toHashCode();
	}

	/**
	 * @return the zuluBookingTimestamp
	 */
	public Date getZuluBookingTimestamp() {
		return zuluBookingTimestamp;
	}

	/**
	 * @param zuluBookingTimestamp
	 *            the zuluBookingTimestamp to set
	 */
	public void setZuluBookingTimestamp(Date zuluBookingTimestamp) {
		this.zuluBookingTimestamp = zuluBookingTimestamp;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the contactInfo
	 */
	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the adminInfo
	 */
	public CommonReservationAdminInfo getAdminInfo() {
		return adminInfo;
	}

	/**
	 * @param adminInfo
	 *            the adminInfo to set
	 */
	public void setAdminInfo(CommonReservationAdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}

	/**
	 * @return the userNote
	 */
	public String getLastUserNote() {
		return lastUserNote;
	}

	/**
	 * @param userNote
	 *            the userNote to set
	 */
	public void setLastUserNote(String lastUserNote) {
		this.lastUserNote = lastUserNote;
	}

	/**
	 * @return the segments
	 */
	public Set<LCCClientReservationSegment> getSegments() {
		return segments;
	}

	/**
	 * @param segments
	 *            the segments to set
	 */
	private void setSegments(Set<LCCClientReservationSegment> segments) {
		this.segments = segments;
	}

	/**
	 * @return the passengers
	 */
	public Set<LCCClientReservationPax> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            the passengers to set
	 */
	private void setPassengers(Set<LCCClientReservationPax> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return the totalPaxAdultCount
	 */
	public int getTotalPaxAdultCount() {
		return totalPaxAdultCount;
	}

	/**
	 * @param totalPaxAdultCount
	 *            the totalPaxAdultCount to set
	 */
	public void setTotalPaxAdultCount(int totalPaxAdultCount) {
		this.totalPaxAdultCount = totalPaxAdultCount;
	}

	/**
	 * @return the totalPaxChildCount
	 */
	public int getTotalPaxChildCount() {
		return totalPaxChildCount;
	}

	/**
	 * @param totalPaxChildCount
	 *            the totalPaxChildCount to set
	 */
	public void setTotalPaxChildCount(int totalPaxChildCount) {
		this.totalPaxChildCount = totalPaxChildCount;
	}

	/**
	 * @return the totalPaxInfantCount
	 */
	public int getTotalPaxInfantCount() {
		return totalPaxInfantCount;
	}

	/**
	 * @param totalPaxInfantCount
	 *            the totalPaxInfantCount to set
	 */
	public void setTotalPaxInfantCount(int totalPaxInfantCount) {
		this.totalPaxInfantCount = totalPaxInfantCount;
	}

	/**
	 * @return the totalTicketFare
	 */
	public BigDecimal getTotalTicketFare() {
		return totalTicketFare;
	}

	/**
	 * @param totalTicketFare
	 *            the totalTicketFare to set
	 */
	public void setTotalTicketFare(BigDecimal totalTicketFare) {
		this.totalTicketFare = totalTicketFare;
	}

	/**
	 * @return the totalTicketSurCharge
	 */
	public BigDecimal getTotalTicketSurCharge() {
		return totalTicketSurCharge;
	}

	/**
	 * @param totalTicketSurCharge
	 *            the totalTicketSurCharge to set
	 */
	public void setTotalTicketSurCharge(BigDecimal totalTicketSurCharge) {
		this.totalTicketSurCharge = totalTicketSurCharge;
	}

	/**
	 * @return the totalTicketTaxCharge
	 */
	public BigDecimal getTotalTicketTaxCharge() {
		return totalTicketTaxCharge;
	}

	/**
	 * @param totalTicketTaxCharge
	 *            the totalTicketTaxCharge to set
	 */
	public void setTotalTicketTaxCharge(BigDecimal totalTicketTaxCharge) {
		this.totalTicketTaxCharge = totalTicketTaxCharge;
	}

	/**
	 * @return the totalTicketCancelCharge
	 */
	public BigDecimal getTotalTicketCancelCharge() {
		return totalTicketCancelCharge;
	}

	/**
	 * @param totalTicketCancelCharge
	 *            the totalTicketCancelCharge to set
	 */
	public void setTotalTicketCancelCharge(BigDecimal totalTicketCancelCharge) {
		this.totalTicketCancelCharge = totalTicketCancelCharge;
	}

	/**
	 * @return the totalTicketModificationCharge
	 */
	public BigDecimal getTotalTicketModificationCharge() {
		return totalTicketModificationCharge;
	}

	/**
	 * @param totalTicketModificationCharge
	 *            the totalTicketModificationCharge to set
	 */
	public void setTotalTicketModificationCharge(BigDecimal totalTicketModificationCharge) {
		this.totalTicketModificationCharge = totalTicketModificationCharge;
	}

	/**
	 * @return the totalTicketAdjustmentCharge
	 */
	public BigDecimal getTotalTicketAdjustmentCharge() {
		return totalTicketAdjustmentCharge;
	}

	/**
	 * @param totalTicketAdjustmentCharge
	 *            the totalTicketAdjustmentCharge to set
	 */
	public void setTotalTicketAdjustmentCharge(BigDecimal totalTicketAdjustmentCharge) {
		this.totalTicketAdjustmentCharge = totalTicketAdjustmentCharge;
	}

	/**
	 * @return the totalPaidAmount
	 */
	public BigDecimal getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            the totalPaidAmount to set
	 */
	public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	/**
	 * @return the totalAvailableBalance
	 */
	public BigDecimal getTotalAvailableBalance() {
		return totalAvailableBalance;
	}

	/**
	 * @param totalAvailableBalance
	 *            the totalAvailableBalance to set
	 */
	public void setTotalAvailableBalance(BigDecimal totalAvailableBalance) {
		this.totalAvailableBalance = totalAvailableBalance;
	}

	/**
	 * Get the pnr for the reservation Depending on the reservation, either group pnr or normal pnr will be returned.
	 * Group pnr check can be done using {@link #isGroupPNR()}
	 * 
	 * @return the groupPNR
	 * @see #isGroupPNR()
	 */
	public String getPNR() {
		return pnr;
	}

	/**
	 * Set the PNR for the reservation if reservation is a group pnr {@link #setGroupPNR(boolean)} should also be set
	 * 
	 * @param pnr
	 *            the PNR to set
	 * @see #setGroupPNR(boolean)
	 */
	public void setPNR(String pnr) {
		this.pnr = pnr;
	}

	public String getMarketingCarrier() {
		return marketingCarrier;
	}

	public void setMarketingCarrier(String marketingCarrier) {
		this.marketingCarrier = marketingCarrier;
	}

	/**
	 * @param carrierPNR
	 *            the carrierPNR to set
	 */
	public void setCarrierPNR(Map<String, String> carrierPNR) {
		this.carrierPNR = carrierPNR;
	}

	/**
	 * @return the carrierPNR
	 */
	public Map<String, String> getCarrierPNR() {
		return carrierPNR;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	public List<TaxTO> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<TaxTO> taxes) {
		this.taxes = taxes;
	}

	public List<FeeTO> getFees() {
		return fees;
	}

	public void setFees(List<FeeTO> fees) {
		this.fees = fees;
	}

	public List<SurchargeTO> getSurcharges() {
		return surcharges;
	}

	public void setSurcharges(List<SurchargeTO> surcharges) {
		this.surcharges = surcharges;
	}

	/**
	 * @return the zuluReleaseTimeStamp
	 */
	public Date getZuluReleaseTimeStamp() {
		return zuluReleaseTimeStamp;
	}

	/**
	 * @param zuluReleaseTimeStamp
	 *            the zuluReleaseTimeStamp to set
	 */
	public void setZuluReleaseTimeStamp(Date zuluReleaseTimeStamp) {
		this.zuluReleaseTimeStamp = zuluReleaseTimeStamp;
	}

	public List<LCCClientCarrierOndGroup> getCarrierOndGrouping() {
		return carrierOndGrouping;
	}

	public void setCarrierOndGrouping(List<LCCClientCarrierOndGroup> carrierOndGrouping) {
		this.carrierOndGrouping = carrierOndGrouping;
	}

	/**
	 * @return the lccClientPaymentHolder
	 */
	public LCCClientPaymentHolder getLccClientPaymentHolder() {
		return lccClientPaymentHolder;
	}

	/**
	 * @param lccClientPaymentHolder
	 *            the lccClientPaymentHolder to set
	 */
	public void setLccClientPaymentHolder(LCCClientPaymentHolder lccClientPaymentHolder) {
		this.lccClientPaymentHolder = lccClientPaymentHolder;
	}

	/**
	 * @return the totalTicketPrice
	 */
	public BigDecimal getTotalTicketPrice() {
		return totalTicketPrice;
	}

	/**
	 * @param totalTicketPrice
	 *            the totalTicketPrice to set
	 */
	public void setTotalTicketPrice(BigDecimal totalTicketPrice) {
		this.totalTicketPrice = totalTicketPrice;
	}

	/**
	 * @return the carrierName
	 */
	public Map<String, String> getCarrierName() {
		return carrierName;
	}

	/**
	 * @param carrierName
	 *            the carrierName to set
	 */
	public void setCarrierName(Map<String, String> carrierName) {
		this.carrierName = carrierName;
	}

	public List<LCCClientReservationInsurance> getReservationInsurances() {
		return reservationInsurances;
	}

	public void setReservationInsurances(List<LCCClientReservationInsurance> reservationInsurances) {
		this.reservationInsurances = reservationInsurances;
	}

	public List<LCCClientAlertInfoTO> getAlertInfoTOs() {
		return alertInfoTOs;
	}

	public void setAlertInfoTOs(List<LCCClientAlertInfoTO> alertInfoTOs) {
		this.alertInfoTOs = alertInfoTOs;
	}

	public BigDecimal getTotalWithoutExternalCharges() {
		BigDecimal total = BigDecimal.ZERO;
		if (getPassengers() != null) {
			for (LCCClientReservationPax pax : getPassengers()) {
				LCCClientPaymentAssembler resAsm = pax.getLccClientPaymentAssembler();
				if (resAsm != null) {
					total = AccelAeroCalculator.add(total, resAsm.getTotalWithoutExternalCharges());
				}
			}
		}
		return total;
	}

	/**
	 * Indicate whether the {@code pnr} is a group PNR or not. Interline booking has group PNR and will return true. Own
	 * airline booking will return false
	 * 
	 * @return
	 * @see #getPNR()
	 */
	public boolean isGroupPNR() {
		return this.groupPNR;
	}

	/**
	 * Identify whether the {@code pnr} for the reservation is a group PNR. Group PNR is only present in the interline
	 * booking and value should be set to true For own airline booking this value should be set to false.
	 * 
	 * @param groupPNR
	 * @see #setPNR(String)
	 */
	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public List<LCCClientAlertInfoTO> getFlexiAlertInfoTOs() {
		return flexiAlertInfoTOs;
	}

	public void setFlexiAlertInfoTOs(List<LCCClientAlertInfoTO> flexiAlertInfoTOs) {
		this.flexiAlertInfoTOs = flexiAlertInfoTOs;
	}

	public void setLastCurrencyCode(String lastCurrencyCode) {
		this.lastCurrencyCode = lastCurrencyCode;
	}

	public String getLastCurrencyCode() {
		return lastCurrencyCode;
	}

	/**
	 * @return the preferrenceInfo
	 */
	public CommonReservationPreferrenceInfo getPreferrenceInfo() {
		return preferrenceInfo;
	}

	/**
	 * @param preferrenceInfo
	 *            the preferrenceInfo to set
	 */
	public void setPreferrenceInfo(CommonReservationPreferrenceInfo preferrenceInfo) {
		this.preferrenceInfo = preferrenceInfo;
	}

	public Date getLastModificationTimestamp() {
		return lastModificationTimestamp;
	}

	public void setLastModificationTimestamp(Date lastModificationTimestamp) {
		this.lastModificationTimestamp = lastModificationTimestamp;
	}

	public List<LCCClientAlertInfoTO> getSegmentInfoTOs() {
		return segmentInfoTOs;
	}

	public void setSegmentInfoTOs(List<LCCClientAlertInfoTO> segmentInfoTOs) {
		this.segmentInfoTOs = segmentInfoTOs;
	}

	public List<LCCClientAlertInfoTO> getOpenReturnSegmentInfoTOs() {
		return openReturnSegmentInfoTOs;
	}

	public void setOpenReturnSegmentInfoTOs(List<LCCClientAlertInfoTO> openReturnSegmentInfoTOs) {
		this.openReturnSegmentInfoTOs = openReturnSegmentInfoTOs;
	}

	public Map<EXTERNAL_CHARGES, BigDecimal> getExternalChargersSummary() {
		return externalChargesSummary;
	}

	public void setExternalChargersSummary(Map<EXTERNAL_CHARGES, BigDecimal> externalChargersSummary) {
		this.externalChargesSummary = externalChargersSummary;
	}

	public List<String> getInterlineModificationParams() {
		return interlineModificationParams;
	}

	public void setInterlineModificationParams(List<String> interlineModificationParams) {
		this.interlineModificationParams = interlineModificationParams;
	}

	/**
	 * @return the useOtherCarrierPaxCredit
	 */
	public boolean isUseOtherCarrierPaxCredit() {
		return useOtherCarrierPaxCredit;
	}

	/**
	 * @param useOtherCarrierPaxCredit
	 *            the useOtherCarrierPaxCredit to set
	 */
	public void setUseOtherCarrierPaxCredit(boolean useOtherCarrierPaxCredit) {
		this.useOtherCarrierPaxCredit = useOtherCarrierPaxCredit;
	}

	/**
	 * @return : Interline charge adjustment privileges.
	 */
	public Map<String, List<ChargeAdjustmentPrivilege>> getInterlineChargeAdjustmentPrivileges() {
		return interlineChargeAdjustmentPrivileges;
	}

	/**
	 * @param interlineChargeAdjustmentPrivileges
	 *            : The interline charge adjustment privileges to set.
	 */
	public void setInterlineChargeAdjustmentPrivileges(
			Map<String, List<ChargeAdjustmentPrivilege>> interlineChargeAdjustmentPrivileges) {
		this.interlineChargeAdjustmentPrivileges = interlineChargeAdjustmentPrivileges;
	}

	/**
	 * @return the modifiableReservation
	 */
	public boolean isModifiableReservation() {
		return modifiableReservation;
	}

	/**
	 * @param modifiableReservation
	 *            the modifiableReservation to set
	 */
	public void setModifiableReservation(boolean modifiableReservation) {
		this.modifiableReservation = modifiableReservation;
	}

	/**
	 * @return the endorsementNote
	 */
	public String getEndorsementNote() {
		return endorsementNote;
	}

	/**
	 * @param endorsementNote
	 *            the endorsementNote to set
	 */
	public void setEndorsementNote(String endorsementNote) {
		this.endorsementNote = endorsementNote;
	}

	/**
	 * @return
	 */
	public String getBookingCategory() {
		return bookingCategory;
	}

	/**
	 * @param bookingCategory
	 */
	public void setBookingCategory(String bookingCategory) {
		this.bookingCategory = bookingCategory;
	}

	public String getItineraryFareMask() {
		return itineraryFareMask;
	}

	public void setItineraryFareMask(String itineraryFareMask) {
		this.itineraryFareMask = itineraryFareMask;
	}

	public Date getTicketValidTill() {
		return ticketValidTill;
	}

	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	public Date getTicketValidFrom() {
		return ticketValidFrom;
	}

	public void setTicketValidFrom(Date ticketValidFrom) {
		this.ticketValidFrom = ticketValidFrom;
	}

	/**
	 * As per the current implementation and Date of Issue will be the date of the first payment.
	 * 
	 * @return
	 */
	public Date getDateOfIssue() {
		if (this.dateOfIssue == null && getPassengers() != null) {
			for (LCCClientReservationPax pax : getPassengers()) {
				if (pax.getLccClientPaymentHolder() != null && pax.getLccClientPaymentHolder().getPayments() != null) {
					for (LCCClientPaymentInfo payment : pax.getLccClientPaymentHolder().getPayments()) {
						if (payment.getTxnDateTime() != null) {
							if (this.dateOfIssue == null || payment.getTxnDateTime().compareTo(this.dateOfIssue) < 0) {
								this.dateOfIssue = payment.getTxnDateTime();
							}
						}
					}
				}
			}
		}

		return this.dateOfIssue;
	}

	public String getDisplayStatus() {
		return displayStatus;
	}

	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	/**
	 * Return total charge amount
	 * 
	 * @return
	 */
	public BigDecimal getTotalChargeAmount() {
		return AccelAeroCalculator.add(this.getTotalTicketFare(), this.getTotalTicketTaxCharge(), this.getTotalTicketSurCharge(),
				this.getTotalTicketCancelCharge(), this.getTotalTicketModificationCharge(),
				this.getTotalTicketAdjustmentCharge());
	}

	/**
	 * Return total charge amounts
	 * 
	 * @return
	 */
	public BigDecimal[] getTotalChargeAmounts() {
		return new BigDecimal[] { this.getTotalTicketFare(), this.getTotalTicketTaxCharge(), this.getTotalTicketSurCharge(),
				this.getTotalTicketCancelCharge(), this.getTotalTicketModificationCharge(),
				this.getTotalTicketAdjustmentCharge() };
	}

	public Map<String, Collection<RefundableChargeDetailDTO>> getRefundableChargeDetails() {
		return refundableChargeDetails;
	}

	public void setRefundableChargeDetails(Map<String, Collection<RefundableChargeDetailDTO>> refundableChargeDetails) {
		this.refundableChargeDetails = refundableChargeDetails;
	}

	public void setSuccessfulRefund(boolean successfulRefund) {
		this.successfulRefund = successfulRefund;
	}

	public boolean isSuccessfulRefund() {
		return successfulRefund;
	}

	public boolean isAllowVoidReservation() {
		return allowVoidReservation;
	}

	public void setAllowVoidReservation(boolean allowVoidReservation) {
		this.allowVoidReservation = allowVoidReservation;
	}

	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public AutoCancellationInfo getAutoCancellationInfo() {
		return autoCancellationInfo;
	}

	public void setAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo) {
		this.autoCancellationInfo = autoCancellationInfo;
	}

	/**
	 * @return the gdsId
	 */
	public Integer getGdsId() {
		return gdsId;
	}

	/**
	 * @param gdsId
	 *            the gdsId to set
	 */
	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	/**
	 * @param stationContacts
	 *            the stationContacts to set
	 */
	public void setStationContacts(Set<StationContactDTO> stationContacts) {
		this.stationContacts = stationContacts;
	}

	/**
	 * @return the stationContacts
	 */
	public Set<StationContactDTO> getStationContacts() {
		return stationContacts;
	}

	/**
	 * @return the lccPromotionInfoTO
	 */
	public LCCPromotionInfoTO getLccPromotionInfoTO() {
		return lccPromotionInfoTO;
	}

	/**
	 * @param lccPromotionInfoTO
	 *            the lccPromotionInfoTO to set
	 */
	public void setLccPromotionInfoTO(LCCPromotionInfoTO lccPromotionInfoTO) {
		this.lccPromotionInfoTO = lccPromotionInfoTO;
	}

	/**
	 * @return the nameChangeThresholdTimePerStation
	 */
	public String getNameChangeThresholdTimePerStation() {
		return nameChangeThresholdTimePerStation;
	}

	/**
	 * @param nameChangeThresholdTimePerStation
	 *            the nameChangeThresholdTimePerStation to set
	 */
	public void setNameChangeThresholdTimePerStation(String nameChangeThresholdTimePerStation) {
		this.nameChangeThresholdTimePerStation = nameChangeThresholdTimePerStation;
	}

	/**
	 * @return the nameChangeCutoffTime
	 */
	public long getNameChangeCutoffTime() {
		return nameChangeCutoffTime;
	}

	/**
	 * @param nameChangeCutoffTime
	 *            the nameChangeCutoffTime to set
	 */
	public void setNameChangeCutoffTime(long nameChangeCutoffTime) {
		this.nameChangeCutoffTime = nameChangeCutoffTime;
	}

	public long getGroupBookingRequestID() {
		return groupBookingRequestID;
	}

	public void setGroupBookingRequestID(long groupBookingRequestID) {
		this.groupBookingRequestID = groupBookingRequestID;
	}

	public List<BundledFareDTO> getBundledFareDTOs() {
		return bundledFareDTOs;
	}

	public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
		this.bundledFareDTOs = bundledFareDTOs;
	}

	public Set<ServiceTaxContainer> getApplicableServiceTaxes() {
		if (applicableServiceTaxes == null) {
			applicableServiceTaxes = new HashSet<ServiceTaxContainer>();
		}
		return applicableServiceTaxes;
	}

	public void setApplicableServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		this.applicableServiceTaxes = applicableServiceTaxes;
	}

	public Integer getNameChangeCount() {
		return nameChangeCount;
	}

	public void setNameChangeCount(Integer nameChangeCount) {
		this.nameChangeCount = nameChangeCount;
	}

	public Set<LCCClientOtherAirlineSegment> getOthertAirlineSegments() {
		return othertAirlineSegments;
	}

	public void setOthertAirlineSegments(Set<LCCClientOtherAirlineSegment> othertAirlineSegments) {
		this.othertAirlineSegments = othertAirlineSegments;
	}

	public String getExternalPos() {
		return externalPos;
	}

	public void setExternalPos(String externalPos) {
		this.externalPos = externalPos;
	}

	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	public String getUserNoteType() {
		return UserNoteType;
	}

	public void setUserNoteType(String userNoteType) {
		UserNoteType = userNoteType;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public BigDecimal getTotalGOQUOAmount() {
		return totalGOQUOAmount;
	}

	public void setTotalGOQUOAmount(BigDecimal totalGOQUOAmount) {
		this.totalGOQUOAmount = totalGOQUOAmount;
	}

	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public void setInfantPaymentSeparated(boolean infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}

	/**
	 * @return the taxInvoicesList
	 */
	public List<TaxInvoice> getTaxInvoicesList() {
		return taxInvoicesList;
	}

	/**
	 * @param taxInvoicesList the taxInvoicesList to set
	 */
	public void setTaxInvoicesList(List<TaxInvoice> taxInvoicesList) {
		this.taxInvoicesList = taxInvoicesList;
	}

	public boolean isUseEtsForReservation() {
		return useEtsForReservation;
	}
	

	public void setUseEtsForReservation(boolean useEtsForReservation) {
		this.useEtsForReservation = useEtsForReservation;
	}

	public LCCOfflinePaymentDetails getLccOfflinePaymentDetails() {
		return lccOfflinePaymentDetails;
	}

	public void setLccOfflinePaymentDetails(LCCOfflinePaymentDetails lccOfflinePaymentDetails) {
		this.lccOfflinePaymentDetails = lccOfflinePaymentDetails;
	}

	public String getOriginCountryOfCall() {
		return originCountryOfCall;
	}

	public void setOriginCountryOfCall(String originCountryOfCall) {
		this.originCountryOfCall = originCountryOfCall;
	}

	public String getPaymentRef() {
		return paymentRef;
	}

	public void setPaymentRef(String paymentRef) {
		this.paymentRef = paymentRef;
	}

	public String getFatourati() {
		return fatourati;
	}

	public void setFatourati(String fatourati) {
		this.fatourati = fatourati;
	}

	public String getReasonForAllowBlPax() {
		return reasonForAllowBlPax;
	}

	public void setReasonForAllowBlPax(String reasonForAllowBlPax) {
		this.reasonForAllowBlPax = reasonForAllowBlPax;
	}

	/**
	 * @return the actionedByIBE
	 */
	public boolean isActionedByIBE() {
		return actionedByIBE;
	}

	/**
	 * @param actionedByIBE the actionedByIBE to set
	 */
	public void setActionedByIBE(boolean actionedByIBE) {
		this.actionedByIBE = actionedByIBE;
	}

}