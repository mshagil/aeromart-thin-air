package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class UserNoteHistoryTO implements Serializable {
	private static final long serialVersionUID = 5729249427904084318L;

	private ArrayList<String> userNotes;
	private Date modifiedDate;
	private String displayUserName;
	private String displayUserNote;
	private String displayModifyDate;
	private String displayAction;
	private String displaySystemNote;
	private String displayCarrierCode;
	private String historyLanguage;
	private String action;
	private String systemNote;
	private String userName;
	private String userText;
	private String carrierCode;

	public ArrayList<String> getUserNotes() {
		return userNotes;
	}

	public void setUserNotes(ArrayList<String> userNotes) {
		this.userNotes = userNotes;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getDisplayUserName() {
		return displayUserName;
	}

	public void setDisplayUserName(String displayUserName) {
		this.displayUserName = displayUserName;
	}

	public String getDisplayUserNote() {
		return displayUserNote;
	}

	public void setDisplayUserNote(String displayUserNote) {
		this.displayUserNote = displayUserNote;
	}

	public String getDisplayModifyDate() {
		return displayModifyDate;
	}

	public void setDisplayModifyDate(String displayModifyDate) {
		this.displayModifyDate = displayModifyDate;
	}

	public String getDisplayAction() {
		return displayAction;
	}

	public void setDisplayAction(String displayAction) {
		this.displayAction = displayAction;
	}

	public String getDisplaySystemNote() {
		return displaySystemNote;
	}

	public void setDisplaySystemNote(String displaySystemNote) {
		this.displaySystemNote = displaySystemNote;
	}

	public String getDisplayCarrierCode() {
		return displayCarrierCode;
	}

	public void setDisplayCarrierCode(String displayCarrierCode) {
		this.displayCarrierCode = displayCarrierCode;
	}

	public String getHistoryLanguage() {
		return historyLanguage;
	}

	public void setHistoryLanguage(String historyLanguage) {
		this.historyLanguage = historyLanguage;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getSystemNote() {
		return systemNote;
	}

	public void setSystemNote(String systemNote) {
		this.systemNote = systemNote;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserText() {
		return userText;
	}

	public void setUserText(String userText) {
		this.userText = userText;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
