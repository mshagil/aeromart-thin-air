package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class LCCClientSegmentSummaryTO implements Serializable {

	private static final long serialVersionUID = -585843671820638080L;

	// Current
	private BigDecimal currentFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentSurchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentSeatAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentAutoCheckinAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentMealAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentBaggageAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentInsuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentSSRAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentCnxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentModAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentAdjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentNonRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentModificationPenatly = AccelAeroCalculator.getDefaultBigDecimalZero();

	// New
	private BigDecimal newFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newSurchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newSeatAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newAutoCheckinAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newMealAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newBaggageAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newInsuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newSSRAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newCnxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newModAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newAdjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal outBoundExternalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal inBoundExternalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal modificationPenatly;
	
	private BigDecimal newExtraFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/**
	 * @return the currentFareAmount
	 */
	public BigDecimal getCurrentFareAmount() {
		return currentFareAmount;
	}

	/**
	 * @param currentFareAmount
	 *            the currentFareAmount to set
	 */
	public void setCurrentFareAmount(BigDecimal currentFareAmount) {
		this.currentFareAmount = currentFareAmount;
	}

	/**
	 * @return the currentTaxAmount
	 */
	public BigDecimal getCurrentTaxAmount() {
		return currentTaxAmount;
	}

	/**
	 * @param currentTaxAmount
	 *            the currentTaxAmount to set
	 */
	public void setCurrentTaxAmount(BigDecimal currentTaxAmount) {
		this.currentTaxAmount = currentTaxAmount;
	}

	/**
	 * @return the currentSurchargeAmount
	 */
	public BigDecimal getCurrentSurchargeAmount() {
		return currentSurchargeAmount;
	}

	/**
	 * @param currentSurchargeAmount
	 *            the currentSurchargeAmount to set
	 */
	public void setCurrentSurchargeAmount(BigDecimal currentSurchargeAmount) {
		this.currentSurchargeAmount = currentSurchargeAmount;
	}

	/**
	 * @return the currentCnxAmount
	 */
	public BigDecimal getCurrentCnxAmount() {
		return currentCnxAmount;
	}

	/**
	 * @param currentCnxAmount
	 *            the currentCnxAmount to set
	 */
	public void setCurrentCnxAmount(BigDecimal currentCnxAmount) {
		this.currentCnxAmount = currentCnxAmount;
	}

	/**
	 * @return the currentModAmount
	 */
	public BigDecimal getCurrentModAmount() {
		return currentModAmount;
	}

	/**
	 * @param currentModAmount
	 *            the currentModAmount to set
	 */
	public void setCurrentModAmount(BigDecimal currentModAmount) {
		this.currentModAmount = currentModAmount;
	}

	/**
	 * @return the currentAdjAmount
	 */
	public BigDecimal getCurrentAdjAmount() {
		return currentAdjAmount;
	}

	/**
	 * @param currentAdjAmount
	 *            the currentAdjAmount to set
	 */
	public void setCurrentAdjAmount(BigDecimal currentAdjAmount) {
		this.currentAdjAmount = currentAdjAmount;
	}

	/**
	 * @return the newFareAmount
	 */
	public BigDecimal getNewFareAmount() {
		return newFareAmount;
	}

	/**
	 * @param newFareAmount
	 *            the newFareAmount to set
	 */
	public void setNewFareAmount(BigDecimal newFareAmount) {
		this.newFareAmount = newFareAmount;
	}

	/**
	 * @return the newTaxAmount
	 */
	public BigDecimal getNewTaxAmount() {
		return newTaxAmount;
	}

	/**
	 * @param newTaxAmount
	 *            the newTaxAmount to set
	 */
	public void setNewTaxAmount(BigDecimal newTaxAmount) {
		this.newTaxAmount = newTaxAmount;
	}

	/**
	 * @return the newSurchargeAmount
	 */
	public BigDecimal getNewSurchargeAmount() {
		return newSurchargeAmount;
	}

	/**
	 * @param newSurchargeAmount
	 *            the newSurchargeAmount to set
	 */
	public void setNewSurchargeAmount(BigDecimal newSurchargeAmount) {
		this.newSurchargeAmount = newSurchargeAmount;
	}

	/**
	 * @return the newCnxAmount
	 */
	public BigDecimal getNewCnxAmount() {
		return newCnxAmount;
	}

	/**
	 * @param newCnxAmount
	 *            the newCnxAmount to set
	 */
	public void setNewCnxAmount(BigDecimal newCnxAmount) {
		this.newCnxAmount = newCnxAmount;
	}

	/**
	 * @return the newModAmount
	 */
	public BigDecimal getNewModAmount() {
		return newModAmount;
	}

	/**
	 * @param newModAmount
	 *            the newModAmount to set
	 */
	public void setNewModAmount(BigDecimal newModAmount) {
		this.newModAmount = newModAmount;
	}

	/**
	 * @return the newAdjAmount
	 */
	public BigDecimal getNewAdjAmount() {
		return newAdjAmount;
	}

	/**
	 * @param newAdjAmount
	 *            the newAdjAmount to set
	 */
	public void setNewAdjAmount(BigDecimal newAdjAmount) {
		this.newAdjAmount = newAdjAmount;
	}

	/**
	 * @return the currentRefunds
	 */
	public BigDecimal getCurrentRefunds() {
		return currentRefunds;
	}

	/**
	 * @param currentRefunds
	 *            the currentRefunds to set
	 */
	public void setCurrentRefunds(BigDecimal currentRefunds) {
		this.currentRefunds = currentRefunds;
	}

	/**
	 * @return the currentNonRefunds
	 */
	public BigDecimal getCurrentNonRefunds() {
		return currentNonRefunds;
	}

	/**
	 * @param currentNonRefunds
	 *            the currentNonRefunds to set
	 */
	public void setCurrentNonRefunds(BigDecimal currentNonRefunds) {
		this.currentNonRefunds = currentNonRefunds;
	}

	/**
	 * @return the currentTotalPrice
	 */
	public BigDecimal getCurrentTotalPrice() {
		return currentTotalPrice;
	}

	/**
	 * @param currentTotalPrice
	 *            the currentTotalPrice to set
	 */
	public void setCurrentTotalPrice(BigDecimal currentTotalPrice) {
		this.currentTotalPrice = currentTotalPrice;
	}

	/**
	 * @return the newTotalPrice
	 */
	public BigDecimal getNewTotalPrice() {
		return newTotalPrice;
	}

	/**
	 * @param newTotalPrice
	 *            the newTotalPrice to set
	 */
	public void setNewTotalPrice(BigDecimal newTotalPrice) {
		this.newTotalPrice = newTotalPrice;
	}

	public BigDecimal getOutBoundExternalCharge() {
		return outBoundExternalCharge;
	}

	public void setOutBoundExternalCharge(BigDecimal outBoundExternalCharge) {
		this.outBoundExternalCharge = outBoundExternalCharge;
	}

	public BigDecimal getInBoundExternalCharge() {
		return inBoundExternalCharge;
	}

	public void setInBoundExternalCharge(BigDecimal inBoundExternalCharge) {
		this.inBoundExternalCharge = inBoundExternalCharge;
	}

	public void setModificationPenalty(BigDecimal modificationPenatly) {
		this.modificationPenatly = modificationPenatly;
	}

	public BigDecimal getModificationPenalty() {
		return modificationPenatly;
	}

	public BigDecimal getCurrentDiscount() {
		return currentDiscount;
	}

	public void setCurrentDiscount(BigDecimal currentDiscount) {
		this.currentDiscount = currentDiscount;
	}

	public BigDecimal getNewDiscount() {
		return newDiscount;
	}

	public void setNewDiscount(BigDecimal newDiscount) {
		this.newDiscount = newDiscount;
	}

	public BigDecimal getCurrentModificationPenatly() {
		return currentModificationPenatly;
	}

	public void setCurrentModificationPenatly(BigDecimal currentModificationPenatly) {
		this.currentModificationPenatly = currentModificationPenatly;
	}

	public BigDecimal getCurrentSeatAmount() {
		return currentSeatAmount;
	}

	public void setCurrentSeatAmount(BigDecimal currentSeatAmount) {
		this.currentSeatAmount = currentSeatAmount;
	}

	public BigDecimal getCurrentMealAmount() {
		return currentMealAmount;
	}

	public void setCurrentMealAmount(BigDecimal currentMealAmount) {
		this.currentMealAmount = currentMealAmount;
	}

	public BigDecimal getCurrentBaggageAmount() {
		return currentBaggageAmount;
	}

	public void setCurrentBaggageAmount(BigDecimal currentBaggageAmount) {
		this.currentBaggageAmount = currentBaggageAmount;
	}

	public BigDecimal getCurrentInsuranceAmount() {
		return currentInsuranceAmount;
	}

	public void setCurrentInsuranceAmount(BigDecimal currentInsuranceAmount) {
		this.currentInsuranceAmount = currentInsuranceAmount;
	}

	public BigDecimal getNewSeatAmount() {
		return newSeatAmount;
	}

	public void setNewSeatAmount(BigDecimal newSeatAmount) {
		this.newSeatAmount = newSeatAmount;
	}

	public BigDecimal getNewMealAmount() {
		return newMealAmount;
	}

	public void setNewMealAmount(BigDecimal newMealAmount) {
		this.newMealAmount = newMealAmount;
	}

	public BigDecimal getNewBaggageAmount() {
		return newBaggageAmount;
	}

	public void setNewBaggageAmount(BigDecimal newBaggageAmount) {
		this.newBaggageAmount = newBaggageAmount;
	}

	public BigDecimal getNewInsuranceAmount() {
		return newInsuranceAmount;
	}

	public void setNewInsuranceAmount(BigDecimal newInsuranceAmount) {
		this.newInsuranceAmount = newInsuranceAmount;
	}

	public BigDecimal getCurrentSSRAmount() {
		return currentSSRAmount;
	}

	public void setCurrentSSRAmount(BigDecimal currentSSRAmount) {
		this.currentSSRAmount = currentSSRAmount;
	}

	public BigDecimal getNewSSRAmount() {
		return newSSRAmount;
	}

	public void setNewSSRAmount(BigDecimal newSSRAmount) {
		this.newSSRAmount = newSSRAmount;
	}

	public BigDecimal getNewExtraFeeAmount() {
		return newExtraFeeAmount;
	}

	public void setNewExtraFeeAmount(BigDecimal newExtraFeeAmount) {
		this.newExtraFeeAmount = newExtraFeeAmount;
	}

	/**
	 * @return the currentAutoCheckinAmount
	 */
	public BigDecimal getCurrentAutoCheckinAmount() {
		return currentAutoCheckinAmount;
	}

	/**
	 * @param currentAutoCheckinAmount
	 *            the currentAutoCheckinAmount to set
	 */
	public void setCurrentAutoCheckinAmount(BigDecimal currentAutoCheckinAmount) {
		this.currentAutoCheckinAmount = currentAutoCheckinAmount;
	}

	/**
	 * @return the newAutoCheckinAmount
	 */
	public BigDecimal getNewAutoCheckinAmount() {
		return newAutoCheckinAmount;
	}

	/**
	 * @param newAutoCheckinAmount
	 *            the newAutoCheckinAmount to set
	 */
	public void setNewAutoCheckinAmount(BigDecimal newAutoCheckinAmount) {
		this.newAutoCheckinAmount = newAutoCheckinAmount;
	}

}
