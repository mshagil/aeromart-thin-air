package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;

public class LCCSeatMapDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String transactionIdentifier;

	private SYSTEM queryingSystem;

	private List<FlightSegmentTO> flightDetails;

	private List<LCCSegmentSeatMapDTO> lccSegmentSeatMapDTOs;

	List<BundledFareDTO> bundledFareDTOs;

	private String pnr;

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public SYSTEM getQueryingSystem() {
		return queryingSystem;
	}

	public void setQueryingSystem(SYSTEM queryingSystem) {
		this.queryingSystem = queryingSystem;
	}

	public List<FlightSegmentTO> getFlightDetails() {
		return flightDetails;
	}

	public void setFlightDetails(List<FlightSegmentTO> flightDetails) {
		this.flightDetails = flightDetails;
	}

	public List<LCCSegmentSeatMapDTO> getLccSegmentSeatMapDTOs() {
		return lccSegmentSeatMapDTOs;
	}

	public void setLccSegmentSeatMapDTOs(List<LCCSegmentSeatMapDTO> lccSegmentSeatMapDTOs) {
		this.lccSegmentSeatMapDTOs = lccSegmentSeatMapDTOs;
	}

	public List<BundledFareDTO> getBundledFareDTOs() {
		return bundledFareDTOs;
	}

	public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
		this.bundledFareDTOs = bundledFareDTOs;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
