package com.isa.thinair.airproxy.api.service;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * The Airproxy alerting API methods
 * 
 * @author sanjaya
 * 
 */
/**
 * @author suneth
 *
 */
public interface AirproxyAlertingBD {

	public static final String SERVICE_NAME = "AirproxyReservationService";

	/**
	 * Retrieves the alerts matching the given search criteria.
	 * 
	 * @param searchCriteria
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria, BasicTrackInfo trackInfo) throws ModuleException;

	
	/**
	 * This method can be used to get the requests submitted by a specific user
	 * @param searchCraiteria 
	 * 
	 * @return submitted requests
	 * @throws ModuleException
	 */
	public Page retrieveSubmittedRequests(QueueRequestDetailsDTO searchCriteria) throws ModuleException;
	
	
	/** 
	 *  This method can be used to save or update a queue request
	 * 
	 * @param queueRequest QueueRequest model object
	 * @throws ModuleException
	 */
	public void submitOrEditRequest(QueueRequest queueRequest) throws ModuleException;
	
	
	/**
	 * This method can be used to get a request using the request ID
	 * 
	 * @param requestId
	 * @return QueueReuest Model
	 * @throws ModuleException
	 */
	public QueueRequest getQueueRequest(int requestId) throws ModuleException;
	
	
	/**
	 * This method can be used to add a new request History entry
	 * 
	 * @param requestHistory
	 * @throws ModuleException
	 */
	public void addRequestHistory(RequestHistory requestHistory) throws ModuleException;
	
	
	/**
	 * This method can be used to remove a request
	 * 
	 * @param requestId this the id of the request that wants to be removed
	 * @throws ModuleException
	 */
	public void deleteRequest(int requestId) throws ModuleException;
	
	
	/**
	 * This can be used to get the history of a given request by id
	 * 
	 * @param requestId
	 * @return
	 * @throws ModuleException
	 */
	public Page getRequestHistory(int requestId) throws ModuleException;
	
	/**
	 * 
	 * This method can be used to get the requests that can be action by the given user
	 * 
	 * @param userID
	 * @param queueId
	 * @param status
	 * @return Requests that can be action by the given user
	 * @throws ModuleException
	 */
	public Page retrieveSubmittedRequestsForAction(String userID , String queueId , String status) throws ModuleException;
	
	/**
	 * This is used to get the request status and other informtation relating to the request
	 * 
	 * @param pnrId
	 * @return
	 * @throws ModuleException
	 */
	public Page getRequestStatusForPnr(int page, int maxResult,String pnrId) throws ModuleException;
	
	/**
	 * This is used to get the request history for a particular request
	 * 
	 * @param queueId the queue id for retrieve history
	 * @return Page
	 * @throws ModuleException
	 */
	public Page getHistoryForRequest(int page, int maxResult,int queueId) throws ModuleException;
	
}
