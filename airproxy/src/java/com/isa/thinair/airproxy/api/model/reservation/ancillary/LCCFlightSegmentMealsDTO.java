package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class LCCFlightSegmentMealsDTO implements Serializable, Comparable<LCCFlightSegmentMealsDTO> {
	private static final long serialVersionUID = 1L;
	private FlightSegmentTO flightSegmentTO;
	private List<LCCMealDTO> meals;
	// Segment level logical cabin class based multi meal configuration;
	private boolean multiMealEnabledForLogicalCabinClass;

	private boolean isAnciOffer;
	private String anciOfferName;
	private String anciOfferDescription;

	/** Anci Offer's Seat template id used to customize the seat charges */
	private Integer offeredTemplateID;

	public FlightSegmentTO getFlightSegmentTO() {
		return flightSegmentTO;
	}

	public void setFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		this.flightSegmentTO = flightSegmentTO;
	}

	public List<LCCMealDTO> getMeals() {
		if (meals == null) {
			meals = new ArrayList<LCCMealDTO>();
		}
		return meals;
	}

	public boolean isMultiMealEnabledForLogicalCabinClass() {
		return multiMealEnabledForLogicalCabinClass;
	}

	public void setMultiMealEnabledForLogicalCabinClass(boolean multiMealEnabledForLogicalCabinClass) {
		this.multiMealEnabledForLogicalCabinClass = multiMealEnabledForLogicalCabinClass;
	}

	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	public String getAnciOfferName() {
		return anciOfferName;
	}

	public void setAnciOfferName(String anciOfferName) {
		this.anciOfferName = anciOfferName;
	}

	public String getAnciOfferDescription() {
		return anciOfferDescription;
	}

	public void setAnciOfferDescription(String anciOfferDescription) {
		this.anciOfferDescription = anciOfferDescription;
	}

	/**
	 * @return the offeredTemplateID
	 */
	public Integer getOfferedTemplateID() {
		return offeredTemplateID;
	}

	/**
	 * @param offeredTemplateID
	 *            the offeredTemplateID to set
	 */
	public void setOfferedTemplateID(Integer offeredTemplateID) {
		this.offeredTemplateID = offeredTemplateID;
	}

	@Override
	public int compareTo(LCCFlightSegmentMealsDTO o) {
		return this.flightSegmentTO.getDepartureDateTime().compareTo(o.getFlightSegmentTO().getDepartureDateTime());
	}
}
