package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;

public class AnciAvailabilityRS implements Serializable {
	private static final long serialVersionUID = -5919525166845684248L;

	private List<LCCAncillaryAvailabilityOutDTO> lccAncillaryAvailabilityOutDTOs;
	private boolean jnTaxApplicable;
	private BigDecimal taxRatio;
	private boolean applyPenaltyForModification;

	public List<LCCAncillaryAvailabilityOutDTO> getLccAncillaryAvailabilityOutDTOs() {
		return lccAncillaryAvailabilityOutDTOs;
	}

	public void setLccAncillaryAvailabilityOutDTOs(List<LCCAncillaryAvailabilityOutDTO> lccAncillaryAvailabilityOutDTOs) {
		this.lccAncillaryAvailabilityOutDTOs = lccAncillaryAvailabilityOutDTOs;
	}

	public boolean isJnTaxApplicable() {
		return jnTaxApplicable;
	}

	public void setJnTaxApplicable(boolean jnTaxApplicable) {
		this.jnTaxApplicable = jnTaxApplicable;
	}

	public BigDecimal getTaxRatio() {
		return taxRatio;
	}

	public void setTaxRatio(BigDecimal taxRatio) {
		this.taxRatio = taxRatio;
	}

	public boolean isApplyPenaltyForModification() {
		return applyPenaltyForModification;
	}

	public void setApplyPenaltyForModification(boolean applyPenaltyForModification) {
		this.applyPenaltyForModification = applyPenaltyForModification;
	}

}
