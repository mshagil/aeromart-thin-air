package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

public class ServiceTaxBaseCriteriaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String paxState;

	private String paxCountryCode;

	private String paxTaxRegistrationNo;

	public String getPaxState() {
		return paxState;
	}

	public String getPaxCountryCode() {
		return paxCountryCode;
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public String getPaxTaxRegistrationNo() {
		return paxTaxRegistrationNo;
	}

	public void setPaxTaxRegistrationNo(String paxTaxRegistrationNo) {
		this.paxTaxRegistrationNo = paxTaxRegistrationNo;
	}

}
