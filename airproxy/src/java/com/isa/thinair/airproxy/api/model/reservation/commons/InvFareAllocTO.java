package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InvFareAllocTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum BCType {
		NONSTD, STD, FIXED;

	}

	private String bookingCode;
	private String ccCode;
	private String logicalCabinClass;
	private int allocatedSeats;
	private BCType bcType;
	private Integer nestRank;
	private int soldSeats;
	private int onholdSeats;
	private int availableSeats;
	private String bcInvStatus;
	private int availableMaxSeats;
	private String onholdRestricted;
	private List<FareRuleFareDTO> fareRuleFares;
	private boolean isSameBookingClassBeingSearch;
	private boolean bookedFlightCabinBeingSearched;
	private boolean flownOnd;
	private int availableNestedSeats;
	private int actualSeatsSold;
	private int actualSeatsOnHold;
	private boolean isCommonAllSegments;

	public int getAllocatedSeats() {
		return allocatedSeats;
	}

	public void setAllocatedSeats(int allocatedSeats) {
		this.allocatedSeats = allocatedSeats;
	}

	public int getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	public String getBcInvStatus() {
		return bcInvStatus;
	}

	public void setBcInvStatus(String bcInvStatus) {
		this.bcInvStatus = bcInvStatus;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public BCType getBcType() {
		return bcType;
	}

	public void setBcType(BCType bcType) {
		this.bcType = bcType;
	}

	public Integer getNestRank() {
		return nestRank;
	}

	public void setNestRank(Integer nestRank) {
		this.nestRank = nestRank;
	}

	public int getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	public int getAvailableMaxSeats() {
		return availableMaxSeats;
	}

	public void setAvailableMaxSeats(int availableMaxSeats) {
		this.availableMaxSeats = availableMaxSeats;
	}

	public String getOnholdRestricted() {
		return onholdRestricted;
	}

	public void setOnholdRestricted(String onholdRestricted) {
		this.onholdRestricted = onholdRestricted;
	}

	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public List<FareRuleFareDTO> getFareRuleFares() {
		if (fareRuleFares == null) {
			fareRuleFares = new ArrayList<FareRuleFareDTO>();
		}
		return fareRuleFares;
	}

	public void setFareRuleFares(List<FareRuleFareDTO> fareRuleFares) {
		this.fareRuleFares = fareRuleFares;
	}

	public void addFareRuleFare(FareRuleFareDTO fareRuleDTO) {
		getFareRuleFares().add(fareRuleDTO);
	}

	public boolean isSameBookingClassBeingSearch() {
		return isSameBookingClassBeingSearch;
	}

	public void setSameBookingClassBeingSearch(boolean isSameBookingClassBeingSearch) {
		this.isSameBookingClassBeingSearch = isSameBookingClassBeingSearch;
	}

	public void setBookedFlightCabinBeingSearched(boolean bookedFlightCabinBeingSearched) {
		this.bookedFlightCabinBeingSearched = bookedFlightCabinBeingSearched;
	}

	public void setFlownOnd(boolean flownOnd) {
		this.flownOnd = flownOnd;
	}

	public boolean isBookedFlightCabinBeingSearched() {
		return bookedFlightCabinBeingSearched;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	public int getAvailableNestedSeats() {
		return availableNestedSeats;
	}

	public void setAvailableNestedSeats(int availableNestedSeats) {
		this.availableNestedSeats = availableNestedSeats;
	}

	public int getActualSeatsSold() {
		return actualSeatsSold;
	}

	public void setActualSeatsSold(int actualSeatsSold) {
		this.actualSeatsSold = actualSeatsSold;
	}

	public int getActualSeatsOnHold() {
		return actualSeatsOnHold;
	}

	public void setActualSeatsOnHold(int actualSeatsOnHold) {
		this.actualSeatsOnHold = actualSeatsOnHold;
	}

	/**
	 * @return the isCommonAllSegments
	 */
	public boolean isCommonAllSegments() {
		return isCommonAllSegments;
	}

	/**
	 * @param isCommonAllSegments the isCommonAllSegments to set
	 */
	public void setCommonAllSegments(boolean isCommonAllSegments) {
		this.isCommonAllSegments = isCommonAllSegments;
	}

}
