package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

public class BlacklistPAXRuleTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID field for Blacklist PAX rule Id
	 */
	private Long ruleId;

	/**
	 * ID field for rule name
	 */
	private String label;

	/**
	 * check field for firstname
	 */
	private String firstNameChecked;

	/**
	 * check field for lastname
	 */
	private String laststNameChecked;
	
	/**
	 * check field for fullName
	 */
	private String fullNameChecked;

	/**
	 * check field for date of birth
	 */
	private String dateOfBirthChecked;

	/**
	 * check field for passport no
	 */
	private String passportNoChecked;

	/**
	 * check field for nationality
	 */
	private String nationalityChecked;

	/**
	 * check field for nationality
	 */
	private String status;

	/**
	 * ID field for version
	 */
	private Long version;

	/**
	 * String to hold all rules as a comma separated string
	 */
	private String strRules;

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getFirstNameChecked() {
		return firstNameChecked;
	}

	public void setFirstNameChecked(String firstNameChecked) {
		this.firstNameChecked = firstNameChecked;
	}

	public String getLaststNameChecked() {
		return laststNameChecked;
	}

	public void setLaststNameChecked(String laststNameChecked) {
		this.laststNameChecked = laststNameChecked;
	}

	public String getDateOfBirthChecked() {
		return dateOfBirthChecked;
	}

	public void setDateOfBirthChecked(String dateOfBirthChecked) {
		this.dateOfBirthChecked = dateOfBirthChecked;
	}

	public String getPassportNoChecked() {
		return passportNoChecked;
	}

	public void setPassportNoChecked(String passportNoChecked) {
		this.passportNoChecked = passportNoChecked;
	}

	public String getNationalityChecked() {
		return nationalityChecked;
	}

	public void setNationalityChecked(String nationalityChecked) {
		this.nationalityChecked = nationalityChecked;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStrRules() {
		return strRules;
	}

	public void setStrRules(String strRules) {
		this.strRules = strRules;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getFullNameChecked() {
		return fullNameChecked;
	}

	public void setFullNameChecked(String fullNameChecked) {
		this.fullNameChecked = fullNameChecked;
	}
	

}
