package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * Hold the Origination and Destination information including FlightSegment, selected status and the system which the
 * result belongs to
 * 
 */
public class OriginDestinationOptionTO implements Serializable, Comparable<OriginDestinationOptionTO> {

	private static final long serialVersionUID = 1L;

	private boolean isSelected = false;

	private SYSTEM system = SYSTEM.INT;

	public List<FlightSegmentTO> flightSegmentList;

	private List<FlightFareSummaryTO> flightFareSummaryList;

	private boolean seatAvailable;

	private String ondCode;

	public Date getFirstDepartureDate() {
		if (flightSegmentList != null && flightSegmentList.size() > 0) {
			Collections.sort(flightSegmentList);
			return flightSegmentList.get(0).getDepartureDateTimeZulu();
		}
		return null;
	}

	public Date getFirstDepartureDateLocal() {
		if (flightSegmentList != null && flightSegmentList.size() > 0) {
			Collections.sort(flightSegmentList);
			return flightSegmentList.get(0).getDepartureDateTime();
		}
		return null;
	}

	public String getOndCode() {
		if (ondCode == null) {
			Collections.sort(getFlightSegmentList());
			for (IFlightSegment segment : getFlightSegmentList()) {
				if (ondCode == null) {
					ondCode = segment.getSegmentCode();
				} else {
					String transit = segment.getSegmentCode().substring(0, 3);
					if (ondCode.endsWith(transit)) {
						ondCode = ondCode + segment.getSegmentCode().substring(3);
					} else {
						ondCode = ondCode + "/" + segment.getSegmentCode();
					}
				}
			}
		}
		return ondCode;
	}

	/**
	 * Return a list of FlightSegmentDTO's associated with the Ond If flightSegment list is null return a empty list
	 * 
	 * @return List of flight segment
	 */
	public List<FlightSegmentTO> getFlightSegmentList() {
		if (flightSegmentList == null)
			flightSegmentList = new ArrayList<FlightSegmentTO>();
		return flightSegmentList;
	}

	/**
	 * Set the flight segment list associated with the Ond
	 * 
	 * @param flightSegmentList
	 */
	public void setFlightSegmentList(List<FlightSegmentTO> flightSegmentList) {
		this.flightSegmentList = flightSegmentList;
	}

	/**
	 * returns true if this Ond is the selected Ond
	 * 
	 * @return
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * Set whether or not this Ond is the selected Ond
	 * 
	 * @param isSelected
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	/**
	 * @return the the System (interline or AA or both) to which this Ond belongs
	 */
	public SYSTEM getSystem() {
		return system;
	}

	/**
	 * @param system
	 *            , set system (interline or AA or both) to which this Ond belongs
	 */
	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public List<FlightFareSummaryTO> getFlightFareSummaryList() {
		return flightFareSummaryList;
	}

	public FlightFareSummaryTO getSelectedFlightFareSummary() {
		if (this.flightFareSummaryList != null) {
			for (FlightFareSummaryTO flightFareSummaryTO : this.flightFareSummaryList) {
				if (flightFareSummaryTO.isSelected()) {
					return flightFareSummaryTO;
				}
			}
		}
		return null;
	}

	public void setFlightFareSummaryList(List<FlightFareSummaryTO> flightFareSummaryList) {
		this.flightFareSummaryList = flightFareSummaryList;
	}

	public void setSeatAvailable(boolean seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

	public boolean isSeatAvailable() {
		return seatAvailable;
	}

	@Override
	public int compareTo(OriginDestinationOptionTO otherONDOption) {
		return this.getFirstDepartureDate().compareTo(otherONDOption.getFirstDepartureDate());
	}

	public List<Integer> getFlightSegIdList() {
		List<Integer> fltSegIdList = new ArrayList<Integer>();
		for (FlightSegmentTO fltSegTO : getFlightSegmentList()) {
			fltSegIdList.add(fltSegTO.getFlightSegId());
		}
		return fltSegIdList;
	}

	public void fixMissingOndSequence(int ondSequence) {
		for (FlightSegmentTO flightSegment : getFlightSegmentList()) {
			flightSegment.setOndSequence(ondSequence);
		}
	}
	
	public Date getFirstDepartureDateTimeByRef() {
		if (flightSegmentList != null && flightSegmentList.size() > 0) {
			List<Date> list = new ArrayList<>();
			for (FlightSegmentTO flightSegment : flightSegmentList) {
				String flightRefNumber = flightSegment.getFlightRefNumber();
				if (!StringUtil.isNullOrEmpty(flightRefNumber)) {
					Date departureDate = FlightRefNumberUtil.getDepartureDateFromFlightRPH(flightRefNumber);

					if (departureDate != null)
						list.add(departureDate);
				}
			}
			if (!list.isEmpty()) {
				Collections.sort(list);
				return list.get(0);
			}

		}
		return null;
	}
}
