package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FareAvailDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal fareAmount;
	private BigDecimal totalAmount;
	private List<SegInvSelection> selectedInvSegments;

	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public List<SegInvSelection> getSelectedInvSegments() {
		if (selectedInvSegments == null) {
			selectedInvSegments = new ArrayList<SegInvSelection>();
		}
		return selectedInvSegments;
	}

	public void addSelectedInvSegment(SegInvSelection selectedInvSegment) {
		getSelectedInvSegments().add(selectedInvSegment);
	}

}
