package com.isa.thinair.airproxy.api.model.reservation.core;

/**
 * Tranfer Object used for charge adjustment operations
 * 
 */
public class LCCClientChargeAdustment extends BasicChargeInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String carrierCode;

	private String carrierOndGroupRPH;

	private String segmentCode;

	private String travelerRefNumber;

	private boolean isRefundable;

	private String userNote;

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCarrierOndGroupRPH() {
		return carrierOndGroupRPH;
	}

	public void setCarrierOndGroupRPH(String carrierOndGroupRPH) {
		this.carrierOndGroupRPH = carrierOndGroupRPH;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public boolean isRefundable() {
		return isRefundable;
	}

	public void setRefundable(boolean isRefundable) {
		this.isRefundable = isRefundable;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

}
