package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;

public class OfflinePaymentInfo implements Serializable, PaymentInfo {

	private String carrierCode;

	private BigDecimal totalAmount;

	private PayCurrencyDTO payCurrencyDTO;

	private boolean includeExternalCharges;

	private Integer actualPaymentMethod;

	private String payReference;

	private String payCarrier;

	private String pnr;

	private String paymentTnxReference;

	private Integer paymentTnxId;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	private IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public boolean isIncludeExternalCharges() {
		return includeExternalCharges;
	}

	public void setIncludeExternalCharges(boolean includeExternalCharges) {
		this.includeExternalCharges = includeExternalCharges;
	}

	public Integer getActualPaymentMethod() {
		return actualPaymentMethod;
	}

	public void setActualPaymentMethod(Integer actualPaymentMethod) {
		this.actualPaymentMethod = actualPaymentMethod;
	}

	public String getPayReference() {
		return payReference;
	}

	public void setPayReference(String payReference) {
		this.payReference = payReference;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	@Override
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;

	}

	public String getPayCarrier() {
		return payCarrier;
	}

	public void setPayCarrier(String payCarrier) {
		this.payCarrier = payCarrier;

	}

	@Override
	public Integer getPaymentTnxId() {
		return paymentTnxId;
	}

	@Override
	public void setPaymentTnxId(Integer paymentTnxId) {
		this.paymentTnxId = paymentTnxId;

	}

	public String getPaymentTnxReference() {
		return paymentTnxReference;
	}

	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;
	}

	public IPGIdentificationParamsDTO getIpgIdentificationParamsDTO() {
		return ipgIdentificationParamsDTO;
	}

	public void setIpgIdentificationParamsDTO(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	public Object clone() {
		OfflinePaymentInfo offlinePaymentInfo = new OfflinePaymentInfo();
		offlinePaymentInfo.setPnr(this.pnr);
		offlinePaymentInfo.setPayCarrier(this.payCarrier);
		offlinePaymentInfo.setPayCurrencyDTO(this.payCurrencyDTO);
		offlinePaymentInfo.setPayReference(this.payReference);
		offlinePaymentInfo.setActualPaymentMethod(this.actualPaymentMethod);
		offlinePaymentInfo.setIpgIdentificationParamsDTO(this.ipgIdentificationParamsDTO);
		offlinePaymentInfo.setTotalAmount(this.totalAmount);
		offlinePaymentInfo.setIncludeExternalCharges(this.includeExternalCharges);
		offlinePaymentInfo.setCarrierCode(this.carrierCode);
		offlinePaymentInfo.setPaymentTnxId(this.paymentTnxId);
		offlinePaymentInfo.setPaymentTnxReference(this.paymentTnxReference);
		return offlinePaymentInfo;
	}

	public static boolean compare(OfflinePaymentInfo cardPaymentInfoObj1, OfflinePaymentInfo cardPaymentInfoObj2) {
		String payRef1 = BeanUtils.nullHandler(cardPaymentInfoObj1.getPayReference());
		String pnr1 = BeanUtils.nullHandler(cardPaymentInfoObj1.getPnr());
		String carrier1 = BeanUtils.nullHandler(cardPaymentInfoObj1.getPayCarrier());

		String payRef2 = BeanUtils.nullHandler(cardPaymentInfoObj2.getPayReference());
		String pnr2 = BeanUtils.nullHandler(cardPaymentInfoObj2.getPnr());
		String carrier2 = BeanUtils.nullHandler(cardPaymentInfoObj2.getPayCarrier());

		if (payRef1.equals(payRef2) && pnr1.equals(pnr2) && carrier1.equals(carrier2)) {
			return true;
		} else {
			return false;
		}
	}
}
