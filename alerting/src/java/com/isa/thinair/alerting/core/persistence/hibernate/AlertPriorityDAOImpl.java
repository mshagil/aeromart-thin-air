/*
 * Created on Jul 6, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.alerting.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.alerting.api.model.AlertPriority;
import com.isa.thinair.alerting.core.persistence.dao.AlertPriorityDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author sumudu
 * 
 * 
 */
public class AlertPriorityDAOImpl extends PlatformHibernateDaoSupport implements AlertPriorityDAO {

	public List getAlertPriorites() {
		return find("from AlertPriority", AlertPriority.class);
	}

	public AlertPriority getAlertPriority(int alertTypeID) {
		return (AlertPriority) get(AlertPriority.class, new Integer(alertTypeID));
		// return (AlertPriority)get("");
	}

	public void saveAlertPriority(AlertPriority alertPrority) {
		hibernateSaveOrUpdate(alertPrority);
	}

	public void removeAlertPriority(int alertPriority) {
		Object alertObjPriority = load(AlertPriority.class, new Integer(alertPriority));
		delete(alertObjPriority);
	}
}
