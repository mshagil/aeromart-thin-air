package com.isa.thinair.alerting.core.service.bd;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.dto.FlightNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrSearchNotificationDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.alerting.api.dto.RequestQueueUser;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.model.ApplicablePage;
import com.isa.thinair.alerting.api.model.FlightNotification;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.alerting.api.model.Queue;
import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.alerting.core.bl.AlertingServicesBL;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.bd-impl id="alerting.service.local" description="Locally accessible alerting service BD"
 */
public class AlertingBDLocalImpl extends PlatformBaseServiceDelegate implements AlertingBD {
	private final Log log = LogFactory.getLog(getClass());

	public List retrieveAlerts(Collection alertIds) throws ModuleException {
		return AlertingServicesBL.retrieveAlerts(alertIds);
	}

	public AlertTemplate getAlertTemplate(AlertTemplateEnum templateID) throws ModuleException {
		return AlertingServicesBL.getAlertTemplate(templateID);
	}

	public List retrieveAlertsForPrivileges(List privilegeIds) throws ModuleException {
		return AlertingServicesBL.retrieveAlertsForPrivileges(privilegeIds);
	}

	public List retrieveAlertsForUser(String userId) throws ModuleException {
		return AlertingServicesBL.retrieveAlertsForUser(userId);
	}

	public List retrieveAlertsForPnrSegments(List pnrSegmentIds) throws ModuleException {
		return AlertingServicesBL.retrieveAlertsForPnrSegments(pnrSegmentIds);
	}

	public void addAlert(Alert alert) throws ModuleException {
		AlertingServicesBL.addAlert(alert);
	}

	public void addAlerts(Collection alert) throws ModuleException {
		AlertingServicesBL.addAlerts(alert);
	}

	public void updateAlertStatus(Long alertId, String status) throws ModuleException {
		AlertingServicesBL.updateAlertStatus(alertId, status);
	}

	public void updateAlert(Alert alert) throws ModuleException {
		AlertingServicesBL.updateAlert(alert);
	}

	public void updateAlerts(Collection<Alert> alerts) throws ModuleException {
		AlertingServicesBL.updateAlerts(alerts);
	}

	public Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria) throws ModuleException {
		return AlertingServicesBL.retrieveAlertsForSearchCriteria(searchCriteria);
	}

	public void removeAlerts(Collection alertIds) throws ModuleException {
		AlertingServicesBL.removeAlerts(alertIds);
	}

	public void removeAlertsOfPnrSegments(List pnrSegmentIds) throws ModuleException {
		AlertingServicesBL.removeAlertsOfPnrSegments(pnrSegmentIds);
	}

	public Map getAlertsForPnrSegments(Collection pnrSegmentIds) throws ModuleException {
		return AlertingServicesBL.getAlertsForPnrSegments(pnrSegmentIds);
	}

	public Collection<Integer> getPNRSegIDsOfAlerts(Collection<Integer> colAlertIDs) throws ModuleException {
		return AlertingServicesBL.getPNRSegIDsOfAlerts(colAlertIDs);
	}

	public Page retrieveFlightPnrDetailsForSearchCriteria(FlightPnrNotificationDTO searchCriteria) throws ModuleException {
		return AlertingServicesBL.retrieveFlightPnrDetailsForSearchCriteria(searchCriteria);
	}

	public Page retrieveFlightPnrDetailsForViewSearchCriteria(FlightPnrNotificationDTO searchCriteria) throws ModuleException {
		return AlertingServicesBL.retrieveFlightPnrDetailsForViewSearchCriteria(searchCriteria);
	}

	public void notifyReservations(int flightId, FlightNotificationDTO fltDTO, Collection colFlightPnrDTO,
			UserPrincipal userPrincipal) throws ModuleException {
		AlertingServicesBL.notifyReservations(flightId, fltDTO, colFlightPnrDTO, userPrincipal);
	}

	public String getNextNotificationCode(String baseCode) throws ModuleException {
		return AlertingServicesBL.getNextNotificationCode(baseCode);
	}

	public ArrayList getNotificationsForPnr(int pnrSegId) throws ModuleException {
		return AlertingServicesBL.getNotificationsForPnr(pnrSegId);
	}

	public int saveFlightNotification(FlightNotification obj) throws ModuleException {
		return AlertingServicesBL.saveFlightNotification(obj);
	}

	public void saveFlightPnrNotification(FlightPnrNotification obj) throws ModuleException {
		AlertingServicesBL.saveFlightPnrNotification(obj);
	}

	public ArrayList getAllNotificationsForFlight(int flightId, String pnr) throws ModuleException {
		return AlertingServicesBL.getAllNotificationsForFlight(flightId, pnr);
	}

	public Page retrieveDetailedNotificationList(int fltNotificationId, Date notifyDate, String pnr) throws ModuleException {
		return AlertingServicesBL.retrieveDetailedNotificationList(fltNotificationId, notifyDate, pnr);
	}

	public Integer getFlightID(String fltNum, Date depDateLocal) throws ModuleException {
		return AlertingServicesBL.getFlightID(fltNum, depDateLocal);
	}
	
	public Page retrieveSubmittedRequests(QueueRequestDetailsDTO searchCriteria) throws ModuleException{
		
		return AlertingServicesBL.retrieveSubmittedRequests(searchCriteria);
		
	}
	
	public void submitOrEditRequest(QueueRequest queueRequest) throws ModuleException{
		
		AlertingServicesBL.submitOrEditRequest(queueRequest);
		
	}
	
	public QueueRequest getQueueRequest(int requestId) throws ModuleException{
		
		return AlertingServicesBL.getQueueRequest(requestId);
		
	}
	
	public void addRequestHistory(RequestHistory requestHistory) throws ModuleException{
		
		AlertingServicesBL.addRequestHistory(requestHistory);
		
	}
	
	public void deleteRequest(int requestId) throws ModuleException{
		
		AlertingServicesBL.deleteRequest(requestId);
		
	}
	
	public Page getRequestHistory(int requestId) throws ModuleException{
		
		return AlertingServicesBL.getRequestHistory(requestId);
		
	}
	
	public Page retrieveSubmittedRequestsForAction(String userID , String queueId , String status) throws ModuleException{
		
		return AlertingServicesBL.retrieveSubmittedRequestsForAction(userID , queueId , status);
		
	}
	
	public ServiceResponce deleteRequestQueue(int queueId) throws ModuleException{
		
		return AlertingServicesBL.deleteRequestQueue(queueId);
		
	}
	
	public Page searchRequestQueue(int page, int maxResult, String queueName, String applicablePage) throws ModuleException{
		
		return AlertingServicesBL.searchRequestQueue(page,maxResult,queueName,applicablePage);
		
	}
	
	public List<RequestQueueUser> searchUser(String userNamePart) throws ModuleException{
		
		return AlertingServicesBL.searchUser(userNamePart);
		
	}
	
	public List<ApplicablePage> searchPages(String pageNamePart) throws ModuleException{
		
		return AlertingServicesBL.searchPages(pageNamePart);
		
	}
	
	public void updateRequestQueue(Queue queue, List<String> pageList) throws ModuleException{
		
		 AlertingServicesBL.updateRequestQueue(queue,pageList);
		
	}
	
	public void saveRequestQueue(Queue queue,List<String> pagelist,List<String> add) throws ModuleException{
		
		 AlertingServicesBL.saveRequestQueue(queue,pagelist,add);
		
	}
	
	public void addRemoveUsers(List<String> add,List<String> remove,int queue) throws ModuleException{
		
		 AlertingServicesBL.addRemoveUsers(add,remove,queue);
		
	}
	
	public Queue readRequestQueue(int queue) throws ModuleException{
		
		 return AlertingServicesBL.readRequestQueue(queue);
		
	}
	
	public Page getRequestStatusForPnr(int page, int maxResult,String pnrId) throws ModuleException{
		
		 return AlertingServicesBL.getRequestStatusForPnr(page, maxResult, pnrId);
		
	}
	
	public Page getHistoryForRequest(int page, int maxResult,int requestId) throws ModuleException{
		
		 return AlertingServicesBL.getHistoryForRequest(page, maxResult, requestId);
		
	}

    public List<Integer> getFlightIDList(String fltNum, Date startDateLocal,Date endDateLocal) throws ModuleException {
        return AlertingServicesBL.getFlightIDList(fltNum, startDateLocal, endDateLocal);
    }
	
	public Page retrieveFlightDetailsForSearchCriteria(FlightPnrSearchNotificationDTO searchCriteria) throws ModuleException {
		return AlertingServicesBL.retrieveFlightDetailsForSearchCriteria(searchCriteria);
	}

	public Page retrieveMessageListForLoadCriteria(Date depDate) throws ModuleException {
		return AlertingServicesBL.retrieveMessageListForLoadCriteria(depDate);
	}
}
