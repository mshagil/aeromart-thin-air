package com.isa.thinair.alerting.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO for getting alert details
 * 
 * @author Vinothini
 */
public class AlertDetailDTO implements Serializable {

	private static final long serialVersionUID = -3113523921522577099L;

	private Long alertId;

	private Long alertTypeId;

	private Date alertDate;

	private String flightNumber;

	private Date depDate;

	private String pnr;

	private String content;

	private String privilege;

	private Date fromDate;

	private Date toDate;

	private int totalRecs;

	private int startRec;

	private int endRec;

	private String originAirport;

	private String agentCode;

	private boolean viewAnyAlert;

	private boolean viewOwnAlert;

	private boolean viewReptAgentAlert;

	private boolean checkForRescheduled;

	private Date originDepDate;

	private String originatorPnr;

	private String anciReprotectStatus;

	private String flightStatus;

	private String carrierCode;

	private Long priorityCode;

	private Integer pnrSegId;

	private Date timeStamp;

	/**
	 * The airline to be searched for alerts.
	 */
	private String searchAirline;

	public Long getAlertId() {
		return this.alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public Date getAlertDate() {
		return this.alertDate;
	}

	public void setAlertDate(Date alertDate) {
		this.alertDate = alertDate;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getPnr() {
		return this.pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDepDate() {
		return this.depDate;
	}

	public void setDepDate(Date depDate) {
		this.depDate = depDate;
	}

	public String getPrivilege() {
		return this.privilege;
	}

	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public int getTotalRecs() {
		return this.totalRecs;
	}

	public void setTotalRecs(int totalRecs) {
		this.totalRecs = totalRecs;
	}

	public int getStartRec() {
		return startRec;
	}

	public void setStartRec(int startRec) {
		this.startRec = startRec;
	}

	public int getEndRec() {
		return endRec;
	}

	public void setEndRec(int endRec) {
		this.endRec = endRec;
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the viewAnyAlert.
	 */
	public boolean isViewAnyAlert() {
		return viewAnyAlert;
	}

	/**
	 * @param viewAnyAlert
	 *            The viewAnyAlert to set.
	 */
	public void setViewAnyAlert(boolean viewAnyAlert) {
		this.viewAnyAlert = viewAnyAlert;
	}

	/**
	 * @return Returns the viewOwnAlert.
	 */
	public boolean isViewOwnAlert() {
		return viewOwnAlert;
	}

	/**
	 * @param viewOwnAlert
	 *            The viewOwnAlert to set.
	 */
	public void setViewOwnAlert(boolean viewOwnAlert) {
		this.viewOwnAlert = viewOwnAlert;
	}

	/**
	 * @return Returns the viewReptAgentAlert.
	 */
	public boolean isViewReptAgentAlert() {
		return viewReptAgentAlert;
	}

	/**
	 * @param viewReptAgentAlert
	 *            The viewReptAgentAlert to set.
	 */
	public void setViewReptAgentAlert(boolean viewReptAgentAlert) {
		this.viewReptAgentAlert = viewReptAgentAlert;
	}

	public Date getOriginDepDate() {
		return originDepDate;
	}

	public void setOriginDepDate(Date originDepDate) {
		this.originDepDate = originDepDate;
	}

	public String getOriginatorPnr() {
		return originatorPnr;
	}

	public void setOriginatorPnr(String originatorPnr) {
		this.originatorPnr = originatorPnr;
	}

	/**
	 * @return The search airline
	 */
	public String getSearchAirline() {
		return searchAirline;
	}

	/**
	 * @param searchAirline
	 *            The airline to search
	 */
	public void setSearchAirline(String searchAirline) {
		this.searchAirline = searchAirline;
	}

	/**
	 * @return : the anci reprotect status.
	 */
	public String getAnciReprotectStatus() {
		return anciReprotectStatus;
	}

	/**
	 * @param anciReprotectStatus
	 *            : The anci reprotect status to set.
	 */
	public void setAnciReprotectStatus(String anciReprotectStatus) {
		this.anciReprotectStatus = anciReprotectStatus;
	}

	public String getFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	/**
	 * @return the checkForRescheduled
	 */
	public boolean isCheckForRescheduled() {
		return checkForRescheduled;
	}

	/**
	 * @param checkForRescheduled
	 *            the checkForRescheduled to set
	 */
	public void setCheckForRescheduled(boolean checkForRescheduled) {
		this.checkForRescheduled = checkForRescheduled;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Long getAlertTypeId() {
		return alertTypeId;
	}

	public void setAlertTypeId(Long alertTypeId) {
		this.alertTypeId = alertTypeId;
	}

	public Long getPriorityCode() {
		return priorityCode;
	}

	public void setPriorityCode(Long priorityCode) {
		this.priorityCode = priorityCode;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

}
