package com.isa.thinair.alerting.api.model;

import java.util.List;

public class RequestQueueSearchResult{
	private String description;
	private String queueName;
	private int queueId;
	private List<ApplicablePage> applicablePages;
	private List<QueueUserDTO> queueUsers;
	
	public List<QueueUserDTO> getQueueUsers() {
		return queueUsers;
	}

	public void setQueueUsers(List<QueueUserDTO> queueUsers) {
		this.queueUsers = queueUsers;
	}

	public List<ApplicablePage> getApplicablePages() {
		return applicablePages;
	}

	public void setApplicablePages(List<ApplicablePage> applicablePages) {
		this.applicablePages = applicablePages;
	}

	public int getQueueId() {
		return queueId;
	}

	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	public RequestQueueSearchResult(int queueId,String a,String b){
		this.queueId=queueId;
		description=a;
		queueName=b;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
}
