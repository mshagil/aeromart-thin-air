package com.isa.thinair.alerting.api.dto;

import java.io.Serializable;

public class AlertTemplateDTO implements Serializable {
	private static final long serialVersionUID = 6694088466242779633L;

	private int alertTemplateId;

	private String alertContent;

	private String preferredLangContent;

	/**
	 * @return the alertTemplateId
	 */
	public int getAlertTemplateId() {
		return alertTemplateId;
	}

	/**
	 * @param alertTemplateId
	 *            the alertTemplateId to set
	 */
	public void setAlertTemplateId(int alertTemplateId) {
		this.alertTemplateId = alertTemplateId;
	}

	/**
	 * @return the alertContent
	 */
	public String getAlertContent() {
		return alertContent;
	}

	/**
	 * @param alertContent
	 *            the alertContent to set
	 */
	public void setAlertContent(String alertContent) {
		this.alertContent = alertContent;
	}

	/**
	 * @return the preferredLangContent
	 */
	public String getPreferredLangContent() {
		return preferredLangContent;
	}

	/**
	 * @param preferredLangContent
	 *            the preferredLangContent to set
	 */
	public void setPreferredLangContent(String preferredLangContent) {
		this.preferredLangContent = preferredLangContent;
	}
}
