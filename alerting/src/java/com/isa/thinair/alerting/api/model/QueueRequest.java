/**
 * 
 */
package com.isa.thinair.alerting.api.model;

import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * @hibernate.class table = "T_QUEUE_REQUEST" QueueRequest is the entity class to represent a QueueRequest model
 * @author suneth
 *
 */
public class QueueRequest extends Tracking{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6665035965762216081L;

	private Integer requestId;
	
	private Integer queueId;
	
	private String status;
	
	private String pnr;
	
	private String assignedTo;
	
	private Date assignedOn;
	
	private String description;

	private Set<RequestHistory> requestHistory;
	
	public QueueRequest(){
		
	}
	
	public QueueRequest(UserPrincipal userPrincipal){
		
		setUserDetails(userPrincipal);
		
	} 
	
	/**
	 * Returns the request ID
	 * 
	 * @return the requestId
	 * @hibernate.id column = "QUEUE_REQUEST_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_QUEUE_REQUEST"
	 */
	public Integer getRequestId() {
		return requestId;
	}

	/**
	 * Sets the request ID
	 * 
	 * @param requestId the requestId to set
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * Returns the queue ID of the request
	 * 
	 * @return the queueId
	 * @hibernate.property column = "QUEUE_ID"
	 * 
	 */
	public Integer getQueueId() {
		return queueId;
	}

	/**
	 * Sets the Queue ID of the request
	 * 
	 * @param queueId the queueId to set
	 */
	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	/**
	 * Returns the status of the request
	 * 
	 * @return the status
	 * @hibernate.property column = "QUEUE_REQUEST_STATUS_CODE"
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status of the request
	 * 
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Returns the related pnr for the request
	 * 
	 * @return the pnr
	 * @hibernate.property column = "PNR"
	 * 
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * Sets the related pnr for the request
	 * 
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * Returns the user Id of the person who has assigned the request
	 * 
	 * @return the assignedTo
	 * @hibernate.property column = "ASSIGNED_TO"
	 * 
	 */
	public String getAssignedTo() {
		return assignedTo;
	}

	/**
	 * Sets the user Id of the person who has assigned the request
	 * 
	 * @param assignedTo the assignedTo to set
	 */
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	/**
	 * Returns the request assigned date
	 * 
	 * @return the assignedOn
	 * @hibernate.property column = "ASSIGNED_ON"
	 * 
	 */
	public Date getAssignedOn() {
		return assignedOn;
	}

	/**
	 * Sets the request assigned date
	 * 
	 * @param assignedOn the assignedOn to set
	 */
	public void setAssignedOn(Date assignedOn) {
		this.assignedOn = assignedOn;
	}

	/**
	 * Returns the description of the request
	 * 
	 * @return the description
	 * @hibernate.property column = "QUEUE_REQUEST_DESCRIPTION"
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description of the request
	 * 
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * Returns the history object set of this request
	 * 
	 * @return the requestHistory
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" order-by="TIMESTAMP" inverse="true"
	 * @hibernate.collection-key column="QUEUE_REQUEST_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.alerting.api.model.RequestHistory"
	 * 
	 */
	public Set<RequestHistory> getRequestHistory() {
		return requestHistory;
	}


	/**
	 * Sets the history object set of this request
	 * 
	 * @param requestHistory the requestHistory to set
	 */
	public void setRequestHistory(Set<RequestHistory> requestHistory) {
		this.requestHistory = requestHistory;
	}
	
	

}
