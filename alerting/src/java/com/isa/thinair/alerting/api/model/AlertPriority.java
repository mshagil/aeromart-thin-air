package com.isa.thinair.alerting.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table="T_ALERT_PRIORITY"
 * 
 */
public class AlertPriority extends Persistent implements Serializable {

	private static final long serialVersionUID = -6910086169690312251L;

	/** identifier field */
	private Long priorityCode;

	/** nullable persistent field */
	private String description;

	/** nullable persistent field */
	private Integer priorityLevel;

	/** full constructor */
	public AlertPriority(Long priorityCode, String description, Integer priorityLevel) {
		this.priorityCode = priorityCode;
		this.description = description;
		this.priorityLevel = priorityLevel;
	}

	/** default constructor */
	public AlertPriority() {
	}

	/** minimal constructor */
	public AlertPriority(Long priorityCode) {
		this.priorityCode = priorityCode;
	}

	/**
	 * @hibernate.id generator-class="assigned" type="java.lang.Long" column="PRIORITY_CODE"
	 * 
	 */
	public Long getPriorityCode() {
		return this.priorityCode;
	}

	public void setPriorityCode(Long priorityCode) {
		this.priorityCode = priorityCode;
	}

	/**
	 * @hibernate.property column="DESCRIPTION" length="255"
	 * 
	 */
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @hibernate.property column="PRIORITY_LEVEL" length="2"
	 * 
	 */
	public Integer getPriorityLevel() {
		return this.priorityLevel;
	}

	public void setPriorityLevel(Integer priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public String toString() {
		return new ToStringBuilder(this).append("priorityCode", getPriorityCode()).toString();
	}

	public boolean equals(Object other) {
		if (!(other instanceof AlertPriority))
			return false;
		AlertPriority castOther = (AlertPriority) other;
		return new EqualsBuilder().append(this.getPriorityCode(), castOther.getPriorityCode()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getPriorityCode()).toHashCode();
	}

}
