package com.isa.thinair.alerting.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table="T_ALERT_TEMPLATE"
 */
public class AlertTemplate implements Serializable {

	private static final long serialVersionUID = 1211748305237785491L;

	private int alertTemplateId;

	private String alertTemplateDesc;

	private String alertTemplateContent;

	/**
	 * returns the alertTemplateId
	 * 
	 * @return Returns the alertTemplateId.
	 * 
	 * @hibernate.id column = "ALERT_TEMPLATE_ID" generator-class = "assigned"
	 */
	public int getAlertTemplateId() {
		return alertTemplateId;
	}

	/**
	 * sets the alertTemplateId
	 * 
	 * @param alertTemplateId
	 *            The alertTemplateId to set.
	 */
	private void setAlertTemplateId(int alertTemplateId) {
		this.alertTemplateId = alertTemplateId;
	}

	/**
	 * returns the alertTemplateContent
	 * 
	 * @return Returns the alertTemplateContent.
	 * 
	 * @hibernate.property column = "ALERT_TEMPLATE_CONTENT"
	 */
	public String getAlertTemplateContent() {
		return alertTemplateContent;
	}

	/**
	 * sets the alertTemplateContent
	 * 
	 * @param alertTemplateContent
	 *            The alertTemplateContent to set.
	 */
	private void setAlertTemplateContent(String alertTemplateContent) {
		this.alertTemplateContent = alertTemplateContent;
	}

	/**
	 * returns the alertTemplateDesc
	 * 
	 * @return Returns the alertTemplateDesc.
	 * 
	 * @hibernate.property column = "ALERT_TEMPLATE_DESC"
	 */
	public String getAlertTemplateDesc() {
		return alertTemplateDesc;
	}

	/**
	 * sets the alertTemplateDesc
	 * 
	 * @param alertTemplateDesc
	 *            The alertTemplateDesc to set.
	 */
	private void setAlertTemplateDesc(String alertTemplateDesc) {
		this.alertTemplateDesc = alertTemplateDesc;
	}
}
