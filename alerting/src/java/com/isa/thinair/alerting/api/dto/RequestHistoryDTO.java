/**
 * 
 */
package com.isa.thinair.alerting.api.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author suneth
 *
 */
public class RequestHistoryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4715441373888881098L;
	
	private int historyId;
	
	private int requestId;
	
	private String action;
	
	private String statusChange;
	
	private String statusFrom;
	
	private String statusTo;
	
	private String remark;
	
	private Timestamp timestamp;
	
	private String userId;
	
	public RequestHistoryDTO(){
		
	}
	
	public RequestHistoryDTO(int historyId,int requestId,String action,String statusChange,String remark,Timestamp timestamp,String userId){
		this.historyId=historyId;
		this.requestId=requestId;
		this.action=action;
		this.statusChange=statusChange;
		this.remark=remark;
		this.timestamp=timestamp;
		this.userId=userId;
	}

	/**
	 * @return the historyId
	 */
	public int getHistoryId() {
		return historyId;
	}

	/**
	 * @param historyId the historyId to set
	 */
	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

	/**
	 * @return the requestId
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the statusChange
	 */
	public String getStatusChange() {
		return statusChange;
	}

	/**
	 * @param statusChange the statusChange to set
	 */
	public void setStatusChange(String statusChange) {
		this.statusChange = statusChange;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the statusFrom
	 */
	public String getStatusFrom() {
		return statusFrom;
	}

	/**
	 * @param statusFrom the statusFrom to set
	 */
	public void setStatusFrom(String statusFrom) {
		this.statusFrom = statusFrom;
	}

	/**
	 * @return the statusTo
	 */
	public String getStatusTo() {
		return statusTo;
	}

	/**
	 * @param statusTo the statusTo to set
	 */
	public void setStatusTo(String statusTo) {
		this.statusTo = statusTo;
	}
	
	
}
