/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.dao;

import com.isa.thinair.client.connectivitycheck.core.ProxyConfig;
import com.isa.thinair.client.connectivitycheck.core.Settings;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author danuruddha
 */
public class Persistance {

    private final String CONFIG_FILE = "config.data";
    private final String PROXY_CONFIG_FILE = "proxy_conf.data";

    private void objectWrite(Object object, String path) throws Exception {
        FileOutputStream fos = null;
        ObjectOutputStream out = null;

        fos = new FileOutputStream(path);
        out = new ObjectOutputStream(fos);
        out.writeObject(object);
        out.flush();
        out.close();
        fos.close();
    }

    private Object objectRead(String path) throws Exception {
        Object obj = null;
        FileInputStream fis = null;
        ObjectInputStream in = null;
        try {
            fis = new FileInputStream(path);
            in = new ObjectInputStream(fis);
            obj = in.readObject();
            in.close();
        } catch (ClassNotFoundException cnf) {
            // do nothing
        } catch (Exception e) {
            throw e;
        }

        return obj;
    }

    public void saveSettings(Settings settings) throws Exception {
        objectWrite(settings, CONFIG_FILE);
    }

    public Settings readSettings() throws Exception {
        Settings settings = null;
        try {
            settings = (Settings) objectRead(CONFIG_FILE);
        } catch (Exception e) {
            settings = new Settings();
            saveSettings(settings);
        }
        if (settings == null) {
            settings = new Settings();
        }
        return settings;
    }

    public void saveProxySettings(ProxyConfig config) throws Exception {
        objectWrite(config, PROXY_CONFIG_FILE);
    }

    public ProxyConfig readProxyConfig() throws Exception {
        ProxyConfig config = null;
        try {
            config = (ProxyConfig) objectRead(PROXY_CONFIG_FILE);
        } catch (Exception e) {
            config = new ProxyConfig();
            saveProxySettings(config);
        }
        if (config == null) {
            config = new ProxyConfig();
        }
        return config;
    }
}
