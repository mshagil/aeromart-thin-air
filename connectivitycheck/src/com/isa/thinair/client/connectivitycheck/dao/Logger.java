/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.dao;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author dilan
 */
public class Logger {
    private String logFile;
    public Logger(){
        FixedConfig conf = new FixedConfig();
       logFile = conf.getLogFile();
    }

    public void writeToLog(String entry) {

        try {
            FileWriter fstream = new FileWriter(logFile,true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(entry+"\r\n");
            out.flush();
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
