/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 *
 * @author danuruddha
 */
public class TestSSL {

    public static void main(String args[]) throws Exception {
        HttpClient httpclient = new HttpClient();
        GetMethod httpget = new GetMethod("https://10.200.2.170:8443/airadmin/");
        try {
            httpclient.executeMethod(httpget);
            System.out.println(httpget.getStatusLine());
        } finally {
            httpget.releaseConnection();
        }
    }
}
