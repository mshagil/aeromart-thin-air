/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.core;

/**
 *
 * @author dilan
 */
public class AirlineApplication {

    private URLAccount agent;
    private URLAccount callCenter;
    private URLAccount ibe;
    private URLAccount speed;

    public AirlineApplication(String prefix, URLAccount agent, URLAccount callCenter, URLAccount ibe) {
        agent.setPrefix(prefix);
        callCenter.setPrefix(prefix);
        ibe.setPrefix(prefix);
        this.agent = agent;
        this.callCenter = callCenter;
        this.ibe = ibe;
        this.speed = new URLAccount(URLAccount.SYSTEM_TYPE.SPEED_IMAGE);
        this.speed.setPrefix(prefix);
    }

    public AirlineApplication(String prefix, URLAccount agent) {
        agent.setPrefix(prefix);
        this.agent = agent;
        callCenter = new URLAccount();
        ibe = new URLAccount();
        this.speed = new URLAccount(URLAccount.SYSTEM_TYPE.SPEED_IMAGE);
        this.speed.setPrefix(prefix);
    }

    public AirlineApplication(String prefix, URLAccount agent, URLAccount ibe) {
        agent.setPrefix(prefix);
        ibe.setPrefix(prefix);
        this.agent = agent;
        this.callCenter = new URLAccount();
        this.ibe = ibe;
        this.speed = new URLAccount(URLAccount.SYSTEM_TYPE.SPEED_IMAGE);
        this.speed.setPrefix(prefix);
    }

    public URLAccount getAgent() {
        return agent;
    }

    public URLAccount getCallCenter() {
        return callCenter;
    }

    public URLAccount getIbe() {
        return ibe;
    }

    public URLAccount getSpeedAccount() {
        return speed;
    }
}
