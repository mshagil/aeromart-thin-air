/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.comm;

import com.isa.thinair.client.connectivitycheck.dao.Logger;
import com.isa.thinair.client.connectivitycheck.core.ProxyConfig;
import com.isa.thinair.client.connectivitycheck.core.TestReport;
import java.io.IOException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

/**
 *
 * @author danuruddha
 */
public class NotificationStub {

    private String NotificationURL = "http://localhost/notification.php";
    private boolean ENABLE_HTTP_NOTIFICATIONS = false;
    private boolean ENABLE_SMTP_NOTIFICATIONS = true;

    public void notifyAll(TestReport report) {
        /*write to the log */
        Logger log = new Logger();
        log.writeToLog(report.toLog());

        if (ENABLE_HTTP_NOTIFICATIONS) {
            /*Making an http call */
            HttpClient client = new HttpClient();
            ProxyConfig proxyConfig = report.getProxyConfig();

            if (!proxyConfig.isNoProxy()) {
                client.getHostConfiguration().setProxy(proxyConfig.getProxyHost(), proxyConfig.getProxyPort());
            }

            PostMethod post = new PostMethod(NotificationURL);
            NameValuePair[] data = {
                new NameValuePair("status", report.toWeb())
            };
            post.setRequestBody(data);
            try {
                client.executeMethod(post);
                int statusCode = post.getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    System.out.println("Notification to the ISA URL " + NotificationURL + " is OK");
                } else {
                    System.out.println("Notification failed");
                }
            } catch (IOException ex) {
                System.out.println("Notification failed");
            } finally {
                post.releaseConnection();
            }

        }

        if (ENABLE_SMTP_NOTIFICATIONS) {
            /*Notification via SMTP */
            MailSender smtpSender = new MailSender();
            smtpSender.sendMail("AccelAero Reports", report.toMail(), MailSender.MailStatus.NORMAL);
        }
    }
}
