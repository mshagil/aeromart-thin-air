/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.isa.thinair.client.connectivitycheck.comm;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 *
 * @author danuruddha
 */
public class SimpleSmtpAuthenticator extends Authenticator{
    private String username ;
    private String password;
    public SimpleSmtpAuthenticator(String username,String password){
     this.username = username;
     this.password = password;
    }

    public PasswordAuthentication getPasswordAuthentication() {

		return new PasswordAuthentication(username,
				password);
	}
}
