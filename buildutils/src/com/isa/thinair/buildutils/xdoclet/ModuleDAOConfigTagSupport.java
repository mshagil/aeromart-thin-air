/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 5, 2005
 * 
 * ===============================================================================
 */
package com.isa.thinair.buildutils.xdoclet;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import xdoclet.XDocletTagSupport;
import xjavadoc.XClass;

/**
 * Class to define custom xdoclet tags to generate the module dao configguration file
 * 
 * @author Lasantha Pambagoda
 */
public class ModuleDAOConfigTagSupport extends XDocletTagSupport {

	/**
	 * list of interfaces implemented by this class
	 * 
	 * @param prop
	 * @return list of interfaces implemented by this class
	 */
	public String daoInterfaces(Properties prop) {

		List list = getCurrentClass().getInterfaces();
		Iterator it = list.iterator();

		boolean first = true;
		String interList = "";

		while (it.hasNext()) {
			XClass xclass = (XClass) it.next();
			String interName = xclass.getQualifiedName();
			if (!interName.equals(INTERFACE_NOT_TO_INCLUDE)) {
				interList = first ? interName : (interList + "," + interName);
				first = false;
			}
		}
		return interList;
	}

	public String hbmFileName(Properties prop) {
		String className = getCurrentClass().getType();
		String fileName = className.substring(0, className.lastIndexOf(".class"));
		fileName = fileName.replace(FST, BSLSH) + HBM_EXT;
		return fileName;
	}

	/** a interface not to incude in the module dao config file */
	private static final String INTERFACE_NOT_TO_INCLUDE = "org.springframework.beans.factory.InitializingBean";

	/** extentiion of the hibernate mapping file */
	private static final String HBM_EXT = ".hbm.xml";

	// rest
	/** fullstop */
	private static final char FST = '.';

	/** back slash */
	private static final char BSLSH = '/';

}
