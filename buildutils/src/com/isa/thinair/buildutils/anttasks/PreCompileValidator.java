/*
 * Created on Jun 26, 2005
 *
 */
package com.isa.thinair.buildutils.anttasks;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * @author Nilindra Fernando
 * @since March 19, 2008
 */
public class PreCompileValidator extends Task {

	private String projectCarrierCode;

	private String isJSFilesCompress;

	public void execute() {
		log("Pre Compile Validation initiated...");

		if (projectCarrierCode != null && projectCarrierCode.trim().length() > 0) {
			log("Project carrier code detected as " + projectCarrierCode);
		} else {
			throw new BuildException("Project carrier code is not defined. "
					+ "Please check whether a value exist for ${project.carrier.code}"
					+ " [Hint: Have you checked the project.properties file?] ");
		}

		if (isJSFilesCompress != null && isJSFilesCompress.trim().equalsIgnoreCase("true")) {
			log("Property [project.js.compress.do] set to [true] ");
			getProject().setNewProperty("project.js.compress.do", "true");
		}
	}

	/**
	 * @return Returns the projectCarrierCode.
	 */
	public String getProjectCarrierCode() {
		return projectCarrierCode;
	}

	/**
	 * @param projectCarrierCode
	 *            The projectCarrierCode to set.
	 */
	public void setProjectCarrierCode(String projectCarrierCode) {
		this.projectCarrierCode = projectCarrierCode;
	}

	/**
	 * @return Returns the isJSFilesCompress.
	 */
	public String getIsJSFilesCompress() {
		return isJSFilesCompress;
	}

	/**
	 * @param isJSFilesCompress
	 *            The isJSFilesCompress to set.
	 */
	public void setIsJSFilesCompress(String isJSFilesCompress) {
		this.isJSFilesCompress = isJSFilesCompress;
	}
}
