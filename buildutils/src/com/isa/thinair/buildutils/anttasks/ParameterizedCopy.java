/*
 * Created on 06-Jul-2005
 *
 */
package com.isa.thinair.buildutils.anttasks;

import java.io.File;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.types.FileSet;

/**
 * @author Nilindra Fernando
 * @since March 19, 2008
 */
public class ParameterizedCopy extends Task {

	private String excludes = "";

	private String includes = "**/**";

	private File projectBaseDir;

	private File projectOverrideBaseDir;

	private File todir;

	public File getProjectBaseDir() {
		return projectBaseDir;
	}

	public void setProjectBaseDir(File projectBaseDir) {
		this.projectBaseDir = projectBaseDir;
	}

	public void execute() {

		if (projectBaseDir != null && projectBaseDir.exists()) {
			Copy copy = new Copy();
			copy.setTaskName(this.getTaskName());
			copy.setProject(this.getProject());
			copy.setOverwrite(false);
			copy.setTodir(todir);

			FileSet fileset = new FileSet();
			fileset.setDir(projectBaseDir);
			fileset.setIncludes(includes);
			fileset.setExcludes(excludes);

			copy.addFileset(fileset);
			copy.execute();
		}

		if (projectOverrideBaseDir != null && projectOverrideBaseDir.exists()) {
			Copy copy = new Copy();
			copy.setTaskName(this.getTaskName());
			copy.setProject(this.getProject());
			copy.setOverwrite(true);
			copy.setTodir(todir);

			FileSet fileset = new FileSet();
			fileset.setDir(projectOverrideBaseDir);
			fileset.setIncludes(includes);
			fileset.setExcludes(excludes);

			copy.addFileset(fileset);
			copy.execute();
		}
	}

	/**
	 * @return Returns the excludes.
	 */
	public String getExcludes() {
		return excludes;
	}

	/**
	 * @param excludes
	 *            The excludes to set.
	 */
	public void setExcludes(String excludes) {
		this.excludes = excludes;
	}

	/**
	 * @return Returns the includes.
	 */
	public String getIncludes() {
		return includes;
	}

	/**
	 * @param includes
	 *            The includes to set.
	 */
	public void setIncludes(String includes) {
		this.includes = includes;
	}

	/**
	 * @return Returns the projectOverrideBaseDir.
	 */
	public File getProjectOverrideBaseDir() {
		return projectOverrideBaseDir;
	}

	/**
	 * @param projectOverrideBaseDir
	 *            The projectOverrideBaseDir to set.
	 */
	public void setProjectOverrideBaseDir(File projectOverrideBaseDir) {
		this.projectOverrideBaseDir = projectOverrideBaseDir;
	}

	/**
	 * @return Returns the todir.
	 */
	public File getTodir() {
		return todir;
	}

	/**
	 * @param todir
	 *            The todir to set.
	 */
	public void setTodir(File todir) {
		this.todir = todir;
	}
}
