/*
 * Created on 06-Jul-2005
 *
 */
package com.isa.thinair.buildutils.anttasks;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.types.FileSet;

/**
 * @author Nasly
 * 
 */
public class RepositoryPopulator extends Copy {
	private final static String PLATFORM_MODULE_NAME = "platform";

	private final static String COMMONS_MODULE_NAME = "commons";

	private final static String FILE_EXT = "*.mod.xml";

	private final static String WEBPLATFORM_MODULE_NAME = "webplatform";

	private String fileNamesToInclude = FILE_EXT;

	private String modulesNeedAllFiles = "webplatform";

	private List modulesNeedAllFilesList = new ArrayList();

	private String moduleNames;

	private List modulesList = new ArrayList();

	private File projectBaseDir;

	private String projectCarrierCode;

	public String getModuleNames() {
		return moduleNames;
	}

	public void setModuleNames(String moduleNames) {
		this.moduleNames = moduleNames;
	}

	public List getModulesList() {
		setModulesList();
		return modulesList;
	}

	public void setModulesList() {
		StringTokenizer tokenizer = new StringTokenizer(getModuleNames(), ",");
		while (tokenizer.hasMoreTokens()) {
			modulesList.add(tokenizer.nextToken().trim());
		}
	}

	public String getFileNamesToInclude() {
		return fileNamesToInclude;
	}

	public void setFileNamesToInclude(String fileNamesToInclude) {
		this.fileNamesToInclude = fileNamesToInclude;
	}

	public File getProjectBaseDir() {
		return projectBaseDir;
	}

	public void setProjectBaseDir(File projectBaseDir) {
		this.projectBaseDir = projectBaseDir;
	}

	public String getModulesNeedAllFiles() {
		return modulesNeedAllFiles;
	}

	public void setModulesNeedAllFiles(String modulesNeedAllFiles) {
		this.modulesNeedAllFiles = modulesNeedAllFiles;
		if (modulesNeedAllFiles != null) {
			String[] moduleNames = StringUtils.split(modulesNeedAllFiles, ",");
			if (moduleNames != null) {
				for (int i = 0; i < moduleNames.length; i++)
					modulesNeedAllFilesList.add(moduleNames[i].trim());
			} else {
				modulesNeedAllFilesList.add(WEBPLATFORM_MODULE_NAME);
			}
		}
	}

	public void execute() {
		setOverwrite(true);
		Iterator it = getModulesList().iterator();

		while (it.hasNext()) {

			String moduleDirName = (String) it.next();
			System.out.println("adding configuration files from module [" + moduleDirName + "]");
			if (!moduleDirName.equals(PLATFORM_MODULE_NAME)) {
				File dir = new File(projectBaseDir, moduleDirName + File.separator + "src" + File.separator + "resources");

				if (dir.exists()) {
					System.out.println("Updating config repository from [" + dir.getAbsolutePath() + "]");
					addFileset(moduleDirName, dir);
				}

				if (projectCarrierCode != null && projectCarrierCode.trim().length() > 0) {
					dir = new File(projectBaseDir, moduleDirName + File.separator + "src" + File.separator + "clients"
							+ File.separator + projectCarrierCode + File.separator + "resources");

					if (dir.exists()) {
						System.out.println("Updating config repository from [" + dir.getAbsolutePath() + "]");
						addFileset(moduleDirName, dir);
					}
				}
			}
		}

		super.execute();
	}

	/**
	 * Add file set
	 */
	private void addFileset(String moduleDirName, File dir) {
		FileSet fileset = new FileSet();
		fileset.setDir(dir);
		if (modulesNeedAllFilesList.contains(moduleDirName)) {
			fileset.setIncludes("**/" + moduleDirName + "/" + FILE_EXT);
		} else if (moduleDirName.equals(COMMONS_MODULE_NAME)) {
			fileset.setIncludes("**/" + moduleDirName + "/" + FILE_EXT);
			fileset.setExcludes("**/" + moduleDirName + "/" + "persistence.mod.xml");
		} else {
			String[] extensions = null;
			if (fileNamesToInclude != null && !fileNamesToInclude.equals(""))
				extensions = StringUtils.split(fileNamesToInclude, ",");

			if (extensions != null) {
				for (int i = 0; i < extensions.length; i++) {
					String include = "**/" + moduleDirName + "/" + extensions[i];
					System.out.println("Including = " + include);
					fileset.setIncludes(include);
				}
			} else {
				fileset.setIncludes("**/" + moduleDirName + "/" + FILE_EXT);
			}
		}
		addFileset(fileset);
	}

	/**
	 * @return Returns the projectCarrierCode.
	 */
	public String getProjectCarrierCode() {
		return projectCarrierCode;
	}

	/**
	 * @param projectCarrierCode
	 *            The projectCarrierCode to set.
	 */
	public void setProjectCarrierCode(String projectCarrierCode) {
		this.projectCarrierCode = projectCarrierCode;
	}
}
