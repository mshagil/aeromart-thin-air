$.fn.ajaxSubmit = function(options) {
	 // abort any pending request
	var settings = {
				dataType: 'json',
				processData: false,
				data:{},
				url:null,
				async:true,
				beforeSubmit: function(){return true},
				success: function(){return true},
				error:function(){return true},
				complete:function(){return true}
				}
	var o = $.extend(settings, options);
	var request;
    if (request) {
        request.abort();
    }
    
    $.fn.serializeObject = function(){
    	var serializedData = this.serializeArray();
        
    	//Sometimes data is just put there in an Array object. This result in an zero length array with data that cannot be iterated.
    	if(Object.prototype.toString.call(o.data) == "[object Array]"){
    		o.data=$.extend({},o.data);
    	}
	    	
    	//Add the passed data to the serialized array in the same format.
        $.each(o.data, function(key,value) {
        	//Remove the duplicate element
        	serializedData = $.grep(serializedData, function(element) { 
        		return element.name == key;
        	},true);
        	
            serializedData.push({'name':key,'value':value});
        });
        
        return serializedData;
     };
     
    // setup some local variables
    var $form = $(this);
    // let's select and cache all the fields
    var $inputs = $form.find("input, select, button, textarea");
    // serialize the data in the form
    var serializedData = $form.serializeObject();
 
    // disable the inputs for the duration of the ajax request
    
    
    
    // fire off the request to /form.php
    if (o.url==null)o.url=$(this).attr("action");
    var request = $.ajax({
        url: o.url,
        dataType: o.dataType,
        beforeSend : o.beforeSubmit, 
        type:"post",
        cache: false,
        async:o.async,
        data: serializedData
    });
 
    // callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // log a message to the console
        o.success(response, textStatus, jqXHR);
    });
 
    // callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // log the error to the console
       /* console.error(
            "The following error occured: "+
            textStatus, errorThrown
        );*/
    	 o.error(jqXHR, textStatus, errorThrown);
    });
 
    // callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function (response, textStatus, jqXHR) {
        // reenable the inputs
        //$inputs.prop("disabled", false);
    	o.complete(response, textStatus, jqXHR);
    });
 
    // prevent default posting of form
    //event.preventDefault();
};