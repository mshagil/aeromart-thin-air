
	$(function () {
		$(".singleProp").live("click" , function(){
			var collepse = $(this).next(".prop").css("display")
			$(this).next(".prop").slideToggle();
			if(collepse == "block")
				$(this).removeClass("collepse");
			else
				$(this).addClass("collepse");	
		});
		
		$(".fares").live("click", function (){
			if ($(this).hasClass("selected")){
				$(this).removeClass("selected");
				hideButton();
				removeItrmProperties(this.id);
				//$("#"+this.id+" .cl").slider("disable");
			}else{	
				$(this).addClass("selected");
				//$("#"+this.id+" .cl").slider("enable");
				displayButton($(this).position());
				addItrmProperties(this.id);
			}
				$(".properties").slideDown();
		});
		
		function displayButton(pos){
			var position = pos;
			$(".buttonSet").css("position","absolute");
			$(".buttonSet").css("left",position.left+390);
			$(".buttonSet").css("top",position.top);
			$(".buttonSet").fadeIn("slow");
		}
		
		function hideButton(){
			$(".buttonSet").fadeOut("slow");
		}
		
		addItrmProperties = function(obj){
			var aLength = jsfareProperties.length;
			jsfareProperties[aLength] = {
						 name:obj,
						 fareValue:$("#"+obj+"FV").val(),
						 bcalss:$("#"+obj+"bClass").val(),
						 svs:$("#"+obj+"SVS").val(),
						 sve: $("#"+obj+"SVE").val(),
						 dvs:$("#"+obj+"DVS").val(),
						 dve:$("#"+obj+"DVE").val(),
						 rvs:$("#"+obj+"RVS").val(),
						 rve:$("#"+obj+"RVE").val()
						};
			fillproperties(jsfareProperties);
			$("#selectedFare").val($("#"+obj+"FV").val());
			$("#selectedSeg").val(obj);
			$("#selectedClass").val('H');
			$("#selectedBasis").val($("#"+obj+"bClass").val());
			$('.oldfare').val($("#selectedFare").val());
		};

		removeItrmProperties = function(obj){
			$.each (jsfareProperties, function (key, value){
				if (value.name == obj){
					jsfareProperties[key] = {};
				}
				$("#selectedFare").val("");
				$("#selectedSeg").val("");
				$("#selectedClass").val("");
				$("#selectedBasis").val("");
				$('.oldfare').val($("#selectedFare").val());
			});
			fillproperties(jsfareProperties) 
		};

		fillproperties = function (filled){
			var str = "";
			$(".addProperty").empty();
			
			$.each (filled, function (key, value){
				if (value.name != undefined || value.svs != undefined){
					str +='<div class="singleProp"><div class="icon"></div><b>'+value.name+'</b></div>';
					str +='<div class="prop"><table cellspacing="0" cellpadding="0" align="center" width="100%">'+
					'<tr><td>Fare id</td><td>'+value.name+'</td></tr>'+
					'<tr><td>Fare value</td><td>'+value.fareValue+'</td></tr>'+
					'<tr><td>Base Class</td><td>'+value.bcalss+'</td></tr>'+
					'<tr><td>SVS</td><td>'+value.svs+'</td></tr>'+
					'<tr><td>SVE</td><td>'+value.sve+'</td></tr>'+
					'<tr><td>DVS</td><td>'+value.dvs+'</td></tr>'+
					'<tr><td>DVE</td><td>'+value.dve+'</td></tr>'+
					'<tr><td>RVS</td><td>'+value.rvs+'</td></tr>'+
					'<tr><td>RVE</td><td>'+value.rve+'</td></tr>'+
					'</table></div>';
				}
			});
			$(".addProperty").html(str);
			$(".addProperty div:last-child").css("display","block");
			$(".addProperty div:last-child").prev(".singleProp").addClass("collepse")
			
		};
		
		$("._chk").live("click",function(){
			var _kta = this.id.split("_");
			if (_kta[1]== "svcheck")
				dispalyFares(_kta[0], $("#"+this.id).attr("checked"),"Sales");
			else if (_kta[1]== "dvcheck")
				dispalyFares(_kta[0], $("#"+this.id).attr("checked"),"Departure");
			else
				dispalyFares(_kta[0], $("#"+this.id).attr("checked"),"Return");
		});
		
		function dispalyFares(_parent, chk,_class){
			if (chk){
				$("#load-"+_parent+" ."+_class).css("display","block");
			}else{
				$("#load-"+_parent+" ."+_class).css("display","none");
			}
		};
		
		$(".cabinClass span").live("click",function(){
			$(".cabinClass span").removeClass("select");
			$(this).addClass("select");
		});
		
		$("#tabs2").tabs();
		
		$("._save").live("click", function(){
			$("#tabs").css("display","none");
			$("._save").css("display","none");
			$(".bDetails").animate(
			{width: 1},
			{duration:500,
				complete: function() {
					$(".bDetails").css("display","none");
				}
			});
		});
		
		

	});
	