var errorLog = [];
var top_0 = document;
var top_1 = document;
var top_2 = document;
var errHTML = '<div id="errorMSGouter" style="display: none;"><div class="errorBox"><span id="errorMSG" style=""></span><span class="errorClose">&nbsp;</span></div></div>';
//alert(window.top[2]);
if (window.top[2] != undefined && window.top[2].name.toUpperCase() == "BOTTOM"){
	errorLog = window.parent.top.errorLog;
	top_0 = window.top[0];
	top_1 = window.top[1];
	top_2 = window.top[2];
}else{
	top_0 = this.document;
	top_1 = this.document;
	top_2 = this.document;
	$("body").append(errHTML);
	var erroImgPath = "";
}		

		function addtoErrorLog(o){
			errorLog[errorLog.length] = o
			setTimeout("closeError()",7000);
			if (errorLog.length > 0){
				$("#spnErrorLog", top_0.document).show();
				$("#spnErrorLog", top_0.document).css({
					display: "block"
				});
				//$("#spnErrorLog", top_0).bind("click")
				$("#spnErrorLog", top_0.document).parent().addClass("errorIconBG");
				$("#imgMM1", top_0.document).attr("src",erroImgPath);
			}
		};
		function loadErrors(type){
			var str ="";
			var errObj = errorLog; 
			$("#errorPopup", top_1.document).find("#errorPopupTable").html("");
			$.each(errObj, function(i,k){
				if (type == k.type){
					var code = (k.code == "")? "&nbsp;" : k.code;
					str += "<tr>";
					str += "<td width='5%' class='errorBoder "+k.type+"' >&nbsp;</td>";
					str += "<td width='20%' class='errorBoder code'>"+code+"</td>";
					str += "<td width='75%' class='errorBoder'>"+k.discription+"</td>"
					str += "</tr>";
				}else if(type == "All"){
					var code = (k.code == "")? "&nbsp;" : k.code;
					str += "<tr>";
					str += "<td width='5%' class='errorBoder "+k.type+"' >&nbsp;</td>";
					str += "<td width='20%' class='errorBoder code'>"+code+"</td>";
					str += "<td width='75%' class='errorBoder'>"+k.discription+"</td>"
					str += "</tr>";
				}
			});
			$("#errorPopup", top_1.document).find("#errorPopupTable").html(str);
		};
		
		$("#spnErrorLog", top_0.document).live("click", function(){
			$("html, body", top_1.document).find("#errorPopup").remove();
			$("html, body", top_1.document).append($("#errorPopup").clone());
			$("#errorPopup", top_1.document).css({
				position:"absolute",
				top:"0px",
				right:"10px",
				zIndex:"3000",
				display:"block",
				width:"400px",
				height:"30px"
			});
			$("#errorPopup", top_1.document).animate({height:"261px"},{duration:300});
			if ($("body", top_1.document).length>1)
				$("body", top_1.document).find("#errorPopup").remove();
			
			$(".errorPopupHeader .errorClose", top_1.document).bind("click",function(){
				$(this).parent().parent().remove();
				$("html", top_1.document).find("#errorPopup").remove();
				if (errorLog == ""){
					$("#spnErrorLog", top_0.document).hide();
					$("#spnErrorLog", top_0.document).parent().removeClass("errorIconBG");
				}
			});
			$("#errorPopup .errorClear", top_1.document).bind("click",function(){
				$("#errorPopup", top_1.document).find("#errorPopupTable").html("");
				errorLog = [];
			});
			$(".errorBtn", top_1.document).bind("click",function(){
				$(".errorBtn", top_1.document).removeClass("errorSelected");
				$(this, top_1.document).addClass("errorSelected");
				if ($(this, top_1.document).hasClass("errorClear"))
					$(this, top_1.document).removeClass("errorSelected");
				else if ($(this, top_1.document).hasClass("showErrors"))
					loadErrors("Error");
				else if ($(this, top_1.document).hasClass("showWarnings"))
					loadErrors("Warning");
				else if ($(this, top_1.document).hasClass("showConfirm"))
					loadErrors("Confirm");
				else
					loadErrors("All");
			});
			loadErrors("All");
		});

		function  closeError(){
			$("#errorMSGouter", top_1.document).fadeOut();
			$("#errorMSGouter", top_1.document).remove();
		};
		
		$("#spnPLRListIcon", top_0.document).live("click",function(){
			if (window.top[1].UI_searchReservation != undefined){
				window.top[1].UI_searchReservation.moveClick();
			}
				
		});
		
		function dispalyError(errorTxt,type1){
			var trunckError = function(error){
				var str = "";
				if (error.length>100){str = error.substr(0,95)+ "... ";}else{str = error;}
				return str;
			};
		
			var type = (type1 == "") ? newMsgType : type1;
			var obj = "";
			if (errorTxt!=null && errorTxt.indexOf(": ")>-1){
				var temp = errorTxt.split(":");
				obj = {"code":temp[0],"discription":temp[1],"type":type};
			}else{
				obj = {"code":"","discription":errorTxt,"type":type};
			}
			addtoErrorLog(obj);
			if ($("#errorMSGouter").length==0){
				$("body").append(errHTML);
			}
			$("html, body", top_1.document).append($("#errorMSGouter").clone());
			
			$(".errorBox .errorClose", top_1.document).bind("click",function(){
				closeError();
			});
			
			$("#errorMSG", top_1.document).html(trunckError(errorTxt));
			
			$("#errorMSG", top_1.document).addClass(type);
			var winWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			$("#errorMSGouter", top_1.document).css({
				position:"absolute",
				top:"0px",
				left:(winWidth - 550) /2,
				display:"block",
				width:"550px",
				zIndex:"39"
			});
			$(".errorBox", top_2.document).css({
				position:"relative",
				opacity:"1",
				left:"0px"
			});
		};