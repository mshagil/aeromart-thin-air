package com.isa.thinair.webplatform.api.util;

import generated.PTCFareBreakdownType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.OTAAirBookRQ.PriceInfo;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTravelerEticket;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Eticket;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.ReservationAssemblerConvertUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.webplatform.api.dto.CurrencyInfoTO;
import com.isa.thinair.webplatform.api.dto.PAXSummeryDTO;
import com.isa.thinair.webplatform.api.dto.ResBalancesSummaryDTO;
import com.isa.thinair.webplatform.api.dto.SegmentSummeryDTO;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.WSClientConfig;

/**
 * Reservation alteration helper functionalities.
 * 
 * @author Mohamed Nasly
 */
public class ReservationUtil {

	private static Log log = LogFactory.getLog(ReservationUtil.class);

	private static WSClientConfig wsClientConfig = WSClientModuleUtils.getModuleConfig();

	/**
	 * Returns OND balances and PAX balances for reservation alteration.
	 * 
	 * @param res
	 * @param pnrSegIds
	 * @param alterationType
	 * @return
	 * @throws ModuleException
	 */
	public static ResBalancesSummaryDTO getResBalancesForAlt(Reservation res, Collection<OndFareDTO> fareChargesAndSegmentDTOs,
			Collection<Integer> pnrSegIds, String alterationType, Collection parentPnrPaxIDs) throws ModuleException {
		ResBalancesSummaryDTO resSummaryDTO = new ResBalancesSummaryDTO();

		// Pax charges for specified pnrSegments
		Collection<PnrChargesDTO> pnrPaxCharges = null;
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)) {
			pnrPaxCharges = WPModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), false, false, true, res.getVersion());
		} else if (ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
			pnrPaxCharges = WPModuleUtils.getResSegmentBD().getOndChargesForCancellation(res.getPnr(), pnrSegIds, false,
					res.getVersion(), null, false, false);
		} else if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			Collection<Integer> newFltSegIds = new ArrayList<Integer>();
			for (OndFareDTO fareChargesAndSegmentDTO : fareChargesAndSegmentDTOs) {
				newFltSegIds.addAll(fareChargesAndSegmentDTO.getSegmentsMap().keySet());
			}
			pnrPaxCharges = WPModuleUtils.getResSegmentBD().getOndChargesForModification(res.getPnr(), pnrSegIds, newFltSegIds,
					false, true, res.getVersion(), null, false);
		} else if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			pnrPaxCharges = WPModuleUtils.getResSegmentBD().getPnrCharges(res.getPnr(), false, false, true, res.getVersion());
		}
		Map<Integer, PnrChargesDTO> pnrPaxChargesMap = null;
		if (pnrPaxCharges != null) {
			pnrPaxChargesMap = indexOndChargesByPax(pnrPaxCharges);
		}

		// Current and new OND balances
		resSummaryDTO
				.setSegmentSummeryDTO(getONDBalancesForAlt(res, pnrPaxChargesMap, fareChargesAndSegmentDTOs, alterationType));

		// Pax balances
		resSummaryDTO.setPaxSummaryDTOCol(getPaxBalancesForAlt(res, fareChargesAndSegmentDTOs, pnrPaxChargesMap, alterationType,
				parentPnrPaxIDs));

		// Reservation balances
		BigDecimal totalNewPrice = null;
		if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			totalNewPrice = AccelAeroCalculator.add( // current charges
					resSummaryDTO.getSegmentSummeryDTO().getNewTotalCharges(), // new charges
					resSummaryDTO.getSegmentSummeryDTO().getIdentifiedTotalCnxCharge(), // cnx charges
					resSummaryDTO.getSegmentSummeryDTO().getIdentifiedTotalModCharge()); // mod charges
		} else {
			totalNewPrice = AccelAeroCalculator.add((AccelAeroCalculator.subtract(res.getTotalChargeAmount(), resSummaryDTO
					.getSegmentSummeryDTO().getCurrentRefunds())), // current charges
					resSummaryDTO.getSegmentSummeryDTO().getNewTotalCharges(), // new charges
					resSummaryDTO.getSegmentSummeryDTO().getIdentifiedTotalCnxCharge(), // cnx charges
					resSummaryDTO.getSegmentSummeryDTO().getIdentifiedTotalModCharge()); // mod charges
		}

		BigDecimal newBalanceAmount = AccelAeroCalculator.subtract(totalNewPrice, // new charges
				res.getTotalPaidAmount());// existing payment

		resSummaryDTO.setTotalCnxChargeForCurrentAlt(resSummaryDTO.getSegmentSummeryDTO().getIdentifiedTotalCnxCharge());
		resSummaryDTO.setTotalModChargeForCurrentAlt(resSummaryDTO.getSegmentSummeryDTO().getIdentifiedTotalModCharge());
		resSummaryDTO.setTotalAmountDue(newBalanceAmount.doubleValue() > 0 ? newBalanceAmount : AccelAeroCalculator
				.getDefaultBigDecimalZero());
		resSummaryDTO.setTotalAvailableBalance(newBalanceAmount.doubleValue() < 0
				? newBalanceAmount.negate()
				: AccelAeroCalculator.getDefaultBigDecimalZero());
		resSummaryDTO.setTotalPrice(totalNewPrice);

		return resSummaryDTO;
	}

	/**
	 * Return pnrPaxCharges indexed by pnrPaxId.
	 * 
	 * @param pnrPaxCharges
	 * @return
	 */
	public static Map<Integer, PnrChargesDTO> indexOndChargesByPax(Collection<PnrChargesDTO> pnrPaxCharges) {
		Map<Integer, PnrChargesDTO> pnrPaxChargesMap = new HashMap<Integer, PnrChargesDTO>();
		for (Iterator<PnrChargesDTO> pnrPaxChargesIt = pnrPaxCharges.iterator(); pnrPaxChargesIt.hasNext();) {
			PnrChargesDTO pnrPaxChargesDTO = pnrPaxChargesIt.next();
			pnrPaxChargesMap.put(pnrPaxChargesDTO.getPnrPaxId(), pnrPaxChargesDTO);
		}
		return pnrPaxChargesMap;
	}

	/**
	 * Returns Balances for reservation alterations (Modifications/Cancellations/Additions) for specified OND(s).
	 * 
	 * @param res
	 * @param fareChargesAndSegmentDTOs
	 * @param type
	 * @return
	 */
	public static SegmentSummeryDTO getONDBalancesForAlt(Reservation res, Map<Integer, PnrChargesDTO> pnrPaxChargesMap,
			Collection<OndFareDTO> fareChargesAndSegmentDTOs, String alterationType) {

		SegmentSummeryDTO segmentSummeryDTO = new SegmentSummeryDTO();

		BigDecimal identifiedTotCnx = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal identifiedTotMod = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal identifiedTotExtraFees = AccelAeroCalculator.getDefaultBigDecimalZero();

		// For existing OND
		BigDecimal currentTotFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotTaxAndSurcharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotMod = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotCnx = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotAdj = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotRefundable = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotNonRefundable = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {

			for (Iterator<ReservationPax> paxIt = res.getPassengers().iterator(); paxIt.hasNext();) {
				ReservationPax pax = paxIt.next();
				if (ReservationApiUtils.isAdultType(pax) || ReservationApiUtils.isChildType(pax)) {
					PnrChargesDTO paxOndCharges = pnrPaxChargesMap.get(pax.getPnrPaxId());
					BigDecimal[] actualAmounts = paxOndCharges.getActualAmounts();
					currentTotFare = AccelAeroCalculator.add(currentTotFare, paxOndCharges.getActualFare());
					currentTotTaxAndSurcharges = AccelAeroCalculator.add(currentTotTaxAndSurcharges, actualAmounts[1],
							actualAmounts[2]);// tax+sur charges
					currentTotCnx = AccelAeroCalculator.add(currentTotCnx, actualAmounts[3]);// existing cnx charges
					currentTotMod = AccelAeroCalculator.add(currentTotMod, actualAmounts[4]);// existing mod charges
					currentTotAdj = AccelAeroCalculator.add(currentTotAdj, actualAmounts[5]);
					currentTotCharges = AccelAeroCalculator.add(currentTotCharges, paxOndCharges.getTotalActualAmount());
					currentTotNonRefundable = AccelAeroCalculator.add(
							currentTotNonRefundable,
							(AccelAeroCalculator.subtract(paxOndCharges.getTotalActualAmount(),
									paxOndCharges.getTotalRefundableAmount())));
					currentTotRefundable = AccelAeroCalculator
							.add(currentTotRefundable, paxOndCharges.getTotalRefundableAmount());

					identifiedTotCnx = AccelAeroCalculator.add(identifiedTotCnx, paxOndCharges.getIdentifiedCancellationAmount()); // identified
																																	// cnx
																																	// charge
																																	// for
																																	// current
																																	// operation
					identifiedTotMod = AccelAeroCalculator.add(identifiedTotMod, paxOndCharges.getIdentifiedModificationAmount());// identified
																																	// mod
																																	// charge
																																	// for
																																	// current
																																	// modification
					identifiedTotExtraFees = AccelAeroCalculator.add(identifiedTotExtraFees,
							paxOndCharges.getIdentifiedExtraFeeAmount());
				}
			}
		}

		// For new OND
		BigDecimal newTotFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotTaxAndSurcharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotMod = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotCnx = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotAdj = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotRefundable = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotNonRefundable = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {

			BigDecimal[] faresAndCharges = getFaresAndCharges(fareChargesAndSegmentDTOs);

			newTotFare = AccelAeroCalculator.add(
					(AccelAeroCalculator.multiply(faresAndCharges[0], new BigDecimal(res.getTotalPaxAdultCount()))),
					(AccelAeroCalculator.multiply(faresAndCharges[2], new BigDecimal(res.getTotalPaxInfantCount()))),
					(AccelAeroCalculator.multiply(faresAndCharges[4], new BigDecimal(res.getTotalPaxChildCount()))));
			newTotTaxAndSurcharges = AccelAeroCalculator.add(
					AccelAeroCalculator.multiply(faresAndCharges[1], new BigDecimal(res.getTotalPaxAdultCount())),
					AccelAeroCalculator.multiply(faresAndCharges[3], new BigDecimal(res.getTotalPaxInfantCount())),
					AccelAeroCalculator.multiply(faresAndCharges[5], new BigDecimal(res.getTotalPaxChildCount())));

			newTotCharges = AccelAeroCalculator.add(newTotFare, newTotTaxAndSurcharges);
			// TODO - set refundable/non-refundable totals
		}

		if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {

			BigDecimal[] faresAndCharges = getFaresAndCharges(fareChargesAndSegmentDTOs);

			// if(pnr)

			newTotFare = AccelAeroCalculator.add(
					(AccelAeroCalculator.multiply(faresAndCharges[0], new BigDecimal(res.getTotalPaxAdultCount()))),
					(AccelAeroCalculator.multiply(faresAndCharges[2], new BigDecimal(res.getTotalPaxInfantCount()))),
					(AccelAeroCalculator.multiply(faresAndCharges[4], new BigDecimal(res.getTotalPaxChildCount()))));
			newTotTaxAndSurcharges = AccelAeroCalculator.add(
					AccelAeroCalculator.multiply(faresAndCharges[1], new BigDecimal(res.getTotalPaxAdultCount())),
					AccelAeroCalculator.multiply(faresAndCharges[3], new BigDecimal(getInfantCount(fareChargesAndSegmentDTOs))),
					AccelAeroCalculator.multiply(faresAndCharges[5], new BigDecimal(res.getTotalPaxChildCount())));

			newTotCharges = AccelAeroCalculator.add(newTotFare, newTotTaxAndSurcharges);
			// TODO - set refundable/non-refundable totals
		}

		newTotCharges = AccelAeroCalculator.add(newTotFare, newTotTaxAndSurcharges);

		// Existing OND Fares & Charges
		segmentSummeryDTO.setCurrentFare(currentTotFare);
		segmentSummeryDTO.setCurrentCharge(currentTotTaxAndSurcharges);
		segmentSummeryDTO.setCurrentCnxCharge(currentTotCnx);
		segmentSummeryDTO.setCurrentModCharge(currentTotMod);
		segmentSummeryDTO.setCurrentAdjustments(currentTotAdj);
		segmentSummeryDTO.setCurrentTotalCharges(currentTotCharges);
		segmentSummeryDTO.setCurrentRefunds(currentTotRefundable);
		segmentSummeryDTO.setCurrentNonRefunds(currentTotNonRefundable);

		// New OND Fare & Charges
		segmentSummeryDTO.setNewFare(newTotFare);
		segmentSummeryDTO.setNewCharge(newTotTaxAndSurcharges);
		segmentSummeryDTO.setNewCnxCharge(newTotCnx);
		segmentSummeryDTO.setNewModCharge(newTotMod);
		segmentSummeryDTO.setNewAdjustments(newTotAdj);
		segmentSummeryDTO.setNewTotalCharges(newTotCharges);
		segmentSummeryDTO.setNewRefunds(newTotRefundable);
		segmentSummeryDTO.setNewNonRefunds(newTotNonRefundable);

		// Identified total CNX/MOD charges
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
			segmentSummeryDTO.setIdentifiedTotalCnxCharge(identifiedTotCnx);
		}
		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			segmentSummeryDTO.setIdentifiedTotalModCharge(identifiedTotMod);
		}

		segmentSummeryDTO.setIdentifiedExtraFeeAmount(identifiedTotExtraFees);

		return segmentSummeryDTO;
	}

	public static int getInfantCount(Collection<OndFareDTO> fareChargesAndSegmentDTOs) {
		int infantCount = 0;
		for (OndFareDTO ondFareDTO : fareChargesAndSegmentDTOs) {
			Iterator i = ondFareDTO.getSegmentSeatDistsDTO().iterator();
			while (i.hasNext()) {
				SegmentSeatDistsDTO distsDTO = (SegmentSeatDistsDTO) i.next();
				infantCount += distsDTO.getNoOfInfantSeats();
			}
		}
		return infantCount;
	}

	/**
	 * Returns Pax Balances for reservation alterations (Modifications/Cancellations).
	 * 
	 * @param res
	 * @param fareChargesAndSegmentDTOs
	 * @param type
	 * @return
	 */
	public static Collection<PAXSummeryDTO> getPaxBalancesForAlt(Reservation res,
			Collection<OndFareDTO> fareChargesAndSegmentDTOs, Map<Integer, PnrChargesDTO> pnrPaxChargesMap,
			String alterationType, Collection<Integer> parentPnrPaxIDs) {
		// Index the reservation passengers
		Map<Integer, ReservationPax> reservationPaxMap = prepareReservationPaxMap(res);
		Map<Integer, BigDecimal[]> newPaxTotals = getPaxTotals(reservationPaxMap, pnrPaxChargesMap, fareChargesAndSegmentDTOs,
				alterationType);
		Collection<PAXSummeryDTO> paxSummaries = new ArrayList<PAXSummeryDTO>();

		for (ReservationPax pax : reservationPaxMap.values()) {
			BigDecimal[] paxTotals = newPaxTotals.get(pax.getPnrPaxId());
			ReservationPax reservationPax = reservationPaxMap.get(pax.getPnrPaxId());

			PAXSummeryDTO paxSummeryDTO = new PAXSummeryDTO();
			paxSummeryDTO.setPNRPaxId(pax.getPnrPaxId());
			paxSummeryDTO.setPaxName((reservationPax.getTitle() != null ? reservationPax.getTitle() + " " : "")
					+ reservationPax.getFirstName() + " " + reservationPax.getLastName());

			paxSummeryDTO.setCurrentCharges(paxTotals[0]);
			paxSummeryDTO.setNewTotalCharges(paxTotals[1]);

			// FIXME: Add infant scenario should be taken care of
			// if (parentPnrPaxIDs != null && parentPnrPaxIDs.contains(pax.getPnrPaxId())) {
			// paxSummeryDTO.setNewTotalCharges(paxTotals[0]);
			// } else {
			// paxSummeryDTO.setNewTotalCharges(paxTotals[1]);
			// }
			paxSummeryDTO.setCurrentPayments(paxTotals[2]);
			paxSummeryDTO.setNewBalance(AccelAeroCalculator.subtract(paxSummeryDTO.getNewTotalCharges(),
					paxSummeryDTO.getCurrentPayments()));

			paxSummaries.add(paxSummeryDTO);
		}

		return paxSummaries;
	}

	/**
	 * Retuns Map of double [] {currentTotalCharges, newTotalCharges, currentTotalPayment} keyed by pnrPaxId
	 * 
	 * @param reservationPaxMap
	 * @param pnrPaxChargesMap
	 * @param fareChargesAndSegmentDTOs
	 * @param alterationType
	 * @return
	 */
	private static Map<Integer, BigDecimal[]>
			getPaxTotals(Map<Integer, ReservationPax> reservationPaxMap, Map<Integer, PnrChargesDTO> pnrPaxChargesMap,
					Collection<OndFareDTO> fareChargesAndSegmentDTOs, String alterationType) {
		Map<Integer, BigDecimal[]> newPaxTotalsMap = new HashMap<Integer, BigDecimal[]>();

		BigDecimal[] quotedFaresAndCharges = null;
		if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			quotedFaresAndCharges = getFaresAndCharges(fareChargesAndSegmentDTOs);
		}

		BigDecimal newPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentPaxTotalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		PnrChargesDTO pnrChargesDTO = null;

		BigDecimal newInfPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentInfPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentInfPaxTotalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		PnrChargesDTO infPNRChargesDTO = null;
		ReservationPax infReservationPax = null;

		for (ReservationPax reservationPax : reservationPaxMap.values()) {
			currentPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			newPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			currentPaxTotalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();

			newInfPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			currentInfPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			currentInfPaxTotalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
			infPNRChargesDTO = null;
			infReservationPax = null;

			if (pnrPaxChargesMap != null) {
				pnrChargesDTO = pnrPaxChargesMap.get(reservationPax.getPnrPaxId());
			}

			if (ReservationApiUtils.isAdultType(reservationPax) || ReservationApiUtils.isChildType(reservationPax)) {
				if (reservationPax.getAccompaniedPaxId() != null) {
					infReservationPax = (ReservationPax) reservationPax.getInfants().iterator().next();
				}

				Set<ReservationPaxFare> reservationPaxFares = reservationPax.getPnrPaxFares();

				// Total current prices
				for (ReservationPaxFare reservationPaxFare : reservationPaxFares) {
					currentPaxTotalPrice = AccelAeroCalculator.add(currentPaxTotalPrice,
							reservationPaxFare.getTotalChargeAmount());
					newPaxTotalPrice = AccelAeroCalculator.add(newPaxTotalPrice, reservationPaxFare.getTotalChargeAmount());
				}

				// Total current infant prices
				if (infReservationPax != null) {
					Set<ReservationPaxFare> infReservationPaxFares = infReservationPax.getPnrPaxFares();
					for (ReservationPaxFare reservationPaxFare : infReservationPaxFares) {
						currentInfPaxTotalPrice = AccelAeroCalculator.add(currentInfPaxTotalPrice,
								reservationPaxFare.getTotalChargeAmount());
						newInfPaxTotalPrice = AccelAeroCalculator.add(newInfPaxTotalPrice,
								reservationPaxFare.getTotalChargeAmount());
					}
				}

				// For handling adult and infant charges and payment seperately
				// AARESAA-11046
				// currentPaxTotalPrice = AccelAeroCalculator.subtract(currentPaxTotalPrice, currentInfPaxTotalPrice);
				// newPaxTotalPrice = AccelAeroCalculator.subtract(newPaxTotalPrice, newInfPaxTotalPrice);

				// Deduct cancelled segments' total refundable amount
				if (!ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
						&& !ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
					newPaxTotalPrice = AccelAeroCalculator.subtract(newPaxTotalPrice, pnrChargesDTO.getTotalRefundableAmount());
					if (infReservationPax != null) {
						infPNRChargesDTO = pnrPaxChargesMap.get(infReservationPax.getPnrPaxId());
						newInfPaxTotalPrice = AccelAeroCalculator.subtract(newInfPaxTotalPrice,
								infPNRChargesDTO.getTotalRefundableAmount());

						// For handling adult and infant charges and payment seperately
						newPaxTotalPrice = AccelAeroCalculator.add(newPaxTotalPrice, infPNRChargesDTO.getTotalRefundableAmount());
					}
				}

				// Add cancellation or modification charges
				if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
					newPaxTotalPrice = AccelAeroCalculator.add(newPaxTotalPrice, pnrChargesDTO.getIdentifiedCancellationAmount());
				} else if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
					newPaxTotalPrice = AccelAeroCalculator.add(newPaxTotalPrice, pnrChargesDTO.getIdentifiedModificationAmount());
				}

				// Add identified extra fees total amount
				newPaxTotalPrice = AccelAeroCalculator.add(newPaxTotalPrice, pnrChargesDTO.getIdentifiedExtraFeeAmount());

				// Add new quoted segments' prices
				if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
						|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {

					if (ReservationApiUtils.isAdultType(reservationPax)) {
						newPaxTotalPrice = AccelAeroCalculator.add(newPaxTotalPrice, quotedFaresAndCharges[0],
								quotedFaresAndCharges[1]);
					} else if (ReservationApiUtils.isChildType(reservationPax)) {
						newPaxTotalPrice = AccelAeroCalculator.add(newPaxTotalPrice, quotedFaresAndCharges[4],
								quotedFaresAndCharges[5]);
					}

					if (infReservationPax != null) {
						newInfPaxTotalPrice = AccelAeroCalculator.add(newInfPaxTotalPrice, quotedFaresAndCharges[2],
								quotedFaresAndCharges[3]);
					}
				}

				if (ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
					quotedFaresAndCharges = getFaresAndCharges(fareChargesAndSegmentDTOs);

					if (ReservationApiUtils.isAdultType(reservationPax)) {
						newPaxTotalPrice = AccelAeroCalculator.add(quotedFaresAndCharges[0], quotedFaresAndCharges[1],
								quotedFaresAndCharges[3]);
					} else if (ReservationApiUtils.isChildType(reservationPax)) {
						newPaxTotalPrice = AccelAeroCalculator.add(newPaxTotalPrice, quotedFaresAndCharges[4],
								quotedFaresAndCharges[5]);
					}

					if (infReservationPax != null) {
						newInfPaxTotalPrice = AccelAeroCalculator.add(newInfPaxTotalPrice, quotedFaresAndCharges[2],
								quotedFaresAndCharges[3]);
					}

				}

				BigDecimal totalPaid = null;
				if (pnrChargesDTO != null) {
					// Capture current payments/refunds; Handling infant payment seperately
					totalPaid = pnrChargesDTO.isPaymentMade()
							? calculateTotalPayment(pnrChargesDTO.getPaymentInfo())
							: AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				if (totalPaid != null && totalPaid.doubleValue() > currentInfPaxTotalPrice.doubleValue()) {
					currentInfPaxTotalPayment = currentInfPaxTotalPrice;
					currentPaxTotalPayment = AccelAeroCalculator.subtract(totalPaid, currentInfPaxTotalPayment);
				} else if (totalPaid != null && totalPaid.doubleValue() > 0) {
					currentInfPaxTotalPayment = currentInfPaxTotalPrice;
					currentPaxTotalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
				} else {
					currentInfPaxTotalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
					currentPaxTotalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				newPaxTotalsMap.put(reservationPax.getPnrPaxId(), new BigDecimal[] { currentPaxTotalPrice, newPaxTotalPrice,
						currentPaxTotalPayment });
				if (infReservationPax != null) {
					newPaxTotalsMap.put(infReservationPax.getPnrPaxId(), new BigDecimal[] { currentInfPaxTotalPrice,
							newInfPaxTotalPrice, currentInfPaxTotalPayment });
				}
			}
		}
		return newPaxTotalsMap;
	}

	/**
	 * Sums the total payment.
	 * 
	 * @param paymentInfo
	 * @return
	 */
	public static BigDecimal calculateTotalPayment(Collection<ReservationTnx> paymentInfo) {
		BigDecimal totalPaid = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationTnx reservationTnx : paymentInfo) {
			totalPaid = AccelAeroCalculator.add(totalPaid, reservationTnx.getAmount());
		}
		return totalPaid.negate();
	}

	/**
	 * Index reservation passengers with PnrPaxId.
	 * 
	 * @param res
	 * @return
	 */
	public static Map<Integer, ReservationPax> prepareReservationPaxMap(Reservation res) {
		Map<Integer, ReservationPax> reservationPaxMap = new HashMap<Integer, ReservationPax>();
		for (Iterator paxIt = res.getPassengers().iterator(); paxIt.hasNext();) {
			ReservationPax pax = (ReservationPax) paxIt.next();
			reservationPaxMap.put(pax.getPnrPaxId(), pax);
		}
		return reservationPaxMap;
	}

	/**
	 * Prepares new segments for add/mod segments.
	 * 
	 * @param existingRes
	 * @param newFareChargesAndSegDTOs
	 * @return
	 * @throws ModuleException
	 */
	public static ISegment prepareNewResSegments(Reservation existingRes, Collection<OndFareDTO> newFareChargesAndSegDTOs,
			Map<Integer, String> groundStationBySegmentMap, Map<Integer, Integer> groundSementFlightByAirFlightMap,
			Map<Integer, String> flightSegIdWiseBaggageOndGrpId) throws ModuleException {
		ISegment segmentAssembler = new SegmentAssembler(newFareChargesAndSegDTOs);

		int[] nextSegAndOndSequences = getNextSegAndOndSequences(existingRes);

		int segmentSeq = nextSegAndOndSequences[0];
		int intOndGrp = nextSegAndOndSequences[1];
		int intReturnOndGrp = nextSegAndOndSequences[2];
		Integer baggageOndGroupId = null;

		List<Collection<ReservationSegmentTO>> ondFlightSegments = ReservationAssemblerConvertUtil.getOndWiseFlights(
				newFareChargesAndSegDTOs, null);
		boolean isReturnBooking = OndFareUtil.isReturnBooking(newFareChargesAndSegDTOs);

		Date openRTConfirmExpiry = OndFareUtil.getOpenReturnPeriods(newFareChargesAndSegDTOs).getConfirmBefore();
		boolean openRTSegExists = false;

		// FIXME a small hack until the full fix is done for OPENRT
		if (ondFlightSegments.size() == 2 && openRTConfirmExpiry != null) {
			openRTSegExists = true;
		}

		int ondSequence = 0;
		for (Collection<ReservationSegmentTO> perOndFlights : ondFlightSegments) {
			int lastFareTypeId = -1;
			for (ReservationSegmentTO segmentInfo : perOndFlights) {
				baggageOndGroupId = null;
				Date openRTConfirmBefore = null;
				boolean isReturnOND = false;

				if (openRTSegExists && ondSequence == 1) {
					openRTConfirmBefore = openRTConfirmExpiry;
				}

				if (isReturnBooking && ondSequence == 1) {
					isReturnOND = true;
				}
				int fareTypeId = segmentInfo.getOndFareType();
				if (lastFareTypeId != -1 && lastFareTypeId != FareTypes.PER_FLIGHT_FARE && fareTypeId != lastFareTypeId) {
					intOndGrp++;
					intReturnOndGrp++;
				}

				if (flightSegIdWiseBaggageOndGrpId != null && !flightSegIdWiseBaggageOndGrpId.isEmpty()
						&& flightSegIdWiseBaggageOndGrpId.containsKey(segmentInfo.getFlightSegId())) {
					String tempBaggageOndGroupId = flightSegIdWiseBaggageOndGrpId.get(segmentInfo.getFlightSegId());
					if (tempBaggageOndGroupId != null && !tempBaggageOndGroupId.isEmpty()) {
						baggageOndGroupId = Integer.valueOf(tempBaggageOndGroupId);
					}
				}

				segmentAssembler.addOndSegment((ondSequence + 1), segmentSeq++, segmentInfo.getFlightSegId(), intOndGrp,
						intReturnOndGrp, openRTConfirmBefore, null, baggageOndGroupId, false, isReturnOND,
						segmentInfo.getSelectedBundledFarePeriodId(), null, null, segmentInfo.getCsOcCarrierCode(),
						segmentInfo.getCsOcFlightNumber());
				segmentAssembler.addGroundStationDataToPNRSegment(groundStationBySegmentMap, groundSementFlightByAirFlightMap);
				if (fareTypeId == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE || fareTypeId == FareTypes.PER_FLIGHT_FARE) {
					intOndGrp++;
					intReturnOndGrp++;
				}
				lastFareTypeId = fareTypeId;
			}
			if (lastFareTypeId != FareTypes.PER_FLIGHT_FARE) {
				intOndGrp++;
			}
			if (lastFareTypeId == FareTypes.OND_FARE) {
				intReturnOndGrp++;
			}
			ondSequence++;

		}

		// Object[] inOutboundSegments = ReservationUtil.extractNewResSegmentsInfo(newFareChargesAndSegDTOs,
		// nextSegAndOndSequences[0], nextSegAndOndSequences[1]);
		//
		// Collection outBoundSegments = (Collection) inOutboundSegments[0];
		// Collection inBoundSegments = (Collection) inOutboundSegments[1];
		//
		// for (Iterator obSegIt = outBoundSegments.iterator(); obSegIt.hasNext();) {
		// ReservationSegmentTO segmentInfo = (ReservationSegmentTO) obSegIt.next();
		// segmentAssembler.addOutgoingSegment((OndSequence.OUT_BOUND + 1), segmentInfo.getSegmentSeq(),
		// segmentInfo.getFlightSegId(), segmentInfo.getOndGroupId(), null);
		// segmentAssembler.addGroundStationDataToPNRSegment(groundStationBySegmentMap,
		// groundSementFlightByAirFlightMap);
		// }
		//
		// if (inBoundSegments != null) {
		// for (Iterator ibSegIt = inBoundSegments.iterator(); ibSegIt.hasNext();) {
		// ReservationSegmentTO segmentInfo = (ReservationSegmentTO) ibSegIt.next();
		// segmentAssembler.addReturnSegment((OndSequence.IN_BOUND + 1), segmentInfo.getSegmentSeq(),
		// segmentInfo.getFlightSegId(), segmentInfo.getOndGroupId(), null, null);
		// }
		// }
		return segmentAssembler;
	}

	private static Integer getBaggageOndGrpId(Map<Integer, String> flightSegIdWiseBaggageOndGrpId, int flightSegId) {
		if (flightSegIdWiseBaggageOndGrpId != null && flightSegIdWiseBaggageOndGrpId.size() > 0) {
			String baggageOndGrpId = BeanUtils.nullHandler(flightSegIdWiseBaggageOndGrpId.get(flightSegId));
			if (baggageOndGrpId.length() > 0) {
				return new Integer(baggageOndGrpId);
			}
		}
		return null;
	}

	/**
	 * Returns next seg and ond sequences for add/mod segments.
	 * 
	 * @param existingRes
	 * @return
	 */
	private static int[] getNextSegAndOndSequences(Reservation existingRes) {
		int nextSegSeq = 0;
		int nextOndSeq = 0;
		int nextReturnOndSeq = 0;

		for (Iterator<ReservationSegment> resSegsIt = existingRes.getSegments().iterator(); resSegsIt.hasNext();) {
			ReservationSegment resSeg = resSegsIt.next();
			if (resSeg.getSegmentSeq().intValue() > nextSegSeq) {
				nextSegSeq = resSeg.getSegmentSeq().intValue();
			}
			if (resSeg.getOndGroupId().intValue() > nextOndSeq) {
				nextOndSeq = resSeg.getOndGroupId().intValue();
			}
			if (resSeg.getReturnOndGroupId() != null && resSeg.getReturnOndGroupId().intValue() > nextReturnOndSeq) {
				nextReturnOndSeq = resSeg.getReturnOndGroupId().intValue();
			}
		}
		return new int[] { ++nextSegSeq, ++nextOndSeq, ++nextReturnOndSeq };
	}

	/**
	 * Extracts segment information from segsSestsAndFaresCol to be used in save booking.
	 * 
	 * @param segsSeatsAndFaresCol
	 * @return Object [] - outbound flight segments, inbound flight segments TODO: all the segment seq and grouping
	 *         assembling should be removed form this method
	 */
	public static Object[] extractNewResSegmentsInfo(Collection<OndFareDTO> segsSeatsAndFaresCol, Integer nextSegSeq,
			Integer nextOndGroup) {
		int fareGroup = nextOndGroup.intValue();

		Collection<ReservationSegmentTO> obFlightSegments = new LinkedList<ReservationSegmentTO>();
		Collection<ReservationSegmentTO> ibFlightSegments = null;
		for (OndFareDTO ondFareDTO : segsSeatsAndFaresCol) {
			// need to sort the new segments
			for (Iterator<FlightSegmentDTO> fltSegDTOIt = ondFareDTO.getSegmentsMap().values().iterator(); fltSegDTOIt.hasNext();) {
				FlightSegmentDTO flightSegmentDTO = fltSegDTOIt.next();

				if (!ondFareDTO.isInBoundOnd()) {
					obFlightSegments.add(new ReservationSegmentTO(flightSegmentDTO.getSegmentId(), fareGroup, flightSegmentDTO
							.getDepartureDateTimeZulu(), ondFareDTO.getSelectedBundledFarePeriodId(), null, null, flightSegmentDTO
							.getCsOcCarrierCode(), flightSegmentDTO.getCsOcFlightNumber()));
				} else {
					if (ibFlightSegments == null) {
						ibFlightSegments = new LinkedList<ReservationSegmentTO>();
					}
					ibFlightSegments.add(new ReservationSegmentTO(flightSegmentDTO.getSegmentId(), fareGroup, flightSegmentDTO
							.getDepartureDateTimeZulu(), ondFareDTO.getSelectedBundledFarePeriodId(), null, null, flightSegmentDTO
							.getCsOcCarrierCode(), flightSegmentDTO.getCsOcFlightNumber()));
				}
			}
			++fareGroup;
		}
		// set segment sequence
		SegmentUtil.setSegmentSequnceNumbers(segsSeatsAndFaresCol, obFlightSegments, ibFlightSegments, nextSegSeq.intValue());

		return new Object[] { obFlightSegments, ibFlightSegments };
	}

	/**
	 * Calculate total prices from quoted fares and charges.
	 * 
	 * @return double [] {totalTicketPrice, perAdultPaxTotalPrice, perInfantPaxTotalPrice}
	 * @throws ModuleException
	 */
	public static BigDecimal[] getQuotedTotalPrices(Collection<OndFareDTO> quotedCharges, int adultsCount, int childcount,
			int infantsCount) throws ModuleException {
		BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perAdultPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perInfantPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perChildPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		double[] perOndPrice;

		for (OndFareDTO quotedOndFareDTO : quotedCharges) {
			perOndPrice = quotedOndFareDTO.getTotalCharges();

			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getAdultFare())) {
				perAdultPaxTotalPrice = AccelAeroCalculator.add(perAdultPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(perOndPrice[0]),
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getAdultFare()));
			}

			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getInfantFare())) {
				perInfantPaxTotalPrice = AccelAeroCalculator.add(perInfantPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(perOndPrice[1]),
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getInfantFare()));
			}

			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getChildFare())) {
				perChildPaxTotalPrice = AccelAeroCalculator.add(perChildPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(perOndPrice[2]),
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getChildFare()));
			}

		}
		totalTicketPrice = AccelAeroCalculator.add(
				(AccelAeroCalculator.multiply(perAdultPaxTotalPrice, new BigDecimal(adultsCount))),
				(AccelAeroCalculator.multiply(perChildPaxTotalPrice, new BigDecimal(childcount))),
				(AccelAeroCalculator.multiply(perInfantPaxTotalPrice, new BigDecimal(infantsCount))));

		return new BigDecimal[] { totalTicketPrice, perAdultPaxTotalPrice, perInfantPaxTotalPrice, perChildPaxTotalPrice };
	}

	public static BigDecimal getRequestedTotalPrices(PriceInfo priceInfo, int adultsCount, int childcount, int infantsCount) {

		PTCFareBreakdownType p = new PTCFareBreakdownType();
		p.getTravelerRefNumber();
		p.getPassengerFare().getBaseFare();
		p.getPassengerFare().getTaxes();
		p.getPassengerFare().getFees();

		return null;
	}

	/**
	 * For reconciling external payment transactions manually via XBE.
	 * 
	 * Retrieves external bank transaction statuses, reconcile them with AccelAero transaction records and updates
	 * reservation payments if needed.
	 * 
	 * @param pnrExtTransactionsTO
	 * @return
	 * @throws ModuleException
	 */
	public static PNRExtTransactionsTO syncPNRExtPayTransactions(String pnr) throws ModuleException {

		// FIX done to send the PNR transaction status to the respective service providers
		Set serviceProviders = null;
		if (wsClientConfig.getServiceAgentsMap() != null) {
			serviceProviders = wsClientConfig.getServiceAgentsMap().keySet();
		}
		boolean tnxReconciled = false;
		if (serviceProviders != null) {
			for (Iterator iterServiceProviders = serviceProviders.iterator(); iterServiceProviders.hasNext();) {
				String serviceProvider = (String) iterServiceProviders.next();

				try {
					// Exclude already reconciled transactions
					PNRExtTransactionsTO pnrExtTnxAlreadyReconExcluded = getTxnsForManualRecon(pnr, (String) wsClientConfig
							.getServiceAgentsMap().get(serviceProvider));

					if (log.isDebugEnabled())
						log.debug("Before Calling Status Check :: "
								+ (pnrExtTnxAlreadyReconExcluded != null ? pnrExtTnxAlreadyReconExcluded.getSummary() : "null"));

					if (pnrExtTnxAlreadyReconExcluded != null) {
						// Query bank transactions status
						WSClientBD ebiWebervices = WPModuleUtils.getWSClientBD();
						// PNRExtTransactionsTO pnrExtTnxWithBankStatus =
						// ebiWebervices.getEBITransactionStatus(pnrExtTnxAlreadyReconExcluded);
						PNRExtTransactionsTO pnrExtTnxWithBankStatus = ebiWebervices.getPNRTransactionStatus(
								pnrExtTnxAlreadyReconExcluded, serviceProvider);

						if (log.isDebugEnabled())
							log.debug("After Calling Status Check :: "
									+ (pnrExtTnxWithBankStatus != null ? pnrExtTnxWithBankStatus.getSummary() : "null"));

						// Reconcile AA transactions status with bank transaction status
						Collection pnrExtTnxCol = new ArrayList();
						pnrExtTnxCol.add(pnrExtTnxWithBankStatus);
						WPModuleUtils.getReservationBD().reconcileExtPayTransactions(pnrExtTnxCol, true, null, null,
								(String) wsClientConfig.getServiceAgentsMap().get(serviceProvider)); // FIXME
						tnxReconciled = true;
					}
				} catch (Exception e) {
					log.error(" syncPNRExtPayTransactions failed for serviceProvider = " + serviceProvider, e);
				}
			}
		}

		PNRExtTransactionsTO pnrExtTnxAfterRecon = null;
		if (tnxReconciled) { // FIXME - if transactions for any one service provider is successfully reconciled, sync
								// pnr will return "sync success" status, even if other transactions failed to recon.
								// User has to manually find this out while sync process.
			pnrExtTnxAfterRecon = WPModuleUtils.getResQueryBD().getPNRExtPayTransactions(pnr);
		}

		if (log.isDebugEnabled())
			log.debug("After Calling Recon :: " + ((pnrExtTnxAfterRecon != null) ? pnrExtTnxAfterRecon.getSummary() : "null"));

		return pnrExtTnxAfterRecon;
	}

	/**
	 * Prepares {@link PNRExtTransactionsTO} excluding already reconciled transactions.
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	private static PNRExtTransactionsTO getTxnsForManualRecon(String pnr, String serviceAgent) throws ModuleException {
		PNRExtTransactionsTO unReconciledTnxs = null;
		ExtPayTxCriteriaDTO unsyncedTnxCriteria = new ExtPayTxCriteriaDTO();
		unsyncedTnxCriteria.setPnr(pnr);
		unsyncedTnxCriteria.addReconStatusToInclude(ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED);
		unsyncedTnxCriteria.addReconStatusToInclude(ReservationInternalConstants.ExtPayTxReconStatus.RECON_FAILED);
		unsyncedTnxCriteria.setIncludeAbortedManualReconTxns(true);
		unsyncedTnxCriteria.setAgentCode(serviceAgent);

		Map unReconciledTnxMap = WPModuleUtils.getResQueryBD().getExtPayTransactions(unsyncedTnxCriteria);

		if (unReconciledTnxMap != null && unReconciledTnxMap.containsKey(pnr)) {
			unReconciledTnxs = (PNRExtTransactionsTO) unReconciledTnxMap.get(pnr);
		}
		return unReconciledTnxs;
	}

	/**
	 * Calculates total outstanding balance to pay.
	 * 
	 * @param res
	 * @return
	 */
	public static BigDecimal getTotalBalToPay(Reservation res) {
		BigDecimal balToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ReservationPax> reservationPaxes = res.getPassengers();
		for (ReservationPax traveler : reservationPaxes) {
			if (traveler.getTotalAvailableBalance().doubleValue() > 0) {
				balToPay = AccelAeroCalculator.add(balToPay, traveler.getTotalAvailableBalance());
			}
		}
		return balToPay;
	}

	/**
	 * Calculates total outstanding balance to pay.
	 * 
	 * @param lccClientReservation
	 * @return
	 */
	public static BigDecimal getTotalBalToPay(LCCClientReservation lccClientReservation) {
		BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<LCCClientReservationPax> reservationPaxes = lccClientReservation.getPassengers();

		for (LCCClientReservationPax traveler : reservationPaxes) {
			if (traveler.getTotalAvailableBalance().doubleValue() > 0) {
				balanceToPay = AccelAeroCalculator.add(balanceToPay, traveler.getTotalAvailableBalance());
			}
		}
		return balanceToPay;
	}

	/**
	 * Returns double[] {adultFare, adultCharges, infantFare, infantCharge}
	 * 
	 * @param fareChargesAndSegmentDTOs
	 * @return
	 */
	public static BigDecimal[] getFaresAndCharges(Collection<OndFareDTO> fareChargesAndSegmentDTOs) {
		double[] tmpCharges;
		BigDecimal newAdultFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newInfantFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newChildFare = AccelAeroCalculator.getDefaultBigDecimalZero();

		BigDecimal newAdultCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newInfantChages = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newChildChages = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (Iterator<OndFareDTO> fareChargesAndSegmentDTOsIt = fareChargesAndSegmentDTOs.iterator(); fareChargesAndSegmentDTOsIt
				.hasNext();) {
			OndFareDTO fareChargesAndSegmentDTO = fareChargesAndSegmentDTOsIt.next();

			if (ReservationApiUtils.isFareExist(fareChargesAndSegmentDTO.getAdultFare())) {
				newAdultFare = AccelAeroCalculator.add(newAdultFare,
						AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO.getAdultFare()));
			}

			if (ReservationApiUtils.isFareExist(fareChargesAndSegmentDTO.getInfantFare())) {
				newInfantFare = AccelAeroCalculator.add(newInfantFare,
						AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO.getInfantFare()));
			}

			if (ReservationApiUtils.isFareExist(fareChargesAndSegmentDTO.getChildFare())) {
				newChildFare = AccelAeroCalculator.add(newChildFare,
						AccelAeroCalculator.parseBigDecimal(fareChargesAndSegmentDTO.getChildFare()));
			}

			tmpCharges = fareChargesAndSegmentDTO.getTotalCharges();
			newAdultCharges = AccelAeroCalculator.add(newAdultCharges, AccelAeroCalculator.parseBigDecimal(tmpCharges[0]));
			newInfantChages = AccelAeroCalculator.add(newInfantChages, AccelAeroCalculator.parseBigDecimal(tmpCharges[1]));
			newChildChages = AccelAeroCalculator.add(newChildChages, AccelAeroCalculator.parseBigDecimal(tmpCharges[2]));
		}

		return new BigDecimal[] { newAdultFare, newAdultCharges, newInfantFare, newInfantChages, newChildFare, newChildChages };
	}

	/**
	 * Returns the Passenger Map with PnrPaxId and ReservationPax Object
	 * 
	 * @param setPax
	 * @return
	 */
	public static Map<Integer, ReservationPax> getPassengerMap(Set setPax) {
		Map<Integer, ReservationPax> mapPax = new HashMap<Integer, ReservationPax>();

		for (Iterator iter = setPax.iterator(); iter.hasNext();) {
			ReservationPax pax = (ReservationPax) iter.next();

			mapPax.put(pax.getPnrPaxId(), pax);
		}

		return mapPax;
	}

	/**
	 * Returns the reservation segment map
	 * 
	 * @param colReservationSegment
	 * @return
	 */
	public static Map getReservationSegmentMap(Collection colReservationSegment) {
		Map<Integer, ReservationSegment> mapReservationSegment = new HashMap<Integer, ReservationSegment>();

		for (Iterator iter = colReservationSegment.iterator(); iter.hasNext();) {
			ReservationSegment reservationSegment = (ReservationSegment) iter.next();

			mapReservationSegment.put(reservationSegment.getPnrSegId(), reservationSegment);
		}

		return mapReservationSegment;
	}

	/**
	 * Calculates the payment amount based on the refundable amounts, modification charges and available credits
	 * 
	 * @param pnrChargesDTO
	 * @param decimal
	 * @return
	 */
	private static BigDecimal getCalculatedPaymentAmount(PnrChargesDTO pnrChargesDTO, BigDecimal paxAmount,
			BigDecimal seatCharge, BigDecimal mealCharge, BigDecimal aptCharge, BigDecimal flexiCharge) {
		BigDecimal totalPayAmount = AccelAeroCalculator.add(seatCharge, mealCharge, aptCharge, flexiCharge, paxAmount,
				pnrChargesDTO.getIdentifiedModificationAmount());
		BigDecimal totalCreditAmount = AccelAeroCalculator.add(pnrChargesDTO.getTotalRefundableAmount(), pnrChargesDTO
				.getAvailableBalance().negate());

		BigDecimal payDueAmount = AccelAeroCalculator.subtract(totalPayAmount, totalCreditAmount);

		if (payDueAmount.doubleValue() < 0) {
			return AccelAeroCalculator.getDefaultBigDecimalZero();
		} else {
			return payDueAmount;
		}
	}

	/**
	 * Returns the amount to pay for a reservation modification Returns a negative value if credit can be utilized to
	 * pay the amount
	 * 
	 * @param collFares
	 * @param collCharges
	 * @return
	 */
	public static BigDecimal getBalanceAmount(Collection collFares, Collection collCharges, Map<Integer, BigDecimal> seatChrges,
			Map<Integer, BigDecimal> mealChrges, Map<Integer, BigDecimal> airPortCharges,
			Map<Integer, BigDecimal> paxWiseFlexiChgMap) {
		BigDecimal[] freshPayAmounts = getNewPaxTypeTotals(collFares);

		Iterator itCollCharges = collCharges.iterator();
		PnrChargesDTO pnrChargesDTO;
		Map<Integer, BigDecimal> paymentAmount = new HashMap<Integer, BigDecimal>();

		while (itCollCharges.hasNext()) {
			pnrChargesDTO = (PnrChargesDTO) itCollCharges.next();

			BigDecimal seatCharge = seatChrges.get(pnrChargesDTO.getPnrPaxId());
			if (seatCharge == null)
				seatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			BigDecimal mealCharge = mealChrges.get(pnrChargesDTO.getPnrPaxId());
			if (mealCharge == null)
				mealCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			BigDecimal aptCharge = airPortCharges.get(pnrChargesDTO.getPnrPaxId());
			if (aptCharge == null)
				aptCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			BigDecimal flexiCharge = paxWiseFlexiChgMap.get(pnrChargesDTO.getPnrPaxId());
			if (flexiCharge == null)
				flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (pnrChargesDTO.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)) {

				if (pnrChargesDTO.isParent() != null && pnrChargesDTO.isParent()) {
					BigDecimal combinedFreshAmount = AccelAeroCalculator.add(freshPayAmounts[0], freshPayAmounts[2]);
					paymentAmount.put(
							pnrChargesDTO.getPnrPaxId(),
							getCalculatedPaymentAmount(pnrChargesDTO, combinedFreshAmount, seatCharge, mealCharge, aptCharge,
									flexiCharge));
				} else {
					paymentAmount.put(
							pnrChargesDTO.getPnrPaxId(),
							getCalculatedPaymentAmount(pnrChargesDTO, freshPayAmounts[0], seatCharge, mealCharge, aptCharge,
									flexiCharge));
				}
			} else if (pnrChargesDTO.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {
				paymentAmount.put(
						pnrChargesDTO.getPnrPaxId(),
						getCalculatedPaymentAmount(pnrChargesDTO, freshPayAmounts[1], seatCharge, mealCharge, aptCharge,
								flexiCharge));
			} /*
			 * else if(pnrChargesDTO.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)){
			 * paymentAmount.put(pnrChargesDTO.getPnrPaxId(), getCalculatedPaymentAmount(pnrChargesDTO,
			 * freshPayAmounts[2], seatCharge)); }
			 */
		}

		BigDecimal totalPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (BigDecimal amount : paymentAmount.values()) {
			totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);
		}

		if (log.isDebugEnabled()) {
			log.debug("::: Pay Amount = " + totalPayAmount.toString());
		}

		return totalPayAmount;
	}

	/**
	 * Returns new fare quote request passenger type wise payment amounts
	 * 
	 * @param collFares
	 * @return
	 */
	public static BigDecimal[] getNewPaxTypeTotals(Collection collFares) {
		Iterator iterColFares = collFares.iterator();
		OndFareDTO ondFareDTO = new OndFareDTO();

		BigDecimal dblAFareSegment = AccelAeroCalculator.getDefaultBigDecimalZero(); // Adult Fare
		BigDecimal dblCFareSegment = AccelAeroCalculator.getDefaultBigDecimalZero(); // Child Fare
		BigDecimal dblIFareSegment = AccelAeroCalculator.getDefaultBigDecimalZero(); // Infant Fare

		BigDecimal dblATaxSegment = AccelAeroCalculator.getDefaultBigDecimalZero(); // Total Adult Charge
		BigDecimal dblITaxSegment = AccelAeroCalculator.getDefaultBigDecimalZero(); // Total Infant Charge
		BigDecimal dblCTaxSegment = AccelAeroCalculator.getDefaultBigDecimalZero(); // Total Child Charge

		BigDecimal dblASurChg = AccelAeroCalculator.getDefaultBigDecimalZero(); // Total Adult Sur Charges
		BigDecimal dblISurChg = AccelAeroCalculator.getDefaultBigDecimalZero(); // Total Infant Sur Charges
		BigDecimal dblCSurChg = AccelAeroCalculator.getDefaultBigDecimalZero(); // Total Child Sur Charges

		BigDecimal[] dblArrTax;
		BigDecimal[] dblSurcharges;
		while (iterColFares.hasNext()) {
			ondFareDTO = (OndFareDTO) iterColFares.next();
			// fare
			dblAFareSegment = AccelAeroCalculator.add(dblAFareSegment,
					AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare()));
			dblCFareSegment = AccelAeroCalculator.add(dblCFareSegment,
					AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare()));
			dblIFareSegment = AccelAeroCalculator.add(dblIFareSegment,
					AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare()));

			// tax adult / infant / child
			dblArrTax = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges(ChargeGroups.TAX));
			dblATaxSegment = AccelAeroCalculator.add(dblATaxSegment, dblArrTax[0]);
			dblITaxSegment = AccelAeroCalculator.add(dblITaxSegment, dblArrTax[1]);
			dblCTaxSegment = AccelAeroCalculator.add(dblCTaxSegment, dblArrTax[2]);

			// sur
			dblSurcharges = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges(ChargeGroups.SURCHARGE));
			dblASurChg = AccelAeroCalculator.add(dblASurChg, dblSurcharges[0]);
			dblISurChg = AccelAeroCalculator.add(
					dblISurChg,
					AccelAeroCalculator.add(dblSurcharges[1],
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges(ChargeGroups.INFANT_SURCHARGE)[1])));
			dblCSurChg = AccelAeroCalculator.add(dblCSurChg, dblSurcharges[2]);
		}

		return new BigDecimal[] { AccelAeroCalculator.add(dblAFareSegment, dblATaxSegment, dblASurChg),
				AccelAeroCalculator.add(dblCFareSegment, dblCTaxSegment, dblCSurChg),
				AccelAeroCalculator.add(dblIFareSegment, dblITaxSegment, dblISurChg) };
	}

	/**
	 * 
	 * Calculates the fare and taxes surcharges to be paid for reservation
	 * 
	 * @param collFares
	 * @param collCharges
	 * @return
	 */
	public static BigDecimal getFreshPayAmount(Collection collFares, int adultCount, int childCount, int infantCount) {
		BigDecimal[] freshPayments = getNewPaxTypeTotals(collFares);

		BigDecimal adultTot = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal childTot = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal infantTot = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (adultCount > 0) {
			adultTot = AccelAeroCalculator.multiply(freshPayments[0], adultCount);
		}

		if (childCount > 0) {
			childTot = AccelAeroCalculator.multiply(freshPayments[1], childCount);
		}

		if (infantCount > 0) {
			infantTot = AccelAeroCalculator.multiply(freshPayments[2], infantCount);
		}

		BigDecimal grandTotal = AccelAeroCalculator.add(adultTot, childTot, infantTot);

		if (log.isDebugEnabled()) {
			log.debug("::: Pay Amount = " + grandTotal.toString());
		}

		return grandTotal;
	}

	/**
	 * Returns credit card transaction fee.
	 * 
	 * @param totalPrice
	 * @return
	 * @throws ModuleException
	 */
	public static ExternalChgDTO getCCCharge(BigDecimal totalPrice) throws ModuleException {
		Collection colEXTERNAL_CHARGES = new ArrayList();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);

		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		Map externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
				ChargeRateOperationType.MAKE_ONLY);

		ExternalChgDTO externalChgDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.CREDIT_CARD);
		if (externalChgDTO == null) {
			String msg = "Error loading credit card charge info[null]";
			throw new RuntimeException(msg);
		} else {
			externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
			externalChgDTO.calculateAmount(totalPrice);
		}

		return externalChgDTO;
	}

	/**
	 * Returns credit card transaction fee.
	 * 
	 * @param totalPrice
	 * @return
	 * @throws ModuleException
	 */
	public static ExternalChgDTO getCCCharge(BigDecimal totalPrice, int payablePaxCount, int noOfSegments, int chargeRateType)
			throws ModuleException {
		Collection colEXTERNAL_CHARGES = new ArrayList();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);

		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		Map externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, chargeRateType);

		ExternalChgDTO externalChgDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.CREDIT_CARD);
		if (externalChgDTO == null) {
			String msg = "Error loading credit card charge info[null]";
			throw new RuntimeException(msg);
		} else {
			externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
			if (externalChgDTO.isRatioValueInPercentage()) {
				externalChgDTO.calculateAmount(totalPrice);
			} else {
				externalChgDTO.calculateAmount(payablePaxCount, noOfSegments);
			}
		}
		return externalChgDTO;
	}
	
	public static ExternalChgDTO getBSPCharge(int payablePaxCount)
			throws ModuleException {
		
		Collection externalCharges = new ArrayList();
		externalCharges.add(EXTERNAL_CHARGES.BSP_FEE);

		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		Map externalChargesMap = reservationBD.getQuotedExternalCharges(externalCharges, null, ChargeRateOperationType.ANY);

		ExternalChgDTO externalChgDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.BSP_FEE);
		
		if (externalChgDTO == null) {
			String msg = "Error loading BSP Feecharge info[null]";			
			throw new RuntimeException(msg);
		} else {
			externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();			
			externalChgDTO.calculateAmount(payablePaxCount);			
		}
		
		return externalChgDTO;
	}

	/**
	 * returns the Handler fee for the current agent
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static ExternalChgDTO getHangleCharges() throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		@SuppressWarnings("unchecked")
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, ChargeRateOperationType.MAKE_ONLY);
		ExternalChgDTO externalChgDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
		if (externalChgDTO == null) {
			String msg = "Error loading handler charge info[null]";
			throw new RuntimeException(msg);
		}
		return externalChgDTO;
	}

	/**
	 * Retuns the flexi charge information.
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static ExternalChgDTO getFlexiCharges() throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.FLEXI_CHARGES);
		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, null);
		ExternalChgDTO externalChgDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.FLEXI_CHARGES);
		if (externalChgDTO == null) {
			String msg = "Error loading flexi charge info[null]";
			throw new RuntimeException(msg);
		} else {
			externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
		}
		return externalChgDTO;
	}

	/**
	 * 
	 * Retrieves currency information. (Copied from XBE)
	 * 
	 * @param currencyCode
	 * @param applicableExRateTimestamp
	 * @return
	 * @throws ModuleException
	 */
	public static CurrencyInfoTO getCurrencyInfo(String currencyCode, ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		CurrencyInfoTO currencyInfoTO = new CurrencyInfoTO();
		currencyInfoTO.setCurrencyCode(currencyCode);
		currencyInfoTO.setExchangeRateTimestamp(exchangeRateProxy.getExecutionDate());

		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);

		if (currencyExchangeRate != null) {
			Currency currency = currencyExchangeRate.getCurrency();
			currencyInfoTO.setCurrencyDesc(currency.getCurrencyDescriprion());

			if (currency.getStatus().equals(Currency.STATUS_ACTIVE) && currency.getXBEVisibility() == 1) {
				currencyInfoTO.setBoundryValue(currency.getBoundryValue());
				currencyInfoTO.setBreakPointValue(currency.getBreakPoint());
				currencyInfoTO.setDefaultIpgId(currency.getDefaultXbePGId());
				if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultXbePGId() != null) {
					currencyInfoTO.setIsCardPaymentEnabled(true);
				} else {
					currencyInfoTO.setIsCardPaymentEnabled(false);
				}
				currencyInfoTO.setCurrencyExchangeRate(currencyExchangeRate.getMultiplyingExchangeRate());
				boolean blnRoundAppParam = AppSysParamsUtil.isCurrencyRoundupEnabled();

				if (blnRoundAppParam && currencyInfoTO.getBoundryValue() != null
						&& currencyInfoTO.getBoundryValue().doubleValue() != 0 && currencyInfoTO.getBreakPointValue() != null) {
					currencyInfoTO.setIsApplyRounding(true);
				} else {
					currencyInfoTO.setIsApplyRounding(false);
				}
			}
		}

		return currencyInfoTO;
	}

	/**
	 * Identifies and retrieves card payment currency information. (Copied from XBE)
	 * 
	 * @param selectedCurrencyInfoTO
	 * @param applicableExchangeRateTimestamp
	 * @param ipgId
	 * @return
	 * @throws ModuleException
	 */
	public static CurrencyInfoTO getCardPaymentCurrencyInfo(CurrencyInfoTO selectedCurrencyInfoTO,
			ExchangeRateProxy exchangeRateProxy, Integer ipgId) throws ModuleException {
		CurrencyInfoTO cardPayCurrencyTO = null;

		if ((ipgId != null && selectedCurrencyInfoTO != null && selectedCurrencyInfoTO.getIsCardPaymentEnabled() != null
				&& selectedCurrencyInfoTO.getIsCardPaymentEnabled() && WPModuleUtils.getPaymentBrokerBD()
				.isPaymentGatewaySupportsCurrency(ipgId, selectedCurrencyInfoTO.getCurrencyCode()))) {
			cardPayCurrencyTO = selectedCurrencyInfoTO;
		} else {
			if (ipgId != null) {
				String ipgCurrencyCode = ((IPGPaymentOptionDTO) (WPModuleUtils.getPaymentBrokerBD().getPaymentGateways(ipgId)
						.toArray()[0])).getBaseCurrency();
				getCurrencyInfo(ipgCurrencyCode, exchangeRateProxy);
				cardPayCurrencyTO = getCurrencyInfo(ipgCurrencyCode, exchangeRateProxy);

			} else {
				// Choosing default card payment currency (RES_69)
				String defaultCardPayCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
				cardPayCurrencyTO = getCurrencyInfo(defaultCardPayCurrencyCode, exchangeRateProxy);

				if (AppSysParamsUtil.isXBECreditCardPaymentsEnabled()) {
					if (cardPayCurrencyTO == null || cardPayCurrencyTO.getIsCardPaymentEnabled() == null
							|| !cardPayCurrencyTO.getIsCardPaymentEnabled()) {
						// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
						// "No payment gateway configured for default payment currency");
						throw new ModuleException("");
					}
				}
			}
		}

		return cardPayCurrencyTO;
	}

	/**
	 * Prepares PayCurrencyDTO for card payment.
	 * 
	 * @param selectedCurrencyCode
	 * @param applicableExchangeRateTimestamp
	 * @param ipgId
	 * @return
	 * @throws ModuleException
	 */
	public static PayCurrencyDTO getPayCurrencyDTOForCCPay(String selectedCurrencyCode, ExchangeRateProxy exchangeRateProxy,
			Integer ipgId) throws ModuleException {
		PayCurrencyDTO payCurrencyDTO = null;

		if (AppSysParamsUtil.isStorePaymentsInAgentCurrencyEnabled()) {
			CurrencyInfoTO payCurrencyInfoTO = null;

			CurrencyInfoTO selectedCurrencyInfoTO = getCurrencyInfo(selectedCurrencyCode, exchangeRateProxy);

			payCurrencyInfoTO = getCardPaymentCurrencyInfo(selectedCurrencyInfoTO, exchangeRateProxy, ipgId);

			payCurrencyDTO = new PayCurrencyDTO(payCurrencyInfoTO.getCurrencyCode(), payCurrencyInfoTO.getCurrencyExchangeRate(),
					payCurrencyInfoTO.getBoundryValue(), payCurrencyInfoTO.getBreakPointValue());

		}
		return payCurrencyDTO;
	}

	/**
	 * Retrieves payment gateway parameters for specified.
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static IPGIdentificationParamsDTO getPaymentGatewayParams(String selectedCurrencyCode, Integer ipgId,
			PayCurrencyDTO payCurrencyDTO) throws ModuleException {

		String paymentCurrencyCode = null;
		if (payCurrencyDTO == null) {
			paymentCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
			if (!AppSysParamsUtil.getBaseCurrency().equals(paymentCurrencyCode)) {
				// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
				// "Card payment not supported for base currency");
				throw new ModuleException("");
			}
		} else {
			paymentCurrencyCode = payCurrencyDTO.getPayCurrencyCode();
		}

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, paymentCurrencyCode);

		boolean isPGExists = WPModuleUtils.getPaymentBrokerBD().checkForIPG(ipgIdentificationParamsDTO);

		if (!isPGExists) {
			// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
			// "Card payment gateway configuration not found");
			throw new ModuleException("");
		}

		ipgIdentificationParamsDTO.setFQIPGConfigurationName(ipgId + "_" + paymentCurrencyCode);
		ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);

		return ipgIdentificationParamsDTO;
	}

	/**
	 * Splits the external charges among pax
	 * 
	 * @param payingPaxCount
	 * @param totalExternalCharges
	 * @return
	 * @throws ModuleException
	 */
	public static LinkedList getExternalChagersPerPassengers(int payingPaxCount, ExternalChgDTO externalChgDTO)
			throws ModuleException {

		Collection[] externalCharges = new Collection[payingPaxCount];
		if (externalChgDTO.getAmount().doubleValue() > 0) {
			Collection colExternalChgDTO = ReservationApiUtils.getPerPaxExternalCharges((ExternalChgDTO) externalChgDTO.clone(),
					payingPaxCount);
			externalCharges = ReservationApiUtils.copyToCollectionArray(externalCharges, colExternalChgDTO);
		}
		return ReservationApiUtils.createLinkedList(externalCharges);
	}

	/**
	 * Splits credit card transaction fee among pax. (Copied from XBE) TODO remove this method after review.
	 * 
	 * @param payingPaxCount
	 * @param bookingAmount
	 * @return
	 * @throws ModuleException
	 */
	public static LinkedList getCCChargePayAmountsPerPassengers(int payingPaxCount, BigDecimal bookingAmount)
			throws ModuleException {

		Collection[] externalCharges = new Collection[payingPaxCount];
		BigDecimal totalAccumilated = AccelAeroCalculator.add(bookingAmount, AccelAeroCalculator.getDefaultBigDecimalZero());
		ExternalChgDTO externalChgDTO;

		if (totalAccumilated.doubleValue() > 0 && AppSysParamsUtil.applyCreditCardCharge()) {
			externalChgDTO = getCCCharge(bookingAmount);
			Collection colExternalChgDTO = ReservationApiUtils.getPerPaxExternalCharges(externalChgDTO, payingPaxCount);
			externalCharges = ReservationApiUtils.copyToCollectionArray(externalCharges, colExternalChgDTO);

			totalAccumilated = AccelAeroCalculator.add(totalAccumilated, externalChgDTO.getAmount());
		}

		return ReservationApiUtils.createLinkedList(externalCharges);
	}

	/**
	 * Extracts segment information from segsSestsAndFaresCol to be used in save booking.
	 * 
	 * 
	 * FIXME remove duplication in create/modify flows and merge both flows
	 * 
	 * @param segsSeatsAndFaresCol
	 * @return Object [] - outbound flight segments, inbound flight segments TODO: verify fareGroup for return fares.
	 */
	public static Object[] extractNewResSegmentsInfo(Collection<OndFareDTO> segsSeatsAndFaresCol, Integer nextSegSeq,
			Integer nextOndGroup, Integer nextReturnOndGroup) {

		int fareGroup = nextOndGroup.intValue();
		int returnOndGroup = nextReturnOndGroup.intValue();

		Collection<ReservationSegmentTO> obFlightSegments = new LinkedList<ReservationSegmentTO>();
		Collection<ReservationSegmentTO> ibFlightSegments = null;

		for (OndFareDTO ondFareDTO : segsSeatsAndFaresCol) {
			for (Iterator<FlightSegmentDTO> fltSegDTOIt = ondFareDTO.getSegmentsMap().values().iterator(); fltSegDTOIt.hasNext();) {
				FlightSegmentDTO flightSegmentDTO = fltSegDTOIt.next();
				if (!ondFareDTO.isInBoundOnd()) {
					obFlightSegments.add(new ReservationSegmentTO(flightSegmentDTO.getSegmentId(), fareGroup, returnOndGroup,
							ondFareDTO.getSelectedBundledFarePeriodId()));
				} else {
					if (ibFlightSegments == null) {
						ibFlightSegments = new LinkedList<ReservationSegmentTO>();
					}
					ibFlightSegments.add(new ReservationSegmentTO(flightSegmentDTO.getSegmentId(), fareGroup, returnOndGroup,
							ondFareDTO.getSelectedBundledFarePeriodId()));
				}
			}
			++fareGroup;

			if (ondFareDTO.getFareType() == FareTypes.RETURN_FARE || ondFareDTO.getFareType() == FareTypes.HALF_RETURN_FARE)
				;
			else
				++returnOndGroup;
		}
		// set segment sequence
		SegmentUtil.setSegmentSequnceNumbers(segsSeatsAndFaresCol, obFlightSegments, ibFlightSegments, nextSegSeq.intValue());

		return new Object[] { obFlightSegments, ibFlightSegments };
	}

	/**
	 * Prepares new segments for add/mod segments.
	 * 
	 * @param existingRes
	 * @param newFareChargesAndSegDTOs
	 * @return
	 */
	public static ISegment prepareNewResSegments(Reservation existingRes, Collection<OndFareDTO> newFareChargesAndSegDTOs,
			Collection pnrSegIdsForMod) {
		ISegment segmentAssembler = new SegmentAssembler(newFareChargesAndSegDTOs);

		int[] nextSegAndOndSequences = getNextSegAndOndSequences(existingRes);

		// check for segment type
		boolean blnOutFlight = true;
		int modifiedReturnOndGroupId = -1;

		if (pnrSegIdsForMod != null && !pnrSegIdsForMod.isEmpty()) {
			ReservationSegment resSegment;
			Iterator iterSegments = existingRes.getSegments().iterator();
			while (iterSegments.hasNext()) {
				resSegment = (ReservationSegment) iterSegments.next();
				if (pnrSegIdsForMod.contains(resSegment.getPnrSegId())) {
					modifiedReturnOndGroupId = resSegment.getReturnOndGroupId();
					break;
				}
			}
			blnOutFlight = isOutFlight(existingRes, pnrSegIdsForMod);
		}
		if (modifiedReturnOndGroupId != -1) {
			for (OndFareDTO ondFareDTO : newFareChargesAndSegDTOs) {
				if (ondFareDTO.getFareType() == FareTypes.HALF_RETURN_FARE) {
					nextSegAndOndSequences[2] = modifiedReturnOndGroupId;
					break;
				}
			}
		}

		Object[] inOutboundSegments = ReservationUtil.extractNewResSegmentsInfo(newFareChargesAndSegDTOs,
				nextSegAndOndSequences[0], nextSegAndOndSequences[1], nextSegAndOndSequences[2]);

		Collection outBoundSegments = (Collection) inOutboundSegments[0];
		Collection inBoundSegments = (Collection) inOutboundSegments[1];

		for (Iterator obSegIt = outBoundSegments.iterator(); obSegIt.hasNext();) {
			ReservationSegmentTO segmentInfo = (ReservationSegmentTO) obSegIt.next();
			if (blnOutFlight)
				segmentAssembler.addOutgoingSegment((OndSequence.OUT_BOUND + 1), segmentInfo.getSegmentSeq(),
						segmentInfo.getFlightSegId(), segmentInfo.getOndGroupId(), segmentInfo.getReturnOndGroupId(), null,
						segmentInfo.getSelectedBundledFarePeriodId(), segmentInfo.getCodeShareFlightNumber(),
						segmentInfo.getCodeShareBookingClass());
			else
				segmentAssembler.addReturnSegment((OndSequence.IN_BOUND + 1), segmentInfo.getSegmentSeq(),
						segmentInfo.getFlightSegId(), segmentInfo.getOndGroupId(), null, segmentInfo.getReturnOndGroupId(), null,
						segmentInfo.getSelectedBundledFarePeriodId(), segmentInfo.getCodeShareFlightNumber(),
						segmentInfo.getCodeShareBookingClass());
		}

		if (inBoundSegments != null) {
			for (Iterator ibSegIt = inBoundSegments.iterator(); ibSegIt.hasNext();) {
				ReservationSegmentTO segmentInfo = (ReservationSegmentTO) ibSegIt.next();
				segmentAssembler.addReturnSegment((OndSequence.IN_BOUND + 1), segmentInfo.getSegmentSeq(),
						segmentInfo.getFlightSegId(), segmentInfo.getOndGroupId(), null, segmentInfo.getReturnOndGroupId(), null,
						segmentInfo.getSelectedBundledFarePeriodId(), segmentInfo.getCodeShareFlightNumber(),
						segmentInfo.getCodeShareBookingClass());
			}
		}
		return segmentAssembler;
	}

	/**
	 * Prepares new segments for add/mod segments.
	 * 
	 * @param existingRes
	 * @param newFareChargesAndSegDTOs
	 * @param groundStationBySegmentMap
	 * @param groundSementFlightByAirFlightMap
	 * @param pnrSegIdsForMod
	 * @return
	 * @throws ModuleException
	 */
	public static ISegment prepareNewResSegments(Reservation existingRes, Collection<OndFareDTO> newFareChargesAndSegDTOs,
			Map<Integer, String> groundStationBySegmentMap, Map<Integer, Integer> groundSementFlightByAirFlightMap,
			Collection pnrSegIdsForMod) throws ModuleException {
		ISegment segmentAssembler = new SegmentAssembler(newFareChargesAndSegDTOs);

		int[] nextSegAndOndSequences = getNextSegAndOndSequences(existingRes);

		// check for segment type
		boolean blnOutFlight = true;
		int modifiedReturnOndGroupId = -1;

		if (pnrSegIdsForMod != null && !pnrSegIdsForMod.isEmpty()) {
			ReservationSegment resSegment;
			Iterator iterSegments = existingRes.getSegments().iterator();
			while (iterSegments.hasNext()) {
				resSegment = (ReservationSegment) iterSegments.next();
				if (pnrSegIdsForMod.contains(resSegment.getPnrSegId())) {
					modifiedReturnOndGroupId = resSegment.getReturnOndGroupId();
					break;
				}
			}
			blnOutFlight = isOutFlight(existingRes, pnrSegIdsForMod);
		}
		if (modifiedReturnOndGroupId != -1) {
			for (OndFareDTO ondFareDTO : newFareChargesAndSegDTOs) {
				if (ondFareDTO.getFareType() == FareTypes.HALF_RETURN_FARE) {
					nextSegAndOndSequences[2] = modifiedReturnOndGroupId;
					break;
				}
			}
		}

		Object[] inOutboundSegments = ReservationUtil.extractNewResSegmentsInfo(newFareChargesAndSegDTOs,
				nextSegAndOndSequences[0], nextSegAndOndSequences[1], nextSegAndOndSequences[2]);

		Collection outBoundSegments = (Collection) inOutboundSegments[0];
		Collection inBoundSegments = (Collection) inOutboundSegments[1];

		for (Iterator obSegIt = outBoundSegments.iterator(); obSegIt.hasNext();) {
			ReservationSegmentTO segmentInfo = (ReservationSegmentTO) obSegIt.next();
			if (blnOutFlight)
				segmentAssembler.addOutgoingSegment((OndSequence.OUT_BOUND + 1), segmentInfo.getSegmentSeq(),
						segmentInfo.getFlightSegId(), segmentInfo.getOndGroupId(), segmentInfo.getReturnOndGroupId(), null,
						segmentInfo.getSelectedBundledFarePeriodId(), segmentInfo.getCodeShareFlightNumber(),
						segmentInfo.getCodeShareBookingClass());
			else
				segmentAssembler.addReturnSegment((OndSequence.IN_BOUND + 1), segmentInfo.getSegmentSeq(),
						segmentInfo.getFlightSegId(), segmentInfo.getOndGroupId(), null, segmentInfo.getReturnOndGroupId(), null,
						segmentInfo.getSelectedBundledFarePeriodId(), segmentInfo.getCodeShareFlightNumber(),
						segmentInfo.getCodeShareBookingClass());

			segmentAssembler.addGroundStationDataToPNRSegment(groundStationBySegmentMap, groundSementFlightByAirFlightMap);
		}

		if (inBoundSegments != null) {
			for (Iterator ibSegIt = inBoundSegments.iterator(); ibSegIt.hasNext();) {
				ReservationSegmentTO segmentInfo = (ReservationSegmentTO) ibSegIt.next();
				segmentAssembler.addReturnSegment((OndSequence.IN_BOUND + 1), segmentInfo.getSegmentSeq(),
						segmentInfo.getFlightSegId(), segmentInfo.getOndGroupId(), null, segmentInfo.getReturnOndGroupId(), null,
						segmentInfo.getSelectedBundledFarePeriodId(), segmentInfo.getCodeShareFlightNumber(),
						segmentInfo.getCodeShareBookingClass());

				segmentAssembler.addGroundStationDataToPNRSegment(groundStationBySegmentMap, groundSementFlightByAirFlightMap);
			}
		}
		return segmentAssembler;
	}

	public static void setExistingSegInfo(Reservation reservation, AvailableFlightSearchDTO availableFlightSearchDTO,
			Collection<Integer> modifiedPnrSegIds) throws ModuleException {
		// set existing reservation segments

		// boolean blnAllowHalfReturnFaresForModification = AppSysParamsUtil.isAllowHalfReturnFaresForModification();

		availableFlightSearchDTO.setExistingFlightSegments(getFlightSegFromRes(reservation));

		String enforceSameHigherFare = WSClientModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.ENFORCE_SAME_HIGHER);

		if (modifiedPnrSegIds != null) {
			// set modified reservation segments
			Collection<FlightSegmentDTO> modifiedFlightSegmentDTOs = getFlightModSegFromRes(reservation, modifiedPnrSegIds);
			availableFlightSearchDTO.setModifiedFlightSegments(modifiedFlightSegmentDTOs);

			if (availableFlightSearchDTO.isDateChangeModification()) {
				ReservationPax firstAdultOrChildPax = null;
				// Pick first adult pax
				for (Iterator<ReservationPax> paxIt = reservation.getPassengers().iterator(); paxIt.hasNext();) {
					ReservationPax pax = paxIt.next();
					if (ReservationInternalConstants.PassengerType.ADULT.equals(pax.getPaxType())) {
						firstAdultOrChildPax = pax;
						break;
					}
				}

				if (firstAdultOrChildPax == null) {
					// Consider first child pax
					for (Iterator<ReservationPax> paxIt = reservation.getPassengers().iterator(); paxIt.hasNext();) {
						ReservationPax pax = paxIt.next();
						if (!ReservationInternalConstants.PassengerType.INFANT.equals(pax.getPaxType())) {
							firstAdultOrChildPax = pax;
							break;
						}
					}
				}

				BigDecimal ondFareAmount = null;
				FareTO fareTO = null;
				Integer returnOndGroupId = null;
				if (firstAdultOrChildPax != null) {
					for (Iterator<ReservationPaxFare> paxFareIt = firstAdultOrChildPax.getPnrPaxFares().iterator(); paxFareIt
							.hasNext();) {
						ReservationPaxFare paxFare = paxFareIt.next();
						for (Iterator<ReservationPaxFareSegment> paxFareSegIt = paxFare.getPaxFareSegments().iterator(); paxFareSegIt
								.hasNext();) {
							ReservationPaxFareSegment paxFareSeg = paxFareSegIt.next();
							if (modifiedPnrSegIds.contains(paxFareSeg.getPnrSegId())) {
								ondFareAmount = paxFare.getTotalFare();
								fareTO = paxFareSeg.getSegment().getFareTO(null);
								returnOndGroupId = paxFareSeg.getSegment().getReturnOndGroupId();
								// if(blnAllowHalfReturnFaresForModification){
								ReservationApiUtils.setHalfReturnSearchCriteria(paxFareSeg, fareTO, returnOndGroupId,
										availableFlightSearchDTO);
								// }
								break;
							}
						}
						/*
						 * if (ondFareAmount != null){
						 * availableFlightSearchDTO.setModifiedOndPaxFareAmount(ondFareAmount); if(enforceSameHigherFare
						 * != null && enforceSameHigherFare.equalsIgnoreCase("Y"))
						 * availableFlightSearchDTO.setEnforceFareCheckForModOnd(true); break; }
						 */
					}
				}
			}
		}
	}

	/**
	 * Returns Collection<FlightSegmentDTO> of confirmed segments from the reservation.
	 * 
	 * @param res
	 * @return
	 */
	private static Collection getFlightSegFromRes(Reservation res) {
		Collection colExistRes = null;
		Collection<FlightSegmentDTO> colExistFlt = new HashSet<FlightSegmentDTO>();
		if (res != null)
			colExistRes = res.getSegmentsView();
		if (colExistRes != null) {
			ReservationSegmentDTO resSegDto = null;

			Iterator rsSegIter = colExistRes.iterator();
			while (rsSegIter.hasNext()) {
				resSegDto = (ReservationSegmentDTO) rsSegIter.next();
				FlightSegmentDTO fltSeg = new FlightSegmentDTO();
				if (resSegDto != null
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSegDto.getStatus())) {
					fltSeg.setArrivalDateTime(resSegDto.getArrivalDate());
					fltSeg.setDepartureDateTime(resSegDto.getDepartureDate());
					fltSeg.setFlightId(resSegDto.getFlightId());
					fltSeg.setFlightNumber(resSegDto.getFlightNo());
					fltSeg.setSegmentCode(resSegDto.getSegmentCode());
					fltSeg.setArrivalDateTimeZulu(resSegDto.getZuluArrivalDate());
					fltSeg.setDepartureDateTimeZulu(resSegDto.getZuluDepartureDate());
					fltSeg.setSegmentId(resSegDto.getFlightSegId());
					fltSeg.setOperationTypeID(resSegDto.getOperationTypeID());
					colExistFlt.add(fltSeg);
				}
			}
		}
		return colExistFlt;
	}

	/**
	 * Returns Collection<FlightSegmentDTO> of segments corresponding to the pnrSegIds passed.
	 * 
	 * @param res
	 * @param pnrSegs
	 * @return
	 */
	private static Collection getFlightModSegFromRes(Reservation res, Collection pnrSegs) {
		Collection colExistRes = null;
		List<FlightSegmentDTO> colExistFlt = new ArrayList<FlightSegmentDTO>();
		if (res != null)
			colExistRes = res.getSegmentsView();
		if (colExistRes != null) {
			ReservationSegmentDTO resSegDto = null;

			Iterator rsSegIter = colExistRes.iterator();
			while (rsSegIter.hasNext()) {
				resSegDto = (ReservationSegmentDTO) rsSegIter.next();
				FlightSegmentDTO fltSeg = new FlightSegmentDTO();
				if (resSegDto != null
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSegDto.getStatus())
						&& isSegmentModified(resSegDto.getPnrSegId(), pnrSegs)) {
					fltSeg.setArrivalDateTime(resSegDto.getArrivalDate());
					fltSeg.setDepartureDateTime(resSegDto.getDepartureDate());
					fltSeg.setFlightId(resSegDto.getFlightId());
					fltSeg.setFlightNumber(resSegDto.getFlightNo());
					fltSeg.setSegmentCode(resSegDto.getSegmentCode());
					fltSeg.setArrivalDateTimeZulu(resSegDto.getZuluArrivalDate());
					fltSeg.setDepartureDateTimeZulu(resSegDto.getZuluDepartureDate());
					fltSeg.setSegmentId(resSegDto.getFlightSegId());
					fltSeg.setFromAirport(resSegDto.getSegmentCode().substring(0, 3));
					fltSeg.setToAirport(resSegDto.getSegmentCode().substring(resSegDto.getSegmentCode().length() - 3));
					fltSeg.setOperationTypeID(resSegDto.getOperationTypeID());
					colExistFlt.add(fltSeg);
				}
			}
		}

		if (colExistFlt.size() > 1) {
			Collections.sort(colExistFlt);
		}

		return colExistFlt;
	}

	private static boolean isSegmentModified(Integer chekval, Collection<Integer> col) {
		boolean sameSeg = false;
		Integer againsval = null;
		if (chekval != null && col != null) {
			Iterator<Integer> intIter = col.iterator();
			while (intIter.hasNext()) {
				againsval = (Integer) intIter.next();
				if (againsval.compareTo(chekval) == 0)
					return true;
			}
		}
		return sameSeg;
	}

	private static boolean isOutFlight(Reservation reservation, Collection<Integer> collOldSegmentIds) {
		boolean blnReturnFlight = true;

		Collection<ReservationSegmentDTO> colSegDTO = reservation.getSegmentsView();
		Iterator<ReservationSegmentDTO> iterSegments = colSegDTO.iterator();

		for (Iterator<ReservationSegmentDTO> iter = colSegDTO.iterator(); iter.hasNext();) {
			ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) iterSegments.next();

			for (Iterator<Integer> iterColOldSegId = collOldSegmentIds.iterator(); iterColOldSegId.hasNext();) {
				Integer oldPNRSegId = (Integer) iterColOldSegId.next();

				if (oldPNRSegId.equals(reservationSegmentDTO.getPnrSegId())) {
					return reservationSegmentDTO.getReturnFlag().equals(
							ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE) ? true : false;
				}
			}
		}

		return blnReturnFlight;
	}

	public static List<FareRuleDTO> getMergedFareRuleDTOs(List<FareRuleDTO> fareRules) {
		List<FareRuleDTO> fareRuleDTOs = new ArrayList<FareRuleDTO>();
		List<FareRuleDTO> fareRuleDTOsToAdd = new ArrayList<FareRuleDTO>();
		if (fareRules != null) {
			for (FareRuleDTO fareRuleDTO : fareRules) {
				if (!(isOnDCodePresent(fareRuleDTOs, fareRuleDTO))) {
					fareRuleDTOs.add(cloneFareRuleDTO(fareRuleDTO));
				} else {
					fareRuleDTOsToAdd.add(cloneFareRuleDTO(fareRuleDTO));
				}
			}
		}
		for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
			for (FareRuleDTO fareRuleDTOToAdd : fareRuleDTOsToAdd) {
				if (fareRuleDTO.getOrignNDest().equals(fareRuleDTOToAdd.getOrignNDest())) {
					fareRuleDTO.setAdultFareAmount(AccelAeroCalculator.add(fareRuleDTO.getAdultFareAmount(),
							fareRuleDTOToAdd.getAdultFareAmount()));
					fareRuleDTO.setChildFareAmount(AccelAeroCalculator.add(fareRuleDTO.getChildFareAmount(),
							fareRuleDTOToAdd.getChildFareAmount()));
				}
			}
		}
		return fareRuleDTOs;
	}

	/**
	 * Till we implement an efficient way to sort the fare rules we will inject departure date from OriginDestionation
	 * Options
	 * 
	 * @param fareRules
	 * @param ondList
	 */
	public static void injectDepartureDateToFareRules(List<FareRuleDTO> fareRules, List<OriginDestinationInformationTO> ondList) {
		for (OriginDestinationInformationTO onDInfoTO : ondList) {
			for (OriginDestinationOptionTO optionTO : onDInfoTO.getOrignDestinationOptions()) {
				if (optionTO.isSelected()) {
					for (FareRuleDTO fareRuleDTO : fareRules) {
						FLT_SEGS: for (IFlightSegment flightSegmentTO : optionTO.getFlightSegmentList()) {
							if (fareRuleDTO.getOrignNDest().equals(flightSegmentTO.getSegmentCode())) {
								fareRuleDTO.setDepartureDateLong(flightSegmentTO.getDepartureDateTime().getTime());
								break FLT_SEGS;
							}
						}
					}
				}
			}
		}
	}

	public static void injectDepartureDateToFareRules(List<FareRuleDTO> fareRules, Map<String, Long> segmentDepartureTimeMap) {
		for (FareRuleDTO fareRuleDTO : fareRules) {
			if (segmentDepartureTimeMap.containsKey(fareRuleDTO.getOrignNDest())) {
				fareRuleDTO.setDepartureDateLong(segmentDepartureTimeMap.get(fareRuleDTO.getOrignNDest()));
			}
		}
	}

	/**
	 * Return the no ONDs for the reservation. FIXME duplicate with airproxy ReservationUtil
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static int getUniqueFareIds(Reservation reservation) throws ModuleException {
		Collection<ReservationSegment> pnrSegments = reservation.getSegments();
		Map pnrSegIdAndFareTO = ReservationApiUtils.getFareTOs(reservation, null);

		Collection<Integer> UniqueFareIds = new HashSet<Integer>();

		ReservationSegment reservationSegment;
		for (Iterator itColReservationSegment = pnrSegments.iterator(); itColReservationSegment.hasNext();) {
			reservationSegment = (ReservationSegment) itColReservationSegment.next();
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())) {
				UniqueFareIds.add(reservationSegment.getOndGroupId());
			}

		}

		return UniqueFareIds.size();
	}

	private static FareRuleDTO cloneFareRuleDTO(FareRuleDTO originalFareRuleDTO) {
		FareRuleDTO fareRuleDTO = new FareRuleDTO();

		if (originalFareRuleDTO.getOrignNDest() != null)
			fareRuleDTO.setOrignNDest(new String(originalFareRuleDTO.getOrignNDest()));
		if (originalFareRuleDTO.getFareRuleCode() != null)
			fareRuleDTO.setFareRuleCode(new String(originalFareRuleDTO.getFareRuleCode()));
		if (originalFareRuleDTO.getFareBasisCode() != null)
			fareRuleDTO.setFareBasisCode(new String(originalFareRuleDTO.getFareBasisCode()));
		if (originalFareRuleDTO.getFareCategoryCode() != null)
			fareRuleDTO.setFareCategoryCode(new String(originalFareRuleDTO.getFareCategoryCode()));
		if (originalFareRuleDTO.getDescription() != null)
			fareRuleDTO.setDescription(new String(originalFareRuleDTO.getDescription()));
		if (originalFareRuleDTO.getBookingClassCode() != null)
			fareRuleDTO.setBookingClassCode(new String(originalFareRuleDTO.getBookingClassCode()));
		if (originalFareRuleDTO.getCabinClassCode() != null)
			fareRuleDTO.setCabinClassCode(new String(originalFareRuleDTO.getCabinClassCode()));
		fareRuleDTO.setFareRuleId(originalFareRuleDTO.getFareRuleId());

		if (originalFareRuleDTO.getAdultFareAmount() != null)
			fareRuleDTO.setAdultFareAmount(new BigDecimal(originalFareRuleDTO.getAdultFareAmount().toPlainString()));
		fareRuleDTO.setAdultFareApplicable(originalFareRuleDTO.isAdultFareApplicable());
		fareRuleDTO.setAdultFareRefundable(originalFareRuleDTO.isAdultFareRefundable());
		fareRuleDTO.setDepartureDateLong(originalFareRuleDTO.getDepartureDateLong());
		fareRuleDTO.setComments(originalFareRuleDTO.getComments());
		fareRuleDTO.setAgentInstructions(originalFareRuleDTO.getAgentInstructions());

		if (originalFareRuleDTO.getChildFareAmount() != null)
			fareRuleDTO.setChildFareAmount(new BigDecimal(originalFareRuleDTO.getChildFareAmount().toPlainString()));
		fareRuleDTO.setChildFareApplicable(originalFareRuleDTO.isChildFareApplicable());
		fareRuleDTO.setChildFareRefundable(originalFareRuleDTO.isChildFareRefundable());
		if (originalFareRuleDTO.getChildFareType() != null)
			fareRuleDTO.setChildFareType(new String(originalFareRuleDTO.getChildFareType()));

		if (originalFareRuleDTO.getInfantFareAmount() != null)
			fareRuleDTO.setInfantFareAmount(new BigDecimal(originalFareRuleDTO.getInfantFareAmount().toPlainString()));
		fareRuleDTO.setInfantFareApplicable(originalFareRuleDTO.isInfantFareApplicable());
		fareRuleDTO.setInfantFareRefundable(originalFareRuleDTO.isInfantFareRefundable());
		if (originalFareRuleDTO.getInfantFareType() != null)
			fareRuleDTO.setInfantFareType(new String(originalFareRuleDTO.getInfantFareType()));

		// No deep cloning required as depAPFareDTO is defined for OND
		fareRuleDTO.setCurrencyCode(originalFareRuleDTO.getCurrencyCode());
		fareRuleDTO.setDepAPFareDTO(originalFareRuleDTO.getDepAPFareDTO());
		fareRuleDTO.setOndSequence(originalFareRuleDTO.getOndSequence());

		return fareRuleDTO;
	}

	private static boolean isOnDCodePresent(List<FareRuleDTO> fareRuleDTOs, FareRuleDTO fareRuleDTO2) {
		for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
			if (fareRuleDTO.getOrignNDest().equals(fareRuleDTO2.getOrignNDest())
					&& fareRuleDTO.getOndSequence() == fareRuleDTO2.getOndSequence()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param airTravelerEtickets
	 * @return
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> createTicketMap(List<AirTravelerEticket> airTravelerEtickets) {
		Map<Integer, Map<Integer, EticketDTO>> eTicketMap = new HashMap<Integer, Map<Integer, EticketDTO>>();
		if (airTravelerEtickets != null && !airTravelerEtickets.isEmpty()) {
			for (AirTravelerEticket paxEticket : airTravelerEtickets) {
				Map<Integer, EticketDTO> paxEticketmap = new HashMap<Integer, EticketDTO>();
				eTicketMap.put(paxEticket.getPaxSequence(), paxEticketmap);
				for (Eticket eticket : paxEticket.getETickets()) {
					EticketDTO eticketDTO = new EticketDTO(eticket.getEticketNumber(), eticket.getCouponNo());
					paxEticketmap.put(eticket.getSegmentSequence(), eticketDTO);
				}
			}
		}
		return eTicketMap;

	}

	public static List<LCCClientExternalChgDTO> transform(List<ExternalChargeTO> surchargeTOs,
			List<FlightSegmentTO> flightSegmentTOs) {
		List<LCCClientExternalChgDTO> externalChgDTOs = new ArrayList<LCCClientExternalChgDTO>();
		for (SurchargeTO surchargeTO : surchargeTOs) {
			int applicableOnds = getApplicableSegmentCount(surchargeTO.getSegmentCode(), flightSegmentTOs,
					surchargeTO.getOndSequence());
			if (applicableOnds > 0) {
				BigDecimal[] amountBreakDown = AccelAeroCalculator.roundAndSplit(surchargeTO.getAmount(), applicableOnds);
				int segCount = 0;
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					if (surchargeTO.getSegmentCode().contains(flightSegmentTO.getSegmentCode())
							&& surchargeTO.getOndSequence() == flightSegmentTO.getOndSequence()) {
						ExternalChargeTO externalChargeTO = (ExternalChargeTO) surchargeTO;
						LCCClientFlexiExternalChgDTO externalChgDTO = new LCCClientFlexiExternalChgDTO();
						externalChgDTO.setAmount(amountBreakDown[segCount]);
						externalChgDTO.setAmountConsumedForPayment(false);
						externalChgDTO.setCarrierCode(externalChargeTO.getCarrierCode());
						externalChgDTO.setCode(externalChargeTO.getSurchargeCode());
						externalChgDTO.setExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);
						externalChgDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
						externalChgDTO.setFlexiBilities(externalChargeTO.getAdditionalDetails());
						externalChgDTO.setSegmentCode(externalChargeTO.getSegmentCode());
						externalChgDTOs.add(externalChgDTO);
						segCount++;
					}
				}
			}
		}
		return externalChgDTOs;
	}

	public static boolean isDateModification(List<FlightSegmentTO> flightSegmentTOs, List<String> modifyingFlightList) {

		if (modifyingFlightList.size() != flightSegmentTOs.size()) {
			return false;
		}

		Collections.sort(flightSegmentTOs);
		Collections.sort(modifyingFlightList, new Comparator<String>() {

			@Override
			public int compare(String str1, String str2) {
				Date depDate1 = FlightRefNumberUtil.getDepartureDateFromFlightRPH(str1);
				Date depDate2 = FlightRefNumberUtil.getDepartureDateFromFlightRPH(str2);

				return depDate1.compareTo(depDate2);
			}

		});

		for (int i = 0; i < modifyingFlightList.size(); i++) {
			String flightRefNum = modifyingFlightList.get(i);
			String modifyingSegCode = FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightRefNum);
			if (flightSegmentTOs.size() > i) {
				if (!flightSegmentTOs.get(i).getSegmentCode().equals(modifyingSegCode)) {
					return false;
				}
			}
		}

		return true;
	}

	private static int getApplicableSegmentCount(String surChgSegCode, List<FlightSegmentTO> flightSegmentTOs, int ondSequence) {
		int i = 0;
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (surChgSegCode.contains(flightSegmentTO.getSegmentCode()) && ondSequence == flightSegmentTO.getOndSequence()) {
				i++;
			}
		}
		return i;
	}

	public static List<ReservationSegmentDTO> sortBySegSequnce(List<ReservationSegmentDTO> resSegments) {
		Collections.sort(resSegments, new Comparator<ReservationSegmentDTO>() {
			@Override
			public int compare(ReservationSegmentDTO at1, ReservationSegmentDTO at2) {
				return (new Integer(at1.getSegmentSeq())).compareTo(at2.getSegmentSeq());
			}
		});
		return resSegments;
	}

	public static List<FlightSegmentTO> getUnflownFlightSegmentsFromRes(Reservation reservation,
			List<AirTravelerEticket> existingEtickets) {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();

		List<Integer> flownFlightSegments = getFlownFlightSegmentList(existingEtickets);
		List<Integer> exchangedExternalSegmentSeqList = getExchangedExternalSegmentSeqList(reservation);

		for (ReservationSegmentDTO reservationSegment : reservation.getSegmentsView()) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())) {
				if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.DATE
						&& reservationSegment.getZuluDepartureDate().compareTo(currentDate) < 0) {
					continue;
				} else if (flownFlightSegments.contains(reservationSegment.getFlightSegId())) {
					continue;
				} else {
					FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
					flightSegmentTO.setDepartureDateTimeZulu(reservationSegment.getZuluDepartureDate());
					flightSegmentTO.setSegmentCode(reservationSegment.getSegmentCode());
					flightSegmentTO.setFlightSegId(reservationSegment.getFlightSegId());
					flightSegmentTO.setOperatingAirline(reservationSegment.getCarrierCode());
					flightSegmentTO.setSegmentSequence(reservationSegment.getSegmentSeq());
					flightSegmentTOs.add(flightSegmentTO);
				}
			}
		}

		Map<String, Integer> carrierWiseSegSeq = new HashMap<String, Integer>();
		for (ExternalPnrSegment externalPnrSegment : reservation.getExternalReservationSegment()) {
			ExternalFlightSegment externalFlightSegment = externalPnrSegment.getExternalFlightSegment();
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(externalPnrSegment.getStatus())) {
				if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.DATE
						&& externalFlightSegment.getEstTimeDepatureZulu().compareTo(currentDate) < 0) {
					continue;
				} else if (flownFlightSegments.contains(externalFlightSegment.getExternalFlightSegRef())) {
					continue;
				} else {
					FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
					flightSegmentTO.setDepartureDateTimeZulu(externalFlightSegment.getEstTimeDepatureZulu());
					flightSegmentTO.setSegmentCode(externalFlightSegment.getSegmentCode());
					flightSegmentTO.setFlightSegId(externalFlightSegment.getExternalFlightSegId());
					flightSegmentTO.setOperatingAirline(externalFlightSegment.getExternalCarrierCode());
					
					if (reservation.getSegmentsView() == null || reservation.getSegmentsView().isEmpty()) {
						flightSegmentTO.setSegmentSequence(externalPnrSegment.getSegmentSeq());
					} else {
						//TODO: We need to keep actual segment sequence of external segments and get this from it.
						Integer currentSegSeq = 1;
						if (flownFlightSegments != null && !flownFlightSegments.isEmpty()) {
							currentSegSeq = externalPnrSegment.getSegmentSeq();
						} else if (!exchangedExternalSegmentSeqList.isEmpty()) {
							currentSegSeq = exchangedExternalSegmentSeqList.size() + 1;
						}
						if (carrierWiseSegSeq.get(externalFlightSegment.getExternalCarrierCode()) != null) {
							currentSegSeq = carrierWiseSegSeq.get(externalFlightSegment.getExternalCarrierCode()) + 1;
						}
						carrierWiseSegSeq.put(externalFlightSegment.getExternalCarrierCode(), currentSegSeq);
						flightSegmentTO.setSegmentSequence(currentSegSeq);
					}
					flightSegmentTOs.add(flightSegmentTO);
				}
			}
		}

		return flightSegmentTOs;
	}
	
	private static List<Integer> getExchangedExternalSegmentSeqList(Reservation reservation) {
		List<Integer> exchangedExternalSegmentSeqList = new ArrayList<Integer>();
		for (ExternalPnrSegment externalPnrSegment : reservation.getExternalReservationSegment()) {
			if (ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(externalPnrSegment.getSubStatus())) {
				exchangedExternalSegmentSeqList.add(externalPnrSegment.getSegmentSeq());
			}
		}
		return exchangedExternalSegmentSeqList;
	}


	private static List<Integer> getFlownFlightSegmentList(List<AirTravelerEticket> existingEtickets) {
		List<Integer> flownFlightSegmentList = new ArrayList<Integer>();
		if (existingEtickets != null && existingEtickets.size() > 0) {
			for (AirTravelerEticket airTravelerEticket : existingEtickets) {
				for (Eticket eticket : airTravelerEticket.getETickets()) {
					if (eticket.getFlightSegmentRefNumber() != null
							&& AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET
							&& ReservationInternalConstants.PaxFareSegmentTypes.FLOWN.equals(eticket.getTicketStatus())) {
						flownFlightSegmentList.add(Integer.parseInt(eticket.getFlightSegmentRefNumber()));
					} else if (eticket.getFlightSegmentRefNumber() != null
							&& AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.PFS
							&& ReservationInternalConstants.PaxFareSegmentTypes.FLOWN.equals(eticket.getPaxStatus())) {
						flownFlightSegmentList.add(Integer.parseInt(eticket.getFlightSegmentRefNumber()));
					}else if(eticket.getFlightSegmentRefNumber() != null
							&& AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()
							&&	(EticketStatus.CHECKEDIN.code().equals(eticket.getTicketStatus())
									|| EticketStatus.BOARDED.code().equals(eticket.getTicketStatus()))){
						flownFlightSegmentList.add(Integer.parseInt(eticket.getFlightSegmentRefNumber()));
					}
				}
			}
		}

		return flownFlightSegmentList;
	}

	public static String getDepartureSegmentCode(List<FlightSegmentTO> flightSegmentTOs) {
		if (flightSegmentTOs != null && !flightSegmentTOs.isEmpty()) {
			Collections.sort(flightSegmentTOs);
			return flightSegmentTOs.iterator().next().getSegmentCode();
		}

		return null;
	}
	
	public static LCCClientReservationSegment getFirstFlightSeg(Set<LCCClientReservationSegment> setReservationSegments) {
		LCCClientReservationSegment[] segArray = SortUtil.sort(setReservationSegments);
		return segArray[0];
	}
	
	
	public static boolean isBulkTicketReservation(Reservation reservation) {
		if (reservation != null && reservation.getSegments() != null) {
			Set<ReservationSegment> resSegs = reservation.getSegments();
			for (ReservationSegment seg : resSegs) {
				try {
					if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(seg.getStatus())
							&& seg != null && seg.getFareTO(null) != null && seg.getFareTO(null).isBulkTicketFareRule()) {
						return true;
					}
				} catch (ModuleException e) {
					log.info(e);
				}
			}
		}
		return false;
	}
	
	public static boolean isAssociateInfantHasAmountDue(String adultTravelerRefNumber,
			Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {
		boolean associateInfantHasAmountDue = true;
		if (adultTravelerRefNumber != null && !"".equals(adultTravelerRefNumber) && passengerSummaryList != null
				&& !passengerSummaryList.isEmpty()) {
			int adultSequence = PaxTypeUtils.getPaxSeq(adultTravelerRefNumber);
			for (LCCClientPassengerSummaryTO lccClientPaxSummaryTO : passengerSummaryList) {
				if (lccClientPaxSummaryTO.getPaxType().equals(PaxTypeTO.INFANT)) {
					if (adultSequence == PaxTypeUtils.getParentSeq(lccClientPaxSummaryTO.getTravelerRefNumber())) {
						associateInfantHasAmountDue = (lccClientPaxSummaryTO.getTotalAmountDue() != null && (lccClientPaxSummaryTO
								.getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0));
						break;
					}
				}
			}
		}
		return associateInfantHasAmountDue;
	}

	public static boolean hasPaxAmountDue(String travelerRefNumber, Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {
		boolean hasPaxAmountDue = true;
		if (passengerSummaryList != null && !passengerSummaryList.isEmpty()) {
			for (LCCClientPassengerSummaryTO lccClientPaxSummaryTO : passengerSummaryList) {
				if (travelerRefNumber != null && travelerRefNumber.equals(lccClientPaxSummaryTO.getTravelerRefNumber())) {
					if (lccClientPaxSummaryTO.getTotalAmountDue() != null
							&& !(lccClientPaxSummaryTO.getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0)) {
						hasPaxAmountDue = false;
						break;
					}
				}
			}
		}
		return hasPaxAmountDue;
	}

	private static String
			getParentTravelerRefNumFromSeq(int paxSeq, Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {
		String parentTravelerRefNum = null;
		for (LCCClientPassengerSummaryTO paxSummaryTO : passengerSummaryList) {
			if (!PaxTypeTO.INFANT.equals(paxSummaryTO.getPaxType())
					&& paxSeq == PaxTypeUtils.getPaxSeq(paxSummaryTO.getTravelerRefNumber())) {
				parentTravelerRefNum = paxSummaryTO.getTravelerRefNumber();
			}
		}
		return parentTravelerRefNum;
	}

	public static boolean isOnlyInfantHasAmountDue(String infantTravelerRefNumber,
			Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {
		boolean isOnlyInfantHasAmountDue = false;
		if (passengerSummaryList != null && !passengerSummaryList.isEmpty()) {
			boolean isInfantHasAmountDue = hasPaxAmountDue(infantTravelerRefNumber, passengerSummaryList);
			int parentSeq = PaxTypeUtils.getParentSeq(infantTravelerRefNumber);
			String parentTravelerRefNum = getParentTravelerRefNumFromSeq(parentSeq, passengerSummaryList);
			boolean isAssignedAdultHasAmountDue = hasPaxAmountDue(parentTravelerRefNum, passengerSummaryList);
			isOnlyInfantHasAmountDue = isInfantHasAmountDue && !isAssignedAdultHasAmountDue;
		}
		return isOnlyInfantHasAmountDue;
	}

	public static final String TBA_USER = "T B A";

}
