package com.isa.thinair.webplatform.api.v2.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.FlightStopOverDisplayUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;

public class AvailableFlightInfo implements Comparable<AvailableFlightInfo> {

	private final String DATETIME_DISPLAY_FMT = "EEE ddMMMyy HH:mm";
	private final String DATETIME_PROCESSING_FMT = "yyMMddHHmm";

	private int index;
	private String ondCode;
	private List<String> segmentCodeList;
	private List<String> flightNoList;
	private List<String> carrierList;
	private List<String> csOcCarrierList;
	private List<String> departureList;
	private List<String> arrivalList;
	private List<String> depatureTimeList;
	private List<String> displayArrivalTime;
	private List<String> availabilityList;
	private List<String> departureZuluList;
	private List<String> arrivalTimeZuluList;
	private List<Boolean> domesticFlightList;
	private Collection<String> flightRPHList;
	private boolean isSelected = false;
	private boolean returnFlag = false;
	private boolean seatAvailable = true;
	private String system;
	private boolean isAvailableForGroundSegmentAdd;
	private Date firstDepatureDateTimeZulu;

	private List<String> flightModelNumberList;
	private List<String> flightModelDescriptionList;
	private List<String> flightDurationList;
	private List<String> remarksList;
	private List<String> flightStopOverDurationList;
	private List<String> noOfStopsList;
	private List<String> flightSegIdList;
	private List<String> availabilityByCOSList;
	private Map<String, Boolean> seatAvailableByCOSMap = new HashMap<String, Boolean>();
	private List<String> seatAvaiStatusByCOSList;
	private List<String> segmentFullNameList;
	private List<String> departureTerminalList;
	private List<String> arrivalTerminalList;
	private List<Boolean> waitListedList;
	private List<String> viaPointList;
	private List<String> requestedOndSequenceList;

	private boolean viewAvalableSeats = true;

	public AvailableFlightInfo() {
	}

	public AvailableFlightInfo(boolean viewAvalableSeats) {
		this.viewAvalableSeats = viewAvalableSeats;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public List<String> getSegmentCodeList() {
		if (segmentCodeList == null)
			segmentCodeList = new ArrayList<String>();
		return segmentCodeList;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public void addSegmentCode(String displaySegmentCode) {
		getSegmentCodeList().add(displaySegmentCode);
		if (ondCode == null) {
			ondCode = displaySegmentCode;
		} else {
			String transit = displaySegmentCode.substring(0, 3);
			if (ondCode.endsWith(transit)) {
				ondCode = ondCode + displaySegmentCode.substring(3);
			} else {
				ondCode = ondCode + "/" + displaySegmentCode;
			}
		}
	}

	public List<String> getFlightNoList() {
		if (flightNoList == null)
			flightNoList = new ArrayList<String>();
		return flightNoList;
	}

	public void addFlightNo(String getDisplayFlightNo) {
		getFlightNoList().add(getDisplayFlightNo);
	}

	public List<String> getCarrierList() {
		if (carrierList == null)
			carrierList = new ArrayList<String>();
		return carrierList;
	}

	public void addCarrier(String displayCarrier) {
		getCarrierList().add(displayCarrier);
	}

	public List<String> getDepatureList() {
		if (departureList == null)
			departureList = new ArrayList<String>();
		return departureList;
	}

	public void addDepature(String displayDepature) {
		getDepatureList().add(displayDepature);
	}

	public List<String> getArrivalList() {
		if (arrivalList == null)
			arrivalList = new ArrayList<String>();
		return arrivalList;
	}

	public void addArrival(String displayArrival) {
		getArrivalList().add(displayArrival);
	}

	public List<String> getAvailabilityList() {
		if (availabilityList == null)
			availabilityList = new ArrayList<String>();
		return availabilityList;
	}

	public void addAvailability(String displayAvailability) {
		getAvailabilityList().add(displayAvailability);
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Collection<String> getFlightRPHList() {
		if (flightRPHList == null)
			flightRPHList = new ArrayList<String>();
		return flightRPHList;
	}

	public void setFlightRPHList(Collection<String> flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public void addFlightRPH(String flightRPH) {
		getFlightRPHList().add(flightRPH);
	}

	public List<String> getDepatureTimeList() {
		if (depatureTimeList == null)
			depatureTimeList = new ArrayList<String>();
		return depatureTimeList;
	}

	public void addDepatureTime(String displayDepatureTime) {
		getDepatureTimeList().add(displayDepatureTime);
	}

	public List<String> getArrivalTimeList() {
		if (displayArrivalTime == null)
			displayArrivalTime = new ArrayList<String>();
		return displayArrivalTime;
	}

	public void addArrivalTime(String displayArrivalTime) {
		getArrivalTimeList().add(displayArrivalTime);
	}

	public List<String> getDepartureTimeZuluList() {
		if (this.departureZuluList == null)
			this.departureZuluList = new ArrayList<String>();
		return this.departureZuluList;
	}

	public void addDepartureTimeZulu(String timeZulu) {
		getDepartureTimeZuluList().add(timeZulu);
	}

	public List<String> getArrivalTimeZuluList() {
		if (this.arrivalTimeZuluList == null)
			this.arrivalTimeZuluList = new ArrayList<String>();
		return this.arrivalTimeZuluList;
	}

	public void addArrivalTimeZulu(String timeZulu) {
		getArrivalTimeZuluList().add(timeZulu);
	}

	public List<Boolean> getDomesticFlightList() {
		if (this.domesticFlightList == null)
			this.domesticFlightList = new ArrayList<Boolean>();
		return domesticFlightList;
	}

	public void addDomesticFlightList(Boolean domesticFlight) {
		getDomesticFlightList().add(domesticFlight);
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * @param system
	 *            the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	public boolean isSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(boolean seatAvailable) {
		this.seatAvailable = (this.seatAvailable && seatAvailable);
	}

	public boolean isAvailableForGroundSegmentAdd() {
		return isAvailableForGroundSegmentAdd;
	}

	public void setAvailableForGroundSegmentAdd(boolean isAvailableForGroundSegmentAdd) {
		this.isAvailableForGroundSegmentAdd = isAvailableForGroundSegmentAdd;
	}

	public void setFirstDepartureDate(Date firstDepatureDateTimeZulu) {
		this.firstDepatureDateTimeZulu = firstDepatureDateTimeZulu;
	}

	public Date getFirstDepartureDate() {
		return firstDepatureDateTimeZulu;
	}

	public List<String> getFlightModelNumberList() {
		if (flightModelNumberList == null)
			flightModelNumberList = new ArrayList<String>();

		return flightModelNumberList;
	}

	public void addFlightModelNumberList(String flightModelNumber) {
		getFlightModelNumberList().add(flightModelNumber);
	}

	public List<String> getFlightModelDescriptionList() {

		if (flightModelDescriptionList == null)
			flightModelDescriptionList = new ArrayList<String>();

		return flightModelDescriptionList;
	}

	public void addFlightModelDescriptionList(String flightModelDescription) {
		getFlightModelDescriptionList().add(flightModelDescription);
	}

	public List<String> getFlightDurationList() {
		if (flightDurationList == null)
			flightDurationList = new ArrayList<String>();

		return flightDurationList;
	}

	public void addFlightDurationList(String flightDuration) {
		getFlightDurationList().add(flightDuration);
	}

	public List<String> getRemarksList() {
		if (remarksList == null)
			remarksList = new ArrayList<String>();

		return remarksList;
	}

	public void addRemarksList(String remarks) {
		getRemarksList().add(remarks);
	}

	/**
	 * FIXME Actually this is transit duration list. Refactor the frontend and correct
	 */
	public List<String> getFlightStopOverDurationList() {
		if (flightStopOverDurationList == null)
			flightStopOverDurationList = new ArrayList<String>();

		return flightStopOverDurationList;
	}

	/**
	 * FIXME Actually this is transit duration list. Refactor the frontend and correct
	 */
	public void addFlightStopOverDurationList(String duration) {
		getFlightStopOverDurationList().add(duration);
	}

	public List<String> getNoOfStopsList() {

		if (noOfStopsList == null)
			noOfStopsList = new ArrayList<String>();

		return noOfStopsList;
	}

	public void addNoOfStopsList(String noOfStops) {
		getNoOfStopsList().add(noOfStops);
	}

	@Override
	public int compareTo(AvailableFlightInfo availFlightInfo) {
		if (firstDepatureDateTimeZulu.after(availFlightInfo.getFirstDepartureDate())) {
			int temp = availFlightInfo.getIndex();
			availFlightInfo.setIndex(getIndex());
			setIndex(temp);
		}
		return this.firstDepatureDateTimeZulu.compareTo(availFlightInfo.getFirstDepartureDate());
	}

	public void addFlightSegment(FlightSegmentTO flightSegmentTO) {

		int availableAdultCount = flightSegmentTO.getAdultCount() + flightSegmentTO.getChildCount();
		int availableInfantCount = flightSegmentTO.getInfantCount();
		// Setting available seats
		String strAdultPlus = "";
		String strInfantPlus = "";
		String strSeatCount = Integer.toString(availableAdultCount) + "/" + Integer.toString(availableInfantCount);

		if (!viewAvalableSeats) {
			if (availableAdultCount > AppSysParamsUtil.getShowSeatCount()) {
				availableAdultCount = AppSysParamsUtil.getShowSeatCount();
				strAdultPlus = "+";
			}

			if (availableInfantCount > AppSysParamsUtil.getShowSeatCount()) {
				availableInfantCount = AppSysParamsUtil.getShowSeatCount();
				strInfantPlus = "+";
			}
		}

		strSeatCount = Integer.toString(availableAdultCount) + strAdultPlus + "/" + Integer.toString(availableInfantCount)
				+ strInfantPlus;

		// TODO decide based on the system
		if (flightSegmentTO.getSeatAvailCount() != null) {
			strSeatCount = flightSegmentTO.getSeatAvailCount();
		}

		String fltDuration = flightSegmentTO.getFlightDuration();
		String fltModelDescription = flightSegmentTO.getFlightModelDescription();
		String fltModelNo = flightSegmentTO.getFlightModelNumber();
		String fltRemarks = flightSegmentTO.getRemarks();

		if (StringUtil.isNullOrEmpty(fltDuration)) {
			fltDuration = "-";
		}

		if (StringUtil.isNullOrEmpty(fltModelDescription)) {
			fltModelDescription = "-";
		}

		if (StringUtil.isNullOrEmpty(fltModelNo)) {
			fltModelNo = "-";
		}

		if (StringUtil.isNullOrEmpty(fltRemarks)) {
			fltRemarks = "-";
		}

		if (this.firstDepatureDateTimeZulu == null) {
			this.firstDepatureDateTimeZulu = flightSegmentTO.getDepartureDateTimeZulu();
		}

		String fltDepartureTerminalName = flightSegmentTO.getDepartureTerminalName();
		String fltArrivalTerminalName = flightSegmentTO.getArrivalTerminalName();

		if (StringUtil.isNullOrEmpty(fltDepartureTerminalName)) {
			fltDepartureTerminalName = "-";
		}

		if (StringUtil.isNullOrEmpty(fltArrivalTerminalName)) {
			fltArrivalTerminalName = "-";
		}

		addDepartureTerminalList(fltDepartureTerminalName);
		addArrivalTerminalList(fltArrivalTerminalName);
		addSegmentCode(flightSegmentTO.getSegmentCode());
		addFlightNo(flightSegmentTO.getFlightNumber());
		addCarrier(flightSegmentTO.getOperatingAirline());
		addCsOcCarrier(flightSegmentTO.getCsOcCarrierCode());
		addDepature(DateUtil.formatDate(flightSegmentTO.getDepartureDateTime(), DATETIME_DISPLAY_FMT));
		addArrival(DateUtil.formatDate(flightSegmentTO.getArrivalDateTime(), DATETIME_DISPLAY_FMT));
		addDepatureTime(DateUtil.formatDate(flightSegmentTO.getDepartureDateTime(), DATETIME_PROCESSING_FMT));
		addArrivalTime(DateUtil.formatDate(flightSegmentTO.getArrivalDateTime(), DATETIME_PROCESSING_FMT));
		addFlightRPH(flightSegmentTO.getFlightRefNumber()
				+ (flightSegmentTO.getRouteRefNumber() != null ? "#" + flightSegmentTO.getRouteRefNumber() : ""));
		addDepartureTimeZulu(DateUtil.formatDate(flightSegmentTO.getDepartureDateTimeZulu(), DATETIME_PROCESSING_FMT));
		addArrivalTimeZulu(DateUtil.formatDate(flightSegmentTO.getArrivalDateTimeZulu(), DATETIME_PROCESSING_FMT));
		addDomesticFlightList(flightSegmentTO.isDomesticFlight());
		addAvailability(strSeatCount);
		addFlightDurationList(fltDuration);
		addFlightModelDescriptionList(fltModelDescription);
		addFlightModelNumberList(fltModelNo);
		addRemarksList(fltRemarks);
		addNoOfStopsList(FlightStopOverDisplayUtil.getStopOverStations(flightSegmentTO.getSegmentCode()));
		addFlightSegId(flightSegmentTO.getFlightSegId().toString());
		addSegmentFullName(ReservationBeanUtil.getONDDetails(flightSegmentTO.getSegmentCode()));
		addWaitListedStatus(flightSegmentTO.isWaitListed());
		addRequestedOndSequence(flightSegmentTO.getRequestedOndSequence() + "");

	}

	public void setLogicalCabinClassAvailabilityInfo(FlightSegmentTO flightSegmentTO, List<String> availableLogicalCabClassList,
			int adultCount, int infantCount, String bookingType) {
		// "FLT_SEG_ID$COS,AD,IN,AVAIL_STATUS#"
		Iterator<String> availableLogCabClassItr = availableLogicalCabClassList.iterator();
		String formatStr = "";
		int i = 0;
		while (availableLogCabClassItr.hasNext()) {
			String availableLogCabClass = availableLogCabClassItr.next();
			Integer adultAvailableSeatCount = 0;
			Integer infantAvailableSeatCount = 0;
			boolean isSeatsAvailable = false;
			boolean isSeatsAvaiInSameLogCabClass = false;
			String strAdultPlus = "";
			String strInfantPlus = "";
			String strSeatCount = "";

			if (flightSegmentTO.getAvailableAdultCountMap().keySet().contains(availableLogCabClass)) {
				adultAvailableSeatCount = flightSegmentTO.getAvailableAdultCountMap().get(availableLogCabClass);
			}

			if (flightSegmentTO.getAvailableInfantCountMap().keySet().contains(availableLogCabClass)) {
				infantAvailableSeatCount = flightSegmentTO.getAvailableInfantCountMap().get(availableLogCabClass);
			}

			if (BookingClass.BookingClassType.OVERBOOK.equals(bookingType)
					|| BookingClass.BookingClassType.STANDBY.equals(bookingType)) {
				// when booking type is OVERBOOK, only when cabin class is not defined availability is false,all the
				// other cases availability should true
				if (flightSegmentTO.getAvailableAdultCountMap().keySet().contains(availableLogCabClass)) {
					isSeatsAvailable = true;
				}
			} else {
				if (adultAvailableSeatCount >= adultCount && infantAvailableSeatCount >= infantCount) {
					isSeatsAvailable = true;
				}
			}

			isSeatsAvaiInSameLogCabClass = isSeatsAvailable;

			if (seatAvailableByCOSMap != null && seatAvailableByCOSMap.containsKey(availableLogCabClass)) {
				Boolean existFlag = seatAvailableByCOSMap.get(availableLogCabClass);
				isSeatsAvaiInSameLogCabClass = (isSeatsAvaiInSameLogCabClass && existFlag.booleanValue());
			}

			seatAvailableByCOSMap.put(availableLogCabClass, new Boolean(isSeatsAvaiInSameLogCabClass));

			if (i > 0) {
				formatStr += "#";
			}

			// available seat count cabin-class level
			if (!viewAvalableSeats) {
				if (adultAvailableSeatCount > AppSysParamsUtil.getShowSeatCount()) {
					adultAvailableSeatCount = AppSysParamsUtil.getShowSeatCount();
					strAdultPlus = "+";
				}

				if (infantAvailableSeatCount > AppSysParamsUtil.getShowSeatCount()) {
					infantAvailableSeatCount = AppSysParamsUtil.getShowSeatCount();
					strInfantPlus = "+";
				}
			}

			strSeatCount = Integer.toString(adultAvailableSeatCount) + strAdultPlus + "/"
					+ Integer.toString(infantAvailableSeatCount) + strInfantPlus;

			formatStr += availableLogCabClass + "," + adultAvailableSeatCount + "," + infantAvailableSeatCount + ","
					+ isSeatsAvailable + "," + strSeatCount;

			i++;
		}
		String flightAvailabilityByCOS = flightSegmentTO.getFlightSegId() + "$" + formatStr;
		addAvailabilityByCOS(flightAvailabilityByCOS);
	}

	public List<String> getAvailabilityByCOSList() {
		if (availabilityByCOSList == null)
			availabilityByCOSList = new ArrayList<String>();

		return availabilityByCOSList;
	}

	public void addAvailabilityByCOS(String displayAvailability) {
		getAvailabilityByCOSList().add(displayAvailability);
	}

	public List<String> getFlightSegIdList() {
		if (flightSegIdList == null)
			flightSegIdList = new ArrayList<String>();

		return flightSegIdList;
	}

	public void addFlightSegId(String fltSegId) {
		getFlightSegIdList().add(fltSegId);
	}

	public void setSeatAvaiStatusByCOSList(List<String> ondWiseAvailableCOSList) {

		if (seatAvaiStatusByCOSList == null)
			seatAvaiStatusByCOSList = new ArrayList<String>();

		if (seatAvailableByCOSMap != null && seatAvailableByCOSMap.size() > 0) {
			for (String logCabClass : seatAvailableByCOSMap.keySet()) {
				String seatAvailStatus = logCabClass + "#" + seatAvailableByCOSMap.get(logCabClass);

				if (ondWiseAvailableCOSList != null && !ondWiseAvailableCOSList.contains(logCabClass)
						&& seatAvailableByCOSMap.get(logCabClass) != null && seatAvailableByCOSMap.get(logCabClass)) {
					ondWiseAvailableCOSList.add(logCabClass);
				}

				seatAvaiStatusByCOSList.add(seatAvailStatus);
			}
		}

	}

	public List<String> getSeatAvaiStatusByCOSList() {
		return seatAvaiStatusByCOSList;
	}

	public List<String> getSegmentFullNameList() {
		if (segmentFullNameList == null)
			segmentFullNameList = new ArrayList<String>();
		return segmentFullNameList;
	}

	public void addSegmentFullName(String segmentFullName) {
		getSegmentFullNameList().add(segmentFullName);

	}

	public List<Boolean> getWaitListedList() {
		if (waitListedList == null)
			waitListedList = new ArrayList<Boolean>();
		return waitListedList;
	}

	public void addWaitListedStatus(Boolean waitListedStatus) {
		getWaitListedList().add(waitListedStatus);
	}

	public List<String> getDepartureTerminal() {
		if (departureTerminalList == null)
			departureTerminalList = new ArrayList<String>();

		return departureTerminalList;
	}

	public void addDepartureTerminalList(String departureTerminal) {
		getDepartureTerminal().add(departureTerminal);
	}

	public List<String> getArrivalTerminal() {
		if (arrivalTerminalList == null)
			arrivalTerminalList = new ArrayList<String>();

		return arrivalTerminalList;
	}

	public void addArrivalTerminalList(String arrivalTerminal) {
		getArrivalTerminal().add(arrivalTerminal);
	}

	public List<String> getViaPointList() {
		if (viaPointList == null)
			viaPointList = new ArrayList<String>();

		return viaPointList;
	}

	public void setViaPointList(String viaPoint) {
		getViaPointList().add(viaPoint);
	}

	public List<String> getRequestedOndSequenceList() {
		if (requestedOndSequenceList == null)
			requestedOndSequenceList = new ArrayList<String>();

		return requestedOndSequenceList;
	}

	private void addRequestedOndSequence(String requestedOndSequence) {
		getViaPointList().add(requestedOndSequence);
	}

	public List<String> getCsOcCarrierList() {
		if (csOcCarrierList == null)
			csOcCarrierList = new ArrayList<String>();
		return csOcCarrierList;
	}

	public void addCsOcCarrier(String displayCarrier) {
		getCsOcCarrierList().add(displayCarrier);
	}

}
