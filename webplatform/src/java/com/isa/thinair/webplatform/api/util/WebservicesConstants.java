/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Constants shared across the webservices module
 */
public interface WebservicesConstants {

	public static final String NEWLINE = System.getProperty("line.separator");

	public static final String PHONE_NUMBER_SEPERATOR = "-";

	public static SimpleDateFormat DATE_FORMAT_FOR_LOGGING = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public static final String DEFAULT_PAX_CATEGORY = "A";

	public interface OTAConstants {

		public static interface CardTypes {
			public static final String CREDIT = "1";
			public static final String DEBIT = "2";
			public static final String CENTRAL_BILL = "3";
		}

		/** Default Langugae */
		public static final String PRIMARY_LANG_CODE = "en-us";

		/** Decimals in currency values */
		public static final BigInteger DECIMALS_AED = new BigInteger("2");

		/** Default decimals truncation for BigDecimal input */
		public DecimalFormat REFAULT_DECIMAL_FORMAT = new DecimalFormat(".####");

		/** Versions */
		public static final BigDecimal VERSION_Ping = new BigDecimal("20061.00"); // format
																					// :
																					// ota
																					// spec
																					// version.internal
																					// version
		public static final BigDecimal VERSION_AirAvail = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AirPrice = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_RetrieveRes = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_RetrieveResList = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_ResSearch = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_Book = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AirBookModify = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AgentAvailCredit = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_PaxCreditReq = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AirScheudle = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AirFltInfo = new BigDecimal("20061.00");
	}

}
