package com.isa.thinair.webplatform.api.v2.reservation;

import java.util.Collection;

public class FareQuoteTO {

	private String ondCode;

	private int ondSequence;

	private String fullOndCode;

	private Collection<PaxFareTO> paxWise;

	private String totalPrice;

	private Collection<SegmentFareTO> segmentWise;

	private int fareType;

	private int fareId;	

	public String getOndCode() {
		return ondCode;
	}

	public String getFullOndCode() {
		if (fullOndCode != null) {
			return fullOndCode;
		}
		return getOndCode();
	}

	public void setOndCode(String ondCode) {
		setOndCode(ondCode, true);
	}

	public void setOndCode(String ondCode, boolean isFullOndCode) {
		this.ondCode = ondCode;
		if (isFullOndCode) {
			this.fullOndCode = ondCode;
		}
	}

	/**
	 * @return the ondSequence
	 */
	public int getOndSequence() {
		return ondSequence;
	}

	/**
	 * @param ondSequence
	 *            the ondSequence to set
	 */
	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	/**
	 * @return the paxWise
	 */
	public Collection<PaxFareTO> getPaxWise() {
		return paxWise;
	}

	/**
	 * @param paxWise
	 *            the paxWise to set
	 */
	public void setPaxWise(Collection<PaxFareTO> paxWise) {
		this.paxWise = paxWise;
	}

	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the segmentWise
	 */
	public Collection<SegmentFareTO> getSegmentWise() {
		return segmentWise;
	}

	/**
	 * @param segmentWise
	 *            the segmentWise to set
	 */
	public void setSegmentWise(Collection<SegmentFareTO> segmentWise) {
		this.segmentWise = segmentWise;
	}

	public int getFareType() {
		return fareType;
	}

	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}	
}
