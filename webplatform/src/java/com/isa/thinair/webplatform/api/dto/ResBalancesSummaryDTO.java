package com.isa.thinair.webplatform.api.dto;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Wraps OND summary and PAX summary info for reservation alterations.
 */
public class ResBalancesSummaryDTO {

	private SegmentSummeryDTO segmentSummeryDTO;

	private Collection<PAXSummeryDTO> paxSummaryDTOCol;

	private BigDecimal totalCnxChargeForCurrentAlt = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalModChargeForCurrentAlt = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAvailableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

	public Collection<PAXSummeryDTO> getPaxSummaryDTOCol() {
		return paxSummaryDTOCol;
	}

	public void setPaxSummaryDTOCol(Collection<PAXSummeryDTO> paxSummaryDTOCol) {
		this.paxSummaryDTOCol = paxSummaryDTOCol;
	}

	public SegmentSummeryDTO getSegmentSummeryDTO() {
		return segmentSummeryDTO;
	}

	public void setSegmentSummeryDTO(SegmentSummeryDTO segmentSummeryDTO) {
		this.segmentSummeryDTO = segmentSummeryDTO;
	}

	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public BigDecimal getTotalAvailableBalance() {
		return totalAvailableBalance;
	}

	public void setTotalAvailableBalance(BigDecimal totalAvailableBalance) {
		this.totalAvailableBalance = totalAvailableBalance;
	}

	public BigDecimal getTotalCnxChargeForCurrentAlt() {
		return totalCnxChargeForCurrentAlt;
	}

	public void setTotalCnxChargeForCurrentAlt(BigDecimal totalCnxChargeForCurrentAlt) {
		this.totalCnxChargeForCurrentAlt = totalCnxChargeForCurrentAlt;
	}

	public BigDecimal getTotalModChargeForCurrentAlt() {
		return totalModChargeForCurrentAlt;
	}

	public void setTotalModChargeForCurrentAlt(BigDecimal totalModChargeForCurrentAlt) {
		this.totalModChargeForCurrentAlt = totalModChargeForCurrentAlt;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

}
