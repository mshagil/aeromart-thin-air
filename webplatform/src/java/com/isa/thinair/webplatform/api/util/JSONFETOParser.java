package com.isa.thinair.webplatform.api.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.webplatform.api.util.annotations.AnnotationValidator;

/**
 * <h1>
 * Front-end Object Parser.</h1>
 * <p>
 * Will de-serialize the given JSON string to a required DTO
 * 
 * </p>
 * <p>
 * use org.apache.struts2.json
 * </p>
 * 
 * 
 * @author Dilan Anuruddha
 * @param
 */
public class JSONFETOParser {

	private static final String JSON_DATE_FMT = "yyyy-MM-dd'T'HH:mm:ss";

	@SuppressWarnings("rawtypes")
	private static Map<Class, Class> interfaceImplMap = new HashMap<Class, Class>();

	private static AnnotationValidator validator = null;

	static {
		interfaceImplMap.put(Collection.class, ArrayList.class);
		interfaceImplMap.put(List.class, ArrayList.class);
		interfaceImplMap.put(Map.class, HashMap.class);
		interfaceImplMap.put(Set.class, HashSet.class);
		validator = new AnnotationValidator();
	}

	private static Log log = LogFactory.getLog(JSONFETOParser.class);

	/**
	 * Use this method if the target object is not a java collection type If it's collection type refer
	 * {@link #parseCollection(Class, Class, String) parse}
	 * 
	 * @param <E>
	 * 
	 * 
	 * 
	 * @param typeOfClass
	 *            (Implementation/Concrete class required)
	 * @param jsonString
	 * @return
	 */
	public static <E> E parse(Class<E> typeOfClass, String jsonString) {
		Object t = null;
		try {
			if (jsonString != null) {
				Object obj = JSONUtil.deserialize(jsonString);
				t = parse(typeOfClass, null, obj);
			}
		} catch (JSONException e) {
			log.error("Error parsing JSON ", e);
		}
		return typeOfClass.cast(t);
	}

	public static Date parseDate(String jsonString) {
		if (jsonString == null || "".equals(jsonString.trim()) || "null".equals(jsonString.trim())) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(JSON_DATE_FMT);
		try {
			return sdf.parse(jsonString);
		} catch (ParseException e) {
			log.error("Date parse Exception ", e);
			return null;
		}
	}

	private static Object invokeValidator(String obj, Method invM) {
		if (validator != null && invM != null && obj != null) {
			return validator.getValidatedValue(obj, invM);
		}
		return obj;
	}

	/**
	 * use this method if the target object is a java collection type
	 * 
	 * @param <K>
	 * 
	 * @param typeOfClass
	 *            (List/Collection/ArrayList etc)
	 * @param collTypeClass
	 *            (Type of Collection class e.g. Reservation.class) <br />
	 *            (Implementation/Concrete class required)
	 * @param jsonString
	 * @return
	 */
	public static <E extends Collection<K>, K> E parseCollection(Class<E> typeOfClass, Class<K> collType, String jsonString) {
		Object t = null;
		try {
			if (jsonString != null && !jsonString.trim().equals("")) {
				Object obj = JSONUtil.deserialize(jsonString);
				t = parse(typeOfClass, new Class[] { collType }, obj);
			}
		} catch (JSONException e) {
			log.error("Error parsing JSON ", e);
		}
		return typeOfClass.cast(t);
	}

	/**
	 * use this method if the target object is a java map type
	 * 
	 * @param typeOfClass
	 * @param valueType
	 * @param jsonString
	 * @return
	 */
	public static <E extends Map<String, V>, K, V> E parseMap(Class<E> typeOfClass, Class<K> valueType, String jsonString) {
		Object t = null;
		try {
			if (jsonString != null && !jsonString.trim().equals("")) {
				Object obj = JSONUtil.deserialize(jsonString);
				t = parse(typeOfClass, new Class[] { valueType }, obj);
			}
		} catch (JSONException e) {
			log.error("Error parsing JSON ", e);
		}
		return typeOfClass.cast(t);
	}

	/**
	 * Return the object of type T with populated data from Object obj
	 * 
	 * @param classT
	 *            - Type of class that need to be populated
	 * @param typeOfT
	 *            - in case of classT being a collection, this will denote the type of variables in collection
	 * @param obj
	 *            - JSON parsed object of data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static <E> E parse(Class<E> classT, Type[] typeOfT, Object obj) {
		Object t = null;
		if (obj == null) {
			return null;
		}
		try {
			classT = getImplClass(classT);
			t = classT.newInstance();
			if (isMapImp(classT)) {
				Method m = null;
				for (Method m1 : classT.getMethods()) {
					if (m1.getName().startsWith("put")) {
						m = m1;
						break;
					}
				}
				Map<String, Object> map = (Map<String, Object>) obj;
				for (String key : map.keySet()) {
					log.debug("key=" + key + ", value=" + map.get(key));
					if (typeOfT == null) {
						m.invoke(t, key, getValue(map.get(key), m.getParameterTypes()[0], null, m));
					} else {
						if (typeOfT.length == 1) {
							m.invoke(t, key, getValue(map.get(key), (Class<?>) typeOfT[0], null, m));
						} else if (typeOfT.length > 1) {
							m.invoke(t, key, getValue(map.get(key), (Class<?>) typeOfT[1], null, m));
						}
					}
				}
			} else if (isCollectionImp(classT)) {
				Method m = null;
				for (Method m1 : classT.getMethods()) {
					if (m1.getName().startsWith("add") && m1.getParameterTypes().length == 1) {
						m = m1;
						break;
					}
				}
				List<Object> list = (List<Object>) obj;
				for (Object val : list) {
					log.debug("value=" + val);
					if (typeOfT == null) {
						m.invoke(t, getValue(val, m.getParameterTypes()[0], null, m));
					} else {
						m.invoke(t, getValue(val, (Class<?>) typeOfT[0], null, m));
					}
				}
			} else {
				Map<String, Object> map = (Map<String, Object>) obj;
				for (Method m : classT.getMethods()) {
					if (m.getName().startsWith("set") && map.containsKey(variableName(m.getName()))) {
						log.debug("m.getName()=" + m.getName() + ", value=" + map.get(variableName(m.getName())));
						Type[] genericClasses = getGenericClasses(m, 0);
						try {
							if (typeOfT == null) {
								m.invoke(t, getValue(map.get(variableName(m.getName())), m.getParameterTypes()[0], genericClasses, m));
							} else {
								m.invoke(t, getValue(map.get(variableName(m.getName())), (Class<?>) typeOfT[0], genericClasses, m));
							}
						} catch (IllegalArgumentException e) {
							log.error("Illegal arguments ", e);
						}						
					}
				}
			}
		} catch (InstantiationException e) {
			log.error("Instantion failed", e);
		} catch (IllegalAccessException e) {
			log.error("Illegal Access ", e);
		} catch (IllegalArgumentException e) {
			log.error("Illegal arguments ", e);
		} catch (InvocationTargetException e) {
			log.error("Invocation Target Exception ", e);
		} catch (JSONException e) {
			log.error("JSON Exception ", e);
		}
		return classT.cast(t);
	}

	/**
	 * Return the first Generic argument of the given argument position of the given method.
	 * 
	 * @param m
	 * @param atArgPosition
	 * @return
	 */
	@SuppressWarnings({ "unused", "rawtypes" })
	private static Class getGenericClass(Method m, int atArgPosition) {
		return getGenericClass(m, atArgPosition, 0);
	}

	/**
	 * Return the list of generics of given argument position for the given method
	 * 
	 * @param m
	 * @param atArgPosition
	 * @return
	 */
	private static Type[] getGenericClasses(Method m, int atArgPosition) {
		if (m.getGenericParameterTypes() != null && m.getGenericParameterTypes().length > atArgPosition) {
			if (m.getGenericParameterTypes()[atArgPosition] instanceof ParameterizedType) {
				ParameterizedType paramType = ((ParameterizedType) m.getGenericParameterTypes()[atArgPosition]);
				if (paramType.getActualTypeArguments().length > 0) {
					if (paramType.getActualTypeArguments()[0] instanceof Class) {
						return (Type[]) paramType.getActualTypeArguments();
					}
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @param m
	 * @param atArgPosition
	 * @param genPoition
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private static Class getGenericClass(Method m, int atArgPosition, int genPoition) {
		Type classes[] = getGenericClasses(m, atArgPosition);
		if (classes != null && classes.length > genPoition) {
			return (Class) classes[genPoition];
		}
		return null;
	}

	/**
	 * Check if the class is Map Implementation
	 * 
	 * @param classIn
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private static boolean isMapImp(Class classIn) {
		for (Class intf : classIn.getInterfaces()) {
			if (intf.getCanonicalName().endsWith(Map.class.getCanonicalName())) {
				return true;
			} else if (isMapImp(intf)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if the class is a collection implementation
	 * 
	 * @param classIn
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private static boolean isCollectionImp(Class classIn) {
		for (Class intf : classIn.getInterfaces()) {
			if (intf.getCanonicalName().endsWith(Collection.class.getCanonicalName())) {
				return true;
			} else if (isCollectionImp(intf)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param obj
	 * @param parameterClass
	 * @param invM
	 * @return
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws JSONException
	 */
	@SuppressWarnings({ "unchecked" })
	private static Object getValue(Object obj, Class<?> parameterClass, Type typeClass[], Method invM)
			throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException,
			JSONException {

		if (obj == null)
			return null;

		if (compare(parameterClass, Date.class)) {
			SimpleDateFormat sdf = new SimpleDateFormat(JSON_DATE_FMT);
			try {
				Object tm = invokeValidator(obj.toString(), invM);
				if (tm == null)
					return tm;
				return sdf.parse(tm.toString());
			} catch (ParseException e) {
				log.error("Date parse Exception ", e);
				return null;
			}
		} else if (compare(parameterClass, Calendar.class)) {
			SimpleDateFormat sdf = new SimpleDateFormat(JSON_DATE_FMT);
			try {
				Object tm = invokeValidator(obj.toString(), invM);
				if (tm == null)
					return tm;
				Calendar cal = Calendar.getInstance();
				cal.setTime(sdf.parse(tm.toString()));
				return cal;
			} catch (ParseException e) {
				log.error("Date parse Exception ", e);
				return null;
			}
		} else if (compare(parameterClass, Integer.class)) {
			if (obj instanceof Integer) {
				return obj;
			} else if (obj instanceof Long) {
				return ((Long) obj).intValue();
			}
			Object tm = invokeValidator(obj.toString(), invM);
			if (tm == null)
				return tm;
			return Integer.parseInt(tm.toString());
		} else if (compare(parameterClass, Double.class)) {
			if (obj instanceof Double) {
				return obj;
			}
			Object tm = invokeValidator(obj.toString(), invM);
			if (tm == null)
				return tm;
			return Double.parseDouble(tm.toString());
		} else if (compare(parameterClass, BigDecimal.class)) {
			if (obj instanceof BigDecimal) {
				return obj;
			}
			Object tm = invokeValidator(obj.toString(), invM);
			if (tm == null)
				return tm;
			return new BigDecimal(tm.toString());
		} else if (compare(parameterClass, Float.class)) {
			if (obj instanceof Float) {
				return obj;
			}
			Object tm = invokeValidator(obj.toString(), invM);
			if (tm == null)
				return tm;
			return Float.parseFloat(tm.toString());
		} else if (compare(parameterClass, Long.class)) {
			if (obj instanceof Integer || obj instanceof Long) {
				return obj;
			}
			Object tm = invokeValidator(obj.toString(), invM);
			if (tm == null)
				return tm;
			return Long.parseLong(tm.toString());
		} else if (compare(parameterClass, String.class)) {
			Object tm = invokeValidator(obj.toString(), invM);
			if (tm == null)
				return tm;
			return tm.toString();
		}

		if (!parameterClass.isPrimitive() && !parameterClass.getPackage().getName().equals("java.lang")
				&& !parameterClass.getPackage().getName().equals("java.math")) {
			return JSONFETOParser.parse(getImplClass(parameterClass), typeClass, obj);
		}
		return obj;
	}

	/**
	 * If interfaces classes is parsed, and in configuration if an implementation default class is provided it will be
	 * return otherwise the interface class it self will be returned
	 * 
	 * @param <T>
	 * 
	 * @param <T>
	 * 
	 * @param interfaceClass
	 * @return
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private static <T> Class getImplClass(Class<T> interfaceClass) {
		if (interfaceClass.isInterface() && interfaceImplMap.containsKey(interfaceClass)) {
			return interfaceImplMap.get(interfaceClass);
		}
		return interfaceClass;
	}

	/**
	 * Return matching boxer classes from primitives
	 * 
	 * @param primitiveStrName
	 * @return
	 */
	private static String getMatchingBoxerClass(String primitiveStrName) {
		if (primitiveStrName.equals(int.class.getCanonicalName())) {
			return Integer.class.getCanonicalName();
		} else if (primitiveStrName.equals(long.class.getCanonicalName())) {
			return Long.class.getCanonicalName();
		} else if (primitiveStrName.equals(short.class.getCanonicalName())) {
			return Short.class.getCanonicalName();
		} else if (primitiveStrName.equals(float.class.getCanonicalName())) {
			return Float.class.getCanonicalName();
		} else if (primitiveStrName.equals(double.class.getCanonicalName())) {
			return Double.class.getCanonicalName();
		} else if (primitiveStrName.equals(char.class.getCanonicalName())) {
			return Character.class.getCanonicalName();
		}
		return primitiveStrName;
	}

	/**
	 * Compare and find if the classes are of the same type In case of primitives, matching with equivalent Boxer
	 * classes are considered
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private static boolean compare(Class first, Class second) {
		String str1 = null;
		String str2 = null;
		if (first.isPrimitive()) {
			str1 = getMatchingBoxerClass(first.getName());
		} else {
			str1 = first.getCanonicalName();
		}

		if (second.isPrimitive()) {
			str2 = getMatchingBoxerClass(second.getName());
		} else {
			str2 = second.getCanonicalName();
		}
		return str1.equals(str2);
	}

	/**
	 * Derive a bean standard variable name from a method name
	 * 
	 * @param methodName
	 * @return variable Name
	 */
	private static String variableName(String methodName) {
		if (methodName.startsWith("set")) {
			methodName = methodName.substring(3);
		} else if (methodName.startsWith("is")) {
			methodName = methodName.substring(2);
		} else if (methodName.startsWith("get")) {
			methodName = methodName.substring(3);
		}
		methodName = methodName.substring(0, 1).toLowerCase() + methodName.substring(1);
		return methodName;
	}

	/**
	 * Run your test cases here
	 * 
	 * @param args
	 * @throws JSONException
	 */
	public static void main(String args[]) throws JSONException {
		List<String> list = new ArrayList<String>();
		list.add("Dilan");
		list.add("Anuruddha");
		String jsonStr = JSONUtil.serialize(list);
		System.out.println(jsonStr);

		@SuppressWarnings("unchecked")
		List<String> deList = JSONFETOParser.parseCollection(ArrayList.class, String.class, jsonStr);
		System.out.println(deList);
	}
}