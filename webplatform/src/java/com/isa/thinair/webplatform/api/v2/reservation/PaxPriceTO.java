package com.isa.thinair.webplatform.api.v2.reservation;

public class PaxPriceTO {

	private String paxType;

	private String paxTotalPrice;

	private String paxTotalPriceWithInfant;

	private String paxTotalPriceInSelectedCurr;

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the paxTotalPrice
	 */
	public String getPaxTotalPrice() {
		return paxTotalPrice;
	}

	/**
	 * @param paxTotalPrice
	 *            the paxTotalPrice to set
	 */
	public void setPaxTotalPrice(String paxTotalPrice) {
		this.paxTotalPrice = paxTotalPrice;
	}

	/**
	 * @return the paxTotalPriceWithInfant
	 */
	public String getPaxTotalPriceWithInfant() {
		return paxTotalPriceWithInfant;
	}

	/**
	 * @param paxTotalPriceWithInfant
	 *            the paxTotalPriceWithInfant to set
	 */
	public void setPaxTotalPriceWithInfant(String paxTotalPriceWithInfant) {
		this.paxTotalPriceWithInfant = paxTotalPriceWithInfant;
	}

	/**
	 * @return the paxTotalPriceInSelectedCurr
	 */
	public String getPaxTotalPriceInSelectedCurr() {
		return paxTotalPriceInSelectedCurr;
	}

	/**
	 * @param paxTotalPriceInSelectedCurr
	 *            the paxTotalPriceInSelectedCurr to set
	 */
	public void setPaxTotalPriceInSelectedCurr(String paxTotalPriceInSelectedCurr) {
		this.paxTotalPriceInSelectedCurr = paxTotalPriceInSelectedCurr;
	}

}
