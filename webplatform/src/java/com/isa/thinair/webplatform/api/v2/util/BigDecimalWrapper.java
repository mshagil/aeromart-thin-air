package com.isa.thinair.webplatform.api.v2.util;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class BigDecimalWrapper {

	private BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();

	public BigDecimal getValue() {
		return value;
	}

	public void add(BigDecimal addition) {
		value = AccelAeroCalculator.add(value, addition);
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getDisplayValue() {
		return AccelAeroCalculator.formatAsDecimal(this.value);
	}

	@Override
	public String toString() {
		return value.toString();
	}
}
