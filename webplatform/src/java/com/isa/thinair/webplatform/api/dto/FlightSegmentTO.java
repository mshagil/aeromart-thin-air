package com.isa.thinair.webplatform.api.dto;

public class FlightSegmentTO {

	private int segmentId;

	private String segmentCode;

	private boolean validFlag;

	private int overlapSegmentId;

	/**
	 * @return the segmentId
	 */
	public int getSegmentId() {
		return segmentId;
	}

	/**
	 * @param segmentId
	 *            the segmentId to set
	 */
	public void setSegmentId(int segmentId) {
		this.segmentId = segmentId;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the validFlag
	 */
	public boolean getValidFlag() {
		return validFlag;
	}

	/**
	 * @param validFlag
	 *            the validFlag to set
	 */
	public void setValidFlag(boolean validFlag) {
		this.validFlag = validFlag;
	}

	/**
	 * @return the overlapSegmentId
	 */
	public int getOverlapSegmentId() {
		return overlapSegmentId;
	}

	/**
	 * @param overlapSegmentId
	 *            the overlapSegmentId to set
	 */
	public void setOverlapSegmentId(int overlapSegmentId) {
		this.overlapSegmentId = overlapSegmentId;
	}

}