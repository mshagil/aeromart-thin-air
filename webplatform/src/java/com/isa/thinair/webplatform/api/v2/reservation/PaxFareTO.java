package com.isa.thinair.webplatform.api.v2.reservation;

public class PaxFareTO {
	private String paxType;

	private String fare;

	private String totalPaxFare;

	private String tax;

	private String sur;

	private String perPax;

	private String noPax;

	private String total;

	private String other;

	private String selCurtotal;

	private String selCurTax;

	private String selCurSur;

	private String selCurFare;

	private String selCurTotalPaxFare;

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the fare
	 */
	public String getFare() {
		return fare;
	}

	/**
	 * @param fare
	 *            the fare to set
	 */
	public void setFare(String fare) {
		this.fare = fare;
	}

	/**
	 * @return the tax
	 */
	public String getTax() {
		return tax;
	}

	/**
	 * @param tax
	 *            the tax to set
	 */
	public void setTax(String tax) {
		this.tax = tax;
	}

	/**
	 * @return the sur
	 */
	public String getSur() {
		return sur;
	}

	/**
	 * @param sur
	 *            the sur to set
	 */
	public void setSur(String sur) {
		this.sur = sur;
	}

	/**
	 * @return the perPax
	 */
	public String getPerPax() {
		return perPax;
	}

	/**
	 * @param perPax
	 *            the perPax to set
	 */
	public void setPerPax(String perPax) {
		this.perPax = perPax;
	}

	/**
	 * @return the noPax
	 */
	public String getNoPax() {
		return noPax;
	}

	/**
	 * @param noPax
	 *            the noPax to set
	 */
	public void setNoPax(String noPax) {
		this.noPax = noPax;
	}

	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other
	 *            the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}

	/**
	 * @return the selCurtotal
	 */
	public String getSelCurtotal() {
		return selCurtotal;
	}

	/**
	 * @param selCurtotal
	 *            the selCurtotal to set
	 */
	public void setSelCurtotal(String selCurtotal) {
		this.selCurtotal = selCurtotal;
	}

	public String getSelCurTax() {
		return selCurTax;
	}

	public void setSelCurTax(String selCurTax) {
		this.selCurTax = selCurTax;
	}

	public String getSelCurSur() {
		return selCurSur;
	}

	public void setSelCurSur(String selCurSur) {
		this.selCurSur = selCurSur;
	}

	public String getSelCurFare() {
		return selCurFare;
	}

	public void setSelCurFare(String selCurFare) {
		this.selCurFare = selCurFare;
	}

	public String getTotalPaxFare() {
		return totalPaxFare;
	}

	public void setTotalPaxFare(String totalPaxFare) {
		this.totalPaxFare = totalPaxFare;
	}

	/**
	 * @param selCurTotalPaxFare
	 *            the selCurTotalPaxFare to set
	 */
	public void setSelCurTotalPaxFare(String selCurTotalPaxFare) {
		this.selCurTotalPaxFare = selCurTotalPaxFare;
	}

	/**
	 * @return the selCurTotalPaxFare
	 */
	public String getSelCurTotalPaxFare() {
		return selCurTotalPaxFare;
	}

}
