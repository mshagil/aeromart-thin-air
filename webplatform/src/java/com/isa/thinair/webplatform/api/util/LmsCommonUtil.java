package com.isa.thinair.webplatform.api.util;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.wsclient.api.util.LMSConstants;

/**
 * @author chethiya
 *
 */
public class LmsCommonUtil {

	private static Log log = LogFactory.getLog(LmsCommonUtil.class);
	
	/**
	 * @param modelLmsMember
	 * @param carrierCode
	 * @param lmsManagementBD TODO
	 * @param lmsMemberDelegate TODO
	 * @param locale TODO
	 * @param isServiceAppRQ TODO
	 * @throws ModuleException
	 */
	public static void lmsEnroll(LmsMember modelLmsMember, String carrierCode, 
			LoyaltyManagementBD lmsManagementBD, LmsMemberServiceBD lmsMemberDelegate, Locale locale, boolean isServiceAppRQ) throws ModuleException{
						
		boolean headOfFamilyValid = true;
		
		if (modelLmsMember.getHeadOFEmailId()!="" && modelLmsMember.getHeadOFEmailId()!=null){
			LoyaltyMemberCoreDTO headOfFamily = lmsManagementBD.getLoyaltyMemberCoreDetails(modelLmsMember.getHeadOFEmailId());
			if(headOfFamily.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK){
				headOfFamilyValid = false;
			}					
		}
		if(headOfFamilyValid){
			LoyaltyMemberCoreDTO existingMember = lmsManagementBD.getLoyaltyMemberCoreDetails(modelLmsMember.getFfid());
			if(existingMember.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK){
				lmsMemberDelegate.register(modelLmsMember, carrierCode, locale, isServiceAppRQ);
			}
			else{
				lmsMemberDelegate.confirmMergeLmsMember(modelLmsMember, modelLmsMember.getFfid(), carrierCode, locale,
						isServiceAppRQ);
			}
		}
	}

	public static boolean isValidFamilyHeadAccount(String familyHeadFfid, LoyaltyMemberCoreDTO loyaltyMember) {

		boolean availability = false;
		if (loyaltyMember != null && validateFFIDEmail(familyHeadFfid)
					&& loyaltyMember.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK
					&& loyaltyMember.isEligibleForFamilyHead()) {
				availability = true;
		}
		return availability;
	}	
	
	public static boolean isNonMandAccountAvailable(String ffid, LoyaltyManagementBD lmsManagementBD){
						
		boolean availability = true;
		try{
			if (validateFFIDEmail(ffid)){
				LoyaltyMemberCoreDTO member = lmsManagementBD.getLoyaltyMemberCoreDetails(ffid);
				if(member.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK){
					availability = false;
				}					
			}
		}catch(ModuleException me){
			log.error("LmsCommonUtil ==>", me);
		}
		return availability;
	}
	
	public static boolean availableForCarrier(String ffid, LmsMemberServiceBD lmsMemberDelegate){
		boolean available = false;
		try {
			LmsMember lmsMember = lmsMemberDelegate.getLmsMember(ffid);
			if (lmsMember != null) {
				available = true;
			}
		} catch (ModuleException me) {
			log.error("LmsCommonUtil ==>", me);
		}
		return available;
	}
	
	public static LoyaltyMemberCoreDTO getPassenger(String ffid, LoyaltyManagementBD lmsManagementBD){
		
		LoyaltyMemberCoreDTO lmsMemberCoreDetails = null;
		try {
			 lmsMemberCoreDetails = lmsManagementBD.getLoyaltyMemberCoreDetails(ffid);
		} catch(ModuleException me){
			log.error("LmsCommonUtil ==>", me);
		}
		return lmsMemberCoreDetails;
	}
	
	public static void resendLmsConfEmail(LmsMember lmsMember, String carrierCode, String validationString,
			LmsMemberServiceBD lmsMemberDelegate, LoyaltyManagementBD loyaltyManagementBD, Locale locale,
			boolean isServiceAppRQ) {
	
		try {
			boolean available = isNonMandAccountAvailable(lmsMember.getFfid(), loyaltyManagementBD);
			lmsMemberDelegate.resendLmsConfEmail(lmsMember, validationString, carrierCode, available, locale, isServiceAppRQ);
		} catch (ModuleException me) {
			log.error("LmsCommonUtil ==>", me);
		}
	}
	
	public static void sendLMSMergeSuccessEmail(LmsMember lmsMember, String carrierCode, LmsMemberServiceBD lmsMemberDelegate){
	
		try {
			lmsMemberDelegate.sendLMSMergeSuccessEmail(lmsMember, carrierCode);
		} catch (ModuleException me) {
			log.error("LmsCommonUtil ==>", me);
		}
	}
	
	public static LmsMemberDTO getLmsByCustomer(Customer customer, LmsMemberDTO lmsDetails){
		
		lmsDetails.setEmailId(customer.getEmailId());
		lmsDetails.setFirstName(customer.getFirstName());
		lmsDetails.setLastName(customer.getLastName());
		lmsDetails.setGenderCode(customer.getTitle());
		lmsDetails.setMobileNumber(customer.getMobile());
		lmsDetails.setPhoneNumber(customer.getTelephone());
		lmsDetails.setEmailStatus("N");
		lmsDetails.setNationalityCode(String.valueOf(customer.getNationalityCode()));
		lmsDetails.setResidencyCode(customer.getCountryCode());		
		
		return lmsDetails;
		
	}
	
	public static String getValidPhoneNumber(String phone) {

		String returnPhone = "";
		if (phone != null) {
			phone = phone.replace("-", "");
			phone = phone.replace(" ", "");

			if (!"".equals(phone)) {
				returnPhone = phone;
			}
		}
		return returnPhone;
	}

	public static boolean validateFFIDEmail(String ffid){
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(ffid);
		return matcher.matches(); 
	}
	
	
}
