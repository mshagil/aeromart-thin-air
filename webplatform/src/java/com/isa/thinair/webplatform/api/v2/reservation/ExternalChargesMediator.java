package com.isa.thinair.webplatform.api.v2.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author Nilindra Fernando
 */
public class ExternalChargesMediator {

	private boolean isCCPayment = false;
	private boolean withAncillary = false;
	private boolean calculateJNTaxForCCCharge = false;
	private boolean isBSPPayment = false;

	private Collection<? extends IChargeablePax> paxList;
	private Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExtChgs;

	public ExternalChargesMediator(IBookingShoppingCart bookingShoppingCart, boolean isCCPayment) {

		this.paxList = bookingShoppingCart.getPaxList();
		this.selectedExtChgs = bookingShoppingCart.getSelectedExternalCharges();
		this.setCCPayment(isCCPayment);
	}

	public ExternalChargesMediator(Collection<? extends IChargeablePax> paxList,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExtChgs, boolean isCCPayment, boolean withAncillary) {

		this.selectedExtChgs = selectedExtChgs;
		this.paxList = paxList;
		this.setCCPayment(isCCPayment);
		this.setWithAncillary(withAncillary);
	}

	public ExternalChargesMediator(IBookingShoppingCart bookingShoppingCart, boolean isCCPayment, boolean withAncillary) {

		this.paxList = bookingShoppingCart.getPaxList();
		this.selectedExtChgs = bookingShoppingCart.getSelectedExternalCharges();
		this.setCCPayment(isCCPayment);
		this.setWithAncillary(withAncillary);
	}

	private List<ExternalChgDTO> getPaxAssocExternalCharges(EXTERNAL_CHARGES externalCharge) {
		List<ExternalChgDTO> chgDTOs = null;
		for (IChargeablePax pax : paxList) {
			if (chgDTOs == null) {
				chgDTOs = new ArrayList<ExternalChgDTO>();
			}

			ExternalChgDTO chargeDTO = null;
			if (isAncillaryExternalCharge(externalCharge)) {
				chargeDTO = pax.getAncillaryChargeDTO(externalCharge);
			} else {
				chargeDTO = pax.getExternalCharge(externalCharge);
			}
			if (chargeDTO != null) {
				chgDTOs.add(chargeDTO);
			}
		}
		return chgDTOs;
	}

	private List<ExternalChgDTO> getInfantAssocServiceTaxCharges() {
		List<ExternalChgDTO> chgDTOs = null;
		for (IChargeablePax pax : paxList) {
			if (chgDTOs == null) {
				chgDTOs = new ArrayList<ExternalChgDTO>();
			}
			ExternalChgDTO chargeDTO = pax.getInfantExternalCharge(EXTERNAL_CHARGES.SERVICE_TAX);

			if (chargeDTO != null) {
				chgDTOs.add(chargeDTO);
			}
		}
		return chgDTOs;
	}

	/**
	 * Number of paying passengers. The paying passenger can be either Adult or Child
	 * 
	 * @param noOfPayingPassengers
	 * @param infantPaxCount
	 * @return
	 */
	public LinkedList getExternalChargesForAFreshPayment(int noOfPayingPassengers, int infantPaxCount) {
		int paxCountWithInfant = noOfPayingPassengers + infantPaxCount;
		Collection[] externalCharges = new Collection[paxCountWithInfant];
		ExternalChgDTO handlingExtChgDTO = this.selectedExtChgs.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
		if (handlingExtChgDTO != null) {
			externalCharges = sum(externalCharges, composeExternalCharges(handlingExtChgDTO, paxCountWithInfant));
		}

		if (isCCPayment()) {

			ExternalChgDTO ExtChgDTO = this.selectedExtChgs.get(EXTERNAL_CHARGES.CREDIT_CARD);
			Collection<ExternalChgDTO> colCCExtChgDTOs = composeExternalCharges(ExtChgDTO, paxCountWithInfant);

			// if (paxCountWithInfant != noOfPayingPassengers && infantExternalCharges != null) {
			//
			// List<ExternalChgDTO> list = new ArrayList<ExternalChgDTO>(colCCExtChgDTOs);
			// Collection<ExternalChgDTO> adulChildCCCharges = list.subList(0, noOfPayingPassengers);
			// Collection<ExternalChgDTO> infantCCCharges = list.subList(noOfPayingPassengers, list.size());
			//
			// externalCharges = sum(externalCharges, adulChildCCCharges);
			// infantExternalCharges.addAll(infantCCCharges);
			//
			// } else {
			externalCharges = sum(externalCharges, colCCExtChgDTOs);
			// }

		}

		if (isBSPPayment()) {

			ExternalChgDTO ExtChgDTO = this.selectedExtChgs.get(EXTERNAL_CHARGES.BSP_FEE);
			Collection<ExternalChgDTO> colBSPExtChgDTOs = composeExternalCharges(ExtChgDTO, paxCountWithInfant);

			// if (paxCountWithInfant != noOfPayingPassengers && infantExternalCharges != null &&
			// !colBSPExtChgDTOs.isEmpty()) {
			// List<ExternalChgDTO> list = new ArrayList<ExternalChgDTO>(colBSPExtChgDTOs);
			// Collection<ExternalChgDTO> adulChildCCCharges = list.subList(0, noOfPayingPassengers);
			// Collection<ExternalChgDTO> infantCCCharges = list.subList(noOfPayingPassengers, list.size());
			//
			// externalCharges = sum(externalCharges, adulChildCCCharges);
			// infantExternalCharges.addAll(infantCCCharges);
			//
			// } else {
			externalCharges = sum(externalCharges, colBSPExtChgDTOs);
			// }

		}

		if (isCalculateJNTaxForCCCharge() || isBSPPayment()) {
			// JN Tax for Handling & CC charges
			ExternalChgDTO serviceChargeTax = this.selectedExtChgs.get(EXTERNAL_CHARGES.JN_OTHER);
			if (serviceChargeTax != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(serviceChargeTax, noOfPayingPassengers));
			}
		}

		if (isWithAncillary()) {
			ExternalChgDTO insExtChgDTO = this.selectedExtChgs.get(EXTERNAL_CHARGES.INSURANCE);
			if (insExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(insExtChgDTO, noOfPayingPassengers));
			}
			List<ExternalChgDTO> mealExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.MEAL);
			if (mealExtChgDTOs != null) {
				externalCharges = sum(externalCharges, mealExtChgDTOs);
			}

			List<ExternalChgDTO> baggageExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.BAGGAGE);
			if (baggageExtChgDTOs != null) {
				externalCharges = sum(externalCharges, baggageExtChgDTOs);
			}

			List<ExternalChgDTO> ssrExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
			if (ssrExtChgDTOs != null) {
				externalCharges = sum(externalCharges, ssrExtChgDTOs);
			}

			List<ExternalChgDTO> seatExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.SEAT_MAP);
			if (seatExtChgDTOs != null) {
				externalCharges = sum(externalCharges, seatExtChgDTOs);
			}

			List<ExternalChgDTO> airportExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);
			if (airportExtChgDTOs != null) {
				externalCharges = sum(externalCharges, airportExtChgDTOs);
			}

			List<ExternalChgDTO> airportTransferExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.AIRPORT_TRANSFER);
			if (airportTransferExtChgDTOs != null) {
				externalCharges = sum(externalCharges, airportTransferExtChgDTOs);
			}

			List<ExternalChgDTO> autoCheckinExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
			if (autoCheckinExtChgDTOs != null) {
				externalCharges = sum(externalCharges, autoCheckinExtChgDTOs);
			}

			List<ExternalChgDTO> anciTaxExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.JN_ANCI);
			if (anciTaxExtChgDTOs != null && !anciTaxExtChgDTOs.isEmpty()) {
				externalCharges = sum(externalCharges, anciTaxExtChgDTOs);
			}

			List<ExternalChgDTO> anciPenaltyChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.ANCI_PENALTY);
			if (anciPenaltyChgDTOs != null && !anciPenaltyChgDTOs.isEmpty()) {
				externalCharges = sum(externalCharges, anciPenaltyChgDTOs);
			}

			List<ExternalChgDTO> serviceTaxeExtChgDTOs = getPaxAssocExternalCharges(EXTERNAL_CHARGES.SERVICE_TAX);
			List<ExternalChgDTO> infantServiceTaxExtChgDTOs = getInfantAssocServiceTaxCharges();
			if (serviceTaxeExtChgDTOs != null) {
				externalCharges = sum(externalCharges, serviceTaxeExtChgDTOs);
			}

			if (infantServiceTaxExtChgDTOs != null) {
				externalCharges = sum(externalCharges, infantServiceTaxExtChgDTOs);
			}
		}

		return ReservationApiUtils.createLinkedList(externalCharges);
	}

	public Collection<ExternalChgDTO> getAllExternalCharges() {
		Collection[] externalCharges = new Collection[1];

		ExternalChgDTO handlingExtChgDTO = this.selectedExtChgs.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
		if (handlingExtChgDTO != null) {
			externalCharges = sum(externalCharges, composeExternalCharges(handlingExtChgDTO, 1));
		}

		ExternalChgDTO bspFeeExtChgDTO = this.selectedExtChgs.get(EXTERNAL_CHARGES.BSP_FEE);
		if (bspFeeExtChgDTO != null) {
			externalCharges = sum(externalCharges, composeExternalCharges(bspFeeExtChgDTO, 1));
		}

		if (isCCPayment()) {
			ExternalChgDTO ccExtChgDTO = this.selectedExtChgs.get(EXTERNAL_CHARGES.CREDIT_CARD);
			externalCharges = sum(externalCharges, composeExternalCharges(ccExtChgDTO, 1));
		}

		// JN Tax for Handling & CC charges
		ExternalChgDTO serviceChargeTax = this.selectedExtChgs.get(EXTERNAL_CHARGES.JN_OTHER);
		if (serviceChargeTax != null) {
			externalCharges = sum(externalCharges, composeExternalCharges(serviceChargeTax, 1));
		}

		ExternalChgDTO nextSeatChgDTO = this.selectedExtChgs.get(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE);
		if (nextSeatChgDTO != null) {
			externalCharges = sum(externalCharges, composeExternalCharges(nextSeatChgDTO, 1));
		}

		if (isWithAncillary()) {
			ExternalChgDTO insExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.INSURANCE);
			if (insExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(insExtChgDTO, 1));
			}

			ExternalChgDTO mealExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.MEAL);
			if (mealExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(mealExtChgDTO, 1));
			}

			ExternalChgDTO baggageExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.BAGGAGE);
			if (baggageExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(baggageExtChgDTO, 1));
			}

			ExternalChgDTO ssrExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
			if (ssrExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(ssrExtChgDTO, 1));
			}

			ExternalChgDTO seatExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.SEAT_MAP);
			if (seatExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(seatExtChgDTO, 1));
			}

			ExternalChgDTO autoCheckinExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
			if (autoCheckinExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(autoCheckinExtChgDTO, 1));
			}

			ExternalChgDTO aiportExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.AIRPORT_SERVICE);
			if (aiportExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(aiportExtChgDTO, 1));
			}

			ExternalChgDTO aiportTransferExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.AIRPORT_TRANSFER);
			if (aiportTransferExtChgDTO != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(aiportTransferExtChgDTO, 1));
			}

			ExternalChgDTO anciPenaltyChgDTOs = getSelectedExternalCharge(EXTERNAL_CHARGES.ANCI_PENALTY);
			if (anciPenaltyChgDTOs != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(anciPenaltyChgDTOs, 1));
			}
		}

		ExternalChgDTO anciTaxExtChgDTO = getSelectedExternalCharge(EXTERNAL_CHARGES.JN_ANCI);
		if (anciTaxExtChgDTO != null) {
			externalCharges = sum(externalCharges, composeExternalCharges(anciTaxExtChgDTO, 1));
		}

		return (Collection<ExternalChgDTO>) ReservationApiUtils.createLinkedList(externalCharges).get(0);
	}

	private Collection[] fillExternalCharges(Collection[] externalCharges, int paxCountWithInfant, EXTERNAL_CHARGES chargeType) {

		if (paxCountWithInfant != 0) {

			ExternalChgDTO ExtChgDTO = this.selectedExtChgs.get(chargeType);
			Collection<ExternalChgDTO> colCCExtChgDTOs = composeExternalCharges(ExtChgDTO, paxCountWithInfant);

			// if (paxCountWithInfant != noOfAdults && infantExternalCharges != null) {
			// List<ExternalChgDTO> list = new ArrayList<ExternalChgDTO>(colCCExtChgDTOs);
			//
			// if (!list.isEmpty()) {
			//
			// if (noOfAdults != 0) {
			// Collection<ExternalChgDTO> adulChildCCCharges = list.subList(0, noOfAdults);
			// externalCharges = sum(externalCharges, adulChildCCCharges);
			// }
			//
			// if (infantPaxCount != 0) {
			// Collection<ExternalChgDTO> infantCCCharges = list.subList(noOfAdults, list.size());
			// infantExternalCharges.addAll(infantCCCharges);
			// }
			// }
			//
			// } else {
			externalCharges = sum(externalCharges, colCCExtChgDTOs);
			// }

		}

		return externalCharges;

	}

	public LinkedList getExternalChargesForABalancePayment(int noOfAdults, int infantPaxCount) {
		int paxCountWithInfant = noOfAdults + infantPaxCount;
		Collection[] externalCharges = new Collection[paxCountWithInfant];

		if (isCCPayment()) {
			externalCharges = fillExternalCharges(externalCharges, paxCountWithInfant, EXTERNAL_CHARGES.CREDIT_CARD);
		}

		if (isBSPPayment()) {
			externalCharges = fillExternalCharges(externalCharges, paxCountWithInfant, EXTERNAL_CHARGES.BSP_FEE);

		}

		if ((isCCPayment() && isCalculateJNTaxForCCCharge() || isBSPPayment()) && (noOfAdults != 0)) {
			// JN Tax for Handling & CC charges
			ExternalChgDTO serviceChargeTax = this.selectedExtChgs.get(EXTERNAL_CHARGES.JN_OTHER);
			if (serviceChargeTax != null) {
				externalCharges = sum(externalCharges, composeExternalCharges(serviceChargeTax, noOfAdults));
			}
		}

		return ReservationApiUtils.createLinkedList(externalCharges);
	}

	public Collection<ExternalChgDTO> getPaxServiceTaxExternalCharge(int paxSequence) {
		Collection<ExternalChgDTO> externalServiceTaxCharges = new ArrayList<ExternalChgDTO>();
		for (IChargeablePax pax : paxList) {
			ReservationPaxTO resPaxTo = (ReservationPaxTO) pax;
			if (resPaxTo.getSeqNumber() == paxSequence) {
				for (LCCClientExternalChgDTO chgDTO : resPaxTo.getExternalCharges()) {
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.SERVICE_TAX) {
						ServiceTaxExtChgDTO serviceTaxExtChgDTO = new ServiceTaxExtChgDTO();
						serviceTaxExtChgDTO.setAmount(chgDTO.getAmount());
						serviceTaxExtChgDTO.setAmountConsumedForPayment(true);
						serviceTaxExtChgDTO.setChargeCode(chgDTO.getCode());
						serviceTaxExtChgDTO.setExternalChargesEnum(chgDTO.getExternalCharges());
						serviceTaxExtChgDTO.setTaxDepositCountryCode(chgDTO.getTaxDepositCountryCode());
						serviceTaxExtChgDTO.setTaxDepositStateCode(chgDTO.getTaxDepositStateCode());
						serviceTaxExtChgDTO.setFlightRefNumber(chgDTO.getFlightRefNumber());
						serviceTaxExtChgDTO.setTaxableAmount(chgDTO.getTaxableAmount());
						serviceTaxExtChgDTO.setNonTaxableAmount(chgDTO.getNonTaxableAmount());
						serviceTaxExtChgDTO.setTicketingRevenue(chgDTO.isTicketingRevenue());
						externalServiceTaxCharges.add(serviceTaxExtChgDTO);
					}
				}
			}
		}
		return externalServiceTaxCharges;
	}

	public Collection<ExternalChgDTO> getInfantServiceTaxExternalCharge(int attachedAdultPaxSequence) {
		Collection<ExternalChgDTO> externalServiceTaxCharges = new ArrayList<ExternalChgDTO>();
		for (IChargeablePax pax : paxList) {
			ReservationPaxTO resPaxTo = (ReservationPaxTO) pax;
			if (resPaxTo.getSeqNumber() == attachedAdultPaxSequence && resPaxTo.getInfantExternalCharges() != null) {
				for (LCCClientExternalChgDTO chgDTO : resPaxTo.getInfantExternalCharges()) {
					if (chgDTO.getExternalCharges() == EXTERNAL_CHARGES.SERVICE_TAX) {
						ServiceTaxExtChgDTO serviceTaxExtChgDTO = new ServiceTaxExtChgDTO();
						serviceTaxExtChgDTO.setAmount(chgDTO.getAmount());
						serviceTaxExtChgDTO.setAmountConsumedForPayment(true);
						serviceTaxExtChgDTO.setChargeCode(chgDTO.getCode());
						serviceTaxExtChgDTO.setExternalChargesEnum(chgDTO.getExternalCharges());
						serviceTaxExtChgDTO.setTaxDepositCountryCode(chgDTO.getTaxDepositCountryCode());
						serviceTaxExtChgDTO.setTaxDepositStateCode(chgDTO.getTaxDepositStateCode());
						serviceTaxExtChgDTO.setFlightRefNumber(chgDTO.getFlightRefNumber());
						serviceTaxExtChgDTO.setTaxableAmount(chgDTO.getTaxableAmount());
						serviceTaxExtChgDTO.setNonTaxableAmount(chgDTO.getNonTaxableAmount());
						serviceTaxExtChgDTO.setTicketingRevenue(chgDTO.isTicketingRevenue());
						externalServiceTaxCharges.add(serviceTaxExtChgDTO);
					}
				}
			}
		}
		return externalServiceTaxCharges;
	}

	private Collection[] sum(Collection[] externalCharges, Collection<ExternalChgDTO> colExternalChgDTO) {

		if (colExternalChgDTO != null && colExternalChgDTO.size() > 0) {
			externalCharges = ReservationApiUtils.copyToCollectionArray(externalCharges, colExternalChgDTO);
		}

		return externalCharges;
	}

	private Collection<ExternalChgDTO> composeExternalCharges(ExternalChgDTO extChgDTO, int noOfAdults) {
		Collection<ExternalChgDTO> colExternalChgDTO = new ArrayList<ExternalChgDTO>();
		if (extChgDTO != null) {
			extChgDTO = (ExternalChgDTO) extChgDTO.clone();
			extChgDTO.setAmountConsumedForPayment(false);

			if (extChgDTO.getAmount().doubleValue() > 0) {
				colExternalChgDTO.addAll(ReservationApiUtils.getPerPaxExternalCharges(extChgDTO, noOfAdults));
			}
		}

		return colExternalChgDTO;
	}

	private ExternalChgDTO getSelectedExternalCharge(EXTERNAL_CHARGES externalCharge) {
		if (isAncillaryExternalCharge(externalCharge)) {
			return getAncillaryExternalChargeDTO(externalCharge);
		} else {
			return this.selectedExtChgs.get(externalCharge);
		}
	}

	private boolean isAncillaryExternalCharge(EXTERNAL_CHARGES externalCharge) {
		return (externalCharge == EXTERNAL_CHARGES.SEAT_MAP || externalCharge == EXTERNAL_CHARGES.MEAL
				|| externalCharge == EXTERNAL_CHARGES.INFLIGHT_SERVICES || externalCharge == EXTERNAL_CHARGES.BAGGAGE
				|| externalCharge == EXTERNAL_CHARGES.AIRPORT_SERVICE || externalCharge == EXTERNAL_CHARGES.AIRPORT_TRANSFER
				|| externalCharge == EXTERNAL_CHARGES.JN_ANCI || externalCharge == EXTERNAL_CHARGES.ANCI_PENALTY
				|| externalCharge == EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
	}

	private BigDecimal getAncillaryExternalCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = BigDecimal.ZERO;
		if (this.paxList == null)
			return total;
		for (IChargeablePax pax : this.paxList) {
			total = AccelAeroCalculator.add(total, pax.getAncillaryCharge(externalCharge));
		}
		return total;
	}

	private ExternalChgDTO getAncillaryExternalChargeDTO(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = getAncillaryExternalCharge(externalCharge);
		ExternalChgDTO chgDTO = new ExternalChgDTO();
		chgDTO.setExternalChargesEnum(externalCharge);
		chgDTO.setAmount(total);
		return chgDTO;
	}

	/**
	 * @return the isCCPayment
	 */
	private boolean isCCPayment() {
		return isCCPayment;
	}

	/**
	 * @param isCCPayment
	 *            the isCCPayment to set
	 */
	private void setCCPayment(boolean isCCPayment) {
		this.isCCPayment = isCCPayment;
	}

	/**
	 * 
	 * @return the isWithAncillary
	 */
	public boolean isWithAncillary() {
		return withAncillary;
	}

	/**
	 * 
	 * @param withAncillary
	 */
	public void setWithAncillary(boolean withAncillary) {
		this.withAncillary = withAncillary;
	}

	public boolean isCalculateJNTaxForCCCharge() {
		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			calculateJNTaxForCCCharge = true;
		}
		return calculateJNTaxForCCCharge;
	}

	public void setCalculateJNTaxForCCCharge(boolean calculateJNTaxForCCCharge) {
		this.calculateJNTaxForCCCharge = calculateJNTaxForCCCharge;
	}

	public boolean isBSPPayment() {
		return isBSPPayment;
	}

	public void setBSPPayment(boolean isBSPPayment) {
		this.isBSPPayment = isBSPPayment;
	}

}
