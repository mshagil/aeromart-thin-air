package com.isa.thinair.webplatform.api.dto;

/**
 * @author eric
 * 
 */
public class PaxSet {

	private int noOfAdluts;

	private int noOfChildren;

	private int noOfInfants;

	public PaxSet() {
		super();
	}

	public PaxSet(int noOfAdluts, int noOfChildren, int noOfInfants) {
		super();
		this.noOfAdluts = noOfAdluts;
		this.noOfChildren = noOfChildren;
		this.noOfInfants = noOfInfants;
	}

	public int getNoOfAdluts() {
		return noOfAdluts;
	}

	public void setNoOfAdluts(int noOfAdluts) {
		this.noOfAdluts = noOfAdluts;
	}

	public int getNoOfChildren() {
		return noOfChildren;
	}

	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}

	public int getNoOfInfants() {
		return noOfInfants;
	}

	public void setNoOfInfants(int noOfInfants) {
		this.noOfInfants = noOfInfants;
	}

}