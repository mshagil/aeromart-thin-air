package com.isa.thinair.webplatform.api.v2.reservation;

public class CalendarFlightAvailInfo {

	private String date;
	private boolean isIsFareAvailable = false;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean isIsFareAvailable() {
		return isIsFareAvailable;
	}

	public void setIsFareAvailable(boolean isIsFareAvailable) {
		this.isIsFareAvailable = isIsFareAvailable;
	}
}
