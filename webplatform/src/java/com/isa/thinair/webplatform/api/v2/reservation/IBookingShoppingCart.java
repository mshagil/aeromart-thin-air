package com.isa.thinair.webplatform.api.v2.reservation;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;

/**
 * @author Dilan Anuruddha
 */
public interface IBookingShoppingCart {

	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getSelectedExternalCharges();

	public void removeSelectedExternalCharge(EXTERNAL_CHARGES externalCharge);

	public Collection<ReservationPaxTO> getPaxList();

	public BigDecimal getTotalPrice();

	public BigDecimal getTotalPayable();

	public BigDecimal getTotalTax();

	public BigDecimal getTotalSurcharges();

	public BigDecimal getTotalBaseFare();
}
