package com.isa.thinair.webplatform.api.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class IoUtil {
	static Log log = LogFactory.getLog(IoUtil.class);

	private IoUtil() {
	}

	public static Collection<String> readContent(File file) {
		BufferedReader reader = null;
		String currentLine;
		Collection<String> lines = new ArrayList<String>();



		try {
			reader = new BufferedReader(new FileReader(file));

			while ((currentLine = reader.readLine()) != null) {
				currentLine = currentLine.trim();
				lines.add(currentLine);
			}

		} catch (Exception e) {
			log.error("File Read Error: " + file.getName(), e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					log.error("Reader Close Error", e);
				}
			}
		}

		return lines;
	}


	public static String readContent(File file, String eol) {
		StringBuilder sbContent = new StringBuilder();
		String content = null;

		Collection<String> lines = readContent(file);
		Iterator<String> itrLines = lines.iterator();
		while (itrLines.hasNext()) {
			sbContent.append(itrLines.next());
			if (itrLines.hasNext()) {
				sbContent.append(eol);
			}
		}
		content = sbContent.toString();

		return content;
	}
}
