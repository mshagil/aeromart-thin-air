package com.isa.thinair.webplatform.api.dto;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class CarLinkParamsTO {
	
	private String pkDateTime;
	
	private String rtDateTime;
	
	private String locCode;
	
	private String residencyId;
	
	private String clientId;
	
	private String lang;
	
	private String staticURL;
	
	private String currency;
	
	private String orderId;
	
	public CarLinkParamsTO() {
		residencyId = "";
		clientId =  AppSysParamsUtil.getRentACarClientId();
		lang = "EN";
	}

	public String getPkDateTime() {
		return pkDateTime;
	}

	public void setPkDateTime(String pkDateTime) {
		this.pkDateTime = pkDateTime;
	}

	public String getRtDateTime() {
		return rtDateTime;
	}

	public void setRtDateTime(String rtDateTime) {
		this.rtDateTime = rtDateTime;
	}

	public String getLocCode() {
		return locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public String getResidencyId() {
		return residencyId;
	}

	public void setResidencyId(String residencyId) {
		this.residencyId = residencyId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getStaticURL() {
		return staticURL;
	}

	public void setStaticURL(String staticURL) {
		this.staticURL = staticURL;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
