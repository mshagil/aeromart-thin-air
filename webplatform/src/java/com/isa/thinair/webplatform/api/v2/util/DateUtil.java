package com.isa.thinair.webplatform.api.v2.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

/**
 * Date Utility
 * 
 * @author Thushara
 * @since Dec 23, 2009
 */
public class DateUtil {

	/**
	 * Convert Zulu date to a Local date
	 * 
	 * @param airportCode
	 * @param zuluDate
	 * @return
	 * @throws ModuleException
	 */
	public static Date getLocalDateTime(Date zuluDate, String airportCode) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(WPModuleUtils.getAirportBD());

		return helper.getLocalDateTime(airportCode, zuluDate);
	}

	/**
	 * Convert Zulu date to a Local date string
	 * 
	 * @param zuluDate
	 * @param airPortCode
	 * @return
	 * @throws ModuleException
	 */
	public static String getLocalDateTimeString(Date zuluDate, String airportCode, String strDateFormat) throws ModuleException {
		Date localDate = DateUtil.getLocalDateTime(zuluDate, airportCode);

		return DateUtil.formatDate(localDate, strDateFormat);
	}

	/**
	 * get the local time for a given agent. This is also support the dry agents as well
	 * 
	 * @param zuluTime
	 * @param colUserDST
	 * @param strStation
	 * @param strDateFormat
	 * @return
	 * @throws ModuleException
	 */
	public static String getAgentLocalTime(Date zuluTime, Collection colUserDST, String strStation, String strDateFormat)
			throws ModuleException {
		if (colUserDST != null && !colUserDST.isEmpty()) {
			Iterator itColUserDST = colUserDST.iterator();
			boolean matchFound = false;
			Date localDate = null;
			UserDST userDST;
			while (itColUserDST.hasNext()) {
				userDST = (UserDST) itColUserDST.next();

				if (compareZuluDates(userDST.getDstStartDateTime(), userDST.getDstEndDateTime(), zuluTime)) {
					localDate = CalendarUtil.getLocalDate(zuluTime, userDST.getGmtOffsetMinutes(), userDST.getAdjustMinutes());
					matchFound = true;
					break;
				}
			}
			// If match is not found
			if (matchFound == false) {
				itColUserDST = colUserDST.iterator();
				if (itColUserDST.hasNext()) {
					userDST = (UserDST) itColUserDST.next();
					localDate = CalendarUtil.getLocalDate(zuluTime, userDST.getGmtOffsetMinutes(), 0);
				}
			}
			return DateUtil.formatDate(localDate, strDateFormat);
		} else {
			return getLocalDateTimeString(zuluTime, strStation, strDateFormat);
		}
	}

	/**
	 * Convert Local date to a Zulu date
	 * 
	 * @param airportCode
	 * @param zuluDate
	 * @return
	 * @throws ModuleException
	 */
	public static Date getZuluDateTime(Date localDate, String airportCode) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(WPModuleUtils.getAirportBD());

		return helper.getZuluDateTime(airportCode, localDate);
	}

	/**
	 * Convert Local date to a Zulu date string
	 * 
	 * @param zuluDate
	 * @param airPortCode
	 * @return
	 * @throws ModuleException
	 */
	public static String getZuluDateTimeString(Date localDate, String airportCode, String strDateFormat) throws ModuleException {
		Date zuluDate = DateUtil.getLocalDateTime(localDate, airportCode);

		return DateUtil.formatDate(zuluDate, strDateFormat);
	}

	/**
	 * Formats specified date with specified format
	 * 
	 * @return String the formated date
	 */
	public static String formatDate(Date utilDate, String fmt) {
		String formatedDate = "";

		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			formatedDate = sdFmt.format(utilDate);
		}

		return formatedDate;
	}

	/**
	 * Parse Date
	 * 
	 * @param strDate
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static Date parseDate(String strDate, String format) throws ParseException {
		if (strDate != null && !strDate.isEmpty()) {
			return new SimpleDateFormat(format).parse(strDate);
		}

		return null;
	}

	public static Date getCurrentZuluDateTime() {
		return getCurrentDateTime("GMT");
	}

	private static Date getCurrentDateTime(String strTimeZone) {
		TimeZone timeZone = null;
		Calendar cal = Calendar.getInstance();

		if (strTimeZone == null || "".equals(strTimeZone)) {
			timeZone = TimeZone.getDefault();
		} else {
			TimeZone.getTimeZone(strTimeZone);
		}

		cal.setTimeZone(timeZone);

		return cal.getTime();
	}

	/**
	 * Compare Zulu Dates
	 * 
	 * @param from
	 * @param to
	 * @param currentTime
	 * @return
	 */
	public static boolean compareZuluDates(Date from, Date to, Date currentTime) {

		if (from == null || to == null || currentTime == null) {
			return false;
		} else if (currentTime.after(from) && currentTime.before(to)) {
			return true;
		}
		return false;
	}

	/**
	 * Get dates difference in no of days
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static int getDateDifference(Date from, Date to) {
		return (int) ((to.getTime() - from.getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * Use to get current zulu time
	 * 
	 * @return current zulu time
	 */
	public static Date getZuluTime() {
		Calendar c = Calendar.getInstance();
		TimeZone z = c.getTimeZone();
		c.add(Calendar.MILLISECOND, -(z.getRawOffset() + z.getDSTSavings()));
		return c.getTime();
	}
}