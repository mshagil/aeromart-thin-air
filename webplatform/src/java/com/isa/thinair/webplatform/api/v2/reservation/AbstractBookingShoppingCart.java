package com.isa.thinair.webplatform.api.v2.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Dilan Anuruddha
 */
public abstract class AbstractBookingShoppingCart implements IBookingShoppingCart {

	private PriceInfoTO priceInfoTO;

	private Map<EXTERNAL_CHARGES, ExternalChgDTO> allExternalChargesMap;

	private Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExternalCharges = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();

	private CurrencyExchangeRate currencyExchangeRate;

	private Collection<ReservationPaxTO> paxList;

	private Date selectedCurrencyTimestamp;

	protected void resetSelectedAncillary() {
		selectedExternalCharges = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		paxList = null;
	}

	public void addSelectedExternalCharge(EXTERNAL_CHARGES externalCharge) {
		ExternalChgDTO externalChgDTO = getAllExternalChargesMap().get(externalCharge);

		if (externalChgDTO != null) {
			this.getSelectedExternalCharges().put(externalCharge, externalChgDTO);
		}
	}

	public void addSelectedExternalCharge(ExternalChgDTO externalChgDTO) {
		this.getSelectedExternalCharges().put(externalChgDTO.getExternalChargesEnum(), externalChgDTO);
	}

	public void removeSelectedExternalCharge(EXTERNAL_CHARGES externalCharge) {
		this.getSelectedExternalCharges().remove(externalCharge);
	}

	public ExternalChgDTO getSelectedExternalCharge(EXTERNAL_CHARGES externalCharge) {
		if (isAncillaryExternalCharge(externalCharge)) {
			return getAncillaryExternalChargeDTO(externalCharge);
		} else {
			return this.getSelectedExternalCharges().get(externalCharge);
		}
	}

	private BigDecimal getAncillaryExternalCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPaxList() != null) {
			for (IChargeablePax pax : getPaxList()) {
				total = AccelAeroCalculator.add(total, pax.getAncillaryCharge(externalCharge));
			}
		}
		return total;
	}

	public BigDecimal getAncillaryTotalExternalCharge(boolean withInsurance) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPaxList() != null) {
			for (ReservationPaxTO pax : getPaxList()) {
				total = AccelAeroCalculator.add(total, pax.getAncillaryTotal(false));
			}
			if (withInsurance) {
				if (getSelectedExternalCharge(EXTERNAL_CHARGES.INSURANCE) != null) {
					total = AccelAeroCalculator.add(total, getSelectedExternalCharge(EXTERNAL_CHARGES.INSURANCE).getAmount());
				}
			}
		}
		return total;
	}

	public BigDecimal getExternalChargesBalanceToPay() {
		return AccelAeroCalculator.add(getTotalSelectedExternalChargesAmount(true), getAncillaryToUpdateTotalCharge(),
				getAncillaryToRemoveTotalCharge().negate());
	}

	public BigDecimal getExternalChargesBalanceToPay(EXTERNAL_CHARGES... excludeChargeCodes) {
		return AccelAeroCalculator.add(getTotalSelectedExternalCharges(excludeChargeCodes), getAncillaryToUpdateTotalCharge(),
				getAncillaryToRemoveTotalCharge().negate());
	}

	public BigDecimal getExternalChargesIncludingCCChargesBalanceToPay() {
		return AccelAeroCalculator.add(getTotalSelectedExternalChargesAmount(false), getAncillaryToUpdateTotalCharge(),
				getAncillaryToRemoveTotalCharge().negate());
	}

	private BigDecimal getAncillaryToRemoveTotalCharge() {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPaxList() != null) {
			for (ReservationPaxTO pax : getPaxList()) {
				total = AccelAeroCalculator.add(total, pax.getToRemoveAncillaryTotal());
			}
		}
		return total;
	}

	private BigDecimal getAncillaryToUpdateTotalCharge() {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPaxList() != null) {
			for (ReservationPaxTO pax : getPaxList()) {
				total = AccelAeroCalculator.add(total, pax.getToUpdateAncillaryTotal());
			}
		}
		return total;
	}

	private ExternalChgDTO getAncillaryExternalChargeDTO(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = getAncillaryExternalCharge(externalCharge);
		ExternalChgDTO chgDTO = new ExternalChgDTO();
		chgDTO.setExternalChargesEnum(externalCharge);
		chgDTO.setAmount(total);
		return chgDTO;
	}

	private boolean isAncillaryExternalCharge(EXTERNAL_CHARGES externalCharge) {
		return (externalCharge == EXTERNAL_CHARGES.SEAT_MAP || externalCharge == EXTERNAL_CHARGES.MEAL
				|| externalCharge == EXTERNAL_CHARGES.INFLIGHT_SERVICES);
	}

	public BigDecimal getTotalSelctedExternalChargesAmount() {
		return getTotalSelectedExternalChargesAmount(false);
	}

	protected BigDecimal getTotalSelectedExternalChargesAmount(boolean isExcludeCCCharge) {
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (isExcludeCCCharge) {
			totalAmount = getTotalSelectedExternalCharges(EXTERNAL_CHARGES.CREDIT_CARD);
		} else {
			totalAmount = getTotalSelectedExternalCharges();

		}
		return totalAmount;
	}

	private boolean isNonExcluded(EXTERNAL_CHARGES[] excludeChargeCodes, EXTERNAL_CHARGES charge) {
		if (excludeChargeCodes != null && charge != null && excludeChargeCodes.length > 0) {
			for (int i = 0; i < excludeChargeCodes.length; i++) {
				if (excludeChargeCodes[i] == charge) {
					return false;
				}
			}
		}

		return true;

	}

	protected BigDecimal getTotalSelectedExternalCharges(EXTERNAL_CHARGES... excludeChargeCodes) {
		Collection<ExternalChgDTO> colExternalChgDTO = this.getSelectedExternalCharges().values();
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		
		boolean excludeServiceTaxForCC = containsExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD, excludeChargeCodes);

		if (colExternalChgDTO != null && !colExternalChgDTO.isEmpty()) {
			for (ExternalChgDTO externalChgDTO : colExternalChgDTO) {
				if (isNonExcluded(excludeChargeCodes, externalChgDTO.getExternalChargesEnum())) {
					totalAmount = AccelAeroCalculator.add(totalAmount, externalChgDTO.getAmount());
				}

			}
		}
		totalAmount = AccelAeroCalculator.add(totalAmount, getAncillaryTotalExternalCharge(false),
				getTotalServiceTaxExternalCharge(excludeServiceTaxForCC));
		return totalAmount;
	}

	protected BigDecimal getSelectedFareAmount() {
		BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPriceInfoTO() != null) {// && getReservation() == null
			fareAmount = priceInfoTO.getFareTypeTO().getTotalPrice();
		}
		return fareAmount;
	}

	public BigDecimal getTotalPrice() {
		return AccelAeroCalculator.add(getSelectedFareAmount(), this.getTotalSelectedExternalChargesAmount(true));
	}

	public BigDecimal getTotalPriceIncludingCCCharges() {
		return AccelAeroCalculator.add(getSelectedFareAmount(), this.getTotalSelectedExternalChargesAmount(false));
	}

	public BigDecimal getTotalPayable() {
		return AccelAeroCalculator.add(getSelectedFareAmount(), this.getTotalSelectedExternalChargesAmount(true));
	}

	public BigDecimal getTotalPayableIncludingCCCharges() {
		return AccelAeroCalculator.add(getSelectedFareAmount(), this.getTotalSelectedExternalChargesAmount(false));
	}

	public BigDecimal getTotalBaseFare() {
		BigDecimal totalBaseFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPriceInfoTO() != null) {
			totalBaseFare = this.priceInfoTO.getFareTypeTO().getTotalBaseFare();
		}
		return totalBaseFare;
	}

	public BigDecimal getTotalTax() {
		BigDecimal totalTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPriceInfoTO() != null) {
			totalTax = this.priceInfoTO.getFareTypeTO().getTotalTaxes();
		}
		return totalTax;
	}

	public BigDecimal getTotalSurcharges() {
		BigDecimal totalSurcharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPriceInfoTO() != null) {
			totalSurcharges = this.priceInfoTO.getFareTypeTO().getTotalSurcharges();
		}
		return totalSurcharges;
	}

	public void setTotalExternalChangeAmount(EXTERNAL_CHARGES externalCharge) {
		if (getSelectedExternalCharge(externalCharge) != null) {
			getSelectedExternalCharge(externalCharge).setAmount(getSelectedExternalCharge(externalCharge).getTotalAmount());
		}
	}

	/**
	 * 
	 * @return
	 */
	public PriceInfoTO getPriceInfoTO() {
		return priceInfoTO;
	}

	/**
	 * 
	 * @param priceInfoTO
	 */
	public void setPriceInfoTO(PriceInfoTO priceInfoTO) {
		this.priceInfoTO = priceInfoTO;
	}

	/**
	 * @return the selectedExternalCharges
	 */
	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getSelectedExternalCharges() {
		return selectedExternalCharges;
	}

	/**
	 * @return the allExternalChargesMap
	 */
	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getAllExternalChargesMap() {
		return allExternalChargesMap;
	}

	/**
	 * @param allExternalChargesMap
	 *            the allExternalChargesMap to set
	 */
	protected void setAllExternalChargesMap(Map<EXTERNAL_CHARGES, ExternalChgDTO> allExternalChargesMap) {
		this.allExternalChargesMap = allExternalChargesMap;
	}

	/**
	 * @return the currencyExchangeRate
	 */
	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	/**
	 * @param currencyExchangeRate
	 *            the currencyExchangeRate to set
	 */
	public void setCurrencyExchangeRate(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	/**
	 * @return the paxList
	 */
	public Collection<ReservationPaxTO> getPaxList() {
		if (paxList == null) {
			return new ArrayList<ReservationPaxTO>();
		}
		return paxList;
	}

	/**
	 * @param paxList
	 *            the paxList to set
	 */
	public void setPaxList(Collection<ReservationPaxTO> paxList) {
		this.paxList = paxList;
	}

	public void removeServiceTaxAppliedToCCFee() {
		for (ReservationPaxTO pax : getPaxList()) {
			pax.removeServiceTaxAppliedToCCFee();
		}
	}

	public Map<Integer, ReservationPaxTO> getPassengerMap() {
		Map<Integer, ReservationPaxTO> paxMap = new HashMap<Integer, ReservationPaxTO>();

		if (this.getPaxList() != null) {
			for (ReservationPaxTO pax : this.getPaxList()) {
				paxMap.put(pax.getSeqNumber(), pax);
			}
		}

		return paxMap;
	}

	public Date getSelectedCurrencyTimestamp() {
		return selectedCurrencyTimestamp;
	}

	public void setSelectedCurrencyTimestamp(Date selectedCurrencyTimestamp) {
		this.selectedCurrencyTimestamp = selectedCurrencyTimestamp;
	}

	public void resetSelectedExternalChargeAmount(Collection<EXTERNAL_CHARGES> extCharges) {
		for (EXTERNAL_CHARGES extChg : extCharges) {
			ExternalChgDTO externalChgDTO = getSelectedExternalCharge(extChg);
			if (externalChgDTO != null) {
				externalChgDTO.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
				externalChgDTO.setTotalAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			}
		}
	}

	public Map<String, String> getFlightRefWiseOndBaggageGroup() {
		Map<String, String> flightRefWiseOndBaggageGroup = new HashMap<String, String>();
		if (paxList != null && !paxList.isEmpty()) {
			for (ReservationPaxTO reservationPaxTO : paxList) {
				List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries = reservationPaxTO.getSelectedAncillaries();
				if (selectedAncillaries != null && !selectedAncillaries.isEmpty()) {
					for (LCCSelectedSegmentAncillaryDTO selAnci : selectedAncillaries) {
						FlightSegmentTO flightSegmentTO = selAnci.getFlightSegmentTO();
						String ondBaggageGroupId = flightSegmentTO.getBaggageONDGroupId();
						if (ondBaggageGroupId != null && !ondBaggageGroupId.equals("")
								&& !flightRefWiseOndBaggageGroup.containsKey(flightSegmentTO.getFlightRefNumber())) {
							flightRefWiseOndBaggageGroup.put(flightSegmentTO.getFlightRefNumber(), ondBaggageGroupId);
						}
					}
				}
			}
		}

		return flightRefWiseOndBaggageGroup;
	}

	public BigDecimal getTotalPayable(EXTERNAL_CHARGES... excludeChargeCodes) {
		return AccelAeroCalculator.add(getSelectedFareAmount(), this.getExternalChargesBalanceToPay(excludeChargeCodes));
	}

	public BigDecimal getTotalInfantExternalCharge() {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPaxList() != null) {
			for (ReservationPaxTO pax : getPaxList()) {
				total = AccelAeroCalculator.add(total, pax.getInfantExternalChargeTotal());
			}
		}
		return total;
	}

	public BigDecimal getTotalServiceTaxExternalCharge(boolean excludeServiceTaxForCC) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPaxList() != null) {
			for (ReservationPaxTO pax : getPaxList()) {
				total = AccelAeroCalculator.add(total, pax.getTotalServiceTaxCharge(true, excludeServiceTaxForCC));
			}
		}
		return total;
	}
	
	private boolean containsExternalCharge(EXTERNAL_CHARGES externalCharge, EXTERNAL_CHARGES... excludeChargeCodes) {
		boolean containsExternalCharge = false;
		for (EXTERNAL_CHARGES extCharge: excludeChargeCodes) {
			if (extCharge == externalCharge) {
				containsExternalCharge = true;
			}
			break;
		}
		return containsExternalCharge;
	}

}
