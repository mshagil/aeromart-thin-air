package com.isa.thinair.webplatform.core.schedrept;

import javax.servlet.http.HttpServletRequest;

public interface SRPComposer {

	public ReportInputs composeReportInputs(HttpServletRequest request);
}
