package com.isa.thinair.webplatform.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class BasicSingleResultExtractor implements ResultSetExtractor {
	private String[] col;

	public BasicSingleResultExtractor(String[] col) {
		this.col = col;
	}

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		// Note : added this else part because this is for some cases (eg. getOctupusCity in SelectListGenerator)
		// empty list leads to SQL exception
		if (rs.next())
			return new String[] { rs.getString(col[0]), rs.getString(col[1]) };
		else
			return new String[] { "", "" };
	}

}
