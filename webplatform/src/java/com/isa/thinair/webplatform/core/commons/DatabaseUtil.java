package com.isa.thinair.webplatform.core.commons;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.LookupUtils;

public class DatabaseUtil {

	private static final String KEY_AGENT_CURRENTCY = "agentCurrency";

	private static final String KEY_CUSTOMER_ID = "customerId";

	private static final String KEY_NOMINAL_CODES = "nominalCodes";

	private static final String KEY_AGENT_CODE = "agentCode";

	private static final String KEY_CHARGE_RATE_ID_DESC_MAP = "chargeRateIdDescMap";

	private static final String KEY_CHARGE_GROUP_CODE_DESC_MAP = "chargeGroupCodeDescMap";

	private static final String KEY_BASE_CURRENTCY = "baseCurrency";

	private static final String KEY_ALL_AGENT_NAMES = "allAgents";

	private static final String KEY_IS_ACTIVE_AGENT = "activeAgent";

	private static final String KEY_USER_PASSOWRD_RESETED = "userPWDReseted";

	private static final String KEY_FLIGHT_SEGMENT = "filghtSegment";

	private static Map mapNominalCodes;

	private static Map mapChargeGroupCodeDesc;

	private static String KEY_AIRPORT_NAME = "airportName";

	private static String KEY_ALL_SSR_DESC = "allSsrDesc";

	private static String KEY_FLIGHT_SEGMENT_ID = "flightSegId";

	private static String KEY_ALLOW_RECIEPT_GANERATE = "allowRecieptGenerate";

	/**
	 * 
	 * @param paramString
	 * @param params
	 * @return
	 * @throws ModuleException
	 */
	private final static String getValue(String paramString, String[] params) throws ModuleException {
		int colCount = 1;
		Collection col = null;
		String value = null;
		col = LookupUtils.LookupDAO().getList(paramString, params, colCount);
		Iterator iter = col.iterator();

		if (iter.hasNext()) {
			String[] s = (String[]) iter.next();
			value = s[0];
		}

		return value;
	}

	/**
	 * 
	 * @param paramString
	 * @param params
	 * @return
	 * @throws ModuleException
	 */
	private final static Map getMap(String paramString, String[] params) throws ModuleException {
		int colCount = 2;
		Collection col = null;
		Map<String, String> map = new HashMap<String, String>();
		col = LookupUtils.LookupDAO().getList(paramString, params, colCount);
		Iterator iter = col.iterator();

		while (iter.hasNext()) {
			String[] s = (String[]) iter.next();
			map.put(s[0], s[1]);
		}

		return map;
	}

	/**
	 * Returns the customer id
	 * 
	 * @param customerEmailId
	 * @return
	 * @throws ModuleException
	 */
	public final static int getCustomerId(String customerEmailId) throws ModuleException {
		String customerId = getValue(KEY_CUSTOMER_ID, new String[] { customerEmailId });
		return Integer.parseInt(customerId);
	}

	/**
	 * gets the agent code
	 * 
	 * @param userId
	 * @return
	 * @throws ModuleException
	 */
	public final static String getAgentCode(String userId) throws ModuleException {
		String salesChannelCode = getValue(KEY_AGENT_CODE, new String[] { userId });
		return salesChannelCode;
	}

	/**
	 * returns nominal codes
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static Map getNominalCodes() throws ModuleException {
		if (mapNominalCodes == null) {
			loadNominalCodes();
		}
		return mapNominalCodes;
	}

	/**
	 * loads nominal codes
	 * 
	 * @throws ModuleException
	 */
	private static synchronized void loadNominalCodes() throws ModuleException {
		if (mapNominalCodes == null) {
			mapNominalCodes = getMap(KEY_NOMINAL_CODES, new String[] {});
		}

	}

	public final static Map getChargeGroupCodes() throws ModuleException {
		if (mapChargeGroupCodeDesc == null) {
			loadChargeGroupCodes();
		}
		return mapChargeGroupCodeDesc;
	}

	/**
	 * loads charge group codes
	 * 
	 * @throws ModuleException
	 */
	private static synchronized void loadChargeGroupCodes() throws ModuleException {
		if (mapChargeGroupCodeDesc == null) {
			mapChargeGroupCodeDesc = getMap(KEY_CHARGE_GROUP_CODE_DESC_MAP, new String[] {});
		}

	}

	/**
	 * returns charge group codes
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static Map getChargeRateIdDescMap() throws ModuleException {
		Map mapChargeRateIdDesc = getMap(KEY_CHARGE_RATE_ID_DESC_MAP, new String[] {});
		return mapChargeRateIdDesc;
	}

	/**
	 * gets currncy code
	 * 
	 * @param agentCode
	 * @throws ModuleException
	 */
	public static Entry getAgentCurrency(String agentCode) throws ModuleException {
		Map mapCurrncy = getMap(KEY_AGENT_CURRENTCY, new String[] { agentCode });
		Entry entry = null;

		Set seyKeys = mapCurrncy.keySet();

		if (seyKeys.isEmpty()) {
			entry = null;
		} else {
			entry = (Entry) mapCurrncy.entrySet().iterator().next();
		}

		return entry;

	}

	/**
	 * gets base currency
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public static Entry getBaseCurrency() throws ModuleException {
		Map mapCurrncy = getMap(KEY_BASE_CURRENTCY, new String[] {});
		Entry entry = null;

		Set seyKeys = mapCurrncy.keySet();

		if (seyKeys.isEmpty()) {
			throw new RuntimeException("Cannot retrive base currency");
		} else {
			entry = (Entry) mapCurrncy.entrySet().iterator().next();
		}

		return entry;

	}

	public static Map getAgentNamesMap() throws ModuleException {
		Map mapAgent = getMap(KEY_ALL_AGENT_NAMES, new String[] {});

		return mapAgent;
	}

	/**
	 * get whether agent has done any transactions
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public static boolean getHasTxn(String agentCode) throws ModuleException {
		int intActive = Integer.parseInt(getValue(KEY_IS_ACTIVE_AGENT, new String[] { agentCode }));
		if (intActive > 0) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * validate whether user password has been reseted by administrator
	 * 
	 * @param userId
	 * @return
	 * @throws ModuleException
	 */
	public static boolean getIsUserPWDReseted(String userId) throws ModuleException {
		String strPWDStatus = getValue(KEY_USER_PASSOWRD_RESETED, new String[] { userId });
		return strPWDStatus.equals("Y");
	}

	/**
	 * 
	 * @param flightSegmentID
	 * @return
	 * @throws ModuleException
	 */
	public static String getFlightSegment(String flightSegmentID) throws ModuleException {
		String flightSegmentMap = getValue(KEY_FLIGHT_SEGMENT, new String[] { flightSegmentID });
		return flightSegmentMap;
	}

	/**
	 * 
	 * 
	 * @param airportCode
	 * @return
	 * @throws ModuleException
	 */
	public static String getAirportName(String airportCode) throws ModuleException {
		String strAirPort = getValue(KEY_AIRPORT_NAME, new String[] { airportCode });
		return strAirPort;
	}

	/**
	 * @param langCode
	 * @return
	 * @throws ModuleException
	 */
	public static Map getSsrDesc(String langCode) throws ModuleException {
		Map mapSsrDesc = getMap(KEY_ALL_SSR_DESC, new String[] { langCode });
		return mapSsrDesc;
	}

	public static Collection getActualPayment() throws ModuleException {
		return getCollection("actualPaymentMethod", new String[] {}, 3);
	}

	private final static Collection getCollection(String paramString, String[] params, int colCount) throws ModuleException {
		return LookupUtils.LookupDAO().getList(paramString, params, colCount);
	}

	public static String getFlightSegmentId(String pnrSegId) throws ModuleException {
		String fltSegId = getValue(KEY_FLIGHT_SEGMENT_ID, new String[] { pnrSegId });
		return fltSegId;
	}

	public static boolean isRecieptGenerationEnable(String payMethod) throws ModuleException {
		String allowRecieptGeneration = getValue(KEY_ALLOW_RECIEPT_GANERATE, new String[] { payMethod });
		return allowRecieptGeneration.equals("Y") ? true : false;
	}

}
