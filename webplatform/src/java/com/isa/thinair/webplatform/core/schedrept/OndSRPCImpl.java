package com.isa.thinair.webplatform.core.schedrept;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class OndSRPCImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(OndSRPCImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		String reportTemplate = "ONDReport.jasper";

		String strAgents = request.getParameter("hdnAgents");
		String strBCs = request.getParameter("hdnBCs");
		String strSegments = request.getParameter("hdnSegments");
		String strStations = request.getParameter("hdnStations");
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);

		// To provide Report Format Options
		String strReportFormat = request.getParameter("radRptNumFormat");

		ReportsSearchCriteria search = new ReportsSearchCriteria();
		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = "HTML";
		String reportTempleteRelativePath = "linkReport";

		try {

			search.setReportType(ReportsSearchCriteria.OND_REPORT);
			WPModuleUtils.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1", getReportTemplate(reportTemplate));
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
			}
			if (strAgents != null && !strAgents.equals("")) {
				search.setAgentCode(strAgents);
			}
			if (strBCs != null && !strBCs.equals("")) {
				search.setBookinClass(strBCs);
			}
			if (strSegments != null && !strSegments.equals("")) {
				search.setSegment(strSegments);
			}
			if (strStations != null && !strStations.equals("")) {
				search.setStation(strStations);
			}
			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}

		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.OND_REPORT);
	}

}
