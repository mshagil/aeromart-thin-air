package com.isa.thinair.webplatform.core.schedrept;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

/**
 * @author Navod Ediriweera
 */
public class TransactionReportSRPCImpl extends ScheduleReportCommon {

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String reportOption = request.getParameter("ccReportName");
		String strEntity = request.getParameter("selEntity");
		String strEntityText = request.getParameter("hdnEntityText");
		boolean isDetailReport = new Boolean(request.getParameter("hdnDetail"));

		String id = "UC_REPM_015";
		String reportTemplate = "";
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);
		Map<String, Object> parameters = new HashMap<String, Object>();
		String reportFormat = setReportExportType(value);
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		String reportTempleteRelativePath = null;
		try {
			search.setReportOption(reportOption);
			if (fromDate != null && !fromDate.equals("")) {
				if (reportOption.equals("CC_TRAN_DETAIL")) {
					search.setDateRangeFrom(convertDateFromDetail(fromDate));
				} else {
					search.setDateRangeFrom(convertDate(fromDate));
				}
			}
			if (toDate != null && !toDate.equals("")) {
				if (reportOption.equals("CC_TRAN_DETAIL")) {
					search.setDateRangeTo(convertDateFromDetail(toDate));
				} else {
					search.setDateRangeTo(convertDate(toDate));
				}
			}

			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			search.setEntity(strEntity);
			boolean isIncludeAdvanceCCDetails = true;
			search.setIncludeAdvanceCCDetails(isIncludeAdvanceCCDetails);
			/**
			 * Choose the jasper according to the report options and the resultSet will be generating based on the
			 * option
			 */

			if (reportOption.equals("CC_TRAN") && isDetailReport) {
				reportFormat = setReportExportType("CSV");
				reportTemplate = "CCPaymnetDetailTR.jasper";
				search.setDetailReport(isDetailReport);
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			} else if (reportOption.equals("CC_REFUND") && isDetailReport) {
				reportFormat = setReportExportType("CSV");
				reportTemplate = "CCRefundDetailTR.jasper";
				search.setDetailReport(isDetailReport);
				search.setReportType(ReservationInternalConstants.ExtPayTxStatus.REFUNDED);
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			} else if (reportOption.equals("CC_TRAN")) {
				reportTemplate = "CCPaymnetTR.jasper";
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			} else if (reportOption.equals("CC_TRAN_DETAIL")) {
				reportTemplate = "CCPaymnetDetailTR.jasper";
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			} else if (reportOption.equals("CC_REFUND")) {
				reportTemplate = "CCPaymnetTRRefund.jasper";
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			} else if (reportOption.equals("CC_REFUND_DETAIL")) {
				reportTemplate = "CCRefundDetailTR.jasper";
				search.setReportType(ReservationInternalConstants.ExtPayTxStatus.REFUNDED);
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			} else {
				reportTemplate = "CCPaymnetTRPendingRefund.jasper";
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			}

			this.setReportLogoLocationObject(reportFormat, parameters);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("INCLUDE_ADVANCE_CC_DETAILS", isIncludeAdvanceCCDetails);
			parameters.put("USE_EXISTING_FORMAT", !isIncludeAdvanceCCDetails);
			parameters.put("ENTITY",strEntityText);
			parameters.put("ENTITY_CODE",strEntity);
			
			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormatObject(reportNumFormat, parameters);

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.TRANSACTION_REPORT);
	}
	
	

}
