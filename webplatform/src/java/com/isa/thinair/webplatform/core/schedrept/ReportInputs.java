package com.isa.thinair.webplatform.core.schedrept;

import java.util.Map;

import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class ReportInputs {

	private Map<String, ? extends Object> reportParams;

	private ReportsSearchCriteria criteria;

	private String reportFormat;

	private String reportTempleteRelativePath;

	private String reportName;

	public ReportInputs(Map<String, ? extends Object> reportParams, ReportsSearchCriteria criteria, String reportFormat,
			String reportTempleteRelativePath, String reportName) {
		this.reportParams = reportParams;
		this.criteria = criteria;
		this.reportFormat = reportFormat;
		this.reportTempleteRelativePath = reportTempleteRelativePath;
		this.reportName = reportName;
	}

	public Map<String, ? extends Object> getReportParams() {
		return reportParams;
	}

	public ReportsSearchCriteria getCriteria() {
		return criteria;
	}

	public String getReportFormat() {
		return reportFormat;
	}

	public void setReportFormat(String reportFormat) {
		this.reportFormat = reportFormat;
	}

	public String getReportTempleteRelativePath() {
		return reportTempleteRelativePath;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
}
