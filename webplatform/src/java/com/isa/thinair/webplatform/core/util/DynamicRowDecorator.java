package com.isa.thinair.webplatform.core.util;

import java.util.HashMap;

public interface DynamicRowDecorator<T> {

	public void decorateRow(HashMap<String, Object> row, T record, int i);
}
