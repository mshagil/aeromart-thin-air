package com.isa.thinair.webplatform.core.schedrept;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unchecked")
public class GSTAdditionalReportSRPCImpl extends ScheduleReportCommon {

	@SuppressWarnings("unchecked")
	@Override public ReportInputs composeReportInputs(HttpServletRequest request) {

		String txtToDate = request.getParameter("txtToDate");
		String txtFromDate = request.getParameter("txtFromDate");
		String countryCode = request.getParameter("selCountry");
		String value = request.getParameter("radReportOption");
		String reportMode = request.getParameter("hdnLive");

		String reportName = ScheduleReportUtil.ScheduledReportMetaDataIDs.GST_ADDITIONAL_REPORT;
		String gstAdditionalReportTemplate = "GSTAdditionalReport.jasper";
		String reportTemplateRelativePath = getReportTemplateRelativeLocation(gstAdditionalReportTemplate);

		Map parameters = new HashMap();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);

		ReportsSearchCriteria search = new ReportsSearchCriteria();

		try {
			search.setCountryCode(countryCode);
			search.setStateCode(request.getParameter("hdnStateList"));
			search.setDateRangeTo(txtToDate + " 23:59:59");
			search.setDateRangeFrom(txtFromDate + " 00:00:00");

			if (reportMode != null) {
				if (ScheduleReportUtil.ScheduleWebConstants.REP_LIVE.equals(reportMode)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (ScheduleReportUtil.ScheduleWebConstants.REP_OFFLINE.equals(reportMode)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			parameters.put("FROM_DATE", txtFromDate);
			parameters.put("TO_DATE", txtToDate);
			parameters.put("COUNTRY", WSClientModuleUtils.getCommonServiceBD().getCountryByName(countryCode).getCountryName());
			parameters.put("CARRIER", strCarrier);
			parameters.put("OPTION", value);
			parameters.put("REPORT_MEDIUM", request.getParameter("hdnLive"));

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTemplateRelativePath, reportName);
	}
}
