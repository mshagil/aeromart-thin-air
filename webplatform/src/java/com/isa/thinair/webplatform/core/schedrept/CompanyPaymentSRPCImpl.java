package com.isa.thinair.webplatform.core.schedrept;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;

public class CompanyPaymentSRPCImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(CompanyPaymentSRPCImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		HashMap<String, String> parameters = new HashMap<String, String>();
		String reportFormat = null;
		String reportTempleteRelativePath = null;

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String agents = "";
		String reportView = request.getParameter("hdnReportView");
		String strlive = request.getParameter("hdnLive");
		String strPaySource = request.getParameter("selPaySource");
		String showEticket = globalConfig.getBizParam(SystemParamKeys.SHOW_PAX_ETICKET);
		String stations = request.getParameter("hdnStations");
		// String reportDelimiter = "";
		// String currencies = request.getParameter("currencies");
		if (strPaySource == null) {
			strPaySource = "INTERNAL";
		}

		boolean isSelectedAllAgents = false;
		if (request.getParameter("hdnSelectedAllAgents") != null && request.getParameter("hdnSelectedAllAgents").equals("true")) {
			isSelectedAllAgents = true;
		}

		String localTime = request.getParameter("chkLocalTime");
		String reportViewType = request.getParameter("chkNewview");
		String selFlightType = request.getParameter("selFlightType");
		boolean isSales = request.getParameter("chkSales") != null && request.getParameter("chkSales").trim().equals("Sales");
		boolean isRefunds = request.getParameter("chkRefunds") != null
				&& request.getParameter("chkRefunds").trim().equals("Refunds");
		boolean modificationDetails = request.getParameter("chkModifications") != null
				&& request.getParameter("chkModifications").trim().equals("Modifications");
		boolean isNewView = (reportViewType != null && reportViewType.equalsIgnoreCase("on")) ? true : false;
		String fareDiscountCode = request.getParameter("selFareDiscountCode");
		search.setFareDiscountCode(fareDiscountCode);
		boolean isReportInLocalTime = (localTime != null && localTime.equalsIgnoreCase("on")) ? true : false;

		String chkAddInfo = request.getParameter("chkAddInfo");
		boolean bolAdditionalInfo = (chkAddInfo != null) ? true : false;
		boolean hasSelectedAgents = true;
		
		String strEntity = request.getParameter("selEntity");
		String strEntityText = request.getParameter("hdnEntityText");
		
		try {
			if (isReportInLocalTime) {
				Collection<UserDST> userDST = ((UserPrincipal) request.getUserPrincipal()).getColUserDST();
				for (UserDST dst : userDST) {

					if (dst.getDstStartDateTime() == null
							|| DateUtil.compareZuluDates(dst.getDstStartDateTime(), dst.getDstEndDateTime(),
									DateUtil.parseDate(fromDate, "dd/MM/yyyy"))) {
						search.setGmtFromOffsetInMinutes(dst.getGmtOffsetMinutes());
					}

					if (dst.getDstStartDateTime() == null
							|| DateUtil.compareZuluDates(dst.getDstStartDateTime(), dst.getDstEndDateTime(),
									DateUtil.parseDate(toDate, "dd/MM/yyyy"))) {
						search.setGmtToOffsetInMinutes(dst.getGmtOffsetMinutes());
					}
				}
			}
		} catch (ParseException e) {
			log.error("Error while parsing date", e);
		}

		search.setReportViewNew(isNewView);
		if (!"All".equals(selFlightType)) {
			search.setSearchFlightType(selFlightType);
		}
		if (modificationDetails) {
			search.setModificationDetails(modificationDetails);
		}

		agents = request.getParameter("hdnAgents");

		ArrayList<String> agentCol = new ArrayList<String>();
		String payments = request.getParameter("hdnPayments");

		String id = "UC_REPM_023";
		String templateDetail = "CompanyPaymentReportDetail.jasper";
		// String templateSummary = "CompanyPaymentReportSummary.jasper";
		// Replaces consolidated Summary Report Template
		String templateCompanyPaymentSummary = "CompPayRptSummary.jasper";

		String showPaymentCurrency = "N";
		String showOnlyPaymentBreakDown = "N";
		String showCurrencyWithBreakDown = "N";
		String showAdditionalInfo = "N";

		if (!isSelectedAllAgents) {
			String agentArr[] = agents.split(",");
			for (int r = 0; r < agentArr.length; r++) {
				agentCol.add(agentArr[r]);
			}
		}
		ArrayList<String> paymentCol = new ArrayList<String>();

		try {

			search.setAgentCode(agents);
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(convertDate(fromDate));
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(convertDate(toDate));
			}

			if (strlive != null) {
				if (strlive.equals("LIVE")) {
					String offlineReportParams = AppSysParamsUtil.getEnableOfflineReportParams();
					String[] reportParams = offlineReportParams.split(",");
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
					Date reportFromDate = DateUtil.parseDate(search.getDateRangeFrom(), "dd-MMM-yyyy");
					int noOfLiveReportDays = Integer.parseInt(reportParams[1]);

					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DAY_OF_YEAR, -1 * noOfLiveReportDays);
					Date validReportFromDate = dateFormat.parse(dateFormat.format(cal.getTime()));
					if (validReportFromDate.compareTo(reportFromDate) > 0) {
						search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
					} else {
						search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
					}
				} else if (strlive.equals("OFFLINE")) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			String paymentArr[] = null;
			if (payments.indexOf(",") != -1) {
				paymentArr = payments.split(",");
			} else {
				paymentArr = new String[1];
				paymentArr[0] = payments;
			}

			boolean isSalesModifications = isSales;
			search.setSales(isSales);
			search.setRefund(isRefunds);
			if (AppSysParamsUtil.isModificationFilterEnabled()) {
				isSalesModifications = isSales || modificationDetails;
			}

			String paymentStr = "";
			if (paymentArr != null) {
				for (int r = 0; r < paymentArr.length; r++) {

					if (paymentArr[r].indexOf(".") != -1) {
						paymentStr = paymentArr[r].substring(0, paymentArr[r].indexOf("."));
					} else {
						paymentStr = paymentArr[r];

					}
				}

				String refundCodes[] = { "29", "24", "23", "22", "26", "25", "31", "33", "37" };
				for (int r = 0; r < paymentArr.length; r++) {
					if (paymentArr[r].indexOf(".") != -1) {
						paymentStr = paymentArr[r].substring(0, paymentArr[r].indexOf("."));
					} else {
						paymentStr = paymentArr[r];
					}
					if (paymentStr.equals("28")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[0]);
					}
					if (paymentStr.equals("17")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[1]);
					}
					if (paymentStr.equals("16")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[2]);
					}
					if (paymentStr.equals("15")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[3]);
					}
					if (paymentStr.equals("18")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[4]);
					}
					if (paymentStr.equals("19")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[5]);
						if (strPaySource.equals("EXTERNAL")) {// hard coding all other codes are hard coded
							accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, "10", "11");
						}
					}
					if (paymentStr.equals("30")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[6]);
					}
					if (paymentStr.equals("32")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[7]);
					}
					if (paymentStr.equals("36")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[8]);
					}
				}
			}
			Iterator<String> paymentIte = paymentCol.iterator();
			@SuppressWarnings("unused")
			String pmts = "";
			while (paymentIte.hasNext()) {
				pmts += (String) paymentIte.next() + ",";
			}

			search.setSelectedAllAgents(isSelectedAllAgents);
			if(agentCol.size() == 1 && agentCol.get(0).equals("")){
				agentCol = null;
				hasSelectedAgents = false;
			}
			search.setAgents(agentCol);
			search.setPaymentSource(strPaySource);
			search.setReportViewNew(isNewView);
			
			if(stations !=null && !stations.isEmpty()){
				String stationArr[] = null;
				stationArr = stations.split(",");
				search.setStations(Arrays.asList(stationArr));
				search.setByStation(true);
			}else{
				search.setStations(null);
				search.setByStation(false);
			}

			if (!hasSelectedAgents && hasPrivilege(request, "rpt.ta.comp.rpt")
					&& !hasPrivilege(request, "rpt.ta.comp.all")) {

				UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
				if (hasPrivilege(request, "rpt.ta.comp.stations.full")
						&& userPrincipal.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					search.setOnlyReportingAgents(false);
				} else {					
					search.setGsaCode(userPrincipal.getAgentCode());
					search.setOnlyReportingAgents(true);				
				}
			}else{
				search.setOnlyReportingAgents(false);
			}
			
			String paymentBreakDown = globalConfig.getBizParam(SystemParamKeys.SHOW_BREAKDOWN);
			String reportInPayCur = globalConfig.getBizParam(SystemParamKeys.REPORTS_IN_AGENTS_CURRENCY);
			String chkBase = request.getParameter("chkBase");

			if (paymentBreakDown.equalsIgnoreCase("Y") && strPaySource.equals("INTERNAL")) {
				showOnlyPaymentBreakDown = "Y";
			}

			if ((chkBase == null) && reportInPayCur.equalsIgnoreCase("Y")) {
				showPaymentCurrency = "Y";
			}
			if (showPaymentCurrency.equalsIgnoreCase("Y") && showOnlyPaymentBreakDown.equalsIgnoreCase("Y")) {
				showCurrencyWithBreakDown = "Y";
				showOnlyPaymentBreakDown = "N";
			}

			if (bolAdditionalInfo) {
				showAdditionalInfo = "Y";
				showCurrencyWithBreakDown = "N";
				showOnlyPaymentBreakDown = "N";
			}

			search.setInbaseCurr(true);
			if (globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY).equalsIgnoreCase("Y")) {
				if (reportInPayCur.equalsIgnoreCase("Y")) {
					templateDetail = "CompPayRptDetailCurr.jasper";
					// templateSummary = "CompPayRptSummByCurr.jasper";
					search.setInbaseCurr(false);
					if (hasPrivilege(request, "rpt.ta.comp.base") && (chkBase != null) && (chkBase.equals("on"))) {
						templateDetail = "CompPayRptDetailBase.jasper";
						// templateSummary = "CompPayRptSummByBase.jasper";
						search.setInbaseCurr(true);
					}
				}
			}

			if (reportView.equals("SUMMARY")) {
				search.setReportType(ReportsSearchCriteria.COMPANY_PAYMENT_SUMMARY);
				reportTempleteRelativePath = getReportTemplateRelativeLocation(templateCompanyPaymentSummary);
				search.setPaymentTypes(paymentCol);
			} else {
				search.setReportType(ReportsSearchCriteria.COMPANY_PAYMENT_DETAILS);
				reportTempleteRelativePath = getReportTemplateRelativeLocation(templateDetail);
				search.setPaymentTypes(paymentCol);
			}

			search.setEntity(strEntity);
			
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("AGENT_CODE", agents);
			parameters.put("AGENT_NAME", request.getParameter("hdnAgentName"));
			parameters.put("PAYMENT_MODES", payments);
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("SOURCE", strPaySource);
			if ("Y".equalsIgnoreCase(showEticket)) {
				parameters.put("ETICKET", "Y");
			}
			if (isNewView) {
				parameters.put("VIEW", "ON");
			}
			parameters.put("SHOW_BREAKDOWN", showOnlyPaymentBreakDown);
			parameters.put("SHOW_PAY_CURR", showPaymentCurrency);
			parameters.put("SHOW_CUR_BREAKDOWN", showCurrencyWithBreakDown);
			parameters.put("SHOW_ADDITIONAL_INFO", showAdditionalInfo);

			if (chkBase != null) {
				parameters.put("CHK_BASE", "on");
			}
			parameters.put("DISPLAY_ADDITIONAL_PAYMENT_MODE", new Boolean(AppSysParamsUtil.isAllowCapturePayRef()).toString());
			parameters.put("CARRIER", strCarrier);

			Collection actualPayModes = DatabaseUtil.getActualPayment();
			String showActualPayMode = "N";
			if (actualPayModes != null && actualPayModes.size() > 1 && AppSysParamsUtil.isAllowCapturePayRef()) {
				showActualPayMode = "Y";
			}
			parameters.put("DISPLAY_ACTUAL_PAY_MODE", showActualPayMode);

			parameters.put("SEL_FLIGHT_TYPE", selFlightType);
			parameters.put("SEL_DISCOUNT_TYPE", fareDiscountCode);
			parameters.put("CHK_SALES", isSales ? "Sales" : null);
			parameters.put("CHK_REFUND", isRefunds ? "Refunds" : null);
			parameters.put("CHK_MODIFICATIONS", modificationDetails ? "Modifications" : null);
			parameters.put("CHK_TIME_IN_LOCAL", isReportInLocalTime ? "on" : null);
			parameters.put("TIME_ZONE", isReportInLocalTime ? "LOCAL" : "ZULU");
			parameters.put("ENTITY",strEntityText);
			parameters.put("ENTITY_CODE",strEntity);
			
			parameters.put("language","en");
			
			
			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				reportFormat = "HTML";
			} else if (value.trim().equals("PDF")) {
				reportFormat = "PDF";
			} else if (value.trim().equals("EXCEL")) {
				reportFormat = "EXCEL";
			} else if (value.trim().equals("CSV")) {
				reportFormat = "CSV";
			}
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}

		String reportName = "";
		if (reportView.equals("DETAIL")) {
			reportName = ScheduleReportUtil.ScheduledReportMetaDataIDs.COMPANY_PAYMENT_DETAIL;
		} else {
			reportName = ScheduleReportUtil.ScheduledReportMetaDataIDs.COMPANY_PAYMENT;
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath, reportName);
	}

	public Object getAttribInRequest(HttpServletRequest request, String attributeName) {
		Object objReturn = null;
		try {
			objReturn = request.getAttribute(attributeName);
		} catch (Exception e) {
			log.error("error in retriving attribute" + e.getMessage());
		}
		return objReturn;
	}

	@SuppressWarnings("rawtypes")
	public boolean hasPrivilege(HttpServletRequest request, String privilegeId) {

		Map mapPrivileges = (Map) request.getSession().getAttribute("sesPrivilegeIds");
		boolean has = (mapPrivileges.get(privilegeId) != null) ? true : false;

		return has;
	}

	public String convertDate(String dateString) {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}

	public void setPreferedReportFormat(String prefRptFormat, Map<String, String> parameters) {
		parameters.put("LOCALE", "US");
		parameters.put("DELIMITER", ",");
		if (prefRptFormat != null && !prefRptFormat.equals("")) {
			if (prefRptFormat.equalsIgnoreCase("FR") || prefRptFormat.equalsIgnoreCase("IT")) {
				parameters.put("LOCALE", "IT");
				parameters.put("DELIMITER", ";");
			}
		}
	}

	private static void accumulateNominalCodes(ArrayList<String> paymentCol, boolean isSales, boolean isRefunds, String saleNC,
			String refundNC) {
		if (isSales) {
			paymentCol.add(saleNC);
		}
		if (isRefunds) {
			paymentCol.add(refundNC);
		}
	}
}
