package com.isa.thinair.webplatform.core.constants;

public interface FareTypeCodes {
	public static final String SEGMENT = "S";
	public static final String RETURN = "R";
	public static final String CONNECTION = "C";
	public static final String HALF_RETURN = "HR";
}
