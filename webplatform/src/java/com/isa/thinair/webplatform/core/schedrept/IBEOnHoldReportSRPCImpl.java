package com.isa.thinair.webplatform.core.schedrept;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;




public class IBEOnHoldReportSRPCImpl  extends ScheduleReportCommon  {
	
	private static Log log = LogFactory.getLog(IBEOnHoldReportSRPCImpl.class);
	
	@Override
		public ReportInputs composeReportInputs(HttpServletRequest request) {
	
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String timeOption = request.getParameter("radTimeOption");
		String releaseTimeOption = request.getParameter("radReleaseTimeOption");
		String flightNo = request.getParameter("txtFlightNumber");
		String depFromDate = request.getParameter("txtFromDepDate");
		String depToDate = request.getParameter("txtToDepDate");
		String rptOption = request.getParameter("radOption");
		String bookingFromDate = request.getParameter("txtBookFromDate");
		String bookingToDate = request.getParameter("txtBookToDate");
	
		String consideredTimePeriod = request.getParameter("txtReleaseHours");
	
		String bookCurrentStatus = request.getParameter("selBookCurStatus");
		String id = "UC_REPM_006";
		String reportTemplate = "IBEOnHoldPassengerReportForFlight.jasper";
		String reportFormat = setReportExportType(value);
		String agents = "";
		Map<String, String> parameters = new HashMap<String, String>();
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		String reportTempleteRelativePath = null;
		
	try{
		if (rptOption.equals("DATE_RANGE")) {
			if (releaseTimeOption.equals("LOCAL")) {// Mon Apr 01 04:30:00 IST 2013
				LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(WPModuleUtils.getAirportBD());
				String agentStation = WPModuleUtils.getDataExtractionBD().getAgentStationCode();
				Date localFromDate = timeAdder.getLocalDateTime(agentStation, new Date(convertDate(fromDate)
						+ "  00:00:00"));
				Date localToDate = timeAdder.getLocalDateTime(agentStation, new Date(convertDate(toDate)
						+ "  23:59:59"));
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					search.setDateRangeFrom(sdf.format(localFromDate));
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					search.setDateRangeTo(sdf.format(localToDate));
				}
			} else {
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					search.setDateRangeFrom(convertDate(fromDate) + "  00:00:00");
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					search.setDateRangeTo(convertDate(toDate) + "  23:59:59");
				}
			}
		} else {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
			search.setDateRangeFrom(dateFormat.format(new Date()));
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, Integer.valueOf(consideredTimePeriod));
			search.setDateRangeTo(dateFormat.format(cal.getTime()));
		}
	
		if (!StringUtil.isNullOrEmpty(depToDate) && !StringUtil.isNullOrEmpty(depFromDate)) {
			search.setDepartureDateRangeTo(convertDate(depToDate));
			search.setDepartureDateRangeFrom(convertDate(depFromDate));
		}
	
		if (!StringUtil.isNullOrEmpty(bookingFromDate) && !StringUtil.isNullOrEmpty(bookingToDate)) {
			search.setDateRange2From(convertDate(bookingFromDate));
			search.setDateRange2To(convertDate(bookingToDate));
		}
		if (!StringUtil.isNullOrEmpty(consideredTimePeriod)) {
			search.setConTime(consideredTimePeriod);
		}
		search.setStation(((UserPrincipal) request.getUserPrincipal()).getAgentStation());
		search.setReportType(rptOption);
		search.setFlightNumber(flightNo);
		search.setAgentCode(agents);
		search.setStatus(bookCurrentStatus);
	
		if (timeOption.trim().equals("LOCAL")) {
			search.setShowInLocalTime(true);
		} else if (timeOption.trim().equals("ZULU")) {
			search.setShowInLocalTime(false);
		}
	
		parameters.put("FROM_DATE", search.getDateRangeFrom());
		parameters.put("TO_DATE", search.getDateRangeTo());
		parameters.put("FLIGHT_NO", flightNo.toUpperCase());
		parameters.put("ID", id);
	
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME)
				+ " Reservation System";
		parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);
		this.setReportLogoLocation(reportFormat, parameters);
		reportTempleteRelativePath  = getReportTemplateRelativeLocation(reportTemplate);
	
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.IBE_ONHOLD_REPORT);
	}
}
