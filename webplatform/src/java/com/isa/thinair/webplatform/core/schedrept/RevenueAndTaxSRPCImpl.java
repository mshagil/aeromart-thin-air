package com.isa.thinair.webplatform.core.schedrept;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class RevenueAndTaxSRPCImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(CompanyPaymentSRPCImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		String bookedFromDate = request.getParameter("txtBookedFromDate");
		String bookedToDate = request.getParameter("txtBookedToDate");

		String strAgents = request.getParameter("hdnAgents");
		String strBCs = request.getParameter("hdnBCs");
		String strSegments = request.getParameter("hdnSegments");
		String strStations = request.getParameter("hdnStations");
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);
		String strCarrierCodes = request.getParameter("hdnCarrierCode");
		String strChargesCodes = request.getParameter("hdnChargesCode");
		String strChargesGroupCodes = request.getParameter("hdnChargeGroupCodes");

		/* get Report Type from jsp */
		String strReportType = request.getParameter("selReportType");

		// To provide Report Format Options
		String strReportFormat = request.getParameter("radRptNumFormat");
		

		String includeCabinClass = request.getParameter("chkCabinClass");
		if (includeCabinClass != null && includeCabinClass.trim().equals("CabinClass")) {
			includeCabinClass = "Y";
		} else {
			includeCabinClass = "N";
		}

		ReportsSearchCriteria search = new ReportsSearchCriteria();
		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = "CSV";
		String reportTempleteRelativePath = "linkReport";

		if (strReportType.equals(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY)) {
			reportTempleteRelativePath = getReportTemplateRelativeLocation("RevenueByAgentSummeryReport.jasper");
		}

		try {

			search.setReportType(ReportsSearchCriteria.REVENUE_TAX_REPORT);

			/* Default Report : Detailed */
			if (strReportType != null && !strReportType.equals(""))
				search.setReportOption(strReportType);
			else
				search.setReportOption(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_DETAIL);

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
			}
			if ((bookedFromDate != null && !bookedFromDate.isEmpty()) && (bookedToDate != null && !bookedToDate.isEmpty())) {
				search.setDateRange2From(convertDate(bookedFromDate) + " 00:00:00");
				search.setDateRange2To(convertDate(bookedToDate) + " 23:59:59");
			}
			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}
			if (strAgents != null && !strAgents.equals("")) {
				search.setAgentCode(strAgents);
			}
			if (strBCs != null && !strBCs.equals("")) {
				search.setBookinClass(strBCs);
			}
			search.setCos(includeCabinClass);
			
			if (strSegments != null && !strSegments.equals("")) {
				search.setSegment(strSegments);
			}
			if (strStations != null && !strStations.equals("")) {
				search.setStation(strStations);
			}
			if (strChargesCodes != null && !strChargesCodes.isEmpty()) {
				search.setCharge(strChargesCodes);
			}
			if (strChargesGroupCodes != null && !strChargesGroupCodes.isEmpty()) {
				search.setChargeGrooup(strChargesGroupCodes);
			}
			if (strCarrierCodes != null && !strCarrierCodes.equals("")) {
				search.setCarrierCode(strCarrierCodes);
			} else {
				search.setCarrierCode(CommonUtil.getCollectionElementsStr(RevenueAndTaxSRPCImpl.getUserCarrierCodes(request)));
			}

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}

		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.REVENUE_AND_TAX);
	}

	public static Collection<String> getUserCarrierCodes(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(ScheduleWebConstants.SES_CURR_USER);
		if (user.getCarriers() == null || user.getCarriers().size() < 1) {
			throw new ModuleException("module.user.carrier");
		}
		return user.getCarriers();
	}

}
