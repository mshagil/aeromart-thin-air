/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.webplatform.core.commons;

import java.util.Map;

/**
 * class to represent menu
 * 
 * @author Lasantha Pambagoda
 */
public class Menu {

	private String name;

	private String description;

	private String url;

	private String toolTip;

	private Map submenus;

	private Map functions;

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the functions.
	 */
	public Map getFunctions() {
		return functions;
	}

	/**
	 * @param functions
	 *            The functions to set.
	 */
	public void setFunctions(Map functions) {
		this.functions = functions;
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Returns the submenus.
	 */
	public Map getSubmenus() {
		return submenus;
	}

	/**
	 * @param submenus
	 *            The submenus to set.
	 */
	public void setSubmenus(Map submenus) {
		this.submenus = submenus;
	}

	/**
	 * @return Returns the toolTip.
	 */
	public String getToolTip() {
		return toolTip;
	}

	/**
	 * @param toolTip
	 *            The toolTip to set.
	 */
	public void setToolTip(String toolTip) {
		this.toolTip = toolTip;
	}

	/**
	 * @return Returns the url.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            The url to set.
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * method to get whether this menu has sub menus or not
	 */
	public boolean hasSubMenus() {
		boolean has = false;
		if (submenus != null && !(submenus.size() > 0)) {
			has = true;
		}
		return has;
	}

	/**
	 * method to get whether this menu has menu functions or not
	 */
	public boolean hasMenuFunctions() {
		boolean has = false;
		if (functions != null && !(functions.size() > 0)) {
			has = true;
		}
		return has;
	}
}
