package com.isa.thinair.webplatform.core.schedrept;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.util.LookupUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class OriginCountryOfCallSRPCImpl extends ScheduleReportCommon {

	private static final String REPORT_ID = "UC_REPM_088";

	private static final String COUNTRY_LIST = "countrylist";

	private static final String TXT_FROM_DATE = "txtFromDate";

	private static final String TXT_TO_DATE = "txtToDate";

	private static final String REPORT_OPTION = "radReportOption";

	private static final String HDN_COUNTRIES = "hdnCountries";

	private static final String REPORT_FORMAT = "radRptNumFormat";


	@Override public ReportInputs composeReportInputs(HttpServletRequest request) {

		String fromDate = request.getParameter(TXT_FROM_DATE);
		String toDate = request.getParameter(TXT_TO_DATE);
		String value = request.getParameter(REPORT_OPTION);
		String strCountryList = request.getParameter(HDN_COUNTRIES);
		List<String> countryList = new ArrayList<>();
		if (!StringUtil.isNullOrEmpty(strCountryList)) {
			countryList = Arrays.asList(strCountryList.split(","));
		}
		List<String> countryCodes = new ArrayList<>();
		List<String[]> countries = LookupUtils.LookupDAO().getTwoColumnMap(COUNTRY_LIST);
		for (String[] countryDetails : countries) {
			if (countryList.contains(countryDetails[1])) {
				countryCodes.add(countryDetails[0]);
			}
		}

		String templateOriginCountrySummary = "OriginCountryOfCallSummaryReport.jasper";

		Map<String, String> parameters = new HashMap<>();
		ReportsSearchCriteria search = new ReportsSearchCriteria();

		String reportFormat = setReportExportType(value);
		String reportTempleteRelativePath;

		try {

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				search.setDateRangeFrom(convertDate(fromDate));
			}

			if (!StringUtil.isNullOrEmpty(toDate)) {
				search.setDateRangeTo(convertDate(toDate));
			}

			if (countryCodes != null && countryCodes.size() > 0) {
				search.setSelectedCountries(countryCodes);
			}

			reportTempleteRelativePath = getReportTemplateRelativeLocation(templateOriginCountrySummary);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", REPORT_ID);

			parameters.put("DETAIL_ABS_TARGET", "");

			String reportNumFormat = request.getParameter(REPORT_FORMAT);
			setPreferedReportFormat(reportNumFormat, parameters);

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.ORIGIN_COUNTRY_OF_CALL_REPORT);

	}
}
