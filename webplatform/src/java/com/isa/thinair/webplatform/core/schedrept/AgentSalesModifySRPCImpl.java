package com.isa.thinair.webplatform.core.schedrept;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class AgentSalesModifySRPCImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(AgentSalesModifySRPCImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");

		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);

		String reportType = request.getParameter("selReportType");

		String strReportFormat = request.getParameter("radRptNumFormat");

		String flighNo = request.getParameter("txtFlightNumber");
		String flightType = request.getParameter("selFlightType");
		String COS = request.getParameter("selCOS");
		String journeyType = request.getParameter("selJourneyType");
		String depAirport = request.getParameter("selDept");
		String arrAirport = request.getParameter("selArival");
		String fareBasisCode = request.getParameter("selBasisCode");
		String stations = request.getParameter("selStations");
		String country = request.getParameter("selCountry");
		String paidCurrency = request.getParameter("selPaidCurrency");

		String agentCity = request.getParameter("selAgentCity");
		String agentTerritory = request.getParameter("selTerritory");
		String paxTypeCode = request.getParameter("selPaxType");

		String strUsrerId = request.getUserPrincipal().getName();

		String agents = request.getParameter("hdnAgents");

		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = "CSV";
		String reportTempleteRelativePath = "linkReport";
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		try {
			String agentArr[] = agents.split(",");
			ArrayList<String> agentCol = new ArrayList<String>();
			for (int r = 0; r < agentArr.length; r++) {
				agentCol.add(agentArr[r]);
			}
			
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}

			if (!StringUtil.isNullOrEmpty(toDate)) {
				search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
			}

			if (agentCol != null && agentCol.size() == 1) {
				search.setAgentCode(agents);
			}

			if (!StringUtil.isNullOrEmpty(strReportFormat)) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}

			if (!StringUtil.isNullOrEmpty(flighNo) && !flighNo.equalsIgnoreCase("All")) {
				search.setFlightNumber(flighNo);
			}

			if (!StringUtil.isNullOrEmpty(flightType) && !flightType.equalsIgnoreCase("All")) {
				search.setSearchFlightType(flightType);
			}

			if (!StringUtil.isNullOrEmpty(COS) && !COS.equalsIgnoreCase("All")) {
				search.setCos(COS);
			}

			if (!StringUtil.isNullOrEmpty(journeyType) && !journeyType.equals("0")) {
				if (journeyType.equals("1")) {
					search.setJourneyType("N");
				} else {
					search.setJourneyType("Y");
				}
			}

			if (!StringUtil.isNullOrEmpty(depAirport) && !depAirport.equalsIgnoreCase("All")) {
				search.setOriginAirport(depAirport);
			}

			if (!StringUtil.isNullOrEmpty(arrAirport) && !arrAirport.equalsIgnoreCase("All")) {
				search.setDestinationAirport(arrAirport);
			}

			if (!StringUtil.isNullOrEmpty(fareBasisCode) && !fareBasisCode.equalsIgnoreCase("All")) {
				search.setFareBasisCode(fareBasisCode);
			}

			if (!StringUtil.isNullOrEmpty(stations) && !stations.equalsIgnoreCase("All")) {
				search.setStation(stations);
			}

			if (!StringUtil.isNullOrEmpty(country) && !country.equalsIgnoreCase("All")) {
				search.setCountryCode(country);
			}

			if (!StringUtil.isNullOrEmpty(paidCurrency) && !paidCurrency.equalsIgnoreCase("All")) {
				search.setCurrencies(paidCurrency);
			}

			if (!StringUtil.isNullOrEmpty(agentCity) && !agentCity.equalsIgnoreCase("All")) {
				search.setAddressCity(agentCity);
			}

			if (!StringUtil.isNullOrEmpty(agentTerritory) && !agentTerritory.equalsIgnoreCase("All")) {
				search.setTerritory(agentTerritory);
			}

			if (!StringUtil.isNullOrEmpty(paxTypeCode) && !paxTypeCode.equalsIgnoreCase("All")) {
				search.setPaxCategory(paxTypeCode);
			}

			if (!StringUtil.isNullOrEmpty(reportType) && !reportType.equalsIgnoreCase("All")) {
				search.setReportType(reportType);
			}

			if (!StringUtil.isNullOrEmpty(fromDate) && !StringUtil.isNullOrEmpty(toDate)) {
				// ModuleServiceLocator.getDataExtractionBD().getAgentSalesModifyRefundReports(search);
			}

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}

		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.AGENT_SALES_REFUND_REPORT);
	}

}
