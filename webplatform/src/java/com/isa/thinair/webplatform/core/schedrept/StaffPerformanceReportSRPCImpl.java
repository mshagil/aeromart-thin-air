package com.isa.thinair.webplatform.core.schedrept;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

/**
 * @author Janaka Padukka
 * 
 */
public class StaffPerformanceReportSRPCImpl extends ScheduleReportCommon {

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		String adminServerUrl = "http://" + globalConfig.getAdminServerAddressAndPort() + "/airadmin";
		Calendar cal = null;
		String formattedDate = null;
		String reportFormat = null;
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String userIds = request.getParameter("hdnUserCode");
		String paymentTypes = request.getParameter("hdnBookingPaymentType");
		String period = request.getParameter("radReportPeriod");
		String id = "UC_REPM_010";
		String reportTemplate = "PerformanceOfSalesStaff.jasper";
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);
		Vector<String> usersCol = new Vector<String>();
		Vector<String> paymentTypeCol = new Vector<String>();
		String users[] = userIds.split(",");
		String paymentTypeIds[] = paymentTypes.split(",");

		for (int i = 0; i < users.length; i++) {
			usersCol.add(users[i]);
		}

		for (int i = 0; i < paymentTypeIds.length; i++) {
			paymentTypeCol.add(paymentTypeIds[i]);
		}

		Map<String, Object> parameters = new HashMap<String, Object>();

		String reportTempleteRelativePath = null;
		ReportsSearchCriteria search = new ReportsSearchCriteria();

		try {
			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (period.trim().equals("Today")) {
				cal = new GregorianCalendar();

				formattedDate = formatter.format(cal.getTime());
				search.setDateRangeFrom(convertDate(formattedDate));
				search.setDateRangeTo(convertDate(formattedDate));
				parameters.put("FROM_DATE", formattedDate);
				parameters.put("TO_DATE", formattedDate);
			} else if (period.trim().equals("ThisWeek")) {
				cal = new GregorianCalendar();

				cal.set(Calendar.DAY_OF_WEEK, 1);
				formattedDate = formatter.format(cal.getTime());
				parameters.put("FROM_DATE", formattedDate);
				search.setDateRangeFrom(convertDate(formattedDate));

				cal.set(Calendar.DAY_OF_WEEK, 7);
				formattedDate = formatter.format(cal.getTime());
				search.setDateRangeTo(convertDate(formattedDate));
				parameters.put("TO_DATE", formattedDate);

			} else if (period.trim().equals("ThisMonth")) {
				cal = new GregorianCalendar();

				cal.set(Calendar.DAY_OF_MONTH, 1);
				formattedDate = formatter.format(cal.getTime());
				parameters.put("FROM_DATE", formattedDate);
				search.setDateRangeFrom(convertDate(formattedDate));

				cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				formattedDate = formatter.format(cal.getTime());
				parameters.put("TO_DATE", formattedDate);
				search.setDateRangeTo(convertDate(formattedDate));
			} else if (period.trim().equals("ThisYear")) {
				cal = new GregorianCalendar();

				cal.set(Calendar.DAY_OF_YEAR, 1);
				formattedDate = formatter.format(cal.getTime());
				parameters.put("FROM_DATE", formattedDate);
				search.setDateRangeFrom(convertDate(formattedDate));

				cal.set(Calendar.DAY_OF_YEAR, cal.getActualMaximum(Calendar.DAY_OF_YEAR));
				formattedDate = formatter.format(cal.getTime());
				parameters.put("TO_DATE", formattedDate);
				search.setDateRangeTo(convertDate(formattedDate));
			} else if (period.trim().equals("Other")) {
				if (fromDate != null && !fromDate.equals("")) {
					search.setDateRangeFrom(convertDateTime(fromDate));
				}
				if (toDate != null && !toDate.equals("")) {
					search.setDateRangeTo(convertDateTime(toDate));
				}
				parameters.put("FROM_DATE", fromDate);
				parameters.put("TO_DATE", toDate);
			}

			search.setUsers(usersCol);
			search.setPaymentTypes(paymentTypeCol);

			parameters.put("OPTION", value);
			parameters.put("ID", id);
			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("PAYMENT_TYPES", paymentTypes);

			if (AppSysParamsUtil.isPromoCodeEnabled()) {
				parameters.put("SHW_DISCOUNT", true);
			} else {
				parameters.put("SHW_DISCOUNT", false);
			}

			reportFormat = setReportExportType(value);
			if (HTML.equals(reportFormat)) {
				parameters.put("IMG", adminServerUrl + "/images/" + strLogo);
			} else if (PDF.equals(reportFormat)) {
				parameters.put("IMG", getReportTemplate(strLogo));
			}
			parameters.put("DETAIL_ABS_TARGET", adminServerUrl + "/private/master/");

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			parameters.put("LOCALE", "US");
			parameters.put("DELIMITER", ",");
			if (reportNumFormat != null && !reportNumFormat.equals("")) {
				if (reportNumFormat.equalsIgnoreCase("FR") || reportNumFormat.equalsIgnoreCase("IT")) {
					parameters.put("LOCALE", "IT");
					parameters.put("DELIMITER", ";");
				}
			}

			reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.STAFF_PERFORMANCE_REPORT);

	}

}
