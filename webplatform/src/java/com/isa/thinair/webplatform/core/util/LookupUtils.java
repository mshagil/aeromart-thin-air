package com.isa.thinair.webplatform.core.util;

import com.isa.thinair.platform.api.LookupServiceFactory;

public class LookupUtils {

	private static final String dao_path = "isa:base://modules/webplatform?id=WebplatformDAO";
	private static final String web_service_path = "isa:base://modules/webplatform?id=WebServicesDAO";
	private static final String lcc_dao_path = "isa:base://modules/webplatform?id=LCCWebplatformDAO";

	public static WebplatformDAO LookupDAO() {
		WebplatformDAO webplatformDAO = (WebplatformDAO) LookupServiceFactory.getInstance().getBean(dao_path);
		return webplatformDAO;
	}

	public static WebServicesDAO getWebServiceDAO() {
		return (WebServicesDAO) LookupServiceFactory.getInstance().getBean(web_service_path);

	}

	public static LCCWebplatformDAO lccLookupDAO() {
		LCCWebplatformDAO lccWebplatformDAO = (LCCWebplatformDAO) LookupServiceFactory.getInstance().getBean(lcc_dao_path);
		return lccWebplatformDAO;
	}
}
