package com.isa.thinair.webplatform.core.schedrept;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class AgentChargeAdjustmentReportSenderImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(AgentChargeAdjustmentReportSenderImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {
		String reportTempleteRelativePath = null;

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String selectedCarriers = request.getParameter("hdnSelectedCarriers");
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);

		String chkPayTypeR = request.getParameter("chkTypeR");
		String chkPayTypeC = request.getParameter("chkTypeC");
		String chkPayTypeD = request.getParameter("chkTypeD");
		String chkPayTypeRR = request.getParameter("chkTypeRR");
		String chkPayTypeA = request.getParameter("chkTypeA");
		String chkPayTypeRV = request.getParameter("chkTypeRV");
		String chkPayTypeQ = request.getParameter("chkTypeQ");
		String chkPayTypeBSP = request.getParameter("chkTypeBSP");

		String agents = "";
		String users = "";
		String id = "UC_REPM_082";

		String strCarrier = AppSysParamsUtil.getDefaultCarrierCode() + " Reservation System";

		ReportsSearchCriteria search = new ReportsSearchCriteria();

		if (request.getAttribute("displayAgencyMode") != null && request.getAttribute("displayAgencyMode").equals("0")) {
			agents = (String) request.getAttribute("currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}

		String agentArr[] = agents.split(",");
		ArrayList<String> agentCol = new ArrayList<String>();
		for (int r = 0; r < agentArr.length; r++) {
			agentCol.add(agentArr[r]);
		}
		users = request.getParameter("hdnUsers");
		ArrayList<String> userCol = new ArrayList<String>();
		if (!"".equals(users)) {
			String userArr[] = users.split(",");
			for (int r = 0; r < userArr.length; r++) {
				userCol.add(userArr[r]);
			}
		}

		if (toDate != null && !toDate.equals("")) {
			search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
		}
		if (fromDate != null && !fromDate.equals("")) {
			search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
		}
		
		Collection<Integer> trnsColl = new HashSet<Integer>();

		if (getNotNull(chkPayTypeR))
			trnsColl.add(AgentTransactionNominalCode.ACC_SALE.getCode());
		if (getNotNull(chkPayTypeC))
			trnsColl.add(AgentTransactionNominalCode.ACC_CREDIT.getCode());
		if (getNotNull(chkPayTypeD))
			trnsColl.add(AgentTransactionNominalCode.ACC_DEBIT.getCode());
		if (getNotNull(chkPayTypeRR))
			trnsColl.add(AgentTransactionNominalCode.ACC_REFUND.getCode());
		if (getNotNull(chkPayTypeA))
			trnsColl.add(AgentTransactionNominalCode.PAY_INV_CASH.getCode());
		if (getNotNull(chkPayTypeQ))
			trnsColl.add(AgentTransactionNominalCode.PAY_INV_CHQ.getCode());
		if (getNotNull(chkPayTypeRV))
			trnsColl.add(AgentTransactionNominalCode.ACC_PAY_REVERSE.getCode());
		if (getNotNull(chkPayTypeBSP))
			trnsColl.add(AgentTransactionNominalCode.BSP_SETTLEMENT.getCode());

		search.setAgents(agentCol);
		search.setAgentCode(agents);
		search.setUsers(userCol);

		if (selectedCarriers != null && !selectedCarriers.isEmpty()) {
			List<String> carrierList = Arrays.asList(selectedCarriers.split(","));
			if (!carrierList.contains("ALL")) {
				search.setCarrierCodes(carrierList);
			}
		}

		search.setNominalCodes(trnsColl);

		if (strlive != null) {
			if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		String agentChargeAdjustmentRpt = "agentChargeAdjustmentsReport.jasper";

		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);
		try {
			search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);

			reportTempleteRelativePath = getReportTemplateRelativeLocation(agentChargeAdjustmentRpt);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormat(reportNumFormat, parameters);
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.AGENT_CHARGE_ADJUSTMENT_REPORT);

	}

	private static boolean getNotNull(String str) {
		return ((str != null) && !(str.trim().equals(""))) ? true : false;
	}
}
