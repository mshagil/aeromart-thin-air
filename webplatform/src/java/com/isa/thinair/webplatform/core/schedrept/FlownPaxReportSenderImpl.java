package com.isa.thinair.webplatform.core.schedrept;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class FlownPaxReportSenderImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(FlownPaxReportSenderImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {
		String reportTempleteRelativePath = null;

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String flightNumber = request.getParameter("txtFlightNumber");
		String value = request.getParameter("radReportOption");

		String id = "UC_REPM_099";
		String flownPassengerRpt = "FlownPassengerReport.jasper";
		ReportsSearchCriteria search = new ReportsSearchCriteria();

		if (fromDate != null && !fromDate.equals("")) {
			search.setDateRangeFrom(getLocalDateForMOP(fromDate + " 00:00:00"));
		}

		if (toDate != null && !toDate.equals("")) {
			search.setDateRangeTo(getLocalDateForMOP(toDate + " 23:59:59"));
		}
		if (flightNumber != null && !flightNumber.equals("")) {
			search.setFlightNumber(flightNumber);
		}

		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);
		try {
			search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);

			reportTempleteRelativePath = getReportTemplateRelativeLocation(flownPassengerRpt);
			
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("OPTION", value);
			//parameters.put("REPORT_NAME", search.getReportType());

			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormat(reportNumFormat, parameters);
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.FLOWN_PAX_REPORT);

	}

	private static String getLocalDateForMOP(String str) {
		String strNew = "";
		SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat convsdt = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		long localt = 4 * 3600 * 1000;
		Date serchDate = null;
		try {
			serchDate = sdt.parse(str);
			long searct = serchDate.getTime();
			searct = searct - localt;
			serchDate = new Date(searct);
			strNew = convsdt.format(serchDate);
		} catch (Exception e) {
			log.error("Error in prsingdate in MOde of payment" + e);
		}
		return strNew;
	}

}
