package com.isa.thinair.webplatform.core.util;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

public class StringUtil {

	/**
	 * encrypts a given string
	 * 
	 * @param str
	 * @return
	 * @throws ModuleRuntimeException
	 */
	public static String encrypt(String str) throws ModuleRuntimeException {
		String encryptedStr = null;
		try {
			encryptedStr = WPModuleUtils.getCryptoServiceBD().encrypt(str);
		} catch (ModuleException e) {
			throw new ModuleRuntimeException(e, e.getExceptionCode(), Constants.MODULE_CODE);
		}

		return encryptedStr;
	}

	/**
	 * decrypts a given string
	 * 
	 * @param encryptedStr
	 * @return
	 * @throws ModuleRuntimeException
	 */
	public static String decrypt(String encryptedStr) throws ModuleRuntimeException {
		String str = null;
		try {
			str = WPModuleUtils.getCryptoServiceBD().decrypt(encryptedStr);
		} catch (ModuleException e) {
			throw new ModuleRuntimeException(e, e.getExceptionCode(), Constants.MODULE_CODE);
		}

		return str;
	}

}
