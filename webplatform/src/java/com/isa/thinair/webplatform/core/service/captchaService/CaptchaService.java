package com.isa.thinair.webplatform.core.service.captchaService;

import com.isa.thinair.webplatform.core.service.captchaService.captchaEngine.AccelAeroCaptchaEngine;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;
import com.octo.captcha.service.image.ImageCaptchaService;

public class CaptchaService {

	private static ImageCaptchaService instance = null;
	
	private CaptchaService(){
		
	}

	public static ImageCaptchaService getInstance() {
		if (instance == null){
			instance = new DefaultManageableImageCaptchaService(new FastHashMapCaptchaStore(), new AccelAeroCaptchaEngine(), 180,
					100000, 75000);
			return instance;
		}
		else {
			return instance;
		}
	}

}
