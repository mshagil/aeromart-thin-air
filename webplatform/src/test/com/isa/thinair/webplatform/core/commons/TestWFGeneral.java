package com.isa.thinair.webplatform.core.commons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

import com.isa.thinair.webplatform.core.util.StringUtil;
import com.isa.thinair.webplatform.core.util.WebplatformDAO;

/**
 * @author Sudheera
 * 
 */
public class TestWFGeneral extends PlatformTestCase {

    /**
     * 
     * @throws Exception .
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();

    }

    /**
     * 
     * @throws Exception .
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    // public void testJdbcTemplate() throws ModuleException {
    // DataSource dataSource = (DataSource)
    // LookupServiceFactory.getInstance().getBean("isa:base://modules?id=myDataSource");
    // JdbcTemplate jt = new JdbcTemplate(dataSource);
    //
    //
    // List list = (List) jt.query(new PreparedStatementCreator() {
    //
    // public PreparedStatement createPreparedStatement(Connection conn) throws
    // SQLException {
    // PreparedStatement ps = conn.prepareStatement("select user_id from
    // t_user");
    //
    // return ps;
    // }
    //
    // }, new ResultSetExtractor() {
    //
    // public Object extractData(ResultSet rs) throws SQLException,
    // DataAccessException {
    // List list = new ArrayList();
    // while (rs.next()) {
    //
    // System.out.println(rs.getString("USER_ID"));
    // list.add(rs.getString("USER_ID"));
    // }
    // return list;
    // }
    //
    // });
    //
    // }

    // public void testGetSalesChannelCode() throws ModuleException {
    // String userId = "Janaki";
    //
    // int salesChannelCode = DatabaseUtil.getSalesChannelCode(userId);
    //
    // assertTrue(salesChannelCode != 0);
    // System.out.println("salesChannelCode:" + salesChannelCode);
    //
    // }

    // public void testGetAgentCode() throws ModuleException {
    // String userId = "Janaki";
    //    
    // String agentCode = DatabaseUtil.getAgentCode(userId);
    //    
    // assertNotNull(agentCode);
    // System.out.println("agentCode:" + agentCode);
    //    
    // }

    // public void testEncryption() throws ModuleException {
    // String userId = "password";
    // String encryptrdUserId = StringUtil.encrypt(userId);
    //        
    //
    // assertNotNull(encryptrdUserId);
    // assertNotSame(userId, encryptrdUserId);
    //        
    // System.out.println("encryptrdUserId:" + encryptrdUserId);
    //
    // }

    // public void testReportingAgents() throws ModuleException {
    // String agentCode = "CMB001";
    // String reportingAgents =
    // SelectListGenerator.createReportingAgentArray(agentCode);
    // System.out.println("reportingAgents:" + reportingAgents);
    //    
    // }

    // public void testGetNominalCodes() throws ModuleException {
    // Map map = DatabaseUtil.getNominalCodes();
    // Set keys = map.keySet();
    //        
    // System.out.println("map:" + map);
    //
    // }

    // public void testCreateCabinClassCodeArray() throws ModuleException {
    // String str = SelectListGenerator.createCabinClassCodeArray();
    //        
    // System.out.println(str);
    //
    // }

    // public void testCreateCreditCardTypesArray() throws ModuleException {
    // String str = SelectListGenerator.createCreditCardTypesArray();
    //        
    // System.out.println(str);
    //
    // }

    // public void testPaxTitleArray() throws ModuleException {
    // String str = SelectListGenerator.createPaxTitleArray();
    //        
    // System.out.println(str);

    // }

    // public void testGetCustomerId() throws ModuleException {
    // int customerId = DatabaseUtil.getCustomerId("nilindra@jkcsworld.com");
    // System.out.println("customerId " + customerId);
    // }

    // public void testAgentCurrency() throws ModuleException {
    // String agentCode="CMB1";
    // Entry currency = DatabaseUtil.getAgentCurrency(agentCode);
    // System.out.println("currency " + currency.toString());
    // }
    //    
    // public void testBaseCurrency() throws ModuleException {
    // String agentCode="CMB1";
    // Entry currency = DatabaseUtil.getAgentCurrency(agentCode);
    // System.out.println("currency " + currency.toString());
    // }

    // public void testBaseCurrency() throws ModuleException {
    // String html = SelectListGenerator.createAllAgentsArray();
    // System.out.println("currency " + html);
    // }

    public void testAgentCurrency() throws ModuleException {
        Map map = DatabaseUtil.getChargeRateIdDescMap();
        System.out.println("ChargeRateIdDescMap " + map);
    }

}
