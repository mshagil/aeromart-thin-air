package com.isa.thinair.webservices.tests;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRS;
import org.opentravel.ota._2003._05.AAOTAPaxCreditRS;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirFlifoRS;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAAirScheduleRS;
import org.opentravel.ota._2003._05.OTAResRetrieveRS;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.helpers.AddONDRequest;
import com.isa.thinair.webservices.helpers.AddUserNotesRequest;
import com.isa.thinair.webservices.helpers.AgentCreditLimitRequest;
import com.isa.thinair.webservices.helpers.AvailabilityRequest;
import com.isa.thinair.webservices.helpers.BalancePaymentRequest;
import com.isa.thinair.webservices.helpers.BalancesForAddONDRequest;
import com.isa.thinair.webservices.helpers.BalancesForCancelResRequest;
import com.isa.thinair.webservices.helpers.BalancesForModONDRequest;
import com.isa.thinair.webservices.helpers.BookRequest;
import com.isa.thinair.webservices.helpers.CancelReservationRequest;
import com.isa.thinair.webservices.helpers.CancelONDRequest;
import com.isa.thinair.webservices.helpers.CancellationBalancesRequest;
import com.isa.thinair.webservices.helpers.Constants;
import com.isa.thinair.webservices.helpers.ExtPayReconciliationRequest;
import com.isa.thinair.webservices.helpers.ExtendOnholdRequest;
import com.isa.thinair.webservices.helpers.FlightInfoRequest;
import com.isa.thinair.webservices.helpers.FltScheduleRequest;
import com.isa.thinair.webservices.helpers.NameChangeRequest;
import com.isa.thinair.webservices.helpers.PassengerCreditRequest;
import com.isa.thinair.webservices.helpers.PingRequest;
import com.isa.thinair.webservices.helpers.PriceQuoteRequest;
import com.isa.thinair.webservices.helpers.RemovePassengerRequest;
import com.isa.thinair.webservices.helpers.RetreiveResListRequest;
import com.isa.thinair.webservices.helpers.RetreiveResRequest;
import com.isa.thinair.webservices.helpers.SSRUpdateRequest;
import com.isa.thinair.webservices.helpers.SplitReservationRequest;
import com.isa.thinair.webservices.helpers.TransferBookingRequest;
import com.isa.thinair.webservices.helpers.UpdateContactDetailsRequest;
import com.isa.thinair.webservices.helpers.UpdateReservation;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
/**
 * 
 * @author WS Team
 *
 */
public class TestAAResWebService extends BaseTestUtil {

	public TestAAResWebService(){
		
	}
	
	private static OTAAirPriceRS priceRS;
	private static OTAAirAvailRS availableRS;
	private static OTAAirBookRS bookRS;
	private static String pnr = "";
	
	public void testPing() {
		runTest(new PingRequest(),null);
	}

	public void testAgentAvailCreditLimit() {
		AAOTAAgentAvailCreditRS availCreditRS = 	(AAOTAAgentAvailCreditRS) runTest(new AgentCreditLimitRequest(),null);
		assertNotNull(availCreditRS);
	}
	
	public void testRetreiveReservationList() {
		OTAResRetrieveRS otaResRetrieveRS = (OTAResRetrieveRS)runTest(new RetreiveResListRequest(),new Object[]{});
		assertNotNull(otaResRetrieveRS);
	}

	public void testCreateBooking(){
		availableRS = (OTAAirAvailRS) runTest(new AvailabilityRequest(), new Object[]{Constants.JourneyType.ONEWAY, bookRS,null});
		priceRS = (OTAAirPriceRS)runTest(new PriceQuoteRequest(), new Object[]{availableRS});
		bookRS = (OTAAirBookRS)runTest(new BookRequest(), new Object[]{priceRS});
		
		for (Object o : bookRS.getSuccessAndWarningsAndAirReservation()) {
			if (o instanceof AirReservation) {
				AirReservation reservation = (AirReservation) o;
				assertEquals(1, reservation.getBookingReferenceID().size());
				pnr = reservation.getBookingReferenceID().get(0).getID();
				break;
			}
		}
	}
	
	public void testRetreiveReservation(){
//		pnr = "20150729";
		if(pnr == null){
			testCreateBooking();
		}
		
		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);
		bookRS = (OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
		assertNotNull(bookRS);
	}
	
    public void testUpdateSSR() {
//    	String pnr = "20150729";
    	
    	if(pnr == null){
    		testCreateBooking();
    	}

        AALoadDataOptionsType options = new AALoadDataOptionsType();
        options.setLoadAirItinery(true);
        options.setLoadPriceInfoTotals(true);
        options.setLoadFullFilment(true);
        options.setLoadTravelerInfo(true);
        
        bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[] {
                pnr, options });

        SSRUpdateRequest ssrUpdateRequest = new SSRUpdateRequest();
        Object[] helpers = { bookRS, options };
        bookRS = (OTAAirBookRS) runTest(ssrUpdateRequest, helpers);
    }

	//TODO fix this
//	public void testGetBalancesForAddOnd() throws ModuleException, WebservicesException {
//		
//		OTAAirAvailRS availableRS;
//		OTAAirPriceRS priceRS;
//		
//		String pnr = "20150729";
//		
//		AALoadDataOptionsType options = new AALoadDataOptionsType();
//		options.setLoadAirItinery(true);
//		options.setLoadPriceInfoTotals(true);
//		options.setLoadFullFilment(true);
//		options.setLoadTravelerInfo(true);
//		bookRS = (OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
//		
//		Calendar depDate = new GregorianCalendar(2010,9,30,0,0,0);
//		Calendar arrDate =new GregorianCalendar(2010,9,30,0,0,0);
//		
//        availableRS = (OTAAirAvailRS) runTest(new AvailabilityRequest(), 
//				new Object[]{Constants.JourneyType.ONEWAY, bookRS,AvailabilityRequest.getOriginDestinationInformation("SHJ","BOM" , depDate, arrDate)});
//        priceRS = (OTAAirPriceRS)runTest(new PriceQuoteRequest(), new Object[]{availableRS});
//		
//        bookRS = (OTAAirBookRS) runTest(new BalancesForAddONDRequest(), new Object[]{bookRS, priceRS});
//	}
	
	public void testAddUserNotes() {
//		String pnr = "20150729";
		
		if(pnr == null){
			testCreateBooking();
		}
		
		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);
	
		bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[]{pnr, options});
		AirReservationType airReservationType = null;
		for (Object o : bookRS.getSuccessAndWarningsAndAirReservation()) {
			if (o instanceof AirReservation) {
				airReservationType = (AirReservationType) o;
			}
		}
	
		AddUserNotesRequest userNotesRequest = new AddUserNotesRequest();
		Object[] helpers = { airReservationType, options, bookRS };
		bookRS = (OTAAirBookRS) runTest(userNotesRequest, helpers);
		assertNotNull(bookRS);
	}
	
	public void testUpdateContactInfo() {
//		String pnr = "20150729";
		if(pnr == null){
			testCreateBooking();
		}
		
		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);

		bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[]{pnr, options});

		UpdateContactDetailsRequest updateContactRequest = new UpdateContactDetailsRequest();
		Object[] helpers = {bookRS, options};
		bookRS = (OTAAirBookRS) runTest(updateContactRequest, helpers);
		assertNotNull(bookRS);
	}
	
	public void testRemovePassengers() {
//		String pnr = "20150729";
		if(pnr == null){
			testCreateBooking();
		}

		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);

		bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[]{pnr, options});

		RemovePassengerRequest removePassengerRequest = new RemovePassengerRequest();
		Object[] helpers = {bookRS, options};
		bookRS = (OTAAirBookRS) runTest(removePassengerRequest, helpers);
		assertNotNull(bookRS);
	}
	

	//-------------------------my additions-----------------------
	
	
	
	
//	public void testBalancePayment() {
//		String pnr="11678879";
//		OTAAirBookRS otaAirBookRS = null;
//		AALoadDataOptionsType options=new AALoadDataOptionsType();
//		options.setLoadAirItinery(true);
//		options.setLoadPriceInfoTotals(true);
//		options.setLoadFullFilment(true);
//		options.setLoadTravelerInfo(true);
//		otaAirBookRS=(OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
//		
//		AirReservationType airReservationType=null;
//		BigDecimal totalPayments = new BigDecimal("0.00");
//		BigDecimal totalPrice = new BigDecimal("0.00");
//		for (Iterator i=otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();i.hasNext();) {
//			Object o=i.next();
//			if (o instanceof AirReservation) {
//				airReservationType = (AirReservationType) o;
//				if (airReservationType.getFulfillment() != null && airReservationType.getFulfillment().getPaymentDetails() != null){
//					for (PaymentDetailType paymentDetailType : airReservationType.getFulfillment().getPaymentDetails().getPaymentDetail()){
//						totalPayments =  totalPayments.add(paymentDetailType.getPaymentAmount().getAmount());
//					}
//				}
//				totalPrice = airReservationType.getPriceInfo().getItinTotalFare().getTotalFare().getAmount();
//			}
//		}
//		
//		runTest(new BalancePaymentRequest(), new Object[] {otaAirBookRS, totalPrice.subtract(totalPayments)});
//	}
//	
//
//	public void testSplitReservation() {
//		String pnr = "11678620";
//
//		AALoadDataOptionsType options = new AALoadDataOptionsType();
//		options.setLoadAirItinery(true);
//		options.setLoadPriceInfoTotals(true);
//		options.setLoadFullFilment(true);
//		options.setLoadTravelerInfo(true);
//		bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[] {
//				pnr, options });
//
//		SplitReservationRequest splitResRequest = new SplitReservationRequest();
//		Object[] helpers = { bookRS, options };
//		bookRS = (OTAAirBookRS) runTest(splitResRequest, helpers);
//	}
	
//	public void testUpdatePerson(){
//		runTest(new UpdatePersonTest(),null);
//	}
	
//    
//	
//	public void testGetPassengerCredit() {
//        PassengerCreditRequest passengerCreditRequest = new PassengerCreditRequest();
//        AAOTAPaxCreditRS paxCreditRS = 
//            (AAOTAPaxCreditRS) runTest(passengerCreditRequest, new Object[] {});
//       
//    }
//    
//	public void testGetBalancesForCancelOND() {
//		String pnr = "10000011"; //"11678696";
//        
//		OTAAirBookRS otaAirBookRS = null;
//		AALoadDataOptionsType options=new AALoadDataOptionsType();
//		options.setLoadAirItinery(true);
//		options.setLoadPriceInfoTotals(true);
//		options.setLoadFullFilment(true);
//		options.setLoadTravelerInfo(true);
//		otaAirBookRS=(OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
//		
//		runTest(new CancellationBalancesRequest(), new Object[] {otaAirBookRS});
//
//	}
//	
//	public void testGetBalancesForCancelRes() {
//		String pnr="10000011";
//		OTAAirBookRS otaAirBookRS = null;
//		AALoadDataOptionsType options=new AALoadDataOptionsType();
//		options.setLoadAirItinery(true);
//		options.setLoadPriceInfoTotals(true);
//		options.setLoadFullFilment(true);
//		options.setLoadTravelerInfo(true);
//		otaAirBookRS=(OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
//				
//		runTest(new BalancesForCancelResRequest(), new Object[] {otaAirBookRS});
//		
//	}
//	
//	public void testGetBalancesForAddOnd() {		
//		runTest(new BalancesForAddONDRequest(), new Object[] {bookRS, priceRS});
//	}
//	
//	public void testGetBalancesForModifyOnd() {		
//		runTest(new BalancesForModONDRequest(), new Object[] {bookRS, priceRS});
//	}
//	
//	public void testCancelReservation(){
//		String pnr="11678848";
//		OTAAirBookRS otaAirBookRS = null;
//		AALoadDataOptionsType options=new AALoadDataOptionsType();
//		options.setLoadAirItinery(true);
//		options.setLoadPriceInfoTotals(true);
//		options.setLoadFullFilment(true);
//		options.setLoadTravelerInfo(true);
//		otaAirBookRS=(OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
//		
//		runTest(new CancelReservationRequest(), new Object[] {otaAirBookRS});
//	}
//	
//	public void testCancelSegment(){
//		String pnr="11678690";
//		OTAAirBookRS otaAirBookRS = null;
//		AALoadDataOptionsType options=new AALoadDataOptionsType();
//		options.setLoadAirItinery(true);
//		options.setLoadPriceInfoTotals(true);
//		options.setLoadFullFilment(true);
//		options.setLoadTravelerInfo(true);
//		otaAirBookRS=(OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
//		
//		runTest(new CancelONDRequest(), new Object[] {otaAirBookRS});
//	}
//	
//	public void testAddOnd(){
////		String pnr="11678690";
////		OTAAirBookRS otaAirBookRS = null;
////		AALoadDataOptionsType options=new AALoadDataOptionsType();
////		options.setLoadAirItinery(true);
////		options.setLoadPriceInfoTotals(true);
////		options.setLoadFullFilment(true);
////		options.setLoadTravelerInfo(true);
////		otaAirBookRS=(OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
//		
//		runTest(new AddONDRequest(), new Object[] {bookRS, priceRS});
//	}
//	
//	public void testReconcileRequest(){
//		runTest(new ExtPayReconciliationRequest(), new Object []{});
//	}
//	
//	public void testRetreiveFltSchedule() {
//		OTAAirScheduleRS otaAirScheduleRS = (OTAAirScheduleRS) runTest(
//				new FltScheduleRequest(), new Object[] {});
//	}
//	
//	public void testRetreiveFltInfo() {
//		OTAAirFlifoRS otaAirFlifoRS = (OTAAirFlifoRS) runTest(
//				new FlightInfoRequest(), new Object[] {});
//	}
//    
//    public void testExtendOnholdReservation() {
//        String pnr = "10478235";
//
//        AALoadDataOptionsType options = new AALoadDataOptionsType();
//        options.setLoadAirItinery(true);
//        options.setLoadPriceInfoTotals(true);
//        options.setLoadFullFilment(true);
//        options.setLoadTravelerInfo(true);
//        
//        bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[] {
//                pnr, options });
//        Object[] helpers = { bookRS, options };
//        bookRS = (OTAAirBookRS) runTest(new ExtendOnholdRequest(), helpers);
//       
//    }
//    
//    public void testTransferBooking() {
//        String pnr = "10478235";
//        String ownerAgentCode = "CMB55";
//
//        AALoadDataOptionsType options = new AALoadDataOptionsType();
//        options.setLoadAirItinery(true);
//        options.setLoadPriceInfoTotals(true);
//        options.setLoadFullFilment(true);
//        options.setLoadTravelerInfo(true);
//        
//        bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[] {
//                pnr, options });
//        Object[] helpers = { bookRS, ownerAgentCode, options };
//        bookRS = (OTAAirBookRS) runTest(new TransferBookingRequest(), helpers);
//       
//    }
//    
//    
//    public void testNameChange() {
//        String pnr = "10000069";
//
//        AALoadDataOptionsType options = new AALoadDataOptionsType();
//        options.setLoadAirItinery(true);
//        options.setLoadPriceInfoTotals(true);
//        options.setLoadFullFilment(true);
//        options.setLoadTravelerInfo(true);
//        
//        bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[] {
//                pnr, options });
//
//        NameChangeRequest nameChangeRequest = new NameChangeRequest();
//        Object[] helpers = { bookRS, options };
//        bookRS = (OTAAirBookRS) runTest(nameChangeRequest, helpers);
//    }
//    
//    
//    
//    public void testUpdateReservation() {
//        String pnr = "10000069";
//
//        AALoadDataOptionsType options = new AALoadDataOptionsType();
//        options.setLoadAirItinery(true);
//        options.setLoadPriceInfoTotals(true);
//        options.setLoadFullFilment(true);
//        options.setLoadTravelerInfo(true);
//        
//        bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[] {
//                pnr, options });
//
//        UpdateReservation updateRequest = new UpdateReservation();
//        Object[] helpers = { bookRS, options };
//        bookRS = (OTAAirBookRS) runTest(updateRequest, helpers);
//    }
}
