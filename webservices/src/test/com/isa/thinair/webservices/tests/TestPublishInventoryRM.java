package com.isa.thinair.webservices.tests;


import com.isa.thinair.webservices.helpers.rm.PublishInventoryRequest;

/**
 * @author Byorn
 */
public class TestPublishInventoryRM extends BaseTestUtil {


	/**
     * Test Update Optimized Inventory for RM
     *
	 */
	public void testPublishInventoryRM() {
			runTest(new PublishInventoryRequest(),null);
	}

	
	
}
