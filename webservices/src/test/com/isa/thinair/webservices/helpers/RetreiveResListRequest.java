package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.VerificationType;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ReadRequest;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAReadRQExt;

public class RetreiveResListRequest extends AbstractHelper {
	public String getOperation() {
		return "getReservationsList";
	}
	
	public Object[] getRequestObject(Object[] helpers) throws Exception {		
		OTAReadRQ request=new OTAReadRQ();
		
		request.setPOS(new POSType());
		SourceType pos = CommonTestUtils.preparePOS();
    	request.getPOS().getSource().add(pos);
    	
    	request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		request.setVersion(WebservicesConstants.OTAConstants.VERSION_RetrieveResList);
		request.setSequenceNmbr(new BigInteger("1"));
		request.setEchoToken(UID.generate());
		request.setTimeStamp(CommonUtil.parse(new Date()));
		request.setTransactionIdentifier(null);
		
		ReadRequest readRequest = null;
		request.setReadRequests(new ReadRequests());
		request.getReadRequests().getReadRequest().add(readRequest=new ReadRequest());
		
		VerificationType verification = new VerificationType();
		VerificationType.PersonName personName = new VerificationType.PersonName();
		personName.getGivenName().add("Nilindra");
		personName.setSurname("Fernando");
		verification.setPersonName(personName);
		
		readRequest.setVerification(verification);
		
		AAReadRQExt aaReadRQExt = new AAReadRQExt();
	
		return new Object[]{request, aaReadRQExt};
	}
	
	@Override
	public void assertResponse(Object response) {		
		log.debug("asserting response..."+response);
	}
	
	
	public Element marshalNonRootElement(Object o, Class c, String rootTag) throws WebservicesException{
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Document doc=getNewDocument();
			marshaller.marshal( new JAXBElement(
					  new QName("http://delta/thinair/webservices/OTA/Extensions/2003/05", rootTag), c, o), doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}
	
	public Element marshall(Object o, Class c) throws WebservicesException {
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			Document doc=getNewDocument();
			marshaller.marshal(o, doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}
	
	private Document getNewDocument() {
		factory=DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		try {
			builder=factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return builder.newDocument();
	}

	private DocumentBuilderFactory factory=null;
	DocumentBuilder builder=null;
}
