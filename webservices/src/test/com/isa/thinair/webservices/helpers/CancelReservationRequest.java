/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * @author Nasly
 */
public class CancelReservationRequest extends AbstractHelper {

	public String getOperation() {		
		return "modifyReservation";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, FileNotFoundException, JAXBException, ModuleException {
		
		OTAAirBookRS otaAirBookRS = (OTAAirBookRS) helpers[0];

		OTAAirBookModifyRQ airBookModRQ = new OTAAirBookModifyRQ();
		
		airBookModRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		airBookModRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		airBookModRQ.setSequenceNmbr(new BigInteger("2"));
		airBookModRQ.setEchoToken(UID.generate());
		airBookModRQ.setTimeStamp(CommonUtil.parse(new Date()));
		airBookModRQ.setTransactionIdentifier(otaAirBookRS.getTransactionIdentifier());
		
		POSType pos = new POSType();
		airBookModRQ.setPOS(pos);
		SourceType source = CommonTestUtils.preparePOS();
        pos.getSource().add(source);

		
		airBookModRQ.setAirBookModifyRQ(new AirBookModifyRQ());
		airBookModRQ.getAirBookModifyRQ().setModificationType(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCEL_ENTIRE_BOOKING_FILE));
		
		AirReservationType airReservationType = null;
		for (Iterator i=otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();i.hasNext();) {
			Object o=i.next();
			if (o instanceof AirReservation) {
				airReservationType = (AirReservationType) o;
			}
		}
		
		airBookModRQ.getAirBookModifyRQ().getBookingReferenceID().add(airReservationType.getBookingReferenceID().get(0));	
		
		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadFullFilment(true);
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);
		
		AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
		airBookModifyRQAAExt.setAALoadDataOptions(options);
		
		return new Object[] { airBookModRQ, airBookModifyRQAAExt };
	}
}
