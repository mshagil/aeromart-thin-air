package com.isa.thinair.webservices.helpers;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

public class BalancesForCancelResRequest extends AbstractHelper {
	public String getOperation() {		
		return "modifyResQuery";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, FileNotFoundException, JAXBException, ModuleException {
		
		OTAAirBookRS otaAirBookRS = (OTAAirBookRS) helpers[0];

		OTAAirBookModifyRQ otaAirBookModRQ = new OTAAirBookModifyRQ();
		
		otaAirBookModRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		otaAirBookModRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		otaAirBookModRQ.setSequenceNmbr(new BigInteger("1"));
		otaAirBookModRQ.setEchoToken(UID.generate());
		otaAirBookModRQ.setTimeStamp(CommonUtil.parse(new Date()));
		otaAirBookModRQ.setTransactionIdentifier(otaAirBookRS.getTransactionIdentifier());
		
		POSType pos = new POSType();
		otaAirBookModRQ.setPOS(pos);
		SourceType source = CommonTestUtils.preparePOS();
        pos.getSource().add(source);
		
		otaAirBookModRQ.setAirBookModifyRQ(new AirBookModifyRQ());
		otaAirBookModRQ.getAirBookModifyRQ().setModificationType(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCELLATION_BALANCES_FOR_CANCEL_RESERVATION));
		
		AirReservationType airReservation = null;
		for (Iterator i=otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();i.hasNext();) {
			Object o=i.next();
			if (o instanceof AirReservation) {
				airReservation = (AirReservationType) o;
			}
		}
		
		otaAirBookModRQ.getAirBookModifyRQ().getBookingReferenceID().add(airReservation.getBookingReferenceID().get(0));
		
		AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
		
		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadFullFilment(true);
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);
		
		airBookModifyRQAAExt.setAALoadDataOptions(options);
		
		return new Object[] { otaAirBookModRQ, airBookModifyRQAAExt };
	}
}
