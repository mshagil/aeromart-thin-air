/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;

import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PersonNameType;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * @author Mehdi Raza
 */
public class NameChangeRequest extends AbstractHelper {

	public String getOperation() {		
		return "modifyReservation";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, ModuleException {
        OTAAirBookRS otaAirBookRS = (OTAAirBookRS) helpers[0];
        OTAAirBookModifyRQ otaAirBookModifyRQ = new OTAAirBookModifyRQ();
        otaAirBookModifyRQ.setPOS(new POSType());
        otaAirBookModifyRQ.getPOS().getSource().add(CommonTestUtils.preparePOS());
        otaAirBookModifyRQ.setPrimaryLangID(
                WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
        otaAirBookModifyRQ.setVersion(
                WebservicesConstants.OTAConstants.VERSION_AirBookModify);
        otaAirBookModifyRQ.setSequenceNmbr(new BigInteger("1"));
        otaAirBookModifyRQ.setEchoToken(UID.generate());
        otaAirBookModifyRQ.setTimeStamp(CommonUtil.parse(new Date()));
        otaAirBookModifyRQ.setTransactionIdentifier(otaAirBookRS.getTransactionIdentifier());
        
        AirReservationType airReservation = null;
        for (Iterator i = otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator(); 
                i.hasNext();) {
            Object o = i.next();
            if (o instanceof AirReservation) {
                airReservation = (AirReservationType) o;
            }
        }
        
        otaAirBookModifyRQ.setAirBookModifyRQ(new AirBookModifyRQ());
        otaAirBookModifyRQ.getAirBookModifyRQ().setModificationType(
                CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_NAME_CHANGE___CORRECTION));
        otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().add(
                airReservation.getBookingReferenceID().get(0));
        otaAirBookModifyRQ.getAirBookModifyRQ().setTravelerInfo(
                airReservation.getTravelerInfo());
        
        PersonNameType nameType = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler().get(0).getPersonName();
        nameType.getNameTitle().clear();
        nameType.getNameTitle().add("MSTR");
        //nameType.getGivenName().clear();
        //nameType.getGivenName().add("Testing");
        
        AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
        airBookModifyRQAAExt.setAALoadDataOptions((AALoadDataOptionsType) helpers[1]);
        
		return new Object[]{otaAirBookModifyRQ, airBookModifyRQAAExt};
	}
}
