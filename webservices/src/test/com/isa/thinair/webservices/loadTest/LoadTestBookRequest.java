package com.isa.thinair.webservices.loadTest;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.CountryNameType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PaymentCardType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PersonNameType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.AirTravelerType.Address;
import org.opentravel.ota._2003._05.AirTravelerType.Document;
import org.opentravel.ota._2003._05.OTAAirBookRQ.PriceInfo;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmountInPayCur;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.helpers.AbstractHelper;
import com.isa.thinair.webservices.helpers.CommonTestUtils;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAddressType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAContactInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACountryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPersonNameType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATelephoneType;

public class LoadTestBookRequest extends AbstractHelper {

	private static final PaymentCardType[] PAYMENT_CADE = new PaymentCardType[] { getVisa(), getMaster() };
	
	private static final String[] EMAILS  = new String[]{"accept@airarabiaagent.com","review@airarabiaagent.com"};

	@Override
	public String getOperation() {
		return "book";
	}

	@Override
	public Object[] getRequestObject(Object[] helpers) throws Exception {

		OTAAirBookRQ request = new OTAAirBookRQ();
		CountryNameType country = null;
		PaymentDetailType paymentDetail = null;
		PaymentAmount paymentAmount = null;
		PaymentAmountInPayCur paymentAmountInPayCur = null;
		Address address = null;
		Document doc = null;
		AirTravelerType tr = null;

		OTAAirPriceRS otaAirPriceRS = (OTAAirPriceRS) helpers[0];

		request.setPOS(new POSType());
		request.getPOS().getSource().add(CommonTestUtils.preparePOS());
		request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		request.setVersion(WebservicesConstants.OTAConstants.VERSION_Book);
		request.setSequenceNmbr(new BigInteger("1"));
		request.setEchoToken(UID.generate());
		request.setTimeStamp(CommonUtil.parse(new Date()));
		request.setTransactionIdentifier(otaAirPriceRS.getTransactionIdentifier());

		TravelerInfoType trInfo = new TravelerInfoType();
		request.setTravelerInfo(trInfo);

		int adultIndex = 0;
		BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalEqPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		request.setPriceInfo(new PriceInfo());
		for (Object o : otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries()) {
			if (o instanceof PricedItinerariesType) {
				for (PricedItineraryType itinsType : ((PricedItinerariesType) o).getPricedItinerary()) {
					request.setAirItinerary(itinsType.getAirItinerary());
//					request.getPriceInfo().setItinTotalFare(itinsType.getAirItineraryPricingInfo().getItinTotalFare());
					totalPrice = AccelAeroCalculator.add(totalPrice, itinsType.getAirItineraryPricingInfo().getItinTotalFare()
							.getTotalFareWithCCFee().getAmount());
					totalEqPrice = AccelAeroCalculator.add(totalEqPrice, itinsType.getAirItineraryPricingInfo().getItinTotalFare()
							.getTotalEquivFareWithCCFee().getAmount());
					request.getPriceInfo().setFareInfos(itinsType.getAirItineraryPricingInfo().getFareInfos());
					// request.getPriceInfo().setPTCFareBreakdowns(itinsType.getAirItineraryPricingInfo().getPTCFareBreakdowns());

					for (PTCFareBreakdownType each : itinsType.getAirItineraryPricingInfo().getPTCFareBreakdowns().getPTCFareBreakdown()) {
						if (each.getPassengerTypeQuantity().getCode().equals(
								CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
							for (int x = 0; x < each.getPassengerTypeQuantity().getQuantity().intValue(); x++) {
								if (adultIndex == 0) {
									// ################ ADULT 1
									tr = new AirTravelerType();
									tr.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));

									tr.setPersonName(new PersonNameType());
									tr.getPersonName().getNameTitle().add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR));
									tr.getPersonName().getGivenName().add("Ros");
									tr.getPersonName().setSurname("Taylor");
									tr.setBirthDate(CommonUtil.getXMLGregorianCalendar(CommonUtil.parseDate("1976-08-12T00:00:00")));

									AirTravelerType.Telephone telephone = new AirTravelerType.Telephone();
									telephone.setCountryAccessCode("971");
									telephone.setAreaCityCode("6");
									telephone.setPhoneNumber("5088952");
									tr.getTelephone().add(telephone);

									doc = new Document();
									doc.setDocID("MV237934");
									doc.setDocType("PSPT");
									doc.setDocHolderNationality("AE");
									doc.setDocHolderName("Ros Taylor");
									tr.getDocument().add(doc);

									tr.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
									tr.getTravelerRefNumber().setRPH(each.getTravelerRefNumber().get(x).getRPH());

									trInfo.getAirTraveler().add(tr);
									adultIndex++;
								} else {
									// ################ ADULT 2
									tr = new AirTravelerType();
									tr.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
									tr.setPersonName(new PersonNameType());
									tr.getPersonName().getNameTitle().add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR));
									tr.getPersonName().getGivenName().add("Jennie");
									tr.getPersonName().setSurname("Taylor");
									tr.setBirthDate(CommonUtil.getXMLGregorianCalendar(CommonUtil.parseDate("1960-08-12T00:00:00")));

									address = new Address();

									address.setCountryName(country = new CountryNameType());
									country.setCode("AU");
									tr.getAddress().add(address);
									tr.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
									tr.getTravelerRefNumber().setRPH(each.getTravelerRefNumber().get(x).getRPH());
									trInfo.getAirTraveler().add(tr);
								}
							}
						}

					}

				}
			}
		}

		request.setFulfillment(new OTAAirBookRQ.Fulfillment());
		request.getFulfillment().setPaymentDetails(new OTAAirBookRQ.Fulfillment.PaymentDetails());

		// total reservation payments
		request.getFulfillment().getPaymentDetails().getPaymentDetail().add(paymentDetail = new PaymentDetailType());

		paymentDetail.setPaymentCard(PAYMENT_CADE[ (int) (System.currentTimeMillis() % 2)]);
		paymentDetail.setPaymentAmount(paymentAmount = new PaymentAmount());
		paymentAmount.setAmount(totalPrice);
		paymentAmount.setCurrencyCode("MAD");
		paymentAmount.setDecimalPlaces(BigInteger.valueOf(2));

		paymentDetail.setPaymentAmountInPayCur(paymentAmountInPayCur = new PaymentAmountInPayCur());
		paymentAmountInPayCur.setAmount(totalEqPrice);
		paymentAmountInPayCur.setCurrencyCode("USD");
		paymentAmountInPayCur.setDecimalPlaces(BigInteger.valueOf(2));

		// set contact information
		AAContactInfoType wsContactInfo = new AAContactInfoType();
		AAPersonNameType name = new AAPersonNameType();
		wsContactInfo.setPersonName(name);
		name.setTitle(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR));
		name.setFirstName("Adam");
		name.setLastName("Bawa");

		AATelephoneType telephone = new AATelephoneType();
		wsContactInfo.setTelephone(telephone);
		telephone.setPhoneNumber("5088952");
		telephone.setAreaCode("6");
		telephone.setCountryCode("971");

		wsContactInfo.setEmail(EMAILS[(int) (System.currentTimeMillis() % 2)]);

		AAAddressType caddress = new AAAddressType();
		wsContactInfo.setAddress(caddress);

		AACountryType ccountry = new AACountryType();
		caddress.setCountryName(ccountry);
		ccountry.setCountryCode("AE");
		ccountry.setCountryName("United Arab Emirates");
		caddress.setCityName("Dubai");

		AAAirBookRQExt aaAirBookRQExt = new AAAirBookRQExt();
		aaAirBookRQExt.setContactInfo(wsContactInfo);

		return new Object[] { request, aaAirBookRQExt };
	}

	private static PaymentCardType getMaster() {
		PaymentCardType ccBill = new PaymentCardType();
		ccBill.setCardType("1");
		ccBill.setCardNumber("5123456789012346");
		ccBill.setSeriesCode("000");
		ccBill.setExpireDate("0513");
		ccBill.setCardHolderName("Test Test");
		return ccBill;
	}

	private static PaymentCardType getVisa() {
		PaymentCardType ccBill = new PaymentCardType();
		ccBill.setCardType("2");
		ccBill.setCardNumber("4005550000000001");
		ccBill.setSeriesCode("000");
		ccBill.setExpireDate("0513");
		ccBill.setCardHolderName("Test Test");
		return ccBill;
	}

}
