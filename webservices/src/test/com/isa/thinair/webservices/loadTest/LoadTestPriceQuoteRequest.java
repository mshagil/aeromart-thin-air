package com.isa.thinair.webservices.loadTest;

import java.math.BigInteger;
import java.util.Date;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTripType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;
import org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.helpers.AbstractHelper;
import com.isa.thinair.webservices.helpers.CommonTestUtils;

public class LoadTestPriceQuoteRequest extends AbstractHelper {

	@Override
	public String getOperation() {
		return "getPrice";
	}

	@Override
	public Object[] getRequestObject(Object[] helpers) throws Exception {
		OTAAirAvailRS otaAirAvailRS=(OTAAirAvailRS)helpers[0];
		//OTAAirPriceRS otaAirPriceRS = null;
		OTAAirPriceRQ otaAirPriceRQ=null;
		if (otaAirAvailRS.getSuccess() != null){
			log.info("getAvailability SUCCESS [found " + otaAirAvailRS.getOriginDestinationInformation().size() + " Results]");
			for (OriginDestinationInformation originDestinationInformationRS : otaAirAvailRS.getOriginDestinationInformation()){
				
    			otaAirPriceRQ = new OTAAirPriceRQ();
    			otaAirPriceRQ.setPOS(new POSType());
    			otaAirPriceRQ.getPOS().getSource().add(CommonTestUtils.preparePOS());
    	    	
    			otaAirPriceRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
    			otaAirPriceRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirPrice);
    			otaAirPriceRQ.setSequenceNmbr(new BigInteger("1"));
    			otaAirPriceRQ.setEchoToken(UID.generate());
    			otaAirPriceRQ.setTimeStamp(CommonUtil.parse(new Date()));
    			otaAirPriceRQ.setTransactionIdentifier(otaAirAvailRS.getTransactionIdentifier());
                
                AirItineraryType airItineraryType = new AirItineraryType();
                otaAirPriceRQ.setAirItinerary(airItineraryType);
                airItineraryType.setDirectionInd(AirTripType.ONE_WAY);
                
                AirItineraryType.OriginDestinationOptions originDestinationOptions = new AirItineraryType.OriginDestinationOptions();
                airItineraryType.setOriginDestinationOptions(originDestinationOptions);
                
        		for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption : originDestinationInformationRS.getOriginDestinationOptions().getOriginDestinationOption()){
        			OriginDestinationOptionType originDestinationOptionType = new OriginDestinationOptionType();
        			airItineraryType.getOriginDestinationOptions().getOriginDestinationOption().add(originDestinationOptionType);
        			for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment flightSegment : originDestinationOption.getFlightSegment()){
        				BookFlightSegmentType bookFlightSegmentType = new BookFlightSegmentType();
        				originDestinationOptionType.getFlightSegment().add(bookFlightSegmentType);
        				bookFlightSegmentType.setDepartureAirport(flightSegment.getDepartureAirport());
        				bookFlightSegmentType.setArrivalAirport(flightSegment.getArrivalAirport());
        				bookFlightSegmentType.setDepartureDateTime(flightSegment.getDepartureDateTime());
        				bookFlightSegmentType.setArrivalDateTime(flightSegment.getArrivalDateTime());
        				bookFlightSegmentType.setFlightNumber(flightSegment.getFlightNumber());
        			}
        		}
        		
        		TravelerInfoSummaryType travellerInfoSummary1 = new TravelerInfoSummaryType();
                otaAirPriceRQ.setTravelerInfoSummary(travellerInfoSummary1);
                
                TravelerInformationType travelerInfo1 = new TravelerInformationType();
                travellerInfoSummary1.getAirTravelerAvail().add(travelerInfo1);
                
                PassengerTypeQuantityType paxQuantity1 = new PassengerTypeQuantityType();
                paxQuantity1.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
                paxQuantity1.setQuantity(1);
                travelerInfo1.getPassengerTypeQuantity().add(paxQuantity1);
			}
		}

		return new Object[]{otaAirPriceRQ};
	}

}
