package com.isa.thinair.webservices.api.exception;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.opentravel.ota._2003._05.ErrorType;
import org.opentravel.ota._2003._05.WarningType;

/**
 * Exception for propagating exceptional situations & warnings in web services internal implementations to the main
 * service implementation class
 * 
 * @author Mohamed Nasly
 */
public class WebservicesException extends Exception {

	private static final long serialVersionUID = -6571651954258094065L;

	private Collection<ErrorType> otaErrors = new ArrayList<ErrorType>();

	private Collection<WarningType> otaWarnings = new ArrayList<WarningType>();

	private HashMap<String, String> otaErrorCodes = new HashMap<String, String>();

	private HashMap<String, String> otaWarningCodes = new HashMap<String, String>();

	public WebservicesException(Exception cause) {
		super(cause);
	}

	public WebservicesException(ErrorType error) {
		super(error.getShortText());
		otaErrors.add(error);
	}

	public WebservicesException(String otaErrorCode, String contextInfo) {
		otaErrorCodes.put(otaErrorCode, contextInfo);
	}

	public void addOTAError(ErrorType error) {
		otaErrors.add(error);
	}

	public void addOTAErrorCode(String otaErrorCode, String contextInfo) {
		otaErrorCodes.put(otaErrorCode, contextInfo);
	}

	public Collection<WarningType> getOTAWarnings() {
		return otaWarnings;
	}

	public HashMap<String, String> getOtaErrorCodes() {
		return otaErrorCodes;
	}

	public Collection<ErrorType> getOtaErrors() {
		return otaErrors;
	}

}
