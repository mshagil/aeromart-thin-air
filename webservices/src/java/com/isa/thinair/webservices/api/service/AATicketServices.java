package com.isa.thinair.webservices.api.service;

import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRs;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRs;

public interface AATicketServices {
	AeroMartUpdateCouponStatusRs updateCouponStatus(AeroMartUpdateCouponStatusRq request);
	
	AeroMartUpdateExternalCouponStatusRs updateExternalCouponStatus(AeroMartUpdateExternalCouponStatusRq request);
}
