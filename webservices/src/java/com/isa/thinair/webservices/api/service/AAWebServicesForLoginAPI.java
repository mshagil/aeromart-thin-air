package com.isa.thinair.webservices.api.service;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import ae.isa.api.login.AAAPILoginRQ;
import ae.isa.api.login.AAAPILoginRS;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;

@WebService(name = "AAWebServicesForLoginAPI", targetNamespace = "http://www.isa.ae/api/login",
		endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForLoginAPI")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface AAWebServicesForLoginAPI {

	@WebServiceSource(source = WebServiceSource.MethodSource.LOGIN_API)
	@WebMethod(operationName = "login", action = "login")
	@WebResult(name = "AA_API_LoginRS", targetNamespace = "http://www.isa.ae/api/login")
	public AAAPILoginRS login(
			@WebParam(name = "AA_API_LoginRQ", targetNamespace = "http://www.isa.ae/api/login") AAAPILoginRQ loginRQ);
}
