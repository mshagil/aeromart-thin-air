package com.isa.thinair.webservices.api.dtos;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusTotalPriceDTO {
	BigDecimal totalFare = BigDecimal.ZERO;
	BigDecimal totalTax = BigDecimal.ZERO;
	BigDecimal totalFee = BigDecimal.ZERO;
	BigDecimal totalSSR = BigDecimal.ZERO;

	BigDecimal total = BigDecimal.ZERO;

	BigDecimal totalInSelectedCurrency = BigDecimal.ZERO;

	public BigDecimal getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	public BigDecimal getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	public BigDecimal getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(BigDecimal totalFee) {
		this.totalFee = totalFee;
	}

	public BigDecimal getTotalSSR() {
		return totalSSR;
	}

	public void setTotalSSR(BigDecimal totalSSR) {
		this.totalSSR = totalSSR;
	}

	public BigDecimal getTotal() {
		return AccelAeroCalculator.add(totalFare, totalTax, totalFee, totalSSR);
	}

	public BigDecimal getTotalWithoutFee() {
		return AccelAeroCalculator.add(totalFare, totalTax, totalSSR);
	}

	public BigDecimal getTotalInSelectedCurrency() {
		return totalInSelectedCurrency;
	}

	public void setTotalInSelectedCurrency(BigDecimal totalInSelectedCurrency) {
		this.totalInSelectedCurrency = totalInSelectedCurrency;
	}

}
