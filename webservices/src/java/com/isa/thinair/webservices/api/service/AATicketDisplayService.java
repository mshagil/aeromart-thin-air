package com.isa.thinair.webservices.api.service;

import com.isa.thinair.commons.api.dto.ets.TicketDisplayReq;
import com.isa.thinair.commons.api.dto.ets.TicketDisplayRes;

public interface AATicketDisplayService {
	
	TicketDisplayRes invokeTicketDisplay(TicketDisplayReq request);
}
