package com.isa.thinair.webservices.api.dtos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.v2.reservation.IChargeablePax;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusPaxTO implements IChargeablePax {
	private String rph;
	private Collection<ExternalChgDTO> externalCharges;
	private SegmentSSRAssembler segmentSSRAssembler;

	public void addExternalCharges(ExternalChgDTO externalCharge) {
		if (externalCharge == null)
			return;
		if (this.externalCharges == null) {
			this.externalCharges = new ArrayList<ExternalChgDTO>();
		}
	}

	public ExternalChgDTO getAncillaryChargeDTO(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = getAncillaryCharge(externalCharge);
		if (total == null)
			return null;
		ExternalChgDTO chgDTO = new ExternalChgDTO();
		chgDTO.setAmount(total);
		chgDTO.setExternalChargesEnum(externalCharge);
		return chgDTO;
	}

	public BigDecimal getAncillaryCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = BigDecimal.ZERO;
		if (externalCharges != null) {
			for (ExternalChgDTO charge : externalCharges) {
				if (charge.getExternalChargesEnum() == externalCharge) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		} else {
			return null;
		}
		return total;
	}

	public ExternalChgDTO getExternalCharge(EXTERNAL_CHARGES externalCharge) {
		return getAncillaryChargeDTO(externalCharge);
	}

	public Collection<ExternalChgDTO> getExternalCharges() {
		if (externalCharges != null) {
			return externalCharges;
		} else {
			return new ArrayList<ExternalChgDTO>();
		}
	}

	public void setRPH(String rph) {
		this.rph = rph;
	}

	public String getRPH() {
		return rph;
	}

	public SegmentSSRAssembler getSSRAssembler() {
		return segmentSSRAssembler;
	}

	public void setSegmentSSRAssembler(SegmentSSRAssembler segmentSSRAssembler) {
		this.segmentSSRAssembler = segmentSSRAssembler;
	}

	@Override
	public ExternalChgDTO getInfantExternalCharge(EXTERNAL_CHARGES externalCharge) {		
		return null;
	}
}
