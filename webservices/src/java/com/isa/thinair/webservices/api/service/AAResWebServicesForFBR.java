/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.api.service;

import javax.ejb.TransactionAttributeType;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRQ;
import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRS;
import org.opentravel.ota._2003._05.AAOTAPaxCreditRS;
import org.opentravel.ota._2003._05.AAOTAPaxCreditReadRQ;
import org.opentravel.ota._2003._05.AAOTAResAuditRS;
import org.opentravel.ota._2003._05.AAOTAResAuditReadRQ;
import org.opentravel.ota._2003._05.AAOTATermsNConditionsRQ;
import org.opentravel.ota._2003._05.AAOTATermsNConditionsRS;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirFlifoRQ;
import org.opentravel.ota._2003._05.OTAAirFlifoRS;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAAirScheduleRQ;
import org.opentravel.ota._2003._05.OTAAirScheduleRS;
import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAResRetrieveRS;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAReadRQExt;

/**
 * Web Service(s) for core reservation services
 * 
 * @author Mohamed Nasly
 * @since 1.0
 */

@WebService(name = "AAResWebServicesForFBR", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAResWebServicesForFBR")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAResWebServicesForFBR {

	/**
	 * OTA Ping service. Clients can use this service for health check.
	 * 
	 * @param oTAPingRQ
	 * @return OTAPingRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "ping", action = "ping")
	@WebResult(name = "OTA_PingRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public OTAPingRS ping(
			@WebParam(name = "OTA_PingRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAPingRQ oTAPingRQ);

	/**
	 * Flight availability for a given airport pair, for a specific date, for a given number and type of passengers.
	 * 
	 * Clients should call the service with empty transaction when availability search is done for a new booking and
	 * should call with previous transaction when doing availability search for add/modify segment.
	 * 
	 * @param oTAAirAvailRQ
	 * @return OTAAirAvailRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.STARTORMIDDLE)
	@WebMethod(operationName = "getAvailability", action = "getAvailability")
	@WebResult(name = "OTA_AirAvailRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirAvailRS
			getAvailability(
					@WebParam(name = "OTA_AirAvailRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirAvailRQ oTAAirAvailRQ);

	/**
	 * Requests pricing information for specific flights on certain dates for a specific number and type of passengers.
	 * Clients should call the service specifying the previous transaction.
	 * 
	 * @param otaAirPriceRQ
	 * @return OTAAirPriceRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "getPrice", action = "getPrice")
	@WebResult(name = "OTA_AirPriceRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirPriceRS
			getPrice(
					@WebParam(name = "OTA_AirPriceRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirPriceRQ otaAirPriceRQ);

	/**
	 * Return the reservations list. Clients should always call the service with empty transaction specified.
	 * 
	 * @param oTAReadRQ
	 * @return OTAResRetrieveRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRES_NEW, boundry = WebServiceTransaction.TransactionBoundry.START)
	@WebMethod(operationName = "getReservationsList", action = "getReservationsList")
	@WebResult(name = "OTA_ResRetrieveRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAResRetrieveRS
			getReservationsList(
					@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ oTAReadRQ,
					@WebParam(name = "AAReadRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAReadRQExt aaReadRQExt);

	/**
	 * Returns detailed reservation information. Clients should always call the service with empty transaction
	 * specified.
	 * 
	 * @param oTAReadRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRES_NEW, boundry = WebServiceTransaction.TransactionBoundry.START)
	@WebMethod(operationName = "getReservationbyPNR", action = "getReservationbyPNR")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			getReservationbyPNR(
					@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ oTAReadRQ,
					@WebParam(name = "AAReadRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAReadRQExt aaReadRQExt);

	/**
	 * Book a specific itinerary on specified booking class (and fare) for one or more passengers with or without
	 * payment specified. Client must call this service with the transaction initiated by availability search/price
	 * quote.
	 * 
	 * @param otaAirBookRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "book", action = "book")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			book(@WebParam(name = "OTA_AirBookRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirBookRQ otaAirBookRQ,
					@WebParam(name = "AAAirBookRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAAirBookRQExt aaAirBookRQExt);

	/**
	 * Returns flight Schedules information.
	 * 
	 * @param OTAAirScheduleRQ
	 * @return OTAAirScheduleRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getFlightSchedule", action = "getFlightSchedule")
	@WebResult(name = "OTA_AirScheduleRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirScheduleRS
			getFlightSchedule(
					@WebParam(name = "OTA_AirScheduleRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirScheduleRQ otaAirScheduleRQ);

	/**
	 * Returns flight information.
	 * 
	 * @param OTAAirFlifoRQ
	 * @return OTAAirFlifoRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getFlightInfo", action = "getFlightInfo")
	@WebResult(name = "OTA_AirFlifoRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirFlifoRS
			getFlightInfo(
					@WebParam(name = "OTA_AirFlifoRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirFlifoRQ otaAirFlifoRQ);

	/**
	 * Modify Reservation.
	 * 
	 * @param otaAirBookModifyRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "modifyReservation", action = "modifyReservation")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			modifyReservation(
					@WebParam(name = "OTA_AirBookModifyRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirBookModifyRQ otaAirBookModifyRQ,
					@WebParam(name = "AAAirBookModifyRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAAirBookModifyRQExt aaAirModifyRQExt);

	/**
	 * Modify reservation queries.
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirModifyRQExt
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "modifyResQuery", action = "modifyResQuery")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			modifyResQuery(
					@WebParam(name = "OTA_AirBookModifyRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirBookModifyRQ otaAirBookModifyRQ,
					@WebParam(name = "AAAirBookModifyRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAAirBookModifyRQExt aaAirModifyRQExt);

	/**
	 * Returns agent available credit amount.
	 * 
	 * @param agentAvailCreditRQ
	 * @return AAOTAAgentAvailCreditRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getAgentAvailableCredit", action = "getAgentAvailableCredit")
	@WebResult(name = "AA_OTA_AgentAvailCreditRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAAgentAvailCreditRS
			getAgentAvailableCredit(
					@WebParam(name = "AA_OTA_AgentAvailCreditRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAgentAvailCreditRQ agentAvailCreditRQ);

	/**
	 * Retrieve Passenger Credit.
	 * 
	 * @param passengerCreditRQ
	 * @return AAOTAPaxCreditRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getPassengerCredit", action = "getPassengerCredit")
	@WebResult(name = "AA_OTA_PaxCreditRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAPaxCreditRS
			getPassengerCredit(
					@WebParam(name = "AA_OTA_PaxCreditReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAPaxCreditReadRQ aaOTAPaxCreditReadRQ);

	/**
	 * Returns Terms & Conditions.
	 * 
	 * @param aaOTATermsNConditionsRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getTermsNConditions", action = "getTermsNConditions")
	@WebResult(name = "AA_OTA_TermsNConditionsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTATermsNConditionsRS
			getTermsNConditions(
					@WebParam(name = "AA_OTA_TermsNConditionsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTATermsNConditionsRQ aaOTATermsNConditionsRQ);

	/**
	 * Returns audit history.
	 * 
	 * @param aaOTAuditReadRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getResAuditHistory", action = "getResAuditHistory")
	@WebResult(name = "AA_OTA_ResAuditRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAResAuditRS
			getResAuditHistory(
					@WebParam(name = "AA_OTA_ResAuditReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAResAuditReadRQ aaOTAuditReadRQ);
}