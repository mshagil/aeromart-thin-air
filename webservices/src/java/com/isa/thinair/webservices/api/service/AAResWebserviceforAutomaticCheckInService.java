package com.isa.thinair.webservices.api.service;

import com.isa.thinair.commons.api.dto.ets.AutomaticCheckInReq;
import com.isa.thinair.commons.api.dto.ets.AutomaticCheckInRes;
import com.isa.thinair.commons.api.dto.ets.TicketDisplayReq;
import com.isa.thinair.commons.api.dto.ets.TicketDisplayRes;

public interface AAResWebserviceforAutomaticCheckInService {
	
	AutomaticCheckInRes display(AutomaticCheckInReq request);
}
