package com.isa.thinair.webservices.api.service;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isaaviation.thinair.webservices.api.connectiondetails.AAGetConnectionPaxRQ;
import com.isaaviation.thinair.webservices.api.connectiondetails.AAGetConnectionPaxRS;
import com.isaaviation.thinair.webservices.api.connectiondetails.AAGetRouteRQ;
import com.isaaviation.thinair.webservices.api.connectiondetails.AAGetRouteRS;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "AAWebServicesForGetConnection", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForGetConnection")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAWebServicesForGetConnection {


    @WebServiceSource(source = WebServiceSource.MethodSource.AA)
    @WebMethod(operationName = "getConnectionPax", action = "getConnectionPax")
    @WebResult(name = "AAGetConnectionPaxRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/connectiondetails")
    public AAGetConnectionPaxRS
        getConnectionPax(
            @WebParam(name = "AAGetConnectionPaxRQ",
                targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/connectiondetails")
                    AAGetConnectionPaxRQ aaGetConnectionPaxRQ);

    @WebServiceSource(source = WebServiceSource.MethodSource.AA)
    @WebMethod(operationName = "getConnections", action = "getConnections")
    @WebResult(name = "AAGetRouteRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/connectiondetails")
    public AAGetRouteRS
        getConnections(
            @WebParam(name = "AAGetRouteRQ",
                targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/connectiondetails")
                    AAGetRouteRQ aaGetRouteRQ);

}
