package com.isa.thinair.webservices.api.dtos;

import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class SelectedOndFareDTO {
	private Collection<OndFareDTO> fareDTOs;
	private int fareType;

	public Collection<OndFareDTO> getFareDTOs() {
		return fareDTOs;
	}

	public void setFareDTOs(Collection<OndFareDTO> fareDTOs) {
		this.fareDTOs = fareDTOs;
	}

	public int getFareType() {
		return fareType;
	}

	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

}
