package com.isa.thinair.webservices.api.dtos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class OTAPaxSummaryDTO {

	public static enum ChargeType {
		CNX, MOD
	}

	private int paxPnrId;
	private BigDecimal totalChargesCurrent = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalChargesNew = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalPaymentsCurrent = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal balance = AccelAeroCalculator.getDefaultBigDecimalZero();

	public static Collection<OTAPaxSummaryDTO> create(Reservation reservation, ChargeType chargeType, Integer... segmentIds)
			throws ModuleException {
		Map pnrPaxMap = new HashMap();
		// List<OTAPaxSummaryDTO> list=new ArrayList<OTAPaxSummaryDTO>();
		List<Integer> ondCheck = new ArrayList<Integer>();
		Collection<PnrChargesDTO> colOndCharges = WebServicesModuleUtils.ReservationUtil.getOndChargesForCancellation(
				reservation.getPnr(), reservation.getVersion(), segmentIds);
		for (Iterator ii = reservation.getPassengers().iterator(); ii.hasNext();) {
			ReservationPax pax = (ReservationPax) ii.next();
			if (!pnrPaxMap.containsKey(pax.getPnrPaxId())) {
				OTAPaxSummaryDTO dto = new OTAPaxSummaryDTO();
				pnrPaxMap.put(pax.getPnrPaxId(), dto);
				dto.setPaxPnrId(pax.getPnrPaxId());
			}
			fares: for (Iterator iii = pax.getPnrPaxFares().iterator(); iii.hasNext();) {
				ReservationPaxFare paxFare = (ReservationPaxFare) iii.next();
				for (Iterator iv = paxFare.getPaxFareSegments().iterator(); iv.hasNext();) {
					ReservationPaxFareSegment paxFareSegment = (ReservationPaxFareSegment) iv.next();
					if (ondCheck.indexOf(paxFareSegment.getSegment().getOndGroupId()) > -1) {
						continue fares;
					} else
						ondCheck.add(paxFareSegment.getSegment().getOndGroupId());
					// DebugUtil.dumpValues(log, paxFare);
					OTAPaxSummaryDTO paxSummaryDTO = (OTAPaxSummaryDTO) pnrPaxMap.get(pax.getPnrPaxId());
					boolean segmentInRequest = false;
					for (int x = 0; x < segmentIds.length; x++) {
						if (segmentIds[x] == paxFareSegment.getPnrSegId().intValue())
							segmentInRequest = true;
					}
					if (segmentInRequest) {
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalAdjustmentCharge()));
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalCancelCharge()));
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalModificationCharge()));
					} else {
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalAdjustmentCharge()));
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalCancelCharge()));
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalModificationCharge()));
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalFare()));
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalSurCharge()));
						paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
								paxFare.getTotalTaxCharge()));
						// log.info(paxSummaryDTO.getTotalChargesNew());
					}
				}
			}
			OTAPaxSummaryDTO paxSummaryDTO = (OTAPaxSummaryDTO) pnrPaxMap.get(pax.getPnrPaxId());
			PnrChargesDTO pnrCharges = CommonServicesUtil.getFrom(colOndCharges, "getPnrPaxId", paxSummaryDTO.getPaxPnrId());
			paxSummaryDTO.setTotalChargesNew(AccelAeroCalculator.add(paxSummaryDTO.getTotalChargesNew(),
					pnrCharges.getIdentifiedCancellationAmount()));
		}
		return pnrPaxMap.values();
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public int getPaxPnrId() {
		return paxPnrId;
	}

	public void setPaxPnrId(int paxPnrId) {
		this.paxPnrId = paxPnrId;
	}

	public BigDecimal getTotalChargesCurrent() {
		return totalChargesCurrent;
	}

	public void setTotalChargesCurrent(BigDecimal totalChargesCurrent) {
		this.totalChargesCurrent = totalChargesCurrent;
	}

	public BigDecimal getTotalChargesNew() {
		return totalChargesNew;
	}

	public void setTotalChargesNew(BigDecimal totalChargesNew) {
		this.totalChargesNew = totalChargesNew;
	}

	public BigDecimal getTotalPaymentsCurrent() {
		return totalPaymentsCurrent;
	}

	public void setTotalPaymentsCurrent(BigDecimal totalPaymentsCurrent) {
		this.totalPaymentsCurrent = totalPaymentsCurrent;
	}

	@Override
	public boolean equals(Object obj) {
		boolean flag = false;
		if (obj instanceof OTAPaxSummaryDTO) {
			OTAPaxSummaryDTO dto = (OTAPaxSummaryDTO) obj;
			if (dto.getPaxPnrId() == getPaxPnrId())
				flag = true;
		} else if (obj instanceof Integer) {
			if (getPaxPnrId() == ((Integer) obj).intValue())
				flag = true;
		}
		return flag;
	}
}
