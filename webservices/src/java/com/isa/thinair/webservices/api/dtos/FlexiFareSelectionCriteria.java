package com.isa.thinair.webservices.api.dtos;

import java.io.Serializable;

/**
 * This class indicates the user selection of flexi fares.
 * 
 * @author sanjaya
 *
 */
public class FlexiFareSelectionCriteria implements Serializable {

	private static final long serialVersionUID = 7392158616863859533L;
	
	private boolean inBoundFlexiFareSelected = false;
	
	private boolean outBoundFlexiFareSelected = false;

	public boolean isInBoundFlexiFareSelected() {
		return inBoundFlexiFareSelected;
	}

	public void setInBoundFlexiFareSelected(boolean inBoudFlexiFareSelected) {
		this.inBoundFlexiFareSelected = inBoudFlexiFareSelected;
	}

	public boolean isOutBoundFlexiFareSelected() {
		return outBoundFlexiFareSelected;
	}

	public void setOutBoundFlexiFareSelected(boolean outBoundFlexiFareSelected) {
		this.outBoundFlexiFareSelected = outBoundFlexiFareSelected;
	} 
}
