/**
 * 
 */
package com.isa.thinair.webservices.api.dtos;

import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.webplatform.api.util.CommonUtil;

/**
 * @author nafly
 * 
 */
public class OTAPaxCountTO implements IPaxCountAssembler {

	private int adultCount = 0;
	private int childCount = 0;
	private int infantCount = 0;

	public OTAPaxCountTO(TravelerInfoSummaryTO infoSummaryTO) {

		for (PassengerTypeQuantityTO quantityTO : infoSummaryTO.getPassengerTypeQuantityList()) {
			if (PaxTypeTO.ADULT.equals(quantityTO.getPassengerType())) {
				this.adultCount = quantityTO.getQuantity();
			} else if (PaxTypeTO.CHILD.equals(quantityTO.getPassengerType())) {
				this.childCount = quantityTO.getQuantity();
			} else if (PaxTypeTO.INFANT.equals(quantityTO.getPassengerType())) {
				this.infantCount = quantityTO.getQuantity();
			}
		}

	}

	public OTAPaxCountTO(int adults, int children, int infants) {
		this.adultCount = adults;
		this.childCount = children;
		this.infantCount = infants;
	}

	@Override
	public int getInfantCount() {
		return infantCount;
	}

	@Override
	public int getChildCount() {
		return childCount;
	}

	@Override
	public int getAdultCount() {
		return adultCount;
	}

	/**
	 * 
	 * @param paxType
	 * @return
	 */
	public int getPaxCountByOTAPaxType(String paxType) {

		if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
			return this.adultCount;
		} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
			return this.childCount;
		} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
			return this.infantCount;
		}

		return 0;
	}


}
