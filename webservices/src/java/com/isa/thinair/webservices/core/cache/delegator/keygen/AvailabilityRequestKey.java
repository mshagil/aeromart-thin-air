package com.isa.thinair.webservices.core.cache.delegator.keygen;

import java.util.Date;
import java.util.List;

import org.opentravel.ota._2003._05.AirSearchPrefsType.CabinPref;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation;
import org.opentravel.ota._2003._05.TimeInstantType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.util.CacheConstant.CacheKeyParamName;

public class AvailabilityRequestKey extends OTARequestKeyUtil {

	private OTAAirAvailRQ request;
	private String operationCode;

	public AvailabilityRequestKey(OTAAirAvailRQ request, String operationCode) {
		this.request = request;
		this.operationCode = operationCode;
	}

	public String generateKey() throws ModuleException, WebservicesException {
		
		validate();
		StringBuilder key = new StringBuilder();
		
		key.append(getKeyHeader(this.operationCode)).append(moduleSeparator);
		key.append(getPointOfSalesKeyInfo(request.getPOS()));
		key.append(getSpecificAgentKeyInfo());
		key.append(getCurrencyKeyInfo());
		key.append(getTravelerQuantityInfo(request.getTravelerInfoSummary()));
		key.append(getOriginDestinationInfo());
		key.append(getAdditionalInfo());
		
		return key.toString();
	}

	private String getAdditionalInfo() {

		StringBuilder addtionalInfo = new StringBuilder();

		if (request.getFlexiQuote() != null) {
			appendKeyValue(addtionalInfo, CacheKeyParamName.FLEXI_QUOTE,  request.getFlexiQuote() == true ? 1 : 0);
		}

		appendKeyValue(addtionalInfo, CacheKeyParamName.DIRECT_FLIGHT, request.isDirectFlightsOnly() == true ? 1 : 0);

		return addtionalInfo.toString();
	}

	private String getCurrencyKeyInfo() {

		StringBuilder currencybuilder = new StringBuilder();
		TravelerInfoSummaryType travelerInfoSummaryType = request.getTravelerInfoSummary();

		if (travelerInfoSummaryType.getPriceRequestInformation() != null) {
			appendKeyValueWithSeparator(currencybuilder, CacheKeyParamName.CURRENCY,
					travelerInfoSummaryType.getPriceRequestInformation().getCurrencyCode());
		}

		return currencybuilder.toString();
	}

	private void validate() throws WebservicesException {

		if (request == null || request.getModifiedSegmentInfo() != null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION, "Cache doesn't support");
		}

	}

	private String getOriginDestinationInfo() throws ModuleException {

		StringBuilder originDestinationKey = new StringBuilder();
		List<OriginDestinationInformation> originDestinationInformations = request.getOriginDestinationInformation();

		if (originDestinationInformations != null) {

			for (OriginDestinationInformation originDestinationInformation : originDestinationInformations) {

				if (originDestinationInformation.getTravelPreferences() != null
						&& originDestinationInformation.getTravelPreferences().getCabinPref() != null) {

					CabinPref cabinPref = BeanUtils
							.getFirstElement(originDestinationInformation.getTravelPreferences().getCabinPref());

					if (cabinPref.getCabin() != null) {
						appendKeyValueWithSeparator(originDestinationKey, CacheKeyParamName.CABIN_CLASS, cabinPref.getCabin().value());
					}
				}

				getBookingClassKey();

				appendWithSeparator(originDestinationKey, originDestinationInformation.getOriginLocation().getLocationCode());
				appendWithSeparator(originDestinationKey, originDestinationInformation.getDestinationLocation().getLocationCode());

				TimeInstantType timeInstantType = originDestinationInformation.getDepartureDateTime();

				if (timeInstantType != null) {

					Date departureDate = CommonUtil.parseDate(timeInstantType.getValue()).getTime();
					appendWithSeparator(originDestinationKey, CalendarUtil.formatDateYYYYMMDD(departureDate));

					if (timeInstantType.getWindowBefore() != null) {
						appendKeyValue(originDestinationKey, CacheKeyParamName.SEARCH_VARIANCE_BEFORE,
								timeInstantType.getWindowBefore().getDays());
					}

					if (timeInstantType.getWindowAfter() != null) {
						appendKeyValue(originDestinationKey, CacheKeyParamName.SEARCH_VARIANCE_AFTER,
								timeInstantType.getWindowAfter().getDays());
					}

				}

			}

		}

		return originDestinationKey.toString();
	}
	
	

	private String getBookingClassKey() {

		StringBuilder bookingClassKey = new StringBuilder();

		if (request.getSpecificFlightInfo() != null && request.getSpecificFlightInfo().getBookingClassPref() != null) {
			appendKeyValueWithSeparator(bookingClassKey, CacheKeyParamName.BOOKING_CLASS,
					request.getSpecificFlightInfo().getBookingClassPref().getResBookDesigCode());
		}

		return bookingClassKey.toString();
	}

	
}
