package com.isa.thinair.webservices.core.service;

import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.service.AAWebServicesForGetConnection;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.PaxConnectionUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isaaviation.thinair.webservices.api.connectiondetails.*;

import javax.jws.WebService;

import java.util.Collection;

@WebService(serviceName = "AAWebServicesForGetConnection", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForGetConnection")
public class AAWebServicesForGetConnectionImpl  implements AAWebServicesForGetConnection {

    @Override
    public AAGetConnectionPaxRS getConnectionPax(AAGetConnectionPaxRQ aaGetConnectionPaxRQ) {

        AAStatusErrorsAndWarnings status = new AAStatusErrorsAndWarnings();
        AAGetConnectionPaxRS aaGetConnectionPaxRS = new AAGetConnectionPaxRS();

        try {

            validateRequest(aaGetConnectionPaxRQ);

			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

            WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.WSFuncPrivilegesKeys.WS_CONNECTION_PX);

            aaGetConnectionPaxRS = PaxConnectionUtil.getConnectionPax(aaGetConnectionPaxRQ);

            status.setSuccess(true);

        } catch(Exception ex) {

            status = createError(ex);

        } finally {
            aaGetConnectionPaxRS.setStatusErrorsAndWarnings(status);
        }

        return aaGetConnectionPaxRS;
    }

    @Override
    public AAGetRouteRS getConnections(AAGetRouteRQ aaGetRouteRQ) {

        AAStatusErrorsAndWarnings status = new AAStatusErrorsAndWarnings();

        AAGetRouteRS aaGetRouteRS = new AAGetRouteRS();

        try {

            validateRequest(aaGetRouteRQ);

			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

            WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.WSFuncPrivilegesKeys.WS_ROUTES_FOR_GIVEN_ORIGINS);

            aaGetRouteRS = PaxConnectionUtil.getConnections(aaGetRouteRQ);

            status.setSuccess(true);

        } catch (Exception e) {
            status = createError(e);

        } finally {
            aaGetRouteRS.setStatusErrorsAndWarnings(status);
        }

        return aaGetRouteRS;
    }

    private void validateRequest(AAGetConnectionPaxRQ request) throws WebservicesException {

        if(request.getStartDate() == 0 || request.getEndDate() == 0 || request.getStartDate() > request.getEndDate() ||
                request.getConnectionTime() < 0 || request.getSegments() == null || request.getSegments().isEmpty()) {

            throw new WebservicesException("","Invalid input parameter");
        }
    }

    private void validateRequest(AAGetRouteRQ request) throws WebservicesException {

        if(request.getOrigin() == null || request.getOrigin().isEmpty()) {
            throw new WebservicesException("","Invalid input parameter");
        }
    }

    private AAStatusErrorsAndWarnings createError(Exception e) {
        com.isaaviation.thinair.webservices.api.airinventory.AAStatusErrorsAndWarnings error = ExceptionUtil.addAAErrrorsAndWarnings(e);

        AAStatusErrorsAndWarnings aaStatusErrorsAndWarnings = new AAStatusErrorsAndWarnings();
        aaStatusErrorsAndWarnings.setErrorCode(error.getErrorCode());
        aaStatusErrorsAndWarnings.setErrorText(error.getErrorText());
        aaStatusErrorsAndWarnings.setWarningMessage(error.getWarningMessage());
        aaStatusErrorsAndWarnings.setSuccess(error.isSuccess());

        return aaStatusErrorsAndWarnings;
    }
}
