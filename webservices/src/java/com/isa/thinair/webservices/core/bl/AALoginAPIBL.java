package com.isa.thinair.webservices.core.bl;

import java.util.Collection;
import java.util.List;

import javax.ejb.EJBAccessException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirReservationType;

import ae.isa.api.login.AAAPILoginRQ;
import ae.isa.api.login.AAAPILoginRS;
import ae.isa.api.login.ContactInformation;
import ae.isa.api.login.Customer;
import ae.isa.api.login.Error;
import ae.isa.api.login.ErrorType;
import ae.isa.api.login.Reservations;
import ae.isa.api.login.ResponseStatus;
import ae.isa.api.login.SecurityInformation;

import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.interlineUtil.ReservationQueryInterlineUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

public class AALoginAPIBL {

	private static Log log = LogFactory.getLog(AALoginAPIBL.class);

	public static AAAPILoginRS login(AAAPILoginRQ loginRQ) {

		AirCustomerServiceBD airCustomerServiceBD = WebServicesModuleUtils.getAirCustomerServiceBD();
		ReservationQueryInterlineUtil reservationQueryInterlineUtil = new ReservationQueryInterlineUtil();

		AAAPILoginRS loginRS = new AAAPILoginRS();
		int count = 1;
		String token;
		List<ReservationDTO> immediateReservations;

		LCCClientPnrModesDTO pnrModesDTO;
		ReservationListTO reservationListTO;
		ModificationParamRQInfo modificationParamRQInfo;
		LCCClientReservation reservation;
		AirReservationType airReservation;
		ErrorType errorType = new ErrorType();
		ae.isa.api.login.Error error;
		com.isa.thinair.aircustomer.api.model.Customer customer;
		Collection<String> privilegesKeys;

		try {

			customer = airCustomerServiceBD.authenticate(loginRQ.getUserName(), loginRQ.getPassword());
			privilegesKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

			if (AuthorizationUtil.hasPrivileges(privilegesKeys,
					PrivilegesKeys.WSFuncPrivilegesKeys.WS_LOGIN_API_ACCESS)) {

				if (customer != null) {

					loginRS.setCustomer(toCustomer(customer));

					if (AuthorizationUtil.hasPrivileges(privilegesKeys,
							PrivilegesKeys.WSFuncPrivilegesKeys.WS_LOGIN_API_GENERATE_TOKEN)) {

						SecurityInformation securityInformation = new SecurityInformation();
						token = airCustomerServiceBD.generateToken();
						securityInformation.setToken(token);
						loginRS.setSecurityInformation(securityInformation);
						airCustomerServiceBD.createCustomerSession(customer.getCustomerId(), token);

					}

					if (AuthorizationUtil.hasPrivileges(privilegesKeys,
							PrivilegesKeys.WSFuncPrivilegesKeys.WS_LOGIN_API_VIEW_RESERVATIONS)) {

						immediateReservations = WebServicesModuleUtils.getReservationBD().getImmediateReservations(customer.getCustomerId(), count);

						if (!immediateReservations.isEmpty()) {

							AALoadDataOptionsType aaLoadDataOptionsType = new AALoadDataOptionsType();
							aaLoadDataOptionsType.setLoadAirItinery(true);
							aaLoadDataOptionsType.setLoadTravelerInfo(true);
							aaLoadDataOptionsType.setLoadPriceInfoTotals(true);
							aaLoadDataOptionsType.setLoadFullFilment(true);

							pnrModesDTO = new LCCClientPnrModesDTO();
							pnrModesDTO.setLoadFares(true);
							pnrModesDTO.setLoadOndChargesView(true);
							pnrModesDTO.setLoadPaxAvaBalance(true);
							pnrModesDTO.setLoadSegViewBookingTypes(true);
							pnrModesDTO.setLoadSegView(true);
							pnrModesDTO.setLoadPaxAvaBalance(true);
							pnrModesDTO.setLoadSeatingInfo(true);
							pnrModesDTO.setLoadMealInfo(true);
							pnrModesDTO.setLoadBaggageInfo(true);
							pnrModesDTO.setLoadLastUserNote(true);// false
							pnrModesDTO.setRecordAudit(true); // false
							pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
							pnrModesDTO.setLoadEtickets(true);

							modificationParamRQInfo = new ModificationParamRQInfo();
							modificationParamRQInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
							modificationParamRQInfo.setIsRegisteredUser(false);

							Reservations reservations = new Reservations();
							List<AirReservationType> resList = reservations.getReservation();

							ThreadLocalData.getCurrentWSContext();

							for (ReservationDTO res : immediateReservations) {
								if (res.getOriginatorPnr() == null) {
									pnrModesDTO.setPnr(res.getPnr());
								} else {
									pnrModesDTO.setGroupPNR(res.getOriginatorPnr());
								}

								reservation = WebServicesModuleUtils.getAirproxyReservationBD()
										.searchReservationByPNR(pnrModesDTO, null, getTrackInfo());

								airReservation = reservationQueryInterlineUtil.generateAirReservationType(reservation, aaLoadDataOptionsType);

								resList.add(airReservation);
							}
							loginRS.setReservations(reservations);
						}
					}


					loginRS.setStatus(ResponseStatus.SUCCESS);

				} else {
					loginRS = new AAAPILoginRS();
					loginRS.setStatus(ResponseStatus.FAIL);
					error = new Error();
					error.setErrorCode(WebservicesConstants.WebServicesErrorCodes.CREDENTIALS_INVALID.getErrorCode());
					error.setDescription(WebservicesConstants.WebServicesErrorCodes.CREDENTIALS_INVALID.getErrorDescription());
					errorType.getError().add(error);
				}
			} else {
				loginRS = new AAAPILoginRS();
				loginRS.setStatus(ResponseStatus.FAIL);
				error = new Error();
				error.setErrorCode(WebservicesConstants.WebServicesErrorCodes.WS_AUTH_USER_INSUFFICIENT_PRIV.getErrorCode());
				error.setDescription(WebservicesConstants.WebServicesErrorCodes.WS_AUTH_USER_INSUFFICIENT_PRIV.getErrorDescription());
				errorType.getError().add(error);
			}

		} catch (EJBAccessException e) {
			loginRS = new AAAPILoginRS();
			loginRS.setStatus(ResponseStatus.FAIL);
			error = new Error();
			error.setErrorCode(WebservicesConstants.WebServicesErrorCodes.WS_AUTHENTICATION_FAILED.getErrorCode());
			error.setDescription(WebservicesConstants.WebServicesErrorCodes.WS_AUTHENTICATION_FAILED.getErrorDescription());
			errorType.getError().add(error);

			log.error("login(AAAPILoginRQ) ---- ", e);
		} catch (Exception e) {
			loginRS = new AAAPILoginRS();
			loginRS.setStatus(ResponseStatus.FAIL);
			error = new Error();
			error.setErrorCode(WebservicesConstants.WebServicesErrorCodes.SERVER_ERROR.getErrorCode());
			error.setDescription(WebservicesConstants.WebServicesErrorCodes.SERVER_ERROR.getErrorDescription());
			errorType.getError().add(error);

			log.error("login(AAAPILoginRQ) ---- ", e);
		}

		if (!errorType.getError().isEmpty()) {
			loginRS.setError(errorType);
		} else {

		}

		return loginRS;
	}

	private static Customer toCustomer(com.isa.thinair.aircustomer.api.model.Customer customer)
			throws ModuleException {
		CommonMasterBD commonMasterBD = WebServicesModuleUtils.getCommonMasterBD();
		String countryName;
		String nationalityName;

		Customer cust = new Customer();
		cust.setTitle(customer.getTitle());
		cust.setFirstName(StringUtil.toInitCap(customer.getFirstName()));
		cust.setLastName(StringUtil.toInitCap(customer.getLastName()));

		countryName = commonMasterBD.getCountryByName(customer.getCountryCode()).getCountryName();
		cust.setCountryOfResidence(countryName);

		nationalityName = commonMasterBD.getNationality(customer.getNationalityCode()).getDescription();
		cust.setNationality(nationalityName);

		ContactInformation contactInformation = new ContactInformation();
		contactInformation.setEmailAddress(customer.getEmailId());
		contactInformation.setMobileNumber(customer.getMobile());
		cust.setContactInformation(contactInformation);

		return cust;
	}


	private static TrackInfoDTO getTrackInfo() {
		String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
		String ipAddress = BeanUtils.nullHandler(ThreadLocalData.getWSContextParam(
				WebservicesContext.WEB_REQUEST_IP));

		TrackInfoDTO trackInfo = new TrackInfoDTO();
		trackInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
		trackInfo.setSessionId(sessionId);
		trackInfo.setIpAddress(ipAddress);
		if (ipAddress.length() > 0) {
			long ipNumber = AmadeusCommonUtil.getReservationOriginIPAddress(ipAddress);
			trackInfo.setOriginIPNumber(ipNumber);
		}
		trackInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

		return trackInfo;
	}
}
