package com.isa.thinair.webservices.core.adaptor.util;

import java.util.Collection;

import com.isa.thinair.webservices.core.adaptor.Adaptor;

public class AdaptorUtil {
	public static <S, T> void adaptCollection(Collection<S> source, Collection<T> target, Adaptor<S, T> adaptor) {
		for (S sourceItem : source) {
			T targetItem = adaptor.adapt(sourceItem);
			if (targetItem != null) {
				target.add(targetItem);
			}
		}
	}
}
