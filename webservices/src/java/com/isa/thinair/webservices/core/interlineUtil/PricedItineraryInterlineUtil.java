package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opentravel.ota._2003._05.AABundledServiceExt;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.BundledService;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.PricingSourceType;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.api.dtos.FlexiFareSelectionCriteria;
import com.isa.thinair.webservices.api.dtos.OTAPaxCountTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.OTAUtils;
import com.isa.thinair.webservices.core.util.ParameterUtil;
import com.isa.thinair.webservices.core.util.ServiceTaxUtil;
import com.isa.thinair.webservices.core.util.WSFlexiFareUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * Prepare PricedItinerary from SelectedFlightDTO
 * 
 * @author Mohamed Nasly
 * @author nafly - Changed to support interline/dry
 */
public class PricedItineraryInterlineUtil {

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	public static void preparePricedItinerary(PricedItineraryType pricedItinerary,
			List<OriginDestinationOptionTO> selectedOBFlightOptions, List<OriginDestinationOptionTO> selectedIBFlightOptions,
			BaseAvailRQ baseAvailRQ, boolean isFixedPriceQuote, FlexiFareSelectionCriteria flexiSelectionCriteria,
			BaseAvailRS baseAvailRS, QuotedPriceInfoTO quotedPriceInfoTO, boolean isMultiCity) throws WebservicesException,
			ModuleException {
		// set flight segments info
		PriceInfoTO priceInfoTO = baseAvailRS.getSelectedPriceFlightInfo();
		int fareType = priceInfoTO.getFareTypeTO().getFareType();

		boolean isOutboundHasONDFare = false;
		boolean isInboudHasONDFare = false;
		if (fareType == FareTypes.OND_FARE || fareType == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE) {
			isOutboundHasONDFare = true;
		}
		boolean showBookingClass = (baseAvailRQ.getTravelPreferences().getBookingClassCode() != null ? true : false);

		if (isMultiCity) {
			int ondSequence = 0;
			for (OriginDestinationOptionTO ondDestinationOptionTO : selectedOBFlightOptions) {
				addOriginDestinationOptions(pricedItinerary, ondDestinationOptionTO, isOutboundHasONDFare, showBookingClass,
						ondSequence);
				ondSequence++;
			}
		} else {
			for (OriginDestinationOptionTO ondDestinationOptionTO : selectedOBFlightOptions) {
				addOriginDestinationOptions(pricedItinerary, ondDestinationOptionTO, isOutboundHasONDFare, showBookingClass,
						OndSequence.OUT_BOUND);
			}
		}

		if (priceInfoTO.hasReturnFlights()) {
			quotedPriceInfoTO.setReturnQuote(true);
		}
		if (!selectedIBFlightOptions.isEmpty()) {
			if (fareType == FareTypes.OND_FARE || fareType == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE) {
				isInboudHasONDFare = true;
			}
			for (OriginDestinationOptionTO ondDestinationOptionTO : selectedIBFlightOptions) {
				addOriginDestinationOptions(pricedItinerary, ondDestinationOptionTO, isInboudHasONDFare, showBookingClass,
						OndSequence.IN_BOUND);
			}
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		// set individual passenger fare/taxes breakdowns and total fare/taxes
		setItineraryPricingInfo(pricedItinerary, baseAvailRS, baseAvailRQ, isFixedPriceQuote, flexiSelectionCriteria,
				quotedPriceInfoTO, exchangeRateProxy);
	}

	private static void addOriginDestinationOptions(PricedItineraryType pricedItinerary,
			OriginDestinationOptionTO originDestinationOptionTO, boolean isSingleFare, boolean showBookingClass, int ondSequence)
			throws WebservicesException, ModuleException {
		String bookingClass = null;
		// TODO set booking class
		// if (showBookingClass && availableIBOBFlightSegment.getSelectedLogicalCabinClass() != null) {
		// bookingClass = availableIBOBFlightSegment.getFare(availableIBOBFlightSegment.getSelectedLogicalCabinClass())
		// .getBookingClassCode();
		// }

		AirItineraryType.OriginDestinationOptions originDestinationOptions = pricedItinerary.getAirItinerary()
				.getOriginDestinationOptions();

		Collection<FlightSegmentTO> aaFlightSegmentTOs = originDestinationOptionTO.getFlightSegmentList();

		addFlightSegments(originDestinationOptions, aaFlightSegmentTOs, isSingleFare, bookingClass);
		addBundledServices(originDestinationOptions, originDestinationOptionTO, ondSequence);
	}

	private static void addBundledServices(AirItineraryType.OriginDestinationOptions originDestinationOptions,
			OriginDestinationOptionTO originDestinationOptionTO, int ondSequence) {
		List<FlightFareSummaryTO> flightFareSummaryList = originDestinationOptionTO.getFlightFareSummaryList();

		if (flightFareSummaryList != null && !flightFareSummaryList.isEmpty()) {
			List<BundledService> availableBundledServices = new ArrayList<BundledService>();
			for (FlightFareSummaryTO flightFareSummaryTO : flightFareSummaryList) {
				if (flightFareSummaryTO.getBundledFarePeriodId() != null) {
					BundledService bundledService = new BundledService();
					bundledService.setBunldedServiceId(flightFareSummaryTO.getBundledFarePeriodId());
					bundledService.setBundledServiceName(flightFareSummaryTO.getLogicalCCDesc());
					bundledService.setPerPaxBundledFee(flightFareSummaryTO.getBundleFareFee());
					if (flightFareSummaryTO.getBookingCodes() != null) {
						bundledService.getBookingClasses().addAll(flightFareSummaryTO.getBookingCodes());
					}
					bundledService.setDescription(flightFareSummaryTO.getComment());
					bundledService.getIncludedServies().addAll(flightFareSummaryTO.getBundledFareFreeServiceNames());
					availableBundledServices.add(bundledService);
				}
			}

			if (!availableBundledServices.isEmpty()) {
				AABundledServiceExt aaBundledServiceExt = new AABundledServiceExt();
				aaBundledServiceExt.setApplicableOnd(BundleServiceInterlineUtil.getBundleApplicableONDCode(originDestinationOptionTO));
				aaBundledServiceExt.setApplicableOndSequence(ondSequence);
				aaBundledServiceExt.getBundledService().addAll(availableBundledServices);
				originDestinationOptions.getAABundledServiceExt().add(aaBundledServiceExt);
			}
		}
	}

	private static void addFlightSegments(AirItineraryType.OriginDestinationOptions originDestinationOptions,
			Collection<FlightSegmentTO> aaFlightSegmentTOs, boolean isSingleFare, String bookingClass) throws ModuleException {
		OriginDestinationOptionType originDestinationOption = null;
		for (FlightSegmentTO flightSegmentTO : aaFlightSegmentTOs) {
			if (!isSingleFare || (isSingleFare && originDestinationOption == null)) {
				originDestinationOption = new OriginDestinationOptionType();
				originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);
			}

			BookFlightSegmentType bookFlightSegment = new BookFlightSegmentType();
			originDestinationOption.getFlightSegment().add(bookFlightSegment);

			bookFlightSegment.setRPH(flightSegmentTO.getFlightRefNumber());

			GregorianCalendar departureDateTimeCal = new GregorianCalendar();
			departureDateTimeCal.setTime(flightSegmentTO.getDepartureDateTime());
			bookFlightSegment.setDepartureDateTime(CommonUtil.getXMLGregorianCalendar(departureDateTimeCal));

			GregorianCalendar arrivalDateTimeCal = new GregorianCalendar();
			arrivalDateTimeCal.setTime(flightSegmentTO.getArrivalDateTime());
			bookFlightSegment.setArrivalDateTime(CommonUtil.getXMLGregorianCalendar(arrivalDateTimeCal));

			bookFlightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());

			String[] segmentCodeList = flightSegmentTO.getSegmentCode().split("/");

			FlightSegmentBaseType.DepartureAirport departureAirport = new FlightSegmentBaseType.DepartureAirport();
			departureAirport.setLocationCode(segmentCodeList[0]);
			departureAirport.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(), true)[2]);

			FlightSegmentBaseType.ArrivalAirport arrivalAirport = new FlightSegmentBaseType.ArrivalAirport();
			arrivalAirport.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
			arrivalAirport.setTerminal((String) globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[2]);

			bookFlightSegment.setDepartureAirport(departureAirport);
			bookFlightSegment.setArrivalAirport(arrivalAirport);
			if (bookingClass != null) {
				bookFlightSegment.setResBookDesigCode(bookingClass);
			}
			if (segmentCodeList.length > 2) {
				bookFlightSegment.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
			}
		}
	}

	/**
	 * 
	 * @param pricedItinerary
	 * @param priceInfoTO
	 * @param flightAvailRQ
	 * @param isFixedFare
	 * @param flexiSelectionCriteria
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private static void setItineraryPricingInfo(PricedItineraryType pricedItinerary, BaseAvailRS baseAvailRS,
			BaseAvailRQ baseAvailRQ, boolean isFixedFare, FlexiFareSelectionCriteria flexiSelectionCriteria,
			QuotedPriceInfoTO quotedPriceInfoTO, ExchangeRateProxy exchangeRateProxy) throws WebservicesException,
			ModuleException {

		// fares and tax for single adult pax is set
		AirItineraryPricingInfoType airItineraryPricingInfo = new AirItineraryPricingInfoType();
		pricedItinerary.setAirItineraryPricingInfo(airItineraryPricingInfo);
		airItineraryPricingInfo.setPricingSource(PricingSourceType.PUBLISHED);

		// Calculating the Handling fare for the agent
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, ChargeRateOperationType.MAKE_ONLY);

		OTAPaxCountTO paxCountTO = new OTAPaxCountTO(baseAvailRQ.getTravelerInfoSummary());

		// calculate the handling fee
		if (externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE) != null) {
			calculateHandlingFee(baseAvailRS.getSelectedPriceFlightInfo(), paxCountTO,
					externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE), isFixedFare);
		}

		// set Passenger Type, fare and taxes/surcharges breakdown
		setPassengerFareBreakdowns(airItineraryPricingInfo, baseAvailRS, baseAvailRQ, externalChargesMap, isFixedFare,
				flexiSelectionCriteria, paxCountTO, quotedPriceInfoTO, exchangeRateProxy);
	}

	/**
	 * 
	 * @param priceInfoTO
	 * @param otaPaxCountTO
	 * @param externalChgDTO
	 * @param isFixedFare
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public static void calculateHandlingFee(PriceInfoTO priceInfoTO, OTAPaxCountTO otaPaxCountTO, ExternalChgDTO externalChgDTO,
			boolean isFixedFare) throws WebservicesException, ModuleException {

		PaxSet paxSet = new PaxSet(otaPaxCountTO.getAdultCount(), otaPaxCountTO.getChildCount(), otaPaxCountTO.getInfantCount());

		ExternalChargeUtil.calculateExternalChargeAmount(paxSet, priceInfoTO, externalChgDTO, priceInfoTO.hasReturnFlights());

	}

	/**
	 * 
	 * @param priceInfoTO
	 * @param flightAvailRQ
	 * @param externalChargesMap
	 * @param isFixedFare
	 * @param flexiSelectionCriteria
	 * @param otaPaxCountTO
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private static void setPassengerFareBreakdowns(AirItineraryPricingInfoType airItineraryPricingInfo, BaseAvailRS baseAvailRS,
			BaseAvailRQ baseAvailRQ, Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap, boolean isFixedFare,
			FlexiFareSelectionCriteria flexiSelectionCriteria, OTAPaxCountTO otaPaxCountTO, QuotedPriceInfoTO quotedPriceInfoTO,
			ExchangeRateProxy exchangeRateProxy) throws WebservicesException, ModuleException {

		PriceInfoTO priceInfoTO = baseAvailRS.getSelectedPriceFlightInfo();

		AirItineraryPricingInfoType.PTCFareBreakdowns ptcFareBreakdowns = new AirItineraryPricingInfoType.PTCFareBreakdowns();
		// airItineraryPricingInfo.setPTCFareBreakdowns(ptcFareBreakdowns);
		BigDecimal totalInSelectedCurByFareBreakdown = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalSelecteExternalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalSelecteExternalChargesInSelCur = AccelAeroCalculator.getDefaultBigDecimalZero();

		Collection<String> paxTypes = new ArrayList<String>();
		if (otaPaxCountTO.getAdultCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		}
		if (otaPaxCountTO.getChildCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		}
		if (otaPaxCountTO.getInfantCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
		}

		// calculating the per pax handling charge
		int handlingFeePaxCount = otaPaxCountTO.getAdultCount() + otaPaxCountTO.getChildCount();
		ExternalChgDTO externalChgDTO = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE);

		BigDecimal handlingFee = externalChgDTO.getAmount();
		handlingFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handlingFee);

		BigDecimal[] perPaxHandlingCharges = AccelAeroCalculator.roundAndSplit(handlingFee, handlingFeePaxCount);

		int paxSeq = 0;
		int parentSeq = 0;
		int count = 0; // to get the per pax handling fare from the array.
		int ccCount = 0; // to get the per pax cc charge from the array.

		boolean isHoldAllowed = true;
		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		
		if (!AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_ONHOLD)) {
			isHoldAllowed = WSReservationUtil.isHoldAllowed(priceInfoTO);
			quotedPriceInfoTO.setOnholdAllowed(isHoldAllowed);
		}

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();
		String currencyCode = null;
		if (showPriceInselectedCurrency) {
			AvailPreferencesTO availPreferencesTO = baseAvailRQ.getAvailPreferences();
			if (availPreferencesTO.getPreferredCurrency() != null
					&& OTAUtils.isPreferredCurrencyCodeSupportedByPC(availPreferencesTO.getPreferredCurrency())) {
				currencyCode = availPreferencesTO.getPreferredCurrency();
			} else {
				currencyCode = principal.getAgentCurrencyCode();
			}
		}

		// Creates the flexi fare related objects. We can have two cases to display flexi
		// 1. User requests to display the flexi in the availability search
		// 2. The request contains to include flexibilities in the price quote.
		// in both these cases we need to display the available flexi flares.

		FareTypeTO fareTypeTO = priceInfoTO.getFareTypeTO();

		if (fareTypeTO.hasOndExternalCharge(OndSequence.OUT_BOUND) || fareTypeTO.hasOndExternalCharge(OndSequence.IN_BOUND)
				|| WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
			WSFlexiFareUtil.prepareFlexiFareDisplayObjects(airItineraryPricingInfo, priceInfoTO, otaPaxCountTO, currencyCode,
					exchangeRateProxy);
		}

		Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<? extends LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		for (String paxType : paxTypes) {
			quotedPriceInfoTO.getPerPaxCharges().put(paxType, new PerPaxChargesTO(paxType));

			List<PTCFareBreakdownType> fareBreakdownTypesWithExternalCharges = new ArrayList<PTCFareBreakdownType>();

			int paxQuantity;
			BigDecimal fareAmount;
			BigDecimal totalCharges;
			String chargeQuotePaxType;
			BigDecimal totalInSelectedCurPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal taxAmounts;
			BigDecimal totalChargesInSelectedCur = AccelAeroCalculator.getDefaultBigDecimalZero();

			Collection<String> surchargeChargeGroupCodes = new ArrayList<String>();
			surchargeChargeGroupCodes.add(ChargeGroups.SURCHARGE);

			if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
				paxQuantity = otaPaxCountTO.getAdultCount();
				fareAmount = priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.ADULT);
				totalCharges = priceInfoTO.getTotalChargesbyPaxType(priceInfoTO, PaxTypeTO.ADULT);

				taxAmounts = priceInfoTO.getTotalTaxbyPaxType(priceInfoTO, PaxTypeTO.ADULT);

				chargeQuotePaxType = PaxTypeTO.ADULT;

			} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
				paxQuantity = otaPaxCountTO.getChildCount();
				fareAmount = priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.CHILD);
				totalCharges = priceInfoTO.getTotalChargesbyPaxType(priceInfoTO, PaxTypeTO.CHILD);
				taxAmounts = priceInfoTO.getTotalTaxbyPaxType(priceInfoTO, PaxTypeTO.CHILD);
				chargeQuotePaxType = PaxTypeTO.CHILD;

			} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
				paxQuantity = otaPaxCountTO.getInfantCount();
				fareAmount = priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.INFANT);
				totalCharges = priceInfoTO.getTotalChargesbyPaxType(priceInfoTO, PaxTypeTO.INFANT);
				surchargeChargeGroupCodes.add(ChargeGroups.INFANT_SURCHARGE);
				taxAmounts = priceInfoTO.getTotalTaxbyPaxType(priceInfoTO, PaxTypeTO.INFANT);
				chargeQuotePaxType = PaxTypeTO.INFANT;
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
			}

			quotedPriceInfoTO.getPerPaxCharges().get(paxType).setTotalBaseFare(fareAmount);
			quotedPriceInfoTO.getPerPaxCharges().get(paxType).setTotalCharges(totalCharges);
			quotedPriceInfoTO.getPerPaxCharges().get(paxType).setTotalTax(taxAmounts);

			PTCFareBreakdownType ptcFareBreakdown = new PTCFareBreakdownType();
			// airItineraryPricingInfo.getPTCFareBreakdowns().getPTCFareBreakdown().add(ptcFareBreakdown);
			// ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);

			ptcFareBreakdown.setPricingSource(PricingSourceType.PUBLISHED);

			// set pax quantity
			PassengerTypeQuantityType passengerTypeQuantity = new PassengerTypeQuantityType();
			ptcFareBreakdown.setPassengerTypeQuantity(passengerTypeQuantity);
			passengerTypeQuantity.setCode(paxType);
			passengerTypeQuantity.setQuantity(paxQuantity);

			// set fare basis code
			PTCFareBreakdownType.FareBasisCodes fareBasisCodes = new PTCFareBreakdownType.FareBasisCodes();
			ptcFareBreakdown.setFareBasisCodes(fareBasisCodes);
			// FIXME fare basis code to be returned in the price quote
			String dummyFareBasisCode = "P";
			fareBasisCodes.getFareBasisCode().add(dummyFareBasisCode);

			if (!isHoldAllowed) {
				String noHoldAllowedFareBasisCode = "NH"; // FIXME - Remove hard coding
				fareBasisCodes.getFareBasisCode().add(noHoldAllowedFareBasisCode);
			}

			FareType passengerFareInfo = new FareType();
			ptcFareBreakdown.setPassengerFare(passengerFareInfo);

			if (AppSysParamsUtil.isFareRuleDetailsEnabled()) {

				List<FareRuleDTO> fareRules = fareTypeTO.getApplicableFareRules();
				List<PTCFareBreakdownType.FareInfo> fareInfos = new ArrayList<PTCFareBreakdownType.FareInfo>();
				boolean departureAirportCurrency = false;
				for (FareRuleDTO fareRule : fareRules) {
					PTCFareBreakdownType.FareInfo fareInfo = new PTCFareBreakdownType.FareInfo();
					String fareBasisCode = fareRule.getFareBasisCode();

					if (fareRule.getDepAPFareDTO() != null) {
						departureAirportCurrency = true;
					} else {
						departureAirportCurrency = false;
					}

					if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
						if (departureAirportCurrency) {
							fareInfo.setAdultFare(new BigDecimal(fareRule.getDepAPFareDTO().getAdultFare()));
						} else {
							fareInfo.setAdultFare(fareRule.getAdultFareAmount());
						}
					} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
						if (departureAirportCurrency) {
							fareInfo.setChildFare(new BigDecimal(fareRule.getDepAPFareDTO().getChildFare()));
						} else {
							fareInfo.setChildFare(fareRule.getChildFareAmount());
						}
					} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
						if (departureAirportCurrency) {
							fareInfo.setInfantFare(new BigDecimal(fareRule.getDepAPFareDTO().getInfantFare()));
						} else {
							fareInfo.setInfantFare(fareRule.getInfantFareAmount());
						}
					}

					fareInfo.setCurrencyCode(fareRule.getCurrencyCode());
					fareInfo.setSegmentCode(fareRule.getOrignNDest());
					fareInfo.setFareRuleInfo(fareRule.getFareRuleCode());
					fareInfo.setResBookDesigCode(fareRule.getBookingClassCode());
					fareInfo.setFareBasisCode(fareBasisCode);
					fareInfo.setFareRuleReference(fareRule.getComments());
					fareBasisCodes.getFareBasisCode().add(fareBasisCode);
					fareInfos.add(fareInfo);
				}
				ptcFareBreakdown.getFareInfo().addAll(fareInfos);
				ptcFareBreakdown.setFareBasisCodes(fareBasisCodes);

			}

			// base fare for single pax
			FareType.BaseFare baseFare = new FareType.BaseFare();
			passengerFareInfo.setBaseFare(baseFare);
			if (showPriceInselectedCurrency && currencyCode != null) {
				// fareAmount in preferred currency
				BigDecimal fareAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(fareAmount,
						currencyCode, exchangeRateProxy);
				totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, fareAmountInPreferredCurrency);
				OTAUtils.setAmountAndCurrency(baseFare, fareAmountInPreferredCurrency, currencyCode);
			} else {
				OTAUtils.setAmountAndDefaultCurrency(baseFare, fareAmount);
			}

			// taxes for single pax
			FareType.Taxes ptcTaxes = new FareType.Taxes();
			passengerFareInfo.setTaxes(ptcTaxes);

			Collection<String> chargeQuotePaxTypes = new ArrayList<String>();
			chargeQuotePaxTypes.add(chargeQuotePaxType);

			Collection<TaxTO> aaTaxes = getTaxListByPaxType(priceInfoTO, chargeQuotePaxType);

			for (TaxTO tax : aaTaxes) {
				AirTaxType taxType = new AirTaxType();
				ptcTaxes.getTax().add(taxType);
				taxType.setTaxCode(tax.getTaxCode());
				taxType.setTaxName(tax.getTaxName());
				BigDecimal taxAmount = AccelAeroCalculator.parseBigDecimal(tax.getAmount().doubleValue());

				if (showPriceInselectedCurrency && currencyCode != null) {
					// taxType in preferred currency
					BigDecimal taxAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(taxAmount,
							currencyCode, exchangeRateProxy);
					totalChargesInSelectedCur = AccelAeroCalculator.add(taxAmountInPreferredCurrency, totalChargesInSelectedCur);
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, taxAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(taxType, taxAmountInPreferredCurrency, currencyCode);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(taxType, taxAmount);
				}
			}

			// surcharges(fees) for single pax
			FareType.Fees fees = new FareType.Fees();
			passengerFareInfo.setFees(fees);

			Collection<SurchargeTO> aaSurcharges = getSurchargeListByPaxType(priceInfoTO, chargeQuotePaxType);

			for (SurchargeTO surcharge : aaSurcharges) {
				AirFeeType feeType = new AirFeeType();
				fees.getFee().add(feeType);
				BigDecimal feeAmount = AccelAeroCalculator.parseBigDecimal(surcharge.getAmount().doubleValue());
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(feeAmount,
							currencyCode, exchangeRateProxy);
					totalChargesInSelectedCur = AccelAeroCalculator.add(feeAmountInPreferredCurrency, totalChargesInSelectedCur);
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(feeType, feeAmountInPreferredCurrency, currencyCode);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(feeType, feeAmount);
				}
				feeType.setFeeCode(surcharge.getSurchargeCode() + "/" + surcharge.getSurchargeName());
			}

			// add the handling fee only if the pax type is not a infant
			BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (!paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) { // ADULT
																												// or
																												// CHILD
				if (AccelAeroCalculator.isGreaterThan(perPaxHandlingCharges[count],
						(AccelAeroCalculator.getDefaultBigDecimalZero()))) {
					AirFeeType feeType = new AirFeeType();
					if (showPriceInselectedCurrency && currencyCode != null) {
						// feeAmount in preferred currency
						BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
								perPaxHandlingCharges[count], currencyCode, exchangeRateProxy);
						totalChargesInSelectedCur = AccelAeroCalculator.add(feeAmountInPreferredCurrency,
								totalChargesInSelectedCur);
						totalInSelectedCurPerPax = AccelAeroCalculator
								.add(totalInSelectedCurPerPax, feeAmountInPreferredCurrency);
						OTAUtils.setAmountAndCurrency(feeType, feeAmountInPreferredCurrency, currencyCode);
					} else {
						OTAUtils.setAmountAndDefaultCurrency(feeType, perPaxHandlingCharges[count]);
					}
					feeType.setFeeCode(externalChgDTO.getChargeCode() + "/" + externalChgDTO.getChargeDescription());
					fees.getFee().add(feeType);
				}
				// note: adult does not carry the infant charges
				totalPrice = AccelAeroCalculator.add(fareAmount, totalCharges, perPaxHandlingCharges[count]);
				count += paxQuantity;
			} else { // INFANT
				totalPrice = AccelAeroCalculator.add(fareAmount, totalCharges);
			}
			quotedPriceInfoTO.getPerPaxCharges().get(paxType).setTotalChargesInSelectedCur(totalChargesInSelectedCur);

			// ------------Start add flexi-charges------------------
			if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
				AirFeeType flexiFee = WSFlexiFareUtil.getPerPaxAirFeeForFlexiCharges(priceInfoTO, paxType, currencyCode,
						flexiSelectionCriteria, isFixedFare, otaPaxCountTO, exchangeRateProxy);
				fees.getFee().add(flexiFee);

				BigDecimal flexiAmountPerPaxType = flexiFee.getAmount();

				if (showPriceInselectedCurrency && currencyCode != null) {
					// No need to convert as the converstion has already happened at the object creating time.
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, flexiAmountPerPaxType);
				} else {
					totalPrice = AccelAeroCalculator.add(totalPrice, flexiAmountPerPaxType);
				}
			}

			// TODO : SR
			// ------------ end add flexi charges------------------

			FareType.TotalFare totalFare = new FareType.TotalFare();
			passengerFareInfo.setTotalFare(totalFare);
			if (showPriceInselectedCurrency && currencyCode != null) {
				BigDecimal totalPriceInPreferredCurrency = totalInSelectedCurPerPax;
				OTAUtils.setAmountAndCurrency(totalFare, totalPriceInPreferredCurrency, currencyCode);
			} else {
				OTAUtils.setAmountAndDefaultCurrency(totalFare, totalPrice);
			}

			// set traveller reference numbers and introduce new breakdowns if any special services added
			for (int i = 0; i < paxQuantity; i++) {

				PTCFareBreakdownType.TravelerRefNumber travelerRefNumber = new PTCFareBreakdownType.TravelerRefNumber();
				if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					travelerRefNumber.setRPH("A" + (++paxSeq));
				} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					travelerRefNumber.setRPH("C" + (++paxSeq));
				} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					travelerRefNumber.setRPH("I" + (++paxSeq) + "/A" + (++parentSeq));
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
				}

				if (quotedExternalCharges != null && quotedExternalCharges.keySet().contains(travelerRefNumber.getRPH())) {
					// this is calling to split the breakdowns if special services selected
					BigDecimal[] returnVals = splitFareBreakdownsWithSelectedServices(quotedExternalCharges, travelerRefNumber,
							totalSelecteExternalCharges, showPriceInselectedCurrency, currencyCode,
							totalSelecteExternalChargesInSelCur, fareBreakdownTypesWithExternalCharges, ptcFareBreakdown,
							exchangeRateProxy);
					totalSelecteExternalCharges = returnVals[0];
					totalSelecteExternalChargesInSelCur = returnVals[1];
				} else {
					ptcFareBreakdown.getTravelerRefNumber().add(travelerRefNumber);
					ptcFareBreakdown.getPassengerTypeQuantity().setQuantity(ptcFareBreakdown.getTravelerRefNumber().size());
				}
			}

			if (paxQuantity == fareBreakdownTypesWithExternalCharges.size()) {
				if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
						&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, principal.getAgentCode())) {

					for (PTCFareBreakdownType ptcBT : fareBreakdownTypesWithExternalCharges) {
						BigDecimal totalPriceForCalculateCCCharge = ptcBT.getPassengerFare().getTotalFare().getAmount();
						setCcFeeInPTCFareBreakdown(ptcBT.getPassengerFare().getTotalFare().getAmount(), priceInfoTO
								.getFareSegChargeTO().getOndFareSegChargeTOs().size(), showPriceInselectedCurrency, currencyCode,
								exchangeRateProxy, totalInSelectedCurPerPax, ptcBT, quotedPriceInfoTO.getQuotedSegments());
					}

				}
				ptcFareBreakdowns.getPTCFareBreakdown().addAll(fareBreakdownTypesWithExternalCharges);
			} else {
				if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
						&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, principal.getAgentCode())) {

					for (PTCFareBreakdownType ptcBT : fareBreakdownTypesWithExternalCharges) {
						setCcFeeInPTCFareBreakdown(ptcBT.getPassengerFare().getTotalFare().getAmount(), priceInfoTO
								.getFareSegChargeTO().getOndFareSegChargeTOs().size(), showPriceInselectedCurrency, currencyCode,
								exchangeRateProxy, totalInSelectedCurPerPax, ptcBT, quotedPriceInfoTO.getQuotedSegments());
					}

					setCcFeeInPTCFareBreakdown(ptcFareBreakdown.getPassengerFare().getTotalFare().getAmount(), priceInfoTO
							.getFareSegChargeTO().getOndFareSegChargeTOs().size(), showPriceInselectedCurrency, currencyCode,
							exchangeRateProxy, totalInSelectedCurPerPax, ptcFareBreakdown, quotedPriceInfoTO.getQuotedSegments());
				}
				ptcFareBreakdowns.getPTCFareBreakdown().addAll(fareBreakdownTypesWithExternalCharges);
				ptcFareBreakdown.getPassengerTypeQuantity().setQuantity(ptcFareBreakdown.getTravelerRefNumber().size());
				ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);
			}

			totalInSelectedCurByFareBreakdown = AccelAeroCalculator.add(totalInSelectedCurByFareBreakdown,
					AccelAeroCalculator.multiply(totalInSelectedCurPerPax, paxQuantity));
		}

		if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
			WSFlexiFareUtil.addFlexiChargesToExternalChargesMap(baseAvailRS, baseAvailRQ, flexiSelectionCriteria, paxTypes);
		}

		totalInSelectedCurByFareBreakdown = AccelAeroCalculator.add(totalInSelectedCurByFareBreakdown,
				totalSelecteExternalChargesInSelCur);
		// set total fare and total taxes/surcharges for itinerary
		setTotalFaresForItinerary(airItineraryPricingInfo, priceInfoTO, baseAvailRQ, externalChargesMap, isFixedFare,
				ptcFareBreakdowns, totalInSelectedCurByFareBreakdown, flexiSelectionCriteria, totalSelecteExternalCharges,
				otaPaxCountTO, quotedPriceInfoTO, exchangeRateProxy);
	}

	/**
	 * Get the uniformed tax collection for the given tax type
	 * 
	 * @param priceInfoTO
	 * @param paxType
	 * @return
	 */
	private static Collection<TaxTO> getTaxListByPaxType(PriceInfoTO priceInfoTO, String paxType) {
		Collection<TaxTO> collection = new ArrayList<TaxTO>();
		PerPaxPriceInfoTO paxPriceInfoTO = priceInfoTO.getPerPaxPriceInfoTO(paxType);

		Map<String, TaxTO> taxMap = new HashMap<String, TaxTO>();

		if (paxPriceInfoTO != null && paxPriceInfoTO.getPassengerPrice() != null
				&& paxPriceInfoTO.getPassengerPrice().getTaxes() != null) {
			TaxTO newTaxTO = null;
			for (TaxTO taxTO : paxPriceInfoTO.getPassengerPrice().getTaxes()) {
				if (taxTO.getApplicablePassengerTypeCode().contains(paxType)) {
					if (!taxMap.containsKey(taxTO.getTaxCode())) {
						taxMap.put(taxTO.getTaxCode(), taxTO);
					} else {
						newTaxTO = taxMap.get(taxTO.getTaxCode());
						newTaxTO.setAmount(AccelAeroCalculator.add(newTaxTO.getAmount(), taxTO.getAmount()));
						taxMap.put(newTaxTO.getTaxCode(), newTaxTO);
					}
				}
			}

			if (!taxMap.isEmpty()) {
				collection.addAll(taxMap.values());
			}

		}

		return collection;
	}

	/**
	 * Get the uniform surcharge collection for the given pax type
	 * 
	 * @param priceInfoTO
	 * @param paxType
	 * @return
	 */
	private static Collection<SurchargeTO> getSurchargeListByPaxType(PriceInfoTO priceInfoTO, String paxType) {
		Collection<SurchargeTO> collection = new ArrayList<SurchargeTO>();
		PerPaxPriceInfoTO paxPriceInfoTO = priceInfoTO.getPerPaxPriceInfoTO(paxType);

		Map<String, SurchargeTO> surchageCodeMap = new HashMap<String, SurchargeTO>();

		if (paxPriceInfoTO != null && paxPriceInfoTO.getPassengerPrice() != null && paxPriceInfoTO != null
				&& paxPriceInfoTO.getPassengerPrice().getSurcharges() != null) {
			SurchargeTO newSurchargeTo = null;
			for (SurchargeTO surchargeTO : paxPriceInfoTO.getPassengerPrice().getSurcharges()) {
				if (surchargeTO.getApplicablePassengerTypeCode().contains(paxType)) {
					if (!surchageCodeMap.containsKey(surchargeTO.getSurchargeCode())) {
						surchageCodeMap.put(surchargeTO.getSurchargeCode(), surchargeTO);
					} else {
						newSurchargeTo = surchageCodeMap.get(surchargeTO.getSurchargeCode());
						newSurchargeTo.setAmount(AccelAeroCalculator.add(newSurchargeTo.getAmount(), surchargeTO.getAmount()));
						surchageCodeMap.put(newSurchargeTo.getSurchargeCode(), newSurchargeTo);
					}
				}
			}

			if (!surchageCodeMap.isEmpty()) {
				collection.addAll(surchageCodeMap.values());
			}

		}

		return collection;
	}

	private static BigDecimal[] splitFareBreakdownsWithSelectedServices(
			Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges,
			PTCFareBreakdownType.TravelerRefNumber travelerRefNumber, BigDecimal totalSelecteExternalCharges,
			boolean showPriceInselectedCurrency, String currencyCode, BigDecimal totalSelecteExternalChargesInSelCur,
			List<PTCFareBreakdownType> fareBreakdownTypesWithExternalCharges, PTCFareBreakdownType ptcFareBreakdown,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		boolean isSpecialServicesSelected = false;
		boolean isSeatSelected, isMealSelected, isBaggageSelected, isHalaSelected, isInflightServiceSelected, IsInsSelected, isAutomaticCheckinSelected;
		isSeatSelected = isMealSelected = isBaggageSelected = isHalaSelected = isInflightServiceSelected = IsInsSelected = isAutomaticCheckinSelected = false;

		String serviceTaxCode = null;
		String serviceTaxDescription = null;
		List<? extends LCCClientExternalChgDTO> externalCharges = quotedExternalCharges.get(travelerRefNumber.getRPH());

		// getting seat charges total
		BigDecimal seatChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal mealChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal halaChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal inflightChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal insuranceChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal baggageChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal serviceTaxTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal automaticCheckinChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		PTCFareBreakdownType newPtcFareBreakdownType = OTAUtils.clonePtcFareBreakdownType(ptcFareBreakdown);
		BigDecimal totalExternalChargesPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (LCCClientExternalChgDTO externalChargeDTO : externalCharges) {
			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.SEAT_MAP)) {
				seatChargesTotal = AccelAeroCalculator.add(seatChargesTotal, externalChargeDTO.getAmount());
				isSpecialServicesSelected = true;
				isSeatSelected = true;
			}
			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.MEAL)) {
				mealChargesTotal = AccelAeroCalculator.add(mealChargesTotal, externalChargeDTO.getAmount());
				isSpecialServicesSelected = true;
				isMealSelected = true;
			}

			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.AIRPORT_SERVICE)) {
				halaChargesTotal = AccelAeroCalculator.add(halaChargesTotal, externalChargeDTO.getAmount());
				isSpecialServicesSelected = true;
				isHalaSelected = true;
			}

			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.INFLIGHT_SERVICES)) {
				inflightChargesTotal = AccelAeroCalculator.add(inflightChargesTotal, externalChargeDTO.getAmount());
				isSpecialServicesSelected = true;
				isInflightServiceSelected = true;
			}

			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.INSURANCE)) {
				insuranceChargesTotal = AccelAeroCalculator.add(insuranceChargesTotal, externalChargeDTO.getAmount());
				isSpecialServicesSelected = true;
				IsInsSelected = true;
			}

			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.BAGGAGE)) {
				baggageChargesTotal = AccelAeroCalculator.add(baggageChargesTotal, externalChargeDTO.getAmount());
				isSpecialServicesSelected = true;
				isBaggageSelected = true;
			}

			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.JN_ANCI)) {
				isSpecialServicesSelected = true;
				serviceTaxTotal = AccelAeroCalculator.add(serviceTaxTotal, externalChargeDTO.getAmount());
				serviceTaxCode = externalChargeDTO.getCode();
				serviceTaxDescription = externalChargeDTO.getUserNote();
			}

			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.SERVICE_TAX)) {
				isSpecialServicesSelected = true;
				BigDecimal serviceTaxTotalNew = externalChargeDTO.getAmount();
				String taxCode = externalChargeDTO.getCode();

				if (!serviceTaxTotalNew.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
					AirTaxType serviceTax = new AirTaxType();
					totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, serviceTaxTotalNew);

					if (showPriceInselectedCurrency && currencyCode != null) {
						// feeAmount in preferred currency
						BigDecimal taxAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
								serviceTaxTotalNew, currencyCode, exchangeRateProxy);
						totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
								taxAmountInPreferredCurrency);
						OTAUtils.setAmountAndCurrency(serviceTax, taxAmountInPreferredCurrency, currencyCode);
						totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax,
								taxAmountInPreferredCurrency);
					} else {
						OTAUtils.setAmountAndDefaultCurrency(serviceTax, serviceTaxTotalNew);
						totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, serviceTaxTotalNew);
					}
					serviceTax.setTaxCode(taxCode);
					serviceTax.setTaxName(taxCode + "/Service Tax");
					newPtcFareBreakdownType.getPassengerFare().getTaxes().getTax().add(serviceTax);
				}

			}
			
			if (externalChargeDTO.getExternalCharges().equals(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN)) {
				automaticCheckinChargesTotal = AccelAeroCalculator.add(automaticCheckinChargesTotal,
						externalChargeDTO.getAmount());
				isSpecialServicesSelected = true;
				isAutomaticCheckinSelected = true;
			}

		}

		if (isSpecialServicesSelected) {
			// PTCFareBreakdownType newPtcFareBreakdownType = OTAUtils.clonePtcFareBreakdownType(ptcFareBreakdown);

			// BigDecimal totalExternalChargesPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
			// for the seat selection
			if (isSeatSelected) {
				AirFeeType seatFee = new AirFeeType();
				// fees.getFee().add(seatFee);
				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, seatChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(seatChargesTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(seatFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(seatFee, seatChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, seatChargesTotal);
				}
				seatFee.setFeeCode(ReservationInternalConstants.CHAEGE_CODES.SM + "/Seat Selection");
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(seatFee);
			}
			// This is for the meal selection
			if (isMealSelected) {
				AirFeeType mealFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, mealChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(mealChargesTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(mealFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(mealFee, mealChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, mealChargesTotal);
				}
				mealFee.setFeeCode(ReservationInternalConstants.CHAEGE_CODES.ML + "/Meal Selection");
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(mealFee);
			}
			// for hala services
			if (isHalaSelected) {
				AirFeeType halaFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, halaChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(halaChargesTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(halaFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(halaFee, halaChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, halaChargesTotal);
				}
				halaFee.setFeeCode(ReservationInternalConstants.CHAEGE_CODES.HALA + "/Hala Service");
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(halaFee);
			}

			// For In-flight Services
			if (isInflightServiceSelected) {
				AirFeeType inflightFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, inflightChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// Fee amount in preferred language
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
							inflightChargesTotal, currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(inflightFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(inflightFee, inflightChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, inflightChargesTotal);
				}
				inflightFee.setFeeCode(ReservationInternalConstants.CHAEGE_CODES.SSR + "/Inflight Services");
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(inflightFee);
			}

			// for the insurance selection
			if (IsInsSelected) {
				AirFeeType insuranceFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, insuranceChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
							insuranceChargesTotal, currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(insuranceFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(insuranceFee, insuranceChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, insuranceChargesTotal);
				}
				insuranceFee.setFeeCode(ReservationInternalConstants.CHAEGE_CODES.IN + "/Insurance Selection");
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(insuranceFee);
			}
			// This is for the baggage selection
			if (isBaggageSelected) {
				AirFeeType baggageFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, baggageChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(baggageChargesTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(baggageFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(baggageFee, baggageChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, baggageChargesTotal);
				}
				baggageFee.setFeeCode(ReservationInternalConstants.CHAEGE_CODES.BG + "/Baggage Selection");
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(baggageFee);
			}
			
			if (isAutomaticCheckinSelected) {
				AirFeeType automaticCheckinFee = new AirFeeType();
				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, automaticCheckinChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
							automaticCheckinChargesTotal, currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(automaticCheckinFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(automaticCheckinFee, automaticCheckinChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, automaticCheckinChargesTotal);
				}
				automaticCheckinFee.setFeeCode(ReservationInternalConstants.CHAEGE_CODES.AUTOCHKR + "/AutomaticCheckin Selection");
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(automaticCheckinFee);
			}

			if (!serviceTaxTotal.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
				AirTaxType serviceTax = new AirTaxType();
				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, serviceTaxTotal);

				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal taxAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(serviceTaxTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							taxAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(serviceTax, taxAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, taxAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(serviceTax, serviceTaxTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, serviceTaxTotal);
				}
				serviceTax.setTaxCode(serviceTaxCode);
				serviceTax.setTaxName(AirinventoryCustomConstants.ChargeCodes.JN_ANCI_TAX + "/" + serviceTaxDescription);
				newPtcFareBreakdownType.getPassengerFare().getTaxes().getTax().add(serviceTax);
			}

			newPtcFareBreakdownType
					.getPassengerFare()
					.getTotalFare()
					.setAmount(
							AccelAeroCalculator.add(newPtcFareBreakdownType.getPassengerFare().getTotalFare().getAmount(),
									totalExternalChargesPerPax));

			newPtcFareBreakdownType.getTravelerRefNumber().add(travelerRefNumber);
			newPtcFareBreakdownType.getPassengerTypeQuantity().setQuantity(newPtcFareBreakdownType.getTravelerRefNumber().size());

			fareBreakdownTypesWithExternalCharges.add(newPtcFareBreakdownType);

		} else {
			ptcFareBreakdown.getTravelerRefNumber().add(travelerRefNumber);
			ptcFareBreakdown.getPassengerTypeQuantity().setQuantity(ptcFareBreakdown.getTravelerRefNumber().size());
		}

		// BigDecimal[0]- total selected external charges,
		// BigDecimal[1]- total selected external charges in selected currency
		return new BigDecimal[] { totalSelecteExternalCharges, totalSelecteExternalChargesInSelCur };
	}

	/**
	 * 
	 * @param airItineraryPricingInfo
	 * @param priceInfoTO
	 * @param flightAvailRQ
	 * @param externalChargesMap
	 * @param isFixedFare
	 * @param ptcFareBreakdowns
	 * @param totalInSelectedCurByFareBreakdown
	 * @param flexiSelectionCriteria
	 * @param totalSelectedExternalCharges
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static void setTotalFaresForItinerary(AirItineraryPricingInfoType airItineraryPricingInfo, PriceInfoTO priceInfoTO,
			BaseAvailRQ baseAvailRQ, Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap, boolean isFixedFare,
			AirItineraryPricingInfoType.PTCFareBreakdowns ptcFareBreakdowns, BigDecimal totalInSelectedCurByFareBreakdown,
			FlexiFareSelectionCriteria flexiSelectionCriteria, BigDecimal totalSelectedExternalCharges,
			OTAPaxCountTO otaPaxCountTO, QuotedPriceInfoTO quotedPriceInfoTO, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException, WebservicesException {
		FareType itinearyFareInfo = new FareType();
		airItineraryPricingInfo.setItinTotalFare(itinearyFareInfo);

		// CC charge as a fixed value -		
		int totalPayablePaxCountForCreditCardFee = otaPaxCountTO.getAdultCount() + otaPaxCountTO.getChildCount();
		int totalPayablePaxCount = otaPaxCountTO.getAdultCount() + otaPaxCountTO.getChildCount() + otaPaxCountTO.getInfantCount();
		int totalSegmentCount = priceInfoTO.getFareSegChargeTO().getOndFareSegChargeTOs().size();

		// set total base fare
		FareType.BaseFare baseFare = new FareType.BaseFare();
		BigDecimal totalBaseFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		itinearyFareInfo.setBaseFare(baseFare);

		FareType.TotalFare totalFare = new FareType.TotalFare();
		itinearyFareInfo.setTotalFare(totalFare);
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();
		String currencyCode = null;
		String equiCurrencyCode = null;
		BigDecimal totalWithCCFeeInBaseCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (showPriceInselectedCurrency) {

			BigDecimal totalBaseFareInBaseCur = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (baseAvailRQ.getAvailPreferences() != null && baseAvailRQ.getAvailPreferences().getPreferredCurrency() != null
					&& OTAUtils.isPreferredCurrencyCodeSupportedByPC(baseAvailRQ.getAvailPreferences().getPreferredCurrency())) {
				currencyCode = baseAvailRQ.getAvailPreferences().getPreferredCurrency();
				equiCurrencyCode = principal.getAgentCurrencyCode();
			} else {
				currencyCode = principal.getAgentCurrencyCode();
			}

			// Adult Fare
			if (ReservationApiUtils.isFareExist(priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.ADULT))) {
				BigDecimal paxTypeBaseFareInBaseCur = AccelAeroCalculator.multiply(
						priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.ADULT),
						new BigDecimal(otaPaxCountTO.getAdultCount()));
				totalBaseFareInBaseCur = AccelAeroCalculator.add(totalBaseFareInBaseCur, paxTypeBaseFareInBaseCur);

				totalBaseFare = AccelAeroCalculator
						.add(totalBaseFare, WSReservationUtil.getAmountInSpecifiedCurrency(paxTypeBaseFareInBaseCur,
								currencyCode, exchangeRateProxy));

			}

			// Child Fare
			if (ReservationApiUtils.isFareExist(priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.CHILD))) {

				BigDecimal paxTypeBaseFareInBaseCur = AccelAeroCalculator.multiply(
						priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.CHILD),
						new BigDecimal(otaPaxCountTO.getChildCount()));
				totalBaseFareInBaseCur = AccelAeroCalculator.add(totalBaseFareInBaseCur, paxTypeBaseFareInBaseCur);

				totalBaseFare = AccelAeroCalculator
						.add(totalBaseFare, WSReservationUtil.getAmountInSpecifiedCurrency(paxTypeBaseFareInBaseCur,
								currencyCode, exchangeRateProxy));
			}

			// Infant fare
			if (ReservationApiUtils.isFareExist(priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.INFANT))) {

				BigDecimal paxTypeBaseFareInBaseCur = AccelAeroCalculator.multiply(
						priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.INFANT),
						new BigDecimal(otaPaxCountTO.getInfantCount()));
				totalBaseFareInBaseCur = AccelAeroCalculator.add(totalBaseFareInBaseCur, paxTypeBaseFareInBaseCur);

				totalBaseFare = AccelAeroCalculator
						.add(totalBaseFare, WSReservationUtil.getAmountInSpecifiedCurrency(paxTypeBaseFareInBaseCur,
								currencyCode, exchangeRateProxy));
			}

			// Set total BaseFare
			OTAUtils.setAmountAndCurrency(baseFare, totalBaseFare, currencyCode);
			quotedPriceInfoTO.setTotalBaseFare(totalBaseFareInBaseCur);
			BigDecimal[] totals = getQuotedTotalPrices(priceInfoTO, otaPaxCountTO, quotedPriceInfoTO);

			// Adding Handling fare to the total price. For now only external charge is the handling fee
			BigDecimal handleCharge = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE).getAmount();
			handleCharge = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handleCharge);

			BigDecimal totalPrice = AccelAeroCalculator.add(totals[0], handleCharge);
			totalPrice = AccelAeroCalculator.add(totalPrice, totalSelectedExternalCharges);

			BigDecimal handlingChargeInSelCur = WSReservationUtil.getAmountInSpecifiedCurrency(handleCharge, currencyCode,
					exchangeRateProxy);
			totalInSelectedCurByFareBreakdown = AccelAeroCalculator
					.add(totalInSelectedCurByFareBreakdown, handlingChargeInSelCur);

			// adding the selected flexi charges for the price quote
			if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
				BigDecimal totalFlexiChargesForPriceQuote = WSFlexiFareUtil.calculateTotalFlexiFareForPriceQuote(priceInfoTO,
						otaPaxCountTO, flexiSelectionCriteria, isFixedFare);

				totalPrice = AccelAeroCalculator.add(totalPrice, WSReservationUtil.getAmountInSpecifiedCurrency(
						totalFlexiChargesForPriceQuote, currencyCode, exchangeRateProxy));
			}

			// set total price
			OTAUtils.setAmountAndCurrency(totalFare, totalInSelectedCurByFareBreakdown, currencyCode);

			// set total Equiv price
			if (equiCurrencyCode != null) {
				BigDecimal totalPriceInEquivCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(totalPrice,
						equiCurrencyCode, exchangeRateProxy);
				if (totalPriceInEquivCurrency != null) {
					FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
					itinearyFareInfo.setTotalEquivFare(totalEquivFare);
					OTAUtils.setAmountAndCurrency(totalEquivFare, totalPriceInEquivCurrency, equiCurrencyCode);
				}
			} else {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				itinearyFareInfo.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndDefaultCurrency(totalEquivFare, totalPrice);
			}

			// Set total price with card payment transaction fee
			if (AppSysParamsUtil.applyCreditCardCharge()) {
				// Amount in base currency
				FareType.TotalFareWithCCFee totalFareWithCCFee = new FareType.TotalFareWithCCFee();
				itinearyFareInfo.setTotalFareWithCCFee(totalFareWithCCFee);

				// ExternalChgDTO externalChgDTO = ReservationUtil.getCCCharge(totalPrice);
				ExternalChgDTO externalChgDTO = WSReservationUtil.getCCChargeAmount(totalPrice, totalPayablePaxCountForCreditCardFee, totalSegmentCount,
						ChargeRateOperationType.MAKE_ONLY); // ReservationUtil.getCCCharge(totalPrice);

				BigDecimal ccFee = externalChgDTO.getAmount();
				ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

				// calculate service tax
				BigDecimal totalServiceTaxAmount = ServiceTaxUtil.calculateServiceTaxForCCFeeOnly(baseAvailRQ,
						quotedPriceInfoTO.getQuotedSegments(), totalPayablePaxCountForCreditCardFee, externalChgDTO, true, false);
				ccFee = AccelAeroCalculator.add(ccFee, totalServiceTaxAmount);

				totalWithCCFeeInBaseCurrency = AccelAeroCalculator.add(totalPrice, ccFee);
				BigDecimal CCFeeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(ccFee, currencyCode,
						exchangeRateProxy);
				BigDecimal totalWithCCFeeInPreferredCurrency = AccelAeroCalculator.add(totalInSelectedCurByFareBreakdown,
						CCFeeInPreferredCurrency);

				// set total price With CCFee in preferred currency
				if (totalWithCCFeeInPreferredCurrency != null) {
					OTAUtils.setAmountAndCurrency(totalFareWithCCFee, totalWithCCFeeInPreferredCurrency, currencyCode);
				}

				FareType.TotalEquivFareWithCCFee totalEquivFareWithCCFee = new FareType.TotalEquivFareWithCCFee();
				itinearyFareInfo.setTotalEquivFareWithCCFee(totalEquivFareWithCCFee);
				// set total With CCFee Equiv price
				if (equiCurrencyCode != null) {
					BigDecimal toalWithCCFeeInEquivCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
							totalWithCCFeeInBaseCurrency, equiCurrencyCode, exchangeRateProxy);
					OTAUtils.setAmountAndCurrency(totalEquivFareWithCCFee, toalWithCCFeeInEquivCurrency, equiCurrencyCode);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(totalEquivFareWithCCFee, totalWithCCFeeInBaseCurrency);
				}

				if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
						&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, principal.getAgentCode())) {

					// set total Equiv price
					if (equiCurrencyCode != null) {
						BigDecimal toalWithCCFeeInEquivCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
								totalWithCCFeeInBaseCurrency, equiCurrencyCode, exchangeRateProxy);
						if (toalWithCCFeeInEquivCurrency != null) {
							FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
							itinearyFareInfo.setTotalEquivFare(totalEquivFare);
							OTAUtils.setAmountAndCurrency(totalEquivFare, toalWithCCFeeInEquivCurrency, equiCurrencyCode);
						}
					} else {
						FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
						itinearyFareInfo.setTotalEquivFare(totalEquivFare);
						OTAUtils.setAmountAndDefaultCurrency(totalEquivFare, totalWithCCFeeInBaseCurrency);
						OTAUtils.setAmountAndCurrency(totalFare, totalWithCCFeeInPreferredCurrency, currencyCode);
					}
				}
			}

			if (ParameterUtil.isApplyBSPFee()) {			

				FareType.TotalFareWithBSPFee totalFareWithBSPFee = new FareType.TotalFareWithBSPFee();
				itinearyFareInfo.setTotalFareWithBSPFee(totalFareWithBSPFee);

				ExternalChgDTO externalChgDTO = ReservationUtil.getBSPCharge(totalPayablePaxCount);
				BigDecimal bspFee = externalChgDTO.getAmount();
				bspFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.BSP_FEE, bspFee);

				BigDecimal totalServiceTaxAmount = ServiceTaxUtil.calculateServiceTaxForCCFeeOnly(baseAvailRQ,
						quotedPriceInfoTO.getQuotedSegments(), totalPayablePaxCount, externalChgDTO, true, true);

				BigDecimal bspFeeWithServiceTax = AccelAeroCalculator.add(bspFee, totalServiceTaxAmount);
				
				totalPrice = ParameterUtil.isApplyAdminFee(principal.getAgentCode()) ? totalWithCCFeeInBaseCurrency : totalPrice;

				BigDecimal totalWithBSPFeeInBaseCurrency = AccelAeroCalculator.add(totalPrice, bspFeeWithServiceTax);
				BigDecimal bspFeeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(bspFeeWithServiceTax,
						currencyCode, exchangeRateProxy);
				BigDecimal totalWithBSPFeeInPreferredCurrency = AccelAeroCalculator.add(totalInSelectedCurByFareBreakdown,
						bspFeeInPreferredCurrency);

				if (totalWithBSPFeeInPreferredCurrency != null) {
					OTAUtils.setAmountAndCurrency(totalFareWithBSPFee, totalWithBSPFeeInPreferredCurrency, currencyCode);
				}

				FareType.TotalEquivFareWithBSPFee totalEquivFareWithBSPFee = new FareType.TotalEquivFareWithBSPFee();
				itinearyFareInfo.setTotalEquivFareWithBSPFee(totalEquivFareWithBSPFee);

				if (equiCurrencyCode != null) {
					BigDecimal toalWithBSPFeeInEquivCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
							totalWithBSPFeeInBaseCurrency, equiCurrencyCode, exchangeRateProxy);
					OTAUtils.setAmountAndCurrency(totalEquivFareWithBSPFee, toalWithBSPFeeInEquivCurrency, equiCurrencyCode);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(totalEquivFareWithBSPFee, totalWithBSPFeeInBaseCurrency);
				}

			}

		} else {
			// Adult Fare
			if (ReservationApiUtils.isFareExist(priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.ADULT))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(
						priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.ADULT),
						new BigDecimal(otaPaxCountTO.getAdultCount())));
			}

			// Child Fare
			if (ReservationApiUtils.isFareExist(priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.CHILD))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(
						priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.CHILD),
						new BigDecimal(otaPaxCountTO.getChildCount())));
			}

			// Infant fare
			if (ReservationApiUtils.isFareExist(priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.INFANT))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(
						priceInfoTO.getTotalFarebyPaxType(priceInfoTO, PaxTypeTO.INFANT),
						new BigDecimal(otaPaxCountTO.getInfantCount())));
			}

			OTAUtils.setAmountAndDefaultCurrency(baseFare, totalBaseFare);

			quotedPriceInfoTO.setTotalBaseFare(totalBaseFare);
			BigDecimal[] totals = getQuotedTotalPrices(priceInfoTO, otaPaxCountTO, quotedPriceInfoTO);

			BigDecimal totalPrice = totals[0];

			// Adding Handling fare to the total price. For now only external charge is the handling fee
			BigDecimal handleCharge = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE).getTotalAmount();
			handleCharge = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handleCharge);

			totalPrice = AccelAeroCalculator.add(totalPrice, handleCharge);

			// adding selected external charges for the selected services
			totalPrice = AccelAeroCalculator.add(totalPrice, totalSelectedExternalCharges);

			// adding the selected flexi charges for the price quote
			if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
				BigDecimal totalFlexiChargesForPriceQuote = WSFlexiFareUtil.calculateTotalFlexiFareForPriceQuote(priceInfoTO,
						otaPaxCountTO, flexiSelectionCriteria, isFixedFare);
				totalPrice = AccelAeroCalculator.add(totalPrice, totalFlexiChargesForPriceQuote);
			}

			OTAUtils.setAmountAndDefaultCurrency(totalFare, totalPrice);

			// Set total price in agent currency
			BigDecimal totalFareInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(totalPrice,
					principal.getAgentCurrencyCode(), exchangeRateProxy);

			if (totalFareInAgentCurrency != null) {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				itinearyFareInfo.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndCurrency(totalEquivFare, totalFareInAgentCurrency, principal.getAgentCurrencyCode());
			}

			// Set total price with card payment transaction fee
			if (AppSysParamsUtil.applyCreditCardCharge()) {
				// Amount in base currency
				FareType.TotalFareWithCCFee totalFareWithCCFee = new FareType.TotalFareWithCCFee();
				itinearyFareInfo.setTotalFareWithCCFee(totalFareWithCCFee);

				ExternalChgDTO externalChgDTO = WSReservationUtil.getCCChargeAmount(totalPrice, totalPayablePaxCountForCreditCardFee,
						totalSegmentCount, ChargeRateOperationType.MAKE_ONLY); // ReservationUtil.getCCCharge(totalPrice);

				BigDecimal ccFee = externalChgDTO.getAmount();
				ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

				// calculate service tax
				BigDecimal totalServiceTaxAmount = ServiceTaxUtil.calculateServiceTaxForCCFeeOnly(baseAvailRQ,
						quotedPriceInfoTO.getQuotedSegments(), totalPayablePaxCountForCreditCardFee, externalChgDTO, true ,false);

				ccFee = AccelAeroCalculator.add(ccFee, totalServiceTaxAmount);

				totalWithCCFeeInBaseCurrency = AccelAeroCalculator.add(totalPrice, ccFee);

				OTAUtils.setAmountAndDefaultCurrency(totalFareWithCCFee, totalWithCCFeeInBaseCurrency);

				// Amount in agent currency
				FareType.TotalEquivFareWithCCFee totalEquivFareWithCCFee = new FareType.TotalEquivFareWithCCFee();
				itinearyFareInfo.setTotalEquivFareWithCCFee(totalEquivFareWithCCFee);

				BigDecimal toalWithCCFeeInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
						totalWithCCFeeInBaseCurrency, principal.getAgentCurrencyCode(), exchangeRateProxy);
				OTAUtils.setAmountAndCurrency(totalEquivFareWithCCFee, toalWithCCFeeInAgentCurrency,
						principal.getAgentCurrencyCode());

				if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
						&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, principal.getAgentCode())) {

					// set total price, totalFare -> totalFareWithCCFee
					OTAUtils.setAmountAndDefaultCurrency(totalFare, totalWithCCFeeInBaseCurrency);

					// set total Equiv price, totalEquivFare -> totalEquivFareWithCCFee
					// Set total price in agent currency
					if (totalFareInAgentCurrency != null) {
						FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
						itinearyFareInfo.setTotalEquivFare(totalEquivFare);
						OTAUtils.setAmountAndCurrency(totalEquivFare, toalWithCCFeeInAgentCurrency,
								principal.getAgentCurrencyCode());
					}
				}
			}
			
			
			if (ParameterUtil.isApplyBSPFee()) {				
				
				FareType.TotalFareWithBSPFee totalFareWithBSPFee = new FareType.TotalFareWithBSPFee();
				itinearyFareInfo.setTotalFareWithBSPFee(totalFareWithBSPFee);

				ExternalChgDTO externalChgDTO = ReservationUtil.getBSPCharge(totalPayablePaxCount);
				BigDecimal bspFee = externalChgDTO.getAmount();
				bspFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.BSP_FEE, bspFee);	
				
				BigDecimal totalServiceTaxAmount = ServiceTaxUtil.calculateServiceTaxForCCFeeOnly(baseAvailRQ,
						quotedPriceInfoTO.getQuotedSegments(), totalPayablePaxCount, externalChgDTO, true, true);

				BigDecimal bspFeeWithServiceTax = AccelAeroCalculator.add(bspFee, totalServiceTaxAmount);
				
				totalPrice = ParameterUtil.isApplyAdminFee(principal.getAgentCode()) ? totalWithCCFeeInBaseCurrency : totalPrice;
				
				BigDecimal totalWithBSPFeeInBaseCurrency = AccelAeroCalculator.add(totalPrice, bspFeeWithServiceTax);				

				OTAUtils.setAmountAndDefaultCurrency(totalFareWithBSPFee, totalWithBSPFeeInBaseCurrency);
				
				FareType.TotalEquivFareWithBSPFee totalEquivFareWithBSPFee = new FareType.TotalEquivFareWithBSPFee();
				itinearyFareInfo.setTotalEquivFareWithBSPFee(totalEquivFareWithBSPFee);

				BigDecimal toalWithBSPFeeInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
						totalWithBSPFeeInBaseCurrency, principal.getAgentCurrencyCode(), exchangeRateProxy);
				OTAUtils.setAmountAndCurrency(totalEquivFareWithBSPFee, toalWithBSPFeeInAgentCurrency,
						principal.getAgentCurrencyCode());
				
			}
			
			
		}

		// Sets each passenger type wise fare, tax break down and surcharges(fees) breakdown
		airItineraryPricingInfo.setPTCFareBreakdowns(ptcFareBreakdowns);
	}

	/**
	 * 
	 * @param quotedCharges
	 * @param adultsCount
	 * @param childcount
	 * @param infantsCount
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal[] getQuotedTotalPrices(PriceInfoTO priceInfoTO, OTAPaxCountTO otaPaxCountTO,
			QuotedPriceInfoTO quotedPriceInfoTO) throws ModuleException {

		BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perAdultPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perInfantPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perChildPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

		perAdultPaxTotalPrice = priceInfoTO.getTotalPricebyPaxType(priceInfoTO, PaxTypeTO.ADULT);
		perInfantPaxTotalPrice = priceInfoTO.getTotalPricebyPaxType(priceInfoTO, PaxTypeTO.INFANT);
		perChildPaxTotalPrice = priceInfoTO.getTotalPricebyPaxType(priceInfoTO, PaxTypeTO.CHILD);

		totalTicketPrice = AccelAeroCalculator.add(
				(AccelAeroCalculator.multiply(perAdultPaxTotalPrice, new BigDecimal(otaPaxCountTO.getAdultCount()))),
				(AccelAeroCalculator.multiply(perChildPaxTotalPrice, new BigDecimal(otaPaxCountTO.getChildCount()))),
				(AccelAeroCalculator.multiply(perInfantPaxTotalPrice, new BigDecimal(otaPaxCountTO.getInfantCount()))));

		if (quotedPriceInfoTO
				.getPerPaxChargeByPaxType(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT)) != null) {
			quotedPriceInfoTO
					.getPerPaxChargeByPaxType(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))
					.setTotalFare(perAdultPaxTotalPrice);
		}

		if (quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
				.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT)) != null) {
			quotedPriceInfoTO.getPerPaxChargeByPaxType(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT)).setTotalFare(
					perInfantPaxTotalPrice);
		}

		if (quotedPriceInfoTO
				.getPerPaxChargeByPaxType(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD)) != null) {
			quotedPriceInfoTO
					.getPerPaxChargeByPaxType(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))
					.setTotalFare(perChildPaxTotalPrice);
		}

		quotedPriceInfoTO.setTotalTicketPrice(totalTicketPrice);

		return new BigDecimal[] { totalTicketPrice, perAdultPaxTotalPrice, perInfantPaxTotalPrice, perChildPaxTotalPrice };
	}

	private static void setCcFeeInPTCFareBreakdown(BigDecimal totalPriceForCalculateCCCharge, int totalSegmentCount,
			boolean showPriceInselectedCurrency, String currencyCode, ExchangeRateProxy exchangeRateProxy,
			BigDecimal totalInSelectedCurPerPax, PTCFareBreakdownType ptcBT, List<FlightSegmentTO> flightSegments) throws WebservicesException, ModuleException {
		AirFeeType ccFeeType = new AirFeeType();
		BigDecimal ccFee = BigDecimal.ZERO;

		ExternalChgDTO ccChgDTO = WSReservationUtil.getCCChargeAmount(totalPriceForCalculateCCCharge, 1, totalSegmentCount,
				ChargeRateOperationType.MAKE_ONLY); // ReservationUtil.getCCCharge(totalPrice);
		ccFee = ccChgDTO.getAmount();
		ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

		// calculate service tax
		BigDecimal totalServiceTaxAmount = ServiceTaxUtil
				.calculateServiceTaxForCCFeeOnly(ServiceTaxUtil.getDummyBaseAvailRQ(1, 0, 0), flightSegments, 1, ccChgDTO, true,false);
		ccFee = AccelAeroCalculator.add(ccFee, totalServiceTaxAmount);

		if (AccelAeroCalculator.isGreaterThan(ccFee, (AccelAeroCalculator.getDefaultBigDecimalZero()))) {
			ccFeeType = new AirFeeType();
			if (showPriceInselectedCurrency && currencyCode != null) {
				OTAUtils.setAmountAndCurrency(ccFeeType, ccFee, currencyCode);
			} else {
				OTAUtils.setAmountAndDefaultCurrency(ccFeeType, ccFee);
			}
			ccFeeType.setFeeCode(ccChgDTO.getChargeCode() + "/" + ccChgDTO.getChargeDescription());
		}

		ptcBT.getPassengerFare().getFees().getFee().add(ccFeeType);
		ptcBT.getPassengerFare().getTotalFare()
				.setAmount(AccelAeroCalculator.add(ptcBT.getPassengerFare().getTotalFare().getAmount(), ccFee));

	}

}
