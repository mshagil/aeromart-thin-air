package com.isa.thinair.webservices.core.handler.common;

import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.handler.AbstractHandler;
import org.codehaus.xfire.handler.Phase;
import org.codehaus.xfire.jaxb2.JaxbType;
import org.codehaus.xfire.soap.handler.ReadHeadersHandler;
import org.xml.sax.SAXException;

public class JaxbValidationEnablingHandler extends AbstractHandler {

	public JaxbValidationEnablingHandler() throws SAXException {
		super();
		setPhase(Phase.PARSE);
		before(ReadHeadersHandler.class.getName());
	}

	public void invoke(MessageContext ctx) {
		ctx.setProperty(JaxbType.ENABLE_VALIDATION, "true");
	}
}
