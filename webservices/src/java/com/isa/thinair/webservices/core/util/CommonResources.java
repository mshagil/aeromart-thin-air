package com.isa.thinair.webservices.core.util;

import java.util.Map;

public class CommonResources {

	private static Map<String, Object[]> airportInfoMap = null;

	public static void initializeAirportInfo() {
		if (airportInfoMap == null) {
			airportInfoMap = WebServicesModuleUtils.getWebServiceDAO().getAirportShortNames(null);
		}
	}

	private static Map<String, Object[]> getAirportInfoMap() {
		initializeAirportInfo();
		return airportInfoMap;
	}

	public static Object[] getAirportInfo(String airportCode) {
		Object[] airportData = getAirportInfoMap().get(airportCode);
		if (airportData == null) {
			// Updated the cached airport list
			Map newAirportMap = WebServicesModuleUtils.getWebServiceDAO().getAirportShortNames(airportCode);
			if (newAirportMap != null && newAirportMap.containsKey(airportCode)) {
				airportData = (Object[]) newAirportMap.get(airportCode);
				airportInfoMap.put(airportCode, airportData);
			}
		}
		return airportData;
	}
}
