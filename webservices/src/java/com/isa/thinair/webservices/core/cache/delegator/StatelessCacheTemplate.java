package com.isa.thinair.webservices.core.cache.delegator;

import org.springframework.cache.CacheManager;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.delegator.throttle.ThrottleManager;

public interface StatelessCacheTemplate<T, V> {

	public CacheManager getCacheManager();

	public String getCacheName();

	public T emptyResponse();

	public String generateCacheStoreKey(V request) throws ModuleException, WebservicesException;

	public T execute(V request) throws ModuleException, WebservicesException;

	public T cachePostProcess(CacheResponse<T> cacheResponse, V request) throws ModuleException, WebservicesException;

	public ThrottleManager getThrottleManager();

	public boolean isEnabledCache();

	public T composeCacheStoreResponse(T response);
	
	public String getAudit(V request);

}
