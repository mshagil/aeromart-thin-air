package com.isa.thinair.webservices.core.adaptor.impl.pricing;

import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;

import com.isa.thinair.webservices.core.adaptor.Adaptor;
import com.isa.thinair.webservices.core.adaptor.util.AdaptorUtil;

public class AirIternaryOriginDestinationOptionAdaptor implements Adaptor<OriginDestinationOption, OriginDestinationOptionType> {

	@Override
	public OriginDestinationOptionType adapt(OriginDestinationOption source) {

		OriginDestinationOptionType target = new OriginDestinationOptionType();

		BookFlightSegmentTypeAdaptor adaptor = new BookFlightSegmentTypeAdaptor();
		AdaptorUtil.adaptCollection(source.getFlightSegment(), target.getFlightSegment(), adaptor);

		return target;
	}

}
