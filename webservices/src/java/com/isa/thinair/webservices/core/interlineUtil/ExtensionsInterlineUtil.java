package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.OTAUtils;
import com.isa.thinair.webservices.core.util.ParameterUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAlterationBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACnxModAddONDBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACurrencyAmountType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATravelerCnxModAddResBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATravelersCnxModAddResBalancesType;

/**
 * For instantiating OTA Extensions/For extracting info from OTA Extensions
 * 
 * @author Manoj Dhanushka
 */
public class ExtensionsInterlineUtil {

	/**
	 * Prepares WS cancellation/modification/addition balances.
	 * 
	 * @param resSummaryDTO
	 * @return AACnxModBalancesType
	 * @throws ModuleException
	 */
	public static AAAlterationBalancesType prepareWSResAlterationBalances(Collection<LCCClientReservationPax> resPaxes,
			ReservationBalanceTO reservationBalanceTO, String alterationType, int effectingSegsCount) throws ModuleException {
		AAAlterationBalancesType balances = new AAAlterationBalancesType();

		AACnxModAddONDBalancesType updatingONDBalances = null;
		AACnxModAddONDBalancesType newONDBalances = null;

		int adultCount = 0;
		int childCount = 0;

		ThreadLocalData.setCurrentTnxParam(Transaction.REQUOTE_BALANCE_TO, reservationBalanceTO);
		
		String agentCode = ThreadLocalData.getCurrentUserPrincipal().getAgentCode();
		
		for (LCCClientReservationPax pax : resPaxes) {
			if (PaxTypeTO.ADULT.equals(pax.getPaxType())) {
				adultCount++;
			} else if (PaxTypeTO.CHILD.equals(pax.getPaxType())) {
				childCount++;
			}
		}

		// Existing OND data
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			updatingONDBalances = new AACnxModAddONDBalancesType();
			updatingONDBalances.setFare(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary().getCurrentFareAmount()));
			updatingONDBalances.setTaxAndSurcharge(getAACurrenyAmount(AccelAeroCalculator.add(reservationBalanceTO
					.getSegmentSummary().getCurrentTaxAmount(), reservationBalanceTO.getSegmentSummary()
					.getCurrentSurchargeAmount())));
			updatingONDBalances
					.setAdjustments(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary().getCurrentAdjAmount()));
			updatingONDBalances.setOtherCharges(getAACurrenyAmount(AccelAeroCalculator.add(reservationBalanceTO
					.getSegmentSummary().getCurrentCnxAmount(), reservationBalanceTO.getSegmentSummary().getCurrentModAmount())));
			updatingONDBalances.setTotalCharges(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary()
					.getCurrentTotalPrice()));
			updatingONDBalances.setNonRefundableAmount(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary()
					.getCurrentNonRefunds()));
			updatingONDBalances.setRefundableAmount(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary()
					.getCurrentRefunds()));
			// TODO: Show Penalty amount
		}

		// New OND data
		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			newONDBalances = new AACnxModAddONDBalancesType();

			newONDBalances.setFare(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary().getNewFareAmount()));
			newONDBalances.setTotalCharges(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary().getNewTotalPrice()));
			newONDBalances.setTaxAndSurcharge(getAACurrenyAmount(AccelAeroCalculator.add(reservationBalanceTO.getSegmentSummary()
					.getNewTaxAmount(), reservationBalanceTO.getSegmentSummary().getNewSurchargeAmount(), reservationBalanceTO
					.getSegmentSummary().getNewExtraFeeAmount())));
			newONDBalances.setAdjustments(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary().getNewAdjAmount()));
			newONDBalances.setOtherCharges(getAACurrenyAmount(AccelAeroCalculator.add(reservationBalanceTO.getSegmentSummary()
					.getNewCnxAmount(), reservationBalanceTO.getSegmentSummary().getNewModAmount())));
			// TODO: Set these values
			// newONDBalances.setNonRefundableAmount(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary().getNewNonRefunds()));
			// newONDBalances.setRefundableAmount(getAACurrenyAmount(reservationBalanceTO.getSegmentSummary().getNewRefunds()));
			// TODO: Show Penalty amount
		}

		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
			balances.setAACnxONDBalances(updatingONDBalances);
		}

		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			balances.setAAModONDBalances(new AAAlterationBalancesType.AAModONDBalances());
			balances.getAAModONDBalances().setUpdatingONDCharges(updatingONDBalances);
			balances.getAAModONDBalances().setNewONDCharges(newONDBalances);
		}

		if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			balances.setAAAddONDBalances(newONDBalances);
		}

		Map<Integer, LCCClientReservationPax> resPaxMap = new HashMap<Integer, LCCClientReservationPax>();
		for (LCCClientReservationPax resPax : resPaxes) {
			resPaxMap.put(PaxTypeUtils.getPnrPaxId(resPax.getTravelerRefNumber()), resPax);
		}

		// Set pax balances
		AATravelersCnxModAddResBalancesType travelersBalances = new AATravelersCnxModAddResBalancesType();
		Collection<LCCClientPassengerSummaryTO> paxSummaries = reservationBalanceTO.getPassengerSummaryList();
		for (LCCClientPassengerSummaryTO paxSummary : paxSummaries) {
			AATravelerCnxModAddResBalancesType travelerBalances = new AATravelerCnxModAddResBalancesType();
			travelerBalances.setTravelerRefNumberRPH(paxSummary.getTravelerRefNumber());
			// TODO: Set this value
			// travelerBalances.setCurrentTotalCharges(getAACurrenyAmount(paxSummary.getCurrentCharges()));
			travelerBalances.setNewTotalCharges(getAACurrenyAmount(paxSummary.getTotalPrice()));
			travelerBalances.setCurrentTotalPayments(getAACurrenyAmount(paxSummary.getTotalPaidAmount()));
			travelerBalances.setBalance(getAACurrenyAmount(paxSummary.getTotalAmountDue()));
			travelersBalances.getTravelerCnxModAddResBalances().add(travelerBalances);
		}

		balances.setTravelersCnxModAddResBalances(travelersBalances);

		// Set totals
		balances.setTotalAmountDue(getAACurrenyAmount(reservationBalanceTO.getTotalAmountDue()));
		balances.setTotalCreditAmount(getAACurrenyAmount(reservationBalanceTO.getTotalCreditAmount()));
		balances.setTotalCnxChargeForCurrentOperation(getAACurrenyAmount(reservationBalanceTO.getTotalCnxCharge()));
		balances.setTotalModChargeForCurrentOperation(getAACurrenyAmount(reservationBalanceTO.getTotalModCharge()));
		balances.setTotalPrice(getAACurrenyAmount(reservationBalanceTO.getTotalPrice()));

		if (BigDecimal.ZERO.compareTo(reservationBalanceTO.getTotalAmountDue()) == -1) {
			BigDecimal ccFee = AccelAeroCalculator.getDefaultBigDecimalZero();
			ExternalChgDTO ccCharge = WSReservationUtil.getCCChargeAmount(reservationBalanceTO.getTotalAmountDue(), adultCount
					+ childCount, effectingSegsCount, ChargeRateOperationType.ANY);
			ccFee = ccCharge.getAmount();
			ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

			balances.setTotalAmountDueCC(getAACurrenyAmount(AccelAeroCalculator.add(reservationBalanceTO.getTotalAmountDue(),
					ccFee)));
			balances.setTotalPriceCC(getAACurrenyAmount(AccelAeroCalculator.add(reservationBalanceTO.getTotalPrice(), ccFee)));

			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, agentCode)) {

				balances.setTotalAmountDue(getAACurrenyAmount(AccelAeroCalculator.add(reservationBalanceTO.getTotalAmountDue(),
						ccFee)));
				balances.setTotalPrice(getAACurrenyAmount(AccelAeroCalculator.add(reservationBalanceTO.getTotalPrice(), ccFee)));
			}

		} else {
			balances.setTotalAmountDueCC(getAACurrenyAmount(reservationBalanceTO.getTotalAmountDue()));
			balances.setTotalPriceCC(getAACurrenyAmount(reservationBalanceTO.getTotalPrice()));
		}

		return balances;
	}

	/**
	 * Returns amount in AACurrencyAmountType.
	 * 
	 * @param amount
	 * @return
	 */
	public static AACurrencyAmountType getAACurrenyAmount(BigDecimal amount) {
		AACurrencyAmountType currencyAmount = new AACurrencyAmountType();
		currencyAmount.setAmount(CommonUtil.getBigDecimalWithDefaultPrecision(amount));

		return currencyAmount;
	}

	public static void clearRequoteBalanceTO() {
		ThreadLocalData.setCurrentTnxParam(Transaction.REQUOTE_BALANCE_TO, null);
	}

}
