package com.isa.thinair.webservices.core.cache.delegator.keygen;

import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;
import org.springframework.util.ObjectUtils;

import com.isa.thinair.webservices.core.cache.util.CacheCommonUtil;
import com.isa.thinair.webservices.core.cache.util.CacheConstant;
import com.isa.thinair.webservices.core.cache.util.CacheConstant.CacheKeyParamName;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;

public abstract class OTARequestKeyUtil {
	
	protected String moduleSeparator = CacheConstant.cacheModuleSeparator;
	protected String dataSeparator = CacheConstant.cacheDataSeparator;
	
	protected void appendWithSeparator(StringBuilder stringBuilder, Object value) {
		stringBuilder.append(value).append(dataSeparator);
	}

	protected void appendKeyValueWithSeparator(StringBuilder stringBuilder, CacheKeyParamName Key, Object value) {
		if (!ObjectUtils.isEmpty(value)) {
			appendWithSeparator(stringBuilder, Key.getName());
			appendWithSeparator(stringBuilder, value);
		}
	}
	
	protected void appendKeyValue(StringBuilder stringBuilder, CacheKeyParamName Key, Object value) {
		stringBuilder.append(Key.getName());
		appendWithSeparator(stringBuilder, value);		
	}
	
	protected String getKeyHeader(String operationCode) {
		return CacheCommonUtil.getFullQualifiedCacheName(operationCode);
	}
	
	protected String getPointOfSalesKeyInfo(POSType pos) {		
		StringBuilder pointOfSalesKey = new StringBuilder();
		
		if (pos != null && pos.getSource() != null) {
			for (SourceType source : pos.getSource()) {
				appendKeyValue(pointOfSalesKey, CacheKeyParamName.CHANNEL, source.getBookingChannel().getType());				
			}
		}
		return pointOfSalesKey.toString();
	}
	
	protected String getSpecificAgentKeyInfo() {
		StringBuilder specificAgentKeyInfo = new StringBuilder();
		String agentCode = ThreadLocalData.getCurrentUserPrincipal().getAgentCode();
		if (agentCode != null && CacheCommonUtil.isSpecificCacheAgent(agentCode)) {
			appendWithSeparator(specificAgentKeyInfo, agentCode);
		}		
		return specificAgentKeyInfo.toString();
	}
	
	protected String getTravelerQuantityInfo(TravelerInfoSummaryType travelerInfoSummaryType) {

		StringBuilder travelerQuantityKey = new StringBuilder();	

		if (travelerInfoSummaryType != null && travelerInfoSummaryType.getAirTravelerAvail() != null) {

			for (TravelerInformationType travelerInformationType : travelerInfoSummaryType.getAirTravelerAvail()) {

				if (travelerInformationType.getPassengerTypeQuantity() != null) {

					for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType
							.getPassengerTypeQuantity()) {

						appendWithSeparator(travelerQuantityKey,
								passengerTypeQuantityType.getCode() + passengerTypeQuantityType.getQuantity());

					}

				}

			}

		}

		return travelerQuantityKey.toString();
	}


}
