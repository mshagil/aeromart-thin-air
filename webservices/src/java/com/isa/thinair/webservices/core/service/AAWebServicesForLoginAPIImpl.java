package com.isa.thinair.webservices.core.service;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ae.isa.api.login.AAAPILoginRQ;
import ae.isa.api.login.AAAPILoginRS;

import com.isa.thinair.webservices.api.service.AAWebServicesForLoginAPI;
import com.isa.thinair.webservices.core.bl.AALoginAPIBL;

@WebService(name = "AAWebServicesForLoginAPI", targetNamespace = "http://www.isa.ae/api/login",
		endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForLoginAPI")
public class AAWebServicesForLoginAPIImpl implements AAWebServicesForLoginAPI {

	private final Log log = LogFactory.getLog(AAWebServicesForLoginAPIImpl.class);

	public AAAPILoginRS login(AAAPILoginRQ loginRQ) {
		AAAPILoginRS loginRS;
		if (log.isDebugEnabled()) {
			log.debug("BEGIN ---- login(AAAPILoginRQ)");
		}

		loginRS = AALoginAPIBL.login(loginRQ);
		if (log.isDebugEnabled()) {
			log.debug("END ---- login(AAAPILoginRQ)");
		}
		return loginRS;
	}
}
