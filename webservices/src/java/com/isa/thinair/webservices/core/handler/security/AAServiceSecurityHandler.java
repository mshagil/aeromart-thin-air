package com.isa.thinair.webservices.core.handler.security;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.xfire.transport.http.XFireServletController;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.SessionGateway;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * @author Manoj Dhanushka
 */
public class AAServiceSecurityHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Log log = LogFactory.getLog(AAServiceSecurityHandler.class);

	public AAServiceSecurityHandler() {
		// Do nothing
	}

	@Override
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void close(MessageContext context) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		logSOAPMessage(context);
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		logSOAPMessage(context);

		Boolean outMessageIndicator = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (outMessageIndicator.booleanValue()) {
			// do nothing....
		} else {
			String defaultAirlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();

			String sessionId = null;
			HttpSession httpSession = null;
			try {
				httpSession = ((HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST)).getSession();

				if (httpSession != null) {
					/*
					 * access session so that web container initiate the session ?
					 */
					sessionId = httpSession.getId();
					log.debug("HTTP sessionID=" + sessionId);
				} else {
					log.debug("HTTP sessionID is null");
				}
			} catch (Exception e) {
				log.error("HTTP session initialization failed", e);
			}

			try {

				String loginName = null, loginPass = null, defaultSalesChannelId = null;
				// Bind the current WebservicesContext to the current thread
				ThreadLocalData.setCurrentWSContext(new WebservicesContext());

				try {
					// Propagate the request ip to be used for Fraud check
					String forwardingIP = ((HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST))
							.getHeader("X-Forwarded-For");

					if (forwardingIP == null || forwardingIP.trim().length() == 0) {
						forwardingIP = ((HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST)).getRemoteAddr();
					}
					// If there are two ip addresses, take the first one
					if (forwardingIP != null && forwardingIP.length() != 0) {
						if (forwardingIP.contains(",")) {
							String[] ips = forwardingIP.split(",");
							if (ips.length > 0) {
								forwardingIP = ips[0].trim();
							}
						}
					}
					if (forwardingIP != null)
						ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP, forwardingIP);
					else
						ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP,
								XFireServletController.getRequest().getRemoteAddr());

				} catch (Exception e) {
					log.error("HTTP header.host not found", e);
				}

				String requestIp = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_REQUEST_IP);

				if (requestIp == null || requestIp.equals(""))
					throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Sender_is_not_authorized,
							"Sender IP Address is Blank."); // FIXME the error
															// code

				// Amadeus Authentication
				Map userCredentials = AppSysParamsUtil.getAmadeusCredentials();

				if (userCredentials != null) {
					loginName = (String) userCredentials.get(WebservicesConstants.AmadeusConstants.LOGIN_NAME);
					if (loginName != null && loginName.length() > 0) {
						loginName = defaultAirlineCode + loginName;
					}
					loginPass = (String) userCredentials.get(WebservicesConstants.AmadeusConstants.LOGIN_PASS);
					defaultSalesChannelId = (String) userCredentials
							.get(WebservicesConstants.AmadeusConstants.DEFAULT_SALES_CHANNEL_ID);
				} else {
					throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error,
					// FIXME the error code
							"Amadeus credentials not configured in Accelero. Please report!!");
				}

				List<String> amadeusACL = AppSysParamsUtil.getAmadeusAccessControlList();

				if (amadeusACL != null) {
					if (!(amadeusACL.contains(requestIp))) {
						log.error("An unauthorized request came from IP address:" + requestIp);
						throw new WebservicesException(
								WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Authentication_Error,
								"Sender IP Address is not allowed."); // FIXME
																		// the
																		// error
																		// code
					}
				}

				if (log.isDebugEnabled()) {
					log.debug("ServiceInterceptor [ Operation = " + ", UserId = " + loginName + ", SessionId = " + sessionId
							+ " ]");
				}

				ThreadLocalData.getCurrentWSContext().setParameter(WebservicesContext.LOGIN_NAME, loginName);

				ThreadLocalData.getCurrentWSContext().setParameter(WebservicesContext.LOGIN_PASS, loginPass);

				if (httpSession != null) {
					// Propagate the web session to be send to CC Fraud check
					ThreadLocalData.setWSConextParam(WebservicesContext.WEB_SESSION_ID, sessionId);
				} else {
					log.debug("HTTP sessionID is null");
				}

				// temporary set sales channel to default to pass ejb
				// authentication process
				ThreadLocalData.setWSConextParam(WebservicesContext.SALES_CHANNEL_ID, defaultSalesChannelId);

				handleAuthentication();

			} catch (WebservicesException ex) {
				log.error(ex);
				throw new WebServiceException(ex);
			} catch (Exception ex) {
				log.error(ex);
				throw new WebServiceException(ex);
			}
		}
		return true;
	}

	/**
	 * In order to handle authentication for OTA Related - default
	 */
	private void handleAuthentication() {
		String userName = (String) ThreadLocalData.getCurrentWSContext()
				.getParameter(WebservicesContext.LOGIN_NAME);
		String password = (String) ThreadLocalData.getCurrentWSContext()
				.getParameter(WebservicesContext.LOGIN_PASS);
		try {
			Integer salesChannelId = null;
			salesChannelId = Integer.parseInt((String) ThreadLocalData.getCurrentWSContext()
					.getParameter(WebservicesContext.SALES_CHANNEL_ID));
			// Invoke client login module
			ForceLoginInvoker.webserviceLogin(userName, password, salesChannelId);
			// Initialize user session, if not already exists
			SessionGateway.SessionInHandler.handleSessionIn(userName, password, salesChannelId);
			log.debug("SecurityHandler:: Successfully authenticated user [userId=" + userName + "]");
		} catch (LoginException le) {
			String msg = "Authentication failed for [userId=" + userName + "]";
			ThreadLocalData.setTriggerPreProcessError(
					new WebservicesException(IOTACodeTables.ErrorWarningType_EWT_AUTHENTICATION, msg));
			log.error(msg, le);
		} catch (WebservicesException we) {
			ThreadLocalData.setTriggerPreProcessError(we);
			log.error("User session creation failed [userId=" + userName + "]", we);
		}
	}

	private void logSOAPMessage(SOAPMessageContext mtcx) {
		SOAPMessage message = mtcx.getMessage();
		StringOutputStream outputStream = new StringOutputStream();
		try {
			message.writeTo(outputStream);
			if (log.isDebugEnabled()) {
				log.debug("\n" + outputStream.toString() + "\n");
			}
		} catch (Exception e) {
			System.out.println("Exception in handler: " + e);
		}
	}

	class StringOutputStream extends OutputStream {

		private StringBuilder string = new StringBuilder();

		@Override
		public void write(int b) {
			this.string.append((char) b);
		}

		public String toString() {
			return this.string.toString();
		}
	}

}
