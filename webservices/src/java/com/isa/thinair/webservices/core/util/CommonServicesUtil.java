package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.SourceType;

import java.math.BigDecimal;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

public class CommonServicesUtil {

	private static Log log = LogFactory.getLog(CommonServicesUtil.class);

	public static Collection getCollection(String[] arrPNRSegmentIds) {
		Collection col = new ArrayList();
		int len = arrPNRSegmentIds.length;

		for (int i = 0; i < len; i++) {
			col.add(arrPNRSegmentIds[i]);
		}

		return col;
	}

	public static Collection getIntCollection(String[] arrPNRSegmentIds) {
		Collection col = new ArrayList();
		int len = arrPNRSegmentIds.length;

		for (int i = 0; i < len; i++) {
			col.add(new Integer(arrPNRSegmentIds[i]));
		}

		return col;
	}

	public static Map getPaxONDChargeMap(Collection colOndCharges) {
		Map mapPaxONDCharges = new HashMap();

		if (colOndCharges != null) {
			for (Iterator iter = colOndCharges.iterator(); iter.hasNext();) {
				PnrChargesDTO pnrChargesDTO = (PnrChargesDTO) iter.next();

				mapPaxONDCharges.put(pnrChargesDTO.getPnrPaxId(), pnrChargesDTO);
			}
		}

		return mapPaxONDCharges;
	}

	public static boolean checkONDSegments(ReservationPaxFare resPaxFare, Collection colSegmentIds) {
		Set resPaxFareSegment = resPaxFare.getPaxFareSegments();
		ReservationPaxFareSegment reservationPaxFareSegment;

		for (Iterator iter = resPaxFareSegment.iterator(); iter.hasNext();) {
			reservationPaxFareSegment = (ReservationPaxFareSegment) iter.next();

			if (!colSegmentIds.contains(reservationPaxFareSegment.getPnrSegId().toString())) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 
	 * @param pax
	 *            : The reservation pax
	 * @param includePnrPaxID
	 *            : Whether to include pnr pax id with the RPH.
	 * 
	 * @return: The traveler RPH.
	 */
	public static String getTravelerRPH(ReservationPax pax, boolean includePnrPaxID) {
		String paxType = "";
		if (ReservationApiUtils.isAdultType(pax)) {
			paxType = "A";
		} else if (ReservationApiUtils.isChildType(pax)) {
			paxType = "C";
		} else if (ReservationApiUtils.isInfantType(pax)) {
			paxType = "I";
		}

		StringBuffer rph = new StringBuffer(paxType).append(pax.getPaxSequence());
		if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
			rph.append("/A" + pax.getParent().getPaxSequence());
		}
		if (includePnrPaxID) {
			rph.append("$" + pax.getPnrPaxId());
		}
		return rph.toString();
	}

	public static <T> T getFrom(Collection<T> collection, String compareMethod, Object value) {
		Object o = null;
		for (Iterator i = collection.iterator(); i.hasNext();) {
			o = i.next();
			Object result = null;
			try {
				result = o.getClass().getMethod(compareMethod).invoke(o);
			} catch (Exception e) {
				log.error(e);
				return null;
			}
			if (result.equals(value))
				break;
		}
		return (T) o;
	}

	/**
	 * Converts the OTA pax type String to AccelAero Pax Type indication String.
	 * 
	 * @param otaPaxType
	 *            : The OTA Pax type string
	 * 
	 * @return : The AccelAero Pax Type String.
	 * @throws ModuleException
	 *             : if a compatible conversion cannot be done.
	 */
	public static String convertToAccelAeroPaxType(String otaPaxType) throws ModuleException {
		String aaPaxTypeString = null;

		if (StringUtils.isNotEmpty(otaPaxType)) {
			if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT).equals(otaPaxType)) {
				aaPaxTypeString = PaxTypeTO.ADULT;
			} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD).equals(otaPaxType)) {
				aaPaxTypeString = PaxTypeTO.CHILD;
			} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(otaPaxType)) {
				aaPaxTypeString = PaxTypeTO.INFANT;
			}
		}

		if (aaPaxTypeString == null) {
			throw new ModuleException("Cannot convert from OTA pax type to AccelAero pax type!");
		}

		return aaPaxTypeString;
	}

	public static void sortFlightSegByDepDate(List<FlightSegmentDTO> flightSegmentTOs) {
		Collections.sort(flightSegmentTOs, new Comparator<FlightSegmentDTO>() {

			@Override
			public int compare(FlightSegmentDTO fltSegFirst, FlightSegmentDTO fltSegSecond) {
				return fltSegFirst.getDepartureDateTimeZulu().compareTo(fltSegSecond.getDepartureDateTimeZulu());
			}

		});
	}

	/**
	 * Get the flight segment TO list from Price quote response
	 * 
	 * @param flightPriceRS
	 * @return
	 */
	public static Map<Integer, FlightSegmentTO> getFlightSegmentDetails(FlightPriceRS flightPriceRS) {

		Map<Integer, FlightSegmentTO> flightSegments = new HashMap<Integer, FlightSegmentTO>();

		List<OriginDestinationInformationTO> informationTOs = flightPriceRS.getOriginDestinationInformationList();

		if (informationTOs != null) {
			for (OriginDestinationInformationTO ondInfo : informationTOs) {
				for (OriginDestinationOptionTO ondOptionTO : ondInfo.getOrignDestinationOptions()) {
					for (FlightSegmentTO flightSegmentTO : ondOptionTO.getFlightSegmentList()) {
						if (BeanUtils.isNull(flightSegmentTO.getLogicalCabinClassCode())) {
							flightSegmentTO.setLogicalCabinClassCode(flightSegmentTO.getCabinClassCode());
						}
						flightSegments.put(flightSegmentTO.getFlightSegId(), flightSegmentTO);
					}
				}
			}
		}

		return flightSegments;
	}

	/**
	 * Get the flight segment TO list from Price quote response based on return type
	 * 
	 * @param baseAvailRS
	 * @return
	 */
	public static List<FlightSegmentTO> getFlightSegmentDetails(BaseAvailRS baseAvailRS, boolean isReturn) {

		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();

		List<OriginDestinationInformationTO> informationTOs = baseAvailRS.getOriginDestinationInformationList();

		if (informationTOs != null) {
			for (OriginDestinationInformationTO ondInfo : informationTOs) {
				if (ondInfo.isReturnFlag() == isReturn) {
					for (OriginDestinationOptionTO ondOptionTO : ondInfo.getOrignDestinationOptions()) {
						flightSegments.addAll(ondOptionTO.getFlightSegmentList());

					}
				}
			}
		}

		Collections.sort(flightSegments);
		return flightSegments;
	}

	public static ClientCommonInfoDTO getClientInfoDTO(UserPrincipal userPrincipal) {
		ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();

		String defaultCArrier = (userPrincipal == null) ? AppSysParamsUtil.getDefaultCarrierCode() : userPrincipal
				.getDefaultCarrierCode();

		clientInfoDTO.setCarrierCode(defaultCArrier);
		clientInfoDTO.setIpAddress(userPrincipal.getIpAddress());

		return clientInfoDTO;
	}

	/**
	 * Compose BasicTrackInfo object of UserPrinciple If no UserPrinciple is available, method will return null
	 * 
	 * @param request
	 * @return
	 * 
	 * @see UserPrincipal
	 */
	public static BasicTrackInfo getBasicTrackInfo(UserPrincipal userPrincipal) {
		BasicTrackInfo bt = null;
		if (userPrincipal != null) {
			bt = new BasicTrackInfo();
			bt.setSessionId(userPrincipal.getSessionId());
			bt.setCallingInstanceId(userPrincipal.getCallingInstanceId());
		}
		return bt;
	}

	/**
	 * 
	 * @param userPrincipal
	 * @return
	 */
	public static TrackInfoDTO getTrackInfo(UserPrincipal userPrincipal) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		SourceType pos = (SourceType) ThreadLocalData.getCurrentWSContext()
				.getParameter(WebservicesContext.REQUEST_POS);
		String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);

		String ipAddress = BeanUtils.nullHandler(pos != null ? pos.getIPAddress() : null);
		if (ipAddress.length() == 0) {
			ipAddress = BeanUtils
					.nullHandler(ThreadLocalData.getWSContextParam(WebservicesContext.WEB_REQUEST_IP));
		}
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_WS);
		trackInfoDTO.setPaymentAgent(userPrincipal.getAgentCode());
		trackInfoDTO.setSessionId(sessionId);
		trackInfoDTO.setIpAddress(ipAddress);
		trackInfoDTO.setCarrierCode(userPrincipal.getDefaultCarrierCode());
		trackInfoDTO.setDirectBillId(userPrincipal.getDirectBillId());
		return trackInfoDTO;
	}
	
	/**
	 * Assume List<PaymentDetailType> will contain one single entry at this moment...
	 * 
	 * @param userPrincipal
	 * @param airBookModifyRQ
	 */
	public static void setDirectBillIdTo(UserPrincipal userPrincipal, OTAAirBookModifyRQ.AirBookModifyRQ airBookModifyRQ) {
		if (airBookModifyRQ != null && airBookModifyRQ.getFulfillment() != null
				&& airBookModifyRQ.getFulfillment().getPaymentDetails() != null
				&& airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail() != null
				&& !airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail().isEmpty()) {
			List<PaymentDetailType> paymentDetailTypes = airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail();
			PaymentDetailType paymentDetailType = paymentDetailTypes.get(0);
			if (paymentDetailType != null && paymentDetailType.getDirectBill() != null
					&& paymentDetailType.getDirectBill().getDirectBillID() != null) {
				userPrincipal.setDirectBillId(paymentDetailType.getDirectBill().getDirectBillID());
			}
		}
	}
	/**
	 * 
	 * @param airBookModifyRQ
	 * @return
	 */
	public static String getDirectBillId(OTAAirBookModifyRQ.AirBookModifyRQ airBookModifyRQ) {
		String directBillId = "";
		if (airBookModifyRQ != null && airBookModifyRQ.getFulfillment() != null
				&& airBookModifyRQ.getFulfillment().getPaymentDetails() != null
				&& airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail() != null
				&& !airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail().isEmpty()) {
			List<PaymentDetailType> paymentDetailTypes = airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail();
			PaymentDetailType paymentDetailType = paymentDetailTypes.get(0);
			if (paymentDetailType != null && paymentDetailType.getDirectBill() != null
					&& paymentDetailType.getDirectBill().getDirectBillID() != null) {
				directBillId = paymentDetailType.getDirectBill().getDirectBillID();
			}
		}
		return directBillId;
	}

	public static BigDecimal getAmount(OTAAirBookModifyRQ.AirBookModifyRQ airBookModifyRQ) {
		BigDecimal amount = new BigDecimal(0);
		if (airBookModifyRQ != null && airBookModifyRQ.getFulfillment() != null
				&& airBookModifyRQ.getFulfillment().getPaymentDetails() != null
				&& airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail() != null
				&& !airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail().isEmpty()) {
			List<PaymentDetailType> paymentDetailTypes = airBookModifyRQ.getFulfillment().getPaymentDetails().getPaymentDetail();
			PaymentDetailType paymentDetailType = paymentDetailTypes.get(0);
			if (paymentDetailType != null && paymentDetailType.getPaymentAmount() != null
					&& paymentDetailType.getPaymentAmount().getAmount() != null) {
				amount = paymentDetailType.getPaymentAmount().getAmount();
			}
		}
		return amount;
	}
	
	/**
	 * 
	 * @param userPrincipal
	 * @param fullfillment
	 */
	public static void setDirectBillIdTo(UserPrincipal userPrincipal, OTAAirBookRQ.Fulfillment fullfillment) {
		if (fullfillment != null) {
			List<PaymentDetailType> paymentDetailTypes = fullfillment.getPaymentDetails().getPaymentDetail();
			PaymentDetailType paymentDetailType = paymentDetailTypes.get(0);
			if (paymentDetailType != null && paymentDetailType.getDirectBill() != null
					&& paymentDetailType.getDirectBill().getDirectBillID() != null) {
				userPrincipal.setDirectBillId(paymentDetailType.getDirectBill().getDirectBillID());
			}
		}
	}

	public static String getDirectBillId(OTAAirBookRQ.Fulfillment fullfillment) {
		String directBillId = "";
		if (fullfillment != null) {
			List<PaymentDetailType> paymentDetailTypes = fullfillment.getPaymentDetails().getPaymentDetail();
			PaymentDetailType paymentDetailType = paymentDetailTypes.get(0);
			if (paymentDetailType != null && paymentDetailType.getDirectBill() != null
					&& paymentDetailType.getDirectBill().getDirectBillID() != null) {
				directBillId = paymentDetailType.getDirectBill().getDirectBillID();
			}
		}
		return directBillId;
	}
	
	public static BigDecimal getAmount(OTAAirBookRQ.Fulfillment fullfillment) {
		BigDecimal amount = new BigDecimal(0);
		if (fullfillment != null) {
			List<PaymentDetailType> paymentDetailTypes = fullfillment.getPaymentDetails().getPaymentDetail();
			PaymentDetailType paymentDetailType = paymentDetailTypes.get(0);
			if (paymentDetailType != null && paymentDetailType.getPaymentAmount() != null
					&& paymentDetailType.getPaymentAmount().getAmount() != null) {
				amount = paymentDetailType.getPaymentAmount().getAmount();
			}
		}
		return amount;
	}

	public static void setOndSequence(FlightSegmentTO flightSegmentTO) {
		QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);
		if (quotedPriceInfoTO != null && !quotedPriceInfoTO.getQuotedSegments().isEmpty()) {
			for (FlightSegmentTO quotedFlightSeg : quotedPriceInfoTO.getQuotedSegments()) {
				if (quotedFlightSeg != null
						&& PlatformUtiltiies.nullHandler(quotedFlightSeg.getFlightRefNumber()).equals(
								flightSegmentTO.getFlightRefNumber())) {
					flightSegmentTO.setOndSequence(quotedFlightSeg.getOndSequence());
				}
			}
		}
	}
	
	public static boolean isBulkTicketReservation(LCCClientReservation lccClientReservation) {
		Collection<LCCClientReservationSegment> resSegs = lccClientReservation.getSegments();
		for (LCCClientReservationSegment seg : resSegs) {
			if (seg!=null && seg.getFareTO()!=null && seg.getFareTO().isBulkTicketFareRule()) {
				return true;
			}
		}
		return false;
	}
}
