package com.isa.thinair.webservices.core.util;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRQ;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;

public class CreditAvailabilityUtil {

	private static final Log log = LogFactory.getLog(CreditAvailabilityUtil.class);

	/**
	 * Method to authorize a functionality. If not authorized, throw WebservicesException.
	 * 
	 * @param availableCreditRQ
	 * 
	 * @throws WebservicesException
	 */
	public static void authorize(AAOTAAgentAvailCreditRQ availableCreditRQ) throws WebservicesException {

		try {
			String rqAgentId = availableCreditRQ.getAgentID();
			if (rqAgentId != null && rqAgentId.length() > 0) {
				rqAgentId = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + rqAgentId;
			}
			String userId = availableCreditRQ.getPOS().getSource().get(0).getRequestorID().getID();
			if (userId != null && userId.length() > 0) {
				userId = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + userId;
			}
			String agentId = WebServicesModuleUtils.getSecurityBD().getUserBasicDetails(userId).getAgentCode();

			if (!rqAgentId.equals(agentId)) {
				String agentTypeCode = WebServicesModuleUtils.getTravelAgentBD().getAgent(agentId).getAgentTypeCode();
				if (agentTypeCode.equals(AirTravelAgentConstants.AgentTypes.TRAVEL_AGENT)) {
					log.warn("Authorization failure [userId=" + userId + "]");
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR, "Unauthorized Operation");

				} else if (agentTypeCode.equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					if (!isReportingAgent(rqAgentId, agentId)) {
						log.warn("Authorization failure [userId=" + userId + "]");
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
								"Unauthorized Operation");
					}
				}
			}

		} catch (Exception ex) {
			log.warn("Authorization failure");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR, "Unauthorized Operation");
		}
	}

	private static boolean isReportingAgent(String agentCode, String gsaCde) throws ModuleException {

		boolean isReporting = false;
		Collection<Agent> agCol = WebServicesModuleUtils.getTravelAgentBD().getReportingAgentsForGSA(gsaCde);
		if (agCol != null) {
			Iterator<Agent> iter = agCol.iterator();
			Agent tmpAgent = null;
			while (iter.hasNext()) {
				tmpAgent = (Agent) iter.next();
				if (tmpAgent.getAgentCode().equals(agentCode)) {
					isReporting = true;
					break;
				}
			}
		}
		return isReporting;
	}
}
