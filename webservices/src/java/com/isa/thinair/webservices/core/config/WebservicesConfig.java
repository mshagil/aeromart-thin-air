/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.config;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * Webserives module's custom configurations.
 * 
 * @author Mohamed Nasly
 * @isa.module.config-bean
 */
public class WebservicesConfig extends DefaultModuleConfig {

	private static Log log = LogFactory.getLog(WebservicesConfig.class);

	public static String SCHEDULER_OPTION_QUARTZ = "QUARTZ";
	public static String SCHEDULER_OPTION_TIMERTASK = "TIMERTASK";

	private List noPOSRequiredMethods = null;

	private List onewayMethods = null;

	private long userSessTimeoutInMills;

	private long txnTimeoutInMillis;

	private String schedulerOption;

	private long userSessionCleaningGapInMillis;

	private long txnCleaningGapInMillis;

	private boolean scheduleExtPayTxnRecon;

	private String firstExtPayTxnReconStartTimeStr;

	private long extPayTxnReconInterval;

	private String defaultPaymentBroker;

	private boolean schedulePublishInvForOpt;

	private boolean transactionValidityCheck;

	private String publishInvForOptStartTimeStr;

	private long publishInvForOptInterval;
	
	private String userName;
	
	private String  password;
	
	private String ejbAccessUsername;
	
	private String ejbAccessPassword;
	
	private boolean invokeCredentials;
	

	/**
	 * @return the schedulerOption
	 */
	public String getSchedulerOption() {
		return schedulerOption;
	}

	/**
	 * @param schedulerOption
	 *            the schedulerOption to set
	 */
	public void setSchedulerOption(String schedulerOption) {
		this.schedulerOption = schedulerOption;
	}

	/**
	 * @return the userSessionCleaningGapInMillis
	 */
	public long getUserSessionCleaningGapInMillis() {
		return userSessionCleaningGapInMillis;
	}

	/**
	 * @param userSessionCleaningGapInMillis
	 *            the userSessionCleaningGapInMillis to set
	 */
	public void setUserSessionCleaningGapInMillis(long userSessionCleaningGapInMillis) {
		this.userSessionCleaningGapInMillis = userSessionCleaningGapInMillis;
	}

	/**
	 * @return the txnCleaningGapInMillis
	 */
	public long getTxnCleaningGapInMillis() {
		return txnCleaningGapInMillis;
	}

	/**
	 * @param txnCleaningGapInMillis
	 *            the txnCleaningGapInMillis to set
	 */
	public void setTxnCleaningGapInMillis(long txnCleaningGapInMillis) {
		this.txnCleaningGapInMillis = txnCleaningGapInMillis;
	}

	public List getNoPOSRequiredMethods() {
		return noPOSRequiredMethods;
	}

	public void setNoPOSRequiredMethods(List noPOSRequiredMethods) {
		this.noPOSRequiredMethods = noPOSRequiredMethods;
	}

	public long getTxnTimeoutInMillis() {
		return txnTimeoutInMillis;
	}

	public void setTxnTimeoutInMillis(long txnTimeoutInMillis) {
		this.txnTimeoutInMillis = txnTimeoutInMillis;
	}

	public long getUserSessTimeoutInMills() {
		return userSessTimeoutInMills;
	}

	public void setUserSessTimeoutInMills(long userSessTimeoutInMills) {
		this.userSessTimeoutInMills = userSessTimeoutInMills;
	}

	public long getExtPayTxnReconInterval() {
		return extPayTxnReconInterval;
	}

	public void setExtPayTxnReconInterval(long extPayTxnReconInterval) {
		this.extPayTxnReconInterval = extPayTxnReconInterval;
	}

	public String getFirstExtPayTxnReconStartTimeStr() {
		return firstExtPayTxnReconStartTimeStr;
	}

	public void setFirstExtPayTxnReconStartTimeStr(String firstExtPayTxnReconStartTimeStr) {
		this.firstExtPayTxnReconStartTimeStr = firstExtPayTxnReconStartTimeStr;
	}

	public boolean isScheduleExtPayTxnRecon() {
		return scheduleExtPayTxnRecon;
	}

	public void setScheduleExtPayTxnRecon(boolean scheduleExtPayTxnRecon) {
		this.scheduleExtPayTxnRecon = scheduleExtPayTxnRecon;
	}

	public List getOnewayMethods() {
		return onewayMethods;
	}

	public void setOnewayMethods(List onewayMethods) {
		this.onewayMethods = onewayMethods;
	}

	public Date getfirstReconStartTime() {
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(getFirstExtPayTxnReconStartTimeStr());
		} catch (Exception e) {
			log.error("Configuration error found", e);
			throw new RuntimeException(e);
		}

		return date;

	}

	public boolean isSchedulePublishInvForOpt() {
		return schedulePublishInvForOpt;
	}

	public void setSchedulePublishInvForOpt(boolean schedulePublishInvForOpt) {
		this.schedulePublishInvForOpt = schedulePublishInvForOpt;
	}

	public String getPublishInvForOptStartTimeStr() {
		return publishInvForOptStartTimeStr;
	}

	public void setPublishInvForOptStartTimeStr(String publishInvForOptStartTimeStr) {
		this.publishInvForOptStartTimeStr = publishInvForOptStartTimeStr;
	}

	public Date getPublishInvForOptStartTime() {
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(getPublishInvForOptStartTimeStr());
		} catch (Exception e) {
			log.error("Configuration error found", e);
			throw new RuntimeException(e);
		}

		return date;

	}

	public long getPublishInvForOptInterval() {
		return publishInvForOptInterval;
	}

	public void setPublishInvForOptInterval(long publishInvForOptInterval) {
		this.publishInvForOptInterval = publishInvForOptInterval;
	}

	/**
	 * @return Returns the defaultPaymentBroker.
	 */
	public String getDefaultPaymentBroker() {
		return defaultPaymentBroker;
	}

	/**
	 * @param defaultPaymentBroker
	 *            The defaultPaymentBroker to set.
	 */
	public void setDefaultPaymentBroker(String defaultPaymentBroker) {
		this.defaultPaymentBroker = defaultPaymentBroker;
	}

	public boolean isTransactionValidityCheck() {
		return transactionValidityCheck;
	}

	public void setTransactionValidityCheck(boolean transactionValidityCheck) {
		this.transactionValidityCheck = transactionValidityCheck;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEjbAccessUsername() {
		return ejbAccessUsername;
	}

	public void setEjbAccessUsername(String ejbAccessUsername) {
		this.ejbAccessUsername = ejbAccessUsername;
	}

	public String getEjbAccessPassword() {
		return ejbAccessPassword;
	}

	public void setEjbAccessPassword(String ejbAccessPassword) {
		this.ejbAccessPassword = ejbAccessPassword;
	}

	public boolean isInvokeCredentials() {
		return invokeCredentials;
	}

	public void setInvokeCredentials(boolean invokeCredentials) {
		this.invokeCredentials = invokeCredentials;
	}
	
}
