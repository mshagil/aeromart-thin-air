package com.isa.thinair.webservices.core.util;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;

/**
 * 
 * @author Mehdi Raza
 * 
 */
public interface ITransformer {
	public Object[] transform(boolean isInternalCall) throws ModuleException, WebservicesException;
}
