package com.isa.thinair.webservices.core.service;

import javax.jws.HandlerChain;
import javax.jws.WebService;

import com.amadeus.ama._2008._03.AMATLAGetAvailabilityAndPriceRQ;
import com.amadeus.ama._2008._03.AMATLAGetAvailabilityAndPriceRS;
import com.amadeus.ama._2008._03.AMATLAGetBookingRQ;
import com.amadeus.ama._2008._03.AMATLAGetBookingRS;
import com.amadeus.ama._2008._03.AMATLAGetFareRulesRQ;
import com.amadeus.ama._2008._03.AMATLAGetFareRulesRS;
import com.amadeus.ama._2008._03.AMATLAPayAndBookRQ;
import com.amadeus.ama._2008._03.AMATLAPayAndBookRS;
import com.amadeus.ama._2008._03.AMATLAPricingRQ;
import com.amadeus.ama._2008._03.AMATLAPricingRS;
import com.amadeus.ama._2008._03.AMATLARepricingRQ;
import com.amadeus.ama._2008._03.AMATLARepricingRS;
import com.isa.thinair.webservices.api.service.AAAmadeusResWebServices;
import com.isa.thinair.webservices.core.bl.AmadeusAvailabilitySearchBL;
import com.isa.thinair.webservices.core.bl.AmadeusFareRuleBL;
import com.isa.thinair.webservices.core.bl.AmadeusPayAndBook;
import com.isa.thinair.webservices.core.bl.AmadeusPriceQuoteBL;
import com.isa.thinair.webservices.core.bl.AmadeusRetrieveBooking;

@WebService(name = "AAAmadeusResWebServices", targetNamespace = "http://www.amadeus.com/AMA/2008/03", endpointInterface = "com.isa.thinair.webservices.api.service.AAAmadeusResWebServices")
@HandlerChain(file = "/WEB-INF/handler.xml")
public class AAAmadeusResWebServicesImpl implements AAAmadeusResWebServices {

	public AMATLAGetFareRulesRS fareRules(AMATLAGetFareRulesRQ amaTLAGetFareRulesRQ) {
		return AmadeusFareRuleBL.execute(amaTLAGetFareRulesRQ);
	}

	public AMATLAPayAndBookRS payAndBook(AMATLAPayAndBookRQ amaTLAPayAndBookRQ) {
		return AmadeusPayAndBook.execute(amaTLAPayAndBookRQ);
	}

	public AMATLAPricingRS pricing(AMATLAPricingRQ amaTLAPricingRQ) {
		return AmadeusPriceQuoteBL.execute(amaTLAPricingRQ);
	}

	public AMATLARepricingRS repricing(AMATLARepricingRQ amaTLARepricingRQ) {
		throw new UnsupportedOperationException();
	}

	public AMATLAGetBookingRS booking(AMATLAGetBookingRQ amaTLAGetBookingRQ) {
		return AmadeusRetrieveBooking.execute(amaTLAGetBookingRQ);
	}

	public AMATLAGetAvailabilityAndPriceRS availabilityAndPrice(AMATLAGetAvailabilityAndPriceRQ amaTLAGetAvailabilityAndPriceRQ) {
		return AmadeusAvailabilitySearchBL.execute(amaTLAGetAvailabilityAndPriceRQ);
	}

}
