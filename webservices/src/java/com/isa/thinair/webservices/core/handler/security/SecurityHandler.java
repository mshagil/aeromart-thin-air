package com.isa.thinair.webservices.core.handler.security;

import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSSecurityEngineResult;
import org.apache.ws.security.WSUsernameTokenPrincipal;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.apache.ws.security.handler.WSHandlerResult;
import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.fault.XFireFault;
import org.codehaus.xfire.security.wss4j.WSS4JInHandler;
import org.codehaus.xfire.transport.http.XFireHttpSession;
import org.codehaus.xfire.transport.http.XFireServletController;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * Authenticates the user and propergates the credentials
 * 
 * @author Mehdi Raza
 * @author Nasly
 * 
 */
public class SecurityHandler extends WSS4JInHandler {

	private final Log log = LogFactory.getLog(getClass());

	public SecurityHandler() {
		super();
	}

	public SecurityHandler(Properties prop) {
		super(prop);

	}

	@Override
	public void invoke(MessageContext ctx) throws XFireFault {
		super.invoke(ctx);

		// Bind the current WebservicesContext to the current thread
		ThreadLocalData.setCurrentWSContext(new WebservicesContext());

		try {
			WSUsernameTokenPrincipal principal = getPrincipal(ctx);
			ThreadLocalData.getCurrentWSContext().setParameter(WebservicesContext.LOGIN_NAME, principal.getName());
			ThreadLocalData.getCurrentWSContext()
					.setParameter(WebservicesContext.LOGIN_PASS, principal.getPassword());
		} catch (IOException e) {
			ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_PREPROCESS_ERROR, new Boolean(true));
			ThreadLocalData.setWSConextParam(
					WebservicesContext.PREPROCESS_ERROR_OBJECT,
					new WebservicesException(IOTACodeTables.ErrorWarningType_EWT_AUTHENTICATION,
							"Invalid authentication data received."));
			return;
		}

		try {
			// Propagate the request ip to be used for Fraud check
			String forwardingIP = getHTTPHeaderValue(XFireServletController.getRequest(), "X-Forwarded-For");
			
			// If there are two ip addresses, take the first one
			if (forwardingIP != null && forwardingIP.length() != 0) {
				if (forwardingIP.contains(",")) {
					String[] ips = forwardingIP.split(",");
					if (ips.length > 0) {
						forwardingIP = ips[0].trim();
					}
				}
			}

			if (forwardingIP != null) {

				if (log.isDebugEnabled()) {
					log.debug("Forwarding IP for Fawry request is : " + forwardingIP);
				}
				ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP, forwardingIP);
			} else {
				ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP,
						XFireServletController.getRequest().getRemoteAddr());
			}
		} catch (Exception e) {
			log.error("HTTP header.host not found", e);
		}

		try {
			HttpSession httpSession = ((XFireHttpSession) ctx.getSession()).getSession();
			if (httpSession != null) {
				String id = httpSession.getId();// access session so that web container initiate the session ?
				log.debug("HTTP sessionID=" + id);
				// Propagate the web session to be send to CC Fraud check
				ThreadLocalData.setWSConextParam(WebservicesContext.WEB_SESSION_ID, id); 
			} else {
				log.debug("HTTP sessionID is null");
			}
		} catch (Exception e) {
			log.error("HTTP session initialization failed", e);
		}

	}

	private WSUsernameTokenPrincipal getPrincipal(MessageContext ctx) throws IOException {
		Vector result = (Vector) ctx.getProperty(WSHandlerConstants.RECV_RESULTS);
		WSUsernameTokenPrincipal principal = null;
		for (int i = 0; i < result.size(); i++) {
			WSHandlerResult res = (WSHandlerResult) result.get(i);
			for (int j = 0; j < res.getResults().size(); j++) {
				WSSecurityEngineResult secRes = (WSSecurityEngineResult) res.getResults().get(j);
				int action = secRes.getAction();
				// USER TOKEN
				if ((action & WSConstants.UT) > 0) {
					principal = (WSUsernameTokenPrincipal) secRes.getPrincipal();
					// Set user property to user from UT to allow response encryption
					// context.setProperty(WSHandlerConstants.ENCRYPTION_USER,principal.getName());
					
					if (log.isDebugEnabled()) {
						log.debug("SecurityHandler:: [UserName= " + principal.getName() + ",PasswordLength: "
								+ (principal.getPassword() != null ? principal.getPassword().length() : 0) + "]");
					}
				}
				// SIGNATURE
				if ((action & WSConstants.SIGN) > 0) {
					// java.security.cert.X509Certificate cert = secRes.getCertificate();
					// X500Name x500principal = (X500Name) secRes.getPrincipal();
					// Do something whith cert
					// System.out.print("Signature for : " + x500principal.getCommonName());
				}
			}
		}
		return principal;
	}
	
	/**
	 * Get specified attributes in header.
	 * 
	 * @param request
	 * @param headerName
	 * @return String
	 */
	private String getHTTPHeaderValue(HttpServletRequest request, String headerName) {
		String headerParamValue = "";
		try {
			headerParamValue = request.getHeader(headerName);
		} catch (Exception e) {
			headerParamValue = null;
		}
		return headerParamValue;
	}
}
