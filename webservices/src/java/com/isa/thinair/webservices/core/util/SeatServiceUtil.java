package com.isa.thinair.webservices.core.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.CabinClassType;
import org.opentravel.ota._2003._05.CabinClassType.AirRows;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.FlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirSeatMapRQ;
import org.opentravel.ota._2003._05.OTAAirSeatMapRQ.SeatMapRequests.SeatMapRequest;
import org.opentravel.ota._2003._05.OTAAirSeatMapRS;
import org.opentravel.ota._2003._05.OTAAirSeatMapRS.SeatMapResponses;
import org.opentravel.ota._2003._05.OTAAirSeatMapRS.SeatMapResponses.SeatMapResponse;
import org.opentravel.ota._2003._05.OTAAirSeatMapRS.SeatMapResponses.SeatMapResponse.FlightSegmentInfo;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.RowDetailsType;
import org.opentravel.ota._2003._05.RowDetailsType.AirSeats;
import org.opentravel.ota._2003._05.RowDetailsType.AirSeats.AirSeat;
import org.opentravel.ota._2003._05.SeatMapDetailsType;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TravelerInformationType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.FlightSeatStatuses;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirColumnGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCCabinClassDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDetailDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;

public class SeatServiceUtil extends BaseUtil {
	private static final Log log = LogFactory.getLog(SeatServiceUtil.class);

	public OTAAirSeatMapRS getSeatMap(OTAAirSeatMapRQ otaAirSeatMapRQ) throws WebservicesException {

		OTAAirSeatMapRS otaAirSeatMapRS = null;

		try {
			List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

			for (SeatMapRequest seatMapRequest : otaAirSeatMapRQ.getSeatMapRequests().getSeatMapRequest()) {

				FlightSegmentType flightSegmentType = seatMapRequest.getFlightSegmentInfo();

				FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
				flightSegmentTO.setArrivalDateTime(flightSegmentType.getArrivalDateTime().toGregorianCalendar().getTime());
				flightSegmentTO.setArrivalTerminalName(flightSegmentType.getArrivalAirport().getTerminal());
				flightSegmentTO.setDepartureDateTime(flightSegmentType.getDepartureDateTime().toGregorianCalendar().getTime());
				flightSegmentTO.setFlightNumber(flightSegmentType.getFlightNumber());
				if (flightSegmentType.getSegmentCode() != null) {
					flightSegmentTO.setSegmentCode(flightSegmentType.getSegmentCode());
				} else {
					flightSegmentTO.setSegmentCode(flightSegmentType.getDepartureAirport().getLocationCode() + "/"
							+ flightSegmentType.getArrivalAirport().getLocationCode());
				}
				flightSegmentTO.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
				if (flightSegmentType.getRPH() != null) {
					Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegmentType.getRPH());
					flightSegmentTO.setFlightSegId(flightSegId);
					flightSegmentTO.setFlightRefNumber(flightSegmentType.getRPH());
				}
				String cabinClassCode = "Y";
				if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
					cabinClassCode = AppSysParamsUtil.getDefaultCOS();
				}
				flightSegmentTO.setCabinClassCode(cabinClassCode);
				flightSegmentTO.setFlightRefNumber(flightSegmentType.getRPH());
				CommonServicesUtil.setOndSequence(flightSegmentTO);
				flightSegmentTOs.add(flightSegmentTO);
			}

			// checking the seat map availability with privileges
			if (isSeatMapAvailable(flightSegmentTOs)) {
				LCCSeatMapDTO inLCCSeatMapDTO = new LCCSeatMapDTO();
				inLCCSeatMapDTO.setFlightDetails(flightSegmentTOs);
				// setting system as AA due to WS API is not supporting for DRY/INTERLINE
				inLCCSeatMapDTO.setQueryingSystem(SYSTEM.AA);
				inLCCSeatMapDTO.setTransactionIdentifier(otaAirSeatMapRQ.getTransactionIdentifier());
				inLCCSeatMapDTO.setBundledFareDTOs(WSAncillaryUtil.getOndSelectedBundledFares());

				LCCSeatMapDTO outSeatMapDTO = WebServicesModuleUtils.getAirproxyAncillaryBD().getSeatMap(inLCCSeatMapDTO, null,
						null, ApplicationEngine.WS);

				otaAirSeatMapRS = processLCCSeatMap(outSeatMapDTO);
			} else {
				StringBuffer flightNos = new StringBuffer();
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					flightNos.append(flightSegmentTO.getFlightNumber() + ",");
				}
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_SEAT_MAP_NOT_AVAILABLE_FOR_REQUESTED_CARRIER,
						(flightSegmentTOs.size() == 1)
								? flightNos.deleteCharAt(flightNos.length() - 1).toString()
								: flightNos.toString());
			}

		} catch (ModuleException e) {
			log.error(e);
			throw new WebservicesException(e);
		}

		return otaAirSeatMapRS;
	}

	public static OTAAirSeatMapRS processLCCSeatMap(LCCSeatMapDTO outSeatMapDTO) throws ModuleException {

		OTAAirSeatMapRS otaAirSeatMapRS = new OTAAirSeatMapRS();

		otaAirSeatMapRS.setErrors(new ErrorsType());
		otaAirSeatMapRS.setWarnings(new WarningsType());

		SeatMapResponses seatMapResponses = new SeatMapResponses();
		for (LCCSegmentSeatMapDTO lccSegmentSeatMapDTO : outSeatMapDTO.getLccSegmentSeatMapDTOs()) {
			SeatMapResponse seatMapResponse = new SeatMapResponse();

			LCCSeatMapDetailDTO lccSeatMapDetailDTO = lccSegmentSeatMapDTO.getLccSeatMapDetailDTO();
			FlightSegmentTO flightSegmentTO = lccSegmentSeatMapDTO.getFlightSegmentDTO();
			FlightSegmentInfo flightSegmentInfo = getFlightSegmentInfo(flightSegmentTO);

			seatMapResponse.setFlightSegmentInfo(flightSegmentInfo);

			SeatMapDetailsType seatMapDetailsType = new SeatMapDetailsType();

			for (LCCCabinClassDTO lccCabinClassDTO : lccSeatMapDetailDTO.getCabinClass()) {
				String cabinType = lccCabinClassDTO.getCabinType();

				CabinClassType cabinClassType = new CabinClassType();
				cabinClassType.setCabinType(OTAUtils.getCabinType(cabinType));

				AirRows airRows = new AirRows();

				Map<Integer, ArrayList<LCCAirSeatDTO>> rowAndSeats = new HashMap<Integer, ArrayList<LCCAirSeatDTO>>();

				// creating row and seat map by using the AA seat map
				for (LCCAirRowGroupDTO lccAirRowGroupDTO : lccCabinClassDTO.getAirRowGroupDTOs()) {
					for (LCCAirColumnGroupDTO lccAirColumnGroupDTO : lccAirRowGroupDTO.getAirColumnGroups()) {
						for (LCCAirRowDTO lccAirRowDTO : lccAirColumnGroupDTO.getAirRows()) {
							for (LCCAirSeatDTO lccAirSeatDTO : lccAirRowDTO.getAirSeats()) {
								Integer rowNumber = Integer.parseInt(lccAirSeatDTO.getSeatNumber().substring(0,
										lccAirSeatDTO.getSeatNumber().length() - 1));
								if (rowAndSeats.get(rowNumber) == null) {
									rowAndSeats.put(rowNumber, new ArrayList<LCCAirSeatDTO>());
								}
								rowAndSeats.get(rowNumber).add(lccAirSeatDTO);
							}
						}
					}
				}

				ArrayList<Integer> rowNumbers = new ArrayList<Integer>();
				rowNumbers.addAll(rowAndSeats.keySet());
				Collections.sort(rowNumbers);

				for (int row : rowNumbers) {
					RowDetailsType rowDetailsType = new RowDetailsType();
					rowDetailsType.setRowNumber(new BigInteger(new Integer(row).toString()));

					AirSeats airSeats = new AirSeats();

					ArrayList<LCCAirSeatDTO> seats = rowAndSeats.get(row);
					Collections.sort(seats, new LCCSeatComparator());

					for (LCCAirSeatDTO seat : seats) {
						AirSeat airSeat = new AirSeat();
						airSeat.setSeatNumber(new Character(seat.getSeatNumber().charAt(seat.getSeatNumber().length() - 1))
								.toString());
						airSeat.setSeatAvailability(seat.getSeatAvailability());
						airSeat.getSeatCharacteristics().add(seat.getSeatCharge().toString());

						airSeats.getAirSeat().add(airSeat);
					}

					rowDetailsType.setAirSeats(airSeats);
					airRows.getAirRow().add(rowDetailsType);
				}

				cabinClassType.setAirRows(airRows);
				seatMapDetailsType.getCabinClass().add(cabinClassType);
			}

			seatMapResponse.getSeatMapDetails().add(seatMapDetailsType);
			seatMapResponses.getSeatMapResponse().add(seatMapResponse);

		}
		otaAirSeatMapRS.setSeatMapResponses(seatMapResponses);
		otaAirSeatMapRS.setSuccess(new SuccessType());

		return otaAirSeatMapRS;
	}

	public static FlightSegmentInfo getFlightSegmentInfo(FlightSegmentTO flightSegmentTO) throws ModuleException {
		FlightSegmentInfo flightSegmentInfo = new FlightSegmentInfo();

		String[] airPorts = flightSegmentTO.getSegmentCode().split("/");

		ArrivalAirport arrivalAirport = new ArrivalAirport();
		arrivalAirport.setTerminal(flightSegmentTO.getArrivalTerminalName());
		arrivalAirport.setLocationCode(airPorts[airPorts.length - 1]);

		DepartureAirport departureAirport = new DepartureAirport();
		departureAirport.setTerminal(flightSegmentTO.getDepartureTerminalName());
		departureAirport.setLocationCode(airPorts[0]);

		flightSegmentInfo.setArrivalAirport(arrivalAirport);
		flightSegmentInfo.setDepartureAirport(departureAirport);
		flightSegmentInfo.setArrivalDateTime(CommonUtil.parse(flightSegmentTO.getArrivalDateTime()));
		flightSegmentInfo.setDepartureDateTime(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
		flightSegmentInfo.setFlightNumber(flightSegmentTO.getFlightNumber());
		flightSegmentInfo.setSegmentCode(flightSegmentTO.getSegmentCode());
		if (flightSegmentTO.getFlightSegId() != null) {
			flightSegmentInfo.setRPH(flightSegmentTO.getFlightSegId() + "");
		}

		return flightSegmentInfo;
	}

	private static class LCCSeatComparator implements Comparator<LCCAirSeatDTO> {

		@Override
		public int compare(LCCAirSeatDTO seat1, LCCAirSeatDTO seat2) {
			return seat1.getSeatNumber().compareTo(seat1.getSeatNumber());
		}

	}

	public static boolean isSeatMapAvailable(List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {

		boolean isSeatMapAvailable = false;

		LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityInDTO = new LCCAncillaryAvailabilityInDTO();
		lccAncillaryAvailabilityInDTO.setFlightDetails(flightSegmentTOs);
		lccAncillaryAvailabilityInDTO.addAncillaryType(LCCAncillaryType.SEAT_MAP);
		lccAncillaryAvailabilityInDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		lccAncillaryAvailabilityInDTO.setModifyAncillary(false);
		lccAncillaryAvailabilityInDTO.setOnHold(false);
		lccAncillaryAvailabilityInDTO.setQueryingSystem(SYSTEM.AA);
		lccAncillaryAvailabilityInDTO.setDepartureSegmentCode(ReservationUtil.getDepartureSegmentCode(flightSegmentTOs));

		AnciAvailabilityRS anciAvailabilityRS = WebServicesModuleUtils.getAirproxyAncillaryBD().getAncillaryAvailability(
				lccAncillaryAvailabilityInDTO, null);
		List<LCCAncillaryAvailabilityOutDTO> lccAncillaryAvailabilityOutDTOs = anciAvailabilityRS
				.getLccAncillaryAvailabilityOutDTOs();
		lccAncillaryAvailabilityOutDTOs = checkPrivilegesAndBufferTimes(lccAncillaryAvailabilityOutDTOs);

		isSeatMapAvailable = anciActiveInLeastOneSeg(lccAncillaryAvailabilityOutDTOs, LCCAncillaryType.SEAT_MAP);

		return isSeatMapAvailable;
	}

	private static List<LCCAncillaryAvailabilityOutDTO> checkPrivilegesAndBufferTimes(
			List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				Date departureDate = ancillaryAvailabilityOutDTO.getFlightSegmentTO().getDepartureDateTime();
				String airportCode = ancillaryAvailabilityOutDTO.getFlightSegmentTO().getSegmentCode().split("/")[0];
				Date depatureDateZulu = null;
				try {
					depatureDateZulu = DateUtil.getZuluDateTime(departureDate, airportCode);
				} catch (ModuleException e) {
					depatureDateZulu = departureDate;
				}
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					ancillaryStatus.setAvailable(ancillaryStatus.isAvailable() && isEnableService(depatureDateZulu));

				}
			}
		}
		return availabilityOutDTO;
	}

	private static boolean isEnableService(Date departDate) {
		Calendar currentTime = Calendar.getInstance();
		long diffMils = AppSysParamsUtil.getXBESeatmapStopCutoverInMillis();

		if (departDate.getTime() - diffMils < currentTime.getTimeInMillis()) {
			
			Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

			boolean hasPrivAddSeatsInCutover = AuthorizationUtil.hasPrivilege(privilegeKeys,
					PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_ADD_SEATS_IN_CUTOVER);
			
			if (hasPrivAddSeatsInCutover) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	private static boolean anciActiveInLeastOneSeg(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					if ((ancillaryStatus.isAvailable())
							&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static Map<String, Map<Integer, LCCAirSeatDTO>> getSelectedSeatsDetails(
			Map<String, Map<Integer, String>> flightDetailsAndSeatMap, SelectedFlightDTO selectedFlightDTO)
			throws ModuleException {
		// key|value - travelerRPH|Map<Integer, LCCAirSeatDTO>
		Map<String, Map<Integer, LCCAirSeatDTO>> segmentAndSeatsMap = new HashMap<String, Map<Integer, LCCAirSeatDTO>>();

		Collection<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
		List<Integer> flightSegmentIDs = new ArrayList<Integer>();

		for (OndFareDTO ondFareDTO : selectedFlightDTO.getOndFareDTOs(false)) {
			flightSegmentDTOs.addAll(ondFareDTO.getSegmentsMap().values());
			flightSegmentIDs.addAll(ondFareDTO.getSegmentsMap().keySet());
		}

		// getting the seat maps
		Map<Integer, FlightSeatsDTO> seatMap = WebServicesModuleUtils.getSeatMapBD()
				.getFlightSeats(flightSegmentIDs, null, false);

		// override seats if bundled fare is selected
		setSeatOffers(seatMap, selectedFlightDTO);

		for (String travelerRef : flightDetailsAndSeatMap.keySet()) {

			// key | value - flight segment id | seat number
			Map<Integer, String> flightDetailsAndSeat = flightDetailsAndSeatMap.get(travelerRef);

			for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {

				for (Integer flightSegID : flightDetailsAndSeat.keySet()) {
					String seatNo = flightDetailsAndSeat.get(flightSegID);
					// validating against the flight segment ID
					if (flightSegID.equals(flightSegmentDTO.getSegmentId())) {
						FlightSeatsDTO flightSeatsDTO = seatMap.get(flightSegID);
						for (SeatDTO seatDTO : (Collection<SeatDTO>) flightSeatsDTO.getSeats()) {
							if (seatDTO.getSeatCode().equals(seatNo)) {
								LCCAirSeatDTO lccAirSeatDTO = new LCCAirSeatDTO();

								if (seatDTO.getSeatType() != null && seatDTO.getSeatType().equalsIgnoreCase("EXIT")) {
									lccAirSeatDTO.getNotAllowedPassengerType().add(PaxTypeTO.INFANT);
									lccAirSeatDTO.getNotAllowedPassengerType().add(PaxTypeTO.CHILD);
								}
								lccAirSeatDTO.setBookedPassengerType(seatDTO.getPaxType());
								lccAirSeatDTO.setSeatAvailability(seatDTO.getStatus());
								lccAirSeatDTO.setSeatCharge(seatDTO.getChargeAmount());
								lccAirSeatDTO.setSeatMessage(seatDTO.getSeatMessage());
								lccAirSeatDTO.setSeatNumber(seatDTO.getSeatCode());
								lccAirSeatDTO.setSeatType(seatDTO.getSeatType());
								lccAirSeatDTO.setSeatLocationType(seatDTO.getSeatLocationType());

								if (segmentAndSeatsMap.get(travelerRef) == null) {
									segmentAndSeatsMap.put(travelerRef, new HashMap<Integer, LCCAirSeatDTO>());
								}
								segmentAndSeatsMap.get(travelerRef).put(flightSegID, lccAirSeatDTO);
							}
						}
					}
				}
			}
		}

		return segmentAndSeatsMap;
	}

	public static void setSeatOffers(Map<Integer, FlightSeatsDTO> seatMap, SelectedFlightDTO selectedFlightDTO)
			throws ModuleException {
		Map<Integer, Integer> fltSegWiseSeatTemplates = WSAncillaryUtil.getFlightSegmentWiseBundledServiceTemplate(
				selectedFlightDTO, EXTERNAL_CHARGES.SEAT_MAP.toString());

		if (fltSegWiseSeatTemplates != null && !fltSegWiseSeatTemplates.isEmpty()) {
			for (Entry<Integer, FlightSeatsDTO> seatEntry : seatMap.entrySet()) {
				Integer fltSegId = seatEntry.getKey();
				Collection<SeatDTO> selectedSeats = seatEntry.getValue().getSeats();
				if (fltSegWiseSeatTemplates.containsKey(fltSegId) && fltSegWiseSeatTemplates.get(fltSegId) != null) {
					Integer seatTemplateId = fltSegWiseSeatTemplates.get(fltSegId);

					Collection<SeatDTO> bundledFareSeats = WebServicesModuleUtils.getSeatMapBD().getFlightSeatsByChargeTemplate(
							seatTemplateId);

					if (bundledFareSeats != null && !bundledFareSeats.isEmpty()) {
						for (SeatDTO selectedSeat : selectedSeats) {
							for (SeatDTO offeredSeat : bundledFareSeats) {
								if (selectedSeat.getSeatCode().equals(offeredSeat.getSeatCode())) {
									selectedSeat.setChargeAmount(offeredSeat.getChargeAmount());
									if (FlightSeatStatuses.INACTIVE.equals(offeredSeat.getStatus())) {
										selectedSeat.setStatus(FlightSeatStatuses.RESERVED);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public static Map<String, List<ExternalChgDTO>> getExternalChargesMapForSeats(
			Map<String, Map<Integer, LCCAirSeatDTO>> seatDetailsMap) throws ModuleException {

		// getting quoted external charge dto to get the required information
		Collection<EXTERNAL_CHARGES> exChargestypes = new HashSet<EXTERNAL_CHARGES>();
		exChargestypes.add(EXTERNAL_CHARGES.SEAT_MAP);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedMap = WebServicesModuleUtils.getReservationBD().getQuotedExternalCharges(
				exChargestypes, null, null);

		SMExternalChgDTO quotedChgDTO = (SMExternalChgDTO) quotedMap.get(EXTERNAL_CHARGES.SEAT_MAP);

		if (quotedChgDTO == null) {
			throw new ModuleException("Seat quotation failed");
		}

		Map<String, List<ExternalChgDTO>> externalCharegs = new HashMap<String, List<ExternalChgDTO>>();

		// creating the seat map related external charges against traveler RPH
		for (String travelerRef : seatDetailsMap.keySet()) {
			List<ExternalChgDTO> charegsList = new ArrayList<ExternalChgDTO>();
			Map<Integer, LCCAirSeatDTO> seatsMap = seatDetailsMap.get(travelerRef);
			for (Integer flightSegID : seatsMap.keySet()) {
				LCCAirSeatDTO airSeatDTO = seatsMap.get(flightSegID);

				SMExternalChgDTO externalChargeTO = new SMExternalChgDTO();

				externalChargeTO.setAmount(airSeatDTO.getSeatCharge());
				externalChargeTO.setChargeCode(airSeatDTO.getSeatNumber());
				externalChargeTO.setChargeDescription("Seat Selection");
				externalChargeTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.SUR);
				externalChargeTO.setChgRateId(quotedChgDTO.getChgRateId());
				externalChargeTO.setExternalChargesEnum(EXTERNAL_CHARGES.SEAT_MAP);
				externalChargeTO.setSeatCharge(airSeatDTO.getSeatCharge());
				externalChargeTO.setSeatNumber(airSeatDTO.getSeatNumber());
				externalChargeTO.setFlightSegId(flightSegID);

				charegsList.add(externalChargeTO);
			}

			externalCharegs.put(travelerRef, charegsList);

		}

		return externalCharegs;
	}

	public static void validateSeatRequest(OTAAirPriceRQ otaAirPriceRQ, Map<String, Map<Integer, LCCAirSeatDTO>> seatDetailsMap)
			throws WebservicesException, ModuleException {

		// to get the adults traveling with infants
		int infants = 0;
		for (TravelerInformationType travelerInformationType : otaAirPriceRQ.getTravelerInfoSummary().getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType.getPassengerTypeQuantity()) {
				if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					infants += passengerTypeQuantityType.getQuantity();
				}
			}
		}

		List<String> adultsTravellingWithInfants = new ArrayList<String>();

		for (int i = 0; i < infants; ++i) {
			adultsTravellingWithInfants.add("A" + (i + 1));
		}

		for (String travelerRef : seatDetailsMap.keySet()) {
			Map<Integer, LCCAirSeatDTO> seatsMap = seatDetailsMap.get(travelerRef);
			for (Integer flightSegId : seatsMap.keySet()) {
				LCCAirSeatDTO seatDTO = seatsMap.get(flightSegId);
				// validate the seat availability
				if (!seatDTO.getSeatAvailability().equals("VAC")) {
					throw new WebservicesException(IOTACodeTables.AirSeatType_AST_NO_SEAT_AT_THIS_LOCATION,
							"Seat is not available at the moment");
				} else if (seatDTO.getNotAllowedPassengerType() != null && seatDTO.getNotAllowedPassengerType().size() > 0) {
					// validating not allowed passenger types
					if (travelerRef.startsWith("A") && seatDTO.getNotAllowedPassengerType().contains(PaxTypeTO.ADULT)) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
								"Not allowed seat for adult");
					} else if (travelerRef.startsWith("C") && seatDTO.getNotAllowedPassengerType().contains(PaxTypeTO.CHILD)) {
						throw new WebservicesException(IOTACodeTables.AirSeatType_AST_NOT_ALLOWED_FOR_UNACCOMPANIED_MINORS,
								"Not allowed seat for Child");
					} else if (travelerRef.startsWith("A")) {
						if (adultsTravellingWithInfants.contains(travelerRef)
								&& seatDTO.getNotAllowedPassengerType().contains(PaxTypeTO.INFANT)) {
							throw new WebservicesException(IOTACodeTables.AirSeatType_AST_NOT_ALLOWED_FOR_INFANTS,
									"Not allowed seat for adult travelling with infant");
						}
					}
				}
				// else if (adultsTravellingWithInfants.contains(travelerRef)) {
				// // validating infants seat rules
				// Integer rowNumber = Integer.parseInt(seatDTO.getSeatNumber().substring(0,
				// seatDTO.getSeatNumber().length() - 1));
				// String seatChar = seatDTO.getSeatNumber().substring(seatDTO.getSeatNumber().length() - 1);
				// if ((seatChar.equals("A") || seatChar.equals("B") || seatChar.equals("C")) && (rowNumber % 2 == 0)) {
				// throw new WebservicesException(IOTACodeTables.AirSeatType_AST_NOT_ALLOWED_FOR_INFANTS,
				// "Not allowed seat for adult travelling with infant");
				// }
				//
				// List<Integer> fltSegIDs = new ArrayList<Integer>();
				// fltSegIDs.add(flightSegId);
				// Map<Integer, FlightSeatsDTO> seatRowsInfo = WebServicesModuleUtils.getSeatMapBD().getFlightSeatsRow(
				// fltSegIDs, null, rowNumber);
				// int rowGroupID = 1;
				// if (seatChar.equals("D") || seatChar.equals("E") || seatChar.equals("F")) {
				// rowGroupID = 2;
				// }
				// for (SeatDTO seatInTheRow : (Collection<SeatDTO>) seatRowsInfo.get(flightSegId).getSeats()) {
				// if (seatInTheRow.getStatus().equals("RES") && seatInTheRow.getPaxType().equals(PaxTypeTO.PARENT)
				// && rowGroupID == seatInTheRow.getRowGroupId()) {
				// throw new WebservicesException(IOTACodeTables.AirSeatType_AST_NOT_ALLOWED_FOR_INFANTS,
				// "Not allowed seat for adult travelling with infant");
				// }
				// }
				// }
			}
		}
	}

}
