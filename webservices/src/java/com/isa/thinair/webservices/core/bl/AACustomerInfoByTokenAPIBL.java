package com.isa.thinair.webservices.core.bl;

import javax.ejb.EJBAccessException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ae.isa.api.loginwithtoken.AAAPILoginWithTokenRS;
import ae.isa.api.loginwithtoken.ContactInformation;
import ae.isa.api.loginwithtoken.Customer;
import ae.isa.api.loginwithtoken.Error;
import ae.isa.api.loginwithtoken.ErrorType;
import ae.isa.api.loginwithtoken.ResponseStatus;

import com.isa.thinair.aircustomer.api.model.CustomerSession;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class AACustomerInfoByTokenAPIBL {

	private static Log log = LogFactory.getLog(AACustomerInfoByTokenAPIBL.class);

	public static AAAPILoginWithTokenRS getCustomerData(String userToken) {
		AirCustomerServiceBD airCustomerServiceBD = WebServicesModuleUtils.getAirCustomerServiceBD();
		AAAPILoginWithTokenRS loginRS = new AAAPILoginWithTokenRS();
		ErrorType errorType = new ErrorType();
		Error error;
		com.isa.thinair.aircustomer.api.model.Customer customer;

		try {
			CustomerSession loadCustomerSession = airCustomerServiceBD.loadCustomerSession(userToken);
			if (loadCustomerSession != null) {
				Integer customerId = loadCustomerSession.getCustomerId();
				customer = airCustomerServiceBD.getCustomer(customerId);
				if (customer != null) {
					loginRS.setCustomer(toCustomer(customer));
				} else {
					loginRS = new AAAPILoginWithTokenRS();
					loginRS.setStatus(ResponseStatus.FAIL);
					error = new Error();
					error.setErrorCode(WebservicesConstants.WebServicesErrorCodes.USER_NOT_FOUND.getErrorCode());
					error.setDescription(WebservicesConstants.WebServicesErrorCodes.USER_NOT_FOUND.getErrorDescription());
					errorType.getError().add(error);
				}
			} else {
				loginRS = new AAAPILoginWithTokenRS();
				loginRS.setStatus(ResponseStatus.FAIL);
				error = new Error();
				error.setErrorCode(WebservicesConstants.WebServicesErrorCodes.INVALID_TOKEN.getErrorCode());
				error.setDescription(WebservicesConstants.WebServicesErrorCodes.INVALID_TOKEN.getErrorDescription());
				errorType.getError().add(error);
			}
		} catch (EJBAccessException e) {
			loginRS = new AAAPILoginWithTokenRS();
			loginRS.setStatus(ResponseStatus.FAIL);
			error = new Error();
			error.setErrorCode(WebservicesConstants.WebServicesErrorCodes.WS_AUTHENTICATION_FAILED.getErrorCode());
			error.setDescription(WebservicesConstants.WebServicesErrorCodes.WS_AUTHENTICATION_FAILED.getErrorDescription());
			errorType.getError().add(error);

			log.error("getCustomerData(UserToken) ---- ", e);
		} catch (Exception e) {
			loginRS = new AAAPILoginWithTokenRS();
			loginRS.setStatus(ResponseStatus.FAIL);
			error = new Error();
			error.setErrorCode(WebservicesConstants.WebServicesErrorCodes.SERVER_ERROR.getErrorCode());
			error.setDescription(WebservicesConstants.WebServicesErrorCodes.SERVER_ERROR.getErrorDescription());
			errorType.getError().add(error);

			log.error("getCustomerData(UserToken) ---- ", e);
		}

		if (!errorType.getError().isEmpty()) {
			loginRS.setError(errorType);
		} else {

		}

		return loginRS;
	}

	private static Customer toCustomer(com.isa.thinair.aircustomer.api.model.Customer customer) throws ModuleException {
		CommonMasterBD commonMasterBD = WebServicesModuleUtils.getCommonMasterBD();
		String countryName;
		String nationalityName;
		Customer cust = new Customer();
		cust.setTitle(customer.getTitle());
		cust.setFirstName(StringUtil.toInitCap(customer.getFirstName()));
		cust.setLastName(StringUtil.toInitCap(customer.getLastName()));
		countryName = commonMasterBD.getCountryByName(customer.getCountryCode()).getCountryName();
		cust.setCountryOfResidence(countryName);
		nationalityName = commonMasterBD.getNationality(customer.getNationalityCode()).getDescription();
		cust.setNationality(nationalityName);
		ContactInformation contactInformation = new ContactInformation();
		contactInformation.setEmailAddress(customer.getEmailId());
		contactInformation.setMobileNumber(customer.getMobile());
		cust.setContactInformation(contactInformation);

		try {
			cust.setCustomerId(customer.getCustomerId());
			cust.setGender(customer.getGender());
			cust.setAlternativeEmailId(customer.getAlternativeEmailId());
			cust.setTelephone(customer.getTelephone());
			cust.setCountryCode(customer.getCountryCode());
			cust.setOfficeTelephone(customer.getOfficeTelephone());

			cust.setDateOfBirth(CalendarUtil.asXMLGregorianCalendar(customer.getDateOfBirth()));
			if (customer.getRegistration_date() != null) {
				cust.setRegistrationDate(CalendarUtil.asXMLGregorianCalendar(customer.getRegistration_date().getTime()));
			}
			if (customer.getConfirmation_date() != null) {
				cust.setConfirmationDate(CalendarUtil.asXMLGregorianCalendar(customer.getConfirmation_date().getTime()));
			}

			cust.setFax(customer.getFax());
			cust.setStatus(customer.getStatus());
			cust.setSecretQuestion(customer.getSecretQuestion());
			cust.setSecretAnswer(customer.getSecretAnswer());
			cust.setCity(customer.getCity());
			cust.setAddressStreet(customer.getAddressStreet());
			cust.setAddressLine(customer.getAddressLine());
			cust.setZipCode(customer.getZipCode());
			cust.setEmgnTitle(customer.getEmgnTitle());
			cust.setEmgnFirstName(customer.getEmgnFirstName());
			cust.setEmgnLastName(customer.getEmgnLastName());
			cust.setEmgnPhoneNo(customer.getEmgnPhoneNo());
			cust.setEmgnEmail(customer.getEmgnEmail());
			cust.setIsLmsMember(String.valueOf(customer.getIsLmsMember()));
		} catch (DatatypeConfigurationException e) {
			throw new ModuleException(e, "Error occurred while Customer Details transformation");
		}

		return cust;
	}
}
