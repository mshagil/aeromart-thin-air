package com.isa.thinair.webservices.core.adaptor.impl.availability;

import org.opentravel.ota._2003._05.AAPricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItinerariesType;

import com.isa.thinair.webservices.core.adaptor.Adaptor;

public class PricedItinerariesTypeAdaptor implements Adaptor<PricedItinerariesType, AAPricedItinerariesType> {

	@Override
	public AAPricedItinerariesType adapt(PricedItinerariesType source) {
		AAPricedItinerariesType target = new AAPricedItinerariesType();
		target.getPricedItinerary().addAll(source.getPricedItinerary());
		return target;
	}

}
