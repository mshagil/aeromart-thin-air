/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.xfire.transport.http.XFireConfigurableServlet;

/**
 * @author Mohamed Nasly
 */
public class XFireAAServlet extends XFireConfigurableServlet {

	private static Log log = LogFactory.getLog(XFireAAServlet.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if (log.isInfoEnabled()) {
			log.info("AccessInfo:" + request.getRemoteAddr() + ":"
					+ getNullSafeValue(getHTTPHeaderValue(request, "X-Forwarded-For")) + ":" + request.getRequestURI() + ":"
					+ request.getRequestedSessionId());
		}

		if (request.getRequestURI().endsWith(".xsd")) {
			response.setContentType("text/xml; charset=utf-8");
			request.getRequestDispatcher(
					"/WEB-INF/classes/META-INF/xfire/schema/"
							+ request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/") + 1)).forward(request,
					response);
			return;
		}
		super.doPost(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	private String getHTTPHeaderValue(HttpServletRequest request, String headerName) {
		String headerParamValue = "";
		try {
			headerParamValue = request.getHeader(headerName);
		} catch (Exception e) {
			headerParamValue = "-";
		}
		return headerParamValue;
	}

	private String getNullSafeValue(String value) {
		return StringUtils.trimToEmpty(value);
	}
}
