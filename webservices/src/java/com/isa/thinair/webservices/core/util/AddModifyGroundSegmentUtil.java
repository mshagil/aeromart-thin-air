/*
 * ==============================================================================
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.datatype.XMLGregorianCalendar;

import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class AddModifyGroundSegmentUtil {

	/**
	 * following functionality is duplicated as its present com.isa.thinair.airproxy.api.utils.CommonUtil when the
	 * booking is directed through airproxy following functionlilty can be replaced with CommonUtil one --meantime what
	 * ever changes related to this has to be maintained seperatly *
	 * 
	 * 1. Iterates through OriginDestinationOptionType and if ground station is present it adds the data to a map with
	 * key = segmentCode | value = groundStation 2.Set Ground Flight Ref ID to Air Segment 1. Identify GroundSegments
	 * And AirSegments | 2. Identify the correct AirSegment for GroundSegment // 3. Add the segment ID
	 * 
	 * Assumption is segment can only have one ground station (either from/to) ONLY
	 * 
	 * @param segments
	 * @param segmentMap
	 *            the map that holds groundstations. Can be null
	 * @return
	 * @throws ModuleException
	 * @throws NumberFormatException
	 */

	public static void mapStationsBySegment(List<OriginDestinationOptionType> segments, Map<Integer, String> segmentMap,
			Map<Integer, Integer> groundSegmentByAirFlightSegmentID) throws NumberFormatException, ModuleException {

		if (!AppSysParamsUtil.isGroundServiceEnabled()) {
			return;
		}

		if (segmentMap == null) {
			segmentMap = new LinkedHashMap<Integer, String>();
		}

		if (groundSegmentByAirFlightSegmentID == null) {
			groundSegmentByAirFlightSegmentID = new LinkedHashMap<Integer, Integer>();
		}
		// FlightReF No : SegmentCode
		Map<Integer, String> airSegments = new LinkedHashMap<Integer, String>();
		// Airport Code of the Parent Ground Segment Connection : Flight Ref.
		// No.
		Map<String, List<Integer>> groundSegments = new LinkedHashMap<String, List<Integer>>();
		boolean isGroundSegment = false;

		Collection<String> obAirports = new ArrayList<String>();
		boolean returnSegments = false;
		// Ordering Segment Sequance. For Local use
		Set<OriginDestinationOptionType> outBoundSegs = new LinkedHashSet<OriginDestinationOptionType>();
		Set<OriginDestinationOptionType> orderedSegSetReturn = new LinkedHashSet<OriginDestinationOptionType>();
		Set<OriginDestinationOptionType> orderedSegments = new LinkedHashSet<OriginDestinationOptionType>();

		for (OriginDestinationOptionType clientSeg : segments) {
			for (BookFlightSegmentType bookFlightSegment : clientSeg.getFlightSegment()) {
				returnSegments = false;
				if (!returnSegments
						&& CommonUtil.isReturnSegment(obAirports, bookFlightSegment.getDepartureAirport().getLocationCode(),
								bookFlightSegment.getArrivalAirport().getLocationCode())) {
					returnSegments = true;
				}

				if (!returnSegments) {
					obAirports.add(bookFlightSegment.getDepartureAirport().getLocationCode());
					obAirports.add(bookFlightSegment.getArrivalAirport().getLocationCode());
				}

				if (returnSegments) {
					orderedSegSetReturn.add(clientSeg);
				} else {
					outBoundSegs.add(clientSeg);
				}
			}

		}

		orderedSegments.addAll(outBoundSegs);
		orderedSegments.addAll(orderedSegSetReturn);
		
		OriginDestinationOptionType[]  sortedSegmentArray = sortONDOptionTypeByDepDate(orderedSegments);		

		for (OriginDestinationOptionType clientSeg : sortedSegmentArray) {
			for (BookFlightSegmentType clientReservationSegment : clientSeg.getFlightSegment()) {
				isGroundSegment = false;
				String segmentCode = clientReservationSegment.getDepartureAirport().getLocationCode() + "/"
						+ clientReservationSegment.getArrivalAirport().getLocationCode();

				if (isContainGroundSegment(AirproxyModuleUtils.getAirportBD().getCachedOwnAirportMap(getAirportCollection(segmentCode)))) {

					String groundStation = null;

					groundStation = returnGroundSegment(AirproxyModuleUtils.getAirportBD().getCachedOwnAirportMap(
							getAirportCollection(segmentCode)));

					segmentMap.put(Integer.parseInt(getFlightSegmentRefNo(clientReservationSegment.getRPH())), groundStation);
					isGroundSegment = true;
					List<Integer> existing = groundSegments.get(groundStation);
					// assuming segments are sent in the Dep/ return order: TODO:
					// CLEAN THIS UP
					if (existing == null) {
						existing = new ArrayList<Integer>();
						if (groundStation != null) {
							groundSegments.put(groundStation, existing);
						}
					}

					existing.add(Integer.parseInt(getFlightSegmentRefNo(clientReservationSegment.getRPH())));

				}

				if (!isGroundSegment) {
					airSegments.put(Integer.parseInt(getFlightSegmentRefNo(clientReservationSegment.getRPH())), segmentCode);
				}
			}

		}

		for (Entry<Integer, String> airSegmentMapEntry : airSegments.entrySet()) {
			for (Entry<String, List<Integer>> geoundSegMapEntry : groundSegments.entrySet()) {
				if (SubStationUtil.isPrimaryStationBelongingToGroundStationPresent(airSegmentMapEntry.getValue(),
						geoundSegMapEntry.getKey())) {
					List<Integer> existing = geoundSegMapEntry.getValue();
					if (existing != null && existing.size() > 0)
						groundSegmentByAirFlightSegmentID.put(existing.remove(0), airSegmentMapEntry.getKey());
					break;
				}
			}
		}
	}

	/**
	 * Assumption only one ground station will return.
	 * 
	 * @param airportCodes
	 * @return
	 */
	private static String returnGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (Iterator<CachedAirportDTO> iterator = airports.iterator(); iterator.hasNext();) {
			CachedAirportDTO airport = iterator.next();
			if (airport.isSurfaceSegment()) {
				return airport.getAirportCode();
			}
		}
		return null;
	}

	private static String getFlightSegmentRefNo(String rphCode) {
		if (rphCode != null)
			return rphCode.split("-")[0];

		return "";
	}

	public static Collection getAirportCollection(String segmentCode) {
		Collection airports = Arrays.asList(segmentCode.split("/"));
		return airports;
	}

	public static boolean isContainGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (Iterator<CachedAirportDTO> iterator = airports.iterator(); iterator.hasNext();) {
			CachedAirportDTO airport = iterator.next();
			if (airport.isSurfaceSegment()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Validate whether the connecting surface segment is valid
	 * 
	 * @param allSegment
	 *            contains {modfy segment + newground segment}
	 * @param linkedSegmentPnrSegId
	 * @return
	 */
	public static boolean isValidSegment(List<OriginDestinationOptionType> allSegment, Integer linkedSegmentPnrSegId) {
		try {

			BookFlightSegmentType parentResSeg = null;
			BookFlightSegmentType newResSeg = null;

			Long parentSegmentDepArrTime;
			Long childSegmentDepArrTime;
			boolean isIncomming = false;

			if (linkedSegmentPnrSegId == null || allSegment == null) {
				return false;
			}

			if (allSegment != null && allSegment.size() != 2) {
				return false;
			}

			for (OriginDestinationOptionType clientSeg : allSegment) {
				for (BookFlightSegmentType cliResSegment : clientSeg.getFlightSegment()) {
					// if (Integer.parseInt(cliResSegment.getRPH()) == linkedSegmentPnrSegId) {
					if (Integer.parseInt(getFlightSegmentRefNo(cliResSegment.getRPH())) == linkedSegmentPnrSegId) {
						parentResSeg = cliResSegment;
					} else {
						newResSeg = cliResSegment;
					}
				}
			}

			if (parentResSeg == null || newResSeg == null) {
				return false;
			}

			long flightclsgapMin = Long.parseLong(CommonsServices.getGlobalConfig().getBizParam(
					SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
			long flightClsgapMs = flightclsgapMin * 60 * 1000;
			long connectionTime = 43200000L;

			String newResSegCode = newResSeg.getDepartureAirport().getLocationCode() + "/"
					+ newResSeg.getArrivalAirport().getLocationCode();
			String parResSegCode = parentResSeg.getDepartureAirport().getLocationCode() + "/"
					+ parentResSeg.getArrivalAirport().getLocationCode();

			if (isGetDepartureTime(parResSegCode)) {
				parentSegmentDepArrTime = xmlCalToTime(parentResSeg.getDepartureDateTime());
			} else {
				parentSegmentDepArrTime = xmlCalToTime(parentResSeg.getArrivalDateTime());
			}

			if (isGetDepartureTime(newResSegCode)) {
				childSegmentDepArrTime = xmlCalToTime(newResSeg.getDepartureDateTime());
			} else {

				childSegmentDepArrTime = xmlCalToTime(newResSeg.getArrivalDateTime());
				isIncomming = true;
			}

			if (isIncomming) {
				long parentflightClosureTime = parentSegmentDepArrTime - flightClsgapMs;
				if ((childSegmentDepArrTime > parentflightClosureTime)
						|| (childSegmentDepArrTime < parentflightClosureTime - connectionTime)) {
					return false;
				}
			} else {
				long childflightClosureTime = childSegmentDepArrTime - flightClsgapMs;
				if ((childflightClosureTime < parentSegmentDepArrTime)
						|| childflightClosureTime > parentSegmentDepArrTime + connectionTime) {
					return false;
				}
			}

			if (!isGroundSegmentConnectionValid(parentResSeg, newResSeg)) {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private static Long xmlCalToTime(XMLGregorianCalendar cal) {
		Calendar gre = GregorianCalendar.getInstance();
		gre.set(cal.getYear(), cal.getMonth(), cal.getDay(), cal.getHour(), cal.getMinute(), cal.getSecond());
		return gre.getTime().getTime();
	}

	private static Date xmlCalToDate(XMLGregorianCalendar cal) {
		Calendar gre = GregorianCalendar.getInstance();
		gre.set(cal.getYear(), cal.getMonth(), cal.getDay(), cal.getHour(), cal.getMinute(), cal.getSecond());
		return gre.getTime();
	}

	private static boolean isFirstSegmentConnectingStation(String segmentCode) {

		String[] selFltSegments = segmentCode.split("/");

		if (isConnectingStation(selFltSegments[0])) {
			return true;
		}
		return false;
	}

	private static boolean isGetDepartureTime(String segmentCode) {
		return isFirstSegmentConnectingStation(segmentCode);
	}

	private static boolean isConnectingStation(String station) {
		return station != null && !"".equals(station)
				&& CommonsServices.getGlobalConfig().getMapParentStationByGroundStation().containsKey(station.trim()) ? true
				: false;
	}

	public static boolean isSegContainGroundSeg(List<OriginDestinationOptionType> segmentsColl) throws ModuleException {
		boolean isGroundSegment = false;
		for (OriginDestinationOptionType clientSeg : segmentsColl) {
			for (BookFlightSegmentType clientReservationSegment : clientSeg.getFlightSegment()) {

				String segmentCode = clientReservationSegment.getDepartureAirport().getLocationCode() + "/"
						+ clientReservationSegment.getArrivalAirport().getLocationCode();

				if (isContainGroundSegment(AirproxyModuleUtils.getAirportBD().getCachedOwnAirportMap(
						AddModifyGroundSegmentUtil.getAirportCollection(segmentCode)))) {
					isGroundSegment = true;
					break;
				}
			}
		}
		return isGroundSegment;
	}

	public static boolean isModifySegmentValidSurfaceSegment(Reservation existingRes,
			List<OriginDestinationOptionType> newSegment, Collection pnrSegIdsForMod) {

		if (pnrSegIdsForMod != null && !pnrSegIdsForMod.isEmpty()) {
			ReservationSegment resSegment;
			ReservationSegment parentSegment = null;
			Iterator iterSegments = existingRes.getSegments().iterator();
			while (iterSegments.hasNext()) {
				resSegment = (ReservationSegment) iterSegments.next();
				if (pnrSegIdsForMod.contains(resSegment.getPnrSegId())) {
					parentSegment = resSegment;
					break;
				}
			}

			if (parentSegment != null) {
				// Modify/Parent segment is Cancel
				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(parentSegment.getStatus())) {
					return false;
				}

				// Already has a CONF Ground Segment
				if (parentSegment.getGroundSegment() != null
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(parentSegment
								.getGroundSegment().getStatus())) {
					return false;
				}
			}

		}
		return true;
	}

	// check this

	public static boolean isGroundSegmentConnectionValid(BookFlightSegmentType parentResSeg, BookFlightSegmentType newResSeg) {
		try {
			String airlineCarrier = parentResSeg.getFlightNumber().substring(0, 2);
			String busCarrier = SelectListGenerator.getDefaultBusCarrierCode(airlineCarrier);

			Set<String> busConnectingAirports = CommonsServices.getGlobalConfig().getBusConnectingAirports();

			String flightSegmentCode = parentResSeg.getDepartureAirport().getLocationCode() + "/"
					+ parentResSeg.getArrivalAirport().getLocationCode();

			String[] minMaxTransitDurations = null;
			Calendar valiedTimeFrom = Calendar.getInstance();
			Calendar valiedTimeTo = Calendar.getInstance();

			Calendar newSegDepDateTime = Calendar.getInstance();
			newSegDepDateTime.setTime(xmlCalToDate(newResSeg.getDepartureDateTime()));

			Calendar newSegArrDateTime = Calendar.getInstance();
			newSegArrDateTime.setTime(xmlCalToDate(newResSeg.getArrivalDateTime()));

			if (busConnectingAirports.contains(flightSegmentCode.substring(0, 3))) {

				minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
						flightSegmentCode.substring(0, 3), busCarrier, airlineCarrier);

				String[] minDurationHHMM = minMaxTransitDurations[1].split(":");

				valiedTimeFrom.setTime(xmlCalToDate(parentResSeg.getDepartureDateTime()));
				valiedTimeFrom.add(Calendar.HOUR, -(Integer.parseInt(minDurationHHMM[0])));
				valiedTimeFrom.add(Calendar.MINUTE, -(Integer.parseInt(minDurationHHMM[1])));

				String[] maxDurationHHMM = minMaxTransitDurations[0].split(":");
				valiedTimeTo.setTime(xmlCalToDate(parentResSeg.getDepartureDateTime()));
				valiedTimeTo.add(Calendar.HOUR, -(Integer.parseInt(maxDurationHHMM[0])));
				valiedTimeTo.add(Calendar.MINUTE, -(Integer.parseInt(maxDurationHHMM[1])));

				// validate arrival date
				if (!isWithinTimeRange(newSegArrDateTime, valiedTimeFrom, valiedTimeTo)) {
					return false;
				}

			} else if (busConnectingAirports.contains(flightSegmentCode.substring(flightSegmentCode.length() - 3))) {
				minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
						flightSegmentCode.substring(flightSegmentCode.length() - 3), airlineCarrier, busCarrier);

				String[] minDurationHHMM = minMaxTransitDurations[0].split(":");
				valiedTimeFrom.setTime(xmlCalToDate(parentResSeg.getArrivalDateTime()));
				valiedTimeFrom.add(Calendar.HOUR, (Integer.parseInt(minDurationHHMM[0])));
				valiedTimeFrom.add(Calendar.MINUTE, (Integer.parseInt(minDurationHHMM[1])));

				String[] maxDurationHHMM = minMaxTransitDurations[1].split(":");
				valiedTimeTo.setTime(xmlCalToDate(parentResSeg.getArrivalDateTime()));
				valiedTimeTo.add(Calendar.HOUR, (Integer.parseInt(maxDurationHHMM[0])));
				valiedTimeTo.add(Calendar.MINUTE, (Integer.parseInt(maxDurationHHMM[1])));

				// Calendar dptStart = Calendar.getInstance();
				// dptStart.setTime(xmlCalToDate(newResSeg.getDepartureDateTime()));
				// Calendar dptEnd = Calendar.getInstance();
				// dptEnd.setTime(xmlCalToDate(newResSeg.getDepartureDateTime()));
				//
				// List<Calendar> valiedPeriod = valiedPeriod(dptStart, dptEnd, valiedTimeFrom, valiedTimeTo);
				//
				// if (valiedPeriod.size() == 2) {
				// valiedTimeFrom.setTime(valiedPeriod.get(0).getTime());
				// valiedTimeTo.setTime(valiedPeriod.get(1).getTime());
				// }

				// validate departure date
				if (!isWithinTimeRange(newSegDepDateTime, valiedTimeFrom, valiedTimeTo)) {
					return false;
				}
			}

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private static boolean isWithinTimeRange(Calendar timeToCheck, Calendar validFrom, Calendar validTo) {

		if (timeToCheck.compareTo(validFrom) < 0 || timeToCheck.compareTo(validTo) > 0) {
			return false;
		}

		return true;
	}
	
	private static OriginDestinationOptionType[] sortONDOptionTypeByDepDate(
			Set<OriginDestinationOptionType> setReservationSegments) {
		int size = setReservationSegments.size();
		OriginDestinationOptionType[] arrayToSort = setReservationSegments.toArray(new OriginDestinationOptionType[size]);
		Arrays.sort(arrayToSort, new Comparator<OriginDestinationOptionType>() {
			@Override
			public int compare(OriginDestinationOptionType rs1, OriginDestinationOptionType rs2) {
				Date rs1FltDep = new Date();
				Date rs2FltDep = new Date();
				for (BookFlightSegmentType bookFlightSegment : rs1.getFlightSegment()) {
					rs1FltDep = CalendarUtil.asDate(bookFlightSegment.getDepartureDateTime());
				}

				for (BookFlightSegmentType bookFlightSegment : rs2.getFlightSegment()) {
					rs2FltDep = CalendarUtil.asDate(bookFlightSegment.getDepartureDateTime());
				}

				return rs1FltDep.compareTo(rs2FltDep);
			}

		});

		return arrayToSort;
	}
}
