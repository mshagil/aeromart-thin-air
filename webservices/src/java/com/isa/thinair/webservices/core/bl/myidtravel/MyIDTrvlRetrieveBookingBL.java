/**
 * 
 */
package com.isa.thinair.webservices.core.bl.myidtravel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.OTAAirBookRS;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveBookingResponse;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlRetrieveBookingBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlRetrieveBookingBL.class);

	public static MyIDTravelResAdapterRetrieveBookingResponse execute(
			MyIDTravelResAdapterRetrieveBookingRequest myIDRetBookReq) {

		if (log.isDebugEnabled()) {
			log.debug("BEGIN retrieveBooking(MyIDTravelResAdapterRetrieveBookingRequest)");
		}

		MyIDTravelResAdapterRetrieveBookingResponse myIDRetBookRes = null;
		try {
			myIDRetBookRes = retrieveBooking(myIDRetBookReq);
		} catch (Exception ex) {
			log.error("book(MyIDTravelResAdapterBookingRequest) failed.", ex);
			if (myIDRetBookRes == null) {
				myIDRetBookRes = new MyIDTravelResAdapterRetrieveBookingResponse();
				OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
				otaAirBookRS.setEchoToken(myIDRetBookReq.getOTAReadRQ().getEchoToken());
				myIDRetBookRes.setOTAAirBookRS(otaAirBookRS);
			}
			if (myIDRetBookRes.getOTAAirBookRS().getErrors() == null)
				myIDRetBookRes.getOTAAirBookRS().setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDRetBookRes.getOTAAirBookRS().getErrors().getError(),
					myIDRetBookRes.getOTAAirBookRS().getSuccessAndWarningsAndAirReservation(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END retrieveBooking(MyIDTravelResAdapterRetrieveBookingRequest)");
		return myIDRetBookRes;
	}

	private static MyIDTravelResAdapterRetrieveBookingResponse retrieveBooking(
			MyIDTravelResAdapterRetrieveBookingRequest myIDRetBookReq) throws ModuleException, WebservicesException {

		MyIDTravelResAdapterRetrieveBookingResponse myIDRetBookRes = new MyIDTravelResAdapterRetrieveBookingResponse();
		myIDRetBookRes.setEmployeeData(myIDRetBookReq.getEmployeeData());

		OTAAirBookRS otaAirBookRS = MyIDTrvlCommonUtil.generateOTAAirBookRS(myIDRetBookReq.getOTAReadRQ(), true, false,
				false);
		myIDRetBookRes.setOTAAirBookRS(otaAirBookRS);

		return myIDRetBookRes;

	}
}
