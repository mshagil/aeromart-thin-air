package com.isa.thinair.webservices.core.handler.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Enumeration;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.MyIDTravelConstants;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.sun.xml.wss.impl.callback.CertificateValidationCallback;
import com.sun.xml.wss.impl.callback.DecryptionKeyCallback;
import com.sun.xml.wss.impl.callback.EncryptionKeyCallback;
import com.sun.xml.wss.impl.callback.SignatureKeyCallback;

/**
 * 
 * @author Janaka Padukka
 *
 */
public  class AAMyIDSecurityEnvHandler implements CallbackHandler {

	private String keyStoreURL;
	private String keyStorePassword;
	private String keyStoreType;

	private String trustStoreURL;
	private String trustStorePassword;
	private String trustStoreType;

	private KeyStore keyStore;
	private KeyStore trustStore;

	private static Log log = LogFactory.getLog(AAMyIDSecurityEnvHandler.class);

	public AAMyIDSecurityEnvHandler() throws Exception {


		String keyStoreAbsPath = PlatformConstants.getConfigRootAbsPath() + File.separatorChar + WebServicesModuleUtils.getMyIDTravelConfig().getSecurityConfigMap().get(MyIDTravelConstants.KEYSTORE_FILE_REL_PATH).toString();
		String keyStoreType  =   WebServicesModuleUtils.getMyIDTravelConfig().getSecurityConfigMap().get(MyIDTravelConstants.KEYSTORE_TYPE).toString();
		String keyStorePassword =  WebServicesModuleUtils.getMyIDTravelConfig().getSecurityConfigMap().get(MyIDTravelConstants.KEYSTORE_PASSWORD).toString();

		this.keyStoreURL = keyStoreAbsPath;
		this.keyStoreType = keyStoreType;
		this.keyStorePassword = keyStorePassword;

		this.trustStoreURL = keyStoreAbsPath;
		this.trustStoreType = keyStoreType;
		this.trustStorePassword = keyStorePassword;

	}

	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {

		for (int i=0; i < callbacks.length; i++) {            

			if (callbacks[i] instanceof SignatureKeyCallback) {
				SignatureKeyCallback cb = (SignatureKeyCallback)callbacks[i];               
				if (cb.getRequest() instanceof SignatureKeyCallback.AliasPrivKeyCertRequest) {
					SignatureKeyCallback.AliasPrivKeyCertRequest request =
							(SignatureKeyCallback.AliasPrivKeyCertRequest) cb.getRequest();
					String alias = request.getAlias();
					if (keyStore == null)
						initKeyStore();
					try {
						X509Certificate cert =
								(X509Certificate) keyStore.getCertificate(alias);
						request.setX509Certificate(cert);
						// Assuming key passwords same as the keystore password
						PrivateKey privKey =
						(PrivateKey) keyStore.getKey(alias, keyStorePassword.toCharArray());
						request.setPrivateKey(privKey);
					} catch (Exception e) {
						log.error(e);
						throw new IOException(e.getMessage());
					}

				} else {
					throw new UnsupportedCallbackException(null, "Unsupported Callback Type Encountered");
				}

			} else if (callbacks[i] instanceof DecryptionKeyCallback) {
				DecryptionKeyCallback cb = (DecryptionKeyCallback)callbacks[i];

				if (cb.getRequest() instanceof  DecryptionKeyCallback.X509SubjectKeyIdentifierBasedRequest) {
					DecryptionKeyCallback.X509SubjectKeyIdentifierBasedRequest request =
							(DecryptionKeyCallback.X509SubjectKeyIdentifierBasedRequest) cb.getRequest();
					byte[] ski = request.getSubjectKeyIdentifier();
					if (keyStore == null)
						initKeyStore();
					PrivateKey privKey = getPrivateKey(ski);
					request.setPrivateKey(privKey);                   
				} else  {
					throw new UnsupportedCallbackException(null, "Unsupported Callback Type Encountered");
				}

			} else if (callbacks[i] instanceof EncryptionKeyCallback) {
				EncryptionKeyCallback cb = (EncryptionKeyCallback)callbacks[i];

				if (cb.getRequest() instanceof EncryptionKeyCallback.AliasX509CertificateRequest) {
					EncryptionKeyCallback.AliasX509CertificateRequest request =
							(EncryptionKeyCallback.AliasX509CertificateRequest) cb.getRequest();
					if (trustStore == null)
						initTrustStore();
					String alias = request.getAlias();
					try {
						X509Certificate cert =
								(X509Certificate) trustStore.getCertificate(alias);
						request.setX509Certificate(cert);
					} catch (Exception e) {
						log.error(e);
						throw new IOException(e.getMessage());
					}        
				} else {
					throw new UnsupportedCallbackException(null, "Unsupported Callback Type Encountered");
				}

			} else if (callbacks[i] instanceof CertificateValidationCallback) {
				CertificateValidationCallback cb = (CertificateValidationCallback)callbacks[i];
				cb.setValidator(new X509CertificateValidatorImpl());

			} else {
				throw new UnsupportedCallbackException(null, "Unsupported Callback Type Encountered");
			}
		}
	}


	private void initTrustStore() throws IOException {
		try {
			trustStore = KeyStore.getInstance(trustStoreType);
			trustStore.load(new FileInputStream(trustStoreURL), trustStorePassword.toCharArray());
		} catch (Exception e) {
			log.error(e);
			throw new IOException(e.getMessage());
		}
	}

	private void initKeyStore() throws IOException {
		try {
			keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(new FileInputStream(keyStoreURL), keyStorePassword.toCharArray());
		} catch (Exception e) {
			log.error(e);
			throw new IOException(e.getMessage());
		}
	}

	public PrivateKey getPrivateKey(byte[] ski) throws IOException {

		try {
			Enumeration aliases = keyStore.aliases();
			while (aliases.hasMoreElements()) {
				String alias = (String) aliases.nextElement();
				if (!keyStore.isKeyEntry(alias))
					continue;
				Certificate cert = keyStore.getCertificate(alias);
				if (cert == null || !"X.509".equals(cert.getType())) {
					continue;
				}
				X509Certificate x509Cert = (X509Certificate) cert;
				byte[] keyId = getSubjectKeyIdentifier(x509Cert);
				if (keyId == null) {
					// Cert does not contain a key identifier
					continue;
				}
				if (Arrays.equals(ski, keyId)) {
					// Asuumed key password same as the keystore password
					return (PrivateKey) keyStore.getKey(alias, keyStorePassword.toCharArray());
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new IOException(e.getMessage());
		}
		return null;
	}

	private static byte[] getSubjectKeyIdentifier(X509Certificate cert) {
		String SUBJECT_KEY_IDENTIFIER_OID = "2.5.29.14";
		byte[] subjectKeyIdentifier =
				cert.getExtensionValue(SUBJECT_KEY_IDENTIFIER_OID);
		if (subjectKeyIdentifier == null)
			return null;
		byte[] dest = new byte[subjectKeyIdentifier.length - 4];
		System.arraycopy(
				subjectKeyIdentifier, 4, dest, 0, subjectKeyIdentifier.length - 4);
		return dest;
	}


	private class X509CertificateValidatorImpl implements CertificateValidationCallback.CertificateValidator {

		public boolean validate(X509Certificate certificate)
				throws CertificateValidationCallback.CertificateValidationException {

			if (isSelfCert(certificate)) {
				return true;
			}

			try {
				certificate.checkValidity();
			} catch (CertificateExpiredException e) {
				log.error(e);
				throw new CertificateValidationCallback.CertificateValidationException("X509Certificate Expired", e);
			} catch (CertificateNotYetValidException e) {
				log.error(e);
				throw new CertificateValidationCallback.CertificateValidationException("X509Certificate not yet valid", e);
			}

//			X509CertSelector certSelector = new X509CertSelector();
//			certSelector.setCertificate(certificate);
//
//			PKIXBuilderParameters parameters;
//			CertPathBuilder builder;
//			try {
//				if (trustStore == null)
//					initTrustStore();
//				parameters = new PKIXBuilderParameters(trustStore, certSelector);
//				parameters.setRevocationEnabled(false);
//				builder = CertPathBuilder.getInstance("PKIX", "BC");
//			} catch (Exception e) {
//				log.error(e);
//				throw new CertificateValidationCallback.CertificateValidationException(e.getMessage(), e);
//			}
//			
//			if (log.isDebugEnabled()) {
//				log.debug("Certifcate Content:\n" + certificate.toString() + "\n");
//				log.debug("===================================================================================");
//				log.debug("CertPath Builder Parameters:\n" + parameters.toString());
//			}
//
//			try {
//				PKIXCertPathBuilderResult result =
//						(PKIXCertPathBuilderResult) builder.build(parameters);
//			} catch (Exception e) {
//				log.error(e);
//				return false;
//			}
			return true;
		}

		private boolean isSelfCert(X509Certificate cert)
				throws CertificateValidationCallback.CertificateValidationException {
			try {
				if (keyStore == null)
					initKeyStore();
				Enumeration aliases = keyStore.aliases();
				while (aliases.hasMoreElements()) {
					String alias = (String) aliases.nextElement();
					if (keyStore.isKeyEntry(alias)) {
						X509Certificate x509Cert =
								(X509Certificate) keyStore.getCertificate(alias);
						if (x509Cert != null) {
							if (x509Cert.equals(cert))
								return true;
						}
					}
				}
				return false;
			} catch (Exception e) {
				log.error(e);
				throw new CertificateValidationCallback.CertificateValidationException(e.getMessage(), e);
			}
		}
	}
}
