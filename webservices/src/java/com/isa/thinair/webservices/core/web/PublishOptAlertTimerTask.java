package com.isa.thinair.webservices.core.web;

import java.util.Date;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.wsclient.api.service.WSClientBD;

/**
 * Timer task for publishing inventory data to Revenue Management module for optimization.
 * 
 * @author Nasly
 */
public class PublishOptAlertTimerTask extends TimerTask {

	private static Log log = LogFactory.getLog(PublishOptAlertTimerTask.class);

	@Override
	public void run() {
		log.info("Start publish inventory for optimization job [timestamp="
				+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date()) + "]");
		WSClientBD wsClientBD = (WSClientBD) WebServicesModuleUtils.getWSClientBD();
		try {
			wsClientBD.publishRMInventoryAlerts();
		} catch (Throwable t) {
			log.error("Publishing inventory for optimization job failed", t);
		}
		log.info("End publish inventory for optimization job [timestamp="
				+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date()) + "]");
	}

}
