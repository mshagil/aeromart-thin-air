package com.isa.thinair.webservices.core.cache.store.impl;

import com.isa.thinair.webservices.core.cache.store.AbstractStoreFactory;
import com.isa.thinair.webservices.core.cache.store.TransactionStore;

/**
 * 
 * Factory to get the the TrasnsactionStore based on the configuration
 *
 */
public class TransactionStoreFactory extends AbstractStoreFactory<TransactionStore> {

}
