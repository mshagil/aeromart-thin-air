package com.isa.thinair.webservices.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amadeus.ama._2008._03.AMATLAGetAvailabilityAndPriceRQ;
import com.amadeus.ama._2008._03.AMATLAGetAvailabilityAndPriceRS;
import com.amadeus.ama._2008._03.ErrorWarningType;
import com.amadeus.ama._2008._03.SourceType;
import com.amadeus.ama._2008._03.SuccessType;
import com.amadeus.ama._2008._03.TLAAirportLocationType;
import com.amadeus.ama._2008._03.TLAAvailabilityType;
import com.amadeus.ama._2008._03.TLABookingClassDataType;
import com.amadeus.ama._2008._03.TLAErrorType;
import com.amadeus.ama._2008._03.TLAJourneyTypeRS;
import com.amadeus.ama._2008._03.TLAOperatingAirlineType;
import com.amadeus.ama._2008._03.TLAOriginDestinationInformationTypeA;
import com.amadeus.ama._2008._03.TLAOriginDestinationTypeRS;
import com.amadeus.ama._2008._03.TLAOriginDestinationsTypeRS;
import com.amadeus.ama._2008._03.TLASegmentTypeA;
import com.amadeus.ama._2008._03.TLAUseCaseType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.IAvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.AmadeusWSException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusAvailabilitySearchBL {

	private final static Log log = LogFactory.getLog(AmadeusAvailabilitySearchBL.class);

	public static AMATLAGetAvailabilityAndPriceRS execute(AMATLAGetAvailabilityAndPriceRQ amaTLAGetAvailabilityAndPriceRQ) {
		AMATLAGetAvailabilityAndPriceRS availRS = new AMATLAGetAvailabilityAndPriceRS();
		availRS.setRelease(WebservicesConstants.AmadeusConstants.ReleaseAvailPriceRS);
		availRS.setVersion(WebservicesConstants.AmadeusConstants.VersionAvailPriceRS);
		try {
			AvailableFlightSearchDTO searchDTO = composeAvailSearchDTO(amaTLAGetAvailabilityAndPriceRQ);
			AvailableFlightDTO availableFlightDTO = WebServicesModuleUtils.getReservationQueryBD()
					.getAvailableFlightsWithAllFares(searchDTO, null);
			transform(availableFlightDTO, availRS, amaTLAGetAvailabilityAndPriceRQ);
		} catch (ModuleException e) {
			log.error("Error occured", e);
			TLAErrorType error = new TLAErrorType();
			if (e instanceof AmadeusWSException) {
				AmadeusWSException ae = (AmadeusWSException) e;
				error.setCode(ae.getErrorCode());
				error.setType(ae.getErrorWarningType());
				// TODO fetch the descriptive message from resource bundle
				if (ae.getDescription() != null && !"".equals(ae.getDescription())) {
					error.setValue(ae.getDescription());
				}
			} else {
				error.setCode(Integer.valueOf(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_RETRY.getErrorCode()));
				error.setValue(e.getMessage());
				error.setType(ErrorWarningType.UNKNOWN);
			}

			availRS.setError(error);
		}
		return availRS;
	}

	/**
	 * Transform all the flights returned in search to Amadeus Response
	 * 
	 * @param availableFlightDTO
	 * @param availRS
	 * @param availRQ
	 * @throws ModuleException
	 */
	private static void transform(AvailableFlightDTO availableFlightDTO, AMATLAGetAvailabilityAndPriceRS availRS,
			AMATLAGetAvailabilityAndPriceRQ availRQ) throws ModuleException {
		String rqPaxType = availRQ.getPaxType();
		String paxType = PaxTypeTO.ADULT;
		if (AmadeusConstants.ADULT.equals(rqPaxType)) {
			paxType = PaxTypeTO.ADULT;
		} else if (AmadeusConstants.CHILD.equals(rqPaxType)) {
			paxType = PaxTypeTO.CHILD;
		} else if (AmadeusConstants.INFANT.equals(rqPaxType)) {
			paxType = PaxTypeTO.INFANT;
		}

		String originISOCountryCode = null;
		if (availRQ.getPOS() != null) {
			if (availRQ.getPOS().getSource() != null && availRQ.getPOS().getSource().size() > 0) {
				SourceType pos = availRQ.getPOS().getSource().get(0);
				originISOCountryCode = pos.getISOCountry();
			}
		}

		String currencyCode = AmadeusCommonUtil.getPGWCurrencyCode(availRQ.getDisplayCurrencyCode(), originISOCountryCode);
		CurrencyExchangeRate currencyExgRate = AmadeusCommonUtil.getCurrencyExchangeRate(currencyCode);

		TLAOriginDestinationsTypeRS ondType = new TLAOriginDestinationsTypeRS();
		TLAOriginDestinationTypeRS outOndList = getOndList(availableFlightDTO.getAvailableOndFlights(OndSequence.OUT_BOUND),
				currencyExgRate, paxType);
		outOndList.setOriginDestinationInfo(availRQ.getOutboundFlightSearch());
		ondType.getOutboundFlight().add(outOndList);

		if (availRQ.getUseCase() == TLAUseCaseType.ROUND_TRIP) {
			TLAOriginDestinationTypeRS inOndList = getOndList(availableFlightDTO.getAvailableOndFlights(OndSequence.IN_BOUND),
					currencyExgRate, paxType);
			inOndList.setOriginDestinationInfo(availRQ.getReturnFlightSearch());
			ondType.getReturnFlight().add(inOndList);
		}

		availRS.setDisplayCurrencyCode(currencyCode);
		availRS.setOriginDestinations(ondType);
		availRS.setSuccess(new SuccessType());
	}

	/**
	 * Compose the availability search request based on the amadeus request
	 * 
	 * @param amaTLAGetAvailabilityAndPriceRQ
	 * @return
	 * @throws ModuleException
	 */
	private static AvailableFlightSearchDTO
			composeAvailSearchDTO(AMATLAGetAvailabilityAndPriceRQ amaTLAGetAvailabilityAndPriceRQ) throws ModuleException {

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		Integer departureVariance = null;
		Integer returnVariance = null;
		String bookingType = BookingClass.BookingClassType.NORMAL;
		if (principal == null) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_PERMISSION_DENIED, ErrorWarningType.AUTHENTICATION,
					"Authentication failed");
		}
		
		OriginDestinationInfoDTO outboundOndInfo = new OriginDestinationInfoDTO();

		TLAOriginDestinationInformationTypeA obFltSearch = amaTLAGetAvailabilityAndPriceRQ.getOutboundFlightSearch();
		
		String fromAirport = obFltSearch.getOriginLocation().getLocationCode();
		String toAirport = obFltSearch.getDestinationLocation().getLocationCode();
		AmadeusValidationUtil.validateAirports(fromAirport, toAirport);
		outboundOndInfo.setOrigin(fromAirport);
		outboundOndInfo.setDestination(toAirport);
		
		ZuluLocalTimeConversionHelper zuluHelper = new ZuluLocalTimeConversionHelper(WebServicesModuleUtils.getAirportBD());
		
		
		try {
			Date zuluDepatureDate = zuluHelper.getZuluDateTime(fromAirport,
					CommonUtil.parseDateTime(obFltSearch.getDepartureDateTime().getValue()));
			AmadeusValidationUtil.validateDepDate(zuluDepatureDate);
		} catch (AmadeusWSException ex) {
			throw new AmadeusWSException(AmadeusErrorCodes.INVALID_DEPARTURE_DATE_FOR_SEGMENT, ErrorWarningType.BIZ_RULE,
					"Invalid departure date for segment " + fromAirport + "-"
							+ toAirport);
		}
		
		Date outboundDepartureDate = CommonUtil.parseDate(obFltSearch.getDepartureDateTime().getValue()).getTime();
		outboundOndInfo.setDepartureDateTimeStart(outboundDepartureDate);
		
		if (obFltSearch.getDepartureDateTime().getWindowBefore() != null
				|| obFltSearch.getDepartureDateTime().getWindowAfter() != null) {
			int windowBeforeOut = obFltSearch.getDepartureDateTime().getWindowBefore() != null ? obFltSearch
					.getDepartureDateTime().getWindowBefore().getDays() : 0;
			int windowAfterOut = obFltSearch.getDepartureDateTime().getWindowAfter() != null ? obFltSearch.getDepartureDateTime()
					.getWindowAfter().getDays() : 0;
			departureVariance = Math.max(windowBeforeOut, windowAfterOut);
		}
		
		Date preferredDepatureDateTimeStart = CalendarUtil.getStartTimeOfDate(outboundDepartureDate);
		Date depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(preferredDepatureDateTimeStart,
				departureVariance != null ? Math.min(3, departureVariance.intValue()) * -1 : 0);
		Date preferredDepatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(outboundDepartureDate);
		Date depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(preferredDepatureDateTimeEnd,
				departureVariance != null ? Math.min(3, departureVariance.intValue()) : 0);
		String cabinClass = "Y";
		if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
			cabinClass = AppSysParamsUtil.getDefaultCOS();
		}
		
		outboundOndInfo.setPreferredDateTimeStart(preferredDepatureDateTimeStart);
		outboundOndInfo.setPreferredDateTimeEnd(preferredDepatureDateTimeEnd);
		outboundOndInfo.setDepartureDateTimeStart(depatureDateTimeStart);
		outboundOndInfo.setDepartureDateTimeEnd(depatureDateTimeEnd);
		outboundOndInfo.setPreferredBookingType(bookingType);
		outboundOndInfo.setPreferredClassOfService(cabinClass);
		
		availableFlightSearchDTO.addOriginDestination(outboundOndInfo);
		
		
		if (amaTLAGetAvailabilityAndPriceRQ.getReturnFlightSearch() != null) {
			TLAOriginDestinationInformationTypeA ibFltSearch = amaTLAGetAvailabilityAndPriceRQ.getReturnFlightSearch();
			
			OriginDestinationInfoDTO inboundOndInfo = new OriginDestinationInfoDTO();
			inboundOndInfo.setOrigin(outboundOndInfo.getDestination());
			inboundOndInfo.setDestination(outboundOndInfo.getOrigin());
			
			Date inboundDepartureDate = CommonUtil.parseDate(ibFltSearch.getArrivalDateTime().getValue()).getTime();
			
			if (ibFltSearch.getArrivalDateTime().getWindowBefore() != null
					|| ibFltSearch.getArrivalDateTime().getWindowAfter() != null) {
				int windowBeforeIn = ibFltSearch.getArrivalDateTime().getWindowBefore() != null ? ibFltSearch
						.getArrivalDateTime().getWindowBefore().getDays() : 0;
				int windowAfterIn = ibFltSearch.getArrivalDateTime().getWindowAfter() != null ? ibFltSearch.getArrivalDateTime()
						.getWindowAfter().getDays() : 0;
				returnVariance = Math.max(windowBeforeIn, windowAfterIn);
			}
			
			Date preferredReturnDateTimeStart = CalendarUtil.getStartTimeOfDate(inboundDepartureDate);
			Date returnDateTimeStart = CalendarUtil.getOfssetAddedTime(preferredReturnDateTimeStart, returnVariance != null
					? Math.min(3, returnVariance.intValue()) * -1
					: 0);

			Date preferredReturnDateTimeEnd = CalendarUtil.getEndTimeOfDate(inboundDepartureDate);
			Date returnDateTimeEnd = CalendarUtil.getOfssetAddedTime(preferredReturnDateTimeEnd,
					returnVariance != null ? Math.min(3, returnVariance.intValue()) : 0);
			
			inboundOndInfo.setPreferredDateTimeStart(preferredReturnDateTimeStart);
			inboundOndInfo.setPreferredDateTimeEnd(preferredReturnDateTimeEnd);
			inboundOndInfo.setDepartureDateTimeStart(returnDateTimeStart);
			inboundOndInfo.setDepartureDateTimeEnd(returnDateTimeEnd);
			inboundOndInfo.setPreferredBookingType(bookingType);
			inboundOndInfo.setPreferredClassOfService(cabinClass);
			
			availableFlightSearchDTO.addOriginDestination(inboundOndInfo);
			
			boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
			if (!availableFlightSearchDTO.isOpenReturnSearch()) {
				availableFlightSearchDTO.setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);
			}
		}
		
		int adultCount = 0;
		int childCount = 0;
		int infantCount = 0;
		if (amaTLAGetAvailabilityAndPriceRQ.getPaxType() == null
				|| amaTLAGetAvailabilityAndPriceRQ.getPaxType().equals(AmadeusConstants.ADULT)) {
			adultCount = amaTLAGetAvailabilityAndPriceRQ.getPaxCount();
		} else if (amaTLAGetAvailabilityAndPriceRQ.getPaxType().equals(AmadeusConstants.CHILD)) {
			childCount = amaTLAGetAvailabilityAndPriceRQ.getPaxCount();
		} else if (amaTLAGetAvailabilityAndPriceRQ.getPaxType().equals(AmadeusConstants.INFANT)) {
			infantCount = amaTLAGetAvailabilityAndPriceRQ.getPaxCount();
		}

		AmadeusValidationUtil.validatePaxCounts(adultCount, childCount, infantCount);
		
		availableFlightSearchDTO.setAdultCount(adultCount);
		availableFlightSearchDTO.setChildCount(childCount);
		availableFlightSearchDTO.setInfantCount(infantCount);
		
		availableFlightSearchDTO.setPosAirport(principal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(principal.getAgentCode());
		
		availableFlightSearchDTO.setAppIndicator(ApplicationEngine.WS.toString());
		
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		availableFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
		
		Map<Integer, Boolean> ondFlexiQuote = new HashMap<Integer, Boolean>();
		ondFlexiQuote.put(OndSequence.OUT_BOUND, false);
		ondFlexiQuote.put(OndSequence.IN_BOUND, false);
		
		availableFlightSearchDTO.setOndQuoteFlexi(ondFlexiQuote);
		
		Collection privilegeKeys=ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
	
		int availabilityRestriction = (AuthorizationUtil.hasPrivileges(privilegeKeys, PriviledgeConstants.MAKE_RES_ALLFLIGHTS) || BookingClass.BookingClassType.STANDBY
				.equals(bookingType))
				? AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION
				: AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;

		availableFlightSearchDTO.setAvailabilityRestrictionLevel(availabilityRestriction);
		
		if (amaTLAGetAvailabilityAndPriceRQ.getDisplayCurrencyCode() != null) {
			availableFlightSearchDTO.setPreferredCurrencyCode(amaTLAGetAvailabilityAndPriceRQ.getDisplayCurrencyCode());
		} else {
			availableFlightSearchDTO.setPreferredCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		}
		availableFlightSearchDTO.setQuoteFares(true);
		
		return availableFlightSearchDTO;
	}

	public static TLAOriginDestinationTypeRS getOndList(Collection<AvailableIBOBFlightSegment> onewayAvailableFlightsCol,
			CurrencyExchangeRate currencyExgRate, String paxType) throws ModuleException {

		if (onewayAvailableFlightsCol == null || onewayAvailableFlightsCol.size() == 0) {
			throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE, "No flights found");
		}
		SegmentSequencer ss = new SegmentSequencer();
		TLAOriginDestinationTypeRS ondType = new TLAOriginDestinationTypeRS();
		int invalidBookingCodes = 0;
		int invalidFareCodes = 0;
		int availableFlightsIndex = 0;

		for (IAvailableFlightSegment onewayAvailableFlight : onewayAvailableFlightsCol) {

			availableFlightsIndex++;
			Collection<AvailableFlightSegment> availableFlightSegmentCol = null;

			Map<String, List<TLABookingClassDataType>> segmentFareMap = new HashMap<String, List<TLABookingClassDataType>>();
			Map<String, String> segRPHSeqMap = new HashMap<String, String>();
			int segSeq = 1;
			if (onewayAvailableFlight.isDirectFlight()) {
				AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) onewayAvailableFlight;
				String bookingClassCode = "Y";
				String fareCode = "YOW";

				String segmentRPH = FlightRefNumberUtil.composeFlightRPH(availableFlightSegment.getFlightSegmentDTO());
				segmentFareMap.put(segmentRPH, new ArrayList<TLABookingClassDataType>());
				segRPHSeqMap.put(segmentRPH, (segSeq++) + "");

				String selectedLogicalCabinClass = availableFlightSegment.getSelectedLogicalCabinClass();
				if (selectedLogicalCabinClass != null) {
					BigDecimal minFare = AccelAeroCalculator.parseBigDecimal(availableFlightSegment.getFare(
							selectedLogicalCabinClass).getFareAmount(paxType));
					BigDecimal chargesAndTaxTotal = AccelAeroCalculator.parseBigDecimal(availableFlightSegment.getTotalCharges(
							selectedLogicalCabinClass, paxType));// Charges for Adult

					TLABookingClassDataType bookingClassData = AmadeusCommonUtil.composeBookingClassData(minFare,
							chargesAndTaxTotal, bookingClassCode, fareCode, currencyExgRate);

					segmentFareMap.get(segmentRPH).add(bookingClassData);
				} else {
					// AARESAA-6629 - Error occurs when retrieving fares. But Amadeus is only interested in flights
					// with available fares. Hence we throw "Flights not found" instead of "Fares not available" at all
					// occasions.
					if (onewayAvailableFlightsCol.size() == availableFlightsIndex
							&& (ondType.getJourney() == null || ondType.getJourney().size() == 0)) {
						log.error("FARES NOT AVAILABLE!!!");
						throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE,
								"No flights found");
					} else {
						continue;
					}
				}

				availableFlightSegmentCol = new ArrayList<AvailableFlightSegment>();
				availableFlightSegmentCol.add((AvailableFlightSegment) onewayAvailableFlight);
			} else {
				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) onewayAvailableFlight;

				// Non-fixed fare amount
				BigDecimal minFare = null;
				BigDecimal chargesAndTaxTotal = null;
				String selectedLogicalCabinClass = availableConnectedFlight.getSelectedLogicalCabinClass();
				if (availableConnectedFlight.getFare(selectedLogicalCabinClass) != null) {
					if (onewayAvailableFlightsCol.size() == availableFlightsIndex
							&& (ondType.getJourney() == null || ondType.getJourney().size() == 0)) {
						log.error("FIXME - CONNECTION FARES ARE NOT HANDLED YET!");
						throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE,
								"No flights found");
					} else {
						continue;
					}
					// TODO FIXME when connection fares how to set ???

				} else {
					for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
						selectedLogicalCabinClass = availableFlightSegment.getSelectedLogicalCabinClass();
						if (availableFlightSegment.getFare(selectedLogicalCabinClass) != null) {
							minFare = AccelAeroCalculator.parseBigDecimal(availableFlightSegment.getFare(
									selectedLogicalCabinClass).getFareAmount(paxType));
							chargesAndTaxTotal = AccelAeroCalculator.parseBigDecimal(availableFlightSegment.getTotalCharges(
									selectedLogicalCabinClass, paxType));

							String bookingClassCode = AmadeusCommonUtil.getBookingClassCode(availableFlightSegment
									.getFare(selectedLogicalCabinClass));
							if (bookingClassCode == null) {
								invalidBookingCodes++;
								continue;
							}

							String fareCode = null;
							try {
								fareCode = AmadeusCommonUtil.getFareCode(availableFlightSegment
										.getFare(selectedLogicalCabinClass));
							} catch (Exception e) {
								if (onewayAvailableFlightsCol.size() == availableFlightsIndex
										&& (ondType.getJourney() == null || ondType.getJourney().size() == 0)) {
									log.error("FARES NOT AVAILABLE!!!");
									throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE,
											"No flights found");
								} else {
									break;
								}
							}

							if (fareCode == null) {
								invalidFareCodes++;
								continue;
							}

							TLABookingClassDataType bookingClassData = AmadeusCommonUtil.composeBookingClassData(minFare,
									chargesAndTaxTotal, bookingClassCode, fareCode, currencyExgRate);
							String segmentRPH = FlightRefNumberUtil
									.composeFlightRPH(availableFlightSegment.getFlightSegmentDTO());
							if (!segmentFareMap.containsKey(segmentRPH)) {
								segmentFareMap.put(segmentRPH, new ArrayList<TLABookingClassDataType>());
								segRPHSeqMap.put(segmentRPH, (segSeq++) + "");
							}
							segmentFareMap.get(segmentRPH).add(bookingClassData);
						} else {
							if (onewayAvailableFlightsCol.size() == availableFlightsIndex
									&& (ondType.getJourney() == null || ondType.getJourney().size() == 0)) {
								log.error("FARES NOT AVAILABLE!!!");
								throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE,
										"No flights found");
							} else {
								// We are looking for a connection with segment fares, if at least one segment
								// doesn't have a fare, no point in proceeding, hence aborting!!!
								break;
							}
						}
					}
				}

				availableFlightSegmentCol = availableConnectedFlight.getAvailableFlightSegments();
			}

			Iterator<AvailableFlightSegment> itrAvailSeg = availableFlightSegmentCol.iterator();

			TLAJourneyTypeRS journey = new TLAJourneyTypeRS();
			while (itrAvailSeg.hasNext()) {
				AvailableFlightSegment availSeg = itrAvailSeg.next();
				/*
				 * if any segment doesn't have seats ( no seats or no fare rules ) then seat availability should be
				 * false
				 */

				FlightSegmentDTO segDTO = availSeg.getFlightSegmentDTO();

				TLAAirportLocationType depApLocation = new TLAAirportLocationType();
				String segArr[] = segDTO.getSegmentCode().split("/");
				depApLocation.setLocationCode(segArr[0]);
				TLAAirportLocationType arrApLocation = new TLAAirportLocationType();
				arrApLocation.setLocationCode(segArr[segArr.length - 1]);
				TLAOperatingAirlineType airline = new TLAOperatingAirlineType();
				airline.setCode(segDTO.getCarrierCode());
				airline.setFlightNumber(segDTO.getFlightNumber().substring(segDTO.getCarrierCode().length()));

				XMLGregorianCalendar departureTime = CommonUtil.parse(segDTO.getDepartureDateTime());// segDTO.getDepartureDateTime();
				XMLGregorianCalendar arrivalDateTime = CommonUtil.parse(segDTO.getArrivalDateTime());

				String segmentRPH = FlightRefNumberUtil.composeFlightRPH(segDTO);

				TLASegmentTypeA segment = new TLASegmentTypeA();
				segment.setDepartureAirport(depApLocation);
				segment.setArrivalAirport(arrApLocation);
				segment.setRPH(ss.getSeq(segmentRPH));
				segment.setDepartureDateTime(departureTime);
				segment.setArrivalDateTime(arrivalDateTime);
				segment.setOperatingAirline(airline);

				TLAAvailabilityType availForSegment = new TLAAvailabilityType();

				availForSegment.setFlightSegmentRPH(ss.getSeq(segmentRPH));

				// For each and every booking class this should be available
				List<TLABookingClassDataType> bookingClassDataList = segmentFareMap.get(segmentRPH);
				if (bookingClassDataList == null || bookingClassDataList.size() == 0) {
					if (onewayAvailableFlightsCol.size() == availableFlightsIndex
							&& (ondType.getJourney() == null || ondType.getJourney().size() == 0)) {
						log.error("FARES NOT AVAILABLE!!!");
						throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE,
								"No flights found");
					} else {
						continue;
					}
				}
				availForSegment.getBookingClassData().addAll(bookingClassDataList);

				journey.getSegment().add(segment);
				journey.getAvailabilityForSegment().add(availForSegment);
			}

			ondType.getJourney().add(journey);
		}
		if (invalidBookingCodes == onewayAvailableFlightsCol.size()) {
			throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE, "No flights found");
		}

		if (invalidFareCodes == onewayAvailableFlightsCol.size()) {
			log.error("FARES NOT AVAILABLE!!!");
			throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE, "No flights found");
		}
		return ondType;
	}
}

class SegmentSequencer {
	private int counter = 1;
	private final Map<String, String> seqMap = new HashMap<String, String>();

	public String getSeq(String rph) {
		if (!seqMap.containsKey(rph)) {
			seqMap.put(rph, (counter++) + "");
		}
		return seqMap.get(rph);
	}
}
