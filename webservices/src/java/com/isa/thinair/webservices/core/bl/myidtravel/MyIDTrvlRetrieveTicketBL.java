/**
 * 
 */
package com.isa.thinair.webservices.core.bl.myidtravel;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.OTAAirBookRS;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveTicketRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveTicketResponse;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlRetrieveTicketBL {
	private final static Log log = LogFactory.getLog(MyIDTrvlRetrieveTicketBL.class);

	public static MyIDTravelResAdapterRetrieveTicketResponse execute(
			MyIDTravelResAdapterRetrieveTicketRequest myIDRetTktReq) {

		if (log.isDebugEnabled()) {
			log.debug("BEGIN retrieveTicket(MyIDTravelResAdapterRetrieveTicketRequest)");
		}

		MyIDTravelResAdapterRetrieveTicketResponse myIDRetTktRes = null;
		try {
			myIDRetTktRes = retrieveTicket(myIDRetTktReq);
		} catch (Exception ex) {
			log.error("retrieveTicket(MyIDTravelResAdapterRetrieveTicketRequest) failed.", ex);
			if (myIDRetTktRes == null) {
				myIDRetTktRes = new MyIDTravelResAdapterRetrieveTicketResponse();
				OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
				otaAirBookRS.setEchoToken(myIDRetTktReq.getOTAReadRQ().getEchoToken());
				myIDRetTktRes.setOTAAirBookRS(otaAirBookRS);
			}
			if (myIDRetTktRes.getOTAAirBookRS().getErrors() == null)
				myIDRetTktRes.getOTAAirBookRS().setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDRetTktRes.getOTAAirBookRS().getErrors().getError(),
					myIDRetTktRes.getOTAAirBookRS().getSuccessAndWarningsAndAirReservation(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END retrieveBooking(MyIDTravelResAdapterRetrieveBookingRequest)");
		return myIDRetTktRes;
	}

	private static MyIDTravelResAdapterRetrieveTicketResponse retrieveTicket(
			MyIDTravelResAdapterRetrieveTicketRequest myIDRetTktReq) throws ModuleException, WebservicesException {

		MyIDTravelResAdapterRetrieveTicketResponse myIDRetTktRes = new MyIDTravelResAdapterRetrieveTicketResponse();
		myIDRetTktRes.setEmployeeData(myIDRetTktReq.getEmployeeData());
		myIDRetTktRes.setDateOfIssue(CommonUtil.parse(new Date()));

		OTAAirBookRS otaAirBookRS = MyIDTrvlCommonUtil.generateOTAAirBookRS(myIDRetTktReq.getOTAReadRQ(), true, true,
				false);
		myIDRetTktRes.setOTAAirBookRS(otaAirBookRS);
		return myIDRetTktRes;
	}
}
