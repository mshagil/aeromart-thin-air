/**
 * 
 */
package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ReadRequest;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.UniqueIDType;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.CommonsConstants.WSConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.interlineUtil.TransformInterlineUtil.TransformTO;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AABookingCategoryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * Create booking process.
 * 
 * @author Nasly
 * @author nafly - Modified to support dry/interline bookings
 */
public class BookInterlineUtil extends BaseUtil {

	private static Log log = LogFactory.getLog(BookInterlineUtil.class);
	/**
	 * 
	 * @param otaAirBookRQ
	 * @param aaAirBookRQExt
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public OTAAirBookRS book(OTAAirBookRQ otaAirBookRQ, AAAirBookRQExt aaAirBookRQExt) throws ModuleException,
			WebservicesException, Exception {

		OTAAirBookRS airBookRS = new OTAAirBookRS();
		airBookRS.setErrors(new ErrorsType());

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

		AABookingCategoryType bookingCategoryType = aaAirBookRQExt.getBookingCategoryType();

		String bookingCategoryCode;
		if (bookingCategoryType != null) {
			bookingCategoryCode = aaAirBookRQExt.getBookingCategoryType().getTypeCode();
		} else {
			bookingCategoryCode = BookingCategory.STANDARD.getCode();
			bookingCategoryType = new AABookingCategoryType();
			bookingCategoryType.setTypeCode(bookingCategoryCode);
			aaAirBookRQExt.setBookingCategoryType(bookingCategoryType);

		}

		WSReservationUtil.authorizeBookingCategory(privilegeKeys, bookingCategoryCode, WSConstants.ALLOW_MAKE_RESERVATION);
		
		TransformInterlineUtil transformInterlineUtil= new TransformInterlineUtil();

		CommonReservationAssembler commonReservationAssembler = transformInterlineUtil.createBookRequestParam(otaAirBookRQ,
				aaAirBookRQExt, userPrincipal, privilegeKeys);

		CommonServicesUtil.setDirectBillIdTo(userPrincipal, (otaAirBookRQ != null) ? 
				otaAirBookRQ.getFulfillment():null);
		ServiceResponce responce = WebServicesModuleUtils.getAirproxyReservationBD().makeReservation(
					commonReservationAssembler, CommonServicesUtil.getTrackInfo(userPrincipal));

		LCCClientReservation lccClientReservation = (LCCClientReservation) responce
					.getResponseParam(CommandParamNames.RESERVATION);
		
		log.info("###WSLOGS### CREATE BOOKING COMLETED*** PNR: " + lccClientReservation.getPNR());

		LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(lccClientReservation.getPNR(), lccClientReservation.isGroupPNR(), null,
				false, lccClientReservation.getMarketingCarrier(), null, null);

		// Reconcile dummy carrier reservation if other carrier credit used
		boolean isOtherCarrierCreditUsed = (Boolean) responce.getResponseParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED);
		if (isOtherCarrierCreditUsed && lccClientReservation.isGroupPNR()) {
			WebServicesModuleUtils.getAirproxyReservationBD().reconcileDummyCarrierReservation(pnrModesDTO,
					CommonServicesUtil.getTrackInfo(userPrincipal));
		}

		// Update release timestamp of the dummy booking if not own airline involved reservation
		if (commonReservationAssembler.isOnHoldBooking()
				&& lccClientReservation.getZuluReleaseTimeStamp().compareTo(
						commonReservationAssembler.getPnrZuluReleaseTimeStamp()) != 0) {

			WebServicesModuleUtils.getAirproxyReservationBD().updateReleaseTimeForDummyReservation(lccClientReservation.getPNR(),
					lccClientReservation.getZuluReleaseTimeStamp());
		}

		if (log.isDebugEnabled()) {
			log.debug("OTA Book Request Successful, PNR [" + lccClientReservation.getPNR() + "] isGroupPNR ["
					+ lccClientReservation.isGroupPNR() + "]");
		}

		if (commonReservationAssembler.isOnHoldBooking()
				&& AuthorizationUtil.hasPrivileges(privilegeKeys,
						PrivilegesKeys.WSFuncPrivilegesKeys.WS_EMAIL_ITINERARY_OHDBOOKING)
				|| !commonReservationAssembler.isOnHoldBooking()
				&& AuthorizationUtil.hasPrivileges(privilegeKeys,
						PrivilegesKeys.WSFuncPrivilegesKeys.WS_EMAIL_ITINERARY_CNFBOOKING)) {
			log.info("###WSLOGS### SENDING EMAIL START*** PNR: " + lccClientReservation.getPNR());

			sendEmailItenary(pnrModesDTO, commonReservationAssembler.getLccreservation().getContactInfo(),
					userPrincipal, lccClientReservation.isGroupPNR());
			log.info("###WSLOGS### SENDING EMAIL END*** PNR: " + lccClientReservation.getPNR());
		}

		PriceQuoteInterlineUtil.clearTotalPrice();
		PriceQuoteInterlineUtil.clearPriceQuoteRelatedTransactionParams();
		PriceQuoteInterlineUtil.clearAvailableOndBundledFare();
		log.info("###WSLOGS### CLEARED TRANSACTION PARAMS COMPLETED*** PNR: " + lccClientReservation.getPNR());

		OTAReadRQ readRQ = new OTAReadRQ();
		readRQ.setPOS(new POSType());
		readRQ.getPOS().getSource().add(otaAirBookRQ.getPOS().getSource().get(0));

		readRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		readRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_ResSearch);
		readRQ.setSequenceNmbr(otaAirBookRQ.getSequenceNmbr());
		readRQ.setEchoToken(otaAirBookRQ.getEchoToken());
		readRQ.setTimeStamp(CommonUtil.parse(new Date()));
		readRQ.setTransactionIdentifier(otaAirBookRQ.getTransactionIdentifier());

		AALoadDataOptionsType loadDataOptions = new AALoadDataOptionsType();
		loadDataOptions.setLoadAirItinery(true);
		loadDataOptions.setLoadPriceInfoTotals(true);
		loadDataOptions.setLoadPTCPriceInfo(true);
		loadDataOptions.setLoadFullFilment(true);
		loadDataOptions.setLoadTravelerInfo(true);

		ReadRequest resReadRQ = null;
		readRQ.setReadRequests(new ReadRequests());
		readRQ.getReadRequests().getReadRequest().add(resReadRQ = new ReadRequest());

		resReadRQ.setUniqueID(new UniqueIDType());
		resReadRQ.getUniqueID().setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));
		resReadRQ.getUniqueID().setID(lccClientReservation.getPNR());

		transformInterlineUtil.createAirBookResponse(airBookRS, TransformTO.AirBookRS, readRQ, loadDataOptions, userPrincipal, true);

		return airBookRS;
	}


	/**
	 * 
	 * @param pnrModesDTO
	 * @param contactInfo
	 * @param userPrincipal
	 */
	public static void sendEmailItenary(LCCClientPnrModesDTO pnrModesDTO, CommonReservationContactInfo contactInfo,
			UserPrincipal userPrincipal, boolean isInterlineBooking) {

		if (contactInfo != null && !"".equals(contactInfo.getEmail())) {
			String emailLang = (contactInfo.getPreferredLanguage() == null) ? Locale.ENGLISH.toString() : contactInfo
					.getPreferredLanguage();

			try {
				pnrModesDTO.setPreferredLanguage(emailLang);
				CommonItineraryParamDTO commonItineraryParam = getCommonItineraryParamDTOForItinerary(emailLang,
						userPrincipal.getAgentStation(), isInterlineBooking);
				WebServicesModuleUtils.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
						CommonServicesUtil.getTrackInfo(userPrincipal));
			} catch (Exception e) {
				// Not throwing because, sending email should not affect the
				// booking process, pax should get a valid PNR
				// through
				// the response and he can later request/print the itenary
				log.error("Error while trying to email itenary to the passenger, PNR : " + pnrModesDTO.getPnr());
				e.printStackTrace();
			}
		}
	}
	
	public static String getItineraryContent(LCCClientPnrModesDTO pnrModesDTO, UserPrincipal userPrincipal,
			boolean isInterlineBooking) throws ModuleException {

		CommonItineraryParamDTO commonItineraryParam = getCommonItineraryParamDTOForItinerary(
				pnrModesDTO.getPreferredLanguage(), userPrincipal.getAgentStation(), isInterlineBooking);

		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(pnrModesDTO.getPnr());

		return WebServicesModuleUtils.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO, commonItineraryParam,
				reservationSearchDTO, CommonServicesUtil.getTrackInfo(userPrincipal));

	}
	
	private static CommonItineraryParamDTO getCommonItineraryParamDTOForItinerary(String preferredLang, String station,
			boolean isInterlineBooking) throws ModuleException {
		
		CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
		commonItineraryParam.setItineraryLanguage(preferredLang);
		commonItineraryParam.setIncludePaxFinancials(true);
		commonItineraryParam.setIncludePaymentDetails(true);
		commonItineraryParam.setIncludeTicketCharges(false);
		commonItineraryParam.setStation(station);
		commonItineraryParam.setAppIndicator(ApplicationEngine.WS);
		commonItineraryParam.setIncludeTermsAndConditions(true);
		commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(isInterlineBooking));
		
		return commonItineraryParam;
	}

	/**
	 * Adds the selected external charges in the current transaction context to the given payment assembler of the given
	 * traveler ref number.
	 * 
	 * @param iPayment
	 *            : The payment assembler.
	 * @param travelerRefNo
	 *            : The traveler reference number.
	 */
	public static void addSelectedExternalCharges(LCCClientPaymentAssembler payment, String travelerRefNo) {
		
		Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<? extends LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
		if (quotedExternalCharges != null) {
			for (String travelerRef : quotedExternalCharges.keySet()) {
				if (travelerRef.equals(travelerRefNo)) {
					// Not using generics because it fails to identify the ? extends LCCClientExternalChgDTO
					// This is required because LCCClientFlexiExternalChgDTO is a child of LCCClientExternalChgDTO
					// List<? extends LCCClientExternalChgDTO>
					List externalChgDTOs = quotedExternalCharges.get(travelerRef);
					if (externalChgDTOs != null && externalChgDTOs.size() > 0) {
						payment.addAllExternalCharges(externalChgDTOs);
					}
				}
			}
		}
	}

	/**
	 * @param currency
	 *            - Selected currency
	 * @return The total amount of the external charges in the current transaction context.
	 */
	public static BigDecimal getTotalAmountOfSelectedExternalCharges(String currency, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();

		Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<? extends LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
		if (quotedExternalCharges != null) {
			for (List<? extends LCCClientExternalChgDTO> externalChgDTOs : quotedExternalCharges.values()) {
				for (LCCClientExternalChgDTO externalChgDTO : externalChgDTOs) {
					total = AccelAeroCalculator.add(total, WSReservationUtil.getAmountInSpecifiedCurrency(
							externalChgDTO.getAmount(), currency, exchangeRateProxy));
				}
			}
		}

		return total;
	}

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, String preferredLanguage,
			boolean recordAudit, String marketingAirlineCode, String airlineCode, Long interlineAgreementId) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			pnrModesDTO.setMarketingAirlineCode(marketingAirlineCode);
			pnrModesDTO.setAirlineCode(airlineCode);
		}

		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadLocalTimes(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true); // baggage
		pnrModesDTO.setAppIndicator(ApplicationEngine.WS);
		pnrModesDTO.setPreferredLanguage(preferredLanguage);
		pnrModesDTO.setInterlineAgreementId(interlineAgreementId);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadEtickets(true);

		return pnrModesDTO;

	}
	
	public static void addAutomaticCheckinEmail(LCCClientPaymentAssembler payment, String travelerRefNo,
			String automaticCheckinEmail) {
		Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<? extends LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
		if (quotedExternalCharges != null) {
			for (String travelerRef : quotedExternalCharges.keySet()) {
				if (travelerRef.equals(travelerRefNo)) {
					List<? extends LCCClientExternalChgDTO> externalChgDTOs = quotedExternalCharges.get(travelerRef);
					if (externalChgDTOs != null && externalChgDTOs.size() > 0) {
						for (LCCClientExternalChgDTO clientExternalChgDTO : externalChgDTOs) {
							if (EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.equals(clientExternalChgDTO.getExternalCharges()))
								clientExternalChgDTO.setEmail(automaticCheckinEmail);
						}
					}
				}
			}
		}
	}

}
