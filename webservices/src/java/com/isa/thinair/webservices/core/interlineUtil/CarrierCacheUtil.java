/**
 * 
 */
package com.isa.thinair.webservices.core.interlineUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.SegmentCarrierDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * @author nafly
 * 
 */
public class CarrierCacheUtil {

	private static CarrierCacheUtil carrierCacheUtil = null;

	private static final long CASH_TIMEOUT = AppSysParamsUtil.getMasterDataCacheTimeout();

	private volatile Date recordAge = new Date(System.currentTimeMillis() - CASH_TIMEOUT);

	private final Object lock = new Object();
	private static final Object staticLock = new Object();

	private static Log log = LogFactory.getLog(CarrierCacheUtil.class);

	// Map of <Segment Code , SegmentCarrierDTO which includes the SYSTEM>
	private static Map<String, SegmentCarrierDTO> segmentCarrierList = new HashMap<String, SegmentCarrierDTO>();

	private CarrierCacheUtil() {
		super();
	}

	public static synchronized CarrierCacheUtil getInstance() {
		if (carrierCacheUtil == null) {
			synchronized (staticLock) {
				if (carrierCacheUtil == null) {
					carrierCacheUtil = new CarrierCacheUtil();
				}
			}
		}
		return carrierCacheUtil;
	}

	private boolean addSegmentToCache(String origin, String destination) throws ModuleException {
		boolean isAdded = false;
		if (System.currentTimeMillis() - recordAge.getTime() >= CASH_TIMEOUT) {
			segmentCarrierList.clear();
			recordAge.setTime(System.currentTimeMillis());
		}

		try {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			SegmentCarrierDTO segmentCarrierDTO = WebServicesModuleUtils.getAirportBD().getSegmentCarrierInfo(origin,
					destination, carrierCode);

			if (segmentCarrierDTO != null) {
				isAdded = true;
				segmentCarrierList.put(segmentCarrierDTO.getSegment(), segmentCarrierDTO);

			}
		} catch (CommonsDataAccessException e) {
			log.error("carrier details refresh fails", e);
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "webservice.desc");
		} catch (Exception e) {
			log.error("carrier details refresh fails", e);
			throw new ModuleException("webservice.technical.error", "webservice.desc");
		}

		return isAdded;
	}

	public SegmentCarrierDTO getCarrierDetails(String origin, String destination) throws ModuleException {
		if (origin != null && !"".equals(origin) && destination != null && !"".equals(destination)) {
			synchronized (lock) {

				if (System.currentTimeMillis() - recordAge.getTime() >= CASH_TIMEOUT) {
					segmentCarrierList.clear();
					recordAge.setTime(System.currentTimeMillis());
				}

				String segmentCode = origin.concat("/".concat(destination));
				if (segmentCarrierList.get(segmentCode) != null) {
					return segmentCarrierList.get(segmentCode);
				} else if (addSegmentToCache(origin, destination)) {
					return getCarrierDetails(origin, destination);
				}

			}
		}

		return null;
	}

}
