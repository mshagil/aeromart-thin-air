package com.isa.thinair.webservices.core.bl.myidtravel;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPricingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPricingResponse;

/**
 * 
 * @author Janaka Padukka
 *
 */
public class MyIDTrvlPriceQuoteBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlPriceQuoteBL.class);

	public static MyIDTravelResAdapterPricingResponse execute(MyIDTravelResAdapterPricingRequest myIDPriceReq) {

		if (log.isDebugEnabled()) {
			log.debug("BEGIN getPrice(MyIDTravelResAdapterPricingRequest)");
		}

		MyIDTravelResAdapterPricingResponse myIDPriceRes = null;
		try {
			myIDPriceRes = getPrice(myIDPriceReq);
		} catch (Exception ex) {
			log.error("getPrice(MyIDTravelResAdapterPricingRequest) failed.", ex);
			if (myIDPriceRes == null) {
				myIDPriceRes = new MyIDTravelResAdapterPricingResponse();
				OTAAirPriceRS otaAirPriceRS = new OTAAirPriceRS();
				otaAirPriceRS.setEchoToken(myIDPriceReq.getOTAAirPriceRQ().getEchoToken());
				myIDPriceRes.setOTAAirPriceRS(otaAirPriceRS);
			}
			if (myIDPriceRes.getOTAAirPriceRS().getErrors() == null)
				myIDPriceRes.getOTAAirPriceRS().setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDPriceRes.getOTAAirPriceRS().getErrors().getError(), myIDPriceRes
					.getOTAAirPriceRS().getSuccessAndWarningsAndPricedItineraries(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END getPrice(MyIDTravelResAdapterPricingRequest)");
		return myIDPriceRes;

	}

	private static MyIDTravelResAdapterPricingResponse getPrice(MyIDTravelResAdapterPricingRequest myIDPriceReq)
			throws ModuleException, WebservicesException {

		MyIDTravelResAdapterPricingResponse myIDPriceRes = new MyIDTravelResAdapterPricingResponse();
		OTAAirPriceRS otaAirPriceRS = new OTAAirPriceRS();
		otaAirPriceRS.setErrors(new ErrorsType());
		myIDPriceRes.setOTAAirPriceRS(otaAirPriceRS);

		OTAAirPriceRQ otaAirPriceRQ = myIDPriceReq.getOTAAirPriceRQ();

		if (myIDPriceReq.getBookingReferenceID().getID() != null) {
			Reservation reservation = MyIDTrvlCommonUtil.getReservation(myIDPriceReq.getBookingReferenceID().getID());

			Map<Integer, String> flightRPHMap = getFlightRPHMap(otaAirPriceRQ.getAirItinerary());
			Map<Integer, String> paxRPHMap = getPaxRPHMap(otaAirPriceRQ.getTravelerInfoSummary());

			// Price should include, myidtravelcharge and credit card payment
			// charge both
			AirItineraryPricingInfoType priceInfo = (AirItineraryPricingInfoType) MyIDTrvlCommonUtil.getPricingInfo(
					reservation, flightRPHMap, paxRPHMap, true, null);

			myIDPriceRes.setEmployeeData(myIDPriceReq.getEmployeeData());

			otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries().add(new SuccessType());
			otaAirPriceRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirPrice);
			otaAirPriceRS.setTransactionIdentifier(otaAirPriceRQ.getTransactionIdentifier());
			otaAirPriceRS.setSequenceNmbr(otaAirPriceRQ.getSequenceNmbr());
			otaAirPriceRS.setEchoToken(otaAirPriceRQ.getEchoToken());
			otaAirPriceRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

			PricedItinerariesType pricedItineraries = new PricedItinerariesType();
			otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries().add(pricedItineraries);

			PricedItineraryType pricedItinerary = new PricedItineraryType();
			pricedItineraries.getPricedItinerary().add(pricedItinerary);
			AirItineraryType airItinerary = new AirItineraryType();
			pricedItinerary.setAirItinerary(airItinerary);
			pricedItinerary.setSequenceNumber(new BigInteger("1"));
			pricedItinerary.setAirItinerary(otaAirPriceRQ.getAirItinerary());
			pricedItinerary.setAirItineraryPricingInfo(priceInfo);
		} else {
			// no PNR
		}

		return myIDPriceRes;
	}

	private static Map<Integer, String> getPaxRPHMap(TravelerInfoSummaryType travelerInfoSummary) {
		Map<Integer, String> paxRPHMap = new HashMap<Integer, String>();

		List<TravelerInformationType> travelerInfo = travelerInfoSummary.getAirTravelerAvail();
		for (TravelerInformationType traveler : travelerInfo) {
			paxRPHMap.put(new Integer(traveler.getAirTraveler().getTravelerRefNumber().getRPH()), traveler
					.getAirTraveler().getPassengerTypeCode());
		}

		return paxRPHMap;
	}

	private static Map<Integer, String> getFlightRPHMap(AirItineraryType otaItinerary) throws ModuleException,
			WebservicesException {

		Collection<FlightSegmentDTO> flightSegments = new ArrayList<FlightSegmentDTO>();

		Map<String, String> tmpSegRPHMap = new HashMap<String, String>();
		Map<Integer, String> segRPHMap = new HashMap<Integer, String>();

		for (OriginDestinationOptionType originDestinationOption : otaItinerary.getOriginDestinationOptions()
				.getOriginDestinationOption()) {
			for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
				FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
				aaFlightSegmentDTO.setFromAirport(bookFlightSegment.getDepartureAirport().getLocationCode());
				aaFlightSegmentDTO.setToAirport(bookFlightSegment.getArrivalAirport().getLocationCode());
				aaFlightSegmentDTO.setFlightNumber(bookFlightSegment.getOperatingAirline().getCode()
						+ bookFlightSegment.getOperatingAirline().getFlightNumber());
				aaFlightSegmentDTO.setDepartureDateTime(bookFlightSegment.getDepartureDateTime().toGregorianCalendar()
						.getTime());
				aaFlightSegmentDTO.setArrivalDateTime(bookFlightSegment.getArrivalDateTime().toGregorianCalendar()
						.getTime());
				flightSegments.add(aaFlightSegmentDTO);
				tmpSegRPHMap.put(MyIDTrvlCommonUtil.getTmpSegRefNo(aaFlightSegmentDTO), bookFlightSegment.getRPH());
			}
		}

		Collection<FlightSegmentDTO> filledFlightSegments = WebServicesModuleUtils.getFlightBD()
				.getFilledFlightSegmentDTOs(flightSegments);

		if (filledFlightSegments.size() != flightSegments.size()) {
			log.error("Segment sell and price quote segments are not maching");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"[Invalid segments in price quotes]");
		}

		for (FlightSegmentDTO flightSegment : filledFlightSegments) {
			String rph = tmpSegRPHMap.get(MyIDTrvlCommonUtil.getTmpSegRefNo(flightSegment));
			segRPHMap.put(flightSegment.getSegmentId(), rph);
		}

		return segRPHMap;
	}
}