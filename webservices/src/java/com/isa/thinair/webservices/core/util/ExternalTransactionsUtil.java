package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.OTAAirBookRS;

import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;

public class ExternalTransactionsUtil extends BaseUtil {

	/**
	 * Records balance query request.
	 * 
	 * @param otaAirBookRS
	 * @param balanceToPay
	 * @return
	 * @throws ModuleException
	 */
	public ExternalPaymentTnx initializeExtTransaction(OTAAirBookRS otaAirBookRS, BigDecimal balanceToPay, String channel)
			throws ModuleException {
		ExternalPaymentTnx externalPaymentTnx = new ExternalPaymentTnx();

		AirReservationType airReservation = null;
		Iterator it = otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();
		while (it.hasNext()) {
			Object o = it.next();
			if (o instanceof AirReservationType) {
				airReservation = (AirReservationType) o;
				break;
			}
		}

		String pnr = airReservation.getBookingReferenceID().get(0).getID();

		externalPaymentTnx.setBalanceQueryKey(generateBalanceQueryKey(pnr));
		externalPaymentTnx.setPnr(pnr);
		externalPaymentTnx.setAmount(balanceToPay);
		externalPaymentTnx.setStatus(ReservationInternalConstants.ExtPayTxStatus.INITIATED);
		externalPaymentTnx.setExternalPayStatus(ReservationInternalConstants.ExtPayTxStatus.UNKNOWN);
		externalPaymentTnx.setUserId(ThreadLocalData.getCurrentUserPrincipal().getUserId());
		externalPaymentTnx.setChannel(channel);
		externalPaymentTnx.setInternalTnxStartTimestamp(new Date());
		externalPaymentTnx.setReconcilationStatus(ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED);
		externalPaymentTnx.setExternalReqID((String) ThreadLocalData
				.getCurrentTnxParam(WebservicesContext.EXTERNAL_REQ_ID));

		WebServicesModuleUtils.getReservationBD().saveOrUpdateExternalPayTxInfo(externalPaymentTnx);

		return externalPaymentTnx;
	}

	private String generateBalanceQueryKey(String pnr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/ddHHmmssSSS");
		String date[] = StringUtils.split(sdf.format(new Date()), '/');
		int y = Integer.parseInt(date[0]) + 64;
		int m = Integer.parseInt(date[1]) + 64;

		return pnr + (String.valueOf((char) y) + String.valueOf((char) m)) + date[2];

	}

}
