/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirTravelerType.CustLoyalty;
import org.opentravel.ota._2003._05.AirTravelerType.Document;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ.PriceInfo;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType.TravelerRefNumber;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.StringStringMap;
import org.opentravel.ota._2003._05.TravelersFulfillments;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryFoidTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webservices.api.dtos.SSRCustomDTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * Transforms OTA data to AA data.
 * 
 * @author Mehdi
 * @author Nasly
 * 
 */
public class TransformUtil {

	/**
	 * Transform booking request and returns reservation objects ready to pass to back end
	 * 
	 * @author Mehdi
	 * 
	 */
	public static class BookingRequest implements ITransformer {

		private final Log log = LogFactory.getLog(getClass());

		protected OTAAirBookRQ _bookingRequest;
		protected AAAirBookRQExt _bookingRequestExt;

		// Nili August 25, 2011. Now what is this error margin?
		// ----------------------------------------------------
		// This was introduced as a temporary measure, because we provide fare quotes in agent currencies.
		// Our price quote processing is not universal, SelectedFlightDTO and the ondfaredto(s) will produce different
		// results.
		// LCConnect Phase 3 is planned to support LCC connect bookings from OTA, there fore the plan is to fix this
		// permanently with this CR.
		// So for now a temporary fix was done by Manoj to save our souls.
		private static final double ERROR_MARGIN = 0.5;
		private static final double ERROR_MARGIN_PER_PAX_BASE_CURRENCY = 0.01;

		public BookingRequest(OTAAirBookRQ request, AAAirBookRQExt aaAirBookRQExt) {
			_bookingRequest = request;
			_bookingRequestExt = aaAirBookRQExt;
		}

		private OTAAirBookRQ getRequest() {
			return _bookingRequest;
		}

		private AAAirBookRQExt getRequestExt() {
			return _bookingRequestExt;
		}

		/**
		 * @return new Object[]{reservation, dtReleaseTime, isOnholdReservation, splitReservation, listRPH};
		 * @throws ModuleException
		 * @throws WebservicesException
		 */
		public Object[] transform(boolean isInternalCall) throws ModuleException, WebservicesException {

			IReservation reservation = new ReservationAssembler(new ArrayList(), null);
			AAAirBookRQExt airBookRQAAExt = getRequestExt();
			ReservationContactInfo aaContactInfo = null;
			String aaUserNotes = null;
			UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
			Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargeMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();

			String bookingCategory = airBookRQAAExt.getBookingCategoryType().getTypeCode();
			reservation.setBookingCategory(bookingCategory);

			QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
					.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);

			if (airBookRQAAExt != null) {
				if (airBookRQAAExt.getContactInfo() != null) {
					aaContactInfo = ExtensionsUtil.transformToAAContactInfo(airBookRQAAExt.getContactInfo());
				}
				if (airBookRQAAExt.getUserNotes() != null) {
					aaUserNotes = ExtensionsUtil.transformToAAUserNote(airBookRQAAExt.getUserNotes());
				}

			}

			if (aaContactInfo == null) {
				aaContactInfo = prepareContactInfoTravelersInfo(getRequest().getTravelerInfo().getAirTraveler());
			}
			// set reservation contact info
			reservation.addContactInfo(aaUserNotes, aaContactInfo, null);

			int[] paxTypeQuantities = getPaxCounts(getRequest().getTravelerInfo().getAirTraveler());
			PriceQuoteReqParamsExtractUtill priceQuoteReqParamsExtractUtill = new PriceQuoteReqParamsExtractUtill(userPrincipal);
			Collection<AvailableFlightSearchDTO> availableFlightSearchDTOs = this.extractPriceQuoteReqParam(getRequest(),
					priceQuoteReqParamsExtractUtill);
			Collection<OndFareDTO> quotedFaresSegmentsSeatsInfo = null;
			try {
				quotedFaresSegmentsSeatsInfo = priceQuoteReqParamsExtractUtill
						.getSelectedSegsFaresSeatsColForBookingFromFareSegChargeTO(availableFlightSearchDTOs);
			} catch (ModuleException ex) {
				// If inventory is exhausted between the price quote and the book request, a meaningful error message
				// should be
				// propagated. User understands exhaustion of booking classes only by the price difference, therefore
				// error message
				// is shown as below
				if (ex.getExceptionCode().equalsIgnoreCase("airinventory.booking.class.inventory.unavilable")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
							"Quoted price no longer available,Please search again");
				} else {
					throw ex;
				}
			}

			if (quotedFaresSegmentsSeatsInfo != null && quotedFaresSegmentsSeatsInfo.size() > 0) {
				Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
				for (OndFareDTO ondFare : quotedFaresSegmentsSeatsInfo) {
					if (standByBCList.contains(ondFare.getFareSummaryDTO().getBookingClassCode())) {
						reservation.setBookingType(BookingClass.BookingClassType.STANDBY);
					}
					break;
				}
			}

			String currencyCode = null;

			if (OTAUtils.isShowPriceInselectedCurrency()) {
				OTAAirBookRQ.Fulfillment fulfillment = getRequest().getFulfillment();
				if (fulfillment != null && fulfillment.getPaymentDetails() != null) {
					List<PaymentDetailType> paymentDetails = fulfillment.getPaymentDetails().getPaymentDetail();
					for (PaymentDetailType paymentDetail : paymentDetails) {
						currencyCode = paymentDetail.getPaymentAmount().getCurrencyCode();
						break;
					}
				}
			}

			if (currencyCode == null) {
				currencyCode = AppSysParamsUtil.getBaseCurrency();
			}

			// calculate handling fee
			ExternalChgDTO handlingFee = ReservationUtil.getHangleCharges();
			if (handlingFee != null) {
				PaxSet paxSet = new PaxSet(paxTypeQuantities[0], paxTypeQuantities[2], paxTypeQuantities[1]);

				ExternalChargeUtil.calculateExternalChargeAmount(quotedFaresSegmentsSeatsInfo, paxSet, handlingFee,
						OTAUtils.isReturnQuote(quotedFaresSegmentsSeatsInfo));
				externalChargeMap.put(EXTERNAL_CHARGES.HANDLING_CHARGE, handlingFee);
			}

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			BigDecimal totalFareFromPriceBreakdown = priceQuoteReqParamsExtractUtill.getTotalAmountFromPriceBreakdown(
					paxTypeQuantities, currencyCode, quotedFaresSegmentsSeatsInfo, externalChargeMap, exchangeRateProxy);

			// adding selected external charges
			totalFareFromPriceBreakdown = AccelAeroCalculator.add(totalFareFromPriceBreakdown,
					BookUtil.getTotalAmountOfSelectedExternalCharges(currencyCode, exchangeRateProxy));

			List<String> listRPH = new ArrayList<String>();
			HashMap<String, IPayment> travelersPayments = null;
			HashMap<String, Object[]> paxRelationships = preparePaxRelationships(getRequest().getTravelerInfo().getAirTraveler());

			Date holdReleaseTimestamp = null;
			boolean isHoldBooking = false;
			boolean isPartialPayment = false;
			Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		
			// get the par pax wise handling fee for the agent.
			travelersPayments = getHandlerFee(getRequest().getTravelerInfo().getAirTraveler(), paxTypeQuantities, handlingFee);

			if (getRequest().getFulfillment() == null || getRequest().getFulfillment().getPaymentDetails() == null) {
				isHoldBooking = true;
			} else {
				travelersPayments = getTravlersPayments(totalFareFromPriceBreakdown, paxRelationships, travelersPayments,
						paxTypeQuantities, quotedFaresSegmentsSeatsInfo, airBookRQAAExt, currencyCode, exchangeRateProxy);

				for (IPayment travelerPayment : travelersPayments.values()) {
					WSReservationUtil.authorizePayment(privilegeKeys, travelerPayment, userPrincipal.getAgentCode(), null);
				}
				if (travelersPayments.size() != (paxTypeQuantities[0] + paxTypeQuantities[1] + paxTypeQuantities[2])) {
					isPartialPayment = true;
				}
			}

			if (log.isDebugEnabled()) {
				log.debug("################ BOOK REQUEST Onhold ? " + isHoldBooking);
				log.debug("################ BOOK REQUEST partialPay ? " + isPartialPayment);
			}

			if (isHoldBooking || isPartialPayment) {
				holdReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(
						ReleaseTimeUtil.getFlightDepartureInfo(quotedFaresSegmentsSeatsInfo), privilegeKeys, true,
						userPrincipal.getAgentCode(), AppIndicatorEnum.APP_WS.toString(),
						ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(quotedFaresSegmentsSeatsInfo));
			} else {
				holdReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(
						ReleaseTimeUtil.getFlightDepartureInfo(quotedFaresSegmentsSeatsInfo), privilegeKeys, false,
						userPrincipal.getAgentCode(), AppIndicatorEnum.APP_WS.toString(),
						ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(quotedFaresSegmentsSeatsInfo));
			}

			if (holdReleaseTimestamp == null) {
				if (isPartialPayment) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_SPLIT_OPERATION_,
							"Current partially paid reservation is not eligible for hold");
				} else {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_NOT_ELIGIBLE_FOR_HOLD,
							null);
				}
			}

			// Setting the PNR Zulu Release Time Stamp
			reservation.setPnrZuluReleaseTimeStamp(holdReleaseTimestamp);

			Integer nationalityCode = null;
			IPayment payment = null;
			Object[] currentPaxSequences;
			String foidNumber = null;
			String nationalIDNo = "";
			PaxCategoryFoidTO paxCategoryFOIDTO = null;

			String employeeId = "";
			Date dateOfJoin = null;
			String idCategory = "";

			AirTravelerType firstAdultPax = getFirstAdultPax(getRequest().getTravelerInfo().getAirTraveler());

			boolean skipValidation = AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode());

			// Map contains both In-flight services & Airport Services
			Map<String, List<SSRCustomDTO>> paxWiseSpecialServiceMap = SSRDetailsUtil.processPaxWiseSpecialServices(getRequest()
					.getTravelerInfo());

			// Set segment seq SSRCustomDTO by flight segment info
			setSegmentSequenceForSpecialServices(paxWiseSpecialServiceMap, quotedFaresSegmentsSeatsInfo);
			boolean isDomesticSegmentExist = false;
			if (quotedPriceInfoTO != null) {
				isDomesticSegmentExist = OTAUtils.isDomesticSegmentExists(quotedPriceInfoTO.getQuotedSegments());
			} 
			for (AirTravelerType traveler : getRequest().getTravelerInfo().getAirTraveler()) {

				listRPH.add(traveler.getTravelerRefNumber().getRPH());
				currentPaxSequences = paxRelationships.get(traveler.getTravelerRefNumber().getRPH());

				// pax nationalitiy
				nationalityCode = null;
				Nationality nationality = null;

				foidNumber = null;

				// Supports only PSPT and ID
				if (traveler.getDocument().size() > 0) {
					for (Document document : traveler.getDocument()) {

						if (CommonsConstants.SSRCode.PSPT.toString().equals(document.getDocType())) {
							if (document.getDocHolderNationality() != null) {
								nationality = WebServicesModuleUtils.getCommonMasterBD().getNationality(
										document.getDocHolderNationality());
							}

							if (document.getDocID() != null && document.getDocType() != null) {
								foidNumber = document.getDocID();
								String foidType = document.getDocType();
								paxCategoryFOIDTO = OTAUtils
										.getPaxCategoryFOIDFromFOID(foidType, traveler.getPassengerTypeCode());
							}

						} else if (CommonsConstants.SSRCode.ID.toString().equals(document.getDocType())) {
							if (document.getDocID() != null && document.getDocType() != null) {
								employeeId = document.getDocID();
								dateOfJoin = document.getEffectiveDate() != null ? document.getEffectiveDate()
										.toGregorianCalendar().getTime() : null;
								idCategory = document.getDocIssueAuthority();

							}
						}

						if (nationality == null) {
							if (document.getDocHolderNationality() != null) {
								nationality = WebServicesModuleUtils.getCommonMasterBD().getNationality(
										document.getDocHolderNationality());
							}
						}

					}
				}

				if (nationality != null) {
					nationalityCode = nationality.getNationalityCode();
				} else {// only enforce the nationality code validation for agents with extra validation
					if (!skipValidation) {
						// nationality is only required for the first adult.
						if (firstAdultPax.getTravelerRefNumber().getRPH().equals(traveler.getTravelerRefNumber().getRPH())) {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"NationalityCode not present");
						}
					}
				}

				// fall back to the previous way of obtaining the foidNumber & paxFOIDId
				if (foidNumber == null) {
					paxCategoryFOIDTO = OTAUtils.getPaxCategoryFOID(WebservicesConstants.DEFAULT_PAX_CATEGORY,
							traveler.getPassengerTypeCode());
					if (airBookRQAAExt.getTravelerAdditionalInfo() != null) {
						foidNumber = OTAUtils.getFOIDNumber(airBookRQAAExt.getTravelerAdditionalInfo().getAAAirTravelersType(),
								traveler.getTravelerRefNumber().getRPH());
					}
				}

				String paxFfid = null;
				// Capture loyalty information
				if (AppSysParamsUtil.isLMSEnabled() && !traveler.getCustLoyalty().isEmpty()) {
					List<CustLoyalty> custLoyalty = traveler.getCustLoyalty();
					paxFfid = custLoyalty.iterator().next().getMembershipID();
				}
				if (airBookRQAAExt.getTravelerAdditionalInfo() != null) {
					nationalIDNo = OTAUtils.getNationalIDNo(airBookRQAAExt.getTravelerAdditionalInfo().getAAAirTravelersType(),
							traveler.getTravelerRefNumber().getRPH());
				}

				PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
				paxAdditionalInfoDTO.setPassportNo(foidNumber);
				paxAdditionalInfoDTO.setDateOfJoin(dateOfJoin);
				paxAdditionalInfoDTO.setEmployeeId(employeeId);
				paxAdditionalInfoDTO.setIdCategory(idCategory);
				paxAdditionalInfoDTO.setFfid(paxFfid);
				paxAdditionalInfoDTO.setNationalIDNo(nationalIDNo);

				// OTAUtils.validateFOIDNumber(paxCategoryFOIDTO, foidNumber);

				// Holds In-flight Services & Airport Services requested by User
				SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();

				if (paxWiseSpecialServiceMap != null) {
					List<SSRCustomDTO> specialServiceList = paxWiseSpecialServiceMap
							.get(traveler.getTravelerRefNumber().getRPH());

					if (specialServiceList != null) {
						for (SSRCustomDTO ssrDTO : specialServiceList) {
							if (ssrDTO != null) {
								segmentSSRs.addPaxSegmentSSR(ssrDTO.getSegmentSequence(), SSRUtil.getSSRId(ssrDTO.getSsrCode()),
										ssrDTO.getText(), null, ssrDTO.getServiceCharge(), ssrDTO.getSsrCode(), null, null,
										ssrDTO.getAirportCode(), ssrDTO.getAirportType(), null, null, null, null);
							}
						}
					}
				}

				// If it's an adult
				if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {

					if (travelersPayments != null) {
						payment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
					} else {
						payment = new PaymentAssembler();// On Hold booking.
					}

					if (isHoldBooking) {
						// adds any selected external charges - etc : flexi/seat/meal for the on-hold booking.
						BookUtil.addSelectedExternalCharges(payment, traveler.getTravelerRefNumber().getRPH());
						ServiceTaxCalculator.calculatePaxJNTax(payment);
					}

					if ((Integer) currentPaxSequences[1] == -1) {// adult traveler

						Date dob = traveler.getBirthDate() != null
								? traveler.getBirthDate().toGregorianCalendar().getTime()
								: null; // birth date

						// Adult validation
						OTAUtils.validatePaxDetails(CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(),
								0), CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0), traveler
								.getPersonName().getSurname(), dob, nationalityCode, null, foidNumber, null, null,
								PaxTypeTO.ADULT, false, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
								null, paxFfid, isDomesticSegmentExist, null, null, null);

						validateAge(dob, PaxTypeTO.ADULT);

						reservation.addSingle(
								CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0), // first
																													// name
								traveler.getPersonName().getSurname(), // last name
								CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0), // title
								dob, // birth date
								nationalityCode, // nationality
								(Integer) currentPaxSequences[0], // pax sequence
								paxAdditionalInfoDTO, null, payment, segmentSSRs, null,
								null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

					} else {// parent (adult with an infant) traveler

						Date dob = traveler.getBirthDate() != null
								? traveler.getBirthDate().toGregorianCalendar().getTime()
								: null; // birth date

						OTAUtils.validatePaxDetails(CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(),
								0), CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0), traveler
								.getPersonName().getSurname(), dob, nationalityCode, null, foidNumber, null, null,
								PaxTypeTO.ADULT, false, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
								null, paxFfid, isDomesticSegmentExist, null, null, null);

						validateAge(dob, PaxTypeTO.ADULT);

						reservation.addParent(
								CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0), // first
																													// name
								traveler.getPersonName().getSurname(), // last name
								CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0), // title
								dob, // birth date
								nationalityCode, // nationality
								(Integer) currentPaxSequences[0], // pax sequence
								(Integer) currentPaxSequences[1],// infant sequece
								paxAdditionalInfoDTO, null, payment, segmentSSRs, null,
								null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
					}

					// If it's a child
				} else if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {

					if (travelersPayments != null) {
						payment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
					} else {
						payment = new PaymentAssembler();// 0 payment
					}

					if (isHoldBooking) {
						// adds any selected external charges - etc : flexi/seat/meal for the on-hold booking.
						BookUtil.addSelectedExternalCharges(payment, traveler.getTravelerRefNumber().getRPH());
						ServiceTaxCalculator.calculatePaxJNTax(payment);
					}

					Date dob = traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null; // birth
																																	// date

					// Child validation
					OTAUtils.validatePaxDetails(CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0),
							CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0), traveler
									.getPersonName().getSurname(), dob, nationalityCode, null, foidNumber, null, null,
							PaxTypeTO.CHILD, false, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory, null,
							paxFfid, isDomesticSegmentExist, null, null, null);

					validateAge(dob, PaxTypeTO.CHILD);

					reservation.addChild(
							CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0), // first
																												// name
							traveler.getPersonName().getSurname(), // last name
							CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0), // title
							dob, // birth date
							nationalityCode, // nationality
							(Integer) currentPaxSequences[0], // pax sequence
							paxAdditionalInfoDTO, null, payment, segmentSSRs, null, null,
							null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

					// If it's an infant
				} else if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {// infant
																										// traveler
					
					if (travelersPayments != null) {
						payment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
					} else {
						payment = new PaymentAssembler();// 0 payment
					}

					Date dob = traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null; // birth
																																	// date

					// Infant validation
					OTAUtils.validatePaxDetails(CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0),
							CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0), traveler
									.getPersonName().getSurname(), dob, nationalityCode, (Integer) currentPaxSequences[1],
							foidNumber, null, null, PaxTypeTO.INFANT, false, skipValidation, employeeId, dateOfJoin, idCategory,
							bookingCategory, null, paxFfid, isDomesticSegmentExist, null, null, null);

					validateAge(dob, PaxTypeTO.INFANT);

					reservation
							.addInfant(
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0), // first
																														// name
									traveler.getPersonName().getSurname(), // last name
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0), // title
									dob, // birth date
									nationalityCode, // nationality
									(Integer) currentPaxSequences[0], // pax sequence
									(Integer) currentPaxSequences[1], // parent sequence
									paxAdditionalInfoDTO, null, payment, segmentSSRs, null,
									null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
				}
			}

			// Set reservation admin info
			SourceType pos = (SourceType) ThreadLocalData.getCurrentWSContext()
					.getParameter(WebservicesContext.REQUEST_POS);
			ReservationAdminInfoTO reservationAdminInfoTO = new ReservationAdminInfoTO();
			reservationAdminInfoTO.setOriginSalesTerminal(pos.getTerminalID());
			reservationAdminInfoTO.setLastSalesTerminal(pos.getTerminalID());
			reservation.setReservationAdminInfoTO(reservationAdminInfoTO);

			// add Insurance if selected in priceQuote
			Object insuranceObject = ThreadLocalData.getCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED);
			if (insuranceObject != null) {
				Integer inusranceRefNo = Integer.parseInt((String) insuranceObject);
				InsuranceServiceUtil.populateInsurance(null, reservation, null, inusranceRefNo, false);
			}

			return new Object[] { reservation, holdReleaseTimestamp, isHoldBooking, isPartialPayment, listRPH,
					quotedFaresSegmentsSeatsInfo, aaContactInfo };
		}

		private void setSegmentSequenceForSpecialServices(Map<String, List<SSRCustomDTO>> paxWiseAirportServiceMap,
				Collection<OndFareDTO> colOndFareDTOs) {
			if (colOndFareDTOs != null) {
				// Get all flight segments
				List<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
				for (OndFareDTO ondFareDTO : colOndFareDTOs) {
					if (ondFareDTO.getSegmentsMap() != null) {
						for (Entry<Integer, FlightSegmentDTO> entry : ondFareDTO.getSegmentsMap().entrySet()) {
							flightSegmentDTOs.add(entry.getValue());
						}
					}
				}

				CommonServicesUtil.sortFlightSegByDepDate(flightSegmentDTOs);

				for (int i = 0; i < flightSegmentDTOs.size(); i++) {

					for (Entry<String, List<SSRCustomDTO>> entry : paxWiseAirportServiceMap.entrySet()) {
						List<SSRCustomDTO> customDTOList = entry.getValue();
						if (customDTOList != null) {
							for (SSRCustomDTO ssrCustomDTO : customDTOList) {
								String flightRPH = BeanUtils.getFirstElement(ssrCustomDTO.getFlightRefNumberRPHList());
								if (flightSegmentDTOs.get(i).getSegmentId().toString().equals(flightRPH.split("-")[0])) {
									ssrCustomDTO.setSegmentSequence(i + 1);
								}
							}
						}
					}

				}

			}
		}

		/**
		 * This method validate the given age(for the arrival datetime) of a traveler with the given age category
		 * 
		 * @param dob
		 *            date of birth
		 * @param passengerType
		 *            AD,CH, or IN
		 * @throws WebservicesException
		 *             if validation fails.
		 */
		private void validateAge(Date dateOfBirth, String passengerType) throws WebservicesException {

			UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

			if (dateOfBirth != null && !AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode())) {
				GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();
				int maxAgeLimitInYears = globalConfig.getPaxTypeMap().get(passengerType).getCutOffAgeInYears();
				Calendar dob = Calendar.getInstance();
				Date startTimeOfDOB = CalendarUtil.getStartTimeOfDate(dateOfBirth);
				dob.setTime(startTimeOfDOB);

				// get the first travel date see AARESAA-2715
				List<Date> departureList = new ArrayList<Date>();
				for (OriginDestinationOptionType originDestinationOptionType : getRequest().getAirItinerary()
						.getOriginDestinationOptions().getOriginDestinationOption()) {
					for (BookFlightSegmentType bookFlightSegmentType : originDestinationOptionType.getFlightSegment()) {
						departureList.add(bookFlightSegmentType.getDepartureDateTime().toGregorianCalendar().getTime());
					}
				}
				Collections.sort(departureList);
				Calendar flyDate = Calendar.getInstance();
				Date startTimeOfFltDate = CalendarUtil.getStartTimeOfDate(departureList.get(0));
				flyDate.setTime(startTimeOfFltDate);

				int diff = getYearDifference((Calendar) dob.clone(), flyDate, maxAgeLimitInYears);
				if (diff > maxAgeLimitInYears) { // validation error for DOB. DOB is not fit to the age category
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, String.format(
							"Invalide dob for %s", passengerType));
				}

				// validate the lower bound for age.
				if (passengerType.equals(PaxTypeTO.ADULT)) {
					int minAgeInMonths = globalConfig.getPaxTypeMap().get(passengerType).getAgeLowerBoundaryInMonths();
					int diffInMonth = getMonthDifference((Calendar) dob.clone(), flyDate);
					if (diffInMonth < minAgeInMonths) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, String.format(
								"Invalide dob for %s", passengerType));
					}
				} else if (passengerType.equals(PaxTypeTO.CHILD)) {
					int minAgeInMonths = globalConfig.getPaxTypeMap().get(passengerType).getAgeLowerBoundaryInMonths();
					int diffInMonth = getMonthDifferenceWithoutBoundry((Calendar) dob.clone(), flyDate);
					if (diffInMonth < minAgeInMonths) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, String.format(
								"Invalide dob for %s", passengerType));
					}

				} else if (passengerType.equals(PaxTypeTO.INFANT)) {
					int minAgeInDays = globalConfig.getPaxTypeMap().get(passengerType).getAgeLowerBoundaryInDays();
					int diffInDays = getDateDifference((Calendar) dob.clone(), flyDate);
					if (diffInDays < minAgeInDays) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, String.format(
								"Invalide dob for %s", passengerType));
					}
				}
			}
		}

		private int getYearDifference(Calendar fromCalendar, Calendar toCalendar, int maxAge) {

			int age = toCalendar.get(Calendar.YEAR) - fromCalendar.get(Calendar.YEAR);

			if (fromCalendar.get(Calendar.MONTH) > toCalendar.get(Calendar.MONTH)
					|| (fromCalendar.get(Calendar.MONTH) == toCalendar.get(Calendar.MONTH) && fromCalendar.get(Calendar.DATE) > toCalendar
							.get(Calendar.DATE))) {
				age--;
			} else if ((age == maxAge)
					&& (fromCalendar.get(Calendar.MONTH) < toCalendar.get(Calendar.MONTH) || (fromCalendar.get(Calendar.MONTH) == toCalendar
							.get(Calendar.MONTH) && fromCalendar.get(Calendar.DATE) < toCalendar.get(Calendar.DATE)))) {
				age++;
			}
			return age;

		}

		private int getMonthDifference(Calendar fromCalendar, Calendar toCalendar) {
			int count = 0;
			for (fromCalendar.add(Calendar.MONTH, 1); fromCalendar.compareTo(toCalendar) <= 0; fromCalendar
					.add(Calendar.MONTH, 1)) {
				count++;
			}
			return count;
		}
		
		private int getMonthDifferenceWithoutBoundry(Calendar fromCalendar, Calendar toCalendar) {
			int count = 0;
			for (fromCalendar.add(Calendar.MONTH, 1); fromCalendar.compareTo(toCalendar) < 0; fromCalendar.add(Calendar.MONTH, 1)) {
				count++;
			}
			return count;
		}

		private int getDateDifference(Calendar fromCalendar, Calendar toCalendar) {
			long diff = toCalendar.getTimeInMillis() - fromCalendar.getTimeInMillis();
			return (int) (diff / (1000 * 60 * 60 * 24) + 1);
		}

		/**
		 * Prepares contact info from the first adult traveler
		 * 
		 * @throws WebservicesException
		 */
		private ReservationContactInfo prepareContactInfoTravelersInfo(List<AirTravelerType> travelers)
				throws WebservicesException {
			ReservationContactInfo reservationContactInfo = new ReservationContactInfo();
			AirTravelerType firstAdultTraveler = getFirstAdultPax(travelers);

			reservationContactInfo.setFirstName(CommonUtil.getValueFromNullSafeList(firstAdultTraveler.getPersonName()
					.getGivenName(), 0));
			reservationContactInfo.setLastName(firstAdultTraveler.getPersonName().getSurname());

			AirTravelerType.Email email = CommonUtil.getValueFromNullSafeList(firstAdultTraveler.getEmail(), 0);
			if (email != null && email.getValue() != null) {
				reservationContactInfo.setEmail(email.getValue());
			}

			AirTravelerType.Address address = CommonUtil.getValueFromNullSafeList(firstAdultTraveler.getAddress(), 0);
			if (address != null) {
				reservationContactInfo.setCountryCode(address.getCountryName().getCode());
				reservationContactInfo.setCity(address.getCityName());

				reservationContactInfo.setStreetAddress1(CommonUtil.getValueFromNullSafeList(address.getAddressLine(), 0));
				reservationContactInfo.setStreetAddress2(CommonUtil.getValueFromNullSafeList(address.getAddressLine(), 1));
			}

			String telephoneCountryCode = null;
			String telephoneAreaCode = null;
			AirTravelerType.Telephone telephone = CommonUtil.getValueFromNullSafeList(firstAdultTraveler.getTelephone(), 0);
			if (telephone != null) {
				telephoneCountryCode = telephone.getCountryAccessCode();
				telephoneAreaCode = telephone.getAreaCityCode();
				String phoneNumber = telephoneCountryCode != null ? telephoneCountryCode
						+ WebservicesConstants.PHONE_NUMBER_SEPERATOR : "";
				if (telephone.getAreaCityCode() != null) {
					phoneNumber += telephoneAreaCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR;
				}
				phoneNumber += telephone.getPhoneNumber();
				// ExtensionsUtil.validateCountryAccessCode(telephoneCountryCode,telephoneAreaCode,
				// telephone.getPhoneNumber(), reservationContactInfo.getCountryCode());
				reservationContactInfo.setPhoneNo(phoneNumber);
			}
			return reservationContactInfo;
		}

		private Collection<AvailableFlightSearchDTO> extractPriceQuoteReqParam(OTAAirBookRQ otaAirBookRQ,
				PriceQuoteReqParamsExtractUtill utill) throws WebservicesException, ModuleException {
			AirItineraryType itineraryType = otaAirBookRQ.getAirItinerary();
			return utill.extractPriceQuoteReqParams(itineraryType, getPaxCounts(otaAirBookRQ.getTravelerInfo().getAirTraveler()));
		}

		/**
		 * Extracts the traveler with the least RPH ( valid adult/child traveler RPH are of the form Ai where i is a
		 * possitive integer)
		 */
		private AirTravelerType getFirstAdultPax(List<AirTravelerType> travelers) {
			AirTravelerType firstAdultPax = null;
			for (AirTravelerType traveler : travelers) {
				if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))
						|| traveler.getPassengerTypeCode().equals(
								CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					if (firstAdultPax == null
							|| Integer.parseInt(firstAdultPax.getTravelerRefNumber().getRPH().substring(1)) > Integer
									.parseInt(traveler.getTravelerRefNumber().getRPH().substring(1))) {
						firstAdultPax = traveler;
					}
				}
			}
			return firstAdultPax;
		}

		/**
		 * Extracts the pax wise handling chargers
		 * 
		 * @param travelers
		 * @param paxCounts
		 * @return
		 * @throws ModuleException
		 */
		private HashMap<String, IPayment> getHandlerFee(List<AirTravelerType> travelers, int[] paxCounts,
				ExternalChgDTO handlingFee) throws ModuleException {

			HashMap<String, IPayment> travelersPayments = new HashMap<String, IPayment>();
			int adultAndChildCount = paxCounts[0] + paxCounts[2];
			LinkedList externalChargeSplit = ReservationUtil.getExternalChagersPerPassengers(adultAndChildCount, handlingFee);
			for (AirTravelerType traveler : travelers) {
				IPayment paxPayment = new PaymentAssembler();
				// Handling fee will be added to only adults and children
				if (!traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					paxPayment.addExternalCharges((Collection<ExternalChgDTO>) externalChargeSplit.pop());
				}
				travelersPayments.put(traveler.getTravelerRefNumber().getRPH(), paxPayment);
			}
			return travelersPayments;
		}

		/**
		 * Extracts pax wise payment details from AirBookRQ
		 * 
		 * @param totalFareFromPriceBreakdown
		 * @param paxRelationships
		 * @param travelersPayments
		 * @param paxCounts
		 * @param quotedPricingSegmentsInfo
		 * @param aaBookRQAAExt
		 * @param totalFareFromPriceBreakdownInSelectedCurrency
		 * @return
		 * @throws WebservicesException
		 * @throws ModuleException
		 */
		private HashMap<String, IPayment> getTravlersPayments(BigDecimal totalFareFromPriceBreakdown,
				HashMap<String, Object[]> paxRelationships, HashMap<String, IPayment> travelersPayments, int[] paxCounts,
				Collection<OndFareDTO> quotedPricingSegmentsInfo, AAAirBookRQExt aaBookRQAAExt, String selectedCurrencyCode,
				ExchangeRateProxy exchangeRateProxy) throws WebservicesException, ModuleException {

			OTAAirBookRQ.Fulfillment fulfillment = getRequest().getFulfillment();
			List<AirTravelerType> travelers = getRequest().getTravelerInfo().getAirTraveler();
			TravelersFulfillments travelersFulfillments = getRequest().getTravelersFulfillments();
			PriceInfo priceInfo = getRequest().getPriceInfo();
			AirItineraryType airItineraryType = getRequest().getAirItinerary(); // use to create the CC fraud check REQ
																				// to the eurocommernce.
			if (fulfillment != null && fulfillment.getPaymentDetails() != null) {
				BigDecimal[] expectedPaymentAmounts = ReservationUtil.getQuotedTotalPrices(quotedPricingSegmentsInfo,
						paxCounts[0], paxCounts[2], paxCounts[1]);
				int totalPaxCount = 0;
				for (int loopId = 0; loopId < paxCounts.length; loopId++) {
					totalPaxCount += paxCounts[loopId];
				}

				UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
				PaymentAssembler reservationPayments = new PaymentAssembler();
				OTAUtils.extractReservationPayments(fulfillment.getPaymentDetails().getPaymentDetail(), reservationPayments,
						principal.getAgentCurrencyCode(), exchangeRateProxy);

				// extra validation on total price and price breakdown
				if (!AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(principal.getAgentCode())) {
					// total fare validation from ItinTotalFare
					if (!isTotalPricesEqualFromItin(priceInfo, totalFareFromPriceBreakdown, totalPaxCount)) {
						log.error("TotalFare given in ItinTotalFare does not match with expected Total fair"
								+ " [TotalFare given = " + priceInfo.getItinTotalFare().getTotalFare().getAmount()
								+ ", TotalFare expected = " + totalFareFromPriceBreakdown + "]");
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
								"TotalFare given in ItinTotalFare does not match with expected Total fair");
					}

					// total fare validation from the price breakdown info
					if (!isTotalPricesEqualsFromPriceInfo(priceInfo, totalFareFromPriceBreakdown, totalPaxCount)) {
						log.error("TotalFare calculated from priceInfo does not match with expected Total fair");
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
								"TotalFare calculated from priceInfo does not match with expected Total fair");
					}
					// }
				}

				if (travelersFulfillments == null) {// full payment, single payment type expected
					BigDecimal paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

					if (reservationPayments.getPayments().size() == 1) {
						// FIXME support multiple payment options in one request
						Object resPayment = reservationPayments.getPayments().iterator().next();
						if (resPayment instanceof AgentCreditInfo) {

							if (OTAUtils.isShowPriceInselectedCurrency()) {
								BigDecimal difference = AccelAeroCalculator.subtract(reservationPayments.getTotalPayAmount(),
										totalFareFromPriceBreakdown).abs();
								if (difference.doubleValue() > ERROR_MARGIN) {
									log.error("Payment does not match with the charges " + " [Received payment = "
											+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
											+ totalFareFromPriceBreakdown + "]");
									throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
											"Quoted price no longer available,Please search again.");
								}
							} else {
								// FIXME change the error msg till the PQ issue is solved .
								BigDecimal difference = AccelAeroCalculator.subtract(reservationPayments.getTotalPayAmount(),
										totalFareFromPriceBreakdown).abs();
								if ((reservationPayments.getTotalPayAmount()).compareTo(totalFareFromPriceBreakdown) < 0
										|| ((reservationPayments.getTotalPayAmount()).compareTo(totalFareFromPriceBreakdown) > 0 && difference
												.doubleValue() > (ERROR_MARGIN_PER_PAX_BASE_CURRENCY * totalPaxCount))) {
									log.error("Payment does not match with the charges " + " [Received payment = "
											+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
											+ totalFareFromPriceBreakdown + "]");
									throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
											"Quoted price no longer available,Please search again.");
								}
							}

							for (AirTravelerType traveler : travelers) {
								paxPaymentAmount = getPaxAmount(traveler, expectedPaymentAmounts, paxRelationships);
								IPayment paxPayment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());

								// adding selected external charges
								BookUtil.addSelectedExternalCharges(paxPayment, traveler.getTravelerRefNumber().getRPH());

								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								String agentCode = ((AgentCreditInfo) resPayment).getAgentCode();
								paxPayment.addAgentCreditPayment(agentCode, paxPaymentAmount, null,
										WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, null, null);

							}
						} else if (resPayment instanceof CardPaymentInfo) {
							int totalSementCount = quotedPricingSegmentsInfo.size();
							int totalpayablePaxCount = paxCounts[0] + paxCounts[2];

							BigDecimal CCCharge = WSReservationUtil.getAmountInSpecifiedCurrency(
									ReservationUtil.getCCCharge(totalFareFromPriceBreakdown, totalpayablePaxCount,
											totalSementCount, ChargeRateOperationType.MAKE_ONLY).getAmount(),
									selectedCurrencyCode, exchangeRateProxy);

							CCCharge = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, CCCharge);

							BigDecimal expectedTotalPayment = AccelAeroCalculator.add(totalFareFromPriceBreakdown, CCCharge);

							if (OTAUtils.isShowPriceInselectedCurrency()) {
								BigDecimal difference = AccelAeroCalculator.subtract(reservationPayments.getTotalPayAmount(),
										expectedTotalPayment).abs();
								if (difference.doubleValue() > ERROR_MARGIN) {
									log.error("Payment does not match with the charges " + " [Received payment = "
											+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
											+ expectedTotalPayment + " ]");
									throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
											"Quoted price no longer available,Please search again.");
								}
							} else {
								// FIXME change the error msg till the PQ issue is solved .
								BigDecimal difference = AccelAeroCalculator.subtract(reservationPayments.getTotalPayAmount(),
										expectedTotalPayment).abs();
								if (difference.doubleValue() > ((ERROR_MARGIN_PER_PAX_BASE_CURRENCY * totalPaxCount) + ERROR_MARGIN_PER_PAX_BASE_CURRENCY)) {
									log.error("Payment does not match with the charges " + " [Received payment = "
											+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
											+ expectedTotalPayment + " ]");
									throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
											"Quoted price no longer available,Please search again.");
								}
							}

							// Splitting CC charges to per pax wise to be added to the payment.
							LinkedList externalChargeSplit = ReservationUtil.getExternalChagersPerPassengers(paxCounts[0]
									+ paxCounts[2], ReservationUtil.getCCCharge(totalFareFromPriceBreakdown,
									totalpayablePaxCount, totalSementCount, ChargeRateOperationType.MAKE_ONLY));

							for (AirTravelerType traveler : travelers) {
								paxPaymentAmount = getPaxAmount(traveler, expectedPaymentAmounts, paxRelationships);
								IPayment paxPayment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());

								paxPayment.addExternalCharges((Collection) externalChargeSplit.pop());

								// adding selected external charges
								BookUtil.addSelectedExternalCharges(paxPayment, traveler.getTravelerRefNumber().getRPH());

								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								CardPaymentInfo cardPayInfo = (CardPaymentInfo) resPayment;

								paxPayment.addCardPayment(cardPayInfo.getType(), cardPayInfo.getEDate(), cardPayInfo.getNo(),
										cardPayInfo.getName(), cardPayInfo.getAddress(), cardPayInfo.getSecurityCode(),
										paxPaymentAmount, AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, null,
										cardPayInfo.getIpgIdentificationParamsDTO(), null, cardPayInfo.getPayCurrencyDTO(),
										null, null, null, null);
							}

						} else {
							// TODO implement support for other payment types
							throw new WebservicesException(
									IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
									"Only agent onaccount payment supported");
						}
					} else {
						// TODO implement for accepting multiple payment
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
								"For multiple payments type, pax payment break down should be provided");
					}

				} else {// partial payment or payments from two or more payment types
					String travelerRPH = null;
					String infTravelerRPH = null;
					Object[] paxRelationship = null;
					AirTravelerType traveler = null;
					BigDecimal expectedPaxPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal totalPaxPayments = AccelAeroCalculator.getDefaultBigDecimalZero();
					for (TravelersFulfillments.TravelerFulfillments travelerPayments : travelersFulfillments
							.getTravelerFulfillments()) {
						travelerRPH = travelerPayments.getTravelerRefNumberRPH();
						paxRelationship = paxRelationships.get(travelerRPH);
						traveler = (AirTravelerType) paxRelationship[3];

						PaymentAssembler paxPayment = new PaymentAssembler();

						// adding selected external charges
						BookUtil.addSelectedExternalCharges(paxPayment, travelerRPH);

						ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

						OTAUtils.extractReservationPayments(travelerPayments.getPaymentDetail(), paxPayment,
								principal.getAgentCurrencyCode(), exchangeRateProxy);

						totalPaxPayments = AccelAeroCalculator.add(totalPaxPayments, paxPayment.getTotalPayAmount());
						if (expectedPaxPayment.compareTo(paxPayment.getTotalPayAmount()) != 0) {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
									"Partial payment for a traveler not allowed");
						}

						travelersPayments.put(travelerRPH, paxPayment);
					}

					if (totalPaxPayments.compareTo(reservationPayments.getTotalPayAmount()) != 0) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
								"Traveler wise payment does not match total payment");
					}
				}
			}

			return travelersPayments;
		}

		private BigDecimal getPaxAmount(AirTravelerType traveler, BigDecimal[] expectedPaymentAmounts,
				HashMap<String, Object[]> paxRelationships) {
			BigDecimal paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (traveler.getPassengerTypeCode()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
				// add adult total payment
				paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, expectedPaymentAmounts[1]);
			} else if (traveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
				// add child total payment
				paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, expectedPaymentAmounts[3]);
			} else if (traveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
				// add infant total payment
				paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, expectedPaymentAmounts[2]);
			}
			return paxPaymentAmount;
		}

		/**
		 * @return int [] {adultsCount, infantsCount, children}
		 * @throws WebservicesException
		 */
		private int[] getPaxCounts(List<AirTravelerType> travelers) throws WebservicesException {
			int adults = 0;
			int infants = 0;
			int children = 0;
			for (AirTravelerType airTraveler : travelers) {
				if (airTraveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					++adults;
				} else if (airTraveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					++children;
				} else if (airTraveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					++infants;
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
				}
			}
			return new int[] { adults, infants, children };
		}

		/**
		 * checks if the total fare price from the ItinTotalFare is equal to the expected one,
		 * 
		 * @param priceInfo
		 * @param expectedPaymentAmounts
		 * @return
		 */
		private boolean isTotalPricesEqualFromItin(PriceInfo priceInfo, BigDecimal expectedPaymentAmounts, int totalPaxCount) {
			BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (priceInfo != null && priceInfo.getItinTotalFare() != null) {
				totalTicketPrice = priceInfo.getItinTotalFare().getTotalFare().getAmount();
				if (OTAUtils.isShowPriceInselectedCurrency()) {
					BigDecimal difference = AccelAeroCalculator.subtract(expectedPaymentAmounts, totalTicketPrice).abs();
					return (difference.doubleValue() <= ERROR_MARGIN);
				} else if (totalTicketPrice.compareTo(expectedPaymentAmounts) > 0) {
					BigDecimal difference = AccelAeroCalculator.subtract(expectedPaymentAmounts, totalTicketPrice).abs();
					return (difference.doubleValue() <= (ERROR_MARGIN_PER_PAX_BASE_CURRENCY * totalPaxCount));
				} else {
					return (totalTicketPrice.compareTo(expectedPaymentAmounts) == 0);
				}
			}

			return true; // total price is not present in the ItinTotalFare.
		}

		/**
		 * checks if the total fare price from the price info equals to the expected one
		 * 
		 * @param priceInfo
		 * @param expectedPaymentAmounts
		 * @return
		 */
		private boolean
				isTotalPricesEqualsFromPriceInfo(PriceInfo priceInfo, BigDecimal expectedPaymentAmounts, int totalPaxCount) {
			BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (priceInfo != null && priceInfo.getPTCFareBreakdowns() != null) {
				for (PTCFareBreakdownType breakdownType : priceInfo.getPTCFareBreakdowns().getPTCFareBreakdown()) {
					Integer paxCount = breakdownType.getPassengerTypeQuantity().getQuantity();
					BigDecimal totalFarePerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
					FareType fareType = breakdownType.getPassengerFare();
					if (fareType != null) {
						BigDecimal baseFare = fareType.getBaseFare().getAmount();
						BigDecimal taxFare = AccelAeroCalculator.getDefaultBigDecimalZero();
						BigDecimal fees = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (fareType.getTaxes() != null) {
							for (AirTaxType airTaxType : fareType.getTaxes().getTax()) {
								taxFare = AccelAeroCalculator.add(taxFare, airTaxType.getAmount());
							}
						}
						if (fareType.getFees() != null) {
							for (AirFeeType fee : fareType.getFees().getFee()) {
								fees = AccelAeroCalculator.add(fees, fee.getAmount());
							}
						}
						totalFarePerPax = AccelAeroCalculator.add(baseFare, taxFare, fees);
					}
					totalTicketPrice = AccelAeroCalculator.add(totalTicketPrice,
							AccelAeroCalculator.multiply(totalFarePerPax, paxCount));
				}
				if (OTAUtils.isShowPriceInselectedCurrency()) {
					BigDecimal difference = AccelAeroCalculator.subtract(expectedPaymentAmounts, totalTicketPrice).abs();
					return (difference.doubleValue() <= ERROR_MARGIN);
				} else if (totalTicketPrice.compareTo(expectedPaymentAmounts) > 0) {
					BigDecimal difference = AccelAeroCalculator.subtract(expectedPaymentAmounts, totalTicketPrice).abs();
					return (difference.doubleValue() <= (ERROR_MARGIN_PER_PAX_BASE_CURRENCY * totalPaxCount));
				} else {
					return (totalTicketPrice.compareTo(expectedPaymentAmounts) == 0);
				}
			}
			return true; // price info not present return true.
		}

		/**
		 * Decodes Adult/Infant relationships and Prepares paxSequence Numbers based on travelerRPH Valid travelerRPH
		 * are Ai, Ij/Ai where i,j are mutually exclusive subsets of positive integers
		 * 
		 * @return HashMap of Object [] {paxSequence, infant/parent paxSeqence or -1, infant/parent paxRPH or null,
		 *         airTraveler}
		 */
		private HashMap<String, Object[]> preparePaxRelationships(List<AirTravelerType> airTravelers) throws WebservicesException {
			HashMap<String, Object[]> paxSequencesMap = new HashMap<String, Object[]>();
			int paxSequence;
			String travelerRPH;
			String parentRPH;
			List<Integer> paxSequenceList = new ArrayList<Integer>();// for validating sequence uniqueness
			for (AirTravelerType airTraveler : airTravelers) {
				if (airTraveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))
						|| airTraveler.getPassengerTypeCode().equals(
								CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					travelerRPH = airTraveler.getTravelerRefNumber().getRPH();
					paxSequence = Integer.parseInt(travelerRPH.substring(1));
					if (paxSequenceList.contains(new Integer(paxSequence))) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_VALUE___TRAVELER_RPH,
								"TravelerRPH [" + travelerRPH + "] is in conflict.");
					} else {
						paxSequenceList.add(new Integer(paxSequence));
					}
					paxSequencesMap.put(travelerRPH, new Object[] { paxSequence, -1, null, airTraveler });
				}
			}

			Object[] parentSeqences;
			for (AirTravelerType airTraveler : airTravelers) {
				if (airTraveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					travelerRPH = airTraveler.getTravelerRefNumber().getRPH();
					// infant sequence
					paxSequence = Integer.parseInt(travelerRPH.substring(1, travelerRPH.indexOf('/')));
					if (paxSequenceList.contains(new Integer(paxSequence))) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_VALUE___TRAVELER_RPH,
								"TravelerRPH [" + travelerRPH + "] is in conflict.");
					} else {
						paxSequenceList.add(new Integer(paxSequence));
					}

					parentRPH = travelerRPH.substring(travelerRPH.indexOf('/') + 1);
					parentSeqences = paxSequencesMap.get(parentRPH);
					if (parentSeqences == null) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_PARENT_TRAVELER_NOT_FOUND_,
								"Infant of RPH [" + airTraveler.getTravelerRefNumber().getRPH() + "] is not linked to any adult.");
					}
					parentSeqences[1] = paxSequence;// infant sequence
					parentSeqences[2] = travelerRPH;// infant RPH
					paxSequencesMap.put(parentRPH, parentSeqences);

					paxSequencesMap.put(travelerRPH, new Object[] { paxSequence, parentSeqences[0], parentRPH, airTraveler });
				}
			}
			return paxSequencesMap;
		}

	}// end BookRequest

	public static class ReadRequest extends BaseUtil implements ITransformer {

		public static enum TransformTO {
			AirBookRS, ResRetrieveRS
		}

		private OTAReadRQ _request;
		private TransformTO _transformTo;
		private AALoadDataOptionsType _loadDataOptions;

		public ReadRequest(OTAReadRQ request, AALoadDataOptionsType loadDataOptions, TransformTO transformTo) {
			_request = request;
			_loadDataOptions = loadDataOptions;
			_transformTo = transformTo;
		}

		public OTAReadRQ getRequest() {
			return _request;
		}

		public AALoadDataOptionsType getLoadDataOptions() {
			return _loadDataOptions;
		}

		public Object[] transform(boolean isInternalCall) throws ModuleException, WebservicesException {
			if (_transformTo == TransformTO.AirBookRS) {
				String PNR = getRequest().getReadRequests().getReadRequest().get(0).getUniqueID().getID();
				if (!ReservationApiUtils.isPNRValid(PNR)) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_INVALID_,
							"Booking Reference [" + PNR + "] is invalid");
				}
				return transformToAirBookRS(getLoadDataOptions(), isInternalCall);
			} else
				return transformToResRetrieveRS();
		}

		private Object[] transformToAirBookRS(AALoadDataOptionsType loadDataOptions, boolean isInternalCall)
				throws ModuleException, WebservicesException {
			ReservationQueryUtil resQUtil = new ReservationQueryUtil();

			return new Object[] { resQUtil.getReservation(getRequest(), loadDataOptions, isInternalCall) };
		}

		private Object[] transformToResRetrieveRS() throws ModuleException, WebservicesException {
			ReservationQueryUtil resQUtil = new ReservationQueryUtil();

			return new Object[] { resQUtil.getReservationList(getRequest()) };
		}
	}

	public static TravelerRefNumber getTravelerRefNumber(String rph, String surName) {
		TravelerRefNumber ref = new TravelerRefNumber();
		ref.setRPH(rph);
		ref.setSurnameRefNumber(surName);
		return ref;
	}

	public static Map<String, String> transformStringStringMap(List<StringStringMap> stringStringList) {
		Map<String, String> stringStringMap = new HashMap<String, String>();
		for (StringStringMap stringStringObj : stringStringList) {
			stringStringMap.put(stringStringObj.getKey(), stringStringObj.getValue());
		}
		return stringStringMap;
	}
}
