package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.OTAAirPriceRQ.ServiceTaxCriteriaOptions;

import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxBaseCriteriaDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.api.dtos.OTAPaxCountTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.interlineUtil.PriceQuoteInterlineUtil;
import com.isa.thinair.webservices.core.interlineUtil.PricedItineraryInterlineUtil;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

/**
 * 
 * @author Rikaz
 * 
 */
public class ServiceTaxUtil {

	private static final Log log = LogFactory.getLog(ServiceTaxUtil.class);

	public static void calculateServiceTax(BaseAvailRQ baseAvailRQ, List<FlightSegmentTO> flightSegments, PriceInfoTO priceInfoTO)
			throws WebservicesException {
		try {

			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();

			ExternalChgDTO handlingChargeDTO = getHandlingChargeDTO(baseAvailRQ.getTravelerInfoSummary(), priceInfoTO);
			BigDecimal handlingFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (handlingChargeDTO != null) {
				handlingFeeAmount = handlingChargeDTO.getTotalAmount();
			}

			BigDecimal[] paxWiseHandlingFee = AccelAeroCalculator.roundAndSplit(handlingFeeAmount,
					baseAvailRQ.getTravelerInfoSummary().getPayablePaxCount());
			Map<Integer, List<LCCClientExternalChgDTO>> quotedExternalChargesMap = getPaxSeqWiseQuotedExternalCharges(
					flightSegments);

			int paxSeq = 0;
			int paxQuantity = 0;
			String paxType = null;
			for (PassengerTypeQuantityTO paxTypeQuantityTO : baseAvailRQ.getTravelerInfoSummary()
					.getPassengerTypeQuantityList()) {
				paxType = paxTypeQuantityTO.getPassengerType();
				paxQuantity = paxTypeQuantityTO.getQuantity();
				if (PaxTypeTO.INFANT.equals(paxType)) {
					continue;
				}
				for (int i = 0; i < paxQuantity; i++) {
					int paxSeqNumber = ++paxSeq;
					List<LCCClientExternalChgDTO> paxExternalCharges = new ArrayList<>();

					paxExternalCharges.addAll(ServiceTaxCalculatorUtil.segmentWiseHandlingChargeDistribution(flightSegments,
							paxWiseHandlingFee[paxSeqNumber - 1], handlingChargeDTO));
					List<LCCClientExternalChgDTO> externalCharges = quotedExternalChargesMap.get(paxSeqNumber);
					if (externalCharges != null && !externalCharges.isEmpty()) {
						paxExternalCharges.addAll(externalCharges);
					}

					paxWiseExternalCharges.put(paxSeqNumber, paxExternalCharges);
					paxWisePaxTypes.put(paxSeqNumber, paxType);

				}
			}

			// TODO: remove once the verification is done or only debug mode
			ServiceTaxCalculatorUtil.printQuotedExternalChargesPaxWise(paxWiseExternalCharges);

			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();

			setServiceTaxBaseCriteriaInfo(serviceTaxQuoteCriteriaDTO);

			serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
			serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(priceInfoTO.getFareSegChargeTO());
			serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegments);
			serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
			serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);

			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = WebServicesModuleUtils
					.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, null);

			addServiceTaxesToPaxExternalCharges(baseAvailRQ, serviceTaxQuoteRS, false);

		} catch (Exception e) {
			log.error("calculateServiceTax Error" , e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "ERROR @ Service Tax Calculation ");
		}
	}

	public static BigDecimal addServiceTaxesToPaxExternalCharges(BaseAvailRQ baseAvailRQ,
			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteForTktRevResTOMap,
			boolean isSkipAddToExternalCharges) {
		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (serviceTaxQuoteForTktRevResTOMap != null && !serviceTaxQuoteForTktRevResTOMap.isEmpty()) {

			for (String carrierCode : serviceTaxQuoteForTktRevResTOMap.keySet()) {
				ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = serviceTaxQuoteForTktRevResTOMap.get(carrierCode);

				String taxDepositCountryCode = serviceTaxQuoteRS.getServiceTaxDepositeCountryCode();
				String taxDepositStateCode = serviceTaxQuoteRS.getServiceTaxDepositeStateCode();

				int paxSeq = 0;
				int parentSeq = 0;
				int paxQuantity = 0;
				String paxType = null;
				for (PassengerTypeQuantityTO paxTypeQuantityTO : baseAvailRQ.getTravelerInfoSummary()
						.getPassengerTypeQuantityList()) {
					paxType = paxTypeQuantityTO.getPassengerType();
					paxQuantity = paxTypeQuantityTO.getQuantity();

					for (int i = 0; i < paxQuantity; i++) {

						PTCFareBreakdownType.TravelerRefNumber travelerRefNumber = new PTCFareBreakdownType.TravelerRefNumber();
						if (PaxTypeTO.ADULT.equals(paxType)) {
							travelerRefNumber.setRPH("A" + (++paxSeq));
						} else if (PaxTypeTO.CHILD.equals(paxType)) {
							travelerRefNumber.setRPH("C" + (++paxSeq));
						} else if (PaxTypeTO.INFANT.equals(paxType)) {
							travelerRefNumber.setRPH("I" + (++paxSeq) + "/A" + (++parentSeq));
						}

						List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();

						if (serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null
								&& !serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
							List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(paxType);
							ServiceTaxCalculatorUtil.addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode,
									taxDepositStateCode, false);
							ServiceTaxCalculatorUtil.printServiceTaxChargesWithTotal(serviceTaxs,
									" PAX-Type " + paxType.toUpperCase());
						}

						if (serviceTaxQuoteRS.getPaxWiseServiceTaxes() != null
								&& !serviceTaxQuoteRS.getPaxWiseServiceTaxes().isEmpty()) {
							List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxWiseServiceTaxes().get(paxSeq);
							ServiceTaxCalculatorUtil.addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode,
									taxDepositStateCode, false);
							ServiceTaxCalculatorUtil.printServiceTaxChargesWithTotal(serviceTaxs, " PAX-Sequence " + paxSeq);
						}

						if (chgDTOs != null && !chgDTOs.isEmpty()) {
							List<LCCClientExternalChgDTO> groupedChargeDTOs = ServiceTaxCalculatorUtil
									.groupServiceTaxByChargeCode(chgDTOs);
							if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
								totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount,
										ServiceTaxCalculatorUtil.getTotalServiceTaxAmount(groupedChargeDTOs));

								if (!isSkipAddToExternalCharges)
									PriceQuoteInterlineUtil.addExternalCharge(travelerRefNumber.getRPH(), groupedChargeDTOs);
							}

						}

					}

				}
			}

		}
		return totalServiceTaxAmount;
	}

	public static BigDecimal calculateServiceTaxForCCFeeOnly(BaseAvailRQ baseAvailRQ, List<FlightSegmentTO> flightSegments,
			int totalPayablePaxCount, ExternalChgDTO ccChgDTO, boolean isSkipAddToExternalCharges, boolean includeInfant) throws WebservicesException {
		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		try {

			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();

			BigDecimal[] paxWiseCardFee = AccelAeroCalculator.roundAndSplit(ccChgDTO.getAmount(), totalPayablePaxCount);

			int paxSeq = 0;
			int paxQuantity = 0;
			String paxType = null;
			for (PassengerTypeQuantityTO paxTypeQuantityTO : baseAvailRQ.getTravelerInfoSummary()
					.getPassengerTypeQuantityList()) {

				paxType = paxTypeQuantityTO.getPassengerType();
				paxQuantity = paxTypeQuantityTO.getQuantity();

				for (int i = 0; i < paxQuantity; i++) {
					 if (PaxTypeTO.INFANT.equals(paxType) && !includeInfant) {
						 continue;
					 }
					int paxSeqNumber = ++paxSeq;
					List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
					chgDTOs.addAll(ServiceTaxCalculatorUtil.segmentWiseCreditCardChargeDistribution(flightSegments,
							paxWiseCardFee[paxSeqNumber - 1], ccChgDTO, true));
					if (!chgDTOs.isEmpty()) {
						paxWiseExternalCharges.put(paxSeqNumber, chgDTOs);
						paxWisePaxTypes.put(paxSeqNumber, paxType);
					}	

				}

			}
			// TODO: remove once the verification is done or only debug mode
			ServiceTaxCalculatorUtil.printQuotedExternalChargesPaxWise(paxWiseExternalCharges);

			if (!paxWiseExternalCharges.isEmpty()) {

				ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();

				setServiceTaxBaseCriteriaInfo(serviceTaxQuoteCriteriaDTO);

				serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
				serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(null);
				serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegments);
				serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
				serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);

				Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = WebServicesModuleUtils
						.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, null);

				totalServiceTaxAmount = addServiceTaxesToPaxExternalCharges(baseAvailRQ, serviceTaxQuoteRS,
						isSkipAddToExternalCharges);
			}

		} catch (Exception e) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"ERROR @ Service Tax Calculation For CC Fee");
		}
		return totalServiceTaxAmount;
	}

	@SuppressWarnings("unchecked")
	private static Map<Integer, List<LCCClientExternalChgDTO>>
			getPaxSeqWiseQuotedExternalCharges(List<FlightSegmentTO> flightSegments) {
		Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<? extends LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		if (quotedExternalCharges != null && !quotedExternalCharges.isEmpty()) {
			for (String travelRef : quotedExternalCharges.keySet()) {
				String paxType = travelRef.substring(0, 1);
				if ("A".equals(paxType)) {
					paxType = PaxTypeTO.ADULT;
				} else if ("C".equals(paxType)) {
					paxType = PaxTypeTO.CHILD;
				} else if ("I".equals(paxType)) {
					paxType = PaxTypeTO.INFANT;
					continue;
				}
				List<LCCClientExternalChgDTO> paxExternalCharges = new ArrayList<>();
				List<LCCClientExternalChgDTO> externalCharges = (List<LCCClientExternalChgDTO>) quotedExternalCharges
						.get(travelRef);
				paxExternalCharges = ServiceTaxCalculatorUtil.segmentWiseInsuranceChargeDistribution(flightSegments,
						externalCharges);

				Integer paxSequence = Integer.parseInt(travelRef.substring(1));
				paxWiseExternalCharges.put(paxSequence, paxExternalCharges);

			}
		}
		return paxWiseExternalCharges;
	}

	public static ExternalChgDTO getHandlingChargeDTO(TravelerInfoSummaryTO travelerInfoSummary, PriceInfoTO priceInfoTO)
			throws ModuleException, WebservicesException {
		ExternalChgDTO handlingChargeDTO = null;
		if (priceInfoTO != null && travelerInfoSummary != null) {

		}
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, ChargeRateOperationType.MAKE_ONLY);
		handlingChargeDTO = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
		OTAPaxCountTO paxCountTO = new OTAPaxCountTO(travelerInfoSummary);
		if (externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE) != null) {
			PricedItineraryInterlineUtil.calculateHandlingFee(priceInfoTO, paxCountTO,
					externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE), false);
		}
		return handlingChargeDTO;
	}

	public static void validatePassangerEmbarkationInformation(OTAAirPriceRQ otaAirPriceRQ)
			throws ModuleException, WebservicesException {

		String stateCode = null;
		String taxRegNo = null;
		String paxCountryCode = null;
		ServiceTaxCriteriaOptions serviceTaxOptions = otaAirPriceRQ.getServiceTaxCriteriaOptions();
		if (serviceTaxOptions != null) {
			paxCountryCode = serviceTaxOptions.getCountryCode();
			stateCode = serviceTaxOptions.getStateCode();
			taxRegNo = serviceTaxOptions.getTaxRegistrationNo();
		}

		String[] taxRegNoEnabledCoutryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");

		if (taxRegNoEnabledCoutryCodes != null && taxRegNoEnabledCoutryCodes.length > 0) {
			for (String countryCode : taxRegNoEnabledCoutryCodes) {
				if (countryCode.equals(paxCountryCode)) {
					if (StringUtil.isNullOrEmpty(stateCode)) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_STATE_CODE,
								"State code should be provided for the given country");
					} else {
						List<State> states = WebServicesModuleUtils.getCommonMasterBD().getStates(countryCode);
						boolean isValidStateCode = false;
						for (State state : states) {
							if (state.getStateCode().trim().equals(stateCode)) {
								isValidStateCode = true;
								break;
							}
						}

						if (!isValidStateCode) {
							throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_STATE_CODE,
									"Given state code is not a state in the given country");
						}
					}
				}
			}
		}

		if (!StringUtil.isNullOrEmpty(taxRegNo) && taxRegNo.length() != 15) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_TAX_REGISTRATION_NUMBER,
					"Tax registration number should be a 15 digit number");
		}

		if (!StringUtil.isNullOrEmpty(taxRegNo) && !stateCode.equals(taxRegNo.substring(0, 2))) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_TAX_REGISTRATION_NUMBER,
					"Given tax registration number and the state code does not match");
		}

		if (!StringUtil.isNullOrEmpty(taxRegNo) && !Pattern
				.compile("\\d{2}[a-zA-Z]{5}\\d{4}[a-zA-Z]{1}[a-zA-Z\\d]{1}[a-zA-Z]{1}[a-zA-Z\\d]{1}").matcher(taxRegNo)
				.matches()) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_TAX_REGISTRATION_NUMBER,
					"Given tax registration number is in invalid format");
		}


		ServiceTaxBaseCriteriaDTO taxQuoteBaseDTO = new ServiceTaxBaseCriteriaDTO();
		taxQuoteBaseDTO.setPaxCountryCode(paxCountryCode);
		taxQuoteBaseDTO.setPaxState(stateCode);
		taxQuoteBaseDTO.setPaxTaxRegistrationNo(taxRegNo);

		ThreadLocalData.setCurrentTnxParam(Transaction.SERVICE_TAX_BASE_CRITERIA_TO, taxQuoteBaseDTO);

	}

	private static void setServiceTaxBaseCriteriaInfo(ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO) {
		ServiceTaxBaseCriteriaDTO taxQuoteBaseDTO = (ServiceTaxBaseCriteriaDTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.SERVICE_TAX_BASE_CRITERIA_TO);
		if (taxQuoteBaseDTO != null) {
			serviceTaxQuoteCriteriaDTO.setPaxState(taxQuoteBaseDTO.getPaxState());
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(taxQuoteBaseDTO.getPaxCountryCode());

			if (!StringUtil.isNullOrEmpty(taxQuoteBaseDTO.getPaxTaxRegistrationNo()))
				serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(true);

		} else {
			serviceTaxQuoteCriteriaDTO.setPaxState(null);
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(null);
			serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(false);
		}
	}

	public static void validatePriceQuoteServiceTaxCriteriaWithContactDetails(String countryCode, String stateCode,
			String taxRegNo) throws WebservicesException {
		boolean isTaxCriteriaMissmatch = false;
		ServiceTaxBaseCriteriaDTO taxQuoteBaseDTO = (ServiceTaxBaseCriteriaDTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.SERVICE_TAX_BASE_CRITERIA_TO);

		if (taxQuoteBaseDTO != null) {
			String paxCountryCode = taxQuoteBaseDTO.getPaxCountryCode();
			String paxState = taxQuoteBaseDTO.getPaxState();
			if (!StringUtil.isNullOrEmpty(paxCountryCode) && !paxCountryCode.equalsIgnoreCase(countryCode)) {
				isTaxCriteriaMissmatch = true;
			}

			if (!StringUtil.isNullOrEmpty(paxState) && !paxState.equalsIgnoreCase(stateCode)) {
				isTaxCriteriaMissmatch = true;
			}

			if (!StringUtil.isNullOrEmpty(countryCode) && !StringUtil.isNullOrEmpty(stateCode)) {

				String[] taxRegNoEnabledCoutryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");

				if (taxRegNoEnabledCoutryCodes != null && taxRegNoEnabledCoutryCodes.length > 0) {
					List<String> taxRegNoEnabledCoutryList = Arrays.asList(taxRegNoEnabledCoutryCodes);

					if (taxRegNoEnabledCoutryList.contains(countryCode)) {
						if (!countryCode.equalsIgnoreCase(paxCountryCode)) {
							isTaxCriteriaMissmatch = true;
						}

						if (!stateCode.equalsIgnoreCase(paxState)) {
							isTaxCriteriaMissmatch = true;
						}
					}

				}

			}

		}

		if (StringUtil.isNullOrEmpty(taxRegNo) && !StringUtil.isNullOrEmpty(taxQuoteBaseDTO.getPaxTaxRegistrationNo())) {
			isTaxCriteriaMissmatch = true;
		}

		if (!StringUtil.isNullOrEmpty(taxRegNo) && StringUtil.isNullOrEmpty(taxQuoteBaseDTO.getPaxTaxRegistrationNo())) {
			isTaxCriteriaMissmatch = true;
		}

		if (!StringUtil.isNullOrEmpty(taxRegNo) && !StringUtil.isNullOrEmpty(taxQuoteBaseDTO.getPaxTaxRegistrationNo())
				&& !taxRegNo.equalsIgnoreCase(taxQuoteBaseDTO.getPaxTaxRegistrationNo())) {
			isTaxCriteriaMissmatch = true;
		}

		if (isTaxCriteriaMissmatch) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_TAX_REGISTERED_DETAILS,
					"Price quoted tax details does not match with contact details tax registered details.");
		}

	}
	
	public static BaseAvailRQ getDummyBaseAvailRQ(int adultCount, int childCount, int infantCount) {

		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
		AvailPreferencesTO availPref = baseAvailRQ.getAvailPreferences();
		availPref.setSearchSystem(SYSTEM.AA);

		TravelerInfoSummaryTO traverlerInfo = baseAvailRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(adultCount);

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(childCount);

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(infantCount);

		return baseAvailRQ;
	}
	
	/**
	 * For Re-Quote flow Service Tax is Calculated from BE with RequoteBalanceCalculator, any associated service tax
	 * during price quote should be eliminated to avoid duplicate charges.
	 */
	@SuppressWarnings("unchecked")
	public static void removeServiceTaxExternalChargesForRequote(String modificationType) {

		if (CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCELLATION_BALANCES_FOR_CANCEL_OND)
				.equals(modificationType)
				|| CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_MODIFICATION_BALANCES_FOR_MODIFY_OND)
						.equals(modificationType)
				|| CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_BALANCES_FOR_ADD_OND)
						.equals(modificationType)) {
			Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<? extends LCCClientExternalChgDTO>>) ThreadLocalData
					.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

			if (quotedExternalCharges != null && quotedExternalCharges.size() > 0) {
				for (String travelRefKey : quotedExternalCharges.keySet()) {
					List<? extends LCCClientExternalChgDTO> externalChgDTOs = quotedExternalCharges.get(travelRefKey);

					if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
						Iterator<? extends LCCClientExternalChgDTO> externalChgDTOsItr = externalChgDTOs.iterator();
						while (externalChgDTOsItr.hasNext()) {
							LCCClientExternalChgDTO externalChgDTO = externalChgDTOsItr.next();
							if (EXTERNAL_CHARGES.SERVICE_TAX.equals(externalChgDTO.getExternalCharges())) {
								externalChgDTOsItr.remove();
							}
						}

					}
				}
				ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES, quotedExternalCharges);
			}

		}

	}

}
