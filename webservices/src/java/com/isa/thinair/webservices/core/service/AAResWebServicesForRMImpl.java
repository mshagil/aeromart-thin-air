package com.isa.thinair.webservices.core.service;

import javax.jws.WebService;

import com.isa.thinair.webservices.api.service.AAResWebServicesForRM;

/**
 * 
 * @author Manoj Dhanushka
 *
 */
@WebService(serviceName = "AAResWebServicesForRM", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAResWebServicesForRM")
public class AAResWebServicesForRMImpl extends BaseWebServicesImpl implements AAResWebServicesForRM {

}
