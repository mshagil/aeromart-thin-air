package com.isa.thinair.webservices.core.cache.delegator.dto;

import java.io.Serializable;

public class JourenyInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String origin;
	private String detination;
	private int windowBefore;
	private int windowAfter;
	private String departureDate;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDetination() {
		return detination;
	}

	public void setDetination(String detination) {
		this.detination = detination;
	}

	public int getWindowBefore() {
		return windowBefore;
	}

	public void setWindowBefore(int windowBefore) {
		this.windowBefore = windowBefore;
	}

	public int getWindowAfter() {
		return windowAfter;
	}

	public void setWindowAfter(int windowAfter) {
		this.windowAfter = windowAfter;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
}
