package com.isa.thinair.promotion.core.bl.lms;

import java.io.File;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SFTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_STATUS;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;
import com.isa.thinair.wsclient.api.util.LMSConstants.LMS_SERVICE_PROVIDER;

public class SmartButtonFileUploader extends AbstractLmsFileUploader {

	private Log log = LogFactory.getLog(SmartButtonFileUploader.class);

	@Override
	public void uploadPendingFiles(FILE_TYPE fileType) throws ModuleException {
		List<LoyaltyFileLog> uploadPendingFiles = PromotionsHandlerFactory.getLoyaltyManagmentDAO().getUploadPendingFiles(
				fileType, LMS_SERVICE_PROVIDER.SMART_BUTTON);

		if (uploadPendingFiles != null && !uploadPendingFiles.isEmpty()) {
			for (LoyaltyFileLog loyaltyFileLog : uploadPendingFiles) {
				String fileName = loyaltyFileLog.getFileName();
				SFTPUtil sftpUtil = getSFTPUtil();
				if (sftpUtil != null && fileName != null && !"".equals(fileName)) {
					LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator
							.getLoyaltyExternalConfig();
					StringBuilder localFilePath = new StringBuilder();
					StringBuilder remoteFilePath = new StringBuilder();
					if (fileType == FILE_TYPE.FLOWN_FILE) {
						localFilePath.append(PlatformConstants.getAbsLoyaltyFlownFileUploadPath()).append(File.separatorChar);
						remoteFilePath.append(externalConfig.getFlownFileRemotePath()).append(File.separatorChar);
					} else {
						localFilePath.append(PlatformConstants.getAbsLoyaltyProductFileUploadPath()).append(File.separatorChar);
						remoteFilePath.append(externalConfig.getProductFileRemotePath()).append(File.separatorChar);
					}
					localFilePath.append(fileName);
					remoteFilePath.append(fileName);
					boolean uploadStatus = true;
					try {
						sftpUtil.uploadFile(localFilePath.toString(), remoteFilePath.toString());
					} catch (Exception e) {
						log.error("SFTP file upload failed for Loyalty file:" + fileName, e);
						uploadStatus = false;
					} finally {
						loyaltyFileLog.setStatus(uploadStatus ? FILE_STATUS.UPLOADED.toString() : FILE_STATUS.UPLOAD_FAILED
								.toString());
						PromotionsHandlerFactory.getLoyaltyManagmentDAO().saveOrUpdateLoyaltyFileLog(loyaltyFileLog);
						sftpUtil.closeAndLogout();
					}
				}
			}
		}
	}

	private SFTPUtil getSFTPUtil() {
		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		if (externalConfig.getLmsFtpServerName() != null && !"".equals(externalConfig.getLmsFtpServerName())) {
			SFTPUtil sftpUtil = new SFTPUtil(externalConfig.getLmsFtpServerName(), externalConfig.getLmsFtpServerUserName(),
					externalConfig.getLmsFtpServerPassword());
			return sftpUtil;
		}
		return null;
	}

}
