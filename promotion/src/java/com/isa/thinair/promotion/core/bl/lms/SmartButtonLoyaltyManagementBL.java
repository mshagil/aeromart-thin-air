package com.isa.thinair.promotion.core.bl.lms;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.springframework.util.StringUtils;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.to.lms.RedeemLoyaltyPointsReq;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.promotion.core.util.LMSHelperUtil;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardResponse;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSCommonUtil;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSSmartButtonWebServiceInvoker;

public class SmartButtonLoyaltyManagementBL extends AbstractLoyalatyManagementBL {

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String createLoyaltyMember(Customer customer) throws ModuleException {

		LmsMember lmsMember = customer.getLMSMemberDetails();
		String sbInternalMemberId = PromotionModuleServiceLocator.getWSClientBD().enrollMemberWithEmail(customer);

		if (!StringUtils.isEmpty(sbInternalMemberId)) {
			String memberAccountId = lmsMember.getFfid();
			// call set password
			String password = lmsMember.getPassword();
			try {
				PromotionModuleServiceLocator.getWSClientBD().setMemberPassword(memberAccountId, password);
			} catch (Exception me) {
				if (log.isInfoEnabled()) {
					log.info("LMS member enrollment succeed for " + memberAccountId + ". Other user details saving failed.");
				}
				log.error("Error in saving password of LMS member ", me);
			}

			// call set phone numbers
			String phoneNumber = StringUtil.getNotNullString(lmsMember.getPhoneNumber());
			String mobileNumber = StringUtil.getNotNullString(lmsMember.getMobileNumber());
			try {
				PromotionModuleServiceLocator.getWSClientBD().saveMemberPhoneNumbers(memberAccountId, phoneNumber, mobileNumber);
			} catch (Exception me) {
				if (log.isInfoEnabled()) {
					log.info("LMS member enrollment and saving password succeed for " + memberAccountId
							+ ". Other user details saving failed.");
				}
				log.error("Error in saving Mobile/Phone numbers", me);
			}

			// call set custom fields
			try {
				PromotionModuleServiceLocator.getWSClientBD().saveMemberCustomFields(customer);
			} catch (Exception me) {
				if (log.isInfoEnabled()) {
					log.info("LMS member enrollment,saving password and phone numbers succeed for " + memberAccountId
							+ ". Other user details saving failed.");
				}
				log.error("Error in saving Custom member fields", me);
			}

			// call set preferred communication language
			String prefCommLanguage = StringUtil.getNotNullString(lmsMember.getLanguage());
			try {
				PromotionModuleServiceLocator.getWSClientBD().saveMemberPreferredCommunicationLanguage(memberAccountId,
						prefCommLanguage);
			} catch (Exception me) {
				if (log.isInfoEnabled()) {
					log.info("LMS member enrollment,saving password, phone numbers and custom fields succeed for "
							+ memberAccountId + ". Other user details saving failed.");
				}
				log.error("Error in saving member preferred comm language", me);
			}

			String headOfHosueHold = StringUtil.getNotNullString(lmsMember.getHeadOFEmailId());

			try {
				PromotionModuleServiceLocator.getWSClientBD().saveMemberHeadOfHouseHold(memberAccountId, headOfHosueHold);
			} catch (Exception e) {
				if (log.isInfoEnabled()) {
					log.info("LMS member enrollment,saving head of household is succeed for " + memberAccountId);
				}
				log.error("Error in saving member head of household", e);
			}

			// call set referred email id language
			String refEmail = StringUtil.getNotNullString(lmsMember.getRefferedEmail());
			try {
				PromotionModuleServiceLocator.getWSClientBD().saveLoyaltySystemReferredByUser(memberAccountId, refEmail);
			} catch (Exception me) {
				if (log.isInfoEnabled()) {
					log.info("LMS member enrollment,saving password, phone numbers, custom fields and preferred languge succeed for "
							+ memberAccountId + ". Other user details saving failed.");
				}
				log.error("Error in saving user reffered email ID", me);
			}
			// Integrate Mashreq With LMS
			if (AppSysParamsUtil.isIntegrateMashreqWithLMS()) {
				String LoyaltyAccountNo = PromotionModuleServiceLocator.getLoyaltyCustomerBD().getLoyaltyAccountNo(
						lmsMember.getCustomerId());
				if (LoyaltyAccountNo != null) {
					try {
						PromotionModuleServiceLocator.getWSClientBD().addMemberAccountId(
								LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId,
								sbInternalMemberId, "Mid", LoyaltyAccountNo);
						log.debug("Successfully called Smart Button API to link Mashreq Account " + LoyaltyAccountNo
								+ " and LMS account " + memberAccountId);
					} catch (Exception me) {
						if (log.isInfoEnabled()) {
							log.info("Mashreq Loyalty Integration with LMS failed for user " + memberAccountId);
						}
						log.error("Error in Integrating Mashreq Loyalty with LMS", me);
					}
				}
			}
			// Call send member welcome message
			try {
				boolean welcomeResponse = PromotionModuleServiceLocator.getWSClientBD().sendMemberWelcomeMessage(memberAccountId);
				if (log.isInfoEnabled()) {
					if (welcomeResponse) {
						log.info("Welcome message sent for member " + memberAccountId);
					} else {
						log.info("Welcome message sending failed for member " + memberAccountId);
					}
				}

			} catch (Exception e) {
				if (log.isInfoEnabled()) {
					log.info("LMS member enrollment,saving password, phone numbers, custom fields, preferred languge and referred ID succeed for "
							+ memberAccountId + ". Other user details saving failed.");
				}
				log.error("Error in sending welcome message", e);
			}
		}

		return sbInternalMemberId;
	}
	
	@Override
	public void updateMemberProfile(Customer customer) throws ModuleException {
		if (customer.getLMSMemberDetails() != null) {
			LmsMember lmsMember = customer.getLMSMemberDetails();

			if (PromotionModuleServiceLocator.getWSClientBD().getLoyaltyMemberCoreDetails(lmsMember.getFfid()).getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {

				// update phone numbers
				try {
					PromotionModuleServiceLocator.getWSClientBD().saveMemberPhoneNumbers(lmsMember.getFfid(),
							lmsMember.getPhoneNumber(), lmsMember.getMobileNumber());
				} catch (ModuleException me) {
					log.error("LmsMemberBLImpl==>updateLMSmemberProfile.mobile", me);
				}

				// update custom fields
				try {
					PromotionModuleServiceLocator.getWSClientBD().saveMemberCustomFields(customer);
				} catch (ModuleException me) {
					log.error("LmsMemberBLImpl==>updateLMSmemberProfile.customFields", me);
				}
				
				// delete existing head of house for the dependent
				try {
					String memberId = StringUtil.getNotNullString(lmsMember.getFfid());
					PromotionModuleServiceLocator.getWSClientBD().removeMemberHeadOfHouseHold(memberId);
				} catch (ModuleException me) {
					log.error("LmsMemberBLImpl==>updateLMSmemberProfile.headOfHouse.delete", me);
				}
				
				// set head of house
				try {
					String headOfHosueHold = StringUtil.getNotNullString(lmsMember.getHeadOFEmailId());
					PromotionModuleServiceLocator.getWSClientBD().saveMemberHeadOfHouseHold(lmsMember.getFfid(),
							headOfHosueHold);
				} catch (ModuleException me) {
					log.error("LmsMemberBLImpl==>updateLMSmemberProfile.headOfHouse.save", me);
				}

				// update pref language
				try {
					PromotionModuleServiceLocator.getWSClientBD().saveMemberPreferredCommunicationLanguage(lmsMember.getFfid(),
							lmsMember.getLanguage());
				} catch (ModuleException me) {
					log.error("LmsMemberBLImpl==>updateLMSmemberProfile.commLanguage", me);
				}

				// update name
				try {
					PromotionModuleServiceLocator.getWSClientBD().saveMemberName(customer);
				} catch (ModuleException me) {
					log.error("LmsMemberBLImpl==>updateLMSmemberProfile.saveMemberName", me);
				}

			}
		}
	}

	@Override
	public ServiceResponce issueLoyaltyRewards(RedeemLoyaltyPointsReq redeemLoyaltyPointsReq) throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(false);

		BigDecimal pointToCurrencyConversionRate = redeemLoyaltyPointsReq.getLoyaltyPointsToCurrencyConversionRate();

		// Issue variable rewards
		Map<String, BigDecimal> productCurrencyValue = LMSHelperUtil.convertLoyaltyPointsToCurrencyValue(
				redeemLoyaltyPointsReq.getProductPoints(), pointToCurrencyConversionRate);

		String locationExtReference = LMSCommonUtil.getLocationExternalReference(redeemLoyaltyPointsReq.getAppIndicator());

		IssueRewardRequest issueRewardRequest = LMSHelperUtil.populateIssueRewardRequest(
				redeemLoyaltyPointsReq.getMemberAccountId(), locationExtReference, productCurrencyValue);
		IssueRewardResponse issueRewardResponse = PromotionModuleServiceLocator.getWSClientBD().issueVariableRewards(
				issueRewardRequest, redeemLoyaltyPointsReq.getPnr(), redeemLoyaltyPointsReq.getProductPoints());

		if (issueRewardResponse != null) {
			String[] rewardIds = issueRewardResponse.getIssuedRewardIds();
			response.addResponceParam(ResponceCodes.ResponseParams.LOYALTY_REWARD_IDS, rewardIds);
			response.setSuccess(true);

			log.debug("Reward IDs: " + Arrays.toString(rewardIds) + " for Loyalty Member: "
					+ redeemLoyaltyPointsReq.getMemberAccountId());
		}

		return response;
	}

	@Override
	public ServiceResponce redeemIssuedRewards(String[] rewardIds, AppIndicatorEnum appIndicator, String memberAccountId)
			throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		String locationExtReference = LMSCommonUtil.getLocationExternalReference(appIndicator);
		if (rewardIds != null && rewardIds.length > 0) {
			boolean cancelResponse = PromotionModuleServiceLocator.getWSClientBD().redeemMemberRewards(locationExtReference,
					rewardIds, memberAccountId);

			response.setSuccess(cancelResponse);
		}
		return response;
	}

	@Override
	public ServiceResponce cancelRedeemedLoyaltyPoints(String pnr,String[] rewardIds, String memberAccountId) throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (rewardIds != null && rewardIds.length > 0) {
			boolean cancelResponse = PromotionModuleServiceLocator.getWSClientBD().cancelMemberRewards(pnr,
					rewardIds, memberAccountId);
			response.setSuccess(cancelResponse);
		}
		return response;	
	}

	@Override
	public void updatePendingFiles(FILE_TYPE fileType) throws ModuleException {
		SmartButtonFileUploader smartButtonFileUploader = new SmartButtonFileUploader();
		smartButtonFileUploader.uploadPendingFiles(fileType);		
	}

	@Override
	public String getCrossPortalLoginUrl(LmsMember lmsMember, String menuItemIntRef) throws ModuleException {
		return PromotionModuleServiceLocator.getWSClientBD()
				.getCrossPortalLoginUrl(lmsMember.getFfid(), lmsMember.getPassword(), menuItemIntRef);
	}

}
