package com.isa.thinair.promotion.core.bl.lms;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;

public abstract class AbstractLmsFileUploader {

	public abstract void uploadPendingFiles(FILE_TYPE fileType) throws ModuleException;

}
