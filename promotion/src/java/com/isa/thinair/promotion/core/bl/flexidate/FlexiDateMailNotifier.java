package com.isa.thinair.promotion.core.bl.flexidate;

import java.util.HashMap;

import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.promotion.core.bl.common.PromotionMailNotifier;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.util.CommandParamNames;

/**
 * 
 * @isa.module.command name="flexiDateMailNotifier"
 */
public class FlexiDateMailNotifier extends PromotionMailNotifier {

	protected HashMap<String, Object> getTopicParams(NotificationReceiverTo notificationReceiver) {
		String eventType = (String) getParameter(CommandParamNames.EVENT_TYPE);
		HashMap<String, Object> topicParams = new HashMap<String, Object>();

		topicParams.put("pnr", notificationReceiver.getPnr());
		topicParams.put("ibe_url", AppSysParamsUtil.getSecureIBEUrl());
		topicParams.put("ibe_url_root", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));


		if (eventType.equals(CommandParamNames.EVENT_PROMO_NOTIFY)) {
			topicParams.put("promo_id", notificationReceiver.getPromotionId());
		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_SUBSCRIBE)) {
			topicParams.put("promo_id", notificationReceiver.getPromotionId());
		}

		return topicParams;
	}

	protected String getTopicName() {
		String eventType = (String) getParameter(CommandParamNames.EVENT_TYPE);

		if (eventType.equals(CommandParamNames.EVENT_PROMO_NOTIFY)) {
			return "promotion_offer_flexi_date";
		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_APPROVE)) {
			return "promotion_status_flexi_date";
		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_SUBSCRIBE)) {
			return "promotion_subscribed_flexi_date";
		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_EXPIRE)) {
			return "promotion_expired_flexi_date";
		}

		return null;
	}

}
