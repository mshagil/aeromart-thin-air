package com.isa.thinair.promotion.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.VoucherTemplate;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateRequest;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;
import com.isa.thinair.promotion.core.persistence.dao.VoucherTemplateDao;
import com.isa.thinair.promotion.core.service.bd.VoucherTemplateBDImpl;
import com.isa.thinair.promotion.core.service.bd.VoucherTemplateBDLocalImpl;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;
import com.isa.thinair.promotion.core.util.VoucherTemplateDataHelper;

/**
 * @author chanaka
 *
 */
@Stateless
@RemoteBinding(jndiBinding = "VoucherTemplate.remote")
@LocalBinding(jndiBinding = "VoucherTemplate.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class VoucherTemplateBean extends PlatformBaseSessionBean implements VoucherTemplateBDImpl,
		VoucherTemplateBDLocalImpl {

	private static final Log log = LogFactory.getLog(VoucherTemplateBean.class);

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce createVoucherTemplate(VoucherTemplateTo voucherTemplateTo) throws ModuleException {
		try {
			VoucherTemplateDao voucherTemplateDao = PromotionsHandlerFactory.getVoucherTemplateDao();
			DefaultServiceResponse resp = new DefaultServiceResponse(true);

			VoucherTemplate voucherTemplate = VoucherTemplateDataHelper.toVoucherTemplate(voucherTemplateTo,
					getUserPrincipal());

			if (resp.isSuccess()) {

				voucherTemplateDao.saveVoucherTemplate(voucherTemplate);

				Audit audit = generateAudit(voucherTemplate, getUserPrincipal());
				PromotionModuleServiceLocator.getAuditorBD().audit(audit, null);
			}

			return resp;
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page<VoucherTemplateTo> searchVoucherTemplates(VoucherTemplateRequest voucherTemplateRequest, Integer start,
			Integer size) throws ModuleException {

		try {
			Page<VoucherTemplate> voucherTemplates = PromotionsHandlerFactory.getVoucherTemplateDao()
					.searchVoucherTemplates(voucherTemplateRequest, start, size);

			List<VoucherTemplateTo> voucherTemplateTos = new ArrayList<VoucherTemplateTo>();

			for (VoucherTemplate voucherTemplate : voucherTemplates.getPageData()) {
				voucherTemplateTos.add(VoucherTemplateDataHelper.toVoucherTemplateTo(voucherTemplate));
			}

			Collections.sort(voucherTemplateTos, new Comparator<VoucherTemplateTo>() {
				@Override
				public int compare(VoucherTemplateTo a, VoucherTemplateTo b) {
					return new Integer(b.getVoucherId()).compareTo(new Integer(a.getVoucherId()));
				}
			});

			return new Page<VoucherTemplateTo>(voucherTemplates.getTotalNoOfRecords(),
					voucherTemplates.getStartPosition(), voucherTemplates.getTotalNoOfRecordsInSystem(),
					voucherTemplateTos);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@Override
	public ArrayList<VoucherTemplateTo> getVoucherInSalesPeriod() throws ModuleException {

		Date presentDate = new Date();

		List<VoucherTemplate> validVoucherTemplates = PromotionsHandlerFactory.getVoucherTemplateDao()
				.getVoucherInSalesPeriod(presentDate);
		ArrayList<VoucherTemplateTo> voucherTemplateTOs = new ArrayList<VoucherTemplateTo>();
		Iterator<VoucherTemplate> iterator = validVoucherTemplates.iterator();

		while (iterator.hasNext()) {
			voucherTemplateTOs.add(VoucherTemplateDataHelper.toVoucherTemplateTo(iterator.next()));
		}

		return voucherTemplateTOs;
	}

	private static Audit generateAudit(VoucherTemplate voucherTemplate, UserPrincipal userPrincipal) {

		String details = "voucher_name := " + voucherTemplate.getVoucherName();
		return new Audit(TasksUtil.DEFINE_VOUCHER_TEMPLATE, new Date(), "airadmin", userPrincipal.getUserId(), details);

	}

	@Override
	public ArrayList<VoucherTemplateTo> getVouchers() throws ModuleException {

		List<VoucherTemplate> validVoucherTemplates = PromotionsHandlerFactory.getVoucherTemplateDao().getVouchers();
		ArrayList<VoucherTemplateTo> voucherTemplateTOs = new ArrayList<VoucherTemplateTo>();
		Iterator<VoucherTemplate> iterator = validVoucherTemplates.iterator();

		while (iterator.hasNext()) {
			voucherTemplateTOs.add(VoucherTemplateDataHelper.toVoucherTemplateTo(iterator.next()));
		}

		return voucherTemplateTOs;
	}
}
