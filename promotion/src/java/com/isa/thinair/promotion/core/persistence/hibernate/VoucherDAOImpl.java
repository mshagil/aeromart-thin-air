package com.isa.thinair.promotion.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.core.util.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.promotion.api.model.Voucher;
import com.isa.thinair.promotion.api.model.VoucherRedemption;
import com.isa.thinair.promotion.api.to.VoucherSearchRequest;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.core.persistence.dao.VoucherDAO;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

/**
 * @author chethiya
 * @isa.module.dao-impl dao-name="VoucherDAO"
 *
 */
public class VoucherDAOImpl extends PlatformHibernateDaoSupport implements VoucherDAO {

	Log log = LogFactory.getLog(VoucherDAOImpl.class);

	private static String VOUCHER_SEQUENCE = "S_VOUCHER";
	private static final String VOUCHER_GROUP_SEQUENCE = "S_VOUCHER_GROUP";

	@Override
	public void saveVoucher(Voucher voucher) {
		getSession().save(voucher);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Voucher getVoucher(String voucherId) {
		Voucher voucher = null;
		List<Voucher> list = null;

		String hql = "from Voucher as voucher where voucher.voucherId=:voucherID";

		Query query = getSession().createQuery(hql).setString("voucherID", voucherId);
		list = query.list();

		if (list.size() != 0) {
			voucher = (Voucher) list.get(0);
		}

		return voucher;
	}

	@Override
	public List<Voucher> getVouchersFromGroupId(String groupId) {
		List<Voucher> vouchers = new ArrayList<>();
		if (!StringUtils.isEmpty(groupId)) {
			vouchers = find("Select voucher " +
					"from Voucher as voucher where voucher.voucherGroupId=?", groupId, Voucher.class);
		}
		return vouchers;
	}

	@Override
	public Voucher getValidVoucher(String voucherId) {
		Voucher voucher = null;
		List<Voucher> list = null;

		String hql = "from Voucher as voucher where voucher.voucherId=:voucherID WHERE :date BETWEEN voucher.validFrom AND voucher.validTo";

		Query query = getSession().createQuery(hql).setString("voucherID", voucherId).setDate("date", new Date());

		list = query.list();
		if (list.size() != 0) {
			voucher = (Voucher) list.get(0);
		}

		return voucher;
	}

	@SuppressWarnings("unchecked")
	public Page<Voucher> searchVoucher(int start, int pageSize, VoucherSearchRequest voucherSearchRequest, String voucherType) {

		String countHql = "select count(*)";
		String hql = " from Voucher where voucherId like :voucherId AND email like :email "
				+ " AND paxFirstName like :paxFirstName AND paxLastName like :paxLastName ";

		if(StringUtils.isNotEmpty(voucherSearchRequest.getIssuedAgent())) {
			hql = hql + "AND issuedUserId like :issuedUserId " ;
		}

		if (voucherType.equals(PromotionCriteriaConstants.VoucherType.GIFT_VOUCHER)) {
			hql += " AND templateId is not null ";
		} else {
			hql += " AND templateId is null ";
		}
		hql += " ORDER BY voucherId DESC ";

		countHql = countHql + hql;

		Query countQuery = getSession().createQuery(countHql)
				.setString("voucherId", "%" + voucherSearchRequest.getVoucherId() + "%")
				.setString("email", "%" + voucherSearchRequest.getEmail() + "%")
				.setString("paxFirstName", "%" + voucherSearchRequest.getFirstName() + "%")
				.setString("paxLastName", "%" + voucherSearchRequest.getLastName() + "%");

		Query query = getSession().createQuery(hql).setString("voucherId", "%" + voucherSearchRequest.getVoucherId() + "%")
				.setString("email", "%" + voucherSearchRequest.getEmail() + "%")
				.setString("paxFirstName", "%" + voucherSearchRequest.getFirstName() + "%")
				.setString("paxLastName", "%" + voucherSearchRequest.getLastName() + "%");

		if(StringUtils.isNotEmpty(voucherSearchRequest.getIssuedAgent())) {
			query = query.setString("issuedUserId", "%" + voucherSearchRequest.getIssuedAgent() + "%");
			countQuery = countQuery.setString("issuedUserId", "%" + voucherSearchRequest.getIssuedAgent() + "%");
		}

		Long count = (Long) countQuery.uniqueResult();
		query.setFirstResult(start);
		query.setMaxResults(pageSize);
		List<Voucher> records = query.list();

		return new Page<Voucher>(pageSize, start, start + pageSize, count.intValue(), records);

	}

	@Override
	public void updateVoucher(Voucher voucher) {
		getSession().saveOrUpdate(voucher);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Voucher> getVoucherList(List<String> voucherIDList, Character status) {

		String hql = "from Voucher where voucherId IN (:voucherIDList) and status = :status";
		Query query = getSession().createQuery(hql)
				.setParameterList("voucherIDList", voucherIDList)
				.setCharacter("status", status);
		return query.list();
	}

	@Override
	public void saveOrUpdateAll(List<Voucher> voucherIDList) {
		hibernateSaveOrUpdateAll(voucherIDList);
	}

	@Override
	public String getNextVoucherId() {

		String voucherIDSql = "SELECT " + VOUCHER_SEQUENCE + ".NEXTVAL VOUCHER_ID FROM DUAL";

		log.debug("Inside getNextPnrNumber");
		JdbcTemplate jt = new JdbcTemplate(PromotionModuleServiceLocator.getDatasource());
		String voucherID;

		log.debug("############################################");
		log.debug(" SQL to excute            : " + voucherIDSql);
		log.debug("############################################");

		voucherID = (String) jt.query(voucherIDSql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String voucherID = "";

				if (rs != null) {
					if (rs.next()) {
						voucherID = BeanUtils.nullHandler(rs.getString("VOUCHER_ID"));
					}
				}

				return voucherID;
			}
		});

		return voucherID;

	}

	@Override
	public void cancelVoucher(VoucherDTO voucherDTO) {

		try {

			String voucherId = voucherDTO.getVoucherId();
			String cancelRemarks = voucherDTO.getCancelRemarks();
			Character status = PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_CANCELED;

			String hqlUpdate = "update Voucher set status =:status, " + "cancelRemarks =:cancelRemarks " + "where "
					+ "voucherId =:voucherId";
			getSession().createQuery(hqlUpdate).setParameter("voucherId", voucherId).setParameter("status", status)
					.setParameter("cancelRemarks", cancelRemarks).executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	@Override
	public void saveVoucherRedemption(VoucherRedemption voucherRedemption) {
		getSession().save(voucherRedemption);
	}

	@Override
	public Map<Integer, String> getFlightFarePaxWise(Collection<Integer> paxIds, Collection<Integer> pnrSegIds) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT distinct ppf.pnr_pax_id,ppf.total_fare ");
		sql.append("FROM t_pnr_pax_fare ppf , t_pnr_pax_fare_segment ppfs, t_pnr_segment ps ");
		sql.append("WHERE ps.pnr_seg_id in (" + BeanUtils.constructINStringForInts(pnrSegIds) + ") ");
		sql.append("AND ps.pnr_seg_id  = ppfs.pnr_seg_id ");
		sql.append("AND ppfs.ppf_id = ppf.ppf_id ");
		sql.append("AND ppf.pnr_pax_id in (" + BeanUtils.constructINStringForInts(paxIds) + ")");

		@SuppressWarnings("unchecked")
		HashMap<Integer, String> mapResult = (HashMap<Integer, String>) jt.query(sql.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				HashMap<Integer, String> paxFareMap = new HashMap<Integer, String>();
				if (rs != null) {
					while (rs.next()) {
						Integer pnrPaxId = rs.getInt("pnr_pax_id");
						String amount = rs.getString("total_fare");
						paxFareMap.put(pnrPaxId, amount);
					}
				}
				return paxFareMap;
			}
		});
		return mapResult;
	}

	@Override
	public List<Voucher> getExpiredVouchers() {
		String hql = "SELECT v FROM Voucher v WHERE v.validTo < :date  AND v.status=:status ";
		Query templateQuery = getSession().createQuery(hql).setTimestamp("date", new Date())
				.setCharacter("status", PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_ISSUED);
		@SuppressWarnings("unchecked")
		List<Voucher> voucherList = templateQuery.list();
		return voucherList;
	}

	@Override
	public String getNextVoucherGroupId() {

		String voucherGroupIDSql = "SELECT " + VOUCHER_GROUP_SEQUENCE + ".NEXTVAL VOUCHER_GROUP_ID FROM DUAL";

		log.debug("Inside getNextVoucherGroupNumber");
		JdbcTemplate jt = new JdbcTemplate(PromotionModuleServiceLocator.getDatasource());
		String voucherGroupID;

		log.debug("############################################");
		log.debug(" SQL to excute            : " + voucherGroupIDSql);
		log.debug("############################################");

		voucherGroupID = (String) jt.query(voucherGroupIDSql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String voucherGroupID = "";

				if (rs != null) {
					if (rs.next()) {
						voucherGroupID = BeanUtils.nullHandler(rs.getString("VOUCHER_GROUP_ID"));
					}
				}

				return voucherGroupID;
			}
		});

		return voucherGroupID;

	}

	@Override
	public void saveVouchers(List<Voucher> voucherlist) {
		hibernateSaveOrUpdateAll(voucherlist);
	}

	@Override
	public List<Voucher> getBlockedVouchers() {
		String hql = "SELECT v FROM Voucher v WHERE v.status=:status ";
		Query templateQuery = getSession().createQuery(hql).setCharacter("status",
				PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_BLOCKED);
		@SuppressWarnings("unchecked")
		List<Voucher> voucherList = templateQuery.list();
		return voucherList;
	}

}
