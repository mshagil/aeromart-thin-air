package com.isa.thinair.promotion.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentHolder;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.to.PromotionRequestApproveTo;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.persistence.dao.NextSeatFreePromotionAdminDao;
import com.isa.thinair.promotion.core.persistence.dao.PromotionAdministrationDao;
import com.isa.thinair.promotion.core.service.bd.PromotionAdministrationBDImpl;
import com.isa.thinair.promotion.core.service.bd.PromotionAdministrationBDLocalImpl;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsDaoHelper;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

@Stateless
@RemoteBinding(jndiBinding = "PromotionAdministration.remote")
@LocalBinding(jndiBinding = "PromotionAdministration.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class PromotionAdministrationBean extends PlatformBaseSessionBean implements PromotionAdministrationBDImpl,
		PromotionAdministrationBDLocalImpl {

	private static Log log = LogFactory.getLog(PromotionAdministrationBean.class);

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void notifyPromotionOffer(PromotionType promotionType) throws ModuleException {
		try {

			HashMap<String, Object> notifyParams = new HashMap<String, Object>();
			notifyParams.put(CommandParamNames.PROMOTION_TYPE, promotionType);

			log.info("Sending Promotion Notifications- start ---- " + promotionType.toString());

			PromotionAdministrationDao dao = PromotionsHandlerFactory.getPromotionAdministrationDao(promotionType);
			List<NotificationReceiverTo> receivers = dao.getNotificationDestsEntitledForPromotion(notifyParams);
			notifyParams.put(CommandParamNames.PROMOTION_NOTIFICATION_DESTINATIONS, receivers);
			notifyParams.put(CommandParamNames.EVENT_TYPE, CommandParamNames.EVENT_PROMO_NOTIFY);

			DefaultBaseCommand notifyCommand = PromotionsHandlerFactory.getPromotionsNotifier(promotionType);
			notifyCommand.setParameters(notifyParams);

			notifyCommand.execute();
			log.info("Sending Promotion Notifications -end ---- " + promotionType.toString());
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Object> getPromotionInquiryView(PromotionType promotionType, Integer flightId) throws ModuleException {
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(CommandParamNames.PROMOTION_TYPE, promotionType);
			params.put(CommandParamNames.FLIGHT_ID, flightId);
			params.put(CommandParamNames.OPERATION_CODE, CommandParamNames.OPERATION_APPROVAL_VIEW);
			params.put(CommandParamNames.USER_PRINCPAL, getUserPrincipal());
			
			DefaultBaseCommand inquiryHandler = PromotionsHandlerFactory.getPromotionAdminInquiryHandler(promotionType);
			inquiryHandler.setParameters(params);
			ServiceResponce sr = inquiryHandler.execute();
			return (List<Object>) sr.getResponseParam(CommandParamNames.RESP_APPROVAL_VIEW);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void approveRejectPromotionRequests(PromotionType promotionType, List<HashMap<String, Object>> approvalData)
			throws ModuleException {
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(CommandParamNames.PROMOTION_TYPE, promotionType);
			params.put(CommandParamNames.PROMOTION_REQUEST_CUSTOM, approvalData);
			params.put(CommandParamNames.EVENT_TYPE, CommandParamNames.EVENT_PROMO_APPROVE);
			params.put(CommandParamNames.USER_PRINCPAL, getUserPrincipal());

			DefaultBaseCommand approvalHandler = PromotionsHandlerFactory.getPromotionRequestApprover(promotionType);
			approvalHandler.setParameters(params);
			approvalHandler.execute();
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cleanUpPromotionRequests(PromotionType promotionType) throws ModuleException {
		PromotionAdministrationDao dao = (PromotionAdministrationDao) PromotionsHandlerFactory
				.getPromotionAdministrationDao(promotionType);
		List<Map<String, Object>> promoReqs = dao.getPromotionRequestsToExpire(promotionType);

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(CommandParamNames.PROMOTION_TYPE, promotionType);
		params.put(CommandParamNames.PROMOTION_REQUESTS, promoReqs);
		params.put(CommandParamNames.EVENT_TYPE, CommandParamNames.EVENT_PROMO_EXPIRE);
		params.put(CommandParamNames.USER_PRINCPAL, getUserPrincipal());

		DefaultBaseCommand approvalHandler = PromotionsHandlerFactory.getPromotionRequestApprover(promotionType);
		approvalHandler.setParameters(params);
		approvalHandler.execute();
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page<Object> getAuditView(String flightNo, String status, Date startDate, Date endDate, Integer startRec, Integer size)
			throws ModuleException {

		try {
			NextSeatFreePromotionAdminDao dao = (NextSeatFreePromotionAdminDao) PromotionsHandlerFactory
					.getPromotionAdministrationDao(PromotionType.PROMOTION_NEXT_SEAT_FREE);
			return (Page) dao.getPromotionsApprovalSummaryView(flightNo, status, startDate, endDate, startRec, size);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	/*
	 * ----------------------------------------------------------------------------------------------------
	 * -------------------- Service Delegates of Internal Usages
	 * ----------------------------------------------------------------------------------------------------
	 */

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce approveFlexiDatePromotionRequest(HashMap<String, Object> promoRequest) throws ModuleException {
		try {
			ServiceResponce serviceResponce = new DefaultServiceResponse(true);
			Long versionId;
			Integer pnrSegId;
			Integer flightSegId;
			Map<Integer, Integer> pnrSegAndFltSeg = new HashMap<Integer, Integer>();
			String pnr = (String) promoRequest.get(CommandParamNames.PNR);
			List<Map<String, String>> transferSegs = (List<Map<String, String>>) promoRequest
					.get(CommandParamNames.TRANSFER_SEGMENTS);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			versionId = PromotionModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null).getVersion();

			for (Map<String, String> transferSeg : transferSegs) {
				pnrSegId = Integer.parseInt(transferSeg.get(CommandParamNames.RESERVATION_SEGMENT_ID));
				flightSegId = Integer.parseInt(transferSeg.get(CommandParamNames.FLIGHT_SEGMENT_ID));
				pnrSegAndFltSeg.put(pnrSegId, flightSegId);
			}

			// Reprotect Res Seg
			if (!pnrSegAndFltSeg.isEmpty()) {
				serviceResponce = PromotionModuleServiceLocator.getResSegmentBD().transferReservationSegment(pnr,
						pnrSegAndFltSeg, true, false, versionId, false, null, null, null);
			}

			return serviceResponce;
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void approveNextSeatFreePromotionRequest(PromotionRequest promoRequest, Reservation reservation,
			FlightSegement flightSegment, Map<String, List<FlightSeatStatusTO>> selectedSeatMap, UserPrincipal userPrincipal) throws ModuleException {

		try {
			Integer paxSequence = null;
			String carrierCode = null;

			Map<String, List<LCCSelectedSegmentAncillaryDTO>> selectecLCCAnciSegDTOMap = PromotionsDaoHelper
					.constructSeatAncilaries(selectedSeatMap, reservation, promoRequest, flightSegment);

			if (selectecLCCAnciSegDTOMap != null) {
				for (LCCSelectedSegmentAncillaryDTO seg : selectecLCCAnciSegDTOMap.get(PromotionsInternalConstants.ADD)) {
					paxSequence = PaxTypeUtils.getPaxSeq(seg.getTravelerRefNumber());
					carrierCode = seg.getCarrierCode();
					break;
				}
			}

			CommonAncillaryModifyAssembler anciAssembler = new CommonAncillaryModifyAssembler();
			anciAssembler.setPnr(promoRequest.getPnr());
			anciAssembler.setVersion(Long.toString(reservation.getVersion()));
			anciAssembler.setTargetSystem(SYSTEM.AA);
			anciAssembler.setTransactionIdentifier(null);
			anciAssembler.setTemporyPaymentMap(null);
			anciAssembler.getLccSegments();

			anciAssembler.addAncillary(paxSequence, selectecLCCAnciSegDTOMap.get(PromotionsInternalConstants.ADD));
			anciAssembler.removeAncillary(paxSequence, selectecLCCAnciSegDTOMap.get(PromotionsInternalConstants.REMOVE));

			CommonReservationContactInfo contactInfo = PromotionsDaoHelper.transFormContactInfo(reservation.getContactInfo());

			TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
			trackInfoDTO.setCarrierCode(carrierCode);

			PromotionModuleServiceLocator.getAirproxyReservationBD().updateAncillary(anciAssembler,
					AppSysParamsUtil.isFraudCheckEnabled(userPrincipal.getSalesChannel()), contactInfo, false, null,
					trackInfoDTO);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BigDecimal processNextSeatChargeAndRefund(PromotionRequestApproveTo promoReq, BigDecimal refund,
			List<String> segCodes, Reservation reservation) throws ModuleException {

		DefaultBaseCommand refundHandler = PromotionsHandlerFactory
				.getPromotionChargesRefundsHandler(PromotionType.PROMOTION_NEXT_SEAT_FREE);
		refundHandler.setParameter(CommandParamNames.PROMO_PROCESS_CATEGORY, CommandParamNames.PROCESS_REFUNDS);
		refundHandler.setParameter(CommandParamNames.PROMOTION_REQUEST, promoReq.getPromotionRequest());
		// refundHandler.setParameter(CommandParamNames.FLIGHT_SEGMENT_CODE, flightSegment.getSegmentCode());
		refundHandler.setParameter(CommandParamNames.FLIGHT_SEGMENT_CODES, segCodes);
		refundHandler.setParameter(CommandParamNames.REQUEST_TYPE, CommandParamNames.REQUEST_EXECUTE);
		ServiceResponce resp = refundHandler.execute();

		refund = (BigDecimal) resp.getResponseParam(CommandParamNames.TOTAL_CHARGES_REFUNDS);

		// Refund the Seat Charge
		LCCClientReservationPax lccPax = PromotionModuleServiceLocator.getAirproxyPassengerBD().loadPassenger(
				reservation.getPnr(), promoReq.getReservationPaxId(), null, false);

		IPassenger iPassenger = new PassengerAssembler(null);
		IPayment ipaymentPassenger = new PaymentAssembler();
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setPnr(reservation.getPnr());
		Reservation res = ReservationProxy.getReservation(pnrModesDTO);

		// FIXME ----
		LCCClientPaymentHolder payHolder = lccPax.getLccClientPaymentHolder();

		List<CommonCreditCardPaymentInfo> cardPayments = new ArrayList<CommonCreditCardPaymentInfo>();
		for (LCCClientPaymentInfo objPay : payHolder.getPayments()) {
			if (objPay instanceof CommonCreditCardPaymentInfo) {
				cardPayments.add((CommonCreditCardPaymentInfo) objPay);
			}
		}
		Collections.sort(cardPayments, new Comparator<CommonCreditCardPaymentInfo>() {
			@Override
			public int compare(CommonCreditCardPaymentInfo o1, CommonCreditCardPaymentInfo o2) {
				return o1.getTxnDateTime().compareTo(o2.getTxnDateTime());
			}
		});
		CommonCreditCardPaymentInfo payInfo = cardPayments.get(cardPayments.size() - 1);

		String paymentGatewayName = PromotionModuleServiceLocator.getPaymentBrokerBD().getPaymentGatewayNameForCCTransaction(
				Integer.valueOf(payInfo.getOriginalPayReference()), true);

		if (paymentGatewayName == null) {
			throw new ModuleException("paymentbroker.refund.missingcardpayment");
		}
		Integer ipgId = new Integer(paymentGatewayName.split("_")[0]);
		String currency = paymentGatewayName.split("_")[1];
		IPGIdentificationParamsDTO ipg = new IPGIdentificationParamsDTO(ipgId, currency);

		ReservationBD reservationBD = PromotionModuleServiceLocator.getReservationBD();

		Collection<Integer> colTnxId = new ArrayList<Integer>();
		colTnxId.add(Integer.valueOf(payInfo.getOriginalPayReference()));
		Map mapCreditCardDetail = reservationBD.getCreditCardInfo(colTnxId, true);
		CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) mapCreditCardDetail.get(Integer.valueOf(payInfo
				.getOriginalPayReference()));

		try {
			ipaymentPassenger.addCardPayment(payInfo.getType(), payInfo.geteDate(), payInfo.getNo(), payInfo.getName(),
					payInfo.getAddress(), payInfo.getSecurityCode(), refund.negate(), AppIndicatorEnum.APP_XBE,
					TnxModeEnum.MAIL_TP_ORDER, cardPaymentInfo.getCcdId(), ipg, payInfo.getCarrierCode(),
					payInfo.getPayCurrencyDTO(), payInfo.getLccUniqueTnxId(), payInfo.getAuthorizationId(),
					payInfo.getPaymentBrokerId(), Integer.valueOf(payInfo.getOriginalPayReference()));

			iPassenger.addPassengerPayments(promoReq.getReservationPaxId(), ipaymentPassenger);

			PromotionModuleServiceLocator.getPassengerBD().passengerRefund(reservation.getPnr(), iPassenger,
					PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_SEAT_CHRG.getRefundDescription(), true,
					res.getVersion(), trackInfoDTO, false, null, null, false);
			// TODO validate whether last two arguments are correct
		} catch (Exception e) {
			// Should not break the credit giving even if refund fails
			log.error("Credit card refund failed for pnr_pax_id in Next seat promotion : " + promoReq.getReservationPaxId(), e);
		}

		return refund;
	}

}
