package com.isa.thinair.promotion.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.bundledfare.ApplicableBundledFareSelectionCriteria;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.BundleFareDescriptionTemplate;
import com.isa.thinair.promotion.api.model.BundledFare;
import com.isa.thinair.promotion.api.model.BundledFareFreeService;
import com.isa.thinair.promotion.api.model.BundledFarePeriod;
import com.isa.thinair.promotion.api.model.BundledFarePeriodImage;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareConfigurationTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplayDTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareFreeServiceTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFarePeriodTO;
import com.isa.thinair.promotion.api.to.constants.BundledFareConstants;
import com.isa.thinair.promotion.api.utils.BundleFareDescriptionTemplateUtils;

/**
 * Helper class to transform Bundled Fare related data types
 * 
 * @author rumesh
 * 
 */
public class BundledFareHelper {
	
	private static final Log log = LogFactory.getLog(BundledFareHelper.class);

	public static BundledFare transformDTOtoModel(BundledFareConfigurationTO bundledFareTO) {
		BundledFare bundledFare = new BundledFare();
		bundledFare.setAgents(bundledFareTO.getAgents());
		bundledFare.setBookingClasses(bundledFareTO.getBookingClasses());
		bundledFare.setBundledFareId(bundledFareTO.getId());
		bundledFare.setBundledFareName(bundledFareTO.getBundledFareName());
		bundledFare.setDefaultBundledFee(bundledFareTO.getBundleFee());
		bundledFare.setChargeCode(bundledFareTO.getChargeCode());
		bundledFare.setFlightDateFrom(bundledFareTO.getFlightDateFrom());
		bundledFare.setFlightDateTo(bundledFareTO.getFlightDateTo());
		bundledFare.setFlightNumbers(bundledFareTO.getFlightNumbers());
		bundledFare.setBundledFarePeriods(transformToBundledFarePeriodsModelSet(bundledFareTO.getBundledFarePeriods()));
		bundledFare.setOndCodes(bundledFareTO.getOndCodes());
		bundledFare.setStatus(bundledFareTO.getStatus() ? BundledFare.ACTIVE : BundledFare.INACTIVE);
		bundledFare.setVersion(bundledFareTO.getVersion());
		bundledFare.setApplicableSalesChannels(bundledFareTO.getApplicableSalesChannels());
		bundledFare.setCutOffTime(bundledFareTO.getCutOffTime());
		bundledFare.setBundledCategoryID(bundledFareTO.getBundledCategoryID());
		bundledFare.setDefaultBundled(bundledFareTO.getDefaultBundled());
		bundledFare.setPointOfSale(bundledFareTO.getPointOfSale());
		return bundledFare;
	}

	private static Set<BundledFarePeriod> transformToBundledFarePeriodsModelSet(Set<BundledFarePeriodTO> bundledFarePeriods) {
		Set<BundledFarePeriod> bundleFarePeriods = null;
		if (bundledFarePeriods != null && !bundledFarePeriods.isEmpty()) {
			bundleFarePeriods = new HashSet<BundledFarePeriod>();
			for (Iterator<BundledFarePeriodTO> iterator = bundledFarePeriods.iterator(); iterator.hasNext();) {
				BundledFarePeriodTO bundledFarePeriodTO = iterator.next();
				bundleFarePeriods.add(transformToBundledFarePeriodModel(bundledFarePeriodTO));
			}
		}
		return bundleFarePeriods;
	}

	private static BundledFarePeriod transformToBundledFarePeriodModel(BundledFarePeriodTO bundledFarePeriodTO) {
		BundledFarePeriod bundledFarePeriod = new BundledFarePeriod();
		bundledFarePeriod.setBundleFarePeriodId(bundledFarePeriodTO.getBundleFarePeriodId());
		bundledFarePeriod.setFlightDateFrom(bundledFarePeriodTO.getFlightFrom());
		bundledFarePeriod.setFlightDateTo(bundledFarePeriodTO.getFlightTo());
		bundledFarePeriod.setBundledFee(bundledFarePeriodTO.getBundleFee());
		bundledFarePeriod.setBundledFareServices(transformToFreeServieModelSet(bundledFarePeriodTO.getBundledServices()));
		bundledFarePeriod.setVersion(bundledFarePeriodTO.getVersion());
		return bundledFarePeriod;
	}

	public static Set<BundledFareFreeService> transformToFreeServieModelSet(Set<BundledFareFreeServiceTO> freeServiceDTOs) {
		Set<BundledFareFreeService> freeServices = null;
		if (freeServiceDTOs != null && !freeServiceDTOs.isEmpty()) {
			freeServices = new HashSet<BundledFareFreeService>();
			for (BundledFareFreeServiceTO bundledFareFreeServiceTO : freeServiceDTOs) {
				freeServices.add(transformToFreeServiceModel(bundledFareFreeServiceTO));
			}
		}
		return freeServices;
	}

	public static BundledFareFreeService transformToFreeServiceModel(BundledFareFreeServiceTO freeServiceTO) {
		BundledFareFreeService freeService = new BundledFareFreeService();
		freeService.setFreeServiceId(freeServiceTO.getId());
		freeService.setServiceName(freeServiceTO.getServiceName());
		freeService.setTemplateId(freeServiceTO.getTemplateId());
		freeService.setIncludedFreeServices(freeServiceTO.getIncludedFreeServices());
		freeService.setAllowMultipleSelect(freeServiceTO.isAllowMultipleSelect());
		freeService.setVersion(freeServiceTO.getVersion());
		return freeService;
	}

	public static Page<BundledFareConfigurationTO> transformToBundledFarePageDTO(Page<BundledFare> pageBundleFares) {
		Page<BundledFareConfigurationTO> pageBundlePageTo = new Page<BundledFareConfigurationTO>(
				pageBundleFares.getTotalNoOfRecords(), pageBundleFares.getStartPosition(), pageBundleFares.getEndPosition(),
				transformToBundledFareToCollectin(pageBundleFares.getPageData()));

		return pageBundlePageTo;
	}

	public static Collection<BundledFareConfigurationTO> transformToBundledFareToCollectin(Collection<BundledFare> bundledFares) {
		Collection<BundledFareConfigurationTO> bundledFareConfigurationTOs = new ArrayList<BundledFareConfigurationTO>();
		for (BundledFare bundledFare : bundledFares) {
			bundledFareConfigurationTOs.add(transformToBundledFareConfigurationTo(bundledFare));
		}
		return bundledFareConfigurationTOs;
	}

	public static BundledFareConfigurationTO transformToBundledFareConfigurationTo(BundledFare bundledFare) {
		BundledFareConfigurationTO bundledFareConfigurationTO = new BundledFareConfigurationTO();
		bundledFareConfigurationTO.setId(bundledFare.getBundledFareId());
		bundledFareConfigurationTO.setBundledFareName(bundledFare.getBundledFareName());
		bundledFareConfigurationTO.setBookingClasses(bundledFare.getBookingClasses());
		bundledFareConfigurationTO.setFlightDateFrom(bundledFare.getFlightDateFrom());
		bundledFareConfigurationTO.setFlightDateTo(bundledFare.getFlightDateTo());
		bundledFareConfigurationTO.setOndCodes(bundledFare.getOndCodes());
		bundledFareConfigurationTO.setAgents(bundledFare.getAgents());
		bundledFareConfigurationTO.setFlightNumbers(bundledFare.getFlightNumbers());
		bundledFareConfigurationTO.setChargeCode(bundledFare.getChargeCode());
		bundledFareConfigurationTO.setBundleFee(bundledFare.getDefaultBundledFee());
		bundledFareConfigurationTO.setStatus(BundledFare.ACTIVE.equals(bundledFare.getStatus()));
		bundledFareConfigurationTO.setVersion(bundledFare.getVersion());
		bundledFareConfigurationTO.setBundledFarePeriods(transformToBundleFarePeriodsDTOSet(bundledFare.getBundledFarePeriods()));
		bundledFareConfigurationTO.setApplicableSalesChannels(bundledFare.getApplicableSalesChannels());
		bundledFareConfigurationTO.setCutOffTime(bundledFare.getCutOffTime());
		bundledFareConfigurationTO.setBundledCategoryID(bundledFare.getBundledCategoryID());
		bundledFareConfigurationTO.setDefaultBundled(bundledFare.getDefaultBundled());
		bundledFareConfigurationTO.setPointOfSale(bundledFare.getPointOfSale());
		return bundledFareConfigurationTO;
	}

	private static Set<BundledFarePeriodTO> transformToBundleFarePeriodsDTOSet(Set<BundledFarePeriod> bundledFarePeriods) {
		Set<BundledFarePeriodTO> bundleFarePeriodsTOs = new HashSet<BundledFarePeriodTO>();
		if (bundledFarePeriods != null) {
			for (BundledFarePeriod bundledFarePeriod : bundledFarePeriods) {
				bundleFarePeriodsTOs.add(transformToBundleFarePeriodsDTO(bundledFarePeriod));
			}
		}
		return bundleFarePeriodsTOs;
	}

	private static BundledFarePeriodTO transformToBundleFarePeriodsDTO(BundledFarePeriod bundledFarePeriod) {
		BundledFarePeriodTO bundledFarePeriodTO = new BundledFarePeriodTO();
		bundledFarePeriodTO.setBundleFarePeriodId(bundledFarePeriod.getBundleFarePeriodId());
		bundledFarePeriodTO.setFlightFrom(bundledFarePeriod.getFlightDateFrom());
		bundledFarePeriodTO.setFlightTo(bundledFarePeriod.getFlightDateTo());
		bundledFarePeriodTO.setBundleFee(bundledFarePeriod.getBundledFee());
		bundledFarePeriodTO.setBundledServices(transformToFreeServiceDTOSet(bundledFarePeriod.getBundledFareServices()));
		bundledFarePeriodTO.setVersion(bundledFarePeriod.getVersion());
		return bundledFarePeriodTO;
	}

	private static Set<BundledFareFreeServiceTO>
			transformToFreeServiceDTOSet(Set<BundledFareFreeService> bundledFareFreeServices) {
		Set<BundledFareFreeServiceTO> bundledFareFreeServiceTOs = new HashSet<BundledFareFreeServiceTO>();
		if (bundledFareFreeServices != null) {
			for (BundledFareFreeService bundledFareFreeService : bundledFareFreeServices) {
				bundledFareFreeServiceTOs.add(transformToFreeServiceDTO(bundledFareFreeService));
			}
		}
		return bundledFareFreeServiceTOs;
	}

	private static BundledFareFreeServiceTO transformToFreeServiceDTO(BundledFareFreeService bundledFareFreeService) {
		BundledFareFreeServiceTO bundledFareFreeServiceTO = new BundledFareFreeServiceTO();
		bundledFareFreeServiceTO.setId(bundledFareFreeService.getFreeServiceId());
		bundledFareFreeServiceTO.setServiceName(bundledFareFreeService.getServiceName());
		bundledFareFreeServiceTO.setTemplateId(bundledFareFreeService.getTemplateId());
		bundledFareFreeServiceTO.setIncludedFreeServices(bundledFareFreeService.getIncludedFreeServices());
		bundledFareFreeServiceTO.setAllowMultipleSelect(bundledFareFreeService.getAllowMultipleSelect());
		bundledFareFreeServiceTO.setVersion(bundledFareFreeService.getVersion());
		return bundledFareFreeServiceTO;
	}
	
	public static Collection<String> getSelectedPeriodI18nMessageKeys(BundledFare bundleFare) {
		Collection<String> translationKeys = new HashSet<String>();
		BundledFarePeriod selectedBundledFarePeriod = bundleFare.getSelectedBundledFarePeriod();
		if (selectedBundledFarePeriod != null) {
			if (!StringUtil.isNullOrEmpty(selectedBundledFarePeriod.getTranslationNameKey())) {
				translationKeys.add(selectedBundledFarePeriod.getTranslationNameKey());
			}
			if (!StringUtil.isNullOrEmpty(selectedBundledFarePeriod.getTranslationDecriptionKey())) {
				translationKeys.add(selectedBundledFarePeriod.getTranslationDecriptionKey());
			}
		}
		return translationKeys;
	}

	public static List<BundledFare> filterBundledFares(List<BundledFare> bundledFares,
			ApplicableBundledFareSelectionCriteria bundledFareSelectionCriteria) {

		Set<String> bookingClasses = bundledFareSelectionCriteria.getBookingClasses();
		Set<String> flightNumbers = bundledFareSelectionCriteria.getFlightNumbers();
		String agentCode = bundledFareSelectionCriteria.getAgentCode();
		Map<Integer, BundledFare> nonDuplicateBundleCategories = new HashMap<Integer, BundledFare>();

		for (BundledFare bundledFare : bundledFares) {
			if (isListElementsMatched(bundledFare.getBookingClasses(), bookingClasses)
					&& isListElementsMatched(bundledFare.getFlightNumbers(), flightNumbers)
					&& isAgentMatched(bundledFare, agentCode)
					&& isValidChargeRateExists(bundledFare.getChargeCode(),
							bundledFareSelectionCriteria.getFirstDepartureFlightDate())) {
				if (bundledFare.getBookingClasses() != null && !bundledFare.getBookingClasses().isEmpty()) {
					// Override by booking classes selected by fare quote
					bundledFare.setBookingClasses(bookingClasses);
				}

				if (nonDuplicateBundleCategories.containsKey(bundledFare.getBundledCategoryID())) {
					if (AccelAeroCalculator.isGreaterThan(bundledFare.getEffectiveBundleFee(),
							nonDuplicateBundleCategories.get(bundledFare.getBundledCategoryID()).getEffectiveBundleFee())) {
						nonDuplicateBundleCategories.put(bundledFare.getBundledCategoryID(), bundledFare);
					} else if (AccelAeroCalculator.isEqual(bundledFare.getEffectiveBundleFee(),
							nonDuplicateBundleCategories.get(bundledFare.getBundledCategoryID()).getEffectiveBundleFee())) {
						if ((nonDuplicateBundleCategories.get(bundledFare.getBundledCategoryID()).getCreatedDate() != null && bundledFare
								.getCreatedDate() != null)
								&& nonDuplicateBundleCategories.get(bundledFare.getBundledCategoryID()).getCreatedDate()
										.before(bundledFare.getCreatedDate())) {
							nonDuplicateBundleCategories.put(bundledFare.getBundledCategoryID(), bundledFare);
						}
					}
				} else {
					nonDuplicateBundleCategories.put(bundledFare.getBundledCategoryID(), bundledFare);
				}
			}
		}

		return new ArrayList<BundledFare>(nonDuplicateBundleCategories.values());
	}

	public static List<BundledFareLiteDTO> transformToLiteDTOs(List<BundledFare> bundledFares,
			Map<String, Map<String, String>> translations, String preferredLanguage, BundledFare defaultBundled) {

		List<BundledFareLiteDTO> bundledFareLiteDTOs = new ArrayList<BundledFareLiteDTO>();
		for (BundledFare bundledFare : bundledFares) {
			BundledFareLiteDTO tranformedLiteDTO = transformToLiteDTO(bundledFare, translations, preferredLanguage,
					defaultBundled);
			bundledFareLiteDTOs.add(tranformedLiteDTO);
		}

		return bundledFareLiteDTOs;
	}

	public static BundledFareLiteDTO transformToLiteDTO(BundledFare bundledFare, Map<String, Map<String, String>> translations,
			String preferredLanguage, BundledFare defaultBundled) {

		BundledFareLiteDTO bundledFareLiteDTO = new BundledFareLiteDTO();
		bundledFareLiteDTO.setBundleFarePeriodId(bundledFare.getSelectedBundlePeriodId());
		bundledFareLiteDTO.setBundledFareName(bundledFare.getBundledFareName());

		BundledFarePeriod bundledFarePeriod = bundledFare.getSelectedBundledFarePeriod();
		if (bundledFarePeriod != null) {
			// override bundle fare name from from preferred language
			String preferredLangName = getPreferredLanguageTranslation(translations, bundledFarePeriod.getTranslationNameKey(),
					preferredLanguage);
			if (!StringUtil.isNullOrEmpty(preferredLangName)) {
				bundledFareLiteDTO.setBundledFareName(preferredLangName);
			}

			String preferredLangDescription = getPreferredLanguageTranslation(translations,
					bundledFarePeriod.getTranslationDecriptionKey(), preferredLanguage);
			bundledFareLiteDTO.setDescription(preferredLangDescription);
		}

		bundledFareLiteDTO.setBookingClasses(bundledFare.getBookingClasses());
		bundledFareLiteDTO.setFlexiIncluded(bundledFare.isServiceIncluded(EXTERNAL_CHARGES.FLEXI_CHARGES.toString()));
		bundledFareLiteDTO.setPerPaxBundledFee(bundledFare.getEffectiveBundleFee());
		bundledFareLiteDTO.setFreeServices(bundledFare.getSelectedServiceNames());
		bundledFareLiteDTO.setImagesUrlMap(getSelectedPeriodImagesUrlMap(bundledFare));
		bundledFareLiteDTO.setImageUrl(preferredLanguage);
		bundledFareLiteDTO.setPriority(bundledFare.getBundledFareCategory().getPriority());
		bundledFareLiteDTO.setBundleFareDescriptionTemplateDTO(getSelectedPeriodFareDescriptionTemplateDTO(bundledFare));
		
		if (defaultBundled != null && bundledFare.getSelectedBundledFarePeriod() == defaultBundled.getSelectedBundledFarePeriod()) {
			bundledFareLiteDTO.setDefault(true);
		}

		return bundledFareLiteDTO;
	}
	
	/**
	 * Due to preserve performance - only required OBJECT models are loaded on demand
	 */
	public static Map<String, String> getSelectedPeriodImagesUrlMap(BundledFare bundledFare) {
		BundledFarePeriod bundledFarePeriod = bundledFare.getSelectedBundledFarePeriod();
		Map<String, String> imagesUrlMap = null;
		
		if (bundledFarePeriod != null) {
			imagesUrlMap = new HashMap<String, String>();
			if (isImagesExists(bundledFarePeriod.getPeriodImageIds())) {
				Set<Integer> bundleFarePeriodIds = new HashSet<Integer>();
				bundleFarePeriodIds.add(bundledFarePeriod.getBundleFarePeriodId());
				List<BundledFarePeriodImage> periodImages = PromotionsHandlerFactory.getBundledFareDisplaySettingsDao()
						.getBundledFarePeriodImages(bundleFarePeriodIds);
				if (periodImages != null & !periodImages.isEmpty()) {
					for (BundledFarePeriodImage periodImage : periodImages) {
						String languageCode = periodImage.getLanguageCode();
						String imageUrl = periodImage.getImageUrl();
						imagesUrlMap.put(languageCode, imageUrl);
					}
				}
			}
		}
		return imagesUrlMap;
	}
	
	
	public static boolean isImagesExists(Set<Integer> periodImageIds) {
		return periodImageIds != null && !periodImageIds.isEmpty();
	}

	public static BundleFareDescriptionTemplateDTO getSelectedPeriodFareDescriptionTemplateDTO(BundledFare bundledFare) {
		BundleFareDescriptionTemplateDTO templateDTO = null;
		BundledFarePeriod bundledFarePeriod = bundledFare.getSelectedBundledFarePeriod();
		if (bundledFarePeriod != null) {
			Integer descriptionTemplateId = bundledFarePeriod.getDescriptionTemplateId();
			if (descriptionTemplateId != null) {
				BundleFareDescriptionTemplate template = PromotionsHandlerFactory.getBundleFareDescriptionDAO()
						.getBundleFareDescriptionTemplateById(descriptionTemplateId);
				if (template != null) {
					templateDTO = BundleFareDescriptionTemplateUtils.toBundleFareDescriptionTemplateDTO(template);
				}
			}
		}

		return templateDTO;
	}

	public static BundledFareDTO transformToBundledFareDTO(BundledFare bundledFare) {
		BundledFareDTO bundledFareDTO = new BundledFareDTO();
		bundledFareDTO.setBookingClasses(bundledFare.getBookingClasses());
		bundledFareDTO.setBundledFarePeriodId(bundledFare.getSelectedBundlePeriodId());
		bundledFareDTO.setBundledFareName(bundledFare.getBundledFareName());
		bundledFareDTO.setPerPaxBundledFee(bundledFare.getEffectiveBundleFee());
		bundledFareDTO.setChargeCode(bundledFare.getChargeCode());
		bundledFareDTO.setApplicableServices(transformToFreeServiceDTOSet(bundledFare.getSelectedBundledFareServices()));
		bundledFareDTO.setImagesUrlMap(getSelectedPeriodImagesUrlMap(bundledFare));
		bundledFareDTO.setBundleFareDescriptionTemplateDTO(getSelectedPeriodFareDescriptionTemplateDTO(bundledFare));
		return bundledFareDTO;
	}

	private static String getPreferredLanguageTranslation(Map<String, Map<String, String>> translations, String messageKey,
			String preferredLanguage) {

		if (messageKey != null && translations != null && !translations.isEmpty() && translations.containsKey(messageKey)) {
			Map<String, String> languageDescriptions = translations.get(messageKey);
			if (languageDescriptions != null && languageDescriptions.containsKey(preferredLanguage)) {
				return languageDescriptions.get(preferredLanguage);
			}
		}

		return "";
	}

	private static boolean isListElementsMatched(Set<String> configuredValues, Set<String> listValues) {
		if (configuredValues == null || configuredValues.isEmpty()) {
			return true;
		} else if (configuredValues.containsAll(listValues)) {
			return true;
		}

		return false;
	}

	private static boolean isAgentMatched(BundledFare bundledFare, String agentCode) {
		if (bundledFare.getAgents() == null || bundledFare.getAgents().isEmpty()) {
			return true;
		} else if (bundledFare.getAgents().contains(agentCode)) {
			return true;
		}

		return false;
	}

	private static boolean isValidChargeRateExists(String chargeCode, Date flightDate) {
		try {
			Charge charge = PromotionModuleServiceLocator.getChargeBD().getCharge(chargeCode);
			if (charge != null && Charge.STATUS_ACTIVE.equals(charge.getStatus()) && charge.getChargeRates() != null
					&& !charge.getChargeRates().isEmpty()) {
				Set<ChargeRate> chargeRates = charge.getChargeRates();
				for (ChargeRate chargeRate : chargeRates) {
					if (ChargeRate.Status_Active.equals(chargeRate.getStatus())
							&& CalendarUtil.isBetween(flightDate, chargeRate.getChargeEffectiveFromDate(),
									chargeRate.getChargeEffectiveToDate())
							&& CalendarUtil.isBetween(CalendarUtil.getCurrentSystemTimeInZulu(), chargeRate.getSalesFromDate(),
									chargeRate.getSaleslToDate())) {
						return true;
					}
				}
			}
		} catch (ModuleException e) {
			return false;
		}
		return false;
	}

	public static BundledFare getDefaultBundle(List<BundledFare> bundledFares) {
		BundledFare defaultBundledFare = null;
		for (BundledFare bundledFare : bundledFares) {
			if (CommonsConstants.YES.equals(bundledFare.getDefaultBundled())) {
				if (defaultBundledFare == null
						|| (defaultBundledFare.getBundledFareCategory().getPriority() > bundledFare.getBundledFareCategory()
								.getPriority())) {
					defaultBundledFare = bundledFare;
				}
			}
		}
		return defaultBundledFare;
	}

	public static Map<String, String> transformToBundledFarePeriodImagesMap(List<BundledFarePeriodImage> bundledFarePeriodImages) {
		Map<String, String> transformedImages = new HashMap<String, String>();
		if (bundledFarePeriodImages != null && !bundledFarePeriodImages.isEmpty()) {
			for (BundledFarePeriodImage bundledFarePeriodImage : bundledFarePeriodImages) {
				String languageCode = bundledFarePeriodImage.getLanguageCode();
				String imageUrl = bundledFarePeriodImage.getImageUrl();
				transformedImages.put(languageCode, imageUrl);
			}
		}
		return transformedImages;
	}

	/**
	 * Transform to front-end friendly translations
	 * 
	 * @param bundledFareDisplayDTO
	 * @param bundledFarePeriod
	 * @throws ModuleException
	 */
	public static void fillTranslations(BundledFareDisplayDTO bundledFareDisplayDTO, BundledFarePeriod bundledFarePeriod)
			throws ModuleException {

		if (bundledFarePeriod != null) {
			String nameKey = bundledFarePeriod.getTranslationNameKey();
			String descKey = bundledFarePeriod.getTranslationDecriptionKey();

			Collection<String> msgKeys = new ArrayList<String>();
			msgKeys.add(nameKey);
			msgKeys.add(descKey);

			Map<String, Map<String, String>> translatedMessages = null;
			if (msgKeys != null && !msgKeys.isEmpty()) {
				translatedMessages = PromotionModuleServiceLocator.getCommonMasterBD().getTranslatedMessagesForKeys(msgKeys);
			}

			Map<String, Map<String, String>> transformedMessages = new HashMap<String, Map<String, String>>();
			transformTranslations(transformedMessages, translatedMessages, nameKey, BundledFareConstants.DisplayKeys.name);
			transformTranslations(transformedMessages, translatedMessages, descKey, BundledFareConstants.DisplayKeys.description);

			bundledFareDisplayDTO.setTranslations(transformedMessages);
		}

	}

	private static void transformTranslations(Map<String, Map<String, String>> transformedMessages,
			Map<String, Map<String, String>> translatedMessages, String msgKey, String transformedKey) {

		if (translatedMessages != null && translatedMessages.containsKey(msgKey)) {
			Map<String, String> contentHolder = translatedMessages.get(msgKey);
			if (contentHolder != null && !contentHolder.isEmpty()) {
				for (String language : contentHolder.keySet()) {
					String translation = contentHolder.get(language);

					if (translation != null && !translation.isEmpty()) {
						if (!transformedMessages.containsKey(language)) {
							transformedMessages.put(language, new HashMap<String, String>());
						}
						Map<String, String> translations = transformedMessages.get(language);
						translations.put(transformedKey, translation);
					}

				}
			}
		}
	}

	public static void removeUnMatchingBundleServicePeriods(BundledFare bundledFare, Integer bundledFarePeriodId) {
		if (bundledFare != null) {
			Set<BundledFarePeriod> servicePeriods = bundledFare.getBundledFarePeriods();
			if (servicePeriods != null && !servicePeriods.isEmpty()) {
				Iterator<BundledFarePeriod> periodsIterator = servicePeriods.iterator();
				while (periodsIterator.hasNext()) {
					BundledFarePeriod bundleFarePeriod = periodsIterator.next();
					if (!bundleFarePeriod.getBundleFarePeriodId().equals(bundledFarePeriodId)) {
						periodsIterator.remove();
					}
				}
				Set<BundledFarePeriod> bundleFarePeriods = bundledFare.getBundledFarePeriods();
				if (bundleFarePeriods != null && !bundleFarePeriods.isEmpty()) {
					if (bundleFarePeriods.size() > 1) {
						//ideally this should not happen
						log.error("Inconsistent (multiple templates) found for the bundle fare period id: " + bundledFarePeriodId);
					}
					bundledFare.setPeriodSelected(true);
				} else {
					//ideally this should not happen
					log.error("Inconsistent (no templates) date found for the bundle fare period id: " + bundledFarePeriodId);
				}
			}
		}
	}

	public static void removeUnMatchingBundleServicePeriods(BundledFare bundledFare, Date requiredDate) {
		if (bundledFare != null) {
			Set<BundledFarePeriod> servicePeriods = bundledFare.getBundledFarePeriods();
			if (servicePeriods != null && !servicePeriods.isEmpty()) {
				Iterator<BundledFarePeriod> periodsIterator = servicePeriods.iterator();
				while (periodsIterator.hasNext()) {
					BundledFarePeriod bundleFarePeriod = periodsIterator.next();
					if (!CalendarUtil.isBetweenIncludingLimits(requiredDate, bundleFarePeriod.getFlightDateFrom(),
							bundleFarePeriod.getFlightDateTo())) {
						periodsIterator.remove();
					}
				}
				Set<BundledFarePeriod> filteredPeriods = bundledFare.getBundledFarePeriods();
				if (filteredPeriods != null && !filteredPeriods.isEmpty()) {
					if (filteredPeriods.size() > 1) {
						//ideally this should not happen
						log.error("Inconsistent (multiple templates) found on bundled fare id " + bundledFare.getBundledFareId());
					}
					bundledFare.setPeriodSelected(true);
				} else {
					//ideally this should not happen
					log.error("Inconsistent (no templates) data found on bundled fare id " + bundledFare.getBundledFareId());
				}
			}
		}
	}

	public static void removeUnMatchingBundleServicePeriods(List<BundledFare> bundledFares, Date requiredDate) {
		Iterator<BundledFare> iterartor = bundledFares.iterator();
		while (iterartor.hasNext()) {
			BundledFare bundledFare = iterartor.next();
			BundledFareHelper.removeUnMatchingBundleServicePeriods(bundledFare, requiredDate);
		}
	}

}
