package com.isa.thinair.promotion.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.google.common.collect.Sets;
import com.isa.thinair.airproxy.api.utils.AnciOfferAuditCreator;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.model.AnciOfferCriteria;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;
import com.isa.thinair.promotion.core.service.bd.AnciOfferBDLocal;
import com.isa.thinair.promotion.core.service.bd.AnciOfferBDRemote;
import com.isa.thinair.promotion.core.util.AnciOfferHelper;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

/**
 * Implementation of the AnciOfferBD interface.
 * 
 * @author thihara
 * 
 */
@Stateless
@RemoteBinding(jndiBinding = "AnciOfferManager.remote")
@LocalBinding(jndiBinding = "AnciOfferManager.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AnciOfferManager extends PlatformBaseSessionBean implements AnciOfferBDLocal, AnciOfferBDRemote {

	private static final Log log = LogFactory.getLog(AnciOfferManager.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page<AnciOfferCriteriaTO> searchAnciOfferCriterias(AnciOfferCriteriaSearchTO searchCriteriaTO, Integer startPosition,
			Integer recSize) throws ModuleException {

		try {
			Page<AnciOfferCriteria> resultPage = PromotionsHandlerFactory.getAnciOfferCriteriaDAO().searchAnciOfferCriterias(
					searchCriteriaTO, startPosition, recSize);

			Collection<String> allI18nKeys = AnciOfferHelper.getI18nMessageKeys(resultPage.getPageData());

			Map<String, Map<String, String>> translatedMessages = PromotionModuleServiceLocator.getCommonMasterBD()
					.getTranslatedMessagesForKeys(allI18nKeys);

			Page<AnciOfferCriteriaTO> transformedPage = AnciOfferHelper.convertPageToDTO(resultPage);

			AnciOfferHelper.setTranslatedMessages(transformedPage.getPageData(), translatedMessages);

			return transformedPage;
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateAnciOfferCriteria(AnciOfferCriteriaTO anciOfferCriteriaTO, TrackInfoDTO trackingInfo,
			UserPrincipal userPrincipal, boolean onlyUpdateName) throws ModuleException {

		try {
			AnciOfferCriteria oldAnciOffer = PromotionsHandlerFactory.getAnciOfferCriteriaDAO().getAnciOferByID(
					anciOfferCriteriaTO.getId());

			AnciOfferCriteria anciOffer = onlyUpdateName ? oldAnciOffer : AnciOfferHelper
					.transformToAnciOfferCriteria(anciOfferCriteriaTO);
			anciOffer.setUserDetails(userPrincipal);

			String nameI18nKey = I18nTranslationUtil.getI18nAnciOfferNameKey(anciOffer.getId());
			String descI18nKey = I18nTranslationUtil.getI18nAnciOfferDescKey(anciOffer.getId());

			// These don't change when editing, so entering these values here.
			anciOffer.setAnciOfferNameI18nMessageKey(nameI18nKey);
			anciOffer.setAnciOfferDescriptionI18nMessageKey(descI18nKey);

			Map<String, Map<String, String>> translationMap = new HashMap<String, Map<String, String>>();
			translationMap.put(nameI18nKey, anciOfferCriteriaTO.getNameTranslations());
			translationMap.put(descI18nKey, anciOfferCriteriaTO.getDescriptionTranslations());

			PromotionModuleServiceLocator.getCommonMasterBD().saveTranslations(translationMap);

			if (!onlyUpdateName) {
				PromotionsHandlerFactory.getAnciOfferCriteriaDAO().updateAnciOfferCriteria(anciOffer);
			}

			Audit audit = AnciOfferAuditCreator.generateAudit(oldAnciOffer, anciOffer, userPrincipal);

			PromotionModuleServiceLocator.getAuditorBD().audit(audit, null);

		} catch (Exception e) {
			throw handleException(e, true, log);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addAnciOfferCriteria(AnciOfferCriteriaTO anciOfferCriteriaTO, TrackInfoDTO trackingInfo,
			UserPrincipal userPrincipal) throws ModuleException {

		try {
			AnciOfferCriteria anciOffer = AnciOfferHelper.transformToAnciOfferCriteria(anciOfferCriteriaTO);
			anciOffer.setUserDetails(userPrincipal);

			Integer assignedID = PromotionsHandlerFactory.getAnciOfferCriteriaDAO().addAnciOfferCriteria(anciOffer);

			String nameI18nKey = I18nTranslationUtil.getI18nAnciOfferNameKey(assignedID);
			String descI18nKey = I18nTranslationUtil.getI18nAnciOfferDescKey(assignedID);

			PromotionModuleServiceLocator.getCommonMasterBD().saveTranslations(anciOfferCriteriaTO.getNameTranslations(),
					nameI18nKey, I18nTranslationUtil.I18nMessageCategory.ANCI_OFFER_NAME.toString());

			PromotionModuleServiceLocator.getCommonMasterBD().saveTranslations(anciOfferCriteriaTO.getDescriptionTranslations(),
					descI18nKey, I18nTranslationUtil.I18nMessageCategory.ANCI_OFFER_DESC.toString());

			/*
			 * Since the generated ID is used to create the I18nKeys a second update is unfortunately necessary. :-(
			 */
			anciOffer.setId(assignedID);
			anciOffer.setAnciOfferNameI18nMessageKey(nameI18nKey);
			anciOffer.setAnciOfferDescriptionI18nMessageKey(descI18nKey);

			PromotionsHandlerFactory.getAnciOfferCriteriaDAO().updateAnciOfferCriteria(anciOffer);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AnciOfferCriteriaTO getAvailableAnciOffer(final AnciOfferCriteriaSearchTO searchCriteria) throws ModuleException {

		Collection<AnciOfferCriteriaTO> selectedOfferDTOs = getAllAvailableAnciOffers(searchCriteria);

		return selectedOfferDTOs.isEmpty()?null:selectedOfferDTOs.iterator().next();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<AnciOfferCriteriaTO> getAllAvailableAnciOffers(final AnciOfferCriteriaSearchTO searchCriteria)
			throws ModuleException {

		if (!AppSysParamsUtil.isAnciOfferEnabled()) {
			return Collections.emptyList();
		}

		searchCriteria.setSearchONDSet(Sets.newHashSet(searchCriteria.getOND()));

		List<AnciOfferCriteria> selectedOffers = new ArrayList<AnciOfferCriteria>();

		try {
			selectedOffers = PromotionsHandlerFactory.getAnciOfferCriteriaDAO().searchApplicableAnciOffers(
				searchCriteria);
			Collections.sort(selectedOffers, new Comparator<AnciOfferCriteria>() {

				@Override
				public int compare(AnciOfferCriteria o1, AnciOfferCriteria o2) {
					return o1.getId().compareTo(o2.getId());
				}
			});
			// TODO Remove this hack and modify query
		} catch (Exception exp) {
			log.debug("NO AVAILABLE ANCI OFFERS for GIVEN SEARCH");
		}

		if (selectedOffers.isEmpty()) {
			final Set<String> wildCardAppendedONDSet = AnciOfferUtil.getWildCardAppendedONDsExcludingOriginal(searchCriteria
					.getOND());

			searchCriteria.setSearchONDSet(wildCardAppendedONDSet);

			try {
				selectedOffers = PromotionsHandlerFactory.getAnciOfferCriteriaDAO().searchApplicableAnciOffers(searchCriteria);
			} catch (Exception exp) { // TODO Remove this hack and modify query
				log.debug("NO AVAILABLE ANCI OFFERS for GIVEN SEARCH");
			}

			if (selectedOffers.isEmpty())
				return Collections.emptyList();

			Collections.sort(selectedOffers, new Comparator<AnciOfferCriteria>() {

				@Override
				public int compare(AnciOfferCriteria o1, AnciOfferCriteria o2) {
					Integer o1Weight = AnciOfferUtil.getONDCriteriaPriority(o1, wildCardAppendedONDSet);
					Integer o2Weight = AnciOfferUtil.getONDCriteriaPriority(o2, wildCardAppendedONDSet);

					return o1Weight.compareTo(o2Weight);
				}
			});
		}

		Collection<String> allI18nKeys = AnciOfferHelper.getI18nMessageKeys(selectedOffers);

		Map<String, Map<String, String>> translatedMessages = PromotionModuleServiceLocator.getCommonMasterBD()
				.getTranslatedMessagesForKeys(allI18nKeys);

		Collection<AnciOfferCriteriaTO> selectedOfferDTOs = AnciOfferHelper.setTranslatedMessages(
				AnciOfferHelper.toAnciOfferDTOCollection(selectedOffers), translatedMessages);

		return selectedOfferDTOs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean doesAnActiveReplicateCriteriaExist(AnciOfferCriteriaTO anciOfferCriteria) throws ModuleException {

		AnciOfferCriteria anciOffer = AnciOfferHelper.transformToAnciOfferCriteria(anciOfferCriteria);

		List<AnciOfferCriteria> activeOffers = PromotionsHandlerFactory.getAnciOfferCriteriaDAO().getActiveCriterisForType(
				anciOffer.getAnciTemplateType());

		return activeOffers.contains(anciOffer);
	}
}
