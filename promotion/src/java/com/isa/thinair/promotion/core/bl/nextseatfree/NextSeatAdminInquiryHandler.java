package com.isa.thinair.promotion.core.bl.nextseatfree;

import static com.isa.thinair.promotion.core.util.CommandParamNames.OPERATION_APPROVAL_VIEW;
import static com.isa.thinair.promotion.core.util.CommandParamNames.OPERATION_CODE;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.core.bl.common.PromotionAdminInquiryHandler;


public class NextSeatAdminInquiryHandler extends PromotionAdminInquiryHandler {

	protected ServiceResponce handleRequest() throws ModuleException {
		DefaultServiceResponse serviceResponce = new DefaultServiceResponse(false);

		if (getParameter(OPERATION_CODE).equals(OPERATION_APPROVAL_VIEW)) {
			serviceResponce.setSuccess(true);
		}

		return serviceResponce;
	}

}
