package com.isa.thinair.promotion.core.bl.lms;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_STATUS;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;
import com.isa.thinair.wsclient.api.util.LMSConstants.LMS_SERVICE_PROVIDER;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.LMSGravtyWebServiceInvoker;

public class GravtyFileUploader extends AbstractLmsFileUploader {

	private static final String PROXY_HOST = CommonsServices.getGlobalConfig().getHttpProxy();
	private static final int PROXY_PORT = CommonsServices.getGlobalConfig().getHttpPort();
	private static final boolean IS_USE_PROXY = LMSGravtyWebServiceInvoker.gravtyConfig.isApplyProxy();
	private static final String TEXT_PLAIN = "text/plain";
	private static final String MULTI_PART_FROM_DATA = "multipart/form-data";


	private Log log = LogFactory.getLog(GravtyFileUploader.class);

	@Override
	public void uploadPendingFiles(FILE_TYPE fileType) throws ModuleException {

		List<LoyaltyFileLog> uploadPendingFiles = PromotionsHandlerFactory.getLoyaltyManagmentDAO().getUploadPendingFiles(
				fileType, LMS_SERVICE_PROVIDER.GRAVTY);

		if (uploadPendingFiles != null && !uploadPendingFiles.isEmpty()) {
			for (LoyaltyFileLog loyaltyFileLog : uploadPendingFiles) {
				String fileName = loyaltyFileLog.getFileName();

				LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator
						.getLoyaltyExternalConfig();
				StringBuilder localFilePath = new StringBuilder();
				StringBuilder remoteFilePath = new StringBuilder();

				if (fileType == FILE_TYPE.FLOWN_FILE) {
					localFilePath.append(PlatformConstants.getAbsLoyaltyFlownFileUploadPath()).append(File.separatorChar);
					remoteFilePath.append(externalConfig.getFlownFileRemotePath()).append(File.separatorChar);
				} else {
					localFilePath.append(PlatformConstants.getAbsLoyaltyProductFileUploadPath()).append(File.separatorChar);
					remoteFilePath.append(externalConfig.getProductFileRemotePath()).append(File.separatorChar);
				}

				localFilePath.append(fileName);
				remoteFilePath.append(fileName);

				boolean uploadStatus = true;
				try {

					uploadFileAsPlainText(fileName, localFilePath.toString());

				} catch (Exception e) {
					log.error("Gravty file upload failed for Loyalty file:" + fileName, e);
					uploadStatus = false;
				} finally {
					loyaltyFileLog.setStatus(uploadStatus ? FILE_STATUS.UPLOADED.toString() : FILE_STATUS.UPLOAD_FAILED
							.toString());
					PromotionsHandlerFactory.getLoyaltyManagmentDAO().saveOrUpdateLoyaltyFileLog(loyaltyFileLog);

				}

			}
		}
	}

	private void uploadFileAsPlainText(String fileName, String filePath) throws IOException, ModuleException {
		log.info("========================== Gravity signed url retrieving ====================");
		String signedUrl = getSignUrl(fileName, TEXT_PLAIN);
		log.info("========================== Gravity signed url retrieval Success====================");

		File file = new File(filePath);

		log.info("========================== flwon file reading start ====================");
		String content = FileUtils.readFileToString(file);
		log.info("========================== flwon file reading end ====================");
		HttpPut put = new HttpPut(signedUrl);
		put.setEntity(new StringEntity(content));
		put.setHeader("Content-Type", TEXT_PLAIN);
		try {
			HttpClient httpclient = getHttpClient();

			log.info("========================== flwon file uploading start ====================");
			HttpResponse response = httpclient.execute(put);
			log.info("========================== flwon file reading end ====================");

			int statusCode = response.getStatusLine().getStatusCode();
			validateStatusCode(statusCode);

			HttpEntity responseEntity = response.getEntity();
			String responseString = EntityUtils.toString(responseEntity, "UTF-8");

			log.info("[" + statusCode + "] " + responseString);

		} catch (Exception e) {
			log.error(e.getCause() + "      File Name : " + fileName);
			throw new ModuleException("lms.gravity.file.upload.fail");
		}

	}

	private void uploadFileAsMultiPart(String fileName, String filePath) throws ModuleException {

		try {

			HttpClient client = getHttpClient();
			String url = getSignUrl(fileName, MULTI_PART_FROM_DATA);

			HttpPut put = new HttpPut(url);
			File file = new File(filePath);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addBinaryBody("file", file, ContentType.MULTIPART_FORM_DATA, file.getName());

			HttpEntity entity = builder.build();
			put.setEntity(entity);
			put.setHeader("Content-Type", "multipart/form-data");

			HttpResponse response = client.execute(put);

			int statusCode = response.getStatusLine().getStatusCode();
			validateStatusCode(statusCode);

			HttpEntity responseEntity = response.getEntity();
			String responseString = EntityUtils.toString(responseEntity, "UTF-8");

			log.info("[" + statusCode + "] " + responseString);

		} catch (Exception e) {
			log.error(e.getCause() + "      File Name : " + fileName);
			throw new ModuleException("lms.gravity.file.upload.fail");
		}

	}

	private void validateStatusCode(int statusCode) throws ModuleException {
		if (statusCode != HttpStatus.SC_OK) {
			throw new ModuleException("lms.gravity.file.upload.fail");
		}
	}

	private String getSignUrl(String fileName, String fileType) throws ClientProtocolException, IOException, ModuleException {
		return PromotionModuleServiceLocator.getWSClientBD().generateS3SignedUrl(fileName, fileType);
	}

	private HttpClient getHttpClient() {
		HttpClient httpClient;
		if (IS_USE_PROXY) {
			HttpHost proxy = new HttpHost(PROXY_HOST, PROXY_PORT);
			httpClient = HttpClientBuilder.create().setProxy(proxy).build();
		} else {
			httpClient = HttpClientBuilder.create().build();
		}
		return httpClient;
	}

}
