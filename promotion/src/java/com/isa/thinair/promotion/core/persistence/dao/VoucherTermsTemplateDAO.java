package com.isa.thinair.promotion.core.persistence.dao;

import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateSearchDTO;
import com.isa.thinair.promotion.api.model.VoucherTermsTemplate;

/**
 * The interface defining data access operations related to {@link VoucherTermsTemplate} and it's auditing mechanism.
 * 
 * @author chanaka
 * 
 */
public interface VoucherTermsTemplateDAO {

	/**
	 * Retrieves all of the voucher terms template data.
	 * 
	 * @return Collection of {@link VoucherTermsTemplate}
	 */
	public List<VoucherTermsTemplate> getAllVoucherTermsTemplates();

	/**
	 * Updates a voucher terms template and the audit data.
	 * 
	 * @param termsTemplate
	 *            New data to be updated.
	 * @param termsTemplateAudit
	 *            Audit data to be created with the update.
	 */
	public void updateVoucherTermsTemplate(VoucherTermsTemplate termsTemplate);

	/**
	 * 
	 * Searches voucher terms templates according to the provided criteria and returns results in a paginated context.
	 * 
	 * @param searchCriteria
	 *            Search parameters to run the query.
	 * @param start
	 *            Result set's start position.
	 * @param recSize
	 *            Size of the results to be returned.
	 * @return A {@link Page} encapsulating the result data in a paginated context.
	 */
	public Page<VoucherTermsTemplate> getVoucherTermsTemplatePage(VoucherTermsTemplateSearchDTO searchCriteria, Integer start, Integer recSize);
	
	/**
	 * 
	 * Searches voucher terms templates according to the provided template name.
	 * 
	 * @param voucherTemplateName
	 *            Search templateName to run the query.
	 * @param languageCode
	 * @return an object of VoucherTermsTemplate.
	 */
	public VoucherTermsTemplate getVoucherTerms(String voucherTemplateName, String languageCode) ;
}
