package com.isa.thinair.promotion.core.util;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibm.icu.text.DecimalFormat;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.model.VoucherTemplate;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;

public class VoucherTemplateDataHelper {

	private static final Log log = LogFactory.getLog(VoucherTemplateDataHelper.class);

	public static VoucherTemplate toVoucherTemplate(VoucherTemplateTo voucherTemplateTo, UserPrincipal userPrincipal)
			throws ModuleException {
		VoucherTemplate voucherTemplate = new VoucherTemplate();
		constructVoucherTemplate(voucherTemplateTo, voucherTemplate, userPrincipal);
		return voucherTemplate;
	}

	public static VoucherTemplateTo toVoucherTemplateTo(VoucherTemplate voucherTemplate) {

		VoucherTemplateTo voucherTemplateTo = new VoucherTemplateTo();

		voucherTemplateTo.setVoucherID(voucherTemplate.getVoucherId());
		voucherTemplateTo.setVoucherName(voucherTemplate.getVoucherName());
		voucherTemplateTo.setSalesFrom(voucherTemplate.getSalesFrom());
		voucherTemplateTo.setSalesTo(voucherTemplate.getSalesTo());
		voucherTemplateTo.setCurrencyCode(voucherTemplate.getCurrencyCode());
		voucherTemplateTo.setVoucherValidity(voucherTemplate.getVoucherValidity());
		voucherTemplateTo.setRemarks(voucherTemplate.getRemarks());
		voucherTemplateTo.setStatus(voucherTemplate.getStatus());
		DecimalFormat df = new DecimalFormat("#.00");
		voucherTemplateTo.setAmountInLocal(df.format(voucherTemplate.getAmountInLocal()));
		voucherTemplateTo.setAmount(df.format(voucherTemplate.getAmount()));

		return voucherTemplateTo;
	}

	private static void constructVoucherTemplate(VoucherTemplateTo voucherTemplateTo, VoucherTemplate voucherTemplate,
			UserPrincipal userPrincipal) throws ModuleException {

		try {
			voucherTemplate.setAmount(convertInToBaseCurrency(voucherTemplateTo.getCurrencyCode(),
					BigDecimal.valueOf(Double.parseDouble(voucherTemplateTo.getAmountInLocal()))));
		} catch (ModuleException e1) {
			log.error("saveVoucherTemplate-currency conversion", e1);
		}
		if (voucherTemplateTo.getVoucherId() != 0) {
			voucherTemplate.setVoucherId(voucherTemplateTo.getVoucherId());
		}
		voucherTemplate.setVoucherName(voucherTemplateTo.getVoucherName());
		voucherTemplate.setSalesFrom(voucherTemplateTo.getSalesFrom());
		voucherTemplate.setSalesTo(voucherTemplateTo.getSalesTo());
		voucherTemplate.setCurrencyCode(voucherTemplateTo.getCurrencyCode());
		voucherTemplate.setAmountInLocal(Double.parseDouble(voucherTemplateTo.getAmountInLocal()));
		voucherTemplate.setRemarks(voucherTemplateTo.getRemarks());
		voucherTemplate.setVoucherValidity(voucherTemplateTo.getVoucherValidity());
		voucherTemplate.setStatus(voucherTemplateTo.getStatus());

	}

	private static BigDecimal convertInToBaseCurrency(String currencyCode, BigDecimal amount) throws ModuleException {

		if (currencyCode == null || amount == null) {
			throw new ModuleException("webplatform.arg.invalidArgs");
		}

		String baseCurrency = AppSysParamsUtil.getBaseCurrency();

		if (currencyCode.equals(baseCurrency)) {
			BigDecimal convertedAmount = AccelAeroCalculator.scaleValueDefault(amount);
			return convertedAmount;
		} else {
			BigDecimal exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getPurchaseExchangeRateAgainstBase(currencyCode);
			return AccelAeroRounderPolicy.convertAndRound(amount, exchangeRate, null, null);
		}
	}

}
