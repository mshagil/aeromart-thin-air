package com.isa.thinair.promotion.core.bl.nextseatfree;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.AirportPassengerMessageTxHsitory;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.PromotionRequestConfig;
import com.isa.thinair.promotion.api.model.PromotionTemplateConfig;
import com.isa.thinair.promotion.api.to.PromotionRequestApproveTo;
import com.isa.thinair.promotion.api.to.PromotionStatusEmailContent;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants.PromotionRequestStatus;
import com.isa.thinair.promotion.core.bl.common.ApprovePromoRequestOnFlightDeparture;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.dto.nextseat.NextSeatFreePromoResTo;
import com.isa.thinair.promotion.core.dto.nextseat.SeatSelectionTo;
import com.isa.thinair.promotion.core.persistence.dao.NextSeatFreePromotionAdminDao;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsDaoHelper;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @isa.module.command name="approveNextSeatFreeRequest"
 */
public class ApproveNextSeatFreeRequest extends ApprovePromoRequestOnFlightDeparture {

	private static Log log = LogFactory.getLog(ApproveNextSeatFreeRequest.class);

	private UserPrincipal userPrincipal;
	private int flightSegVacantSeatsStart;
	private boolean relocate = true;
	private int seatsToFree;
	private BigDecimal refund = BigDecimal.ZERO;
	private Reservation reservation;
	// -------------------------------------------- private Map<String, List<FlightSeatStatusTO>> selectedSeatMap;
	private SeatSelectionTo seatSelectionTo;
	private HashMap<String, String> contentMapForAudit;
	private Map<String, List<PromotionStatusEmailContent>> contentForApprovedRejectedEmails;
	private NextSeatFreePromoResTo promoResTo;
	private Map<Integer, Map<String, List<FlightSeatStatusTO>>> selectedSeatMapList;
	private List<String> segCodes;
	private Map<Integer, String> passengerWisePromoStatus;
	private Map<Integer, SeatSelectionTo> seatSelectionToMap;
	private List<Map<Integer, Map<String, List<FlightSeatStatusTO>>>> flightSegWiseAddedRemovedSeats;
	private Map<Integer, BigDecimal> refundMapForAudit = new HashMap<Integer, BigDecimal>();
	private Map<Integer, Integer> seatsToFreeForAudit = new HashMap<Integer, Integer>();

	// < direction, {pnr_pax_ids }: used to filter requests to be passed to refund
	private Map<Integer, List<Integer>> distinctPaxsInDirection = new HashMap<Integer, List<Integer>>();

	public ServiceResponce approvePromotionRequest() throws ModuleException {
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		NextSeatFreeSeatsSelector nextSeatFreePromotionBL = null;
		String logicalCabinClassCode = null;
		String cabinClassCode = null;

		SeatMapDAO seatMapDAO = PromotionModuleServiceLocator.getSeatMapDAO();
		FlightBD flightBD = PromotionModuleServiceLocator.getFlightServiceBD();
		FlightInventoryDAO flightInventoryDAO = PromotionModuleServiceLocator.getFlightInventoryDAO();

		selectedSeatMapList = new HashMap<Integer, Map<String, List<FlightSeatStatusTO>>>();
		List<PromotionRequestApproveTo> segsInDirection = promoResTo.getPromoReqsForApproval();
		Integer direction = new Integer(promoResTo.getDirection());

		List<Integer> noOfSegsInDirection = new ArrayList<Integer>();
		for (PromotionRequestApproveTo promoReq : segsInDirection) {
			if (!noOfSegsInDirection.contains(promoReq.getFlightSegId())) {
				noOfSegsInDirection.add(promoReq.getFlightSegId());
			}
		}

		passengerWisePromoStatus = new HashMap<Integer, String>();
		seatSelectionToMap = new HashMap<Integer, SeatSelectionTo>();
		flightSegWiseAddedRemovedSeats = new ArrayList<Map<Integer, Map<String, List<FlightSeatStatusTO>>>>();

		try {

			for (PromotionRequestApproveTo promoReq : segsInDirection) {

				int minVacantSeats = 0;
				int preserveSeat = 0;
				int flightSegVacantSeatsCurr = flightInventoryDAO.getTotalVacantSeats(getFlightSegId());
				FlightSegement flightSegment = flightBD.getFlightSegment(promoReq.getFlightSegId());

				for (PromotionRequestConfig promoReqConfig : promoReq.getPromotionRequestConfigs()) {
					if (promoReqConfig.getPromoReqParam().equals(PromoRequestParam.FreeSeat.SEATS_TO_FREE)) {
						seatsToFree = Integer.parseInt(promoReqConfig.getParamValue());
						seatsToFreeForAudit.put(promoReq.getPromotionRequestId(), seatsToFree);
					} else if (promoReqConfig.getPromoReqParam().equals(PromoRequestParam.FreeSeat.MOVE_SEAT)) {
						relocate = promoReqConfig.getParamValue().equals(PromotionsInternalConstants.BOOLEAN_TRUE);
					}
				}

				for (PromotionTemplateConfig promoTemplateConfig : promoReq.getPromotionTemplate().getPromotionTemplateConfigs()) {
					if (promoTemplateConfig.getPromoParam().equals(
							PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_MIN_VACANT_SEATS)) {
						minVacantSeats = Integer.parseInt(promoTemplateConfig.getParamValue());
					} else if (promoTemplateConfig.getPromoParam().equals(
							PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_PRESERVE_SEATS)) {
						preserveSeat = Integer.parseInt(promoTemplateConfig.getParamValue());
					}
				}

				if (minVacantSeats > flightSegVacantSeatsStart || flightSegVacantSeatsCurr - seatsToFree <= preserveSeat) {
					getRequestScopeResp().setSuccess(false);
					promoReq.getPromotionRequest().setStatus(
							PromotionsInternalConstants.PromotionRequestStatus.REJECTED.getStatusCode());
					handleNextSeatRequestRejection(direction, promoReq, noOfSegsInDirection.size());

					if (log.isDebugEnabled())
						log.debug(" Request rejected because of seat factors exceeded");

				}

				if (getRequestScopeResp().isSuccess()) {
					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(promoResTo.getPnr());
					pnrModesDTO.setLoadSeatingInfo(true);
					pnrModesDTO.setLoadSegView(true);
					pnrModesDTO.setLoadSegViewBookingTypes(true);
					pnrModesDTO.setLoadFares(true);

					reservation = PromotionModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);

					nextSeatFreePromotionBL = PromotionsHandlerFactory.getNextSeatFreeSeatsSelector();

					for (ReservationSegment reservationSegment : reservation.getSegments()) {
						if (reservationSegment.getPnrSegId().intValue() == promoReq.getReservationSegmentId().intValue()) {
							logicalCabinClassCode = reservationSegment.getLogicalCCCode();
							cabinClassCode = reservationSegment.getCabinClassCode();
						}

					}

					String paxType = retrivePaxType(reservation, promoReq.getReservationPaxId());
					Integer flightSegId = promoReq.getFlightSegId();

					Map<String, FlightSeatStatusTO> seatInventory = seatMapDAO.getFlightSeatStatusDetails(flightSegId, null);

					addRemoveSeatsToSeatInventory(seatInventory, flightSegId);

					Collection<PaxSeatTO> seats = reservation.getSeats(promoReq.getReservationPaxId());

					boolean hasSeatsForCurrentSeg = false;
					if (seats.size() > 0) {
						for (PaxSeatTO seat : seats) {
							if (seat.getPnrSegId().intValue() == promoReq.getReservationSegmentId().intValue()) {

								seatSelectionTo = nextSeatFreePromotionBL.getSeatsToAllocate(flightSegId,
										promoReq.getPromotionRequest(), seatInventory, paxType, seat, logicalCabinClassCode,
										seatsToFree, cabinClassCode, relocate);
								hasSeatsForCurrentSeg = true;
								break;
							}

						}
						if (!hasSeatsForCurrentSeg) {
							seatSelectionTo = nextSeatFreePromotionBL.getSeatsToAllocate(flightSegId,
									promoReq.getPromotionRequest(), seatInventory, paxType, null, logicalCabinClassCode,
									seatsToFree, cabinClassCode, relocate);
						}
					} else {
						seatSelectionTo = nextSeatFreePromotionBL.getSeatsToAllocate(flightSegId, promoReq.getPromotionRequest(),
								seatInventory, paxType, null, logicalCabinClassCode, seatsToFree, cabinClassCode, relocate);
					}

				}

				if (seatSelectionTo.getSeatSelection() == null) {
					passengerWisePromoStatus.put(promoReq.getReservationPaxId(), PromotionRequestStatus.REJECTED.getStatusCode());
				} else {
					if (passengerWisePromoStatus.get(promoReq.getReservationPaxId()) == null) {
						passengerWisePromoStatus.put(promoReq.getReservationPaxId(),
								PromotionRequestStatus.APPROVED.getStatusCode());
					}
					Map<String, List<FlightSeatStatusTO>> currentAddedRemovedSeats = new HashMap<String, List<FlightSeatStatusTO>>();
					currentAddedRemovedSeats.putAll(seatSelectionTo.getSeatSelection());
					Map<Integer, Map<String, List<FlightSeatStatusTO>>> currentFlightSegAddRemoveSeats = new HashMap<Integer, Map<String, List<FlightSeatStatusTO>>>();
					currentFlightSegAddRemoveSeats.put(promoReq.getFlightSegId(), currentAddedRemovedSeats);
					flightSegWiseAddedRemovedSeats.add(currentFlightSegAddRemoveSeats);
				}
				seatSelectionToMap.put(promoReq.getPromotionRequestId(), seatSelectionTo);

			}

			for (PromotionRequestApproveTo promoReq : segsInDirection) {
				FlightSegement flightSegment = flightBD.getFlightSegment(promoReq.getFlightSegId());

				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(promoReq.getPnr());
				pnrModesDTO.setLoadSeatingInfo(true);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadSegViewBookingTypes(true);
				pnrModesDTO.setLoadFares(true);

				reservation = PromotionModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);

				if (passengerWisePromoStatus.get(promoReq.getReservationPaxId()).equals(
						PromotionRequestStatus.APPROVED.getStatusCode())) {

					Map<String, List<FlightSeatStatusTO>> selectedSeatMapForAudit = new HashMap<String, List<FlightSeatStatusTO>>();
					SeatSelectionTo seatSel = seatSelectionToMap.get(promoReq.getPromotionRequestId());

					selectedSeatMapForAudit.putAll(seatSel.getSeatSelection());
					selectedSeatMapList.put(promoReq.getPromotionRequestId(), selectedSeatMapForAudit);
					getAdministrationBD().approveNextSeatFreePromotionRequest(promoReq.getPromotionRequest(), reservation,
							flightSegment, seatSel.getSeatSelection(), userPrincipal);

					if (!seatSel.isRelocated()) {
						PromotionRequestConfig promoReqConf = new PromotionRequestConfig();
						promoReqConf.setPromotionRequest(promoReq.getPromotionRequest());
						promoReqConf.setPromoReqParam(PromoRequestParam.FreeSeat.ORIGINAL_SEAT_CODE);
						promoReqConf.setParamValue(seatSel.getOriginalSeat().getSeatCode());
						promoReqConf.setStatus(PromotionsInternalConstants.STATUS_ACTIVE);
						promoReqConf.setCreatedBy(userPrincipal.getName());
						promoReqConf.setCreatedDate(new Date());
						promoReq.getPromotionRequest().getPromotionRequestConfigs().add(promoReqConf);
					}

					promoReq.getPromotionRequest().setStatus(
							PromotionsInternalConstants.PromotionRequestStatus.APPROVED.getStatusCode());
					PromotionsHandlerFactory.getPromotionManagementDao().updateEntity(promoReq.getPromotionRequest());
				}

				else if (passengerWisePromoStatus.get(promoReq.getReservationPaxId()).equals(
						PromotionRequestStatus.REJECTED.getStatusCode())) {
					response = new DefaultServiceResponse(false);
					getRequestScopeResp().setSuccess(false);

					Integer currentPaxId = promoReq.getPromotionRequest().getReservationPaxId();

					if (log.isDebugEnabled())
						log.debug("No matching seat is found --  Direction  : " + direction + " pnr_pax_id " + currentPaxId);

					promoReq.getPromotionRequest().setStatus(
							PromotionsInternalConstants.PromotionRequestStatus.REJECTED.getStatusCode());
					handleNextSeatRequestRejection(direction, promoReq, noOfSegsInDirection.size());
					PromotionsHandlerFactory.getPromotionManagementDao().updateEntity(promoReq.getPromotionRequest());
				}

			}

		} catch (Exception e) {
			log.error("Generic exception caught.", e);

			for (PromotionRequestApproveTo promoApproval : segsInDirection) {
				try {
					promoApproval.getPromotionRequest().setStatus(
							PromotionsInternalConstants.PromotionRequestStatus.REJECTED.getStatusCode());
					PromotionsHandlerFactory.getPromotionManagementDao().updateEntity(promoApproval.getPromotionRequest());
					handleNextSeatRequestRejection(direction, promoApproval, noOfSegsInDirection.size());
				} catch (Exception ee) {
					log.error("Handling of seat rejection was failed for pnr_pax_id : " + promoApproval.getReservationPaxId()
							+ " in pnr_seg_id : " + promoApproval.getReservationSegmentId() + e);
					// code should not break for refunding for other paxs, traveling in same direction
				}
			}
			response = new DefaultServiceResponse(false);
		}

		return response;
	}

	protected List<?> getPromotionRequests() throws ModuleException {

		NextSeatFreePromotionAdminDao adminDao = (NextSeatFreePromotionAdminDao) PromotionsHandlerFactory
				.getPromotionAdministrationDao(PromotionType.PROMOTION_NEXT_SEAT_FREE);

		return adminDao.getPromotionRequests(getFlightSegId(), PromotionType.PROMOTION_NEXT_SEAT_FREE);

	}

	protected void promotionsApprovalPreProcess() throws ModuleException {
		FlightInventoryDAO flightInventoryDAO = PromotionModuleServiceLocator.getFlightInventoryDAO();
		flightSegVacantSeatsStart = flightInventoryDAO.getTotalVacantSeats(getFlightSegId());

		contentForApprovedRejectedEmails = new HashMap<String, List<PromotionStatusEmailContent>>();
		userPrincipal = (UserPrincipal) getParameter(CommandParamNames.USER_PRINCPAL);

	}

	protected void promotionRequestPreProcess() throws ModuleException {
		super.promotionRequestPreProcess();

		promoResTo = (NextSeatFreePromoResTo) getPromotionRequest();

		segCodes = new ArrayList<String>();
		for (PromotionRequestApproveTo promoReqTo : promoResTo.getPromoReqsForApproval()) {
			if (!promoReqTo.getStatus().equals(PromotionsInternalConstants.PromotionRequestStatus.ACTIVE.getStatusCode())) {
				getRequestScopeResp().setSuccess(false);
			}
			String segCode = promoReqTo.getSegmentCode();

			if (!segCodes.contains(segCode)) {
				segCodes.add(promoReqTo.getSegmentCode());
			}
		}

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(promoResTo.getPnr());
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadFares(true);

		reservation = PromotionModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);

	}

	protected void promotionRequestPostProcess() throws ModuleException {
		Map<String, List<FlightSeatStatusTO>> seatSelection;
		contentMapForAudit = new HashMap<String, String>();
		List<String> seatCode;
		Collection<String> journeyOnds = new LinkedHashSet<String>();

		List<PromotionRequestApproveTo> requests = promoResTo.getPromoReqsForApproval();
		for (PromotionRequestApproveTo requset : requests) {
			if (!journeyOnds.contains(requset.getSegmentCode())) {
				journeyOnds.add(requset.getSegmentCode());
			}
		}

		promoResTo = (NextSeatFreePromoResTo) getPromotionRequest();
		// PromotionStatusEmailContent promotionStatusEmailContent = new PromotionStatusEmailContent();
		for (PromotionRequestApproveTo promoReqTo : promoResTo.getPromoReqsForApproval()) {

			PromotionStatusEmailContent promotionStatusEmailContent = new PromotionStatusEmailContent();

			promotionStatusEmailContent.setPnr(reservation.getPnr());
			for (ReservationPax pax : reservation.getPassengers()) {
				if (pax.getPnrPaxId().intValue() == promoReqTo.getReservationPaxId().intValue()) {
					promotionStatusEmailContent.setPaxName(WordUtils.capitalize(pax.getTitle()) + " "
							+ WordUtils.capitalize(pax.getFirstName()) + " " + WordUtils.capitalize(pax.getLastName()));
					promotionStatusEmailContent.setPnrPaxID(pax.getPnrPaxId());
				}
			}

			if (promoReqTo.getStatus().equals(PromotionRequestStatus.REJECTED.getStatusCode())) {
				promotionStatusEmailContent.setPromoStatus("Rejected");
			} else if (promoReqTo.getStatus().equals(PromotionRequestStatus.APPROVED.getStatusCode())) {
				promotionStatusEmailContent.setPromoStatus("Approved");
			}

			FlightSegement fS = (FlightSegement) getParameter(CommandParamNames.FLIGHT_SEGMENT);
			promotionStatusEmailContent.setSegmentCode(promoReqTo.getSegmentCode());
			promotionStatusEmailContent.setDepartureDate(fS.getEstTimeDepatureLocal());

			promotionStatusEmailContent.setContactEmail(reservation.getContactInfo().getEmail());
			promotionStatusEmailContent.setPreferdLanguage(reservation.getContactInfo().getPreferredLanguage());
			promotionStatusEmailContent.setPromotionRequestID(promoReqTo.getPromotionRequestId());
			promotionStatusEmailContent.setJourneyOnds(journeyOnds);

			seatCode = new ArrayList<String>();
			if (passengerWisePromoStatus.containsKey(promoReqTo.getReservationPaxId()) &&
					passengerWisePromoStatus.get(promoReqTo.getReservationPaxId()).equals(PromotionRequestStatus.APPROVED.getStatusCode()) &&
					seatSelectionToMap.containsKey(promoReqTo.getPromotionRequestId())) {
				seatSelection = seatSelectionToMap.get(promoReqTo.getPromotionRequestId()).getSeatSelection();
				if (seatSelection != null && !seatSelection.isEmpty()) {

					if (seatSelectionToMap.get(promoReqTo.getPromotionRequestId()).getOriginalSeat() != null) {
						seatCode.add(seatSelectionToMap.get(promoReqTo.getPromotionRequestId()).getOriginalSeat().getSeatCode());
					}

					for (FlightSeatStatusTO seatStatusTO : seatSelection.get(PromotionsInternalConstants.ADD)) {
						seatCode.add(seatStatusTO.getSeatCode());
					}

					for (FlightSeatStatusTO seatStatusTO : seatSelection.get(PromotionsInternalConstants.REMOVE)) {
						for (int a = 0; a < seatCode.size(); a++) {
							if (seatStatusTO.getSeatCode().equals(seatCode.get(a))) {
								seatCode.remove(a);
								break;
							}
						}
					}
				}
			}
			promotionStatusEmailContent.setSeats(seatCode);

			if (contentForApprovedRejectedEmails.containsKey(reservation.getPnr())) {
				List<PromotionStatusEmailContent> emailContentApprovedRejectedPromos = contentForApprovedRejectedEmails
						.get(reservation.getPnr());
				emailContentApprovedRejectedPromos.add(promotionStatusEmailContent);
			} else {
				List<PromotionStatusEmailContent> emailContentApprovedRejectedPromos = new ArrayList<PromotionStatusEmailContent>();
				emailContentApprovedRejectedPromos.add(promotionStatusEmailContent);
				contentForApprovedRejectedEmails.put(reservation.getPnr(), emailContentApprovedRejectedPromos);
			}

			if (promoReqTo.getPromotionRequest().getStatus().equals(PromotionRequestStatus.APPROVED.getStatusCode())) {
				Iterator<Map.Entry<String, List<FlightSeatStatusTO>>> iteratorForContent = selectedSeatMapList
						.get(promoReqTo.getPromotionRequestId()).entrySet().iterator();

				while (iteratorForContent.hasNext()) {

					Entry<String, List<FlightSeatStatusTO>> entry = iteratorForContent.next();
					if (entry.getKey().equals(PromotionsInternalConstants.ADD)) {
						StringBuilder addedSeatsString = new StringBuilder();
						List<FlightSeatStatusTO> addedSeatsForAuditMap = entry.getValue();
						if (!addedSeatsForAuditMap.isEmpty()) {
							for (FlightSeatStatusTO seat : addedSeatsForAuditMap) {
								addedSeatsString.append(seat.getSeatCode() + " ");
							}
						}
						contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.ADDED_SEATS,
								addedSeatsString.toString());
						if (addedSeatsString.toString().length() > 0) {
							log.debug("Seats " + addedSeatsString.toString() + " added for the PNR:" + reservation.getPnr());
						}
					} else if (entry.getKey().equals(PromotionsInternalConstants.REMOVE)) {
						StringBuilder removedSeatsString = new StringBuilder();
						List<FlightSeatStatusTO> removedSeatsForAuditMap = entry.getValue();
						if (!removedSeatsForAuditMap.isEmpty()) {
							for (FlightSeatStatusTO seat : removedSeatsForAuditMap) {
								removedSeatsString.append(seat.getSeatCode() + " ");
							}
						}
						contentMapForAudit.put(
								AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REMOVED_SEATS,
								removedSeatsString.toString());
						if (removedSeatsString.toString().length() > 0) {
							log.debug("Seats " + removedSeatsString.toString() + " removed for the PNR:" + reservation.getPnr());
						}
					}

				}
				contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.NUMBEROFSEATS,
						seatsToFreeForAudit.get(promoReqTo.getPromotionRequestId()).toString());

				for (ReservationPax pax : reservation.getPassengers()) {
					if (pax.getPnrPaxId().intValue() == promoReqTo.getReservationPaxId().intValue()) {
						contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.PAX_NAME,
								pax.getFirstName() + " " + pax.getLastName());
						break;
					}
				}

				contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.SEG_CODE,
						promoReqTo.getSegmentCode());
				auditNextSeatPromotionRequests(promoReqTo, contentMapForAudit);

				// ADL notification
				Collection<AirportPassengerMessageTxHsitory> pnlAdlHistory = null;
				boolean pnlSent = false;
				FlightSegement flightSegment = (FlightSegement) getParameter(CommandParamNames.FLIGHT_SEGMENT);
				if (flightSegment != null) {
					ReservationAuxilliaryDAO reservationAuxilliaryDAO = PromotionModuleServiceLocator
							.getReservationAuxilliaryDAO();
					String airport = flightSegment.getSegmentCode().split("/")[0];
					Flight flight = PromotionModuleServiceLocator.getFlightBD().getFlight(flightSegment.getFlightId());

					String departureDate = flightSegment.getEstTimeDepatureLocal().toString();
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					if (departureDate.indexOf(' ') != -1) {
						departureDate = departureDate.substring(0, departureDate.indexOf(' '));
					}
					try {
						pnlAdlHistory = reservationAuxilliaryDAO.getPnlAdlHistory(flight.getFlightNumber(),
								dateFormat.parse(departureDate), airport);

					} catch (ParseException e) {
						log.error("promotionRequestPostProcess ", e);
					}

					if (pnlAdlHistory != null && pnlAdlHistory.size() > 0) {

						for (AirportPassengerMessageTxHsitory pnlAdlRecord : pnlAdlHistory) {
							if (PNLConstants.MessageTypes.PNL.equals(pnlAdlRecord.getMessageType())
									&& (PNLConstants.SITAMsgTransmission.SUCCESS.equals(pnlAdlRecord.getTransmissionStatus()) || PNLConstants.SITAMsgTransmission.PARTIALLY
											.equals(pnlAdlRecord.getTransmissionStatus()))) {
								pnlSent = true;
								break;
							}
						}
						if (pnlSent) {

							LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
							pnrModesDTO.setPnr(promoResTo.getPnr());
							pnrModesDTO.setLoadSeatingInfo(true);
							pnrModesDTO.setLoadSegView(true);
							pnrModesDTO.setLoadSegViewBookingTypes(true);
							pnrModesDTO.setLoadFares(true);

							Reservation currentReservation = PromotionModuleServiceLocator.getReservationBD().getReservation(
									pnrModesDTO, null);

							ADLNotifyer.updateStatusToCHG(currentReservation);
						}
					}
				}
			} else {
				/*
				 * contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REFUND_AMOUNT
				 * , refund.toPlainString());
				 */
				contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.NUMBEROFSEATS,
						seatsToFreeForAudit.get(promoReqTo.getPromotionRequestId()).toString());
				contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REFUND_AMOUNT,
						refundMapForAudit.get(promoReqTo.getPromotionRequestId()).negate().toString());
				auditNextSeatPromotionRequests(promoReqTo, contentMapForAudit);
			}
		}

	}

	protected void promotionsApprovalPostProcess() throws ModuleException {

		String paxStatus;
		Iterator<Map.Entry<String, List<PromotionStatusEmailContent>>> currentEmailContentIterator = contentForApprovedRejectedEmails
				.entrySet().iterator();

		while (currentEmailContentIterator.hasNext()) {

			Map<Integer, String> passengerPromoStatus = new HashMap<Integer, String>();
			List<PromotionStatusEmailContent> emailContent = (List<PromotionStatusEmailContent>) currentEmailContentIterator
					.next().getValue();

			for (PromotionStatusEmailContent promotionStatusEmailContent : emailContent) {
				paxStatus = promotionStatusEmailContent.getPaxName()
						+ " - "
						+ promotionStatusEmailContent.getPromoStatus()
						+ " For "
						+ promotionStatusEmailContent.getSegmentCode()
						+ ((promotionStatusEmailContent.getSeats() != null && !promotionStatusEmailContent.getSeats().isEmpty())
								? ", Seats allocated: " + promotionStatusEmailContent.getSeats().toString()
								: "");

				passengerPromoStatus.put(promotionStatusEmailContent.getPromotionRequestID(), paxStatus);
			}
			List<NotificationReceiverTo> receivers = new ArrayList<NotificationReceiverTo>();
			NotificationReceiverTo notificationReceiverTo = new NotificationReceiverTo();
			notificationReceiverTo.setPnr(emailContent.get(0).getPnr());
			notificationReceiverTo.setReceiverAddr(emailContent.get(0).getContactEmail());
			notificationReceiverTo.setReceiverName(emailContent.get(0).getPaxName());
			notificationReceiverTo.setLanguage("en");
			notificationReceiverTo.setPassengerPromoStatus(passengerPromoStatus);
			notificationReceiverTo.setJourneyOnds(emailContent.get(0).getJourneyOnds().toString());
			notificationReceiverTo.setDepartureTime(emailContent.get(0).getDepartureDate());

			receivers.add(notificationReceiverTo);

			DefaultBaseCommand notifyCommand = PromotionsHandlerFactory
					.getPromotionsNotifier(PromotionType.PROMOTION_NEXT_SEAT_FREE);
			notifyCommand.setParameter(CommandParamNames.PROMOTION_NOTIFICATION_DESTINATIONS, receivers);
			notifyCommand.setParameter(CommandParamNames.EVENT_TYPE, CommandParamNames.EVENT_PROMO_APPROVED_REJECTED);
			notifyCommand.execute();

			LCCClientPnrModesDTO pnrModesDTO = PromotionsDaoHelper.getPnrModesDTO(emailContent.get(0).getPnr(), false, false,
					null, null);

			CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
			commonItineraryParam.setItineraryLanguage(emailContent.get(0).getPreferdLanguage());
			commonItineraryParam.setIncludePaxFinancials(true);
			commonItineraryParam.setIncludePaymentDetails(true);
			commonItineraryParam.setIncludeTicketCharges(false);
			commonItineraryParam.setIncludeTermsAndConditions(true);
			commonItineraryParam.setAppIndicator(ApplicationEngine.IBE);
			commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(false));

			TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
			trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_IBE);

			try {
				PromotionModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
						trackInfoDTO);
			} catch (Exception e) {
				log.error("[promotion-next seat approval] emailing iternary failed for pnr: " + emailContent.get(0).getPnr() + e);
			}

		}

	}

	private void auditNextSeatPromotionRequests(PromotionRequestApproveTo promoReqTo, HashMap<String, String> contentMapForAudit)
			throws ModuleException {

		contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.PNR, promoResTo.getPnr());
		PromotionModuleServiceLocator.getReservationBD().saveAuditNextSeatPromotionRequest(promoReqTo.getPnr(),
				promoReqTo.getStatus(), contentMapForAudit);

	}

	/**
	 * Charge and refund handler should execute just one time per direction for distinct passenger. provided that,
	 * maximum active promotions subscribed for a passenger is 1
	 * 
	 * @param direction
	 * @param promoReq
	 * @throws ModuleException
	 */
	private void handleNextSeatRequestRejection(Integer direction, PromotionRequestApproveTo promoReq, int noOfSegsInDirection)
			throws ModuleException {
		Integer currentPaxId = promoReq.getPromotionRequest().getReservationPaxId();

		if (distinctPaxsInDirection.containsKey(direction)) {
			List<Integer> passengersInDirection = distinctPaxsInDirection.get(direction);
			if (!passengersInDirection.contains(currentPaxId)) {
				refund = getAdministrationBD().processNextSeatChargeAndRefund(promoReq, refund, segCodes, reservation);
				distinctPaxsInDirection.put(direction, passengersInDirection);
			} else {
				if (log.isDebugEnabled())
					log.debug(currentPaxId + " pax is counted in  direction already : " + direction);
			}

		} else {
			List<Integer> passengersInDirection = new ArrayList<Integer>();
			passengersInDirection.add(currentPaxId);
			refund = getAdministrationBD().processNextSeatChargeAndRefund(promoReq, refund, segCodes, reservation);
			distinctPaxsInDirection.put(direction, passengersInDirection);
		}
		refundMapForAudit.put(promoReq.getPromotionRequestId(), refund.divide(new BigDecimal(noOfSegsInDirection)));

	}

	private void addRemoveSeatsToSeatInventory(Map<String, FlightSeatStatusTO> seatInventory, Integer flightSegId)
			throws ModuleException {
		SeatMapDAO seatMapDAO = PromotionModuleServiceLocator.getSeatMapDAO();
		if (flightSegWiseAddedRemovedSeats.size() > 0) {
			Set<String> addingSeatCodeSet = new HashSet<String>();
			Set<String> removingSeatCodeSet = new HashSet<String>();
			for (Map<Integer, Map<String, List<FlightSeatStatusTO>>> currentSegAddRemoveSeat : flightSegWiseAddedRemovedSeats) {
				if (currentSegAddRemoveSeat.get(flightSegId) != null) {
					Map<String, List<FlightSeatStatusTO>> addRemoveSeats = currentSegAddRemoveSeat.get(flightSegId);
					List<FlightSeatStatusTO> addingSeats = addRemoveSeats.get(PromotionsInternalConstants.REMOVE);
					List<FlightSeatStatusTO> removingSeats = addRemoveSeats.get(PromotionsInternalConstants.ADD);
					if (addingSeats.size() > 0) {
						for (FlightSeatStatusTO flightSeatStatusTO : addingSeats) {
							addingSeatCodeSet.add(flightSeatStatusTO.getSeatCode());
						}
						Map<String, FlightSeatStatusTO> addingSeatInventory = seatMapDAO.getFlightSeatStatusDetails(flightSegId,
								addingSeats.get(0).getSeatCode());

						seatInventory.putAll(addingSeatInventory);
					}
					if (removingSeats.size() > 0) {
						for (FlightSeatStatusTO flightSeatStatusTO : removingSeats) {
							removingSeatCodeSet.add(flightSeatStatusTO.getSeatCode());
						}
						seatInventory.keySet().removeAll(removingSeatCodeSet);
					}
				}
			}
		}
	}

	private String retrivePaxType(Reservation res, Integer reservationPaxId) {

		String paxType = null;
		for (ReservationPax reservationPax : res.getPassengers()) {
			if (reservationPax.getPnrPaxId().equals(reservationPaxId)) {
				if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
					paxType = ReservationInternalConstants.PassengerType.PARENT;
				} else if (ReservationApiUtils.isAdultType(reservationPax)) {
					paxType = ReservationInternalConstants.PassengerType.ADULT;
				} else if (ReservationApiUtils.isChildType(reservationPax)) {
					paxType = ReservationInternalConstants.PassengerType.CHILD;
				} else if (ReservationApiUtils.isInfantType(reservationPax)) {
					paxType = ReservationInternalConstants.PassengerType.INFANT;
				} else {
					throw new IllegalArgumentException();
				}
				break;
			}
		}
		return paxType;
	}
}