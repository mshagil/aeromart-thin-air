package com.isa.thinair.promotion.core.persistence.dao;

import java.util.Date;
import java.util.List;

import com.isa.thinair.aircustomer.api.model.SystemLMSAuthToken;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.promotion.api.model.FlownFileErrorLog;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;
import com.isa.thinair.promotion.api.to.lms.FlownRecordsDTO;
import com.isa.thinair.wsclient.api.util.LMSConstants.LMS_SERVICE_PROVIDER;

/**
 * 
 * @author rumesh
 * 
 */
public interface LoyaltyManagementDAO {

	public List<LoyaltyProduct> getLoyaltyProducts() throws CommonsDataAccessException;

	public void saveOrUpdateFlowFileErrorLog(FlownFileErrorLog flownFileErrorLog) throws CommonsDataAccessException;

	public void saveOrUpdateLoyaltyFileLog(LoyaltyFileLog loyaltyFileLog) throws CommonsDataAccessException;

	public void deleteLoyaltyFileLog(String fileName);

	public Date getLastProductFilePublishTime();

	public List<LoyaltyFileLog> getUploadPendingFiles(FILE_TYPE fileType, LMS_SERVICE_PROVIDER serviceProvider);

	public List<FlownFileErrorLog> getFailedFlownFileRecords();

	public List<FlownRecordsDTO> getFlownRecords(Date rptPeriodStartDate, Date rptPeriodEndDate);
	
	public void saveGravtyAuthToken(String token, String expiresIn) throws CommonsDataAccessException;

	public SystemLMSAuthToken getGravtyAuthTokenDB(String authToken) throws CommonsDataAccessException;

	Nationality getNationalityByCode(Integer code) throws CommonsDataAccessException;

	Country getCountryByCode(String code) throws CommonsDataAccessException;
	
}
