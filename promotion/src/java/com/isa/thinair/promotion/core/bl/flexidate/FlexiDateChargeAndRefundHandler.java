package com.isa.thinair.promotion.core.bl.flexidate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.isa.thinair.promotion.api.model.OndPromotionCharge;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionRequestConfig;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.core.bl.common.PromotionChargeAndRefundHandler;
import com.isa.thinair.promotion.core.util.CommandParamNames;

/**
 * 
 * @isa.module.command name="flexiDateChargeAndRefund"
 */
public class FlexiDateChargeAndRefundHandler extends PromotionChargeAndRefundHandler {

	protected List<OndPromotionCharge>
			filterApplicableCharges(Set<OndPromotionCharge> charges, PromotionRequest promotionRequest) {

		List<OndPromotionCharge> ondPromoChargesRefunds = new ArrayList<OndPromotionCharge>();
		String processCategory = (String) getParameter(CommandParamNames.PROMO_PROCESS_CATEGORY);

		if (processCategory.equals(CommandParamNames.PROCESS_REFUNDS)) {
			int flexiLower = 0;
			int flexiUpper = 0;
			for (PromotionRequestConfig requestConfig : promotionRequest.getPromotionRequestConfigs()) {
				if (requestConfig.getPromoReqParam().equals(PromoRequestParam.FlexiDate.FLEXI_DATE_LOWER_BOUND)) {
					flexiLower = Integer.parseInt(requestConfig.getParamValue());
				} else if (requestConfig.getPromoReqParam().equals(PromoRequestParam.FlexiDate.FLEXI_DATE_UPPER_BOUND)) {
					flexiUpper = Integer.parseInt(requestConfig.getParamValue());
				}
			}

			String applicableRewardCode = PromoTemplateParam.ChargeAndReward.FLEXI_DATE_DAYS_REWARD
					.getFlexibilityRewardParam(flexiLower + flexiUpper);
			for (OndPromotionCharge charge : charges) {
				if (charge.getChargeCode().equals(applicableRewardCode)) {
					ondPromoChargesRefunds.add(charge);
				}
			}
		}

		return ondPromoChargesRefunds;
	}

}
