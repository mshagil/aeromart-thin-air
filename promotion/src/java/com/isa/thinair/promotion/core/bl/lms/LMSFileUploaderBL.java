package com.isa.thinair.promotion.core.bl.lms;

import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.wsclient.api.util.LMSConstants.LMS_SERVICE_PROVIDER;

public class LMSFileUploaderBL {

	public void updatePendingFiles(FILE_TYPE fileType) throws ModuleException {

		uploadFileForServiceProvider(LMS_SERVICE_PROVIDER.GRAVTY, fileType);
		uploadFileForServiceProvider(LMS_SERVICE_PROVIDER.SMART_BUTTON, fileType);

	}

	private void uploadFileForServiceProvider(LMS_SERVICE_PROVIDER serviceProvider, FILE_TYPE fileType) throws ModuleException {
		if (isvalidServicePRoviderForFileUplod(serviceProvider)) {

			AbstractLmsFileUploader lmsFileUploader = getFileUploader(serviceProvider);
			lmsFileUploader.uploadPendingFiles(fileType);
		}
	}

	private AbstractLmsFileUploader getFileUploader(LMS_SERVICE_PROVIDER serviceProvider) {

		AbstractLmsFileUploader abstractLmsFileUploader = null;

		if (LMS_SERVICE_PROVIDER.GRAVTY.equals(serviceProvider)) {
			abstractLmsFileUploader = new GravtyFileUploader();
		} else if (LMS_SERVICE_PROVIDER.SMART_BUTTON.equals(serviceProvider)) {
			abstractLmsFileUploader = new SmartButtonFileUploader();
		}

		return abstractLmsFileUploader;
	}

	private boolean isvalidServicePRoviderForFileUplod(LMS_SERVICE_PROVIDER serviceProvider) {

		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		List<String> productFileUploadEnableLMSProviders = externalConfig.getProductFileUploadEnableLMSProviders();
		List<String> flownFileUploadEnableLMSProviders = externalConfig.getFlownFileUploadEnableLMSProviders();

		return productFileUploadEnableLMSProviders.contains(serviceProvider.toString())
				|| flownFileUploadEnableLMSProviders.contains(serviceProvider.toString());

	}

}
