package com.isa.thinair.promotion.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.promotion.api.model.BundledFarePeriodImage;

public interface BundledFareDisplaySettingsDao {

	public List<BundledFarePeriodImage> getBundledFarePeriodImages(Set<Integer> bundleFarePeriodIds)
			throws CommonsDataAccessException;

	public void saveBundledFareDisplaySettings(Collection<BundledFarePeriodImage> bundledFarePeriodImages)
			throws CommonsDataAccessException;

	public void updateBundledFareTranlationKeys(Integer bundledFarePeriodId, String nameKey, String descKey)
			throws CommonsDataAccessException;
	
	public void updateBundledFareTemplateId(Integer bundledFarePeriodId, Integer descTemplateID)
			throws CommonsDataAccessException;
}
