package com.isa.thinair.promotion.core.persistence.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.isa.thinair.airreservation.api.model.ReservationSegmentNotification;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.dto.nextseat.NextSeatFreePromoResTo;
import com.isa.thinair.promotion.core.persistence.dao.PromotionAdministrationDao;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionsDaoHelper;

@SuppressWarnings(value = { "unchecked" })
public abstract class PromotionAdministrationDaoImpl extends PlatformBaseHibernateDaoSupport implements
		PromotionAdministrationDao {

	public <T, K extends Serializable> T findEntity(Class<T> type, K key) {
		return (T) get(type, key);
	}

	public List<NotificationReceiverTo> getNotificationDestsEntitledForPromotion(Map<String, Object> params) {

		PromotionType promotionType = (PromotionType) params.get(CommandParamNames.PROMOTION_TYPE);
		Map<String, NotificationReceiverTo> tempRecievers = new HashMap<String, NotificationReceiverTo>();
		List<NotificationReceiverTo> applicableDests = new ArrayList<NotificationReceiverTo>();

		String boundry = "24";
		if (promotionType.equals(PromotionType.PROMOTION_NEXT_SEAT_FREE)) {
			boundry = AppSysParamsUtil.getNextSeatPromotionMailNotifierCutOffTime();
		} else if (promotionType.equals(PromotionType.PROMOTION_FLEXI_DATE)) {
			boundry = AppSysParamsUtil.getFlexidDatePromotionMailNotifierCutOffTime();
		}

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append("	SELECT DISTINCT ( res.pnr || '_' || promo.promotion_id ) AS notify_key , res.email email , res.pnr pnr , ")
				.append("        promo.promotion_id promotion_id , res.pnr_seg_id pnr_seg_id , res.segment_code segment_code ,	")
				.append("        promo.ond_code ond_code , res.dep_time dep_time , res.seat_inv seat_inv , initcap ( res.contact_person ) contact_person  FROM	")
				.append("            (   SELECT p.promotion_id promotion_id , TRUNC ( p.start_date ) start_date , TRUNC ( p.end_date ) end_date , op.ond_code ond_code	")
				.append("                FROM t_promotion p , t_ond_promotion op	")
				.append("                WHERE p.promotion_id = op.promotion_id AND p.promotion_type_id = :promotion_type_1 AND p.status = 'ACT'	")
				.append("            ) promo , ")
				.append("            (   SELECT r.pnr pnr , rc.c_email email , fs.segment_code segment_code , TRUNC ( fs.est_time_departure_zulu ) dep_date ,	")
				.append("                    fs.est_time_departure_zulu dep_time , rs.pnr_seg_id pnr_seg_id , COUNT ( fams.flt_seg_id ) seat_inv ,	")
				.append("                    (rc.c_title || '. ' || rc.c_first_name || ' ' || rc.c_last_name) contact_person   ")
				.append("                FROM t_reservation r , t_reservation_contact rc , t_pnr_segment rs , t_flight_segment fs , sm_t_flight_am_seat fams	")
				.append("                WHERE r.pnr = rc.pnr AND r.pnr = rs.pnr AND rs.flt_seg_id = fs.flt_seg_id AND fs.flt_seg_id = fams.flt_seg_id (+)	")
				.append("                    AND r.owner_channel_code = 4 AND rs.status_mod_channel_code =4 AND rs.status != 'CNX' AND r.status = 'CNF' AND fs.est_time_departure_zulu - SYSDATE >=  ")
				.append(boundry + "/24 ")
				.append("                    AND rc.c_email IS NOT NULL AND rs.pnr NOT IN   ")
				.append("                        (   SELECT DISTINCT rs.pnr	")
				.append("                            FROM t_pnr_segment rs , t_pnr_segment_notification psn , t_promotion p WHERE psn.notification_type = :notification_type	")
				.append("                                AND psn.notification_ref = p.promotion_id AND psn.pnr_seg_id = rs.pnr_seg_id AND p.promotion_type_id = :promotion_type_2	")
				.append("                        )  GROUP BY r.pnr , rc.c_email , fs.segment_code , fs.est_time_departure_zulu  , fs.est_time_departure_zulu , rs.pnr_seg_id ,  ")
				.append("                                       rc.c_title , rc.c_first_name , rc.c_last_name   ")
				.append("           ) res	")
				.append("    WHERE res.dep_date BETWEEN promo.start_date AND promo.end_date ")
				.append("    ORDER BY res.pnr , promo.promotion_id , res.dep_time	");

		NotificationReceiverTo receiver;

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("promotion_type_1", promotionType.getPromotionId());
		query.setParameter("promotion_type_2", promotionType.getPromotionId());
		query.setParameter("notification_type", ReservationSegmentNotification.NOTIFICATION_TYPE_PROMOTION);
		query.addScalar("notify_key", StringType.INSTANCE);
		query.addScalar("email", StringType.INSTANCE);
		query.addScalar("pnr", StringType.INSTANCE);
		query.addScalar("promotion_id", IntegerType.INSTANCE);
		query.addScalar("pnr_seg_id", IntegerType.INSTANCE);
		query.addScalar("segment_code", StringType.INSTANCE);
		query.addScalar("ond_code", StringType.INSTANCE);
		query.addScalar("dep_time", TimestampType.INSTANCE);
		query.addScalar("seat_inv", IntegerType.INSTANCE);
		query.addScalar("contact_person", StringType.INSTANCE);
		List<Object[]> results = query.list();

		for (Object[] single : results) {

			if (!tempRecievers.containsKey((String) single[0])) {
				receiver = new NotificationReceiverTo();
				receiver.setLanguage("en");
				receiver.setReceiverAddr((String) single[1]);
				receiver.setPnr((String) single[2]);
				receiver.setPromotionId((Integer) single[3]);
				receiver.setReceiverName((String) single[9]);

				tempRecievers.put((String) single[0], receiver);
			} else {
				receiver = tempRecievers.get((String) single[0]);
			}
			receiver.setPromotionType(promotionType);
			receiver.addReservationSegId((Integer) single[4], (String) single[5]);
			receiver.addPromotionOnd((String) single[6]);
			if(receiver.getDepartureTime() == null) {
				receiver.setDepartureTime((Date)single[7]);
			}
			receiver.setSeatInventoryDefined((Integer)single[8] > 0);
		}

		for (NotificationReceiverTo receiverTo : tempRecievers.values()) {
			if ((!receiverTo.systemRequirementsViolated()) && receiverTo.isApplicableNotification()) {
				applicableDests.add(receiverTo);
			}
		}

		return applicableDests;
	}

	public List<NextSeatFreePromoResTo> getPromotionRequests(int flightSegId, PromotionType promotionType) {
		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append("SELECT fs.flt_seg_id flt_seg_id, fs.est_time_departure_zulu departure_date,fs.segment_code segment_code, {pr.*} ")
				.append("FROM t_promotion_request pr , t_promotion p , t_pnr_segment rs,t_flight_segment fs	")
				.append("  WHERE pr.pnr_seg_id = rs.pnr_seg_id AND pr.promotion_id = p.promotion_id	and pr.status != 'INA' ")
				.append("    AND p.promotion_type_id = :promotionType	")
				.append(" AND rs.flt_seg_id = fs.flt_seg_id ")
				.append(" AND p.status = 'ACT' ")
				.append(" AND rs.pnr in(select pnr from t_pnr_segment ps where ps.flt_seg_id = :flightSegId)");

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("flightSegId", flightSegId);
		query.setParameter("promotionType", promotionType.getPromotionId());
		query.addEntity("pr", PromotionRequest.class);
		query.addScalar("flt_seg_id", IntegerType.INSTANCE);
		query.addScalar("departure_date", TimestampType.INSTANCE);
		query.addScalar("segment_code", StringType.INSTANCE);
		List<Object[]> results = query.list();

		return PromotionsDaoHelper.transformToPromotionRequestTO(results, flightSegId);
	}

	public List<Map<String, Object>> getPromotionRequestsToExpire(PromotionType promotionType) {
		List<Map<String, Object>> promoReqs = new ArrayList<Map<String, Object>>();
		Map<String, Object> promoReq;
		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append(" SELECT DISTINCT pr.pnr pnr ")
				.append(" FROM t_promotion_request pr , t_pnr_segment rs , t_flight_segment fs , t_promotion p ")
				.append(" WHERE pr.pnr_seg_id = rs.pnr_seg_id AND rs.flt_seg_id = fs.flt_seg_id ")
				.append("   AND pr.status = 'ACT' AND pr.promotion_id = p.promotion_id AND p.promotion_type_id = :promo_type ")
				.append("   AND fs.est_time_departure_zulu - SYSDATE <  ").append(AppSysParamsUtil.getFlexiDatePromotionApproveCutOffTime());

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("promo_type", promotionType.getPromotionId());
		query.addScalar("pnr", StringType.INSTANCE);

		List<String> pnrs = query.list();
		for (String pnr : pnrs) {
			promoReq = new HashMap<String, Object>();
			promoReq.put(CommandParamNames.PNR, pnr);

			promoReqs.add(promoReq);
		}

		return promoReqs;
	}


}