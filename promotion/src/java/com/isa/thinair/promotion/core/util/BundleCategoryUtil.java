package com.isa.thinair.promotion.core.util;

import com.isa.thinair.airmaster.api.dto.BundleFareCategoryDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.core.util.StringUtil;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import java.util.HashMap;
import java.util.Map;

public class BundleCategoryUtil {

    public static void saveBundleCategoryTranslations(BundleFareCategoryDTO bundleFareCategoryDTO) throws ModuleException {
        String id = bundleFareCategoryDTO.getBundledCategoryId().toString();
        Map<String,String> categoryNameTranslations = new HashMap<>();
        Map<String,String> descriptionTranslations = new HashMap<>();

        Map<String, BundleFareCategoryDTO.BundledFareCategoryTranslation> translations = bundleFareCategoryDTO.getTranslations();
        for (String language : translations.keySet()) {

            BundleFareCategoryDTO.BundledFareCategoryTranslation translation = translations.get(language);
            if(translation.getCategoryName() != null) {
                categoryNameTranslations.put(language, translation.getCategoryName());
            }

			if (!StringUtil.isNullOrEmpty(translation.getDescription())) {
                descriptionTranslations.put(language, translation.getDescription());
            }

        }

        saveBundledCategoryName(id,categoryNameTranslations);
        saveBundledCategoryDescription(id,descriptionTranslations);
    }

    public static  void saveBundledCategoryName(String bundledFareCategoryId, Map<String, String> translations) throws ModuleException {
        PromotionModuleServiceLocator.getCommonMasterBD().saveTranslations(translations,
                I18nTranslationUtil.I18nMessageCategory.BUNDLE_CATEGORY_NAME.toString() + bundledFareCategoryId,
                I18nTranslationUtil.I18nMessageCategory.BUNDLE_CATEGORY_NAME.toString());
    }

    public static  void saveBundledCategoryDescription(String bundledFareCategoryId,Map<String, String> translations) throws ModuleException {
        PromotionModuleServiceLocator.getCommonMasterBD().saveTranslations(translations,
                I18nTranslationUtil.I18nMessageCategory.BUNDLE_CATEGORY_DESC.toString() + bundledFareCategoryId,
                I18nTranslationUtil.I18nMessageCategory.BUNDLE_CATEGORY_DESC.toString());
    }
}
