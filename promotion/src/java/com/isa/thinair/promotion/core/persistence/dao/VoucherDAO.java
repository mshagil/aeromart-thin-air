package com.isa.thinair.promotion.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.promotion.api.model.Voucher;
import com.isa.thinair.promotion.api.model.VoucherRedemption;
import com.isa.thinair.promotion.api.to.VoucherSearchRequest;

/**
 * @author chethiya
 *
 */
public interface VoucherDAO {

	/**
	 * @param voucher
	 */
	public void saveVoucher(Voucher voucher);

	/**
	 * @param voucherId
	 * @return returns the Voucher for a given voucher ID
	 */
	public Voucher getVoucher(String voucherId);

	/**
	 *
	 * @param groupId
	 * @return
	 */
	public List<Voucher> getVouchersFromGroupId(String groupId);


	/**
	 * @param start
	 * @param pageSize
	 * @param voucherSearchRequest
	 *            TODO
	 * @param voucherType TODO
	 * @return returns the Page of Vouchers for the given start and pageSize
	 */
	public Page<Voucher> searchVoucher(int start, int pageSize, VoucherSearchRequest voucherSearchRequest, String voucherType);

	/**
	 * @param voucher
	 */
	public void updateVoucher(Voucher voucher);

	/**
	 * @return returns the next Voucher ID
	 */
	public String getNextVoucherId();
	
	/**
	 * @param voucher
	 */
	public void cancelVoucher(VoucherDTO voucherDTO);


	/**
	 * @param voucherId
	 * @return
	 */
	public Voucher getValidVoucher(String voucherId);

	/**
	 * @param voucherRedemption
	 */
	public void saveVoucherRedemption(VoucherRedemption voucherRedemption);

	/**
	 * @param voucherIDList
	 * @return
	 */
	public List<Voucher> getVoucherList(List<String> voucherIDList, Character status);

	/**
	 * @param voucherIDList
	 */
	public void saveOrUpdateAll(List<Voucher> voucherIDList);
	
	public Map<Integer,String> getFlightFarePaxWise(Collection<Integer> paxIds, Collection<Integer> pnrSegIds);
	
	public List<Voucher> getExpiredVouchers();
	
	public String getNextVoucherGroupId();
	
	public void saveVouchers(List<Voucher> voucherlist);
	
	public List<Voucher> getBlockedVouchers();
	
}
