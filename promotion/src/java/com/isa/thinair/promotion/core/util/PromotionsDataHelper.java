package com.isa.thinair.promotion.core.util;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.promotion.api.model.OndPromotion;
import com.isa.thinair.promotion.api.model.OndPromotionCharge;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionRequestConfig;
import com.isa.thinair.promotion.api.model.PromotionTemplate;
import com.isa.thinair.promotion.api.model.PromotionTemplateConfig;
import com.isa.thinair.promotion.api.model.PromotionType;
import com.isa.thinair.promotion.api.to.PromoChargeTo;
import com.isa.thinair.promotion.api.to.PromotionRequestTo;
import com.isa.thinair.promotion.api.to.PromotionTemplateTo;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.core.persistence.dao.PromotionManagementDao;

public class PromotionsDataHelper {

	public static PromotionTemplate toPromtionTemplate(PromotionTemplateTo promotionTemplateTo, 
			UserPrincipal userPrincipal) throws ModuleException {
		PromotionTemplate promotionTemplate = new PromotionTemplate();
		constructPromotionTemplate(promotionTemplateTo, promotionTemplate, userPrincipal);
		return promotionTemplate;
	}

	public static PromotionTemplateTo toPromotionTemplateTo(PromotionTemplate promotionTemplate) {
		PromotionTemplateTo promotionTemplateTo = new PromotionTemplateTo();
		promotionTemplateTo.setPromotinId(promotionTemplate.getPromotionId());
		promotionTemplateTo.setPromotionTypeName(promotionTemplate.getPromotionType().getPromotionName());
		promotionTemplateTo.setPromotionTypeId(promotionTemplate.getPromotionType().getPromotionId());
		promotionTemplateTo.setPromoStartDate(promotionTemplate.getStartDate());
		promotionTemplateTo.setPromoEndDate(promotionTemplate.getEndDate());
		promotionTemplateTo.setStatus(promotionTemplate.getStatus());

		for (PromotionTemplateConfig config : promotionTemplate.getPromotionTemplateConfigs()) {
			promotionTemplateTo.addPromotionTemplateParam(config.getPromoParam(), config.getParamValue());
		}

		PromoChargeTo promoChargeTo;
		for (OndPromotion ondPromotion : promotionTemplate.getOnds()) {
			promotionTemplateTo.addRoute(ondPromotion.getOndCode());

			for (OndPromotionCharge ondPromotionCharge : ondPromotion.getOndPromotionCharges()) {
				promoChargeTo = new PromoChargeTo();
				promoChargeTo.setOndCode(ondPromotion.getOndCode());
				promoChargeTo.setCharge(ondPromotionCharge.getAmount().toString());
				promoChargeTo.setChargeCode(ondPromotionCharge.getChargeCode());
				promotionTemplateTo.addPromoCharge(promoChargeTo);
			}
		}

		return promotionTemplateTo;
	}

	public static PromotionTemplate mergeToPromotionTemplate(PromotionTemplateTo promotionTemplateTo,
			PromotionTemplate promotionTemplate, UserPrincipal userPrincipal) throws ModuleException {
		promotionTemplate.clearOndPromotions();
		promotionTemplate.clearPromotionTemplateConfigs();
		PromotionsHandlerFactory.getPromotionManagementDao().updateEntity(promotionTemplate);

		promotionTemplate = PromotionsHandlerFactory.getPromotionManagementDao().findEntity(PromotionTemplate.class,
				promotionTemplate.getPromotionId());
		constructPromotionTemplate(promotionTemplateTo, promotionTemplate, userPrincipal);
		return promotionTemplate;
	}

	public static PromotionRequest toPromotionRequest(PromotionRequestTo promotionRequestTo, UserPrincipal userPrincipal) {
		PromotionRequest promotionRequest = new PromotionRequest();
		constructPromotionRequst(promotionRequestTo, promotionRequest, userPrincipal);
		return promotionRequest;
	}

	private static void constructPromotionTemplate(PromotionTemplateTo promotionTemplateTo, 
			PromotionTemplate promotionTemplate, UserPrincipal userPrincipal)
			throws ModuleException {

		Pair<String, String> originDest = null;
		Map<String, Set<OndPromotionCharge>> promotionChargesMap = new HashMap<String, Set<OndPromotionCharge>>();
		OndPromotionCharge promoCharge;

		for (PromoChargeTo promoChargeTo : promotionTemplateTo.getPromoCharges()) {
			if (!promotionChargesMap.containsKey(promoChargeTo.getOndCode())) {
				promotionChargesMap.put(promoChargeTo.getOndCode(), new HashSet<OndPromotionCharge>());
			}

			promoCharge = new OndPromotionCharge();
			promoCharge.setRewardFlag(PromoTemplateParam.ChargeAndReward.getRewardFlagByCode(promoChargeTo.getChargeCode()));
			promoCharge.setAmount(AccelAeroCalculator.getTwoScaledBigDecimalFromString(promoChargeTo.getCharge()).doubleValue());
			promoCharge.setChargeCode(promoChargeTo.getChargeCode());
			promoCharge.setChargeRate(PromotionsDaoHelper.getChargeRateId(promoChargeTo.getChargeCode()));
			promoCharge.setStatus(PromotionsInternalConstants.STATUS_ACTIVE);
			promoCharge.setCreatedBy(userPrincipal.getUserId());
			promoCharge.setCreatedDate(new Date());

			promotionChargesMap.get(promoChargeTo.getOndCode()).add(promoCharge);
		}

		PromotionType promotionType = PromotionsHandlerFactory.getPromotionManagementDao().findEntity(PromotionType.class,
				promotionTemplateTo.getPromotionTypeId());

		promotionTemplate.setPromotionType(promotionType);
		promotionTemplate.setStartDate(promotionTemplateTo.getPromoStartDate());
		promotionTemplate.setEndDate(promotionTemplateTo.getPromoEndDate());
		promotionTemplate.setStatus(promotionTemplateTo.getStatus());
		promotionTemplate.setCreatedBy(userPrincipal.getUserId());
		promotionTemplate.setCreatedDate(new Date());

		Set<PromotionTemplateConfig> promoTemplateConfigs;
		if (promotionTemplate.getPromotionTemplateConfigs() != null) {
			promoTemplateConfigs = promotionTemplate.getPromotionTemplateConfigs();
		} else {
			promoTemplateConfigs = new HashSet<PromotionTemplateConfig>();
			promotionTemplate.setPromotionTemplateConfigs(promoTemplateConfigs);
		}
		PromotionTemplateConfig promotionTemplateConfig;
		for (String configKey : promotionTemplateTo.getPromotionTemplateParams().keySet()) {
			promotionTemplateConfig = new PromotionTemplateConfig();
			promotionTemplateConfig.setPromotionTemplate(promotionTemplate);
			promotionTemplateConfig.setPromoParam(configKey);

			promotionTemplateConfig.setParamValue(promotionTemplateTo.getPromotionTemplateParams().get(configKey));
			promotionTemplateConfig.setStatus(PromotionsInternalConstants.STATUS_ACTIVE);
			promotionTemplateConfig.setCreatedBy(userPrincipal.getUserId());
			promotionTemplateConfig.setCreatedDate(new Date());

			promoTemplateConfigs.add(promotionTemplateConfig);
		}

		Set<OndPromotion> promoRoutes;
		if (promotionTemplate.getOnds() != null) {
			promoRoutes = promotionTemplate.getOnds();
		} else {
			promoRoutes = new HashSet<OndPromotion>();
			promotionTemplate.setOnds(promoRoutes);
		}
		OndPromotion promoRoute;
		Set<OndPromotionCharge> promoCharges;
		for (String ond : promotionTemplateTo.getRoutes()) {
			originDest = PromotionsUtils.getOriginAndDestinationAirports(ond);
			promoRoute = new OndPromotion();
			promoRoute.setOndCode(ond);
			promoRoute.setOrigin(originDest.getLeft());
			promoRoute.setDestination(originDest.getRight());
			promoRoute.setPromotionTemplate(promotionTemplate);
			promoRoute.setStatus(PromotionsInternalConstants.STATUS_ACTIVE);
			promoRoute.setCreatedBy(userPrincipal.getUserId());
			promoRoute.setCreatedDate(new Date());

			promoCharges = promotionChargesMap.get(ond);
			for (OndPromotionCharge charge : promoCharges) {
				charge.setOndPromotion(promoRoute);
			}
			promoRoute.setOndPromotionCharges(promoCharges);

			promoRoutes.add(promoRoute);
		}
	}

	private static void constructPromotionRequst(PromotionRequestTo promotionRequestTo, 
			PromotionRequest promotionRequest, UserPrincipal userPrincipal) {
		Date createdDate = new Date();
		PromotionManagementDao promotionManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();

		PromotionTemplate promotionTemplate = promotionManagementDao.findEntity(PromotionTemplate.class,
				promotionRequestTo.getPromotionId());
		promotionRequest.setPromotionTemplate(promotionTemplate);
		promotionRequest.setReservationPaxId(promotionRequestTo.getReservationPaxId());
		promotionRequest.setReservationSegmentId(Integer.valueOf(promotionRequestTo.getReservationSegId()));
		promotionRequest.setPnr(promotionRequestTo.getPnr());
		promotionRequest.setStatus(PromotionsInternalConstants.STATUS_ACTIVE);
		promotionRequest.setCreatedBy(userPrincipal.getName());
		promotionRequest.setCreatedDate(createdDate);

		Set<PromotionRequestConfig> promotionRequestConfigs = new HashSet<PromotionRequestConfig>();
		PromotionRequestConfig promotionRequestConfig;

		for (String configKey : promotionRequestTo.getPromotionRequestParams().keySet()) {
			promotionRequestConfig = new PromotionRequestConfig();
			promotionRequestConfig.setPromotionRequest(promotionRequest);
			promotionRequestConfig.setPromoReqParam(configKey);
			promotionRequestConfig.setParamValue(promotionRequestTo.getPromotionRequestParams().get(configKey));
			promotionRequestConfig.setStatus(PromotionsInternalConstants.STATUS_ACTIVE);
			promotionRequestConfig.setCreatedBy(userPrincipal.getName());
			promotionRequestConfig.setCreatedDate(createdDate);

			promotionRequestConfigs.add(promotionRequestConfig);
		}
		promotionRequest.setPromotionRequestConfigs(promotionRequestConfigs);
	}

	public static String[] getFlexidateRanges(PromotionRequest promotionRequest) {

		String[] ranges = new String[2];

		String lowerBound = "0";
		String upperBound = "0";

		Set<PromotionRequestConfig> requestConfigs = promotionRequest.getPromotionRequestConfigs();

		for (PromotionRequestConfig requestConfig : requestConfigs) {
			if (requestConfig.getPromoReqParam().equals(PromoRequestParam.FlexiDate.FLEXI_DATE_LOWER_BOUND)) {
				lowerBound = requestConfig.getParamValue();
			} else if ((requestConfig.getPromoReqParam().equals(PromoRequestParam.FlexiDate.FLEXI_DATE_UPPER_BOUND))) {
				upperBound = requestConfig.getParamValue();
			}
		}
		ranges[0] = lowerBound;
		ranges[1] = upperBound;
		return ranges;

	}

}
