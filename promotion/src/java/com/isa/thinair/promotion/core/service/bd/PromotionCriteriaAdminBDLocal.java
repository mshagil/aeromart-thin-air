package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.promotion.api.service.PromotionCriteriaAdminBD;

@Local
public interface PromotionCriteriaAdminBDLocal extends PromotionCriteriaAdminBD {

}
