package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PromotionTemplateTo implements Serializable {

	private int promotinId;
	private int promotionTypeId;
	private String promotionTypeName;
	private Date promoStartDate;
	private Date promoEndDate;
	private String status;
	private Map<String, String> promotionTemplateParams;
	private List<String> routes;
	private List<PromoChargeTo> promoCharges;

	public PromotionTemplateTo() {
		promotionTemplateParams = new HashMap<String, String>();
		routes = new ArrayList<String>();
		promoCharges = new ArrayList<PromoChargeTo>();
	}

	public int getPromotinId() {
		return promotinId;
	}

	public void setPromotinId(int promotinId) {
		this.promotinId = promotinId;
	}

	public int getPromotionTypeId() {
		return promotionTypeId;
	}

	public void setPromotionTypeId(int promotionTypeId) {
		this.promotionTypeId = promotionTypeId;
	}

	public String getPromotionTypeName() {
		return promotionTypeName;
	}

	public void setPromotionTypeName(String promotionTypeName) {
		this.promotionTypeName = promotionTypeName;
	}

	public Date getPromoStartDate() {
		return promoStartDate;
	}

	public void setPromoStartDate(Date promoStartDate) {
		this.promoStartDate = promoStartDate;
	}

	public Date getPromoEndDate() {
		return promoEndDate;
	}

	public void setPromoEndDate(Date promoEndDate) {
		this.promoEndDate = promoEndDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getRoutes() {
		return routes;
	}

	public void setRoutes(List<String> routes) {
		this.routes = routes;
	}

	public void addRoute(String route) {
		this.routes.add(route);
	}

	public Map<String, String> getPromotionTemplateParams() {
		return promotionTemplateParams;
	}

	public void setPromotionTemplateParams(Map<String, String> promotionTemplateParams) {
		this.promotionTemplateParams = promotionTemplateParams;
	}

	public void addPromotionTemplateParam(String param, String value) {
		this.promotionTemplateParams.put(param, value);
	}

	public List<PromoChargeTo> getPromoCharges() {
		return promoCharges;
	}

	public void setPromoCharges(List<PromoChargeTo> promoCharges) {
		this.promoCharges = promoCharges;
	}

	public void addPromoCharge(PromoChargeTo promoCharge) {
		this.promoCharges.add(promoCharge);
	}
}
