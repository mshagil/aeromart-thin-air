package com.isa.thinair.promotion.api.service;

import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.to.CustomerPromoCodeSelectionRequest;
import com.isa.thinair.promotion.api.to.PromoCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;

/**
 * Class to hold the Administrative functionalities of the {@link PromotionCriteria} class hierachy.
 * 
 * @author thihara
 * 
 */
public interface PromotionCriteriaAdminBD {
	public static final String SERVICE_NAME = "PromotionCriteriaAdmin";

	/**
	 * Finds the PromotionCriteria by ID.
	 * 
	 * @param promotionCriteriaID
	 *            ID of the promotion criteria to be retrieved.
	 * @return Retrieved PromotionCriteria's DTO object. Or null if the specified ID doesn't exist.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public PromotionCriteriaTO findPromotionCriteria(Long promotionCriteriaID) throws ModuleException;

	/**
	 * get PromotionCriteria by originatedPnr
	 * 
	 * @param originatedPnr
	 * @return
	 * @throws ModuleException
	 */
	public PromotionCriteria getPromotionCriteria(String originatedPnr) throws ModuleException;

	/**
	 * Saves the promotion criteria given.
	 * 
	 * @param promotionCriteriaTO
	 *            DTO containing the data to be saved.
	 * @return The assigned ID of the promotion criteria.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public Long savePromotionCriteria(PromotionCriteriaTO promotionCriteriaTO) throws ModuleException;

	/**
	 * 
	 * Saves promotion criteria object
	 * 
	 * @param promotionCriteria
	 * @return
	 * @throws ModuleException
	 */
	public PromotionCriteria savePromotionCriteria(PromotionCriteria promotionCriteria) throws ModuleException;

	/**
	 * Updates the promotion criteria.
	 * 
	 * @param promotionCriteriaTO
	 *            DTO object with the data to be set.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public long updatePromotionCriteria(PromotionCriteriaTO promotionCriteriaTO) throws ModuleException;

	/**
	 * Deletes the specified promotion criteria. This operation only requires the ID, PromotionCriteria type and the
	 * version to be set.
	 * 
	 * @param promotionCriteriaTO
	 *            DTO object with the necessary data populated.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public void deletePromotionCriteria(PromotionCriteriaTO promotionCriteriaTO) throws ModuleException;

	/**
	 * Searches across promotion criteria.
	 * 
	 * @param promoCriteriaSearch
	 *            Search data DTO with the necessary data set.
	 * @param start
	 *            Starting position of the query results.
	 * @param size
	 *            Size of the query result set.
	 * @return a {@link Page} with the search results enclosed.
	 * @throws ModuleException
	 *             If the underlying operation fails.
	 */
	public Page<PromotionCriteriaTO> searchPromotionCriteria(PromoCriteriaSearchTO promoCriteriaSearch, Integer start,
			Integer size) throws ModuleException;

	/**
	 * Update consumed credits & status of given system generated promotion
	 * 
	 * @param consumedCredits
	 * @param status
	 * @throws ModuleException
	 */
	public void updateSystemGeneratedPromotion(Long promotionId, BigDecimal consumedCredits, String status)
			throws ModuleException;

	/**
	 * 
	 * @param promotionCriteriaTO
	 * @return
	 * @throws ModuleException
	 */
	public long updatePromotionCriteriaStatus(PromotionCriteriaTO promotionCriteriaTO) throws ModuleException;

	/**
	 * Activate system generated promotion, after payment made for onhold reservations
	 * 
	 * @param originPnr
	 * @throws ModuleException
	 */
	public void activateSystemGeneratedPromotion(String originPnr) throws ModuleException;

	/**
	 * Add the language key for description
	 * 
	 * @param criteriaId
	 * @param messageKey
	 * @throws ModuleException
	 */

	public void updatePromotionCodeDescKey(long criteriaId, String messageKey) throws ModuleException;

	/**
	 * Update consumed credits & status of given system generated promotion when OHD reservation cancels
	 * 
	 * @param consumedCredits
	 * @param status
	 * @throws ModuleException
	 */
	public void updateSystemGeneratedPromotionforOHD(Long promotionId, BigDecimal consumedCredits, String status)
			throws ModuleException;
	public List<String> generatePromoCodes(int numOfCodes) throws ModuleException;

	public void updatePromoCodeUtilization(String promoCode) throws ModuleException;
	
	public List<String> getPromoCodesByID(int promoCriteriaId);
	
}
