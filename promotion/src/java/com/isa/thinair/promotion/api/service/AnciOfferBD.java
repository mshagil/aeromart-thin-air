package com.isa.thinair.promotion.api.service;

import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;

/**
 * Interface defining operations related to AnciOfferCriteria
 * 
 * @author thihara
 * 
 */
public interface AnciOfferBD {

	public static final String SERVICE_NAME = "AnciOfferManager";

	/**
	 * Retrieves a {@link Page} of {@link AnciOfferCriteriaTO} according to the given search criteria.
	 * 
	 * @param searchCriteria
	 *            The search criteria.
	 * @param startPosition
	 *            Starting position of the result numbers.
	 * @param recSize
	 *            Number of results to be retrieved.
	 * @return A Page with the search results.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public Page<AnciOfferCriteriaTO> searchAnciOfferCriterias(AnciOfferCriteriaSearchTO searchCriteria, Integer startPosition,
			Integer recSize) throws ModuleException;

	/**
	 * Updates an existing AnciOfferCriteria.
	 * 
	 * @param anciOfferCriteria
	 *            The anci offer criteria to be updated.
	 * @param trackingInfo
	 *            Tracking information of the update request.
	 * @param userPrincipal
	 *            User Principal information of the person invoking the update.
	 * @param onlyUpdateName TODO
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public void updateAnciOfferCriteria(AnciOfferCriteriaTO anciOfferCriteria, TrackInfoDTO trackingInfo,
			UserPrincipal userPrincipal, boolean onlyUpdateName) throws ModuleException;

	/**
	 * Add a new AnciOfferCriteria. Creates new I18nMessageKeys.
	 * 
	 * @param anciOfferCriteria
	 *            The new AnciOfferCriteria to be added.
	 * @param trackingInfo
	 *            Tracking information of the update request.
	 * @param userPrincipal
	 *            User Principal information of the person invoking the update.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public void
			addAnciOfferCriteria(AnciOfferCriteriaTO anciOfferCriteria, TrackInfoDTO trackingInfo, UserPrincipal userPrincipal)
					throws ModuleException;

	/**
	 * Gets an AnciOfferCriteria applicable to the given search criteria. If multiple AnciOfferCriteras match the given
	 * search parameter the first element is returned.
	 * 
	 * @param searchCriteria
	 *            The search criteria determining which AnciOfferCriteria will be retrieve.
	 * @return The retrieved AnciOfferCriteria.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public AnciOfferCriteriaTO getAvailableAnciOffer(AnciOfferCriteriaSearchTO searchCriteria) throws ModuleException;

	public boolean doesAnActiveReplicateCriteriaExist(AnciOfferCriteriaTO anciOfferCriteria) throws ModuleException;

	public Collection<AnciOfferCriteriaTO> getAllAvailableAnciOffers(AnciOfferCriteriaSearchTO searchCriteria) throws ModuleException;

}
