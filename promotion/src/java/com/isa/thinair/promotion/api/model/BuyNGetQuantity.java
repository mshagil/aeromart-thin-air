package com.isa.thinair.promotion.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Class specifying the quantity criteria for get and buy promotion. A typical instance of this class will be used to
 * represent something like, Buy 2 Adult, 3 Child, 1 Infant, fares and get 1 Adult, 1 Child, 1 Infant fares free.
 * 
 * @author thihara
 * 
 * @hibernate.class table = "T_PROMO_CODE_BUY_N_GET_QTY"
 * 
 *                  Table columns that aren't directly loaded into the class : BNG_CRITERIA_ID (References
 *                  T_PROMO_CODE_CRITERIA)
 */
public class BuyNGetQuantity extends Persistent {

	private static final long serialVersionUID = 1899788454123L;

	private Long bngQtyID;

	private Integer qualifiedAdultCount;
	private Integer qualifiedChildCount;
	private Integer qualifiedInfantCount;

	private Integer freeAdultCount;
	private Integer freeChildCount;
	private Integer freeInfantCount;

	/**
	 * @hibernate.id column = "BNG_QTY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PROMO_CODE_BUY_N_GET_QTY"
	 * 
	 * @return The assigned ID for this BuyNGetQuantity object. Or null for new unsaved object instances.
	 */
	public Long getBngQtyID() {
		return bngQtyID;
	}

	public void setBngQtyID(Long bngQtyID) {
		this.bngQtyID = bngQtyID;
	}

	/**
	 * @hibernate.property column = "QUALIFYING_ADULT_COUNT"
	 * @return The number of adults needed to match this criteria
	 */
	public Integer getQualifiedAdultCount() {
		return qualifiedAdultCount;
	}

	public void setQualifiedAdultCount(Integer qualifiedAdultCount) {
		this.qualifiedAdultCount = qualifiedAdultCount;
	}

	/**
	 * @hibernate.property column = "QUALIFYING_CHILD_COUNT"
	 * @return The number of children needed to match this criteria
	 */
	public Integer getQualifiedChildCount() {
		return qualifiedChildCount;
	}

	public void setQualifiedChildCount(Integer qualifiedChildCount) {
		this.qualifiedChildCount = qualifiedChildCount;
	}

	/**
	 * @hibernate.property column = "QUALIFYING_INFANT_COUNT"
	 * @return The number of infants needed to match this criteria
	 */
	public Integer getQualifiedInfantCount() {
		return qualifiedInfantCount;
	}

	public void setQualifiedInfantCount(Integer qualifiedInfantCount) {
		this.qualifiedInfantCount = qualifiedInfantCount;
	}

	/**
	 * @hibernate.property column = "FREE_ADULT_COUNT"
	 * @return The number of free adult fares awarded when this quantity criteria is matched
	 */
	public Integer getFreeAdultCount() {
		return freeAdultCount;
	}

	public void setFreeAdultCount(Integer freeAdultCount) {
		this.freeAdultCount = freeAdultCount;
	}

	/**
	 * @hibernate.property column = "FREE_CHILD_COUNT"
	 * @return The number of free child fares awarded when this quantity criteria is matched
	 */
	public Integer getFreeChildCount() {
		return freeChildCount;
	}

	public void setFreeChildCount(Integer freeChildCount) {
		this.freeChildCount = freeChildCount;
	}

	/**
	 * @hibernate.property column = "FREE_INFANT_COUNT"
	 * @return The number of free infant fares awarded when this quantity criteria is matched
	 */
	public Integer getFreeInfantCount() {
		return freeInfantCount;
	}

	public void setFreeInfantCount(Integer freeInfantCount) {
		this.freeInfantCount = freeInfantCount;
	}

}
