package com.isa.thinair.promotion.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author rumesh
 * 
 * @hibernate.class table = "t_loyalty_file_log"
 */
public class LoyaltyFileLog extends Tracking implements Serializable {

	private static final long serialVersionUID = -1606463057269641794L;

	private Integer fileId;
	private String fileName;
	private String fileType;
	private String status;
	private String serviceProvider;

	/**
	 * @hibernate.id column = "LOYALTY_FILE_LOG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_LOYALTY_FILE_LOG"
	 * 
	 * @return the fileId
	 */
	public Integer getFileId() {
		return fileId;
	}

	/**
	 * @param fileId
	 *            the fileId to set
	 */
	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	/**
	 * @hibernate.property column = "LOYALTY_FILE_LOG_NAME"
	 * 
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @hibernate.property column = "LOYALTY_FILE_LOG_TYPE"
	 * 
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType
	 *            the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**LMS_SERVICE_PROVIDE
	 * @hibernate.property column = "STATUS"
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @hibernate.property column = "LMS_SERVICE_PROVIDER"
	 * 
	 * @return the serviceProvider
	 */
	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}



	public enum FILE_STATUS {
		CREATED, UPLOADED, UPLOAD_FAILED
	};

	public enum FILE_TYPE {
		FLOWN_FILE, PRODUCT_FILE
	}
}
