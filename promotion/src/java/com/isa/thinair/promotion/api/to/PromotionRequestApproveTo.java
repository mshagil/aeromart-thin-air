package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionRequestConfig;
import com.isa.thinair.promotion.api.model.PromotionTemplate;

public class PromotionRequestApproveTo implements Serializable {

	private PromotionRequest promotionRequest;
	private int flightSegId;
	private Date departureDate;
	private String segmentCode;

	public PromotionRequest getPromotionRequest() {
		return promotionRequest;
	}

	public void setPromotionRequest(PromotionRequest promotionRequest) {
		this.promotionRequest = promotionRequest;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	// Wrapper methods starting
	public Integer getPromotionRequestId() {
		return promotionRequest.getPromotionRequestId();
	}

	public PromotionTemplate getPromotionTemplate() {
		return promotionRequest.getPromotionTemplate();
	}

	public String getStatus() {
		return promotionRequest.getStatus();
	}

	public Integer getReservationSegmentId() {
		return promotionRequest.getReservationSegmentId();
	}

	public Integer getReservationPaxId() {
		return promotionRequest.getReservationPaxId();
	}

	public Set<PromotionRequestConfig> getPromotionRequestConfigs() {
		return promotionRequest.getPromotionRequestConfigs();
	}

	public String getPnr() {
		return promotionRequest.getPnr();
	}

}
