package com.isa.thinair.promotion.api.model.lms;

/**
 * 
 * @author rumesh
 * 
 */
public class BasicDataType {

	protected DATA_F dataField = null;
	protected String data = null;

	public BasicDataType(DATA_F dataField, String data) {
		this.dataField = dataField;
		this.data = data;
	}

	public BasicDataType() {

	}

	public enum DATA_F {
		MANDATORY, OPTIONAL
	}

	public DATA_F getDataField() {
		return dataField;
	}

	public String getData() {
		return data;
	}
}
