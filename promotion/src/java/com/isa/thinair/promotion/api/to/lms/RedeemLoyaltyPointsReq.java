package com.isa.thinair.promotion.api.to.lms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**
 * Request DTO for redeem loyalty points
 * 
 * @author rumesh
 * 
 */
public class RedeemLoyaltyPointsReq implements Serializable {

	private static final long serialVersionUID = 7217774821664121259L;

	private String memberAccountId;

	private Map<String, Double> productPoints;

	private AppIndicatorEnum appIndicator;

	private BigDecimal loyaltyPointsToCurrencyConversionRate;
	
	private String pnr;
	
	private String memberEnrollingCarrier;

	private String memberExternalId;

	public String getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	public Map<String, Double> getProductPoints() {
		return productPoints;
	}

	public void setProductPoints(Map<String, Double> productPoints) {
		this.productPoints = productPoints;
	}

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public BigDecimal getLoyaltyPointsToCurrencyConversionRate() {
		return loyaltyPointsToCurrencyConversionRate;
	}

	public void setLoyaltyPointsToCurrencyConversionRate(BigDecimal loyaltyPointsToCurrencyConversionRate) {
		this.loyaltyPointsToCurrencyConversionRate = loyaltyPointsToCurrencyConversionRate;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getMemberEnrollingCarrier() {
		return memberEnrollingCarrier;
	}

	public void setMemberEnrollingCarrier(String memberEnrollingCarrier) {
		this.memberEnrollingCarrier = memberEnrollingCarrier;
	}

	public String getMemberExternalId() {
		return memberExternalId;
	}

	public void setMemberExternalId(String memberExternalId) {
		this.memberExternalId = memberExternalId;
	}
	
	
}
