package com.isa.thinair.promotion.api.model;

/**
 * @author chanaka
 * @hibernate.class table = "T_CAMPAIGN_PROMO_CODE" 
 */
public class CampaignPromoCode {
	
	private Integer campaignPromoCodeId;
	
	private Campaign campaign;
	
	private String emailId;
	
	private String promoCode;

	/**
	 * @return the campaignPromoCodeId
	 * @hibernate.id column = "CAMPAIGN_PROMO_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CAMPAIGN_PROMO_CODE"
	 */
	public Integer getCampaignPromoCodeId() {
		return campaignPromoCodeId;
	}

	/**
	 * @param campaignPromoCodeId the campaignPromoCodeId to set
	 */
	public void setCampaignPromoCodeId(Integer campaignPromoCodeId) {
		this.campaignPromoCodeId = campaignPromoCodeId;
	}

	/**
	 * @hibernate.many-to-one column="CAMPAIGN_ID" class="com.isa.thinair.promotion.api.model.Campaign" 
	 * 
	 * @return campaign
	 */
	public Campaign getCampaign() {
		return campaign;
	}

	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}
	
	/**
	 * @return the emailId
	 * @hibernate.property column = "SENT_EMAIL_ID"
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the promoCode
	 * @hibernate.property column = "PROMO_CODE"
	 */
	public String getPromoCode() {
		return promoCode;
	}

	/**
	 * @param promoCode the promoCode to set
	 */
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	
}
