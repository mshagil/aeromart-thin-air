package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;

public class PromotionRequestTo implements Serializable {

	private static final long serialVersionUID = 1L;
	private int promotionRequestId;
	private int promotionId;
	private String status;
	private String reservationSegId;
	private int reservationPaxId;
	private Map<String, String> promotionRequestParams;
	private String resContactName;
	private String pnr;
	private String promoTypeId;
	private String flexiTravelRefNumbers;
	private int lowerBound;
	private int upperBound;
	private String agreedToMove;
	private BigDecimal promoRequestCharge;
	private List<String> boundSegmentIds; // if outbound--> seg1, seg2.. || inbound--> seg1, seg2..
	private int direction = PromotionsInternalConstants.DIRECTION_NONE;

	public PromotionRequestTo() {
		promotionRequestParams = new HashMap<String, String>();
	}

	public int getPromotionRequestId() {
		return promotionRequestId;
	}

	public void setPromotionRequestId(int promotionRequestId) {
		this.promotionRequestId = promotionRequestId;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReservationSegId() {
		return reservationSegId;
	}

	public void setReservationSegId(String reservationSegId) {
		this.reservationSegId = reservationSegId;
	}

	public int getReservationPaxId() {
		return reservationPaxId;
	}

	public void setReservationPaxId(int reservationPaxId) {
		this.reservationPaxId = reservationPaxId;
	}

	public Map<String, String> getPromotionRequestParams() {
		return promotionRequestParams;
	}

	public void setPromotionRequestParams(Map<String, String> promotionRequestParams) {
		this.promotionRequestParams = promotionRequestParams;
	}

	public void addPromotionRequestParam(String param, String value) {
		this.promotionRequestParams.put(param, value);
	}

	public String getResContactName() {
		return resContactName;
	}

	public void setResContactName(String resContactName) {
		this.resContactName = resContactName;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPromoTypeId() {
		return promoTypeId;
	}

	public void setPromoTypeId(String promoTypeId) {
		this.promoTypeId = promoTypeId;
	}

	public String getFlexiTravelRefNumbers() {
		return flexiTravelRefNumbers;
	}

	public void setFlexiTravelRefNumbers(String flexiTravelRefNumbers) {
		this.flexiTravelRefNumbers = flexiTravelRefNumbers;
	}

	public int getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(int lowerBound) {
		this.lowerBound = lowerBound;
	}

	public int getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(int upperBound) {
		this.upperBound = upperBound;
	}

	public String getAgreedToMove() {
		return agreedToMove;
	}

	public void setAgreedToMove(String agreedToMove) {
		this.agreedToMove = agreedToMove;
	}

	public BigDecimal getPromoRequestCharge() {
		return promoRequestCharge;
	}

	public void setPromoRequestCharge(BigDecimal promoRequestCharge) {
		this.promoRequestCharge = promoRequestCharge;
	}

	public List<String> getBoundSegmentIds() {
		return boundSegmentIds;
	}

	public void setBoundSegmentIds(List<String> boundSegmentIds) {
		this.boundSegmentIds = boundSegmentIds;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public PromotionRequestTo clone() {
		PromotionRequestTo cloneRequestTo = new PromotionRequestTo();
		cloneRequestTo.setPromotionRequestId(this.promotionRequestId);
		cloneRequestTo.setPromotionId(this.promotionId);
		cloneRequestTo.setStatus(this.status);
		cloneRequestTo.setReservationSegId(this.reservationSegId);
		cloneRequestTo.setReservationPaxId(this.reservationPaxId);
		cloneRequestTo.setPromotionRequestParams(this.promotionRequestParams);
		cloneRequestTo.setResContactName(this.resContactName);
		cloneRequestTo.setPnr(this.pnr);
		cloneRequestTo.setPromoTypeId(this.promoTypeId);
		cloneRequestTo.setFlexiTravelRefNumbers(this.flexiTravelRefNumbers);
		cloneRequestTo.setLowerBound(this.lowerBound);
		cloneRequestTo.setUpperBound(this.upperBound);
		cloneRequestTo.setAgreedToMove(this.agreedToMove);
		cloneRequestTo.setPromoRequestCharge(this.promoRequestCharge);
		cloneRequestTo.setBoundSegmentIds(this.boundSegmentIds);
		cloneRequestTo.setDirection(this.direction);

		return cloneRequestTo;

	}

	@Override
	public int hashCode() {
		String key = BeanUtils.nullHandler(pnr) 
				+ BeanUtils.nullHandler(promotionId) 
				+ BeanUtils.nullHandler(reservationPaxId)
				+ BeanUtils.nullHandler(reservationSegId);
		return new HashCodeBuilder().append(key).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		} else if (obj == null) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}

		PromotionRequestTo other = (PromotionRequestTo) obj;

		if (EqualsBuilder.reflectionEquals(this.getPnr(), other.getPnr())) {
			if (EqualsBuilder.reflectionEquals(this.getPromotionId(), other.getPromotionId())) {
				if (EqualsBuilder.reflectionEquals(this.getReservationPaxId(), other.getReservationPaxId())) {
					if (EqualsBuilder.reflectionEquals(this.getReservationSegId(), other.getReservationSegId())) {
						return true;
					}
				}
			}
		}

		return false;
	}

}
