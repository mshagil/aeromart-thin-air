package com.isa.thinair.promotion.api.utils;

import com.isa.thinair.promotion.api.model.BundleFareDescriptionTemplate;
import com.isa.thinair.promotion.api.model.BundleFareDescriptionTranslationTemplate;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTranslationTemplateDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BundleFareDescriptionTemplateUtils {

	private static final String DATE_FORMAT = "dd-MMM-yyyy HH:mm:ss";

	private BundleFareDescriptionTemplateUtils() {
	}

	public static Page<BundleFareDescriptionTemplateDTO> convertPageToDTO(
			Page<BundleFareDescriptionTemplate> bundleFareDescriptionTemplatePage) {

		return new Page<>(bundleFareDescriptionTemplatePage.getTotalNoOfRecords(),
				bundleFareDescriptionTemplatePage.getStartPosition(), bundleFareDescriptionTemplatePage.getEndPosition(),
				toBundleFareDescriptionTemplateDTOCollection(bundleFareDescriptionTemplatePage.getPageData()));
	}

	public static Collection<BundleFareDescriptionTemplateDTO> toBundleFareDescriptionTemplateDTOCollection(
			Collection<BundleFareDescriptionTemplate> templates) {
		Collection<BundleFareDescriptionTemplateDTO> bundleFareDescriptionTemplateDTOs = new ArrayList<>();
		if (!CollectionUtils.isEmpty(templates)) {
			for (BundleFareDescriptionTemplate template : templates) {
				bundleFareDescriptionTemplateDTOs.add(toBundleFareDescriptionTemplateDTO(template));
			}
		}
		return bundleFareDescriptionTemplateDTOs;
	}

	public static BundleFareDescriptionTemplate toBundleFareDescriptionTemplate(BundleFareDescriptionTemplateDTO templateDTO)
			throws ParseException {
		BundleFareDescriptionTemplate template = new BundleFareDescriptionTemplate();

		template.setTemplateName(templateDTO.getTemplateName());
		template.setDefaultFreeServicesTemplate(StringUtil.convertToHex(templateDTO.getDefaultFreeServicesContent()));
		template.setDefaultPaidServicesTemplate(StringUtil.convertToHex(templateDTO.getDefaultPaidServicesContent()));

		if (templateDTO.getVersion() != null) {
			template.setVersion(templateDTO.getVersion());
		}

		if (templateDTO.getTemplateID() != null) {
			template.setTemplateID(templateDTO.getTemplateID());
		}

		if (!StringUtil.isNullOrEmpty(templateDTO.getCreatedDate())) {
			template.setCreatedDate(CalendarUtil.getParsedTime(templateDTO.getCreatedDate(), DATE_FORMAT));
		} else {
			template.setCreatedDate(CalendarUtil.getCurrentZuluDateTime());
		}

		if (!CollectionUtils.isEmpty(templateDTO.getTranslationTemplates())) {
			Set<BundleFareDescriptionTranslationTemplate> translationTemplates = new HashSet<>();
			for (BundleFareDescriptionTranslationTemplateDTO translationTemplateDto : templateDTO.getTranslationTemplates()
					.values()) {
				BundleFareDescriptionTranslationTemplate translationTemplate = new BundleFareDescriptionTranslationTemplate();

				if (translationTemplateDto.getVersion() != null) {
					translationTemplate.setVersion(translationTemplateDto.getVersion());
				}

				if (translationTemplateDto.getTemplateID() != null) {
					translationTemplate.setTranslationTemplateId(translationTemplateDto.getTemplateID());
				}

				translationTemplate.setLanguageCode(translationTemplateDto.getLanguageCode());
				translationTemplate
						.setFreeServicesTemplate(StringUtil.convertToHex(translationTemplateDto.getFreeServicesContent()));
				translationTemplate
						.setPaidServicesTemplate(StringUtil.convertToHex(translationTemplateDto.getPaidServicesContent()));
				translationTemplate.setBundleFareDescriptionTemplate(template);

				translationTemplates.add(translationTemplate);
			}
			template.setTranslationTemplates(translationTemplates);
		}

		return template;
	}

	
	
	public static BundleFareDescriptionTranslationTemplate toBundleFareDescriptionTranslationTemplate(
			BundleFareDescriptionTranslationTemplateDTO translationTemplateDto) throws ParseException {
		
		BundleFareDescriptionTranslationTemplate translationdecsTemplate = new BundleFareDescriptionTranslationTemplate();
		translationdecsTemplate.setVersion(translationTemplateDto.getVersion());
		translationdecsTemplate.setTranslationTemplateId(translationTemplateDto.getTemplateID());
		translationdecsTemplate.setLanguageCode(translationTemplateDto.getLanguageCode());
		translationdecsTemplate
				.setFreeServicesTemplate(StringUtil.convertToHex(translationTemplateDto.getFreeServicesContent()));
		translationdecsTemplate
				.setPaidServicesTemplate(StringUtil.convertToHex(translationTemplateDto.getPaidServicesContent()));
		return translationdecsTemplate;
	}
	
	public static BundleFareDescriptionTemplateDTO toBundleFareDescriptionTemplateDTO(BundleFareDescriptionTemplate template) {
		BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO = new BundleFareDescriptionTemplateDTO();
		if (template != null) {
			bundleFareDescriptionTemplateDTO.setTemplateID(template.getTemplateID());
			bundleFareDescriptionTemplateDTO.setVersion(template.getVersion());
			bundleFareDescriptionTemplateDTO.setCreatedDate(CalendarUtil.formatDate(template.getCreatedDate(), DATE_FORMAT));
			bundleFareDescriptionTemplateDTO.setTemplateName(template.getTemplateName());
			bundleFareDescriptionTemplateDTO
					.setDefaultFreeServicesContent(StringUtil.getUnicode(template.getDefaultFreeServicesTemplate()));
			bundleFareDescriptionTemplateDTO
					.setDefaultPaidServicesContent(StringUtil.getUnicode(template.getDefaultPaidServicesTemplate()));
			if (!CollectionUtils.isEmpty(template.getTranslationTemplates())) {
				Map<String, BundleFareDescriptionTranslationTemplateDTO> translationTemplateDTOMap = bundleFareDescriptionTemplateDTO
						.getTranslationTemplates();
				for (BundleFareDescriptionTranslationTemplate translatedTemplate : template.getTranslationTemplates()) {
					BundleFareDescriptionTranslationTemplateDTO translationTemplateDTO =
							toBundleFareDescriptionTranslationTemplateDTO(translatedTemplate);
					translationTemplateDTOMap.put(translationTemplateDTO.getLanguageCode(), translationTemplateDTO);
				}
			}
		}
		return bundleFareDescriptionTemplateDTO;
	}

	private static BundleFareDescriptionTranslationTemplateDTO toBundleFareDescriptionTranslationTemplateDTO(
			BundleFareDescriptionTranslationTemplate translatedTemplate) {
		BundleFareDescriptionTranslationTemplateDTO translationTemplateDTO = new BundleFareDescriptionTranslationTemplateDTO();
		translationTemplateDTO.setVersion(translatedTemplate.getVersion());
		translationTemplateDTO.setLanguageCode(translatedTemplate.getLanguageCode());
		translationTemplateDTO.setTemplateID(translatedTemplate.getTranslationTemplateId());
		translationTemplateDTO
				.setFreeServicesContent(StringUtil.getUnicode(translatedTemplate.getFreeServicesTemplate()));
		translationTemplateDTO
				.setPaidServicesContent(StringUtil.getUnicode(translatedTemplate.getPaidServicesTemplate()));
		return translationTemplateDTO;
	}

}
