package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author chanaka
 *
 */
public class CustomerPromoCodeSelectionRequest implements Serializable{

	private static final long serialVersionUID = 2646471037783543324L;
	
	private Date reservationFromDate;

	private Date reservationToDate;

	private Date flightFromDate;

	private Date flightToDate;
	
	private String ageFrom;
	
	private String ageTo;
	
	private Set<String> locations;
	
	private Set<String> fareTypes;
	
	private Set<String> applicableONDs;
	
	private boolean anyOrigination = false;
	
	private boolean anyDestination = false;
	
	private Set<String> anyOriginationList;
	
	private Set<String> anyDestinationList;

	/**
	 * @return the reservationFromDate
	 */
	public Date getReservationFromDate() {
		return reservationFromDate;
	}

	/**
	 * @param reservationFromDate the reservationFromDate to set
	 */
	public void setReservationFromDate(Date reservationFromDate) {
		this.reservationFromDate = reservationFromDate;
	}

	/**
	 * @return the reservationToDate
	 */
	public Date getReservationToDate() {
		return reservationToDate;
	}

	/**
	 * @param reservationToDate the reservationToDate to set
	 */
	public void setReservationToDate(Date reservationToDate) {
		this.reservationToDate = reservationToDate;
	}

	/**
	 * @return the flightFromDate
	 */
	public Date getFlightFromDate() {
		return flightFromDate;
	}

	/**
	 * @param flightFromDate the flightFromDate to set
	 */
	public void setFlightFromDate(Date flightFromDate) {
		this.flightFromDate = flightFromDate;
	}

	/**
	 * @return the flightToDate
	 */
	public Date getFlightToDate() {
		return flightToDate;
	}

	/**
	 * @param flightToDate the flightToDate to set
	 */
	public void setFlightToDate(Date flightToDate) {
		this.flightToDate = flightToDate;
	}

	/**
	 * @return the ageFrom
	 */
	public String getAgeFrom() {
		return ageFrom;
	}

	/**
	 * @param ageFrom the ageFrom to set
	 */
	public void setAgeFrom(String ageFrom) {
		this.ageFrom = ageFrom;
	}

	/**
	 * @return the ageTo
	 */
	public String getAgeTo() {
		return ageTo;
	}

	/**
	 * @param ageTo the ageTo to set
	 */
	public void setAgeTo(String ageTo) {
		this.ageTo = ageTo;
	}

	/**
	 * @return the locations
	 */
	public Set<String> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(Set<String> locations) {
		this.locations = locations;
	}

	/**
	 * @return the fareTypes
	 */
	public Set<String> getFareTypes() {
		return fareTypes;
	}

	/**
	 * @param fareTypes the fareTypes to set
	 */
	public void setFareTypes(Set<String> fareTypes) {
		this.fareTypes = fareTypes;
	}
	
	/**
	 * @return the applicableONDs
	 */
	public Set<String> getApplicableONDs() {
		return applicableONDs;
	}

	/**
	 * @param applicableONDs the applicableONDs to set
	 */
	public void setApplicableONDs(Set<String> applicableONDs) {
		this.applicableONDs = applicableONDs;
	}

	/**
	 * @return the anyOrigination
	 */
	public boolean isAnyOrigination() {
		return anyOrigination;
	}

	/**
	 * @param anyOrigination the anyOrigination to set
	 */
	public void setAnyOrigination(boolean anyOrigination) {
		this.anyOrigination = anyOrigination;
	}

	/**
	 * @return the anyDestination
	 */
	public boolean isAnyDestination() {
		return anyDestination;
	}

	/**
	 * @param anyDestination the anyDestination to set
	 */
	public void setAnyDestination(boolean anyDestination) {
		this.anyDestination = anyDestination;
	}

	/**
	 * @return the anyOriginationList
	 */
	public Set<String> getAnyOriginationList() {
		return anyOriginationList;
	}

	/**
	 * @param anyOriginationList the anyOriginationList to set
	 */
	public void setAnyOriginationList(Set<String> anyOriginationList) {
		this.anyOriginationList = anyOriginationList;
	}

	/**
	 * @return the anyDestinationList
	 */
	public Set<String> getAnyDestinationList() {
		return anyDestinationList;
	}

	/**
	 * @param anyDestinationList the anyDestinationList to set
	 */
	public void setAnyDestinationList(Set<String> anyDestinationList) {
		this.anyDestinationList = anyDestinationList;
	}

}
