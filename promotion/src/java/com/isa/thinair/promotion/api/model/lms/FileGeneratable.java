package com.isa.thinair.promotion.api.model.lms;

import java.util.Date;

/**
 * 
 * @author rumesh
 * 
 */
public interface FileGeneratable {

	public String getFormattedData();

	public String getFileName(Date processingDate);
}
