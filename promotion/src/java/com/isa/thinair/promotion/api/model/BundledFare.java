package com.isa.thinair.promotion.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * Class holds criteria to offer bundled fares
 * 
 * @author rumesh
 * 
 * @hibernate.class table = "T_BUNDLED_FARE"
 */
public class BundledFare extends Tracking {

	public static final String ACTIVE = "ACT";
	public static final String INACTIVE = "INA";

	private static final long serialVersionUID = 7776456365403090318L;

	private Integer bundledFareId;

	private String bundledFareName;

	private Set<String> bookingClasses;

	private Date flightDateFrom;

	private Date flightDateTo;

	private Set<String> ondCodes;

	private Set<String> agents;

	private Set<String> flightNumbers;

	private Set<String> pointOfSale;

	private String chargeCode;

	private BigDecimal defaultBundledFee;
	
	private boolean periodSelected;
	
	private Set<BundledFarePeriod> bundledFarePeriods;

	private String status;

	protected Set<String> applicableSalesChannels;
	
	private Integer cutOffTime;

	private Integer bundledCategoryID;

	private String defaultBundled;
	
	private Integer sourceBundledFareId;

	private BundledFareCategory bundledFareCategory;

	/**
	 * @return the bundledFareId
	 * @hibernate.id column = "BUNDLED_FARE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BUNDLED_FARE"
	 */
	public Integer getBundledFareId() {
		return bundledFareId;
	}

	/**
	 * @param bundledFareId
	 *            the bundledFareId to set
	 */
	public void setBundledFareId(Integer bundledFareId) {
		this.bundledFareId = bundledFareId;
	}

	/**
	 * @return the bundledFareName
	 * @hibernate.property column = "BUNDLED_FARE_NAME"
	 */
	public String getBundledFareName() {
		return bundledFareName;
	}

	/**
	 * @param bundledFareName
	 *            the bundledFareName to set
	 */
	public void setBundledFareName(String bundledFareName) {
		this.bundledFareName = bundledFareName;
	}

	/**
	 * @return the bookingClasses
	 * @hibernate.set lazy="false" table="T_BUNDLED_FARE_BOOKING_CLASS"
	 * @hibernate.collection-element column="BOOKING_CODE" type="string"
	 * @hibernate.collection-key column="BUNDLED_FARE_ID"
	 */
	public Set<String> getBookingClasses() {
		return bookingClasses;
	}

	/**
	 * @param bookingClasses
	 *            the bookingClasses to set
	 */
	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	/**
	 * @return the flightDateFrom
	 * @hibernate.property column = "FLIGHT_DATE_FROM"
	 */
	public Date getFlightDateFrom() {
		return flightDateFrom;
	}

	/**
	 * @param flightDateFrom
	 *            the flightDateFrom to set
	 */
	public void setFlightDateFrom(Date flightDateFrom) {
		this.flightDateFrom = flightDateFrom;
	}

	/**
	 * @return the flightDateTo
	 * @hibernate.property column = "FLIGHT_DATE_TO"
	 */
	public Date getFlightDateTo() {
		return flightDateTo;
	}

	/**
	 * @param flightDateTo
	 *            the flightDateTo to set
	 */
	public void setFlightDateTo(Date flightDateTo) {
		this.flightDateTo = flightDateTo;
	}

	/**
	 * @return the ondCodes
	 * @hibernate.set lazy="false" table="T_BUNDLED_FARE_OND"
	 * @hibernate.collection-element column="OND_CODE" type="string"
	 * @hibernate.collection-key column="BUNDLED_FARE_ID"
	 * 
	 */
	public Set<String> getOndCodes() {
		return ondCodes;
	}

	/**
	 * @param ondCodes
	 *            the ondCodes to set
	 */
	public void setOndCodes(Set<String> ondCodes) {
		this.ondCodes = ondCodes;
	}

	/**
	 * @return the agents
	 * @hibernate.set lazy="false" table="T_BUNDLED_FARE_AGENT"
	 * @hibernate.collection-element column="AGENT_CODE" type="string"
	 * @hibernate.collection-key column="BUNDLED_FARE_ID"
	 */
	public Set<String> getAgents() {
		return agents;
	}

	/**
	 * @param agents
	 *            the agents to set
	 */
	public void setAgents(Set<String> agents) {
		this.agents = agents;
	}

	/**
	 * @return the flightNumbers
	 * @hibernate.set lazy="false" table="T_BUNDLED_FARE_FLIGHT"
	 * @hibernate.collection-element column="FLIGHT_NUMBER" type="string"
	 * @hibernate.collection-key column="BUNDLED_FARE_ID"
	 */
	public Set<String> getFlightNumbers() {
		return flightNumbers;
	}

	/**
	 * @param flightNumbers
	 *            the flightNumbers to set
	 */
	public void setFlightNumbers(Set<String> flightNumbers) {
		this.flightNumbers = flightNumbers;
	}

	/**
	 * @return the chargeCode
	 * @hibernate.property column = "CHARGE_CODE"
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            the chargeCode to set
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return the bundledFee ==> ONLY BE USED for master data setup otherwise refer getEffectiveBundleFee
	 * @hibernate.property column = "BUNDLE_FEE"
	 */
	public BigDecimal getDefaultBundledFee() {
		return defaultBundledFee;
	}

	/**
	 * @param bundledFee
	 *            the bundledFee to set
	 * 
	 */
	public void setDefaultBundledFee(BigDecimal bundledFee) {
		this.defaultBundledFee = bundledFee;
	}
	
	
	
	/**
	 * @hibernate.set lazy="false" table="BundledFarePeriod" cascade="all-delete-orphan" inverse="false";
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.BundledFarePeriod"
	 * @hibernate.collection-key column="BUNDLED_FARE_ID"
	 */
	public Set<BundledFarePeriod> getBundledFarePeriods() {
		return bundledFarePeriods;
	}

	public void setBundledFarePeriods(Set<BundledFarePeriod> bundledFarePeriods) {
		this.bundledFarePeriods = bundledFarePeriods;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @hibernate.set lazy="false" table="T_BUNDLED_FARE_CHANNEL"
	 * @hibernate.collection-element column="SALES_CHANNEL_CODE" type="string"
	 * @hibernate.collection-key column="BUNDLED_FARE_ID"
	 * 
	 * @see #applicableSalesChannels
	 */
	public Set<String> getApplicableSalesChannels() {
		return applicableSalesChannels;
	}

	public void setApplicableSalesChannels(Set<String> applicableSalesChannels) {
		this.applicableSalesChannels = applicableSalesChannels;
	}
	
	/**
	 * @hibernate.property column = "CUTOFF_TIME_IN_HOURS"
	 * @return
	 */
	public Integer getCutOffTime() {
		return cutOffTime;
	}

	public void setCutOffTime(Integer cutOffTime) {
		this.cutOffTime = cutOffTime;
	}

	/**
	 * @hibernate.property column = "BUNDLED_FARE_CATEGORY_ID"
	 * @return
	 */
	public Integer getBundledCategoryID() {
		return bundledCategoryID;
	}

	public void setBundledCategoryID(Integer bundledCategoryID) {
		this.bundledCategoryID = bundledCategoryID;
	}

	/**
	 * @hibernate.property column = "DEFAULT_BUNDLED"
	 * @return
	 */
	public String getDefaultBundled() {
		return defaultBundled;
	}

	public void setDefaultBundled(String defaultBundled) {
		this.defaultBundled = defaultBundled;
	}

	/**
	 * 
	 * @hibernate.many-to-one class="com.isa.thinair.promotion.api.model.BundledFareCategory"
	 * column="BUNDLED_FARE_CATEGORY_ID" insert="false" update="false" lazy="false"
	 * @return
	 */
	public BundledFareCategory getBundledFareCategory() {
		return bundledFareCategory;
	}

	/**
	 * @return the pointOfSale
	 * @hibernate.set table="T_BUNDLED_FARE_POS" cascade="all"
	 * @hibernate.collection-key column="BUNDLED_FARE_ID"
	 * @hibernate.collection-element type="string" column="COUNTRY_CODE"
	 */
	public Set<String> getPointOfSale() {
		return pointOfSale;
	}

	/**
	 * @param pointOfSale
	 *            the pointOfSale to set
	 */
	public void setPointOfSale(Set<String> pointOfSale) {
		this.pointOfSale = pointOfSale;
	}

	public void setBundledFareCategory(BundledFareCategory bundledFareCategory) {
		this.bundledFareCategory = bundledFareCategory;
	}

	public void setPeriodSelected(boolean bundleSelected) {
		this.periodSelected = bundleSelected;
	}
	
	/**
	 * @hibernate.property column = "SOURCE_BUNDLED_FARE_ID"
	 * @return
	 */
	public Integer getSourceBundledFareId() {
		return sourceBundledFareId;
	}

	public void setSourceBundledFareId(Integer sourceBundledFareId) {
		this.sourceBundledFareId = sourceBundledFareId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((agents == null) ? 0 : agents.hashCode());
		result = prime * result + ((bookingClasses == null) ? 0 : bookingClasses.hashCode());
		result = prime * result + ((bundledFareName == null) ? 0 : bundledFareName.hashCode());
		result = prime * result + ((defaultBundledFee == null) ? 0 : defaultBundledFee.hashCode());
		result = prime * result + ((chargeCode == null) ? 0 : chargeCode.hashCode());
		result = prime * result + ((flightDateFrom == null) ? 0 : flightDateFrom.hashCode());
		result = prime * result + ((flightDateTo == null) ? 0 : flightDateTo.hashCode());
		result = prime * result + ((flightNumbers == null) ? 0 : flightNumbers.hashCode());
		result = prime * result + ((ondCodes == null) ? 0 : ondCodes.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BundledFare)) {
			return false;
		}

		BundledFare other = (BundledFare) obj;
		if (agents == null) {
			if (other.agents != null) {
				return false;
			}
		} else if (!agents.equals(other.agents)) {
			return false;
		}

		if (bookingClasses == null) {
			if (other.bookingClasses != null) {
				return false;
			}
		} else if (!bookingClasses.equals(other.bookingClasses)) {
			return false;
		}

		if (bundledFareName == null) {
			if (other.bundledFareName != null) {
				return false;
			}
		} else if (!bundledFareName.equals(other.bundledFareName)) {
			return false;
		}

		if (defaultBundledFee == null) {
			if (other.defaultBundledFee != null) {
				return false;
			}
		} else if (!defaultBundledFee.equals(other.defaultBundledFee)) {
			return false;
		}

		if (chargeCode == null) {
			if (other.chargeCode != null) {
				return false;
			}
		} else if (!chargeCode.equals(other.chargeCode)) {
			return false;
		}

		if (flightDateFrom == null) {
			if (other.flightDateFrom != null) {
				return false;
			}
		} else if (!flightDateFrom.equals(other.flightDateFrom)) {
			return false;
		}

		if (flightDateTo == null) {
			if (other.flightDateTo != null) {
				return false;
			}
		} else if (!flightDateTo.equals(other.flightDateTo)) {
			return false;
		}

		if (flightNumbers == null) {
			if (other.flightNumbers != null) {
				return false;
			}
		} else if (!flightNumbers.equals(other.flightNumbers)) {
			return false;
		}

		if (ondCodes == null) {
			if (other.ondCodes != null) {
				return false;
			}
		} else if (!ondCodes.equals(other.ondCodes)) {
			return false;
		}

		if (status == null) {
			if (other.status != null) {
				return false;
			}
		} else if (!status.equals(other.status)) {
			return false;
		}

		return true;
	}

	public boolean isServiceIncluded(String serviceName) {
		if (this.periodSelected) {
			Set<BundledFareFreeService> bundledFareFreeServices = this.getSelectedBundledFareServices();
			if (bundledFareFreeServices != null && !bundledFareFreeServices.isEmpty()) {
				for (BundledFareFreeService bundledFareFreeService : bundledFareFreeServices) {
					if (bundledFareFreeService.getServiceName().equalsIgnoreCase(serviceName)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public Set<String> getSelectedServiceNames() {
		Set<String> selectedServiceNames = new HashSet<String>();
		if (this.periodSelected) {
			Set<BundledFarePeriod> effectiveBundleFares = this.getBundledFarePeriods();
			if (effectiveBundleFares != null && !effectiveBundleFares.isEmpty()) {
				for (BundledFarePeriod effectiveBundleFare : effectiveBundleFares) {
					Set<BundledFareFreeService> bundledFareFreeServices = effectiveBundleFare.getBundledFareServices();
					for (BundledFareFreeService bundledFareFreeService : bundledFareFreeServices) {
						selectedServiceNames.add(bundledFareFreeService.getServiceName());
					}
					return selectedServiceNames;
				}
			}
		}
		return selectedServiceNames;
	}

	public Integer getSelectedBundlePeriodId() {
		if (this.periodSelected) {
			Set<BundledFarePeriod> effectiveBundleFares = this.getBundledFarePeriods();
			if (effectiveBundleFares != null && !effectiveBundleFares.isEmpty()) {
				for (BundledFarePeriod effectiveBundleFare : effectiveBundleFares) {
					return effectiveBundleFare.getBundleFarePeriodId();
				}
			}
		}
		return null;
	}

	public BundledFarePeriod getSelectedBundledFarePeriod() {
		if (this.periodSelected) {
			Set<BundledFarePeriod> effectiveBundleFares = this.getBundledFarePeriods();
			if (effectiveBundleFares != null && !effectiveBundleFares.isEmpty()) {
				for (BundledFarePeriod effectiveBundleFare : effectiveBundleFares) {
					return effectiveBundleFare;
				}
			}
		}
		return null;

	}

	public Set<BundledFareFreeService> getSelectedBundledFareServices() {
		if (this.periodSelected) {
			Set<BundledFarePeriod> effectiveBundleFares = this.getBundledFarePeriods();
			if (effectiveBundleFares != null && !effectiveBundleFares.isEmpty()) {
				for (BundledFarePeriod effectiveBundleFare : effectiveBundleFares) {
					return effectiveBundleFare.getBundledFareServices();
				}
			}
		}
		return null;
	}

	public BigDecimal getSelectedBundleFee() {
		if (this.periodSelected) {
			BundledFarePeriod selectedBundleFare = this.getSelectedBundledFarePeriod();
			if (selectedBundleFare != null) {
				return selectedBundleFare.getBundledFee();
			}
		}
		return null;
	}

	/**
	 * Default bundle fee will be over-ridden by the selected slabs bundle fee
	 * 
	 * @return effectiveBundleFee
	 */
	public BigDecimal getEffectiveBundleFee() {
		BigDecimal effectiveBundleFee = this.getSelectedBundleFee();
		if (effectiveBundleFee == null) {
			effectiveBundleFee = this.getDefaultBundledFee();
		}
		return effectiveBundleFee;
	}

}
