package com.isa.thinair.promotion.api.to.lms;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public class FlownFileRecordDTO implements Serializable {

	private static final long serialVersionUID = 4594053510193110674L;

	private String memberAccountId;
	private String title;
	private String lastName;
	private String firstName;
	private String paxType;
	private String dob;
	private String nationality;
	private String operatingAirline;
	private String operaringFlight;
	private String marketingAirline;
	private String marketingFlight;
	private String flightDate;
	private String flightTime;
	private String departureAirport;
	private String arrivalAirport;
	private String cabinClass;
	private String bookingClass;
	private String pnr;
	private String ticketIssuanceDate;
	private String ticketNumber;
	private String couponNumber;
	private String agentInitials;
	private String contactNumber;
	private String contactEmail;
	private String countryOfResidence;
	private String salesChannel;
	private String passportNumber;
	private String productList;
	private Integer errorRecordReference;

	public String getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public String getOperaringFlight() {
		return operaringFlight;
	}

	public void setOperaringFlight(String operaringFlight) {
		this.operaringFlight = operaringFlight;
	}

	public String getMarketingAirline() {
		return marketingAirline;
	}

	public void setMarketingAirline(String marketingAirline) {
		this.marketingAirline = marketingAirline;
	}

	public String getMarketingFlight() {
		return marketingFlight;
	}

	public void setMarketingFlight(String marketingFlight) {
		this.marketingFlight = marketingFlight;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public String getFlightTime() {
		return flightTime;
	}

	public void setFlightTime(String flightTime) {
		this.flightTime = flightTime;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTicketIssuanceDate() {
		return ticketIssuanceDate;
	}

	public void setTicketIssuanceDate(String ticketIssuanceDate) {
		this.ticketIssuanceDate = ticketIssuanceDate;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getAgentInitials() {
		return agentInitials;
	}

	public void setAgentInitials(String agentInitials) {
		this.agentInitials = agentInitials;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getCountryOfResidence() {
		return countryOfResidence;
	}

	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getProductList() {
		return productList;
	}

	public void setProductList(String productList) {
		this.productList = productList;
	}

	public Integer getErrorRecordReference() {
		return errorRecordReference;
	}

	public void setErrorRecordReference(Integer errorRecordReference) {
		this.errorRecordReference = errorRecordReference;
	}
}
