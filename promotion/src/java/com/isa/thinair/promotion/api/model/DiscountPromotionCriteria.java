package com.isa.thinair.promotion.api.model;

import java.util.Set;

/**
 * Class defining the Discount promotion criteria
 * 
 * @author thihara
 * 
 * @hibernate.subclass discriminator-value = "DISCOUNT"
 */
public class DiscountPromotionCriteria extends PromotionCriteria {

	private static final long serialVersionUID = 2345879634L;

	/**
	 * BINs (Bank identification numbers) that will qualify for this promotion.
	 */
	private Set<Integer> applicableBINs;

	/**
	 * Discount to be applied for this promotion.
	 */
	private DiscountDetails discount;

	/**
	 * @hibernate.set lazy="false" cascade="all" table="T_PROMO_DISCOUNT_BIN"
	 * @hibernate.collection-element column="APPLICABLE_BIN" type="integer"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicableBINs
	 */
	public Set<Integer> getApplicableBINs() {
		return applicableBINs;
	}

	public void setApplicableBINs(Set<Integer> applicableBINs) {
		this.applicableBINs = applicableBINs;
	}

	/**
	 * @hibernate.component class = "com.isa.thinair.promotion.api.model.DiscountDetails"
	 * 
	 * @see #discount
	 */
	public DiscountDetails getDiscount() {
		return discount;
	}

	public void setDiscount(DiscountDetails discount) {
		this.discount = discount;
	}

}
