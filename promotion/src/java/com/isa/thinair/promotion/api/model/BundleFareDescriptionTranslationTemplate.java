package com.isa.thinair.promotion.api.model;

import com.isa.thinair.commons.core.framework.Persistent;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @hibernate.class table = "T_BUNDLE_DESC_TEMP_TRANSLATION"
 */
public class BundleFareDescriptionTranslationTemplate extends Persistent {

	private static final long serialVersionUID = 1465467568762347L;

	private Integer translationTemplateId;
	private String freeServicesTemplate;
	private String paidServicesTemplate;
	private String languageCode;
	private BundleFareDescriptionTemplate bundleFareDescriptionTemplate;

	/**
	 * @hibernate.id column = "BUNDLE_DESC_TEMP_TRANS_ID"  generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BUNDLE_DESC_TEMP_TRANSLATION"
	 */
	public Integer getTranslationTemplateId() {
		return translationTemplateId;
	}

	public void setTranslationTemplateId(Integer translationTemplateId) {
		this.translationTemplateId = translationTemplateId;
	}

	/**
	 * @hibernate.property column = "FREE_SERVICES_TEMPLATE"
	 */
	public String getFreeServicesTemplate() {
		return freeServicesTemplate;
	}

	public void setFreeServicesTemplate(String freeServicesTemplate) {
		this.freeServicesTemplate = freeServicesTemplate;
	}

	/**
	 * @hibernate.property column = "PAID_SERVICES_TEMPLATE"
	 */
	public String getPaidServicesTemplate() {
		return paidServicesTemplate;
	}

	public void setPaidServicesTemplate(String paidServicesTemplate) {
		this.paidServicesTemplate = paidServicesTemplate;
	}

	/**
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return
	 * @hibernate.many-to-one column="BUNDLE_DESC_TEMPLATE_ID" class="com.isa.thinair.promotion.api.model.BundleFareDescriptionTemplate"
	 */
	public BundleFareDescriptionTemplate getBundleFareDescriptionTemplate() {
		return bundleFareDescriptionTemplate;
	}

	public void setBundleFareDescriptionTemplate(BundleFareDescriptionTemplate bundleFareDescriptionTemplate) {
		this.bundleFareDescriptionTemplate = bundleFareDescriptionTemplate;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (object == null || !(object instanceof BundleFareDescriptionTranslationTemplate)) {
			return false;
		}

		BundleFareDescriptionTranslationTemplate that = (BundleFareDescriptionTranslationTemplate) object;

		return new EqualsBuilder().append(translationTemplateId, that.translationTemplateId).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(translationTemplateId)
		.append(languageCode).toHashCode();
	}
}
