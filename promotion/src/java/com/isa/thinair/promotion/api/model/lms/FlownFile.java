package com.isa.thinair.promotion.api.model.lms;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

/**
 * 
 * @author rumesh
 * 
 */
public class FlownFile implements FileGeneratable {

	private List<FlownRecord> flownRecords;

	private List<FlownRecord> errorRecords;

	public List<FlownRecord> getFlownRecords() {
		return flownRecords;
	}

	public void addFlownRecords(FlownRecord flownRecord) {
		if (this.flownRecords == null) {
			this.flownRecords = new ArrayList<FlownRecord>();
		}

		this.flownRecords.add(flownRecord);
	}

	public List<FlownRecord> getFlownErrorRecords() {
		return errorRecords;
	}

	public void addFlownErrorRecords(FlownRecord flownRecord) {
		if (this.errorRecords == null) {
			this.errorRecords = new ArrayList<FlownRecord>();
		}

		this.errorRecords.add(flownRecord);
	}

	@Override
	public String getFormattedData() {
		StringBuilder sb = new StringBuilder("");
		List<FlownRecord> processingRecords = flownRecords;

		if (processingRecords != null && !processingRecords.isEmpty()) {
			for (FlownRecord flownRecord : processingRecords) {
				if (flownRecord.isValidFormat()) {
					List<BasicDataType> flownRecordData = flownRecord.getElementList();
					int elemCounter = 1;
					for (BasicDataType basicDataType : flownRecordData) {
						sb.append(BeanUtils.nullHandler(basicDataType.getData()));
						if (elemCounter == flownRecordData.size()) {
							sb.append("\n");
						} else {
							sb.append(FlownRecord.SEPARATOR);
						}
						elemCounter++;
					}
				} else {
					addFlownErrorRecords(flownRecord);
				}

			}
		}
		return sb.toString();
	}

	@Override
	public String getFileName(Date processingDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		return AppSysParamsUtil.getDefaultCarrierCode() + "_" + externalConfig.getFlownFilePrefix() + sdf.format(processingDate)
				+ externalConfig.getFileExtension();
	}

	public boolean isErrorRecordExists(Integer errorRecordReference) {
		if (errorRecords != null && !errorRecords.isEmpty()) {
			for (FlownRecord errorRecord : errorRecords) {
				if (errorRecord != null && errorRecord.getErrorRecordReference() != null
						&& errorRecord.getErrorRecordReference().equals(errorRecordReference)) {
					return true;
				}
			}
		}

		return false;
	}

	public enum PRODUCT_CODE {
		SEAT, INSURANCE, MEAL, AIRPORT_SERVICE, BAGGAGE, BUNDLE_FARE, FLIGHT, FLEXI
	}
}
