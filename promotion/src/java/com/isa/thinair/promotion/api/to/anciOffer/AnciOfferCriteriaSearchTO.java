package com.isa.thinair.promotion.api.to.anciOffer;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.promotion.api.model.AnciOfferType;

/**
 * Class holding the search criteria data for AnciOfferCriteria queries.
 * 
 * @author thihara
 * 
 */
public class AnciOfferCriteriaSearchTO implements Serializable {

	private static final long serialVersionUID = -7665136793599061483L;

	private String fareBasisCode;
	private String bookingClass;
	private Integer flexiID;
	private String lcc;
	private String OND;
	private Set<String> searchONDSet;
	private Date from;
	private Date to;

	private Date departureDate;
	private Date inverseSegmentDepartureDate;

	private int adults;
	private int children;
	private int infants;

	private boolean isJourneyReturn;

	private boolean isSegmentReturn;

	private String active;

	private AnciOfferType anciOfferType;

	private String sortColumn;

	private String sortOrder;

	/**
	 * @return the fareBasisCode
	 */
	public String getFareBasisCode() {
		return fareBasisCode;
	}

	/**
	 * @param fareBasisCode
	 *            the fareBasisCode to set
	 */
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	/**
	 * @return the lcc
	 */
	public String getLcc() {
		return lcc;
	}

	/**
	 * @param lcc
	 *            the lcc to set
	 */
	public void setLcc(String lcc) {
		this.lcc = lcc;
	}

	/**
	 * @return the oND
	 */
	public String getOND() {
		return OND;
	}

	/**
	 * @param oND
	 *            the oND to set
	 */
	public void setOND(String oND) {
		OND = oND;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from
	 *            the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to
	 *            the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	public int getAdults() {
		return adults;
	}

	public void setAdults(int adults) {
		this.adults = adults;
	}

	public int getChildren() {
		return children;
	}

	public void setChildren(int children) {
		this.children = children;
	}

	public int getInfants() {
		return infants;
	}

	public void setInfants(int infants) {
		this.infants = infants;
	}

	public boolean isJourneyReturn() {
		return isJourneyReturn;
	}

	public void setJourneyReturn(boolean isJourneyReturn) {
		this.isJourneyReturn = isJourneyReturn;
	}

	public AnciOfferType getAnciOfferType() {
		return anciOfferType;
	}

	public void setAnciOfferType(AnciOfferType anciOfferType) {
		this.anciOfferType = anciOfferType;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Integer getFlexiID() {
		return flexiID;
	}

	public void setFlexiID(Integer flexiID) {
		this.flexiID = flexiID;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = "".equals(active) || "null".equals(active) ? null : active;
	}

	public void setAnciOfferType(String anciOfferType) {
		this.anciOfferType = anciOfferType == null || anciOfferType.equals("") ? null : AnciOfferType.valueOf(anciOfferType);
	}

	/**
	 * @return the sortColumn
	 */
	public String getSortColumn() {
		return sortColumn;
	}

	/**
	 * @param sortColumn
	 *            the sortColumn to set
	 */
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn == null || sortColumn.equals("") ? "id" : sortColumn;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder == null || sortOrder.equals("") ? "asc" : sortOrder;
	}

	/**
	 * @return the isSegmentReturn
	 */
	public boolean isSegmentReturn() {
		return isSegmentReturn;
	}

	/**
	 * @param isSegmentReturn
	 *            the isSegmentReturn to set
	 */
	public void setSegmentReturn(boolean isSegmentReturn) {
		this.isSegmentReturn = isSegmentReturn;
	}

	/**
	 * @return the wildCardedONDSet
	 */
	public Set<String> getSearchONDSet() {
		return searchONDSet;
	}

	/**
	 * @param wildCardedONDSet
	 *            the wildCardedONDSet to set
	 */
	public void setSearchONDSet(Set<String> wildCardedONDSet) {
		this.searchONDSet = wildCardedONDSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnciOfferCriteriaSearchTO [OND=" + OND + ", searchONDSet=" + searchONDSet + ", anciOfferType=" + anciOfferType
				+ "]";
	}

	/**
	 * @return the inverseSegmentDepartureDate
	 */
	public Date getInverseSegmentDepartureDate() {
		return inverseSegmentDepartureDate;
	}

	/**
	 * @param inverseSegmentDepartureDate
	 *            the inverseSegmentDepartureDate to set
	 */
	public void setInverseSegmentDepartureDate(Date inverseSegmentDepartureDate) {
		this.inverseSegmentDepartureDate = inverseSegmentDepartureDate;
	}

	/**
	 * @return the bookingClass
	 */
	public String getBookingClass() {
		return bookingClass;
	}

	/**
	 * @param bookingClass
	 *            the bookingClass to set
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}
}
