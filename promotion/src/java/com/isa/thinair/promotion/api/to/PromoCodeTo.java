package com.isa.thinair.promotion.api.to;

import java.io.Serializable;

public class PromoCodeTo implements Serializable {

	private static final long serialVersionUID = -2351273913759181225L;
	
	private String promoCode;
	
	private Boolean fullyUtilized;
	
	private Boolean enabled;

	/**
	 * @return the promoCode
	 */
	public String getPromoCode() {
		return promoCode;
	}

	/**
	 * @param promoCode the promoCode to set
	 */
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	/**
	 * @return the fullyUtilized
	 */
	public Boolean getFullyUtilized() {
		return fullyUtilized;
	}

	/**
	 * @param fullyUtilized the fullyUtilized to set
	 */
	public void setFullyUtilized(Boolean fullyUtilized) {
		this.fullyUtilized = fullyUtilized;
	}

	/**
	 * @return the enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}	

}
