package com.isa.thinair.promotion.api.model;

import java.util.Set;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Class holds free service details for bundled fare criteria
 * 
 * @author rumesh
 * 
 * @hibernate.class table = "T_BUNDLED_FARE_FREE_SERVICE"
 */
public class BundledFareFreeService extends Persistent {

	private static final long serialVersionUID = 8788966780160378764L;

	private Integer freeServiceId;

	private String serviceName;

	private Integer templateId;

	private Set<Integer> includedFreeServices;

	private Boolean allowMultipleSelect = Boolean.FALSE;

	/**
	 * @hibernate.id column = "BUNDLED_FARE_FREE_SERVICE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BUNDLED_FARE_FREE_SERVICE"
	 * 
	 * @return the freeServiceId
	 */
	public Integer getFreeServiceId() {
		return freeServiceId;
	}

	/**
	 * @param freeServiceId
	 *            the freeServiceId to set
	 */
	public void setFreeServiceId(Integer freeServiceId) {
		this.freeServiceId = freeServiceId;
	}

	/**
	 * @return the serviceName
	 * @hibernate.property column = "SERVICE_NAME"
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName
	 *            the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the templateId
	 * @hibernate.property column = "TEMPLATE_ID"
	 */
	public Integer getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return
	 * @hibernate.set lazy="false" table="T_BUNDLED_FARE_FREE_APS"
	 * @hibernate.collection-element column="SSR_ID" type="integer"
	 * @hibernate.collection-key column="BUNDLED_FARE_FREE_SERVICE_ID"
	 * 
	 */
	public Set<Integer> getIncludedFreeServices() {
		return includedFreeServices;
	}

	/**
	 * 
	 * @param includedFreeServices
	 */
	public void setIncludedFreeServices(Set<Integer> includedFreeServices) {
		this.includedFreeServices = includedFreeServices;
	}

	/**
	 * @return the allowMultipleSelect
	 * @hibernate.property column = "ALLOW_MULTIPLE_SELECTION" type = "yes_no"
	 */
	public Boolean getAllowMultipleSelect() {
		return allowMultipleSelect;
	}

	/**
	 * @param allowMultipleSelect
	 *            the allowMultipleSelect to set
	 */
	public void setAllowMultipleSelect(Boolean allowMultipleSelect) {
		this.allowMultipleSelect = allowMultipleSelect;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((serviceName == null) ? 0 : serviceName.hashCode());
		result = prime * result + ((includedFreeServices == null) ? 0 : includedFreeServices.hashCode());
		result = prime * result + ((templateId == null) ? 0 : templateId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		// if (!super.equals(obj))
		// return false;
		if (getClass() != obj.getClass())
			return false;
		BundledFareFreeService other = (BundledFareFreeService) obj;

		if (serviceName == null) {
			if (other.serviceName != null) {
				return false;
			}
		} else if (!serviceName.equals(other.serviceName)) {
			return false;
		}

		String aps = EXTERNAL_CHARGES.AIRPORT_SERVICE.toString();
		if (aps.equals(this.serviceName) && aps.equals(other.getServiceName())) {
			if (includedFreeServices == null) {
				if (other.getIncludedFreeServices() != null) {
					return false;
				}
			} else if (includedFreeServices != null && other.getIncludedFreeServices() == null) {
				return false;
			} else if (!includedFreeServices.containsAll(other.getIncludedFreeServices())) {
				return false;
			}
		} else {
			if (templateId == null) {
				if (other.templateId != null) {
					return false;
				}
			} else if (!templateId.equals(other.templateId)) {
				return false;
			}
		}

		if (allowMultipleSelect == null) {
			if (other.allowMultipleSelect != null) {
				return false;
			}
		} else if (!allowMultipleSelect.equals(other.allowMultipleSelect)) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "BundledFareFreeService [freeServiceId=" + freeServiceId + ", serviceName=" + serviceName + ", templateId="
				+ templateId + ", includedFreeServices="+ includedFreeServices +"]";
	}
}
