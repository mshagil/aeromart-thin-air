package com.isa.thinair.promotion.api.model;

import java.io.Serializable;

/**
 * @author chethiya
 *
 * @hibernate.class table = "T_VOUCHER_REDEEM"
 */

public class VoucherRedemption implements Serializable{

	private static final long serialVersionUID = -5844357164642655902L;
	
	/** Holds the voucher redemption ID */
	private Integer voucherRedeemID;
	
	/** Holds the ID of the redeemed voucher at redemption */
	private String voucherID;
	
	/** Holds the corresponding pax transaction ID of voucher redemption for own transactions */
	private Integer transactionID;

	/** Holds the corresponding pax transaction ID of voucher redemption for external carrier transactions */
	private Integer extCarrierPaxTnxID;

	/**
	 * @return the voucher redemption ID
	 * 
	 * @hibernate.id column = "VOUCHER_REDEEM_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_VOUCHER_REDEEM"
	 * 
	 */
	public Integer getVoucherRedeemID() {
		return voucherRedeemID;
	}

	/**
	 * @param voucherRedeemID
	 *            the voucherRedeemID to set voucher redemption ID
	 */
	public void setVoucherRedeemID(Integer voucherRedeemID) {
		this.voucherRedeemID = voucherRedeemID;
	}

	/**
	 * @return the ID of the redeemed voucher at redemption
	 * 
	 * @hibernate.property column = "VOUCHER_ID"
	 * 
	 */
	public String getVoucherID() {
		return voucherID;
	}

	/**
	 * @param voucherID
	 *            the voucherID to set ID of the redeemed voucher at redemption
	 */
	public void setVoucherID(String voucherID) {
		this.voucherID = voucherID;
	}

	/**
	 * @return the corresponding pax transaction ID of voucher redemption
	 * 
	 * @hibernate.property column = "TXN_ID"
	 * 
	 */
	public Integer getTransactionID() {
		return transactionID;
	}

	/**
	 * @param transactionID
	 *            the transactionID to set corresponding pax transaction ID of voucher redemption
	 */
	public void setTransactionID(Integer transactionID) {
		this.transactionID = transactionID;
	}

	/**
	 * @return the extCarrierPaxTnxID
	 * 
	 * @hibernate.property column = "PAX_EXT_CARRIER_TXN_ID"
	 * 
	 */
	public Integer getExtCarrierPaxTnxID() {
		return extCarrierPaxTnxID;
	}

	/**
	 * @param extCarrierPaxTnxID
	 *            the extCarrierPaxTnxID to set
	 */
	public void setExtCarrierPaxTnxID(Integer extCarrierPaxTnxID) {
		this.extCarrierPaxTnxID = extCarrierPaxTnxID;
	}
		
}
