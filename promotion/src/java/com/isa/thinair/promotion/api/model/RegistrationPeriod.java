package com.isa.thinair.promotion.api.model;

/**
 * @author suventhan
 * 
 * @hibernate.class table = "T_PROMO_CRITERIA_RES_PERIOD"
 * 
 */
public class RegistrationPeriod extends DatePeriod {

	private static final long serialVersionUID = 7302853997816592654L;
	
	private Long registrationPeriodId;

	/**
	 * @hibernate.id column = "PROMO_CRITERIA_RES_PERIOD_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PROMO_CRITERIA_RES_PERIOD"
	 * @return registrationPeriodId
	 */
	public Long getRegistrationPeriodId() {
		return registrationPeriodId;
	}

	/**
	 * @param registrationPeriodId
	 */
	public void setRegistrationPeriodId(Long registrationPeriodId) {
		this.registrationPeriodId = registrationPeriodId;
	}
}
