package com.isa.thinair.msgbroker.api.dto;

public class SSROSAGDTO extends SSRDTO {

    /**
     * 
     */
    private static final long serialVersionUID = -7412147173689650386L;
    // SSR OTHS YY OSAG1245365D
    private String osagCode;

    /**
     * @return the osagCode
     */
    public String getOsagCode() {
        return osagCode;
    }

    /**
     * @param osagCode
     *            the osagCode to set
     */
    public void setOsagCode(String osagCode) {
        this.osagCode = osagCode;
    }

}
