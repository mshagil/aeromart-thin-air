/**
 * 
 */
package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

/**
 * @author sanjeewaf
 *
 */
public class ComposedResponseDTO implements Serializable {
    
    private String originatorAddress;
    private String responderAddress;
    private RecordLocatorDTO originatorRecordLocator;
    private RecordLocatorDTO responderRecordLocator;
    

}
