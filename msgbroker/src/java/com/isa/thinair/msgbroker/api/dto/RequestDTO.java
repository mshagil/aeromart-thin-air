/**
 * 
 */
package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

/**
 * @author sanjeewaf
 */
public class RequestDTO implements Serializable {

	private static final long serialVersionUID = 641994738006427634L;

	private String messageIdentifier;

	private String originatorAddress;

	private String responderAddress;
	
	private String responseMessageIdentifier;

	/**
	 * @return the messageIdentifier
	 */
	public String getMessageIdentifier() {
		return messageIdentifier;
	}

	/**
	 * @param messageIdentifier
	 *            the messageIdentifier to set
	 */
	public void setMessageIdentifier(String messageIdentifier) {
		this.messageIdentifier = messageIdentifier;
	}

	/**
	 * @return the originatorAddress
	 */
	public String getOriginatorAddress() {
		return originatorAddress;
	}

	/**
	 * @param originatorAddress
	 *            the originatorAddress to set
	 */
	public void setOriginatorAddress(String originatorAddress) {
		this.originatorAddress = originatorAddress;
	}

	/**
	 * @return the responderAddress
	 */
	public String getResponderAddress() {
		return responderAddress;
	}

	/**
	 * @param responderAddress
	 *            the responderAddress to set
	 */
	public void setResponderAddress(String responderAddress) {
		this.responderAddress = responderAddress;
	}

	public String getResponseMessageIdentifier() {
		return responseMessageIdentifier;
	}

	public void setResponseMessageIdentifier(String responseMessageIdentifier) {
		this.responseMessageIdentifier = responseMessageIdentifier;
	}

}
