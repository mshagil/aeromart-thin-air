package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.api.constants.CommonsConstants.TimeMode;

public class ASMessegeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum Action {
		NEW, CNL, FLT, EQT, RPL, TIM
	};

	private String referenceNumber;

	private Action action;

	private String flightIdentifier;
	private String newFlightIdentifier;

	private String serviceType;
	private String aircraftType;
	private String passengerResBookingDesignator;
	
    private String senderAddress = null;
    private String recipientAddress = null;
    private String timestamp = null;

	private List<SSIFlightLegDTO> legs;
	
	private List<String> externalFlightNos;
	
	private boolean codeShareCarrier;
	
	private String segmentCode;

	private String externalEmailAddres;
	
	private String externalSitaAddress;
	
	private String timeMode = TimeMode.ZULU_TIME_MODE;

	private String supplementaryInfo;

	/** ------------- Action Information --------------- */
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	/** ------------------ Equipment Information ------- */
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getPassengerResBookingDesignator() {
		return passengerResBookingDesignator;
	}

	public void setPassengerResBookingDesignator(String passengerResBookingDesignator) {
		this.passengerResBookingDesignator = passengerResBookingDesignator;
	}

	/** ------------------ Legs Information ------- */
	public List<SSIFlightLegDTO> getLegs() {
		return legs;
	}

	public void setLegs(List<SSIFlightLegDTO> legs) {
		this.legs = legs;
	}

	public void addLeg(SSIFlightLegDTO ssiFlightLeg) {
		if (this.legs == null)
			this.legs = new ArrayList<SSIFlightLegDTO>();
		this.legs.add(ssiFlightLeg);
	}

	/** ------------------ Reference Information ------- */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/** ------------------ Flight Information ------- */
	public String getFlightIdentifier() {
		return flightIdentifier;
	}

	public void setFlightIdentifier(String flightIdentifier) {
		this.flightIdentifier = flightIdentifier;
	}

	public String getNewFlightIdentifier() {
		return newFlightIdentifier;
	}

	public void setNewFlightIdentifier(String newFlightIdentifier) {
		this.newFlightIdentifier = newFlightIdentifier;
	}

	/**
	 * @return the senderAddress
	 */
	public String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * @param senderAddress the senderAddress to set
	 */
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	/**
	 * @return the recipientAddress
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}

	/**
	 * @param recipientAddress the recipientAddress to set
	 */
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	
	/** ------------------ Segment Information ------- */
	public List<String> getExternalFlightNos() {
		return externalFlightNos;
	}

	public void setExternalFlightNos(List<String> externalFlightNos) {
		this.externalFlightNos = externalFlightNos;
	}

	public boolean isCodeShareCarrier() {
		return codeShareCarrier;
	}

	public void setCodeShareCarrier(boolean codeShareCarrier) {
		this.codeShareCarrier = codeShareCarrier;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}

	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}

	public String getExternalEmailAddres() {
		return externalEmailAddres;
	}

	public void setExternalEmailAddres(String externalEmailAddres) {
		this.externalEmailAddres = externalEmailAddres;
	}

	public String getExternalSitaAddress() {
		return externalSitaAddress;
	}

	public void setExternalSitaAddress(String externalSitaAddress) {
		this.externalSitaAddress = externalSitaAddress;
	}
}
