package com.isa.thinair.msgbroker.api.dto;

import java.util.Date;

public class FlightScheduleLegDTO extends FlightLegDTO {
	Date scheduledDateTimeOfDeparture;
	// int dateVariationForSTD; //Conditional
	String scheduledDateTimeOfArrival;
	// int dateVariationForSTA; //Conditional
	String mealServiceNote;

	public String getMealServiceNote() {
		return mealServiceNote;
	}

	public void setMealServiceNote(String mealServiceNote) {
		this.mealServiceNote = mealServiceNote;
	}

	public String getScheduledDateTimeOfArrival() {
		return scheduledDateTimeOfArrival;
	}

	public void setScheduledDateTimeOfArrival(String scheduledDateTimeOfArrival) {
		this.scheduledDateTimeOfArrival = scheduledDateTimeOfArrival;
	}

	public Date getScheduledDateTimeOfDeparture() {
		return scheduledDateTimeOfDeparture;
	}

	public void setScheduledDateTimeOfDeparture(Date scheduledDateTimeOfDeparture) {
		this.scheduledDateTimeOfDeparture = scheduledDateTimeOfDeparture;
	}

}
