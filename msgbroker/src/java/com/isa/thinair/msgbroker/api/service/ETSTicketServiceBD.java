package com.isa.thinair.msgbroker.api.service;

import com.isa.thinair.commons.api.dto.ets.IssueExchangeTKTREQDTO;
import com.isa.thinair.commons.api.dto.ets.IssueExchangeTKTRESDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Manoj Dhanushka
 */
public interface ETSTicketServiceBD {
	
	public static final String SERVICE_NAME = "TicketService";
	
	public IssueExchangeTKTRESDTO issue(IssueExchangeTKTREQDTO issueExchangeTKTREQDTO) throws ModuleException;

}
