package com.isa.thinair.msgbroker.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "MB_T_REQUEST_TYPE"
 */

public class RequestType extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String requestTypeCode;
	private String requestTypeDesc;
	private String messageDirection;
	private Double timeToLive;
	private boolean recordStatus;
	private String serviceType;

	/**
	 * @return Returns the requestTypeCode.
	 * 
	 * @hibernate.id column = "REQUEST_TYPE_CODE" generator-class = "assigned"
	 */
	public String getRequestTypeCode() {
		return requestTypeCode;
	}

	public void setRequestTypeCode(String requestTypeCode) {
		this.requestTypeCode = requestTypeCode;
	}

	/**
	 * returns the requestTypeDesc
	 * 
	 * @return Returns the requestTypeDesc.
	 * 
	 * @hibernate.property column = "REQUEST_TYPE_DESC"
	 */
	public String getRequestTypeDesc() {
		return requestTypeDesc;
	}

	public void setRequestTypeDesc(String requestTypeDesc) {
		this.requestTypeDesc = requestTypeDesc;
	}

	/**
	 * returns the messageDirection
	 * 
	 * @return Returns the messageDirection.
	 * 
	 * @hibernate.property column = "MESSAGE_DIRECTION"
	 */
	public String getMessageDirection() {
		return messageDirection;
	}

	public void setMessageDirection(String messageDirection) {
		this.messageDirection = messageDirection;
	}

	/**
	 * returns the timeToLive in minutes.
	 * 
	 * @return Returns the timeToLive in minutes.
	 * 
	 * @hibernate.property column = "TIME_TO_LIVE"
	 */
	public Double getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(Double timeToLive) {
		this.timeToLive = timeToLive;
	}

	/**
	 * 
	 * @return the Status
	 * 
	 * @hibernate.property column = "RECORD_STATUS" type="yes_no"
	 */
	public boolean getRecordStatus() {
		return recordStatus;
	}

	/**
	 * 
	 * @param recordStatus
	 *            to set the Airport Status
	 */
	public void setRecordStatus(boolean recordStatus) {
		this.recordStatus = recordStatus;
	}

	/**
	 * returns the service type
	 * 
	 * @return Returns the service type.
	 * 
	 * @hibernate.property column = "SERVICE_TYPE"
	 */
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

}