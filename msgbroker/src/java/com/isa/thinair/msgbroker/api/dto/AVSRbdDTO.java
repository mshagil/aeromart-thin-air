package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

import com.isa.thinair.msgbroker.api.util.AVSConstants.AVSRbdEventCode;

public class AVSRbdDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private AVSRbdEventCode avsRbdEventCode;
	private String bookingCode;
	private Integer numericAvailability;
	private Integer bcSeatsAvailable;
	
	/**
	 * @return the avsRbdEventCode
	 */
	public AVSRbdEventCode getAvsRbdEventCode() {
		return avsRbdEventCode;
	}
	/**
	 * @param avsRbdEventCode the avsRbdEventCode to set
	 */
	public void setAvsRbdEventCode(AVSRbdEventCode avsRbdEventCode) {
		this.avsRbdEventCode = avsRbdEventCode;
	}
	/**
	 * @return the bookingCode
	 */
	public String getBookingCode() {
		return bookingCode;
	}
	/**
	 * @param bookingCode the bookingCode to set
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}
	/**
	 * @return the numericAvailability
	 */
	public Integer getNumericAvailability() {
		return numericAvailability;
	}
	/**
	 * @param numericAvailability the numericAvailability to set
	 */
	public void setNumericAvailability(Integer numericAvailability) {
		this.numericAvailability = numericAvailability;
	}
	
	/**
	 * @return the bcSeatsAvailable
	 */
	public Integer getBcSeatsAvailable() {
		return bcSeatsAvailable;
	}
	/**
	 * @param bcSeatsAvailable the bcSeatsAvailable to set
	 */
	public void setBcSeatsAvailable(Integer bcSeatsAvailable) {
		this.bcSeatsAvailable = bcSeatsAvailable;
	}	
}