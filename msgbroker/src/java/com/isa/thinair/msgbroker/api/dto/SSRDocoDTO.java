/**
 * 
 */
package com.isa.thinair.msgbroker.api.dto;

import java.util.Date;
import java.util.List;

/**
 * @author suneth
 *
 */
public class SSRDocoDTO extends SSRDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int noOfPax;
	
	private String placeOfBirth;
	
	private String travelDocType;
	
	private String visaDocNumber;
	
	private String visaDocPlaceOfIssue;
	
	private Date visaDocIssueDate;
	
	private String visaApplicableCountry;
	
	private boolean infant;	
	
	private List<NameDTO> pnrNameDTOs;

	
	public int getNoOfPax() {
		return noOfPax;
	}

	public void setNoOfPax(int noOfPax) {
		this.noOfPax = noOfPax;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getTravelDocType() {
		return travelDocType;
	}

	public void setTravelDocType(String travelDocType) {
		this.travelDocType = travelDocType;
	}

	public String getVisaDocNumber() {
		return visaDocNumber;
	}

	public void setVisaDocNumber(String visaDocNumber) {
		this.visaDocNumber = visaDocNumber;
	}

	public String getVisaDocPlaceOfIssue() {
		return visaDocPlaceOfIssue;
	}

	public void setVisaDocPlaceOfIssue(String visaDocPlaceOfIssue) {
		this.visaDocPlaceOfIssue = visaDocPlaceOfIssue;
	}

	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}

	public List<NameDTO> getPnrNameDTOs() {
		return pnrNameDTOs;
	}

	public void setPnrNameDTOs(List<NameDTO> pnrNameDTOs) {
		this.pnrNameDTOs = pnrNameDTOs;
	}

	public boolean isInfant() {
		return infant;
	}

	public void setInfant(boolean infant) {
		this.infant = infant;
	}

	public Date getVisaDocIssueDate() {
		return visaDocIssueDate;
	}

	public void setVisaDocIssueDate(Date visaDocIssueDate) {
		this.visaDocIssueDate = visaDocIssueDate;
	}

}
