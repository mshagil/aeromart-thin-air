package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.msgbroker.api.util.ReservationMessageProcessingConstants.ScheduleMessage.ActionIdentifier;
import com.isa.thinair.msgbroker.api.util.ReservationMessageProcessingConstants.ScheduleMessage.TimeMode;
import com.isa.thinair.msgbroker.api.util.ReservationMessageProcessingConstants.ScheduleMessage.WeekDay;

public class StandardScheduleMessageDTO implements Serializable {

//	private static final long serialVersionUID = 1152261372021229733L;
	
	private TimeMode timeMode;
	private ActionIdentifier actionIdentifier;
	private String creatorReference;
	private String flightDesignator;
	private Date fromDateOfExistingPeriod; //Used only for 'REV' action identifier to specify effective period's from date.
	private Date toDateOfExistingPeriod; //Used only for 'REV' action identifier to specify effective period's to date.
	private Collection<WeekDay> existingDaysOfOperation; //Used only for 'REV'
	private Date fromDateOfPeriod; 
	private Date toDateOfPeriod; 
	private Collection<WeekDay> daysOfOperation;
	private String newFlightDesignator; //Used only for 'FLT' action identifier
	private String serviceType;
	private String aircraftType;
	private Collection RBDs;
	private List<FlightScheduleLegDTO> legs;
	private List<FlightScheduleSegmentDTO> segments; 
	
//	private boolean localTime; 
//	private Collection jointOperationAirlineCodes;
//	private String codeSharingCommercialDuplicateAirlineCode;
//	private String codeSharingSharedAirlineCode;	 
//	private Date fromDate;
//	private Date toDate;
		

	
	/**
	 * Returns sub message action identifier
	 * Example: NEW
	 * Format: aaa
	 * @return
	 */
	public ActionIdentifier getActionIdentifier() {		
		return actionIdentifier;
	}

	/**
	 * Sets sub message action identifier
	 * Format: aaa
	 * Example: NEW
	 * @param actionIdentifier
	 */
	public void setActionIdentifier(ActionIdentifier actionIdentifier) {
		this.actionIdentifier = actionIdentifier;
	}
	
	/**
	 * Returns new flight designator (new flight number with airline code and operational suffix)
	 * Used only with 'FLT' action identifier
	 * @return
	 */
	public String getNewFlightDesignator() {
		return newFlightDesignator;
	}

	/**
	 * Sets new flight designator (flight number with airline code and operational suffix)
	 * Used only with 'FLT' action identifier
	 * Format: XX(a)nnn(n)(a)
	 * Example: LX544
	 * @param flightDesignator
	 */
	public void setNewFlightDesignator(String newFlightDesignator) {
		this.newFlightDesignator = newFlightDesignator;
	}
	
	/**
	 * Returns service type 
	 * Format: a
	 * Example: G
	 * @return
	 */
	public String getServiceType() {
		return serviceType;
	}

	/**
	 * Sets service type
	 * Format: a
	 * Example: G
	 * @param serviceType
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * Returns aircraft type (aircraft model) 
	 * Format: xxx
	 * Example: M80
	 * @return
	 */
	public String getAircraftType() {
		return aircraftType;
	}

	/**
	 * Sets aircraft type (aircraft model)
	 * Format: xxx
	 * Example: M80
	 * @param aircraftType
	 */
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	/**
	 * Returns creator reference.  This is used for keeping reference to the originator's records  
	 * Format: x(x[-34]) 
	 * Example: REF 123/449
	 * @return
	 */
	public String getCreatorReference() {
		return creatorReference;
	}

	
	/**
	 * Sets creator reference.  This is used for keeping reference to the originator's records  
	 * Format: x(x[-34]) 
	 * Example: REF 123/449
	 * @param creatorReference
	 */
	public void setCreatorReference(String creatorReference) {
		this.creatorReference = creatorReference;
	}

	
	/**
	 * Returns flight operated days.  
	 * @return
	 */
	public Collection<WeekDay> getDaysOfOperation() {
		return daysOfOperation;
	}

	/**
	 * Sets flight operated days.
	 * @param daysOfOperation
	 */
	public void setDaysOfOperation(Collection<WeekDay> daysOfOperation) {
		this.daysOfOperation = daysOfOperation;
	}

	/**
	 * Used only with 'REV' (Revision of Period of Operation and/or Day(s)of Operation) Action Identifier  
	 * @return
	 */
	public Collection<WeekDay> getExistingDaysOfOperation() {
		return existingDaysOfOperation;
	}

	public void setExistingDaysOfOperation(
			Collection<WeekDay> existingDaysOfOperation) {
		this.existingDaysOfOperation = existingDaysOfOperation;
	}

	/**
	 * Returns flight designator (flight number with airline code and operational suffix)
	 * @return
	 */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	/**
	 * Sets flight designator (flight number with airline code and operational suffix)
	 * Format: XX(a)nnn(n)(a)
	 * Example: LX544
	 * @param flightDesignator
	 */
	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	public Date getFromDateOfExistingPeriod() {
		return fromDateOfExistingPeriod;
	}

	public void setFromDateOfExistingPeriod(Date fromDateOfExistingPeriod) {
		this.fromDateOfExistingPeriod = fromDateOfExistingPeriod;
	}

	public Date getFromDateOfPeriod() {
		return fromDateOfPeriod;
	}

	public void setFromDateOfPeriod(Date fromDateOfPeriod) {
		this.fromDateOfPeriod = fromDateOfPeriod;
	}

	public Collection getRBDs() {
		return RBDs;
	}

	public void setRBDs(Collection ds) {
		RBDs = ds;
	}

	public TimeMode getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(TimeMode timeMode) {
		this.timeMode = timeMode;
	}

	public Date getToDateOfExistingPeriod() {
		return toDateOfExistingPeriod;
	}

	public void setToDateOfExistingPeriod(Date toDateOfExistingPeriod) {
		this.toDateOfExistingPeriod = toDateOfExistingPeriod;
	}

	public Date getToDateOfPeriod() {
		return toDateOfPeriod;
	}

	public void setToDateOfPeriod(Date toDateOfPeriod) {
		this.toDateOfPeriod = toDateOfPeriod;
	}
	
	public List<FlightScheduleLegDTO> getLegs() {
		return legs;
	}

	public void setLegs(List<FlightScheduleLegDTO> legs) {
		this.legs = legs;
	}
	
	public List<FlightScheduleSegmentDTO> getSegments() {
		return segments;
	}

	public void setSegments(List<FlightScheduleSegmentDTO> segments) {
		this.segments = segments;
	}

}
