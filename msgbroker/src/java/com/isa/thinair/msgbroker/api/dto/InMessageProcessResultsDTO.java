package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.gdsservices.api.dto.typea.MessageError;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;

public class InMessageProcessResultsDTO implements Serializable {
	private InMessage inMessage;
	private InMessageProcessingStatus inMessageProcessingStatus;
	private OutMessage outMessage;
	private boolean sendNotifications;
	private List<OutMessage> notifications;
	private OutMessageProcessingStatus outMessageProcessingStatus;
	private MessageError messageError;
	
	public InMessage getInMessage() {
		return inMessage;
	}
	public void setInMessage(InMessage inMessage) {
		this.inMessage = inMessage;
	}
	public InMessageProcessingStatus getInMessageProcessingStatus() {
		return inMessageProcessingStatus;
	}
	public void setInMessageProcessingStatus(
			InMessageProcessingStatus inMessageProcessingStatus) {
		this.inMessageProcessingStatus = inMessageProcessingStatus;
	}
	public OutMessage getOutMessage() {
		return outMessage;
	}
	public void setOutMessage(OutMessage outMessage) {
		this.outMessage = outMessage;
	}

	public boolean isSendNotifications() {
		return sendNotifications;
	}

	public void setSendNotifications(boolean sendNotifications) {
		this.sendNotifications = sendNotifications;
	}

	public List<OutMessage> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<OutMessage> notifications) {
		this.notifications = notifications;
	}

	public OutMessageProcessingStatus getOutMessageProcessingStatus() {
		return outMessageProcessingStatus;
	}
	public void setOutMessageProcessingStatus(
			OutMessageProcessingStatus outMessageProcessingStatus) {
		this.outMessageProcessingStatus = outMessageProcessingStatus;
	}
	public MessageError getMessageError() {
		return messageError;
	}
	public void setMessageError(MessageError messageError) {
		this.messageError = messageError;
	}
}
