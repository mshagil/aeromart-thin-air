package com.isa.thinair.msgbroker.api.service;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.dto.ASMessegeDTO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO;

/**
 * @author Ishan
 */
public interface SSMASMServiceBD {

	public static final String SERVICE_NAME = "SSMASMService";

	/**
	 * @param gdsCode
	 * @param airlineCode
	 * @param ssmDTO
	 * @throws ModuleException
	 */
	public void sendStandardScheduleMessage(String gdsCode, String airlineCode, SSIMessegeDTO ssmDTO) throws ModuleException;

	/**
	 * @param gdsCode
	 * @param airlineCode
	 * @param asmDTO
	 * @throws ModuleException
	 */
	public void sendAdHocScheduleMessage(String gdsCode, String airlineCode, ASMessegeDTO asmDTO) throws ModuleException;

	/**
	 * @throws ModuleException
	 */
	public void publishSSMASMMessages() throws ModuleException;
	
	public void sendStandardScheduleMessageForExternalSystem(String airlineCode, SSIMessegeDTO ssmDTO) throws ModuleException;
	
	public void sendAdHocScheduleMessageToExternalSystem(String airlineCode, ASMessegeDTO asmDTO) throws ModuleException; 

	public boolean isEnableAttachingDefaultAnciTemplate(String tty, String requestType) throws ModuleException; 
	
	public void sendStandardScheduleSubMessages(String gdsCode, String airlineCode, Map<Integer, SSIMessegeDTO> ssmMessegeDTOMap,
			Collection<String> bookingClassList) throws ModuleException;
	
	public void sendAdHocScheduleSubMessages(String gdsCode, String airlineCode, Map<Integer, ASMessegeDTO> asmMessegeDTOMap,
			Collection<String> bookingClassList) throws ModuleException;
}
