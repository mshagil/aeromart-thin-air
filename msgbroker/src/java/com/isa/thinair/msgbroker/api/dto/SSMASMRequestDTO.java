package com.isa.thinair.msgbroker.api.dto;

public class SSMASMRequestDTO extends RequestDTO {

	private static final long serialVersionUID = 1082096819633858785L;

	private ASMSSMMetaDataDTO asmssmMetaDataDTO = null;

	private boolean codeShareCarrier;

	private String operatingCarrier;

	private String marketingFlightPrefix;

	private String rawMessage;
	
	private boolean enableAutoScheduleSplit;

	public ASMSSMMetaDataDTO getAsmssmMetaDataDTO() {
		return asmssmMetaDataDTO;
	}

	public void setAsmssmMetaDataDTO(ASMSSMMetaDataDTO asmssmMetaDataDTO) {
		this.asmssmMetaDataDTO = asmssmMetaDataDTO;
	}

	public boolean isCodeShareCarrier() {
		return codeShareCarrier;
	}

	public void setCodeShareCarrier(boolean codeShareCarrier) {
		this.codeShareCarrier = codeShareCarrier;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getMarketingFlightPrefix() {
		return marketingFlightPrefix;
	}

	public void setMarketingFlightPrefix(String marketingFlightPrefix) {
		this.marketingFlightPrefix = marketingFlightPrefix;
	}

	public String getRawMessage() {
		return rawMessage;
	}

	public void setRawMessage(String rawMessage) {
		this.rawMessage = rawMessage;
	}

	public boolean isEnableAutoScheduleSplit() {
		return enableAutoScheduleSplit;
	}

	public void setEnableAutoScheduleSplit(boolean enableAutoScheduleSplit) {
		this.enableAutoScheduleSplit = enableAutoScheduleSplit;
	}

}
