/**
 * 
 */
package com.isa.thinair.msgbroker.api.dto;

import java.util.Date;

/**
 * @author sanjeewaf
 */
public class SSRChildDTO extends SSRDTO {

	/**
     * 
     */
	private static final long serialVersionUID = -3606009081076418991L;

	private Date dateOfBirth;

	private String firstName;

	private String lastName;

	private String title;

	/**
	 * @return the childAge
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param childAge
	 *            the childAge to set
	 */
	public void setDateOfBirth(Date childAge) {
		this.dateOfBirth = childAge;
	}

	/**
	 * @return the childFirstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param childFirstName
	 *            the childFirstName to set
	 */
	public void setFirstName(String childFirstName) {
		this.firstName = childFirstName;
	}

	/**
	 * @return the childLastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param childLastName
	 *            the childLastName to set
	 */
	public void setLastName(String childLastName) {
		this.lastName = childLastName;
	}

	/**
	 * @return the childTitle
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param childTitle
	 *            the childTitle to set
	 */
	public void setTitle(String childTitle) {
		this.title = childTitle;
	}

}
