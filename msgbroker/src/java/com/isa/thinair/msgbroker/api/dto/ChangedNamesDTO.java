package com.isa.thinair.msgbroker.api.dto;
/*
 * DTOPaxBabbage.java
 *
 * Created on August 9, 2006, 10:38 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
import java.io.Serializable;
import java.util.List;
/**
 *
 * @author Chan
 */
public class ChangedNamesDTO implements Serializable{
    
    
    /**
     * 
     */
    private static final long serialVersionUID = 3633170343722197477L;

    /** previos values of names**/
    private List<NameDTO> oldNameDTOs = null;
    
    /** Hold new changed names **/
    private List<NameDTO> newNameDTOs = null;

    /**
     * @return the newNameDTOs
     */
    public List<NameDTO> getNewNameDTOs() {
        return newNameDTOs;
    }

    /**
     * @param newNameDTOs the newNameDTOs to set
     */
    public void setNewNameDTOs(List<NameDTO> newNameDTOs) {
        this.newNameDTOs = newNameDTOs;
    }

    /**
     * @return the oldNameDTOs
     */
    public List<NameDTO> getOldNameDTOs() {
        return oldNameDTOs;
    }

    /**
     * @param oldNameDTOs the oldNameDTOs to set
     */
    public void setOldNameDTOs(List<NameDTO> oldNameDTOs) {
        this.oldNameDTOs = oldNameDTOs;
    }

        

}
