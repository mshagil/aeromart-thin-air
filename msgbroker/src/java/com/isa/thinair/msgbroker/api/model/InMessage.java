package com.isa.thinair.msgbroker.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "MB_T_IN_MESSAGE"
 */

public class InMessage extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer inMessageID;
	private String airLineCode;
	private String messageType;
	private String sentTTYAddress;
	private String recipientTTYAddress;
	private String recordLocator;
	private String inMessageText;
	private Date receivedDate;
	private String statusIndicator;
	private Date processedDate;
	private String lastErrorMessageCode;
	private boolean editableStatus;
	private String editBy;
	private Date editStartTime;
	private String sentEmailAddress;
	private String recipientEmailAddress;
	private String modeOfCommunication;
	private String lockStatus;
	private Date lastLockedTime;
	private String serviceClientCode;
	private String gdsMessageID;	
	private Integer retryCount;
	private String messageSequence;	

	/**
	 * returns the lastLockedTime
	 * 
	 * @return Returns the lastLockedTime.
	 * 
	 * @hibernate.property column = "LAST_LOCKED_TIME"
	 */
	public Date getLastLockedTime() {
		return lastLockedTime;
	}

	public void setLastLockedTime(Date lastLockedTime) {
		this.lastLockedTime = lastLockedTime;
	}

	/**
	 * returns the editStartTime
	 * 
	 * @return Returns the editStartTime.
	 * 
	 * @hibernate.property column = "EDIT_START_TIME"
	 */
	public Date getEditStartTime() {
		return editStartTime;
	}

	public void setEditStartTime(Date editStartTime) {
		this.editStartTime = editStartTime;
	}

	/**
	 * returns the editBy
	 * 
	 * @return Returns the editBy.
	 * 
	 * @hibernate.property column = "EDIT_BY"
	 */
	public String getEditBy() {
		return editBy;
	}

	public void setEditBy(String editBy) {
		this.editBy = editBy;
	}

	/**
	 * @return Returns the editableStatus.
	 * 
	 * @hibernate.property column = "EDITABLE_STATUS" type="yes_no"
	 */
	public boolean getEditableStatus() {
		return editableStatus;
	}

	/**
	 * @param recordStatus
	 *            The recordStatus to set.
	 */
	public void setEditableStatus(boolean editableStatus) {
		this.editableStatus = editableStatus;
	}

	/**
	 * returns the LastErrorMessageCode
	 * 
	 * @return Returns the LastErrorMessageCode.
	 * 
	 * @hibernate.property column = "LAST_ERROR_MESSAGE_CODE"
	 */
	public String getLastErrorMessageCode() {
		return lastErrorMessageCode;
	}

	public void setLastErrorMessageCode(String lastErrorMessageCode) {
		this.lastErrorMessageCode = lastErrorMessageCode;
	}

	/**
	 * returns the airLineCode
	 * 
	 * @return Returns the airLineCode.
	 * 
	 * @hibernate.property column = "AIRLINE_CODE"
	 */
	public String getAirLineCode() {
		return airLineCode;
	}

	public void setAirLineCode(String airLineCode) {
		this.airLineCode = airLineCode;
	}

	/**
	 * returns the inMessageID
	 * 
	 * @return Returns the inMessageID.
	 * 
	 * @hibernate.id column = "MESSAGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "MB_S_IN_MESSAGE"
	 */
	public Integer getInMessageID() {
		return inMessageID;
	}

	public void setInMessageID(Integer inMessageID) {
		this.inMessageID = inMessageID;
	}

	/**
	 * returns the inMessageText
	 * 
	 * @return Returns the inMessageText.
	 * 
	 * @hibernate.property column = "ROW_MESSAGE"
	 */
	public String getInMessageText() {
		return inMessageText;
	}

	public void setInMessageText(String inMessageText) {
		this.inMessageText = inMessageText;
	}

	/**
	 * returns the messageType
	 * 
	 * @return Returns the messageType.
	 * 
	 * @hibernate.property column = "MESSAGE_TYPE_CODE"
	 */
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * returns the processedDate
	 * 
	 * @return Returns the processedDate.
	 * 
	 * @hibernate.property column = "PROCESSED_DATE"
	 */
	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	/**
	 * returns the receivedDate
	 * 
	 * @return Returns the receivedDate.
	 * 
	 * @hibernate.property column = "RECEIVED_DATE"
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * returns the recordLocator
	 * 
	 * @return Returns the recordLocator.
	 * 
	 * @hibernate.property column = "RECORD_LOCATOR"
	 */
	public String getRecordLocator() {
		return recordLocator;
	}

	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}

	/**
	 * returns the sentTTYAddress
	 * 
	 * @return Returns the sentTTYAddress.
	 * 
	 * @hibernate.property column = "SENDER_TTY_ADDRESS"
	 */
	public String getSentTTYAddress() {
		return sentTTYAddress;
	}

	public void setSentTTYAddress(String sentTTYAddress) {
		this.sentTTYAddress = sentTTYAddress;
	}

	/**
	 * returns the recipientTTYAddress
	 * 
	 * @return Returns the recipientTTYAddress.
	 * 
	 * @hibernate.property column = "RECIPIENT_TTY_ADDRESS"
	 */
	public String getRecipientTTYAddress() {
		return recipientTTYAddress;
	}

	public void setRecipientTTYAddress(String recipientTTYAddress) {
		this.recipientTTYAddress = recipientTTYAddress;
	}

	/**
	 * returns the sentEmailAddress
	 * 
	 * @return Returns the sentEmailAddress.
	 * 
	 * @hibernate.property column = "SENDER_EMAIL_ADDRESS"
	 */
	public String getSentEmailAddress() {
		return sentEmailAddress;
	}

	public void setSentEmailAddress(String sentEmailAddress) {
		this.sentEmailAddress = sentEmailAddress;
	}

	/**
	 * returns the recipientEmailAddress
	 * 
	 * @return Returns the recipientEmailAddress.
	 * 
	 * @hibernate.property column = "RECIPIENT_EMAIL_ADDRESS"
	 */
	public String getRecipientEmailAddress() {
		return recipientEmailAddress;
	}

	public void setRecipientEmailAddress(String recipientEmailAddress) {
		this.recipientEmailAddress = recipientEmailAddress;
	}

	/**
	 * returns the statusIndicator
	 * 
	 * @return Returns the statusIndicator.
	 * 
	 * @hibernate.property column = "PROCESS_STATUS"
	 */
	public String getStatusIndicator() {
		return statusIndicator;
	}

	public void setStatusIndicator(String statusIndicator) {
		this.statusIndicator = statusIndicator;
	}

	/**
	 * returns the modeOfCommunication
	 * 
	 * @return Returns the modeOfCommunication.
	 * 
	 * @hibernate.property column = "MODE_OF_COMMUNICATION"
	 */
	public String getModeOfCommunication() {
		return modeOfCommunication;
	}

	public void setModeOfCommunication(String modeOfCommunication) {
		this.modeOfCommunication = modeOfCommunication;
	}

	/**
	 * returns the lockStatus
	 * 
	 * @return Returns the lockStatus.
	 * 
	 * @hibernate.property column = "LOCK_STATUS"
	 */
	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	/**
	 * returns the serviceClientCode
	 * 
	 * @return Returns the serviceClientCode.
	 * 
	 * @hibernate.property column = "SERVICE_CLIENT_CODE"
	 */
	public String getServiceClientCode() {
		return serviceClientCode;
	}

	public void setServiceClientCode(String serviceClientCode) {
		this.serviceClientCode = serviceClientCode;
	}

	
	/**
	 * returns the gdsMessageID
	 * 
	 * @return Returns the gdsMessageID.
	 * 
	 * @hibernate.property column = "GDS_MESSAGE_ID"
	 */
	public String getGdsMessageID() {
		return gdsMessageID;
	}

	public void setGdsMessageID(String gdsMessageID) {
		this.gdsMessageID = gdsMessageID;
	}
	
	/**
	 * @hibernate.property column = "RETRY_COUNT"
	 */
	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}	

	/**
	 * @hibernate.property column = "MESSAGE_SEQUENCE"
	 */
	public String getMessageSequence() {
		return messageSequence;
	}

	public void setMessageSequence(String messageSequence) {
		this.messageSequence = messageSequence;
	}

}