package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.constants.CommonsConstants.TimeMode;

public class SSIMessegeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum Action {
		NEW, CNL, RPL, ADM, EQT, FLT, REV, TIM
	};

	public enum DayOfWeek {
		MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6), SUNDAY(7);

		private final int dayNumber;

		DayOfWeek(int dayNumber) {
			this.dayNumber = dayNumber;
		}

		public int dayNumber() {
			return dayNumber;
		}
	}

	private String referenceNumber;

	private Action action;
	private boolean withdrawal = false;

	private String flightDesignator;
	private String newFlightDesignator;

	private Date startDate;
	private Date endDate;
	private Collection<DayOfWeek> daysOfOperation;

	private Date newStartDate;
	private Date newEndDate;
	private List<DayOfWeek> newDaysOfOperation;

	private String serviceType;
	private String aircraftType;
	private String passengerResBookingDesignator;
	
    private String senderAddress = null;
    private String recipientAddress = null;
    private String timestamp = null;

	private List<SSIFlightLegDTO> legs;
	
	private List<String> externalFlightNos;
	
	private boolean codeShareCarrier;
	
	private String segmentCode;
	
	private String externalEmailAddres;
	
	private String externalSitaAddress;

	private String timeMode = TimeMode.ZULU_TIME_MODE;

	private String supplementaryInfo;

	/** ------------- Action Information --------------- */
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public boolean isWithdrawal() {
		return withdrawal;
	}

	public void setWithdrawal(boolean withdrawal) {
		this.withdrawal = withdrawal;
	}

	/** ------------------ Flight Information ------------ */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	/** ------------------ Flight Information ------------ */
	public String getNewFlightDesignator() {
		return newFlightDesignator;
	}

	public void setNewFlightDesignator(String newFlightDesignator) {
		this.newFlightDesignator = newFlightDesignator;
	}

	/** ------------------ Period and Frequency -------- */
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Collection<DayOfWeek> getDaysOfOperation() {
		return daysOfOperation;
	}

	public void setDaysOfOperation(Collection<DayOfWeek> daysOfOperation) {
		this.daysOfOperation = daysOfOperation;
	}

	public void addDayOfWeek(DayOfWeek day) {
		if (daysOfOperation == null)
			daysOfOperation = new ArrayList<DayOfWeek>();
		daysOfOperation.add(day);
	}

	/** ------------------ New Period and Frequency -------- */
	
	public Date getNewStartDate() {
		return newStartDate;
	}

	public void setNewStartDate(Date newStartDate) {
		this.newStartDate = newStartDate;
	}

	public Date getNewEndDate() {
		return newEndDate;
	}

	public void setNewEndDate(Date newEndDate) {
		this.newEndDate = newEndDate;
	}

	public List<DayOfWeek> getNewDaysOfOperation() {
		return newDaysOfOperation;
	}

	public void setNewDaysOfOperation(List<DayOfWeek> newDaysOfOperation) {
		this.newDaysOfOperation = newDaysOfOperation;
	}
	
	public void addNewDayOfWeek(DayOfWeek day) {
		if (newDaysOfOperation == null)
			newDaysOfOperation = new ArrayList<DayOfWeek>();
		newDaysOfOperation.add(day);
	}

	/** ------------------ Equipment Information ------- */
	public String getServiceType() {
		return serviceType;
	}	

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getPassengerResBookingDesignator() {
		return passengerResBookingDesignator;
	}

	public void setPassengerResBookingDesignator(String passengerResBookingDesignator) {
		this.passengerResBookingDesignator = passengerResBookingDesignator;
	}

	/** ------------------ Legs Information ------- */
	public Collection<SSIFlightLegDTO> getLegs() {
		return legs;
	}

	public void setLegs(List<SSIFlightLegDTO> legs) {
		this.legs = legs;
	}

	public void addLeg(SSIFlightLegDTO ssiFlightLeg) {
		if (this.legs == null)
			this.legs = new ArrayList<SSIFlightLegDTO>();
		this.legs.add(ssiFlightLeg);
	}

	/** ------------------ Reference Information ------- */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @return the senderAddress
	 */
	public String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * @param senderAddress the senderAddress to set
	 */
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	/**
	 * @return the recipientAddress
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}

	/**
	 * @param recipientAddress the recipientAddress to set
	 */
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	
	/** ------------------ Segment Information ------- */
	public List<String> getExternalFlightNos() {
		return externalFlightNos;
	}

	public void setExternalFlightNos(List<String> externalFlightNos) {
		this.externalFlightNos = externalFlightNos;
	}

	public boolean isCodeShareCarrier() {
		return codeShareCarrier;
	}

	public void setCodeShareCarrier(boolean codeShareCarrier) {
		this.codeShareCarrier = codeShareCarrier;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}

	public String getExternalEmailAddres() {
		return externalEmailAddres;
	}

	public void setExternalEmailAddres(String externalEmailAddres) {
		this.externalEmailAddres = externalEmailAddres;
	}

	public String getExternalSitaAddress() {
		return externalSitaAddress;
	}

	public void setExternalSitaAddress(String externalSitaAddress) {
		this.externalSitaAddress = externalSitaAddress;
	}
	
	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}	
}
