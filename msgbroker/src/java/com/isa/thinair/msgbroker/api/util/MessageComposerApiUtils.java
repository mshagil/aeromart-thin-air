package com.isa.thinair.msgbroker.api.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.Hibernate;

import com.isa.thinair.airreservation.api.dto.adl.FlightInformation;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.core.bl.pnl.SendMailUsingAuthentication;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.internal.RecordLocator;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.msgbroker.api.dto.OutMessageTO;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.platform.api.LookupServiceFactory;

public abstract class MessageComposerApiUtils {
	public static String generateBookingOffice(String airlineCode) {
		String airlineHub = AppSysParamsUtil.getHubAirport();
		return airlineHub+airlineCode;
	}

	public static String getExternalPos(RecordLocator originatorRecordLocator) {
		String externalPos = null;

		if (originatorRecordLocator != null) {
			externalPos = GDSApiUtils.maskNull(originatorRecordLocator.getBookingOffice() + " "
					+ originatorRecordLocator.getPointOfSales());
		}

		return externalPos;
	}

	public static String generateFileName(FlightInformation flightInfoElement, String msgType, Integer partNumber) {
		MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MsgbrokerConstants.MODULE_NAME);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
		StringBuffer data = new StringBuffer();
		String path;

		if (PNLConstants.MessageTypes.PNL.equals(msgType) || PNLConstants.MessageTypes.ADL.equals(msgType)) {
			path = moduleConfig.getGencalpath();
		} else {
			path = moduleConfig.getGenadlpath();
		}

		data.append(path);
		data.append(MessageComposerConstants.FWD);
		data.append(flightInfoElement.getFlightNumber());
		data.append(MessageComposerConstants.HYPHEN);
		data.append(flightInfoElement.getBoardingAirport());
		data.append(MessageComposerConstants.HYPHEN);
		data.append(compileDepartureDate(flightInfoElement.getFlightDate()));
		data.append(MessageComposerConstants.HYPHEN);
		data.append(msgType);
		data.append(MessageComposerConstants.HYPHEN);
		data.append(simpleDateFormat.format(new Date()));
		data.append(MessageComposerConstants.HYPHEN);
		data.append(MessageComposerConstants.PNLADLMessageConstants.PART + partNumber);
		data.append(".txt");
		return data.toString();
	}

	public static String compileDepartureDate(Date departureDate) {
		StringBuffer data = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMM");
		data.append(dateFormat.format(departureDate));
		return data.toString().toUpperCase();
	}
	
	public static void saveOutMessage(OutMessageTO outMessageTO) throws ModuleException{
		OutMessage outMessage = new OutMessage();
		
		outMessage.setRecipientTTYAddress(outMessageTO.getRecipientTTYAddress());
		outMessage.setAirLineCode(outMessageTO.getAirLineCode());
		outMessage.setMessageType(outMessageTO.getMessageType());
		outMessage.setModeOfCommunication(outMessageTO.getModeOfCommunication());
		outMessage.setSentTTYAddress(outMessageTO.getSentTTYAddress());
		outMessage.setOutMessageText(outMessageTO.getOutMessageText());
		outMessage.setReceivedDate(new Date());
		outMessage.setStatusIndicator(outMessageTO.getStatusIndicator());
		
		MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);

	}

}
