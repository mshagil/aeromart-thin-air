/**
 * 
 */
package com.isa.thinair.msgbroker.api.dto;


/**
 * @author sanjeewaf
 */
public class InNacDTO extends RequestDTO  {

    /**
     * 
     */
    private static final long serialVersionUID = -8480481435346069991L;
    
    public static enum MessageIdentifier {SSM,ASM,AVS};

    //private MessageIdentifier referredMessageIdentifier;

    private String referredMessageSequenceReference;
    
    private String errorMessage;
    
   // private String referredNacMessage;
    
    //private String originatorAddress;

    //private String responderAddress;


    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * @return the referredMessageSequenceReference
     */
    public String getReferredMessageSequenceReference() {
        return referredMessageSequenceReference;
    }

    /**
     * @param referredMessageSequenceReference the referredMessageSequenceReference to set
     */
    public void setReferredMessageSequenceReference(String referredMessageSequenceReference) {
        this.referredMessageSequenceReference = referredMessageSequenceReference;
    }

//    /**
//     * @return the referredNacMessage
//     */
//    public String getReferredNacMessage() {
//        return referredNacMessage;
//    }
//
//    /**
//     * @param referredNacMessage the referredNacMessage to set
//     */
//    public void setReferredNacMessage(String referredNacMessage) {
//        this.referredNacMessage = referredNacMessage;
//    }
    
    
    
    

}
