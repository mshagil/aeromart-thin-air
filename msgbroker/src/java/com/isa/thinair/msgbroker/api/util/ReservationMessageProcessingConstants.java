package com.isa.thinair.msgbroker.api.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

public class ReservationMessageProcessingConstants implements Serializable {

	public static interface ScheduleMessage {

		public enum TimeMode {
			UTC, LT;
		}

		public enum ActionIdentifier {
			NEW, CNL, RPL, EQT, FLT, REV, TIM;
		}

		public enum WeekDay {
			SUN, MON, TUE, WED, THU, FRI, SAT;
		}

		// public static interface TimeModes {
		// public static final String UTC = "UTC";
		// public static final String LT = "LT";
		// }
		//
		// public static interface FrequencyRate {
		// public static final String W1 = "W1";
		// public static final String W2 = "W2";
		// }
		//
		//
		// public static interface ValidateMessages {
		// public static final String CreatorReferenceLengthInvalid =
		// "Creator reference value length is not between 1 and 35.";
		// }
	}

	public static interface InMessageRequestType {
		public static final String Booking = "BKG";
		public static final String AmendCancel = "AMD";
		public static final String Divide = "DVD";
		public static final String RecordLoactorRequest = "RLR";
		public static final String HostNotification = "HNT";
	}

	public static interface OutMessageRequestType {
		public static final String AvailabilityStatus = "AVS";
		public static final String Booking = "BKG";
		public static final String AmendCancel = "AMD";
		public static final String Divide = "DVD";
		public static final String RecordLoactorRequest = "RLR";
	}

	public static interface HostNotifications {
		public static final String AVSNotification = "AVS";
	}

	public static interface ScheduleMessageType {
		public static final String Schedule_Data_Set = "SDS";
		public static final String Schedule_Chage = "SSM";
		public static final String Schedule_Adhoc = "ASM";
	}

	public static interface ReservationMainOperations {
		public static final String NewBooking = "NEWBOOKING";
		public static final String InvalidRequest = "INVALIDREQUEST";
		public static final String ModifyReservation = "MODIFYRESERVATION";
		public static final String SplitReservation = "SPLITRESERVATION";
	}

	public static interface ReservationSubOperations {
		public static final String AddSegment = "ADDSEGMENT";
		public static final String CancelReservation = "CANCELRESERVATION";
		public static final String DeleteSegment = "DELETESEGMENT";
		public static final String WaitlistedSegment = "WAITLISTEDSEGMENT";
		public static final String NameChange = "NAMECHANGE";
	}

	public static interface BookingRequestActionCodes {
		public static interface NewSegmentActionCodes {
			public static final String SS = "SS";
			public static final String FS = "FS";
			public static final String NN = "NN";
			public static final Collection NewSegmentActionCodes = Arrays.asList("SS", "FS", "NN");
		}

		public static interface DeleteSegmentActionCodes {
			public static final String XX = "XX";
			public static final String XR = "XR";
			public static final String IX = "IX";
			public static final Collection DeleteSegmentActionCodes = Arrays.asList("XX", "XR", "IX");
		}

		public static interface NewDuplicateSegmentActionCodes {
			public static final String IN = "IN";
			public static final String IS = "IS";
			public static final Collection NewDuplicateSegmentActionCodes = Arrays.asList("IN", "IS");
		}

		public static interface WaitListActionCodes {
			public static final String LL = "LL";
			public static final Collection WaitListActionCodes = Arrays.asList("LL");
		}

		public static interface NewSegmentNoReplyActionCodes {
			public static final String SQ = "SQ";
			public static final Collection NewSegmentNoReplyActionCodes = Arrays.asList("SQ");
		}

	}

}
