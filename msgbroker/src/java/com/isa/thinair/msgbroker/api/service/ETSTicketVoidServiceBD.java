package com.isa.thinair.msgbroker.api.service;

import com.isa.thinair.commons.api.dto.ets.VoidTKTREQDTO;
import com.isa.thinair.commons.api.dto.ets.VoidTKTRESResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface ETSTicketVoidServiceBD {

	public static final String SERVICE_NAME = "TicketVoidService";

	public VoidTKTRESResponseDTO voidTicket(VoidTKTREQDTO ticketVoidRequest) throws ModuleException;

}
