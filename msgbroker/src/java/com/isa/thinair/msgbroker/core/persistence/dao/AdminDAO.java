package com.isa.thinair.msgbroker.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;

public interface AdminDAO {

	public ServiceBearer getServiceBearer(String airlineCode, String gdsCode);

	public ServiceBearer getServiceBearer(String carrierCode);

	public ServiceBearer getServiceBearerAirlineForTTYAddress(String ttyAddress);
	
	public ServiceBearer getServiceBearerAirlineForEmailAddress(String emailAddress);

	public List<ServiceBearer> getServiceBearerAirlines();

	public List getServiceBearerAirlinesByDesc();

	public void saveServiceBearerAirline(ServiceBearer airline);

	public void removeServiceBearerAirline(ServiceBearer airline);

	// ServiceClient

	public ServiceClient getServiceClient(String gdsCode);

	public List getServiceClients();

	public void saveServiceClient(ServiceClient gds);

	public void removeServiceClient(ServiceClient gds);

	public List getServiceClients(Collection gdsCodes);

	public ServiceClient getServiceClientForTTYAddress(String ttyAddress);
	
	public String getMailAccesProtocol(String gdsCode);
	
	public List<String> getPendingExternalSystemMessagingModes();

}