package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.concurrent.TimeUnit;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

import com.isa.thinair.commons.api.dto.ets.ETicketGenerationReqDTO;
import com.isa.thinair.commons.api.dto.ets.ETicketGenerationResDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.accelaero.ets.SampleServiceGrpc;
import com.accelaero.ets.SampleServiceWrapper.TestInput;
import com.accelaero.ets.SampleServiceWrapper.TestOutput;
import com.isa.thinair.msgbroker.core.etsclient.TicketGenerationServiceClient;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketGenerationServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketGenerationServiceBeanRemote;

@Stateless
@RemoteBinding(jndiBinding = "TicketGenerationService.remote")
@LocalBinding(jndiBinding = "TicketGenerationService.local")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ETSTicketGenerationServiceBean extends PlatformBaseSessionBean implements ETSTicketGenerationServiceBeanLocal,
		ETSTicketGenerationServiceBeanRemote {

	private final Log log = LogFactory.getLog(ETSTicketGenerationServiceBean.class);

	@Override
	public ETicketGenerationResDTO generateEtickets(ETicketGenerationReqDTO eTicketGenerationReq) throws ModuleException {

		ETicketGenerationResDTO eTicketGenerationResDTO = null;
		try {
			ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9071).usePlaintext(true).build();
			SampleServiceGrpc.SampleServiceBlockingStub blockingStub = SampleServiceGrpc.newBlockingStub(channel);
			callSampleServiceMethod(blockingStub);
			shutdown(channel);			
//			TicketGenerationServiceClient client = new TicketGenerationServiceClient("localhost", 9071);
//			client.callSampleServiceMethod();
//			client.shutdown();
		} catch (Exception e) {
			log.info(e);
		}
		return eTicketGenerationResDTO;
	}
	
	public void callSampleServiceMethod(SampleServiceGrpc.SampleServiceBlockingStub blockingStub) {
		TestInput input = TestInput.newBuilder().build();
		TestOutput output;
		try {
			output = blockingStub.sampleMethod(input);
			System.out.println(output.getMessage());
		} catch (StatusRuntimeException e) {
			e.printStackTrace();
		}
	}
	
	public void shutdown(ManagedChannel channel) throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}	
}
