package com.isa.thinair.msgbroker.core.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.ServiceBearerDTO;
import com.isa.thinair.commons.api.dto.ServiceClientDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.msgbroker.api.dto.BookingRequestDTO;
import com.isa.thinair.msgbroker.api.dto.InMessageProcessResultsDTO;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.core.dto.MessageComposerResponseDTO;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.msgbroker.core.util.BookingRequestDTOConverter;
import com.isa.thinair.msgbroker.core.util.GDSConfigAdaptor;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;

public class OutboundTypeBMessageAssembler {

	private final Log log = LogFactory.getLog(OutboundTypeBMessageAssembler.class);

	private AdminDAO adminDAO;

	private ServiceClientReqDaoJDBC serviceClientReqDaoJDBC;

	private OutMessageProcessingStatus outMessageProcessingStatus;

	private ServiceClientDTO serviceClientDTO;

	private ServiceBearerDTO serviceBearerDTO;

	public InMessageProcessResultsDTO assembleOutboundTypeBMessage(TypeBRQ typeBRQ) {

		log.debug("Inside assembleOutboundTypeBMessage of OutboundTypeBMessageAssembler");

		InMessageProcessResultsDTO inMessageProcessResultsDTO = new InMessageProcessResultsDTO();
		GDSMessageComposer messageComposer = new GDSMessageComposer();
		MessageComposerResponseDTO composerResponseDTO;
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO bookingResponseDTO = typeBRQ.getBookingRequestDTO();

		if (bookingResponseDTO != null) {

			BookingRequestDTO bookingRequestDTO = new BookingRequestDTO();
			OutMessage outMessage = new OutMessage();

			try {
				String serviceClientCode = bookingResponseDTO.getGds().getCode();
				int typeBSyncType = TTYMessageUtil.getTypeBSyncTypeOfGDS(serviceClientCode);
				setGDSConfigsByServiceClientCode(serviceClientCode);

				if (serviceClientDTO != null && serviceBearerDTO != null) {

					String receiverTTYAddress = null;
					if (AppSysParamsUtil.isGDSCsConfigsFromCache()) {
						receiverTTYAddress = TTYMessageUtil.getOutTTYForServiceClientReqType(
								serviceClientDTO.getServiceClientCode(), bookingResponseDTO.getMessageIdentifier());
					} else {
						receiverTTYAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(
								serviceClientDTO.getServiceClientCode(), bookingResponseDTO.getMessageIdentifier());
					}
					bookingRequestDTO.setOriginatorAddress(receiverTTYAddress);
					bookingRequestDTO.setResponderAddress(serviceBearerDTO.getSitaAddress());
					bookingRequestDTO.setMessageReceivedDateStamp(new Date());
					bookingRequestDTO.setBookingSegmentDTOs(BookingRequestDTOConverter
							.convertAccelaeroBookingSegmentDTOsToMsgBroker(bookingResponseDTO.getBookingSegmentDTOs()));
					bookingRequestDTO.setOriginatorRecordLocator(BookingRequestDTOConverter
							.convertAccelaeroRecordLocatorDTOToMsgBroker(bookingResponseDTO.getOriginatorRecordLocator(),
									receiverTTYAddress));
					bookingRequestDTO.setMessageIdentifier(bookingResponseDTO.getMessageIdentifier());
					bookingRequestDTO.setResponseMessageIdentifier(bookingResponseDTO.getResponseMessageIdentifier());
					bookingRequestDTO.setNewNameDTOs(BookingRequestDTOConverter
							.convertAccelaeroNameDTOsToMsgBroker(bookingResponseDTO.getNewNameDTOs()));
					bookingRequestDTO.setChangedNameDTOs(BookingRequestDTOConverter
							.convertAccelaeroChangeNameDTOsToMsgBroker(bookingResponseDTO.getChangedNameDTOs()));
					bookingRequestDTO.setUnChangedNameDTOs(BookingRequestDTOConverter
							.convertAccelaeroUnChangeNameDTOsToMsgBroker(bookingResponseDTO.getUnChangedNameDTOs()));
					bookingRequestDTO.setSsrDTOs(BookingRequestDTOConverter.convertAccelaeroSsrDTOsToMsgBroker(
							bookingResponseDTO.getSsrDTOs(), bookingRequestDTO.getBookingSegmentDTOs()));
					bookingRequestDTO.setMessageCategory(bookingResponseDTO.getMessageCategory());

					outMessage.setAirLineCode(carrierCode);
					if (bookingResponseDTO.getOriginatorRecordLocator() != null) {
						outMessage.setRecordLocator(bookingResponseDTO.getOriginatorRecordLocator().getPnrReference());
					}
					outMessage.setMessageType(bookingResponseDTO.getMessageIdentifier());
					outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
					outMessage.setRecipientTTYAddress(receiverTTYAddress);
					if (serviceClientDTO.getApiUrl() != null) {
						outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.API);
						outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Sent);
						outMessage.setWsServerUrl(serviceClientDTO.getApiUrl());
					} else {
						outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);
						outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
					}
					outMessage.setProcessedDate(new Date());
					outMessage.setReceivedDate(new Date());
					outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(
							receiverTTYAddress, serviceClientDTO.getEmailDomain()));

					composerResponseDTO = messageComposer
							.composeGDSBookingMessage(BookingRequestDTOConverter
									.syncronizeMsgBrokerBookingRequestDTOWithAccelaero(bookingRequestDTO, bookingResponseDTO),
									serviceBearerDTO.getAirlineCode(), typeBSyncType, serviceClientDTO
											.getAddAddressToMessageBody(), true);

					if (composerResponseDTO.isSuccess()) {
						if (bookingRequestDTO.getResponderRecordLocator() != null
								&& bookingRequestDTO.getResponderRecordLocator().getPnrReference() != null) {
							outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
							outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
							outMessage.setSentTTYAddress(serviceBearerDTO.getSitaAddress());
						}
					} else {
						log.debug("Composing TypeB message is not success.");
					}

					inMessageProcessResultsDTO.setOutMessage(outMessage);
					outMessageProcessingStatus = new OutMessageProcessingStatus();
					outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
					outMessageProcessingStatus
							.setErrorMessageCode(MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.RESPONSE_QUEUED_FOR_TRANSMISSION);
					outMessageProcessingStatus.setProcessingDate(new Date());
					outMessageProcessingStatus
							.setProcessingStatus(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
					inMessageProcessResultsDTO.setOutMessageProcessingStatus(outMessageProcessingStatus);

				} else {
					throw new ModuleException(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.UNKNOWN_REQUESTER);
				}
			} catch (ModuleException e) {
				log.error(e);
			} catch (Exception e) {
				log.error(e);
			}
		}
		return inMessageProcessResultsDTO;
	}

	public List<InMessageProcessResultsDTO> prepareOutboundTypeBMessages(List<TypeBRQ> typeBRQs) {
		List<InMessageProcessResultsDTO> inMessageProcessResultsDTOs = new ArrayList<InMessageProcessResultsDTO>();
		for (TypeBRQ typeBRQ : typeBRQs) {
			inMessageProcessResultsDTOs.add(assembleOutboundTypeBMessage(typeBRQ));
		}
		return null;
	}

	private void setGDSConfigsByServiceClientCode(String serviceClientCode) {
		if (AppSysParamsUtil.isGDSCsConfigsFromCache()) {
			serviceClientDTO = TTYMessageUtil.getServiceClientByServiceClientCode(serviceClientCode);
			serviceBearerDTO = TTYMessageUtil.getServiceBearerByServiceClientCode(serviceClientCode);
		} else {
			ServiceClient serviceClient = getAdminDAO().getServiceClient(serviceClientCode);
			if (serviceClient != null) {
				serviceClientDTO = GDSConfigAdaptor.adapt(serviceClient);
				ServiceBearer serviceBearerAirline = getAdminDAO().getServiceBearer(AppSysParamsUtil.getDefaultCarrierCode(),
						serviceClientCode);
				if (serviceBearerAirline != null) {
					serviceBearerDTO = GDSConfigAdaptor.adapt(serviceBearerAirline);
				}
			}
		}
	}

	private ServiceClientReqDaoJDBC getServiceClientReqTypeDAO() {
		if (serviceClientReqDaoJDBC == null) {
			serviceClientReqDaoJDBC = LookupUtil.lookupServiceClientReqDAO();
		}
		return serviceClientReqDaoJDBC;
	}

	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}
}
