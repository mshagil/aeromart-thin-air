package com.isa.thinair.msgbroker.core.adaptor;

import com.isa.cloud.support.generic.adaptor.AbstractRequestGrpcDtoAdapter;
import com.isa.thinair.commons.api.dto.ets.IssueTKTRESResponseDTO;
import com.accelaero.ets.SimplifiedTicketServiceWrapper;

public class IssueTKTRESAdaptor extends AbstractRequestGrpcDtoAdapter<SimplifiedTicketServiceWrapper.IssueTKTRESResponse, IssueTKTRESResponseDTO> {

	public IssueTKTRESAdaptor() {
		super(SimplifiedTicketServiceWrapper.IssueTKTRESResponse.class, IssueTKTRESResponseDTO.class);
	}
}
