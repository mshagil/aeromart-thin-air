/**
 * 
 */
package com.isa.thinair.msgbroker.core.dto;



/**
 * @author sanjeewaf
 *
 */
public class MessageComposerResponseDTO extends ServiceResponseDTO {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String status;
    
    private String formData;
    
    private String composedMessage;

    /**
     * @return the composedMessage
     */
    public String getComposedMessage() {
        return composedMessage;
    }

    /**
     * @param composedMessage the composedMessage to set
     */
    public void setComposedMessage(String composedMessage) {
        this.composedMessage = composedMessage;
    }

    /**
     * @return the formData
     */
    public String getFormData() {
        return formData;
    }

    /**
     * @param formData the formData to set
     */
    public void setFormData(String formData) {
        this.formData = formData;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    

}
