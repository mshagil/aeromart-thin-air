package com.isa.thinair.msgbroker.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.ServiceClientDTO;
import com.isa.thinair.commons.api.dto.ServiceClientTTYDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO;
import com.isa.thinair.msgbroker.api.dto.SSMASMRequestDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.bl.MDBInvoker;
import com.isa.thinair.msgbroker.core.bl.MesageConstants;
import com.isa.thinair.msgbroker.core.bl.SSMASMIBMsgProcessorImpl;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;
import com.isa.thinair.msgbroker.core.dto.QueuedFlightDesignatorInfoDTO;

/**
 * Class for processing inMessages This will direct SSM/ASM messages to queue
 * Other messages will add to existing JMS queue
 * Only purpose of this class to decide weather to queue or not
 * @author rajitha
 *
 */
public class PreMessageProcessor {

	private static final Log log = LogFactory.getLog(PreMessageProcessor.class);

	public void messageProcessor(List<InMessage> unprocessedMessages, List<Integer> msgIdsToProcess)
			throws ModuleException {

		List<Integer> msgIdsAlreadyIntheQueue = getAllMessageIdsInQueue();
		if (msgIdsToProcess != null && !msgIdsToProcess.isEmpty()) {
			for (InMessage currentProcessingInMessage : unprocessedMessages) {
				
				if (isMessagesLockedInProcessingQueue()) {
					if (msgIdsAlreadyIntheQueue.contains(currentProcessingInMessage.getInMessageID())) {
						continue;
					}
				} else if (!msgIdsToProcess.contains(currentProcessingInMessage.getInMessageID())) {
					continue;
				}
				
				if (!MessageFilterCriteria.getMessageTypesToFilter().isEmpty()
						&& MessageFilterCriteria.getMessageTypesToFilter().contains(currentProcessingInMessage.getMessageType())) {
					try {
						
						boolean isMsgAlreadyInQueue = msgIdsAlreadyIntheQueue.contains(currentProcessingInMessage.getInMessageID());
						processMessages(currentProcessingInMessage, isMsgAlreadyInQueue);
					} catch (Exception ex) {
						log.error("Error message queue" + ex);
						mdbInvokerForOtherMessages(currentProcessingInMessage.getInMessageID());
					}
				} else {
					
					mdbInvokerForOtherMessages(currentProcessingInMessage.getInMessageID());
				}
			}
		}
	}

	private List<Integer> getAllMessageIdsInQueue() {
		return LookupUtil.lookupMessageDAO().allInMessageIdsInqueue();
	}

	private void mdbInvokerForOtherMessages(Integer messageId) {
		startMessageProcessing(messageId);
	}
	
	private void processMessages(InMessage inMessageToProcess, boolean isMessageAlreadyInQueue) throws ModuleException {

		Integer inMessageId = inMessageToProcess.getInMessageID();
		MessageParserResponseDTO messageParserResponseDTO = parseInMessage(inMessageToProcess);
		List<QueuedFlightDesignatorInfoDTO> flightDesignatorInfoDTOList = getFlightDesignatorInfo(messageParserResponseDTO,
				inMessageToProcess);

		if (!flightDesignatorInfoDTOList.isEmpty()) {
			Map<Integer, String> msgProcessingQueueFlightDersignatorMap = getInMessageProcessingQueueFlightDesignatorMap(
					flightDesignatorInfoDTOList);
			
			if (!msgProcessingQueueFlightDersignatorMap.isEmpty()) {
				Set<Integer> messageProcessingQueueIdSet = msgProcessingQueueFlightDersignatorMap.keySet();
				for (Integer inMsgProcessingQueueId : messageProcessingQueueIdSet) {
					updateOtherFlightDesignatorTable(inMsgProcessingQueueId, flightDesignatorInfoDTOList);
					if (isScheduleFlightInLockStatus(inMsgProcessingQueueId)) {
						addMessageForWaitingQueue(inMsgProcessingQueueId,
								msgProcessingQueueFlightDersignatorMap.get(inMsgProcessingQueueId), inMessageId);
					} else {
						
						if (isMessageAlreadyInQueue) {
							removeMsgFromWaitingQueue(inMessageId, inMsgProcessingQueueId);
						}
						
						updateMessageStatusToLockAndStartProcessing(inMsgProcessingQueueId, inMessageId);
					}
				}
			} else {
				
				String flightDesignatorToProcessMsg = flightDesignatorInfoDTOList.get(0).getFlightDesignator();
				addMsgToInMessageProcessingQueue(flightDesignatorToProcessMsg, inMessageId);
				Integer inMessageProcessingQueueId = LookupUtil.lookupMessageDAO()
						.retrieveInMsgProcessingQueueId(flightDesignatorToProcessMsg);
				updateOtherFlightDesignatorTable(inMessageProcessingQueueId,flightDesignatorInfoDTOList);
				startMessageProcessing(inMessageId);
				
			}

		}
	}


	private void updateOtherFlightDesignatorTable(Integer inMessageProcessingQueueId,
			List<QueuedFlightDesignatorInfoDTO> flightDesignatorDTOList) {

		for (QueuedFlightDesignatorInfoDTO otherFlightDesignatorDTO : flightDesignatorDTOList) {
			if (otherFlightDesignatorDTO.isFlightDesignatorChanged()) {
				updateFlightDesignatorInfoTable(inMessageProcessingQueueId,
						otherFlightDesignatorDTO.getNewFlightDesignator());
			}
		}

	}
	
	private void updateFlightDesignatorInfoTable(Integer inMessageProcessingQueueId, String newFlightDesignator) {
		LookupUtil.lookupMessageDAO().updateFlightDesignatorDependencyTable(inMessageProcessingQueueId, newFlightDesignator);
	}
	
	private MessageParserResponseDTO parseInMessage(InMessage inMessage) throws ModuleException {

		SSMASMIBMsgProcessorImpl msgParser = new SSMASMIBMsgProcessorImpl();
		MessageParserResponseDTO messageParserResponseDTO = msgParser.ssmAsmInMessageParser(inMessage);
		if(!messageParserResponseDTO.isSuccess()){
			throw new ModuleException("Error when parsing the message");
		}
		return messageParserResponseDTO;
	}

	
	private List<QueuedFlightDesignatorInfoDTO> getFlightDesignatorInfo(MessageParserResponseDTO messageParserResponseDTO, InMessage inMessage)
			throws ModuleException {

		boolean isValidCarrier = false;
		Object obj = messageParserResponseDTO.getRequestDTO();
		List<QueuedFlightDesignatorInfoDTO> queuedFlightDesignatorInfoList = new ArrayList<>();
		if (obj instanceof SSMASMRequestDTO) {

			SSMASMRequestDTO scheduleRequestDTO = (SSMASMRequestDTO) obj;
			SSMASMIBMsgProcessorImpl msgParser = new SSMASMIBMsgProcessorImpl();
			String senderAddress = msgParser.retrieveSenderAddress(inMessage.getSentEmailAddress(),
					inMessage.getSentTTYAddress());
			ServiceClientTTYDTO serviceClientTTYDTO = TTYMessageUtil.getServiceClientTTYForReqTypeTTY(senderAddress,
					inMessage.getMessageType());

			if (serviceClientTTYDTO != null) {

				ServiceClientDTO serviceClientDTO = TTYMessageUtil
						.getServiceClientByServiceClientCode(serviceClientTTYDTO.getServiceClientCode());

				if (serviceClientDTO != null) {
					Map<String, GDSStatusTO> gdsStatusMap = MsgbrokerUtils.getGlobalConfig().getActiveGdsMap();
					String gdsCode = serviceClientDTO.getServiceClientCode();
					if (gdsCode != null && gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
						for (String gdsId : gdsStatusMap.keySet()) {
							GDSStatusTO gdsStatusTO = gdsStatusMap.get(gdsId);
							if (gdsStatusTO != null && gdsCode.equalsIgnoreCase(gdsStatusTO.getGdsCode())) {
								scheduleRequestDTO.setOperatingCarrier(gdsStatusTO.getCarrierCode());
								scheduleRequestDTO.setCodeShareCarrier(gdsStatusTO.isCodeShareCarrier());
								scheduleRequestDTO.setMarketingFlightPrefix(gdsStatusTO.getMarketingFlightPrefix());
								isValidCarrier = true;
								break;
							}
						}

					}
					
					if (isValidCarrier) {

						scheduleRequestDTO.getAsmssmMetaDataDTO().setInMessegeID(inMessage.getInMessageID());
						scheduleRequestDTO.setRawMessage(inMessage.getInMessageText());
						SSMASMMessegeDTO ssmAsmMessageDTO = MessageRequestProcessingUtil
								.convertMsgBrokerScheduleDTOToAccelaero(scheduleRequestDTO);

						if (ssmAsmMessageDTO.isValidRequest()) {

							try {
								for (SSMASMSUBMessegeDTO subMsgDTO : ssmAsmMessageDTO.getSsmASMSUBMessegeList()) {
									if (subMsgDTO.getAction().equals(SSMASMSUBMessegeDTO.Action.ACK)
											|| subMsgDTO.getAction().equals(SSMASMSUBMessegeDTO.Action.NAC)) {

										break;
									}
									QueuedFlightDesignatorInfoDTO queuedFlightDesignatorInfoDTO = new QueuedFlightDesignatorInfoDTO();
									
									String flightNumberWithSuffix = subMsgDTO.getFlightDesignator();

									if (flightNumberWithSuffix != null) {
										queuedFlightDesignatorInfoDTO.setFlightDesignator(flightNumberWithSuffix);
										
										// FIXME :: SSM/ASM Message should be processed based on Message Sequence, once
										// that is done we can enable following code


										// char charAtLast = flightNumberWithSuffix
										// .charAt(flightNumberWithSuffix.length() - 1);
										//
										// if (Character.isLetter(charAtLast)) {
										// String flightNumberWithoutSuffix = flightNumberWithSuffix.substring(0,
										// flightNumberWithSuffix.length() - 1);
										// queuedFlightDesignatorInfoDTO
										// .setFlightDesignator(flightNumberWithoutSuffix);
										// } else {
										//
										// queuedFlightDesignatorInfoDTO.setFlightDesignator(flightNumberWithSuffix);
										// }

										if (subMsgDTO.getAction().equals(SSMASMSUBMessegeDTO.Action.FLT)) {
											queuedFlightDesignatorInfoDTO
													.setNewFlightDesignator(subMsgDTO.getNewFlightDesignator());
											queuedFlightDesignatorInfoDTO.setHasFlightDesignatorChanged(true);
										}
										queuedFlightDesignatorInfoList.add(queuedFlightDesignatorInfoDTO);

									}
								}
							} catch (Exception ex) {
								log.error(" ERROR WHILE RETRIEVING FLIGHT DESIGNATOR " + ex.getCause());
							}
						}
					}
				}
			}
		}

		if (queuedFlightDesignatorInfoList.isEmpty()) {
			log.info(" #NO FLIGHT DESIGNATOR FOUND# ");
			mdbInvokerForOtherMessages(inMessage.getInMessageID());
		}
		return queuedFlightDesignatorInfoList;
	}

	private boolean isScheduleFlightInLockStatus(int inMsgProcessingId) {
		return LookupUtil.lookupMessageDAO().isScheduleLockForProcessing(inMsgProcessingId);
	}


	private void updateMessageStatusToLockAndStartProcessing( Integer msgProcessingQueueId, Integer inMsgId) {
		
		LookupUtil.lookupMessageDAO().updateScheduleForLockStatus(msgProcessingQueueId,
				MesageConstants.InMessageQueueStatus.LOCK, inMsgId);
		startMessageProcessing(inMsgId);
		
	}
	
	
	private Map<Integer, String> getInMessageProcessingQueueFlightDesignatorMap(
			List<QueuedFlightDesignatorInfoDTO> flightDesignatorDTOList) {
		
		Integer inMessageProcessingQueueId = null;
		Map<Integer, String> msgPrtocessingQueueFlightDersignatorMap = new HashMap<>();
		for (QueuedFlightDesignatorInfoDTO flightDesignatorDTO : flightDesignatorDTOList) {
			String flightDesignator = flightDesignatorDTO.getFlightDesignator();
			inMessageProcessingQueueId = LookupUtil.lookupMessageDAO().retrieveInMsgProcessingQueueId(flightDesignator);
			if (inMessageProcessingQueueId != null) {
				msgPrtocessingQueueFlightDersignatorMap.put(inMessageProcessingQueueId, flightDesignator);
				break;
			}
		}

		return msgPrtocessingQueueFlightDersignatorMap;
	}

	private void addMessageForWaitingQueue(Integer msgProcessingQueueId, String flightDesignator, Integer inMessageId) {
		LookupUtil.lookupMessageDAO().addMessageForWaitingQueue(msgProcessingQueueId, inMessageId);
	}

	private void startMessageProcessing(Integer inMessageId) {

		MDBInvoker mdbInvoker = new MDBInvoker();
		mdbInvoker.invokeInboundMessageProcessorMDB(inMessageId);
		log.info("###STARTED### " + inMessageId);

	}

	
	private void  addMsgToInMessageProcessingQueue(String flightDesignatorToProcessMsg, Integer  inMessageId){
		LookupUtil.lookupMessageDAO().setMessageStatusForProcessing(flightDesignatorToProcessMsg, inMessageId,
				MesageConstants.InMessageQueueStatus.LOCK);
	}
	
	private boolean isMessagesLockedInProcessingQueue() {

		return LookupUtil.lookupMessageDAO().isMessagesLockedInProcessingQueue();
	}
	
	private void removeMsgFromWaitingQueue(int inMsgID, Integer msgProcessingQueueId){
		LookupUtil.lookupMessageDAO().removeMessageFromWaitingQueue(inMsgID, msgProcessingQueueId);
	}
}
