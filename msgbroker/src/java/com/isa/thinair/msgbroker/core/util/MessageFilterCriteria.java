package com.isa.thinair.msgbroker.core.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author rajitha
 *
 */
public enum MessageFilterCriteria implements Serializable {
	
	ASM("ASM"),
	SSM("SSM");

	private final String msgType;
	
	MessageFilterCriteria(String msgType){
		 this.msgType = msgType;
	}
	
	public String getMsgType() {
		return this.msgType;
	}
	
	public static List<String> getMessageTypesToFilter(){
		
		List <String> messageTypes = new ArrayList<String>();
		
		for(MessageFilterCriteria criteria : MessageFilterCriteria.values()){
			messageTypes.add(criteria.getMsgType());
		}
		
		return messageTypes;
	}
}
