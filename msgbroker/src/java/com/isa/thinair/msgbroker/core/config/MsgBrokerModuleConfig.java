package com.isa.thinair.msgbroker.core.config;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.platform.api.DefaultModuleConfig;

import java.util.Map;

/**
 * @author Ishan
 * @isa.module.config-bean
 */
public class MsgBrokerModuleConfig extends DefaultModuleConfig {

	private String templatesroot;
	private String ejbAccessUsername;
	private String ejbAccessPassword;
	private boolean invokeCredentials;

	private String genadlpath;
	private String	gencalpath;
	private String pnlAdlOutLocation;
	private String etlPrlInLocation;
	private String etlPrlBackupLocation;
	private String pfsInLocation;
	private String pfsBackupLocation;
	private TypeBMessageStructure typeBMessageStructure;
	private Map<GDSExternalCodes.GDS, TypeBMessages> typeBMessages;
	private String etsHost;
	private int etsPort;
	private String msgAdaptorHost;
	private int msgAdaptorPort;

	public static class TypeBMessages {
		private GDSExternalCodes.GDS gdsCode;
		private boolean sendReservationImage;

		public GDSExternalCodes.GDS getGdsCode() {
			return gdsCode;
		}

		public void setGdsCode(GDSExternalCodes.GDS gdsCode) {
			this.gdsCode = gdsCode;
		}

		public boolean isSendReservationImage() {
			return sendReservationImage;
		}

		public void setSendReservationImage(boolean sendReservationImage) {
			this.sendReservationImage = sendReservationImage;
		}
	}

	public String getTemplatesroot() {
		return templatesroot;
	}

	public void setTemplatesroot(String templatesroot) {
		this.templatesroot = templatesroot;
	}
	public String getGenadlpath() {
		return genadlpath;
	}

	public void setGenadlpath(String genadlpath) {
		this.genadlpath = genadlpath;
	}

	public String getEjbAccessUsername() {
		return ejbAccessUsername;
	}

	public void setEjbAccessUsername(String ejbAccessUsername) {
		this.ejbAccessUsername = ejbAccessUsername;
	}

	public String getEjbAccessPassword() {
		return ejbAccessPassword;
	}

	public void setEjbAccessPassword(String ejbAccessPassword) {
		this.ejbAccessPassword = ejbAccessPassword;
	}

	public boolean isInvokeCredentials() {
		return invokeCredentials;
	}

	public void setInvokeCredentials(boolean invokeCredentials) {
		this.invokeCredentials = invokeCredentials;
	}

	public String getPnlAdlOutLocation() {
		return pnlAdlOutLocation;
	}

	public void setPnlAdlOutLocation(String pnlAdlOutLocation) {
		this.pnlAdlOutLocation = pnlAdlOutLocation;
	}

	public String getEtlPrlInLocation() {
		return etlPrlInLocation;
	}

	public void setEtlPrlInLocation(String etlPrlInLocation) {
		this.etlPrlInLocation = etlPrlInLocation;
	}

	public String getEtlPrlBackupLocation() {
		return etlPrlBackupLocation;
	}

	public void setEtlPrlBackupLocation(String etlPrlBackupLocation) {
		this.etlPrlBackupLocation = etlPrlBackupLocation;
	}

	public String getPfsInLocation() {
		return pfsInLocation;
	}

	public void setPfsInLocation(String pfsInLocation) {
		this.pfsInLocation = pfsInLocation;
	}

	public String getPfsBackupLocation() {
		return pfsBackupLocation;
	}

	public void setPfsBackupLocation(String pfsBackupLocation) {
		this.pfsBackupLocation = pfsBackupLocation;
	}

	public TypeBMessageStructure getTypeBMessageStructure() {
		return typeBMessageStructure;
	}

	public void setTypeBMessageStructure(TypeBMessageStructure typeBMessageStructure) {
		this.typeBMessageStructure = typeBMessageStructure;
	}

	public Map<GDSExternalCodes.GDS, TypeBMessages> getTypeBMessages() {
		return typeBMessages;
	}

	public void setTypeBMessages(Map<GDSExternalCodes.GDS, TypeBMessages> typeBMessages) {
		this.typeBMessages = typeBMessages;
	}

	/**
	 * @return the gencalpath
	 */
	public String getGencalpath() {
		return gencalpath;
	}

	/**
	 * @param gencalpath the gencalpath to set
	 */
	public void setGencalpath(String gencalpath) {
		this.gencalpath = gencalpath;
	}

	public String getEtsHost() {
		return etsHost;
	}

	public int getEtsPort() {
		return etsPort;
	}

	public void setEtsHost(String etsHost) {
		this.etsHost = etsHost;
	}

	public void setEtsPort(int etsPort) {
		this.etsPort = etsPort;
	}
	
	public String getMsgAdaptorHost() {
		return msgAdaptorHost;
	}
	

	public void setMsgAdaptorHost(String msgAdaptorHost) {
		this.msgAdaptorHost = msgAdaptorHost;
	}
	

	public int getMsgAdaptorPort() {
		return msgAdaptorPort;
	}
	

	public void setMsgAdaptorPort(int msgAdaptorPort) {
		this.msgAdaptorPort = msgAdaptorPort;
	}
	
}
