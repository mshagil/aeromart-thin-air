package com.isa.thinair.msgbroker.core.util;

/**
 * Contains the constants
 */

public final class Constants {

	/** Denotes the System Parameter Keys */
	public static interface SystemParamKeys {
		public static final String USERID_MIN_LENGTH = "UIDMN";
		public static final String PASSWORD_MIN_LENGTH = "PWDMN";
		public static final String PASSWORD_MAX_LENGTH = "PWDMX";
		public static final String NO_OF_LOGIN_ATTEMPTS = "LOGAT";
		public static final String DEFAULT_LANGUAGE = "DFLNG";
		public static final String SESSION_TIMEOUT = "STOMS";
		public static final String SESSION_TIMEOUT_RESPONSE = "TORMS";
		public static final String SYSTEM_USER = "SYUSR";
		public static final String SYSTEM_ROLE = "SYRLE";
		public static final String UNDELIVERED_EMAIL_MESSAGE_SUBJECT_PREFIX = "UEMSP";
		public static final String AVS_MESSAGE_SENDING_DELAY_IN_MUNITES = "AMSD";
		public static final String GDS_MAIL_POLING_DELAY_IN_SECONDS = "GMPD";
		public static final String INBOUND_MESSAGE_HANDLING_DELAY_IN_SECONDS = "IMHD";
		public static final String OUTGOING_MESSAGE_HANDLING_DELAY_IN_SECONDS = "OMHD";
		public static final String CURRENT_SERVICE_TYPE = "SRVTP";
		public static final String LOCK_TIMEOUT_TIME_IN_MINUTES = "LKTOT";
	}

	/** Denotes the types of services provided */
	public static interface ServiceTypes {
		public static final String ReservationService = "RES";
		public static final String DepartureControlService = "DCS";
		public static final String SSM = "SSM";
		public static final String ASM = "ASM";
		public static final String AVS = "AVS";
		public static final String DVD = "DVD";
		public static final String RLR = "RLR";
		public static final String ASC = "ASC";
	}

	/** Denotes the device types */
	public static interface DeviceTypes {
		public static final String PC = "PC";
		// public static final String SCANNER = "SCANNER";
	}

	/** Denotes the device types */
	public static interface ApplicationAccessParams {
		public static final String WEB_TO_APPLICATION_SERVER_ACCESS_METHOD_KEY = "web.app.access.method";
		public static final String WEB_TO_APPLICATION_SERVER_LOCAL_ACCESS = "local";
		public static final String WEB_TO_APPLICATION_SERVER_REMOTE_ACCESS = "remote";
	}

	/** Denotes the configuration root paths */
	public static interface ConfigRootPath {
		public static final String DEFAULT_CONFIG_ROOT_PATH_WINDOWS = "C:/msgbrokerconfig";
		public static final String DEFAULT_CONFIG_ROOT_PATH_UNIX = "/home/ishan/apps/configs-G9/templates/email";
		// public static final String DEFAULT_CONFIG_REPORT_TEMPLATE = "/reporting/templates/";
		public static final String DEFAULT_CONFIG_ROOT_PATH_WINDOWS_KEY = "rootPathWindows";
		public static final String DEFAULT_CONFIG_ROOT_PATH_UNIX_KEY = "rootPathUnix";
		// public static final String REPORT_TEMPLATE_KEY = "reportTemplatePath";
		// public static final String FILE_IMPORT_PATH_WINDOWS_KEY = "fileImportPathWindows";
		// public static final String FILE_IMPORT_PATH_UNIX_KEY = "fileImportPathUnix";
		// public static final String USER_IMAGE_PATH_WINDOWS_KEY = "userImagePathWindows";
		// public static final String USER_IMAGE_PATH_UNIX_KEY = "userImagePathUnix";
	}

	/** Denotes ftp server configuration keys */
	public static interface FTPServerConfigurationKeys {
		public static final String FTP_SERVER_CONFIG_1 = "ftpserver_1";
	}

	/** Denotes ftp server configuration keys */
	public static interface FTPFolderNames {
		public static final String FILE_IMPORT_FOLDER = "fileImportFolder";
		public static final String USER_IMAGE_FOLDER = "userImageFolder";
	}

	/** Denotes the operating system */
	public static interface OperatingSystem {
		public static final String WINDOWS = "WINDOWS";
		public static final String UNIX = "UNIX";
	}

	/** Denotes the login parameters */
	public static interface LoginParameters {
		// public static final String BRS_CLIENT_LOGIN = "BRSClient";
		public static final String MSGBROKER_CLIENT_LOGIN = "MSGBrokerClient";
	}

	/** Denotes the error codes */
	public static interface ErrorCodes {
		public static final String MODULE_SAVECONSTRAINT_CHILDERROR = "module.saveconstraint.childerror";
		public static final String MODULE_DUPLICATE_KEY = "module.duplicate.key";
		public static final String MODULE_HIBERNATE_STALE_DATA = "module.hibernate.staledata";
	}

	public interface SitaTexConstants {
		int SITATEX_FILE_NAME_LENGTH = 10;

		String EXTENSION_SEND = ".SND" ;
		String EXTENSION_SENT = ".SNT" ;
		String EXTENSION_RECEIVE = ".RCV" ;

		String EXTENSION_TEMP = ".TMP" ;
	}

	public static interface ScheduleMessageEmailTemplates {

		public static final String SCHEDULE_PROCESS_FAILED_EMAIL = "schedule_message_process_failed_email";
	}
}