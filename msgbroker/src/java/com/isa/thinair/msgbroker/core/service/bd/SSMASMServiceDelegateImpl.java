/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.msgbroker.api.service.SSMASMServiceBD;

/**
 * @author Ishan
 */
@Remote
public interface SSMASMServiceDelegateImpl extends SSMASMServiceBD {

}
