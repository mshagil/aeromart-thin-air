package com.isa.thinair.msgbroker.core.bl;

import java.util.Date;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.msgbroker.api.dto.InMessageProcessResultsDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageFilterCriteria;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.PostMessageProcessor;
import com.isa.thinair.msgbroker.core.util.ServiceUtil;


@MessageDriven(name = "InboundMdb", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/inMessageQueueFilter"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })

public class InboundMdbMessageQueue implements MessageListener{


	private Log log = LogFactory.getLog(getClass());
	private MessageDAO messageDAO;

	public void onMessage(Message message) {
		
		
		log.info("*********InboundMdbMessageQueue*************");

		MsgBrokerModuleConfig config = MsgbrokerUtils.getMsgbrokerConfigs();
		ForceLoginInvoker.login(config.getEjbAccessUsername(), config.getEjbAccessPassword());


		ObjectMessage objMessage = (ObjectMessage) message;
		try {
			if (objMessage.getObject() instanceof Integer) {
				Integer messageId = (Integer) objMessage.getObject();
				InMessageProcessingStatus inMessageProcessingStatus = null;
				OutMessage outMessage = null;
				OutMessageProcessingStatus outMessageProcessingStatus = null;
				// define service type per message
				IBMsgProcessorBD ibMessageProcessor = null;
				ServiceUtil serviceUtil = new ServiceUtil();
				InMessage inMessage = getMessageDAO().getInMessage(messageId);
				if (inMessage.getStatusIndicator().equals(MessageProcessingConstants.InMessageProcessStatus.Unprocessed)
						&& serviceUtil.isMessageUnlockedOrLockTimedOut(inMessage)) {
					inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.LOCKED);
					inMessage.setLastLockedTime(new Date());
					getMessageDAO().saveInMessage(inMessage);

					if (!serviceUtil.isMessageTimedOut(inMessage)) {
						ibMessageProcessor = MessageBeanFactory.getIBMsgProcessor(inMessage.getMessageType());
						if (ibMessageProcessor != null) {
							InMessageProcessResultsDTO inMessageProcessResultsDTO = ibMessageProcessor.processAIRIMPMessage(inMessage);
							
							
							if (inMessageProcessResultsDTO != null) {
								if (inMessageProcessResultsDTO.getInMessage() != null) {
									inMessage = inMessageProcessResultsDTO.getInMessage();
								}
								if (inMessageProcessResultsDTO.getInMessageProcessingStatus() != null) {
									inMessageProcessingStatus = inMessageProcessResultsDTO.getInMessageProcessingStatus();
									inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
									getMessageDAO().saveInMessageProcessingStatus(inMessageProcessingStatus);
								}
								if (inMessageProcessResultsDTO.getOutMessage() != null) {
									outMessage = inMessageProcessResultsDTO.getOutMessage();
									MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
									if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
										outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
										outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
										getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
									}
								}
							} else {
								log.error("No Implementation for the message type " + inMessage != null ? inMessage.getMessageType()
										: "null");
							}
						} else {
							log.error("No Implementation for the message type " + inMessage != null ? inMessage.getMessageType()
									: "null");
						}
					} else {
						// The request timed out, record the error message.
						InMessageProcessingStatus inMessagePassingErrors = new InMessageProcessingStatus();
						inMessagePassingErrors.setInMessageID(inMessage.getInMessageID());
						inMessagePassingErrors
								.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.REQUEST_TIMED_OUT);
						inMessagePassingErrors.setProcessingDate(new Date());
						inMessagePassingErrors.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.TimedOut);
						getMessageDAO().saveInMessageProcessingStatus(inMessagePassingErrors);

						inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.TimedOut);
						inMessage.setProcessedDate(new Date());
						inMessage
								.setLastErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.REQUEST_TIMED_OUT);
					}
					inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.UNLOCKED);
					
					getMessageDAO().saveInMessage(inMessage);
					log.info("End processing msg "+ inMessage.getGdsMessageID());
					
					if (AppSysParamsUtil.isEnableMessageQueuingForSsmAsm()
							&& MessageFilterCriteria.getMessageTypesToFilter().contains(inMessage.getMessageType())) {

						try{
							PostMessageProcessor postProcessor = new PostMessageProcessor();
							postProcessor.postMessageProcess(inMessage);
						}catch(Exception ex){
							log.error("Error message queue " + ex);
						}

					}
				}
			}
		} catch (JMSException e) {
			log.error("JMS Exception", e);
		} catch (ModuleException ex) {
			log.error("Error in Saving to Database", ex);
		}
	}

	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}

}
