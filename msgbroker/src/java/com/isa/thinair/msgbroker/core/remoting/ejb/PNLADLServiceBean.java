package com.isa.thinair.msgbroker.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRS;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airreservation.api.dto.adl.ADLDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.msgbroker.api.dto.PNLADLResponseDTO;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.bl.ADLComposer;
import com.isa.thinair.msgbroker.core.bl.WSADLComposer;
import com.isa.thinair.msgbroker.core.service.bd.PNLADLServiceDelegateImpl;
import com.isa.thinair.msgbroker.core.service.bd.PNLADLServiceLocalDelegateImpl;

/**
 * @author Ishan
 */
@Stateless
@RemoteBinding(jndiBinding = "PNLADLService.remote")
@LocalBinding(jndiBinding = "PNLADLService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class PNLADLServiceBean extends PlatformBaseSessionBean implements PNLADLServiceDelegateImpl,
		PNLADLServiceLocalDelegateImpl {

	private static Log log = LogFactory.getLog(PNLADLServiceBean.class);

	// TODO sending mails through msgbroker
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public PNLADLResponseDTO sendADL(ADLDTO adlElements) throws ModuleException {
		try {
			//ADLComposer adlComposer = new ADLComposer();
			// For the moment sending email through previous implemenation. need to change and send through msgbroker
			PNLADLResponseDTO response = new PNLADLResponseDTO();//adlComposer.composeADL(adlElements);
			
			try {
				if (AppSysParamsUtil.isDCSConnectivityEnabled()) {
					WSADLComposer wsAdlComposer = new WSADLComposer();
					DCSADLInfoRQ dcsadlInfoRQ = wsAdlComposer.composeADL(adlElements);
					if (dcsadlInfoRQ != null) {
						DCSADLInfoRS dcsadlInfoRS = ReservationModuleUtils.getDCSClientBD().sendADLToDCS(dcsadlInfoRQ);
						if (dcsadlInfoRS.getResponseAttributes().getSuccess() != null) {
							response.setWsAdlSuccess(true);
						}
					}
				}
			} catch (Exception e) {e.initCause(e);e.printStackTrace();
				// sending PNL/ADL to DCS failed
				log.error("######### Error sendADLToDCS"+e.getMessage());
			}
			
			return response;

		}  catch (CommonsDataAccessException daex) {
			log.error("sendADL ", daex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(daex, daex.getExceptionCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean sendADLToDcs(ADLDTO adlElements) throws ModuleException {
		try {
			ADLComposer adlComposer = new ADLComposer();
			boolean isInvokedSuccess = false;
			try {
				if (AppSysParamsUtil.isDCSConnectivityEnabled()) {
					WSADLComposer wsAdlComposer = new WSADLComposer();
					DCSADLInfoRQ dcsadlInfoRQ = wsAdlComposer.composeADL(adlElements);
					if (dcsadlInfoRQ != null) {
						DCSADLInfoRS dcsadlInfoRS = ReservationModuleUtils.getDCSClientBD().sendADLToDCS(dcsadlInfoRQ);
						if (dcsadlInfoRS.getResponseAttributes().getSuccess() != null) {
							isInvokedSuccess = true;
						}
					}
				}
			} catch (Exception e) {
				// sending PNL/ADL to DCS failed
				log.error("######### Error sendADLToDCS");
			}
			
			return isInvokedSuccess;

		} catch (CommonsDataAccessException daex) {
			log.error("sendADL ", daex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(daex, daex.getExceptionCode());
		}
	}

}
