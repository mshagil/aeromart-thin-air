package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.msgbroker.api.service.ETSTicketVoidServiceBD;

@Local
public interface ETSTicketVoidServiceBeanLocal extends ETSTicketVoidServiceBD {

}
