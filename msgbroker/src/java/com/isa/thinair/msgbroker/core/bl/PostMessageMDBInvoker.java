package com.isa.thinair.msgbroker.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;

/**
 * 
 * @author rajitha
 *
 */
public class PostMessageMDBInvoker extends PlatformBaseServiceDelegate{
	
	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME_IN_MSG_PROCESSER = "queue/inMessageQueueFilter";

	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	public void invokeInboundMessageProcessorMDB(Integer messageId) {
		try {
			sendMessage(messageId, ReservationModuleUtils.getAirReservationConfig().getJndiProperties(),
					DESTINATION_JNDI_NAME_IN_MSG_PROCESSER,
					CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}


}
