package com.isa.thinair.msgbroker.core.bl;

import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.msgbroker.api.dto.ASMessegeDTO;
import com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action;
import com.isa.thinair.msgbroker.api.dto.SSIFlightLegDTO;
import com.isa.thinair.msgbroker.core.util.ASMDataElementUseage;
import com.isa.thinair.msgbroker.core.util.ASMSubMessageFormat;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.ASMDataElement;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.ASMSubMessageType;
import com.isa.thinair.msgbroker.core.util.MessageComposerUtil;

/**
 * 
 * @author Thejaka
 * 
 */
public class ASMComposer {
	private static Log logger = LogFactory.getLog(ASMComposer.class);
	private List<String> asmElements;
	private ASMDataElementUseage currentASMDataElementUseage;
	private SimpleDateFormat sdfDDMMMYY = new SimpleDateFormat("ddMMMyy");
	private SimpleDateFormat sdfDD = new SimpleDateFormat("dd");
	Calendar cal = Calendar.getInstance();;
	Iterator iterSSIFlightLegDTO;
	private boolean isSkipHeader = false;

	private List<String> generateMessageElements(ASMessegeDTO asMessegeDTO, ASMSubMessageType asmSubMessageType)
			throws ModuleException {
		asmElements = new ArrayList<String>();
		ASMSubMessageFormat asmSubMessageFormat = asmSubMessageType.getASMSubMessageFormat();
		currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(ASMDataElement.BEGIN_ASM);
		while (!currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.END_ASM)) {

			// MESSAGE_HEADING
			generateMessageHeadingElements(asMessegeDTO, asmSubMessageFormat);

			// MESSAGE_REFERENCE
			generateMessageReferenceElements(asMessegeDTO, asmSubMessageFormat);

			// ACTION_INFORMATION
			generateActionInformationElements(asMessegeDTO, asmSubMessageFormat);

			// FLIGHT_INFORMATION
			generateFlightInformationElements(asMessegeDTO, asmSubMessageFormat);

			// EQUIPMENT_INFORMATION
			generateEquipmentInformationElements(asMessegeDTO, asmSubMessageFormat);

			// LEG_INFORMATION
			generateLegInformationElements(asMessegeDTO, asmSubMessageFormat);			
			
			// SEGMENT_INFORMATION
			generateSegmentInformationElements(asMessegeDTO, asmSubMessageFormat);

			// COMMENTS
			generateCommentElements(asMessegeDTO, asmSubMessageFormat);

			// SUPPLEMENTARY_INFORMATIONS
			generateSupplementaryInformationElements(asMessegeDTO, asmSubMessageFormat);

		}
		return asmElements;
	}

	private void generateMessageHeadingElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat) {
		// MESSAGE_HEADING
		List<String> asmElements = new ArrayList<String>();
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.MESSAGE_HEADING_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER)
				&& asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage)) {
			asmElements.add("ASM");
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL)) {
			asmElements.add(MessageComposerConstants.EOL);
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.TIME_MODE)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			asmElements.add(MessageComposerUtil.getMessagingTimeMode(asMessegeDTO.getTimeMode()));
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.TIME_MODE_EOL)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			asmElements.add(MessageComposerConstants.EOL);
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		
		if (!isSkipHeader) {
			this.asmElements.addAll(asmElements);
		}
	}

	private void generateMessageReferenceElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat)
			throws ModuleException {
		// MESSAGE_REFERENCE
		List<String> asmElements = new ArrayList<String>();
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.MESSAGE_REFERENCE_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.MESSAGE_SEQUENCE_REFERENCE)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			if (asMessegeDTO.getReferenceNumber() == null) {
				if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
					throw new ModuleException("reservation.asm.composer.messagesequencereference.invalid.null");
			} else {
				asmElements.add(asMessegeDTO.getReferenceNumber().toUpperCase());
				currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
			}
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.MESSAGE_REFERENCE_EOL)) {
			asmElements.add(MessageComposerConstants.EOL);
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		
		if (!isSkipHeader) {
			this.asmElements.addAll(asmElements);
		}
	}

	private void generateActionInformationElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat)
			throws ModuleException {
		// ACTION_INFORMATION
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.ACTION_INFORMATION_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.ACTION_IDENTIFIER)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			if (asMessegeDTO.getAction() == null) {
				if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
					throw new ModuleException("reservation.asm.composer.actionidentifier.invalid.null");
			} else {
				asmElements.add(asMessegeDTO.getAction().toString());
				currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
			}
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.ACTION_INFORMATION_EOL)) {
			asmElements.add(MessageComposerConstants.EOL);
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
	}

	private void generateFlightInformationElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat)
			throws ModuleException {
		// FLIGHT_INFORMATION
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.FLIGHT_INFORMATION_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.FLIGHT_IDENTIFIER)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			if (asMessegeDTO.getFlightIdentifier() == null || asMessegeDTO.getFlightIdentifier().trim().isEmpty()) {
				if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
					throw new ModuleException("reservation.asm.composer.flightidentifier.invalid.null");
			} else {
				asmElements.add(asMessegeDTO.getFlightIdentifier().toUpperCase());
				currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
			}
		}
		// NEW_FLIGHT_INFORMATION for FLT
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.NEW_FLIGHT_IDENTIFIER)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			if (asMessegeDTO.getNewFlightIdentifier() == null || asMessegeDTO.getNewFlightIdentifier().trim().isEmpty()) {
				if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
					throw new ModuleException("reservation.asm.composer.flightidentifier.invalid.null");
			} else {
				asmElements.add(MessageComposerConstants.SP);
				asmElements.add(asMessegeDTO.getNewFlightIdentifier().toUpperCase());
				currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
			}
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.FLIGHT_INFORMATION_EOL)) {
			asmElements.add(MessageComposerConstants.EOL);
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
	}

	private void generateEquipmentInformationElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat)
			throws ModuleException {
		// EQUIPMENT_INFORMATION
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.EQUIPMENT_INFORMATION_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SERVICE_TYPE)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			if (asMessegeDTO.getServiceType() == null || asMessegeDTO.getServiceType().trim().isEmpty()) {
				if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
					throw new ModuleException("reservation.asm.composer.servicetype.invalid.null");
			} else {
				asmElements.add(asMessegeDTO.getServiceType().toUpperCase());
				currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
			}
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.AIRCRAFT_TYPE)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			if (asMessegeDTO.getAircraftType() == null || asMessegeDTO.getAircraftType().trim().isEmpty()) {
				if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
					throw new ModuleException("reservation.asm.composer.aircrafttype.invalid.null");
			} else {
				asmElements.add(" " + asMessegeDTO.getAircraftType().toUpperCase());
				currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
			}
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			if (asMessegeDTO.getPassengerResBookingDesignator() == null
					|| asMessegeDTO.getPassengerResBookingDesignator().trim().isEmpty()) {
				if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
					throw new ModuleException("reservation.asm.composer.passengerreservationbookingdesignator.invalid.null");
			} else {
				asmElements.add(" " + asMessegeDTO.getPassengerResBookingDesignator().toUpperCase());
				currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
			}
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.EQUIPMENT_INFORMATION_EOL)) {
			asmElements.add(MessageComposerConstants.EOL);
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
	}

	private void generateLegInformationElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat)
			throws ModuleException {
		// LEG_INFORMATION
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.LEG_INFORMATION_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
			// Iterate for all available legs
			iterSSIFlightLegDTO = asMessegeDTO.getLegs().iterator();
			while (iterSSIFlightLegDTO.hasNext()) {
				SSIFlightLegDTO ssiFlightLegDTO = (SSIFlightLegDTO) iterSSIFlightLegDTO.next();
				if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.LEG_INFORMATION_EOL)) {
					asmElements.add(MessageComposerConstants.EOL);
					currentASMDataElementUseage = asmSubMessageFormat
							.getNextASMDataElementUsage(ASMDataElement.LEG_INFORMATION_BOL);
				}
				if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.DEPARTURE_AIRPORT)
						&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
					if (ssiFlightLegDTO.getDepatureAirport() == null || ssiFlightLegDTO.getDepatureAirport().trim().isEmpty()) {
						if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
							throw new ModuleException("reservation.asm.composer.departureairport.invalid.null");
					} else {
						asmElements.add(ssiFlightLegDTO.getDepatureAirport().toUpperCase());
						currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
					}
				}
				if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.DEPATURE_TIME)
						&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
					if (ssiFlightLegDTO.getDepatureTime() == null || ssiFlightLegDTO.getDepatureTime().trim().isEmpty()) {
						if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
							throw new ModuleException("reservation.asm.composer.departuretime.invalid.null");
					} else {
						try {
							if (ssiFlightLegDTO.getDepatureOffiset() != 0) {
								cal.setTime(sdfDDMMMYY.parse((asMessegeDTO.getFlightIdentifier().split("/"))[1]));
								cal.add(Calendar.DATE, ssiFlightLegDTO.getDepatureOffiset());
								asmElements.add(sdfDD.format(cal.getTime()).toUpperCase());
							}
						} catch (ParseException e) {
							throw new ModuleException("reservation.asm.composer.departuretime.invalid.null");
						}
						asmElements.add(ssiFlightLegDTO.getDepatureTime().toUpperCase());
						currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
					}
				}
				if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.ARRIVAL_AIRPORT)
						&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
					if (ssiFlightLegDTO.getArrivalAirport() == null || ssiFlightLegDTO.getArrivalAirport().trim().isEmpty()) {
						if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
							throw new ModuleException("reservation.asm.composer.arrivalairport.invalid.null");
					} else {
						asmElements.add(" " + ssiFlightLegDTO.getArrivalAirport().toUpperCase());
						currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
					}
				}
				if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.ARRIVAL_TIME)
						&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
					if (ssiFlightLegDTO.getArrivalTime() == null || ssiFlightLegDTO.getArrivalTime().trim().isEmpty()) {
						if (asmSubMessageFormat.isMandatoryDataElement(currentASMDataElementUseage))
							throw new ModuleException("reservation.asm.composer.arrivaltime.invalid.null");
					} else {
						try {
							if (ssiFlightLegDTO.getArrivalOffset() != 0) {
								cal.setTime(sdfDDMMMYY.parse((asMessegeDTO.getFlightIdentifier().split("/"))[1]));
								cal.add(Calendar.DATE, ssiFlightLegDTO.getArrivalOffset());
								asmElements.add(sdfDD.format(cal.getTime()).toUpperCase());
							}
						} catch (ParseException e) {
							throw new ModuleException("reservation.asm.composer.arrivaltime.error");
						}
						asmElements.add(ssiFlightLegDTO.getArrivalTime().toUpperCase());
						currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
					}
				}
			}
		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.LEG_INFORMATION_EOL)) {
			asmElements.add(MessageComposerConstants.EOL);
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
	}
	
	private void generateSegmentInformationElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat)
			throws ModuleException {
		// SEGMENT_INFORMATION
		boolean isExternalFltFound = false;
		boolean isCSCarrier = asMessegeDTO.isCodeShareCarrier();
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SEGMENT_INFORMATION_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);

			// Iterate for all external flights
			if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.OTHER_SEGMENT_INFO)
					&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
				currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
				List<String> externalFlightNos = asMessegeDTO.getExternalFlightNos();
				if (externalFlightNos != null && !externalFlightNos.isEmpty()) {
					Iterator<String> externalFlightNoItr = externalFlightNos.iterator();
					StringBuilder cfInfo = new StringBuilder();
					while (externalFlightNoItr.hasNext()) {
						String externalFlightNo = externalFlightNoItr.next();
						if (externalFlightNo != null && !"".equals(externalFlightNo.trim())) {
							cfInfo.append("/" + externalFlightNo.toUpperCase());
						}
					}

					if (isCSCarrier && !StringUtil.isNullOrEmpty(cfInfo.toString())
							&& !StringUtil.isNullOrEmpty(asMessegeDTO.getSegmentCode())) {
						asmElements.add(asMessegeDTO.getSegmentCode());			
						asmElements.add(MessageComposerConstants.SP);
						asmElements.add(MessageComposerConstants.OTHER_SEG_INFO);
						asmElements.add(cfInfo.toString().toUpperCase());
						isExternalFltFound = true;
					}
				}

			}

		}
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SEGMENT_INFORMATION_EOL)) {
			if (isExternalFltFound) {
				asmElements.add(MessageComposerConstants.EOL);
			}
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
	}
	
	private void generateCommentElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat)
			throws ModuleException {
		List<String> tempElements = new ArrayList<String>();
		boolean isCommentElementAvailable = false;
		// COMMENTS
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.COMMENTS_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);

			if (asMessegeDTO.getLegs() != null && !asMessegeDTO.getLegs().isEmpty()) {
				// Iterate for all available legs
				iterSSIFlightLegDTO = asMessegeDTO.getLegs().iterator();
				while (iterSSIFlightLegDTO.hasNext()) {
					SSIFlightLegDTO ssiFlightLegDTO = (SSIFlightLegDTO) iterSSIFlightLegDTO.next();

					if (ssiFlightLegDTO.isEticketCandidate()
							&& !(asMessegeDTO.getAction() == Action.CNL || asMessegeDTO.getAction() == Action.TIM)) {
						tempElements.add(" ");
						tempElements.add(MessageComposerConstants.MsgDataElementIdentifier.E_TICKETING);
						tempElements.add("/");
						tempElements.add(MessageComposerConstants.MsgConstants.E_TICKET_CANDIDATE);
					}

					if (!tempElements.isEmpty()) {
						isCommentElementAvailable = true;
						if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.COMMENTS_EOL)) {
							asmElements.add(MessageComposerConstants.EOL);
							currentASMDataElementUseage = asmSubMessageFormat
									.getNextASMDataElementUsage(ASMDataElement.COMMENTS_BOL);
						}

						if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.COMMENTS)
								&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
							asmElements.add(ssiFlightLegDTO.getDepatureAirport().toUpperCase());
							asmElements.add(ssiFlightLegDTO.getArrivalAirport().toUpperCase());

							asmElements.addAll(tempElements);

							currentASMDataElementUseage = asmSubMessageFormat
									.getNextASMDataElementUsage(currentASMDataElementUseage);
						}
					}
					tempElements.clear();
				}
			}
		}

		// TODO: temp fix above populating logic should be revisited
		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.COMMENTS)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}

		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.COMMENTS_EOL)) {
			if (isCommentElementAvailable)
				asmElements.add(MessageComposerConstants.EOL);

			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
	}

	public String composeASMessage(ASMessegeDTO asMessegeDTO, boolean includeAddressInMsgBody) throws ModuleException {

		ASMSubMessageType asmSubMessageType = MessageComposerUtil.getASMSubMessageType(asMessegeDTO.getAction());
		String outMessage = null;

		List<String> asmElements = generateMessageElements(asMessegeDTO, asmSubMessageType);

		if (!asmElements.isEmpty()) {

			try {
				String templateName = "email/" + TypeBMessageConstants.MessageIdentifiers.ASM
						+ MessageComposerConstants.TEMPLATE_SUFFIX;
				StringWriter writer = new StringWriter();
				HashMap<String, Object> printDataMap = new HashMap<String, Object>();
				if (includeAddressInMsgBody) {
					printDataMap.put("res", asMessegeDTO.getRecipientAddress());
					printDataMap.put("sen", asMessegeDTO.getSenderAddress());
					SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
			        Date dt = new Date();
			        String timestamp = sdf.format(dt).toUpperCase();
					printDataMap.put("timestamp", timestamp);
				}
				printDataMap.put("asmElements", asmElements.iterator());
				new TemplateEngine().writeTemplate(printDataMap, templateName, writer);
				outMessage = writer.toString();

			} catch (Exception e) {
				if (logger.isDebugEnabled()) {
					logger.debug("gdsmessagecomposer.asmessage.error\n");
				}
				logger.info("gdsmessagecomposer.asmessage.error\n" + e.getMessage());
				throw new ModuleException("gdsmessagecomposer.asmessage.error");
			}
			
		} else {

			if (logger.isDebugEnabled()) {
				logger.debug("gdsmessagecomposer.asmessage.noelements\n");
			}
			logger.info("gdsmessagecomposer.asmessage.noelements\n");
			throw new ModuleException("gdsmessagecomposer.asmessage.noelementsr");
		}
		logger.info(outMessage);
		return outMessage;
	}

	private void generateSupplementaryInformationElements(ASMessegeDTO asMessegeDTO, ASMSubMessageFormat asmSubMessageFormat) {

		boolean isSupplementaryInfoFound = false;

		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SUB­MESSAGE_SEPARATION_BOL)
				&& asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}

		if (!StringUtil.isNullOrEmpty(asMessegeDTO.getSupplementaryInfo())) {
			isSupplementaryInfoFound = true;
		}

		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SUB­MESSAGE_SEPARATION)) {
			if (isSupplementaryInfoFound)
				asmElements.add("//");

			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}

		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SUB­MESSAGE_SEPARATION_EOL)) {
			if (isSupplementaryInfoFound)
				asmElements.add(MessageComposerConstants.EOL);

			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}

		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}

		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SUPPLEMENTARY_INFORMATION)
				&& !asmSubMessageFormat.isNotApplicableDataElement(currentASMDataElementUseage)) {
			if (isSupplementaryInfoFound) {
				asmElements.add(MessageComposerConstants.SupplementaryInformation.SUPPLEMENTARY_INFORMATION_INDICATOR);
				asmElements.add(MessageComposerConstants.SP);
				asmElements.add(asMessegeDTO.getSupplementaryInfo().toUpperCase());
			}
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}

		if (currentASMDataElementUseage.getSsmDataElement().equals(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL)) {
			currentASMDataElementUseage = asmSubMessageFormat.getNextASMDataElementUsage(currentASMDataElementUseage);
		}
	}
	
	public String composeASMSubMessages(Map<Integer, ASMessegeDTO> asmMessegeDTOMap, boolean includeAddressInMsgBody,
			Collection bookingClassList, String recipientAddress, String senderAddress, boolean isCodeShareCarrier) throws ModuleException {

		String outMessage = null;
		StringBuilder message = new StringBuilder();
		if (asmMessegeDTOMap != null && !asmMessegeDTOMap.isEmpty()) {
			boolean isFirstMessage = true;

			for (Integer key : asmMessegeDTOMap.keySet()) {
				ASMessegeDTO asMessegeDTO = asmMessegeDTOMap.get(key);

				if (bookingClassList != null && !bookingClassList.isEmpty()) {
					String rbds = "";

					Iterator itRbd = bookingClassList.iterator();
					while (itRbd.hasNext()) {
						String code = (String) itRbd.next();
						rbds += code;
					}

					asMessegeDTO.setPassengerResBookingDesignator(rbds);
				}
				asMessegeDTO.setCodeShareCarrier(isCodeShareCarrier);
				ASMSubMessageType asmSubMessageType = MessageComposerUtil.getASMSubMessageType(asMessegeDTO.getAction());

				List<String> asmElements = generateMessageElements(asMessegeDTO, asmSubMessageType);

				if (!asmElements.isEmpty()) {

					try {
						String templateName = "email/" + TypeBMessageConstants.MessageIdentifiers.ASM
								+ MessageComposerConstants.TEMPLATE_SUFFIX;
						StringWriter writer = new StringWriter();
						HashMap<String, Object> printDataMap = new HashMap<String, Object>();
						if (includeAddressInMsgBody && isFirstMessage) {
							printDataMap.put("res", recipientAddress);
							printDataMap.put("sen", senderAddress);
							SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
							Date dt = new Date();
							String timestamp = sdf.format(dt).toUpperCase();
							printDataMap.put("timestamp", timestamp);
						}
						printDataMap.put("asmElements", asmElements.iterator());
						new TemplateEngine().writeTemplate(printDataMap, templateName, writer);

						if (!isFirstMessage)
							message.append("\n//\n");

						message.append(writer.toString());

						isFirstMessage = false;
						this.isSkipHeader = true;

					} catch (Exception e) {
						if (logger.isDebugEnabled()) {
							logger.debug("gdsmessagecomposer.asmessage.error\n");
						}
						logger.info("gdsmessagecomposer.asmessage.error\n" + e.getMessage());
						throw new ModuleException("gdsmessagecomposer.asmessage.error");
					}

				} else {

					if (logger.isDebugEnabled()) {
						logger.debug("gdsmessagecomposer.asmessage.noelements\n");
					}
					logger.info("gdsmessagecomposer.asmessage.noelements\n");

				}

			}

			outMessage = message.toString();

			if (!StringUtil.isNullOrEmpty(outMessage)) {
				outMessage = outMessage.replaceAll("[\n\r]+", "\n");
				outMessage = outMessage.replaceAll("\n[ \t]*\n", "\n");
			}
		}

		logger.info(outMessage);
		return outMessage;
	}

	public static void main(String[] args) {

		ASMComposer composer = new ASMComposer();

		ASMessegeDTO asMessegeDTO = new ASMessegeDTO();
		// /*
		// * ASM
		// 30SEP00002E003
		// CNL
		// G9525/05NOV08
		// */
		// asMessegeDTO.setAction(com.isa.thinair.msgbroker.reservation.api.dto.ASMessegeDTO.Action.EQT);
		// asMessegeDTO.setReferenceNumber("30SEP00005E004");
		// asMessegeDTO.setFlightIdentifier("G9525/05NOV08");
		// asMessegeDTO.setServiceType("G");
		// asMessegeDTO.setAircraftType("320");
		// asMessegeDTO.setPassengerResBookingDesignator("N");
		asMessegeDTO.setAction(com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action.NEW);
		asMessegeDTO.setReferenceNumber("30SEP00005E004");
		asMessegeDTO.setFlightIdentifier("G97777/25Dec08");
		asMessegeDTO.setServiceType("G");
		asMessegeDTO.setAircraftType("320");
		asMessegeDTO.setPassengerResBookingDesignator("4RDAFGDSIJMINEOQG9SESTAXWZ");
		SSIFlightLegDTO leg = new SSIFlightLegDTO();
		leg.setDepatureAirport("SHJ");
		leg.setDepatureTime("1000");
		leg.setArrivalAirport("MCT");
		leg.setArrivalTime("1100");
		asMessegeDTO.addLeg(leg);
		
		List<String> externalFlightNos = new ArrayList<String>();
		externalFlightNos.add(new String("UF1123"));
		externalFlightNos.add(new String("MH0023"));
		
		asMessegeDTO.setExternalFlightNos(externalFlightNos);
		/* ASM Check */
		try {
			String composedText = composer.composeASMessage(asMessegeDTO,true);
			logger.info(composedText);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
