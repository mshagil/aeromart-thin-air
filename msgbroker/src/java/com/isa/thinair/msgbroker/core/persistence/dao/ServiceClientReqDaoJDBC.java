package com.isa.thinair.msgbroker.core.persistence.dao;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.commons.api.dto.ets.InterchangeHeader;

public interface ServiceClientReqDaoJDBC {
	
	public String getTTYForServiceClientReqType(String serviceClientCode, String requestType);
	
	public String getServiceClientForReqTypeTTY(String tty, String requestType);

	public void clearInMsgProcessingQueue();
	
	public  Collection<Integer> retrieveLockedQueueIds();
	
	public Map<String,String> getInterchangeHeadderInfo(String gdsCode);

}
