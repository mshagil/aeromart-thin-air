package com.isa.thinair.msgbroker.core.bl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.MessageTypes;
import com.isa.thinair.messaging.api.util.MessagingCustomConstants;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.ExternalSitaConfig;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.dto.SitaTexElement;
import com.isa.thinair.msgbroker.core.dto.SitaTexMessage;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.MessageRequestProcessingUtil;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.webplatform.api.util.IoUtil;

public class GDSMessageGatewayFileImpl extends GdsTypeBMessageGateway {

	private boolean hasNewMessages = false;
	private String PFS = "PFS";
	private String ETL = "ETL";
	private String PRL = "PRL";

	Log log = LogFactory.getLog(GDSMessageGatewayFileImpl.class);

	@SuppressWarnings("unchecked")
	public void sendMessages(List<String> messageTypes, boolean useGDSConfiguration) {
		
		log.info("GDSMessageGatewayFileImpl : sending typeB messages files started");
		String outLocation = "";
		SitaTexMessage message;
		SitaTexMessage.MessageHeader header;
		SitaTexMessage.MessageBody body;
		SitaTexMessage.MessageHeader.SitaTexDestination destination;

		OutMessageProcessingStatus outMessageProcessingStatus;
		List<Integer> outMessageIds = new ArrayList<Integer>();
		Set<String> extRecipientAddresses = new HashSet<String>();
		ExternalSitaConfig sitaConfig = null;

		if (useGDSConfiguration) {
			List<String> gdsCodes = new ArrayList<String>();
			for (ServiceBearer serviceBearer : applicableServiceBearers) {
				gdsCodes.add(serviceBearer.getGdsCode());
			}
			outMessageIds = (List<Integer>) messageDAO.getOutMessageIdsInFifoOrderForGds(
					MessageProcessingConstants.OutMessageProcessStatus.Untransmitted, messageTypes, gdsCodes);
		} else {
			outMessageIds = (List<Integer>) messageDAO.getOutMessageIdsInFifoOrder(
					MessageProcessingConstants.OutMessageProcessStatus.Untransmitted, messageTypes);
		}
		for (Integer outMessageId : outMessageIds) {
			OutMessage outMessage = messageDAO.getOutMessage(outMessageId);
			outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Processing);
			outMessage.setProcessedDate(new Date());
			boolean isSuccess = false;
			ServiceBearer serviceBearer = null;
			
			try {
				MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);				
				
				if (outMessage.isNonGDSMessage()
						&& (outMessage.getRecipientTTYAddress() == null || ("").equals(outMessage.getRecipientTTYAddress()))) {
					continue;
				}			
				isSuccess = false;
				if (useGDSConfiguration) {
					// TODO -- cache this
					if (!outMessage.isNonGDSMessage()) {
						serviceBearer = adminDAO.getServiceBearerAirlineForTTYAddress(outMessage.getSentTTYAddress());
						if (serviceBearer == null) {
							serviceBearer = adminDAO.getServiceBearerAirlineForEmailAddress(outMessage.getSentEmailAddress());
						} else {
							outLocation = serviceBearer.getMessageOutLocation();
						}
					} else {
						sitaConfig = (ExternalSitaConfig) ((MessagingModuleConfig) LookupServiceFactory
								.getInstance().getModuleConfig(MessagingConstants.MODULE_NAME))
								.getExternalSitaLocationConfigurationMap().get(MessagingCustomConstants.TEST_EXT_SITA);
						outLocation = sitaConfig.getSitaLocation();
						extRecipientAddresses.add(outMessage.getRecipientTTYAddress());
					}
				} else {
					MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance()
							.getModuleConfig(MsgbrokerConstants.MODULE_NAME);
					outLocation = moduleConfig.getPnlAdlOutLocation();
				}

				message = new SitaTexMessage();
				header = new SitaTexMessage.MessageHeader();
				header.setMessageType(SitaTexElement.SitaTexHeader.SENT);
				header.setDate(new Date());

				destination = new SitaTexMessage.MessageHeader.SitaTexDestination();
				destination.setAddressType(SitaTexElement.SitaTexDestinations.NATIVE);
				destination.setAddress(outMessage.getRecipientTTYAddress());
				header.getDestinations().add(destination);

				header.setAirlineDesignator(AppSysParamsUtil.getDefaultCarrierCode());
				if (serviceBearer != null) {
					header.setOriginAddress(serviceBearer.getSitaAddress());
				} else {
					header.setOriginAddress(outMessage.getSentTTYAddress());
				}
				header.setMessageId(String.valueOf(outMessage.getOutMessageID()));

				body = new SitaTexMessage.MessageBody();
				body.setMessageText(outMessage.getOutMessageText());
				message.setMessageBody(body);
				message.setMessageHeader(header);

				String fileName = String.format("%0" + Constants.SitaTexConstants.SITATEX_FILE_NAME_LENGTH + "d", outMessage.getOutMessageID());

				if (useGDSConfiguration) {
					if (serviceBearer != null) {
						writeFile(outLocation, fileName, message, SitaTexMessage.MessageType.valueOf(serviceBearer.getMessageType()));
					} else {
						if (sitaConfig != null) {
							writeFile(outLocation, fileName, message, SitaTexMessage.MessageType.valueOf(sitaConfig.getMsgType()));
						}
					}
				} else {
					writeFile(outLocation, fileName, message, SitaTexMessage.MessageType.SITA_TEX);
				}
				isSuccess = true;
			} catch (Exception e) {
				log.error("Error in sending GDS Messge, File writing failed", e);
				// Dont throw so that remaining messages will be sent
			}

			/**
			 * Have two catch blocks becoz file writing failure also should update the transmission status as failed
			 * Then just in case message saving fails, we should make sure other messeges are sent
			 **/

			try {
				outMessage.setStatusIndicator(isSuccess
						? MessageProcessingConstants.OutMessageProcessStatus.Sent
						: MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure);
				outMessage.setProcessedDate(new Date());

				if (serviceBearer != null) {
					outMessage.setSentTTYAddress(serviceBearer.getSitaAddress());
					outMessage.setSentEmailAddress(serviceBearer.getEmailAddress());
				}
				messageDAO.saveOutMessage(outMessage);

				outMessageProcessingStatus = new OutMessageProcessingStatus();
				outMessageProcessingStatus
						.setErrorMessageCode(MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.MESSAGE_ACCEPTEDBYMAILSERVER);
				outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
				outMessageProcessingStatus.setProcessingDate(new Date());
				outMessageProcessingStatus
						.setProcessingStatus(isSuccess ? MessageProcessingConstants.OutMessageProcessStatus.Sent
								: MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure);
				messageDAO.saveOutMessageProcessingStatus(outMessageProcessingStatus);
			} catch (Exception e) {
				log.error("Error in saving GDS message status", e);
				// Dont throw so that remaining messages will be sent
			}
		}
	}

	public void receiveMessages(boolean usedGDSConfiguration, Collection<Integer> unprocessedMsgIds) {

		log.info("GDSMessageGatewayFileImpl : receiving typeB messages files started");

		String etlPrlInLocation;
		String pfsInLocation;
		File etlPrlLocation;
		File pfsLocation;
		File[] etlPrlInFiles;
		File[] pfsInFiles;
		String rawMessage;
		SitaTexMessage sitaTexMessage;

		if (usedGDSConfiguration) {
			for (ServiceBearer serviceBearer : applicableServiceBearers) {
				etlPrlInLocation = serviceBearer.getMessageInLocation();

				etlPrlLocation = new File(etlPrlInLocation);
				etlPrlInFiles = etlPrlLocation.listFiles();

				if (etlPrlInFiles != null) {
					for (File file : etlPrlInFiles) {
						if (file.getName().endsWith(Constants.SitaTexConstants.EXTENSION_RECEIVE)) {
							log.info("Reading typeB RCV message file: " + file.getName());
							try {
								rawMessage = IoUtil.readContent(file, SitaTexElement.EOL);
								sitaTexMessage = new SitaTexMessage(rawMessage, SitaTexMessage.MessageType.valueOf(serviceBearer.getMessageType()));
								if ((!sitaTexMessage.isParseFailed()) && (!sitaTexMessage.isAcknowledgement())) {
									log.info("Saving typeB message file: " + file.getName());
									saveMessage(sitaTexMessage, serviceBearer);
								} else {
									handleFailure(sitaTexMessage);
								}
							} catch (Exception e) {
								log.info("Error occured when processing typeB message file: " + file.getName());
								e.printStackTrace();
							} finally {
								backupFile(file, serviceBearer.getMessageInBackupLocation());
							}
						} else if (file.getName().endsWith(Constants.SitaTexConstants.EXTENSION_SENT)) {
							log.info("Reading typeB SNT message file: " + file.getName());
							backupFile(file, serviceBearer.getMessageInBackupLocation());
						}
					}
				}
			}

			//Commented this because we have to check the DB for retry messages
			//if (hasNewMessages) {
				InboundMessageHandler inboundMessageHandler = new InboundMessageHandler();
				inboundMessageHandler.execute(unprocessedMsgIds);
			//}
		} else {
			MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
					MsgbrokerConstants.MODULE_NAME);

			etlPrlInLocation = moduleConfig.getEtlPrlInLocation();

			log.info("####SITATEXT### Trying to read ETL from " + etlPrlInLocation);
			etlPrlLocation = new File(etlPrlInLocation);
			etlPrlInFiles = etlPrlLocation.listFiles();


			pfsInLocation = moduleConfig.getPfsInLocation();
			log.info("####SITATEXT### Trying to read PFS from " + pfsInLocation);
			pfsLocation = new File(pfsInLocation);
			pfsInFiles = pfsLocation.listFiles();

			try {
				if (pfsInFiles != null) {
					log.info("####SITATEXT### Number of files in PFS Location  " + pfsInFiles.length);
					List<String> pfsMessageList = getMessageList(pfsInFiles, moduleConfig.getPfsBackupLocation()).get(
							ReservationInternalConstants.PopMessageType.PFS_MESSAGE);
					if (pfsMessageList != null && pfsMessageList.size() > 0) {
						log.info("####SITATEXT### PFS Message Size : " + pfsMessageList.size());
						MsgbrokerUtils.getReservationBD().getPfsFilesViaSitaTex(pfsMessageList);
					}
				}
				if (etlPrlInFiles != null) {
					log.info("####SITATEXT### Numbe of files in ETL Location  " + etlPrlInFiles.length);
					Map<String, List<String>> etlPrlMessageList = getMessageList(etlPrlInFiles,
							moduleConfig.getEtlPrlBackupLocation());
					List<String> etlMessageList = etlPrlMessageList.get(MessageTypes.ETL.toString());
					List<String> prlMessageList = etlPrlMessageList.get(MessageTypes.PRL.toString());
					if (etlMessageList != null && etlMessageList.size() > 0) {
						log.info("####SITATEXT### ETL Message Size : " + etlMessageList.size());

						MsgbrokerUtils.getETLBD().getEtlPrlFilesViaSitaTex(etlMessageList, MessageTypes.ETL.toString());
					}
					if (prlMessageList != null && prlMessageList.size() > 0) {
						MsgbrokerUtils.getETLBD().getEtlPrlFilesViaSitaTex(prlMessageList, MessageTypes.PRL.toString());
					}
				}

			} catch (ModuleException e) {
				log.error("###SITATEXT### Error while processing files ", e);
			}
		}
	}

	private Map<String, List<String>> getMessageList(File[] inFiles, String backUpLocation) {
		Map<String, List<String>> fileMap = new HashMap<String, List<String>>();
		String rawMessage;
		String messageBody = "";
		String messageHeader = "";
		String messageContent = "";
		SitaTexMessage sitaTexMessage;
		for (File file : inFiles) {
			if (file.getName().endsWith(Constants.SitaTexConstants.EXTENSION_RECEIVE)) {
				try {
					rawMessage = IoUtil.readContent(file, SitaTexElement.EOL);
					sitaTexMessage = new SitaTexMessage(rawMessage, SitaTexMessage.MessageType.SITA_TEX);
					if ((!sitaTexMessage.isParseFailed()) && (!sitaTexMessage.isAcknowledgement())) {
						messageHeader = sitaTexMessage.getMessageHeader().getSmi();
						messageBody = messageHeader + "\n" + sitaTexMessage.getMessageBody().getMessageText();
						
						if (messageBody.indexOf("PART1" + "\n") != -1) {
							messageContent = "From: " + sitaTexMessage.getMessageHeader().getOriginAddress() + "\n" + messageBody;
						} else {
							messageContent = messageBody;
						}

						if (log.isDebugEnabled()) {
							log.debug("###SITATEXT###Message content for file :- " + file.getName() + " is : " + messageContent);
						}

						if (messageBody.startsWith(ReservationInternalConstants.PopMessageType.PFS_MESSAGE)) {
							if (!fileMap.containsKey(ReservationInternalConstants.PopMessageType.PFS_MESSAGE)) {
								fileMap.put(ReservationInternalConstants.PopMessageType.PFS_MESSAGE, new ArrayList<String>());
							}
							fileMap.get(ReservationInternalConstants.PopMessageType.PFS_MESSAGE).add(messageContent);
						} else if (messageBody.startsWith(MessageTypes.ETL.toString())) {
							if (!fileMap.containsKey(MessageTypes.ETL.toString())) {
								fileMap.put(MessageTypes.ETL.toString(), new ArrayList<String>());
							}
							fileMap.get(MessageTypes.ETL.toString()).add(messageContent);
						} else if (messageBody.startsWith(MessageTypes.PRL.toString())) {
							if (!fileMap.containsKey(MessageTypes.PRL.toString())) {
								fileMap.put(MessageTypes.PRL.toString(), new ArrayList<String>());
							}
							fileMap.get(MessageTypes.PRL.toString()).add(messageContent);
						}
					} else {
						log.error("###SITATEXT###Error in parsing File : - " + file.getName());
						handleFailure(sitaTexMessage);
					}
				} catch (Exception e) {
					log.error("###SITATEXT###Error in Processing File : - " + file.getName(), e);
				} finally {
					backupFile(file, backUpLocation);
					file.delete();
				}
			} else {
				log.info("###SITATEXT### File : " + file.getName() + " Does not End with "
						+ Constants.SitaTexConstants.EXTENSION_RECEIVE);
			}
		}

		return fileMap;
	}

	public ServiceBearer.MessagingMode getMessagingMode() {
		return ServiceBearer.MessagingMode.FILE;
	}

	private void saveMessage(SitaTexMessage message, ServiceBearer serviceBearer) throws Exception {
		String msgText = message.getMessageBody().getMessageText();
		InMessage inMessage = new InMessage();
		inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.UNLOCKED);
		inMessage.setAirLineCode(AppSysParamsUtil.getDefaultCarrierCode());
		inMessage.setReceivedDate(new Date());
		inMessage.setGdsMessageID(message.getMessageHeader().getMessageId());		
		
		// if received message is [SSM/ASM/AVS] then need to identify the message type before parsing
		String[] resultStr = MessageRequestProcessingUtil.getMessageIdentifier(msgText, true);
		String smi = message.getMessageHeader().getSmi();

		if (!resultStr[0].isEmpty()) {
			if (Constants.ServiceTypes.SSM.equals(resultStr[0])) {
				inMessage.setMessageType(Constants.ServiceTypes.SSM);
				inMessage.setInMessageText(resultStr[1]);
				inMessage.setMessageSequence(resultStr[4]);					
			} else if (Constants.ServiceTypes.ASM.equals(resultStr[0])) {
				inMessage.setMessageType(Constants.ServiceTypes.ASM);
				inMessage.setInMessageText(resultStr[1]);
				inMessage.setMessageSequence(resultStr[4]);					
			} else if (Constants.ServiceTypes.AVS.equals(resultStr[0])) {
				inMessage.setMessageType(Constants.ServiceTypes.AVS);
			}
		} else if (smi!= null && !smi.isEmpty()) {
			if (Constants.ServiceTypes.AVS.equals(smi)) {
				msgText = smi + "\n" + msgText;
				inMessage.setMessageType(Constants.ServiceTypes.AVS);
			} else if (Constants.ServiceTypes.DVD.equals(smi)) {
				inMessage.setMessageType(Constants.ServiceTypes.DVD);
				msgText = smi + "\n" + msgText;
			} else if (Constants.ServiceTypes.RLR.equals(smi)) {
				inMessage.setMessageType(Constants.ServiceTypes.RLR);
				msgText = smi + "\n" + msgText;
			} else if (Constants.ServiceTypes.SSM.equals(smi)) {
				inMessage.setMessageType(Constants.ServiceTypes.SSM);
				msgText = smi + "\n" + msgText;
			} else if (Constants.ServiceTypes.ASM.equals(smi)) {
				inMessage.setMessageType(Constants.ServiceTypes.ASM);
				msgText = smi + "\n" + msgText;
			} else if (Constants.ServiceTypes.ASC.equals(smi)) {
				inMessage.setMessageType(Constants.ServiceTypes.ASC);
				msgText = smi + "\n" + msgText;
			}else {
				inMessage.setMessageType(Constants.ServiceTypes.ReservationService);
			}
		} else {
			inMessage.setMessageType(Constants.ServiceTypes.ReservationService);
		}
		inMessage.setInMessageText(msgText);
		inMessage.setSentTTYAddress(message.getMessageHeader().getOriginAddress());
		inMessage.setRecipientTTYAddress(serviceBearer.getSitaAddress());
		inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
		inMessage.setEditableStatus(true);
		inMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.SitaTex);
		inMessage = MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessage(inMessage);

		InMessageProcessingStatus inMessagePassingErrors = new InMessageProcessingStatus();
		inMessagePassingErrors.setInMessageID(inMessage.getInMessageID());
		inMessagePassingErrors
				.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.REQUEST_MESSAGE_RECEIVED);
		inMessagePassingErrors.setProcessingDate(new Date());
		if (!message.isParseFailed()) {
			inMessagePassingErrors.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
		} else {
			inMessagePassingErrors.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
		}
		MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(inMessagePassingErrors);

		hasNewMessages = true;
	}

	private void backupFile(File file, String backupLocation) {
		log.info("Renaming and Moving typeB message file: " + file.getName());
		long timeInMs = System.currentTimeMillis();
		File destFile = new File(backupLocation, timeInMs + "_" + file.getName());

		String fileName = file.getName();
		boolean success = file.renameTo(destFile);
		log.info("move success: " + success);
		if (!success) {
			success = copyFile(file, destFile);
			file.delete();
			log.info("copying success: " + success);

			if (!success) {
				success = file.delete();
				log.info("delete success: " + success);
			}
		}
		log.info("Renaming and Moving complted for typeB message file: " + fileName);
	}

	private void writeFile(String outLocation, String tmpFileName, SitaTexMessage message, SitaTexMessage.MessageType messageMode) {

		
		File file = new File(outLocation, tmpFileName);

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(message.toSitaTexMessage(messageMode));
			writer.flush();
		} catch (IOException e) {
			log.error("Error occurred ---- ", e);
		} finally {
			// rename (move) the file with .SND after writing it 
			// this atomic setting prevents BATAP connector from reading the partial file
			String fileName = tmpFileName + Constants.SitaTexConstants.EXTENSION_SEND;
			file.renameTo(new File(outLocation, fileName));
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
				}
			}
		}   
	}

	private boolean copyFile(File src, File dest) {
		Collection<String> lines = IoUtil.readContent(src);
		boolean success = false;

		BufferedWriter writer = null;

		try {
			writer = new BufferedWriter(new FileWriter(dest));

			for (String line : lines) {
				writer.write(line);
				writer.newLine();
			}
			writer.flush();
			success = true;
		} catch (IOException e) {
            log.error("File copying failed -- ", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					log.error("BufferedWriter close failed -- ", e);
				}
			}
		}

		return success;
	}

	private void handleFailure(SitaTexMessage message) {
		log.info("###SITATEXT### Message parsing status " + message.isParseFailed());
		log.info("###SITATEXT### Message Is Acknowledgement " + message.isAcknowledgement());
	}
}
