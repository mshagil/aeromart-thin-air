package com.isa.thinair.msgbroker.core.adaptor;

import com.isa.thinair.commons.api.dto.ets.FlightStatusUpdateReqDTO;
import com.accelaero.ets.SimplifiedTicketServiceWrapper;

public class FlightStatusUpdateRequestAdaptor
		extends AbstractGrpcRequestCreator<SimplifiedTicketServiceWrapper.FlightStatusUpdateRequest, FlightStatusUpdateReqDTO> {

	public FlightStatusUpdateRequestAdaptor() {
		super(SimplifiedTicketServiceWrapper.FlightStatusUpdateRequest.class, FlightStatusUpdateReqDTO.class);
	}

}
