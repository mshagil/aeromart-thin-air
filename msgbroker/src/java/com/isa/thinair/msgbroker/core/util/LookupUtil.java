package com.isa.thinair.msgbroker.core.util;

import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.GdsBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminJdbcDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class LookupUtil {

	private static Object getDAO(String daoName) {
		String daoFullName = "isa:base://modules/" + MsgbrokerConstants.MODULE_NAME + "?id=" + daoName + "Proxy";
		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(daoFullName);
	}

	public static MessageDAO lookupMessageDAO() {
		MessageDAO oMessageDAO = (MessageDAO) getDAO("messageDAO");
		return oMessageDAO;
	}

	public static AdminDAO lookupAdminDAO() {
		AdminDAO adminDAO = (AdminDAO) getDAO("adminDAO");
		return adminDAO;
	}
	
	public static AdminJdbcDAO lookupAdminJdbcDAO() {
		AdminJdbcDAO adminJdbcDAO = (AdminJdbcDAO) getDAO("adminJdbcDAO");
		return adminJdbcDAO;
	}
	
	public final static GdsBD getGdsServiceBD() {
		return (GdsBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, GdsBD.SERVICE_NAME);
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getMsgBrokerModuleConfig(),
				MsgbrokerConstants.MODULE_NAME, "msgbroker.config.dependencymap.invalid");
	}

	public static MsgBrokerModuleConfig getMsgBrokerModuleConfig() {
		return (MsgBrokerModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(MsgbrokerConstants.MODULE_NAME);
	}
	
	public static ServiceClientReqDaoJDBC lookupServiceClientReqDAO() {
		ServiceClientReqDaoJDBC serviceClientReqDAO = (ServiceClientReqDaoJDBC) getDAO("serviceClientReqJDBCDAO");
		return serviceClientReqDAO;
	}

	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryResBD.SERVICE_NAME);
	}
	
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}
}
