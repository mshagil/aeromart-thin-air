package com.isa.thinair.msgbroker.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;

public class PNLADLComposerUtil {

	public static String[] populateGroupIds() {
		String letters[] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
				"U", "V", "W", "X", "Y", "Z" };

		String[] tourIds = new String[702];

		for (int k = 0; k < 26; k++) {
			tourIds[k] = letters[k];
		}
		for (int i = 0; i < 26; i++) {
			for (int j = 0; j < 26; j++) {
				tourIds[i * 26 + j + 26] = letters[i].concat(letters[j]);
			}
		}
		return tourIds;
	}

	public static String determineNextGroupId(String lastGroupId, String tourIds[]) {
		int retVal = 0;
		for (int i = 0; i < tourIds.length; i++) {
			if (lastGroupId.equals(tourIds[i])) {
				retVal = ++i;
				break;
			}
		}
		return tourIds[retVal];
	}

	public static Map<String, List<PassengerInformation>> generatePnrWisePassengers(List<PassengerInformation> passengers) {
		Map<String, List<PassengerInformation>> pnrWisePassengers = new HashMap<String, List<PassengerInformation>>();
		if (passengers != null) {
			for (PassengerInformation PassengerInfo : passengers) {

				if (pnrWisePassengers.get(PassengerInfo.getPnr()) == null) {
					List<PassengerInformation> tmpPassengers = new ArrayList<PassengerInformation>();
					tmpPassengers.add(PassengerInfo);
					pnrWisePassengers.put(PassengerInfo.getPnr(), tmpPassengers);
				} else {
					pnrWisePassengers.get(PassengerInfo.getPnr()).add(PassengerInfo);
				}
			}
		}
		return pnrWisePassengers;
	}
}
