package com.isa.thinair.msgbroker.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.PreMessageProcessor;

public class InboundMessageHandler {

	private MessageDAO messageDAO;
	
	private static final String DUPLICATE_MESSAGE = "DUPLICATE MESSAGE";
	private static final String MESSAGE_SKIPPED = "MESSAGE SKIPPED FOR SEQUENTIAL PROCESSING";
	private static final Log log = LogFactory.getLog(InboundMessageHandler.class);
	
	public void execute(Collection<Integer> unprocessedMsgIdList) {

		boolean enableMsgQueueForSSMASM = AppSysParamsUtil.isEnableMessageQueuingForSsmAsm();
		List<String> msgTypesForSequentialProcessing = null;
		if (AppSysParamsUtil.isEnableSequentialMessageProcessing()) {
			msgTypesForSequentialProcessing = AppSysParamsUtil.getSequentialProcessingAllowedMessageTypes();
		}

		if (enableMsgQueueForSSMASM) {

			log.info("###MESSAGE QUEUE ENABLED###");
			
			List<Integer> allMessageIdsToProcess = new ArrayList<Integer>();
			List<InMessage> unprocessedInMessages = getMessageDAO().getAllLockableInMessagesInFifoOrder(
					MessageProcessingConstants.InMessageProcessStatus.Unprocessed, msgTypesForSequentialProcessing);

			for (InMessage message : unprocessedInMessages) {
				if (unprocessedMsgIdList != null && !unprocessedMsgIdList.contains(message.getInMessageID())) {
					unprocessedMsgIdList.add(message.getInMessageID());
					allMessageIdsToProcess.add(message.getInMessageID());
				}
			}

			List<Integer> messageIdsToProcess = checkAndOmitDuplicateAndSequentialProcessingAllowedMessages(
					allMessageIdsToProcess, msgTypesForSequentialProcessing);

			try {
				filterMessagesForQueue(unprocessedInMessages, messageIdsToProcess);
			} catch (ModuleException e) {
				log.error("Error occured while processing", e);
			}
			log.info("###End of MESSAGE QUEUE ###");

		} else {

			List<Integer> allUnprocessedMessageIds = getMessageDAO().getLockableInMessageIDsInFifoOrder(
					MessageProcessingConstants.InMessageProcessStatus.Unprocessed, msgTypesForSequentialProcessing);

			List<Integer> uniqueUnProcessedMessageIds = new ArrayList<Integer>();

			for (Integer inMessageId : allUnprocessedMessageIds) {
				if (unprocessedMsgIdList != null && !unprocessedMsgIdList.contains(inMessageId)) {
					unprocessedMsgIdList.add(inMessageId);
					uniqueUnProcessedMessageIds.add(inMessageId);
				}
			}

			List<Integer> messageIdsToProcess = checkAndOmitDuplicateAndSequentialProcessingAllowedMessages(
					uniqueUnProcessedMessageIds, msgTypesForSequentialProcessing);

			MDBInvoker mdbInvoker = new MDBInvoker();
			for (Integer messageId : messageIdsToProcess) {
				mdbInvoker.invokeInboundMessageProcessorMDB(messageId);
			}
		}
	}

	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}

	private List<Integer> checkAndOmitDuplicateAndSequentialProcessingAllowedMessages(List<Integer> unprocessedMessageIds,
			List<String> msgTypesForSequentialProcessing) {
		List<InMessage> allUnprocessedMessages = new ArrayList<InMessage>();
		List<InMessage> duplicateMessages = new ArrayList<InMessage>();
		List<InMessage> tempMessages = new ArrayList<InMessage>();
		List<InMessage> sequentialAllowedMessages = new ArrayList<InMessage>();

		ArrayList<Integer> processableMessageIDs = new ArrayList<Integer>();
		for (Integer messageID : unprocessedMessageIds) {
			allUnprocessedMessages.add(getMessageDAO().getInMessage(messageID));
		}
		for (InMessage message : allUnprocessedMessages) {
			boolean isDuplicateMessage = false;

			if (msgTypesForSequentialProcessing != null && !msgTypesForSequentialProcessing.isEmpty()
					&& msgTypesForSequentialProcessing.contains(message.getMessageType())) {
				sequentialAllowedMessages.add(message);
				continue;
			}

			for (InMessage tempMessage : tempMessages) {
				if (message.getInMessageText().equals(tempMessage.getInMessageText())) {
					isDuplicateMessage = true;
					break;
				}
			}

			if (isDuplicateMessage) {
				duplicateMessages.add(message);
			} else {
				tempMessages.add(message);
			}
		}

		for (InMessage message : tempMessages) {
			processableMessageIDs.add(message.getInMessageID());
		}
		if (!processableMessageIDs.isEmpty()) {
			Collections.sort(processableMessageIDs);
		}
		updateDuplicateMessageProcessStatus(duplicateMessages);
		skipSequentialProcessingAllowedMessageTypes(sequentialAllowedMessages);

		return processableMessageIDs;
	}
	
	private void updateDuplicateMessageProcessStatus(Collection<InMessage> duplicateMessages) {
		for (InMessage duplicateMessage : duplicateMessages) {
			InMessageProcessingStatus duplicateMessageProcessingStatus = new InMessageProcessingStatus();
			duplicateMessageProcessingStatus.setInMessageID(duplicateMessage.getInMessageID());
			duplicateMessageProcessingStatus.setErrorMessageText(DUPLICATE_MESSAGE);
			duplicateMessageProcessingStatus
					.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.DUPLICATE_IN_MESSAGE);
			duplicateMessageProcessingStatus.setProcessingDate(new Date());
			duplicateMessageProcessingStatus
					.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
			MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(duplicateMessageProcessingStatus);

			duplicateMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
			duplicateMessage.setLastErrorMessageCode(DUPLICATE_MESSAGE);
			getMessageDAO().saveInMessage(duplicateMessage);
		}
	}
	
	private void filterMessagesForQueue(List<InMessage> unprocessedMessages, List<Integer> msgIdsToProcess)
			throws ModuleException {

		PreMessageProcessor preMsgProcessor = new PreMessageProcessor();
		preMsgProcessor.messageProcessor(unprocessedMessages, msgIdsToProcess);

	}

	private void skipSequentialProcessingAllowedMessageTypes(Collection<InMessage> sequentialAllowedMessages) {
		for (InMessage skippedSequentialMessage : sequentialAllowedMessages) {
			InMessageProcessingStatus skippedMessageProcessingStatus = new InMessageProcessingStatus();
			skippedMessageProcessingStatus.setInMessageID(skippedSequentialMessage.getInMessageID());
			skippedMessageProcessingStatus.setErrorMessageText(MESSAGE_SKIPPED);
			skippedMessageProcessingStatus
					.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.SKIPPED_IN_MESSAGE);
			skippedMessageProcessingStatus.setProcessingDate(new Date());
			skippedMessageProcessingStatus
					.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
			MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(skippedMessageProcessingStatus);

			skippedSequentialMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
			skippedSequentialMessage.setLastErrorMessageCode(MESSAGE_SKIPPED);
			getMessageDAO().saveInMessage(skippedSequentialMessage);
		}
	}
}
