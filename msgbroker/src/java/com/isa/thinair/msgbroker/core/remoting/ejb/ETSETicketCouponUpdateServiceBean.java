package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.concurrent.TimeUnit;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

import com.isa.thinair.commons.api.dto.ets.FlightStatusUpdateReqDTO;
import com.isa.thinair.commons.api.dto.ets.FlightStatusUpdateResDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.msgbroker.core.adaptor.FlightStatusUpdateRequestAdaptor;
import com.isa.thinair.msgbroker.core.adaptor.FlightStatusUpdateResponseAdaptor;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketCouponStatusUpdateServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketCouponStatusUpdateServiceBeanRemote;
import com.accelaero.ets.SimplifiedTicketServiceGrpc;
import com.accelaero.ets.SimplifiedTicketServiceWrapper.FlightStatusUpdateRequest;
import com.accelaero.ets.SimplifiedTicketServiceWrapper.FlightStatusUpdateResponse;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.platform.api.LookupServiceFactory;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

@Stateless
@RemoteBinding(jndiBinding = "TicketCouponStatusUpdateService.remote")
@LocalBinding(jndiBinding = "TicketCouponStatusUpdateService.local")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ETSETicketCouponUpdateServiceBean extends PlatformBaseSessionBean
		implements ETSTicketCouponStatusUpdateServiceBeanLocal, ETSTicketCouponStatusUpdateServiceBeanRemote {

	@Override
	public FlightStatusUpdateResDTO updateETicketCoupnStatus(FlightStatusUpdateReqDTO eTicketCouponStatusUpdateReq)
			throws ModuleException {

		FlightStatusUpdateResDTO flightStatusUpdateResponse = null;
		MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MsgbrokerConstants.MODULE_NAME);
		String etsHost = moduleConfig.getEtsHost();
		int etsPort = moduleConfig.getEtsPort();
		
		try {
			ManagedChannel channel = ManagedChannelBuilder.forAddress(etsHost, etsPort).usePlaintext(true).build();
			SimplifiedTicketServiceGrpc.SimplifiedTicketServiceBlockingStub blockingStub = SimplifiedTicketServiceGrpc
					.newBlockingStub(channel);
			
			FlightStatusUpdateRequestAdaptor reqAdaptor = new FlightStatusUpdateRequestAdaptor();
			FlightStatusUpdateResponseAdaptor resAdaptor = new FlightStatusUpdateResponseAdaptor();
			
			FlightStatusUpdateRequest input = reqAdaptor.fromDTO(eTicketCouponStatusUpdateReq);
			FlightStatusUpdateResponse output = callETSForCouponUpdate(blockingStub, input);
			flightStatusUpdateResponse =  resAdaptor.fromGRPC(output);
			shutdown(channel);
			
		} catch (Exception e) {
			log.error("ERROR :: Error ETS update" + e.getCause());
		}
		return flightStatusUpdateResponse;
	}

	public FlightStatusUpdateResponse callETSForCouponUpdate(SimplifiedTicketServiceGrpc.SimplifiedTicketServiceBlockingStub blockingStub,
			FlightStatusUpdateRequest input) {

		FlightStatusUpdateResponse output = null;
		try {
			output = blockingStub.updateFlightStatusOfCoupons(input);
		} catch (StatusRuntimeException e) {
			log.error("ERROR :: Error while updating ETS" + e.getCause());
		}
		
		return output;
	}

	public void shutdown(ManagedChannel channel) throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}
}
