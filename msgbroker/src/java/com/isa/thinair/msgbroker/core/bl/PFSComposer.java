/**
 * 
 */
package com.isa.thinair.msgbroker.core.bl;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pfs.CSPaxBookingCount;
import com.isa.thinair.airreservation.api.dto.pfs.CabinBookingCountTo;
import com.isa.thinair.airreservation.api.dto.pfs.CabinCountTo;
import com.isa.thinair.airreservation.api.dto.pfs.PFSBookingClassList;
import com.isa.thinair.airreservation.api.dto.pfs.PFSCatagoryByDestination;
import com.isa.thinair.airreservation.api.dto.pfs.PFSDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PFSMessageDataDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PassengerBookingToPFS;
import com.isa.thinair.airreservation.api.dto.pfs.PassengerCategoryToPFS;
import com.isa.thinair.airreservation.api.dto.pfs.PFSPaxInfo;
import com.isa.thinair.airreservation.api.dto.pfs.PaxPFSTo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * Composes PFS file for Marketing carrier when PFS received for a code share flight.
 * 
 * @author M.Rikaz
 * 
 */
public class PFSComposer {

	private static Log logger = LogFactory.getLog(PFSComposer.class);
	public static final String CREW_PREFIX = "CRW";
	public static final String SEC_PREFIX = "SEC";
	public static final String GOSHOW_PREFIX = "GS";
	public static final String BUSINESS_CLASS_PREFIX = "C";
	public static final String PFS_MESSAGE_IDENTIFIER = "PFS";
	private static final String PFS_MESSAGE_END = "ENDPFS";
	private static final int MAX_LINE_LENGTH_IATA = 50;
	private final int MAX_SINGLE_PAX_FULLNAME_WITH_TITLE_LENGTH = 55;
	private static final String PFS_MESSAGE_PART_IDENTIFIER = "PART";
	private static final String PFS_MESSAGE_END_PART_IDENTIFIER = "ENDPART";
	private static final String DECIMAL_FORMAT_TWO = "%02d";
	private static final String DECIMAL_FORMAT_THREE = "%03d";

	private boolean isComposeEmptyPfs;

	public PFSComposer(boolean isComposeEmptyPfs) {
		this.isComposeEmptyPfs = isComposeEmptyPfs;
	}

	public String composePFSMessage(String addressElement, String communicationElement, PFSMessageDataDTO messageDataDTO)
			throws ModuleException {

		String outMessage = "";
		if (messageDataDTO != null) {
			HashMap<String, Object> pfsTemplate = new HashMap<String, Object>();
			pfsTemplate.put("pfsMetaDTO", messageDataDTO);
			if (pfsTemplate != null && !pfsTemplate.isEmpty()) {

				try {
					String templateName = "email/" + TypeBMessageConstants.MessageIdentifiers.PFS
							+ MessageComposerConstants.TEMPLATE_SUFFIX;
					StringWriter writer = new StringWriter();
					new TemplateEngine().writeTemplate(pfsTemplate, templateName, writer);
					outMessage = writer.toString();

				} catch (Exception e) {
					if (logger.isDebugEnabled()) {
						logger.debug("pfscomposer.pfsmessage.error\n");
					}
					logger.info("pfscomposer.pfsmessage.error\n" + e.getMessage());
					throw new ModuleException("pfscomposer.pfsmessage.error");
				}

			} else {

				if (logger.isDebugEnabled()) {
					logger.debug("pfscomposer.pfsmessage.noelements\n");
				}
				logger.info("pfscomposer.pfsmessage.noelements\n");
				throw new ModuleException("pfscomposer.pfsmessage.noelementsr");
			}
			logger.info(outMessage);
		}
		return outMessage;

	}

	private List<PFSMessageDataDTO> formatPFSMailNew(PFSDTO pfsDTOOrg, String addressElement, String communicationElement) {

		List<PFSMessageDataDTO> pfsMetaDataDTOList = new ArrayList<PFSMessageDataDTO>();

		Map<Integer, PFSDTO> partByPfsDTO = this.getPFSDTOPartByPart(pfsDTOOrg);
		boolean isFirstPart = true;
		for (Integer part : partByPfsDTO.keySet()) {
			PFSMessageDataDTO pfsMetaDataDTO = new PFSMessageDataDTO();
			pfsMetaDataDTO.setPartElement(PFS_MESSAGE_PART_IDENTIFIER + part);

			PFSDTO pfsDTO = partByPfsDTO.get(part);
			String originAirport = pfsDTO.getDepartureAirport();
			pfsMetaDataDTO.setFromAddress(addressElement);
			pfsMetaDataDTO.setCommunicationElement(communicationElement);
			pfsMetaDataDTO.setMsgType(PFS_MESSAGE_IDENTIFIER);
			pfsMetaDataDTO.setFlightNumber(pfsDTO.getFlightNumber());
			pfsMetaDataDTO = buildFlightData(pfsMetaDataDTO, pfsDTO, originAirport);

			// SEAT CONFIGURATION ELEMENT
			// TODO : commenting since or pfs parser[pfs.jj] is not supporting this element at this moment
			// This element is conditional.
			// It is used only when the configuration is different from the configuration received on the PNL/ADL and/or
			// SAL.
			// pfsMetaDataDTO = buildSeatConfigurationData(pfsMetaDataDTO, pfsDTO);

			// CLASS CODES ELEMENT
			// TODO : commenting since or pfs parser[pfs.jj] is not supporting this element at this moment
			// This element is conditional, used only when bilaterally agreed.
			// It is used when the PNL/ADL name sortation method is by fare class.
			// pfsMetaDataDTO = buildRBDConfigurationData(pfsMetaDataDTO, pfsDTO);

			pfsMetaDataDTO = buildCategoryByDestination(pfsMetaDataDTO, pfsDTO, originAirport);

			if (isFirstPart) {
				pfsMetaDataDTO = buildNumericByDestination(pfsMetaDataDTO, pfsDTO, originAirport);
				isFirstPart = false;
			}

			if (part == partByPfsDTO.size()) {
				pfsMetaDataDTO.setEndElement(PFS_MESSAGE_END);
			} else {
				pfsMetaDataDTO.setEndElement(PFS_MESSAGE_END_PART_IDENTIFIER + part);
			}

			pfsMetaDataDTOList.add(pfsMetaDataDTO);

		}

		return pfsMetaDataDTOList;
	}

	private List<PFSMessageDataDTO> formatEmptyPFSMail(PFSDTO pfsDTO, String addressElement, String communicationElement)
			throws ModuleException {
		List<PFSMessageDataDTO> pfsMetaDataDTOList = new ArrayList<PFSMessageDataDTO>();
		PFSMessageDataDTO pfsMetaDataDTO = new PFSMessageDataDTO();
		String originAirport = pfsDTO.getDepartureAirport();
		Integer part = 1; // Only part1 used for empty pfs
		
		pfsMetaDataDTO.setPartElement(PFS_MESSAGE_PART_IDENTIFIER + part);
		pfsMetaDataDTO.setFromAddress(addressElement);
		pfsMetaDataDTO.setCommunicationElement(communicationElement);
		pfsMetaDataDTO.setMsgType(PFS_MESSAGE_IDENTIFIER);
		pfsMetaDataDTO.setFlightNumber(pfsDTO.getFlightNumber());
		pfsMetaDataDTO = buildFlightData(pfsMetaDataDTO, pfsDTO, originAirport);

		pfsMetaDataDTO = buildNumericDataForEmptyPFS(pfsMetaDataDTO, pfsDTO, originAirport);
		pfsMetaDataDTO.setEndElement(PFS_MESSAGE_END);
		pfsMetaDataDTOList.add(pfsMetaDataDTO);
		
		return pfsMetaDataDTOList;
	}

	private PFSMessageDataDTO buildFlightData(PFSMessageDataDTO pfsMetaDataDTO, PFSDTO pfsDTO, String originAirport) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMM");
		Date depdate = pfsDTO.getFlightDate();
		pfsMetaDataDTO.setFlightNumber(pfsDTO.getFlightNumber());
		pfsMetaDataDTO.setDayMonth(sdf.format(depdate).toUpperCase());
		pfsMetaDataDTO.setBoardingAirport(originAirport);
		return pfsMetaDataDTO;
	}

	private PFSMessageDataDTO buildCategoryByDestination(PFSMessageDataDTO pfsMetaDTO, PFSDTO pfsDTO, String originAirport) {
		Map<String, Map<String, Map<String, Map<String, Set<PFSBookingClassList>>>>> destinationCabinPaxMap = new HashMap<String, Map<String, Map<String, Map<String, Set<PFSBookingClassList>>>>>();
		Map<String, Map<String, Set<PFSBookingClassList>>> bookingClassMap = new HashMap<String, Map<String, Set<PFSBookingClassList>>>();
		Map<String, Set<PFSBookingClassList>> bookingPaxStatusClassMap = new HashMap<String, Set<PFSBookingClassList>>();
		Map<String, Map<String, Map<String, Set<PFSBookingClassList>>>> cabinClassMap = new HashMap<String, Map<String, Map<String, Set<PFSBookingClassList>>>>();

		for (PFSPaxInfo pax : pfsDTO.getPassengerNameList()) {

			Set<PFSBookingClassList> paxBookingCol = new HashSet<PFSBookingClassList>();
			PFSBookingClassList paxBooking = new PFSBookingClassList();

			String destinationAirport = pax.getArrivalAirport();
			String cabinClass = pax.getCabinClass();
			String paxFirstName = pax.getFirstName();
			String paxLastName = pax.getLastName();
			String bookingClass = pax.getBookingClass();
			String paxTitle = pax.getTitle();
			String pnr = pax.getPnr();
			// String status = pax.getStatus();
			String status = null;
			String travelStatus = pax.getEntryStatus();
			String displayCategory = null;
			paxBooking.setPaxStatus(status);
			paxBooking.setPaxTravelStatus(travelStatus);
			paxBooking.setPaxFirstName(paxFirstName);
			paxBooking.setPaxLastName(paxLastName);
			paxBooking.setPaxTitle(paxTitle);
			paxBooking.setPnr(pnr);

			if (ReservationInternalConstants.PfsPaxStatus.GO_SHORE.equals(travelStatus)) {
				displayCategory = ReservationInternalConstants.PfsCategories.GO_SHOW;
				paxBooking.setPnr("");
			} else if (ReservationInternalConstants.PfsPaxStatus.NO_SHORE.equals(travelStatus)) {
				displayCategory = ReservationInternalConstants.PfsCategories.NO_SHOW;
			} else if (ReservationInternalConstants.PfsPaxStatus.OFF_LD.equals(travelStatus)) {
				displayCategory = ReservationInternalConstants.PfsCategories.OFF_LK;
			} else if (ReservationInternalConstants.PfsPaxStatus.NO_REC.equals(travelStatus)) {
				displayCategory = ReservationInternalConstants.PfsCategories.NO_REC;
				//paxBooking.setPnr("");
			}
			paxBooking.setPaxTravelStatusDisplay(displayCategory);

			if (displayCategory != "") {
				if (!destinationCabinPaxMap.containsKey(destinationAirport)) {
					bookingPaxStatusClassMap = new HashMap<String, Set<PFSBookingClassList>>();
					bookingClassMap = new HashMap<String, Map<String, Set<PFSBookingClassList>>>();
					cabinClassMap = new HashMap<String, Map<String, Map<String, Set<PFSBookingClassList>>>>();
					paxBookingCol.add(paxBooking);
					bookingPaxStatusClassMap.put(displayCategory, paxBookingCol);
					bookingClassMap.put(bookingClass, bookingPaxStatusClassMap);
					cabinClassMap.put(cabinClass, bookingClassMap);
					destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
				} else {
					cabinClassMap = destinationCabinPaxMap.get(destinationAirport);
					if (!cabinClassMap.containsKey(cabinClass)) {
						bookingPaxStatusClassMap = new HashMap<String, Set<PFSBookingClassList>>();
						bookingClassMap = new HashMap<String, Map<String, Set<PFSBookingClassList>>>();
						paxBookingCol.add(paxBooking);
						bookingPaxStatusClassMap.put(displayCategory, paxBookingCol);
						bookingClassMap.put(bookingClass, bookingPaxStatusClassMap);
						cabinClassMap.put(cabinClass, bookingClassMap);
						destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
					} else {
						bookingClassMap = cabinClassMap.get(cabinClass);
						if (!bookingClassMap.containsKey(bookingClass)) {
							bookingPaxStatusClassMap = new HashMap<String, Set<PFSBookingClassList>>();
							paxBookingCol.add(paxBooking);
							bookingPaxStatusClassMap.put(displayCategory, paxBookingCol);
							bookingClassMap.put(bookingClass, bookingPaxStatusClassMap);
							cabinClassMap.put(cabinClass, bookingClassMap);
							destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
						} else {
							bookingPaxStatusClassMap = bookingClassMap.get(bookingClass);
							if (!bookingPaxStatusClassMap.containsKey(displayCategory)) {
								paxBookingCol.add(paxBooking);
								bookingPaxStatusClassMap.put(displayCategory, paxBookingCol);
								bookingClassMap.put(bookingClass, bookingPaxStatusClassMap);
								cabinClassMap.put(cabinClass, bookingClassMap);
								destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
							} else {
								paxBookingCol = bookingPaxStatusClassMap.get(displayCategory);
								paxBookingCol.add(paxBooking);
								bookingPaxStatusClassMap.put(displayCategory, paxBookingCol);
								bookingClassMap.put(bookingClass, bookingPaxStatusClassMap);
								cabinClassMap.put(cabinClass, bookingClassMap);
								destinationCabinPaxMap.put(destinationAirport, cabinClassMap);

							}

						}
					}
				}
			}

		}

		for (Map.Entry<String, Map<String, Map<String, Map<String, Set<PFSBookingClassList>>>>> entry : destinationCabinPaxMap
				.entrySet()) {
			String destinationAirport = entry.getKey();
			Map<String, Map<String, Map<String, Set<PFSBookingClassList>>>> cabinClassDataMap = entry.getValue();
			PFSCatagoryByDestination categoryByDestinationElement = new PFSCatagoryByDestination();
			categoryByDestinationElement.setDestinationAirPort(destinationAirport);
			List<CabinCountTo> cabinClassWiseList = new ArrayList<CabinCountTo>();
			for (Map.Entry<String, Map<String, Map<String, Set<PFSBookingClassList>>>> cabinClassMapEntry : cabinClassDataMap
					.entrySet()) {
				Integer cabinClassCount = 0;
				String cabinClass = cabinClassMapEntry.getKey();
				Map<String, Map<String, Set<PFSBookingClassList>>> bookingClassDataMap = cabinClassMapEntry.getValue();
				List<PassengerBookingToPFS> bookingClassPaxLst = new ArrayList<PassengerBookingToPFS>();
				CabinCountTo cabinClassPaxData = new CabinCountTo();
				cabinClassPaxData.setCabinClass(cabinClass);
				for (Map.Entry<String, Map<String, Set<PFSBookingClassList>>> bookingClassClassMapEntry : bookingClassDataMap
						.entrySet()) {
					Integer bookingClassCount = 0;
					List<PassengerCategoryToPFS> categoryPaxLstData = new ArrayList<PassengerCategoryToPFS>();
					PassengerBookingToPFS bookingClassPax = new PassengerBookingToPFS();
					String bookingClass = bookingClassClassMapEntry.getKey();
					bookingClassPax.setBookingClass(bookingClass);
					Map<String, Set<PFSBookingClassList>> paxListBookingClass = bookingClassClassMapEntry.getValue();
					for (Map.Entry<String, Set<PFSBookingClassList>> bookingCategoryMapEntry : paxListBookingClass.entrySet()) {
						PassengerCategoryToPFS categoryPax = new PassengerCategoryToPFS();
						String category = bookingCategoryMapEntry.getKey();
						categoryPax.setPaxCategory(category);
						List<PaxPFSTo> paxInfoList = new ArrayList<PaxPFSTo>();
						Set<PFSBookingClassList> categoryPaxLst = bookingCategoryMapEntry.getValue();
						for (PFSBookingClassList PFSBookingClassPaxList : categoryPaxLst) {
							PaxPFSTo paxPfsInfo = new PaxPFSTo();
							StringBuilder sbFullName = new StringBuilder("");
							sbFullName.append(PFSBookingClassPaxList.getPaxFirstName());
							sbFullName.append(PFSBookingClassPaxList.getPaxLastName());
							if (PFSBookingClassPaxList.getPnr().equals("")) {
								boolean paxLastnameExists = false;

								for (PaxPFSTo paxPfsTo : paxInfoList) {
									if (paxPfsTo.getPaxLastName().equals(PFSBookingClassPaxList.getPaxLastName())
											&& (sbFullName.length() + paxPfsTo.getPaxFirstName().length() < MAX_SINGLE_PAX_FULLNAME_WITH_TITLE_LENGTH)) {
										sbFullName.append(paxPfsTo.getPaxFirstName());
										paxPfsTo.setPaxFirstName(paxPfsTo.getPaxFirstName()
												+ MessageComposerConstants.FWD
												+ (PFSBookingClassPaxList.getPaxFirstName() + PFSBookingClassPaxList
														.getPaxTitle()));
										paxLastnameExists = true;
										paxPfsTo.setPaxCount(paxPfsTo.getPaxCount() + 1);
									}
								}
								if (paxLastnameExists == false) {
									paxPfsInfo.setPaxFirstName(PFSBookingClassPaxList.getPaxFirstName()
											+ PFSBookingClassPaxList.getPaxTitle());
									paxPfsInfo.setPaxLastName(PFSBookingClassPaxList.getPaxLastName());
									paxPfsInfo.setPaxTitle(PFSBookingClassPaxList.getPaxTitle());
									paxPfsInfo.setPnr(PFSBookingClassPaxList.getPnr());
									paxInfoList.add(paxPfsInfo);
								}
							} else {
								boolean paxLastnameAndPnrExists = false;
								for (PaxPFSTo paxPfsTo : paxInfoList) {
									if (paxPfsTo.getPaxLastName().equals(PFSBookingClassPaxList.getPaxLastName())
											&& paxPfsTo.getPnr().equals(PFSBookingClassPaxList.getPnr())
											&& (sbFullName.length() + paxPfsTo.getPaxFirstName().length() < MAX_SINGLE_PAX_FULLNAME_WITH_TITLE_LENGTH)) {
										sbFullName.append(paxPfsTo.getPaxFirstName());
										paxPfsTo.setPaxFirstName(paxPfsTo.getPaxFirstName()
												+ MessageComposerConstants.FWD
												+ (PFSBookingClassPaxList.getPaxFirstName() + PFSBookingClassPaxList
														.getPaxTitle()));
										paxLastnameAndPnrExists = true;
										paxPfsTo.setPaxCount(paxPfsTo.getPaxCount() + 1);
									}
								}
								if (paxLastnameAndPnrExists == false) {
									paxPfsInfo.setPaxFirstName(PFSBookingClassPaxList.getPaxFirstName()
											+ PFSBookingClassPaxList.getPaxTitle());
									paxPfsInfo.setPaxLastName(PFSBookingClassPaxList.getPaxLastName());
									paxPfsInfo.setPaxTitle(PFSBookingClassPaxList.getPaxTitle());
									paxPfsInfo.setPnr(PFSBookingClassPaxList.getPnr());
									paxInfoList.add(paxPfsInfo);
								}
							}
							bookingClassCount = bookingClassCount + 1;
							cabinClassCount = cabinClassCount + 1;
						}
						categoryPax.setCount(categoryPaxLst.size());
						categoryPax.setCountDisplay(applyDisplayCountFormatter(cabinClass, paxInfoList.size()));
						categoryPax.setPaxList(paxInfoList);
						categoryPaxLstData.add(categoryPax);
					}
					bookingClassPax.setCount(bookingClassCount);
					bookingClassPax.setCountDisplay(String.format(DECIMAL_FORMAT_THREE, bookingClassCount));

					bookingClassPax.setPaxCategoryLst(categoryPaxLstData);
					bookingClassPaxLst.add(bookingClassPax);

				}
				cabinClassPaxData.setCount(cabinClassCount);
				cabinClassPaxData.setCountDisplay(String.format(DECIMAL_FORMAT_THREE, cabinClassCount));

				cabinClassPaxData.setPaxBookingList(bookingClassPaxLst);
				cabinClassWiseList.add(cabinClassPaxData);
			}
			categoryByDestinationElement.setCabinClassWiseList(cabinClassWiseList);
			pfsMetaDTO.getCategoryByDestination().add(categoryByDestinationElement);
		}

		return pfsMetaDTO;
	}

	private PFSMessageDataDTO buildNumericByDestination(PFSMessageDataDTO pfsMetaDataDTO, PFSDTO pfsDTO, String originAirport) {
		Map<String, Map<String, Map<String, Set<PFSBookingClassList>>>> destinationCabinPaxMap = new HashMap<String, Map<String, Map<String, Set<PFSBookingClassList>>>>();
		Map<String, Set<PFSBookingClassList>> bookingClassMap = new HashMap<String, Set<PFSBookingClassList>>();
		Map<String, Map<String, Set<PFSBookingClassList>>> cabinClassMap = new HashMap<String, Map<String, Set<PFSBookingClassList>>>();

		for (PFSPaxInfo pax : pfsDTO.getPassengerNameList()) {

			Set<PFSBookingClassList> paxBookingCol = new HashSet<PFSBookingClassList>();
			PFSBookingClassList paxBooking = new PFSBookingClassList();
			String destinationAirport = pax.getArrivalAirport();

			String cabinClass = pax.getCabinClass();
			String paxFirstName = pax.getFirstName();
			String paxLastName = pax.getLastName();
			String bookingClass = pax.getBookingClass();
			String paxTitle = pax.getTitle();
			String pnr = pax.getPnr();
			paxBooking.setPaxFirstName(paxFirstName);
			paxBooking.setPaxLastName(paxLastName);
			paxBooking.setPaxTitle(paxTitle);
			paxBooking.setPnr(pnr);
			paxBooking.setBookingClass(bookingClass);

			if (!destinationCabinPaxMap.containsKey(destinationAirport)) {
				bookingClassMap = new HashMap<String, Set<PFSBookingClassList>>();
				cabinClassMap = new HashMap<String, Map<String, Set<PFSBookingClassList>>>();
				paxBookingCol.add(paxBooking);
				bookingClassMap.put(bookingClass, paxBookingCol);
				cabinClassMap.put(cabinClass, bookingClassMap);
				destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
			} else {
				cabinClassMap = destinationCabinPaxMap.get(destinationAirport);
				if (!cabinClassMap.containsKey(cabinClass)) {
					bookingClassMap = new HashMap<String, Set<PFSBookingClassList>>();
					paxBookingCol.add(paxBooking);
					bookingClassMap.put(bookingClass, paxBookingCol);
					cabinClassMap.put(cabinClass, bookingClassMap);
					destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
				} else {
					bookingClassMap = cabinClassMap.get(cabinClass);
					if (!bookingClassMap.containsKey(bookingClass)) {
						paxBookingCol.add(paxBooking);
						bookingClassMap.put(bookingClass, paxBookingCol);
						cabinClassMap.put(cabinClass, bookingClassMap);
						destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
					} else {
						paxBookingCol = bookingClassMap.get(bookingClass);
						paxBookingCol.add(paxBooking);
						bookingClassMap.put(bookingClass, paxBookingCol);
						cabinClassMap.put(cabinClass, bookingClassMap);
						destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
					}
				}
			}

		}

		Map<String, Set<CabinBookingCountTo>> cabinBookingClassMap = new HashMap<String, Set<CabinBookingCountTo>>();
		for (Map.Entry<String, Map<String, Map<String, Set<PFSBookingClassList>>>> entry : destinationCabinPaxMap.entrySet()) {
			String destinationAirport = entry.getKey();
			Set<CabinBookingCountTo> cabinBookingCountList = new HashSet<CabinBookingCountTo>();
			Map<String, Map<String, Set<PFSBookingClassList>>> cabinClassDataMap = entry.getValue();
			for (Map.Entry<String, Map<String, Set<PFSBookingClassList>>> cabinClassMapEntry : cabinClassDataMap.entrySet()) {
				Integer cabinClassCount = 0;
				String cabinClass = cabinClassMapEntry.getKey();
				Map<String, Set<PFSBookingClassList>> bookingClassDataMap = cabinClassMapEntry.getValue();
				for (Map.Entry<String, Set<PFSBookingClassList>> bookingClassClassMapEntry : bookingClassDataMap.entrySet()) {
					CabinBookingCountTo cabinbookingCount = new CabinBookingCountTo();
					String bookingClass = bookingClassClassMapEntry.getKey();
					cabinbookingCount.setCabinClass(cabinClass);
					cabinbookingCount.setBookingClass(bookingClass);
					Set<PFSBookingClassList> paxListBookingClass = bookingClassClassMapEntry.getValue();
					List<PaxPFSTo> paxInfoList = new ArrayList<PaxPFSTo>();
					for (PFSBookingClassList PFSBookingClassList : paxListBookingClass) {
						PaxPFSTo paxInfo = new PaxPFSTo();
						paxInfo.setPaxFirstName(PFSBookingClassList.getPaxFirstName());
						paxInfo.setPaxLastName(PFSBookingClassList.getPaxLastName());
						paxInfo.setPnr(PFSBookingClassList.getPnr());
						paxInfoList.add(paxInfo);
						cabinClassCount = cabinClassCount + 1;
					}

					cabinbookingCount.setBookingCountStr(applyDisplayCountFormatter(cabinClass, paxInfoList.size()));

					cabinBookingCountList.add(cabinbookingCount);
				}
			}
			cabinBookingClassMap.put(destinationAirport, cabinBookingCountList);
		}
		pfsMetaDataDTO.setNumericByDestinationStr(getNumericDataByDestination(cabinBookingClassMap));
		return pfsMetaDataDTO;
	}

	private PFSMessageDataDTO buildSeatConfigurationData(PFSMessageDataDTO pfsMetaDataDTO, PFSDTO pfsDTO) {
		StringBuilder seatConfigStr = new StringBuilder("");
		Map<String, Integer> cabinCapacityMap = pfsDTO.getCabinCapacityMap();
		if (cabinCapacityMap != null && !cabinCapacityMap.isEmpty()) {
			for (String cabinClass : cabinCapacityMap.keySet()) {
				Integer capacity = cabinCapacityMap.get(cabinClass);
				seatConfigStr.append(String.format(DECIMAL_FORMAT_THREE, capacity)).append(cabinClass);
			}
		}
		pfsMetaDataDTO.setSeatConfiguration(seatConfigStr.toString());
		return pfsMetaDataDTO;
	}

	private PFSMessageDataDTO buildRBDConfigurationData(PFSMessageDataDTO pfsMetaDTO, PFSDTO pfsDTO) {
		Map<String, List<String>> rbdCabinBookingMap = new HashMap<String, List<String>>();
		StringBuilder classCodeRBDString = new StringBuilder(" ");

		List<String> cabinClassList = new ArrayList<String>();
		for (PFSPaxInfo pax : pfsDTO.getPassengerNameList()) {

			String cabinClass = pax.getCabinClass();
			String bookingClass = pax.getBookingClass();
			if (!cabinClassList.contains(cabinClass) && cabinClassList != null) {
				cabinClassList.add(cabinClass);
			}

			if (!rbdCabinBookingMap.containsKey(cabinClass)) {
				List<String> bookingClassList = new ArrayList<String>();
				bookingClassList.add(bookingClass);
				rbdCabinBookingMap.put(cabinClass, bookingClassList);
			} else {
				List<String> existingBookingClasses = rbdCabinBookingMap.get(cabinClass);
				if (!existingBookingClasses.contains(bookingClass)) {
					existingBookingClasses.add(bookingClass);
				}
				rbdCabinBookingMap.put(cabinClass, existingBookingClasses);
			}

		}

		for (String ccStr : cabinClassList) {
			classCodeRBDString.append(ccStr);
			classCodeRBDString.append(MessageComposerConstants.FWD);
			classCodeRBDString.append(ccStr);
			for (Map.Entry<String, List<String>> entry : rbdCabinBookingMap.entrySet()) {
				String classCode = entry.getKey();
				if (classCode.equals(ccStr)) {
					List<String> bookingClasses = entry.getValue();
					for (String fareBookingClass : bookingClasses) {
						if (!fareBookingClass.equals(classCode)) {
							classCodeRBDString.append(fareBookingClass);
						}
					}
				}

			}
			classCodeRBDString.append(MessageComposerConstants.SP);
		}
		pfsMetaDTO.setClassCodesRBD(classCodeRBDString.toString());
		return pfsMetaDTO;
	}

	private PFSMessageDataDTO buildNumericDataForEmptyPFS(PFSMessageDataDTO pfsMetaDataDTO, PFSDTO pfsDTO, String originAirport)
			throws ModuleException {

		Collection<CSPaxBookingCount> csPaxCountList = MsgbrokerUtils.getReservationBD().getCodeSharePaxCountByBookingClass(
				pfsDTO.getFlightNumber(), pfsDTO.getFlightDate(), pfsDTO.getCarrierCode());

		Map<String, Integer> bookingClassM = new HashMap<String, Integer>();
		Map<String, Map<String, Integer>> cabinClassM = new HashMap<String, Map<String, Integer>>();
		Map<String, Map<String, Map<String, Integer>>> destinationCabinPaxM = new HashMap<String, Map<String, Map<String, Integer>>>();

		if (csPaxCountList != null && !csPaxCountList.isEmpty()) {
			for (CSPaxBookingCount csPaxCount : csPaxCountList) {
				String segCode = csPaxCount.getSegmentCode();
				String csBookingCode = csPaxCount.getBookingClass();
				int paxCount = csPaxCount.getPaxCount();
				int gdsId = csPaxCount.getGdsId();

				String actualBc = TTYMessageUtil.getActualBCForExternalCsBC(gdsId, csBookingCode);
				String logicalCC = TTYMessageUtil.getLogicalCCByActualBookingCode(actualBc);
				if (!StringUtil.isNullOrEmpty(logicalCC)) {

					String[] elements = segCode.split("/");
					String destination = elements[elements.length - 1];

					if (!destinationCabinPaxM.containsKey(destination)) {
						bookingClassM = new HashMap<String, Integer>();
						cabinClassM = new HashMap<String, Map<String, Integer>>();
						bookingClassM.put(csBookingCode, paxCount);
						cabinClassM.put(logicalCC, bookingClassM);
						destinationCabinPaxM.put(destination, cabinClassM);

					} else {

						cabinClassM = destinationCabinPaxM.get(destination);
						if (!cabinClassM.containsKey(logicalCC)) {

							bookingClassM = new HashMap<String, Integer>();
							bookingClassM.put(csBookingCode, paxCount);
							cabinClassM.put(logicalCC, bookingClassM);
							destinationCabinPaxM.put(destination, cabinClassM);

						} else {

							bookingClassM = cabinClassM.get(logicalCC);
							bookingClassM.put(csBookingCode, paxCount);
							destinationCabinPaxM.put(destination, cabinClassM);
						}
					}

				}

			}

		}

		Map<String, Set<CabinBookingCountTo>> cabinBookingClassMap = new HashMap<String, Set<CabinBookingCountTo>>();
		for (Map.Entry<String, Map<String, Map<String, Integer>>> entry : destinationCabinPaxM.entrySet()) {
			String destinationAirport = entry.getKey();
			Set<CabinBookingCountTo> cabinBookingCountList = new HashSet<CabinBookingCountTo>();

			Map<String, Map<String, Integer>> cabinClassDataMap = entry.getValue();
			for (Map.Entry<String, Map<String, Integer>> cabinClassMapEntry : cabinClassDataMap.entrySet()) {

				String cabinClass = cabinClassMapEntry.getKey();
				Map<String, Integer> bookingClassDataMap = cabinClassMapEntry.getValue();
				for (Map.Entry<String, Integer> bookingClassClassMapEntry : bookingClassDataMap.entrySet()) {
					CabinBookingCountTo cabinbookingCount = new CabinBookingCountTo();
					String bookingClass = bookingClassClassMapEntry.getKey();
					cabinbookingCount.setCabinClass(cabinClass);
					cabinbookingCount.setBookingClass(bookingClass);

					int paxCount = bookingClassClassMapEntry.getValue();
					cabinbookingCount.setBookingCountStr(applyDisplayCountFormatter(cabinClass, paxCount));
					cabinBookingCountList.add(cabinbookingCount);
				}
			}
			cabinBookingClassMap.put(destinationAirport, cabinBookingCountList);
		}

		pfsMetaDataDTO.setNumericByDestinationStr(getNumericDataByDestination(cabinBookingClassMap));
		return pfsMetaDataDTO;
	}

	private String getNumericDataByDestination(Map<String, Set<CabinBookingCountTo>> cabinBookingClassMap) {
		StringBuilder numericByDestination = new StringBuilder("");
		if (cabinBookingClassMap != null && !cabinBookingClassMap.isEmpty()) {
			for (Map.Entry<String, Set<CabinBookingCountTo>> cabinBooingCountData : cabinBookingClassMap.entrySet()) {
				if (numericByDestination.toString().length() > 0) {
					numericByDestination.append(MessageComposerConstants.EOL);
				}
				StringBuilder businessClassBookingCountStr = new StringBuilder("");
				StringBuilder economyClassBookingCountStr = new StringBuilder("");
				String destAirport = cabinBooingCountData.getKey();
				Set<CabinBookingCountTo> cabinBookingCountLst = cabinBooingCountData.getValue();
				for (CabinBookingCountTo cabinBookingCountTo : cabinBookingCountLst) {
					if (BUSINESS_CLASS_PREFIX.equals(cabinBookingCountTo.getCabinClass())) {
						businessClassBookingCountStr.append(cabinBookingCountTo.getBookingCountStr());
						businessClassBookingCountStr.append(MessageComposerConstants.FWD);
					} else {
						economyClassBookingCountStr.append(cabinBookingCountTo.getBookingCountStr());
						economyClassBookingCountStr.append(MessageComposerConstants.FWD);
					}
				}
				if (businessClassBookingCountStr.toString().isEmpty()) {
					numericByDestination.append(destAirport);
					numericByDestination.append(MessageComposerConstants.SP);
					numericByDestination.append(economyClassBookingCountStr.toString().substring(0,
							economyClassBookingCountStr.toString().length() - 1));
				} else if (economyClassBookingCountStr.toString().isEmpty()) {
					numericByDestination.append(destAirport);
					numericByDestination.append(MessageComposerConstants.SP);
					numericByDestination.append(businessClassBookingCountStr.toString().substring(0,
							businessClassBookingCountStr.toString().length() - 1));
				} else {
					numericByDestination.append(destAirport);
					numericByDestination.append(MessageComposerConstants.SP);
					numericByDestination.append(businessClassBookingCountStr.toString());
					numericByDestination.append(economyClassBookingCountStr.toString().substring(0,
							economyClassBookingCountStr.toString().length() - 1));
				}

			}
		}

		return numericByDestination.toString();

	}

	private Map<Integer, PFSDTO> getPFSDTOPartByPart(PFSDTO pfsDTO) {
		Map<Integer, PFSDTO> partByPfsDTO = new HashMap<Integer, PFSDTO>();
		List<List<PFSPaxInfo>> partitionedList = getPaxInfoListPartition(pfsDTO.getPassengerNameList(), MAX_LINE_LENGTH_IATA);
		if (partitionedList != null) {
			Integer partNo = 1;
			for (List<PFSPaxInfo> list : partitionedList) {
				PFSDTO copyPfsDto = this.copyPFSDTO(pfsDTO);
				copyPfsDto.getPassengerNameList().addAll(list);
				partByPfsDTO.put(partNo, copyPfsDto);
				partNo++;
			}
		}

		return partByPfsDTO;
	}

	private <T> List<List<T>> getPaxInfoListPartition(List<T> list, final int L) {
		List<List<T>> parts = new ArrayList<List<T>>();
		final int N = list.size();
		for (int i = 0; i < N; i += L) {
			parts.add(new ArrayList<T>(list.subList(i, Math.min(N, i + L))));
		}
		return parts;
	}

	private PFSDTO copyPFSDTO(PFSDTO src) {
		PFSDTO pfsDto = new PFSDTO();
		pfsDto.setFlightNumber(src.getFlightNumber());
		pfsDto.setFlightDate(src.getFlightDate());
		pfsDto.setDepartureAirport(src.getDepartureAirport());
		pfsDto.setArrivalAirport(src.getArrivalAirport());
		pfsDto.setGdsCode(src.getGdsCode());
		pfsDto.setCarrierCode(src.getCarrierCode());

		return pfsDto;

	}

	public List<PFSMessageDataDTO> preparePFSMessageDataDTO(PFSDTO pfsDTO, String addressElement, String communicationElement)
			throws ModuleException {
		List<PFSMessageDataDTO> pfsMetaDataDTOList = null;
		if (!isComposeEmptyPfs) {
			pfsMetaDataDTOList = this.formatPFSMailNew(pfsDTO, addressElement, communicationElement);
		} else {
			pfsMetaDataDTOList = this.formatEmptyPFSMail(pfsDTO, addressElement, communicationElement);
		}
		return pfsMetaDataDTOList;
	}

	private String applyDisplayCountFormatter(String cabinClass, int count) {
		String countStr;
		if (BUSINESS_CLASS_PREFIX.equals(cabinClass)) {
			countStr = String.format(DECIMAL_FORMAT_TWO, count);
		} else {
			countStr = String.format(DECIMAL_FORMAT_THREE, count);
		}
		return countStr;
	}
}
