package com.isa.thinair.msgbroker.core.persistence.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;

import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.msgbroker.core.bl.TypeBMessageConstants;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminJdbcDAO;


public class AdminDaoJDBCImpl extends PlatformBaseJdbcDAOSupport implements AdminJdbcDAO {
	
	@Override
	public String getPnrByExtRecordLocator(String extRecordLocator) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (String) template.queryForObject("SELECT PNR FROM T_RESERVATION WHERE EXTERNAL_REC_LOCATOR ='" + extRecordLocator
				+ "'", String.class);
	}
	
	@Override
	public boolean isReservationExists(String pnr) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String existingPnr = (String) template.queryForObject("SELECT PNR FROM T_RESERVATION WHERE PNR ='" + pnr + "'",
				String.class);
		if (existingPnr != null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String getTypeBElementResFlag(String airimpVersion,
			String primaryElement, String secondaryElement) {
		try {
			JdbcTemplate template = new JdbcTemplate(getDataSource());
			String sql = "SELECT RES_FLAG" + " FROM  T_GDS_TYPEB_ELEMENT_MATRIX"
					+ " WHERE AIRIMP_VER = '" + airimpVersion + "' "
					+ " AND PRIM_ELEMENT = '" + primaryElement + "'"
					+ " AND SECOND_ELEMENT = '" + secondaryElement + "'";
			Object resFlag = template.queryForObject(sql, String.class);
	
			if (resFlag != null) {
				return (String) resFlag;
			}
		} catch(Exception ex) {
			//no nothing
		}
		return null;
	}
	
	@Override
	public Integer getOtherAirlineConfirmedSegmentCount(String pnr) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String count = (String) template.queryForObject("select count(*) from t_pnr_other_airline_segment where status <> '"
				+ TypeBMessageConstants.StatusCodes.HX + "' and pnr = '" + pnr + "'", String.class);
		if (count != null) {
			return Integer.parseInt(count);
		} else {
			return 0;
		}
	}
}
