package com.isa.thinair.msgbroker.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlight;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlightSegment;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlightSegmentMapElement;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSMeal;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassenger;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassportInformation;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSSSRRequest;
import com.isa.connectivity.profiles.dcs.v1.common.dto.PassengersMapElement;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRQ;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airreservation.api.dto.adl.ADLDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLPassengerData;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.FlightInformation;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.util.PNLADLComposerUtil;

//TODO make this more common
public class WSADLComposer {

	public DCSADLInfoRQ composeADL(ADLDTO adlElements) throws ModuleException {

		FlightInformation adlFlightInfo = adlElements.getFlightInfo();
		Airport airport = MsgbrokerUtils.getAirportBD().getAirport(adlFlightInfo.getBoardingAirport());
		DCSADLInfoRQ dcsadlInfoRQ = null;
		if (airport.isWSAADCSEnabled()) {
			dcsadlInfoRQ = new DCSADLInfoRQ();
			DCSFlight flightInfo = this.composeFlightInformation(adlFlightInfo);
			dcsadlInfoRQ.getAddedSegmentInfo().addAll(
					this.composePassengers(adlElements.getPassengerData(), adlElements.getFlightInfo(), AdlActions.A,
							adlElements.getClassCodes()));
			dcsadlInfoRQ.getDeletedSegmentInfo().addAll(
					this.composePassengers(adlElements.getPassengerData(), adlElements.getFlightInfo(), AdlActions.D,
							adlElements.getClassCodes()));
			dcsadlInfoRQ.getUpdatedSegmentInfo().addAll(
					this.composePassengers(adlElements.getPassengerData(), adlElements.getFlightInfo(), AdlActions.C,
							adlElements.getClassCodes()));
			dcsadlInfoRQ.setFlightInfo(flightInfo);
		}
		return dcsadlInfoRQ;
	}

	private List<DCSFlightSegmentMapElement> composePassengers(Map<String, Map<String, ADLPassengerData>> passengerData,
			FlightInformation adlFlightInfo, AdlActions action, Map<String, List<String>> classCodes) throws ModuleException {
		List<DCSFlightSegmentMapElement> dcsfsmes = new ArrayList<DCSFlightSegmentMapElement>();
		int elementCount = 0;
		for (String destinationAirport : passengerData.keySet()) {
			Map<String, ADLPassengerData> cosPassengers = passengerData.get(destinationAirport);
			DCSFlightSegment dcsfs = new DCSFlightSegment();
			dcsfs.setSegmentCode(this.findMatchingFlightSegmentcode(adlFlightInfo.getFlightId(),
					adlFlightInfo.getBoardingAirport(), destinationAirport));
			for (String cos : cosPassengers.keySet()) {
				String cabinClass = cos;
				if (classCodes != null && !classCodes.isEmpty()) {
					for (Entry<String, List<String>> entry : classCodes.entrySet()) {
						if (entry.getValue().contains(cos)) {
							cabinClass = entry.getKey();
							break;
						}
					}
				}
				DCSFlightSegmentMapElement dcsfsme = new DCSFlightSegmentMapElement();
				dcsfsme.setCabinClassCode(cabinClass);

				ADLPassengerData adlPassengerData = cosPassengers.get(cos);

				switch (action) {
				case A:
					if (adlPassengerData.getAddedPassengers() != null) {
						Map<String, List<PassengerInformation>> pnrWiseAddedPassengers = PNLADLComposerUtil
								.generatePnrWisePassengers(adlPassengerData.getAddedPassengers());
						dcsfs.getPnrAndPassengers().addAll(this.composePassengerMapElements(pnrWiseAddedPassengers, true));
					}
					break;
				case D:
					if (adlPassengerData.getDeletedPassengers() != null) {
						Map<String, List<PassengerInformation>> pnrWiseDeletedPassengers = PNLADLComposerUtil
								.generatePnrWisePassengers(adlPassengerData.getDeletedPassengers());
						dcsfs.getPnrAndPassengers().addAll(this.composePassengerMapElements(pnrWiseDeletedPassengers, false));
					}
					break;
				case C:
					if (adlPassengerData.getChangedPassengers() != null) {
						Map<String, List<PassengerInformation>> pnrWiseChangedPassengers = PNLADLComposerUtil
								.generatePnrWisePassengers(adlPassengerData.getChangedPassengers());
						dcsfs.getPnrAndPassengers().addAll(this.composePassengerMapElements(pnrWiseChangedPassengers, true));
					}
					break;
				}
				if (elementCount < dcsfs.getPnrAndPassengers().size()) {
					elementCount = dcsfs.getPnrAndPassengers().size();
					dcsfsme.getFlightSegments().add(dcsfs);
					dcsfsmes.add(dcsfsme);
				}
			}

		}
		return dcsfsmes;
	}

	private ArrayList<PassengersMapElement> composePassengerMapElements(
			Map<String, List<PassengerInformation>> pnrWisePassengers, boolean addRemarks) {
		ArrayList<PassengersMapElement> pmes = new ArrayList<PassengersMapElement>();
		for (String pnr : pnrWisePassengers.keySet()) {
			PassengersMapElement pme = new PassengersMapElement();
			pme.setPnr(pnr);
			List<PassengerInformation> pnrPassengers = pnrWisePassengers.get(pnr);
			DCSPassenger dp = null;
			boolean ccDetailsAdded = false;
			for (PassengerInformation pnrPassenger : pnrPassengers) {
				dp = this.composeDCSPassenger(pnrPassenger, addRemarks);
				pme.getPassengers().add(dp);
				// Adding credit card details once per reservation
				if (pnrPassenger.getCcDigits() != null && !pnrPassenger.getCcDigits().equals("") && !ccDetailsAdded && addRemarks) {
					pme.setCcDigits("CC/XXXX-XXXX-XXXX-" + pnrPassenger.getCcDigits());
					ccDetailsAdded = true;
				}
			}

			pmes.add(pme);
		}
		return pmes;
	}

	private DCSPassenger composeDCSPassenger(PassengerInformation pnrPassenger, boolean addRemarks) {
		DCSPassenger dp = new DCSPassenger();
		dp.setTitle(pnrPassenger.getTitle());
		dp.setFirstName(pnrPassenger.getFirstName());
		dp.setLastName(pnrPassenger.getLastName());
		dp.setPaxType(pnrPassenger.getPaxType());
		dp.setGroupID(pnrPassenger.getGroupId());
		dp.setStandByPax(pnrPassenger.isStandByPassenger());
		dp.setNationality(pnrPassenger.getNationality());
		dp.setETicket(pnrPassenger.getEticketNumber());
		if (pnrPassenger.getCoupon() != null){
			dp.setCouponNumber(pnrPassenger.getCoupon().toString());
		} else {
			dp.setCouponNumber("1");
		}
		DCSPassportInformation dpi = new DCSPassportInformation();
		if (pnrPassenger.getFoidIssuedCountry() != null) {
			dpi.setIssuedCountry(pnrPassenger.getFoidIssuedCountry());
		}
		if (pnrPassenger.getFoidExpiry() != null) {
			dpi.setPassportExpiryDate(pnrPassenger.getFoidExpiry());
		}
		if (pnrPassenger.getFoidNumber() != null) {
			dpi.setPassportNumber(pnrPassenger.getFoidNumber());
		}
		dp.setPassportInfo(dpi);
		dp.setDateOfBirth(pnrPassenger.getDob());
		if (addRemarks) {
			if (pnrPassenger.getInfant() != null) {
				DCSPassenger dpInfant = new DCSPassenger();
				dpInfant.setTitle(pnrPassenger.getInfant().getTitle());
				dpInfant.setFirstName(pnrPassenger.getInfant().getFirstName());
				dpInfant.setLastName(pnrPassenger.getInfant().getLastName());
				dpInfant.setPaxType(pnrPassenger.getInfant().getPaxType());
				dpInfant.setGroupID(pnrPassenger.getGroupId());
				dpInfant.setDateOfBirth(pnrPassenger.getInfant().getDob());
				dpInfant.setETicket(pnrPassenger.getInfant().getEticketNumber());
				dpInfant.setNationality(pnrPassenger.getInfant().getNationality());
				if(pnrPassenger.getInfant().getCoupon() != null)
					dpInfant.setCouponNumber(String.valueOf(pnrPassenger.getInfant().getCoupon()));

				DCSPassportInformation infantdpi = new DCSPassportInformation();
				if (pnrPassenger.getFoidIssuedCountry() != null) {
					infantdpi.setIssuedCountry(pnrPassenger.getInfant().getFoidIssuedCountry());
				}
				if (pnrPassenger.getFoidExpiry() != null) {
					infantdpi.setPassportExpiryDate(pnrPassenger.getInfant().getFoidExpiry());
				}
				if (pnrPassenger.getFoidNumber() != null) {
					infantdpi.setPassportNumber(pnrPassenger.getInfant().getFoidNumber());
				}
				dpInfant.setPassportInfo(infantdpi);
				dp.setInfant(dpInfant);
			}
			if (pnrPassenger.getMeals() != null) {
				for (AncillaryDTO meal : pnrPassenger.getMeals()) {
					DCSMeal dcsMeal = new DCSMeal();
					dcsMeal.setIataMealCode(meal.getCode());
					dcsMeal.setMealDescription(meal.getDescription());
					dp.getMeals().add(dcsMeal);
				}
			}
			if (pnrPassenger.getSeats() != null) {
				for (AncillaryDTO seat : pnrPassenger.getSeats()) {
					dp.getSeatNumbers().add(seat.getDescription());
				}
			}
			if (pnrPassenger.getBaggages() != null) {
				dp.getBaggageInformation().add(pnrPassenger.getBaggages().getDescription() + "kg");
			}
			if (pnrPassenger.getSsrs() != null) {
				for (AncillaryDTO ssr : pnrPassenger.getSsrs()) {
					DCSSSRRequest dssr = new DCSSSRRequest();
					dssr.setIataSSRCode(ssr.getCode());
					dssr.setSsrDescription(ssr.getDescription());
					dp.getSsrRequests().add(dssr);
				}
			}
		}
		return dp;
	}

	private DCSFlight composeFlightInformation(FlightInformation adlFlightInfo) throws ModuleException {
		DCSFlight flightInfo = new DCSFlight();
		flightInfo.setDepartureAirportCode(adlFlightInfo.getBoardingAirport());
		flightInfo.setFlightNumber(adlFlightInfo.getFlightNumber());

		FlightBD flightBD = MsgbrokerUtils.getFlightBD();
		Flight flightObj = flightBD.getFlight(adlFlightInfo.getFlightId());
		flightInfo.setOriginAirportCode(flightObj.getOriginAptCode());

		Date departureDateZulu = null;
		if (flightObj != null) {
			for (FlightSegement fs : (flightObj.getFlightSegements())) {
				if (fs.getEstTimeDepatureZulu() != null && fs.getSegmentCode().startsWith(adlFlightInfo.getBoardingAirport())) {
					departureDateZulu = fs.getEstTimeDepatureZulu();
					break;
				}
			}
		}
		flightInfo.setDepartureTime(departureDateZulu);

		return flightInfo;
	}

	public String findMatchingFlightSegmentcode(int flightId, String departure, String arrival) throws ModuleException {
		FlightBD flightBD = MsgbrokerUtils.getFlightBD();
		Collection<FlightSegement> flightSegments = flightBD.getSegments(flightId);
		for (FlightSegement flightSegment : flightSegments) {
			if (flightSegment.getSegmentCode().startsWith(departure) && flightSegment.getSegmentCode().endsWith(arrival)) {
				return flightSegment.getSegmentCode();
			}
		}
		// This can not be happened. but return this just in case
		return departure + "/" + arrival;
	}
}
