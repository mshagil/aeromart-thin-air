package com.isa.thinair.msgbroker.core.bl;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBAnalyseRS;
import com.isa.thinair.msgbroker.api.dto.BookingRequestDTO;
import com.isa.thinair.msgbroker.api.dto.NameDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDTO;
import com.isa.thinair.msgbroker.api.dto.SSRInfantDTO;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.msgbroker.core.util.LookupUtil;

import java.util.List;

public class GdsDtoTransformer {
	private GdsDtoTransformer() {
	}

	public static TypeBAnalyseRS toTypeBAnalyseRS(BookingRequestDTO bookingRequest) throws ModuleException {
		TypeBAnalyseRS typeBAnalyseRS = new TypeBAnalyseRS();
		
		typeBAnalyseRS.setMessageError(false);
		int paxCount = 0;
		int infantCount = 0;

		ServiceClientReqDaoJDBC serviceClientReqDao = LookupUtil.lookupServiceClientReqDAO();
		String gdsCode = serviceClientReqDao.getServiceClientForReqTypeTTY(bookingRequest.getOriginatorAddress(),
				bookingRequest.getMessageIdentifier());
		Gds gds = LookupUtil.getGdsServiceBD().getGDSByCode(gdsCode);

		typeBAnalyseRS.setGdsCarrierCode(gds.getCarrierCode());
		typeBAnalyseRS.setSegments(bookingRequest.getBookingSegmentDTOs());
		
		for (NameDTO nameDTO : (List<NameDTO>)bookingRequest.getNewNameDTOs()) {
			if (!nameDTO.isInfant()) {
				paxCount++;
			}
		}

		for (SSRDTO ssrdto : bookingRequest.getSsrDTOs()) {
			if (ssrdto instanceof SSRInfantDTO) {
				infantCount++;
			}
		}
		
		typeBAnalyseRS.setPaxCount(paxCount);
		typeBAnalyseRS.setInfantCount(infantCount);

		return typeBAnalyseRS;
	}
}
