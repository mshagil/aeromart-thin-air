package com.isa.thinair.msgbroker.core.config;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

import java.io.Serializable;
import java.util.List;

public class TypeBMessageStructure implements Serializable {

	private List<SyncMessageType> syncMessageTypes;
	
	private List<ResMessageType> resMessageTypes;
	
	private List<CodeshareMessageType> codeshareMessageTypes;

	public static class SyncMessageType {
		private GDSInternalCodes.TypeBSyncType syncType;
		private List<SyncMessageConfig> syncMsgConfigs;

		public GDSInternalCodes.TypeBSyncType getSyncType() {
			return syncType;
		}

		public void setSyncType(GDSInternalCodes.TypeBSyncType syncType) {
			this.syncType = syncType;
		}

		public List<SyncMessageConfig> getSyncMsgConfigs() {
			return syncMsgConfigs;
		}

		public void setSyncMsgConfigs(List<SyncMessageConfig> syncMsgConfigs) {
			this.syncMsgConfigs = syncMsgConfigs;
		}
	}
	
	public static class ResMessageType {
		
		private List<ResMessageConfig> resMsgConfigs;

		public List<ResMessageConfig> getResMsgConfigs() {
			return resMsgConfigs;
		}

		public void setResMsgConfigs(List<ResMessageConfig> resMsgConfigs) {
			this.resMsgConfigs = resMsgConfigs;
		}		
	}
	
	public static class CodeshareMessageType {
		
		private List<ResMessageConfig> resMsgConfigs;

		public List<ResMessageConfig> getResMsgConfigs() {
			return resMsgConfigs;
		}

		public void setResMsgConfigs(List<ResMessageConfig> resMsgConfigs) {
			this.resMsgConfigs = resMsgConfigs;
		}		
	}

	public static class SyncMessageConfig {
		private GDSExternalCodes.SyncMessageIdentifier syncMessageIdentifier;
		private MessageConfig messageConfig;

		public SyncMessageConfig() {
		}

		public GDSExternalCodes.SyncMessageIdentifier getSyncMessageIdentifier() {
			return syncMessageIdentifier;
		}

		public void setSyncMessageIdentifier(GDSExternalCodes.SyncMessageIdentifier syncMessageIdentifier) {
			this.syncMessageIdentifier = syncMessageIdentifier;
		}

		public MessageConfig getMessageConfig() {
			return messageConfig;
		}

		public void setMessageConfig(MessageConfig messageConfig) {
			this.messageConfig = messageConfig;
		}
	}
	
	public static class ResMessageConfig {

		private MessageConfig messageConfig;

		public MessageConfig getMessageConfig() {
			return messageConfig;
		}

		public void setMessageConfig(MessageConfig messageConfig) {
			this.messageConfig = messageConfig;
		}
	}

	public static class MessageConfig {
		private MessageIdentifierConfig msgIdConfig;
		private boolean recLocSender;
		private List<SegmentConfig> segmentConfig;

		public MessageIdentifierConfig getMsgIdConfig() {
			return msgIdConfig;
		}

		public void setMsgIdConfig(MessageIdentifierConfig msgIdConfig) {
			this.msgIdConfig = msgIdConfig;
		}

		public boolean isRecLocSender() {
			return recLocSender;
		}

		public void setRecLocSender(boolean recLocSender) {
			this.recLocSender = recLocSender;
		}

		public List<SegmentConfig> getSegmentConfig() {
			return segmentConfig;
		}

		public void setSegmentConfig(List<SegmentConfig> segmentConfig) {
			this.segmentConfig = segmentConfig;
		}
	}

	public static class MessageIdentifierConfig {
		private boolean msgId;
		private String id;

		public boolean isMsgId() {
			return msgId;
		}

		public void setMsgId(boolean msgId) {
			this.msgId = msgId;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
	}

	public static class SegmentConfig {
		// pre-requisites
		private String actionCode;

		// config
		private boolean departureAndArrivalTime;

		public String getActionCode() {
			return actionCode;
		}

		public void setActionCode(String actionCode) {
			this.actionCode = actionCode;
		}

		public boolean isDepartureAndArrivalTime() {
			return departureAndArrivalTime;
		}

		public void setDepartureAndArrivalTime(boolean departureAndArrivalTime) {
			this.departureAndArrivalTime = departureAndArrivalTime;
		}
	}


	public List<SyncMessageType> getSyncMessageTypes() {
		return syncMessageTypes;
	}

	public void setSyncMessageTypes(List<SyncMessageType> syncMessageTypes) {
		this.syncMessageTypes = syncMessageTypes;
	}

	public List<ResMessageType> getResMessageTypes() {
		return resMessageTypes;
	}

	public void setResMessageTypes(List<ResMessageType> resMessageTypes) {
		this.resMessageTypes = resMessageTypes;
	}

	public List<CodeshareMessageType> getCodeshareMessageTypes() {
		return codeshareMessageTypes;
	}

	public void setCodeshareMessageTypes(List<CodeshareMessageType> codeshareMessageTypes) {
		this.codeshareMessageTypes = codeshareMessageTypes;
	}

}
