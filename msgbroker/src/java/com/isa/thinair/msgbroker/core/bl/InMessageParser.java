/**
 * 
 */
package com.isa.thinair.msgbroker.core.bl;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.msgbroker.api.dto.BookingRequestDTO;
import com.isa.thinair.msgbroker.api.dto.BookingSegmentDTO;
import com.isa.thinair.msgbroker.api.dto.ChangedNamesDTO;
import com.isa.thinair.msgbroker.api.dto.InNacDTO;
import com.isa.thinair.msgbroker.api.dto.NameDTO;
import com.isa.thinair.msgbroker.api.dto.OSIPhoneNoDTO;
import com.isa.thinair.msgbroker.api.dto.OtherServiceInfoDTO;
import com.isa.thinair.msgbroker.api.dto.RecordLocatorDTO;
import com.isa.thinair.msgbroker.api.dto.SSRChildDTO;
import com.isa.thinair.msgbroker.api.dto.SSRCreditCardDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSREmergencyContactDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRFoidDTO;
import com.isa.thinair.msgbroker.api.dto.SSRInfantDTO;
import com.isa.thinair.msgbroker.api.dto.SSRMinorDTO;
import com.isa.thinair.msgbroker.api.dto.SSROSAGDTO;
import com.isa.thinair.msgbroker.api.dto.SSRPassportDTO;
import com.isa.thinair.msgbroker.api.dto.SegmentDTO;
import com.isa.thinair.msgbroker.api.dto.UnchangedNamesDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.core.bl.avs.AVSMessageParser;
import com.isa.thinair.msgbroker.core.bl.ssm.SSMASMessageParser;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.MessageRequestProcessingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author sanjeewaf, manoj
 */
public class InMessageParser {
    
    GDSMessageParser gdsMessageParser ;
    
    private String sStatus;
    
    /** Constant string for ASM/SSM NAC Messages. */
    private static final String ASM_SSM_NAC_TITLE = "~NAC SSM/ASM REJECT";
    
    private static final String NAC = "NAC";

	private static Log log = LogFactory.getLog(InMessageParser.class);
    
    
    public MessageParserResponseDTO parseMessage(InMessage inMessage) {
        try {

        	String messageText = inMessage.getInMessageText();
			if (messageText != null) {
				String inMsg = messageText;
				inMsg = inMsg.replaceAll("\r", "");
				String[] messageTokens = inMsg.split("\n");
				String possibleMidElement = null;
				String firstElementWithoutAddresses = null;
				if (messageTokens != null && messageTokens.length > 2) {
					firstElementWithoutAddresses = messageTokens[0];
					possibleMidElement = messageTokens[2];
				}
				if (possibleMidElement != null && possibleMidElement.trim().equalsIgnoreCase(ASM_SSM_NAC_TITLE)) {
					return NACMessageParser.parseNacMessage(messageText);
				} else if ((firstElementWithoutAddresses != null && firstElementWithoutAddresses.trim().equalsIgnoreCase(NAC))
						|| (possibleMidElement != null && possibleMidElement.trim().equalsIgnoreCase(NAC))) {
					return NACMessageParser.parseNacResMessage(messageText);
				} else if (Constants.ServiceTypes.SSM.equals(inMessage.getMessageType())
						|| Constants.ServiceTypes.ASM.equals(inMessage.getMessageType())) {
					SSMASMessageParser ssmAsmMes = new SSMASMessageParser(inMessage.getMessageType());
					ssmAsmMes.setSenderAddress(inMessage.getSentTTYAddress());
					ssmAsmMes.setRecipeintAddress(inMessage.getRecipientTTYAddress());
					return ssmAsmMes.parseMessage(inMessage.getInMessageText());
				} else if (Constants.ServiceTypes.AVS.equals(inMessage.getMessageType())) {
					AVSMessageParser avsMessageParser = new AVSMessageParser();
					return avsMessageParser.parseMessage(inMessage.getInMessageText());
				} else {
					gdsMessageParser = new GDSMessageParser();
					return gdsMessageParser.parseMessage(inMessage);
				}

			} else {
				sStatus = "Empty text";
				throw new ModuleException("message.text.empty");

			}
        } catch (ModuleException me) {
			log.error("Exception occurred while processing msg: " + inMessage.getInMessageText(), me);

			MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
            oMessageParserResponseDTO.setRequestDTO(null);
            oMessageParserResponseDTO.setSuccess(false);
            oMessageParserResponseDTO.setReasonCode(StringUtil.trimStringMessage(me.getExceptionCode(),90));
            oMessageParserResponseDTO.setStatus(sStatus);
            return oMessageParserResponseDTO;
        } catch (RuntimeException e) {
			log.error("Exception occurred while processing msg: " + inMessage.getInMessageText(), e);

			MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
			oMessageParserResponseDTO.setRequestDTO(null);
			oMessageParserResponseDTO.setSuccess(false);
			oMessageParserResponseDTO.setReasonCode("message.text.empty");
			oMessageParserResponseDTO.setStatus("Empty text");
			return oMessageParserResponseDTO;
        } catch (Exception x) {
        	log.error("Exception occurred while processing msg: " + inMessage.getInMessageText(), x);
            MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
            oMessageParserResponseDTO.setRequestDTO(null);
            oMessageParserResponseDTO.setSuccess(false);
            String errorMsg = x.getMessage();
            if(StringUtil.isNullOrEmpty(errorMsg)){
            	errorMsg= "Encountered Parser Error";
            }            
            oMessageParserResponseDTO.setReasonCode(StringUtil.trimStringMessage(errorMsg,90));
            oMessageParserResponseDTO.setStatus(sStatus);
            return oMessageParserResponseDTO;      	
		}
        
    }
    
    public MessageParserResponseDTO parseMessage(String messageText) {
        try {            
			if (messageText != null) {
				String inMsg = messageText;
				inMsg = inMsg.replaceAll("\r", "");
				String[] messageTokens = inMsg.split("\n");
				String possibleMidElement = null;
				String firstElementWithoutAddresses = null;
				if (messageTokens != null && messageTokens.length > 2) {
					firstElementWithoutAddresses = messageTokens[0];
					possibleMidElement = messageTokens[2];
				}
				
				String[] resultStr = MessageRequestProcessingUtil.getMessageIdentifier(messageText, true);
				
				if (possibleMidElement != null && possibleMidElement.trim().equalsIgnoreCase(ASM_SSM_NAC_TITLE)) {
					return NACMessageParser.parseNacMessage(messageText);
				} else if ((firstElementWithoutAddresses != null && firstElementWithoutAddresses.trim().equalsIgnoreCase(NAC))
						|| (possibleMidElement != null && possibleMidElement.trim().equalsIgnoreCase(NAC))) {
					return NACMessageParser.parseNacResMessage(messageText);
				} else if (Constants.ServiceTypes.SSM.equals(resultStr[0]) || Constants.ServiceTypes.ASM.equals(resultStr[0])) {
					SSMASMessageParser ssmAsmMes = new SSMASMessageParser(resultStr[0]);
					ssmAsmMes.setSenderAddress(resultStr[2]);
					ssmAsmMes.setRecipeintAddress(resultStr[3]);
					return ssmAsmMes.parseMessage(resultStr[1]);				
				} else if (Constants.ServiceTypes.AVS.equals(resultStr[0])) {
					AVSMessageParser avsMessageParser = new AVSMessageParser();
					return avsMessageParser.parseMessage(messageText);
				} else {
					gdsMessageParser = new GDSMessageParser();
					return gdsMessageParser.parseMessage(messageText);
				}

			} else {
				sStatus = "Empty text";
				throw new ModuleException("message.text.empty");

			}
        } catch (ModuleException me) {
            MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
            oMessageParserResponseDTO.setRequestDTO(null);
            oMessageParserResponseDTO.setSuccess(false);
            oMessageParserResponseDTO.setReasonCode(StringUtil.trimStringMessage(me.getExceptionCode(),90));
            oMessageParserResponseDTO.setStatus(sStatus);
            return oMessageParserResponseDTO;
        } catch (Exception e) {
            MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
            oMessageParserResponseDTO.setRequestDTO(null);
            oMessageParserResponseDTO.setSuccess(false);
            String errorMsg = e.getMessage();
            if(StringUtil.isNullOrEmpty(errorMsg)){
            	errorMsg= "Encountered Parser Error";
            }
            oMessageParserResponseDTO.setReasonCode(StringUtil.trimStringMessage(errorMsg,90));
            oMessageParserResponseDTO.setStatus(sStatus);
            return oMessageParserResponseDTO;            
        }
        
    }
    
    public static void main(String[] args) throws Exception {
        
        
        String msg126 = "";
        msg126 = "SHJMMG9\n";
        msg126 += ".HDQBB1S 150546\n";
        msg126 += "HDQ1S IQSRWY/PD5C/99999999\n";
        msg126 += "1ASAS/ASAS\n";
        msg126 += "G9585Y23SEP SHJCMB HK1\n";
        msg126 += "SSR OTHS YY OSAG1245365D\n";
        msg126 += "OSI G9 NXTM TK-NONE\n";
        
        
        String msg1= "";
        msg1= "QP SOFMSH1\n";
        msg1 += ".HDQBB1S 010420Z\n";
        msg1 += "~NAC SSM/ASM REJECT\n";
        msg1 += "INVALID SUB-MSG ACTION CODE\n";
        msg1 += "ERROR PROGRAM: KSB0   NUMBER: 007\n";
        msg1 += "SSM\n";
        msg1 += "LT\n";
        msg1 += "01OCT22002E001\n";
        msg1 += "NEW\n";
        msg1 += "G9625\n";
        msg1 += "G9625\n";
        msg1 += "05NOV 06NOV 1234567\n";
        msg1 += "G 320 S\n";
        msg1 += "SHJ1945 CMB2255\n";
        msg1 += "END REJECT\n";
        
        String sMessage5 = "";
        sMessage5 = "SHJMMG9\n";
        sMessage5 += ".SHJMMG9 040602\n";
        sMessage5 += "HDQ1S XXXADX/PD5C/99999999\n";
        sMessage5 += "1SANJEE/DILANMR 1SANJEE/CHAMIMRS 1TEST/TESTMRS 2WILL/JOHNMR/ANNEMRS\n";
        sMessage5 += "1NEMITHA/DULINAMSTR 1ROSHAN/HASHANMR 2DAVI/FDSFSDMR/ANE\n";
        sMessage5 += "G9789V30NOV SHJCMB HK9\n";
        sMessage5 += "G9789V28NOV SHJCMB NN6/0905 0955\n";
        sMessage5 += "SSR WCHR G9 NN1 SHJCMB0789V28NOV-1ROSHAN/HASHANMR\n";
        sMessage5 += "SSR INFT G9 NN1 SHJCMB0789V28NOV-1SANJEE/DILANMR.DULA/DULA 12MTHS\n";
        sMessage5 += "SSR INFT G9 NN1 SHJCMB0789V28NOV-1SANJEE/CHAMIMRS.SANA/SANA 12MTHS\n";
        sMessage5 += "SSR INFT G9 NN1 SHJCMB0789V28NOV-1TEST/TESTMRS.BANA/BANA 14MTHS\n";
        sMessage5 += "OSI G9 CTCP CMB94552223609\n";
        
        String msg2= "";
        msg2= "QP SHJMMG9\n";
        msg2 += ".HDQBB1S 191915Z\n";
        msg2 += "~NAC SSM/ASM REJECT\n";
        msg2 += "CLS CODE ERROR \n";
        msg2 += "ERROR PROGRAM: KSA4   NUMBER: 006\n";
        msg2 += "SSM\n";
        msg2 += "19NOV00041E001\n";
        msg2 += "NEW\n";
        msg2 += "G9040\n";
        msg2 += "18DEC08 21DEC08 567\n";
        msg2 += "G 320 4RAEDAFGDSGQIJJQMINEOQG9QWESESTAXTETTYWZZR\n";
        msg2 += "SHJ0300 MCT0355\n";
        
        String msg3= "";
        msg3="QP SHJMMG9 \n";
        msg3 +=".HDQBB1S 191915Z\n";
        msg3 +="~NAC SSM/ASM REJECT\n";
        msg3 +="CLS CODE ERROR\n";
        msg3 +="ERROR PROGRAM: KSA4   NUMBER: 006\n";
        msg3 +="SSM\n";
        msg3 +="19NOV00042E001\n";
        msg3 +="NEW\n";
        msg3 +="G9034\n";
        msg3 +="28NOV08 29NOV08 67\n";
        msg3 +="J 320 4RAEDAFGDSGQIJJQMINEOQG9QWESESTAXTETTYWZZR\n";
        msg3 +="MCT0300 SHJ0355\n";
        msg3 +="END REJECT\n";
         
        
        
        String msg4= "";
        msg4="SHJMMG9 \n";
        msg4 +=".HDQBB1S 210452\n";
        msg4 +="HDQ1S LNQAHE/PD5C/99999999\n";
        msg4 +="1SANJEE/DILANMR 1SANJEE/CHAMIMRS 1TEST/TESTMRS 2WILL/JOHNMR/ANNEMRS\n";
        msg4 +="1NEMITHA/DULINAMSTR 1ROSHAN/HASHANMR 2DAVID/FDSFDMR/ANE\n";
        msg4 +="G9898K06DEC SHJCCJ NN9/1800 1830/1\n";
        msg4 +="OSI G9 NXTM RL-NONE\n";
        msg4 +="OSI G9 CTCT CMB1242151 A\n";
      
        
        final String MESSAGE_TO_BE_PASSED = msg4;
        
        InMessageParser inMessageParser = new InMessageParser();
        MessageParserResponseDTO  parserResponseDTO =  inMessageParser.parseMessage(MESSAGE_TO_BE_PASSED);
        if (parserResponseDTO.isSuccess()) {
            Object requestObj = parserResponseDTO.getRequestDTO();
            if (requestObj instanceof BookingRequestDTO) {
                
                BookingRequestDTO   bookingRequestDTO = (BookingRequestDTO) requestObj;
                if (bookingRequestDTO != null && bookingRequestDTO.isGroupBooking()) {
                    System.out.println("!!XXXXXXXXXXXXXXXXXXXXXXXX Group booking XXXXXXXXXXXXXXXXXXXXXXXX\n");
                }
                System.out.println("Raw message \n" + MESSAGE_TO_BE_PASSED + "\nEnd of msg\n ---------------\n");
                if (bookingRequestDTO != null) {
                    System.out.println("Sender = " + bookingRequestDTO.getOriginatorAddress());
//                    System.out.println("Reciver = " + bookingRequestDTO.getResponderAddress());
                    System.out.println("CommunicationReference = " + bookingRequestDTO.getCommunicationReference());
                    System.out.println("MessageIdentifier = " + bookingRequestDTO.getMessageIdentifier());

                    if (bookingRequestDTO != null && bookingRequestDTO.getOriginatorRecordLocator() != null) {
                        RecordLocatorDTO originatorRL = bookingRequestDTO.getOriginatorRecordLocator();

                        System.out.println("---------ORiginator RecordLocator begin------------");
                        System.out.println("Booking office :" + originatorRL.getBookingOffice());
                        System.out.println("PnrReference :" + originatorRL.getPnrReference());
                        System.out.println("Office Code  :" + originatorRL.getTaOfficeCode());
                        System.out.println("user Id      :" + originatorRL.getUserID());
                        System.out.println("getPseudoCityCode  :" + originatorRL.getClosestCityAirportCode());
                        System.out.println("getCariierCode     :" + originatorRL.getCariierCRSCode());
                        System.out.println("UserType           :" + originatorRL.getUserType());
                        System.out.println("CountryCode        :" + originatorRL.getCountryCode());
                        System.out.println("CurrencyCode       :" + originatorRL.getCurrencyCode());
                        System.out.println("AgentCode          :" + originatorRL.getAgentDutyCode());
                        System.out.println("ErspId             :" + originatorRL.getErspId());
                        System.out.println("FirstDeparturefrom :" + originatorRL.getFirstDeparturefrom());
                        System.out.println("---------RecordLocator end------------");
                    }

                    if (bookingRequestDTO != null && bookingRequestDTO.getResponderRecordLocator() != null) {
                        RecordLocatorDTO ResponderRL = bookingRequestDTO.getResponderRecordLocator();
                        System.out.println("---------Responder RecordLocator begin------------");
                        System.out.println("Booking office :" + ResponderRL.getBookingOffice());
                        System.out.println("PnrReference :" + ResponderRL.getPnrReference());
                        System.out.println("Office Code  :" + ResponderRL.getTaOfficeCode());
                        System.out.println("user Id      :" + ResponderRL.getUserID());
                        System.out.println("getPseudoCityCode    :" + ResponderRL.getClosestCityAirportCode());
                        System.out.println("getCariierCode       :" + ResponderRL.getCariierCRSCode());
                        System.out.println("UserType             :" + ResponderRL.getUserType());
                        System.out.println("CountryCode          :" + ResponderRL.getCountryCode());
                        System.out.println("CurrencyCode         :" + ResponderRL.getCurrencyCode());
                        System.out.println("AgentCode            :" + ResponderRL.getAgentDutyCode());
                        System.out.println("ErspId               :" + ResponderRL.getErspId());
                        System.out.println("FirstDeparturefrom   :" + ResponderRL.getFirstDeparturefrom());
                        System.out.println("---------Responder RecordLocator end------------");
                    }

                    Collection pax = bookingRequestDTO.getNewNameDTOs();
                    if (pax.isEmpty()) {
                        System.out.println("NO Newwwwwwwwwwwwwwwwwwwwwwww Names");
                    }
                    Iterator iter1 = pax.iterator();
                    while (iter1.hasNext()) {
                        Object object = iter1.next();
                        System.out.println("***Name*************\nTitle = " + ((NameDTO) object).getPaxTitle()
                                + " First Name = " + ((NameDTO) object).getFirstName() + " lastName : "
                                + ((NameDTO) object).getLastName() + " group Magnitude : "
                                + ((NameDTO) object).getGroupMagnitude() + "\n***********");
                    }

                    Collection collection = bookingRequestDTO.getBookingSegmentDTOs();
                    if (collection.size() < 0) {
                        System.out.println("Booking Segment Dto's are null");
                    } else {
                        for (Iterator iter = collection.iterator(); iter.hasNext();) {
                            BookingSegmentDTO element = (BookingSegmentDTO) iter.next();
                            System.out.println("\n-- Booking Segment Dto :");
                            System.out.println("getCarrierCode()  :" + element.getCarrierCode());
                            System.out.println("getFlightNumber() :" + element.getFlightNumber());
                            System.out.println("getBookingCode()  : " + element.getBookingCode());
                            System.out.println("getDepartureDate():" + element.getDepartureDate());
                            System.out.println("getDepartureStation():" + element.getDepartureStation());
                            System.out.println("getDestinationStation() : " + element.getDestinationStation());
                            System.out.println("getActionCode()   :" + element.getActionOrStatusCode());
                            System.out.println("NoofPax()   :" + element.getNoofPax());
                            System.out.println("getDepartureTime():" + element.getDepartureTime());
                            System.out.println("getArrivalTime()  :" + element.getArrivalTime());
                            System.out.println("getDayChangeFlag():" + element.getDayOffSet());
                            System.out.println("getCarrierCode():" + element.getCarrierCode());
                            System.out.println("getMembersBlockIdentifier():" + element.getMembersBlockIdentifier());
                            System.out.println("-- Booking Segment Dto End :");
                        }
                    }

                    Collection c = bookingRequestDTO.getOsiDTOs();
                    if (c.size() == 0) {
                        System.out.println("NO OSI INFORMATIONS");
                    } else {
                        for (Iterator iter = c.iterator(); iter.hasNext();) {
                            Object obj = iter.next();
                        
                            if(obj instanceof OtherServiceInfoDTO) {
                                OtherServiceInfoDTO element = (OtherServiceInfoDTO) obj ;
                                
                                System.out.println("\n--OSI info Begin :");
                                System.out.println("getCarrierCode    :" + element.getCarrierCode());
                                System.out.println("getCodeOSI     :" + element.getCodeOSI());
                                System.out.println("getFullElement : " + element.getFullElement());
                                List osiNameList = element.getOsiNameDTOs();
                                if(osiNameList!=null && !osiNameList.isEmpty()) {
                                for (Iterator itr = osiNameList.iterator(); itr.hasNext();) {
                                    NameDTO osiName = (NameDTO) itr.next();
                                    System.out.println("OSI Names: : "+"Title = " + osiName.getPaxTitle() + " First Name = "
                                            + osiName.getFirstName() + " LastName : " + osiName.getLastName()
                                            + " Group Magnitude : " + osiName.getGroupMagnitude());
                                }
                                }
                                System.out.println("getServiceInfo :" + element.getServiceInfo());
                                System.out.println("getOsiValue :" + element.getOsiValue());
                                System.out.println("\n--OSI info End  :");
                                
                            } else if(obj instanceof OSIPhoneNoDTO) {
                                OSIPhoneNoDTO phoNoDTO = (OSIPhoneNoDTO) obj ;
                                
                                System.out.println("\n--OSI Phone info Begin :");
                                System.out.println("getCarrierCode    :" + phoNoDTO.getCarrierCode());
                                System.out.println("getCodeOSI     :" + phoNoDTO.getCodeOSI());
                                System.out.println("getFullElement : " + phoNoDTO.getFullElement());
                                System.out.println("getPhoneNo  :" + phoNoDTO.getPhoneNo());
                                System.out.println("getPhone_Code :" + phoNoDTO.getPhone_Code());
                                System.out.println("getOsiValue :" + phoNoDTO.getOsiValue());
                                System.out.println("\n--OSI Phone info End  :");
                                
                            }
                        }
                    }

                    List changeNameDtoList = bookingRequestDTO.getChangedNameDTOs();
                    if (changeNameDtoList != null) {
                        System.out.println("%%%%%%%%%%%%%%%%%%%%%Change Names%%%%%%%%%%%%%%%%%%%%%%%");
                        for (Iterator iterator = changeNameDtoList.iterator(); iterator.hasNext();) {
                            ChangedNamesDTO changedNamesDTO = (ChangedNamesDTO) iterator.next();
                            List oldNameList = changedNamesDTO.getOldNameDTOs();
                            for (Iterator itr = oldNameList.iterator(); itr.hasNext();) {
                                NameDTO oldname = (NameDTO) itr.next();
                                System.out.println("Old Names: : "+"Title = " + oldname.getPaxTitle() + " First Name = "
                                        + oldname.getFirstName() + " LastName : " + oldname.getLastName()
                                        + " Group Magnitude : " + oldname.getGroupMagnitude());
                            }

                            List newNameList = changedNamesDTO.getNewNameDTOs();
                            for (Iterator itr1 = newNameList.iterator(); itr1.hasNext();) {
                                NameDTO newName = (NameDTO) itr1.next();
                                System.out.println("New Names: : " +"Title = " + newName.getPaxTitle() + " First Name = "
                                        + newName.getFirstName() + " LastName : " + newName.getLastName()
                                        + " Group Magnitude : " + newName.getGroupMagnitude());
                            }

                        }

                    } else {
                        System.out.println("%%%%%%%%%%%%%NO CHNT HERE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                    }
                    
                    List unChangedNames = bookingRequestDTO.getUnChangedNameDTOs();
                    if (unChangedNames != null) {
                        
                        for (Iterator iter = unChangedNames.iterator(); iter.hasNext();) {
                            UnchangedNamesDTO unch = (UnchangedNamesDTO) iter.next();
                            List names = unch.getUnChangedNames();
                            for (Iterator itr = names.iterator(); itr.hasNext();) {
                                NameDTO oldname = (NameDTO) itr.next();
                                System.out.println("UnChanged Names : : " +"Title = " + oldname.getPaxTitle() + " First Name = "
                                        + oldname.getFirstName() + " LastName : " + oldname.getLastName()
                                        + " Group Magnitude : " + oldname.getGroupMagnitude());
                            }
                            System.out.println("Party Total :" + unch.getPartyTotal());
                            
                        }
                        
                        
                        
                    }

                    Collection ssrs = bookingRequestDTO.getSsrDTOs();
                    System.out.println(ssrs.size());
                    // iter = ssrs.iterator();

                    for (Iterator iterator = ssrs.iterator(); iterator.hasNext();) {
                        Object obj = iterator.next();
                        if(obj instanceof SSRDetailDTO) {
                            
                            SSRDetailDTO ssrDTO = (SSRDetailDTO) obj ;
                            System.out.println("\n--special service Requests--");
                            System.out.println("code SSR : " + ssrDTO.getCodeSSR());
                            System.out.println("Carriercode: " + ssrDTO.getCarrierCode());
                            System.out.println("SSR Value : " + ssrDTO.getSsrValue());
                            System.out.println("action status code : " + ssrDTO.getActionOrStatusCode());
                            System.out.println("no of pax : " + ssrDTO.getNoOfPax());
                            System.out.println("Free text : " + ssrDTO.getFreeText());
                            System.out.println("Full Element : " + ssrDTO.getFullElement());

                            System.out.println("End of -special service Requests--");
                            SegmentDTO segmentDTO = ssrDTO.getSegmentDTO();
                            if (segmentDTO != null) {
                                System.out.println("Date :" + segmentDTO.getDepartureDate());
                                System.out.println("Departure   : " + segmentDTO.getDepartureStation());
                                System.out.println("Destination :" + segmentDTO.getDestinationStation());
                                System.out.println("Flight No   :" + segmentDTO.getFlightNumber());
                                System.out.println("Booking Code:" + segmentDTO.getBookingCode());

                            } else {
                                System.out.println("Segment Null " + ssrDTO.getSegmentDTO());
                            }
                            List ssrNameList = ssrDTO.getSsrNameDTOs();
                            if (ssrNameList != null) {
                                for (Iterator nameDTOIterator = ssrNameList.iterator(); nameDTOIterator.hasNext();) {
                                    NameDTO nameDTO = (NameDTO) nameDTOIterator.next();

                                    System.out.println("SSR : Title = " + nameDTO.getPaxTitle() + " FirstName = "
                                            + nameDTO.getFirstName() + " LastName : " + nameDTO.getLastName());
                                }
                            } else {
                                System.out.println("SSR NAME NOT AVAILABLE");
                            }
                            
                        } else  if(obj instanceof SSRInfantDTO) {
                            
                            SSRInfantDTO infantDTO = (SSRInfantDTO) obj ;
                            System.out.println("\n--INFANT DTO--");
                            System.out.println("CodeSSR : " + infantDTO.getCodeSSR());
                            System.out.println("isSeatRequired : " + infantDTO.isSeatRequired());
                            System.out.println("InfantFirstName: " + infantDTO.getInfantFirstName());
                            System.out.println("InfantLastName : " + infantDTO.getInfantLastName());
                            System.out.println("InfantTitle : " + infantDTO.getInfantTitle());
                            System.out.println("GuardiantFirstName : " + infantDTO.getGuardianFirstName());
                            System.out.println("GuardiantLastName  : " + infantDTO.getGuardianLastName());
                            System.out.println("GuardiantTitle : " + infantDTO.getGuardianTitle());
                            System.out.println("Action_status_Code : " + infantDTO.getActionOrStatusCode());
                            System.out.println("Advice_status_Code : " + infantDTO.getAdviceOrStatusCode());
                            System.out.println("FullElement: " + infantDTO.getFullElement());
                            System.out.println("getFreetext: " + infantDTO.getFreeText());

                            System.out.println("End of INFANT DTO--");
                            SegmentDTO segmentDTO = infantDTO.getSegmentDTO();

                            System.out.println("End of INFANT Segment DTO--");
                            if (segmentDTO != null) {
                                System.out.println("Date :" + segmentDTO.getDepartureDate());
                                System.out.println("Departure   : " + segmentDTO.getDepartureStation());
                                System.out.println("Destination :" + segmentDTO.getDestinationStation());
                                System.out.println("Flight No   :" + segmentDTO.getFlightNumber());
                                System.out.println("Booking Code:" + segmentDTO.getBookingCode());

                            }
                            System.out.println("End of INFANT Segment DTO--");
                        } else  if(obj instanceof SSREmergencyContactDetailDTO) {
                            
                            System.out.println("\n----------------EmergencyContactDetailDTO------- ");
                            SSREmergencyContactDetailDTO emContactDetailDTO = (SSREmergencyContactDetailDTO)obj;
                            System.out.println("isRefused : " + emContactDetailDTO.isRefused());
                            System.out.println("Carriercode : " + emContactDetailDTO.getCarrierCode());
                            System.out.println("actioncode : " + emContactDetailDTO.getActionOrStatusCode());
                            System.out.println("getEmContactName : " + emContactDetailDTO.getEmContactName());
                            System.out.println("getEmContactNumberwithCountry : "+ emContactDetailDTO.getEmContactNumberwithCountry());
                            System.out.println("getPassengerFirstName() : " + emContactDetailDTO.getPassengerFirstName());
                            System.out.println("getPassengerLastName() : " + emContactDetailDTO.getPassengerLastName());
                            System.out.println("getFreeText() : " + emContactDetailDTO.getFreeText());
                            System.out.println("getFullElement() : " + emContactDetailDTO.getFullElement());
                            System.out.println("----------------EmergencyContactDetailDTO------- ");
                        
                        } else  if(obj instanceof SSRPassportDTO) {
                            System.out.println("\n----------------Passport DTO------- " );
                            SSRPassportDTO passportDTO = (SSRPassportDTO) obj;
                            System.out.println(" getCarrierCode: " + passportDTO.getCarrierCode());
                            System.out.println(" getAction_status_Code: " + passportDTO.getActionOrStatusCode());
                            System.out.println(" getPassengerPassportNo: " + passportDTO.getPassengerPassportNo());
                            System.out.println(" getPassengerFamilyName: " + passportDTO.getPassengerFamilyName());
                            System.out.println(" getPassengerGivenName: " + passportDTO.getPassengerGivenName());
                            System.out.println(" getPassportCountryCode: " + passportDTO.getPassportCountryCode());
                            System.out.println(" getPassengerDateOfBirth: " + passportDTO.getPassengerDateOfBirth());
                            System.out.println(" getPassengerGender: " + passportDTO.getPassengerGender());
                            System.out.println(" isMultiPassengerPassport: " + passportDTO.isMultiPassengerPassport());
                            System.out.println(" getFreeText: " + passportDTO.getFreeText());

                            List ssrPsptNameList = passportDTO.getPnrNameDTOs();
                            if (ssrPsptNameList != null) {
                                for (Iterator nameDTOIterator = ssrPsptNameList.iterator(); nameDTOIterator.hasNext();) {
                                    NameDTO nameDTO = (NameDTO) nameDTOIterator.next();

                                    System.out.println("SSR : Title = " + nameDTO.getPaxTitle() + " FirstName = "
                                            + nameDTO.getFirstName() + " LastName : " + nameDTO.getLastName());
                                }
                            } else {
                                System.out.println("SSR Passport NOT AVAILABLE");
                            }
                            
                            System.out.println("----------------Passport DTO------- ");
                            
                        } else  if(obj instanceof SSRFoidDTO) {
                            
                            System.out.println("\n----------------FOID DTO------- " );
                            SSRFoidDTO foidDTO = (SSRFoidDTO) obj;
                            System.out.println(" getCarrierCode: " + foidDTO.getCarrierCode());
                            System.out.println(" getAction_status_Code: " + foidDTO.getActionOrStatusCode());
                            System.out.println(" getPassengerIdentificationType: " + foidDTO.getPassengerIdentificationType());
                            System.out.println(" getPassengerIdentificationNo: " + foidDTO.getPassengerIdentificationNo());
                            System.out.println(" getFullElement: " + foidDTO.getFullElement());
                            
                            List ssrfoidNameList = foidDTO.getPnrNameDTOs();
                            if (ssrfoidNameList != null) {
                                for (Iterator nameDTOIterator = ssrfoidNameList.iterator(); nameDTOIterator.hasNext();) {
                                    NameDTO nameDTO = (NameDTO) nameDTOIterator.next();

                                    System.out.println("SSR : Title = " + nameDTO.getPaxTitle() + " FirstName = "
                                            + nameDTO.getFirstName() + " LastName : " + nameDTO.getLastName());
                                }
                            } else {
                                System.out.println("SSR FOID NOT AVAILABLE");
                            }
                            
                        } else  if(obj instanceof SSRChildDTO) {
                            
                            System.out.println("\n----------------Child DTO------- " );
                            SSRChildDTO child = (SSRChildDTO) obj;
                            System.out.println(" getCodeSSR: " + child.getCodeSSR());
                            System.out.println(" getCarrierCode: " + child.getCarrierCode());
                            System.out.println(" getAction_status_Code: " + child.getActionOrStatusCode());
                            System.out.println(" getChildFirstName: " + child.getFirstName());
                            System.out.println(" getChildLastName: " + child.getLastName());
                            System.out.println(" getChildTitle: " + child.getTitle());
                            System.out.println(" getChildAge(): " + child.getDateOfBirth());
                            System.out.println(" getFullElement: " + child.getFullElement());
                            
                        } else  if(obj instanceof SSRMinorDTO) {
                            
                            System.out.println("\n----------------Minor DTO------- " );
                            SSRMinorDTO minor = (SSRMinorDTO) obj;
                            System.out.println(" getCodeSSR: " + minor.getCodeSSR());
                            System.out.println(" getCarrierCode: " + minor.getCarrierCode());
                            System.out.println(" getAction_status_Code: " + minor.getActionOrStatusCode());
                            System.out.println(" getChildFirstName: " + minor.getFirstName());
                            System.out.println(" getChildLastName: " + minor.getLastName());
                            System.out.println(" getChildTitle: " + minor.getTitle());
                            System.out.println(" getChildAge(): " + minor.getMinorAge());
                            System.out.println(" getFullElement: " + minor.getFullElement());
                            
                        } else  if(obj instanceof SSRCreditCardDetailDTO) {
                            
                            System.out.println("\n----------------SSR CArD DTO------- " );
                            SSRCreditCardDetailDTO card = (SSRCreditCardDetailDTO) obj;
                            System.out.println(" CodeSSR: " + card.getCodeSSR());
                            System.out.println(" CardNo: " + card.getCardNo());
                            System.out.println(" PinNo: " + card.getPinNumber());
                            System.out.println(" CardType: " + card.getCardType());
                            System.out.println(" ExpiryMonth: " + card.getExpiryMonth() );
                            System.out.println(" ExpiryYear: " + card.getExpiryYear());
                            System.out.println(" car holder name: " + card.getCardHolderName());
                            System.out.println(" FullElement: " + card.getSsrValue() );
                            
                        } else  if(obj instanceof SSROSAGDTO) {
                            
                            System.out.println("\n----------------SSR OSAg DTO------- " );
                            SSROSAGDTO osag = (SSROSAGDTO) obj;
                            System.out.println(" CodeSSR: " + osag.getCodeSSR());
                            System.out.println(" OsagCode: " + osag.getOsagCode());
                            System.out.println(" CarrierCode: " + osag.getCarrierCode());
                            System.out.println(" SsrValue: " + osag.getSsrValue());
                            System.out.println(" FullElement: " + osag.getFullElement());
                         
                            
                        } 
                        else  if(obj instanceof SSRDTO) {
                            
                            System.out.println("\n----------------SSR DTO------- " );
                            SSRDTO ssr = (SSRDTO) obj;
                            System.out.println(" getCodeSSR: " + ssr.getCodeSSR());
                            System.out.println(" getCarrierCode: " + ssr.getCarrierCode());
                            System.out.println(" getAction_status_Code: " + ssr.getActionOrStatusCode());
                            System.out.println(" SsrValue: " + ssr.getSsrValue() );
                            System.out.println(" FreeText: " + ssr.getFreeText());
                            System.out.println(" FullElement: " + ssr.getFullElement() );
                            
                        } else {
                            System.out.println("invalide or unprocessed SSR DTO");
                            throw new Exception("invalide or unprocessed SSR DTO");
                        }
                       

                    }
                   

                }
                
                
            } else if(requestObj instanceof  InNacDTO) {
                InNacDTO nacDTO = (InNacDTO) requestObj;
                
                if (nacDTO !=null) {
                    System.out.println("########NAC MESSAGE###########");
                    System.out.println(MESSAGE_TO_BE_PASSED);
                    System.out.println("########NAC MESSAGE###########");
                    System.out.println("OriginatorAddress : " + nacDTO.getOriginatorAddress());
                    System.out.println("ResponderAddress : "+nacDTO.getResponderAddress());
                    System.out.println("ReferredMessageIdentifier : "+ nacDTO.getMessageIdentifier());
                    System.out.println("ReferredMessageSequenceReference : "+nacDTO.getReferredMessageSequenceReference());
                    System.out.println("ErrorMessage : " +nacDTO.getErrorMessage());
                } else {
                    System.out.println("NACDTO EMPTY");
                }
                
            }
        } else {
            System.out.println("::::::::PARSER ERROR::::::::");
            System.out.println(parserResponseDTO.getReasonCode());
            System.out.println(parserResponseDTO.getStatus());
            System.out.println("::::::::PARSER ERROR::::::::");
        }
        
    }

}
