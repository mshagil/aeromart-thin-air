package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.core.controller.ModuleFramework;

public class TestPublishAvailabilityBL {

	public static void main(String [] args){
		System.setProperty("repository.home", "c://isaconfig-test");
		ModuleFramework.startup();
		
		testAVSForCreateFlight();
		//testAVSForCloseFlight();
	}
	
	public static void testAVSForCreateFlight(){
		NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
		
		NotifyFlightEventRQ notifyFlightEventRQ = new NotifyFlightEventRQ();
		notifyFlightEventsRQ.addNotifyFlightEventRQ(notifyFlightEventRQ);
		notifyFlightEventRQ.setFlightEventCode(FlightEventCode.FLIGHT_CREATED);
		
		ArrayList<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(163647);
		notifyFlightEventRQ.setFlightIds(flightIds);
		
		ArrayList<Integer> gdsIds = new ArrayList<Integer>();
		gdsIds.add(1);
		notifyFlightEventRQ.setGdsIdsAdded(gdsIds);
		
		try {
			new PublishAvailabilityBL().notifyFlightEvent(notifyFlightEventsRQ);
		} catch (ModuleException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void testAVSForCloseFlight(){
		NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
		
		NotifyFlightEventRQ notifyFlightEventRQ = new NotifyFlightEventRQ();
		notifyFlightEventsRQ.addNotifyFlightEventRQ(notifyFlightEventRQ);
		notifyFlightEventRQ.setFlightEventCode(FlightEventCode.FLIGHT_CLOSED);
		
		ArrayList<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(163647);
		notifyFlightEventRQ.setFlightIds(flightIds);
		
		try {
			new PublishAvailabilityBL().notifyFlightEvent(notifyFlightEventsRQ);
		} catch (ModuleException e) {
			e.printStackTrace();
		}
	}
}
