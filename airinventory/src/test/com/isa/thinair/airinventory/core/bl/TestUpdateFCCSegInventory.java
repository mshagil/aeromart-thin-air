package com.isa.thinair.airinventory.core.bl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestUpdateFCCSegInventory extends PlatformTestCase {
    
    protected void setUp() throws Exception{
        super.setUp();
    }
    
    public void testUpdateSingleSegSingleCabinClassFCCSegInvCaseOne() throws ModuleException{
        FCCSegInventoryUpdateDTO updateDTO = new FCCSegInventoryUpdateDTO();
        updateDTO.setFlightId(19050);
        updateDTO.setFccsInventoryId(19050);
        updateDTO.setCabinClassCode("I");
        updateDTO.setSegmentCode("CMB/SHJ");
        updateDTO.setOversellSeats(20);
        updateDTO.setCurtailedSeats(0);
        updateDTO.setInfantAllocation(10);
        updateDTO.setVersion(0);
        
        Set addedFCCSegBCInv = new HashSet();
        Set updatedFCCSegBCInv = new HashSet();
        Set deletedFCCSegBCInv = new HashSet();
        updateDTO.setAddedFCCSegBCInventories(addedFCCSegBCInv);
        updateDTO.setUpdatedFCCSegBCInventories(updatedFCCSegBCInv);
        updateDTO.setDeletedFCCSegBCInventories(deletedFCCSegBCInv);
        FCCSegBCInventory updatedBC = createFCCSegBCInventory(19050, 19050, "I", "CMB/SHJ", 90,
                "IES",FCCSegBCInventory.Status.CLOSED, true , false);
        updatedBC.setFccsbInvId(new Integer(19050));
        updatedBC.setVersion(0);
        updatedFCCSegBCInv.add(updatedBC); 
        
        FCCSegBCInventory updatedBC1 = createFCCSegBCInventory(19050, 19050, "I", "CMB/SHJ", 18,
                "IEF",FCCSegBCInventory.Status.OPEN, true , false);
        updatedBC1.setFccsbInvId(new Integer(19054));
        updatedBC1.setVersion(0);
        updatedFCCSegBCInv.add(updatedBC1);
        
        
        deletedFCCSegBCInv.add(new Integer(19053));
        addedFCCSegBCInv.add(createFCCSegBCInventory(19050, 19050, "I", "CMB/SHJ",50,
                "IGS",FCCSegBCInventory.Status.OPEN, false, false));
        
        getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
        
//        Iterator fccInventoryDTOsIt = getFlightInventoryBL().getFCCInventoryDTOs(19050, "I", null).iterator();
//        while (fccInventoryDTOsIt.hasNext()){
//            FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO)fccInventoryDTOsIt.next();
//            Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
//            assertEquals(1, fccSegInventoryDTOs.size());
//            FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO)fccSegInventoryDTOs.iterator().next();
//            assertNotNull(fccSegInventoryDTO);
//            
//            assertEquals(40, fccSegInventoryDTO.getFccSegInventory().getAvailableSeats());
//            
//            Collection fccSegBCInventories = fccSegInventoryDTO.getFccSegBCInventories();
//            assertEquals(5, fccSegBCInventories.size());
//            String [] expectedBookingCodesArr = {"IES", "IFS", "IGS", "IEN", "IEF"};
//            String [] notExpectedBookingCodesArr = {"IFN"};
//            
//            Iterator fccSegBCInventoriesIt = fccSegBCInventories.iterator();
//            while (fccSegBCInventoriesIt.hasNext()){
//                FCCSegBCInventory fccSegBCInv = (FCCSegBCInventory)fccSegBCInventoriesIt.next();
//                assertTrue(hasBookingCode(fccSegBCInv.getBookingCode(), expectedBookingCodesArr));
//                assertFalse(hasBookingCode(fccSegBCInv.getBookingCode(),notExpectedBookingCodesArr));
//                if (fccSegBCInv.getBookingCode().equals("IES")){
//                    assertEquals(0, fccSegBCInv.getSeatsAvailable());
//                    assertTrue(fccSegBCInv.getManuallyClosed());
//                }
//                if (fccSegBCInv.getBookingCode().equals("IEF")){
//                    assertEquals(3, fccSegBCInv.getSeatsAvailable());
//                }
//            }
//        }
    }
    
    public void testUpdateSingleSegSingleCabinClassFCCSegInvCaseTwo() throws ModuleException{
        //deleting a bc inventory with sold/onhold seats
        try {
            FCCSegInventoryUpdateDTO updateDTO = new FCCSegInventoryUpdateDTO();
            updateDTO.setFlightId(19051);
            updateDTO.setFccsInventoryId(19051);
            updateDTO.setCabinClassCode("I");
            updateDTO.setSegmentCode("CMB/SHJ");
            updateDTO.setOversellSeats(0);
            updateDTO.setCurtailedSeats(0);
            updateDTO.setInfantAllocation(10);
            updateDTO.setVersion(0);
            
            Set deletedFCCSegBCInv = new HashSet();
            updateDTO.setDeletedFCCSegBCInventories(deletedFCCSegBCInv);
            
            deletedFCCSegBCInv.add(new Integer(19055));
            getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
            fail("Exception expected : cannot delete bc inventory with sold/onhold seats");
        } catch (ModuleException mdex){
        }
    }
    
    public void testUpdateSingleSegSingleCabinClassFCCSegInvCaseThree() throws ModuleException{
        //curtailing more than availble seats on the segment
        try {
            FCCSegInventoryUpdateDTO updateDTO = new FCCSegInventoryUpdateDTO();
            updateDTO.setFlightId(19051);
            updateDTO.setFccsInventoryId(19051);
            updateDTO.setCabinClassCode("I");
            updateDTO.setSegmentCode("CMB/SHJ");
            updateDTO.setCurtailedSeats(131);
            updateDTO.setInfantAllocation(10);
            updateDTO.setVersion(0);
            
            getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
            fail("Exception expected : cannot curtail more than available");
        } catch (ModuleException mdex){
        }
    }
    
    public void TestUpdateSingleSegSingleCabinClassFCCSegInvCaseFour() throws ModuleException{
        //test adding a fixed allocation exceeding available seats on the segment
    }
    
    public void testUpdateSingleSegSingleCabinClassFCCSegInvCaseFive() throws ModuleException{
        //updating overloaded flight inventory - failure scenarios
        
        FCCSegInventoryUpdateDTO updateDTO = new FCCSegInventoryUpdateDTO();
        updateDTO.setFlightId(19058);
        updateDTO.setFccsInventoryId(19055);
        updateDTO.setCabinClassCode("I");
        updateDTO.setSegmentCode("CMB/SHJ");
        updateDTO.setCurtailedSeats(5);
        updateDTO.setOversellSeats(4);
        updateDTO.setInfantAllocation(10);
        updateDTO.setVersion(0);
        
        //reduce oversell
        try {
            getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
            fail("expected oversell reduction failure");
        } catch (ModuleException mex){   
        }
        
        updateDTO.setCurtailedSeats(6);
        updateDTO.setOversellSeats(5);
        
        //increase curtailed
        try {
            getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
            fail("expected curtailed increase failure");
        } catch (ModuleException mex){   
        }
        
        
        
        //oversell reduction + additional curtailed > 0
        updateDTO.setCurtailedSeats(7);
        updateDTO.setOversellSeats(6);
        
        try {
            getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
            fail("expected curtailed increase + oversell increase combination failure");
        } catch (ModuleException mex){   
        }
        
        //increase fixed
        updateDTO.setCurtailedSeats(5);
        updateDTO.setOversellSeats(5);
        
        Set updatedFCCSegBCInv = new HashSet();

        updateDTO.setUpdatedFCCSegBCInventories(updatedFCCSegBCInv);
 
        FCCSegBCInventory updatedBC = createFCCSegBCInventory(19058, 19055, "I", "CMB/SHJ", 55,
                "IEF",FCCSegBCInventory.Status.OPEN, false , false);
        updatedBC.setFccsbInvId(new Integer(19059));
        updatedBC.setVersion(0);
        updatedFCCSegBCInv.add(updatedBC); 
        try {
            getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
            fail("expected fixed increase invalid");
        } catch (ModuleException mex){   
        }
        
        //add not allowed fixed amount
        updateDTO.setCurtailedSeats(5);
        updateDTO.setOversellSeats(5);
        
        updateDTO.setUpdatedFCCSegBCInventories(null);
        Set addedFCCSegBCInv = new HashSet();
        updateDTO.setAddedFCCSegBCInventories(addedFCCSegBCInv);
        addedFCCSegBCInv.add(createFCCSegBCInventory(19058, 19055, "I", "CMB/SHJ",5,
                "IFF",FCCSegBCInventory.Status.OPEN, false, false));
        
        try {
            getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
            fail("expected new fixed not allowed");
        } catch (ModuleException mex){   
        }
        
        //oversell negates curtail
        updateDTO.setCurtailedSeats(10);
        updateDTO.setOversellSeats(10);
        updateDTO.setAddedFCCSegBCInventories(null);
        updateDTO.setUpdatedFCCSegBCInventories(null);
        getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
    }
    
    public void testUpdateSingleSegSingleCabinClassFCCSegInvOveloadedFlight() throws ModuleException{
        FCCSegInventoryUpdateDTO updateDTO = new FCCSegInventoryUpdateDTO();
        updateDTO.setFlightId(19059);
        updateDTO.setFccsInventoryId(19056);
        updateDTO.setCabinClassCode("I");
        updateDTO.setSegmentCode("CMB/SHJ");
        updateDTO.setCurtailedSeats(5);
        updateDTO.setOversellSeats(5);
        updateDTO.setInfantAllocation(10);
        updateDTO.setVersion(0);
        
        Set addedFCCSegBCInv = new HashSet();
        Set updatedFCCSegBCInv = new HashSet();
        Set deletedFCCSegBCInv = new HashSet();
        
        updateDTO.setAddedFCCSegBCInventories(addedFCCSegBCInv);
        updateDTO.setUpdatedFCCSegBCInventories(updatedFCCSegBCInv);
        updateDTO.setDeletedFCCSegBCInventories(deletedFCCSegBCInv);
        
        FCCSegBCInventory updatedBC1 = createFCCSegBCInventory(19059, 19056, "I", "CMB/SHJ", 15,
                "IEF",FCCSegBCInventory.Status.OPEN, true , false);
        updatedBC1.setStatus(FCCSegBCInventory.Status.CLOSED);
//        updatedBC1.setManuallyClosed(true);
        updatedBC1.setFccsbInvId(new Integer(19059));
        updatedBC1.setVersion(0);
        updatedFCCSegBCInv.add(updatedBC1);
               
        getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
        
//        Iterator fccInventoryDTOsIt = getFlightInventoryBL().getFCCInventoryDTOs(19059, "I", null).iterator();
//        while (fccInventoryDTOsIt.hasNext()){
//            FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO)fccInventoryDTOsIt.next();
//            Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
//            assertEquals(1, fccSegInventoryDTOs.size());
//            FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO)fccSegInventoryDTOs.iterator().next();
//            assertNotNull(fccSegInventoryDTO);
//            
//            assertEquals(-5, fccSegInventoryDTO.getFccSegInventory().getAvailableSeats());
//            
//            Collection fccSegBCInventories = fccSegInventoryDTO.getFccSegBCInventories();
//            assertEquals(2, fccSegBCInventories.size());
//            String [] expectedBookingCodesArr = {"IES", "IEF"};
//            String [] notExpectedBookingCodesArr = {};
//            
//            Iterator fccSegBCInventoriesIt = fccSegBCInventories.iterator();
//            while (fccSegBCInventoriesIt.hasNext()){
//                FCCSegBCInventory fccSegBCInv = (FCCSegBCInventory)fccSegBCInventoriesIt.next();
//                assertTrue(hasBookingCode(fccSegBCInv.getBookingCode(), expectedBookingCodesArr));
//                assertFalse(hasBookingCode(fccSegBCInv.getBookingCode(),notExpectedBookingCodesArr));
//                if (fccSegBCInv.getBookingCode().equals("IEF")){
//                    assertEquals(0, fccSegBCInv.getSeatsAvailable());
//                }
//            }
//        }
    }
    
    public void testUpdateMultipleSegSingleCabinClassFCCSegInvCaseOne() throws ModuleException{
        FCCSegInventoryUpdateDTO updateDTO = new FCCSegInventoryUpdateDTO();
        updateDTO.setFlightId(19052);
        updateDTO.setFccsInventoryId(19054);
        updateDTO.setCabinClassCode("I");
        updateDTO.setSegmentCode("SSH/LXR");
        updateDTO.setOversellSeats(0);
        updateDTO.setCurtailedSeats(0);
        updateDTO.setInfantAllocation(10);
        updateDTO.setVersion(0);
        
        Set addedFCCSegBCInv = new HashSet();
        Set updatedFCCSegBCInv = new HashSet();
        Set deletedFCCSegBCInv = new HashSet();
        updateDTO.setAddedFCCSegBCInventories(addedFCCSegBCInv);
        updateDTO.setUpdatedFCCSegBCInventories(updatedFCCSegBCInv);
        updateDTO.setDeletedFCCSegBCInventories(deletedFCCSegBCInv);
        FCCSegBCInventory updatedBC = createFCCSegBCInventory(19052, 19054, "I", "SSH/LXR", 100,
                "IES",FCCSegBCInventory.Status.CLOSED, true , false);
        updatedBC.setFccsbInvId(new Integer(19063));
        updatedBC.setVersion(0);
        updatedFCCSegBCInv.add(updatedBC); 
        
        FCCSegBCInventory updatedBC1 = createFCCSegBCInventory(19052, 19054, "I", "SSH/LXR", 10,
                "IFF",FCCSegBCInventory.Status.OPEN, true , false);
        updatedBC1.setFccsbInvId(new Integer(19064));
        updatedBC1.setVersion(0);
        updatedFCCSegBCInv.add(updatedBC1);
        
        
        deletedFCCSegBCInv.add(new Integer(19065));
        addedFCCSegBCInv.add(createFCCSegBCInventory(19052, 19054, "I", "SSH/LXR",40,
                "IEF",FCCSegBCInventory.Status.OPEN, false, false));
        
        getFlightInventoryBL().updateFCCSegInventoryNew(updateDTO);
    }
    
    private boolean hasBookingCode(String bookingCode, String[] expectedBookingCodesArr) {
        for (int i=0; i<expectedBookingCodesArr.length; i++){
            if (bookingCode.equals(expectedBookingCodesArr[i]))
                return true;
        }
        return false;
    }

    protected void tearDown(){
        
    }
    
    private FCCSegBCInventory createFCCSegBCInventory(int flightId,
            int fccsInvId, String cabinClassCode, String segCode,
            int allocatedSeats, String bookingCode, String status,
            boolean manuallyClosed, boolean priorityFlag) {
        FCCSegBCInventory fccSegBCInv = new FCCSegBCInventory();
        fccSegBCInv.setFlightId(flightId);
        fccSegBCInv.setfccsInvId(fccsInvId);
        fccSegBCInv.setCCCode(cabinClassCode);
        fccSegBCInv.setSegmentCode(segCode);
        fccSegBCInv.setSeatsAllocated(allocatedSeats);
        fccSegBCInv.setBookingCode(bookingCode);
        fccSegBCInv.setStatus(status);
//        fccSegBCInv.setManuallyClosed(manuallyClosed);
        fccSegBCInv.setPriorityFlag(priorityFlag);
        return fccSegBCInv;
    }
    
    private FlightInventoryBL getFlightInventoryBL(){
        return new FlightInventoryBL();
    }
}
