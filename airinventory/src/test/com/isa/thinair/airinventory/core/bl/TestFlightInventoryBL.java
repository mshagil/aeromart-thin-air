package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FlightSegSeatDistDTO;
import com.isa.thinair.airinventory.api.dto.SeatDistForRelease;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.OptimizeSeatInvCriteriaDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResDAO;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;


public class TestFlightInventoryBL extends PlatformTestCase {

    
	protected void setUp() throws Exception{
		super.setUp();
	}
	
//	public void TestRetrievingOverlapFlightSegs() throws ModuleException{
//		Iterator fccInventoryDTOsIt = createFlightInventoryBL().getFCCInventoryDTOs(20072, "I", new Integer(20060)).iterator();
//		while (fccInventoryDTOsIt.hasNext()){
//			FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO)fccInventoryDTOsIt.next();
//			Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
//			for (Iterator iter = fccSegInventoryDTOs.iterator(); iter.hasNext();) {
//				FCCSegInventoryDTO element = (FCCSegInventoryDTO) iter.next();
//				System.out.println("Flight Id: " + String.valueOf(element.getFccSegInventory().getFlightId()) + " Segment Code: " + element.getFccSegInventory().getSegmentCode()); 
//			}
//		}
//	}

	/*
	 * [TODO: test] Test rollforwarding fixed allocations for mulit-segment flights
	 */
	public void TestrollForwardFCCSegBCInventories() throws ModuleException{
//		InvRollForwardFlightsSearchCriteria searchCriteriaDTO = new InvRollForwardFlightsSearchCriteria(); 
//		Set segmentCodes = new HashSet();
//		segmentCodes.add("CMB/SHJ");
//		searchCriteriaDTO.setSegmentCodes(segmentCodes);
//		InvRollForwardCriteriaDTO criteriaDTO = new InvRollForwardCriteriaDTO();
//		criteriaDTO.setInvRollForwardFlightsSearchCriteria(searchCriteriaDTO);
//		criteriaDTO.setOverwriteExisting(true);
//		criteriaDTO.setRollFixedBCAllocs(true);
//		criteriaDTO.setRollNonStadardBCAllocs(true);
//		criteriaDTO.setRollStandardBCAllocs(true);
//		criteriaDTO.setSourceCabinClassCode("I");
//		criteriaDTO.setSourceFlightId(9160);
//		Set flightIds = new HashSet();		
//		flightIds.add(new Integer(9161));
//		flightIds.add(new Integer(9162));
//		flightIds.add(new Integer(9163));
//		flightIds.add(new Integer(9164));
//		createFlightInventoryBL().rollForwardFCCSegBCInventories(flightIds,criteriaDTO);				
	}
	
	public void TestGetFCCInventory() throws ModuleException{
		System.out.println("Get FCC Inventory Testing");
		Collection fccInventories = createFlightInventoryBL().getFCCInventoryDTOsNew(20020,"Y",null);
		assertNotNull(fccInventories);
//		assertEquals(2, fccInventories.size());
		
		for (Iterator fccInventoriesIt = fccInventories.iterator(); fccInventoriesIt.hasNext();) {
			FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO) fccInventoriesIt.next();
			Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
			assertNotNull(fccSegInventoryDTOs);
//			assertEquals(3, fccSegInventoryDTOs.size());
			System.out.println("Infant Capacity " + String.valueOf(fccInventoryDTO.getInfantCapacity()));
			for (Iterator fccSegInventoryDTOsIt = fccSegInventoryDTOs.iterator(); fccSegInventoryDTOsIt.hasNext();) {
				FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) fccSegInventoryDTOsIt.next();
				System.out.println("	Segment Inventory > Segment Code : " + fccSegInventoryDTO.getSegmentCode());
				Collection fccSegBCInventories = fccSegInventoryDTO.getFccSegBCInventories();
				assertNotNull(fccSegBCInventories);
//				if (fccSegInventoryDTO.getSegmentCode().equals("CMB/DEL/KHI"))
//					assertEquals(1, fccSegBCInventories.size());
//				if (fccSegInventoryDTO.getSegmentCode().equals("KHI/CMB"))
//					assertEquals(2, fccSegBCInventories.size());
				for (Iterator fccSegBCInventoriesIt = fccSegBCInventories.iterator(); fccSegBCInventoriesIt.hasNext();) {
					ExtendedFCCSegBCInventory exBCInv = (ExtendedFCCSegBCInventory) fccSegBCInventoriesIt.next();
                    FCCSegBCInventory fccSegBCInventory = exBCInv.getFccSegBCInventory();
					System.out.println("		BC Inventory > Booking Code : " + fccSegBCInventory.getBookingCode());
					System.out.println("		BC Inventory > Allocated seats : " + fccSegBCInventory.getSeatsAllocated());
                    Iterator faresIt = exBCInv.getLinkedEffectiveFares().iterator();
                    while (faresIt.hasNext()){
                        FareSummaryDTO fare = (FareSummaryDTO)faresIt.next();
                        System.out.println("        Linked Fares fareId=" +  fare.getFareId() + " amount="+fare.getFareAmount(PaxTypeTO.ADULT) + " ruleId=" +fare.getFareRuleID() );
                    }
                    System.out.println("        Linked Agents =" + exBCInv.getLinkedEffectiveAgents() );
				}
			}
		}
	}

	public void TestGetSeatInventory() throws ModuleException {
		System.out.println("Get Seat Inventory Testing");
		Set flightIds = new HashSet();
		flightIds.add(new Integer(9000));
		flightIds.add(new Integer(9001));
		OptimizeSeatInvCriteriaDTO optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
		optimizeSeatInvCriteriaDTO.setIncludeAllBCs(true);
//		optimizeSeatInvCriteriaDTO.setIncludeStandardBCsOnly(false);
		Set segmentCodes = new HashSet();
		segmentCodes.add("CMB/SHJ");
		segmentCodes.add("SHJ/ALY");
//		Collection col = createFlightInventoryBL().getSeatInventory(flightIds,
//				optimizeSeatInvCriteriaDTO);
//		assertNotNull("Get Seat Inventory returns null", col);
//		for (Iterator iter = col.iterator(); iter.hasNext();) {
//			FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO) iter.next();
//			System.out.println("Flight Id "
//					+ String.valueOf(fccInventoryDTO.getFlightId()));
//			for (Iterator iterSeg = fccInventoryDTO.getFccSegInventoryDTOs()
//					.iterator(); iterSeg.hasNext();) {
//				FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) iterSeg
//						.next();
//				System.out.println("	Segment Inventory > Segment Code : "
//						+ fccSegInventoryDTO.getSegmentCode());
//				for (Iterator iterFccSegBCInventory = fccSegInventoryDTO
//						.getFccSegBCInventories().iterator(); iterFccSegBCInventory
//						.hasNext();) {
//					ExtendedFCCSegBCInventory extendedFCCSegBCInventory = (ExtendedFCCSegBCInventory) iterFccSegBCInventory
//							.next();
//					System.out.println("		BC Inventory > Booking Code : "
//							+ extendedFCCSegBCInventory.getFccSegBCInventory().getBookingCode());
//					System.out.println("		BC Inventory > Allocated seats : "
//							+ extendedFCCSegBCInventory.getFccSegBCInventory().getSeatsAllocated());
//					System.out.println("		BC Inventory > Fixed : "
//							+ (extendedFCCSegBCInventory.isFixedFlag()?"Yes":"No"));
//				}
//			}
//		}
	}


	public void TestGetSeatInventoryInGeneral() throws ModuleException{
    	System.out.println("Get Seat Inventory Testing");
    	
    	Set flightIds = new HashSet();
    	flightIds.add(new Integer(8000));
    	flightIds.add(new Integer(8001));
    	OptimizeSeatInvCriteriaDTO optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
    	optimizeSeatInvCriteriaDTO.setIncludeAllBCs(false);
    	optimizeSeatInvCriteriaDTO.setIncludeStandardBCsOnly(false);
    	Set segmentCodes = new HashSet();
    	segmentCodes.add("CMB/SHJ");
    	segmentCodes.add("SHJ/ALY");
    	FlightInventoryBL flightInventoryBL = createFlightInventoryBL();
    	
//    	Collection col = flightInventoryBL.getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);
//    	assertNotNull("Get Seat Inventory returns null",col);
//    	for (Iterator iter = col.iterator(); iter.hasNext();) {
//    		FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO) iter.next();
//    		System.out.println("Flight Id " + String.valueOf(fccInventoryDTO.getFlightId()));
//    		for (Iterator iterSeg = fccInventoryDTO.getFccSegInventoryDTOs().iterator(); iterSeg.hasNext();) {
//    			FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) iterSeg.next();
//    			System.out.println("	Segment Inventory > Segment Code : " + fccSegInventoryDTO.getSegmentCode());
//    			for (Iterator iterFccSegBCInventory = fccSegInventoryDTO.getFccSegBCInventories().iterator(); iterFccSegBCInventory.hasNext();) {
//    				FCCSegBCInventory fccSegBCInventory = (FCCSegBCInventory) iterFccSegBCInventory.next();
//    				System.out.println("		BC Inventory > Booking Code : " + fccSegBCInventory.getBookingCode());
//    				System.out.println("		BC Inventory > Allocated seats : " + fccSegBCInventory.getSeatsAllocated());
//    			}
//    		}
//    	}
	}

	
	private AircraftBD getAircraftBD(){
		return (AircraftBD) AirInventoryUtil.getInstance().lookupServiceBD("airmaster", "aircraft.service");

	}
	
	private void AssertGetSeatInventory(Collection col, int flightId1, Integer segInvCount1, String segmentCode1, Integer segBCInvCount1,
			int flightId2, Integer segInvCount2, String segmentCode2, Integer segBCInvCount2){
		System.out.println("Printing Tree of Results");
		for (Iterator iter = col.iterator(); iter.hasNext();) {
			FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO) iter.next();
			System.out.println("Flight Id, CC Code : " + String.valueOf(fccInventoryDTO.getFlightId()) + ", " + 
					fccInventoryDTO.getCabinClassCode());
			for (Iterator iterSeg = fccInventoryDTO.getFccSegInventoryDTOs().iterator(); iterSeg.hasNext();) {
				FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) iterSeg.next();
				System.out.println("	Segment Inventory > fccsInvId, Segment Code : " + 
						String.valueOf(fccSegInventoryDTO.getFccSegInventory().getFccsInvId()) + ", " + 
						fccSegInventoryDTO.getSegmentCode());
				for (Iterator iterFccSegBCInventory = fccSegInventoryDTO.getFccSegBCInventories().iterator(); iterFccSegBCInventory.hasNext();) {
					FCCSegBCInventory fccSegBCInventory = (FCCSegBCInventory) iterFccSegBCInventory.next();
					System.out.println("		BC Inventory > Booking Code : " + fccSegBCInventory.getBookingCode());
					System.out.println("		BC Inventory > Allocated seats : " + fccSegBCInventory.getSeatsAllocated());
				}
			}
		}
		System.out.println("End of Printing Tree of Results");
		
		for (Iterator iter = col.iterator(); iter.hasNext();) {
			FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO) iter.next();
			System.out.println("Flight Id " + String.valueOf(fccInventoryDTO.getFlightId()));
			if (fccInventoryDTO.getFccSegInventoryDTOs()!=null){
				if (fccInventoryDTO.getFlightId()==flightId1)				
					assertEquals("Get Seat Inventory for Optimize Testing doesn't return correct number of FCC Seg Inventories " +
							"when All BCs selected and flight id " + String.valueOf(flightId1) + " !",segInvCount1.intValue(),fccInventoryDTO.getFccSegInventoryDTOs().size());
				if (fccInventoryDTO.getFlightId()==flightId2)				
					assertEquals("Get Seat Inventory for Optimize Testing doesn't return correct number of FCC Seg Inventories " +
							"when All BCs selected and flight id " + String.valueOf(flightId2) + " !",segInvCount2.intValue(),fccInventoryDTO.getFccSegInventoryDTOs().size());
				for (Iterator iterSeg = fccInventoryDTO.getFccSegInventoryDTOs().iterator(); iterSeg.hasNext();) {
					FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) iterSeg.next();
					System.out.println("	Segment Inventory > Segment Code : " + fccSegInventoryDTO.getSegmentCode());
					if (fccSegInventoryDTO.getFccSegBCInventories() != null) {
						if (fccInventoryDTO.getFlightId()==flightId1 && fccSegInventoryDTO.getSegmentCode().equals(segmentCode1))				
							assertEquals("Get Seat Inventory for Optimize Testing doesn't return correct number of FCC Seg Inventories " +
									"when All BCs selected and flight id " + String.valueOf(flightId1) + 
											" and Segment Code " + segmentCode1 + " !" ,segBCInvCount1.intValue(),fccSegInventoryDTO.getFccSegBCInventories().size());
						if (fccInventoryDTO.getFlightId()==flightId2 && fccSegInventoryDTO.getSegmentCode().equals(segmentCode2))				
							assertEquals("Get Seat Inventory for Optimize Testing doesn't return correct number of FCC Seg Inventories " +
									"when All BCs selected and flight id " + String.valueOf(flightId2) + 
											" and Segment Code " + segmentCode2 + " !" ,segBCInvCount2.intValue(),fccSegInventoryDTO.getFccSegBCInventories().size());
						for (Iterator iterFccSegBCInventory = fccSegInventoryDTO.getFccSegBCInventories().iterator(); iterFccSegBCInventory.hasNext();) {
							FCCSegBCInventory fccSegBCInventory = (FCCSegBCInventory) iterFccSegBCInventory.next();
							System.out.println("		BC Inventory > Booking Code : " + fccSegBCInventory.getBookingCode());
							System.out.println("		BC Inventory > Allocated seats : " + fccSegBCInventory.getSeatsAllocated());
						}
					}else {
						if (fccInventoryDTO.getFlightId()==flightId1 && fccSegInventoryDTO.getSegmentCode().equals(segmentCode1))				
							assertNotNull("Get Seat Inventory for Optimize Testing doesn't return Null for FCC Seg Inventories " +
									"when All BCs selected and flight id " + String.valueOf(flightId1) + 
											" and Segment Code " + segmentCode1 + " !" ,fccSegInventoryDTO.getFccSegBCInventories());
						if (fccInventoryDTO.getFlightId()==flightId2 && fccSegInventoryDTO.getSegmentCode().equals(segmentCode2))				
							assertNotNull("Get Seat Inventory for Optimize Testing doesn't return Null for FCC Seg Inventories " +
									"when All BCs selected and flight id " + String.valueOf(flightId2) + 
											" and Segment Code " + segmentCode2 + " !" ,fccSegInventoryDTO.getFccSegBCInventories());
					}
				}
			}else {
				if (fccInventoryDTO.getFlightId()==flightId1)				
					assertNotNull("Get Seat Inventory for Optimize Testing doesn't return correct number of FCC Seg Inventories " +
							"when All BCs selected and flight id " + String.valueOf(flightId1) + " !",fccInventoryDTO.getFccSegInventoryDTOs());
				if (fccInventoryDTO.getFlightId()==flightId2)				
					assertNotNull("Get Seat Inventory for Optimize Testing doesn't return correct number of FCC Seg Inventories " +
							"when All BCs selected and flight id " + String.valueOf(flightId2) + " !",fccInventoryDTO.getFccSegInventoryDTOs());
			}
		}
	}
	
	

	public void TestGetSeatInventoryForOptimization() throws ModuleException{
		Set flightIds;
		OptimizeSeatInvCriteriaDTO optimizeSeatInvCriteriaDTO;
		FlightInventoryBL flightInventoryBL = createFlightInventoryBL();		
		System.out.println("Test Seat Inventory for Optimize Testing");			
		
		
		System.out.println("Test Seat Inventory Testing - All Booking Claases Option and nothing else selected");
		
//		flightIds = new HashSet();
//		flightIds.add(new Integer(9501));
//		flightIds.add(new Integer(9510));
////		flightIds.add(new Integer(9100));
//		optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
//		optimizeSeatInvCriteriaDTO.setIncludeAllBCs(true);		
//		Collection col = flightInventoryBL.getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);		
////		assertEquals("Get Seat Inventory for Optimize Testing doesn't return correct number of FCC Inventories when All BCs selected !"
////				,3,col.size());
//		//AssertGetSeatInventory(col,9201,2,"SHJ/SSH/LXR",new Integer(2),9110,3,"SHJ/SSH",null);
//		AssertGetSeatInventory(col,9501,new Integer(3),"SHJ/SSH/LXR",new Integer(2),9510,new Integer(3),"SHJ/SSH",new Integer(0));
//		
//		System.out.println("Test Seat Inventory Testing - Standard Booking Claases Option and nothing else selected");
//		flightIds = new HashSet();
//		flightIds.add(new Integer(9501));
//		flightIds.add(new Integer(9510));
////		flightIds.add(new Integer(9100));
//		optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
//		optimizeSeatInvCriteriaDTO.setIncludeAllBCs(false);
//		optimizeSeatInvCriteriaDTO.setIncludeStandardBCsOnly(true);
//		col = flightInventoryBL.getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);
//		AssertGetSeatInventory(col,9501,new Integer(3),"SSH/LXR",new Integer(1),9510,new Integer(0),"SHJ/SSH",new Integer(0));
//		
//		System.out.println("Test Seat Inventory Testing - Selected Booking Claases Option and nothing else selected");
//		flightIds = new HashSet();
//		flightIds.add(new Integer(9501));
//		flightIds.add(new Integer(9510));
////		flightIds.add(new Integer(9100));
//		Set segmentCodes = new HashSet();
//		segmentCodes.add("SSH/LXR");
//		segmentCodes.add("SHJ/SSH");
//		//segmentCodes.add();
//		optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
//		optimizeSeatInvCriteriaDTO.setIncludeAllBCs(false);
//		optimizeSeatInvCriteriaDTO.setIncludeStandardBCsOnly(false);
//		optimizeSeatInvCriteriaDTO.setSelectedBCs(segmentCodes);
//		col = flightInventoryBL.getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);
//		AssertGetSeatInventory(col,9501,new Integer(2),"SSH/LXR",new Integer(1),9510,new Integer(0),"SHJ/SSH",new Integer(0));
//		
//		System.out.println("Test Seat Inventory Testing - All Booking Claases Option and availability validation on segment" +
//				" inventory selected");
//		flightIds = new HashSet();
//		flightIds.add(new Integer(9501));
//		flightIds.add(new Integer(9510));
////		flightIds.add(new Integer(9100));
//		optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
//		optimizeSeatInvCriteriaDTO.setIncludeAllBCs(true);
//		optimizeSeatInvCriteriaDTO.setPerformAvailableOnSegment(true);
//		optimizeSeatInvCriteriaDTO.setAvailabilityOperator(OptimizeSeatInvCriteriaDTO.AVAILABILITY_OPERATOR_GREATER_THAN);
//		optimizeSeatInvCriteriaDTO.setNoOfSeats(110);
//		col = flightInventoryBL.getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);
//		AssertGetSeatInventory(col,9501,new Integer(2),"SSH/LXR",new Integer(1),9510,new Integer(3),"SHJ/SSH",new Integer(0));
//		
//		System.out.println("Test Seat Inventory Testing - All Booking Claases Option and availability validation on BC inventory selected");
//		flightIds = new HashSet();
//		flightIds.add(new Integer(9501));
//		flightIds.add(new Integer(9510));
////		flightIds.add(new Integer(9100));
//		optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
//		optimizeSeatInvCriteriaDTO.setIncludeAllBCs(true);
//		optimizeSeatInvCriteriaDTO.setPerformAvailableOnBC(true);
//		optimizeSeatInvCriteriaDTO.setAvailabilityOperator(OptimizeSeatInvCriteriaDTO.AVAILABILITY_OPERATOR_GREATER_THAN);
//		optimizeSeatInvCriteriaDTO.setNoOfSeats(189);
//		col = flightInventoryBL.getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);
//		AssertGetSeatInventory(col,9501,new Integer(3),"SSH/LXR",new Integer(1),9510,new Integer(0),"SHJ/SSH",new Integer(0));
//		
//		
//		
//		System.out.println("Test Seat Inventory Testing - All Booking Claases Option and an agent on BC inventory selected");
//		flightIds = new HashSet();
//		flightIds.add(new Integer(9501));
//		flightIds.add(new Integer(9510));
////		flightIds.add(new Integer(9100));
//		optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
//		optimizeSeatInvCriteriaDTO.setIncludeAllBCs(true);
//		optimizeSeatInvCriteriaDTO.setAgentCode("A001");	
//		col = flightInventoryBL.getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);		
//		AssertGetSeatInventory(col,9501,new Integer(3),"SSH/LXR",new Integer(1),9510,new Integer(0),"SHJ/SSH",new Integer(0));
//		
		
		
//		//The results set
//		Set setResults = new HashSet();
//		//A result FCC Seg Inventory (Flight No, COS, Segment Code, Set of Booking Codes)
//		Set setInvSegResults = new HashSet();
//		setInvSegResults.add(new Integer(8000));
//		setInvSegResults.add("The Cos");
//		setInvSegResults.add("The Segment Code");		
//		//A result FCC Seg BC Inventory (Booking Codes)
//		Set setInvSegBCResults = new HashSet();
//		setInvSegBCResults.add("a Booking code");
//		setInvSegBCResults.add("a Booking code");		
//		setInvSegResults.add(setInvSegBCResults);
//		setResults.add(setInvSegResults);
//		
//		int numberOfFCCInvs = 0;

//		
//		
//		
//		
//		flightIds = new HashSet();
//		flightIds.add(new Integer(8000));
//		flightIds.add(new Integer(8001));
//		OptimizeSeatInvCriteriaDTO optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();
//		optimizeSeatInvCriteriaDTO.setIncludeAllBCs(false);
//		optimizeSeatInvCriteriaDTO.setIncludeStandardBCsOnly(false);
//		Set segmentCodes = new HashSet();
//		segmentCodes.add("CMB/SHJ");
//		segmentCodes.add("SHJ/ALY");
//		Collection col = flightInventoryBL.getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);
//		assertNotNull("Get Seat Inventory returns null",col);
//		for (Iterator iter = col.iterator(); iter.hasNext();) {
//			FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO) iter.next();
//			System.out.println("Flight Id " + String.valueOf(fccInventoryDTO.getFlightId()));
//			for (Iterator iterSeg = fccInventoryDTO.getFccSegInventoryDTOs().iterator(); iterSeg.hasNext();) {
//				FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) iterSeg.next();
//				System.out.println("	Segment Inventory > Segment Code : " + fccSegInventoryDTO.getSegmentCode());
//				for (Iterator iterFccSegBCInventory = fccSegInventoryDTO.getFccSegBCInventories().iterator(); iterFccSegBCInventory.hasNext();) {
//					FCCSegBCInventory fccSegBCInventory = (FCCSegBCInventory) iterFccSegBCInventory.next();
//					System.out.println("		BC Inventory > Booking Code : " + fccSegBCInventory.getBookingCode());
//					System.out.println("		BC Inventory > Allocated seats : " + fccSegBCInventory.getSeatsAllocated());
//				}
//			}
//		}
	}


	public void testblockSeatsForRes() throws ModuleException{
		Set flightSegSeatDistDTOs = new HashSet();
        SegmentSeatDistsDTO flightSegSeatDistDTO = new SegmentSeatDistsDTO();
		flightSegSeatDistDTO.setCabinClassCode("I");
		flightSegSeatDistDTO.setFlightId(9561);
		flightSegSeatDistDTO.setSegmentCode("SHJ/SSH/LXR"); //9561
		flightSegSeatDistDTO.setNoOfInfantSeats(2);//InfantsOnHold:1 > 3
		//OnHold:5 > 5+3+1=9
		// Avail Seats 20 > 16
		
		Collection seatDists = new ArrayList();
        SeatDistribution seatDist = new SeatDistribution();
        seatDist.setBookingClassCode("XS1");//0 > 3 // 9553  Not Fixed 
        seatDist.setNoOfSeats(3);
        seatDists.add(seatDist);
        //Availale 190 > 187
        //Alocated 200
        
        SeatDistribution seatDist1 = new SeatDistribution();
        seatDist1.setBookingClassCode("XN1");//3 > 4 //9554  Not Fixed
        seatDist1.setNoOfSeats(1);
        seatDists.add(seatDist1);
        //Availale 9 > 8
        //Alocated 20

		
				
		flightSegSeatDistDTO.setSeatDistributions(seatDists);
		
		flightSegSeatDistDTOs.add(flightSegSeatDistDTO);
		
        //FIX ME
		//createFlightInventoryResBL().blockSeatsForRes((Collection)flightSegSeatDistDTOs);
		
		//Check the results
		
				
		FCCSegInventory fccSegInventory =  getFlightInventoryDAO().getFCCSegInventory(9561,"I","SHJ/SSH/LXR");
//		assertEquals("Blocking: Source sold infants are not updated",7,fccSegInventory.getSoldInfantSeats());
		assertEquals("Blocking: Source onhold infants are not updated",3,fccSegInventory.getOnholdInfantSeats());
		assertEquals("Blocking: Source onhold are not updated",9,fccSegInventory.getOnHoldSeats());
		
		FCCSegBCInventory fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9561,"I","SHJ/SSH/LXR","XS1");
//		assertEquals("Blocking: Source sold are not updated for 'XS1' BC ",13,fccSegBCInventory.getSeatsSold());
		assertEquals("Blocking: Source onhold are not updated for 'XS1' BC",new Integer(3),fccSegBCInventory.getOnHoldSeats());
		
		fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9561,"I","SHJ/SSH/LXR","XN1");
//		assertEquals("Blocking: Source sold are not updated for 'XN1' BC ",10,fccSegBCInventory.getSeatsSold());
		assertEquals("Blocking: Source onhold are not updated for 'XN1' BC ",new Integer(4),fccSegBCInventory.getOnHoldSeats());
		
		System.out.println(">>>>>>>> TestblockSeatsForRes is successful <<<<<<<<<<<<");
		
	}
    
	public void TestDeleteFlightInventories(){
		List flightIds =new ArrayList();
		//[TODO:testdata] prepare test data for this
		flightIds.add(new Integer(9120));
//		createFlightInventoryBL().deleteFlightInventories(flightIds);
	}
	
	public void TestInvalidateFlightInventories(){
		//[TODO:testdata] prepare test data for this
//		createFlightInventoryBL().invalidateFlightInventory(9120);
	}

	public void TestMoveBlockedSeatsToSold(){
    	
    	Set tempFlightSegSeats = new HashSet();
    	tempFlightSegSeats.add(new Integer(9000));
    	tempFlightSegSeats.add(new Integer(9001));
    	tempFlightSegSeats.add(new Integer(9002));
    	
    	//Seg Alloc //9563
    	//	CabinClassCode("I");
    	//	FlightId(9563);
    	//	SegmentCode("SHJ/SSH/LXR"); 
    	//	InfantSeats(1);
    	//InfantsOnHold:1 > 0 //SoldInfants:2 > 3
    	//OnHold:5 > 5-3-1=1
    	//Sold: 5 > 5+3+1=9 
    	//Avail Seats  16
    		  
    	//Seg BC Alloc BC: "XS1", Seats:3; // 9563  Not Fixed
    	//	OnHold:3 > 0
    	//	Sold:10 > 13
    	//Availale  187
    	//Alocated 200
    	
    	 
    	//Seg BC Alloc BC: "XN1", Seats:1; //9564  Not Fixed
    	//	OnHold:4 > 3
    	//	Sold: 8 > 9
    	//Availale  8
    	//Alocated 20
    			
    	
    //	createFlightInventoryResBL().moveBlockedSeatsToSold((Collection)tempFlightSegSeats);
    	
    	//Check the results
    	
    			
    	FCCSegInventory fccSegInventory =  getFlightInventoryDAO().getFCCSegInventory(9563,"I","SHJ/SSH/LXR");
    	assertEquals("Blocking: Source sold infants are not updated",3,fccSegInventory.getSoldInfantSeats());
    	assertEquals("Blocking: Source onhold infants are not updated",0,fccSegInventory.getOnholdInfantSeats());
    	assertEquals("Blocking: Source onhold are not updated",1,fccSegInventory.getOnHoldSeats());
    	assertEquals("Blocking: Source sold are not updated",9,fccSegInventory.getSeatsSold());
    	
    	FCCSegBCInventory fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9563,"I","SHJ/SSH/LXR","XS1");
    	assertEquals("Blocking: Source sold are not updated for 'XS1' BC ",new Integer(13),fccSegBCInventory.getSeatsSold());
    	assertEquals("Blocking: Source onhold are not updated for 'XS1' BC",new Integer(0),fccSegBCInventory.getOnHoldSeats());
    	
    	fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9563,"I","SHJ/SSH/LXR","XN1");
    	assertEquals("Blocking: Source sold are not updated for 'XN1' BC ",new Integer(9),fccSegBCInventory.getSeatsSold());
    	assertEquals("Blocking: Source onhold are not updated for 'XN1' BC ",new Integer(3),fccSegBCInventory.getOnHoldSeats());
    	
    	System.out.println(">>>>>>>> testMoveBlockedSeatsToSold is successful <<<<<<<<<<<<");
	}	

	public void TestReleaseBlockedSeats(){
	
    	Set tempFlightSegSeats = new HashSet();
    	tempFlightSegSeats.add(new Integer(9003));
    	tempFlightSegSeats.add(new Integer(9004));
    	tempFlightSegSeats.add(new Integer(9005));
    	
    	//Seg Alloc //9564
    	//	CabinClassCode("I");
    	//	FlightId(9564);
    	//	SegmentCode("SHJ/SSH/LXR"); 
    	//	InfantSeats(1);
    	//InfantsOnHold:1 > 0 
    	//OnHold:5 > 5-3-1=1	
    	//Avail Infant Seats  17 > 18
    	//Avail Seats  135 > 139
    		  
    	//Seg BC Alloc BC: "XS1", Seats:3; // 9565  Not Fixed
    	//	OnHold:3 > 0
    	//Availale  190 > 193
    	//Alocated 200
    	
    	 
    	//Seg BC Alloc BC: "XN1", Seats:1; //9566  Not Fixed
    	//	OnHold:4 > 3
    	//Availale  9 > 10
    	//Alocated 20
    			
    	
//    	createFlightInventoryResBL().releaseBlockedSeats((Collection)tempFlightSegSeats);
    	
    	//Check the results
    	
    			
    	FCCSegInventory fccSegInventory =  getFlightInventoryDAO().getFCCSegInventory(9564,"I","SHJ/SSH/LXR");
    	assertEquals("Release Blocked: Source available infants are not updated",18,fccSegInventory.getAvailableInfantSeats());
    	assertEquals("Release Blocked: Source onhold infants are not updated",0,fccSegInventory.getOnholdInfantSeats());
    	assertEquals("Release Blocked: Source onhold are not updated",1,fccSegInventory.getOnHoldSeats());
    	assertEquals("Release Blocked: Source available are not updated",139,fccSegInventory.getAvailableSeats());
    	
    	FCCSegBCInventory fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9564,"I","SHJ/SSH/LXR","XS1");
    	assertEquals("Release Blocked: Source sold are not updated for 'XS1' BC ",new Integer(193),fccSegBCInventory.getSeatsAvailable());
    	assertEquals("Release Blocked: Source onhold are not updated for 'XS1' BC",new Integer(0),fccSegBCInventory.getOnHoldSeats());
    	
    	fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9564,"I","SHJ/SSH/LXR","XN1");
    	assertEquals("Release Blocked: Source sold are not updated for 'XN1' BC ",new Integer(10),fccSegBCInventory.getSeatsAvailable());
    	assertEquals("Release Blocked: Source onhold are not updated for 'XN1' BC ",new Integer(3),fccSegBCInventory.getOnHoldSeats());
    	
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9003)));
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9004)));
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9005)));
    	
    	System.out.println(">>>>>>>> testReleaseBlockedSeats is successful <<<<<<<<<<<<");
	}	

    public void TestRemoveBlockedSeats(){
    	
    	Set tempFlightSegSeats = new HashSet();
    	tempFlightSegSeats.add(new Integer(9003));
    	tempFlightSegSeats.add(new Integer(9004));
    	tempFlightSegSeats.add(new Integer(9005));
    	
    	createFlightInventoryResBL().removeBlockedSeats((Collection)tempFlightSegSeats);
    	
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9006)));
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9007)));
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9008)));
    	
    	System.out.println(">>>>>>>> testRemoveBlockedSeats is successful <<<<<<<<<<<<");
    }
    
    public void TestCleanupBlockedSeats(){
    	
    
//    	createFlightInventoryResBL().cleanupBlockedSeats(1);
    	
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9009)));
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9010)));
//    	assertNull("Release Blocked: Temporary records were not removed", getFlightInventoryResDAO().getTempSegBcAlloc(new Integer(9011)));
    	
    	System.out.println(">>>>>>>> testCleanupBlockedSeats is successful <<<<<<<<<<<<");
    }




	
	public void TestReleaseOnHoldSeats() throws ModuleException{
		
		//TODO: remove FlightSegmentSeatDistributionDTO,
		
		
		TransferSeatDTO transferSeatDTO = new TransferSeatDTO();
		transferSeatDTO.setCabinClassCode("I");
		transferSeatDTO.setTargetFlightId(9551);
		transferSeatDTO.setTargetSegCode("SHJ/SSH"); //9551
		//Current:	Infants: Sold:2	OnHold:1
		//After:	Infants: Sold:4	OnHold:2
		//	BC Inv:
		//		XS1:	Current: Sold:5	OnHold:5
		//				after: 	 Sold:8	OnHold:5
		//		XN1:	Current: Sold:-	OnHold:-
		//				after: 	 Sold:2	OnHold:1
		
		Set flightSegSeatDistDTOs = new HashSet();
		FlightSegSeatDistDTO flightSegSeatDistDTO = new FlightSegSeatDistDTO();
		flightSegSeatDistDTO.setCabinClassCode("I");
		flightSegSeatDistDTO.setFlightId(9551);
		flightSegSeatDistDTO.setSegmentCode("SHJ/SSH/LXR"); //9553
		flightSegSeatDistDTO.setInfantSeats(2);//5 > 3
		flightSegSeatDistDTO.setInfantOnHoldSeats(1);//3 > 2
		
		
		HashMap seatDistributions = new HashMap();	
		
		SeatDistForRelease seatDistribution = new SeatDistForRelease();
		seatDistribution.setBookingCode("XS1");//9553  Not Fixed  
		seatDistribution.setSoldSeats(3);//10 > 7
		seatDistribution.setOnHoldSeats(0);//0 > 0
		//Alocated 200
		seatDistributions.put("XS1",seatDistribution);
		
		seatDistribution = new SeatDistForRelease();
		seatDistribution.setBookingCode("XN1"); //9554  Not Fixed
		seatDistribution.setSoldSeats(2);//8 > 6
		seatDistribution.setOnHoldSeats(1);//3 > 2
		//Alocated 20
		seatDistributions.put("XN1",seatDistribution);
		
				
		flightSegSeatDistDTO.setSeatDist(seatDistributions);
		
		flightSegSeatDistDTOs.add(flightSegSeatDistDTO);
		
		transferSeatDTO.setSourceFltSegSeatDists(flightSegSeatDistDTOs);
		
		createFlightInventoryResBL().transferSeats(transferSeatDTO);
		
		//Check the results
		
		FCCSegInventory fccSegInventory =  getFlightInventoryDAO().getFCCSegInventory(9551,"I","SHJ/SSH");
		assertEquals("Target sold infants are not updated",4,fccSegInventory.getSoldInfantSeats());
		assertEquals("Target onhold infants are not updated",2,fccSegInventory.getOnholdInfantSeats());
		
		FCCSegBCInventory fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9551,"I","SHJ/SSH","XS1");
		assertEquals("Target sold are not updated for 'XS1' BC ",new Integer(8),fccSegBCInventory.getSeatsSold());
		assertEquals("Target onhold are not updated for 'XS1' BC",new Integer(5),fccSegBCInventory.getOnHoldSeats());
		
		fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9551,"I","SHJ/SSH","XN1");
		assertEquals("Target sold are not updated for 'XN1' BC ",new Integer(2),fccSegBCInventory.getSeatsSold());
		assertEquals("Target onhold are not updated for 'XN1' BC ",new Integer(1),fccSegBCInventory.getOnHoldSeats());
		
		fccSegInventory =  getFlightInventoryDAO().getFCCSegInventory(9551,"I","SHJ/SSH/LXR");
		assertEquals("Source sold infants are not updated",new Integer(3),new Integer(fccSegInventory.getSoldInfantSeats()));
		assertEquals("Source onhold infants are not updated",new Integer(2),new Integer(fccSegInventory.getOnholdInfantSeats()));
		
		fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9551,"I","SHJ/SSH/LXR","XS1");
		assertEquals("Source sold are not updated for 'XS1' BC ",new Integer(7),fccSegBCInventory.getSeatsSold());
		assertEquals("Source onhold are not updated for 'XS1' BC",new Integer(0),fccSegBCInventory.getOnHoldSeats());
		
		fccSegBCInventory =  getFlightInventoryDAO().getFCCSegBCInventory(9551,"I","SHJ/SSH/LXR","XN1");
		assertEquals("Source sold are not updated for 'XN1' BC ",new Integer(6),fccSegBCInventory.getSeatsSold());
		assertEquals("Source onhold are not updated for 'XN1' BC ",new Integer(2),fccSegBCInventory.getOnHoldSeats());
		
	}
    
    public void TestFCCInventoryUpdatability() throws ModuleException{
        AircraftModel model = (AircraftModel)getAircraftBD().getAircraftModel("A320XINV1");
        Iterator it = model.getAircraftCabinCapacitys().iterator();
        AircraftCabinCapacity ccCapacity = (AircraftCabinCapacity)it.next();
        ccCapacity.setSeatCapacity(140);
        ccCapacity.setInfantCapacity(15);
        List flightIds = new ArrayList();
        flightIds.add(new Integer(19050));
//        assertTrue(createFlightInventoryBL().checkFCCInventoryUpdatability(flightIds, model));
        
        ccCapacity.setSeatCapacity(129);
        ccCapacity.setInfantCapacity(15);
        
//        assertFalse(createFlightInventoryBL().checkFCCInventoryUpdatability(flightIds, model));
    }
    
    public void TestUpdateFCCInventories() throws ModuleException{
        AircraftModel model = (AircraftModel)getAircraftBD().getAircraftModel("A320XINV1");
        Iterator it = model.getAircraftCabinCapacitys().iterator();
        AircraftCabinCapacity ccCapacity = (AircraftCabinCapacity)it.next();
        ccCapacity.setSeatCapacity(140);
        ccCapacity.setInfantCapacity(15);
        List flightIds = new ArrayList();
        flightIds.add(new Integer(19050));
//        createFlightInventoryBL().updateFCCInventoriesNew(flightIds, model);
    }
    
    public void TestGetFlightSegmentInventoriesSummary() throws ModuleException {
        List flightIds = new ArrayList();
        flightIds.add(new Integer(19050));
        createFlightInventoryBL().getFlightSegmentInventoriesSummary(flightIds);
    }

	private FlightInventoryResDAO getFlightInventoryResDAO(){
		return (FlightInventoryResDAO) AirInventoryUtil.getInstance().getLocalBean("FlightInventoryResDAOImplProxy");
	}
	
	private FlightInventoryResBL createFlightInventoryResBL(){
		return new FlightInventoryResBL();
	}


	private FlightInventoryDAO getFlightInventoryDAO(){
		return (FlightInventoryDAO) AirInventoryUtil.getInstance().getLocalBean("FlightInventoryDAOImplProxy");
	}

	
	private FlightInventoryBL createFlightInventoryBL(){
		return new FlightInventoryBL();
	}	
}
