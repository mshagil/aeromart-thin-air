package com.isa.thinair.airinventory.core.config;

import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestAirInventoryConfig extends PlatformTestCase {
	
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	public void testGetQuery(){
		String [] arr = {"?,?,?,?","1"};
		String query = getAirInventoryConfig().
						getQuery("SINGLE_FLT_ONEWAY_MINFARE_SEAT_AVAIL_SEARCH",arr);
		assertNotNull(query);
	}
	
	protected void tearDown() throws Exception{
		super.tearDown();
	}
	
	private AirInventoryConfig getAirInventoryConfig(){
		return (AirInventoryConfig)AirInventoryUtil.getInstance().getModuleConfig();
	}
}
