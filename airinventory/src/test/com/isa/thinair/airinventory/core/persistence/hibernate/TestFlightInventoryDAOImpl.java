/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on August 17, 2005
 * 
 * TestFlightInventoryMasterDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.InterceptingFlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.bl.FlightInventoryBL;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Thejaka
 *
 */
public class TestFlightInventoryDAOImpl extends PlatformTestCase {
	
	protected void setUp(){
		
	}
	
//	public void testGetFCCInventory() throws ModuleException{		
//		System.out.println("Get FCC Inventory Testing");
//		Set orderBySet = new HashSet();
//		orderBySet.add(FlightInventoryDAO.ORDER_BY_SEGMENT_CODE);
////		FlightInventoryDAO daoFIM=initializeFlightInventoryDAO();
//		FlightInventoryDAO daoFIM= getFlightInventoryDAO();
//		Set flightIds = new HashSet();
//		flightIds.add(new Integer(9779));
//		flightIds.add(new Integer(9780));
//		Collection col = daoFIM.getFCCSegInventoryDTOs( flightIds,  "I",  null, 
//				 false,  orderBySet, true,true);
//		
//		
//		
////		Collection col = daoFIM.getFCCSegInventoryDTOs( 9779,  "I",  null, false,  orderBySet, true,true);
//		System.out.println("Get FCC Inventory Testing : Got collection");
//		for (Iterator iter = col.iterator(); iter.hasNext();) {
//			FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO) iter.next();
//			System.out.println("Flight Id : " + String.valueOf(fccInventoryDTO.getFlightId()) + "Infant Capacity " + String.valueOf(fccInventoryDTO.getInfantCapacity()));
//			for (Iterator iterSeg = fccInventoryDTO.getFccSegInventoryDTOs().iterator(); iterSeg.hasNext();) {
//				FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) iterSeg.next();
//				System.out.println("---Segment Inventory > Segment Code : " + fccSegInventoryDTO.getSegmentCode());
//				for (Iterator iterFccSegBCInventory = fccSegInventoryDTO.getFccSegBCInventories().iterator(); iterFccSegBCInventory.hasNext();) {
//					FCCSegBCInventory fccSegBCInventory = (FCCSegBCInventory) iterFccSegBCInventory.next();
//					System.out.println("--------BC Inventory > Booking Code : " + fccSegBCInventory.getBookingCode());
//					System.out.println("--------BC Inventory > Allocated seats : " + fccSegBCInventory.getSeatsAllocated());
//				}
//			}
//		}
//	}
	
	public void testCreateFlightInventory() throws ModuleException{
		FlightInventoryBL bl = new FlightInventoryBL();
		AircraftModel aircraftModel = new AircraftModel();
		AircraftCabinCapacity economy = new AircraftCabinCapacity();
		economy.setCabinClassCode("I");
		economy.setSeatCapacity(170);
		economy.setInfantCapacity(5);
		Set capacities = new HashSet();
		capacities.add(economy);
		aircraftModel.setAircraftCabinCapacitys(capacities);
		
		Collection segments = new HashSet();
		SegmentDTO seg1 = new SegmentDTO();
		seg1.setSegmentCode("CMB/SHJ");
		InterceptingFlightSegmentDTO interceptingSeg1 = new InterceptingFlightSegmentDTO();
		interceptingSeg1.setFlightId(9000);
		interceptingSeg1.setInterceptingSegCode("CMB/SHJ/DOH");
		Collection interceptingSegs = new HashSet();
		interceptingSegs.add(interceptingSeg1);
		seg1.setInterceptingFlightSeg(interceptingSegs);
		segments.add(seg1);
		
		SegmentDTO seg2 = new SegmentDTO();
		seg2.setSegmentCode("CMB/SHJ/DOH");
		Collection interceptingSegs1 = new HashSet();
		
		InterceptingFlightSegmentDTO interceptingSeg2 = new InterceptingFlightSegmentDTO();
		interceptingSeg2.setFlightId(9000);
		interceptingSeg2.setInterceptingSegCode("CMB/SHJ");
		interceptingSegs1.add(interceptingSeg2);
		
		InterceptingFlightSegmentDTO interceptingSeg3 = new InterceptingFlightSegmentDTO();
		interceptingSeg3.setFlightId(9000);
		interceptingSeg3.setInterceptingSegCode("SHJ/DOH");
		interceptingSegs1.add(interceptingSeg3);
		
		seg1.setInterceptingFlightSeg(interceptingSegs1);
		segments.add(seg2);
		
		SegmentDTO seg3 = new SegmentDTO();
		Collection interceptingSegs3 = new HashSet();
		seg3.setSegmentCode("SHJ/DOH");
		InterceptingFlightSegmentDTO interceptingSeg4 = new InterceptingFlightSegmentDTO();
		interceptingSeg4.setFlightId(9000);
		interceptingSeg4.setInterceptingSegCode("CMB/SHJ/DOH");
		
		interceptingSegs3.add(interceptingSeg4);
		seg3.setInterceptingFlightSeg(interceptingSegs3);
		segments.add(seg3);
		
		CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
		createFlightInventoryRQ.setFlightId(9000);
		createFlightInventoryRQ.setAircraftModel(aircraftModel);
		createFlightInventoryRQ.setSegmentDTOs(segments);
		
		bl.createFlightInventory(createFlightInventoryRQ);
	}
	
	public void testUpdateFCCSegmentInventory2(){
		getFlightInventoryDAO().updateFCCSegmentInventory(9001, "I", "CMB/SHJ");
	}
	
	public void testUpdateFCCSegmentBCInventory(){
//		getFlightInventoryDAO().updateFCCSegmentBCInventory(9001, "I", "CMB/SHJ", "T1",99, FCCSegBCInventory.STATUS_OPEN, false, true,0);
	}
	
//	public void testCheckFCCInventoryUpdatability(){
//		assertFalse(getFlightInventoryDAO().isFCCInventoryUpdatable(9001, "I", 129, 5));
//	}
	
//	public void testUpdateAllocatedCapacities(){
////		getFlightInventoryDAO().updateAllocatedCapacities(9001, "I", 135, 9);
//		assertTrue(getFlightInventoryDAO().isFCCInventoryUpdatable(9001, "I", 130, 5));
//	}
	
//	public void TestDeleteFlightInventory(){	
//		getFlightInventoryDAO().deleteInterceptingFCCSegAllocs(9150);
//		Collection fccInventories = getFlightInventoryDAO().getFCCInventory(9150,"I",null);
//		assertEquals(1, fccInventories.size());
//		
////		getFlightInventoryDAO().deleteFlightInventory(9150);
//		
//		fccInventories = getFlightInventoryDAO().getFCCInventory(9150,"I", null);
//		assertEquals(0, fccInventories.size());
//	}
	
//	public void testGetOverlappingFlightId(){
//		Integer overlappingFlightId = getFlightInventoryDAO().getOverlappingFlightId(9100);
//		assertNotNull(overlappingFlightId);
//		assertEquals(9101, overlappingFlightId.intValue());
//		assertNull(getFlightInventoryDAO().getOverlappingFlightId(9001));
//	}
	
//	public void testInvalidateFlightInventory(){
////		getFlightInventoryDAO().invalidateFlightInventory(9110);
//		Collection fccInventories = getFlightInventoryDAO().getFCCInventory(9110, "I", null);
//		Iterator fccInvIt = fccInventories.iterator();
//		while (fccInvIt.hasNext()){
//			FCCInventory fccInv = (FCCInventory)fccInvIt.next();
//			Iterator segInvIt = fccInv.getSegAlloc().iterator();
//			while (segInvIt.hasNext()){
//				FCCSegInventory segInv = (FCCSegInventory)segInvIt.next();
//				assertEquals(false, segInv.getValidFlag());
//			}
//		}
//	}
	
	public void testCheckMaximumFCCSegInventoriesAvailability(){
		assertEquals(120, getFlightInventoryDAO().checkMaximumFCCSegInventoriesAvailability(9120, "I", "SHJ/SSH"));
	}
	
	public void testGetFCCSegmentInventoriesSummary(){
		Collection flightIds = new ArrayList();	
		flightIds.add(new Integer(9161));
		flightIds.add(new Integer(9162));
		flightIds.add(new Integer(9163));
		flightIds.add(new Integer(9164));
		Collection segmentCodes = new ArrayList();
		segmentCodes.add("CMB/SHJ");
		List flightSegmets = getFlightInventoryDAO().getFCCSegmentInventoriesSummary(flightIds, "I", segmentCodes);
		assertEquals(2, flightSegmets.size());
	}
	
    public void testGetFlightInventorySummary(){
        Collection flightIds = new ArrayList();
        flightIds.add(new Integer(9000));
        flightIds.add(new Integer(9001));
        HashMap fltInvSummaries = getFlightInventoryDAO().getFlightInventoriesSummary(flightIds);
        assertNotNull(fltInvSummaries);
    }
    
	//[TODO:testdata] test with proper test data
	public void testUpdateAvailability(){
		List segInvIds = new ArrayList();
		segInvIds.add(new Integer(20242));
		getFlightInventoryDAO().updateFCCSegmentInventoriesAvailability(segInvIds);
	}
    
    public void testGetFlightSegmentInventoriesSummary() {
        List flightIds = new ArrayList();
        flightIds.add(new Integer(9001));
        Collection col = getFlightInventoryDAO().getFlightSegmentInventoriesSummary(flightIds);
        Iterator itInvSum = col.iterator();
        while (itInvSum.hasNext()) {
            FlightInventorySummaryDTO invSummaryDTO = (FlightInventorySummaryDTO) itInvSum.next();
            Collection segInv = invSummaryDTO.getSegmentInventories();
            Iterator itSegInv = segInv.iterator();
            while (itSegInv.hasNext()) {
                FCCSegInvSummaryDTO invSegDTO = (FCCSegInvSummaryDTO) itSegInv.next();  
            }
        }
    }
    
	private FlightInventoryDAO getFlightInventoryDAO(){
		return (FlightInventoryDAO)AirInventoryUtil.getInstance().getLocalBean("FlightInventoryDAOImplProxy");
	}
	
	protected void tearDown(){	
	}
}

