package com.isa.thinair.airinventory.api.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.exception.ModuleException;

import com.isa.thinair.platform.api.util.PlatformTestCase;


public class TestBookingClassBDImpl extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetBookingClass() throws ModuleException {
		assertNotNull(getBookingClassBD().getBookingClass("T1"));
	}

	public void testGetBookingClassesIntIntList() throws ModuleException {
		List list = new ArrayList();
		list.add("bookingCode");
		assertEquals(3, getBookingClassBD().getBookingClasses(0,3,list).getPageData().size());
	}

	public void testGetBookingClassesListIntIntList() throws ModuleException {
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setFieldName("bookingCode");
		criterion.setCondition(ModuleCriterion.CONDITION_LIKE);
		List values = new ArrayList();
		values.add("F%");
		criterion.setValue(values);
		List criteria = new ArrayList();
		criteria.add(criterion);
		List list = new ArrayList();
		list.add("bookingCode");
		Collection data = getBookingClassBD().getBookingClasses(criteria, 0, 3, list).getPageData();
		assertTrue(data.size()>=1);
	}

	public void testCreateBookingClass() throws ModuleException {
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		bc.setBookingCode("T100");
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(9));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassBD().createBookingClass(bcDTO);
		
		getBookingClassBD().deleteBookingClass("T100");
	}
	
	public void testNestRankShifting() throws ModuleException {
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		bc.setBookingCode("T100");
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(7));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassBD().createBookingClass(bcDTO);
		
		bc = new BookingClass();
		bc.setBookingCode("T101");
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(8));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassBD().createBookingClass(bcDTO);
		
		bc = new BookingClass();
		bc.setBookingCode("T102");
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(7));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassBD().createBookingClass(bcDTO);
		
		assertEquals(new Integer(9), getBookingClassBD().getBookingClass("T101").getNestRank());
		getBookingClassBD().deleteBookingClass("T100");
		getBookingClassBD().deleteBookingClass("T101");
		getBookingClassBD().deleteBookingClass("T102");
	}

	public void testUpdateBookingClass() throws ModuleException {
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		bc.setBookingCode("T100");
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(9));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassBD().createBookingClass(bcDTO);
		
		BookingClass bcLoaded = getBookingClassBD().getBookingClass("T100");
		bcLoaded.setCabinClassCode("I");
		bcLoaded.setNestRank(new Integer(8));
		bcLoaded.setStatus(BookingClass.Status.INACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bcLoaded);
		getBookingClassBD().updateBookingClass(bcDTO);
		
		BookingClass bcUpdated = getBookingClassBD().getBookingClass("T100");
		assertEquals(new Integer(8), bcUpdated.getNestRank());
		
		getBookingClassBD().deleteBookingClass("T100");
	}

	public void testDeleteBookingClass() throws ModuleException {
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		bc.setBookingCode("T100");
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(9));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassBD().createBookingClass(bcDTO);
		
		getBookingClassBD().deleteBookingClass("T100");
		
		BookingClass bcDeleted = getBookingClassBD().getBookingClass("T100");
		assertNull(bcDeleted);
	}
	
	private BookingClassBD getBookingClassBD(){
		return (BookingClassBD)AirInventoryUtil.getInstance().
		lookupServiceBD("airinventory", "bookingclass.service");
	}
}
