package com.isa.thinair.airinventory.api.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestFlightInventoryBDImpl extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

//	public void testCreateFlightInventory() {
//
//	}

	public void TestGetFCCInventory() throws ModuleException {
		Collection fccInventories = getFlightInventoryBD().getFCCInventory(8000, "I", null);
		assertEquals(1, fccInventories.size());
		Iterator it = fccInventories.iterator();
		
		FCCInventoryDTO fccInventory = (FCCInventoryDTO)it.next();
		Collection collection = fccInventory.getFccSegInventoryDTOs();
		Iterator segIterator = collection.iterator();
		while (segIterator.hasNext()){
			FCCSegInventoryDTO segInventory = (FCCSegInventoryDTO)segIterator.next();
			assertEquals("CMB/SHJ", segInventory.getSegmentCode());
			Collection bcInventories = segInventory.getFccSegBCInventories();
			assertEquals(4, bcInventories.size());
			Iterator bcIterator = bcInventories.iterator();
			while (bcIterator.hasNext()){
				FCCSegBCInventory bcInv = (FCCSegBCInventory)bcIterator.next();
				assertEquals("CMB/SHJ", bcInv.getSegmentCode());
			}
		}
	}

//	public void testCheckFCCInventoryUpdatability() {
//
//	}
//
//	public void testUpdateFCCInventories() {
//
//	}
//
//	public void testUpdateFCCSegInventory() {
//
//	}
//
	public void TestRollFlightInventory() throws ModuleException {
//		InvRollForwardFlightsSearchCriteria searchCriteriaDTO = new InvRollForwardFlightsSearchCriteria(); 
//		Set segmentCodes = new HashSet();
//		segmentCodes.add("CMB/SHJ");
//		searchCriteriaDTO.setSegmentCodes(segmentCodes);
//		InvRollForwardCriteriaDTO criteriaDTO = new InvRollForwardCriteriaDTO();
//		criteriaDTO.setInvRollForwardFlightsSearchCriteria(searchCriteriaDTO);
//		criteriaDTO.setOverwriteExisting(true);
//		criteriaDTO.setRollFixedBCAllocs(true);
//		criteriaDTO.setRollNonStadardBCAllocs(true);
//		criteriaDTO.setRollStandardBCAllocs(true);
//		criteriaDTO.setSourceCabinClassCode("I");
//		criteriaDTO.setSourceFlightId(8000);
//		Set flightIds = new HashSet();		
//		flightIds.add(new Integer(8001));
//		getFlightInventoryBD().rollFlightInventory(flightIds,criteriaDTO);	
	}
//
//	public void testInvalidateFlightInventories() {
//
//	}
//
//	public void testInvalidateFCCSegInventory() {
//
//	}
//
//	public void testDeleteFlightInventories() {
//
//	}
//
	public void testUpdateFCCSegBCInventories() throws ModuleException {
		Collection fccSegInventoryUpdateDTOs = new HashSet();
		FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO = new FCCSegInventoryUpdateDTO(); 
		fccSegInventoryUpdateDTO.setFccsInventoryId(19056);
		fccSegInventoryUpdateDTO.setFlightId(19059);
		fccSegInventoryUpdateDTO.setSegmentCode("CMB/SHJ");
		fccSegInventoryUpdateDTO.setCabinClassCode("I");
//		fccSegInventoryUpdateDTO.setInfantAllocation(10) ;
		fccSegInventoryUpdateDTOs.add(fccSegInventoryUpdateDTO);		
		getFlightInventoryBD().updateFCCSegInventories( fccSegInventoryUpdateDTOs);
	}

	private FlightInventoryBD getFlightInventoryBD(){
		return (FlightInventoryBD)AirInventoryUtil.getInstance().
		lookupServiceBD("airinventory", "flightinventory.service");
	}
}
