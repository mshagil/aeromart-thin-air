package com.isa.thinair.airinventory.api.util;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.exception.ModuleException;

public class BookingClassUtil {

	/**
	 * Returns true if no segment inventory update done for booking class of specified booking class type
	 */
	public static boolean byPassSegInvUpdate(String bcType) {
		return isStandbyBookingClassType(bcType) || isOpenReturnBookingClassType(bcType) || byPassSegInvUpdateForOverbook(bcType);
	}

	public static boolean byPassAvailableSeatsCheck(String bcType) {
		return isStandbyBookingClassType(bcType) || byPassSegInvUpdateForOverbook(bcType);
	}

	public static boolean byPassReprotectSeats(String bcType) {
		return isStandbyBookingClassType(bcType);
	}

	public static boolean isStandbyBookingClassType(String bcType) {
		return bcType != null && bcType.equals(BookingClass.BookingClassType.STANDBY);
	}

	public static boolean isOpenReturnBookingClassType(String bcType) {
		return bcType != null && (bcType.equals(BookingClass.BookingClassType.OPEN_RETURN));
	}

	public static boolean byPassSegInvUpdateForOverbook(String bcType) {
		return bcType != null && bcType.equals(BookingClass.BookingClassType.OVERBOOK);
	}

	/**
	 * FIXME - Assumed all the booking classes in collection either by passed or not in updating inv
	 */
	public static boolean byPassSegInvUpdate(Collection<String> bcTypeCollection) {
		if (bcTypeCollection != null) {
			for (String bcType : bcTypeCollection) {
				if (byPassSegInvUpdate(bcType)) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean byPassSegInvUpdateForOverbook(Collection<String> bcTypeCollection) {
		if (bcTypeCollection != null) {
			for (String bcType : bcTypeCollection) {
				if (byPassSegInvUpdateForOverbook(bcType)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns collection of bc type corresponding to requested segment inventory update eligibility
	 */
	public static Collection<String> getBcTypes(boolean eligibleForByPassInvUpdate) {
		Collection<String> bcTypes = new ArrayList<String>();

		if (eligibleForByPassInvUpdate) {
			bcTypes.add(BookingClass.BookingClassType.STANDBY);
			bcTypes.add(BookingClass.BookingClassType.OPEN_RETURN);
		} else {
			bcTypes.add(BookingClass.BookingClassType.NORMAL);
			bcTypes.add(BookingClass.BookingClassType.INTERLINE_FREESELL);
			bcTypes.add(BookingClass.BookingClassType.INTERLINE_BLOCKED_SELL);
		}
		return bcTypes;
	}

	/**
	 * Used for legend display in Fare Maintenance screen.
	 */
	public static String getBcTypeThreeLetterCode(String bcType) {
		String threeLetterCode = "   ";
		if (BookingClass.BookingClassType.NORMAL.equals(bcType)) {
			threeLetterCode = "NOR";
		} else if (BookingClass.BookingClassType.STANDBY.equals(bcType)) {
			threeLetterCode = "STB";
		} else if (BookingClass.BookingClassType.INTERLINE_FREESELL.equals(bcType)) {
			threeLetterCode = "INF";
		} else if ((BookingClass.BookingClassType.OPEN_RETURN.equals(bcType))) {
			threeLetterCode = "ORT";
		} else if (BookingClass.BookingClassType.INTERLINE_BLOCKED_SELL.equals(bcType)) {
			threeLetterCode = "INB";
		}
		return threeLetterCode;
	}

	/**
	 * For booking class maintenance screen. FIXME - remove once these programmed to pick from the database itself.
	 */
	public static String createBCTypes(String arrName) throws ModuleException {
		String normalBC = BookingClass.BookingClassType.NORMAL;
		String stanbyBC = BookingClass.BookingClassType.STANDBY;
		String openRTBC = BookingClass.BookingClassType.OPEN_RETURN;
		String interlineFreeSale = BookingClass.BookingClassType.INTERLINE_FREESELL;
		String arrBCTypes = arrName + "[0]=new Array('" + normalBC + "','" + normalBC + "');" + arrName + "[1]=new Array('"
				+ stanbyBC + "','" + stanbyBC + "');" + arrName + "[2]=new Array('" + openRTBC + "','" + openRTBC + "');"
				+ arrName + "[3]=new Array('" + interlineFreeSale + "','" + interlineFreeSale + "');";
		return arrBCTypes;
	}

	public static boolean isOnlyNormalBcType(Collection<String> bcTypeCollection) {
		boolean isNormalOnly = false;
		if (bcTypeCollection != null) {
			for (String bcType : bcTypeCollection) {

				if (BookingClass.BookingClassType.NORMAL.equals(bcType)) {
					isNormalOnly = true;
				} else {
					return false;
				}

			}
		}
		return isNormalOnly;
	}
	
	public static boolean isOnlyNormalOrOverbookBcType(Collection<String> bcTypeCollection) {
		boolean isNormalOrOverbookOnly = false;
		if (bcTypeCollection != null) {
			for (String bcType : bcTypeCollection) {

				if (BookingClass.BookingClassType.NORMAL.equals(bcType) || BookingClass.BookingClassType.OVERBOOK.equals(bcType)) {
					isNormalOrOverbookOnly = true;
				} else {
					return false;
				}

			}
		}
		return isNormalOrOverbookOnly;
	}
}
