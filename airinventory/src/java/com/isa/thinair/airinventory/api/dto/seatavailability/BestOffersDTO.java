package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;

public class BestOffersDTO implements Serializable, Comparable<BestOffersDTO> {

	private static final long serialVersionUID = 1363613210152281014L;

	private String originAirport;

	private String destinationAirport;

	private String originAirportName;

	private String destinationAirportName;

	private Collection<String> flightNos;

	private Date departureDateTimeLocal;

	private Date arrivalDateTimeLocal;

	private BigDecimal fareAmount;

	private BigDecimal totalTaxAmount;

	private BigDecimal totalSurchargesAmount;

	private BigDecimal totalAmount;

	private String currency;

	private int rank;

	private String uniqueKey;

	/** Charges collection without effective amount set */
	private Collection<QuotedChargeDTO> charges = null;

	private String segmentCode;

	private boolean isConnectionWithSegmentFare = false;

	private List<String> bookingCodes;

	public String getOriginAirport() {
		return this.originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public String getDestinationAirport() {
		return this.destinationAirport;
	}

	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public String getOriginAirportName() {
		return this.originAirportName;
	}

	public void setOriginAirportName(String originAirportName) {
		this.originAirportName = originAirportName;
	}

	public String getDestinationAirportName() {
		return this.destinationAirportName;
	}

	public void setDestinationAirportName(String destinationAirportName) {
		this.destinationAirportName = destinationAirportName;
	}

	public Collection<String> getFlightNos() {
		if (this.flightNos == null)
			this.flightNos = new LinkedHashSet<String>();
		return this.flightNos;
	}

	public void setFlightNos(Collection<String> flightNos) {
		this.flightNos = flightNos;
	}

	public Date getDepartureDateTimeLocal() {
		return this.departureDateTimeLocal;
	}

	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	public Date getArrivalDateTimeLocal() {
		return this.arrivalDateTimeLocal;
	}

	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getFareAmount() {
		return this.fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public int getRank() {
		return this.rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getUniqueKey() {
		return this.uniqueKey;
	}

	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

	public int compareTo(BestOffersDTO o) {
		int fareCompare = -1;
		int dateCompare = -1;

		fareCompare = this.fareAmount.compareTo(o.fareAmount);
		dateCompare = this.departureDateTimeLocal.compareTo(o.getDepartureDateTimeLocal());

		if (fareCompare == -1)
			return -1;
		else if (fareCompare == 0) {
			if (dateCompare == -1)
				return -1;
			else if (dateCompare == 0)
				return 0;
			else
				return 1;
		} else
			return 1;

	}

	public BigDecimal getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	public BigDecimal getTotalSurchargesAmount() {
		return totalSurchargesAmount;
	}

	public void setTotalSurchargesAmount(BigDecimal totalSurchargesAmount) {
		this.totalSurchargesAmount = totalSurchargesAmount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Collection<QuotedChargeDTO> getCharges() {
		return charges;
	}

	public void setCharges(Collection<QuotedChargeDTO> charges) {
		this.charges = charges;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the isConnectionWithSegmentFare
	 */
	public boolean isConnectionWithSegmentFare() {
		return isConnectionWithSegmentFare;
	}

	/**
	 * @param isConnectionWithSegmentFare
	 *            the isConnectionWithSegmentFare to set
	 */
	public void setConnectionWithSegmentFare(boolean isConnectionWithSegmentFare) {
		this.isConnectionWithSegmentFare = isConnectionWithSegmentFare;
	}

	public List<String> getBookingCodes() {
		if (bookingCodes == null) {
			bookingCodes = new ArrayList<String>();
		}
		return bookingCodes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BestOffersDTO [originAirport=" + originAirport + ", destinationAirport=" + destinationAirport + ", flightNos="
				+ flightNos + ", departureDateTimeLocal=" + departureDateTimeLocal + ", segmentCode=" + segmentCode
				+ ", isConnectionWithSegmentFare=" + isConnectionWithSegmentFare + "]";
	}

}
