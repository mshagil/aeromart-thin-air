package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

public class SeatAllocationDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int fccsaId;
	
	private int allocatedSeats;
	
	private int soldSeats;
	
	private int onHoldSeats;
	
	private int infantAllocation;
	
	private int soldInfantSeats;
	
	private int onholdInfantSeats;

	public int getFccsaId() {
		return fccsaId;
	}

	public void setFccsaId(int fccsaId) {
		this.fccsaId = fccsaId;
	}

	public int getAllocatedSeats() {
		return allocatedSeats;
	}

	public void setAllocatedSeats(int allocatedSeats) {
		this.allocatedSeats = allocatedSeats;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	public int getOnHoldSeats() {
		return onHoldSeats;
	}

	public void setOnHoldSeats(int onHoldSeats) {
		this.onHoldSeats = onHoldSeats;
	}

	public int getInfantAllocation() {
		return infantAllocation;
	}

	public void setInfantAllocation(int infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	public int getSoldInfantSeats() {
		return soldInfantSeats;
	}

	public void setSoldInfantSeats(int soldInfantSeats) {
		this.soldInfantSeats = soldInfantSeats;
	}

	public int getOnholdInfantSeats() {
		return onholdInfantSeats;
	}

	public void setOnholdInfantSeats(int onholdInfantSeats) {
		this.onholdInfantSeats = onholdInfantSeats;
	}	
	
	
}
