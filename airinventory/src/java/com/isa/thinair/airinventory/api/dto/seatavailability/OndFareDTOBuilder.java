package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.util.FareSummaryUtil;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FlexiQuoteCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;


public class OndFareDTOBuilder {
	private final OndFareDTO ondFareDTO = new OndFareDTO();

	private final OndRebuildCriteria criteria;

	private final OnDFareSegChargesRebuildCriteria ondCriteria;

	public OndFareDTOBuilder(OndRebuildCriteria criteria, OnDFareSegChargesRebuildCriteria ondCriteria) {
		super();
		this.criteria = criteria;
		this.ondCriteria = ondCriteria;
		setFareType(ondCriteria.getFareType());
		setInBoundOnd(ondCriteria.isInbound());
		setOndSequence(ondCriteria.getOndSequence());
		setRequestedOndSequence(ondCriteria.getRequestedOndSequence());
		setModifyInbound(ondCriteria.isModifyInbound());
		setOpenReturn(ondCriteria.isOpenReturn());
		setSelectedBundledFarePeriodId();
		setSplitOperationForBusInvoked(ondCriteria.isSplitOperationForBusInvoked());
	}

	private void setOndSequence(int ondSequence) {
		this.ondFareDTO.setOndSequence(ondSequence);

	}

	private void setRequestedOndSequence(int ondSequence) {
		this.ondFareDTO.setRequestedOndSequence(ondSequence);
	}

	/**
	 * @param fareSummaryDTO
	 *            the fareSummaryDTO to set
	 */
	public void setFareSummaryDTO(FareTO fareTO) {
		int percentage = ondCriteria.getFarePercentage();

		FareSummaryDTO fareSummaryDTO = new FareSummaryDTO(fareTO);
		fareSummaryDTO.setFarePercentage(ondCriteria.getFarePercentage());

		// repeat the same calculation in SelectedFlightDTO.setEffectiveFares()
		BigDecimal adultFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
		BigDecimal childFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD));
		BigDecimal infantFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT));

		if (!isInBoundOnd() && !isModifyInboundFlight()) { // For modify inbound return/half return fare
			FareSummaryUtil.setOutBoundFareSummaryDTO(fareSummaryDTO, percentage);
		} else {
			FareSummaryUtil.setInBoundFareSummaryDTO(fareSummaryDTO, percentage);
		}

		if (ondCriteria.getOverridePaxFares() != null) {
			Map<String, BigDecimal> overridePaxFares = ondCriteria.getOverridePaxFares();
			fareSummaryDTO.setAdultFare(overridePaxFares.get(PaxTypeTO.ADULT).doubleValue());
			fareSummaryDTO.setChildFare(overridePaxFares.get(PaxTypeTO.CHILD).doubleValue());
			fareSummaryDTO.setInfantFare(overridePaxFares.get(PaxTypeTO.INFANT).doubleValue());
			fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
			fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
		}

		this.ondFareDTO.setFareSummaryDTO(fareSummaryDTO);
	}

	public Map<String, Double> getTotalJourneyFare() {
		return this.criteria.getTotalJourneyFare();
	}

	public FareSummaryDTO getFareSummaryDTO() {
		return this.ondFareDTO.getFareSummaryDTO();
	}

	/**
	 * @param allCharges
	 *            the allCharges to set
	 */
	public void setAllCharges(Collection<QuotedChargeDTO> allCharges) {
		this.ondFareDTO.setAllCharges(allCharges);
	}

	/**
	 * @param segmentSeatDistsDTOs
	 *            the segmentSeatDistsDTOs to set
	 */
	public void setSegmentSeatDistsDTOs(Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs) {
		this.ondFareDTO.setSegmentSeatDistsDTOs(segmentSeatDistsDTOs);
	}

	public void setSegmentsMap(List<FlightSegmentDTO> flightSegmentDTOs) {
		// sort by departure time zulu
		Collections.sort(flightSegmentDTOs, new Comparator<FlightSegmentDTO>() {
			@Override
			public int compare(FlightSegmentDTO o1, FlightSegmentDTO o2) {
				return o1.getDepartureDateTimeZulu().compareTo(o2.getDepartureDateTimeZulu());
			}
		});

		for (FlightSegmentDTO dto : flightSegmentDTOs) {
			this.ondFareDTO.addSegment(dto.getSegmentId(), dto);
		}
	}

	/**
	 * @param isInBoundOnd
	 *            the isInBoundOnd to set
	 */
	public void setInBoundOnd(boolean isInBoundOnd) {
		this.ondFareDTO.setInBoundOnd(isInBoundOnd);
	}

	/**
	 * @param isModifyInbound
	 *            the isModifyInbound to set
	 */
	public void setModifyInbound(boolean isModifyInbound) {
		this.ondFareDTO.setModifyInbound(isModifyInbound);
	}

	/**
	 * @param isOpenReturn
	 *            the isOpenReturn to set
	 */
	public void setOpenReturn(boolean isOpenReturn) {
		this.ondFareDTO.setOpenReturn(isOpenReturn);
	}

	/**
	 * @param allFlexiCharges
	 *            the allFlexiCharges to set
	 */
	public void setAllFlexiCharges(Collection<FlexiRuleDTO> allFlexiCharges) {
		this.ondFareDTO.setAllFlexiCharges(allFlexiCharges);
	}

	/**
	 * @param fareType
	 *            the fareType to set
	 */
	public void setFareType(int fareType) {
		this.ondFareDTO.setFareType(fareType);
	}

	public void setSelectedBundledFarePeriodId() {
		if (this.ondCriteria.getBundledFareDTO() != null) {
			this.ondFareDTO.setSelectedBundledFarePeriodId(this.ondCriteria.getBundledFareDTO().getBundledFarePeriodId());
		}
	}

	public OndFareDTO buildOndFareTOD() {
		return this.ondFareDTO;
	}

	public Collection<Integer> getFlightSegIds() {
		return this.ondCriteria.getFlightSegIds();
	}

	public Collection<Integer> getChargeRateIds() {
		return this.ondCriteria.getChargeRateIds();
	}

	public LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> getFsegBCAlloc() {
		return this.ondCriteria.getFsegBCAlloc();
	}

	/**
	 * This will return a QuoteChargesCriteria required to rebuilding the chargers. This method cannot be used to create
	 * the criteria for quoting the chargers.
	 * 
	 * @return
	 */
	public QuoteChargesCriteria getQuoteChargesCriteria() {
		QuoteChargesCriteria criteria = new QuoteChargesCriteria();
		criteria.setFareSummaryDTO(getFareSummaryDTO());

		if (this.ondCriteria != null && this.ondCriteria.getSubJourneyGroup() > 0
				&& this.ondCriteria.getTotalSubJourneyFare() != null) {
			criteria.setTotalJourneyFare(this.ondCriteria.getTotalSubJourneyFare());
		} else {
			criteria.setTotalJourneyFare(this.criteria.getTotalJourneyFare());
		}

		return criteria;
	}

	public Collection<Integer> getFlexiRuleIds() {
		return this.ondCriteria.getFlexiChargeIds();
	}

	/**
	 * This will return the FlexiQuoteCriteria requird to rebuild the flexi chargers. This method cannot be used to
	 * creat the criteria for flexi quoting.
	 * 
	 * @return
	 */
	public FlexiQuoteCriteria getFlexiQuoteCriteria() {
		FlexiQuoteCriteria quoteCriteria = new FlexiQuoteCriteria();
		quoteCriteria.setFareSummaryDTO(getFareSummaryDTO());
		quoteCriteria.setFareRuleID(getFareSummaryDTO().getFareRuleID());
		if (this.ondCriteria.getBundledFareDTO() != null) {
			BundledFareDTO bundledFareDTO = this.ondCriteria.getBundledFareDTO();
			quoteCriteria.setOfferFlexiFreeOfCharge(bundledFareDTO.isServiceIncluded(EXTERNAL_CHARGES.FLEXI_CHARGES.toString()));
		}
		return quoteCriteria;
	}

	/**
	 * {totalAdults,totlaChlid,totalInfants}
	 * 
	 * @return
	 */
	public Integer[] getPaxCount() {
		Integer[] paxCount = { this.criteria.getTotlaAdults(), this.criteria.getTotalChildrens(),
				this.criteria.getTotalInfants() };
		return paxCount;
	}

	public Collection<QuotedChargeDTO> getAllCharges() {
		return this.ondFareDTO.getAllCharges();
	}

	public boolean isInBoundOnd() {
		return this.ondFareDTO.isInBoundOnd();
	}

	public boolean isModifyInboundFlight() {
		return this.ondFareDTO.isModifyInbound();
	}

	public boolean isBlockSeat() {
		return this.criteria.hasBlockSeats();
	}

	public boolean isFlexiQuote() {
		return this.ondCriteria.getFlexiChargeIds() != null && !this.ondCriteria.getFlexiChargeIds().isEmpty();
	}

	public boolean isOpenReturn() {
		return this.ondFareDTO.isOpenReturn();
	}

	public String getBookingType() {
		String bookingType = this.criteria.getBookingType();
		// In multi city search, booking type comes with comma separation
		String[] values = bookingType.split(",");
		if (values.length > 1 && values.length > this.ondFareDTO.getOndSequence()) {
			bookingType = values[this.ondFareDTO.getOndSequence()];
		}
		return bookingType;
	}

	public boolean isAllowOverbookAfterCutoffTime() {
		return this.criteria.isAllowOverbookAfterCutoffTime();
	}
	
	public void setSplitOperationForBusInvoked(boolean isSplitOperationForBusInvoked){
		this.ondFareDTO.setSplitOperationForBusInvoked(isSplitOperationForBusInvoked);;
	}
}
