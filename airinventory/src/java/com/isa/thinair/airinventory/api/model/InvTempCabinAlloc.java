/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 04, 2015 11:04:58
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_INV_TEMPLATE_CABIN_ALLOC"
 * 
 * @author Priyantha
 */
public class InvTempCabinAlloc extends Persistent implements Serializable {

	private static final long serialVersionUID = 1497566894209695504L;

	private Integer invTempCabinAllocID;

	private String cabinClassCode;

	private String logicalCabinClassCode;

	private Integer adultAllocation;

	private Integer infantAllocation;

	Set<InvTempCabinBCAlloc> invTempCabinBCAllocs = new HashSet<InvTempCabinBCAlloc>();

	private InventoryTemplate inventoryTemplate;

	/**
	 * @return Returns the invTempCabinAllocID
	 * 
	 * @hibernate.id column = "INV_TEMPLATE_CABIN_ALLOC_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_INV_TEMPLATE_CABIN_ALLOC"
	 */
	public Integer getInvTempCabinAllocID() {
		return invTempCabinAllocID;
	}

	/**
	 * @param invTempCabinAllocID
	 * 
	 */
	public void setInvTempCabinAllocID(Integer invTempCabinAllocID) {
		this.invTempCabinAllocID = invTempCabinAllocID;
	}

	/**
	 * @return Returns the invTempID
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 * 
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 * 
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the logicalCabinClassCode
	 * 
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 * 
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	/**
	 * @param logicalCabinClassCode
	 * 
	 */
	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return Returns the adultAllocation
	 * 
	 * @hibernate.property column = "ADULT_ALLOCATION"
	 * 
	 */
	public Integer getAdultAllocation() {
		return adultAllocation;
	}

	/**
	 * @param adultAllocation
	 * 
	 */
	public void setAdultAllocation(Integer adultAllocation) {
		this.adultAllocation = adultAllocation;
	}

	/**
	 * @return Returns the infantAllocation
	 * 
	 * @hibernate.property column = "INFANT_ALLOCATION"
	 * 
	 */
	public Integer getInfantAllocation() {
		return infantAllocation;
	}

	/**
	 * @param infantAllocation
	 * 
	 */
	public void setInfantAllocation(Integer infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	/**
	 * @return
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airinventory.api.model.InvTempCabinBCAlloc"
	 * @hibernate.collection-key column="INV_TEMPLATE_CABIN_ALLOC_ID"
	 */
	 public Set<InvTempCabinBCAlloc> getInvTempCabinBCAllocs() {
		return invTempCabinBCAllocs;
	 }
	
	 /**
	 * @param invTempCabinBCAllocs
	 * the invTempCabinBCAllocs to set
	 */
	 public void setInvTempCabinBCAllocs(Set<InvTempCabinBCAlloc> invTempCabinBCAllocs) {
		this.invTempCabinBCAllocs = invTempCabinBCAllocs;
	 }

	/**
	 * @return Returns the invTempID
	 * 
	 * @hibernate.many-to-one column="INV_TEMPLATE_ID" class="com.isa.thinair.airinventory.api.model.InventoryTemplate"
	 */
	public InventoryTemplate getInventoryTemplate() {
		return inventoryTemplate;
	}

	/**
	 * @param inventoryTemplate
	 *            the inventoryTemplate to set
	 */
	public void setInventoryTemplate(InventoryTemplate inventoryTemplate) {
		this.inventoryTemplate = inventoryTemplate;
	}

	/**
	 * Overrided by default to support lazy loading
	 * 
	 * @return
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getInvTempCabinAllocID()).toHashCode();
	}
}
