package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public class FareIdDetailDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer fareRuleId;
	private Integer fareId;
	private String cabinClassCode;
	private String bookingClassCode;
	private Integer fareType;

	public Integer getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public Integer getFareId() {
		return fareId;
	}

	public void setFareId(Integer fareId) {
		this.fareId = fareId;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public Integer getFareType() {
		return fareType;
	}

	public void setFareType(Integer fareType) {
		this.fareType = fareType;
	}

}
