/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:27
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author : B
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_FCC_SEG_BC_RM_MV"
 * 
 *                  FCC is the entity class to repesent a FCC model
 * 
 */
public class FCCInventoryMovemant implements Serializable {

	private static final long serialVersionUID = -2430214203368118505L;

	private Integer id;

	private Integer fromFCCSegBCInvId;

	private Integer toFCCSegBCInvId;

	private Integer numSeats;

	private Date timeStamp;

	private String userId;

	/**
	 * @return the fromFCCSegBCInvId
	 * 
	 * @hibernate.property column = "FROM_FCCSBA_ID"
	 * 
	 */
	public Integer getFromFCCSegBCInvId() {
		return fromFCCSegBCInvId;
	}

	/**
	 * @param fromFCCSegBCInvId
	 *            the fromFCCSegBCInvId to set
	 */
	public void setFromFCCSegBCInvId(Integer fromFCCSegBCInvId) {
		this.fromFCCSegBCInvId = fromFCCSegBCInvId;
	}

	/**
	 * @return the id
	 * @hibernate.id column = "FCCSBRM_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FCC_SEG_BC_RM_MV"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the numSeats
	 * @hibernate.property column = "NUMBER_OF_SEATS"
	 */
	public Integer getNumSeats() {
		return numSeats;
	}

	/**
	 * @param numSeats
	 *            the numSeats to set
	 */
	public void setNumSeats(Integer numSeats) {
		this.numSeats = numSeats;
	}

	/**
	 * @return the timeStamp
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the toFCCSegBCInvId
	 * @hibernate.property column = "TO_FCCSBA_ID"
	 */
	public Integer getToFCCSegBCInvId() {
		return toFCCSegBCInvId;
	}

	/**
	 * @param toFCCSegBCInvId
	 *            the toFCCSegBCInvId to set
	 */
	public void setToFCCSegBCInvId(Integer toFCCSegBCInvId) {
		this.toFCCSegBCInvId = toFCCSegBCInvId;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

}
