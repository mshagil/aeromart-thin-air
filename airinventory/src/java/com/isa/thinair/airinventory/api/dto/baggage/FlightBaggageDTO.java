package com.isa.thinair.airinventory.api.dto.baggage;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author mano
 * @since May 9, 2011
 */
public class FlightBaggageDTO implements Serializable {

	private static final long serialVersionUID = 5687005665032127432L;

	private int flightSegmentID;

	private int templateId;

	private Integer chargeId;

	private Integer flightBaggageId;

	private Integer baggageId;

	private BigDecimal amount;

	private String baggageName;

	private BigDecimal baggageWeight;

	private String translatedBaggageName;

	private int soldPieces;

	private String baggageDescription;

	private String translatedBaggageDescription;

	private String cabinClassCode;

	private String logicalCCCode;

	private String baggageCode;

	private String flightSegmentCode;

	private String defaultBaggage;

	private int seatFactor;

	private BigDecimal totalWeight;

	private BigDecimal availableWeight;
	
	private String ondGroupId;

	private boolean inactiveSelectedBaggage = false;
	
	private String baggageStatus;
	
	private int seatFactorConditionApplyFor;
	
	private boolean isSeatFactorConditionSatisfied;
	
	/**
	 * @return the flightSegmentID
	 */
	public int getFlightSegmentID() {
		return flightSegmentID;
	}

	/**
	 * @param flightSegmentID
	 *            the flightSegmentID to set
	 */
	public void setFlightSegmentID(int flightSegmentID) {
		this.flightSegmentID = flightSegmentID;
	}

	/**
	 * @return the templateId
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the chargeId
	 */
	public Integer getChargeId() {
		return chargeId;
	}

	/**
	 * @param chargeId
	 *            the chargeId to set
	 */
	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	/**
	 * @return the baggageId
	 */
	public Integer getBaggageId() {
		return baggageId;
	}

	/**
	 * @param baggageId
	 *            the baggageId to set
	 */
	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the baggageName
	 */
	public String getBaggageName() {
		return baggageName;
	}

	/**
	 * @param baggageName
	 *            the baggageName to set
	 */
	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	public BigDecimal getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(BigDecimal baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	/**
	 * @return the soldPieces
	 */
	public int getSoldPieces() {
		return soldPieces;
	}

	/**
	 * @param soldPieces
	 *            the soldPieces to set
	 */
	public void setSoldPieces(int soldPieces) {
		this.soldPieces = soldPieces;
	}

	/**
	 * @return the baggageDescription
	 */
	public String getBaggageDescription() {
		return baggageDescription;
	}

	/**
	 * @param baggageDescription
	 *            the baggageDescription to set
	 */
	public void setBaggageDescription(String baggageDescription) {
		this.baggageDescription = baggageDescription;
	}

	/**
	 * @return the flightBaggageId
	 */
	public Integer getFlightBaggageId() {
		return flightBaggageId;
	}

	/**
	 * @param flightBaggageId
	 *            the flightBaggageId to set
	 */
	public void setFlightBaggageId(Integer flightBaggageId) {
		this.flightBaggageId = flightBaggageId;
	}

	/**
	 * @return the cabinClassCode
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the baggageCode
	 */
	public String getBaggageCode() {
		return baggageCode;
	}

	/**
	 * @param baggageCode
	 *            the baggageCode to set
	 */
	public void setBaggageCode(String baggageCode) {
		this.baggageCode = baggageCode;
	}

	/**
	 * @return the flightSegmentCode
	 */
	public String getFlightSegmentCode() {
		return flightSegmentCode;
	}

	/**
	 * @param flightSegmentCode
	 *            the flightSegmentCode to set
	 */
	public void setFlightSegmentCode(String flightSegmentCode) {
		this.flightSegmentCode = flightSegmentCode;
	}

	public String getTranslatedBaggageName() {
		return translatedBaggageName;
	}

	public void setTranslatedBaggageName(String translatedBaggageName) {
		this.translatedBaggageName = translatedBaggageName;
	}

	public String getTranslatedBaggageDescription() {
		return translatedBaggageDescription;
	}

	public void setTranslatedBaggageDescription(String translatedBaggageDescription) {
		this.translatedBaggageDescription = translatedBaggageDescription;
	}

	public String getDefaultBaggage() {
		return defaultBaggage;
	}

	public void setDefaultBaggage(String defaultBaggage) {
		this.defaultBaggage = defaultBaggage;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public int getSeatFactor() {
		return seatFactor;
	}

	public void setSeatFactor(int seatFactor) {
		this.seatFactor = seatFactor;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}

	public BigDecimal getAvailableWeight() {
		return availableWeight;
	}

	public void setAvailableWeight(BigDecimal availableWeight) {
		this.availableWeight = availableWeight;
	}

	/**
	 * @return the ondGroupId
	 */
	public String getOndGroupId() {
		return ondGroupId;
	}

	/**
	 * @param ondGroupId
	 *            the ondGroupId to set
	 */
	public void setOndGroupId(String ondGroupId) {
		this.ondGroupId = ondGroupId;
	}

	public boolean isInactiveSelectedBaggage() {
		return inactiveSelectedBaggage;
	}

	public void setInactiveSelectedBaggage(boolean inactiveSelectedBaggage) {
		this.inactiveSelectedBaggage = inactiveSelectedBaggage;
	}
	/**
	 * 
	 * @return baggageStatus
	 */
	public String getBaggageStatus() {
		return baggageStatus;
	}
	/**
	 * 
	 * @param baggageStatus
	 */
	public void setBaggageStatus(String baggageStatus) {
		this.baggageStatus = baggageStatus;
	}

	public int getSeatFactorConditionApplyFor() {
		return seatFactorConditionApplyFor;
	}

	public void setSeatFactorConditionApplyFor(int seatFactorConditionApplyFor) {
		this.seatFactorConditionApplyFor = seatFactorConditionApplyFor;
	}

	public boolean isSeatFactorConditionSatisfied() {
		return isSeatFactorConditionSatisfied;
	}

	public void setSeatFactorConditionSatisfied(boolean isSeatFactorConditionSatisfied) {
		this.isSeatFactorConditionSatisfied = isSeatFactorConditionSatisfied;
	}

}
