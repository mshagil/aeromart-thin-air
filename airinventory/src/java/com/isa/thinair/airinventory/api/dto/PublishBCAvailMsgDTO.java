package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventCode;

public class PublishBCAvailMsgDTO implements Serializable {

	private static final long serialVersionUID = 6801285175153366254L;

	private Integer publishAvailabilityId;

	private String bookingCode;

	private Integer seatsAvailable;
	
	private Integer bcSeatsAvailable;

	private PublishInventoryEventCode bcEventCode;

	/**
	 * @return the publishAvailabilityId
	 */
	public Integer getPublishAvailabilityId() {
		return publishAvailabilityId;
	}

	/**
	 * @param publishAvailabilityId
	 *            the publishAvailabilityId to set
	 */
	public void setPublishAvailabilityId(Integer publishAvailabilityId) {
		this.publishAvailabilityId = publishAvailabilityId;
	}

	/**
	 * @return the bcEventCode
	 */
	public PublishInventoryEventCode getBcEventCode() {
		return bcEventCode;
	}

	/**
	 * @param bcEventCode
	 *            the bcEventCode to set
	 */
	public void setBcEventCode(PublishInventoryEventCode bcEventCode) {
		this.bcEventCode = bcEventCode;
	}

	/**
	 * @return the bookingCode
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            the bookingCode to set
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return the seatsAvailable
	 */
	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}

	/**
	 * @param seatsAvailable
	 *            the seatsAvailable to set
	 */
	public void setSeatsAvailable(Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	/**
	 * @return the bcSeatsAvailable
	 */
	public Integer getBcSeatsAvailable() {
		return bcSeatsAvailable;
	}

	/**
	 * @param bcSeatsAvailable the bcSeatsAvailable to set
	 */
	public void setBcSeatsAvailable(Integer bcSeatsAvailable) {
		this.bcSeatsAvailable = bcSeatsAvailable;
	}

}
