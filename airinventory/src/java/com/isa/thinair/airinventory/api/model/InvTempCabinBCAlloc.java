package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_INV_TEMPLATE_CABIN_BC_ALLOC"
 * @author Priyantha
 * 
 */
public class InvTempCabinBCAlloc extends Persistent implements Serializable {

	private static final long serialVersionUID = 7457082473223649447L;

	private Integer invTmpCabinBCAllocID;

	private String bookingCode;

	private Integer allocatedSeats;

	private Integer allocWaitListedSeats;

	private String priorityFlag;

	private String statusBcAlloc;

	private InvTempCabinAlloc invTempCabinAlloc;

	/**
	 * 
	 * @hibernate.id column = "INV_TEMPLATE_CABIN_BC_ALLOC_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_INV_TEMPLATE_CABIN_BC_ALLOC"
	 * 
	 * @return invTmpCabinBCAllocID
	 */
	public Integer getInvTmpCabinBCAllocID() {
		return invTmpCabinBCAllocID;
	}

	/**
	 * @param invTmpCabinBCAllocID
	 *            the invTmpCabinBCAllocID to set
	 */
	public void setInvTmpCabinBCAllocID(Integer invTmpCabinBCAllocID) {
		this.invTmpCabinBCAllocID = invTmpCabinBCAllocID;
	}

	/**
	 * @return
	 * 
	 * @hibernate.many-to-one column="INV_TEMPLATE_CABIN_ALLOC_ID"
	 *                        class="com.isa.thinair.airinventory.api.model.InvTempCabinAlloc"
	 */

	public InvTempCabinAlloc getInvTempCabinAlloc() {
		return invTempCabinAlloc;
	}

	public void setInvTempCabinAlloc(InvTempCabinAlloc invTempCabinAlloc) {
		this.invTempCabinAlloc = invTempCabinAlloc;
	}

	/**
	 * 
	 * @hibernate.property column = "BOOKING_CODE"
	 * 
	 * @return the bookingCode
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            the bookingCode to set
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * 
	 * @hibernate.property column = "ALLOCATED_SEATS"
	 * 
	 * @return the allocatedSeats
	 */
	public Integer getAllocatedSeats() {
		return allocatedSeats;
	}

	/**
	 * @param allocatedSeats
	 *            the allocatedSeats to set
	 */
	public void setAllocatedSeats(Integer allocatedSeats) {
		this.allocatedSeats = allocatedSeats;
	}

	/**
	 * 
	 * @hibernate.property column = "ALLOCATED_WAITLISTED_SEATS"
	 * 
	 * @return the allocWaitListedSeats
	 */
	public Integer getAllocWaitListedSeats() {
		return allocWaitListedSeats;
	}

	/**
	 * @param allocWaitListedSeats
	 *            the allocWaitListedSeats to set
	 */
	public void setAllocWaitListedSeats(Integer allocWaitListedSeats) {
		this.allocWaitListedSeats = allocWaitListedSeats;
	}
	
	/**
	 * 
	 * @hibernate.property column = "PRIORITY_FLAG"
	 * 
	 * @return the priorityFlag
	 */
	public String getPriorityFlag() {
		return priorityFlag;
	}

	/**
	 * @param priorityFlag
	 *            the priorityFlag to set
	 */
	public void setPriorityFlag(String priorityFlag) {
		this.priorityFlag = priorityFlag;
	}

	/**
	 * 
	 * @hibernate.property column = "STATUS"
	 * 
	 * @return the statusBcAlloc
	 */
	public String getStatusBcAlloc() {
		return statusBcAlloc;
	}

	/**
	 * @param statusBcAlloc
	 *            the statusBcAlloc to set
	 */
	public void setStatusBcAlloc(String statusBcAlloc) {
		this.statusBcAlloc = statusBcAlloc;
	}

	/**
	 * Overrided by default to support lazy loading
	 * 
	 * @return
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getInvTmpCabinBCAllocID()).toHashCode();
	}

}
