/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @hibernate.class table = "T_ROLL_FORWARD_BATCH_REQ"
 * 
 */
public class RollForwardBatchREQ extends Tracking implements Serializable {

	private static final long serialVersionUID = 1511890680014337321L;

	private Integer batchId;

	private String requestedData;
	
	private String status;

	private Set<RollForwardBatchREQSEG> rollForwardBatchREQSEGSet;

	/**
	 * returns the batchId
	 * 
	 * @return Returns the batchId.
	 * 
	 * @hibernate.id column = "R_FWD_BATCH_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_ROLL_FORWARD_BATCH_REQ"
	 * 
	 * 
	 */
	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}
	
	/**
	 * returns the Requested Data
	 * 
	 * @return Returns the Requested Data
	 * 
	 * @hibernate.property column = "REQUESTED_DATA"
	 */
	public String getRequestedData() {
		return requestedData;
	}

	public void setRequestedData(String requestedData) {
		this.requestedData = requestedData;
	}

	/**
	 * returns Roll Forward status
	 * 
	 * @return Returns status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return the rollForwardBatchREQSEGSet
	 * 
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="R_FWD_BATCH_SEG_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG"
	 */
	public Set<RollForwardBatchREQSEG> getRollForwardBatchREQSEGSet() {
		return rollForwardBatchREQSEGSet;
	}

	public void setRollForwardBatchREQSEGSet(
			Set<RollForwardBatchREQSEG> rollForwardBatchREQSEGSet) {
		this.rollForwardBatchREQSEGSet = rollForwardBatchREQSEGSet;
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getBatchId()).toHashCode();
	}
	@Override
	public boolean equals(Object o) {
		if (o instanceof RollForwardBatchREQ) {
			RollForwardBatchREQ obj = (RollForwardBatchREQ) o;
			if (obj.getBatchId() != null && this.getBatchId() != null) {

				if (obj.getBatchId().intValue() == this.getBatchId().intValue()) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
