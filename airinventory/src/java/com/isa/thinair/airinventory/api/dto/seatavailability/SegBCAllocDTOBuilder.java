package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * DTO representing the temp segment booking class allocations with flight segment details.
 * 
 * @author mekanayake
 * 
 */
public class SegBCAllocDTOBuilder implements Serializable {

	public enum DIRECTION {
		IB, OB;

		public boolean isInbound() {
			return this.equals(DIRECTION.IB);
		}
	}

	private static final long serialVersionUID = -144644041853365874L;

	private final SegmentSeatDistsDTO segmentSeatDistsDTO = new SegmentSeatDistsDTO();

	private Integer fccsaId;

	private int seatsBlocked;

	// private String bookingClassType;

	private Integer nestRecordId;

	private DIRECTION direction;

	private String blockSeatUID;

	// private boolean isFixedQuotaSeats;

	/** form the T_FCC_SEG_BC_ALLOC */
	// private int flightId;

	// private String segmentCode;

	// private int flightSegId;

	// private String ccCode;

	// private int noOfinfants;

	// private List<SeatDistribution> seatDistributions;

	/**
	 * @return the fccsaId
	 */
	public Integer getFccsaId() {
		return fccsaId;
	}

	/**
	 * @param fccsaId
	 *            the fccsaId to set
	 */
	public void setFccsaId(Integer fccsaId) {
		this.fccsaId = fccsaId;
	}

	/**
	 * @return the seatsBlocked
	 */
	public int getSeatsBlocked() {
		return seatsBlocked;
	}

	/**
	 * @param seatsBlocked
	 *            the seatsBlocked to set
	 */
	public void setSeatsBlocked(int seatsBlocked) {
		this.seatsBlocked = seatsBlocked;
	}

	/**
	 * @return the nestRecordId
	 */
	public Integer getNestRecordId() {
		return nestRecordId;
	}

	/**
	 * @param nestRecordId
	 *            the nestRecordId to set
	 */
	public void setNestRecordId(Integer nestRecordId) {
		this.nestRecordId = nestRecordId;
	}

	/**
	 * @return the isFixedQuotaSeats
	 */
	public boolean isFixedQuotaSeats() {
		return segmentSeatDistsDTO.isFixedQuotaSeats();
	}

	/**
	 * @param isFixedQuotaSeats
	 *            the isFixedQuotaSeats to set
	 */
	public void setFixedQuotaSeats(boolean isFixedQuotaSeats) {
		this.segmentSeatDistsDTO.setFixedQuotaSeats(isFixedQuotaSeats);
	}

	/**
	 * @return the direction
	 */
	public DIRECTION getDirection() {
		return direction;
	}

	/**
	 * @param direction
	 *            the direction to set
	 */
	public void setDirection(DIRECTION direction) {
		this.direction = direction;
		this.segmentSeatDistsDTO.setBelongToInboundOnd(direction.isInbound());
	}

	/**
	 * @return the blockSeatUID
	 */
	public String getBlockSeatUID() {
		return blockSeatUID;
	}

	/**
	 * @param blockSeatUID
	 *            the blockSeatUID to set
	 */
	public void setBlockSeatUID(String blockSeatUID) {
		this.blockSeatUID = blockSeatUID;
	}

	/**
	 * @return the flightId
	 */
	public int getFlightId() {
		return segmentSeatDistsDTO.getFlightId();
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightId(int flightId) {
		this.segmentSeatDistsDTO.setFlightId(flightId);
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentSeatDistsDTO.getSegmentCode();
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentSeatDistsDTO.setSegmentCode(segmentCode);
	}

	/**
	 * @return the flightSegId
	 */
	public int getFlightSegId() {
		return segmentSeatDistsDTO.getFlightSegId();
	}

	/**
	 * @param flightSegId
	 *            the flightSegId to set
	 */
	public void setFlightSegId(int flightSegId) {
		this.segmentSeatDistsDTO.setFlightSegId(flightSegId);
	}

	/**
	 * @return the logicalCCCode
	 */
	public String getLogicalCCCode() {
		return segmentSeatDistsDTO.getLogicalCabinClass();
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.segmentSeatDistsDTO.setLogicalCabinClass(logicalCCCode);
	}

	/**
	 * @return the noOfinfants
	 */
	public int getNoOfinfants() {
		return segmentSeatDistsDTO.getNoOfInfantSeats();
	}

	/**
	 * @param noOfinfants
	 *            the noOfinfants to set
	 */
	public void setNoOfinfants(int noOfinfants) {
		this.segmentSeatDistsDTO.setNoOfInfantSeats(noOfinfants);
	}

	/**
	 * @return the seatDistributions
	 */
	public Collection<SeatDistribution> getSeatDistributions() {
		return this.segmentSeatDistsDTO.getSeatDistribution();
	}

	/**
	 * @param seatDistributions
	 *            the seatDistributions to set
	 */
	public void setSeatDistributions(List<SeatDistribution> seatDistributions) {
		for (SeatDistribution sd : seatDistributions) {
			this.segmentSeatDistsDTO.addSeatDistribution(sd);
		}

	}

	/**
	 * pnrSegId and bookingCode not handle.
	 * 
	 * @return
	 */
	protected SegmentSeatDistsDTO buildSegmentSeatDistsDTO() { // NO_UCD
		return this.segmentSeatDistsDTO;
	}
}
