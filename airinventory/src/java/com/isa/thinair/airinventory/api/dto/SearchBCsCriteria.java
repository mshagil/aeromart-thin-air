package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;

public class SearchBCsCriteria implements Serializable {

	private static final long serialVersionUID = 2593840493078357248L;

	private String classOfService = null;
	
	private String logicalCCCode = null;

	private String bookingClassType = BookingClassTypes.ALL;

	private String allocationType = null;

	private int startIndex;

	private int pageSize;

	private String fixedFlag = null;

	private String standardFlag = null;

	private String anyCharacterMatcher = AirinventoryCustomConstants.getAirInventoryConfig().getConstant(
			AirinventoryCustomConstants.DATABASE_ANY_CHARACTER_MATCHER);
	private String bookingCode = null;

	private String status;

	private void setFixedFlag(String fixedFlag) {
		this.fixedFlag = fixedFlag;
	}

	private void setStandardFlag(String standardFlag) {
		this.standardFlag = standardFlag;
	}

	public String getBookingClassType() {
		return bookingClassType;
	}

	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
		if (bookingClassType.equals(BookingClassTypes.ALL)) {
			setStandardFlag(anyCharacterMatcher);
			setFixedFlag(anyCharacterMatcher);
		} else if (bookingClassType.equals(BookingClassTypes.STANDARD_ONLY)) {
			setStandardFlag("Y");
			setFixedFlag("N");
		} else if (bookingClassType.equals(BookingClassTypes.NON_STANDARD_ONLY)) {
			setStandardFlag("N");
			setFixedFlag("N");
		} else if (bookingClassType.equals(BookingClassTypes.FIXED_ONLY)) {
			setStandardFlag("N");
			setFixedFlag("Y");
		} else {
			// if no proper bookingclasstype is not set, take it as all
			setStandardFlag(anyCharacterMatcher);
			setFixedFlag(anyCharacterMatcher);
		}
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}
	
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getFixedFlag() {
		return fixedFlag;
	}

	public String getStandardFlage() {
		return standardFlag;
	}

	/**
	 * @return Returns the allocationType.
	 */
	public String getAllocationType() {
		return allocationType;
	}

	/**
	 * @param allocationType
	 *            The allocationType to set.
	 */
	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public static List<String> getBookingClassTypes() {
		if (bookingClassTypes == null) {
			bookingClassTypes = new ArrayList<String>();
			bookingClassTypes.add(BookingClassTypes.ALL);
			bookingClassTypes.add(BookingClassTypes.STANDARD_ONLY);
			bookingClassTypes.add(BookingClassTypes.NON_STANDARD_ONLY);
			bookingClassTypes.add(BookingClassTypes.FIXED_ONLY);
		}
		return bookingClassTypes;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	private static List<String> bookingClassTypes = null;

	public static interface BookingClassTypes {

		public static String ALL = "All";

		public static String STANDARD_ONLY = "Standard Only";

		public static String NON_STANDARD_ONLY = "Non Standard Only";

		public static String FIXED_ONLY = "Fixed Only";

	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
