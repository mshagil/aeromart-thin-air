package com.isa.thinair.airinventory.api.model;

import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * 
 * @hibernate.class table = "T_LOGICAL_CABIN_CLASS"
 * 
 *                  LogicalCabinClass is the entity class to represent a LogicalCabinClass model
 */
public class LogicalCabinClass extends Tracking {

	private static final long serialVersionUID = 1L;

	private String logicalCCCode;

	private String cabinClassCode;

	private String description;

	/**
	 * The {@link I18nMessageKey} ID that will hold the internationalized messages for description field Unable to map
	 * {@link I18nMessageKey} directly due to modularization
	 */
	private String descriptionI18n;

	private String status;

	private int nestRank;

	private String allowSingleMealOnly;

	private String freeSeatEnabled;

	private String freeFlexiEnabled;

	private String flexiDescription;

	/**
	 * The {@link I18nMessageKey} ID that will hold the internationalized messages for flexiDescription field Unable to
	 * map {@link I18nMessageKey} directly due to modularization
	 */
	private String flexiDescriptionI18n;

	private String comments;

	/**
	 * The {@link I18nMessageKey} ID that will hold the internationalized messages for comments field Unable to map
	 * {@link I18nMessageKey} directly due to modularization
	 */
	private String commentsI18n;

	private String flexiComment;

	/**
	 * The {@link I18nMessageKey} ID that will hold the internationalized messages for flexiComment field Unable to map
	 * {@link I18nMessageKey} directly due to modularization
	 */
	private String flexiCommentI18n;
	
	private Integer bundleDescTemplateId;

	/**
	 * returns the cabinClassCode
	 * 
	 * @return Returns the cabinClassCode.
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * sets the cabinClassCode
	 * 
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the nestRank
	 * 
	 * @hibernate.property column = "LOGICAL_CC_RANK"
	 */
	public int getNestRank() {
		return nestRank;
	}

	/**
	 * @param nestRank
	 *            the nestRank to set
	 */
	public void setNestRank(int nestRank) {
		this.nestRank = nestRank;
	}

	/**
	 * @return the logicalCCCode
	 * @hibernate.id column = "LOGICAL_CABIN_CLASS_CODE" generator-class = "assigned"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the allowSingleMealOnly
	 * @hibernate.property column = "ALLOW_SINGLE_MEAL"
	 */
	public String getAllowSingleMealOnly() {
		return allowSingleMealOnly;
	}

	/**
	 * @param allowSingleMealOnly
	 *            the allowSingleMealOnly to set
	 */
	public void setAllowSingleMealOnly(String multiMealEnabled) {
		this.allowSingleMealOnly = multiMealEnabled;
	}

	/**
	 * @return the freeSeatEnabled
	 * @hibernate.property column = "FREE_SEAT_ENABLED"
	 */
	public String getFreeSeatEnabled() {
		return freeSeatEnabled;
	}

	/**
	 * @param freeSeatEnabled
	 *            the freeSeatEnabled to set
	 */
	public void setFreeSeatEnabled(String freeSeatEnabled) {
		this.freeSeatEnabled = freeSeatEnabled;
	}

	/**
	 * @return the freeFlexiEnabled
	 * @hibernate.property column = "FREE_FLEXI_ENABLED"
	 */
	public String getFreeFlexiEnabled() {
		return freeFlexiEnabled;
	}

	/**
	 * @param freeFlexiEnabled
	 *            the freeFlexiEnabled to set
	 */
	public void setFreeFlexiEnabled(String freeFlexiEnabled) {
		this.freeFlexiEnabled = freeFlexiEnabled;
	}

	/**
	 * @return the flexiDescription
	 * @hibernate.property column ="FLEXI_DESCRIPTION"
	 */
	public String getFlexiDescription() {
		return flexiDescription;
	}

	/**
	 * @param flexiDescription
	 *            the flexiDescription to set
	 */
	public void setFlexiDescription(String flexiDescription) {
		this.flexiDescription = flexiDescription;
	}

	/**
	 * 
	 * @return
	 */
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the flexiComment
	 * @hibernate.property column ="FLEXI_COMMENT"
	 */
	public String getFlexiComment() {
		return flexiComment;
	}

	/**
	 * @param flexiComment
	 *            the flexiComment to set
	 */
	public void setFlexiComment(String flexiComment) {
		this.flexiComment = flexiComment;
	}

	/**
	 * @param descriptionI18n
	 *            the descriptionI18n to set
	 */
	public void setDescriptionI18n(String descriptionI18n) {
		this.descriptionI18n = descriptionI18n;
	}

	/**
	 * @return the descriptionI18n
	 * @hibernate.property column = "DESCRIPTION_I18N_MESSAGE_KEY"
	 */
	public String getDescriptionI18n() {
		return descriptionI18n;
	}

	/**
	 * @param flexiDescriptionI18n
	 *            the flexiDescriptionI18n to set
	 */
	public void setFlexiDescriptionI18n(String flexiDescriptionI18n) {
		this.flexiDescriptionI18n = flexiDescriptionI18n;
	}

	/**
	 * @return the flexiDescriptionI18n
	 * @hibernate.property column = "FLEXI_DESC_I18N_MESSAGE_KEY"
	 */
	public String getFlexiDescriptionI18n() {
		return flexiDescriptionI18n;
	}

	/**
	 * @param commentsI18n
	 *            the commentsI18n to set
	 */
	public void setCommentsI18n(String commentsI18n) {
		this.commentsI18n = commentsI18n;
	}

	/**
	 * @return the commentsI18n
	 * @hibernate.property column = "COMMENTS_I18N_MESSAGE_KEY"
	 */
	public String getCommentsI18n() {
		return commentsI18n;
	}

	/**
	 * @param flexiCommentI18n
	 *            the flexiCommentI18n to set
	 */
	public void setFlexiCommentI18n(String flexiCommentI18n) {
		this.flexiCommentI18n = flexiCommentI18n;
	}

	/**
	 * @return the flexiCommentI18n
	 * @hibernate.property column = "FLEXI_COMMENT_I18N_MESSAGE_KEY"
	 */
	public String getFlexiCommentI18n() {
		return flexiCommentI18n;
	}

	/**
	 * @return the bundleDescTemplateId
	 * @hibernate.property column = "BUNDLE_DESC_TEMPLATE_ID"
	 */
	public Integer getBundleDescTemplateId() {
		return bundleDescTemplateId;
	}

	/**
	 * @param bundleDescTemplateId the bundleDescTemplateId to set
	 */
	public void setBundleDescTemplateId(Integer bundleDescTemplateId) {
		this.bundleDescTemplateId = bundleDescTemplateId;
	}
	
	public void setBundleDescId(String bundleDescId) {
		this.bundleDescTemplateId = Integer.valueOf(bundleDescId);
	}
}
