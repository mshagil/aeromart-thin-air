/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.FlightTo;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Meal inventory maintenace related functionalities
 */
public interface MealBD {

	public static final String SERVICE_NAME = "MealService";

	/**
	 * Gets the map of Meals collection for given Flight segs
	 * 
	 * @param flightSegIdWiseCOS
	 * @param flightSegIdWiseSelectedMeals
	 * @param skipeCutoverValidation
	 * @param salesChannelCode TODO
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, List<FlightMealDTO>> getMeals(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCOS,
			Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, boolean skipeCutoverValidation, PnrFarePassenger pnrFarePax,
			int salesChannelCode) throws ModuleException;

	/**
	 * Gets the map of Meals collection for given Flight segs along with translations
	 * 
	 * @param flightSegIdWiseCOS
	 * @param selectedLanguage
	 * @param flightSegIdWiseSelectedMeals
	 * @param salesChannelCode
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, List<FlightMealDTO>> getMealsWithTranslations(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCOS,
			String selectedLanguage, Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, int salesChannelCode)
			throws ModuleException;

	/**
	 * Retrieves Meal information from the Meal charge template directly. No associated flights are necessary.
	 * 
	 * @param templateID
	 *            Meal charge template ID of the meals to be retrieved.
	 * @param cabinClassCode
	 *            TODO
	 * @param logicalCabinClass
	 *            TODO
	 * @param selectedLanguage
	 *            TODO
	 * @param flightSegID TODO
	 * @param salesChannelCode TODO
	 * @return The Meals attached to the meal charge template. Or an empty collection if the templateID is null or
	 *         invalid.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public List<FlightMealDTO> getMealsByTemplate(Integer templateID, String cabinClassCode, String logicalCabinClass,
			String selectedLanguage, Integer flightSegID, Set<String> existingMeals, int salesChannelCode) throws ModuleException;

	/**
	 * Retrieves Meal information from the Meal charge templates segment wise. No associated flights are necessary.
	 * 
	 * @param segWiseTemplateIDs
	 *            Segment wise meal charge template ID of the meals to be retrieved. Key->Segment ID Value->Meal Charge
	 *            Template ID
	 * @param salesChannelCode TODO
	 * @return Segment wise meals attached to the meal charge templates given. Key-> Segment ID Value -> The Meals
	 *         attached to the meal charge template. Value contains an empty collection if the templateID is invalid.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public Map<Integer, List<FlightMealDTO>> getSegWiseMealsByTemplates(Map<Integer, Integer> segWiseTemplateIDs, int salesChannelCode)
			throws ModuleException;

	/**
	 * @param flightId
	 * @param cos
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Collection getMealTempleateIds(int flightId, String cos) throws ModuleException;

	/**
	 * 
	 * @param sourceFlightId
	 * @param classOfService
	 * @param templateCode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce assignFlightMealChargesRollFoward(int sourceFlightId, Collection<Integer> destinationFlightIds,
			ArrayList<String> lstSelectedSeg) throws ModuleException;

	/**
	 * 
	 * @param flightMealIds
	 * @param flightAMSeatIdsToCancell
	 * @throws ModuleException
	 */
	public void modifyMeal(Collection<Integer> flightMealIds, Collection<Integer> flightAMSeatIdsToCancell)
			throws ModuleException;

	/**
	 * 
	 * @param flightMealDTOs
	 * @param isFromEdit
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce
			assignFlightMealCharges(Collection<FlightMealDTO> flightMealDTOs, Integer chargeId, boolean isFromEdit)
					throws ModuleException;

	/**
	 * 
	 * @param flightMealIds
	 * @throws ModuleException
	 */
	public void vacateMeals(Collection<Integer> flightMealIds) throws ModuleException;

	/**
	 * 
	 * @param flightSegId
	 * @param mealIds
	 * @return
	 * @throws ModuleException
	 */
	public Collection<FlightMealDTO> getAvailableMealDtos(int flightSegId, Collection<Integer> mealIds) throws ModuleException;

	/**
	 * Deletes flight meals
	 * 
	 * @param flightSegId
	 * @throws ModuleException
	 */
	public void deleteFlightMeals(Collection<Integer> flightSegIDs) throws ModuleException;

	/**
	 * Creates flight meal charge if one doesn't already exist.
	 * 
	 * @param mealChargeDetail
	 * @param logicalCCCode
	 * @param cabinClass
	 * @throws ModuleException
	 */
	public FlightMealDTO createFlightMealTemplateIfDoesntExist(Integer offeredTemplateID, Integer flightSegID, String mealCode)
			throws ModuleException;

	public List<FlightTo> getMealRecords(List<FlightTo> flights);

    public Map<Integer, Set<String>> getExistingMeals(List<Integer> pnrSegIds) throws ModuleException;
    
    /**
	 * Gets the Meals collection
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public List<FlightMealDTO> getMealsForPreference() throws ModuleException;
	

	/**
	 * Gets the Meals collection along with translations
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public List<FlightMealDTO> getMealsForPreferenceWithTranslations(String selectedLanguage) throws ModuleException;

}
