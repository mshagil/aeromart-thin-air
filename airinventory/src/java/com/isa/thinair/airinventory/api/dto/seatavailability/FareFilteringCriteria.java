package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Map;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;

public class FareFilteringCriteria implements Serializable {

	private static final long serialVersionUID = 3902623185094321590L;

	/* Journey's outbound ond */
	private String outboundOndCode = null;

	private String inboundOndCode = null;

	// private String cabinClassCode;

	/* True if return journey */
	private boolean returnFlag;

	/* Sales channel code */
	/*-1 = do not filter based on sales channel*/
	private int channelCode = -1;

	/* Agent code */
	private String agentCode = null;

	/* The adult seats count */
	private int numberOfAdults;

	/** The child seats count */
	private int numberOfChilds;

	/** The infant seats count */
	private int numberOfInfants;

	/*
	 * The booking type Possible booking types are defined in BookingClass.BookingClassType
	 */
	private String bookingType;

	/** Specifies whether to include only the min public fare or all */
	private boolean excludeNonLowestPublicFares = false;

	private String selectedAgent = null;

	/** Specifies the booking passenger catergory type */
	private String bookingPaxType = AirPricingCustomConstants.BookingPaxType.ANY;

	/** Denotes fare category type */
	private String fareCategoryType = AirPricingCustomConstants.FareCategoryType.ANY;

	private Map<Integer, Boolean> ondShowFaresWithFlexi;

	private boolean excludeOnewayPublicFaresFromReturnJourneys;

	public int getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}

	public String getInboundOndCode() {
		return inboundOndCode;
	}

	public void setInboundOndCode(String inboundOndCode) {
		this.inboundOndCode = inboundOndCode;
	}

	public String getOutboundOndCode() {
		return outboundOndCode;
	}

	public void setOutboundOndCode(String outboundOndCode) {
		this.outboundOndCode = outboundOndCode;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public int getNumberOfAdults() {
		return numberOfAdults;
	}

	public void setNumberOfAdults(int numberOfAdults) {
		this.numberOfAdults = numberOfAdults;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public boolean isExcludeNonLowestPublicFares() {
		return excludeNonLowestPublicFares;
	}

	public void setExcludeNonLowestPublicFares(boolean excludeNonLowestPublicFares) {
		this.excludeNonLowestPublicFares = excludeNonLowestPublicFares;
	}

	public String getBookingPaxType() {
		return bookingPaxType;
	}

	public void setBookingPaxType(String bookingPaxType) {
		this.bookingPaxType = bookingPaxType;
	}

	public String getFareCategoryType() {
		return fareCategoryType;
	}

	public void setFareCategoryType(String fareCategoryType) {
		this.fareCategoryType = fareCategoryType;
	}

	/**
	 * @return Returns the numberOfChilds.
	 */
	public int getNumberOfChilds() {
		return numberOfChilds;
	}

	/**
	 * @param numberOfChilds
	 *            The numberOfChilds to set.
	 */
	public void setNumberOfChilds(int numberOfChilds) {
		this.numberOfChilds = numberOfChilds;
	}

	/**
	 * @return Returns the numberOfInfants.
	 */
	public int getNumberOfInfants() {
		return numberOfInfants;
	}

	/**
	 * @param numberOfInfants
	 *            The numberOfInfants to set.
	 */
	public void setNumberOfInfants(int numberOfInfants) {
		this.numberOfInfants = numberOfInfants;
	}

	public String getSelectedAgent() {
		return selectedAgent;
	}

	public void setSelectedAgent(String selectedAgent) {
		this.selectedAgent = selectedAgent;
	}

	public Map<Integer, Boolean> getOndShowFaresWithFlexi() {
		return ondShowFaresWithFlexi;
	}

	public void setOndShowFaresWithFlexi(Map<Integer, Boolean> ondShowFaresWithFlexi) {
		this.ondShowFaresWithFlexi = ondShowFaresWithFlexi;
	}

	public boolean isExcludeOnewayPublicFaresFromReturnJourneys() {
		return excludeOnewayPublicFaresFromReturnJourneys;
	}

	public void setExcludeOnewayPublicFaresFromReturnJourneys(boolean excludeOnewayPublicFaresFromReturnJourneys) {
		this.excludeOnewayPublicFaresFromReturnJourneys = excludeOnewayPublicFaresFromReturnJourneys;
	}

}
