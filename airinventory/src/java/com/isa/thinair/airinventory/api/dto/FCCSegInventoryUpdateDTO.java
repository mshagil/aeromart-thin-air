package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Set;

import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;

/**
 * Used when updating a segment inventory [TODO] How do we handle concurrent updation if we are to use DTOs for
 * transfering updation data? [MN] Removed applyCurtailToInterceptingSegs : boolean after confirming with [JK] [MN]
 * added fccSegBCInventories : Set after discussing with web flow designers. The set contains detached persistent
 * objects of type FCCSegBCInventory
 */
public class FCCSegInventoryUpdateDTO implements Serializable {

	private static final long serialVersionUID = -6668090079805897192L;

	private int flightId;

	private int fccsInventoryId;

	private String logicalCCCode;

	private String segmentCode;

	private int curtailedSeats;

	private int oversellSeats;
	
	private int waitListSeats;

	private int infantAllocation;

	private boolean isFCCSegInvUpdated = true;

	@SuppressWarnings("rawtypes")
	private Set deletedFCCSegBCInventories;

	@SuppressWarnings("rawtypes")
	private Set deletedFCCSegBookingCodes;

	private Set<FCCSegBCInventory> addedFCCSegBCInventories;

	private Set<FCCSegBCInventory> updatedFCCSegBCInventories;

	private long version; // TODO: Clarification : Why do we need
							// version?

	public FCCSegInventoryUpdateDTO() {

	}

	public Set<FCCSegBCInventory> getAddedFCCSegBCInventories() {
		return addedFCCSegBCInventories;
	}

	public void setAddedFCCSegBCInventories(Set<FCCSegBCInventory> addedFCCSegBCInventories) {
		this.addedFCCSegBCInventories = addedFCCSegBCInventories;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public int getCurtailedSeats() {
		return curtailedSeats;
	}

	public void setCurtailedSeats(int curtailedSeats) {
		this.curtailedSeats = curtailedSeats;
	}

	@SuppressWarnings("rawtypes")
	public Set getDeletedFCCSegBCInventories() {
		return deletedFCCSegBCInventories;
	}

	@SuppressWarnings("rawtypes")
	public void setDeletedFCCSegBCInventories(Set deletedFCCSegBCInventories) {
		this.deletedFCCSegBCInventories = deletedFCCSegBCInventories;
	}

	@SuppressWarnings("rawtypes")
	public Set getDeletedFCCSegBookingCodes() {
		return deletedFCCSegBookingCodes;
	}

	@SuppressWarnings("rawtypes")
	public void setDeletedFCCSegBookingCodes(Set deletedFCCSegBookingCodes) {
		this.deletedFCCSegBookingCodes = deletedFCCSegBookingCodes;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public int getOversellSeats() {
		return oversellSeats;
	}

	public void setOversellSeats(int oversellSeats) {
		this.oversellSeats = oversellSeats;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Set<FCCSegBCInventory> getUpdatedFCCSegBCInventories() {
		return updatedFCCSegBCInventories;
	}

	public void setUpdatedFCCSegBCInventories(Set<FCCSegBCInventory> updatedFCCSegBCInventories) {
		this.updatedFCCSegBCInventories = updatedFCCSegBCInventories;
	}

	public int getInfantAllocation() {
		return infantAllocation;
	}

	public void setInfantAllocation(int infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public int getFccsInventoryId() {
		return fccsInventoryId;
	}

	public void setFccsInventoryId(int fccsInventoryId) {
		this.fccsInventoryId = fccsInventoryId;
	}

	public boolean isFCCSegInvUpdated() {
		return isFCCSegInvUpdated;
	}

	public void setFCCSegInvUpdated(boolean isFCCSegInvUpdated) {
		this.isFCCSegInvUpdated = isFCCSegInvUpdated;
	}

	public int getWaitListSeats() {
		return waitListSeats;
	}

	public void setWaitListSeats(int waitListSeats) {
		this.waitListSeats = waitListSeats;
	}

}
