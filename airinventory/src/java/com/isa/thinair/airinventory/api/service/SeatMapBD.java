/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Byorn
 * 
 *         Flight seat inventory maintenace related functionalities
 */
public interface SeatMapBD {

	public static final String SERVICE_NAME = "SeatMapService";

	public Map<Integer, FlightSeatsDTO> getFlightSeats(List<Integer> flightSegIds, String localeCode, boolean loadSoicalIdentity) throws ModuleException;

	/**
	 * Retrieves Seat information from the Seat charge template directly. No associated flights are necessary.
	 * 
	 * @param chargeTemplateID
	 *            Seat charge template ID of the seats to be retrieved.
	 * @return The seats attached to the seat charge template. Or an empty collection if the chargeTemplateID is null or
	 *         invalid.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public Collection<SeatDTO> getFlightSeatsByChargeTemplate(Integer chargeTemplateID) throws ModuleException;

	public Map<Integer, FlightSeatsDTO> getFlightSeats(Map<Integer, ClassOfServiceDTO> flightSegIds, String localeCode)
			throws ModuleException;

	/**
	 * Retrieves the seat details for the given seat charge template IDs.
	 * 
	 * @param seatChargeTemplateIDs
	 *            Map<Integer,Integer> in the format of Map<Segment ID, Seat Map Charge Template ID>
	 * @return Map<Integer, Collection<SeatDTO>> The Key is the Segment ID, Value contains the seats attached to the
	 *         seat charge templates given. Value will contain an empty collection if the chargeTemplateID is null or
	 *         invalid.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public Map<Integer, Collection<SeatDTO>> getSeatInfoForSeatChargeTemplates(
			Map<Integer, Integer> flightSegIDWiseOfferTemplateIDs) throws ModuleException;

	/**
	 * @param flightSegIds
	 *            Collection of Flight Segment IDs
	 * @param templateCode
	 *            TemplateCode to assign
	 * @return ServiceResponse contains
	 * @throws ModuleException
	 */

	public Map<Integer, FlightSeatsDTO> getFlightSeatsRow(Collection<Integer> flightSegIds, String localeCode, int rowNo)
			throws ModuleException;

	public ServiceResponce assignFlightSeatCharge(Collection<Integer> flightSegIds, Map<Integer, String> newSeatCharges)
			throws ModuleException;

	public ServiceResponce assignFlightSeatCharges(Collection<Integer> flightSegIds, int templateId, String modelNo,
			ChargeTemplate chargeTemplate) throws ModuleException;

	/**
	 * 
	 * @param flightSegId
	 * @param seatCodes
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, Integer> getFlightSeatIdMap(int flightSegId, Collection<String> seatCodes) throws ModuleException;

	/**
	 * @param flightId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<FlightSeatsDTO> getTempleateIds(int flightId) throws ModuleException;

	/**
	 * 
	 * @param sourceFlightId
	 * @param templateCode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce assignFlightSeatChargesRollFoward(int sourceFlightId, Collection<Integer> destinationFlightIds,
			String modelNo) throws ModuleException;

	/**
	 * 
	 * @param flightSeatDTOs
	 * @param modelNo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce assignFlightSeatCharges(Collection<FlightSeatsDTO> flightSeatDTOs, String modelNo)
			throws ModuleException;

	/**
	 * @param flightAMSeatIdPaxType
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce blockSeats(Map<Integer, String> flightAMSeatIdPaxType) throws ModuleException;

	/**
	 * @param flightAMSeatIds
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce releaseBlockedSeats(Collection<Integer> flightAMSeatIds) throws ModuleException;

	/**
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce releaseBlockedSeats() throws ModuleException;

	/**
	 * @param flightAMSeatIds
	 * @throws ModuleException
	 */
	public void vacateSeats(Collection<Integer> flightAMSeatIds) throws ModuleException;

	/**
	 * @param flightAmSeatIdsPaxTypeToCNF
	 * @param flightAMSeatIdsToCancell
	 * @throws ModuleException
	 */
	public void modifySeat(Map<Integer, String> flightAmSeatIdsPaxTypeToCNF, Collection<Integer> flightAMSeatIdsToCancell)
			throws ModuleException;

	/**
	 * @param oldTemplateId
	 * @param seatCharges
	 * @param newTemplateCode
	 * @param flightSegId
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce copyTemplateAsNew(int oldTemplateId, Collection<SeatCharge> seatCharges, String newTemplateCode,
			int flightSegId) throws ModuleException;

	/**
	 * @param flightSeatIdPaxType
	 * @param paxFlightSeatSwapMap
	 *            TODO
	 * @throws ModuleException
	 */
	public void updatePaxType(Map<Integer, String> flightSeatIdPaxType, Map<Integer, Integer> paxFlightSeatSwapMap)
			throws ModuleException;

	/**
	 * @param flitghtSegID
	 * @param colSeatCharges
	 * @param modelNo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce updateTemplateFromFlight(int flitghtSegID, Collection<SeatCharge> colSeatCharges, String modelNo)
			throws ModuleException;

	/**
	 * @param flightSegmentId
	 * @param seatCode
	 * @return
	 */
	public Integer getFlightSeatID(Integer flightSegmentId, String seatCode);

	/**
	 * @param flightSegmentId
	 * @param seatCode
	 * @return
	 */
	public Integer getFlightAmSeatID(Integer flightSegmentId, String seatCode);

	public void checkSeatingRules(Map<Integer, String> flightSeatIdPaxType, Collection<Integer> excludeCheckSeatIDs)
			throws ModuleException;
	
	public void deleteFlightSeats(Collection<Integer> flightSegIds) throws ModuleException;
}