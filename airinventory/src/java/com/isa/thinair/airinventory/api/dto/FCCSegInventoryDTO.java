package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;

import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;

/**
 * Used for retrieving FCCSegment Inventory along with FCCSegmentBC Inventories
 * 
 * @author MN
 */
public class FCCSegInventoryDTO implements Serializable {

	private static final long serialVersionUID = 6605421874907145538L;

	private int fccsInvId;

	private int flightId;

	private String flightNumber;

	private String cabinClassCode;

	private String logicalCCCode;

	private Integer logicalCCRank;

	private String segmentCode;

	private String flightSegmentStatus;

	private FCCSegInventory fccSegInventory;

	private Collection<ExtendedFCCSegBCInventory> fccSegBCInventories;

	@SuppressWarnings("rawtypes")
	private LinkedHashMap fccSegBCInventoriesMap;

	private boolean overlap = false;

	private int soldSeats; // following properties are used by
							// rollforward, later need to remove
							// FCCSegInventory

	private int onholdSeats;

	private int availableSeats;

	private int oversellSeats;

	private int curtailedSeats;
	
	private int waitListingSeats;

	private int adultAllocation;

	private int infantAllocation;

	public FCCSegInventoryDTO() {

	}

	// just in case if its used in some hql
	public FCCSegInventoryDTO(int fccsInvId, int flightId, String logicalCabinClassCode, String segmentCode, int soldSeats,
			int onholdSeats, int availableSeats) {
		setFccsInvId(fccsInvId);
		setFlightId(flightId);
		setLogicalCCCode(logicalCabinClassCode);
		setSegmentCode(segmentCode);
		setSoldSeats(soldSeats);
		setOnholdSeats(onholdSeats);
		setAvailableSeats(availableSeats);
	}

	public FCCSegInventoryDTO(int fccsInvId, int flightId, String cabinClassCode, String logicalCabinClassCode,
			String segmentCode, int soldSeats, int onholdSeats, int availableSeats, int oversellSeats, int curtailedSeats,
			int waitListingSeats) {
		setFccsInvId(fccsInvId);
		setFlightId(flightId);
		setCabinClassCode(cabinClassCode);
		setLogicalCCCode(logicalCabinClassCode);
		setSegmentCode(segmentCode);
		setSoldSeats(soldSeats);
		setOnholdSeats(onholdSeats);
		setAvailableSeats(availableSeats);
		setOversellSeats(oversellSeats);
		setCurtailedSeats(curtailedSeats);
		setWaitListingSeats(waitListingSeats);
	}

	public FCCSegInventoryDTO(int fccsInvId, int flightId, String cabinClassCode, String logicalCabinClassCode,
			String segmentCode, int soldSeats, int onholdSeats, int adultSeats, int infantSeats) {
		setFccsInvId(fccsInvId);
		setFlightId(flightId);
		setCabinClassCode(cabinClassCode);
		setLogicalCCCode(logicalCabinClassCode);
		setSegmentCode(segmentCode);
		setSoldSeats(soldSeats);
		setOnholdSeats(onholdSeats);
		setAdultAllocation(adultSeats);
		setInfantAllocation(infantSeats);

	}

	public int getFccsInvId() {
		return fccsInvId;
	}

	public void setFccsInvId(int fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public Collection<ExtendedFCCSegBCInventory> getFccSegBCInventories() {
		if (this.fccSegBCInventories == null && getFccSegBCInventoriesMap() != null) {
			setFccSegBCInventories(AirinventoryUtils.getSearializableValues(getFccSegBCInventoriesMap()));
		}
		return this.fccSegBCInventories;
	}

	public void setFccSegBCInventories(Collection<ExtendedFCCSegBCInventory> fccSegBCInventories) {
		this.fccSegBCInventories = fccSegBCInventories;
	}

	@SuppressWarnings("rawtypes")
	public LinkedHashMap getFccSegBCInventoriesMap() {
		return fccSegBCInventoriesMap;
	}

	@SuppressWarnings("rawtypes")
	public void setFccSegBCInventoriesMap(LinkedHashMap fccSegBCInventoriesMap) {
		this.fccSegBCInventoriesMap = fccSegBCInventoriesMap;
	}

	public FCCSegInventory getFccSegInventory() {
		return fccSegInventory;
	}

	public void setFccSegInventory(FCCSegInventory fccSegInventory) {
		this.fccSegInventory = fccSegInventory;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public boolean isOverlap() {
		return overlap;
	}

	public void setOverlap(boolean overlap) {
		this.overlap = overlap;
	}

	public int getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	public int getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	public String getFlightSegmentStatus() {
		return flightSegmentStatus;
	}

	public void setFlightSegmentStatus(String flightSegmentStatus) {
		this.flightSegmentStatus = flightSegmentStatus;
	}

	public int getOversellSeats() {
		return oversellSeats;
	}

	public void setOversellSeats(int oversellSeats) {
		this.oversellSeats = oversellSeats;
	}

	public int getCurtailedSeats() {
		return curtailedSeats;
	}

	public void setCurtailedSeats(int curtailedSeats) {
		this.curtailedSeats = curtailedSeats;
	}

	public int getAdultAllocation() {
		return adultAllocation;
	}

	public void setAdultAllocation(int adultAllocation) {
		this.adultAllocation = adultAllocation;
	}

	public int getInfantAllocation() {
		return infantAllocation;
	}

	public void setInfantAllocation(int infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	public Integer getLogicalCCRank() {
		return logicalCCRank;
	}

	public void setLogicalCCRank(Integer logicalCCRank) {
		this.logicalCCRank = logicalCCRank;
	}

	public int getWaitListingSeats() {
		return waitListingSeats;
	}

	public void setWaitListingSeats(int waitListingSeats) {
		this.waitListingSeats = waitListingSeats;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addFCCSegBCInventory(String bookingCode, FCCSegBCInventory fccSegBCInventory) {
		if (this.fccSegBCInventoriesMap == null) {
			this.fccSegBCInventoriesMap = new LinkedHashMap();
		}
		this.fccSegBCInventoriesMap.put(bookingCode, fccSegBCInventory);
	}

	public boolean hasReservation() {
		if (getSoldSeats() > 0 || getOnholdSeats() > 0) {
			return true;
		}
		return false;
	}
}
