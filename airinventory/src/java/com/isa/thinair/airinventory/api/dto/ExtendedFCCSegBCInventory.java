package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airpricing.api.dto.FareSummaryLightDTO;

public class ExtendedFCCSegBCInventory implements Serializable {

	private static final long serialVersionUID = 5793098933223138583L;

	private FCCSegBCInventory fccSegBCInventory;

	private boolean standardCode;

	private boolean fixedFlag;

	private Integer nestRank;

	private String paxType;

	private String allocationType;

	private String bcType;

	private String paxCategoryType;

	private String fareCategoryTypeCode;

	private int groupId = 0;

	private Collection<FareSummaryLightDTO> linkedEffectiveFares;

	@SuppressWarnings("rawtypes")
	private LinkedHashMap linkedEffectiveFaresMap;

	@SuppressWarnings("rawtypes")
	private Collection linkedEffectiveAgents;

	private Integer gdsId; // haider 14Sep08

	public static final boolean STANDARD_CODE_Y = true;

	public static final boolean STANDARD_CODE_N = false;

	public static final boolean FIXED_FLAG_Y = true;

	public static final boolean FIXED_FLAG_N = false;

	public ExtendedFCCSegBCInventory(FCCSegBCInventory fccSegBCInventory, boolean fixedFlag, boolean standardCode) {
		this.fccSegBCInventory = fccSegBCInventory;
		this.fixedFlag = fixedFlag;
		this.standardCode = standardCode;
	}

	public ExtendedFCCSegBCInventory(FCCSegBCInventory fccSegBCInventory, String fixedFlag, String standardCode,
			String allocationType, String paxType, Integer nestRank, String bcType, String paxCategoryCode,
			String fareCategoryTypeCode) {
		this.fccSegBCInventory = fccSegBCInventory;
		this.fixedFlag = fixedFlag.equals(BookingClass.FIXED_FLAG_Y) ? true : false;
		this.standardCode = standardCode.equals(BookingClass.STANDARD_CODE_Y) ? true : false;
		this.allocationType = allocationType;
		this.paxType = paxType;
		this.nestRank = nestRank;
		this.bcType = bcType;
		this.paxCategoryType = paxCategoryCode;
		this.fareCategoryTypeCode = fareCategoryTypeCode;
	}

	public ExtendedFCCSegBCInventory(FCCSegBCInventory fccSegBCInventory, String fixedFlagChar, String standardCodeChar) {
		this.fccSegBCInventory = fccSegBCInventory;
		this.fixedFlag = (fixedFlagChar.equals("Y") ? FIXED_FLAG_Y : FIXED_FLAG_N);
		this.standardCode = (standardCodeChar.equals("Y") ? STANDARD_CODE_Y : STANDARD_CODE_N);
	}

	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public ExtendedFCCSegBCInventory(FCCSegBCInventory fccSegBCInventory) {
		this.fccSegBCInventory = fccSegBCInventory;
	}

	public FCCSegBCInventory getFccSegBCInventory() {
		return fccSegBCInventory;
	}

	public void setFccSegBCInventory(FCCSegBCInventory fccSegBCInventory) {
		this.fccSegBCInventory = fccSegBCInventory;
	}

	public boolean isFixedFlag() {
		return fixedFlag;
	}

	public void setFixedFlag(boolean fixedFlag) {
		this.fixedFlag = fixedFlag;
	}

	public boolean isStandardCode() {
		return standardCode;
	}

	public void setStandardCode(boolean standardCode) {
		this.standardCode = standardCode;
	}

	@SuppressWarnings("rawtypes")
	public void setLinkedEffectiveAgents(Collection linkedEffectiveAgents) {
		this.linkedEffectiveAgents = linkedEffectiveAgents;
	}

	@SuppressWarnings("rawtypes")
	public Collection getLinkedEffectiveAgents() {
		return linkedEffectiveAgents;
	}

	public Collection<FareSummaryLightDTO> getLinkedEffectiveFares() {
		if (this.linkedEffectiveFares == null && getLinkedEffectiveFaresMap() != null) {
			this.linkedEffectiveFares = AirinventoryUtils.getSearializableValues(getLinkedEffectiveFaresMap());
		}
		return linkedEffectiveFares;
	}

	@SuppressWarnings("rawtypes")
	public void setLinkedEffectiveFares(Collection linkedEffectiveFares) {
		this.linkedEffectiveFares = linkedEffectiveFares;
	}

	@SuppressWarnings("rawtypes")
	public LinkedHashMap getLinkedEffectiveFaresMap() {
		return linkedEffectiveFaresMap;
	}

	@SuppressWarnings("rawtypes")
	public void setLinkedEffectiveFaresMap(LinkedHashMap linkedEffectiveFaresMap) {
		this.linkedEffectiveFaresMap = linkedEffectiveFaresMap;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the nestRank.
	 */
	public Integer getNestRank() {
		return nestRank;
	}

	/**
	 * @param nestRank
	 *            The nestRank to set.
	 */
	public void setNestRank(Integer nestRank) {
		this.nestRank = nestRank;
	}

	/**
	 * @return Returns the allocationType.
	 */
	public String getAllocationType() {
		return allocationType;
	}

	/**
	 * @param allocationType
	 *            The allocationType to set.
	 */
	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	/**
	 * @return Returns the bcType.
	 */
	public String getBcType() {
		return bcType;
	}

	/**
	 * @param bcType
	 *            The bcType to set.
	 */
	public void setBcType(String bcType) {
		this.bcType = bcType;
	}

	public String getPaxCategoryType() {
		return paxCategoryType;
	}

	public void setPaxCategoryType(String paxCategoryType) {
		this.paxCategoryType = paxCategoryType;
	}

	public String getFareCategoryTypeCode() {
		return fareCategoryTypeCode;
	}

	public void setFareCategoryTypeCode(String fareCategoryTypeCode) {
		this.fareCategoryTypeCode = fareCategoryTypeCode;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
}
