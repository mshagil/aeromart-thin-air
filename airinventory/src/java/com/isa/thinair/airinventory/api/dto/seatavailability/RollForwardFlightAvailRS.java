package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author rumesh
 * 
 */
public class RollForwardFlightAvailRS implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean rollForwardEnabled;
	private Date departureDate;
	private List<RollForwardFlightDTO> rollForwardFlights;

	public boolean isRollForwardEnabled() {
		return rollForwardEnabled;
	}

	public void setRollForwardEnabled(boolean rollForwardEnabled) {
		this.rollForwardEnabled = rollForwardEnabled;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public List<RollForwardFlightDTO> getRollForwardFlights() {
		if (this.rollForwardFlights == null) {
			this.rollForwardFlights = new ArrayList<RollForwardFlightDTO>();
		}
		return rollForwardFlights;
	}

	public void setRollForwardFlights(List<RollForwardFlightDTO> rollForwardFlights) {
		this.rollForwardFlights = rollForwardFlights;
	}
}
