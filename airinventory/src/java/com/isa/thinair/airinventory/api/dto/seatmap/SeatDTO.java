/**
 * 
 */
package com.isa.thinair.airinventory.api.dto.seatmap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author indika
 * 
 */
public class SeatDTO implements Serializable {

	private static final long serialVersionUID = 4945744005285687874L;

	private int flightAmSeatID;

	private int chargeID;

	private BigDecimal chargeAmount;

	private String status;

	private int seatID;

	private int templateId;

	private String seatCode;

	private String seatType;

	private int rowId;

	private int colId;

	private int rowGroupId;

	private int colGroupId;

	private long version;

	private String cabinClassCode;

	private String logicalCabinClassCode;

	private int flightSegmentID;

	private String paxType;

	private String seatMessage;

	private boolean isEditedSeat;

	private Map<String, String> socialIdentity;

	private String name;

	private String seatVisibility;

	private String seatLocationType;

	/**
	 * @return Returns the flightSegmentID.
	 */
	public int getFlightSegmentID() {
		return flightSegmentID;
	}

	/**
	 * @param flightSegmentID
	 *            The flightSegmentID to set.
	 */
	public void setFlightSegmentID(int flightSegmentID) {
		this.flightSegmentID = flightSegmentID;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the logicalCabinClassCode
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	/**
	 * @param logicalCabinClassCode
	 *            the logicalCabinClassCode to set
	 */
	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return Returns the chargeAmount.
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            The chargeAmount to set.
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return Returns the chargeID.
	 */
	public int getChargeID() {
		return chargeID;
	}

	/**
	 * @param chargeID
	 *            The chargeID to set.
	 */
	public void setChargeID(int chargeID) {
		this.chargeID = chargeID;
	}

	/**
	 * @return Returns the colGroupId.
	 */
	public int getColGroupId() {
		return colGroupId;
	}

	/**
	 * @param colGroupId
	 *            The colGroupId to set.
	 */
	public void setColGroupId(int colGroupId) {
		this.colGroupId = colGroupId;
	}

	/**
	 * @return Returns the colId.
	 */
	public int getColId() {
		return colId;
	}

	/**
	 * @param colId
	 *            The colId to set.
	 */
	public void setColId(int colId) {
		this.colId = colId;
	}

	/**
	 * @return Returns the flightAmSeatID.
	 */
	public int getFlightAmSeatID() {
		return flightAmSeatID;
	}

	/**
	 * @param flightAmSeatID
	 *            The flightAmSeatID to set.
	 */
	public void setFlightAmSeatID(int flightAmSeatID) {
		this.flightAmSeatID = flightAmSeatID;
	}

	/**
	 * @return Returns the rowGroupId.
	 */
	public int getRowGroupId() {
		return rowGroupId;
	}

	/**
	 * @param rowGroupId
	 *            The rowGroupId to set.
	 */
	public void setRowGroupId(int rowGroupId) {
		this.rowGroupId = rowGroupId;
	}

	/**
	 * @return Returns the rowId.
	 */
	public int getRowId() {
		return rowId;
	}

	/**
	 * @param rowId
	 *            The rowId to set.
	 */
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	/**
	 * @return Returns the seatCode.
	 */
	public String getSeatCode() {
		return seatCode;
	}

	/**
	 * @param seatCode
	 *            The seatCode to set.
	 */
	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	/**
	 * @return Returns the seatID.
	 */
	public int getSeatID() {
		return seatID;
	}

	/**
	 * @param seatID
	 *            The seatID to set.
	 */
	public void setSeatID(int seatID) {
		this.seatID = seatID;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the templateCode.
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateCode
	 *            The templateCode to set.
	 */
	public void setTemplateId(int templateCode) {
		this.templateId = templateCode;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return The seat message.
	 */
	public String getSeatMessage() {
		return seatMessage;
	}

	/**
	 * @param seatMessage
	 *            The seat message to set.
	 */
	public void setSeatMessage(String seatMessage) {
		this.seatMessage = seatMessage;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public boolean isEditedSeat() {
		return isEditedSeat;
	}

	public void setEditedSeat(boolean isEditedSeat) {
		this.isEditedSeat = isEditedSeat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getSocialIdentity() {
		if (socialIdentity == null) {
			socialIdentity = new HashMap<String, String>();
		}
		return socialIdentity;
	}

	public void setSocialIdentity(Map<String, String> socialIdentity) {
		this.socialIdentity = socialIdentity;
	}

	public void addToSocialIdentity(String siteType, String socialSiteCustId) {
		this.getSocialIdentity().put(siteType, socialSiteCustId);
	}

	public String getSeatVisibility() {
		return seatVisibility;
	}

	public void setSeatVisibility(String seatVisibility) {
		this.seatVisibility = seatVisibility;
	}

	public void setSeatLocationType(String seatLocationType) {
		this.seatLocationType = seatLocationType;
	}

	public String getSeatLocationType() {
		return seatLocationType;
	}

}
