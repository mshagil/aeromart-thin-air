package com.isa.thinair.airinventory.api.dto;

/**
 * Created by hasika on 7/4/18.
 */
public class ReProtectDTO {
	private String strLogicalCCCode;
	private String strTargetCabinClassCode;

	// preparing the target flight details
	private String strTargetFlightIds;
	private String strTargetSegCodes;
	private String strTargetFlightSegIds ;
	private String selectedPNRStr;

	// preparing the source flight details
	private String hdnSourceFlightDistributions;
	private String[] arrSourceFlightDistributions;

	private boolean blnEmailAlert;
	private boolean blnSmsAlert;
	private boolean blnSmsCnf;
	private boolean blnSmsOnH;
	private boolean blnEmailAlertOriginAgent;
	private boolean blnEmailAlertOwnerAgent;
	private boolean isPnrListAdded = false;

	public String getStrLogicalCCCode() {
		return strLogicalCCCode;
	}

	public void setStrLogicalCCCode(String strLogicalCCCode) {
		this.strLogicalCCCode = strLogicalCCCode;
	}

	public String getStrTargetCabinClassCode() {
		return strTargetCabinClassCode;
	}

	public void setStrTargetCabinClassCode(String strTargetCabinClassCode) {
		this.strTargetCabinClassCode = strTargetCabinClassCode;
	}

	public String getStrTargetFlightIds() {
		return strTargetFlightIds;
	}

	public void setStrTargetFlightIds(String strTargetFlightIds) {
		this.strTargetFlightIds = strTargetFlightIds;
	}

	public String getStrTargetSegCodes() {
		return strTargetSegCodes;
	}

	public void setStrTargetSegCodes(String strTargetSegCodes) {
		this.strTargetSegCodes = strTargetSegCodes;
	}

	public String getStrTargetFlightSegIds() {
		return strTargetFlightSegIds;
	}

	public void setStrTargetFlightSegIds(String strTargetFlightSegIds) {
		this.strTargetFlightSegIds = strTargetFlightSegIds;
	}

	public String getSelectedPNRStr() {
		return selectedPNRStr;
	}

	public void setSelectedPNRStr(String selectedPNRStr) {
		this.selectedPNRStr = selectedPNRStr;
	}

	public String getHdnSourceFlightDistributions() {
		return hdnSourceFlightDistributions;
	}

	public void setHdnSourceFlightDistributions(String hdnSourceFlightDistributions) {
		this.hdnSourceFlightDistributions = hdnSourceFlightDistributions;
	}

	public String[] getArrSourceFlightDistributions() {
		return arrSourceFlightDistributions;
	}

	public void setArrSourceFlightDistributions(String[] arrSourceFlightDistributions) {
		this.arrSourceFlightDistributions = arrSourceFlightDistributions;
	}

	public boolean isBlnEmailAlert() {
		return blnEmailAlert;
	}

	public void setBlnEmailAlert(boolean blnEmailAlert) {
		this.blnEmailAlert = blnEmailAlert;
	}

	public boolean isBlnSmsAlert() {
		return blnSmsAlert;
	}

	public void setBlnSmsAlert(boolean blnSmsAlert) {
		this.blnSmsAlert = blnSmsAlert;
	}

	public boolean isBlnSmsCnf() {
		return blnSmsCnf;
	}

	public void setBlnSmsCnf(boolean blnSmsCnf) {
		this.blnSmsCnf = blnSmsCnf;
	}

	public boolean isBlnSmsOnH() {
		return blnSmsOnH;
	}

	public void setBlnSmsOnH(boolean blnSmsOnH) {
		this.blnSmsOnH = blnSmsOnH;
	}

	public boolean isBlnEmailAlertOriginAgent() {
		return blnEmailAlertOriginAgent;
	}

	public void setBlnEmailAlertOriginAgent(boolean blnEmailAlertOriginAgent) {
		this.blnEmailAlertOriginAgent = blnEmailAlertOriginAgent;
	}

	public boolean isBlnEmailAlertOwnerAgent() {
		return blnEmailAlertOwnerAgent;
	}

	public void setBlnEmailAlertOwnerAgent(boolean blnEmailAlertOwnerAgent) {
		this.blnEmailAlertOwnerAgent = blnEmailAlertOwnerAgent;
	}

	public boolean isPnrListAdded() {
		return isPnrListAdded;
	}

	public void setPnrListAdded(boolean isPnrListAdded) {
		this.isPnrListAdded = isPnrListAdded;
	}
}
