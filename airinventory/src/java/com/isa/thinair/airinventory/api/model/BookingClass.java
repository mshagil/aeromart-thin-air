/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:11:58
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * The Model class for manipulating T_BOOKING_CLASS table.
 * 
 * <p>
 * Booking classes hold seat allocations against flight segments. The availability of these seats is controlled by the
 * fare rules linked to the booking class via the fare. Booking class also determines the charges applicable based on
 * Charge Groups associated.
 * </p>
 * 
 * <pre>
 * +--------------+ 1   * +--------------+ *   1 +--------------+
 * | BookingClass |-------|     Fare     |-------|   FareRule   |
 * +--------------+       +--------------+       +--------------+
 *         |*
 *         |*
 * +--------------+ 1   * +--------------+
 * | ChargeGroup  |-------|    Charge    |
 * +--------------+       +--------------+
 * </pre>
 * 
 * @author Thejaka
 * @author Nasly
 * 
 * @hibernate.class table = "T_BOOKING_CLASS"
 */
public class BookingClass extends Tracking {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9103317515356962016L;

	public static final String STANDARD_CODE_Y = "Y";

	public static final String STANDARD_CODE_N = "N";

	public static final String FIXED_FLAG_Y = "Y";

	public static final String FIXED_FLAG_N = "N";

	public static final String ON_HOLD_Y = "Y";

	public static final String ON_HOLD_N = "N";

	public static final String RELEASE_FLAG_Y = "Y";

	public static final String RELEASE_FLAG_N = "N";

	public static final int BC_TYPE_ID_OPENRT = 3;

	/** Eligible booking class types */
	public static interface Status {
		/** Active booking class */
		public static final String ACTIVE = "ACT";

		/** Inactive booking class */
		public static final String INACTIVE = "INA";
	}

	public static interface ReleaseFlag {
		/** Active booking class */
		public static final String ACTIVE = "Y";

		/** Inactive booking class */
		public static final String INACTIVE = "N";
	}

	/** Denotes the type of passenger booking class is eligle to carry */
	public static interface PassengerType {
		/** Adult passenger */
		public static final String ADULT = "ADULT";

		/** Adult passenger */
		public static final String INTERLINE = "INTERLINE";

		/** Goshow passenger */
		public static final String GOSHOW = "GOSHOW";
	}

	/** Denotes the type of resrvation booking class is eligible for */
	public static interface BookingClassType {
		/** Booking class eligible for standby reservation */
		public static final String STANDBY = "STANDBY";

		/** Booking class eligible for normal reservation */
		public static final String NORMAL = "NORMAL";

		/** Booking class eligible for standby reservation */
		public static final String OVERBOOK = "OVERBOOK";

		/** Booking class eligible for open ended return reservation */
		public static final String OPEN_RETURN = "OPENRT";

		public static final String INTERLINE_BLOCKED_SELL = "INTERLINE";

		public static final String INTERLINE_FREESELL = "INTLNFS";
	}

	/** Denotes how the seat allocations are shared among segments */
	public static interface AllocationType {
		/** Allocation spans over one segment only */
		public static final String SEGMENT = "SEG";

		/** Allocation spans over mutiple connected segments */
		public static final String CONNECTION = "CON";

		/** Allocation spans over outbound/inbound segment pairs */
		public static final String RETURN = "RET";

		/** Allocation spans over two or more of above types */
		public static final String COMBINED = "COM";
	}

	private String bookingCode;

	private String ccCode;

	private String logicalCCCode;

	private String bookingCodeDescription;

	private boolean standardCode;

	private boolean fixedFlag;

	private Integer nestRank;

	private String remarks;

	private String paxType;

	private String status;

	private Set<String> applicapableCharges;

	private String bcType;

	private Integer bcTypeId;

	private String allocationType;

	private String paxCategoryCode;

	private String fareCategoryCode;

	private Integer releaseCutover;

	private String releaseFlag;

	private boolean onhold;

	private Set<GDSBookingClass> gdsBCs;

	private Integer groupId;

	private String chargeFlags;

	public BookingClass() {
	}

	public BookingClass(String bookingCode, String ccCode, String fixedFlag, String bcType, String onhold, String status,
			String logicalCCCode) {
		this.bookingCode = bookingCode;
		this.ccCode = ccCode;
		if (fixedFlag.equals(FIXED_FLAG_Y))
			this.fixedFlag = true;
		else
			this.fixedFlag = false;
		if (onhold != null && onhold.equals(ON_HOLD_Y))
			this.onhold = true;
		else
			this.onhold = false;
		this.bcType = bcType;
		this.status = status;
		this.logicalCCCode = logicalCCCode;
	}

	public BookingClass(String bookingCode, String description, String ccCode, String fixedFlag, String bcType, Integer bcTypeId,
			String onhold, String status, String logicalCCCode, String paxType, String allocationType, String fareCategoryCode,
			String releaseFlag, Integer groupId) {
		this.bookingCode = bookingCode;
		this.bookingCodeDescription = description;
		this.ccCode = ccCode;
		if (fixedFlag.equals(FIXED_FLAG_Y))
			this.fixedFlag = true;
		else
			this.fixedFlag = false;
		if (onhold != null && onhold.equals(ON_HOLD_Y))
			this.onhold = true;
		else
			this.onhold = false;
		this.bcType = bcType;
		this.bcTypeId = bcTypeId;
		this.status = status;
		this.logicalCCCode = logicalCCCode;
		this.paxType = paxType;
		this.paxCategoryCode = "A";
		this.allocationType = allocationType;
		this.fareCategoryCode = fareCategoryCode;
		this.releaseFlag = releaseFlag;
		this.groupId = groupId;
	}

	/**
	 * returns the bookingCode
	 * 
	 * @return Returns the bookingCode.
	 * 
	 * @hibernate.id column = "BOOKING_CODE" generator-class = "assigned"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * sets the bookingCode
	 * 
	 * @param bookingCode
	 *            the bookingCode to set.
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * returns the bookingCodeDescription
	 * 
	 * @return Returns the bookingCodeDescription.
	 * 
	 * @hibernate.property column = "BOOKING_CODE_DESCRIPTION"
	 */
	public String getBookingCodeDescription() {
		return bookingCodeDescription;
	}

	/**
	 * sets the bookingCodeDescription
	 * 
	 * @param bookingCodeDescription
	 *            The bookingCodeDescription to set.
	 */
	public void setBookingCodeDescription(String bookingCodeDescription) {
		this.bookingCodeDescription = bookingCodeDescription;
	}

	/**
	 * returns the standardCode
	 * 
	 * @return Returns the standardCode.
	 * 
	 * @hibernate.property column = "STANDARD_CODE"
	 */
	@SuppressWarnings("unused")
	private String getStandardCodeChar() {
		if (standardCode)
			return STANDARD_CODE_Y;
		else
			return STANDARD_CODE_N;
	}

	/**
	 * sets the standardCode
	 * 
	 * @param standardCode
	 *            The standardCode to set.
	 */
	@SuppressWarnings("unused")
	private void setStandardCodeChar(String standardCode) {
		if (standardCode.equals(STANDARD_CODE_Y))
			this.standardCode = true;
		else
			this.standardCode = false;
	}

	public boolean getStandardCode() {
		return standardCode;
	}

	public void setStandardCode(boolean standardCode) {
		this.standardCode = standardCode;
	}

	/**
	 * returns the nestRank
	 * 
	 * @return Returns the nestRank.
	 * 
	 * @hibernate.property column = "NEST_RANK"
	 */
	public Integer getNestRank() {
		return nestRank;
	}

	/**
	 * sets the nestRank
	 * 
	 * @param nestRank
	 *            The nestRank to set.
	 */
	public void setNestRank(Integer nestRank) {
		this.nestRank = nestRank;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the Cabin Class Code
	 * 
	 * @return Returns the Cabin Class Code.
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return ccCode;
	}

	/**
	 * sets the Cabin Class Code
	 * 
	 * @param Cabin
	 *            Class Code The Cabin Class Code to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.ccCode = cabinClassCode;
	}

	/**
	 * returns the Logical Cabin Class Code
	 * 
	 * @return Returns the Logical Cabin Class Code.
	 * 
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * sets the Logical Cabin Class Code
	 * 
	 * @param Logical
	 *            Cabin Class Code The Logical Cabin Class Code to set.
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * returns the fixedFlag
	 * 
	 * @return Returns the fixedFlag.
	 * 
	 * @hibernate.property column = "FIXED_FLAG"
	 */
	@SuppressWarnings("unused")
	private String getFixedFlagChar() {
		if (fixedFlag)
			return FIXED_FLAG_Y;
		else
			return FIXED_FLAG_N;
	}

	/**
	 * sets the fixedFlag
	 * 
	 * @param fixedFlag
	 *            The fixedFlag to set.
	 */
	@SuppressWarnings("unused")
	private void setFixedFlagChar(String fixedFlag) {
		if (fixedFlag.equals(FIXED_FLAG_Y))
			this.fixedFlag = true;
		else
			this.fixedFlag = false;
	}

	public boolean getFixedFlag() {
		return fixedFlag;
	}

	public void setFixedFlag(boolean fixedFlag) {
		this.fixedFlag = fixedFlag;
	}

	/**
	 * returns the onhold
	 * 
	 * @return Returns the onhold.
	 * 
	 * @hibernate.property column = "ONHOLD_FLAG"
	 */
	@SuppressWarnings("unused")
	private String getOnHoldChar() {
		if (onhold)
			return ON_HOLD_Y;
		else
			return ON_HOLD_N;
	}

	/**
	 * sets the onhold
	 * 
	 * @param onhold
	 *            The onhold to set.
	 */
	@SuppressWarnings("unused")
	private void setOnHoldChar(String onhold) {
		if (onhold != null && onhold.equals(ON_HOLD_Y))
			this.onhold = true;
		else
			this.onhold = false;
	}

	public boolean getOnHold() {
		return onhold;
	}

	public void setOnHold(boolean onhold) {
		this.onhold = onhold;
	}

	/**
	 * returns the paxType
	 * 
	 * @return Returns the paxType.
	 * 
	 * @hibernate.property column = "PAX_TYPE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * sets the paxType
	 * 
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * 
	 * returns the booking code type
	 * 
	 * @hibernate.property column = "BC_TYPE"
	 */
	public String getBcType() {
		return bcType;
	}

	public void setBcType(String bcType) {
		this.bcType = bcType;
	}

	/**
	 * @return the bcTypeId
	 * 
	 * @hibernate.property column = "BC_TYPE_ID"
	 */
	public Integer getBcTypeId() {
		return bcTypeId;
	}

	/**
	 * @param bcTypeId
	 *            the bcTypeId to set
	 */
	public void setBcTypeId(Integer bcTypeId) {
		this.bcTypeId = bcTypeId;
	}

	/**
	 * returns the allocation type
	 * 
	 * @hibernate.property column = "ALLOCATION_TYPE"
	 */
	public String getAllocationType() {
		return allocationType;
	}

	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	/**
	 * @return pax category code
	 * @hibernate.property column = "PAX_CATEGORY_CODE"
	 */
	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	/**
	 * @hibernate.set table="T_BC_CHARGE_GROUP" cascade="all"
	 * @hibernate.collection-key column="BOOKING_CODE"
	 * @hibernate.collection-element type="string" column="CHARGE_GROUP_CODE"
	 */
	public Set<String> getApplicapableCharges() {
		return applicapableCharges;
	}

	public void setApplicapableCharges(Set<String> applicapableCharges) {
		this.applicapableCharges = applicapableCharges;
	}

	/**
	 * @return Returns the fareCategoryCode.
	 * @hibernate.property column = "fare_cat_id"
	 */
	public String getFareCategoryCode() {
		return fareCategoryCode;
	}

	/**
	 * @param fareCategoryCode
	 *            The fareCategoryCode to set.
	 */
	public void setFareCategoryCode(String fareCategoryCode) {
		this.fareCategoryCode = fareCategoryCode;
	}

	/**
	 * @return the releaseCutover
	 * @hibernate.property column = "release_cutover"
	 */
	public Integer getReleaseCutover() {
		return releaseCutover;
	}

	/**
	 * @param releaseCutover
	 *            the releaseCutover to set
	 */
	public void setReleaseCutover(Integer releaseCutover) {
		this.releaseCutover = releaseCutover;
	}

	/**
	 * @return the releaseFlag
	 * @hibernate.property column = "release_flag"
	 */
	public String getReleaseFlag() {
		return releaseFlag;
	}

	/**
	 * @param releaseFlag
	 *            the releaseFlag to set
	 */
	public void setReleaseFlag(String releaseFlag) {
		this.releaseFlag = releaseFlag;
	}

	/**
	 * @return the groupId
	 * @hibernate.property column = "GROUP_ID"
	 */
	public Integer getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId
	 *            the groupId to set
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	
	/**
	 * @return Returns the gdsBCs.
	 * @hibernate.set lazy="false" cascade="none" 
	 * @hibernate.collection-key column="BOOKING_CODE" 
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airinventory.api.model.GDSBookingClass"
	 * 
	 */
	public Set<GDSBookingClass> getGdsBCs() {
		return gdsBCs;
	}

	/**
	 * @param gdsBCs the gdsBCs to set
	 */
	public void setGdsBCs(Set<GDSBookingClass> gdsBCs) {
		this.gdsBCs = gdsBCs;
	}

	public void setChargeFlags(String chargeFlags) {
		this.chargeFlags = chargeFlags;
	}
	/**
	 * @return the charge flags
	 * @hibernate.property column = "CHARGE_FLAGS"
	 */
	public String getChargeFlags(){
		return chargeFlags;
	}
}
