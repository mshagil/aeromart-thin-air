package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Information filled by available flights search for seat availability search
 * 
 * @author MN
 */
public class FlightSegmentDTO implements Serializable, Comparable<FlightSegmentDTO>, IFlightSegment {

	private static final long serialVersionUID = 8372315957089678554L;

	private Integer flightId;

	private String segmentCode;

	private String fromAirport;

	private String toAirport;

	private Date departureDateTime;

	private Date arrivalDateTime;

	private Date departureDateTimeZulu;

	private Date arrivalDateTimeZulu;

	private String flightNumber;

	private Integer segmentId;

	private Integer operationTypeID;

	private String flightSegmentStatus;

	private String flightStatus;

	/** The departure terminal for the given flight segment */
	private String departureTerminal;

	/** The arrival terminal for the given flight segment */
	private String arrivalTerminal;

	private boolean isDomesticFlight;

	private String flightModelNumber;

	private String flightModelDescription;

	private String flightDuration;

	private String remarks;

	private String pnrSegmentStatus;

	private Integer flightScheduleId;

	private String interlineReturnGroupKey;

	private boolean isOpenReturnSegment;

	private String openRetConfirmBefore;

	private String operatingCarrier;

	// Total available infant seat on Flight CabinClass Segment (irrespective of channel)
	private final Map<String, Integer> availableInfantCount = new HashMap<String, Integer>();

	// Total available ADULT OR CHILD seat on Flight CabinClass Segment (irrespective of channel)
	private final Map<String, Integer> availableAdultCount = new HashMap<String, Integer>();

	private Date lastFareQuotedDate;

	private boolean waitListed;

	private boolean isOverBookEnabled = false;

	private boolean isFlightWithinCutoff = false;
	
	private String csOcCarrierCode;
	
	private String csOcFlightNumber;
	
	private List<CodeshareMcFlightDTO> csMcFlights;
	
	private Integer segSequence;

	public Integer getOperationTypeID() {
		return operationTypeID;
	}

	public void setOperationTypeID(Integer operationTypeID) {
		this.operationTypeID = operationTypeID;
	}

	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public String getToAirport() {
		return toAirport;
	}

	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public String getArrivalAirportCode() {
		if (getSegmentCode() != null) {
			String arr[] = getSegmentCode().split("/");
			return arr[arr.length - 1];
		}
		return null;
	}

	public String getDepartureAirportCode() {
		if (getSegmentCode() != null) {
			return getSegmentCode().split("/")[0];
		}
		return null;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * 
	 * @return Returns the carrier code
	 */
	public String getCarrierCode() {
		return this.flightNumber.substring(0, 2);
	}

	/**
	 * @return Returns the segmentId.
	 */
	public Integer getSegmentId() {
		return segmentId;
	}

	/**
	 * @param segmentId
	 *            The segmentId to set.
	 */
	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	public Date getDepatureDate() {
		if (depatureDate == null) {
			Calendar calSearchedDate = Calendar.getInstance();
			calSearchedDate.setTime(getDepartureDateTime());
			calSearchedDate.set(Calendar.HOUR, 0);
			calSearchedDate.set(Calendar.MINUTE, 0);
			calSearchedDate.set(Calendar.SECOND, 0);
			calSearchedDate.set(Calendar.MILLISECOND, 0);
			depatureDate = calSearchedDate.getTime();
		}
		return depatureDate;
	}

	private Date depatureDate = null;

	private Date ticketValidTill;

	private String flightType;

	/**
	 * Compare departure date time
	 * 
	 * @param flightSegmentDTO
	 * @return 1, 0, -1
	 */
	@Override
	public int compareTo(FlightSegmentDTO flightSegmentDTO) {
		int result = -2;// unknown
		if (departureDateTimeZulu != null && flightSegmentDTO.getDepartureDateTimeZulu() != null) {
			result = departureDateTimeZulu.compareTo(flightSegmentDTO.getDepartureDateTimeZulu());
		} else if (departureDateTime != null && flightSegmentDTO.getDepartureDateTime() != null) {
			result = departureDateTime.compareTo(flightSegmentDTO.getDepartureDateTime());
		}
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof FlightSegmentDTO)) {
			return false;
		}

		FlightSegmentDTO otherFlightSegmentDTO = (FlightSegmentDTO) o;
		return (getSegmentId() != null && getSegmentId().equals(otherFlightSegmentDTO.getSegmentId()));
	}

	@Override
	public int hashCode() {
		int hash = 1;
		hash = hash * 63 + (getSegmentId() != null ? getSegmentId().hashCode() : 0);
		return hash;
	}

	@Override
	public String toString() {
		StringBuffer summary = new StringBuffer();
		summary.append("\t").append("fromAirport	= " + getFromAirport()).append("\n");
		summary.append("\t").append("toAirport		= " + getToAirport()).append("\n");
		summary.append("\t").append("segCode 		= " + getSegmentCode()).append("\n");
		summary.append("\t").append("flightNumber 	= " + getFlightNumber()).append("\n");
		summary.append("\t").append("toAirport 		= " + getToAirport()).append("\n");
		summary.append("\t").append("getDepDate 	= " + getDepartureDateTime()).append("\n");
		summary.append("\t").append("getDepDateZulu 	= " + getDepartureDateTimeZulu()).append("\n");
		summary.append("\t").append("getArrDate 	= " + getArrivalDateTime()).append("\n");
		summary.append("\t").append("getArrDateZulu 	= " + getArrivalDateTimeZulu()).append("\n");
		summary.append("\t").append("flightId 		= " + getFlightId()).append("\n");
		summary.append("\t").append("segmentId 		= " + getSegmentId()).append("\n");
		return summary.toString();
	}

	/**
	 * @return the flightSegmentStatus
	 */
	public String getFlightSegmentStatus() {
		return flightSegmentStatus;
	}

	/**
	 * @param flightSegmentStatus
	 *            the flightSegmentStatus to set
	 */
	public void setFlightSegmentStatus(String flightSegmentStatus) {
		this.flightSegmentStatus = flightSegmentStatus;
	}

	/**
	 * @return the flightStatus
	 */
	public String getFlightStatus() {
		return flightStatus;
	}

	/**
	 * @param flightStatus
	 *            the flightStatus to set
	 */
	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	/**
	 * @return The departure terminal.
	 */
	public String getDepartureTerminal() {
		return departureTerminal;
	}

	/**
	 * @param departureTerminal
	 *            The departure terminal to set.
	 */
	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}

	/**
	 * @return The arrival terminal.
	 */
	public String getArrivalTerminal() {
		return arrivalTerminal;
	}

	/**
	 * @param arrivalTerminal
	 *            : The arrival terminal to set.
	 */
	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}

	/**
	 * 
	 * @return The isDomesticFlight
	 */
	public boolean isDomesticFlight() {
		return isDomesticFlight;
	}

	/**
	 * @param isDomesticFlight
	 *            : The isDomesticFlight to set.
	 */
	public void setDomesticFlight(boolean isDomesticFlight) {
		this.isDomesticFlight = isDomesticFlight;
	}

	public String getFlightModelNumber() {
		return flightModelNumber;
	}

	public void setFlightModelNumber(String flightModelNumber) {
		this.flightModelNumber = flightModelNumber;
	}

	public String getFlightModelDescription() {
		return flightModelDescription;
	}

	public void setFlightModelDescription(String flightModelDescription) {
		this.flightModelDescription = flightModelDescription;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	// AARESAA-7685 - Not the ideal solution should remove pnrSegmentStatus later
	public String getPnrSegmentStatus() {
		return pnrSegmentStatus;
	}

	public void setPnrSegmentStatus(String pnrSegmentStatus) {
		this.pnrSegmentStatus = pnrSegmentStatus;
	}

	public int getTotalAvailableAdultCount() {
		int totalAvailableAdultCount = 0;
		for (Integer logicalCabinClassWiseAvailableAdultCount : availableAdultCount.values()) {
			if (!AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
				totalAvailableAdultCount = Math.max(totalAvailableAdultCount, logicalCabinClassWiseAvailableAdultCount);
			} else {
				totalAvailableAdultCount += logicalCabinClassWiseAvailableAdultCount;
			}
		}
		return totalAvailableAdultCount;
	}

	/**
	 * @param availableAdultConunt
	 *            The availableAdultConunt to set.
	 */
	public void setAvailableAdultConunt(String logicalCCtype, int availableAdultConunt) {
		this.availableAdultCount.put(logicalCCtype, availableAdultConunt);
	}

	public int getTotalAvailableInfantCount() {
		int totalAvailableInfantCount = 0;
		for (Integer logicalCabinClassWiseAvailableInfantCount : availableInfantCount.values()) {
			if (!AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
				totalAvailableInfantCount = Math.max(totalAvailableInfantCount, logicalCabinClassWiseAvailableInfantCount);
			} else {
				totalAvailableInfantCount += logicalCabinClassWiseAvailableInfantCount;
			}
		}
		return totalAvailableInfantCount;
	}

	/**
	 * @param availableInfantConunt
	 *            The availableInfantConunt to set.
	 */
	public void setAvailableInfantConunt(String logicalCCtype, int availableInfantConunt) {
		this.availableInfantCount.put(logicalCCtype, availableInfantConunt);
	}

	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public void setLastFareQuotedDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	public Date getTicketValidTill() {
		return ticketValidTill;
	}

	public Integer getFlightScheduleId() {
		return flightScheduleId;
	}

	public void setFlightScheduleId(Integer flightScheduleId) {
		this.flightScheduleId = flightScheduleId;
	}

	/**
	 * Even though interlineReturnGroupKey,isOpenReturnSegment and openRetConfirmBefore attributes are ideally not
	 * related to FlightSegmentDTO , had to insert them due to its current usage in
	 * webplatform/src/java/com/isa/thinair/webplatform/api/util/ReservationBeanUtil.java method :
	 * convertReservationSegmentsToFlightSegmentDTOs()
	 * 
	 */

	public String getInterlineReturnGroupKey() {
		return interlineReturnGroupKey;
	}

	public void setInterlineReturnGroupKey(String interlineReturnGroupKey) {
		this.interlineReturnGroupKey = interlineReturnGroupKey;
	}

	public boolean isOpenReturnSegment() {
		return isOpenReturnSegment;
	}

	public void setOpenReturnSegment(boolean isOpenReturnSegment) {
		this.isOpenReturnSegment = isOpenReturnSegment;
	}

	public String getOpenRetConfirmBefore() {
		return openRetConfirmBefore;
	}

	public void setOpenRetConfirmBefore(String openRetConfirmBefore) {
		this.openRetConfirmBefore = openRetConfirmBefore;
	}

	public Map<String, Integer> getAvailableInfantCount() {
		return availableInfantCount;
	}

	public Map<String, Integer> getAvailableAdultCount() {
		return availableAdultCount;
	}

	public boolean isWaitListed() {
		return waitListed;
	}

	public void setWaitListed(boolean waitListed) {
		this.waitListed = waitListed;
	}

	/**
	 * @return the flightType
	 */
	public String getFlightType() {
		return flightType;
	}

	/**
	 * @param flightType
	 *            the flightType to set
	 */
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public boolean isOverBookEnabled() {
		return isOverBookEnabled;
	}

	public void setOverBookEnabled(boolean isOverBookEnabled) {
		this.isOverBookEnabled = isOverBookEnabled;
	}

	public boolean isFlightWithinCutoff() {
		return isFlightWithinCutoff;
	}

	public void setFlightWithinCutoff(boolean isFlightWithinCutoff) {
		this.isFlightWithinCutoff = isFlightWithinCutoff;
	}

	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}

	public String getCsOcFlightNumber() {
		return csOcFlightNumber;
	}

	public void setCsOcFlightNumber(String csOcFlightNumber) {
		this.csOcFlightNumber = csOcFlightNumber;
	}

	public List<CodeshareMcFlightDTO> getCsMcFlights() {
		return csMcFlights;
	}

	public void setCsMcFlights(List<CodeshareMcFlightDTO> csMcFlights) {
		this.csMcFlights = csMcFlights;
	}

	public Integer getSegSequence() {
		return segSequence;
	}

	public void setSegSequence(Integer segSequence) {
		this.segSequence = segSequence;
	}
}
