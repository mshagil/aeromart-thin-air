package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CabinClassAvailabilityDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = -1430595144396593457L;
	private String flightNumber;
	private String origin;
	private String destination;
	private Date departureDateTimeStart;
	private Date departureDateTimeEnd;
	private String sequenceNumber;
	private String lineItemNumber;

	/** <CabinClassCode,Count> */
	private Map<String, String> cabinClassAvailability;

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the departureDateTimeStart
	 */
	public Date getDepartureDateTimeStart() {
		return departureDateTimeStart;
	}

	/**
	 * @param departureDateTimeStart
	 *            the departureDateTimeStart to set
	 */
	public void setDepartureDateTimeStart(Date departureDateTimeStart) {
		this.departureDateTimeStart = departureDateTimeStart;
	}

	/**
	 * @return the departureDateTimeEnd
	 */
	public Date getDepartureDateTimeEnd() {
		return departureDateTimeEnd;
	}

	/**
	 * @param departureDateTimeEnd
	 *            the departureDateTimeEnd to set
	 */
	public void setDepartureDateTimeEnd(Date departureDateTimeEnd) {
		this.departureDateTimeEnd = departureDateTimeEnd;
	}

	/**
	 * @return the sequenceNumber
	 */
	public String getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * @param sequenceNumber
	 *            the sequenceNumber to set
	 */
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	/**
	 * @return the lineItemNumber
	 */
	public String getLineItemNumber() {
		return lineItemNumber;
	}

	/**
	 * @param lineItemNumber
	 *            the lineItemNumber to set
	 */
	public void setLineItemNumber(String lineItemNumber) {
		this.lineItemNumber = lineItemNumber;
	}

	/**
	 * @return the cabinClassAvailability
	 */
	public Map<String, String> getCabinClassAvailability() {
		if (cabinClassAvailability == null) {
			cabinClassAvailability = new HashMap<String, String>();
		}

		return cabinClassAvailability;
	}

	/**
	 * @param cabinClassAvailability
	 *            the cabinClassAvailability to set
	 */
	public void setCabinClassAvailability(Map<String, String> cabinClassAvailability) {
		this.cabinClassAvailability = cabinClassAvailability;
	}
}