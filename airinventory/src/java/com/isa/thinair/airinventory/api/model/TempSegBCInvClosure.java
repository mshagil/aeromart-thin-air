package com.isa.thinair.airinventory.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * For keeping track of buckets get closed due segment bucket closure
 * 
 * @author nasly
 * @hibernate.class table = "T_TEMP_SEG_BC_CLOSURE"
 */
public class TempSegBCInvClosure extends Persistent {

	private static final long serialVersionUID = 1147706624637023546L;

	/** Primary key */
	private int id;

	/** Identifier of the TempSegBcAlloc record */
	private int tempAllocId;

	/** FCCSegBCInventory identifier */
	private int fccsbaId;

	/** Status change action prior to bucket clousure */
	private String previousStatusAction;

	/**
	 * @return Returns the id.
	 * @hibernate.id column = "TFCCSBA_CLS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_TEMP_SEG_BC_CLOSURE"
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return Returns the fccsbaId.
	 * @hibernate.property column = "FCCSBA_ID"
	 */
	public int getFccsbaId() {
		return fccsbaId;
	}

	/**
	 * @param fccsbaId
	 *            The fccsbaId to set.
	 */
	public void setFccsbaId(int fccsbaId) {
		this.fccsbaId = fccsbaId;
	}

	/**
	 * @return Returns the previousStatusAction.
	 * @hibernate.property column = "PREV_STATUS_CHG_ACTION"
	 */
	public String getPreviousStatusAction() {
		return previousStatusAction;
	}

	/**
	 * @param previousStatusAction
	 *            The previousStatusAction to set.
	 */
	public void setPreviousStatusAction(String previousStatusAction) {
		this.previousStatusAction = previousStatusAction;
	}

	/**
	 * @return Returns the tempAllocId.
	 * @hibernate.property column = "TFCCSBA_ID"
	 */
	public int getTempAllocId() {
		return tempAllocId;
	}

	/**
	 * @param tempAllocId
	 *            The tempAllocId to set.
	 */
	public void setTempAllocId(int tempAllocId) {
		this.tempAllocId = tempAllocId;
	}

}
