package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.commons.api.exception.ModuleException;

public class OpenReturnPeriodsTO implements Serializable {

	private static final long serialVersionUID = -2831613557887999444L;

	private Date confirmBefore;

	private Date travelExpiry;

	private String airportCode;

	/**
	 * @return the confirmBefore
	 */
	public Date getConfirmBefore() {
		return confirmBefore;
	}

	/**
	 * @param confirmBefore
	 *            the confirmBefore to set
	 */
	public void setConfirmBefore(Date confirmBefore) {
		this.confirmBefore = confirmBefore;
	}

	/**
	 * @return the travelExpiry
	 */
	public Date getTravelExpiry() {
		return travelExpiry;
	}

	/**
	 * @param travelExpiry
	 *            the travelExpiry to set
	 */
	public void setTravelExpiry(Date travelExpiry) {
		this.travelExpiry = travelExpiry;
	}

	private String convertAndFormatDateTime(Date date) throws ModuleException {
		if (date != null) {
			ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(AirInventoryModuleUtils.getAirportBD());
			Date convertedDate = helper.getLocalDateTime(getAirportCode(), date);
			SimpleDateFormat sdFmt = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			return sdFmt.format(convertedDate) + " " + getAirportCode();
		}

		return "";
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getConvertedConfirmBeforeDateForTheAirport() throws ModuleException {
		return convertAndFormatDateTime(getConfirmBefore());
	}

	public String getConvertedTravelExpiryForTheAirport() throws ModuleException {
		return convertAndFormatDateTime(getTravelExpiry());
	}
}
