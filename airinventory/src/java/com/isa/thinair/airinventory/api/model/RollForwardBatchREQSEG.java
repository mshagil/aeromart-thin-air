/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @hibernate.class table = "T_ROLL_FORWARD_BATCH_REQ_SEG"
 * 
 */
public class RollForwardBatchREQSEG extends Tracking implements Serializable{

	private static final long serialVersionUID = -299122086979533082L;

	private Integer batchSegId;

	private RollForwardBatchREQ rollForwardBatchREQ;

	private String segmentBatchData;
	
	private String detailAudit;

	private String flightNumber;

	private Date fromDate;

	private Date toDate;

	private String status;

	private Date processedDate;

	private String remarks;

	/**
	 * returns the batchSegId
	 * 
	 * @return Returns the batchSegId.
	 * 
	 * @hibernate.id column = "R_FWD_BATCH_SEG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_ROLL_FORWARD_BATCH_REQ_SEG"
	 * 
	 * 
	 */
	public Integer getBatchSegId() {
		return batchSegId;
	}

	public void setBatchSegId(Integer batchSegId) {
		this.batchSegId = batchSegId;
	}

	/**
	 * returns the Segment Batch Data
	 * 
	 * @return Returns the Segment Batch Data
	 * 
	 * @hibernate.property column = "SEGMENT_BATCH_DATA"
	 */
	public String getSegmentBatchData() {
		return segmentBatchData;
	}

	public void setSegmentBatchData(String segmentBatchData) {
		this.segmentBatchData = segmentBatchData;
	}
	/**
	 * returns the Audit Details
	 * 
	 * @return Returns the Audit Details
	 * 
	 * @hibernate.property column = "DETAIL_AUDIT"
	 */
	public String getDetailAudit() {
		return detailAudit;
	}

	public void setDetailAudit(String detailAudit) {
		this.detailAudit = detailAudit;
	}
	
	/**
	 * returns the FROM DATE
	 * 
	 * @return Returns the FROM DATE
	 * 
	 * @hibernate.property column = "FROM_DATE"
	 */
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * returns the TO DATE
	 * 
	 * @return Returns the TO DATE
	 * 
	 * @hibernate.property column = "TO_DATE"
	 */
	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * returns the Flight Number
	 * 
	 * @return Returns the Flight Number
	 * 
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the RollForwardBatchREQ
	 * @hibernate.many-to-one cascade="all" column="R_FWD_BATCH_ID"
	 *                        class="com.isa.thinair.airinventory.api.model.RollForwardBatchREQ"
	 * 
	 */
	public RollForwardBatchREQ getRollForwardBatchREQ() {
		return rollForwardBatchREQ;
	}

	public void setRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) {
		this.rollForwardBatchREQ = rollForwardBatchREQ;
	}

	/**
	 * returns the Status
	 * 
	 * @return Returns the Status
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the PROCESSED DATE
	 * 
	 * @return Returns the PROCESSED DATE
	 * 
	 * @hibernate.property column = "PROCESSED_DATE"
	 */
	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	/**
	 * returns the REMARKS
	 * 
	 * @return Returns the REMARKS
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getBatchSegId()).toHashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof RollForwardBatchREQSEG) {
			RollForwardBatchREQSEG obj = (RollForwardBatchREQSEG) o;
			if (obj.getBatchSegId() != null && this.getBatchSegId() != null) {

				if (obj.getBatchSegId().intValue() == this.getBatchSegId().intValue()) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
