/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:42
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Represents Flight CabinClass Segment Inventory Model.
 * 
 * @author : Thejaka
 * @author : MN
 * 
 * @hibernate.class table = "T_FCC_SEG_ALLOC"
 */
public class FCCSegInventory extends Persistent {

	private static final long serialVersionUID = -3454037151906670205L;

	public static final String VALID_FLAG_Y = "Y";

	public static final String VALID_FLAG_N = "N";

	private int fccsInvId;

	private Integer fccInvId;

	/* flightId, segmentCode and ccCode combination uniquely identifies a FCCSegmentInventory */
	private int flightId;

	private String segmentCode;

	private int flightSegId;

	private String logicalCCCode;

	private int allocatedSeats;

	private int oversellSeats;

	private int curtailedSeats;

	/* sum of sold seats on child FCCSegmentBookingClassInventories */
	private int seatsSold;

	/* sum of onhold seats on child FCCSegmentBookingClassInventories */
	private int onHoldSeats;

	/* sum of fixed quota seats on child FCCSegmentBookingClassInventories */
	private int fixedSeats;

	/*
	 * availabe seats is calculated considering allocated, oversell, curtailed, sold and onhold on the segment and sold,
	 * onhold, fixed on the intercepting segments
	 */
	private int availableSeats;

	private int infantAllocation;

	private int soldInfantSeats;

	private int onholdInfantSeats;

	private int allocatedWaitListSeats;

	private int waitListedSeats;

	/*
	 * availabe infant seats is calculated considering infant allocated, infant sold and infant onhold on the segment
	 * and infant sold, infant onhold on the intercepting segments
	 */
	private int availableInfantSeats;

	private Integer overlappingFlightId;

	private String validFlagChar;

	@SuppressWarnings("unused")
	private int childAllocation;

	@SuppressWarnings("unused")
	private int soldChildSeats;

	@SuppressWarnings("unused")
	private int onholdChildSeats;

	@SuppressWarnings("unused")
	private int avilableChildSeats;

	/**
	 * returns the fccsInvId
	 * 
	 * @return Returns the fccsInvId.
	 * @hibernate.id column = "FCCSA_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FCC_SEG_ALLOC_FCCSA_ID"
	 */
	public int getFccsInvId() {
		return fccsInvId;
	}

	/**
	 * sets the fccsInvId
	 * 
	 * @param fccsInvId
	 *            The fccsInvId to set.
	 */
	public void setFccsInvId(int fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	/**
	 * returns the fccInvId
	 * 
	 * @return Returns the fccInvId.
	 * 
	 * @hibernate.property column = "FCCA_ID"
	 */
	public Integer getFccInvId() {
		return fccInvId;
	}

	/**
	 * sets the fccInvId
	 * 
	 * @param fccInvId
	 *            The fccInvId to set.
	 */
	public void setFccInvId(Integer fccInvId) {
		this.fccInvId = fccInvId;
	}

	/**
	 * returns the logicalCCCode
	 * 
	 * @return Returns the logicalCCCode.
	 * 
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * sets the logicalCCCode
	 * 
	 * @param logicalCCCode
	 *            The Logical CabinClass Code.
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * returns the FlightId
	 * 
	 * @return Returns the FlightId.
	 * 
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * sets the FlightId
	 * 
	 * @param FlightId
	 *            The FlightId to set.
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	/**
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * returns the allocatedSeats
	 * 
	 * @return Returns the allocatedSeats.
	 * 
	 * @hibernate.property column = "ALLOCATED_SEATS"
	 */
	public int getAllocatedSeats() {
		return allocatedSeats;
	}

	/**
	 * sets the allocatedSeats
	 * 
	 * @param allocatedSeats
	 *            The allocatedSeats to set.
	 */
	public void setAllocatedSeats(int allocatedSeats) {
		this.allocatedSeats = allocatedSeats;
	}

	/**
	 * returns the oversellSeats
	 * 
	 * @return Returns the oversellSeats.
	 * 
	 * @hibernate.property column = "OVERSELL_SEATS"
	 */
	public int getOversellSeats() {
		return oversellSeats;
	}

	/**
	 * sets the oversellSeats
	 * 
	 * @param oversellSeats
	 *            The oversellSeats to set.
	 */
	public void setOversellSeats(int oversellSeats) {
		this.oversellSeats = oversellSeats;
	}

	/**
	 * returns the onHoldSeats
	 * 
	 * @return Returns the onHoldSeats.
	 * 
	 * @hibernate.property column = "ON_HOLD_SEATS"
	 */
	public int getOnHoldSeats() {
		return onHoldSeats;
	}

	/**
	 * sets the onHoldSeats
	 * 
	 * @param onHoldSeats
	 *            The onHoldSeats to set.
	 */
	public void setOnHoldSeats(int onHoldSeats) {
		this.onHoldSeats = onHoldSeats;
	}

	/**
	 * returns the seatsSold
	 * 
	 * @return Returns the seatsSold.
	 * 
	 * @hibernate.property column = "SOLD_SEATS"
	 */
	public int getSeatsSold() {
		return seatsSold;
	}

	/**
	 * sets the seatsSold
	 * 
	 * @param seatsSold
	 *            The seatsSold to set.
	 */
	public void setSeatsSold(int seatsSold) {
		this.seatsSold = seatsSold;
	}

	/**
	 * returns the segmentCode
	 * 
	 * @return Returns the segmentCode.
	 * 
	 * @hibernate.property column = "SEGMENT_CODE"
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * sets the segmentCode
	 * 
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * returns the curtailedSeats
	 * 
	 * @return Returns the curtailedSeats.
	 * 
	 * @hibernate.property column = "CURTAILED_SEATS"
	 */
	public int getCurtailedSeats() {
		return curtailedSeats;
	}

	/**
	 * sets the curtailedSeats
	 * 
	 * @param curtailedSeats
	 *            The curtailedSeats to set.
	 */
	public void setCurtailedSeats(int curtailedSeats) {
		this.curtailedSeats = curtailedSeats;
	}

	/**
	 * returns the availableSeats
	 * 
	 * @return Returns the availableSeats.
	 * 
	 * @hibernate.property column = "AVAILABLE_SEATS"
	 */
	public int getAvailableSeats() {
		return availableSeats;
	}

	/**
	 * sets the availableSeats
	 * 
	 * @param availableSeats
	 *            The availableSeats to set.
	 */
	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	/**
	 * returns the infantAllocation
	 * 
	 * @return Returns the infantAllocation.
	 * 
	 * @hibernate.property column = "INFANT_ALLOCATION"
	 */
	public int getInfantAllocation() {
		return infantAllocation;
	}

	/**
	 * sets the infantAllocation
	 * 
	 * @param infantAllocation
	 *            The infantAllocation to set.
	 */
	public void setInfantAllocation(int infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	/**
	 * returns the soldInfantSeats
	 * 
	 * @return Returns the soldInfantSeats.
	 * 
	 * @hibernate.property column = "SOLD_INFANT_SEATS"
	 */
	public int getSoldInfantSeats() {
		return soldInfantSeats;
	}

	/**
	 * sets the soldInfantSeats
	 * 
	 * @param soldInfantSeats
	 *            The soldInfantSeats to set.
	 */
	public void setSoldInfantSeats(int soldInfantSeats) {
		this.soldInfantSeats = soldInfantSeats;
	}

	/**
	 * returns the onholdInfantSeats
	 * 
	 * @return Returns the onholdInfantSeats.
	 * 
	 * @hibernate.property column = "ONHOLD_INFANT_SEATS"
	 */
	public int getOnholdInfantSeats() {
		return onholdInfantSeats;
	}

	/**
	 * sets the onholdInfantSeats
	 * 
	 * @param onholdInfantSeats
	 *            The onholdInfantSeats to set.
	 */
	public void setOnholdInfantSeats(int onholdInfantSeats) {
		this.onholdInfantSeats = onholdInfantSeats;
	}

	/**
	 * @hibernate.property column = "FIXED_SEATS"
	 */
	public int getFixedSeats() {
		return fixedSeats;
	}

	public void setFixedSeats(int fixedSeats) {
		this.fixedSeats = fixedSeats;
	}

	/**
	 * @hibernate.property column = "OVERLAPPING_FLIGHT_ID"
	 */
	public Integer getOverlappingFlightId() {
		return overlappingFlightId;
	}

	public void setOverlappingFlightId(Integer overlappingFlightId) {
		this.overlappingFlightId = overlappingFlightId;
	}

	public boolean getValidFlag() {
		return (getValidFlagChar().equals(VALID_FLAG_Y)) ? true : false;
	}

	public void setValidFlag(boolean validFlag) {
		if (validFlag)
			setValidFlagChar(VALID_FLAG_Y);
		else
			setValidFlagChar(VALID_FLAG_N);
	}

	/**
	 * @hibernate.property column = "VALID_FLAG"
	 */
	private String getValidFlagChar() {
		return validFlagChar;
	}

	private void setValidFlagChar(String validFlagStr) {
		this.validFlagChar = validFlagStr;
	}

	/**
	 * @hibernate.property column = "AVAILABLE_INFANT_SEATS"
	 */
	public int getAvailableInfantSeats() {
		return availableInfantSeats;
	}

	public void setAvailableInfantSeats(int availableInfantSeats) {
		this.availableInfantSeats = availableInfantSeats;
	}

	public int getSeatAvailabilityOnSegOnly() {
		return allocatedSeats + oversellSeats - curtailedSeats - seatsSold - onHoldSeats;
	}

	public int getInfantSeatAvailabilityOnSegOnly() {
		return infantAllocation - soldInfantSeats - onholdInfantSeats;
	}

	public int getSoldAndOnholdAdultSeats() {
		return seatsSold + onHoldSeats;
	}

	public int getSoldAndOnholdInfantSeats() {
		return soldInfantSeats + onholdInfantSeats;
	}

	/**
	 * @return Integer waitListedSeats
	 * @hibernate.property column = "ALLOCATED_WAITLIST_SEATS"
	 */
	public int getAllocatedWaitListSeats() {
		return allocatedWaitListSeats;
	}

	public void setAllocatedWaitListSeats(Integer allocatedWaitListSeats) {
		this.allocatedWaitListSeats = allocatedWaitListSeats;
	}

	/**
	 * @return Integer waitListedSeats
	 * @hibernate.property column = "WAITLISTED_SEATS"
	 */
	public int getWaitListedSeats() {
		return waitListedSeats;
	}

	public void setWaitListedSeats(Integer waitListedSeats) {
		this.waitListedSeats = waitListedSeats;
	}

}
