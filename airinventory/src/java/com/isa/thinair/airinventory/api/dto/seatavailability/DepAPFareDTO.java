package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class DepAPFareDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double adultFare;
	private double childFare;
	private double infantFare;

	private double getDefaultPrecisionFare(double fare) {
		return (new Double(AccelAeroCalculator.parseBigDecimal(fare).toString())).doubleValue();
	}

	public double getAdultFare() {
		return adultFare;
	}

	public void setAdultFare(double adultFare) {
		this.adultFare = getDefaultPrecisionFare(adultFare);
	}

	public double getChildFare() {
		return childFare;
	}

	public void setChildFare(double childFare) {
		this.childFare = getDefaultPrecisionFare(childFare);
	}

	public double getInfantFare() {
		return infantFare;
	}

	public void setInfantFare(double infantFare) {
		this.infantFare = getDefaultPrecisionFare(infantFare);
	}

	@Override
	public String toString() {
		return "DepAPFareDTO [adultFare=" + adultFare + ", childFare=" + childFare + ", infantFare=" + infantFare + "]";
	}

}
