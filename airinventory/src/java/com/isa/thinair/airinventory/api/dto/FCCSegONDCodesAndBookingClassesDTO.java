package com.isa.thinair.airinventory.api.dto;

/**
 * DTO to retrieve ONDCodes ,bookingclasses and return_flag of passengers the
 * flight segment
 * 
 * @author : Isuru Samaraweera
 */
public final class FCCSegONDCodesAndBookingClassesDTO {
	/** Booking Code of reservation pax */
	private String bookingCode;
	/** Return flag of the fare rule for the reservation pax */
	private boolean returnFlag;
	/** Origin Destination Code for reservation pax fare */
	private String ondCode;

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(final String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(final boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(final String ondCode) {
		this.ondCode = ondCode;
	}
}
