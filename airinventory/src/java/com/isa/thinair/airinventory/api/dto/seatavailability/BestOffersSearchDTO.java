package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class BestOffersSearchDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = 3146920691617943617L;

	private String countryCode;

	private boolean summaryFlag;

	private String originAirport;

	private String destinationAirport;

	private Integer monthOfTravel;

	private Date fromDate;

	private Date toDate;

	private Integer noOfMonths;

	private boolean totalQuoteFlag;
	
	private String channelCode;

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public boolean isSummaryFlag() {
		return this.summaryFlag;
	}

	public void setSummaryFlag(boolean summaryFlag) {
		this.summaryFlag = summaryFlag;
	}

	public String getOriginAirport() {
		return this.originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public String getDestinationAirport() {
		return this.destinationAirport;
	}

	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public Integer getMonthOfTravel() {
		return this.monthOfTravel;
	}

	public void setMonthOfTravel(Integer monthOfTravel) {
		this.monthOfTravel = monthOfTravel;
	}

	public Date getDateFrom() {
		if (this.fromDate == null)
			setDateFrom();
		return this.fromDate;
	}

	public void setDateFrom() {
		Calendar currCal = Calendar.getInstance();
		Calendar cal = Calendar.getInstance();
		if (this.monthOfTravel != null && this.monthOfTravel > 0) {

			if (cal.get(Calendar.MONTH) > (this.monthOfTravel - 1)) {
				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
			}
			cal.set(Calendar.MONTH, this.monthOfTravel - 1);
			cal.set(Calendar.DATE, 1);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.AM_PM, Calendar.AM);
		}
		this.fromDate = new Timestamp(cal.getTimeInMillis());
		if (cal.getTimeInMillis() < currCal.getTimeInMillis())
			this.fromDate = new Timestamp(currCal.getTimeInMillis());
	}

	public void setDateFrom(Date fromDate) {
		this.fromDate = fromDate;
	}

	public void setDateTo(Date toDate) {
		this.toDate = toDate;
	}

	public Date getDateTo() {
		if (this.toDate == null)
			setDateTo();
		return this.toDate;
	}

	public void setDateTo() {
		Calendar currCal = Calendar.getInstance();
		Calendar cal = Calendar.getInstance();
		if (this.monthOfTravel != null || this.noOfMonths != null) {
			if (this.monthOfTravel != null && this.monthOfTravel > 0) {
				if (cal.get(Calendar.MONTH) > (this.monthOfTravel - 1)) {
					cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
				}
				cal.set(Calendar.MONTH, this.monthOfTravel - 1);
				cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
				cal.set(Calendar.HOUR, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				cal.set(Calendar.MILLISECOND, 0);
				cal.set(Calendar.AM_PM, Calendar.AM);
			} else
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + this.noOfMonths);
		}
		this.toDate = new Timestamp(cal.getTimeInMillis());
		if (cal.getTimeInMillis() < currCal.getTimeInMillis())
			this.fromDate = new Timestamp(currCal.getTimeInMillis());
	}

	public Integer getNoOfMonths() {
		return this.noOfMonths;
	}

	public void setNoOfMonths(Integer noOfMonths) {
		this.noOfMonths = noOfMonths;
	}

	public Object clone() {
		try {
			BestOffersSearchDTO cloned = (BestOffersSearchDTO) super.clone();
			cloned.countryCode = countryCode;
			cloned.summaryFlag = summaryFlag;
			cloned.originAirport = originAirport;
			cloned.destinationAirport = destinationAirport;
			cloned.monthOfTravel = monthOfTravel;
			cloned.fromDate = (Date) fromDate.clone();
			cloned.toDate = (Date) toDate.clone();
			cloned.noOfMonths = noOfMonths;
			return cloned;
		} catch (CloneNotSupportedException e) {
			System.out.println(e);
			return null;
		}
	}

	public boolean isTotalQuoteFlag() {
		return totalQuoteFlag;
	}

	public void setTotalQuoteFlag(boolean totalQuoteFlag) {
		this.totalQuoteFlag = totalQuoteFlag;
	}

	/**
	 * @return the channelCode
	 */
	public String getChannelCode() {
		return channelCode;
	}

	/**
	 * @param channelCode the channelCode to set
	 */
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	
	

}

