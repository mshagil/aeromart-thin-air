/**
 * 	mano
	May 9, 2011 
	2011
 */
package com.isa.thinair.airinventory.api.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author mano
 * 
 */
public interface BaggageBusinessDelegate {

	public static final String SERVICE_NAME = "BaggageService";

	/**
	 * Gets the map of Baggages collection for given Flight segs
	 * 
	 * @param flightSegIdWiseCos
	 * @param requoteSegExchanged
	 * @param skipCutoverValidation
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, List<FlightBaggageDTO>> getBaggages(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			boolean requoteSegExchanged, boolean skipCutoverValidation) throws ModuleException;

	/**
	 * Gets the map of Baggages collection for given Flight segs
	 * 
	 * @param isRequote
	 *            TODO
	 * @param flightSegIdWiseCos
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, List<FlightBaggageDTO>> getONDBaggages(List<FlightSegmentTO> flightSegmentTOs, boolean checkCutOver,
			boolean isRequote) throws ModuleException;

	/**
	 * Retrieves Baggage information from the Baggage charge template directly. No associated flights are necessary.
	 * 
	 * @param baggageTemplateID
	 *            Baggage charge template ID of the meals to be retrieved.
	 * @param selectedLanguage TODO
	 * @param cabinClassCode TODO
	 * @return The Baggages attached to the Baggage charge template. Or an empty collection if the baggageTemplateID is
	 *         null or invalid.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public List<FlightBaggageDTO> getBaggageFromTemplate(Integer baggageTemplateID, String selectedLanguage, String cabinClassCode) throws ModuleException;

	public Map<Integer, List<FlightBaggageDTO>> getBaggage(BaggageRq baggageRq) throws ModuleException;

	public Map<Integer, List<FlightBaggageDTO>> getBaggage(List<FlightSegmentTO> flightSegmentTOs, Integer baggageChargeId)
			throws ModuleException;

	/**
	 * Gets the map of Baggages collection for given Flight segs
	 * 
	 * @param flightSegIdWiseCos
	 * @param selectedLanguage
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, List<FlightBaggageDTO>> getBaggagesWithTranslations(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			String selectedLanguage) throws ModuleException;

	/**
	 * 
	 * @param flightBaggageIds
	 * @param flightAMSeatIdsToCancell
	 * @throws ModuleException
	 */
	public void modifyBaggage(Collection<Integer> flightBaggageIds, Collection<Integer> flightAMSeatIdsToCancell)
			throws ModuleException;

	/**
	 * 
	 * @param sourceFlightId
	 * @param destinationFlightIds
	 * @param lstSelectedSeg
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce assignFlightBaggageChargesRollFoward(int sourceFlightId, Collection<Integer> destinationFlightIds,
			ArrayList<String> lstSelectedSeg, String classOfService) throws ModuleException;

	public ServiceResponce assignFlightBaggageCharges(Collection<FlightBaggageDTO> flightBaggageDTOs, Integer chargeId,
			boolean isFromEdit, String classOfService) throws ModuleException;

	public Collection<FlightBaggageDTO> getBaggageTempleateIds(int flightId) throws ModuleException;

	/**
	 * 
	 * @param flightBaggageIds
	 * @throws ModuleException
	 */
	public void vacateBaggages(Collection<Integer> flightBaggageIds) throws ModuleException;

	public Collection<FlightBaggageDTO> getAvailableBaggageDtos(int flightSegId, Collection<Integer> baggageIds)
			throws ModuleException;

	public Integer getNextBaggageOndGroupId();

	void deleteFlightBaggages(Collection<Integer> flightIds) throws ModuleException;

	public Collection<FlightBaggageDTO> getSoldBaggages(int flightSegID) throws ModuleException;

	public Map<Integer, BigDecimal> getTotalBaggageWeight(Map<Integer, Set<Integer>> interceptingFlightSegIds);

}
