package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Utility class assist with reservation journey related functionality
 * 
 * @author M.Rikaz
 * @since 14th Aug 2013
 */

public class JourneyDetectionUtil {

	public class Node {
		private String name;
		private HashSet<Edge> inEdges;
		private HashSet<Edge> outEdges;

		public Node(String name) {
			this.name = name;
			inEdges = new HashSet<Edge>();
			outEdges = new HashSet<Edge>();
		}

		public Node addEdge(Node node) {
			Edge e = new Edge(this, node);
			outEdges.add(e);
			node.inEdges.add(e);
			return this;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public class Edge {
		private Node from;
		private Node to;

		public Edge(Node from, Node to) {
			this.from = from;
			this.to = to;
		}

		@Override
		public boolean equals(Object obj) {
			Edge e = (Edge) obj;
			return e.from == from && e.to == to;
		}
	}

	/**
	 * function to identify whether any circular/round trip journey found in the complete ond list, only CNF ONDs should
	 * be passed in expected MAP format
	 * 
	 * @param allNodeSet
	 *            TODO
	 * */
	public boolean isCircularJourneyFound(Map<String, List<String>> ondVertexMap, Set<String> allNodeSet) {

		boolean cycle = false;
		if (ondVertexMap != null && ondVertexMap.size() > 0 && allNodeSet != null && allNodeSet.size() > 0) {
			List<Node> nodeList = new ArrayList<Node>();
			Map<String, Node> nodeMap = new HashMap<String, Node>();

			Iterator<String> allNodeItr = allNodeSet.iterator();
			while (allNodeItr.hasNext()) {
				String nodeStr = allNodeItr.next();
				if (nodeStr != null && !nodeStr.trim().equals("")) {
					nodeMap.put(nodeStr, new JourneyDetectionUtil().new Node(nodeStr));
				}
			}

			for (String airport : nodeMap.keySet()) {
				Node node = nodeMap.get(airport);

				if (ondVertexMap.get(airport) != null && ondVertexMap.get(airport).size() > 0) {
					ArrayList<String> lt = (ArrayList<String>) ondVertexMap.get(airport);
					Iterator<String> destListItr = lt.iterator();

					while (destListItr.hasNext()) {
						String destAirport = destListItr.next();
						Node destNode = nodeMap.get(destAirport);
						if (destNode != null) {
							node.addEdge(destNode);
						}

					}

					nodeList.add(node);
				}

			}

			Node[] allNodes = new Node[nodeList.size()];
			nodeList.toArray(allNodes);

			// L <- Empty list that will contain the sorted elements
			ArrayList<Node> L = new ArrayList<Node>();

			// S <- Set of all nodes with no incoming edges
			HashSet<Node> S = new HashSet<Node>();
			for (Node n : allNodes) {
				if (n.inEdges.size() == 0) {
					S.add(n);
				}
			}

			// while S is non-empty do
			while (!S.isEmpty()) {
				// remove a node n from S
				Node n = S.iterator().next();
				S.remove(n);

				// insert n into L
				L.add(n);

				// for each node m with an edge e from n to m do
				for (Iterator<Edge> it = n.outEdges.iterator(); it.hasNext();) {
					// remove edge e from the GraphW
					Edge e = it.next();
					Node m = e.to;
					it.remove();// Remove edge from n
					m.inEdges.remove(e);// Remove edge from m

					// if m has no other incoming edges then insert m into S
					if (m.inEdges.isEmpty()) {
						S.add(m);
					}
				}
			}
			// Check to see if all edges are removed

			for (Node n : allNodes) {
				if (!n.inEdges.isEmpty()) {
					cycle = true;
					break;
				}
			}
		}

		return cycle;
	}
}
