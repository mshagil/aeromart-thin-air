package com.isa.thinair.airinventory.api.dto;

public class FCCSegInventoryUpdateStatusDTO extends OperationStatusDTO {

	private static final long serialVersionUID = 111325687L;

	public static final String SEGMENT_INVENTORY_FAILURE = "segment";

	public static final String BC_INVENTORY_FAILURE = "bc";

	public static final String FIXED_BC_INVENTORY_FAILURE = "fixedbc";

	public static final String UNIDENTIFIED_FAILURE = "unidentified";

	private String failureLevel = "";

	private Integer fccsInvId;

	private Integer fccsbInvId;

	private String bookingCode;

	public FCCSegInventoryUpdateStatusDTO(int operationStatus, String msg, Exception exception, String failureLevel,
			Integer fccsInvId, Integer fccsbInvId) {
		super(operationStatus, msg, exception);
		setFailureLevel(failureLevel);
		setFccsInvId(fccsInvId);
		setFccsbInvId(fccsbInvId);
		setBookingCode(bookingCode);
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getFailureLevel() {
		return failureLevel;
	}

	public void setFailureLevel(String failureLevel) {
		this.failureLevel = failureLevel;
	}

	public Integer getFccsbInvId() {
		return fccsbInvId;
	}

	public void setFccsbInvId(Integer fccsbInvId) {
		this.fccsbInvId = fccsbInvId;
	}

	public Integer getFccsInvId() {
		return fccsInvId;
	}

	public void setFccsInvId(Integer fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		summary.append("-------------Segment inventory update status-----------------\n\r");
		summary.append("Operation status 	= " + (getStatus() == OPERATION_FAILURE ? "FAILED" : "COMPLETED") + "\n\r");
		summary.append("Message				= " + getMsg() + "\n\r");
		summary.append("Failure Level 		= " + getFailureLevel() + "\n\r");
		summary.append("SegId, BCInvId, BookingCode = [" + getFccsInvId() + "," + getFccsbInvId() + "," + getBookingCode() + "]");
		return summary;
	}

}
