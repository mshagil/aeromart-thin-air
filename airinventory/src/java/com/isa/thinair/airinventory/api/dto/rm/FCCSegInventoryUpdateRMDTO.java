package com.isa.thinair.airinventory.api.dto.rm;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;

public class FCCSegInventoryUpdateRMDTO extends FCCSegInventoryUpdateDTO implements Serializable {

	private static final long serialVersionUID = 4203717992654036319L;
	private Date depDateZulu;
	private String flightNumber;
	private boolean isPublish = false;
	private boolean oversellUpdate = false;
	private boolean curtailUpdate = false;

	/**
	 * @return the depDateZulu
	 */
	public Date getDepDateZulu() {
		return depDateZulu;
	}

	/**
	 * @param depDateZulu
	 *            the depDateZulu to set
	 */
	public void setDepDateZulu(Date depDateZulu) {
		this.depDateZulu = depDateZulu;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the isPublish
	 */
	public boolean isPublish() {
		return isPublish;
	}

	/**
	 * @param isPublish
	 *            the isPublish to set
	 */
	public void setPublish(boolean isPublish) {
		this.isPublish = isPublish;
	}

	public boolean isOversellUpdate() {
		return oversellUpdate;
	}

	public boolean isCurtailUpdate() {
		return curtailUpdate;
	}

	public void setOversellUpdate(boolean oversellUpdate) {
		this.oversellUpdate = oversellUpdate;
	}

	public void setCurtailUpdate(boolean curtailUpdate) {
		this.curtailUpdate = curtailUpdate;
	}

}
