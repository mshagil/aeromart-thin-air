package com.isa.thinair.airinventory.api.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.BatchStatus;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author Raghuraman Kumar
 * 
 * This is the thread class for executing the batch processing logic for advance rollforward
 *
 */
public class RollForwardThread implements Runnable {
	private static Log log = LogFactory.getLog(RollForwardThread.class);

	private RollForwardBatchREQSEG rollForwardBatchREQSEG;
	private InvRollForwardCriteriaDTO invRollForwardCriteriaDTO;
	
	public RollForwardThread(RollForwardBatchREQSEG rollForwardBatchREQSEG, InvRollForwardCriteriaDTO invRollForwardCriteriaDTO) {
		this.rollForwardBatchREQSEG = rollForwardBatchREQSEG;
		this.invRollForwardCriteriaDTO = invRollForwardCriteriaDTO;
	}

	@Override
	public void run() {

		log.info("Processing rollforward logic for batch SEG Id :: " + rollForwardBatchREQSEG.getBatchSegId());

		try {
			
			// Updating Segment status to IN_PROGRESS
			AirinventoryUtils.getRollForwardBatchREQSEGBD().updateRollForwardBatchREQSEGStatus(
					rollForwardBatchREQSEG.getBatchSegId(), rollForwardBatchREQSEG.getFlightNumber(), 
					rollForwardBatchREQSEG.getFromDate(), rollForwardBatchREQSEG.getToDate(), BatchStatus.IN_PROGRESS, null);

			//Roll Forwarding the flights and updating the batch status to Completed
			AirinventoryUtils.getRollForwardBatchREQSEGBD().rollForwardBatchSegment(invRollForwardCriteriaDTO, rollForwardBatchREQSEG);

		} catch (ModuleException moduleException) {
			try {
				if(!moduleException.getExceptionCode().equalsIgnoreCase("airinventory.arg.rollforward.terminated")){
					AirinventoryUtils.getRollForwardBatchREQSEGBD().updateRollForwardBatchREQSEGStatus(
							rollForwardBatchREQSEG.getBatchSegId(), rollForwardBatchREQSEG.getFlightNumber(), 
							rollForwardBatchREQSEG.getFromDate(), rollForwardBatchREQSEG.getToDate(), BatchStatus.ERROR, null);
					log.error("Exception while executing Segment "+rollForwardBatchREQSEG.getBatchSegId()+" "+moduleException);
				}else{
					log.error("Segment "+rollForwardBatchREQSEG.getBatchSegId()+" rollforward has been already terminated.");
				}
			} catch (Exception rollforwardException) {
				log.error("Exception while updating Segment "+rollForwardBatchREQSEG.getBatchSegId()+" to Error "+rollforwardException);
			}
		}catch (Exception exception) {
			try {
				AirinventoryUtils.getRollForwardBatchREQSEGBD().updateRollForwardBatchREQSEGStatus(
						rollForwardBatchREQSEG.getBatchSegId(), rollForwardBatchREQSEG.getFlightNumber(), 
						rollForwardBatchREQSEG.getFromDate(), rollForwardBatchREQSEG.getToDate(), BatchStatus.ERROR, null);
				log.error("Exception while executing Segment "+rollForwardBatchREQSEG.getBatchSegId()+" "+exception);
			} catch (Exception rollforwardException) {
				log.error("Exception while updating Segment "+rollForwardBatchREQSEG.getBatchSegId()+"  to Error "+exception);
			}
		}
		log.info("Rollforward processing completed for Batch SEG Id :: " + rollForwardBatchREQSEG.getBatchSegId());
		
	}

}
