package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.util.FareRulesUtil;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Represents single flight segment along with seat availability on the segment
 * 
 * @author Nasly
 */
public class AvailableFlightSegment extends AvailableIBOBFlightSegment {

	private static final long serialVersionUID = -1867844184794689885L;

	private boolean interlineSeatsAvailable = false;

	private boolean isInboundFlightSegment = false;

	/* Number of infant seats requested */
	private int numberOfInfantSeats = 0;

	private Integer requestedNoOfAdults;

	private Integer requestedNoOfChildren;

	private final Map<String, Collection<SeatDistribution>> seatsAvailability = new HashMap<String, Collection<SeatDistribution>>();

	private AvailableConnectedFlight availableConnectedFlight = null;

	private final Map<String, FilteredBucketsDTO> ondWiseFilteredBucketsDTOsForConnections = new HashMap<String, FilteredBucketsDTO>();

	private final Map<String, FilteredBucketsDTO> ondWiseFilteredBucketsDTOsForReturns = new HashMap<String, FilteredBucketsDTO>();

	private FilteredBucketsDTO filteredBucketsDTOForSegmentFares = null;

	private SeatAllocationDTO seatAllocationDTO = null;

	private Map<String, Double> totalSurchargeMap = null;

	public FlightSegmentDTO getFlightSegmentDTO() {
		return super.getFlightSegmentDTOs().get(0);
	}

	public Collection<SeatDistribution> getSeatsAvailability(String logicalCCtype) {
		return seatsAvailability.get(logicalCCtype);
	}

	public void setSeatsAvailability(String logicalCCtype, Collection<SeatDistribution> seatsAvailability) {
		if ((seatsAvailability != null) && (seatsAvailability.size() > 0)) {
			setSeatsAvailable(logicalCCtype, true);
		}
		this.seatsAvailability.put(logicalCCtype, seatsAvailability);
	}

	/**
	 * @return the interlineSeatsAvailable
	 */
	public boolean isInterlineSeatsAvailable() {
		return interlineSeatsAvailable;
	}

	/**
	 * @param interlineSeatsAvailable
	 *            the interlineSeatsAvailable to set
	 */
	public void setInterlineSeatsAvailable(boolean interlineSeatsAvailable) {
		this.interlineSeatsAvailable = interlineSeatsAvailable;
	}

	@Override
	public boolean isInboundFlightSegment() {
		return isInboundFlightSegment;
	}

	@Override
	public void setInboundFlightSegment(boolean isInboundFlightSegment) {
		this.isInboundFlightSegment = isInboundFlightSegment;
	}

	public int getNumberOfInfantSeats() {
		return numberOfInfantSeats;
	}

	// TODO need to set the number of infants
	public void setNumberOfInfantSeats(int numberOfInfantSeats) {
		this.numberOfInfantSeats = numberOfInfantSeats;
	}

	@Override
	public void setSelectedFlightDTO(SelectedFlightDTO selectedFlightDTO) {
		this.selectedFlightDTO = selectedFlightDTO;

	}

	// ---------------------------------------------------------------------------
	// utility methods for the caller
	// ---------------------------------------------------------------------------

	@Override
	public Double getFareAmount(String logicalCCtype, String paxType) {
		if (getFareType(logicalCCtype) != FareTypes.NO_FARE) {
			return this.getFare(logicalCCtype).getFareAmount(paxType);
		} else {
			// should not reach this reach line
			return null;
		}
	}

	@Override
	public List<Double> getTotalCharges(String logicalCCtype) {
		if (((getFareType(logicalCCtype) != null && getFareType(logicalCCtype) == FareTypes.PER_FLIGHT_FARE)
				|| (getFareType(logicalCCtype) != null && getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE))
				&& this.totalCharges.get(logicalCCtype) == null) {
			this.totalCharges.put(logicalCCtype, ChargeQuoteUtils.calculateTotalCharges(getEffectiveCharges(logicalCCtype)));
		}
		return this.totalCharges.get(logicalCCtype);
	}

	public double getTotalCharges(String logicalCCtype, String paxType) {
		List<Double> charges = getTotalCharges(logicalCCtype);
		if (PaxTypeTO.ADULT.equals(paxType)) {
			return charges.get(0);
		} else if (PaxTypeTO.CHILD.equals(paxType)) {
			return charges.get(1);
		} else if (PaxTypeTO.INFANT.equals(paxType)) {
			return charges.get(2);
		} else {
			throw new RuntimeException("Invalid pax type");
		}
	}

	public Collection<QuotedChargeDTO> getEffectiveCharges(String logicalCCtype) {
		if (super.geteffectiveCharges(logicalCCtype) == null && getFare(logicalCCtype) != null) {
			seteffectiveCharges(logicalCCtype, getEffectiveCharges(logicalCCtype, false));
		}
		return super.geteffectiveCharges(logicalCCtype);
	}

	public Collection<QuotedChargeDTO> getEffectiveCharges(String logicalCCtype, boolean excludeTotalSurcharge) {
		// pass total journey fare for charge calculation
		Map<String, Double> totalJourneyFare = null;
		Map<String, Double> totalSurcharge = null;
		if (this.getSelectedFlightDTO() != null && this.getSelectedFlightDTO().isSeatsAvailable()) {
			totalJourneyFare = this.getSelectedFlightDTO().getTotalFareMap();
		} else if (this.getAvailableConnectedFlight() != null
				&& this.getAvailableConnectedFlight().isSeatsAvailable(logicalCCtype)) {
			totalJourneyFare = this.getAvailableConnectedFlight().getTotalFareMap(logicalCCtype);
		} else {
			totalJourneyFare = this.getTotalFareMap(logicalCCtype);
		}
		if (!excludeTotalSurcharge) {
			if (this.getSelectedFlightDTO() != null) {
				totalSurcharge = this.getSelectedFlightDTO().getTotalSurcharge();
			} else if (this.getAvailableConnectedFlight() != null) {
				totalSurcharge = this.getAvailableConnectedFlight().getTotalSurcharge(logicalCCtype);
			} else {
				totalSurcharge = this.getTotalSurcharge(logicalCCtype);
			}
		}
		return ChargeQuoteUtils.getChargesWithEffectiveChargeValues(getCharges(logicalCCtype), this.getFare(logicalCCtype),
				getRequestedNoOfAdults(), getRequestedNoOfChildren(), totalJourneyFare, totalSurcharge);
	}

	@Override
	public Collection<OndFareDTO> getFareChargesSeatsSegmentsDTOs(String logicalCCtype) {
		if (getFare(logicalCCtype) != null
				&& (getFareType(logicalCCtype) == FareTypes.PER_FLIGHT_FARE
						|| getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE)
				&& super.getFareChargesSeatsSegmentsDTOs(logicalCCtype) == null) {
			setFareChargesSeatsSegmentsDTOs(logicalCCtype, new ArrayList<OndFareDTO>());
			OndFareDTO fareChargesSeatsSegmentsDTO = new OndFareDTO();
			FareSummaryDTO fareSummaryDTO = getFare(logicalCCtype).clone();
			injectFarePercentage(logicalCCtype, fareSummaryDTO);
			injectDepAirportCurrFares(fareSummaryDTO, getFareType(logicalCCtype));
			fareChargesSeatsSegmentsDTO.setFareSummaryDTO(fareSummaryDTO);
			fareChargesSeatsSegmentsDTO.setFareType(getFareType(logicalCCtype));
			fareChargesSeatsSegmentsDTO.setAllCharges(getEffectiveCharges(logicalCCtype));
			fareChargesSeatsSegmentsDTO.addSegment(getFlightSegmentDTO().getSegmentId(), getFlightSegmentDTO());
			fareChargesSeatsSegmentsDTO.setSegmentSeatDistsDTOs(
					AirinventoryUtils.getSearializableValues(getSegmentSeatDistributionMap(logicalCCtype)));
			fareChargesSeatsSegmentsDTO.setAllFlexiCharges(getEffectiveFlexiCharges(logicalCCtype));
			fareChargesSeatsSegmentsDTO.setInBoundOnd(isInboundFlightSegment());
			fareChargesSeatsSegmentsDTO.setOndSequence(this.getOndSequence());
			if (this.getRequestedOndSequence() != null) {
				fareChargesSeatsSegmentsDTO.setRequestedOndSequence(this.getRequestedOndSequence());
			} else {
				fareChargesSeatsSegmentsDTO.setRequestedOndSequence(this.getOndSequence());
			}
			fareChargesSeatsSegmentsDTO
					.setOpenReturn(this.getSelectedFlightDTO() != null ? this.getSelectedFlightDTO().isOpenReturn() : false);
			fareChargesSeatsSegmentsDTO.setSubJourneyGroup(this.getSubJourneyGroup());

			super.getFareChargesSeatsSegmentsDTOs(logicalCCtype).add(fareChargesSeatsSegmentsDTO);
		}
		return super.getFareChargesSeatsSegmentsDTOs(logicalCCtype);
	}

	@Override
	public LinkedHashMap<Integer, SegmentSeatDistsDTO> getSegmentSeatDistributionMap(String logicalCCtype) {
		if ((getFareType(logicalCCtype) == FareTypes.PER_FLIGHT_FARE || getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE)
				&& segmentSeatDistributionMap.get(logicalCCtype) == null) {
			segmentSeatDistributionMap = new HashMap<String, LinkedHashMap<Integer, SegmentSeatDistsDTO>>();
			segmentSeatDistributionMap.put(logicalCCtype, new LinkedHashMap<Integer, SegmentSeatDistsDTO>());
			SegmentSeatDistsDTO segmentSeatDistsDTO = new SegmentSeatDistsDTO();
			segmentSeatDistsDTO.setSeatDistributions(getSeatsAvailability(logicalCCtype));
			segmentSeatDistsDTO.setFlightId(getFlightSegmentDTO().getFlightId().intValue());
			segmentSeatDistsDTO.setFlightSegId(getFlightSegmentDTO().getSegmentId().intValue());
			segmentSeatDistsDTO.setSegmentCode(getFlightSegmentDTO().getSegmentCode());
			segmentSeatDistsDTO.setLogicalCabinClass(logicalCCtype);
			segmentSeatDistsDTO.setCabinClassCode(getCabinClassCode());
			segmentSeatDistsDTO.setNoOfInfantSeats(getNumberOfInfantSeats());
			segmentSeatDistsDTO.setFixedQuotaSeats(getFixedQuotaFlag(segmentSeatDistsDTO.getSeatDistribution()));
			segmentSeatDistributionMap.get(logicalCCtype).put(getFlightSegmentDTO().getSegmentId(), segmentSeatDistsDTO);
		}
		return this.segmentSeatDistributionMap.get(logicalCCtype);
	}

	SegmentSeatDistsDTO getSegmentSeatDistributionDTO(String logicalCCtype, Integer segmentId) {
		if (getSegmentSeatDistributionMap(logicalCCtype) != null) {
			return getSegmentSeatDistributionMap(logicalCCtype).get(segmentId);
		}
		return null;
	}

	private boolean getFixedQuotaFlag(Collection<SeatDistribution> seatDistributions) {
		for (SeatDistribution seatDistribution : seatDistributions) {
			if (seatDistribution.isFixedBC()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getOndCode() {
		return getFlightSegmentDTO().getSegmentCode();
	}

	@Override
	public AvailableFlightSegment getCleanedInstance() {
		AvailableFlightSegment availableFlightSegment = new AvailableFlightSegment();
		availableFlightSegment.getFlightSegmentDTOs().add(getFlightSegmentDTO());
		availableFlightSegment.setCabinClassCode(getCabinClassCode());
		availableFlightSegment.setInboundFlightSegment(isInboundFlightSegment());
		availableFlightSegment.setSeatAllocationDTO(getSeatAllocationDTO());
		availableFlightSegment.setOndSequence(getOndSequence());
		availableFlightSegment.setRequestedOndSequence(getRequestedOndSequence());
		availableFlightSegment.setSplitOperationForBusInvoked(isSplitOperationForBusInvoked());
		return availableFlightSegment;
	}

	@Override
	public void addCharge() {
		throw new UnsupportedOperationException("Adding charge operation is not implemented yet.");
	}

	/**
	 * @return the requestedNoOfAdults
	 */
	public Integer getRequestedNoOfAdults() {
		return requestedNoOfAdults;
	}

	/**
	 * @param requestedNoOfAdults
	 *            the requestedNoOfAdults to set
	 */
	public void setRequestedNoOfAdults(Integer requestedNoOfAdults) {
		this.requestedNoOfAdults = requestedNoOfAdults;
	}

	/**
	 * @return the requestedNoOfChildren
	 */
	public Integer getRequestedNoOfChildren() {
		return requestedNoOfChildren;
	}

	/**
	 * @param requestedNoOfChildren
	 *            the requestedNoOfChildren to set
	 */
	public void setRequestedNoOfChildren(Integer requestedNoOfChildren) {
		this.requestedNoOfChildren = requestedNoOfChildren;
	}

	@Override
	public Collection<FlexiRuleDTO> getEffectiveFlexiCharges(String logicalCCtype) {
		if (super.getEffectiveFlexiCharges(logicalCCtype) == null && getFare(logicalCCtype) != null) {
			setEffectiveFlexiCharges(logicalCCtype, ChargeQuoteUtils.getFlexiChargesWithEffectiveChargeValues(
					getFlexiCharges(logicalCCtype), this.getFare(logicalCCtype), this.getEffectiveCharges(logicalCCtype)));
		}
		return super.getEffectiveFlexiCharges(logicalCCtype);
	}

	@Override
	public Map<String, Double> getTotalFareMap(String logicalCCtype) {
		if (isSeatsAvailable(logicalCCtype) && super.getTotalFareMap(logicalCCtype) == null) {
			setTotalFareMap(logicalCCtype, new HashMap<String, Double>());
			if (getFareType(logicalCCtype) == FareTypes.PER_FLIGHT_FARE
					|| getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE) {
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.ADULT, getFare(logicalCCtype).getFareAmount(PaxTypeTO.ADULT));
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.CHILD, getFare(logicalCCtype).getFareAmount(PaxTypeTO.CHILD));
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.INFANT,
						getFare(logicalCCtype).getFareAmount(PaxTypeTO.INFANT));
			}
		}
		return super.getTotalFareMap(logicalCCtype);
	}

	@Override
	public Map<String, Double> getTotalSurcharge(String logicalCCtype) {
		if (totalSurchargeMap == null) {
			Collection<QuotedChargeDTO> currentEffChgs = getEffectiveCharges(logicalCCtype, true);
			totalSurchargeMap = ChargeQuoteUtils.getTotalSurcharges(currentEffChgs);
			setEffectiveFlexiCharges(logicalCCtype, null);
		}
		return totalSurchargeMap;
	}

	public AvailableConnectedFlight getAvailableConnectedFlight() {
		return this.availableConnectedFlight;
	}

	public void setAvailableConnectedFlight(AvailableConnectedFlight availableConnectedFlight) {
		this.availableConnectedFlight = availableConnectedFlight;
	}

	@Override
	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r";
		String t = "\t";
		summary.append(nl + "--------------------Available Flight Segment Summary---------------------" + nl);
		summary.append("Flight Segment Information " + nl);
		summary.append(t + "FlightId        = " + getFlightSegmentDTO().getFlightId() + nl);
		summary.append(t + "SegmentId       = " + getFlightSegmentDTO().getSegmentId() + nl);
		summary.append(t + "SegmentCode     = " + getFlightSegmentDTO().getSegmentCode() + nl);
		summary.append(t + "From Airport    = " + getFlightSegmentDTO().getFromAirport() + nl);
		summary.append(t + "To Airport      = " + getFlightSegmentDTO().getToAirport() + nl);
		summary.append(t + "Depaturre Date  = " + getFlightSegmentDTO().getDepartureDateTime() + nl);
		summary.append(t + "Arrival Date    = " + getFlightSegmentDTO().getArrivalDateTime() + nl + nl);
		summary.append("Seat Availability on flight the segment " + nl);
		summary.append(
				t + "Total Available Adult Seats for any channel  = " + getFlightSegmentDTO().getTotalAvailableAdultCount() + nl);
		summary.append(t + "Total Available Infant Seats for any channel = "
				+ getFlightSegmentDTO().getTotalAvailableInfantCount() + nl);

		for (String logicalCabinClass : getAvailableLogicalCabinClasses()) {
			if (isSeatsAvailable(logicalCabinClass)) {
				summary.append(t + "Seats Available on the flight segment for the channel" + nl);
				if (getSeatsAvailability(logicalCabinClass) != null) {
					summary.append(t + "Available Seats Distribution for logical cabin class " + logicalCabinClass + "	= ");
					for (SeatDistribution seatsDist : getSeatsAvailability(logicalCabinClass)) {
						summary.append("[" + seatsDist.getBookingClassCode() + "," + seatsDist.getNoOfSeats() + "]");
					}
					summary.append(nl);
				}
				if (getFare(logicalCabinClass) != null) {
					summary.append("Fare quoted on the flight segment " + nl);
					summary.append(t + "FareId	= " + getFare(logicalCabinClass).getFareId() + nl);
					summary.append(t + "Adult Fare Amount	= " + getFare(logicalCabinClass).getFareAmount(PaxTypeTO.ADULT) + nl);
					summary.append(t + "Child Fare Amount	= " + getFare(logicalCabinClass).getFareAmount(PaxTypeTO.CHILD) + nl);
					summary.append(
							t + "Infant Fare Amount	= " + getFare(logicalCabinClass).getFareAmount(PaxTypeTO.INFANT) + nl);

					if (getEffectiveCharges(logicalCabinClass) != null) {
						summary.append("Charges quoted on the flight segment " + nl);
						summary.append(t + "Total Adult Charges Amount	= " + getTotalCharges(logicalCabinClass).get(0) + nl);
						summary.append(t + "Total Infant Charges Amount	= " + getTotalCharges(logicalCabinClass).get(1) + nl);
						summary.append(t + "Total Child Charges Amount	= " + getTotalCharges(logicalCabinClass).get(2) + nl);

						summary.append("Charges break down \n\t[chargeCode,chargeRateId,chargeCatCode,chargeGroup,"
								+ "chargeValuePercentage,isInPercentage,effectiveChargeValue,appliesTo]" + nl);
						for (QuotedChargeDTO charge : getEffectiveCharges(logicalCabinClass)) {
							summary.append(t + "[" + charge.getChargeCode()).append(",");
							summary.append(charge.getChargeRateId()).append(",");
							summary.append(charge.getChargeCategoryCode()).append(",");
							summary.append(charge.getChargeGroupCode()).append(",");
							summary.append(charge.getChargeValuePercentage()).append(",");
							summary.append(charge.isChargeValueInPercentage()).append(",");
							summary.append(charge.getEffectiveChargeAmount(PaxTypeTO.ADULT)).append(",");
							summary.append(charge.getApplicableTo()).append("]" + nl);
						}
					} else {
						summary.append("No Charge quoted on the flight segment " + nl);
					}
				} else {
					summary.append("No Fare quoted on the flight segment	" + nl);
				}
			} else {
				summary.append(t + "Seats NOT Available on the flight segment for the channel	" + nl);
			}
		}
		summary.append("--------------------------------------------------------------------------" + nl);
		return summary;
	}

	public SeatAllocationDTO getSeatAllocationDTO() {
		return seatAllocationDTO;
	}

	public void setSeatAllocationDTO(SeatAllocationDTO seatAllocationDTO) {
		this.seatAllocationDTO = seatAllocationDTO;
	}

	@Override
	public boolean isDirectFlight() {
		// This is true always. This is a direct flight
		return true;
	}

	@Override
	public Date getDepartureDate() {
		return this.getFlightSegmentDTO().getDepartureDateTime();
	}

	@Override
	public Date getDepartureDateZulu() {
		return this.getFlightSegmentDTO().getDepartureDateTimeZulu();
	}

	@Override
	public Date getArrivalDate() {
		return this.getFlightSegmentDTO().getArrivalDateTime();
	}

	public void addOndWiseFilteredBucketsDTOsForConnections(String ondCode, FilteredBucketsDTO filteredBucketsDTO) {
		ondWiseFilteredBucketsDTOsForConnections.put(ondCode, filteredBucketsDTO);
	}

	public void addOndWiseFilteredBucketsDTOsForReturns(String ondCode, FilteredBucketsDTO filteredBucketsDTO) {
		ondWiseFilteredBucketsDTOsForReturns.put(ondCode, filteredBucketsDTO);
	}

	public FilteredBucketsDTO getOndFilteredBucketsDTOsForConnections(String ondCode) {
		return ondWiseFilteredBucketsDTOsForConnections.get(ondCode);
	}

	public FilteredBucketsDTO getOndFilteredBucketsDTOsForReturns(String ondCode) {
		return ondWiseFilteredBucketsDTOsForReturns.get(ondCode);
	}

	public FilteredBucketsDTO getFilteredBucketsDTOForSegmentFares() {
		return filteredBucketsDTOForSegmentFares;
	}

	public void setFilteredBucketsDTOForSegmentFares(FilteredBucketsDTO filteredBucketsDTOForSegmentFares) {
		this.filteredBucketsDTOForSegmentFares = filteredBucketsDTOForSegmentFares;
	}

	@Override
	public Set<String> getAvailableExternalLogicalCabinClasses() {
		if (getFilteredBucketsDTOForSegmentFares() != null) {
			return (Set<String>) getFilteredBucketsDTOForSegmentFares().getExternalBookingClassAvailability();
		}
		return new HashSet<String>();
	}

	/**
	 * Shallowly copy itself. Currently copies FlightSegmentDTOs, CalibnClassCode and OndSequence.
	 */
	@Override
	public AvailableIBOBFlightSegment getShallowCopy() {
		AvailableIBOBFlightSegment seg = new AvailableFlightSegment();
		seg.getFlightSegmentDTOs().addAll(this.getFlightSegmentDTOs());
		seg.setCabinClassCode(getCabinClassCode());
		seg.setOndSequence(getOndSequence());
		seg.setRequestedOndSequence(getRequestedOndSequence());
		return seg;
	}

	public boolean isBusSegment() {
		FlightSegmentDTO flightSegmentDTO = getFlightSegmentDTO();
		if (flightSegmentDTO.getOperationTypeID() != null
				&& flightSegmentDTO.getOperationTypeID() == AirScheduleCustomConstants.OperationTypes.BUS_SERVICE) {
			return true;
		}

		return false;
	}

	private void injectDepAirportCurrFares(FareSummaryDTO fareSummaryDTO, Integer fareType) {
		List<FareSummaryDTO> fareSumColl = new ArrayList<FareSummaryDTO>();
		if (fareSummaryDTO != null) {
			fareSumColl.add(fareSummaryDTO);
			try {
				FareRulesUtil.injectDepAirportCurrFares(fareSumColl);
			} catch (ModuleException e) {
			}
			if (fareType == FareTypes.RETURN_FARE || fareType == FareTypes.HALF_RETURN_FARE) {
				DepAPFareDTO depAPFareDTO = fareSumColl.get(0).getDepartureAPFare();
				if (depAPFareDTO != null) {
					DepAPFareDTO depAPFare = new DepAPFareDTO();

					double journeyFareAmount = AccelAeroCalculator.divide(new BigDecimal(depAPFareDTO.getAdultFare()), 2)
							.doubleValue();
					depAPFare.setAdultFare(journeyFareAmount);

					journeyFareAmount = AccelAeroCalculator.divide(new BigDecimal(depAPFareDTO.getChildFare()), 2).doubleValue();
					depAPFare.setChildFare(journeyFareAmount);

					journeyFareAmount = AccelAeroCalculator.divide(new BigDecimal(depAPFareDTO.getInfantFare()), 2).doubleValue();
					depAPFare.setInfantFare(journeyFareAmount);

					fareSumColl.get(0).setDepartureAPFare(depAPFare);
				}
			}
		}
	}

	public void cleanFares() {
		super.cleanFares();
		seatsAvailability.clear();
	}

	@Override
	public String getSelectedFareChargeRatio(String logicalCCtype) {
		FareSummaryDTO selectedFare = getFare(logicalCCtype);
		if (selectedFare != null && selectedFare.getRatioCategory() != null) {
			return selectedFare.getRatioCategory();
		}
		return null;
	}

	public FlightSegmentDTO getFirstFlightSegment() {
		return getFlightSegmentDTOs().iterator().next();
	}

	public FlightSegmentDTO getLastFlightSegment() {
		Object flightSegmentDTO = null;
		for (Iterator<FlightSegmentDTO> flightSegmentDTOIt = this.getFlightSegmentDTOs().iterator(); flightSegmentDTOIt
				.hasNext();) {
			flightSegmentDTO = flightSegmentDTOIt.next();
		}
		return (FlightSegmentDTO) flightSegmentDTO;
	}

	@Override
	public String getOriginAirport() {
		return getFirstFlightSegment().getDepartureAirportCode();
	}

	@Override
	public String getDestinationAirport() {
		return getLastFlightSegment().getArrivalAirportCode();
	}

	@Override
	public Collection<QuotedChargeDTO> getAllEffectiveCharges(String logicalCCType) {
		return super.geteffectiveCharges(logicalCCType);
	}
}
