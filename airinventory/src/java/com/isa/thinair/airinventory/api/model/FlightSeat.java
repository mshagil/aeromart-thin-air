/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:11:58
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @hibernate.class table = "SM_T_FLIGHT_AM_SEAT"
 * @author Byorn
 * 
 */
public class FlightSeat extends Tracking {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int flightAmSeatId;

	private Integer flightSegInventoryId;

	private Integer flightSegId;

	private Integer chargeId;

	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String status;

	private Date timestamp;

	private String paxType;

	private Integer seatId;

	private String logicalCCCode;

	private String seatCode;

	// Keeping this value when updating flight seats in split operation
	private boolean isUpdateToADifLogicalCC;

	/**
	 * 
	 * @return
	 * @hibernate.property column = "AM_SEAT_ID"
	 */
	public Integer getSeatId() {
		return seatId;
	}

	/**
	 * 
	 * @param seatId
	 */
	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "PAX_TYPE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * 
	 * @param paxType
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public FlightSeat() {
		this.setCreatedDate(new Date());
	}

	/**
	 * @return the flightAmSeat
	 * @hibernate.id column = "FLIGHT_AM_SEAT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "SM_S_FLIGHT_AM_SEAT"
	 * 
	 */
	public int getFlightAmSeatId() {
		return flightAmSeatId;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "CHARGE_AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @return the chargeId
	 * @hibernate.property column = "AMS_CHARGE_ID"
	 */
	public Integer getChargeId() {
		return chargeId;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param chargeId
	 *            the chargeId to set
	 */
	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param flightAmSeatId
	 *            the flightAmSeatId to set
	 */
	public void setFlightAmSeatId(int flightAmSeatId) {
		this.flightAmSeatId = flightAmSeatId;
	}

	/**
	 * 
	 * 
	 * @hibernate.property column = "flt_seg_id"
	 * 
	 * @return the flightSegId
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightSegId
	 *            the flightSegId to set
	 */
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * 
	 * @hibernate.property column = "fccsa_id"
	 * @return the flightSegInventoryId
	 */
	public Integer getFlightSegInventoryId() {
		return flightSegInventoryId;
	}

	/**
	 * @param flightSegInventoryId
	 *            the flightSegInventoryId to set
	 */
	public void setFlightSegInventoryId(Integer flightSegInventoryId) {
		this.flightSegInventoryId = flightSegInventoryId;
	}

	/**
	 * @hibernate.property column = "BLOCKED_TIME"
	 * @return
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * 
	 * @param timestamp
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/***
	 * 
	 * @return
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public boolean isUpdateToADifLogicalCC() {
		return isUpdateToADifLogicalCC;
	}

	public void setUpdateToADifLogicalCC(boolean isUpdateToADifLogicalCC) {
		this.isUpdateToADifLogicalCC = isUpdateToADifLogicalCC;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

}
