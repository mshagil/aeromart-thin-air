package com.isa.thinair.airinventory.api.dto.rm;

import java.io.Serializable;

/**
 * DTO is Contains in UpdateOptimizedInventoryDTO
 * 
 * @author byorn.
 * 
 */
public class InventorySeatMovementDTO implements Serializable {

	private static final long serialVersionUID = 3732523863992915661L;

	private String fromBookingClass;

	private String toBookingClass;

	private int noOfSeats;

	/** Above Threshold or Below Threshold **/
	private String messageType;

	/**
	 * @return the noOfSeats
	 */
	public int getNoOfSeats() {
		return noOfSeats;
	}

	/**
	 * @param noOfSeats
	 *            the noOfSeats to set
	 */
	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	/**
	 * @return the fromBookingClass
	 */
	public String getFromBookingClass() {
		return fromBookingClass;
	}

	/**
	 * @param fromBookingClass
	 *            the fromBookingClass to set
	 */
	public void setFromBookingClass(String fromBookingClass) {
		this.fromBookingClass = fromBookingClass;
	}

	/**
	 * @return the toBookingClass
	 */
	public String getToBookingClass() {
		return toBookingClass;
	}

	/**
	 * @param toBookingClass
	 *            the toBookingClass to set
	 */
	public void setToBookingClass(String toBookingClass) {
		this.toBookingClass = toBookingClass;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

}
