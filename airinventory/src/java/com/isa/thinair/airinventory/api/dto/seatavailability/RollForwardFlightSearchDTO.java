package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class RollForwardFlightSearchDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Map<Integer, List<RollForwardFlightDTO>> allFltMap;
	private int adultCount;
	private int childCount;
	private int infantCount;
	private Date startDate;
	private Date endDate;
	private List<Integer> outboundInboundDateDiff;
	private int firstDepartureSegStartGap;
	private int firstDepartureSegEndGap;
	private String bcType;
	private String agentCode;
	
	public String getBcType() {
		return bcType;
	}

	public void setBcType(String bcType) {
		this.bcType = bcType;
	}

	public Map<Integer, List<RollForwardFlightDTO>> getAllFltMap() {
		if (allFltMap == null) {
			allFltMap = new HashMap<Integer, List<RollForwardFlightDTO>>();
		} 
		return allFltMap;
	}

	public void setAllFltMap(Map<Integer, List<RollForwardFlightDTO>> allFltMap) {
		this.allFltMap = allFltMap;
	}
	
	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<Integer> getOutboundInboundDateDiff() {
		return outboundInboundDateDiff;
	}

	public void setOutboundInboundDateDiff(List<Integer> outboundInboundDateDiff) {
		this.outboundInboundDateDiff = outboundInboundDateDiff;
	}

	public int getFirstDepartureSegStartGap() {
		return firstDepartureSegStartGap;
	}

	public void setFirstDepartureSegStartGap(int firstDepartureSegStartGap) {
		this.firstDepartureSegStartGap = firstDepartureSegStartGap;
	}

	public int getFirstDepartureSegEndGap() {
		return firstDepartureSegEndGap;
	}

	public void setFirstDepartureSegEndGap(int firstDepartureSegEndGap) {
		this.firstDepartureSegEndGap = firstDepartureSegEndGap;
	}
	
	public void calculateOutboundInboundDifference() {
		List<Integer> outboundInboundDateDiffs = new ArrayList<Integer>();
		if (this.allFltMap != null && this.allFltMap.size() > 1) {
			for (int i = 1; i < allFltMap.size(); i++) {
				RollForwardFlightDTO firstOutboundSeg = BeanUtils.getFirstElement(this.allFltMap.get(i - 1));
				RollForwardFlightDTO firstInboundSeg = BeanUtils.getFirstElement(this.allFltMap.get(i));
				int outboundInboundDateDiff = CalendarUtil.getTimeDifferenceInDays(firstOutboundSeg.getDepartureDateTime(),
						firstInboundSeg.getDepartureDateTime());
				outboundInboundDateDiffs.add(outboundInboundDateDiff);
			}
		} else {
			outboundInboundDateDiffs.add(0);
		}
		this.outboundInboundDateDiff = outboundInboundDateDiffs;
	}

	public void calculateFirstDeptStartEndGap() {
		if (this.allFltMap != null && this.allFltMap.size() > 0 && this.startDate != null && this.endDate != null) {
			RollForwardFlightDTO firstOutboundSeg = BeanUtils.getFirstElement(this.allFltMap.get(0));
			this.firstDepartureSegStartGap = CalendarUtil.getTimeDifferenceInDays(
					CalendarUtil.getStartTimeOfDate(firstOutboundSeg.getDepartureDateTime()), this.startDate);
			this.firstDepartureSegEndGap = CalendarUtil.getTimeDifferenceInDays(
					CalendarUtil.getStartTimeOfDate(firstOutboundSeg.getDepartureDateTime()), this.endDate);
		}
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}	
}
