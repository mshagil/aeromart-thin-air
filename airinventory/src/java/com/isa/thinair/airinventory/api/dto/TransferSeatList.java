/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Lasantha Pambagoda
 */
public class TransferSeatList implements Serializable {

	private static final long serialVersionUID = -1189441065263857315L;

	private int sourceFlightId;

	private String sourceSegmentCode;

	private int sourceSegmentId;

	private int confirmedInfantCount;

	private int onHoldInfantCount;

	private int transferSeatCount;

	private HashMap<String, TransferSeatBcCount> bcCount;

	private boolean isTransferAll;

	private ArrayList<String> pnrList;

	/**
	 * @return Returns the bcCount.
	 */
	public HashMap<String, TransferSeatBcCount> getBcCount() {
		return bcCount;
	}

	/**
	 * @param bcCount
	 *            The bcCount to set.
	 */
	public void setBcCount(HashMap<String, TransferSeatBcCount> bcCount) {
		this.bcCount = bcCount;
	}

	/**
	 * @return Returns the confirmedInfantCount.
	 */
	public int getConfirmedInfantCount() {
		return confirmedInfantCount;
	}

	/**
	 * @param confirmedInfantCount
	 *            The confirmedInfantCount to set.
	 */
	public void setConfirmedInfantCount(int infantCount) {
		this.confirmedInfantCount = infantCount;
	}

	/**
	 * @return Returns the sourceFlightId.
	 */
	public int getSourceFlightId() {
		return sourceFlightId;
	}

	/**
	 * @param sourceFlightId
	 *            The sourceFlightId to set.
	 */
	public void setSourceFlightId(int sourceFlightId) {
		this.sourceFlightId = sourceFlightId;
	}

	/**
	 * @return Returns the sourceSegmentCode.
	 */
	public String getSourceSegmentCode() {
		return sourceSegmentCode;
	}

	/**
	 * @param sourceSegmentCode
	 *            The sourceSegmentCode to set.
	 */
	public void setSourceSegmentCode(String sourceSegmentCode) {
		this.sourceSegmentCode = sourceSegmentCode;
	}

	/**
	 * @return Returns the transferSeatCount.
	 */
	public int getTransferSeatCount() {
		return transferSeatCount;
	}

	/**
	 * @param transferSeatCount
	 *            The transferSeatCount to set.
	 */
	public void setTransferSeatCount(int transferSeatCount) {
		this.transferSeatCount = transferSeatCount;
	}

	/**
	 * @return Returns the sourceSegmentId.
	 */
	public int getSourceSegmentId() {
		return sourceSegmentId;
	}

	/**
	 * @param sourceSegmentId
	 *            The sourceSegmentId to set.
	 */
	public void setSourceSegmentId(int sourceSegmentId) {
		this.sourceSegmentId = sourceSegmentId;
	}

	/**
	 * method to add bc count
	 * 
	 * @param bcCode
	 * @param trBcCount
	 */
	public void addBCCount(String bcCode, TransferSeatBcCount trBcCount) {

		if (bcCount == null)
			bcCount = new HashMap<String, TransferSeatBcCount>();
		TransferSeatBcCount existing = (TransferSeatBcCount) bcCount.get(bcCode);

		if (existing == null) {

			bcCount.put(bcCode, trBcCount);

		} else {

			existing.setOnHoldCount(existing.getOnHoldCount() + trBcCount.getOnHoldCount());
			existing.setSoldCount(existing.getSoldCount() + trBcCount.getSoldCount());

			bcCount.put(bcCode, existing);
		}
	}

	/**
	 * @return Returns the onHoldInfantCount.
	 */
	public int getOnHoldInfantCount() {
		return onHoldInfantCount;
	}

	/**
	 * @param onHoldInfantCount
	 *            The onHoldInfantCount to set.
	 */
	public void setOnHoldInfantCount(int onHoldInfantCount) {
		this.onHoldInfantCount = onHoldInfantCount;
	}

	/**
	 * @return Returns the isTransferAll.
	 */
	public boolean isTransferAll() {
		return isTransferAll;
	}

	/**
	 * @param isTransferAll
	 *            The isTransferAll to set.
	 */
	public void setTransferAll(boolean isTransferAll) {
		this.isTransferAll = isTransferAll;
	}

	public ArrayList<String> getPnrList() {
		return pnrList;
	}

	public void setPnrList(ArrayList<String> pnrList) {
		this.pnrList = pnrList;
	}

}
