package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

/**
 * BookingCode wise seat distribution
 * 
 * @author MN /BJ
 */
public class SeatDistribution implements Serializable, Cloneable {

	private static final long serialVersionUID = 3239533009769609177L;

	private String bookingClassCode;

	private int noOfSeats;

	private boolean nestedSeats = false;

	private String bookingClassType = null;

	private boolean isOnholdRestricted = false;
	
	private boolean isFixedBC;

	private boolean overbook;

	private boolean bookedFlightCabinBeingSearched;

	private boolean sameBookingClassAsExisting;

	private boolean flownOnd;
	
	private boolean waitListed;
	
	private boolean unTouchedOnd = false;
	
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	public boolean isNestedSeats() {
		return nestedSeats;
	}

	public void setNestedSeats(boolean nestedSeats) {
		this.nestedSeats = nestedSeats;
	}

	public String getBookingClassType() {
		return bookingClassType;
	}

	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}

	@Override
	public SeatDistribution clone() {
		SeatDistribution clone = new SeatDistribution();
		clone.setBookingClassCode(getBookingClassCode());
		clone.setNoOfSeats(getNoOfSeats());
		clone.setBookingClassType(getBookingClassType());
		clone.setOnholdRestricted(isOnholdRestricted());
		clone.setNestedSeats(isNestedSeats());
		clone.setOverbook(isOverbook());
		clone.setWaitListed(isWaitListed());
		clone.setBookedFlightCabinBeingSearched(isBookedFlightCabinBeingSearched());
		clone.setSameBookingClassAsExisting(isSameBookingClassAsExisting());
		clone.setFlownOnd(isFlownOnd());
		return clone;
	}

	public boolean isOnholdRestricted() {
		return isOnholdRestricted;
	}

	public void setOnholdRestricted(boolean isOnholdRestricted) {
		this.isOnholdRestricted = isOnholdRestricted;
	}

	public void setOverbook(boolean overbook) {
		this.overbook = overbook;
	}

	public boolean isOverbook() {
		return overbook;
	}

	public void setBookedFlightCabinBeingSearched(boolean bookedFlightCabinBeingSearched) {
		this.bookedFlightCabinBeingSearched = bookedFlightCabinBeingSearched;
	}

	public boolean isBookedFlightCabinBeingSearched() {
		return bookedFlightCabinBeingSearched;
	}

	@Override
	public String toString() {
		StringBuffer summary = new StringBuffer();
		summary.append("\t").append("-------------------SeatDistribution-------------------").append("\n");
		summary.append("\t").append("bookingClassCode	= " + bookingClassCode).append("\n");
		summary.append("\t").append("bookingClassType	= " + bookingClassType).append("\n");
		summary.append("\t").append("isOnholdRestricted	= " + isOnholdRestricted).append("\n");
		summary.append("\t").append("nestedSeats		= " + nestedSeats).append("\n");
		summary.append("\t").append("noOfSeats			= " + noOfSeats).append("\n");
		summary.append("\t").append("isOverbook			= " + overbook).append("\n");
		summary.append("\t").append("waitListed			= " + waitListed).append("\n");
		summary.append("\t").append("bookedFlightCabinBeingSearched			= " + isBookedFlightCabinBeingSearched()).append("\n");
		summary.append("\t").append("isSameBookingClassAsExisting			= " + isSameBookingClassAsExisting()).append("\n");
		summary.append("\t").append("isFlownOnd			= " + isFlownOnd()).append("\n");
		summary.append("\t").append("isUnTouchedOnd		= " + isUnTouchedOnd()).append("\n");
		summary.append("\t").append("------------------------------------------------------").append("\n");
		return summary.toString();
	}

	public void setSameBookingClassAsExisting(boolean sameBookingClassAsExisting) {
		this.sameBookingClassAsExisting = sameBookingClassAsExisting;
	}

	public boolean isSameBookingClassAsExisting() {
		return sameBookingClassAsExisting;
	}

	public void setFlownOnd(boolean flownOnd) {
		this.flownOnd = flownOnd;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	public boolean isWaitListed() {
		return waitListed;
	}

	public void setWaitListed(boolean waitListed) {
		this.waitListed = waitListed;
	}

	public boolean isUnTouchedOnd() {
		return unTouchedOnd;
	}

	public void setUnTouchedOnd(boolean unTouchedOnd) {
		this.unTouchedOnd = unTouchedOnd;
	}

	public boolean isFixedBC() {
		return isFixedBC;
	}

	public void setFixedBC(boolean isFixedBC) {
		this.isFixedBC = isFixedBC;
	}
}
