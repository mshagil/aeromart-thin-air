package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class FlightInvRollResultsSummaryDTO implements Serializable {

	// public static final String SUCCESSFULLY_OVER = "Successfully Over";
	// public static final String FAILED_FLIGHT_MODEL_NOT_MATCH =
	// "Failed because source and target flight models doesn't match";
	// public static final String FAILED_RESERVATIONS_EXIST = "Failed because reservations exist";
	// public static final String FAILED_HAS_INVENTORY = "Failed because there are inventories created";
	// public static final String SUCCESS="Successfully rolled the inventory to the target segment";
	// public static final String
	// SUCCESS_WITH_EXISTING_BC_INV_DELETED="Successfully rolled the inventory after deleting existing bc inventories";

	// public static final String NO_MATCHING_SOURCE_FLIGHT_BC_INV_FOUND =
	// "No matching booking class inventories from source flight";

	private static final long serialVersionUID = -5805953799779273880L;
	public static final String SEG_BC_DELETION_FAILED = "Failed, Has Reservation";
	public static final String SEG_COMPLETED_WITHOUT_ERROR = "";

	public static final String BC_CREATED = "Created";
	public static final String BC_UPDATED = "Updated";
	public static final String BC_REMOVED = "Removed";
	public static final String BC_NOT_UPDATED = "Not Updated";
	public static final String BC_UPDATE_FAILED = "Updating Failed";
	public static final String SEG_INV_UPDATE_FAILED = "Segment Inv Update Failed";
	public static final String SEG_INV_UPDATE_SUCESS = "Segment Inv Updated";

	private int fccSegInvId;
	private int flightId;
	private String segmentCode;
	private String operationStatus;
	private LinkedHashMap<String, String> bcRollStatus = new LinkedHashMap<String, String>();
	private boolean needsFlightStatusUpdating = false;
	private String curtail;
	private String overSel;
	private String flightNo;
	private Date departureDateTime;

	public String getCurtail() {
		return curtail;
	}

	public void setCurtail(String curtail) {
		this.curtail = curtail;
	}

	public String getOverSel() {
		return overSel;
	}

	public void setOverSel(String overSel) {
		this.overSel = overSel;
	}

	public int getFccSegInvId() {
		return fccSegInvId;
	}

	public void setFccSegInvId(int fccSegInvId) {
		this.fccSegInvId = fccSegInvId;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getOperationStatus() {
		return operationStatus;
	}

	public void setOperationStatus(String operationStatus) {
		this.operationStatus = operationStatus;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public void addBCRollStatus(String bookingCode, String status) {
		this.bcRollStatus.put(bookingCode, status);
	}

	public LinkedHashMap<String, String> getBcRollStatus() {
		return bcRollStatus;
	}

	public boolean isNeedsFlightStatusUpdating() {
		return needsFlightStatusUpdating;
	}

	public void setNeedsFlightStatusUpdating(boolean needsFlightStatusUpdating) {
		this.needsFlightStatusUpdating = needsFlightStatusUpdating;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getSummary() {
		String status = (flightNo != null ? ("Flight No:=" + flightNo) : "") + " Flight Id:=" + flightId + ":" + segmentCode
				+ (departureDateTime != null ? (":" + departureDateTime) : "") + ":[";
		Iterator<String> bcStatusIt = getBcRollStatus().keySet().iterator();
		String bcStatus = "[BC- Alloc||Priority||Status][";
		while (bcStatusIt.hasNext()) {
			if (!bcStatus.equals(""))
				bcStatus += " | ";
			Object bCode = bcStatusIt.next();
			bcStatus += bCode + "-" + bcRollStatus.get(bCode);
		}
		status += bcStatus + "]]";
		if (operationStatus != null && !operationStatus.equals("")) {
			status += ":" + operationStatus;
		}
		return status;
	}

	public StringBuilder getFormattedSummary() {
		
		
		
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		
		final String BR = "<BR>";
		StringBuilder strSummary = new StringBuilder(BR);
		strSummary.append("SEGMENT INVENTORY ID : ").append(fccSegInvId);
		strSummary.append("   FLIGHT ID            : ").append(flightId);
		
		//if cndition add by dhy
		if (isDescriptiveAuditEnabled){
		
		
		strSummary.append("   FLIGHT NO            : ").append(flightNo);
		}
		strSummary.append("   SEGMENT CODE         : ").append(segmentCode);
		if (isDescriptiveAuditEnabled){
		strSummary.append("   DEP DATE             : ").append(departureDateTime).append(BR);
		}
		strSummary.append(BR);
		Iterator<String> bcStatusIt = getBcRollStatus().keySet().iterator();
		strSummary.append("[BC- Alloc||Priority||Status][");
		while (bcStatusIt.hasNext()) {
			Object bCode = bcStatusIt.next();
			strSummary.append(bCode).append("-").append(bcRollStatus.get(bCode)).append(BR);
		}
		strSummary.append(
				operationStatus != null && !operationStatus.equals("") ? "OPERATION STATUS     : " + operationStatus : "")
				.append(BR);

		strSummary.append("----");
		strSummary.append(BR);
		return strSummary;
	}

}
