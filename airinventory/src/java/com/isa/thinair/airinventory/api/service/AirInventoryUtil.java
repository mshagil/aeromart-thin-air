package com.isa.thinair.airinventory.api.service;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class AirInventoryUtil {
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("airinventory");
	}
}
