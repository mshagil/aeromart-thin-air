package com.isa.thinair.airinventory.api.util;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;

/**
 * Holds the fare percentage calculator to calculate the percentages
 * 
 * @author Nilindra Fernando
 * @since Sep 13, 2012
 */
public class FarePercentageCalculator {

	public static final int RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE = 50;

	public static int getFarePercentage(int totalJourneyFareType, int ondFareType) {
		int farePercentage = 100;
		// If the total journey is a return type
		if (totalJourneyFareType == FareTypes.RETURN_FARE) {
			farePercentage = RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE;
			// If the ondFare is halfReturn
		} else if (ondFareType == FareTypes.HALF_RETURN_FARE) {
			// HALF_RETURN_FARE cannot be taken form the SelectedFlightDTO level.
			farePercentage = RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE;
		} else {
			// OND_FARE || OND_FARE_AND_PER_FLIGHT_FARE || PER_FLIGHT_FARE_AND_OND_FARE || PER_FLIGHT_FARE ||
			// (INTERLINE_FARE ? to be handle )
			farePercentage = 100; // per flight fare.
		}

		return farePercentage;

	}

}
