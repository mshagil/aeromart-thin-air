package com.isa.thinair.airinventory.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresBCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.DepAPFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class FareRulesUtil {

	/**
	 * Return all the references of FareSummaryDTOs in AvailableFlightDTO
	 * 
	 * @param availableFlightsDTO
	 * @return
	 */
	private static Collection<FareSummaryDTO> getFareSummaryReferences(AvailableFlightDTO availableFlightsDTO) {
		Collection<FareSummaryDTO> oFareSummaryRefs = new ArrayList<FareSummaryDTO>();

		if (availableFlightsDTO != null) {
			if (availableFlightsDTO.getSelectedFlight() != null) {
				SelectedFlightDTO oSelectedFilghtDTO = availableFlightsDTO.getSelectedFlight();
				oFareSummaryRefs.addAll(getFareSummaryReferences(oSelectedFilghtDTO));
			}

			for (AvailableIBOBFlightSegment availableFlightSegment : availableFlightsDTO.getAllAvailableOndFlights()) {
				if (availableFlightSegment.isDirectFlight()) {
					oFareSummaryRefs
							.addAll(getFareSummaryDTOCollectionFromAvailableFlightSegment(composeAvailableFlightSegments((AvailableFlightSegment) availableFlightSegment)));
				} else {
					oFareSummaryRefs
							.addAll(getFareSummaryDTOCollectionFromAvailableFlightSegment(((AvailableConnectedFlight) availableFlightSegment)
									.getAvailableFlightSegments()));
				}
			}
		}
		return oFareSummaryRefs;
	}

	private static List<AvailableFlightSegment> composeAvailableFlightSegments(AvailableFlightSegment... availableFlightSegments) {
		List<AvailableFlightSegment> flightSegments = new ArrayList<AvailableFlightSegment>();
		for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
			flightSegments.add(availableFlightSegment);
		}
		return flightSegments;
	}

	/**
	 * Return all the references of FareSummaryDTOs in SelectedFlightDTO
	 * 
	 * @param oSelectedFilghtDTO
	 * @return
	 */
	private static Collection<FareSummaryDTO> getFareSummaryReferences(SelectedFlightDTO oSelectedFilghtDTO) {
		Collection<FareSummaryDTO> oFareSummaryRefs = new ArrayList<FareSummaryDTO>();

		for (AvailableIBOBFlightSegment availableFlightSegment : oSelectedFilghtDTO.getSelectedOndFlights()) {
			String selectedLogicalCabinClass = availableFlightSegment.getSelectedLogicalCabinClass();
			if (selectedLogicalCabinClass != null) {
				if (availableFlightSegment.isDirectFlight()) {
					if (availableFlightSegment.getFare(selectedLogicalCabinClass) != null) {
						oFareSummaryRefs.add(availableFlightSegment.getFare(selectedLogicalCabinClass));
					}

					if (availableFlightSegment.getFareChargesSeatsSegmentsDTOs(selectedLogicalCabinClass) != null) {
						oFareSummaryRefs.addAll(getFareSummaryDTOCollectionFromOndFareDTO(availableFlightSegment
								.getFareChargesSeatsSegmentsDTOs(selectedLogicalCabinClass)));
					}

				} else {
					oFareSummaryRefs
							.addAll(getFareSummaryDTOCollectionFromAvailableFlightSegment(((AvailableConnectedFlight) availableFlightSegment)
									.getAvailableFlightSegments()));

					if (availableFlightSegment.getFareChargesSeatsSegmentsDTOs(selectedLogicalCabinClass) != null) {
						oFareSummaryRefs.addAll(getFareSummaryDTOCollectionFromOndFareDTO(availableFlightSegment
								.getFareChargesSeatsSegmentsDTOs(selectedLogicalCabinClass)));
					}
				}
			}
		}

		return oFareSummaryRefs;
	}

	/**
	 * Return FareSummary Reference Collection
	 * 
	 * @param colAvailbaleFareSeg
	 * @return
	 */
	private static Collection<FareSummaryDTO> getFareSummaryDTOCollectionFromAvailableFlightSegment(
			List<AvailableFlightSegment> colAvailbaleFareSeg) {
		Collection<FareSummaryDTO> oFareSummaryRef = new ArrayList<FareSummaryDTO>();
		for (AvailableIBOBFlightSegment oAvailFlightSeg : colAvailbaleFareSeg) {
			if (oAvailFlightSeg != null && oAvailFlightSeg.getFare(oAvailFlightSeg.getSelectedLogicalCabinClass()) != null) {
				FareSummaryDTO oFareSummary = oAvailFlightSeg.getFare(oAvailFlightSeg.getSelectedLogicalCabinClass());
				oFareSummaryRef.add(oFareSummary);
			}
		}
		return oFareSummaryRef;
	}

	private static Collection<FareSummaryDTO>
			getFareSummaryDTOCollectionFromOndFareDTO(Collection<OndFareDTO> colAvailbaleFareSeg) {
		Collection<FareSummaryDTO> oFareSummaryRef = new ArrayList<FareSummaryDTO>();
		for (OndFareDTO oOndFare : colAvailbaleFareSeg) {
			if (oOndFare != null && oOndFare.getFareSummaryDTO() != null) {
				FareSummaryDTO oFareSummary = oOndFare.getFareSummaryDTO();
				oFareSummaryRef.add(oFareSummary);
			}
		}

		return oFareSummaryRef;
	}

	/**
	 * Set applicability for given FareSumaryReferences
	 * 
	 * @param colFareSummaryRef
	 * @throws ModuleException
	 */
	public static void setApplicability(Collection<FareSummaryDTO> colFareSummaryRef) throws ModuleException {

		Collection<Integer> colFareIds = getFareRuleIdsfromFareSummary(colFareSummaryRef);
		Map<Integer, FareSummaryDTO> mapFareSumDetails = AirInventoryModuleUtils.getFareBD().getPaxDetails(colFareIds);

		for (FareSummaryDTO oFareSummaryDTORef : colFareSummaryRef) {
			// Get new FareSummaryDTO for setting applicability
			FareSummaryDTO oNewFareSummary = mapFareSumDetails.get(oFareSummaryDTORef.getFareRuleID());
			oFareSummaryDTORef.setApplicabilityFromFareSummaryDTO(oNewFareSummary);
		}

	}

	public static void injectDepAirportCurrFares(Collection<FareSummaryDTO> colFareSummaryRef) throws ModuleException {
		if (AppSysParamsUtil.isShowFareDefInDepAirPCurr()) {
			Collection<Integer> fareIds = getFareIdsFromFareSummary(colFareSummaryRef);
			Map<Integer, FareSummaryDTO> selectedCurrencyFareSummaryDTO = AirInventoryModuleUtils.getFareBD().getLocalFares(fareIds);
			Map<Integer, DepAPFareDTO> depFareIds = new HashMap<Integer, DepAPFareDTO>();
			Map<Integer, String> fareCurr = new HashMap<Integer, String>();
			for (FareSummaryDTO fareSumm : colFareSummaryRef) {
				Integer fareId = fareSumm.getFareId();
				if (!depFareIds.containsKey(fareId)) {
					DepAPFareDTO deptFare = new DepAPFareDTO();
					FareSummaryDTO fSum = selectedCurrencyFareSummaryDTO.get(fareId);
					if (fSum.getCurrenyCode() != null && !AppSysParamsUtil.getBaseCurrency().equals(fSum.getCurrenyCode())) {
						deptFare.setAdultFare(fSum.getFareAmount(PaxTypeTO.ADULT));
						deptFare.setChildFare(fSum.getFareAmount(PaxTypeTO.CHILD));
						deptFare.setInfantFare(fSum.getFareAmount(PaxTypeTO.INFANT));
						depFareIds.put(fareId, deptFare);
						fareCurr.put(fareId, fSum.getCurrenyCode());
					} else {
						fareSumm.setCurrenyCode(AppSysParamsUtil.getBaseCurrency());
						continue;
					}
				}
				fareSumm.setDepartureAPFare(depFareIds.get(fareId));
				fareSumm.setCurrenyCode(fareCurr.get(fareId));
			}
		}
	}

	private static
			Collection<FareSummaryDTO>
			getFareSummaryReferences(
					LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketOndFaresCombintationsForSegment) {
		Set<Integer> oSet = allBucketOndFaresCombintationsForSegment.keySet();
		Collection<FareSummaryDTO> colFareSummaryRef = new HashSet<FareSummaryDTO>();
		Iterator<Integer> Itr = oSet.iterator();
		while (Itr.hasNext()) {
			LinkedHashMap<String, AllFaresBCInventorySummaryDTO> oLinkedHashMap = allBucketOndFaresCombintationsForSegment
					.get(Itr.next());

			Set<String> oKey2 = oLinkedHashMap.keySet();
			Iterator<String> Itr2 = oKey2.iterator();
			while (Itr2.hasNext()) {
				AllFaresBCInventorySummaryDTO oAllFaresBCInventorySummaryDTO = oLinkedHashMap.get(Itr2.next());
				colFareSummaryRef.addAll(getFareSummaryReferences(oAllFaresBCInventorySummaryDTO));
			}

		}
		return colFareSummaryRef;

	}

	private static Collection<FareSummaryDTO> getFareSummaryReferences(
			AllFaresBCInventorySummaryDTO oAllFaresBCInventorySummaryDTO) {
		Collection<FareSummaryDTO> colFareSummaryRef = new HashSet<FareSummaryDTO>();
		LinkedHashMap<Integer, FareSummaryDTO> oFareSummaryDTOs = oAllFaresBCInventorySummaryDTO.getFareSummaryDTOs();
		Set<Integer> oFSSet = oFareSummaryDTOs.keySet();
		Iterator<Integer> Itrs = oFSSet.iterator();
		while (Itrs.hasNext()) {
			FareSummaryDTO oFareSummary = oFareSummaryDTOs.get(Itrs.next());
			colFareSummaryRef.add(oFareSummary);
		}
		return colFareSummaryRef;
	}

	/**
	 * Set Applicability for AllBucketOndFaresCombintationsForSegment
	 * 
	 * @param allBucketOndFaresCombintationsForSegment
	 * @throws ModuleException
	 */
	public static
			void
			setApplicabilityForAllBucketOndFaresCombintationsForSegment(
					LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketOndFaresCombintationsForSegment)
					throws ModuleException {
		Collection<FareSummaryDTO> colFareSummaryRefs = getFareSummaryReferences(allBucketOndFaresCombintationsForSegment);
		setApplicability(colFareSummaryRefs);
	}

	private static Collection<FareSummaryDTO> getFareSummaryReferences(AllFaresDTO oAllFaresDTO) {
		Collection<FareSummaryDTO> colFareSummaryDTORef = new ArrayList<FareSummaryDTO>();
		LinkedHashMap<Integer, SegmentFaresDTO> ogetSegmentsFaresDTOMap = oAllFaresDTO.getSegmentsFaresDTOMap();
		Set<Integer> oKey = ogetSegmentsFaresDTOMap.keySet();
		Iterator<Integer> Itr = oKey.iterator();
		while (Itr.hasNext()) {
			SegmentFaresDTO oSegmentFaresDTO = ogetSegmentsFaresDTOMap.get(Itr.next());
			HashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> oSegmentFares = oSegmentFaresDTO
					.getSegmentsFares();
			Set<Integer> oKey2 = oSegmentFares.keySet();
			Iterator<Integer> Itr2 = oKey2.iterator();
			while (Itr2.hasNext()) {
				LinkedHashMap<String, AllFaresBCInventorySummaryDTO> oLinkedHashMap = oSegmentFares.get(Itr2.next());

				Set<String> oKey3 = oLinkedHashMap.keySet();
				Iterator<String> Itr3 = oKey3.iterator();
				while (Itr3.hasNext()) {
					AllFaresBCInventorySummaryDTO oAllFaresBCInventorySummaryDTO = oLinkedHashMap.get(Itr3.next());
					colFareSummaryDTORef.addAll(getFareSummaryReferences(oAllFaresBCInventorySummaryDTO));
				}
			}
		}
		return colFareSummaryDTORef;
	}

	public static void setApplicabilityForAllFaresDTO(AllFaresDTO oAllFaresDTO) throws ModuleException {
		Collection<FareSummaryDTO> colFareSummaryRefs = getFareSummaryReferences(oAllFaresDTO);
		setApplicability(colFareSummaryRefs);
	}

	/**
	 * Set applicability for AvailableFlightDTO
	 * 
	 * @param availableFlightsDTO
	 * @throws ModuleException
	 */
	public static void setDepAPCurrForAvailableFlight(AvailableFlightDTO availableFlightsDTO) throws ModuleException {
		if (AppSysParamsUtil.isShowFareDefInDepAirPCurr()) {
			Collection<FareSummaryDTO> oFareSummaryRefs = getFareSummaryReferences(availableFlightsDTO);
			injectDepAirportCurrFares(oFareSummaryRefs);
		}
	}

	/**
	 * Set applicability for SelectedFlightDTO
	 * 
	 * @param availableFlightsDTO
	 * @throws ModuleException
	 */
	public static void setDepAPCurrForAvailableFlight(SelectedFlightDTO oSelectedFlightDTO) throws ModuleException {
		if (AppSysParamsUtil.isShowFareDefInDepAirPCurr()) {
			Collection<FareSummaryDTO> oFareSummaryRefs = getFareSummaryReferences(oSelectedFlightDTO);
			injectDepAirportCurrFares(oFareSummaryRefs);
		}
	}

	private static Collection<Integer> getFareRuleIdsfromFareSummary(Collection<FareSummaryDTO> colFareSummaryDTOs) {
		Collection<Integer> colFareIds = new HashSet<Integer>();
		for (FareSummaryDTO oFareSmmary : colFareSummaryDTOs) {
			colFareIds.add(oFareSmmary.getFareRuleID());
		}
		return colFareIds;
	}

	private static Collection<Integer> getFareIdsFromFareSummary(Collection<FareSummaryDTO> colFareSummaryDTOs) {
		Collection<Integer> colFareIds = new HashSet<Integer>();
		Iterator<FareSummaryDTO> Itr = colFareSummaryDTOs.iterator();
		while (Itr.hasNext()) {
			FareSummaryDTO oFareSmmary = Itr.next();
			colFareIds.add(oFareSmmary.getFareId());
		}
		return colFareIds;
	}
}
