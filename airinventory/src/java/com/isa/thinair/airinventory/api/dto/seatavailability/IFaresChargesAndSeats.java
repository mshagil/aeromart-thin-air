package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author MN
 */
public interface IFaresChargesAndSeats {

	public Integer getFareType(String logicalCCtype);

	public Double getFareAmount(String logicalCCtype, String sPaxTypeTO);

	/**
	 * @return double []; element 0 = total adult charges element 1 = total infant charges element 2 = total child
	 *         chatges return null if no charge quote is carried out
	 */
	public List<Double> getTotalCharges(String logicalCCtype);

	/**
	 * @return Collection of OndFareDTO
	 */
	public Collection<OndFareDTO> getFareChargesSeatsSegmentsDTOs(String logicalCCtype);

	/**
	 * @return Map of seat distributions per flight segment key = segmentId value = SegmentSeatDistsDTO
	 */
	public LinkedHashMap<Integer, SegmentSeatDistsDTO> getSegmentSeatDistributionMap(String logicalCCtype);

	public Collection<FlightSegmentDTO> getFlightSegmentDTOs();

	/**
	 * @return Summary of information contained within
	 */
	public StringBuffer getSummary();

}
