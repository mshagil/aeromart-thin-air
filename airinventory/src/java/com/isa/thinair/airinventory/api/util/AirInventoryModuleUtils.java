package com.isa.thinair.airinventory.api.util;

import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BestOffersDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightSeatDAO;
import com.isa.thinair.airinventory.core.persistence.dao.InventoryTemplateDAO;
import com.isa.thinair.airinventory.core.persistence.dao.InventoryTemplateJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.MealDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapJDBCDAO;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.AirportTransferBD;
import com.isa.thinair.airmaster.api.service.AutomaticCheckinBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.EventServiceBD;
import com.isa.thinair.airmaster.api.service.GdsBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.AirPricingBD;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.lccclient.api.service.LCCFlightServiceBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.promotion.api.service.BunldedFareBD;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;

/**
 * 
 * @author byorn.
 * 
 */
public class AirInventoryModuleUtils {

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getAirInventoryConfig(),
				AirinventoryConstants.MODULE_NAME, "airinventory.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirInventoryConfig(),
				AirinventoryConstants.MODULE_NAME, "airinventory.config.dependencymap.invalid");
	}

	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static AircraftBD getAircraftBD() {
		return (AircraftBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AircraftBD.SERVICE_NAME);
	}

	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}
	
	public static SsrBD getSsrServiceBD() {
		return (SsrBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, SsrBD.SERVICE_NAME);
	}
	
	public static AirportTransferBD getAirportTransferBD() {
		return (AirportTransferBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportTransferBD.SERVICE_NAME);
	}
	
	public static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}
	
	public static ReservationQueryBD getReservationQueryBD() {
		return (ReservationQueryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationQueryBD.SERVICE_NAME);
	}

	public static FlightInventoryBD getLocalFlightInventoryBD() {
		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	public static AutomaticCheckinBD getAutomaticCheckinBD() {
		return (AutomaticCheckinBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AutomaticCheckinBD.SERVICE_NAME);
	}

	public static ChargeBD getChargeBD() {
		return (ChargeBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	public static FareBD getFareBD() {
		return (FareBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryResBD.SERVICE_NAME);
	}
	/**
	 * Gets the Flight inventory Service
	 * 
	 * @return FlightInventoryBD the Flight Inventory delegate
	 */
	public final static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD)lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	public static LCCFlightServiceBD getLCCFlightServiceBD() {
		return (LCCFlightServiceBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCFlightServiceBD.SERVICE_NAME);
	}

	public static FlightInventoryDAO getFlightInventoryDAO() {
		FlightInventoryDAO flightInventoryDAO = (FlightInventoryDAO) AirInventoryUtil.getInstance().getLocalBean(
				"FlightInventoryDAOImplProxy");
		return flightInventoryDAO;
	}

	public static BookingClassDAO getBookingClassDAO() {
		BookingClassDAO bookingClassDAO = (BookingClassDAO) AirInventoryUtil.getInstance().getLocalBean(
				"BookingClassDAOImplProxy");
		return bookingClassDAO;
	}

	public static FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance().getLocalBean(
				"flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

	public static AirInventoryConfig getAirInventoryConfig() {
		return (AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig();
	}

	public static FlightInventoryResJDBCDAO getFlightInventoryResJDBCDAO() {
		return (FlightInventoryResJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("flightInventoryResJdbcDAO");
	}

	public static FlightSeatDAO getFlightSeatDAO() {
		return (FlightSeatDAO) AirInventoryUtil.getInstance().getLocalBean("FlightSeatDAOImplProxy");

	}

	public static SeatMapJDBCDAO getSeatMapJDBCDAO() {
		return (SeatMapJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("seatMapJDBCDAO");
	}

	public static MealDAO getMealDAO() {
		return (MealDAO) AirInventoryUtil.getInstance().getLocalBean("MealDAOImplProxy");

	}

	public static AuditorBD getAuditorBD() {
		return (AuditorBD) lookupServiceBD(AuditorConstants.MODULE_NAME, AuditorConstants.BDKeys.AUDITOR_SERVICE);

	}

	public static AirPricingBD getAirPricingBD() {
		return (AirPricingBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, AirPricingBD.SERVICE_NAME);
	}

	public static BestOffersDAO getBestOffersDAO() {
		BestOffersDAO bestOffersDAO = (BestOffersDAO) AirInventoryUtil.getInstance().getLocalBean("BestOffersDAOImplProxy");
		return bestOffersDAO;
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static BaggageDAO getBaggageDAO() {
		return (BaggageDAO) AirInventoryUtil.getInstance().getLocalBean("BaggageDAOImplProxy");
	}
	
	public final static GdsBD getGdsServiceBD() {
		return (GdsBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, GdsBD.SERVICE_NAME);
	}

    public final static MealBD getMealBD() {
        return (MealBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, MealBD.SERVICE_NAME);
    }

	public final static BaggageJDBCDAO getBaggageJDBCDAO() {
		return (BaggageJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("baggageJDBCDAO");
	}

	public final static PromotionManagementBD getPromotionManagementBD() {
		return (PromotionManagementBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, PromotionManagementBD.SERVICE_NAME);
	}

	public final static BaggageBusinessDelegate getBaggageBD() {
		return (BaggageBusinessDelegate) lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);
	}

	public static final BunldedFareBD getBundledFareBD() {
		return (BunldedFareBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, BunldedFareBD.SERVICE_NAME);
	}
	
	
	public final static EventServiceBD getEventServiceBD() {
		return (EventServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, EventServiceBD.SERVICE_NAME);
	}

	public static Object getBean(String beanName) {
		return LookupServiceFactory.getInstance().getModule(AirinventoryConstants.MODULE_NAME).getLocalBean(beanName);
	}

	public static InventoryTemplateJDBCDAO getInventoryTemplateJDBCDAO() {
		InventoryTemplateJDBCDAO inventoryTemplateJDBCDAO = (InventoryTemplateJDBCDAO) AirInventoryUtil.getInstance()
				.getLocalBean("inventoryTemplateJDBCDAO");
		return inventoryTemplateJDBCDAO;
	}

	public static InventoryTemplateDAO getInventoryTemplateDAO() {
		InventoryTemplateDAO inventoryTemplateDAO = (InventoryTemplateDAO) AirInventoryUtil.getInstance().getLocalBean(
				"InventoryTemplateDAOProxy");
		return inventoryTemplateDAO;
	}
}
