/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * model class to represent the temp record for Seg Bc Allocation
 * 
 * @author Byorn
 * 
 * @hibernate.class table = "T_TEMP_SEG_BC_ALLOC_BKP"
 * 
 * 
 */
public class TempSegBcAllocBKP extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5508795368215438865L;

	private Integer id;

	private Integer fccaId;

	private Integer fccsbaId;

	private Integer seatsBlocked;

	private Date timestamp;

	private String bookingClassType;

	private String prevStatusChgAction;

	private Integer nestRecordId;

	private String userId;

	private String status;

	private Date transferedTime;

	private String skipSegInvUpdate;

	public TempSegBcAllocBKP() {
	}

	/**
	 * @return Returns the id.
	 * 
	 * @hibernate.id column = "ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_TEMP_SEG_BC_ALLOC"
	 * 
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Returns the fccaId.
	 * @hibernate.property column = "FCCSA_ID"
	 */
	public Integer getFccaId() {
		return fccaId;
	}

	/**
	 * @param fccaId
	 *            The fccaId to set.
	 */
	public void setFccaId(Integer fccaId) {
		this.fccaId = fccaId;
	}

	/**
	 * @return Returns the fccsbaId.
	 * @hibernate.property column = "FCCSBA_ID"
	 */
	public Integer getFccsbaId() {
		return fccsbaId;
	}

	/**
	 * @param fccsbaId
	 *            The fccsbaId to set.
	 */
	public void setFccsbaId(Integer fccsbaId) {
		this.fccsbaId = fccsbaId;
	}

	/**
	 * @return Returns the bookingClassType.
	 * @hibernate.property column = "BC_TYPE"
	 */
	public String getBookingClassType() {
		return bookingClassType;
	}

	/**
	 * @param bookingClassType
	 *            The bookingClassType to set.
	 */
	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}

	/**
	 * @return Returns the seatsBlocked.
	 * @hibernate.property column = "SEATS_BLOCKED"
	 */
	public Integer getSeatsBlocked() {
		return seatsBlocked;
	}

	/**
	 * @param seatsBlocked
	 *            The seatsBlocked to set.
	 */
	public void setSeatsBlocked(Integer seatsBlocked) {
		this.seatsBlocked = seatsBlocked;
	}

	/**
	 * @return Returns the timestamp.
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            The timestamp to set.
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return Returns the prevStatusChgAction.
	 * @hibernate.property column = "PREV_STATUS_CHG_ACTION"
	 */
	public String getPrevStatusChgAction() {
		return prevStatusChgAction;
	}

	/**
	 * @param prevStatusChgAction
	 *            The prevStatusChgAction to set.
	 */
	public void setPrevStatusChgAction(String prevStatusChgAction) {
		this.prevStatusChgAction = prevStatusChgAction;
	}

	/**
	 * @hibernate.property column = "FCCSBAN_ID"
	 */
	public Integer getNestRecordId() {
		return nestRecordId;
	}

	public void setNestRecordId(Integer nestRecordId) {
		this.nestRecordId = nestRecordId;
	}

	/**
	 * @return Returns the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the user id.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the transferedTime.
	 * @hibernate.property column = "TRANSFER_TIMESTAMP"
	 */
	public Date getTransferedTime() {
		return transferedTime;
	}

	/**
	 * @param transferedTime
	 *            The transferedTime to set.
	 */
	public void setTransferedTime(Date transferedTime) {
		this.transferedTime = transferedTime;
	}

	/**
	 * @return Returns the tqofId
	 * @hibernate.property column = "SKIP_SEG_INV_UPDATE"
	 */
	public String getSkipSegInvUpdate() {
		return skipSegInvUpdate;
	}

	public void setSkipSegInvUpdate(String skipSegInvUpdate) {
		this.skipSegInvUpdate = skipSegInvUpdate;
	}

}
