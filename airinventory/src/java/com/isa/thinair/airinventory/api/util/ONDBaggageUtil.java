package com.isa.thinair.airinventory.api.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.ONDBaggageDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ONDBaggageUtil {
	/**
	 * populate OND code from ordered segment list
	 * 
	 * @param colSegs
	 * @return
	 * @throws ModuleException
	 */
	public static ONDBaggageDTO getONDBaggageDTO(List<FlightSegmentTO> colSegs) throws ModuleException {
		if (colSegs == null || colSegs.size() == 0) {
			throw new ModuleException("airinventory.util.fsegments.notfound");
		}
		ONDBaggageDTO ondBaggageDTO = new ONDBaggageDTO();
		String ondCode = colSegs.get(0).getSegmentCode();
		Date fromDate = colSegs.get(0).getDepartureDateTime();
		Date toDate = colSegs.get(0).getArrivalDateTime();

		int size = colSegs.size();

		if (size == 1) {
			ondBaggageDTO.setSegmentSearch(true);
		}

		if (size > 1) {
			List<String> ondCodes = new ArrayList<String>();
			for (IFlightSegment flightSegmentTO : colSegs) {
				ondCodes.add(flightSegmentTO.getSegmentCode());
			}

			ondCode = ReservationApiUtils.getOndCode(ondCodes);
			toDate = colSegs.get(size - 1).getArrivalDateTime();
		}

		int count = 1;
		for (IFlightSegment flightSegmentTO : colSegs) {

			ondBaggageDTO.getSegCodes().add(flightSegmentTO.getSegmentCode());
			if (count == colSegs.size()) {
				ondBaggageDTO.setLastSegCode(flightSegmentTO.getSegmentCode());
			}
			count++;
		}

		ondBaggageDTO.setOndCode(ondCode);
		ondBaggageDTO.setFromDate(fromDate);
		ondBaggageDTO.setToDate(toDate);

		return ondBaggageDTO;
	}

	public static ONDBaggageDTO getONDBaggageDTO(BaggageRq baggageRq) throws ModuleException{
		List<FlightSegmentTO> flightSegmentTOs = baggageRq.getFlightSegmentTOs();
		List<String> flightNos = new ArrayList<String>();

		if (flightSegmentTOs == null || flightSegmentTOs.size() == 0) {
			throw new ModuleException("airinventory.util.fsegments.notfound");
		}
		ONDBaggageDTO ondBaggageDTO = new ONDBaggageDTO();
		String ondCode = flightSegmentTOs.get(0).getSegmentCode();
		Date fromDate = flightSegmentTOs.get(0).getDepartureDateTime();
		Date toDate = flightSegmentTOs.get(0).getArrivalDateTime();

		int size = flightSegmentTOs.size();

		if (size == 1) {
			ondBaggageDTO.setSegmentSearch(true);
		} else {
			List<String> ondCodes = new ArrayList<String>();
			for (IFlightSegment flightSegmentTO : flightSegmentTOs) {
				ondCodes.add(flightSegmentTO.getSegmentCode());
			}

			ondCode = ReservationApiUtils.getOndCode(ondCodes);
			toDate = flightSegmentTOs.get(size - 1).getArrivalDateTime();
		}

		int count = 1;
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {

			ondBaggageDTO.getSegCodes().add(flightSegmentTO.getSegmentCode());
			if (count == flightSegmentTOs.size()) {
				ondBaggageDTO.setLastSegCode(flightSegmentTO.getSegmentCode());
			}
			count++;

			flightNos.add(flightSegmentTO.getFlightNumber());
		}

		ondBaggageDTO.setOndCode(ondCode);
		ondBaggageDTO.setFromDate(fromDate);
		ondBaggageDTO.setToDate(toDate);
		ondBaggageDTO.setBookingClasses(getApplicableBcs(baggageRq.getBookingClasses(), ondCode));
		ondBaggageDTO.setAgent(baggageRq.getAgent());
		ondBaggageDTO.setSalesChannel(String.valueOf(baggageRq.getSalesChannel()));
		ondBaggageDTO.setFlightNumbers(flightNos);

		return ondBaggageDTO;
	}


	public static ONDBaggageDTO getONDBaggageDTO(BaggageRq baggageRq, List<FlightSegmentTO> applicableFlightSegs) throws ModuleException{
		List<String> flightNos = new ArrayList<String>();

		if (applicableFlightSegs == null || applicableFlightSegs.size() == 0) {
			throw new ModuleException("airinventory.util.fsegments.notfound");
		}
		ONDBaggageDTO ondBaggageDTO = new ONDBaggageDTO();
		String ondCode;
		Date fromDate = applicableFlightSegs.get(0).getDepartureDateTime();
		int size = applicableFlightSegs.size();
		Date toDate = applicableFlightSegs.get(size - 1).getArrivalDateTime();



		List<String> ondCodes = new ArrayList<String>();
		for (FlightSegmentTO flightSegmentTO : applicableFlightSegs) {
			ondCodes.add(flightSegmentTO.getSegmentCode());
			flightNos.add(flightSegmentTO.getFlightNumber());
			ondBaggageDTO.getSegCodes().add(flightSegmentTO.getSegmentCode());
		}
		ondCode = ReservationApiUtils.getOndCode(ondCodes);

		ondBaggageDTO.setOndCode(ondCode);
		ondBaggageDTO.setFromDate(fromDate);
		ondBaggageDTO.setToDate(toDate);
		ondBaggageDTO.setBookingClasses(getApplicableBcs(baggageRq.getBookingClasses(), ondCode));
		//ondBaggageDTO.setClassOfService(getApplicableClassOfService(baggageRq.getClassesOfService(), ondCode));
		ondBaggageDTO.setAgent(baggageRq.getAgent());
		ondBaggageDTO.setSalesChannel(String.valueOf(baggageRq.getSalesChannel()));
		ondBaggageDTO.setFlightNumbers(flightNos);

		return ondBaggageDTO;
	}

	public static void spitAmountSegWise(List<LCCBaggageDTO> flightBaggageDTOs, int segCount) {
		BigDecimal amount = null;
		for (LCCBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			if (amount == null && !flightBaggageDTO.getBaggageCharge().equals(new BigDecimal(0))) {
				amount = flightBaggageDTO.getBaggageCharge();
				break;
			}
		}

		if (amount != null) {
			for (LCCBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
				flightBaggageDTO.setBaggageCharge(AccelAeroCalculator.divide(amount, segCount));
			}
		}
	}

	public static List<FlightSegmentTO> populateSegmentTOForBaggage(Collection<FlightSegmentDTO> segmentDTOs,
			Map<Integer, Boolean> fltSegIdWiseReturnFlag) throws ModuleException {

		List<FlightSegmentDTO> removeLstFlightSegmentDTO = new ArrayList<FlightSegmentDTO>();
		for (FlightSegmentDTO flightSeg : segmentDTOs) {
			if (AirInventoryModuleUtils.getAirportBD().isBusSegment(flightSeg.getSegmentCode())) {
				removeLstFlightSegmentDTO.add(flightSeg);
			}
		}
		if (removeLstFlightSegmentDTO.size() > 0) {
			segmentDTOs.removeAll(removeLstFlightSegmentDTO);
		}

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		for (FlightSegmentDTO dto : segmentDTOs) {
			FlightSegmentTO segmentTO = new FlightSegmentTO();
			segmentTO.setFlightSegId(dto.getSegmentId());
			segmentTO.setDepartureDateTime(dto.getDepartureDateTime());
			segmentTO.setArrivalDateTime(dto.getArrivalDateTime());
			segmentTO.setFlightNumber(dto.getFlightNumber());
			segmentTO.setSegmentCode(dto.getSegmentCode());
			segmentTO.setDepartureDateTimeZulu(dto.getDepartureDateTimeZulu());
			segmentTO.setArrivalDateTimeZulu(dto.getArrivalDateTimeZulu());
			segmentTO.setReturnFlag(fltSegIdWiseReturnFlag.get(dto.getSegmentId()));

			flightSegmentTOs.add(segmentTO);
		}

		if (flightSegmentTOs.size() > 0) {
			Collections.sort(flightSegmentTOs);
		}

		return flightSegmentTOs;
	}

	public static List<FlightSegmentTO> getModifySegInfo(List<LCCSelectedSegmentAncillaryDTO> anciSegList) throws ModuleException {
		if (anciSegList == null || anciSegList.size() == 0) {
			throw new ModuleException("airinventory.util.fsegments.notfound");
		}
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		for (LCCSelectedSegmentAncillaryDTO dto : anciSegList) {
			if (!dto.getBaggageDTOs().isEmpty()) {
				flightSegmentTOs.add(composeFlightSegmentTO(dto.getFlightSegmentTO()));
				if (dto.getExternalFlightSegments() != null && dto.getExternalFlightSegments().size() > 0) {
					for (FlightSegmentTO externalFlightSegmentTO : dto.getExternalFlightSegments()) {
						flightSegmentTOs.add(composeFlightSegmentTO(externalFlightSegmentTO));
					}
				}
			}
		}

		return flightSegmentTOs;
	}

	public static List<String> getApplicableBcs(Map<String, String> bookingClasses, String ondCode) {
		// A/B/C --> .*A(/|(/.*/))B(/|(/.*/))C.*

		String regExBcMatcher;
		List<String> bcs = new ArrayList<String>();
		for(String ond : bookingClasses.keySet()) {
			regExBcMatcher = ".*" + ond.replaceAll("/", "(/|(/.*/))") + ".*";
			if (ondCode.matches(regExBcMatcher)) {
				bcs.add(bookingClasses.get(ond));
			}
		}
		return bcs;
	}

	public static String getApplicableClassOfService(Map<String, String> classesOfService, String ondCode) {
		String cos = null;
		String regExCosMatcher;

		for (String ond : classesOfService.keySet()) {
			regExCosMatcher = ".*" + ond.replaceAll("/", "(/|(/.*/))") + ".*";
			if (ondCode.matches(regExCosMatcher)) {
				cos = classesOfService.get(ond);
				break;
			}
		}

		return cos;
	}

	private static FlightSegmentTO composeFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		String refNumber = flightSegmentTO.getFlightRefNumber();
		FlightSegmentTO to = new FlightSegmentTO();
		to.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(refNumber));
		to.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(refNumber));
		to.setDepartureDateTime(FlightRefNumberUtil.getDepartureDateFromFlightRPH(refNumber));
		to.setDepartureDateTimeZulu(FlightRefNumberUtil.getDepartureDateFromFlightRPH(refNumber));
		to.setReturnFlag(flightSegmentTO.isReturnFlag());
		to.setArrivalDateTime(new Date());// dummy date
		to.setBaggageONDGroupId(flightSegmentTO.getBaggageONDGroupId());
		to.setPnrSegId(flightSegmentTO.getPnrSegId());
		to.setOperatingAirline(flightSegmentTO.getOperatingAirline());
		return to;
	}

}