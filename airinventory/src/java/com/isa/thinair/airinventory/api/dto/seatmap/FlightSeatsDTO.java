/**
 * 
 */
package com.isa.thinair.airinventory.api.dto.seatmap;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author indika
 * 
 */
public class FlightSeatsDTO implements Serializable {

	private static final long serialVersionUID = 3627548030612057244L;

	private int flightSegmentID;

	private int flightId;

	private String segmentCode;

	private int templateId;

	private int smTemplateId;

	// collection of SeatDTO
	private Collection<SeatDTO> seats;

	private boolean flightFull;

	/** Anci Offer name if applicable */
	private String anciOfferName;

	/** Anci Offer description if applicable */
	private String anciOfferDescription;

	/** Boolean indicating if this is an anci offer or not. */
	private boolean isAnciOffer;

	/** Anci Offer's Seat template id used to customize the seat charges */
	private Integer offeredTemplateID;

	/**
	 * @return Returns the flightId.
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return Returns the flightSegmentID.
	 */
	public int getFlightSegmentID() {
		return flightSegmentID;
	}

	/**
	 * @param flightSegmentID
	 *            The flightSegmentID to set.
	 */
	public void setFlightSegmentID(int flightSegmentID) {
		this.flightSegmentID = flightSegmentID;
	}

	/**
	 * 
	 * @return Returns the Collection of SeatDTOs
	 */
	public Collection<SeatDTO> getSeats() {
		return seats;
	}

	/**
	 * @param Collection
	 *            of SeatDTO
	 */
	public void setSeats(Collection<SeatDTO> seats) {
		this.seats = seats;
	}

	/**
	 * @return Returns the segmentCode.
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return Returns the templateId.
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            The templateId to set.
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	public boolean isFlightFull() {
		return flightFull;
	}

	public void setFlightFull(boolean flightFull) {
		this.flightFull = flightFull;
	}

	/**
	 * See {@link #anciOfferName}
	 * 
	 * @return the anciOfferName
	 */
	public String getAnciOfferName() {
		return anciOfferName;
	}

	/**
	 * See {@link #anciOfferName}
	 * 
	 * @param anciOfferName
	 *            the anciOfferName to set
	 */
	public void setAnciOfferName(String anciOfferName) {
		this.anciOfferName = anciOfferName;
	}

	/**
	 * See {@link #anciOfferDescription}
	 * 
	 * @return the anciOfferDescription
	 */
	public String getAnciOfferDescription() {
		return anciOfferDescription;
	}

	/**
	 * See {@link #anciOfferDescription}
	 * 
	 * @param anciOfferDescription
	 *            the anciOfferDescription to set
	 */
	public void setAnciOfferDescription(String anciOfferDescription) {
		this.anciOfferDescription = anciOfferDescription;
	}

	/**
	 * See {@link #isAnciOffer}
	 * 
	 * @return the isAnciOffer
	 */
	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	/**
	 * See {@link #isAnciOffer}
	 * 
	 * @param isAnciOffer
	 *            the isAnciOffer to set
	 */
	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	/**
	 * @return the offeredTemplateID
	 */
	public Integer getOfferedTemplateID() {
		return offeredTemplateID;
	}

	/**
	 * @param offeredTemplateID the offeredTemplateID to set
	 */
	public void setOfferedTemplateID(Integer offeredTemplateID) {
		this.offeredTemplateID = offeredTemplateID;
	}

	public int getSmTemplateId() {
		return smTemplateId;
	}

	public void setSmTemplateId(int smTemplateId) {
		this.smTemplateId = smTemplateId;
	}
}
