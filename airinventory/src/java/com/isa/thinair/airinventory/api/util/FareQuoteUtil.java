package com.isa.thinair.airinventory.api.util;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;

public class FareQuoteUtil {

	public static void setHalfReturnFareQuoteOption(AvailableFlightSearchDTO availableFlightSearchDTO,
			boolean isModifyBooking) throws ModuleException {
		if (((availableFlightSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP
				|| availableFlightSearchDTO.getJourneyType() == JourneyType.CIRCULAR
				|| availableFlightSearchDTO.getJourneyType() == JourneyType.OPEN_JAW)
				&& ((availableFlightSearchDTO.isHalfReturnFareQuote() && availableFlightSearchDTO.isOpenReturnSearch())
						|| !availableFlightSearchDTO.isOpenReturnSearch()))
				|| availableFlightSearchDTO.getStayOverMillis() > 0) {

			boolean blnAllowHalfReturnFares = false;
			if (!isModifyBooking) {
				blnAllowHalfReturnFares = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
			} else {
				blnAllowHalfReturnFares = AppSysParamsUtil.isAllowHalfReturnFaresForModification();
			}

			if (JourneyType.OPEN_JAW == availableFlightSearchDTO.getJourneyType()) {
				blnAllowHalfReturnFares = AppSysParamsUtil.isAllowHalfReturnFaresForOpenJawJourney();
			}
			availableFlightSearchDTO.setHalfReturnFareQuote(blnAllowHalfReturnFares);
		} else if (availableFlightSearchDTO.getJourneyType() == JourneyType.SINGLE_SECTOR
				&& availableFlightSearchDTO.getOriginDestinationInfoDTOs().size() == 1) {

			boolean blnAllowHalfReturnFares = false;

			/*
			 * For modifications we dont want half return fares when return fare
			 * segment is open return or when these are just oneway
			 * modifications.
			 */
			if (!isModifyBooking || (isModifyBooking && availableFlightSearchDTO.isInverseSegmentOpenReturn())
					|| availableFlightSearchDTO.getInverseSegmentsOfModifiedFlightSegment() == null
					|| availableFlightSearchDTO.getInverseSegmentsOfModifiedFlightSegment().isEmpty()) {
				// For create flow with segment fares, can't serve half returns.
				blnAllowHalfReturnFares = availableFlightSearchDTO.isHalfReturnFareQuote();
			} else {
				// With non requote flow, for oneway, we give half returns. This
				// is wrong by definition.
				// However due to 30/70 rule business lost will be reduced.
				if (availableFlightSearchDTO.isInboundOutboundChanged()) {
					blnAllowHalfReturnFares = false;
				} else {
					blnAllowHalfReturnFares = AppSysParamsUtil.isAllowHalfReturnFaresForModification();
					// If the inverse fare is segment fare, half return cannot
					// select for modification.
					if (availableFlightSearchDTO.getInverseFareID() != null) {
						FareDTO fare = AirproxyModuleUtils.getFareBD()
								.getFare(availableFlightSearchDTO.getInverseFareID());
						if (fare.getFareRule().getReturnFlag() == 'N') {
							blnAllowHalfReturnFares = false;
						}
					}

				}
			}
			availableFlightSearchDTO.setHalfReturnFareQuote(blnAllowHalfReturnFares);

			if (blnAllowHalfReturnFares) {
				availableFlightSearchDTO.addOriginDestination(
						availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).cloneAndSwapOnD());
			}
		}
	}

	public static void setSearchCriteriaForModify(AvailableFlightSearchDTO flightSearchDTO,
			FlightSegmentDTO selectedFlightSegmentDTO, boolean isHalfReturnFareQuote) throws ModuleException {
		setSearchCriteriaForModify(flightSearchDTO, selectedFlightSegmentDTO.getDepartureDateTime(),
				selectedFlightSegmentDTO.getArrivalDateTime(), selectedFlightSegmentDTO.getDepartureAirportCode(),
				selectedFlightSegmentDTO.getArrivalAirportCode(), isHalfReturnFareQuote);
	}

	public static void setSearchCriteriaForModify(AvailableFlightSearchDTO flightSearchDTO, Date departureDateTime,
			Date arrivalDateTime, String origin, String destination, boolean isHalfReturnFareQuote)
			throws ModuleException {

		boolean applySameOrHigherFare = AppSysParamsUtil.isEnforceSameHigher();

		if (flightSearchDTO.getModifiedFlightSegments() != null && flightSearchDTO.isDateChangeModification()
				&& flightSearchDTO.getModifiedOndPaxFareAmount() != null) {
			if (applySameOrHigherFare && flightSearchDTO.getEnforceFareCheckForModOnd()) {
				flightSearchDTO.setEnforceFareCheckForModOnd(applySameOrHigherFare);
			}
			if (flightSearchDTO.getActualModifiedOndFareAmt() == null) // set
																		// the
																		// actual
																		// amount
																		// to
																		// compare
																		// for
																		// each
																		// flight
																		// in
																		// case
																		// of
																		// variance
																		// search
				flightSearchDTO.setActualModifiedOndFareAmt(flightSearchDTO.getModifiedOndPaxFareAmount());
			else
				flightSearchDTO.setModifiedOndPaxFareAmount(flightSearchDTO.getActualModifiedOndFareAmt());

			FareDTO modifiedFare = AirInventoryModuleUtils.getFareBD().getFare(flightSearchDTO.getModifiedOndFareId());
			if (modifiedFare != null) {
				boolean isReturnFare = "Y".equals(String.valueOf(modifiedFare.getFareRule().getReturnFlag()));
				flightSearchDTO.setModifiedOndFareType(isReturnFare ? 2 : 0);
				JourneyType journeyType = flightSearchDTO.getJourneyType();
				if (isReturnFare && !(journeyType == JourneyType.ROUNDTRIP || journeyType == JourneyType.CIRCULAR
						|| journeyType == JourneyType.OPEN_JAW)) {
					// get the prorated amount of actual fare amount for the
					// modified seg
					BigDecimal ondFareAmount = AccelAeroCalculator
							.divide(AccelAeroCalculator.multiply(flightSearchDTO.getModifiedOndPaxFareAmount(),
									FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE), 100);
					flightSearchDTO.setModifiedOndPaxFareAmount(ondFareAmount);
				}
			}
		}

		if (applySameOrHigherFare && flightSearchDTO.getEnforceFareCheckForModOnd()
				&& flightSearchDTO.isRequoteFlightSearch()) {
			flightSearchDTO.setEnforceFareCheckForModOnd(applySameOrHigherFare);

			for (OriginDestinationInfoDTO originDestinationInfoDTO : flightSearchDTO.getOrderedOriginDestinations()) {
				if (origin.equals(originDestinationInfoDTO.getOrigin())
						&& destination.equals(originDestinationInfoDTO.getDestination())
						&& (CalendarUtil.formatSQLDate(departureDateTime).equals(
								CalendarUtil.formatSQLDate(originDestinationInfoDTO.getDepartureDateTimeStart()))
								|| flightSearchDTO.isOpenReturnSearch())) {

					if (originDestinationInfoDTO.getOldPerPaxFareTO() != null) {
						FareDTO modifiedFareTmp = AirInventoryModuleUtils.getFareBD()
								.getFare(originDestinationInfoDTO.getOldPerPaxFareTO().getFareId());
						if (modifiedFareTmp != null) {
							boolean isReturnFare = "Y"
									.equals(String.valueOf(modifiedFareTmp.getFareRule().getReturnFlag()));
							flightSearchDTO.setModifiedOndFareType(isReturnFare ? 2 : 0);
							if (!isReturnFare && originDestinationInfoDTO.getFlightSegmentIds().size() > 1) {
								flightSearchDTO.setModifiedOndFareType(1);
							}
							JourneyType journeyType = flightSearchDTO.getJourneyType();
							if (isReturnFare && !(journeyType == JourneyType.ROUNDTRIP
									|| journeyType == JourneyType.CIRCULAR || journeyType == JourneyType.OPEN_JAW)) {
								// get the prorated amount of actual fare amount
								// for the modified seg
								BigDecimal ondFareAmount = AccelAeroCalculator.divide(
										AccelAeroCalculator.multiply(
												originDestinationInfoDTO.getOldPerPaxFareTO().getAmount(),
												FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE),
										100);
								flightSearchDTO.setModifiedOndPaxFareAmount(ondFareAmount);
							} else {
								flightSearchDTO.setModifiedOndPaxFareAmount(
										originDestinationInfoDTO.getOldPerPaxFareTO().getAmount());
							}
						}
					}
					break;
				} else if (originDestinationInfoDTO.getFlightSegmentIds().size() > 1) {

					FareDTO modifiedFareTmp = AirInventoryModuleUtils.getFareBD()
							.getFare(originDestinationInfoDTO.getOldPerPaxFareTO().getFareId());
					if (modifiedFareTmp != null) {
						boolean isReturnFare = "Y"
								.equals(String.valueOf(modifiedFareTmp.getFareRule().getReturnFlag()));
						flightSearchDTO.setModifiedOndFareType(isReturnFare ? 2 : 1);
						JourneyType journeyType = flightSearchDTO.getJourneyType();
						if (isReturnFare && !(journeyType == JourneyType.ROUNDTRIP
								|| journeyType == JourneyType.CIRCULAR || journeyType == JourneyType.OPEN_JAW)) {
							// get the prorated amount of actual fare amount for
							// the modified seg
							if (origin.equals(originDestinationInfoDTO.getOrigin())
									&& destination.endsWith(originDestinationInfoDTO.getDestination())) {
								BigDecimal ondFareAmount = AccelAeroCalculator.divide(
										AccelAeroCalculator.multiply(
												originDestinationInfoDTO.getOldPerPaxFareTO().getAmount(),
												FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE),
										100);
								flightSearchDTO.setModifiedOndPaxFareAmount(ondFareAmount);
							} else {
								BigDecimal ondFareAmount = AccelAeroCalculator.divide(
										AccelAeroCalculator.multiply(
												originDestinationInfoDTO.getOldPerPaxFareTO().getAmount(),
												FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE),
										100);
								flightSearchDTO.setModifiedOndPaxFareAmount(AccelAeroCalculator.divide(ondFareAmount,
										originDestinationInfoDTO.getFlightSegmentIds().size()));
							}

						} else if (isReturnFare) {
							if (!InventoryAPIUtil.isValidOndForConnectingSector(
									originDestinationInfoDTO.getFlightSegmentIds(), origin, destination)) {
								continue;
							}
							flightSearchDTO.setModifiedOndPaxFareAmount(
									originDestinationInfoDTO.getOldPerPaxFareTO().getAmount());
						} else {
							if (origin.equals(originDestinationInfoDTO.getOrigin())
									&& destination.equals(originDestinationInfoDTO.getDestination())) {
								flightSearchDTO
										.setModifiedOndPaxFareAmount(originDestinationInfoDTO.getOldPerPaxFare());
							} else if (originDestinationInfoDTO.getOldPerPaxFareTO().getSegmentCode()
									.contains(origin + "/" + destination)) {
								if (isHalfReturnFareQuote) {
									flightSearchDTO
											.setModifiedOndPaxFareAmount(originDestinationInfoDTO.getOldPerPaxFare());
								} else {
									flightSearchDTO.setModifiedOndPaxFareAmount(
											AccelAeroCalculator.divide(originDestinationInfoDTO.getOldPerPaxFare(),
													originDestinationInfoDTO.getFlightSegmentIds().size()));
								}

							}

						}
					}
				}

			}
		}

		if (flightSearchDTO.getInverseSegmentsOfModifiedFlightSegment() != null
				&& flightSearchDTO.getInverseSegmentsOfModifiedFlightSegment().size() > 0
				&& flightSearchDTO.isDateChangeModification()) { // Inverse
																	// Segment
																	// exist for
																	// modified
																	// segment
			Date outboundDepartureDateTime = null;
			Date inboundDepartureDateTime = null;

			List<FlightSegmentDTO> colInverseFlightSegments = (List<FlightSegmentDTO>) flightSearchDTO
					.getInverseSegmentsOfModifiedFlightSegment();
			if (flightSearchDTO.isInboundFareQuote()) {
				// dep.date of first inverse seg in case of connection flight
				outboundDepartureDateTime = (colInverseFlightSegments.get(0)).getDepartureDateTime();
				inboundDepartureDateTime = departureDateTime; // till end of day
				arrivalDateTime = (colInverseFlightSegments.get(colInverseFlightSegments.size() - 1))
						.getArrivalDateTime();
			} else {
				// outboundDepartureDateTime =
				// flightSearchDTO.getDepatureDateTimeStart(); // from start of
				// day
				OriginDestinationInfoDTO ondInfo = flightSearchDTO.getOrderedOriginDestinations().get(0);
				outboundDepartureDateTime = ondInfo.getDepartureDateTimeStart();
				inboundDepartureDateTime = (colInverseFlightSegments.get(0)).getDepartureDateTime();
			}

			String inverseOndCode = "";
			for (FlightSegmentDTO fltSegDTO : colInverseFlightSegments) {
				inverseOndCode = inverseOndCode + (inverseOndCode.equals("") ? fltSegDTO.getSegmentCode()
						: fltSegDTO.getSegmentCode().substring(fltSegDTO.getSegmentCode().indexOf("/")));
			}
			flightSearchDTO.setInverseOndCode(inverseOndCode);

			Calendar selectedOutboundArrivalTime = new GregorianCalendar();

			Calendar selectedInboundDepartureTime = new GregorianCalendar();

			selectedOutboundArrivalTime.setTime(arrivalDateTime);
			selectedInboundDepartureTime.setTime(inboundDepartureDateTime);

			// Check whether return transition time is ok
			String minRetTransitionTimeStr = CommonsServices.getGlobalConfig()
					.getBizParam(SystemParamKeys.MIN_RETURN_TRANSITION_TIME);
			String hours = minRetTransitionTimeStr.substring(0, minRetTransitionTimeStr.indexOf(":"));
			String mins = minRetTransitionTimeStr.substring(minRetTransitionTimeStr.indexOf(":") + 1);

			GregorianCalendar selectedOutboundArrivalTimeWithTran = new GregorianCalendar();
			selectedOutboundArrivalTimeWithTran.setTime(selectedOutboundArrivalTime.getTime());
			selectedOutboundArrivalTimeWithTran.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
			selectedOutboundArrivalTimeWithTran.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

			if (outboundDepartureDateTime.compareTo(inboundDepartureDateTime) < 0
					&& selectedOutboundArrivalTimeWithTran.getTime().before(selectedInboundDepartureTime.getTime())) {
				// Half return fare applicable only if the modified booking is
				// similar to
				// existing return booking with change in dates only
				flightSearchDTO.setHalfReturnFareQuote(true);
			} else {
				flightSearchDTO.setHalfReturnFareQuote(false);
			}
		}
	}

	public static Map<Integer, String> populateSegmentWiseClassOfService(Map<String, List<Integer>> cabinClassSelection,
			Collection<Integer> segmentIDs) {
		Map<Integer, String> segmentWiseCabinClassMap = new HashMap<Integer, String>();
		for (Integer segmentId : segmentIDs) {
			if (getClassOfserviceFromFlightSegId(cabinClassSelection, segmentId) != null) {
				segmentWiseCabinClassMap.put(segmentId,
						getClassOfserviceFromFlightSegId(cabinClassSelection, segmentId));
			}
		}
		return segmentWiseCabinClassMap;
	}

	/**
	 * Method will return class of service (cabin class code or logical cabin
	 * class code based on map passed) for given flight segment ID
	 * 
	 * @param cabinClassSelection
	 * @param segmentId
	 * @return
	 */
	public static String getClassOfserviceFromFlightSegId(Map<String, List<Integer>> cabinClassSelection,
			Integer segmentId) {
		if (cabinClassSelection != null) {
			for (Entry<String, List<Integer>> entry : cabinClassSelection.entrySet()) {
				if (entry.getValue() != null && entry.getValue().size() > 0) {
					if (entry.getValue().contains(segmentId)) {
						return entry.getKey();
					}
				} else {
					// If value list is empty, then key is set for availability
					// search with on key value
					return entry.getKey();
				}
			}
		}

		return null;
	}

	public static String getClassOfserviceFromOndSequence(Map<String, List<Integer>> cabinClassSelection,
			Integer ondSequence) {
		if (cabinClassSelection != null) {
			for (Entry<String, List<Integer>> entry : cabinClassSelection.entrySet()) {
				if (entry.getValue() != null && entry.getValue().size() > 0) {
					if (entry.getValue().contains(ondSequence)) {
						return entry.getKey();
					}
				}
			}
		}

		return null;
	}

	public static boolean isFlexiAvailable(AvailableIBOBFlightSegment ondFlight,
			String logicalCabinClass) {
		Collection<OndFareDTO> colOndFareDTOForLogicalCC = ondFlight
				.getFareChargesSeatsSegmentsDTOs(logicalCabinClass);
		return isFlexiAvailable(colOndFareDTOForLogicalCC);

	}

	public static boolean isFlexiAvailable(Collection<OndFareDTO> colOndFareDTO) {
		if (colOndFareDTO != null && colOndFareDTO.size() > 0) {
			boolean isFlexiEnabled = true;
			for (OndFareDTO ondFareDTO : colOndFareDTO) {
				boolean busSegmentExists = isBusSegmentExists(ondFareDTO.getSegmentsMap().values());
				if (colOndFareDTO.size() == 1 && busSegmentExists) {
					return false;
				}
				if (ondFareDTO.getSegmentsMap() != null && !busSegmentExists) {
					if (!ondFareDTO.getFareSummaryDTO().isHasFlexi()) {
						isFlexiEnabled = false;
						break;
					}
				}
			}
			return isFlexiEnabled;
		} else {
			return false;
		}
	}

	/**
	 * Retrieves the Flexi rule ID from the given OND fare information.
	 * 
	 * @return The Flexi rule ID or null if no Flexi rules exist.
	 */
	public static Integer getAvailableFlexiID(Collection<OndFareDTO> colOndFareDTO) {
		if (colOndFareDTO != null) {
			for (OndFareDTO ondFareDTO : colOndFareDTO) {
				if (!InventoryAPIUtil.isBusSegmentExists(ondFareDTO.getSegmentsMap().values())) {
					return ondFareDTO.getFareSummaryDTO().getFlexiRuleID();
				}
			}
		}
		return null;
	}

	public static boolean isBusSegmentExists(Collection<FlightSegmentDTO> colFlightSegment) {
		if (colFlightSegment != null && colFlightSegment.size() > 0) {
			for (FlightSegmentDTO flightSegmentDTO : colFlightSegment) {
				if (flightSegmentDTO.getOperationTypeID() != null && flightSegmentDTO
						.getOperationTypeID() == AirScheduleCustomConstants.OperationTypes.BUS_SERVICE) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isConnectionFareServed(List<OriginDestinationInfoDTO> originDestInfolist) {
		if (originDestInfolist != null) {
			for (OriginDestinationInfoDTO originDestinationInfoDTO : originDestInfolist) {
				if (originDestinationInfoDTO.getFlightSegmentIds() != null
						&& originDestinationInfoDTO.getFlightSegmentIds().size() > 1) {
					return true;
				}
			}
		}
		return false;
	}

	public static Set<String> populatePaxTypes(int applicableTo) {
		Set<String> paxTypes = new HashSet<String>();

		if (applicableTo == Charge.APPLICABLE_TO_ADULT_ONLY || applicableTo == Charge.APPLICABLE_TO_ADULT_CHILD
				|| applicableTo == Charge.APPLICABLE_TO_ADULT_INFANT || applicableTo == Charge.APPLICABLE_TO_ALL) {
			paxTypes.add(PaxTypeTO.ADULT);
		}

		if (applicableTo == Charge.APPLICABLE_TO_CHILD_ONLY || applicableTo == Charge.APPLICABLE_TO_ADULT_CHILD
				|| applicableTo == Charge.APPLICABLE_TO_ALL) {
			paxTypes.add(PaxTypeTO.CHILD);
		}

		if (applicableTo == Charge.APPLICABLE_TO_INFANT_ONLY || applicableTo == Charge.APPLICABLE_TO_ADULT_INFANT
				|| applicableTo == Charge.APPLICABLE_TO_ALL) {
			paxTypes.add(PaxTypeTO.INFANT);
		}

		return paxTypes;
	}
}
