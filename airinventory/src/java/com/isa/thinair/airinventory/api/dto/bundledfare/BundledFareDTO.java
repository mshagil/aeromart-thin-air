package com.isa.thinair.airinventory.api.dto.bundledfare;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareFreeServiceTO;

/**
 * DTO to hold bundled fare details required in reservation flow	
 * 
 * @author rumesh
 * 
 */
public class BundledFareDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = -7080104296172587315L;
	
	private Integer bundledFarePeriodId;

	private String bundledFareName;

	private Set<String> bookingClasses;

	private BigDecimal perPaxBundledFee;

	private String chargeCode;

	private Set<BundledFareFreeServiceTO> applicableServices;
	
	private Map<String, String> imagesUrlMap;

	@Deprecated
	private String imageUrl;

	private BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO;

	public String getBundledFareName() {
		return bundledFareName;
	}

	public void setBundledFareName(String bundledFareName) {
		this.bundledFareName = bundledFareName;
	}

	public Set<String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public BigDecimal getPerPaxBundledFee() {
		return perPaxBundledFee;
	}

	public void setPerPaxBundledFee(BigDecimal perPaxBundledFee) {
		this.perPaxBundledFee = perPaxBundledFee;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	
	public Integer getBundledFarePeriodId() {
		return bundledFarePeriodId;
	}

	public void setBundledFarePeriodId(Integer bundledFarePeriodId) {
		this.bundledFarePeriodId = bundledFarePeriodId;
	}
	
	public Set<BundledFareFreeServiceTO> getApplicableServices() {
		return applicableServices;
	}

	public void setApplicableServices(Set<BundledFareFreeServiceTO> applicableServices) {
		this.applicableServices = applicableServices;
	}


	/**
	 * Checks bundled fare offered for the selected period
	 * 
	 * @param serviceName
	 *            is string value of respective EXTERNAL_CHARGE
	 * @return
	 */
	public boolean isServiceIncluded(String serviceName) {
		if (applicableServices != null && !applicableServices.isEmpty()) {
			for (BundledFareFreeServiceTO applicableService : applicableServices) {
				if (applicableService.getServiceName().equalsIgnoreCase(serviceName)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * return requested bundle fare service (for the selected period) if exists
	 * 
	 * @param serviceName
	 *            is string value of respective EXTERNAL_CHARGE
	 * @return
	 */
	public BundledFareFreeServiceTO getApplicableService(String serviceName) {
		if (applicableServices != null && !applicableServices.isEmpty()) {
			for (BundledFareFreeServiceTO applicableService : applicableServices) {
				if (applicableService.getServiceName().equalsIgnoreCase(serviceName)) {
					return applicableService;
				}
			}
		}
		return null;
	}

	public Integer getApplicableServiceTemplateId(String serviceName) {
		if (isServiceIncluded(serviceName)) {
			BundledFareFreeServiceTO freeService = getApplicableService(serviceName);
			return freeService.getTemplateId();
		}

		return null;
	}

	public Set<String> getApplicableServiceNames() {
		Set<String> applicableServiceNames = null;
		if (applicableServices != null && !applicableServices.isEmpty()) {
			applicableServiceNames = new HashSet<String>();
			for (BundledFareFreeServiceTO freeServiceTO : applicableServices) {
				applicableServiceNames.add(freeServiceTO.getServiceName());
			}
		}

		return applicableServiceNames;
	}

	public Set<Integer> getApplicableApsList() {
		String serviceName = EXTERNAL_CHARGES.AIRPORT_SERVICE.toString();
		if (isServiceIncluded(serviceName)) {
			BundledFareFreeServiceTO freeServiceTO = getApplicableService(serviceName);
			return freeServiceTO.getIncludedFreeServices();
		}

		return null;
	}

	public BundledFareDTO clone() {
		BundledFareDTO clonedBundledFareDTO = new BundledFareDTO();
		clonedBundledFareDTO.setBookingClasses(this.bookingClasses);
		clonedBundledFareDTO.setBundledFarePeriodId(this.bundledFarePeriodId);
		clonedBundledFareDTO.setBundleFareDescriptionTemplateDTO(this.bundleFareDescriptionTemplateDTO);
		clonedBundledFareDTO.setBundledFareName(this.bundledFareName);
		clonedBundledFareDTO.setChargeCode(this.chargeCode);
		clonedBundledFareDTO.setApplicableServices(this.applicableServices);
		clonedBundledFareDTO.setPerPaxBundledFee(this.perPaxBundledFee);
		clonedBundledFareDTO.setImagesUrlMap(this.imagesUrlMap);
		return clonedBundledFareDTO;
	}

	public Map<String, String> getImagesUrlMap() {
		return imagesUrlMap;
	}

	public void setImagesUrlMap(Map<String, String> imagesUrlMap) {
		this.imagesUrlMap = imagesUrlMap;
	}

	public BundleFareDescriptionTemplateDTO getBundleFareDescriptionTemplateDTO() {
		return bundleFareDescriptionTemplateDTO;
	}

	public void setBundleFareDescriptionTemplateDTO(BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO) {
		this.bundleFareDescriptionTemplateDTO = bundleFareDescriptionTemplateDTO;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public String toString() {
		return "BundledFareDTO{" +
				"  bundledFarePeriodId=" + bundledFarePeriodId  +
				", bundledFareName=" + bundledFareName  +
				", chargeCode=" + chargeCode +
				", applicableServices=" + (applicableServices != null ? applicableServices.toString() : "NULL") +
				'}';
	}
}
