package com.isa.thinair.airinventory.api.dto.seatavailability;

/**
 * This must be removed one day with multi city search implementation.
 * 
 * @author charith
 * 
 */
public class OndSequence {
	public static int OUT_BOUND = 0;

	public static int IN_BOUND = 1;

	public static int RETURN_OND_SIZE = 2;
}
