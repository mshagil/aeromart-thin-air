package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author raghu
 *
 */
public class SearchRollForwardCriteriaTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3876918246984120170L;

	private String flightNo;
	
	private Integer batchSegId;
	
	private Integer batchId;
	
	private Date fromDate;
	
	private Date toDate;
	
	private String auditLog;
	
	private String status;
	
	private String createdBy;
	
	private Date createdDate;
	
	private String batchStatus;
	
	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Integer getBatchSegId() {
		return batchSegId;
	}

	public void setBatchSegId(Integer batchSegId) {
		this.batchSegId = batchSegId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(String auditLog) {
		this.auditLog = auditLog;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}
	
}
