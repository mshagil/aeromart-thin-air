package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.dto.Frequency;

public class InvRollForwardFlightsSearchCriteria implements Serializable {

	private static final long serialVersionUID = -3847638394584666466L;

	private String flightNumber;
	
	private String flightNumberList;

	private String originAirportCode;

	private String destinationAirportCode;

	private Date fromDate;

	private Date toDate;

	private Integer seatFactorMin = null;

	private Integer seatFactorMax = null;

	/* Booking codes to rollforward in each flight segment */
	private HashMap<String, List<String>> segmentBookingCodesMap = new HashMap<String, List<String>>();

	// HashMap<segmentCode,HashMap< LogicalCabinClassCode < bookingClassCodes> >
	private HashMap<String, HashMap<String, List<String>>> segmentVsSelectedBookingClassesMap;

	private Frequency frequency;

	private boolean restrictNumberOfRecords = false;

	public InvRollForwardFlightsSearchCriteria() {

	}

	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public String getOriginAirportCode() {
		return originAirportCode;
	}

	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	public Set<String> getSegmentCodes() {
		return segmentBookingCodesMap.keySet();
	}

	public void addSegmentBookingCodes(String segmentCode, List<String> bookingCodes) {
		segmentBookingCodesMap.put(segmentCode, bookingCodes);
	}

	public HashMap<String, List<String>> getSegmentBookingCodesMap() {
		return segmentBookingCodesMap;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public boolean isRestrictNumberOfRecords() {
		return restrictNumberOfRecords;
	}

	public void setRestrictNumberOfRecords(boolean restrictNumberOfRecords) {
		this.restrictNumberOfRecords = restrictNumberOfRecords;
	}

	public Integer getSeatFactorMin() {
		return seatFactorMin;
	}

	public void setSeatFactorMin(Integer seatFactorMin) {
		this.seatFactorMin = seatFactorMin;
	}

	public Integer getSeatFactorMax() {
		return seatFactorMax;
	}

	public void setSeatFactorMax(int seatFactorMax) {
		this.seatFactorMax = seatFactorMax;
	}

	public HashMap<String, HashMap<String, List<String>>> getSegmentVsSelectedBookingClassesMap() {
		if (segmentVsSelectedBookingClassesMap == null) {
			segmentVsSelectedBookingClassesMap = new HashMap<String, HashMap<String, List<String>>>();
		}
		return segmentVsSelectedBookingClassesMap;
	}

	public void addBookingClasses(String segmentCode, String logicalCabinClass, List<String> bookingClasses) {

		this.getSegmentVsSelectedBookingClassesMap();

		HashMap<String, List<String>> lccVsBookingCodesMap = segmentVsSelectedBookingClassesMap.get(segmentCode);
		if (lccVsBookingCodesMap == null) {
			lccVsBookingCodesMap = new HashMap<String, List<String>>();
			segmentVsSelectedBookingClassesMap.put(segmentCode, lccVsBookingCodesMap);
		}
		List<String> bookingCodes = lccVsBookingCodesMap.get(logicalCabinClass);
		if (bookingCodes == null) {
			bookingCodes = new ArrayList<String>();
			lccVsBookingCodesMap.put(logicalCabinClass, bookingCodes);
		}
		if(bookingClasses != null){
		bookingCodes.addAll(bookingClasses);
		}
	}

	public Map<String, List<String>> getSelectedSegmentCodesAndLogicalCainClasses() {

		Map<String, List<String>> segmentCodesAndLogicalCabinClasses = new HashMap<String, List<String>>();
		for (String segmentCode : segmentVsSelectedBookingClassesMap.keySet()) {
			List<String> logicalCabinClasses = segmentCodesAndLogicalCabinClasses.get(segmentCode);
			if (logicalCabinClasses == null) {
				logicalCabinClasses = new ArrayList<String>();
				segmentCodesAndLogicalCabinClasses.put(segmentCode, logicalCabinClasses);
			}
			logicalCabinClasses.addAll(segmentVsSelectedBookingClassesMap.get(segmentCode).keySet());
		}
		return segmentCodesAndLogicalCabinClasses;

	}

	public List<String> getSelectedLogicalCabinClasses() {
		List<String> selectedLogicalCabinClasses = new ArrayList<String>();

		for (String segmentCode : this.getSegmentVsSelectedBookingClassesMap().keySet()) {

			for (String logicalCabinClass : this.getSegmentVsSelectedBookingClassesMap().get(segmentCode).keySet()) {
				if (!selectedLogicalCabinClasses.contains(logicalCabinClass)) {
					selectedLogicalCabinClasses.add(logicalCabinClass);
				}
			}
		}
		return selectedLogicalCabinClasses;
	}

	@Override
	public String toString() {
		final String BR = "\n";
		StringBuilder strRollFwdSearchCriteria = new StringBuilder(BR);
		strRollFwdSearchCriteria.append(" Source Flight NO : ").append(this.getFlightNumber()).append(BR);
		strRollFwdSearchCriteria.append(" Source Flight Origin Airport : ").append(this.getOriginAirportCode());
		strRollFwdSearchCriteria.append(" Destination Airport : ").append(this.getDestinationAirportCode()).append(BR);
		strRollFwdSearchCriteria.append(" Rollforward From Date : ").append(this.getFromDate());
		strRollFwdSearchCriteria.append(" To Date : ").append(this.getToDate()).append(BR);
		strRollFwdSearchCriteria.append(" Selected Segment Code - Cabin Class - Booking classes : ").append(BR);
		for (String segmentCode : this.segmentVsSelectedBookingClassesMap.keySet()) {
			strRollFwdSearchCriteria.append(segmentCode).append("-");
			HashMap<String, List<String>> logicalCabinClasses = segmentVsSelectedBookingClassesMap.get(segmentCode);
			for (String logicalCC : logicalCabinClasses.keySet()) {
				strRollFwdSearchCriteria.append(logicalCC).append("-");
				List<String> bookingClasses = logicalCabinClasses.get(logicalCC);
				for (Object element : bookingClasses.toArray()) {
					strRollFwdSearchCriteria.append("'" + (String) element + "',");
				}
				strRollFwdSearchCriteria.append(BR);
			}
		}

		strRollFwdSearchCriteria.append(BR);
		strRollFwdSearchCriteria.append(" Selected ").append(this.getFrequency().toString()).append(BR);

		strRollFwdSearchCriteria.append(" Seat Factor Min : ").append(this.getSeatFactorMin());
		strRollFwdSearchCriteria.append(" Max : ").append(this.getSeatFactorMax());

		return strRollFwdSearchCriteria.toString();
	}

	public String getFlightNumberList() {
		return flightNumberList;
	}

	public void setFlightNumberList(String flightNumberList) {
		this.flightNumberList = flightNumberList;
	}
}
