package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Raghu
 *
 */
public class SearchRollForwardCriteriaSearchTO implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -589204269159385437L;

	private String flightNumber;
	
	private Date fromDate;
	
	private Date toDate;
	
	private Integer batchId;
	
	private Integer batchSegId;
	
	private String status;

	
	public Integer getBatchSegId() {
		return batchSegId;
	}

	public void setBatchSegId(Integer batchSegId) {
		this.batchSegId = batchSegId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

}
