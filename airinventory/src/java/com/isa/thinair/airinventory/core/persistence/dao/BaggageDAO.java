/**
 * 	mano
	May 9, 2011 
	2011
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airinventory.api.model.FlightBaggage;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author mano
 * 
 */
public interface BaggageDAO {

	public Collection<FlightBaggage> getFlightBaggages(Collection<Integer> flightMealIds) throws ModuleException;

	public void saveFlightBaggage(Collection<FlightBaggage> flightBaggages) throws ModuleException;

	public void saveFlightBaggage(FlightBaggage flightBaggage) throws ModuleException;

	public Collection<FlightBaggage> getFlightBaggages(int flightSegId) throws ModuleException;

	public Collection<FlightBaggage> getFlightBaggages(int flightSegId, String cabinClass) throws ModuleException;

	public Collection<FlightBaggage> getFlightBaggages(int flightSegId, int chargeId, String cabinClass) throws ModuleException;

	public void saveFlightBaggageMultiList(Collection<List<FlightBaggage>> flightBaggagesList) throws ModuleException;
	
	void deleteFlightBaggages(Collection<Integer> flightIds) throws ModuleException;
}
