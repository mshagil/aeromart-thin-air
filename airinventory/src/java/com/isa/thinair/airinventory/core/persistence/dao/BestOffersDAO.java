package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersDTO;
import com.isa.thinair.airinventory.api.model.BestOffersRequest;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface BestOffersDAO {

	public Collection<BestOffersDTO> getCashedResultsForBestOffers(int recordId) throws ModuleException;

	public void saveBestOffersDataForCaching(BestOffersRequest bestOffersRequest) throws ModuleException;

	public void removeExpiredBestOffersData(int expireTimeInMins) throws ModuleException;

}
