package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;

@Remote
public interface LogicalCabinClassBDImpl extends LogicalCabinClassBD {

}
