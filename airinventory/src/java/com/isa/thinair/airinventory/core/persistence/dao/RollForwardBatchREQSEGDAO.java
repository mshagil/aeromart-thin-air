/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaSearchTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 *  RollForwardBatchREQSEGDAO is business delegate interface for the Rollforward service apis 
 * 
 * @author Raghuraman Kumar
 * 
 */
public interface RollForwardBatchREQSEGDAO {

	public List<RollForwardBatchREQSEG> getRollForwardBatchREQSEGs() throws ModuleException;

	public RollForwardBatchREQSEG getRollForwardBatchREQSEG(int id)throws ModuleException;

	public void saveRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG)throws ModuleException;
	
	public void updateRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG)throws ModuleException;

	public void removeRollForwardBatchREQSEG(int batchId)throws ModuleException;

	public void removeRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG)throws ModuleException;

	public void saveAllRollForwardBatchREQSEG(Set<RollForwardBatchREQSEG> rollForwardBatchREQSEGSet);

	public Page<RollForwardBatchREQSEG> search(SearchRollForwardCriteriaSearchTO searchRollForwardCriteriaSearchTO, int start, int size);

	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByBatchId(Integer batchId);

	public void updateRollForwardBatchREQSEGStatus(Integer batchSegId, String status, String detailAudit) throws ModuleException;

	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByDateAndFlights(String strFromDate, String strToDate, String flightNumber);

}
