package com.isa.thinair.airinventory.core.remoting.ejb;

import java.util.ArrayList;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.dto.inventory.UpdatedInvTempDTO;
import com.isa.thinair.airinventory.api.model.InvTempCabinAlloc;
import com.isa.thinair.airinventory.api.model.InventoryTemplate;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.bl.InventoryTemplateBL;
import com.isa.thinair.airinventory.core.service.bd.InventoryTemplateBDImpl;
import com.isa.thinair.airinventory.core.service.bd.InventoryTemplateBDLocalImpl;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

/**
 * @author Priyantha
 */
@Stateless
@RemoteBinding(jndiBinding = "InventoryTemplateService.remote")
@LocalBinding(jndiBinding = "InventoryTemplateService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class InventoryTemplateServiceBean extends PlatformBaseSessionBean implements InventoryTemplateBDImpl,
		InventoryTemplateBDLocalImpl {

	InventoryTemplateBL inventoryTemplateBL = null;

	private InventoryTemplateBL getInventoryTemplateBL() {
		if (inventoryTemplateBL != null) {
			return inventoryTemplateBL;
		}

		inventoryTemplateBL = new InventoryTemplateBL();
		return inventoryTemplateBL;
	}

	/**
	 * 
	 * @param invTempDTO
	 * @return invTempDTO
	 * @throws ModuleException
	 */
	@Override
	public InvTempDTO addInventoryTemplate(InvTempDTO invTempDTO) throws ModuleException {

		try {
			InventoryTemplate newTemplate = AirInventoryModuleUtils.getInventoryTemplateDAO().saveInventoryTemplate(invTempDTO);
			invTempDTO.setInvTempID(newTemplate.getInvTempID());
			AuditAirinventory.doAuditCreateInventoryTemplate(invTempDTO, getUserPrincipal());
	
		} catch (CommonsDataAccessException cdaex) {
			log.error("Saving new inventory Template failed.", cdaex);
			this.sessionContext.setRollbackOnly();
		} catch (Exception ex) {
			log.error("Saving new inventory Template failed.", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INVENTORY_TEMPLATE);
			}
		}
		return invTempDTO;
	}

	/**
	 * 
	 * @param invTempDTO
	 *            ,updateList
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public void updateInvTemplate(InvTempDTO invTempDTO, UpdatedInvTempDTO updateList) throws ModuleException {
		try {
			ArrayList<InvTempCabinAlloc> auditList = getInventoryTemplateBL().updateInvTemplate(invTempDTO, updateList);
			AuditAirinventory.doAuditUpdateInventoryTemplate(invTempDTO, auditList, updateList, getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Updating inventory Template failed.", cdaex);
		} catch (Exception ex) {
			log.error("Updating inventory Template failed.", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INVENTORY_TEMPLATE);
			}
		}
	}

	/**
	 * 
	 * @param invTempDTO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public void addInvTemplateCcAlloc(InvTempDTO invTempDTO) throws ModuleException {
		try {
			getInventoryTemplateBL().addInvTemplateCcAlloc(invTempDTO);
			AuditAirinventory.doAuditCreateInventoryTemplate(invTempDTO, getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Add new inventory template cabin alloc failed.", cdaex);
		} catch (Exception ex) {
			log.error("Add new inventory template cabin alloc failed.", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INVENTORY_TEMPLATE);
			}
		}
	}

	/**
	 * 
	 * @param start
	 *            ,pageSize,airCraftModel
	 * @return Page
	 * @throws ModuleException
	 */
	@Override
	public Page<InvTempDTO> searchInvTemp(int start, int pageSize, String airCraftModel) throws ModuleException {
		
		return AirInventoryModuleUtils.getInventoryTemplateDAO().getInvTempBasicDataPage(start, pageSize, airCraftModel);
	}

	/**
	 * 
	 * @param start
	 *            ,pageSize,airCraftModel,editedFlag,invTempID
	 * @return Page
	 * @throws
	 */
	@Override
	public Page<InvTempCCAllocDTO> searchInvTemplCCAlloc(int start, int pageSize, String airCraftModel, boolean editedFlag, int invTempID) {
		
		Page<InvTempCCAllocDTO> ccAllogGridPage = null;
		if(editedFlag){
			ccAllogGridPage = AirInventoryModuleUtils.getInventoryTemplateJDBCDAO().getExistInvTempCCAlloPage(start, pageSize,
					airCraftModel, invTempID);
		} else {
			ccAllogGridPage = AirInventoryModuleUtils.getInventoryTemplateJDBCDAO().getInvTemplCCAllocDataPage(start, pageSize,
					airCraftModel);
		}
		return ccAllogGridPage;
	}

	/**
	 * 
	 * @param ccCode
	 * @return ArrayList
	 * @throws
	 */
	@Override
	public ArrayList<InvTempCCBCAllocDTO> getBCDetailsForInvTemp(String ccCode) {

		return AirInventoryModuleUtils.getInventoryTemplateJDBCDAO().getBCDetailsForInvTemp(ccCode);
	}

	/**
	 * 
	 * @param invTempDTO
	 *            ,cabinClassCode,logicalCabinClassCode
	 * @return ArrayList
	 * @throws ModuleException
	 */
	@Override
	public ArrayList<InvTempCCBCAllocDTO> loadGridBCallocDetails(InvTempDTO invTempDTO, String cabinClassCode, String logicalCabinClassCode) {

		return AirInventoryModuleUtils.getInventoryTemplateJDBCDAO().loadGridBCallocDetails(invTempDTO, cabinClassCode,
				logicalCabinClassCode);
	}

	/**
	 * 
	 * @param invTempDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public void saveSplitExistCcAllocs(InvTempDTO invTempDTO) throws ModuleException {
		
		ArrayList<InvTempCabinAlloc> auditList = getInventoryTemplateBL().saveSplitExistCcAllocs(invTempDTO);
		AuditAirinventory.doAuditUpdateInventoryTemplate(invTempDTO, auditList, null, getUserPrincipal());
	}

	/**
	 * 
	 * @param invTempDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public void updateInvTempStatus(InvTempDTO invTempDTO) throws ModuleException {
		InventoryTemplate existingTemplate = AirInventoryModuleUtils.getInventoryTemplateDAO().getExistingInventoryTemplate(
				invTempDTO.getInvTempID());
		if (!existingTemplate.getStatus().equalsIgnoreCase(invTempDTO.getStatus())) {
			AirInventoryModuleUtils.getInventoryTemplateDAO().updateInvTempStatus(invTempDTO);
			AuditAirinventory.doAuditUpdateInventoryTemplate(invTempDTO, null, null, getUserPrincipal());
		}

	}

	/**
	 *
	 * @param invAirCraftModel
	 *            ,segment
	 * @return boolean
	 * @throws ModuleException
	 */
	@Override
	public boolean validateDuplicateTemplate(String airCraftModel, String segment) {
		boolean isDuplicate = false;
		if (segment != null && !segment.isEmpty()) {
			isDuplicate = getInventoryTemplateBL().validateDuplicateTemp(airCraftModel, segment);
		} else {
			isDuplicate = getInventoryTemplateBL().validateDefaultDuplicateTemp(airCraftModel);
		}
		return isDuplicate;
	}

	@Override
	public ArrayList<InvTempDTO> getInventoryTemplatesForAirCraftModel(String airCraftModelName) {
		return AirInventoryModuleUtils.getInventoryTemplateDAO().getInventoryTemplateForAirCraftModel(airCraftModelName);
	}

}