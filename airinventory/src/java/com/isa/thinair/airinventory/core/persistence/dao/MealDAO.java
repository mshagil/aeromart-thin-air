/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airinventory.api.model.FlightMeal;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface MealDAO {

	public Collection<FlightMeal> getFlightMeals(Collection<Integer> flightMealIds) throws ModuleException;

	public void saveFlightMeal(FlightMeal flightMeal) throws ModuleException;

	public Collection<FlightMeal> getFlightMeals(int flightSegId, String cabinClass) throws ModuleException;

	public Collection<FlightMeal> getFlightMeals(int flightSegId, int chargeId, String cabinClass) throws ModuleException;

	public void saveFlightMeal(Collection<FlightMeal> flightMeals) throws ModuleException;

	public void deleteFlightMeals(Collection<Integer> flightSegIDs) throws ModuleException;

	public Integer createFlightMeal(FlightMeal flightMeal) throws ModuleException;

}
