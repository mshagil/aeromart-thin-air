package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.CollectionUtils;

import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.util.ChargeQuoteCriteriaUtil;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.TransitInfoDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
class ChargeQuoteHelper {

	private HashMap<String, HashMap<String, List<QuotedChargeDTO>>> ondChargeGroupWiseCharges;
	private HashMap<String, HashMap<String, List<QuotedChargeDTO>>> ondChargeGroupWiseServiceTaxes;

	public ChargeQuoteHelper(CalendarOndFlight selectedOndFlight, QuoteChargesCriteria baseCriteria, boolean isFirstOnd,
			boolean isLastOnd) throws ModuleException {
		List<FlightSegmentTO> orderedFlightSegments = selectedOndFlight.getOrderedFlightSegments();
		TransitInfoDTO transitInfoDTO = ChargeQuoteCriteriaUtil.getTransitInfoDTO(orderedFlightSegments, null);
		List<QuoteChargesCriteria> criterias = new ArrayList<QuoteChargesCriteria>();
		Iterator<FlightSegmentTO> segItr = orderedFlightSegments.iterator();
		List<QuoteChargesCriteria> serviceTaxCriterias = new ArrayList<QuoteChargesCriteria>();

		String fareQuotedOnd = selectedOndFlight.getFareQuotedOnd();
		String quotedOnds[] = fareQuotedOnd.split("\\|");

		int index = 0;

		for (String currentOnd : quotedOnds) {
			String localOndPosition = null;
			boolean isQualifyShortTransit = false;
			Collection<Long> transitDurations = null;
			Set<String> transitAirports = null;
			Set<String> depArrAirportsToExclude = null;

			if (quotedOnds.length > 1) {
				if (index == 0) {
					localOndPosition = QuoteChargesCriteria.FIRST_OND;
				} else if (index == quotedOnds.length - 1) {
					localOndPosition = QuoteChargesCriteria.LAST_OND;
				} else {
					localOndPosition = QuoteChargesCriteria.INBETWEEN_OND;
				}

			}
			if (quotedOnds.length == 1 && orderedFlightSegments.size() > 1) {
				isQualifyShortTransit = transitInfoDTO.isQualifyForShortTransit();
				transitDurations = transitInfoDTO.getTransitDurationList();
				transitAirports = transitInfoDTO.getTransitAirports();
				depArrAirportsToExclude = transitInfoDTO.getTransitAirports();
			} else {
				FlightSegmentTO fltSeg = segItr.next();
				Date departureDateZulu = fltSeg.getDepartureDateTimeZulu();
				isQualifyShortTransit = transitInfoDTO.isQualifyForShortTransit(departureDateZulu);
				transitDurations = transitInfoDTO.getTransitDurationList(departureDateZulu);
				transitAirports = transitInfoDTO.getTransitAirports(departureDateZulu);
				depArrAirportsToExclude = transitInfoDTO.getDepArrExcludeAirports(
						SegmentUtil.getFromAirport(fltSeg.getSegmentCode()), SegmentUtil.getToAirport(fltSeg.getSegmentCode()));
			}

			String ondPosition = ChargeQuoteCriteriaUtil.getONDPosition(isFirstOnd, isLastOnd, localOndPosition);

			QuoteChargesCriteria criteria = getChargeQuoteCriteria(getClonedCriteria(baseCriteria), currentOnd, ondPosition,
					isQualifyShortTransit, transitDurations, transitAirports, depArrAirportsToExclude);

			criterias.add(criteria);

			if ((QuoteChargesCriteria.FIRST_OND.equals(ondPosition)
					|| QuoteChargesCriteria.FIRST_AND_LAST_OND.equals(ondPosition)) && index == 0) {
				QuoteChargesCriteria serviceTaxCriteria = getChargeQuoteCriteria(getClonedCriteria(baseCriteria), currentOnd,
						ondPosition, isQualifyShortTransit, transitDurations, transitAirports, depArrAirportsToExclude);
				setServiceTaxCriteriaInfo(serviceTaxCriteria);
				serviceTaxCriteria.setServiceTaxQuote(true);
				serviceTaxCriterias.add(serviceTaxCriteria);

			}

			index++;
		}

		HashMap<String, HashMap<String, QuotedChargeDTO>> ondWiseChargeCodeWiseCharges = getChargeBD().quoteCharges(criterias);
		HashMap<String, HashMap<String, QuotedChargeDTO>> ondWiseSetviceTax = getChargeBD().quoteCharges(serviceTaxCriterias);

		for (String ond : ondWiseChargeCodeWiseCharges.keySet()) {
			for (QuotedChargeDTO charge : ondWiseChargeCodeWiseCharges.get(ond).values()) {
				addCharge(ond, charge);
			}
		}

		for (String ond : ondWiseSetviceTax.keySet()) {
			for (QuotedChargeDTO charge : ondWiseSetviceTax.get(ond).values()) {
				addServiceTaxCharge(ond, charge);
			}
		}
	}

	private void setServiceTaxCriteriaInfo(QuoteChargesCriteria serviceTaxCriteria) throws ModuleException {

		String pointOfEmbarkationAirport = null;
		String pointOfEmbarkationCountry = null;
		String pointOfEmbarkationState = null;

		serviceTaxCriteria.setServiceTaxQuote(true);
		serviceTaxCriteria.setOnd(serviceTaxCriteria.getOnd());
		if (serviceTaxCriteria.getOnd() != null) {
			pointOfEmbarkationAirport = serviceTaxCriteria.getOnd().split("/")[0];
			if (pointOfEmbarkationAirport != null) {
				Object[] pointOfEmbarkationInfo = CommonsServices.getGlobalConfig()
						.retrieveAirportInfo(pointOfEmbarkationAirport);

				if (pointOfEmbarkationInfo == null) {
					Map<String, CachedAirportDTO> mapAirports = AirInventoryModuleUtils.getAirportBD()
							.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(pointOfEmbarkationAirport)));

					if (mapAirports != null && !mapAirports.isEmpty() && mapAirports.get(pointOfEmbarkationAirport) != null) {
						pointOfEmbarkationCountry = mapAirports.get(pointOfEmbarkationAirport).getCountryCode();
					}

				} else {
					pointOfEmbarkationCountry = pointOfEmbarkationInfo[3].toString();

					if (pointOfEmbarkationInfo[5] != null) {
						pointOfEmbarkationState = pointOfEmbarkationInfo[5].toString();
					}
				}
			}
		}
		serviceTaxCriteria.setChargeQuoteDate(new Date());
		serviceTaxCriteria.setServiceTaxDepositeStateCode(pointOfEmbarkationState);
		serviceTaxCriteria.setServiceTaxDepositeCountryCode(pointOfEmbarkationCountry);
		serviceTaxCriteria.setServiceTaxPlaceOfSupplyStateCode(pointOfEmbarkationState);
		serviceTaxCriteria.setServiceTaxPlaceOfSupplyCountryCode(pointOfEmbarkationCountry);
		
	}

	private void addCharge(String ond, QuotedChargeDTO charge) {
		if (ondChargeGroupWiseCharges == null) {
			ondChargeGroupWiseCharges = new HashMap<String, HashMap<String, List<QuotedChargeDTO>>>();
		}
		if (!ondChargeGroupWiseCharges.containsKey(ond)) {
			ondChargeGroupWiseCharges.put(ond, new HashMap<String, List<QuotedChargeDTO>>());
		}

		if (!ondChargeGroupWiseCharges.get(ond).containsKey(charge.getChargeGroupCode())) {
			ondChargeGroupWiseCharges.get(ond).put(charge.getChargeGroupCode(), new ArrayList<QuotedChargeDTO>());
		}

		ondChargeGroupWiseCharges.get(ond).get(charge.getChargeGroupCode()).add(charge);
	}

	private void addServiceTaxCharge(String ond, QuotedChargeDTO charge) {
		if (this.ondChargeGroupWiseServiceTaxes == null) {
			ondChargeGroupWiseServiceTaxes = new HashMap<String, HashMap<String, List<QuotedChargeDTO>>>();
		}
		if (!ondChargeGroupWiseServiceTaxes.containsKey(ond)) {
			ondChargeGroupWiseServiceTaxes.put(ond, new HashMap<String, List<QuotedChargeDTO>>());
		}

		if (!ondChargeGroupWiseServiceTaxes.get(ond).containsKey(charge.getChargeGroupCode())) {
			ondChargeGroupWiseServiceTaxes.get(ond).put(charge.getChargeGroupCode(), new ArrayList<QuotedChargeDTO>());
		}

		ondChargeGroupWiseServiceTaxes.get(ond).get(charge.getChargeGroupCode()).add(charge);
	}

	private ChargeBD getChargeBD() {
		return AirInventoryModuleUtils.getChargeBD();
	}

	private QuoteChargesCriteria getClonedCriteria(QuoteChargesCriteria baseCriteria) {
		QuoteChargesCriteria newCriteria = new QuoteChargesCriteria();
		newCriteria.setPosAirport(baseCriteria.getPosAirport());
		newCriteria.setMinDepartureDate(baseCriteria.getMinDepartureDate());
		newCriteria.setMaxDepartureDate(baseCriteria.getMaxDepartureDate());
		newCriteria.setChargeGroups(baseCriteria.getChargeGroups());
		newCriteria.setCabinClassCode(baseCriteria.getCabinClassCode());
		newCriteria.setChargeCatCodes(baseCriteria.getChargeCatCodes());
		newCriteria.setAgentCode(baseCriteria.getAgentCode());
		newCriteria.setChargeQuoteDate(baseCriteria.getChargeQuoteDate());
		newCriteria.setChannelId(baseCriteria.getChannelId());
		newCriteria.setVarianceQuote(baseCriteria.isVarianceQuote());
		return newCriteria;
	}

	static QuoteChargesCriteria getBaseCriteria(Date chargeQuoteDate, OriginDestinationInformationTO ond,
			CalendarSearchRQ searchRQ) {
		QuoteChargesCriteria criterion = new QuoteChargesCriteria();
		criterion.setPosAirport(searchRQ.getAvailPreferences().getPosAirportCode());
		criterion.setMinDepartureDate(ond.getDepartureDateTimeStart());
		criterion.setMaxDepartureDate(ond.getDepartureDateTimeEnd());
		criterion.setVarianceQuote(true);
		criterion.setChargeGroups(AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS);
		criterion.setCabinClassCode(ond.getPreferredClassOfService());
		criterion.setChargeCatCodes(AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION);
		criterion.setAgentCode(searchRQ.getAvailPreferences().getTravelAgentCode());
		criterion.setChargeQuoteDate(chargeQuoteDate);
		int channelId = SalesChannelsUtil.SALES_CHANNEL_PUBLIC;
		if (ApplicationEngine.IBE == searchRQ.getAvailPreferences().getAppIndicator()) {
			channelId = SalesChannelsUtil.SALES_CHANNEL_WEB;
		} else if (ApplicationEngine.IBE == searchRQ.getAvailPreferences().getAppIndicator()) {
			channelId = SalesChannelsUtil.SALES_CHANNEL_AGENT;
		}
		criterion.setChannelId(channelId);
		return criterion;
	}

	private QuoteChargesCriteria getChargeQuoteCriteria(QuoteChargesCriteria criterion, String ond, String ondPosition,
			boolean isOndQualifyForShortTransit, Collection<Long> ondTransitDurList, Set<String> transitAirports,
			Set<String> excludeDepArrAPs) throws ModuleException {
		criterion.setOnd(ond);
		criterion.setOndPosition(ondPosition);
		criterion.setQualifyForShortTransit(isOndQualifyForShortTransit);
		criterion.setOndTransitDurList(ondTransitDurList);
		criterion.setTransitAiports(transitAirports, excludeDepArrAPs);

		return criterion;
	}

	public void applyMatchingCharges(CalendarOndFlight selectedOndFlight, PaxCountAssembler paxAssmblr,
			Map<String, Collection<String>> bcChargeGroups) {
		Map<String, Double> totalFareMap = new HashMap<String, Double>();
		Map<String, Double> totalSurchargeMap = new HashMap<String, Double>();
		Map<String, Double> totalSurchargeMapForServiceTax = new HashMap<String, Double>();

		Set<LightFareDTO> fareSet = new HashSet<LightFareDTO>(selectedOndFlight.getFareMap().values());
		for (LightFareDTO fare : fareSet) {
			if (paxAssmblr.getAdultCount() > 0) {
				if (!totalFareMap.containsKey(PaxTypeTO.ADULT)) {
					totalFareMap.put(PaxTypeTO.ADULT, new Double(0));
				}
				totalFareMap.put(PaxTypeTO.ADULT, totalFareMap.get(PaxTypeTO.ADULT) + fare.getFareAmount().doubleValue());
			}
			if (paxAssmblr.getChildCount() > 0) {
				if (!totalFareMap.containsKey(PaxTypeTO.CHILD)) {
					totalFareMap.put(PaxTypeTO.CHILD, new Double(0));
				}
				totalFareMap.put(PaxTypeTO.CHILD, totalFareMap.get(PaxTypeTO.CHILD) + fare.getChildFare().doubleValue());
			}
			if (paxAssmblr.getInfantCount() > 0) {
				if (!totalFareMap.containsKey(PaxTypeTO.INFANT)) {
					totalFareMap.put(PaxTypeTO.INFANT, new Double(0));
				}
				totalFareMap.put(PaxTypeTO.INFANT, totalFareMap.get(PaxTypeTO.INFANT) + fare.getInfantFare().doubleValue());
			}
		}

		Map<String, Map<String, Double>> groupWisePaxWiseChargeTotal = calculateCharges(selectedOndFlight, paxAssmblr,
				bcChargeGroups, totalFareMap, totalSurchargeMap);
		totalSurchargeMap = groupWisePaxWiseChargeTotal.get(ReservationInternalConstants.ChargeGroup.SUR);

		Map<String, Map<String, Map<String, Double>>> groupWisePaxWiseChargeTotalForServiceTaxMap = calculateServiceTaxApplicableCharges(
				selectedOndFlight, paxAssmblr, bcChargeGroups, totalFareMap, totalSurchargeMapForServiceTax);

		Map<String, Double> paxWiseTotal = getPaxWiseTotal(groupWisePaxWiseChargeTotal);

		Map<String, Map<String, Double>> serviceTaxApplicablePaxWiseTotalMap = new HashMap<String, Map<String, Double>>();
		for (Entry<String, Map<String, Map<String, Double>>> entry : groupWisePaxWiseChargeTotalForServiceTaxMap.entrySet()) {
			serviceTaxApplicablePaxWiseTotalMap.put(entry.getKey(), getPaxWiseTotal(entry.getValue()));
		}

		BigDecimal totalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

		totalCharges = AccelAeroCalculator.add(totalCharges, addPaxTypeCharge(selectedOndFlight, paxAssmblr.getAdultCount(),
				totalFareMap, paxWiseTotal, serviceTaxApplicablePaxWiseTotalMap, PaxTypeTO.ADULT));

		totalCharges = AccelAeroCalculator.add(totalCharges, addPaxTypeCharge(selectedOndFlight, paxAssmblr.getChildCount(),
				totalFareMap, paxWiseTotal, serviceTaxApplicablePaxWiseTotalMap, PaxTypeTO.CHILD));

		totalCharges = AccelAeroCalculator.add(totalCharges, addPaxTypeCharge(selectedOndFlight, paxAssmblr.getInfantCount(),
				totalFareMap, paxWiseTotal, serviceTaxApplicablePaxWiseTotalMap, PaxTypeTO.INFANT));

		selectedOndFlight.setTotalChargeAmount(totalCharges);
	}

	private BigDecimal addPaxTypeCharge(CalendarOndFlight selectedOndFlight, int typeWisePaxCount, Map<String, Double> totalFareMap,
			Map<String, Double> paxWiseTotal, Map<String, Map<String, Double>> serviceTaxApplicablePaxWiseTotalMap,
			String paxType) {
		BigDecimal totalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paxWiseTotal.get(paxType) != null && typeWisePaxCount > 0) {

			totalCharges = AccelAeroCalculator.add(totalCharges,
					AccelAeroCalculator.multiply(new BigDecimal(paxWiseTotal.get(paxType)), typeWisePaxCount));
			if (ondChargeGroupWiseServiceTaxes != null) {
				Map<String, BigDecimal> serviceTaxWiseApplicableTotalCharges = getServiceTaxApplicableTotal(
						serviceTaxApplicablePaxWiseTotalMap, typeWisePaxCount, paxType);
				totalCharges = AccelAeroCalculator.add(totalCharges,
						getTotalPaxServiceTax(selectedOndFlight.getFareQuotedOnd(), typeWisePaxCount,
						serviceTaxWiseApplicableTotalCharges, new BigDecimal(totalFareMap.get(paxType))));
			}
		}
		
		return totalCharges;
	}

	private Map<String, BigDecimal> getServiceTaxApplicableTotal(
			Map<String, Map<String, Double>> serviceTaxApplicablePaxWiseTotalMap, int paxTypeCount, String paxType) {
		Map<String, BigDecimal> serviceTaxableTotalCharges = new HashMap<>();
		for (Entry<String, Map<String, Double>> entry : serviceTaxApplicablePaxWiseTotalMap.entrySet()) {
			if (!entry.getValue().isEmpty()) {
				BigDecimal totalChargeValue = serviceTaxableTotalCharges.get(entry.getKey());
				if (totalChargeValue == null) {
					totalChargeValue = AccelAeroCalculator.getDefaultBigDecimalZero();
				}
				serviceTaxableTotalCharges.put(entry.getKey(), AccelAeroCalculator.add(totalChargeValue,
						AccelAeroCalculator.multiply(new BigDecimal(entry.getValue().get(paxType)), paxTypeCount)));
			}
		}

		return serviceTaxableTotalCharges;
	}

	private BigDecimal getTotalPaxServiceTax(String ond, int typeWisePaxCount,
			Map<String, BigDecimal> serviceTaxWiseApplicableTotalCharges,
			BigDecimal totalFare) {
		BigDecimal totalServiceTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (QuotedChargeDTO quotedChargeDTO : this.ondChargeGroupWiseServiceTaxes.get(ond)
				.get(PricingConstants.ChargeGroups.TAX)) {
			if (quotedChargeDTO.isChargeValueInPercentage()) {
				BigDecimal serviceTaxApplicableTotal = AccelAeroCalculator.add(
						AccelAeroCalculator.multiply(totalFare, typeWisePaxCount),
						serviceTaxWiseApplicableTotalCharges.get(quotedChargeDTO.getChargeCode()));
				BigDecimal serviceTax = AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.calculatePercentage(
						serviceTaxApplicableTotal, (float) quotedChargeDTO.getChargeValuePercentage(), RoundingMode.UP));
				totalServiceTax = AccelAeroCalculator.add(totalServiceTax, serviceTax);
			}
		}
		return totalServiceTax;
	}
	
	private Map<String, List<String>> getServiceTaxWiseExcludableChargeCodes() {
		Map<String, List<String>> serviceTaxWiseExcludableChargeCodes = new HashMap<>();
		HashSet<String> reservationChargeCodes = new HashSet<>();
		for (HashMap<String, List<QuotedChargeDTO>> chargeGroup : ondChargeGroupWiseCharges.values()) {
			for (List<QuotedChargeDTO> charges : chargeGroup.values()) {
				for (QuotedChargeDTO charge : charges) {
					reservationChargeCodes.add(charge.getChargeCode());
				}
			}
		}
		HashSet<String> quotedServiceTaxCodesForFirstOnd = new HashSet<>();
		if (ondChargeGroupWiseServiceTaxes != null) {
			for (HashMap<String, List<QuotedChargeDTO>> chargeGroup : ondChargeGroupWiseServiceTaxes.values()) {
				for (List<QuotedChargeDTO> charges : chargeGroup.values()) {
					for (QuotedChargeDTO charge : charges) {
						quotedServiceTaxCodesForFirstOnd.add(charge.getChargeCode());
					}
				}
			}
		}
		if (!quotedServiceTaxCodesForFirstOnd.isEmpty() && !reservationChargeCodes.isEmpty()) {
			serviceTaxWiseExcludableChargeCodes = getChargeJdbcDAO().getServiceTaxWiseExcludableChargeCodes(
					quotedServiceTaxCodesForFirstOnd, reservationChargeCodes);
		}
		return serviceTaxWiseExcludableChargeCodes;
	}

	private Map<String, Double> getPaxWiseTotal(Map<String, Map<String, Double>> groupWisePaxWiseChargeTotal) {
		Map<String, Double> map = new HashMap<String, Double>();
		for (Map<String, Double> groupCharges : groupWisePaxWiseChargeTotal.values()) {
			for (String paxType : groupCharges.keySet()) {
				if (!map.containsKey(paxType)) {
					map.put(paxType, new Double(0));
				}

				map.put(paxType, map.get(paxType) + groupCharges.get(paxType));
			}
		}
		return map;
	}
	
	private ChargeJdbcDAO getChargeJdbcDAO() {
		return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
	}

	public Map<String, Map<String, Double>> calculateCharges(CalendarOndFlight selectedOndFlight, PaxCountAssembler paxAssmblr,
			Map<String, Collection<String>> bcChargeGroups, Map<String, Double> totalFareMap,
			Map<String, Double> totalSurchargeMap) {
		String fareQuotedOnd = selectedOndFlight.getFareQuotedOnd();
		String quotedOnds[] = fareQuotedOnd.split("\\|");
		Iterator<FlightSegmentTO> segItr = selectedOndFlight.getOrderedFlightSegments().iterator();
		Map<String, Map<String, Double>> groupWisePaxWiseCharges = new HashMap<String, Map<String, Double>>();

		for (String currentOnd : quotedOnds) {
			Map<String, Double> surchargeMap = new HashMap<String, Double>();
			FlightSegmentTO segment = segItr.next();
			LightFareDTO fare = selectedOndFlight.getFareMap().get(segment);

			if (!CollectionUtils.isEmpty(bcChargeGroups)) {
				
				Collection<String> chargeGroupCodes = bcChargeGroups.get(fare.getBookingCode());
				
				if (!CollectionUtils.isEmpty(chargeGroupCodes) &&
					!chargeGroupCodes.contains(ReservationInternalConstants.ChargeGroup.SUR)) {
				
					if (paxAssmblr.getAdultCount() > 0) {
						surchargeMap.put(PaxTypeTO.ADULT, new Double(0));
					}
					if (paxAssmblr.getChildCount() > 0) {
						surchargeMap.put(PaxTypeTO.CHILD, new Double(0));
					}
					if (paxAssmblr.getInfantCount() > 0) {
						surchargeMap.put(PaxTypeTO.INFANT, new Double(0));
					}
				}
			}

			List<QuotedChargeDTO> ondFareCharges = null;

			ondFareCharges = getChargesFor(currentOnd, bcChargeGroups.get(fare.getBookingCode()));

			ondFareCharges = getValidCharge(ondFareCharges, paxAssmblr, segment.getDepartureDateTime());
			iterateChargeCollection(fare, ondFareCharges, groupWisePaxWiseCharges, paxAssmblr, totalFareMap, surchargeMap,
					totalSurchargeMap);

			if (surchargeMap.isEmpty() && selectedOndFlight.isONDFare()) {
				surchargeMap = groupWisePaxWiseCharges.get(ReservationInternalConstants.ChargeGroup.SUR);
				groupWisePaxWiseCharges = new HashMap<String, Map<String, Double>>();
				iterateChargeCollection(fare, ondFareCharges, groupWisePaxWiseCharges, paxAssmblr, totalFareMap, surchargeMap,
						totalSurchargeMap);
			}

		}
		return groupWisePaxWiseCharges;
	}

	public Map<String, Map<String, Map<String, Double>>> calculateServiceTaxApplicableCharges(CalendarOndFlight selectedOndFlight,
			PaxCountAssembler paxAssmblr, Map<String, Collection<String>> bcChargeGroups, Map<String, Double> totalFareMap,
			Map<String, Double> totalSurchargeMap) {
		String fareQuotedOnd = selectedOndFlight.getFareQuotedOnd();
		String quotedOnds[] = fareQuotedOnd.split("\\|");
		Iterator<FlightSegmentTO> segItr = selectedOndFlight.getOrderedFlightSegments().iterator();
		Map<String, Map<String, Map<String, Double>>> groupWisePaxWiseChargesMap = new HashMap<String, Map<String, Map<String, Double>>>();

		for (String currentOnd : quotedOnds) {
			Map<String, Double> surchargeMap = new HashMap<String, Double>();
			FlightSegmentTO segment = segItr.next();
			LightFareDTO fare = selectedOndFlight.getFareMap().get(segment);

			if (!CollectionUtils.isEmpty(bcChargeGroups)) {
				
				Collection<String> chargeGroupCodes = bcChargeGroups.get(fare.getBookingCode());
				
				if (!CollectionUtils.isEmpty(chargeGroupCodes) &&
					!chargeGroupCodes.contains(ReservationInternalConstants.ChargeGroup.SUR)) {
				
					if (paxAssmblr.getAdultCount() > 0) {
						surchargeMap.put(PaxTypeTO.ADULT, new Double(0));
					}
					if (paxAssmblr.getChildCount() > 0) {
						surchargeMap.put(PaxTypeTO.CHILD, new Double(0));
					}
					if (paxAssmblr.getInfantCount() > 0) {
						surchargeMap.put(PaxTypeTO.INFANT, new Double(0));
					}
				}
			}

			Map<String, List<QuotedChargeDTO>> ondFareChargesMap = getServiceTaxApplicableCharges(currentOnd,
					bcChargeGroups.get(fare.getBookingCode()));

			for (Map.Entry<String, List<QuotedChargeDTO>> ondFareChargesEntry : ondFareChargesMap.entrySet()) {

				List<QuotedChargeDTO> ondFareCharges = getValidCharge(ondFareChargesEntry.getValue(), paxAssmblr,
						segment.getDepartureDateTime());
				Map<String, Map<String, Double>> groupWisePaxWiseCharges = new HashMap<String, Map<String, Double>>();

				iterateChargeCollection(fare, ondFareChargesEntry.getValue(), groupWisePaxWiseCharges, paxAssmblr, totalFareMap,
						surchargeMap, totalSurchargeMap);

				if (CollectionUtils.isEmpty(surchargeMap)) {
					surchargeMap = groupWisePaxWiseCharges.get(ReservationInternalConstants.ChargeGroup.SUR);
					groupWisePaxWiseCharges = new HashMap<String, Map<String, Double>>();
					iterateChargeCollection(fare, ondFareCharges, groupWisePaxWiseCharges, paxAssmblr, totalFareMap, surchargeMap,
							totalSurchargeMap);
				}

				groupWisePaxWiseChargesMap.put(ondFareChargesEntry.getKey(), groupWisePaxWiseCharges);

			}

		}
		return groupWisePaxWiseChargesMap;
	}

	private void iterateChargeCollection(LightFareDTO fare, List<QuotedChargeDTO> ondFareCharges,
			Map<String, Map<String, Double>> groupWisePaxWiseCharges, PaxCountAssembler paxAssmblr,
			Map<String, Double> totalFareMap, Map<String, Double> surchargeMap, Map<String, Double> totalSurchargeMap) {
		for (QuotedChargeDTO charge : ondFareCharges) {
			String group = charge.getChargeGroupCode();
			if (!groupWisePaxWiseCharges.containsKey(group)) {
				groupWisePaxWiseCharges.put(group, new HashMap<String, Double>());
			}

			Double amount;
			if (paxAssmblr.getAdultCount() > 0 && isChargeValidForPaxType(charge, PaxTypeTO.ADULT)) {

				amount = ChargeQuoteUtils.getEffectiveChargeAmount(charge, PaxTypeTO.ADULT, fare.getFareAmount()
						.doubleValue(), totalFareMap.get(PaxTypeTO.ADULT), surchargeMap.get(PaxTypeTO.ADULT), totalSurchargeMap
						.get(PaxTypeTO.ADULT));
				amount = ChargeQuoteUtils.getAdjustedEffectiveCharge(amount, charge, paxAssmblr.getAdultCount());
				addPaxFareAmount(groupWisePaxWiseCharges, group, PaxTypeTO.ADULT, amount);
			}

			if (paxAssmblr.getChildCount() > 0 && isChargeValidForPaxType(charge, PaxTypeTO.CHILD)) {
				amount = ChargeQuoteUtils.getEffectiveChargeAmount(charge, PaxTypeTO.CHILD, fare.getChildFare()
						.doubleValue(), totalFareMap.get(PaxTypeTO.CHILD), surchargeMap.get(PaxTypeTO.CHILD), totalSurchargeMap
						.get(PaxTypeTO.CHILD));
				amount = ChargeQuoteUtils.getAdjustedEffectiveCharge(amount, charge, paxAssmblr.getChildCount());
				addPaxFareAmount(groupWisePaxWiseCharges, group, PaxTypeTO.CHILD, amount);
			}

			if (paxAssmblr.getAdultCount() > 0 && isChargeValidForPaxType(charge, PaxTypeTO.INFANT)) {
				amount = ChargeQuoteUtils.getEffectiveChargeAmount(charge, PaxTypeTO.INFANT, fare.getInfantFare()
						.doubleValue(), totalFareMap.get(PaxTypeTO.INFANT), surchargeMap.get(PaxTypeTO.INFANT), totalSurchargeMap
						.get(PaxTypeTO.INFANT));
				amount = ChargeQuoteUtils.getAdjustedEffectiveCharge(amount, charge, paxAssmblr.getChildCount());
				addPaxFareAmount(groupWisePaxWiseCharges, group, PaxTypeTO.INFANT, amount);
			}

		}
	}

	private void addPaxFareAmount(Map<String, Map<String, Double>> groupWisePaxWiseCharges, String group, String paxType,
			Double amount) {
		if (!groupWisePaxWiseCharges.get(group).containsKey(paxType)) {
			groupWisePaxWiseCharges.get(group).put(paxType, new Double(0));
		}
		if (amount != null) {
			groupWisePaxWiseCharges.get(group).put(paxType, groupWisePaxWiseCharges.get(group).get(paxType) + amount);
		}
	}

	private List<QuotedChargeDTO> getValidCharge(List<QuotedChargeDTO> ondFareCharges, PaxCountAssembler paxAssmblr, Date date) {
		List<QuotedChargeDTO> charges = null;
		if (ondFareCharges != null) {
			charges = new ArrayList<QuotedChargeDTO>();
			for (QuotedChargeDTO charge : ondFareCharges) {
				if (withinValidity(charge, date)
						&& (paxAssmblr.getAdultCount() > 0 && isChargeValidForPaxType(charge, PaxTypeTO.ADULT))
						|| (paxAssmblr.getChildCount() > 0 && isChargeValidForPaxType(charge, PaxTypeTO.CHILD))
						|| (paxAssmblr.getInfantCount() > 0 && isChargeValidForPaxType(charge, PaxTypeTO.INFANT))) {
					charges.add(charge);
				}
			}
		}
		return charges;
	}

	private boolean withinValidity(QuotedChargeDTO charge, Date date) {
		return charge.getEffectiveFromDate().compareTo(date) <= 0 && charge.getEffectiveToDate().compareTo(date) >= 0;
	}

	public static boolean isChargeValidForPaxType(QuotedChargeDTO charge, String paxType) {
		if (PaxTypeTO.ADULT.equals(paxType)
				&& (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
						|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
						|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT || charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD)
				|| charge.getApplicableTo() == Charge.APPLICABLE_TO_OND) {
			return true;
		}
		if (PaxTypeTO.CHILD.equals(paxType)
				&& (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
						|| charge.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY || charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD)
				|| charge.getApplicableTo() == Charge.APPLICABLE_TO_OND) {
			return true;
		}
		if (PaxTypeTO.INFANT.equals(paxType)
				&& (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
						|| charge.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY || charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT)) {
			return true;
		}
		return false;
	}

	public List<QuotedChargeDTO> getChargesFor(String ond, Collection<String> bcGroups) {
		List<QuotedChargeDTO> list = null;
		if (this.ondChargeGroupWiseCharges != null) {
			list = new ArrayList<QuotedChargeDTO>();
			if (bcGroups != null) {
				for (String bcGroup : bcGroups) {
					if (this.ondChargeGroupWiseCharges.get(ond) != null) {
						List<QuotedChargeDTO> chargeQuoteForBCGroup = this.ondChargeGroupWiseCharges.get(ond).get(bcGroup);
						if (chargeQuoteForBCGroup != null) {
							list.addAll(chargeQuoteForBCGroup);
						}
					}
				}
			}
		}
		return list;
	}

	public Map<String, List<QuotedChargeDTO>> getServiceTaxApplicableCharges(String ond, Collection<String> bcGroups) {
		Map<String, List<QuotedChargeDTO>> serviceTaxApplicableCharges = new HashMap<>();
		Map<String, List<String>> serviceTaxWiseExcludableChargeCodes = getServiceTaxWiseExcludableChargeCodes();
		if (this.ondChargeGroupWiseCharges != null && this.ondChargeGroupWiseServiceTaxes != null) {
			for (QuotedChargeDTO serviceTax : this.ondChargeGroupWiseServiceTaxes.get(ond)
					.get(PricingConstants.ChargeGroups.TAX)) {
				List<QuotedChargeDTO> list = new ArrayList<QuotedChargeDTO>();
				serviceTaxApplicableCharges.put(serviceTax.getChargeCode(), list);
				if (bcGroups != null) {
					for (String bcGroup : bcGroups) {
						for (QuotedChargeDTO quotedChargeDTO : this.ondChargeGroupWiseCharges.get(ond).get(bcGroup)) {
							if (!serviceTaxWiseExcludableChargeCodes.isEmpty() && !serviceTaxWiseExcludableChargeCodes
									.get(serviceTax.getChargeCode()).contains(quotedChargeDTO.getChargeCode())) {
								list.add(quotedChargeDTO);
							}
						}
					}
				}
			}
		}
		return serviceTaxApplicableCharges;
	}
}