package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airschedules.api.dto.AvailableONDFlight;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

class CalendarOndFlight {
	private BigDecimal totalFareAmount;
	private Map<FlightSegmentTO, LightFareDTO> fareMap;

	private boolean isONDFare = false;
	private List<FlightSegmentTO> orderedFlightSegments;
	private String fareQuotedOnd = null;

	private BigDecimal totalChargeAmount;

	public CalendarOndFlight(BigDecimal totalFareAmount, AvailableONDFlight ondFlight, Map<FlightSegmentTO, LightFareDTO> fareMap) {
		this.totalFareAmount = totalFareAmount;
		this.fareMap = fareMap;
	}

	public CalendarOndFlight(LightFareDTO fare, AvailableONDFlight flight, IPaxCountAssembler paxAssmblr) {
		this.totalFareAmount = fare.getTotalFare(paxAssmblr);
		fareMap = new HashMap<FlightSegmentTO, LightFareDTO>();
		for (FlightSegmentTO segment : flight.getFlightSegments()) {
			fareMap.put(segment, fare);
		}
		isONDFare = true;
	}

	public CalendarOndFlight(Map<FlightSegmentTO, LightFareDTO> segmentWisefare, AvailableONDFlight flight,
			IPaxCountAssembler paxAssmblr) {
		this.totalFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		fareMap = new HashMap<FlightSegmentTO, LightFareDTO>();
		for (FlightSegmentTO segment : flight.getFlightSegments()) {
			LightFareDTO fare = segmentWisefare.get(segment);
			this.totalFareAmount = AccelAeroCalculator.add(totalFareAmount, fare.getTotalFare(paxAssmblr));
			fareMap.put(segment, fare);
		}
	}


	public Map<FlightSegmentTO, LightFareDTO> getFareMap() {
		return fareMap;
	}

	public void setTotalChargeAmount(BigDecimal totalChargeAmount) {
		this.totalChargeAmount = totalChargeAmount;
	}

	public BigDecimal getTotalFareAmount() {
		return this.totalFareAmount;
	}

	public BigDecimal getTotalAmount() {
		return AccelAeroCalculator.add(this.totalFareAmount, this.totalChargeAmount);
	}

	public boolean isONDFare() {
		return isONDFare;
	}

	public List<FlightSegmentTO> getOrderedFlightSegments() {
		if (this.orderedFlightSegments == null) {
			this.orderedFlightSegments = new ArrayList<FlightSegmentTO>(fareMap.keySet());
			Collections.sort(this.orderedFlightSegments);
		}
		return this.orderedFlightSegments;
	}

	public String getFareQuotedOnd() {
		if (this.fareQuotedOnd == null) {

			if (isONDFare) {
				this.fareQuotedOnd = fareMap.values().iterator().next().getOndCode();
			} else {
				StringBuilder sb = new StringBuilder();
				boolean first = true;
				for (IFlightSegment flt : getOrderedFlightSegments()) {
					if (!first) {
						sb.append("|");
					}
					sb.append(flt.getSegmentCode());
					first = false;
				}
				this.fareQuotedOnd = sb.toString();
			}
		}
		return this.fareQuotedOnd;
	}
}