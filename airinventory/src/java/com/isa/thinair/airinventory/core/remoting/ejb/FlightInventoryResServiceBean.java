/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on September 27, 2005
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.remoting.ejb;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRS;
import com.isa.thinair.airinventory.api.dto.InvDowngradeDTO;
import com.isa.thinair.airinventory.api.dto.TempSegBcAllocDto;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChangeFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.ReconcilePassengersTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTOForRelease;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.GDSBookingClass;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.bl.BaggageBusinessLayer;
import com.isa.thinair.airinventory.core.bl.BestOffersBL;
import com.isa.thinair.airinventory.core.bl.FlightInventoryBL;
import com.isa.thinair.airinventory.core.bl.FlightInventoryResBL;
import com.isa.thinair.airinventory.core.bl.FlightSeatBL;
import com.isa.thinair.airinventory.core.bl.MealBL;
import com.isa.thinair.airinventory.core.bl.SeatAvailabilityBL;
import com.isa.thinair.airinventory.core.bl.availability.calendar.CalendarSearchBL;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResDAO;
import com.isa.thinair.airinventory.core.service.bd.FlightInventoryResBDImpl;
import com.isa.thinair.airinventory.core.service.bd.FlightInventoryResBDLocalImpl;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRS;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.WLConfirmationRequestDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.Pair;

@Stateless
@RemoteBinding(jndiBinding = "FlightInventoryResService.remote")
@LocalBinding(jndiBinding = "FlightInventoryResService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class FlightInventoryResServiceBean extends PlatformBaseSessionBean implements FlightInventoryResBDImpl,
		FlightInventoryResBDLocalImpl {

	private static final long serialVersionUID = 1370896008670912034L;
	FlightInventoryResDAO _flightInventoryResDAO = null;
	FlightInventoryDAO flightInventoryDAO = null;
	FlightInventoryResBL _flightInventoryResBL = null;
	SeatAvailabilityBL _seatAvailabilityBL = null;
	FlightSeatBL _flightSeatBL = null;
	MealBL _flightMealBL = null;
	BaggageBusinessLayer _flightBaggageBL = null;
	BestOffersBL _bestOffersBL = null;
	BookingClassDAO bookingClassDAO = null;
	private final Log log = LogFactory.getLog(getClass());
	private CalendarSearchBL _calendarSearchBL;

	/**
	 * 
	 * @param availableFlightsDTO
	 * @param availableFlightSearchDTO
	 * @return AvailableFlightSearchDTO
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AvailableFlightDTO searchAvailableFlightsSeatAvailability(AvailableFlightDTO availableFlightsDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		try {
			availableFlightsDTO.setSelectedFlight(getSeatAvailabilityBL().searchAvailableFlightsSeatAvailability(
					availableFlightsDTO.getSelectedFlight(), availableFlightSearchDTO, false));
			return availableFlightsDTO;
		} catch (CommonsDataAccessException cdaex) {
			log.error("Seat availability search failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param availableFlightsDTO
	 * @param availableFlightSearchDTO
	 * @return AvailableFlightSearchDTO
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AvailableFlightDTO nonSelectedDatesFareQuoteInIBE(AvailableFlightDTO availableFlightsDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean includeSelectedDate) throws ModuleException {
		try {
			getSeatAvailabilityBL().nonSelectedDatesFareQuoteForIBE(availableFlightsDTO, availableFlightSearchDTO,
					includeSelectedDate);
			return availableFlightsDTO;
		} catch (CommonsDataAccessException cdaex) {
			log.error("Seat availability search failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param selectedFlightDTO
	 * @param restrictionsDTO
	 * @return SelectedFlightDTO
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SelectedFlightDTO searchSelectedFlightsSeatAvailability(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote) throws ModuleException {
		try {
			return getSeatAvailabilityBL().searchAvailableFlightsSeatAvailability(selectedFlightDTO, availableFlightSearchDTO,
					skipChargeQuote);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Selected flight fare quoting failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public SelectedFlightDTO searchSelectedFlightsSeatAvailabilityTnx(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote) throws ModuleException {
		try {
			return getSeatAvailabilityBL().searchAvailableFlightsSeatAvailability(selectedFlightDTO, availableFlightSearchDTO,
					skipChargeQuote);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Selected flight fare quoting failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SelectedFlightDTO chargeQuoteSelectedFlight(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		try {
			return getSeatAvailabilityBL().chargeQuoteSelectedFlight(selectedFlightDTO, availableFlightSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Selected flight charge quoting failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<OndFareDTO> recreateFareSegCharges(OndRebuildCriteria criteria) throws ModuleException {
		try {
			return getSeatAvailabilityBL().recreateFareSegCharges(criteria);
		} catch (CommonsDataAccessException cdaex) {
			log.error("fare quoting rebuild fails", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean checkInventoryAvailability(OndRebuildCriteria criteria) throws ModuleException {
		try {
			return getSeatAvailabilityBL().checkInventoryAvailability(criteria);
		} catch (CommonsDataAccessException cdaex) {
			log.error("fare quoting rebuild fails", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param allFaresDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AllFaresDTO getAllBucketFareCombinationsForSegment(AllFaresDTO allFaresDTO, FareFilteringCriteria criteria,
			AvailableFlightSearchDTO fltSearchDTO) throws ModuleException {
		try {
			return getSeatAvailabilityBL().getAllBucketFareCombinationsForSegment(allFaresDTO, criteria, fltSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading all applicable fares for segment failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param goshowFQCriteria
	 * @return Collection of OndFareDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<OndFareDTO> quoteGoshowFares(ReconcilePassengersTO goshowFQCriteria) throws ModuleException {
		try {
			return getSeatAvailabilityBL().quoteGoshowFares(goshowFQCriteria);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Goshow fare quoting failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public OndFareDTO quoteExternallyBookedReservationFares(ReconcilePassengersTO gdsReconcilePassengersTO)
			throws ModuleException {
		try {
			return getSeatAvailabilityBL().quoteGDSFares(gdsReconcilePassengersTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Fare quoting failed for external reservation.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * @param flightIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Integer> filterFlightsHavingReservations(List<Integer> flightIds) throws ModuleException {
		try {
			return getFlightInventoryResDAO().filterFlightsHavingReservations(flightIds, null);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Filtering flights having reservations failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * @param flightSegIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Integer> filterSegmentsHavingReservations(Collection<Integer> flightSegIds) throws ModuleException {
		return getFlightInventoryResDAO().filterFlightSegsHavingReservations(flightSegIds, null);
	}

	/**
	 * @param Collection
	 *            of FlightSegmentSeatDistributionDTOs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, String> transferSeats(TransferSeatDTO transferSeatDTO) throws ModuleException {
		Map<Integer, String> bookingClassCodes = null;
		try {
			bookingClassCodes = getFlightInventoryResBL().transferSeats(transferSeatDTO, getUserPrincipal().getUserId());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Transfer seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Transfer seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
		return bookingClassCodes;
	}

	/**
	 * @param Collection
	 *            of FlightSegmentSeatDistributionDTOs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void transferWithoutRelease(TransferSeatDTO transferSeatDTO) throws ModuleException {
		try {
			getFlightInventoryResBL().reprotectSeatsWithoutRelease(transferSeatDTO, getUserPrincipal().getUserId());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Transfer seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Transfer seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * @param Collection
	 *            of SegmentSeatDistsDTOForRelease
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void releaseFlightInventory(Collection<SegmentSeatDistsDTOForRelease> colSegmentSeatDistsDTOForRelease) throws ModuleException {
		try {
			getFlightInventoryResBL().releaseInventory(colSegmentSeatDistsDTOForRelease);

		} catch (CommonsDataAccessException cdaex) {
			log.error("Release seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Release seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}

	}

	/**
	 * 
	 * @param segmentSeatDistDTOs
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void confirmOnholdSeats(Collection<SegmentSeatDistsDTO> segmentSeatDistDTOs, Collection<Integer> exchangedSegInvIds)
			throws ModuleException {
		try {
			getFlightInventoryResBL().confirmOnholdSeats(segmentSeatDistDTOs, exchangedSegInvIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Onhold confirmation failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Onhold confirmation failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * blocks seats for the reservation
	 * 
	 * @param dtoList
	 * @return collection of temp record ids
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<TempSegBcAlloc> blockSeatsForRes(Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs,
			Collection<Integer> flightAMSeatIds) throws ModuleException {
		try {

			if (flightAMSeatIds != null && flightAMSeatIds.size() > 0) {
				getFlightSeatBL().updateFlightSeats(SMUtil.adaptToAHashMap(flightAMSeatIds),
						AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED, getUserPrincipal(), null);
			}
			return getFlightInventoryResBL().blockSeatsForRes(segmentSeatDistsDTOs, getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Seat blocking failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Seat blocking failed.", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				handleError(ex);
			}
			return null;// to avoid compiler error
		}
	}

	// TODO -- this must be TransactionAttributeType.REQUIRED ---- check GDSServicesBD.processRequests, GDSServicesBD.processRequest
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection<TempSegBcAlloc> adjustSeatsBlocked(List<Integer> blockedSeats, int adjustmentAdult, int adjustmentInfant) throws ModuleException {

		try {
			return getFlightInventoryResBL().adjustSeatsBlocked(blockedSeats, adjustmentAdult, adjustmentInfant, getUserPrincipal());
		} catch (Exception e) {
			sessionContext.setRollbackOnly();
			log.error("Seat Block Adjustment Failed .... ", e);
			throw new ModuleException("seat adjustment failed");
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<TempSegBcAllocDto> getBlockedSeatsInfo(List<Integer> blockSeatIds) {

		try {
			return getFlightInventoryResBL().getBlockedSeatsInfo(blockSeatIds);
		} catch (Exception e) {
			sessionContext.setRollbackOnly();
			log.error("Seat Block Adjustment Failed .... ", e);
		}

		return null;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void confirmWaitListedSeats(WLConfirmationRequestDTO wlRequest, boolean isConfirmedReservation) throws ModuleException {
		try {
			getFlightInventoryResBL().confirmWaitListedSeats(wlRequest, isConfirmedReservation);

		} catch (CommonsDataAccessException cdaex) {
			log.error("confirmation of wait listed seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("confirmation of wait listed seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				handleError(ex);
			}
		}
	}

	/**
	 * 
	 * @param blockedRecords
	 * @param adultSeatsToSell
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void partiallyMoveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedRecords, int adultSeatsToSell,
			int infantSeatsToSell) throws ModuleException {
		try {
			getFlightInventoryResBL().partiallyMoveBlockedSeatsToSold(blockedRecords, adultSeatsToSell, infantSeatsToSell,
					getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Partially Confirming temporarily blocked seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Partially Confirming temporarily blocked seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * 
	 * @param blockedRecords
	 * @param adultSeatsToSell
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void partiallyMoveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedRecords, int adultSeatsToSell,
			int infantSeatsToSell, Map<Integer, String> flightAmSeatIdPaxTypeMap, Collection<Integer> flightMealIds,
			Collection<Integer> flightBaggageIds) throws ModuleException {
		try {
			getFlightInventoryResBL().partiallyMoveBlockedSeatsToSold(blockedRecords, adultSeatsToSell, infantSeatsToSell,
					getUserPrincipal());
			if (flightAmSeatIdPaxTypeMap != null && flightAmSeatIdPaxTypeMap.size() > 0) {
				getFlightSeatBL().updateFlightSeats(flightAmSeatIdPaxTypeMap,
						AirinventoryCustomConstants.FlightSeatStatuses.RESERVED, getUserPrincipal(), null);
			}
			if (flightMealIds != null && flightMealIds.size() > 0) {
				getFlightMealBL().updateFlightMeals(flightMealIds, AirinventoryCustomConstants.FlightSeatStatuses.RESERVED,
						getUserPrincipal(), null);
			}

			if (flightBaggageIds != null && flightBaggageIds.size() > 0) {
				getFlightBaggageBL().updateFlightBaggages(flightBaggageIds,
						AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED);
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error("Partially Confirming temporarily blocked seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Partially Confirming temporarily blocked seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * Move blocked seats to the Sold seats
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void moveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedIds) throws ModuleException {
		try {
			getFlightInventoryResBL().moveBlockedSeatsToSold(blockedIds, getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Confirming temporarily blocked seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Confirming temporarily blocked seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * Move blocked seats to the Sold seats
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void moveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedIds, Map<Integer, String> flightAMSeatIdPaxTypeMap,
			Collection<Integer> flightMealIds, Collection<Integer> flightBaggageIds) throws ModuleException {
		try {
			getFlightInventoryResBL().moveBlockedSeatsToSold(blockedIds, getUserPrincipal());
			if (flightAMSeatIdPaxTypeMap != null && flightAMSeatIdPaxTypeMap.size() > 0) {
				getFlightSeatBL().updateFlightSeats(flightAMSeatIdPaxTypeMap,
						AirinventoryCustomConstants.FlightSeatStatuses.RESERVED, getUserPrincipal(), null);
			}
			if (flightMealIds != null && flightMealIds.size() > 0) {
				getFlightMealBL().updateFlightMeals(flightMealIds, AirinventoryCustomConstants.FlightSeatStatuses.RESERVED,
						getUserPrincipal(), null);
			}
			if (flightBaggageIds != null && flightBaggageIds.size() > 0) {
				getFlightBaggageBL().updateFlightBaggages(flightBaggageIds,
						AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED);
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error("Confirming temporarily blocked seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Confirming temporarily blocked seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * unblock the blocked seats (cancelling the blocked seats)
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void releaseBlockedSeats(Collection<TempSegBcAlloc> blockedIds, boolean isFailSilenty) throws ModuleException {
		try {
			getFlightInventoryResBL().releaseBlockedInventory(blockedIds, false, getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Releasing temporarily blocked seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Releasing temporarily blocked seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * unblock the blocked seats (cancelling the blocked seats) - run in an independent trasaction
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void releaseBlockedSeatsInIndepedentTx(Collection<TempSegBcAlloc> blockedIds, boolean isFailSilenty)
			throws ModuleException {
		try {
			getFlightInventoryResBL().releaseBlockedInventory(blockedIds, isFailSilenty, getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Releasing temporarily blocked seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Releasing temporarily blocked seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * Blocked seats records whose release time is less than the current time are deleted
	 * 
	 * @param numberOfMins
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cleanupBlockedSeats(boolean isScheduleService) throws ModuleException {
		try {
			getFlightInventoryResBL().cleanupBlockedSeats(isScheduleService);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Temporarily blocked seats cleanup failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Temporarily blocked seats cleanup failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * 
	 * @param existingOndFareDTOs
	 * @param updatedOndFareDTOs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ChangeFaresDTO recalculateChargesForFareChange(ChangeFaresDTO changeFaresDTO,
			AvailableFlightSearchDTO availableFlightSearchCriteria) throws ModuleException {
		try {
			return getSeatAvailabilityBL().recalculateChargesForFareChange(changeFaresDTO, availableFlightSearchCriteria);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Recalculating charges failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * Retrieve overall seat availability, unsold fixed quota for agent, unsold fixed for other agents/non-agents
	 * 
	 * @param flightId
	 * @param segmentCode
	 * @param cabinClassCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Integer> getAvailableNonFixedAndFixedSeatsCounts(int flightId, String segmentCode, String logicalCabinClass)
			throws ModuleException {
		try {
			return getSeatAvailabilityBL()
					.getAvailableNonFixedAndFixedSeatsCounts(flightId, segmentCode, null, logicalCabinClass);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Getting available non fixed and fixed " + "seats count failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * Returns the infant quote information
	 * 
	 * @param ondFareDTOs
	 * @param station
	 * @param override
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<OndFareDTO> getInfantQuote(Collection<OndFareDTO> ondFareDTOs, String station, boolean override,
			String agentCode) throws ModuleException {
		try {
			return getSeatAvailabilityBL().getInfantQuote(ondFareDTOs, station, override, agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Get Infant Quote failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * Method added for testing blocking and confirming in single transaction
	 * 
	 * @param seagmentSeatDists
	 * @throws ModuleException
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void testBlockAndSeatSeats(Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs, int adultSeatsToSell,
			int infantSeatsToSell) throws ModuleException {
		Collection<TempSegBcAlloc> blockedSeats = null;
		try {
			blockedSeats = getFlightInventoryResBL().blockSeatsForRes(segmentSeatDistsDTOs, getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Seat blocking failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Seat blocking failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}

		try {
			getFlightInventoryResBL().partiallyMoveBlockedSeatsToSold(blockedSeats, adultSeatsToSell, infantSeatsToSell,
					getUserPrincipal());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Partially Confirming temporarily blocked seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Partially Confirming temporarily blocked seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	/**
	 * Move blocked seats to the Sold seats
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void getFlightSeats() throws ModuleException {
		try {

		} catch (CommonsDataAccessException cdaex) {
			log.error("getFlightSeats.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("getFlightSeats.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<BestOffersDTO> getDynamicBestOffers(BestOffersSearchDTO bestOffersSearchDTO) throws ModuleException {
		try {
			return getBestOffersBL().getBestOffers(bestOffersSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getDynamicBestOffers ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void removeExpiredBestOffers() throws ModuleException {
		try {
			log.info(" +===================================+ ");
			log.info("  Started Expire Best Offers ");
			log.info(" +===================================+ ");
			getBestOffersBL().removeExpiredBestOffersData();
			log.info(" +===================================+  ");
			log.info(" Expire Best Offers Completed ");
			log.info(" +===================================+  ");
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::removeExpiredBestOffers ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Map<InvDowngradeDTO, Integer> getAvailableBookingClassInventory(Integer flightSegId,
			Collection<String> availableCabinClasses) throws ModuleException {
		Map<InvDowngradeDTO, Integer> invGDSMap = new HashMap<InvDowngradeDTO, Integer>();
		InvDowngradeDTO inventoryTO = null;
		boolean normalBC = false;
		for (String cabinClass : availableCabinClasses) {
			FCCSegInventory fccSegInventory = getFlightInventoryDAO().getFCCSegInventory(flightSegId, cabinClass);
			if (fccSegInventory != null) {
				Collection<FCCSegBCInventory> fccSegBCInventories = getFlightInventoryDAO().getFCCSegBCInventories(
						fccSegInventory.getFccsInvId());
				// Get the first bc inventory
				for (FCCSegBCInventory fccSegBCInventory : fccSegBCInventories) {
					BookingClass bc = getBookingClassDAO().getBookingClass(fccSegBCInventory.getBookingCode());
					if (!bc.getBcType().equals(BookingClass.BookingClassType.OPEN_RETURN)
							&& !bc.getBcType().equals(BookingClass.BookingClassType.STANDBY) && !bc.getFixedFlag()) {
						if (bc.getGdsBCs() != null && !bc.getGdsBCs().isEmpty()) {
							Set<GDSBookingClass> gdsBCs = bc.getGdsBCs();
							for (GDSBookingClass gdsBookingClass : gdsBCs) {
								if (!invGDSMap.containsValue(gdsBookingClass.getGdsId())) {
									inventoryTO = new InvDowngradeDTO();
									inventoryTO.setBookingClass(fccSegBCInventory.getBookingCode());
									inventoryTO.setFccsbInvId(fccSegBCInventory.getFccsbInvId());
									inventoryTO.setTotalFixedSeatsInSegment(fccSegInventory.getFixedSeats());
									inventoryTO.setTotalAllocatedSeatsInSegment(fccSegInventory.getAllocatedSeats());
									inventoryTO.setSoldInfant(fccSegInventory.getSoldInfantSeats());
									inventoryTO.setOnholdInfant(fccSegInventory.getOnholdInfantSeats());
									inventoryTO.setCabinClass(cabinClass);
									invGDSMap.put(inventoryTO, gdsBookingClass.getGdsId());
								}
							}
						} else if (!normalBC) {
							normalBC = true;
							inventoryTO = new InvDowngradeDTO();
							inventoryTO.setBookingClass(fccSegBCInventory.getBookingCode());
							inventoryTO.setFccsbInvId(fccSegBCInventory.getFccsbInvId());
							inventoryTO.setTotalFixedSeatsInSegment(fccSegInventory.getFixedSeats());
							inventoryTO.setTotalAllocatedSeatsInSegment(fccSegInventory.getAllocatedSeats());
							inventoryTO.setSoldInfant(fccSegInventory.getSoldInfantSeats());
							inventoryTO.setOnholdInfant(fccSegInventory.getOnholdInfantSeats());
							inventoryTO.setCabinClass(cabinClass);
							invGDSMap.put(inventoryTO, null);
						}
					}
				}
			}
		}
		return invGDSMap;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, List<Pair<String, Integer>>> getBcAvailabilityForMyIDTravel(List<Integer> flightSegIds, String agentCode)
			throws ModuleException {
		try {
			return getSeatAvailabilityBL().getBcAvailabilityForMyIDTravel(flightSegIds, agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("MyID Seat availability failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param availableFlightSegment
	 * @param availableFlightSearchDTO
	 * @return List of FilteredBucketsDTO
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FilteredBucketsDTO> getSingleFlightOneWayCheapestFare(AvailableFlightSegment availableFlightSegment,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		try {
			return getSeatAvailabilityBL().getSingleFlightOneWayCheapestFare(availableFlightSegment, availableFlightSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Single Flight OneWay Cheapest Fare quoting failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}



	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CalendarSearchRS searchCalendarFares(CalendarSearchRQ calendarSearchRQ) throws ModuleException {
		try {
			return getCalendarSearchBL().getAvailableMinimumFares(calendarSearchRQ);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Calendar flight search failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	private void handleError(Exception ex) throws ModuleException {
		if (ex instanceof ModuleException) {
			throw (ModuleException) ex;
		} else if (ex != null && ex.getCause() instanceof SQLException) {
			int sqlErrorCode = ((SQLException) ex.getCause()).getErrorCode();
			if (sqlErrorCode == 54) {
				throw new ModuleException(ex.getCause(), "module.oracle.jdbc.resourcebusy",
						AirinventoryCustomConstants.INV_MODULE_CODE);
			} else if (sqlErrorCode == 60) {
				throw new ModuleException(ex.getCause(), "module.oracle.jdbc.deadlock",
						AirinventoryCustomConstants.INV_MODULE_CODE);
			} else {
				throw new ModuleException(ex.getCause(), "module.oracle.jdbc.other", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		} else {
			throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	private FlightInventoryResDAO getFlightInventoryResDAO() {
		if (_flightInventoryResDAO == null) {
			_flightInventoryResDAO = (FlightInventoryResDAO) AirInventoryUtil.getInstance().getLocalBean(
					"FlightInventoryResDAOImplProxy");
		}
		return _flightInventoryResDAO;
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		if (flightInventoryDAO == null) {
			flightInventoryDAO = (FlightInventoryDAO) AirInventoryUtil.getInstance().getLocalBean("FlightInventoryDAOImplProxy");
		}
		return flightInventoryDAO;
	}

	private FlightInventoryResBL getFlightInventoryResBL() {
		if (_flightInventoryResBL == null) {
			_flightInventoryResBL = new FlightInventoryResBL();
		}
		return _flightInventoryResBL;
	}

	private CalendarSearchBL getCalendarSearchBL() {
		if (_calendarSearchBL == null) {
			_calendarSearchBL = new CalendarSearchBL();
		}
		return _calendarSearchBL;
	}

	private SeatAvailabilityBL getSeatAvailabilityBL() {
		if (_seatAvailabilityBL == null) {
			_seatAvailabilityBL = new SeatAvailabilityBL();
		}
		return _seatAvailabilityBL;
	}

	private FlightSeatBL getFlightSeatBL() {
		if (_flightSeatBL == null) {
			_flightSeatBL = new FlightSeatBL();
		}
		return _flightSeatBL;
	}

	private MealBL getFlightMealBL() {
		if (_flightMealBL == null) {
			_flightMealBL = new MealBL();
		}
		return _flightMealBL;
	}

	private BaggageBusinessLayer getFlightBaggageBL() {
		if (_flightBaggageBL == null) {
			_flightBaggageBL = new BaggageBusinessLayer();
		}
		return _flightBaggageBL;
	}

	private BestOffersBL getBestOffersBL() {
		if (_bestOffersBL == null) {
			_bestOffersBL = new BestOffersBL();
		}
		return _bestOffersBL;
	}

	private BookingClassDAO getBookingClassDAO() {
		if (bookingClassDAO == null) {
			bookingClassDAO = (BookingClassDAO) AirInventoryUtil.getInstance().getLocalBean("BookingClassDAOImplProxy");
		}
		return bookingClassDAO;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isValidConnectionRoute(String segmentCode) throws ModuleException {
		try {
			return getSeatAvailabilityBL().isValidConnectionRoute(segmentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("MyID Seat availability failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservationAudit releaseAncillaryInventory(Collection<Integer> flightAMSeatIds, Collection<Integer> flightMealIds,
			Collection<Integer> flightBaggageIds) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		try {
			if (flightAMSeatIds != null && flightAMSeatIds.size() > 0) {
				getFlightSeatBL().updateFlightSeats(SMUtil.adaptToAHashMap(flightAMSeatIds),
						AirinventoryCustomConstants.FlightSeatStatuses.VACANT, getUserPrincipal(), reservationAudit);
			}
			if (flightMealIds != null && flightMealIds.size() > 0) {
				getFlightMealBL().updateFlightMeals(flightMealIds, AirinventoryCustomConstants.FlightMealStatuses.VACANT,
						getUserPrincipal(), reservationAudit);
			}

			if (flightBaggageIds != null && flightBaggageIds.size() > 0) {
				getFlightBaggageBL()
						.updateFlightBaggages(flightBaggageIds, AirinventoryCustomConstants.FlightMealStatuses.VACANT);
			}

		} catch (CommonsDataAccessException cdaex) {
			log.error("Release seats failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Release seats failed.", ex);
			this.sessionContext.setRollbackOnly();
			handleError(ex);
		}

		return reservationAudit;
	}

    public AASelectedAncillaryRS getSelectedAncillaryDetails(AASelectedAncillaryRQ aaSelectedAncillaryRQ, Map<String, BundledFareDTO> segmentBundledFareMap, TrackInfoDTO trackInfoDTO) throws ModuleException {
        return new FlightInventoryBL().getSelectedAncillaryDetails(aaSelectedAncillaryRQ, segmentBundledFareMap, trackInfoDTO, getUserPrincipal().getSalesChannel());
    }

}
