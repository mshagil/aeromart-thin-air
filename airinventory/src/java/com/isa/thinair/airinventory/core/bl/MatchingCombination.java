package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
class MatchingCombination {
	/**
	 * 
	 */
	private SeatAvailabilityBL seatAvailBL;
	private List<AvailableIBOBFlightSegment> fltList = new ArrayList<AvailableIBOBFlightSegment>();
	private long minTransitTimeMillis;
	private boolean valid = true;
	private boolean ondSegmentsAdded = false;

	private AvailableIBOBFlightSegment lastFltSegment = null;

	private int expectedOndSequence = 0;

	private String combinationKey = null;

	private boolean suppressOndCheck = false;

	MatchingCombination(SeatAvailabilityBL seatAvailabilityBL, JourneyType journeyType, boolean suppressOndCheck) {
		this(seatAvailabilityBL, journeyType, 0);
		this.suppressOndCheck = suppressOndCheck;
	}

	MatchingCombination(SeatAvailabilityBL seatAvailabilityBL, JourneyType journeyType) {
		this(seatAvailabilityBL, journeyType, 0);
	}

	MatchingCombination(SeatAvailabilityBL seatAvailabilityBL, JourneyType journeyType, int combinationSequence) {
		seatAvailBL = seatAvailabilityBL;
		this.minTransitTimeMillis = 0;
		if (journeyType == JourneyType.ROUNDTRIP) {
			String minRetTransitionTimeStr = CommonsServices.getGlobalConfig()
					.getBizParam(SystemParamKeys.MIN_RETURN_TRANSITION_TIME);
			int hours = Integer.parseInt(minRetTransitionTimeStr.substring(0, minRetTransitionTimeStr.indexOf(":")));
			int mins = Integer.parseInt(minRetTransitionTimeStr.substring(minRetTransitionTimeStr.indexOf(":") + 1));
			this.minTransitTimeMillis = (hours * 60 * 60 * 1000) + (mins * 60 * 1000);

		}
		setCombinationKey(combinationSequence);
	}

	private void setCombinationKey(int combinationSequence) {
		if (combinationSequence > 0) {
			this.combinationKey = "MC" + combinationSequence;
		}
	}

	MatchingCombination(SeatAvailabilityBL seatAvailabilityBL, JourneyType journeyType, AvailableIBOBFlightSegment fltSegment) {
		this(seatAvailabilityBL, journeyType, fltSegment, 0);
	}

	MatchingCombination(SeatAvailabilityBL seatAvailabilityBL, JourneyType journeyType, AvailableIBOBFlightSegment fltSegment,
			int combinationSequence) {
		this(seatAvailabilityBL, journeyType, combinationSequence);
		expectedOndSequence = fltSegment.getOndSequence();
		addFlightSegment(fltSegment);
	}

	private MatchingCombination(SeatAvailabilityBL seatAvailabilityBL, List<AvailableIBOBFlightSegment> fltList,
			long minTransitTimeMillis, int expectedOndSequence, int combinationSequenceNew) {
		seatAvailBL = seatAvailabilityBL;
		this.minTransitTimeMillis = minTransitTimeMillis;
		setCombinationKey(combinationSequenceNew);
		for (AvailableIBOBFlightSegment fltSeg : fltList) {
			addFlightSegment(fltSeg, true);
			if (combinationKey != null) {
				fltSeg.addMatchingCombinationKey(combinationKey);
			}
		}
		this.expectedOndSequence = expectedOndSequence;
	}

	public MatchingCombination clone(int combinationSequenceNew) {
		MatchingCombination mc = new MatchingCombination(seatAvailBL, this.fltList, this.minTransitTimeMillis,
				this.expectedOndSequence, combinationSequenceNew);
		return mc;
	}

	public void addFlightSegment(AvailableIBOBFlightSegment fltSegment) {
		addFlightSegment(fltSegment, this.suppressOndCheck);
		if (combinationKey != null) {
			fltSegment.addMatchingCombinationKey(combinationKey);
		}
	}

	private void addFlightSegment(AvailableIBOBFlightSegment fltSegment, boolean suppressOndCheck) {
		ondSegmentsAdded = true;
		if (valid) {

			if (suppressOndCheck || fltSegment.getOndSequence() == expectedOndSequence) {
				if (fltList.isEmpty()) {
					fltList.add(fltSegment);
					lastFltSegment = fltSegment;
					if (!suppressOndCheck) {
						expectedOndSequence++;
					}
				} else {
					Date date = CalendarUtil.addMilliSeconds(lastFltSegment.getArrivalDate(), minTransitTimeMillis);
					if (date.before(fltSegment.getDepartureDate())) {
						fltList.add(fltSegment);
						lastFltSegment = fltSegment;
						if (!suppressOndCheck) {
							expectedOndSequence++;
						}
					} else {
						valid = false;
						lastFltSegment = null;
						fltList.clear();
					}
				}
			} else {
				valid = false;
				lastFltSegment = null;
				fltList.clear();
			}
		}
	}

	public List<AvailableIBOBFlightSegment> getMatchingFlightList() {
		return fltList;
	}

	public boolean isValid() {
		return valid && ondSegmentsAdded;
	}

	public String getCombinationKey() {
		return combinationKey;
	}
}