/**
 * 
 */
package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airinventory.api.service.MealBD;

@Local
public interface MealBDLocalImpl extends MealBD {

}
