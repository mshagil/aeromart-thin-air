package com.isa.thinair.airinventory.core.util;

import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResJDBCDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class AirInventoryDAOUtils {

	private static Object getDAO(String daoName) {
		String daoFullName = "isa:base://modules/" + AirinventoryConstants.MODULE_NAME + "?id=" + daoName + "Target";
		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(daoFullName);
	}

	private static interface DAONames {
		public static String INVENTORY_RES_DAO = "flightInventoryResJdbcDAO";
	}

	public static FlightInventoryResJDBCDAO getInventoryResDAO() {
		return (FlightInventoryResJDBCDAO) getDAO(AirInventoryDAOUtils.DAONames.INVENTORY_RES_DAO);
	}
}
