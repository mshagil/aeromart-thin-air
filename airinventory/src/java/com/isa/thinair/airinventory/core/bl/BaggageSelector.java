package com.isa.thinair.airinventory.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.baggage.FlightSegmentBaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.ONDBaggageDTO;
import com.isa.thinair.airinventory.api.dto.baggage.ReservationBaggageTo;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.ONDBaggageUtil;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageJDBCDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airschedules.api.dto.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentInfoCriteriaDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentInfoDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.BaggageCriterion;
import com.isa.thinair.commons.api.constants.CommonsConstants.SeatFactorConditionApplyFor;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class BaggageSelector {

	private final Log log = LogFactory.getLog(BaggageSelector.class);

	public static final int EXACT_MATCH_CRITERION_PRIORITY = 1;
	public static final int ALL_CRITERION_PRIORITY = 100;
	public static final int EMPTY_CRITERION_PRIORITY = 101;

	public ReservationBaggageTo selectBaggage(BaggageRq baggageRq) throws ModuleException {

		List<String> baggageCriterionList = AppSysParamsUtil.getBaggageSelectionCriterionByPriority();
		CommonsConstants.BaggageCriterion baggageCriterion;
		ReservationBaggageTo reservationBaggageTo = new ReservationBaggageTo();
		ReservationBaggageTo reservationBaggageToTemp;
		List<FlightSegmentTO> filteredSegs;
		List<FlightSegmentTO> flightSegmentTOs = baggageRq.getFlightSegmentTOs();
		int baggageOndGroupId;
		List<List<FlightSegmentTO>> groupedSegs;
		List<CommonsConstants.BaggageCriterion> baggageCriterionOrder = new ArrayList<CommonsConstants.BaggageCriterion>();

		Map<CommonsConstants.BaggageCriterion, SortedMap<Integer, Set<Integer>>> matchedBaggageIds = new HashMap<CommonsConstants.BaggageCriterion, SortedMap<Integer, Set<Integer>>>();
		SortedMap<Integer, Set<Integer>> criteriaMatchedBaggageIds;
		List<Integer> applicableBaggageTemplateIds;
		ONDBaggageDTO baggageDTO;
		List<FlightSegmentBaggageRq> flightSegs;
		List<Integer> baggageTemplateIds;
		Map<Integer, List<FlightBaggageDTO>> baggageDtos;
		Map<Integer, Map<Integer, FlightBaggageDTO>> selectedBaggage;
		String fltRefNo;

		BaggageJDBCDAO baggageJDBCDAO = AirInventoryModuleUtils.getBaggageJDBCDAO();

		filteredSegs = removeBusSegments(flightSegmentTOs);

		removePastFlights(filteredSegs);

		Collections.sort(filteredSegs, new Comparator<FlightSegmentTO>() {
			public int compare(FlightSegmentTO o1, FlightSegmentTO o2) {
				return o1.getDepartureDateTimeZulu().compareTo(o2.getDepartureDateTimeZulu());
			}
		});

		for (String criterion : baggageCriterionList) {
			if (contains(BaggageCriterion.class, criterion)) {
				baggageCriterion = CommonsConstants.BaggageCriterion.valueOf(criterion);

				// ibe bookings won't consider AGENT
				// TODO -- revise condition check

				// Temp fix for: AARESAA-15884
				if ((baggageCriterion == CommonsConstants.BaggageCriterion.AGENT && baggageRq.getAgent() != null)
						|| baggageCriterion != CommonsConstants.BaggageCriterion.AGENT) {
					if ((baggageCriterion == CommonsConstants.BaggageCriterion.BOOKING_CLASS
							&& baggageRq.getBookingClasses() != null && (!baggageRq.getBookingClasses().isEmpty()))
							|| baggageCriterion != CommonsConstants.BaggageCriterion.BOOKING_CLASS) {
						baggageCriterionOrder.add(baggageCriterion);
					}
				}
			}
		}

		Map<String, List<FlightSegmentTO>> splitsByOndGroupId = splitFlightSegmentTOByBaggageONDGroupId(filteredSegs);
		// new FlightSegmentTOs comes under the key 'NULL'
		for (String ondGroupId : splitsByOndGroupId.keySet()) {

			if (ondGroupId != null && !ondGroupId.isEmpty()) {
				groupedSegs = new ArrayList<List<FlightSegmentTO>>();
				groupedSegs.add(splitsByOndGroupId.get(ondGroupId));
			} else {
				groupedSegs = AirinventoryUtils.splitFlightSegmentTOs(splitsByOndGroupId.get(ondGroupId), baggageRq.isRequote());
//				groupedSegs = AirinventoryUtils.splitFlightSegmentTOs(splitsByOndGroupId.get(ondGroupId));
			}

			for (List<FlightSegmentTO> singleGroup : groupedSegs) {

				try {
					checkPreValidations(singleGroup);
				} catch (ModuleException e) {
					reservationBaggageTo.getBaggage().putAll(getEmptyBaggageResponse(singleGroup).getBaggage());
					continue;
				}

				Map<Integer, List<String>> ondSeqRPHMap = new HashMap<Integer, List<String>>();
				for (FlightSegmentTO flightSegmentTo : singleGroup) {
					if (ondSeqRPHMap.get(flightSegmentTo.getOndSequence()) == null) {
						ondSeqRPHMap.put(flightSegmentTo.getOndSequence(), new ArrayList<String>());
					}

					fltRefNo = flightSegmentTo.getFlightRefNumber().lastIndexOf("-") > 0 ?
							flightSegmentTo.getFlightRefNumber().substring(0, flightSegmentTo.getFlightRefNumber().lastIndexOf("-"))
							: flightSegmentTo.getFlightRefNumber();
					ondSeqRPHMap.get(flightSegmentTo.getOndSequence()).add(fltRefNo);
				}

				baggageDTO = ONDBaggageUtil.getONDBaggageDTO(baggageRq, singleGroup);

				String classOfService = extractClassOfserviceForBaggages(baggageRq, ondSeqRPHMap);
				String logicalCC = extractLogicalCCeForBaggages(baggageRq, ondSeqRPHMap);

				baggageDTO.setClassOfService(classOfService);

				baggageOndGroupId = AirinventoryUtils.getNewBaggageONDGroupId(ondGroupId);
				reservationBaggageToTemp = new ReservationBaggageTo();
				flightSegs = toFlightSegmentBaggageRqs(singleGroup, baggageRq);
				selectedBaggage = getSelectedBaggage(flightSegs);

				for (CommonsConstants.BaggageCriterion criterion : baggageCriterionOrder) {
					criteriaMatchedBaggageIds = baggageJDBCDAO.getBaggageTemplateIds(criterion, baggageDTO);
					matchedBaggageIds.put(criterion, criteriaMatchedBaggageIds);
				}

				applicableBaggageTemplateIds = baggageMatcher(baggageCriterionOrder, matchedBaggageIds);

				level_baggage_template: for (Integer applicableBaggageTemplateId : applicableBaggageTemplateIds) {
					baggageTemplateIds = new ArrayList<Integer>();
					baggageTemplateIds.add(applicableBaggageTemplateId);
					baggageDtos = baggageJDBCDAO.getONDBaggageDTOs(baggageTemplateIds, null, classOfService, logicalCC);
					
					if (baggageDtos.isEmpty()) {
						continue level_baggage_template;
					}

					this.setSeatFactorConditionForBaggages(flightSegs,baggageDtos.get(applicableBaggageTemplateId));

					for (FlightSegmentBaggageRq flightSegmentBaggageRq : flightSegs) {
						reservationBaggageToTemp.putBaggage(flightSegmentBaggageRq.getFlightSegment().getFlightSegId(),
								baggageDtos.get(applicableBaggageTemplateId));
					}
					reservationBaggageToTemp.setCriterionSatisfied(true);

					postProcessBaggage(flightSegs, reservationBaggageToTemp, selectedBaggage);

					if (reservationBaggageToTemp.isCriterionSatisfied()) {
						for (List<FlightBaggageDTO> baggageList : reservationBaggageToTemp.getBaggage().values()) {
							for (FlightBaggageDTO baggage : baggageList) {
								baggage.setOndGroupId(String.valueOf(baggageOndGroupId));
							}
						}
						reservationBaggageTo.getBaggage().putAll(reservationBaggageToTemp.getBaggage());

						break level_baggage_template;
					}
				}

				// all the possibilities are gone
				if (!reservationBaggageToTemp.isCriterionSatisfied()) {
					reservationBaggageTo.getBaggage().putAll(getEmptyBaggageResponse(singleGroup).getBaggage());
					// add selected baggage
					if (!selectedBaggage.isEmpty()) {
						for (FlightSegmentTO flightSegmentTO : singleGroup) {
							if (selectedBaggage.containsKey(flightSegmentTO.getFlightSegId())) {
								reservationBaggageTo.getBaggage().get(flightSegmentTO.getFlightSegId())
										.addAll(selectedBaggage.get(flightSegmentTO.getFlightSegId()).values());
							}
						}
					}
				}
			}
		}

		if (!reservationBaggageTo.getBaggage().isEmpty()) {
			return reservationBaggageTo;
		} else {
			return getEmptyBaggageResponse(filteredSegs);
		}
	}

	private String extractClassOfserviceForBaggages(BaggageRq baggageRq, Map<Integer, List<String>> ondSeqRPHMap) {
		for (Entry<Integer, List<String>> entry : ondSeqRPHMap.entrySet()) {
			List<String> rphList = entry.getValue();
			for (String rph : rphList) {
				String classOfService = baggageRq.getClassesOfService().get(rph);
				if (classOfService != null) {
					return classOfService;
				}
			}
		}
		return null;
	}
	
	private String extractLogicalCCeForBaggages(BaggageRq baggageRq, Map<Integer, List<String>> ondSeqRPHMap) {
		for (Entry<Integer, List<String>> entry : ondSeqRPHMap.entrySet()) {
			List<String> rphList = entry.getValue();
			for (String rph : rphList) {
				if (baggageRq.getLogicalCC() != null) {
					String logicalCC = baggageRq.getLogicalCC().get(rph);
					if (logicalCC != null) {
						return logicalCC;
					}
				}
			}
		}
		return null;
	}

	private boolean contains(Class<? extends Enum> clazz, String val) {
		Object[] arr = clazz.getEnumConstants();
		for (Object e : arr) {
			if (((Enum) e).name().equals(val)) {
				return true;
			}
		}
		return false;
	}

	protected void postProcessBaggage(List<FlightSegmentBaggageRq> flightSegmentBaggageRqs,
			ReservationBaggageTo reservationBaggageTo, Map<Integer, Map<Integer, FlightBaggageDTO>> selectedBaggage)
			throws ModuleException {

		List<FlightBaggageDTO> baggageToRemove;
		boolean isBaggageContains;
		boolean classOfServiceMismatched;
		boolean templateParametersViolated;

		for (FlightSegmentBaggageRq rq : flightSegmentBaggageRqs) {
			baggageToRemove = new ArrayList<FlightBaggageDTO>();
			for (FlightBaggageDTO baggageDTO : reservationBaggageTo.getBaggage().get(rq.getFlightSegment().getFlightSegId())) {

				// res modifications allow them no-matter-what
//				if (!(selectedBaggage.containsKey(rq.getFlightSegment().getFlightSegId()) && selectedBaggage.get(
//						rq.getFlightSegment().getFlightSegId()).containsKey(baggageDTO.getBaggageId()))) {

					classOfServiceMismatched = false;
					if (baggageDTO.getCabinClassCode() != null) {
						classOfServiceMismatched = !baggageDTO.getCabinClassCode().equals(
								rq.getFlightSegment().getCabinClassCode());
					} else if (baggageDTO.getLogicalCCCode() != null) {
						classOfServiceMismatched = !baggageDTO.getLogicalCCCode().equals(
								rq.getFlightSegment().getLogicalCabinClassCode());
					}

					templateParametersViolated = (rq.getTotalWeight().compareTo(baggageDTO.getTotalWeight()) > 0)
							|| (!baggageDTO.isSeatFactorConditionSatisfied());

					if (baggageDTO.getBaggageStatus().equals(Baggage.STATUS_INACTIVE) || classOfServiceMismatched
							|| templateParametersViolated) {
						baggageToRemove.add(baggageDTO);
					}

//				}
			}

			reservationBaggageTo.getBaggage().get(rq.getFlightSegment().getFlightSegId()).removeAll(baggageToRemove);

			if (reservationBaggageTo.getBaggage().get(rq.getFlightSegment().getFlightSegId()).isEmpty()) {
				reservationBaggageTo.setCriterionSatisfied(false);
				return;
			}

			if (selectedBaggage.containsKey(rq.getFlightSegment().getFlightSegId())) {
				for (int baggageId : selectedBaggage.get(rq.getFlightSegment().getFlightSegId()).keySet()) {
					isBaggageContains = false;
					for (FlightBaggageDTO flightBaggageDTO : reservationBaggageTo.getBaggage().get(
							rq.getFlightSegment().getFlightSegId())) {
						if (baggageId == flightBaggageDTO.getBaggageId()) {
							isBaggageContains = true;
							// updates baggage charge to previous amount
							flightBaggageDTO.setAmount(selectedBaggage.get(rq.getFlightSegment().getFlightSegId()).get(baggageId).getAmount());
							break;
						}
					}

					if (!isBaggageContains) {
						reservationBaggageTo.getBaggage().get(rq.getFlightSegment().getFlightSegId())
								.add(selectedBaggage.get(rq.getFlightSegment().getFlightSegId()).get(baggageId));
					}
				}
			}

			for (FlightBaggageDTO baggageDTO : reservationBaggageTo.getBaggage().get(rq.getFlightSegment().getFlightSegId())) {
				baggageDTO.setAvailableWeight(baggageDTO.getTotalWeight().subtract(rq.getTotalWeight()));
			}
		}
	}

	private void setSeatFactorConditionForBaggages(List<FlightSegmentBaggageRq> flightSegments,
			List<FlightBaggageDTO> baggageDtos) {

		FlightSegmentBaggageRq mostRestrictiveFlightSegment = null;
		FlightSegmentBaggageRq firstFlightSegment = null;
		FlightSegmentBaggageRq lastFlightSegmentBaggageRq = null;
		List<FlightSegmentBaggageRq>  cloneFlightSegments = new ArrayList<FlightSegmentBaggageRq>(flightSegments);

		for (FlightBaggageDTO baggageDTO : baggageDtos) {
			if (SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition() == baggageDTO
					.getSeatFactorConditionApplyFor()) {
				if (mostRestrictiveFlightSegment == null) {
					mostRestrictiveFlightSegment = getPreferedFlightSegmentBaggageRq(cloneFlightSegments,
							SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition());
				}
				baggageDTO.setSeatFactorConditionSatisfied(baggageDTO
						.getSeatFactor() > mostRestrictiveFlightSegment.getLoadFactor().intValue());
			} else if (SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition() == baggageDTO
					.getSeatFactorConditionApplyFor()) {
				if (firstFlightSegment == null) {
					firstFlightSegment = getPreferedFlightSegmentBaggageRq(cloneFlightSegments,
							SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition());
				}
				baggageDTO.setSeatFactorConditionSatisfied(baggageDTO.getSeatFactor() > firstFlightSegment.getLoadFactor()
						.intValue());
			} else if (SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition() == baggageDTO
					.getSeatFactorConditionApplyFor()) {
				if (lastFlightSegmentBaggageRq == null) {
					lastFlightSegmentBaggageRq = getPreferedFlightSegmentBaggageRq(cloneFlightSegments,
							SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition());
				}
				baggageDTO.setSeatFactorConditionSatisfied(baggageDTO.getSeatFactor() > lastFlightSegmentBaggageRq
						.getLoadFactor().intValue());
			}
		}
	}

	private FlightSegmentBaggageRq getPreferedFlightSegmentBaggageRq(List<FlightSegmentBaggageRq> flightSegments, int condition) {

		//return the flightSegment with highest Load factor
		if (SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition() == condition) {
			Collections.sort(flightSegments, new Comparator<FlightSegmentBaggageRq>() {
				public int compare(FlightSegmentBaggageRq o1, FlightSegmentBaggageRq o2) {
					return o2.getLoadFactor().compareTo(o1.getLoadFactor());
				}
			});
			return flightSegments.get(0);
		} else if (SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition() == condition) {
			Collections.sort(flightSegments, new Comparator<FlightSegmentBaggageRq>() {
				public int compare(FlightSegmentBaggageRq o1, FlightSegmentBaggageRq o2) {
					return o1.getFlightSegment().getDepartureDateTimeZulu()
							.compareTo(o2.getFlightSegment().getDepartureDateTimeZulu());
				}
			});
			return flightSegments.get(0);
		} else if (SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition() == condition) {
			Collections.sort(flightSegments, new Comparator<FlightSegmentBaggageRq>() {
				public int compare(FlightSegmentBaggageRq o1, FlightSegmentBaggageRq o2) {
					return o2.getFlightSegment().getDepartureDateTimeZulu()
							.compareTo(o1.getFlightSegment().getDepartureDateTimeZulu());
				}
			});
			return flightSegments.get(0);
		}

		return null;
	}
	
	private Map<Integer, Map<Integer, FlightBaggageDTO>> getSelectedBaggage(List<FlightSegmentBaggageRq> flightSegmentBaggageRqs)
			throws ModuleException {
		List<Integer> pnrSegs = new ArrayList<Integer>();
		for (FlightSegmentBaggageRq rq : flightSegmentBaggageRqs) {
			if (rq.getFlightSegment().getPnrSegId() != null) {
				pnrSegs.add(Integer.parseInt(rq.getFlightSegment().getPnrSegId()));
			}
		}

		return AirInventoryModuleUtils.getBaggageJDBCDAO().getSelectedBaggages(pnrSegs);
	}

	private List<FlightSegmentBaggageRq> toFlightSegmentBaggageRqs(List<FlightSegmentTO> flightSegmentTOs, BaggageRq rq) {
		List<FlightSegmentBaggageRq> rqs = new ArrayList<FlightSegmentBaggageRq>();
		Map<Integer, BigDecimal> loadFactors = null;
		Map<Integer, BigDecimal> totalWeight = null;
		FlightSegmentBaggageRq baggageRq;
		List<FlightSegmentInfoDTO> flightSegmentInfo;

		try {
			if (rq.isOwnReservation()) {
				Map<Integer, Set<Integer>> interceptingSegs;
				List<Integer> flightSegIds = new ArrayList<Integer>();
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					flightSegIds.add(flightSegmentTO.getFlightSegId());
				}
				loadFactors = AirInventoryModuleUtils.getLocalFlightInventoryBD().getFlightLoadFactor(flightSegIds);
				interceptingSegs = AirInventoryModuleUtils.getLocalFlightInventoryBD().getInterceptingFlightSegmentIds(
						flightSegIds);
				totalWeight = AirInventoryModuleUtils.getBaggageBD().getTotalBaggageWeight(interceptingSegs);
			} else {
				FlightSegmentInfoCriteriaDTO criteriaDTO = new FlightSegmentInfoCriteriaDTO();
				List<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
				FlightSegmentDTO flightSegmentDTO;
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					flightSegmentDTO = new FlightSegmentDTO();
					flightSegmentDTO.setFlightSegId(flightSegmentTO.getFlightSegId());
					flightSegmentDTO.setOperatingAirline(flightSegmentTO.getOperatingAirline());

					flightSegmentDTOs.add(flightSegmentDTO);
				}
				criteriaDTO.setFlightSegmentDTOs(flightSegmentDTOs);
				criteriaDTO.setCalculateSeatFactor(true);
				criteriaDTO.setClaculateBaggageWeight(true);

				flightSegmentInfo = AirInventoryModuleUtils.getLCCFlightServiceBD().getFlightSegmentInfo(criteriaDTO, null);

				loadFactors = new HashMap<Integer, BigDecimal>();
				totalWeight = new HashMap<Integer, BigDecimal>();
				for (FlightSegmentInfoDTO flightSegmentInfoDTO : flightSegmentInfo) {
					loadFactors.put(flightSegmentInfoDTO.getFlightSegId(), flightSegmentInfoDTO.getLoadFactor());
					totalWeight.put(flightSegmentInfoDTO.getFlightSegId(), flightSegmentInfoDTO.getBaggageWeight());
				}
			}
		} catch (Exception e) {
			log.error("Error occurred while retrieving segment information", e);
		}

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			baggageRq = new FlightSegmentBaggageRq();
			baggageRq.setFlightSegment(flightSegmentTO);
			if (loadFactors != null && loadFactors.containsKey(flightSegmentTO.getFlightSegId())) {
				baggageRq.setLoadFactor(loadFactors.get(flightSegmentTO.getFlightSegId()));
			} else {
				baggageRq.setLoadFactor(BigDecimal.ZERO);
			}
			if (totalWeight != null && totalWeight.containsKey(flightSegmentTO.getFlightSegId())) {
				baggageRq.setTotalWeight(totalWeight.get(flightSegmentTO.getFlightSegId()));
			} else {
				baggageRq.setTotalWeight(BigDecimal.ZERO);
			}
			rqs.add(baggageRq);
		}

		return rqs;
	}

	private List<FlightSegmentTO> removeBusSegments(List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {
		List<FlightSegmentTO> removeLstFlightSegmentTO = new ArrayList<FlightSegmentTO>();
		AirportBD airportBD = AirInventoryModuleUtils.getAirportBD();

		for (FlightSegmentTO flightSeg : flightSegmentTOs) {
			if (airportBD.isBusSegment(flightSeg.getSegmentCode())) {
				removeLstFlightSegmentTO.add(flightSeg);
			}
		}
		if (removeLstFlightSegmentTO.size() > 0) {
			flightSegmentTOs.removeAll(removeLstFlightSegmentTO);
		}

		return flightSegmentTOs;
	}

	// Remove all past flights with baggage selection
	private void removePastFlights(List<FlightSegmentTO> flightSegmentTOs) {
		List<FlightSegmentTO> pastFlights = new ArrayList<FlightSegmentTO>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if ((flightSegmentTO.getBaggageONDGroupId() == null || flightSegmentTO.getBaggageONDGroupId() == "")
					&& CalendarUtil.isLessThan(flightSegmentTO.getDepartureDateTimeZulu(),
							CalendarUtil.getCurrentSystemTimeInZulu())) {
				pastFlights.add(flightSegmentTO);
			}
		}

		if (!pastFlights.isEmpty()) {
			flightSegmentTOs.removeAll(pastFlights);
		}
	}

	private void checkPreValidations(List<FlightSegmentTO> fltSegs) throws ModuleException {
		isBaggageCutOverTimeReached(fltSegs);
	}

	private void isBaggageCutOverTimeReached(List<FlightSegmentTO> fltSegs) throws ModuleException {
		for (IFlightSegment fltSeg : fltSegs) {
			boolean isWithInFinalCutOver = BaggageTimeWatcher.isWithInFinalCutOver(fltSeg.getDepartureDateTimeZulu());

			if (isWithInFinalCutOver) {
				// TODO -- exc code
				throw new ModuleException("");
			}
		}
	}

	private ReservationBaggageTo getEmptyBaggageResponse(List<FlightSegmentTO> fltSegs) {
		ReservationBaggageTo reservationBaggageTo = new ReservationBaggageTo();
		for (FlightSegmentTO flightSegmentTO : fltSegs) {
			reservationBaggageTo.putBaggage(flightSegmentTO.getFlightSegId(), new ArrayList<FlightBaggageDTO>());
		}

		return reservationBaggageTo;
	}

	private Map<String, List<FlightSegmentTO>> splitFlightSegmentTOByBaggageONDGroupId(List<FlightSegmentTO> flightSegs) {

		Map<String, List<FlightSegmentTO>> splits = new HashMap<String, List<FlightSegmentTO>>();

		for (FlightSegmentTO flightSeg : flightSegs) {
			if (!splits.containsKey(flightSeg.getBaggageONDGroupId())) {
				splits.put(flightSeg.getBaggageONDGroupId(), new ArrayList<FlightSegmentTO>());
			}

			splits.get(flightSeg.getBaggageONDGroupId()).add(flightSeg);
		}

		return splits;
	}

	private static Map<Integer, FlightBaggageDTO> getPnrBaggagesMap(List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		Map<Integer, FlightBaggageDTO> fltBaggageMap = new HashMap<Integer, FlightBaggageDTO>();
		List<String> pnrSegIds = new ArrayList<String>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getPnrSegId() != null) {
				pnrSegIds.add(flightSegmentTO.getPnrSegId());
			}
		}

		if (pnrSegIds.size() > 0) {
			List<FlightBaggageDTO> flightBaggageDTOs = AirInventoryModuleUtils.getBaggageJDBCDAO().getPnrBaggages(pnrSegIds);

			for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
				fltBaggageMap.put(flightBaggageDTO.getFlightSegmentID(), flightBaggageDTO);
			}
		}

		return fltBaggageMap;
	}

	public List<Integer> baggageMatcher(final List<CommonsConstants.BaggageCriterion> baggageCriterionOrder,
	// Each Set<Integer> in the SortedMap is with equal priority
	// In List highest priority is at first
	// Map Contains all the matched baggage template IDs which satisfied individually
			Map<CommonsConstants.BaggageCriterion, SortedMap<Integer, Set<Integer>>> matchedBaggageIds) {

		class PriorityBasedComparator implements Comparable<PriorityBasedComparator> {
			private Integer baggageTemplateId;
			private Map<CommonsConstants.BaggageCriterion, Integer> matchedPriorities;

			PriorityBasedComparator() {
				matchedPriorities = new HashMap<CommonsConstants.BaggageCriterion, Integer>();
			}

			public Integer getBaggageTemplateId() {
				return baggageTemplateId;
			}

			public void setBaggageTemplateId(Integer baggageTemplateId) {
				this.baggageTemplateId = baggageTemplateId;
			}

			public Map<CommonsConstants.BaggageCriterion, Integer> getMatchedPriorities() {
				return matchedPriorities;
			}

			public void setMatchedPriorities(Map<CommonsConstants.BaggageCriterion, Integer> matchedPriorities) {
				this.matchedPriorities = matchedPriorities;
			}

			public void putMatchedPriority(CommonsConstants.BaggageCriterion criterion, Integer priority) {
				this.matchedPriorities.put(criterion, priority);
			}

			public int compareTo(PriorityBasedComparator compared) {
				int comparison = 0;
				Integer priority;
				Integer comparedPriority;
				for (CommonsConstants.BaggageCriterion baggageCriterion : baggageCriterionOrder) {
					// lower value is higher priority
					// order by descending order so best match will come first
					priority = matchedPriorities.get(baggageCriterion);
					comparedPriority = compared.getMatchedPriorities().get(baggageCriterion);

					// priority-wise ALL_CRITERION_PRIORITY = EMPTY_CRITERION_PRIORITY
					// 2 values introduced in case a change in the rules ;-)
					if (!((priority.intValue() == EMPTY_CRITERION_PRIORITY || priority.intValue() == ALL_CRITERION_PRIORITY) && (comparedPriority
							.intValue() == EMPTY_CRITERION_PRIORITY || comparedPriority.intValue() == ALL_CRITERION_PRIORITY))) {

						comparison = priority.compareTo(comparedPriority);
						if (comparison != 0) {
							break;
						}
					}

				}
				return comparison;
			}
		}

		// steps ----

		List<PriorityBasedComparator> applicableBaggageIds = new ArrayList<PriorityBasedComparator>();
		PriorityBasedComparator tempComparator;
		List<Integer> prioritizedBaggageIds = new ArrayList<Integer>();
		Set<Integer> addedBaggageIds = new HashSet<Integer>();
		Integer matchingBaggageId = null;
		Iterator<CommonsConstants.BaggageCriterion> baggageCriterionIterator;
		Iterator<CommonsConstants.BaggageCriterion> baggageCriterionIteratorComparing;
		CommonsConstants.BaggageCriterion baggageCriterion;
		CommonsConstants.BaggageCriterion baggageCriterionComparing;
		boolean matches = false;
		boolean ignore;
		List<CommonsConstants.BaggageCriterion> comparingBaggageCriterionOrder;
		comparingBaggageCriterionOrder = baggageCriterionOrder.subList(1, baggageCriterionOrder.size());

		if (!baggageCriterionOrder.isEmpty()) {
			baggageCriterionIterator = baggageCriterionOrder.iterator();
			baggageCriterion = baggageCriterionIterator.next();

			for (int a : matchedBaggageIds.get(baggageCriterion).keySet()) {
				Set<Integer> prioritizedBaggageIds1 = matchedBaggageIds.get(baggageCriterion).get(a);
				level_baggage_ids: for (Integer baggageId1 : prioritizedBaggageIds1) {
					baggageCriterionIteratorComparing = comparingBaggageCriterionOrder.iterator();
					matchingBaggageId = baggageId1;
					matches = true;
					ignore = a == EMPTY_CRITERION_PRIORITY;

					tempComparator = new PriorityBasedComparator();
					tempComparator.putMatchedPriority(baggageCriterion, a);

					level_baggage_criterion: while (baggageCriterionIteratorComparing.hasNext()) {
						matches = false;
						baggageCriterionComparing = baggageCriterionIteratorComparing.next();
						for (int b : matchedBaggageIds.get(baggageCriterionComparing).keySet()) {

							Set<Integer> prioritizedBaggageIds2 = matchedBaggageIds.get(baggageCriterionComparing).get(b);
							tempComparator.putMatchedPriority(baggageCriterionComparing, b);

							if (prioritizedBaggageIds2.contains(matchingBaggageId)) {
								matches = true;
								ignore = ignore && (b == EMPTY_CRITERION_PRIORITY);

								continue level_baggage_criterion;
							}
						}

						if (!matches) {
							continue level_baggage_ids;
						}
					}

					// even though all the criterion are matched if all the values are empty in each criterion don't add
					// them
					if (matches && !ignore && !addedBaggageIds.contains(matchingBaggageId)) {
						tempComparator.setBaggageTemplateId(matchingBaggageId);
						applicableBaggageIds.add(tempComparator);
						addedBaggageIds.add(matchingBaggageId);
					}
				}
			}
		}

		Collections.sort(applicableBaggageIds);
		for (PriorityBasedComparator item : applicableBaggageIds) {
			prioritizedBaggageIds.add(item.getBaggageTemplateId());
		}

		return prioritizedBaggageIds;
	}
}
