package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.BCONDFaresAgentsSummaryDTO;
import com.isa.thinair.airinventory.api.dto.inventory.BCDetailDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.jdbc.extractor.BCONDFaresAgentsSummaryRsExtractor;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * JDBC calls for manipulating Booking Class.
 * 
 * @author Thejaka
 * @author Nasly
 */
public class BookingClassJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements BookingClassJDBCDAO {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String, BCONDFaresAgentsSummaryDTO> getBCONDFaresAgentsSummaryMap(int flightId, String segmentCode,
			String logicalCabinClassCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		Object[] params = {
				new Integer(flightId),
				(segmentCode != null) && !segmentCode.equals("") ? segmentCode : "%",
				new Integer(flightId),
				new java.sql.Timestamp(CalendarUtil.getCurrentSystemTimeInZulu().getTime()),
				Fare.STATUS_ACTIVE,
				BookingClass.PassengerType.ADULT,
				logicalCabinClassCode != null && !logicalCabinClassCode.equals("")
						? logicalCabinClassCode
						: AirinventoryCustomConstants.getAirInventoryConfig().getConstant(
								AirinventoryCustomConstants.DATABASE_ANY_CHARACTER_MATCHER) };
		return (LinkedHashMap<String, BCONDFaresAgentsSummaryDTO>) template.query(
				getQuery(AirinventoryCustomConstants.GET_ALL_BOOKING_CLASSES_FARES_SUMMARY_FOR_FLT_LOGICALCABINCLASS), params,
				new BCONDFaresAgentsSummaryRsExtractor());
	}

	public Object[] getMinimumPublicFare(int flightId, String segmentCode, String logicalCabinClassCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		Object[] params = { segmentCode, new Integer(flightId), segmentCode, logicalCabinClassCode };
		return (Object[]) template.query(getQuery(AirinventoryCustomConstants.GET_ONEWAY_BOOKING_CLASSES_FARES_SEATS_FOR_FLT),
				params, new ResultSetExtractor() {
					Object[] returnArray;
					boolean standardFaresFound = false;
					boolean nonStandardFaresFound = false;

					@Override
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						while (resultSet.next()) {
							String standardCode = resultSet.getString("standard_code");
							if ((standardCode.equals("Y") && !standardFaresFound)
									|| (resultSet.getString("standard_code").equals("N") && !nonStandardFaresFound)) {
								if (returnArray == null || (Double) returnArray[1] > resultSet.getDouble("fare_amount")) {
									standardFaresFound = standardCode.equals("Y");
									nonStandardFaresFound = standardCode.equals("N");
									returnArray = new Object[] { resultSet.getString("booking_code"),
											resultSet.getDouble("fare_amount"), resultSet.getInt("available_seats") };
								}
							}
						}
						return returnArray;
					}
				});
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, boolean[]> getFareRuleAgentCountsLinkedToBCs(Collection<String> bookingCodes) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (HashMap<String, boolean[]>) template.query(
				getQuery("GET_FR_AGENT_COUNTS_LINKED_TO_BCS", new String[] { getSqlInValuesStr(bookingCodes) }),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						HashMap<String, boolean[]> bcDTOs = new HashMap<String, boolean[]>();
						while (rs.next()) {
							bcDTOs.put(rs.getString("booking_code"),
									new boolean[] { rs.getInt("fare_rule_count") > 0 ? true : false,
											rs.getInt("agent_count") > 0 ? true : false });
						}
						return bcDTOs;
					}
				});
	}

	@SuppressWarnings("unchecked")
	public List<String> getBookingClassesLinkedToInv(Collection<String> bookingCodes) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (List<String>) template.query(getQuery("GET_BCs_LINKED_INV", new String[] { getSqlInValuesStr(bookingCodes) }),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<String> bcList = new ArrayList<String>();
						while (rs.next()) {
							bcList.add(rs.getString("booking_code"));
						}
						return bcList;
					}
				});
	}

	@SuppressWarnings("unchecked")
	public Map<String, Collection<String>> getBookingClassCodes(Collection<Integer> gdsIds) {

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		StringBuilder sb = new StringBuilder();
		sb.append("select distinct mapped_bc, gds_id, ranking from t_booking_class_gds_mapping where gds_id in (");
		sb.append(Util.buildIntegerInClauseContent(gdsIds));
		sb.append(") order by ranking");

		Map<String, Collection<String>> mapBookingClassCodes = (Map<String, Collection<String>>) template.query(sb.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, Collection<String>> codeMap = new HashMap<String, Collection<String>>();

						if (rs != null) {
							while (rs.next()) {
								String gdsId = rs.getString("GDS_ID");
								String bookingClass = rs.getString("MAPPED_BC");

								Collection<String> bcList = (Collection<String>) codeMap.get(gdsId);
								if (bcList == null)
									bcList = new ArrayList<String>();

								bcList.add(bookingClass);
								codeMap.put(gdsId, bcList);
							}
						}

						return codeMap;
					}
				});

		return mapBookingClassCodes;
	}

	public List<BCDetailDTO> getBCsForGrouping() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		String sql = "select bc.booking_code, bc.cabin_class_code, bc.logical_cabin_class_code, bc.group_id, "
				+ "(select f_convert_message(msg.message_content) from t_i18n_message msg where "
				+ "lcc.description_i18n_message_key=msg.i18n_message_key and msg.message_locale='en') as description "
				+ "from t_booking_class bc, t_logical_cabin_class lcc where bc.logical_cabin_class_code=lcc.logical_cabin_class_code "
				+ "and lcc.status = 'ACT' and bc.status = 'ACT' and bc.standard_code = 'Y' order by lcc.logical_cabin_class_code, bc.booking_code";

		@SuppressWarnings("unchecked")
		Map<String, BCDetailDTO> bcsForGrouping = (Map<String, BCDetailDTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, BCDetailDTO> bookingClassesForGrouping = new HashMap<String, BCDetailDTO>();

				if (rs != null) {
					while (rs.next()) {
						BCDetailDTO bcTO = new BCDetailDTO();
						bcTO.setBcCode(rs.getString("booking_code"));
						bcTO.setCcCode(rs.getString("cabin_class_code"));
						bcTO.setLogicalCCCode(rs.getString("logical_cabin_class_code"));
						bcTO.setGroupId(rs.getInt("group_id"));
						bcTO.setEditable(true);
						bcTO.setLogicalCCDescription(rs.getString("description"));

						bookingClassesForGrouping.put(rs.getString("booking_code"), bcTO);
					}
				}

				return bookingClassesForGrouping;
			}
		});

		// Make editable false if BC is linked to a Fare rule or for an Agent
		Map<String, boolean[]> linkedBCs = getFareRuleAgentCountsLinkedToBCs(bcsForGrouping.keySet());
		for (Map.Entry<String, boolean[]> linkedBC : linkedBCs.entrySet()) {
			boolean[] linkedFRAgent = linkedBC.getValue();
			for (boolean ntEditable : linkedFRAgent) {
				if (ntEditable && bcsForGrouping.get(linkedBC.getKey()) != null
						&& bcsForGrouping.get(linkedBC.getKey()).getGroupId() != 0) {
					bcsForGrouping.get(linkedBC.getKey()).setEditable(false);
					break;
				}
			}
		}

		// Make editable false if BC is allocated to a flight
		List<String> allocatedBCs = getBookingClassesLinkedToInv(bcsForGrouping.keySet());
		for (String bc : allocatedBCs) {
			if (bcsForGrouping.get(bc) != null && bcsForGrouping.get(bc).getGroupId() != 0) {
				bcsForGrouping.get(bc).setEditable(false);
			}
		}

		List<BCDetailDTO> bcDTOs = new ArrayList<BCDetailDTO>();
		bcDTOs.addAll(bcsForGrouping.values());
		Collections.sort(bcDTOs);
		return bcDTOs;
	}

	public String getBookingClassType(String bookingCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (String) template.queryForObject("SELECT BC_TYPE " + "FROM T_BOOKING_CLASS " + "WHERE BOOKING_CODE='"
				+ bookingCode + "'", String.class);
	}
}