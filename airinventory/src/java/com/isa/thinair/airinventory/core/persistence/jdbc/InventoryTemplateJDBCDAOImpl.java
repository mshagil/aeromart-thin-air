package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airinventory.core.persistence.dao.InventoryTemplateJDBCDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;

/**
 * @author Priyantha
 * 
 */
public class InventoryTemplateJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements InventoryTemplateJDBCDAO {

	private static final org.apache.commons.logging.Log log = LogFactory.getLog(InventoryTemplateJDBCDAOImpl.class);

	@Override
	public DataSource getDataSource() {
		return ((AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig()).getDataSource();
	}


	@Override
	public Page<InvTempCCAllocDTO> getInvTemplCCAllocDataPage(int start, int pageSize, String aircraftModel) {
		StringBuffer query = new StringBuffer();
		query.append("select  cc.cabin_class_code, ");
		query.append(" (select LC.LOGICAL_CABIN_CLASS_CODE from T_LOGICAL_CABIN_CLASS lc ");
		query.append(" where lc.cabin_class_code = CC.CABIN_CLASS_CODE and ");
		query.append(" LC.LOGICAL_CC_RANK in(select min(LOGICAL_CC_RANK)from T_LOGICAL_CABIN_CLASS where cabin_class_code = CC.CABIN_CLASS_CODE)) ");
		query.append(" as logical_cabin_class_code ,ac.seat_capacity,ac.infant_capacity ");
		query.append(" FROM T_AIRCRAFT_CC_CAPACITY ac, T_CABIN_CLASS cc  ");
		query.append(" WHERE ac.model_number = '" + aircraftModel + "' and AC.CABIN_CLASS_CODE = CC.CABIN_CLASS_CODE ");

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		Page<InvTempCCAllocDTO> invTempCCAllocDTOPage = null;

		List<InvTempCCAllocDTO> invTempCCAllocList = (List<InvTempCCAllocDTO>) jt.query(query.toString(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<InvTempCCAllocDTO> invTempCCAllocList = new ArrayList<InvTempCCAllocDTO>();
						while (rs.next()) {
							InvTempCCAllocDTO invTempCCAllocDTO = new InvTempCCAllocDTO();

							invTempCCAllocDTO.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
							invTempCCAllocDTO.setLogicalCabinClassCode(rs.getString("LOGICAL_CABIN_CLASS_CODE"));
							invTempCCAllocDTO.setAdultAllocation(rs.getInt("SEAT_CAPACITY"));
							invTempCCAllocDTO.setInfantAllocation(rs.getInt("INFANT_CAPACITY"));

							invTempCCAllocList.add(invTempCCAllocDTO);
						}
						return invTempCCAllocList;
					}
				});

		invTempCCAllocDTOPage = new Page<InvTempCCAllocDTO>(pageSize, start, start + pageSize, invTempCCAllocList.size(),
				invTempCCAllocList);

		return invTempCCAllocDTOPage;
	}

	@Override
	public ArrayList<InvTempCCBCAllocDTO> getBCDetailsForInvTemp(String ccCode) {
		StringBuffer query = new StringBuffer();
		query.append("SELECT  bc.booking_code,bc.cabin_class_code,bc.standard_code,bc.nest_rank,bc.fixed_flag  from T_BOOKING_CLASS bc  where cabin_class_code = '"
				+ ccCode + "' order by bc.booking_code");

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		@SuppressWarnings("unchecked")
		ArrayList<InvTempCCBCAllocDTO> bCDetails = (ArrayList<InvTempCCBCAllocDTO>) jt.query(query.toString(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						ArrayList<InvTempCCBCAllocDTO> bCDetails = new ArrayList<InvTempCCBCAllocDTO>();
						while (rs.next()) {
							InvTempCCBCAllocDTO newBCDetailDTO = new InvTempCCBCAllocDTO();

							newBCDetailDTO.setBookingCode(rs.getString("BOOKING_CODE"));
							newBCDetailDTO.setCabinClass(rs.getString("CABIN_CLASS_CODE"));
							newBCDetailDTO.setFixFlag(rs.getString("FIXED_FLAG"));
							newBCDetailDTO.setStandardCode(rs.getString("STANDARD_CODE"));
							newBCDetailDTO.setNestRank(rs.getString("NEST_RANK"));

							bCDetails.add(newBCDetailDTO);
						}
						return bCDetails;
					}
				});
		return bCDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<InvTempCCBCAllocDTO> loadGridBCallocDetails(InvTempDTO invTempDTO, String cabinClassCode, String logicalCabinClassCode) {
		StringBuffer query = new StringBuffer();
		query.append("select   cc.inv_template_id,bc.inv_template_cabin_alloc_id,bc.inv_template_cabin_bc_alloc_id ,bc.allocated_seats,bc.booking_code,bc.priority_flag,bc.status ");
		query.append(" from t_inv_template_cabin_bc_alloc bc,t_inv_template iv,t_inv_template_cabin_alloc cc");
		query.append(" where iv.model_number ='" + invTempDTO.getAirCraftModel() + "'");
		if (invTempDTO.getSegment() != null && !invTempDTO.getSegment().isEmpty()) {
			query.append(" and iv.segment_code ='" + invTempDTO.getSegment() + "'");
		} else {
			query.append(" and iv.segment_code is null");
		}
		query.append(" and iv.status ='" + invTempDTO.getStatus() + "'");
		query.append(" and cc.cabin_class_code ='" + cabinClassCode + "'");
		query.append(" and cc.logical_cabin_class_code='" + logicalCabinClassCode + "'");
		query.append(" and iv.inv_template_id=cc.inv_template_id and cc.inv_template_cabin_alloc_id = bc.inv_template_cabin_alloc_id");
		query.append(" and iv.inv_template_id=" + invTempDTO.getInvTempID());

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		List<InvTempCCBCAllocDTO> invTempCCBCAllocDTOs = (List<InvTempCCBCAllocDTO>) jt.query(query.toString(),
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<InvTempCCBCAllocDTO> invTempCCBCAllocDTOs = new ArrayList<InvTempCCBCAllocDTO>();
						while (rs.next()) {
							InvTempCCBCAllocDTO newBCAllocDTO = new InvTempCCBCAllocDTO();

							newBCAllocDTO.setInvTempID(rs.getInt("INV_TEMPLATE_ID"));
							newBCAllocDTO.setAllocatedSeats(rs.getInt("ALLOCATED_SEATS"));
							newBCAllocDTO.setBookingCode(rs.getString("BOOKING_CODE"));
							newBCAllocDTO.setPriorityFlag(rs.getString("PRIORITY_FLAG"));
							newBCAllocDTO.setStatusBcAlloc(rs.getString("STATUS"));
							newBCAllocDTO.setInvTempCabinAllocID(rs.getInt("INV_TEMPLATE_CABIN_ALLOC_ID"));
							newBCAllocDTO.setInvTmpCabinBCAllocID(rs.getInt("INV_TEMPLATE_CABIN_BC_ALLOC_ID"));

							invTempCCBCAllocDTOs.add(newBCAllocDTO);
						}
						return invTempCCBCAllocDTOs;
					}
				});

		return (ArrayList<InvTempCCBCAllocDTO>) invTempCCBCAllocDTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<InvTempCCAllocDTO> getExistInvTempCCAlloPage(int start, int pageSize, String airCraftModel, int invTempID) {
		StringBuffer query = new StringBuffer();
		query.append("select cc.cabin_class_code,cc.logical_cabin_class_code,");
		query.append(" cc.adult_allocation,cc.infant_allocation,cc.inv_template_cabin_alloc_id");
		query.append(" from t_inv_template_cabin_alloc cc where inv_template_id =" + invTempID);
		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		Page<InvTempCCAllocDTO> invTempCCAllocDTOPage = null;
		List<InvTempCCAllocDTO> invTempCCAllocList = (List<InvTempCCAllocDTO>) jt.query(query.toString(),
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<InvTempCCAllocDTO> invTempCCAllocList = new ArrayList<InvTempCCAllocDTO>();
						while (rs.next()) {
							InvTempCCAllocDTO invTempCCAllocDTO = new InvTempCCAllocDTO();

							invTempCCAllocDTO.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
							invTempCCAllocDTO.setLogicalCabinClassCode(rs.getString("LOGICAL_CABIN_CLASS_CODE"));
							invTempCCAllocDTO.setAdultAllocation(rs.getInt("ADULT_ALLOCATION"));
							invTempCCAllocDTO.setInfantAllocation(rs.getInt("INFANT_ALLOCATION"));
							invTempCCAllocDTO.setInvTempCabinAllocID(rs.getInt("INV_TEMPLATE_CABIN_ALLOC_ID"));

							invTempCCAllocList.add(invTempCCAllocDTO);
						}
						return invTempCCAllocList;
					}
				});
		invTempCCAllocDTOPage = new Page<InvTempCCAllocDTO>(pageSize, start, start + pageSize, invTempCCAllocList.size(),
				invTempCCAllocList);
		return invTempCCAllocDTOPage;
	}

}
