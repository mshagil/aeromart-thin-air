/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.FlightSegmentTo;
import com.isa.thinair.airinventory.api.dto.FlightTo;
import com.isa.thinair.airinventory.api.dto.PassengerTo;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.meal.MealTo;
import com.isa.thinair.airinventory.core.persistence.dao.MealJDBCDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.Util;

public class MealJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements MealJDBCDAO {

	@SuppressWarnings("unchecked")
	public List<FlightMealDTO> getMealDTOs(int flightSegID, String cabinClass, String logicalCabinClass,
			Set<String> selectedMeals, PnrFarePassenger pnrFarePax, int salesChannelCode) throws ModuleException {

		boolean filterPaxReservedMeals = false;
		if (pnrFarePax != null) {
			filterPaxReservedMeals = true;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT fmc.flt_seg_id, fmc.flight_meal_charge_id, mc.meal_template_id, mc.meal_charge_id,  ml.meal_id, mc.amount, ml.meal_name, mc.popularity,");
		sb.append("   ml.meal_code, fmc.allocated_meals, fmc.sold_meals, fmc.available_meals, ml.meal_desc,  mc.cabin_class_code, mt.meal_category_id, mt.meal_category,  ml.status ,");
		sb.append(" (select rest.meal_category_id from ML_T_MEAL_TEMPL_RESTRICT_CAT rest where rest.meal_template_id =mtemp.meal_template_id ");
		sb.append(" and rest.meal_category_id =mt.meal_category_id) as restricted ");
		sb.append("    FROM ml_t_flight_meal_charge fmc, ");
		sb.append("   ml_t_meal ml                     , ");
		sb.append("   ml_t_meal_charge mc              , ");
		sb.append("   ml_t_meal_category mt            , ");
		sb.append("   ml_t_meal_template mtemp         , ");
		sb.append("   ml_t_meal_channel mcha             ");

		if (filterPaxReservedMeals) {
			sb.append("   , ml_t_pnr_pax_seg_meal pm  ");
			sb.append("   WHERE pm.pnr_seg_id=").append(pnrFarePax.getPnrSegmentId());
			sb.append(" AND pm.pnr_pax_id    =").append(pnrFarePax.getPnrPaxId());
			sb.append(" AND pm.ppf_id        =").append(pnrFarePax.getPnrPaxFareId());
			sb.append(" AND pm.status        ='RES' ");
			sb.append(" AND pm.flight_meal_charge_id = fmc.flight_meal_charge_id ");
		} else {
			sb.append(" WHERE fmc.status                   = 'ACT' ");
		}

		sb.append(" AND fmc.meal_charge_id       = mc.meal_charge_id ");
		sb.append(" AND mc.meal_id                   = ml.meal_id ");
		sb.append(" AND mc.meal_template_id    = mtemp.meal_template_id");
		sb.append(" AND ml.meal_category_id          = mt.meal_category_id ");
		sb.append(" AND ml.meal_id             = mcha.meal_id ");		
		sb.append(" AND (((fmc.available_meals + fmc.sold_meals) > 0 AND mtemp.status = 'ACT')");
		sb.append(" AND mc.cabin_class_code = '").append(cabinClass).append("' ");
		sb.append(getSelectedMealsCondition(selectedMeals));
		sb.append(" AND fmc.flt_seg_id =").append(flightSegID);
		sb.append(AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "mc"));
		sb.append(" AND mcha.sales_channel_code = '").append(salesChannelCode).append("' ");
		sb.append(" ORDER BY mt.meal_category_id, ");
		sb.append("   ml.meal_name ");

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		List<FlightMealDTO> colMealDtos = (List<FlightMealDTO>) template.query(sb.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightMealDTO> colSeats = new ArrayList<FlightMealDTO>();

				while (rs.next()) {
					FlightMealDTO mealDTO = new FlightMealDTO();
					mealDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					mealDTO.setAmount(rs.getBigDecimal("amount"));
					mealDTO.setMealName(rs.getString("meal_name"));
					mealDTO.setMealId(rs.getInt("meal_id"));
					mealDTO.setAvailableMeals(rs.getInt("available_meals"));
					mealDTO.setAllocatedMeal(rs.getInt("allocated_meals"));
					mealDTO.setSoldMeals(rs.getInt("sold_meals"));
					mealDTO.setChargeId(rs.getInt("meal_charge_id"));
					mealDTO.setMealDescription(rs.getString("meal_desc"));
					mealDTO.setTemplateId(rs.getInt("meal_template_id"));
					mealDTO.setFlightMealId(rs.getInt("flight_meal_charge_id"));
					mealDTO.setMealCode(rs.getString("meal_code"));
					mealDTO.setCabinClassCode(rs.getString("cabin_class_code"));
					mealDTO.setMealCategoryID(rs.getInt("meal_category_id"));
					mealDTO.setMealCategoryCode(rs.getString("meal_category"));
					mealDTO.setMealStatus(rs.getString("status"));
					mealDTO.setCategoryRestricted(rs.getString("restricted") !=null?true:false);
					mealDTO.setPopularity(rs.getString("popularity"));
					colSeats.add(mealDTO);
				}
				return colSeats;
			}
		});

		return colMealDtos;
	}

	@SuppressWarnings("unchecked")
	public List<FlightMealDTO> getMealDTOsWithTranslations(int flightSegID, String cabinClass, String logicalCabinClass,
			String selectedLanguage, Set<String> selectedMeals) throws ModuleException {

		String query = "SELECT fmc.flt_seg_id, fmc.flight_meal_charge_id, mc.meal_template_id, mc.meal_charge_id, ml.meal_id, mc.amount, mc.popularity, "
				+ " ml.meal_name, ml.meal_code, fmc.allocated_meals, fmc.sold_meals, fmc.available_meals, ml.meal_desc, mc.cabin_class_code, "
				+ " md.meal_for_display_id, md.meal_description_ol, md.meal_title_ol , mt.meal_category_id, mt.meal_category, msg.message_content  ,"
				+ "  (select rest.meal_category_id from ML_T_MEAL_TEMPL_RESTRICT_CAT rest where rest.meal_template_id =mc.meal_template_id "
				+ "  and rest.meal_category_id =mt.meal_category_id) as restricted "
				+ " FROM ml_t_flight_meal_charge fmc INNER JOIN ml_t_meal_charge mc ON fmc.meal_charge_id = mc.meal_charge_id "
				+ " AND (fmc.available_meals> 0 "
				+ getSelectedMealsCondition(selectedMeals)
				+ " AND fmc.flt_seg_id= "
				+ flightSegID
				+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "mc")
				+ " AND fmc.status = 'ACT' INNER JOIN ml_t_meal ml ON mc.meal_id= ml.meal_id "
				+ " LEFT OUTER JOIN ml_t_meal_for_display md ON ml.meal_id= md.meal_id AND md.language_code= '"
				+ selectedLanguage
				+ "' INNER JOIN ml_t_meal_category mt ON ml.meal_category_id= mt.meal_category_id"
				+ " LEFT OUTER JOIN T_I18n_message msg"
				+ " ON mt.i18n_message_key = msg.i18n_message_key"
				+ " AND msg.message_locale= '" + selectedLanguage + "' ORDER BY ml.meal_name ";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		List<FlightMealDTO> colMealDtos = (List<FlightMealDTO>) template.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightMealDTO> colSeats = new ArrayList<FlightMealDTO>();

				while (rs.next()) {
					FlightMealDTO mealDTO = new FlightMealDTO();
					mealDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					mealDTO.setAmount(rs.getBigDecimal("amount"));
					mealDTO.setMealName(rs.getString("meal_name"));
					mealDTO.setMealId(rs.getInt("meal_id"));
					mealDTO.setAvailableMeals(rs.getInt("available_meals"));
					mealDTO.setAllocatedMeal(rs.getInt("allocated_meals"));
					mealDTO.setSoldMeals(rs.getInt("sold_meals"));
					mealDTO.setChargeId(rs.getInt("meal_charge_id"));
					mealDTO.setMealDescription(rs.getString("meal_desc"));
					mealDTO.setTemplateId(rs.getInt("meal_template_id"));
					mealDTO.setFlightMealId(rs.getInt("flight_meal_charge_id"));
					mealDTO.setMealCode(rs.getString("meal_code"));
					mealDTO.setCabinClassCode(rs.getString("cabin_class_code"));
					mealDTO.setTranslatedMealTitle(rs.getString("meal_title_ol"));
					mealDTO.setTranslatedMealDescription(rs.getString("meal_description_ol"));
					mealDTO.setMealCategoryID(rs.getInt("meal_category_id"));
					mealDTO.setMealCategoryCode(rs.getString("meal_category"));
					mealDTO.setTranslatedMealCategory(rs.getString("message_content"));
					mealDTO.setCategoryRestricted(rs.getString("restricted") !=null?true:false);
					mealDTO.setPopularity(rs.getString("popularity"));

					colSeats.add(mealDTO);
				}
				return colSeats;
			}
		});

		return getFlightMealsWithoutDuplicate(colMealDtos);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<FlightMealDTO> getMealsByTemplate(Integer templateID, String cabinClass, String logicalCabinClass,
			Integer segmentID, Set<String> existingMeals, int salesChannelCode) {
		String query = "SELECT DISTINCT mc.meal_template_id, DECODE(fmc.available_meals, null, mc.allocated_meals, fmc.available_meals) available_meals, mc.meal_charge_id, ml.meal_id, "
				+ " mc.amount, ml.meal_name, ml.meal_code, mc.popularity, mc.allocated_meals, ml.meal_desc,DECODE(fmc.sold_meals, null, 0, fmc.sold_meals) sold_meals, "
				+ " mc.cabin_class_code, mt.meal_category_id, mt.meal_category ,"
				+ " (select rest.meal_category_id from ML_T_MEAL_TEMPL_RESTRICT_CAT rest where rest.meal_template_id =mc.meal_template_id "
				+ " and rest.meal_category_id =mt.meal_category_id) as restricted "
				+ " FROM ml_t_meal ml "
				+ " join  ml_t_meal_charge mc on mc.meal_id  = ml.meal_id "
				+ " join  ml_t_meal_category mt on mt.meal_category_id = ml.meal_category_id "
				+ " join  ml_t_meal_channel mcha  on ml.meal_id = mcha.meal_id "
				+ " left join ml_t_flight_meal_charge fmc on mc.meal_charge_id = fmc.meal_charge_id and fmc.flt_seg_id=? "
				+ " where mc.status = 'ACT' and mc.meal_template_id=? and ((fmc.available_meals > 0 or fmc.available_meals is null) "
				+ " and mcha.sales_channel_code =? "
				+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "mc")
				+ " "
				+ getSelectedMealsCondition(existingMeals) + " order by mt.meal_category_id, ml.meal_name ";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		List<FlightMealDTO> colMealDtos = (List<FlightMealDTO>) template.query(query, new Object[] { segmentID, templateID, salesChannelCode },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<FlightMealDTO> colSeats = new ArrayList<FlightMealDTO>();

						while (rs.next()) {
							FlightMealDTO mealDTO = new FlightMealDTO();
							mealDTO.setAmount(rs.getBigDecimal("amount"));
							mealDTO.setMealId(rs.getInt("meal_id"));
							mealDTO.setMealCode(rs.getString("meal_code"));

							mealDTO.setMealName(rs.getString("meal_name"));

							int availableMeals = rs.getInt("available_meals");
							mealDTO.setAvailableMeals(rs.wasNull() ? rs.getInt("allocated_meals") : availableMeals);

							mealDTO.setAllocatedMeal(rs.getInt("allocated_meals"));

							int soldMeals = rs.getInt("sold_meals");
							mealDTO.setSoldMeals(rs.wasNull() ? 0 : soldMeals);

							mealDTO.setChargeId(rs.getInt("meal_charge_id"));
							mealDTO.setMealDescription(rs.getString("meal_desc"));
							mealDTO.setTemplateId(rs.getInt("meal_template_id"));

							mealDTO.setCabinClassCode(rs.getString("cabin_class_code"));
							mealDTO.setMealCategoryID(rs.getInt("meal_category_id"));
							mealDTO.setMealCategoryCode(rs.getString("meal_category"));
							mealDTO.setCategoryRestricted(rs.getString("restricted") !=null?true:false);
							mealDTO.setPopularity(rs.getString("popularity"));

							colSeats.add(mealDTO);
						}
						return colSeats;
					}
				});

		return colMealDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<FlightMealDTO> getMealsByTemplateWithTranslations(Integer templateID, String cabinClass,
			String logicalCabinClass, String selectedLanguage, Integer segmentID, Set<String> existingMeals, int salesChannelCode) {
		String query = "SELECT  mc.meal_template_id,"
				+ "DECODE(fmc.available_meals, null, mc.allocated_meals, fmc.available_meals) available_meals,"
				+ "mc.meal_charge_id,"
				+ "ml.meal_id,"
				+ "mc.amount,"
				+ "mc.popularity,"
				+ "ml.meal_name,"
				+ "ml.meal_code,"
				+ "DECODE(fmc.sold_meals, null, 0, fmc.sold_meals) sold_meals,"
				+ "mc.allocated_meals,"
				+ "ml.meal_desc,"
				+ "mc.cabin_class_code,"
				+ "mt.meal_category_id,"
				+ "mt.meal_category,"
				+ "md.meal_for_display_id, md.meal_description_ol, md.meal_title_ol, msg.message_content , "
				+ " (select rest.meal_category_id from ML_T_MEAL_TEMPL_RESTRICT_CAT rest where rest.meal_template_id =mc.meal_template_id "
				+ " and rest.meal_category_id =mt.meal_category_id) as restricted "
				+ " FROM ml_t_meal ml"
				+ " join  ml_t_meal_charge mc on mc.meal_id  = ml.meal_id"
				+ " join  ml_t_meal_category mt on mt.meal_category_id = ml.meal_category_id"
				+ " join  ml_t_meal_channel mcha  on ml.meal_id = mcha.meal_id "
				+ " LEFT OUTER JOIN ml_t_meal_for_display md ON ml.meal_id= md.meal_id AND md.language_code= ?"
				+ " LEFT OUTER JOIN T_I18n_message msg ON mt.i18n_message_key = msg.i18n_message_key AND msg.message_locale= ?"
				+ " left join ml_t_flight_meal_charge fmc on mc.meal_charge_id = fmc.meal_charge_id and fmc.flt_seg_id=?"
				+ " where mc.status = 'ACT' and mc.meal_template_id=? and ((fmc.available_meals > 0 or fmc.available_meals is null) "
				+ " and mcha.sales_channel_code =? "
				+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "mc") + " "
				+ getSelectedMealsCondition(existingMeals) + " order by mt.meal_category_id, ml.meal_name";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		List<FlightMealDTO> colMealDtos = (List<FlightMealDTO>) template.query(query, new Object[] { selectedLanguage,
				selectedLanguage, segmentID, templateID, salesChannelCode }, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightMealDTO> colSeats = new ArrayList<FlightMealDTO>();

				while (rs.next()) {
					FlightMealDTO mealDTO = new FlightMealDTO();
					mealDTO.setAmount(rs.getBigDecimal("amount"));
					mealDTO.setMealId(rs.getInt("meal_id"));
					mealDTO.setMealCode(rs.getString("meal_code"));

					mealDTO.setMealName(rs.getString("meal_name"));

					int availableMeals = rs.getInt("available_meals");
					mealDTO.setAvailableMeals(rs.wasNull() ? rs.getInt("allocated_meals") : availableMeals);

					mealDTO.setAllocatedMeal(rs.getInt("allocated_meals"));

					int soldMeals = rs.getInt("sold_meals");
					mealDTO.setSoldMeals(rs.wasNull() ? 0 : soldMeals);

					mealDTO.setChargeId(rs.getInt("meal_charge_id"));
					mealDTO.setMealDescription(rs.getString("meal_desc"));
					mealDTO.setTemplateId(rs.getInt("meal_template_id"));

					mealDTO.setCabinClassCode(rs.getString("cabin_class_code"));
					mealDTO.setMealCategoryID(rs.getInt("meal_category_id"));
					mealDTO.setMealCategoryCode(rs.getString("meal_category"));

					mealDTO.setTranslatedMealCategory(rs.getString("message_content"));
					mealDTO.setTranslatedMealTitle(rs.getString("meal_title_ol"));
					mealDTO.setTranslatedMealDescription(rs.getString("meal_description_ol"));
					mealDTO.setCategoryRestricted(rs.getString("restricted") !=null?true:false);
					mealDTO.setPopularity(rs.getString("popularity"));

					colSeats.add(mealDTO);
				}
				return colSeats;
			}
		});

		return getFlightMealsWithoutDuplicate(colMealDtos);
	}

	private String getSelectedMealsCondition(Set<String> selectedMeals) {
		String cnd = "";

		if (selectedMeals != null && !selectedMeals.isEmpty()) {
			String strList = Util.constructINStringForCharactors(selectedMeals);
			if (strList != null & !"".equals(strList)) {
				cnd += " OR ml.meal_code in (" + strList + ")";
			}

		}

		return cnd + ") ";
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightMealDTO> getTempleateIds(int flightId, String cos) throws ModuleException {

		String sql = "SELECT distinct(d.meal_template_id), a.flt_seg_id, a.segment_code, c.cabin_class_code,c.logical_cabin_class_code"
				+ " FROM t_flight_segment a, ml_t_flight_meal_charge b, ml_t_meal_charge c, ml_t_meal_template d"
				+ " WHERE a.flight_id="
				+ flightId
				+ "  AND a.flt_seg_id = b.flt_seg_id AND b.meal_charge_id = c.meal_charge_id"
				+ " AND c.meal_template_id = d.meal_template_id and b.status = 'ACT' "
				+ ((cos != null) ? (" AND b.cabin_class_code = '" + cos + "'") : "");

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (Collection<FlightMealDTO>) template.query(sql, new ResultSetExtractor() {
			Collection<FlightMealDTO> colTemplateIds = new ArrayList<FlightMealDTO>();

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					FlightMealDTO flightMealDTO = new FlightMealDTO();
					flightMealDTO.setTemplateId(rs.getInt("meal_template_id"));
					flightMealDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					flightMealDTO.setCabinClassCode(rs.getString("cabin_class_code"));
					flightMealDTO.setLogicalCCCode(rs.getString("logical_cabin_class_code"));
					flightMealDTO.setFlightSegmentCode(rs.getString("segment_code"));
					colTemplateIds.add(flightMealDTO);
				}
				return colTemplateIds;
			}
		});
	}

	private static String getCabinClassCondition(String classOfService) {
		return (classOfService != null && !"".equals(classOfService)) ? (" AND b.cabin_class_code='" + classOfService + "'") : "";
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightMealDTO> getSoldMeals(int flightSegID) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		String sql;
		sb.append(" select b.meal_id, count(b.meal_id) as sold_meal ");
		sb.append(" from ml_t_pnr_pax_seg_meal b ");
		sb.append(" where b.status != 'CNX' and b.meal_id is not null and b.pnr_seg_id in ( ");
		sb.append(" 	select a.pnr_seg_id ");
		sb.append(" 	from t_pnr_segment a ");
		sb.append("  	where a.flt_seg_id = '" + flightSegID + "')");
		sb.append(" group by (b.meal_id)");

		sql = sb.toString();

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (Collection<FlightMealDTO>) template.query(sql, new ResultSetExtractor() {
			Collection<FlightMealDTO> colTemplateIds = new ArrayList<FlightMealDTO>();

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					FlightMealDTO flightMealDTO = new FlightMealDTO();
					flightMealDTO.setMealId(rs.getInt("meal_id"));
					flightMealDTO.setSoldMeals(rs.getInt("sold_meal"));
					colTemplateIds.add(flightMealDTO);
				}
				return colTemplateIds;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightMealDTO> getAvailbleMealDTOs(int flightSegID, Collection<Integer> mealIds) throws ModuleException {

		// TODO if cabin class needed

		String query = "select fmc.flt_seg_id, fmc.flight_meal_charge_id, mc.meal_template_id, mc.meal_charge_id, ml.meal_id, mc.amount, "
				+ " ml.meal_name, fmc.allocated_meals, fmc.sold_meals, fmc.available_meals, ml.meal_desc, fmc.cabin_class_code, mc.popularity"
				+ " from ml_t_flight_meal_charge fmc, ml_t_meal ml, ml_t_meal_charge mc "
				+ " where fmc.meal_charge_id = mc.meal_charge_id "
				+ " and mc.meal_id = ml.meal_id "
				+ " AND fmc.available_meals > 0 "
				+ " and fmc.flt_seg_id = "
				+ flightSegID
				+ " and mc.meal_id in ( "
				+ Util.buildIntegerInClauseContent(mealIds) + ") " + " and fmc.status = 'ACT' " + " order by ml.meal_name";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Collection<FlightMealDTO> colMealDtos = (Collection<FlightMealDTO>) template.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightMealDTO> colSeats = new ArrayList<FlightMealDTO>();

				while (rs.next()) {
					FlightMealDTO mealDTO = new FlightMealDTO();
					mealDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					mealDTO.setAmount(rs.getBigDecimal("amount"));
					mealDTO.setMealName(rs.getString("meal_name"));
					mealDTO.setMealId(rs.getInt("meal_id"));
					mealDTO.setAvailableMeals(rs.getInt("available_meals"));
					mealDTO.setAllocatedMeal(rs.getInt("allocated_meals"));
					mealDTO.setSoldMeals(rs.getInt("sold_meals"));
					mealDTO.setChargeId(rs.getInt("meal_charge_id"));
					mealDTO.setMealDescription(rs.getString("meal_desc"));
					mealDTO.setTemplateId(rs.getInt("meal_template_id"));
					mealDTO.setFlightMealId(rs.getInt("flight_meal_charge_id"));
					mealDTO.setPopularity(rs.getString("popularity"));
					mealDTO.setCabinClassCode(BeanUtils.nullHandler(rs.getString("cabin_class_code")));
					colSeats.add(mealDTO);
				}
				return colSeats;
			}
		});

		return colMealDtos;
	}

	@Override
	public FlightMealDTO checkIfMealExistsForFlight(Integer flightSegID, Integer mealChargeCode) {
	 	String sqlQuery = "select fmc.flight_meal_charge_id, fmc.available_meals,fmc.allocated_meals," 
				+" fmc.sold_meals,fmc.meal_charge_id,mc.amount"
				+" from ml_t_flight_meal_charge fmc inner join ml_t_meal_charge mc"
				+" on  mc.meal_charge_id = fmc.meal_charge_id"
				+" where flt_seg_id=? and mc.meal_charge_id=?";	
				
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		FlightMealDTO mealDTO = (FlightMealDTO) template.query(sqlQuery, new Object[] { flightSegID, mealChargeCode },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						FlightMealDTO mealDTO = null;
						while (rs.next()) {
							mealDTO = new FlightMealDTO();
							mealDTO.setFlightMealId(rs.getInt("flight_meal_charge_id"));
							mealDTO.setAmount(rs.getBigDecimal("amount"));
							mealDTO.setAvailableMeals(rs.getInt("available_meals"));
							mealDTO.setAllocatedMeal(rs.getInt("allocated_meals"));
							mealDTO.setSoldMeals(rs.getInt("sold_meals"));
							mealDTO.setChargeId(rs.getInt("meal_charge_id"));

						}
						return mealDTO;
					}
				});
		return mealDTO;
	}

	@Override
	public FlightMealDTO getMealChargeDetails(Integer templateID, String mealCode) {
		String sqlQuery = "select mc.* from ml_t_meal_charge mc, ml_t_meal ml where mc.meal_template_id= ? "
				+ "and mc.meal_id = ml.meal_id and ml.meal_code = ?";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		FlightMealDTO mealDTO = (FlightMealDTO) template.query(sqlQuery, new Object[] { templateID, mealCode },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						FlightMealDTO mealDTO = new FlightMealDTO();
						while (rs.next()) {
							mealDTO.setAmount(rs.getBigDecimal("amount"));
							mealDTO.setMealId(rs.getInt("meal_id"));
							mealDTO.setAllocatedMeal(rs.getInt("allocated_meals"));
							mealDTO.setChargeId(rs.getInt("meal_charge_id"));
							mealDTO.setCabinClassCode(rs.getString("cabin_class_code"));
							mealDTO.setLogicalCCCode(rs.getString("logical_cabin_class_code"));
							mealDTO.setPopularity(rs.getString("popularity"));
						}
						return mealDTO;
					}
				});
		return mealDTO;
	}

	public List<FlightTo> getMealRecords(List<FlightTo> flights) {

		FlightTo flightTo;
		StringBuilder query = new StringBuilder();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		List<FlightTo> mealRecords;

		query
				.append(" SELECT f.flight_id flight_id, f.flight_number flight_number, fs.est_time_departure_local departure_time, ")
				.append("   fs.flt_seg_id flt_seg_id, fs.segment_code segment_code, p.pnr_pax_id pnr_pax_id, p.pnr pnr, ")
				.append("   p.title title, p.first_name first_name, p.last_name last_name, pm.meal_id meal_id, ")
				.append("   m.meal_code meal_code, m.meal_name meal_name, pm.meal_count meal_count, ams.seat_code seat_code ")
				.append(" FROM t_flight f, t_flight_segment fs , t_pnr_segment rs, sm_t_pnr_pax_seg_am_seat ps, ")
				.append("   sm_t_flight_am_seat fas, sm_t_aircraft_model_seats ams, t_pnr_passenger p, ")
				.append("   ( SELECT pm.pnr_pax_id pnr_pax_id, pm.pnr_seg_id pnr_seg_id, pm.meal_id meal_id, COUNT(pm.meal_id) meal_count ")
				.append("       FROM ml_t_pnr_pax_seg_meal pm ")
				.append("       WHERE pm.status != 'CNX' ")
				.append("       GROUP BY pm.pnr_pax_id, pm.pnr_seg_id, pm.meal_id ) pm, ml_t_meal m ")
				.append(" WHERE f.flight_id = fs.flight_id AND fs.flt_seg_id = rs.flt_seg_id (+) ")
				.append("   AND rs.pnr_seg_id = pm.pnr_seg_id (+) AND pm.meal_id = m.meal_id (+) AND rs.pnr_seg_id = ps.pnr_seg_id (+) ")
				.append("   AND ps.flight_am_seat_id = fas.flight_am_seat_id (+) AND fas.am_seat_id = ams.am_seat_id (+) ")
				.append("   AND pm.pnr_pax_id = p.pnr_pax_id (+) AND rs.status != 'CNX' AND (ps.status IS NULL OR ps.status != 'CNX') AND ")
				.append(" (");

		for (int a = 0; a < flights.size(); ) {
			flightTo = flights.get(a);
			query.append(" (f.flight_number = '" + flightTo.getFlightNumber() + "' AND ").append(
					" trunc(fs.est_time_departure_local) = to_date ( '" + dateFormat.format(flightTo.getDepartureDate())
							+ "', 'dd-mm-yyyy') ) ");

			if (++a < flights.size()) {
				query.append(" OR ");
			}
		}
		query.append(" ) ");

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		mealRecords = (List<FlightTo>)template.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				FlightTo flight;
				FlightSegmentTo flightSegment;
				PassengerTo passenger;
				MealTo meal;

				String flightId;
				String segmentId;
				String pnrPaxid;
				List<FlightTo> meals = new ArrayList<FlightTo>();
				// map < flight-id , flight >
				Map<String, FlightTo> flightToMap = new HashMap<String, FlightTo>();
				// map <flight-seg-id , flight-segment>
				Map<String, FlightSegmentTo> flightSegmentToMap = new HashMap<String, FlightSegmentTo>();
				// map <flight-seg-id , map <pnr-pax-id , passenger>>
				Map<String, Map<String, PassengerTo>> passengerToMap = new HashMap<String, Map<String, PassengerTo>>();

				while (resultSet.next()) {
					flightId = resultSet.getString("flight_id");
					segmentId = resultSet.getString("flt_seg_id");
					pnrPaxid = resultSet.getString("pnr_pax_id") ;

					if (!flightToMap.containsKey(flightId)) {
						flight = new FlightTo();
						flight.setFlightNumber(resultSet.getString("flight_number"));
						flight.setDepartureDate(resultSet.getTimestamp("departure_time"));
						flightToMap.put(flightId, flight);
						meals.add(flight);
					}
					flight = flightToMap.get(flightId);

					if (!flightSegmentToMap.containsKey(segmentId)) {
						flightSegment = new FlightSegmentTo();
						flightSegment.setSegmentCode(resultSet.getString("segment_code"));

						flightSegmentToMap.put(segmentId, flightSegment);
						flight.addFlightSegment(flightSegment);

						// adding to pax map as well
						passengerToMap.put(segmentId, new HashMap<String, PassengerTo>());
					}
					flightSegment = flightSegmentToMap.get(segmentId);

					if (pnrPaxid != null) {
						if (!passengerToMap.get(segmentId).containsKey(pnrPaxid)) {
							passenger = new PassengerTo();
							passenger.setPnr(resultSet.getString("pnr"));
							passenger.setTitle(resultSet.getString("title"));
							passenger.setFirstName(resultSet.getString("first_name"));
							passenger.setLastName(resultSet.getString("last_name"));
							passenger.setSeatCode(resultSet.getString("seat_code"));

							passengerToMap.get(segmentId).put(pnrPaxid, passenger);
							flightSegment.addPassenger(passenger);
						}
						passenger = passengerToMap.get(segmentId).get(pnrPaxid);

						meal = new MealTo();
						meal.setMealCode(resultSet.getString("meal_code"));
						meal.setMealName(resultSet.getString("meal_name"));
						meal.setQuantity(resultSet.getInt("meal_count"));

						passenger.addMeal(meal);
					}
				}

				return meals;
			}
		});
		return mealRecords;
	}

	@Override
	public Map<Integer, Set<String>> getExistingMeals(List<Integer> pnrSegIds) throws ModuleException {
		Map<Integer, Set<String>> existingMeals;
		String sqlQuery = " SELECT DISTINCT S.FLT_SEG_ID FLT_SEG_ID, M.MEAL_CODE MEAL_CODE "
				+ " FROM ML_T_PNR_PAX_SEG_MEAL PM, ML_T_MEAL m, T_PNR_SEGMENT S "
				+ " WHERE PM.PNR_SEG_ID = S.PNR_SEG_ID AND pm.MEAL_ID = m.MEAL_ID "
				+ " AND PM.STATUS = 'RES' AND S.PNR_SEG_ID IN ( " + Util.buildIntegerInClauseContent(pnrSegIds) + " ) ";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		if (!pnrSegIds.isEmpty()) {
			existingMeals = (Map<Integer, Set<String>>) template.query(sqlQuery, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Map<Integer, Set<String>> existingMeals = new HashMap<Integer, Set<String>>();
					Integer flightSegId;
					String mealCode;
					while (rs.next()) {
						flightSegId = rs.getInt("FLT_SEG_ID");
						mealCode = rs.getString("MEAL_CODE");

						if (!existingMeals.containsKey(flightSegId)) {
							existingMeals.put(flightSegId, new HashSet<String>());
						}

						existingMeals.get(flightSegId).add(mealCode);

					}
					return existingMeals;
				}
			});
		} else {
			existingMeals = new HashMap<Integer, Set<String>>();
		}
		return existingMeals;
	}
	
	@SuppressWarnings("unchecked")
	public List<FlightMealDTO> getMealDTOs() throws ModuleException {
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ml.meal_id, ");
		sb.append(" ml.meal_name, ");
		sb.append(" ml.meal_code, ");
		sb.append(" ml.meal_desc, ");
		sb.append(" mlc.meal_category, ");
		sb.append(" ml.meal_category_id, ");
		sb.append(" ml.status ");
		sb.append("FROM ml_t_meal ml ");
		sb.append("INNER JOIN ml_t_meal_category mlc ");
		sb.append("ON ml.meal_category_id   = mlc.meal_category_id ");
		sb.append("WHERE ml.status          = 'ACT' ");
		sb.append("ORDER BY ml.meal_category_id, ml.meal_name ");

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		List<FlightMealDTO> colMealDtos = (List<FlightMealDTO>) template.query(sb.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightMealDTO> colSeats = new ArrayList<FlightMealDTO>();

				while (rs.next()) {
					FlightMealDTO mealDTO = new FlightMealDTO();
					mealDTO.setMealName(rs.getString("meal_name"));
					mealDTO.setMealId(rs.getInt("meal_id"));
					mealDTO.setAvailableMeals(10);
					mealDTO.setAllocatedMeal(0);
					mealDTO.setSoldMeals(0);
					mealDTO.setMealDescription(rs.getString("meal_desc"));
					mealDTO.setMealCode(rs.getString("meal_code"));
					mealDTO.setMealCategoryID(rs.getInt("meal_category_id"));
					mealDTO.setMealCategoryCode(rs.getString("meal_category"));
					mealDTO.setMealStatus(rs.getString("status"));
					colSeats.add(mealDTO);
				}
				return colSeats;
			}
		});

		return colMealDtos;
	}
	
	private List<FlightMealDTO> getFlightMealsWithoutDuplicate(List<FlightMealDTO> meals) {		
		return meals.stream().distinct().collect(Collectors.toList());
	}
}
