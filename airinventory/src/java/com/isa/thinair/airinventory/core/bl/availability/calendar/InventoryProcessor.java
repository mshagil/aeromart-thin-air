package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.isa.thinair.airinventory.api.dto.seatavailability.LightBCAvailability;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.SeatAvailabilityDAOJDBC;
import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airpricing.api.dto.LightFareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airschedules.api.dto.AvailableONDFlight;
import com.isa.thinair.airschedules.api.dto.LogicalCabinAvailability;
import com.isa.thinair.airschedules.api.dto.SegmentInvAvailability;
import com.isa.thinair.commons.core.util.CalendarUtil;

class InventoryProcessor {

	private List<AvailableONDFlight> ondFlights;
	private LightFareDTO fare;
	private IPaxCountAssembler paxAssmblr;

	public InventoryProcessor(IPaxCountAssembler paxAssmblr) {
		this.paxAssmblr = paxAssmblr;
	}

	public InventoryProcessor(IPaxCountAssembler paxAssmblr, LightFareDTO fare, List<AvailableONDFlight> ondFlights) {
		this.fare = fare;
		this.ondFlights = ondFlights;
		this.paxAssmblr = paxAssmblr;
	}

	public AvailableONDFlight reduceToMatchingFlight() {
		AvailableONDFlight matchingFlight = null;
		for (AvailableONDFlight flight : ondFlights) {
			if (hasValidInventory(flight, this.fare)) {
				matchingFlight = flight;
				break;
			}
		}
		return matchingFlight;
	}

	public boolean hasValidInventory(AvailableONDFlight ondFlight, LightFareDTO fare) {
		for (FlightSegmentTO segment : ondFlight.getFlightSegments()) {
			if (!hasValidInventory(segment, ondFlight.getSegmentInventoryAvailability(segment), fare)) {
				return false;
			}
		}
		if (ondFlight.getFlightSegments().size() > 0) {
			return true;
		}
		return false;
	}

	public boolean hasValidInventory(FlightSegmentTO segment, SegmentInvAvailability segInv, LightFareDTO fare) {
		if (!isSegmentIsEligible(fare, segment, segInv)) {
			return false;
		}
		if (!isFareRulesMatching(fare, segment)) {
			return false;
		}
		LightBCAvailability bcAvail = getSeatAvailabilityDAO().getSegmentBCAvailability(fare.getBookingCode(),
				fare.isStandardBC(), segment.getFlightSegId());
		if (isAvailable(bcAvail)) {
			return true;
		}
		return false;
	}

	private boolean isSegmentIsEligible(LightFareDTO fare, IFlightSegment flight, SegmentInvAvailability segmentInvAvailability) {
		LogicalCabinAvailability lccAvail = segmentInvAvailability.getAvailabilityFor(fare.getLogicalCabinClass());
		if (lccAvail == null || !isLoadFactorWithinRange(fare, lccAvail.getLoadFactor())) {
			return false;
		}
		return true;
	}

	private boolean isLoadFactorWithinRange(LightFareDTO fare, double loadFactor) {
		if ((fare.getFareRule().getLoadFactorMin() == null || fare.getFareRule().getLoadFactorMin() <= loadFactor)
				&& (fare.getFareRule().getLoadFactorMax() == null || fare.getFareRule().getLoadFactorMax() >= loadFactor)) {
			return true;
		}
		return false;
	}

	private boolean isFareRulesMatching(LightFareDTO fare, IFlightSegment segment) {
		if (!fare.isWithinOutboundValidity(segment.getDepartureDateTimeZulu())) {
			return false;
		}

		LightFareRuleDTO fareRule = fare.getFareRule();
		long datesToDeparture = getMinutesToDeparture(segment);
		if (fareRule.getAdvanceBookingDays() != null && (datesToDeparture < fareRule.getAdvanceBookingDays())) {
			return false;
		}

		if (!fareRule.getDepartureHourValidity().isValid(segment.getDepartureDateTimeZulu())) {
			return false;
		}
		if (!fareRule.getDepartureWeekFrequency().isValid(CalendarUtil.getCalendar(segment.getDepartureDateTimeZulu()))) {
			return false;
		}

		return true;
	}

	private long getMinutesToDeparture(IFlightSegment segment) {
		long difference = segment.getDepartureDateTimeZulu().getTime() - CalendarUtil.getCurrentSystemTimeInZulu().getTime();
		return TimeUnit.MINUTES.convert(difference, TimeUnit.MILLISECONDS);
	}

	public boolean isAvailable(LightBCAvailability bcAvail) {
		return bcAvail != null && (paxAssmblr.getAdultCount() + paxAssmblr.getChildCount()) <= bcAvail.getAvailability();
	}

	private SeatAvailabilityDAOJDBC getSeatAvailabilityDAO() {
		return (SeatAvailabilityDAOJDBC) AirInventoryUtil.getInstance().getLocalBean("seatAvailabilityDAOJDBC");
	}

}