package com.isa.thinair.airinventory.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.baggage.ONDBaggageDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author mano
 */
public interface BaggageJDBCDAO {

	List<FlightBaggageDTO> getBaggageDTOs(int flightSegId, String cabinClass, String logicalCabinClass) throws ModuleException;

	List<FlightBaggageDTO> getBaggageDTOsWithTranslations(int flightSegId, String cabinClass, String logicalCabinClass,
			String selectedLanguage) throws ModuleException;

	public Collection<FlightBaggageDTO> getTempleateIds(int flightId) throws ModuleException;

	public Collection<FlightBaggageDTO> getSoldBaggages(int flightSegID) throws ModuleException;

	public Collection<FlightBaggageDTO> getAvailbleBaggageDTOs(int flightSegID, Collection<Integer> baggageIds)
			throws ModuleException;

	public List<FlightBaggageDTO> getONDBaggageDTOs(ONDBaggageDTO ondBaggageDTO) throws ModuleException;

	public Map<Integer, List<FlightBaggageDTO>> getONDBaggageDTOs(List<Integer> templateIds, List<Integer> templateChargeId, String cabinClassCode, String logicalCC)
			throws ModuleException;

	public Integer getNextBaggageOndGroupId();

	public List<FlightBaggageDTO> getPnrBaggages(List<String> colPnrSegIds) throws ModuleException;

	public List<FlightBaggageDTO> getSelectedBaggages(String pnrSegId) throws ModuleException;

	public Map<Integer, Map<Integer, FlightBaggageDTO>> getSelectedBaggages(List<Integer> pnrSegIds) throws ModuleException;

	/**
	 * Retrieves Baggage information from the Baggage charge template directly. No associated flights are necessary.
	 * 
	 * @param templateID
	 *            Baggage charge template ID of the meals to be retrieved.
	 * @param cabinClassCode TODO
	 * @return The Baggages attached to the Baggage charge template. Or an empty collection if the templateID is
	 *         invalid.
	 * @throws ModuleException
	 *             If any of the underlying data access operations fail.
	 */
	public List<FlightBaggageDTO> getBaggageByTemplateID(Integer templateID, String cabinClassCode) throws ModuleException;

	public List<FlightBaggageDTO> getBaggageByTemplateIDWithTranslations(Integer templateID, String languageCode, String cabinClassCode)
			throws ModuleException;

	public Map<Integer, BigDecimal> getTotalBaggageWeight(Map<Integer, Set<Integer>> interceptingFlightSegIds);

	public Integer getBaggageTemplateId(CommonsConstants.BaggageCriterion criterion, ONDBaggageDTO baggageDTO)
			throws ModuleException;

	public SortedMap<Integer, Set<Integer>> getBaggageTemplateIds(CommonsConstants.BaggageCriterion criterion, ONDBaggageDTO baggageDTO)
			throws ModuleException;
}