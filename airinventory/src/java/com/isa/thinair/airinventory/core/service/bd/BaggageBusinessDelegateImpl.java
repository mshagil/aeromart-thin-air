/**
 * 	mano
	May 9, 2011 
	2011
 */
package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;

/**
 * @author mano
 * 
 */

@Remote
public interface BaggageBusinessDelegateImpl extends BaggageBusinessDelegate {

}
