package com.isa.thinair.airinventory.core.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableBCAllocationSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareIdDetailDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketCheapestBCDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.HRTAvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.core.bl.IFareCalculator.FareCalType;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.Util;

class GroupedOndFlightsFareCalculator {

	private final List<AvailableIBOBFlightSegment> availableIBOBFlightSegments = new ArrayList<AvailableIBOBFlightSegment>();

	private AvailableFlightSearchDTO availableFlightSearchDTO;

	private SeatAvailabilityBL seatAvailabilityBL;

	private boolean minimumFareSet = false;

	private boolean allOndsHasConnectedFlights = true;

	private List<IFareCalculator.FareCalType> systemSupportedFareTypes;

	public boolean isAllOndHasConnectedFlights() {
		return allOndsHasConnectedFlights;
	}

	/**
	 * @return the minimumFareSet
	 */
	public boolean isMinimumFareSet() {
		return minimumFareSet;
	}

	/**
	 * @param minimumFareSet
	 *            the minimumFareSet to set
	 */
	public void setMinimumFareSet(boolean minimumFareSet) {
		this.minimumFareSet = minimumFareSet;
	}

	public GroupedOndFlightsFareCalculator(AvailableFlightSearchDTO availableFlightSearchDTO,
			List<AvailableIBOBFlightSegment> availableIBOBFlightSegments,
			List<IFareCalculator.FareCalType> systemSupportedFareTypes) {
		this.availableFlightSearchDTO = availableFlightSearchDTO;
		this.systemSupportedFareTypes = systemSupportedFareTypes;
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableIBOBFlightSegments) {
			this.availableIBOBFlightSegments.add(availableIBOBFlightSegment);
			allOndsHasConnectedFlights = allOndsHasConnectedFlights && !availableIBOBFlightSegment.isDirectFlight();
		}
	}

	public AvailableIBOBFlightSegment getAvailableOndFlight(Integer ondSequence) {
		if (ondSequence < availableIBOBFlightSegments.size()) {
			return availableIBOBFlightSegments.get(ondSequence);
		}
		return null;
	}

	public List<AvailableIBOBFlightSegment> getAvailableIBOBFlightSegments() {
		return availableIBOBFlightSegments;
	}

	public List<AvailableFlightSegment> getAllAvailableFlightSegments() {
		List<AvailableFlightSegment> availableFlightSegments = new ArrayList<AvailableFlightSegment>();
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableIBOBFlightSegments) {
			if (availableIBOBFlightSegment.isDirectFlight()) {
				availableFlightSegments.add((AvailableFlightSegment) availableIBOBFlightSegment);
			} else {
				for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableIBOBFlightSegment)
						.getAvailableFlightSegments()) {
					availableFlightSegments.add(availableFlightSegment);
				}
			}
		}
		return availableFlightSegments;
	}

	public List<AvailableFlightSegment> getAvailableFlightSegments(Integer ondSequence) {
		List<AvailableFlightSegment> availableFlightSegments = new ArrayList<AvailableFlightSegment>();
		if (isFlightsAvailableForOnd(ondSequence)) {
			AvailableIBOBFlightSegment availableIBOBFlightSegment = availableIBOBFlightSegments.get(ondSequence);
			if (availableIBOBFlightSegment.isDirectFlight()) {
				availableFlightSegments.add((AvailableFlightSegment) availableIBOBFlightSegment);
			} else {
				for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableIBOBFlightSegment)
						.getAvailableFlightSegments()) {
					availableFlightSegments.add(availableFlightSegment);
				}
			}
		}
		return availableFlightSegments;
	}

	public void addAvailableIBOBFlightSegment(Integer ondSequence, AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		availableIBOBFlightSegments.add(ondSequence, availableIBOBFlightSegment);
	}

	public AvailableFlightSearchDTO getAvailableFlightSearchDTO() {
		return availableFlightSearchDTO;
	}

	public void setAvailableFlightSearchDTO(AvailableFlightSearchDTO availableFlightSearchDTO) {
		this.availableFlightSearchDTO = availableFlightSearchDTO;
	}

	private boolean isFlightsAvailableForOnd(Integer... ondSequences) {
		for (Integer ondSequence : ondSequences) {
			if (ondSequence >= availableIBOBFlightSegments.size()) {
				return false;
			}
		}
		return true;
	}

	private String getRequestedLogicalCabinClass(int ondSequence) {
		if (availableFlightSearchDTO.getOndInfo(ondSequence) != null) {
			return availableFlightSearchDTO.getOndInfo(ondSequence).getPreferredLogicalCabin();
		}
		return null;
	}

	/**
	 * Gets the Fare Calculator list based on the given FareCalType s.
	 * 
	 * @param fareTypes
	 *            Array of fare calculator types to be considered.
	 * @return List if Fare calculators implementing IFareCalculator.
	 */
	private List<IFareCalculator> getFareCalList(IFareCalculator.FareCalType[] fareTypes) {
		List<IFareCalculator> fareCalList = new ArrayList<IFareCalculator>();

		boolean excludeSegFaresForConAndRet = AppSysParamsUtil.isExcludeSegFareFromConAndRet()
				&& ((availableFlightSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP
						&& getAvailableIBOBFlightSegments().size() == 2) || this.isAllOndHasConnectedFlights());

		boolean excludeOneWayFaresFromConRet = AppSysParamsUtil.isExcludeOneWayFareFromConRet()
				&& (availableFlightSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP && this.isAllOndHasConnectedFlights());

		if (fareTypes != null) {
			Set<FareCalType> validFareTypes = new HashSet<IFareCalculator.FareCalType>();
			for (FareCalType type : fareTypes) {
				if (this.systemSupportedFareTypes.contains(type)) {
					validFareTypes.add(type);
					if (type == FareCalType.HRT) {
						fareCalList.add(new HalfReturnFareCal(this, availableFlightSearchDTO));
					} else if (type == FareCalType.RT) {
						fareCalList.add(new ReturnFareCal(this, availableFlightSearchDTO,
								getOutboundInboundAvailableLogicalCabinClasses(), getAvailableIBOBFlightSegments().size()));
					} else if (!(excludeSegFaresForConAndRet && FareCalType.SEG.equals(type))) {
						fareCalList.add(new MixFareCal(this, availableFlightSearchDTO, type));
					} else {
						validFareTypes.remove(type);
					}
				}
			}
			if (validFareTypes.contains(FareCalType.SEG) && validFareTypes.contains(FareCalType.CON)) {
				fareCalList.add(new MixFareCal(this, availableFlightSearchDTO, FareCalType.CON, FareCalType.SEG));
			}
		} else {
			if (!availableFlightSearchDTO.isOpenReturnSearch() && !excludeOneWayFaresFromConRet) {
				if (excludeSegFaresForConAndRet && this.systemSupportedFareTypes.contains(FareCalType.CON)) {
					fareCalList.add(new MixFareCal(this, availableFlightSearchDTO, FareCalType.CON));
				} else {
					if (this.systemSupportedFareTypes.contains(FareCalType.CON)
							&& this.systemSupportedFareTypes.contains(FareCalType.SEG)) {
						if (AppSysParamsUtil.isFareBiasingEnabled()) {
							fareCalList.add(new MixFareCal(this, availableFlightSearchDTO, FareCalType.CON));
						}
						fareCalList.add(new MixFareCal(this, availableFlightSearchDTO, FareCalType.CON, FareCalType.SEG));
					}

					if (this.systemSupportedFareTypes.contains(FareCalType.CON)
							&& !this.systemSupportedFareTypes.contains(FareCalType.SEG)) {
						fareCalList.add(new MixFareCal(this, availableFlightSearchDTO, FareCalType.CON));
					}

					if (!this.systemSupportedFareTypes.contains(FareCalType.CON)
							&& this.systemSupportedFareTypes.contains(FareCalType.SEG)) {
						fareCalList.add(new MixFareCal(this, availableFlightSearchDTO, FareCalType.SEG));
					}
				}
			}

			if (this.systemSupportedFareTypes.contains(FareCalType.HRT)) {
				fareCalList.add(new HalfReturnFareCal(this, availableFlightSearchDTO));
			}

			if (this.systemSupportedFareTypes.contains(IFareCalculator.FareCalType.RT)) {
				fareCalList.add(new ReturnFareCal(this, availableFlightSearchDTO,
						getOutboundInboundAvailableLogicalCabinClasses(), getAvailableIBOBFlightSegments().size()));
			}

		}

		return fareCalList;
	}

	private boolean isFareTypeRequired(List<IFareCalculator> fareCalList, FareCalType fareType) {
		for (IFareCalculator fareCal : fareCalList) {
			if (fareCal.isValidForJourney()) {
				if (fareCal.dependantFareTypes().contains(fareType)) {
					return true;
				}
			}
		}
		return false;
	}

	private void fillHRTFlags(HRTAvailableFlightSearchDTO availDTO, AvailableIBOBFlightSegment current, int currentIndex)
			throws ModuleException {
		List<AvailableIBOBFlightSegment> ibobFltList = getAvailableIBOBFlightSegments();

		AvailableIBOBFlightSegment ob = null;
		AvailableIBOBFlightSegment ib = null;

		Map<Integer, Boolean> ondSequenceReturnMap = availDTO.getOndSequenceReturnMap();

		if (currentIndex == ibobFltList.size() - 1) {
			ob = ibobFltList.get(0);
			ib = current;
		} else {
			ob = current;
			ib = ibobFltList.get(currentIndex + 1);
		}

		if (ob != null) {
			availDTO.setFQOBDepartureDate(ob.getDepartureDate());
			availDTO.setFQOBDepartureDateZulu(ob.getDepartureDateZulu());
			availDTO.setFQOBArrivalDate(ob.getArrivalDate());
			availDTO.setFQOnd(ob.getOndCode());
		}

		// for open jaw jounrney
		if (ondSequenceReturnMap != null && ondSequenceReturnMap.size() > 1) {
			boolean isReturn = ondSequenceReturnMap.get(current.getOndSequence());
			if (isReturn) {
				availDTO.setFQOnd(Util.reverseOndCode(current.getOndCode()));
			}
		}

		if (availDTO.getJourneyType() == JourneyType.OPEN_JAW) {
			availDTO.setFQIBDepartureDate(availDTO.getFirstInBoundDate());
			availDTO.setFQIBDepartureDateZulu(ib != null ? ib.getDepartureDateZulu() : null);
		} else if (ib != null) {
			availDTO.setFQIBDepartureDate(ib.getDepartureDate());
			availDTO.setFQIBDepartureDateZulu(ib.getDepartureDateZulu());
		}

		availDTO.setOndCode(current.getOndCode());

		if (!AppSysParamsUtil.isRequoteEnabled() && availDTO.isInboundFareQuote() && availDTO.isHalfReturnFareQuote()) {
			availDTO.setFQOnd(ReservationApiUtils.getInverseOndCode(current.getOndCode()));
		}
	}

	/**
	 * @param pbmc
	 * @return
	 * @throws ModuleException
	 */
	public List<AvailableIBOBFlightSegment> minimumFareAvailableFlight(PriceBasedMinimumCal pbmc) throws ModuleException {
		FareCalType fareTypesToCalculate[] = null;
		return minimumFareAvailableFlight(pbmc, fareTypesToCalculate);
	}

	private List<IFareCalculator> getFilteredFareCalculators(List<Set<String>> ondWiseAvailableLogicalCabinClasses,
			FareCalType... fareTypes) throws ModuleException {

		List<IFareCalculator> fareCalList = getFareCalList(fareTypes);
		int ondSequence = 0;
		FilteredBucketCheapestBCDTO outBoundCheapestBucketBCDTO = null;
		for (AvailableIBOBFlightSegment fltSeg : getAvailableIBOBFlightSegments()) {

			Iterator<IFareCalculator> fareCalItr = fareCalList.iterator();
			Map<FareCalType, List<FilteredBucketsDTO>> mapCalTypeBuckets = new HashMap<FareCalType, List<FilteredBucketsDTO>>();
			if (availableFlightSearchDTO.isQuoteSameFareBucket() && fltSeg.isUnTouchedOnd()) {
				// pick same fare on re-quote flow
				pickSameFareBucket(fltSeg, mapCalTypeBuckets, ondWiseAvailableLogicalCabinClasses, ondSequence, fareCalList);
			} else {
				if (isFareTypeRequired(fareCalList, FareCalType.SEG)) {
					List<FilteredBucketsDTO> buckets = getCheapestSegmentFare(fltSeg,
							ondWiseAvailableLogicalCabinClasses.get(ondSequence));
					if (buckets.size() > 0) {
						mapCalTypeBuckets.put(FareCalType.SEG, buckets);
					}
				}
				if (!fltSeg.isDirectFlight() && isFareTypeRequired(fareCalList, FareCalType.CON)) {

					getSeatAvailabilityBL().connectedFlightOnewayAvailabilitySearch((AvailableConnectedFlight) fltSeg,
							availableFlightSearchDTO);
					List<FilteredBucketsDTO> buckets = getMinimumFareBCAllocationSummaryDTO(
							AirInventoryDataExtractUtil.getOndFlightWiseFilteredBucketsDTOs(Arrays.asList(fltSeg), false, false),
							getRequestedLogicalCabinClass(fltSeg.getOndSequence()),
							Arrays.asList(ondWiseAvailableLogicalCabinClasses.get(ondSequence)));

					if (buckets.size() > 0) {
						mapCalTypeBuckets.put(FareCalType.CON, buckets);
					}
				}
				if (isFareTypeRequired(fareCalList, FareCalType.RT) || isFareTypeRequired(fareCalList, FareCalType.HRT)) {
					List<FilteredBucketsDTO> buckets = null;
					List<FilteredBucketsDTO> hrtBuckets = null;
					HRTAvailableFlightSearchDTO availDTO = new HRTAvailableFlightSearchDTO(availableFlightSearchDTO);
					fillHRTFlags(availDTO, fltSeg, ondSequence);

					if (availableFlightSearchDTO.isOpenReturnSearch() && availableFlightSearchDTO.isInBoundOnd(ondSequence)
							&& outBoundCheapestBucketBCDTO != null) {
						availDTO.setOutBoundCheapestBucketBCDTO(outBoundCheapestBucketBCDTO);
					}

					if (!fltSeg.isDirectFlight()) {
						getSeatAvailabilityBL().connectedFlightsHalfReturnAvailabilitySearch(fltSeg, availDTO);
						if (isFareTypeRequired(fareCalList, FareCalType.HRT)) {
							hrtBuckets = getMinimumFareBCAllocationSummaryDTO(
									AirInventoryDataExtractUtil.getOndFlightWiseFilteredBucketsDTOs(fltSeg, true, true),
									getRequestedLogicalCabinClass(ondSequence), ondWiseAvailableLogicalCabinClasses);
						}
					} else {
						getSeatAvailabilityBL().singleFlightsHalfReturnAvailabilitySearch(fltSeg, availDTO);
						if (isFareTypeRequired(fareCalList, FareCalType.HRT)) {
							hrtBuckets = getMinimumFareBCAllocationSummaryDTO(
									AirInventoryDataExtractUtil.getOndFlightWiseFilteredBucketsDTOs(fltSeg, true, true),
									getRequestedLogicalCabinClass(ondSequence), ondWiseAvailableLogicalCabinClasses);
						}
					}

					if (isFareTypeRequired(fareCalList, FareCalType.RT)) {
						buckets = AirInventoryDataExtractUtil.getOndFlightWiseFilteredBucketsDTOs(fltSeg, true, false);
						if (buckets.size() > 0) {
							mapCalTypeBuckets.put(FareCalType.RT, buckets);
						}
					}
					if (isFareTypeRequired(fareCalList, FareCalType.HRT)) {
						if (hrtBuckets != null && hrtBuckets.size() > 0) {
							mapCalTypeBuckets.put(FareCalType.HRT, hrtBuckets);
						}
					}

					// update OutBound Cheapest Bucket Booking class info for OPEN-RETURN
					outBoundCheapestBucketBCDTO = updateOBCheapestBucketBookingClassInfoForOpenReturn(ondSequence, buckets);

				}
			}

			while (fareCalItr.hasNext()) {
				IFareCalculator fareCal = fareCalItr.next();
				fareCal.addOndFlightSegment(mapCalTypeBuckets);
				if (!fareCal.isValid()) {
					fareCalItr.remove();
				}
			}

			if (fareCalList.isEmpty()) { // no valid fares exits for given journey so break and continue
				break;
			}

			ondSequence++;
		}

		return fareCalList;
	}

	/**
	 * Gets the minimum available fare.
	 * 
	 * @param fareTypes
	 *            Types of Fare's to be taken into consideration when doing the calculation.
	 */
	public List<AvailableIBOBFlightSegment> minimumFareAvailableFlight(PriceBasedMinimumCal pbmc, FareCalType... fareTypes)
			throws ModuleException {
		List<Set<String>> ondWiseAvailableLogicalCabinClasses = getOutboundInboundAvailableLogicalCabinClasses();

		List<IFareCalculator> fareCalList = getFilteredFareCalculators(ondWiseAvailableLogicalCabinClasses, fareTypes);
		// TODO generalize the logic - TODO DILAN
		// if (AppSysParamsUtil.isConnectionFareBiasingEnabled() && isAllOndHasConnectedFlights()) {
		// preProcessFareCalsForConFareBiasing(fareCalList);
		// }

		if (!fareCalList.isEmpty()) {
			Collections.sort(fareCalList, new Comparator<IFareCalculator>() {
				@Override
				public int compare(IFareCalculator first, IFareCalculator second) {
					return first.getPriority() - second.getPriority();
				}

			});
			PriceBasedMinimumCal ondPbmc = new PriceBasedMinimumCal();
			for (IFareCalculator fareCal : fareCalList) {
				if (fareCal.isValid()) {
					double calMinFare = fareCal.getTotalMinimumFare();
					if (AirInventoryDataExtractUtil.isFirstFareLess(calMinFare, -1)) {
						pbmc.considerCal(fareCal, this);
						ondPbmc.considerCal(fareCal, this);
						fareCal.setFareAndLogiCC(getAvailableIBOBFlightSegments(), false);
						updateAvailableLogcalCabinClasses(ondWiseAvailableLogicalCabinClasses, getAvailableIBOBFlightSegments());
					}
				}
			}
			ondPbmc.calculateMinimum();
		}

		return getAvailableIBOBFlightSegments();
	}

	private void preProcessFareCalsForConFareBiasing(List<IFareCalculator> fareCalList) {

		boolean connectionReturnFareAvailable = false;
		boolean connectionOneWayFareAvailableForNonSelectedDates = false;

		for (IFareCalculator fareCal : fareCalList) {
			if (fareCal.isValid()) {
				if (fareCal instanceof ReturnFareCal) {
					connectionReturnFareAvailable = true;
					break;
				}

				if (fareCal.dependantFareTypes().size() == 1 && fareCal.dependantFareTypes().contains(FareCalType.CON)) {
					connectionOneWayFareAvailableForNonSelectedDates = true;
					break;
				}
			}
		}

		if (connectionReturnFareAvailable || connectionOneWayFareAvailableForNonSelectedDates) {
			Iterator<IFareCalculator> fareCalItr = fareCalList.iterator();
			while (fareCalItr.hasNext()) {
				IFareCalculator fareCal = fareCalItr.next();
				if (fareCal.isValid() && (fareCal instanceof MixFareCal)
						&& (((MixFareCal) fareCal).isAllOndsServedByInputFareType(FareCalType.CON)
								|| ((MixFareCal) fareCal).isAllOndsServedByInputFareType(FareCalType.HRT))) {
					continue;
				} else if (!(fareCal instanceof ReturnFareCal)) {
					fareCalItr.remove();
				}

			}
		}
	}

	void setFareAndLogicalCabinClass(AvailableIBOBFlightSegment availableIBOBFlightSegment,
			List<FilteredBucketsDTO> filteredBucketsDTOs, Integer fareType, boolean setMinimumFare,
			AvailableIBOBFlightSegment availableConFlightSegment) {
		FilteredBucketsDTO firstFilteredBucketsDTO = filteredBucketsDTOs.get(0);
		String selectedLogicalCabinClass = firstFilteredBucketsDTO.getChecpestFareLogicalCabinClass();
		if (setMinimumFare) {
			availableIBOBFlightSegment.setSelectedLogicalCabinClass(selectedLogicalCabinClass);

			Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestBCAllocationSummaryDTOs = firstFilteredBucketsDTO
					.getLogicalCCWiseCheapestBCAllocationSummaryDTOs();
			for (String availableLogicalCabinClass : logicalCCWiseCheapestBCAllocationSummaryDTOs.keySet()) {
				// Load all logical CC fares and fare types
				FareSummaryDTO selectedFare = AirInventoryDataExtractUtil
						.getLastElement(logicalCCWiseCheapestBCAllocationSummaryDTOs.get(availableLogicalCabinClass))
						.getFareSummaryDTO();
				availableIBOBFlightSegment.setFare(availableLogicalCabinClass, selectedFare);

				availableIBOBFlightSegment.setFareType(availableLogicalCabinClass, fareType);
				if (!availableIBOBFlightSegment.isDirectFlight()) {
					for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableIBOBFlightSegment)
							.getAvailableFlightSegments()) {
						availableFlightSegment.setSeatsAvailable(availableLogicalCabinClass, true);
						availableFlightSegment.setFareType(availableLogicalCabinClass, FareTypes.PER_FLIGHT_FARE);
						for (FilteredBucketsDTO filteredBucketsDTO : filteredBucketsDTOs) {
							if (filteredBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs()
									.get(availableLogicalCabinClass).size() > 0
									&& ((!AppSysParamsUtil.showNestedAvailableSeats()
											&& !filteredBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs()
													.get(availableLogicalCabinClass).get(0).isNestedBc())
											|| !filteredBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs()
													.get(availableLogicalCabinClass).get(0).isStandardBC()
											|| (filteredBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs()
													.get(availableLogicalCabinClass).get(0).getSegmentCode()
													.equals(availableFlightSegment.getFlightSegmentDTO().getSegmentCode())))) {
								availableFlightSegment
										.setSeatsAvailability(availableLogicalCabinClass,
												AirInventoryDataExtractUtil.populateSeatDistributions(
														filteredBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs()
																.get(availableLogicalCabinClass),
														availableFlightSegment.isWaitListed()));
								break;
							}
						}
					}
				} else {
					((AvailableFlightSegment) availableIBOBFlightSegment).setSeatsAvailability(availableLogicalCabinClass,
							AirInventoryDataExtractUtil.populateSeatDistributions(
									logicalCCWiseCheapestBCAllocationSummaryDTOs.get(availableLogicalCabinClass),
									((AvailableFlightSegment) availableIBOBFlightSegment).isWaitListed()));
					((AvailableFlightSegment) availableIBOBFlightSegment).setSeatsAvailable(availableLogicalCabinClass, true);
					if (availableConFlightSegment != null) {
						availableConFlightSegment.setFareType(availableLogicalCabinClass, fareType);
					}
				}
			}
		} else {
			Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestBCAllocationSummaryDTOs = firstFilteredBucketsDTO
					.getLogicalCCWiseCheapestBCAllocationSummaryDTOs();
			for (String availableLogicalCabinClass : logicalCCWiseCheapestBCAllocationSummaryDTOs.keySet()) {
				// Load all logical CC fares for IBE display
				List<AvailableBCAllocationSummaryDTO> cheapestAvailableBCAllocationSummaryDTOs = logicalCCWiseCheapestBCAllocationSummaryDTOs
						.get(availableLogicalCabinClass);
				if (availableIBOBFlightSegment.getFare(availableLogicalCabinClass) == null) {
					boolean setFare = true;
					if (!availableIBOBFlightSegment.isDirectFlight()) {
						setFare = false;
						for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableIBOBFlightSegment)
								.getAvailableFlightSegments()) {
							if (availableFlightSegment.getFare(availableLogicalCabinClass) == null) {
								setFare = true;
								break;
							}
						}
					}
					if (setFare) {
						availableIBOBFlightSegment.setFare(availableLogicalCabinClass, AirInventoryDataExtractUtil
								.getLastElement(cheapestAvailableBCAllocationSummaryDTOs).getFareSummaryDTO());
						availableIBOBFlightSegment.setFareType(availableLogicalCabinClass, fareType);

						if (availableIBOBFlightSegment.getSelectedLogicalCabinClass() == null) {
							availableIBOBFlightSegment.setSelectedLogicalCabinClass(availableLogicalCabinClass);
						}
						if (!availableIBOBFlightSegment.isDirectFlight()) {
							for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableIBOBFlightSegment)
									.getAvailableFlightSegments()) {
								if (availableFlightSegment.getSeatsAvailability(availableLogicalCabinClass) == null) {
									availableFlightSegment.setSeatsAvailable(availableLogicalCabinClass, true);
									for (FilteredBucketsDTO filteredBucketsDTO : filteredBucketsDTOs) {
										if (!filteredBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs()
												.get(availableLogicalCabinClass).get(0).isNestedBc()
												|| (filteredBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs()
														.get(availableLogicalCabinClass).get(0).getSegmentCode()
														.equals(availableFlightSegment.getFlightSegmentDTO().getSegmentCode()))) {
											availableFlightSegment.setSeatsAvailability(availableLogicalCabinClass,
													AirInventoryDataExtractUtil.populateSeatDistributions(
															filteredBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs()
																	.get(availableLogicalCabinClass),
															availableFlightSegment.isWaitListed()));
											break;
										}
									}
								}
								if (availableFlightSegment.getSelectedLogicalCabinClass() == null) {
									availableFlightSegment.setSelectedLogicalCabinClass(availableLogicalCabinClass);
								}
							}
						} else {
							AvailableFlightSegment tmpAvailableFlightSegment = ((AvailableFlightSegment) availableIBOBFlightSegment);
							if (tmpAvailableFlightSegment.getSeatsAvailability(availableLogicalCabinClass) == null) {
								tmpAvailableFlightSegment.setSeatsAvailability(availableLogicalCabinClass,
										AirInventoryDataExtractUtil.populateSeatDistributions(
												cheapestAvailableBCAllocationSummaryDTOs,
												((AvailableFlightSegment) availableIBOBFlightSegment).isWaitListed()));
								tmpAvailableFlightSegment.setSeatsAvailable(availableLogicalCabinClass, true);
								if (tmpAvailableFlightSegment.getSelectedLogicalCabinClass() == null) {
									tmpAvailableFlightSegment.setSelectedLogicalCabinClass(availableLogicalCabinClass);
								}
							}
						}
					}
				}
			}
		}
	}

	private List<FilteredBucketsDTO> getCheapestSegmentFare(AvailableIBOBFlightSegment availableIBOBFlightSegment,
			Set<String> availableLogicalCabinClasses) throws ModuleException {
		List<FilteredBucketsDTO> clonedFilteredBucketsDTOs = new ArrayList<FilteredBucketsDTO>();
		if (availableIBOBFlightSegment.isDirectFlight()) {
			AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) availableIBOBFlightSegment;
			getSeatAvailabilityBL().singleFlightOnewayAvailabilitySearch(availableFlightSegment, availableFlightSearchDTO);
			FilteredBucketsDTO filteredBucketsDTO = AirInventoryDataExtractUtil
					.getLastFilteredBucketsDTO(getMinimumFareBCAllocationSummaryDTO(
							Arrays.asList(availableFlightSegment.getFilteredBucketsDTOForSegmentFares()),
							getRequestedLogicalCabinClass(availableIBOBFlightSegment.getOndSequence()),
							Arrays.asList(availableLogicalCabinClasses)));
			if (filteredBucketsDTO != null) {
				clonedFilteredBucketsDTOs.add(filteredBucketsDTO);
			}
		} else {
			AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) availableIBOBFlightSegment;
			getSeatAvailabilityBL().connectedFlightOnewayAvailabilitySearchForSegmentFares(availableConnectedFlight,
					availableFlightSearchDTO);
			clonedFilteredBucketsDTOs
					.addAll(getConnectionFlightCheapestSegmentFares(availableConnectedFlight, availableLogicalCabinClasses));
		}
		return clonedFilteredBucketsDTOs;
	}

	private List<FilteredBucketsDTO> getConnectionFlightCheapestSegmentFares(AvailableConnectedFlight availableConnectedFlight,
			Set<String> availableLogicalCabinClasses) {
		Set<String> commonLogicalCabinClasses = new HashSet<String>();
		boolean firstIteration = true;
		for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
			String selectedLogicalCabinClass = getRequestedLogicalCabinClass(availableConnectedFlight.getOndSequence());
			if (selectedLogicalCabinClass == null || selectedLogicalCabinClass.equals("")) {
				FilteredBucketsDTO filteredBucketsDTO = availableFlightSegment.getFilteredBucketsDTOForSegmentFares();
				if (filteredBucketsDTO != null) {
					if (firstIteration) {
						commonLogicalCabinClasses.addAll(filteredBucketsDTO.getAvailableLogicalCabinClasses());
					} else {
						commonLogicalCabinClasses.retainAll(filteredBucketsDTO.getAvailableLogicalCabinClasses());
					}
				} else {
					commonLogicalCabinClasses.clear();
					break;
				}
			} else {
				commonLogicalCabinClasses.add(selectedLogicalCabinClass);
				break;
			}
			firstIteration = false;
		}

		if (commonLogicalCabinClasses.isEmpty()) {
			return new ArrayList<FilteredBucketsDTO>();
		}

		List<LogicalCabinClassDTO> logicalCabinClassDTOs = new ArrayList<LogicalCabinClassDTO>();
		LogicalCabinClassDTO cabinClassDTO = null;
		for (String availableLogicalCabinClass : commonLogicalCabinClasses) {
			if (availableLogicalCabinClass != null && !"".equals(availableLogicalCabinClass.trim())) {
				cabinClassDTO = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap().get(availableLogicalCabinClass);
				if (cabinClassDTO != null) {
					logicalCabinClassDTOs.add(cabinClassDTO);
				}
			}
		}
		Collections.sort(logicalCabinClassDTOs);

		List<FilteredBucketsDTO> clonedFilteredBucketsDTOs = new ArrayList<FilteredBucketsDTO>();
		double totalFare = -1;
		for (LogicalCabinClassDTO logicalCabinClassDTO : logicalCabinClassDTOs) {
			String availableLogicalCabinClass = logicalCabinClassDTO.getLogicalCCCode();
			double logicalCabinClassTotalFare = 0;
			List<FilteredBucketsDTO> clonedLogicalCabinClassFilteredBucketsDTOs = new ArrayList<FilteredBucketsDTO>();
			for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
				FilteredBucketsDTO clonedFilteredBucketsDTO = availableFlightSegment.getFilteredBucketsDTOForSegmentFares();
				List<AvailableBCAllocationSummaryDTO> logicalCabinClassCheapestBcAllocationSummaryDTOs = getLogicalCabinClassFare(
						availableLogicalCabinClass, Arrays.asList(clonedFilteredBucketsDTO), clonedFilteredBucketsDTO);
				double logicalCabinClassFare = getFareAmount(logicalCabinClassCheapestBcAllocationSummaryDTOs);
				if (logicalCabinClassFare > -1) {
					logicalCabinClassTotalFare += logicalCabinClassFare;
				} else {
					logicalCabinClassTotalFare = -1;
					break;
				}
				clonedLogicalCabinClassFilteredBucketsDTOs.add(clonedFilteredBucketsDTO);
			}

			int iterator = 0;
			for (FilteredBucketsDTO filteredBucketsDTO : clonedLogicalCabinClassFilteredBucketsDTOs) {
				AvailableFlightSegment availableFlightSegment = availableConnectedFlight.getAvailableFlightSegments()
						.get(iterator);
				availableFlightSegment.setFilteredBucketsDTOForSegmentFares(filteredBucketsDTO);
				iterator++;
			}

			if (logicalCabinClassTotalFare > -1) {
				for (FilteredBucketsDTO clonedFilteredBucketsDTO : clonedLogicalCabinClassFilteredBucketsDTOs) {
					List<AvailableBCAllocationSummaryDTO> logicalCabinClassCheapestBcAllocationSummaryDTOs = getLogicalCabinClassFare(
							availableLogicalCabinClass, Arrays.asList(clonedFilteredBucketsDTO), clonedFilteredBucketsDTO);
					setCheapestBCAllocationSummaryDTO(Arrays.asList(availableLogicalCabinClasses), availableLogicalCabinClass,
							logicalCabinClassCheapestBcAllocationSummaryDTOs, Arrays.asList(clonedFilteredBucketsDTO));
				}
			}

			if (totalFare < 0 && logicalCabinClassTotalFare > -1 || totalFare > logicalCabinClassTotalFare) {
				totalFare = logicalCabinClassTotalFare;
				clonedFilteredBucketsDTOs.clear();
				clonedFilteredBucketsDTOs.addAll(clonedLogicalCabinClassFilteredBucketsDTOs);
				for (FilteredBucketsDTO clonedFilteredBucketsDTO : clonedLogicalCabinClassFilteredBucketsDTOs) {
					clonedFilteredBucketsDTO.setChecpestFareLogicalCabinClass(availableLogicalCabinClass);
				}
			}

		}
		return clonedFilteredBucketsDTOs;
	}

	private List<AvailableBCAllocationSummaryDTO> getLogicalCabinClassFare(String availableLogicalCabinClass,
			List<FilteredBucketsDTO> filteredBucketsDTOs, FilteredBucketsDTO filteredBucketsDTO) {
		List<AvailableBCAllocationSummaryDTO> cheapestCommonStdBucket = getCheapestBucket(filteredBucketsDTOs, filteredBucketsDTO,
				availableLogicalCabinClass, true, false);
		List<AvailableBCAllocationSummaryDTO> cheapestCommonNonStdBucket = getCheapestBucket(filteredBucketsDTOs,
				filteredBucketsDTO, availableLogicalCabinClass, false, false);
		List<AvailableBCAllocationSummaryDTO> cheapestCommonFixedBucket = getCheapestBucket(filteredBucketsDTOs,
				filteredBucketsDTO, availableLogicalCabinClass, false, true);

		List<AvailableBCAllocationSummaryDTO> logicalCabinClassCheapestBcAllocationSummaryDTOs = null;
		if (AppSysParamsUtil.allowOnlyAvailableFixedSeatsAndSkipStdAndNstdSeats() && cheapestCommonFixedBucket != null) {
			logicalCabinClassCheapestBcAllocationSummaryDTOs = cheapestCommonFixedBucket;
		} else {
			logicalCabinClassCheapestBcAllocationSummaryDTOs = getLogicalCabinClassCheapestCommonBucket(cheapestCommonStdBucket,
					cheapestCommonNonStdBucket, cheapestCommonFixedBucket, availableFlightSearchDTO);
		}
		return logicalCabinClassCheapestBcAllocationSummaryDTOs;
	}

	private double getFareAmount(List<AvailableBCAllocationSummaryDTO> logicalCabinClassCheapestBcAllocationSummaryDTOs) {
		if (!logicalCabinClassCheapestBcAllocationSummaryDTOs.isEmpty()) {
			FareSummaryDTO fareSummaryDTO = logicalCabinClassCheapestBcAllocationSummaryDTOs
					.get(logicalCabinClassCheapestBcAllocationSummaryDTOs.size() - 1).getFareSummaryDTO();
			return AirInventoryDataExtractUtil.getFareAmount(fareSummaryDTO, availableFlightSearchDTO);
		}
		return -1;
	}

	List<FilteredBucketsDTO> getMinimumFareBCAllocationSummaryDTO(List<FilteredBucketsDTO> filteredBucketsDTOs,
			String requestedLogicalCabinClass, List<Set<String>> ondWiseAvailableLogicalCabinClasses) {
		List<FilteredBucketsDTO> clonedFilteredBucketsDTOs = new ArrayList<FilteredBucketsDTO>();
		double totalFare = -1;
		String selectedLogicalCabinClass = null;
		boolean cheapestBCAllocationAvailable = true;
		FilteredBucketsDTO filteredBucketsDTO = filteredBucketsDTOs.get(0);
		LogicalCabinClassDTO cabinClassDTO = null;
		if (filteredBucketsDTO != null) {
			List<LogicalCabinClassDTO> logicalCabinClassDTOs = new ArrayList<LogicalCabinClassDTO>();
			if (requestedLogicalCabinClass == null || requestedLogicalCabinClass.equals("")) {
				for (String availableLogicalCabinClass : filteredBucketsDTO.getAvailableLogicalCabinClasses()) {
					cabinClassDTO = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap().get(availableLogicalCabinClass);
					if (cabinClassDTO != null) {
						logicalCabinClassDTOs.add(cabinClassDTO);
					}
				}
			} else {
				cabinClassDTO = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap().get(requestedLogicalCabinClass);
				if (cabinClassDTO != null) {
					logicalCabinClassDTOs.add(cabinClassDTO);
				}
			}
			for (FilteredBucketsDTO bucketsDTO : filteredBucketsDTOs) {
				clonedFilteredBucketsDTOs.add(bucketsDTO.clone());
			}
			Collections.sort(logicalCabinClassDTOs);
			for (LogicalCabinClassDTO logicalCabinClassDTO : logicalCabinClassDTOs) {
				String availableLogicalCabinClass = logicalCabinClassDTO.getLogicalCCCode();
				double logicalCabinClassTotalFare = -1;
				List<AvailableBCAllocationSummaryDTO> logicalCabinClassCheapestBcAllocationSummaryDTOs = getLogicalCabinClassFare(
						availableLogicalCabinClass, filteredBucketsDTOs, filteredBucketsDTO);

				logicalCabinClassTotalFare = getFareAmount(logicalCabinClassCheapestBcAllocationSummaryDTOs);
				if ((totalFare < 0 && logicalCabinClassTotalFare > -1 || totalFare > logicalCabinClassTotalFare)
						&& ((requestedLogicalCabinClass == null || requestedLogicalCabinClass.equals(""))
								|| requestedLogicalCabinClass.equals(availableLogicalCabinClass))) {
					totalFare = logicalCabinClassTotalFare;
					selectedLogicalCabinClass = availableLogicalCabinClass;
				}
				if (logicalCabinClassTotalFare > -1
						&& ((requestedLogicalCabinClass == null || requestedLogicalCabinClass.equals(""))
								|| requestedLogicalCabinClass.equals(availableLogicalCabinClass))) {
					cheapestBCAllocationAvailable = setCheapestBCAllocationSummaryDTO(ondWiseAvailableLogicalCabinClasses,
							availableLogicalCabinClass, logicalCabinClassCheapestBcAllocationSummaryDTOs,
							clonedFilteredBucketsDTOs);
				}
			}
			if (selectedLogicalCabinClass != null && cheapestBCAllocationAvailable) {
				for (FilteredBucketsDTO clonedFilteredBucketsDTO : clonedFilteredBucketsDTOs) {
					clonedFilteredBucketsDTO.setChecpestFareLogicalCabinClass(selectedLogicalCabinClass);
				}
				return clonedFilteredBucketsDTOs;
			}
		}
		return new ArrayList<FilteredBucketsDTO>();
	}

	private boolean setCheapestBCAllocationSummaryDTO(List<Set<String>> ondWiseAvailableLogicalCabinClasses,
			String availableLogicalCabinClass,
			List<AvailableBCAllocationSummaryDTO> logicalCabinClassCheapestBcAllocationSummaryDTOs,
			List<FilteredBucketsDTO> clonedFilteredBucketsDTOs) {
		for (Set<String> availableLogicalCabinClasses : ondWiseAvailableLogicalCabinClasses) {
			availableLogicalCabinClasses.add(availableLogicalCabinClass);
		}

		int segmentCount = 0;
		boolean cheapestBCAllocationAvailable = true;
		for (FilteredBucketsDTO clonedBucketsDTO : clonedFilteredBucketsDTOs) {
			List<AvailableBCAllocationSummaryDTO> ondWiseLogicalCabinClassCheapestBcAllocationSummaryDTOs = new ArrayList<AvailableBCAllocationSummaryDTO>();
			boolean servedFromStandardBC = false;
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCabinClassCheapestBcAllocationSummaryDTOs) {
				servedFromStandardBC = availableBCAllocationSummaryDTO.isStandardBC();
				AvailableBCAllocationSummaryDTO allocationSummaryDTO = clonedBucketsDTO.getBucket(
						availableBCAllocationSummaryDTO.getBookingCode(), availableLogicalCabinClass,
						availableBCAllocationSummaryDTO.isStandardBC(), availableBCAllocationSummaryDTO.isFixedBC(),
						availableBCAllocationSummaryDTO.getFareSummaryDTO().getFareId());
				if (allocationSummaryDTO != null) {
					ondWiseLogicalCabinClassCheapestBcAllocationSummaryDTOs.add(allocationSummaryDTO);
				}
			}
			if (AppSysParamsUtil.showNestedAvailableSeats() && servedFromStandardBC && segmentCount > 0) {
				List<AvailableBCAllocationSummaryDTO> allocationSummaryDTOs = clonedBucketsDTO
						.getBucket(availableLogicalCabinClass, true, false, false);
				if (ondWiseLogicalCabinClassCheapestBcAllocationSummaryDTOs.size() < allocationSummaryDTOs.size()) {
					ondWiseLogicalCabinClassCheapestBcAllocationSummaryDTOs.clear();
					if (logicalCabinClassCheapestBcAllocationSummaryDTOs
							.get(logicalCabinClassCheapestBcAllocationSummaryDTOs.size() - 1).getBookingCode()
							.equals(allocationSummaryDTOs.get(allocationSummaryDTOs.size() - 1).getBookingCode())) {
						ondWiseLogicalCabinClassCheapestBcAllocationSummaryDTOs.addAll(allocationSummaryDTOs);
					}
				}
			}
			if (ondWiseLogicalCabinClassCheapestBcAllocationSummaryDTOs.size() == 0) {
				cheapestBCAllocationAvailable = false;
			}
			clonedBucketsDTO.getLogicalCCWiseCheapestBCAllocationSummaryDTOs().put(availableLogicalCabinClass,
					ondWiseLogicalCabinClassCheapestBcAllocationSummaryDTOs);
			segmentCount++;
		}
		return cheapestBCAllocationAvailable;
	}

	private List<AvailableBCAllocationSummaryDTO> getCheapestBucket(List<FilteredBucketsDTO> filteredBucketsDTOs,
			FilteredBucketsDTO filteredBucketsDTO, String availableLogicalCabinClass, boolean standardFlag, boolean fixedFlag) {
		List<AvailableBCAllocationSummaryDTO> cheapestBCAllocationSummaryDTOs = new ArrayList<AvailableBCAllocationSummaryDTO>();
		List<AvailableBCAllocationSummaryDTO> allocationSummaryDTOs = filteredBucketsDTO.getBucket(availableLogicalCabinClass,
				standardFlag, fixedFlag, false);
		if (allocationSummaryDTOs != null) {
			int count = 0;
			for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : allocationSummaryDTOs) {
				if (allocationSummaryDTO.isNestedBc()) {
					cheapestBCAllocationSummaryDTOs.add(allocationSummaryDTO);
					if (count != allocationSummaryDTOs.size() - 1) {
						continue;
					}
				}
				boolean bcAcllocationFound = true;
				for (FilteredBucketsDTO bucketsDTO : filteredBucketsDTOs) {
					if (!bucketsDTO.getBucket(availableLogicalCabinClass, standardFlag, fixedFlag, false)
							.contains(allocationSummaryDTO)) {
						bcAcllocationFound = false;
						cheapestBCAllocationSummaryDTOs.clear();
						break;
					}
				}
				if (bcAcllocationFound && !cheapestBCAllocationSummaryDTOs.contains(allocationSummaryDTO)) {
					cheapestBCAllocationSummaryDTOs.add(allocationSummaryDTO);
					break;
				}
			}
			count++;
		}
		return cheapestBCAllocationSummaryDTOs;
	}

	/**
	 * Filter the cheaper buckets from among cheapest standard buckets and cheapest non standard bucket
	 * 
	 * @param cheapestCommonStdBuckets
	 * @param cheapestCommonNonStdBucket
	 * @return
	 */
	private List<AvailableBCAllocationSummaryDTO> getLogicalCabinClassCheapestCommonBucket(
			List<AvailableBCAllocationSummaryDTO> cheapestCommonStdBuckets,
			List<AvailableBCAllocationSummaryDTO> cheapestCommonNonStdBuckets,
			List<AvailableBCAllocationSummaryDTO> cheapestCommonFixedBuckets, AvailableFlightSearchDTO availableFlightSearchDTO) {

		AvailableBCAllocationSummaryDTO cheapestCommonStdBucket = AirInventoryDataExtractUtil
				.getLastElement(cheapestCommonStdBuckets);
		AvailableBCAllocationSummaryDTO cheapestCommonNonStdBucket = AirInventoryDataExtractUtil
				.getLastElement(cheapestCommonNonStdBuckets);
		AvailableBCAllocationSummaryDTO cheapestCommonFixedBucket = AirInventoryDataExtractUtil
				.getLastElement(cheapestCommonFixedBuckets);
		double totalStandardFare = cheapestCommonStdBucket == null
				? -1
				: AirInventoryDataExtractUtil.getFareAmount(cheapestCommonStdBucket, availableFlightSearchDTO);
		double totalNonStandardFare = cheapestCommonNonStdBucket == null
				? -1
				: AirInventoryDataExtractUtil.getFareAmount(cheapestCommonNonStdBucket, availableFlightSearchDTO);
		double totalFixedFare = cheapestCommonFixedBucket == null
				? -1
				: AirInventoryDataExtractUtil.getFareAmount(cheapestCommonFixedBucket, availableFlightSearchDTO);

		if (totalStandardFare >= 0 && totalNonStandardFare >= 0 && totalFixedFare >= 0) {
			if (totalStandardFare <= totalNonStandardFare && totalStandardFare <= totalFixedFare) {
				return cheapestCommonStdBuckets;
			} else if (totalNonStandardFare < totalStandardFare && totalNonStandardFare <= totalFixedFare) {
				return cheapestCommonNonStdBuckets;
			} else {
				return cheapestCommonFixedBuckets;
			}
		} else if (totalStandardFare >= 0 && totalNonStandardFare >= 0) {
			if (totalStandardFare > totalNonStandardFare) {
				return cheapestCommonNonStdBuckets;
			} else {
				return cheapestCommonStdBuckets;
			}
		} else if (totalStandardFare >= 0 && totalFixedFare >= 0) {
			if (totalStandardFare > totalFixedFare) {
				return cheapestCommonFixedBuckets;
			} else {
				return cheapestCommonStdBuckets;
			}
		} else if (totalStandardFare >= 0) {
			return cheapestCommonStdBuckets;
		} else if (totalNonStandardFare >= 0 && totalFixedFare >= 0) {
			if (totalNonStandardFare > totalFixedFare) {
				return cheapestCommonFixedBuckets;
			} else {
				return cheapestCommonNonStdBuckets;
			}
		} else if (totalNonStandardFare >= 0) {
			return cheapestCommonNonStdBuckets;
		} else if (totalFixedFare >= 0) {
			return cheapestCommonFixedBuckets;
		} else {
			return new ArrayList<AvailableBCAllocationSummaryDTO>();
		}
	}

	private List<Set<String>> getOutboundInboundAvailableLogicalCabinClasses() {
		List<Set<String>> ondWiseAvailableLogicalCabinClasses = new ArrayList<Set<String>>();
		for (int ondSequence = 0; ondSequence < getAvailableIBOBFlightSegments().size(); ondSequence++) {
			ondWiseAvailableLogicalCabinClasses.add(ondSequence, new HashSet<String>());
		}
		return ondWiseAvailableLogicalCabinClasses;
	}

	void updateAvailableLogcalCabinClasses(List<Set<String>> ondWiseAvailableLogicalCabinClasses,
			List<AvailableIBOBFlightSegment> availableIBOBFlightSegments) {
		int ondSequence = 0;
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableIBOBFlightSegments) {
			for (String availableLogicalCabinClass : ondWiseAvailableLogicalCabinClasses.get(ondSequence++)) {
				availableIBOBFlightSegment.setSeatsAvailable(availableLogicalCabinClass, true);
			}
		}
	}

	private SeatAvailabilityBL getSeatAvailabilityBL() {
		if (seatAvailabilityBL == null) {
			seatAvailabilityBL = new SeatAvailabilityBL();
		}
		return seatAvailabilityBL;
	}

	public String debugFareInfo() {
		StringBuffer sb = new StringBuffer();
		if (getAvailableIBOBFlightSegments() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMMHH:mm");
			for (AvailableIBOBFlightSegment x : getAvailableIBOBFlightSegments()) {
				sb.append(x.getOndCode());
				sb.append("(" + x.toString() + ")(");
				for (FlightSegmentDTO y : x.getFlightSegmentDTOs()) {
					sb.append(y.getFlightNumber() + ":" + sdf.format(y.getDepartureDateTime()) + ",");
				}
				sb.append("):[");
				for (String lcc : x.getAvailableLogicalCabinClasses()) {
					double fare = -1;
					String additionalInfo = "";
					FareSummaryDTO fareSum = x.getFare(lcc);
					if (fareSum != null) {
						fare = fareSum.getAdultFare();
						additionalInfo = fareSum.getBookingClassCode();

					}
					if (fare == -1 && x.isSeatsAvailable(lcc) && !x.isDirectFlight()) {
						for (AvailableFlightSegment af : ((AvailableConnectedFlight) x).getAvailableFlightSegments()) {
							fareSum = af.getFare(lcc);
							if (fareSum != null) {
								if (fare == -1) {
									fare = fareSum.getAdultFare();
									additionalInfo = fareSum.getBookingClassCode();
								} else {
									fare += fareSum.getAdultFare();
									additionalInfo += "," + fareSum.getBookingClassCode();
								}

							}
						}
					}

					sb.append(lcc);
					sb.append("=");
					if (fare > -1) {
						sb.append(fare);
						sb.append("(" + additionalInfo + ")");
					} else {

						sb.append("nofare");
					}
				}
				sb.append("], ");
			}
		}

		return sb.toString();
	}

	private void pickSameFareBucket(AvailableIBOBFlightSegment fltSeg,
			Map<FareCalType, List<FilteredBucketsDTO>> mapCalTypeBuckets, List<Set<String>> ondWiseAvailableLogicalCabinClasses,
			int ondSequence, List<IFareCalculator> fareCalList) throws ModuleException {

		int fareType = 0;
		for (FlightSegmentDTO fltSegDTO : fltSeg.getFlightSegmentDTOs()) {
			FareIdDetailDTO fareIdDetailDTO = availableFlightSearchDTO.getFlightSegFareDetails().get(fltSegDTO.getSegmentId());
			if (fareIdDetailDTO != null) {
				fareType = fareIdDetailDTO.getFareType();
				break;
			}
		}

		List<FilteredBucketsDTO> buckets = null;

		switch (fareType) {
		case 1:
		case 6:
			// return & half-return fare
			List<FilteredBucketsDTO> hrtBuckets = null;
			HRTAvailableFlightSearchDTO availDTO = new HRTAvailableFlightSearchDTO(availableFlightSearchDTO);
			fillHRTFlags(availDTO, fltSeg, ondSequence);

			if (!fltSeg.isDirectFlight()) {
				getSeatAvailabilityBL().connectedFlightsHalfReturnAvailabilitySearch(fltSeg, availDTO);
				if (isFareTypeRequired(fareCalList, FareCalType.HRT)) {
					hrtBuckets = getMinimumFareBCAllocationSummaryDTO(
							AirInventoryDataExtractUtil.getOndFlightWiseFilteredBucketsDTOs(fltSeg, true, true),
							getRequestedLogicalCabinClass(ondSequence), ondWiseAvailableLogicalCabinClasses);
				}
			} else {
				getSeatAvailabilityBL().singleFlightsHalfReturnAvailabilitySearch(fltSeg, availDTO);
				if (isFareTypeRequired(fareCalList, FareCalType.HRT)) {
					hrtBuckets = getMinimumFareBCAllocationSummaryDTO(
							AirInventoryDataExtractUtil.getOndFlightWiseFilteredBucketsDTOs(fltSeg, true, true),
							getRequestedLogicalCabinClass(ondSequence), ondWiseAvailableLogicalCabinClasses);
				}
			}

			if (isFareTypeRequired(fareCalList, FareCalType.RT)) {
				buckets = AirInventoryDataExtractUtil.getOndFlightWiseFilteredBucketsDTOs(fltSeg, true, false);
				if (buckets.size() > 0) {
					mapCalTypeBuckets.put(FareCalType.RT, buckets);
				}
			}
			if (isFareTypeRequired(fareCalList, FareCalType.HRT)) {
				if (hrtBuckets != null && hrtBuckets.size() > 0) {
					mapCalTypeBuckets.put(FareCalType.HRT, hrtBuckets);
				}
			}

			break;

		case 5:
			// segment-fare
			buckets = getCheapestSegmentFare(fltSeg, ondWiseAvailableLogicalCabinClasses.get(ondSequence));
			if (buckets.size() > 0) {
				mapCalTypeBuckets.put(FareCalType.SEG, buckets);
			}
			break;
		case 2:
		default:

			if (!fltSeg.isDirectFlight()) {
				// connection
				getSeatAvailabilityBL().connectedFlightOnewayAvailabilitySearch((AvailableConnectedFlight) fltSeg,
						availableFlightSearchDTO);
				buckets = getMinimumFareBCAllocationSummaryDTO(
						AirInventoryDataExtractUtil.getOndFlightWiseFilteredBucketsDTOs(Arrays.asList(fltSeg), false, false),
						getRequestedLogicalCabinClass(fltSeg.getOndSequence()),
						Arrays.asList(ondWiseAvailableLogicalCabinClasses.get(ondSequence)));

				if (buckets.size() > 0) {
					mapCalTypeBuckets.put(FareCalType.CON, buckets);
				}
			}

			break;

		}

	}

	private FilteredBucketCheapestBCDTO updateOBCheapestBucketBookingClassInfoForOpenReturn(Integer ondSequence,
			List<FilteredBucketsDTO> buckets) {
		FilteredBucketCheapestBCDTO outBoundCheapestBucketBCDTO = null;
		if (availableFlightSearchDTO != null && availableFlightSearchDTO.isOpenReturnSearch() && ondSequence != null
				&& !availableFlightSearchDTO.isInBoundOnd(ondSequence) && buckets != null && buckets.size() > 0) {

			outBoundCheapestBucketBCDTO = new FilteredBucketCheapestBCDTO();
			Iterator<FilteredBucketsDTO> bucketsItr = buckets.iterator();
			while (bucketsItr.hasNext()) {
				FilteredBucketsDTO filteredBucketDTO = bucketsItr.next();
				Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestStandardBuckets = filteredBucketDTO
						.getLogicalCCWiseCheapestStandardBuckets();
				if (logicalCCWiseCheapestStandardBuckets != null && logicalCCWiseCheapestStandardBuckets.size() > 0) {
					for (String logicalCC : logicalCCWiseCheapestStandardBuckets.keySet()) {
						outBoundCheapestBucketBCDTO.setStandardBookingCode(
								getQuotedBookingCodeForOpenReturn(logicalCCWiseCheapestStandardBuckets.get(logicalCC)));
					}
				}

				Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestNonStandardBuckets = filteredBucketDTO
						.getLogicalCCWiseCheapestNonStandardBuckets();

				if (logicalCCWiseCheapestNonStandardBuckets != null && logicalCCWiseCheapestNonStandardBuckets.size() > 0) {
					for (String logicalCC : logicalCCWiseCheapestNonStandardBuckets.keySet()) {
						outBoundCheapestBucketBCDTO.setNonStandardBookingCode(
								getQuotedBookingCodeForOpenReturn(logicalCCWiseCheapestNonStandardBuckets.get(logicalCC)));
					}
				}

				Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestFixedBuckets = filteredBucketDTO
						.getLogicalCCWiseCheapestFixedBuckets();
				if (logicalCCWiseCheapestFixedBuckets != null && logicalCCWiseCheapestFixedBuckets.size() > 0) {
					for (String logicalCC : logicalCCWiseCheapestFixedBuckets.keySet()) {
						outBoundCheapestBucketBCDTO.setFixedBookingCode(
								getQuotedBookingCodeForOpenReturn(logicalCCWiseCheapestFixedBuckets.get(logicalCC)));
					}
				}

			}

		} else {
			outBoundCheapestBucketBCDTO = null;
		}

		return outBoundCheapestBucketBCDTO;
	}

	private String getQuotedBookingCodeForOpenReturn(List<AvailableBCAllocationSummaryDTO> logicalCCWiseCheapestBuckets) {

		if (logicalCCWiseCheapestBuckets != null && logicalCCWiseCheapestBuckets.size() > 0) {
			for (AvailableBCAllocationSummaryDTO availBCAllocSummaryDTO : logicalCCWiseCheapestBuckets) {
				if (!availBCAllocSummaryDTO.isNestedBc()) {
					return availBCAllocSummaryDTO.getBookingCode();
				}
			}
		}

		return null;
	}

	private IFareCalculator getMinimumFareCalculator(List<IFareCalculator> fareCalList) {
		IFareCalculator minCalculator = null;
		if (!fareCalList.isEmpty()) {
			double minFareAmount = -1d;
			Collections.sort(fareCalList, new Comparator<IFareCalculator>() {
				@Override
				public int compare(IFareCalculator first, IFareCalculator second) {
					return first.getPriority() - second.getPriority();
				}

			});
			for (IFareCalculator fareCal : fareCalList) {
				if (fareCal.isValid()) {
					double calMinFare = fareCal.getTotalMinimumFare();
					if (AirInventoryDataExtractUtil.isFirstFareLess(calMinFare, -1)
							&& AirInventoryDataExtractUtil.isFirstFareLess(calMinFare, minFareAmount)) {
						minFareAmount = calMinFare;
						minCalculator = fareCal;
					}
				}
			}
		}
		return minCalculator;
	}

	public void updateMinimumFareByFareType(FareCalType... fareCalTypes) throws ModuleException {
		List<IFareCalculator> returnSupportedFareCalculators = getFilteredFareCalculators(
				getOutboundInboundAvailableLogicalCabinClasses(), fareCalTypes);
		IFareCalculator minimumReturnFareCalculator = getMinimumFareCalculator(returnSupportedFareCalculators);

		if (minimumReturnFareCalculator != null && minimumReturnFareCalculator instanceof ReturnFareCal) {
			ReturnFareCal returnCal = (ReturnFareCal) minimumReturnFareCalculator;
			returnCal.setMinimumFares(getAvailableIBOBFlightSegments());
		}
	}
}
