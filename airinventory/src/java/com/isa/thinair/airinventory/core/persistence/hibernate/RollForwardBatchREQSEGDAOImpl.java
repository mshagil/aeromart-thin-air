/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;

import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaSearchTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.RollForwardBatchREQSEGDAO;
import com.isa.thinair.commons.api.constants.CommonsConstants.BatchStatus;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * BHive: Rollforward module
 * 
 * @author Raghuraman Kumar
 * 
 *         RollForwardBatchREQSEGDAOImplRollForwardBatchREQSEDDAOImpl is the RollForwardBatchREQSEDDAO implementation
 *         for hibernate
 * @isa.module.dao-impl dao-name="RollForwardBatchREQSEGDAO"
 * 
 */
public class RollForwardBatchREQSEGDAOImpl extends PlatformBaseHibernateDaoSupport implements RollForwardBatchREQSEGDAO {

	private static Log log = LogFactory.getLog(RollForwardBatchREQSEGDAOImpl.class);

	/** get list of Rollforward Batches **/
	@Override
	public List<RollForwardBatchREQSEG> getRollForwardBatchREQSEGs() {

		return find("from RollForwardBatchREQSEG", RollForwardBatchREQSEG.class);

	}

	/** get RollForwardBatchREQSEG for batchId **/
	@Override
	public RollForwardBatchREQSEG getRollForwardBatchREQSEG(int batchSegId) {
		return (RollForwardBatchREQSEG) getSession().get(RollForwardBatchREQSEG.class, new Integer(batchSegId));

	}

	/** save RollForwardBatchREQSEG **/
	@Override
	public void saveRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG) {
		hibernateSaveOrUpdate(rollForwardBatchREQSEG);
	}

	/** save RollForwardBatchREQSEG **/
	@Override
	public void updateRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG) {
		hibernateSaveOrUpdate(rollForwardBatchREQSEG);
	}

	/** remove RollForwardBatchREQSEG **/
	@Override
	public void removeRollForwardBatchREQSEG(int batchId) {
		Object rollForwardBatchREQSEG = load(RollForwardBatchREQSEG.class, new Integer(batchId));

		delete(rollForwardBatchREQSEG);
	}

	/** remove RollForwardBatchREQSEG **/
	@Override
	public void removeRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG) {

		delete(rollForwardBatchREQSEG);
	}

	@Override
	public void saveAllRollForwardBatchREQSEG(Set<RollForwardBatchREQSEG> rollForwardBatchREQSEGSet) {
		hibernateSaveOrUpdateAll(rollForwardBatchREQSEGSet);
	}

	@Override
	public Page<RollForwardBatchREQSEG> search(SearchRollForwardCriteriaSearchTO searchRollForwardCriteriaSearchTO, int start,
			int size) {

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MMM/yyyy");
		if (Objects.requireNonNull(searchRollForwardCriteriaSearchTO) != null) {

			String sql = "from RollForwardBatchREQSEG rfbrs where 1=1 ";

			if (null != searchRollForwardCriteriaSearchTO.getFlightNumber()) {
				sql += "and rfbrs.flightNumber= '" + searchRollForwardCriteriaSearchTO.getFlightNumber() + "' ";
			}

			if (null != searchRollForwardCriteriaSearchTO.getBatchId() && searchRollForwardCriteriaSearchTO.getBatchId() > 0) {
				sql += "and rfbrs.rollForwardBatchREQ= '" + searchRollForwardCriteriaSearchTO.getBatchId() + "' ";
			}

			if (null != searchRollForwardCriteriaSearchTO.getFromDate()) {
				sql += "and rfbrs.createdDate>= TO_DATE('" + dateFormatter.format(searchRollForwardCriteriaSearchTO.getFromDate())
						+ "') ";
			}

			if (null != searchRollForwardCriteriaSearchTO.getToDate()) {
				sql += "and rfbrs.createdDate<= TO_DATE('" + dateFormatter.format(CalendarUtil.addDateVarience(searchRollForwardCriteriaSearchTO.getToDate(), 1))
						+ "') ";
			}

			if (!StringUtil.isNullOrEmpty(searchRollForwardCriteriaSearchTO.getStatus())) {
				if (!searchRollForwardCriteriaSearchTO.getStatus().equals("-")) {
					sql += "and rfbrs.status= '" + searchRollForwardCriteriaSearchTO.getStatus() + "' ";
				}
			}

			sql += " order by rfbrs.batchSegId asc";

			Query query = getSession().createQuery(sql);

			int count = query.list().size();

			query.setFirstResult(start).setMaxResults(size);

			Collection<RollForwardBatchREQSEG> results = query.list();

			return new Page<RollForwardBatchREQSEG>(count, start, start + results.size(), results);
		}
		return null;
	}

	@Override
	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByBatchId(Integer batchId) {

		String sql = "from RollForwardBatchREQSEG rfbrs where rfbrs.rollForwardBatchREQ.batchId=" + batchId;
		Query query = getSession().createQuery(sql);

		Collection<RollForwardBatchREQSEG> results = query.list();

		return results;
	}

	@Override
	public void updateRollForwardBatchREQSEGStatus(Integer batchSegId, String status, String detailAudit) throws ModuleException {

		RollForwardBatchREQSEG rollForwardBatchREQSEG = null;
		rollForwardBatchREQSEG = getRollForwardBatchREQSEG(batchSegId);

		if (rollForwardBatchREQSEG.getStatus() != null && rollForwardBatchREQSEG.getStatus().equals(BatchStatus.TERMINATED)) {
			throw new ModuleException("airinventory.arg.rollforward.terminated", AirinventoryCustomConstants.INV_MODULE_CODE);
		} else {
			if (status.equals(BatchStatus.DONE) || status.equalsIgnoreCase(BatchStatus.ERROR)) {
				rollForwardBatchREQSEG.setProcessedDate(new Date());
				rollForwardBatchREQSEG.setDetailAudit(detailAudit);
			}
			rollForwardBatchREQSEG.setStatus(status);
			getSession().saveOrUpdate(rollForwardBatchREQSEG);
		}
	}

	@Override
	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByDateAndFlights(String strFromDate, String strToDate,
			String flightNumber) {

		StringBuilder sql = new StringBuilder("FROM RollForwardBatchREQSEG rfbrs WHERE rfbrs.flightNumber= '" + flightNumber
				+ "' ");

		sql.append("AND (rfbrs.fromDate BETWEEN TO_DATE('" + strFromDate + "','DD/MM/RRRR') AND TO_DATE('" + strToDate + "','DD/MM/RRRR') ");

		sql.append("OR rfbrs.toDate BETWEEN TO_DATE('" + strFromDate + "','DD/MM/RRRR') AND TO_DATE('" + strToDate + "','DD/MM/RRRR') ");

		sql.append("OR rfbrs.fromDate >TO_DATE('" + strFromDate + "','DD/MM/RRRR') AND rfbrs.toDate  < TO_DATE('" + strToDate + "','DD/MM/RRRR')) ");

		sql.append("AND rfbrs.status IN ('" + BatchStatus.IN_PROGRESS + "', '" + BatchStatus.WAITING + "')");

		Query query = getSession().createQuery(sql.toString());

		Collection<RollForwardBatchREQSEG> results = query.list();

		return results;
	}

}
