package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.dto.inventory.UpdatedInvTempDTO;
import com.isa.thinair.airinventory.api.model.InvTempCabinAlloc;
import com.isa.thinair.airinventory.api.model.InvTempCabinBCAlloc;
import com.isa.thinair.airinventory.api.model.InventoryTemplate;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.core.persistence.dao.InventoryTemplateDAO;
/**
 * 
 * @author Priyantha
 *
 */
public class InventoryTemplateBL {
	private final Log log = LogFactory.getLog(getClass());
	
	private Set<InvTempCabinBCAlloc> editedOrDeletedBcAllocSet = new HashSet<InvTempCabinBCAlloc>();

	public ArrayList<InvTempCabinAlloc> updateInvTemplate(InvTempDTO invTempDTO, UpdatedInvTempDTO updatedList) {

		// Get existing inventory template
		InventoryTemplate existingTemplate = getInventoryTemplateDAO().getExistingInventoryTemplate(invTempDTO.getInvTempID());

		// Get existing bc allocs arrayList of existing cabin class
		Set<InvTempCabinBCAlloc> existingBookingclassAllocs = null;
		Set<InvTempCabinAlloc> existingCCAllocs = existingTemplate.getInvTempCabinAllocs();
		InvTempCabinAlloc ccAllocToUpdate = new InvTempCabinAlloc();
		Iterator<InvTempCabinAlloc> ccAllocit = existingCCAllocs.iterator();
		while (ccAllocit.hasNext()) {
			InvTempCabinAlloc exsistingCCAlloc = ccAllocit.next();
			if (exsistingCCAlloc.getInvTempCabinAllocID().equals(updatedList.getInvTmpCabinAllocID())) {
				ccAllocToUpdate = exsistingCCAlloc;
				existingBookingclassAllocs = exsistingCCAlloc.getInvTempCabinBCAllocs();
			}
		}

		// check and remove deleted bcs from existing cabin class alloc
		if ((updatedList.getDeletedInvTempBCAllocs() != null) && (!updatedList.getDeletedInvTempBCAllocs().isEmpty())) {
			Iterator<InvTempCCBCAllocDTO> deleledSetIt = updatedList.getDeletedInvTempBCAllocs().iterator();
			while (deleledSetIt.hasNext()) {
				Integer deletedBCallocID = deleledSetIt.next().getInvTmpCabinBCAllocID();
				Iterator<InvTempCabinBCAlloc> currentBCsetIt = existingBookingclassAllocs.iterator();
				while (currentBCsetIt.hasNext()) {
					InvTempCabinBCAlloc currentBCAlloc = currentBCsetIt.next();
					if (deletedBCallocID != null && deletedBCallocID.equals(currentBCAlloc.getInvTmpCabinBCAllocID())) {
						editedOrDeletedBcAllocSet.add(currentBCAlloc);
						ccAllocToUpdate.getInvTempCabinBCAllocs().remove(currentBCAlloc);
						break;

					}
				}
			}
		}

		// Check and update the existing cabin class alloc
		if ((updatedList.getEditedInvTempBCAllocs() != null) && (!updatedList.getEditedInvTempBCAllocs().isEmpty())) {
			Iterator<InvTempCCBCAllocDTO> editedSetIt = updatedList.getEditedInvTempBCAllocs().iterator();
			while (editedSetIt.hasNext()) {
				InvTempCCBCAllocDTO editedBCAllocDTO = editedSetIt.next();
				Iterator<InvTempCabinBCAlloc> existingBCAllocIt = ccAllocToUpdate.getInvTempCabinBCAllocs().iterator();
				while (existingBCAllocIt.hasNext()) {
					InvTempCabinBCAlloc existingBCAlloc = existingBCAllocIt.next();
					if (editedBCAllocDTO.getInvTmpCabinBCAllocID().equals(existingBCAlloc.getInvTmpCabinBCAllocID())) {
						if ((!editedBCAllocDTO.getAllocatedSeats().equals(existingBCAlloc.getAllocatedSeats()))
								|| (!editedBCAllocDTO.getPriorityFlag().equalsIgnoreCase(existingBCAlloc.getPriorityFlag()))
								|| (!editedBCAllocDTO.getStatusBcAlloc().equalsIgnoreCase(existingBCAlloc.getStatusBcAlloc()))) {
							ccAllocToUpdate = updateWithEditedBCAlloc(ccAllocToUpdate, editedBCAllocDTO);
							break;

						}
					}
				}
			}

		}

		// check and add new bc allocs to existing cabin class alloc
		if ((updatedList.getAddedInvTempBCAllocs() != null) && (!updatedList.getAddedInvTempBCAllocs().isEmpty())) {
			Iterator<InvTempCCBCAllocDTO> freshBCAllocDTOSet = updatedList.getAddedInvTempBCAllocs().iterator();
			while (freshBCAllocDTOSet.hasNext()) {
				InvTempCCBCAllocDTO freshBCAllocDTO = freshBCAllocDTOSet.next();
				Iterator<InvTempCabinBCAlloc> alreadyexistIt = existingBookingclassAllocs.iterator();
				while (alreadyexistIt.hasNext()) {
					InvTempCabinBCAlloc existBCAlloc = alreadyexistIt.next();
					if (existBCAlloc.getBookingCode().equalsIgnoreCase(freshBCAllocDTO.getBookingCode())
							&& ccAllocToUpdate.getInvTempCabinAllocID().equals(freshBCAllocDTO.getInvTempCabinAllocID())) {
						// remove this bc alloc from addList.bcz this one already exist
						updatedList.getAddedInvTempBCAllocs().remove(freshBCAllocDTO);
					}
				}
			}
			Iterator<InvTempCCBCAllocDTO> addableBCallocIt = updatedList.getAddedInvTempBCAllocs().iterator();
			while (addableBCallocIt.hasNext()) {
				InvTempCCBCAllocDTO freshBcAllocToAdd = addableBCallocIt.next();
				ccAllocToUpdate = addToexistTemplate(freshBcAllocToAdd, ccAllocToUpdate);
			}
		}

		// Save Updated Inventory Template Cabin class alloc
		AirInventoryModuleUtils.getInventoryTemplateDAO().updateExistingInvTemplateCabinAlloc(ccAllocToUpdate);

		// Update status if changed
		if (!existingTemplate.getStatus().equalsIgnoreCase(invTempDTO.getStatus())) {
			getInventoryTemplateDAO().updateInvTempStatus(invTempDTO);
		}

		// Return updated details to audit.
		ArrayList<InvTempCabinAlloc> auditList = new ArrayList<InvTempCabinAlloc>();
		InvTempCabinAlloc auditDetails = new InvTempCabinAlloc();
		auditDetails.setCabinClassCode(ccAllocToUpdate.getCabinClassCode());
		auditDetails.setLogicalCabinClassCode(ccAllocToUpdate.getLogicalCabinClassCode());
		auditDetails.setInvTempCabinBCAllocs(editedOrDeletedBcAllocSet);
		auditList.add(auditDetails);

		return auditList;
	}

	public InvTempCabinAlloc addToexistTemplate(InvTempCCBCAllocDTO freshBCAllocDTO, InvTempCabinAlloc ccAlloc) {

		InvTempCabinBCAlloc freshInvTempCabinBCAlloc = new InvTempCabinBCAlloc();

		freshInvTempCabinBCAlloc.setAllocatedSeats(freshBCAllocDTO.getAllocatedSeats());
		freshInvTempCabinBCAlloc.setAllocWaitListedSeats(0);
		freshInvTempCabinBCAlloc.setBookingCode(freshBCAllocDTO.getBookingCode());
		freshInvTempCabinBCAlloc.setPriorityFlag(freshBCAllocDTO.getPriorityFlag());
		freshInvTempCabinBCAlloc.setStatusBcAlloc(freshBCAllocDTO.getStatusBcAlloc());
		freshInvTempCabinBCAlloc.setInvTempCabinAlloc(ccAlloc);
		
		ccAlloc.getInvTempCabinBCAllocs().add(freshInvTempCabinBCAlloc);

		return ccAlloc;
	}

	public InvTempCabinAlloc updateWithEditedBCAlloc(InvTempCabinAlloc invTempCabinAlloc, InvTempCCBCAllocDTO editedBCAllocDTO) {

		Iterator<InvTempCabinBCAlloc> iterator = invTempCabinAlloc.getInvTempCabinBCAllocs().iterator();
		while (iterator.hasNext()) {
			InvTempCabinBCAlloc existBCAllo = iterator.next();
			if (existBCAllo.getInvTmpCabinBCAllocID().equals(editedBCAllocDTO.getInvTmpCabinBCAllocID())) {
				InvTempCabinBCAlloc updatedBcAlloc = new InvTempCabinBCAlloc();
				updatedBcAlloc.setAllocatedSeats(editedBCAllocDTO.getAllocatedSeats());
				updatedBcAlloc.setAllocWaitListedSeats(0);
				updatedBcAlloc.setBookingCode(editedBCAllocDTO.getBookingCode());
				updatedBcAlloc.setPriorityFlag(editedBCAllocDTO.getPriorityFlag());
				updatedBcAlloc.setStatusBcAlloc(editedBCAllocDTO.getStatusBcAlloc());
				updatedBcAlloc.setInvTempCabinAlloc(invTempCabinAlloc);
				updatedBcAlloc.setInvTmpCabinBCAllocID(editedBCAllocDTO.getInvTempCabinAllocID());
				editedOrDeletedBcAllocSet.add(existBCAllo);
				invTempCabinAlloc.getInvTempCabinBCAllocs().remove(existBCAllo);
				invTempCabinAlloc.getInvTempCabinBCAllocs().add(updatedBcAlloc);
				break;
			}
		}
		return invTempCabinAlloc;
	}

	public void addInvTemplateCcAlloc(InvTempDTO invTempDTO) {

		// Get existing inventory template
		InventoryTemplate existingTemplate = getInventoryTemplateDAO().getExistingInventoryTemplate(invTempDTO.getInvTempID());

		// Create InvTempCabinAlloc to save
		InvTempCabinAlloc newInvTempCabinAlloc = new InvTempCabinAlloc();

		Iterator<InvTempCCAllocDTO> iterator = invTempDTO.getInvTempCCAllocDTOList().iterator();
		while (iterator.hasNext()) {
			InvTempCCAllocDTO newInvTempCCAllocDTO = iterator.next();
			newInvTempCabinAlloc.setCabinClassCode(newInvTempCCAllocDTO.getCabinClassCode());
			newInvTempCabinAlloc.setAdultAllocation(newInvTempCCAllocDTO.getAdultAllocation());
			newInvTempCabinAlloc.setLogicalCabinClassCode(newInvTempCCAllocDTO.getLogicalCabinClassCode());
			newInvTempCabinAlloc.setInfantAllocation(newInvTempCCAllocDTO.getInfantAllocation());
			newInvTempCabinAlloc.setInventoryTemplate(existingTemplate);
			Iterator<InvTempCCBCAllocDTO> bcDtoIt = newInvTempCCAllocDTO.getInvTempCCBCAllocDTOList().iterator();
			while (bcDtoIt.hasNext()) {
				InvTempCCBCAllocDTO newBcAllocDTO = bcDtoIt.next();
				newInvTempCabinAlloc = addToexistTemplate(newBcAllocDTO, newInvTempCabinAlloc);
			}
			// Save new Inventory Template Cabin class alloc to existing template
			AirInventoryModuleUtils.getInventoryTemplateDAO().updateExistingInvTemplateCabinAlloc(newInvTempCabinAlloc);
		}
	}
	
	public ArrayList<InvTempCabinAlloc> saveSplitExistCcAllocs(InvTempDTO invTempDTO){
		// Get existing inventory template
		InventoryTemplate existingTemplate = getInventoryTemplateDAO().getExistingInventoryTemplate(invTempDTO.getInvTempID());
		
		ArrayList<InvTempCabinAlloc> auditList = new ArrayList<InvTempCabinAlloc>();
		Iterator<InvTempCCAllocDTO> newCcAllocDTOIt = invTempDTO.getInvTempCCAllocDTOList().iterator();
		while (newCcAllocDTOIt.hasNext()) {
			InvTempCCAllocDTO newCcAllocDTO = newCcAllocDTOIt.next();
			if (newCcAllocDTO.getInvTempCabinAllocID() == null || newCcAllocDTO.getInvTempCabinAllocID().equals(0)) {
				// non existing cc alloc
				InvTempCabinAlloc newCcAlloc = new InvTempCabinAlloc();
				newCcAlloc.setAdultAllocation(newCcAllocDTO.getAdultAllocation());
				newCcAlloc.setInfantAllocation(newCcAllocDTO.getInfantAllocation());
				newCcAlloc.setCabinClassCode(newCcAllocDTO.getCabinClassCode());
				newCcAlloc.setLogicalCabinClassCode(newCcAllocDTO.getLogicalCabinClassCode());
				auditList.add(newCcAlloc);
				newCcAlloc.setInventoryTemplate(existingTemplate);

				AirInventoryModuleUtils.getInventoryTemplateDAO().updateExistingInvTemplateCabinAlloc(newCcAlloc);
			} else {
				Iterator<InvTempCabinAlloc> existCabinAllocIt = existingTemplate.getInvTempCabinAllocs().iterator();
				while (existCabinAllocIt.hasNext()) {
					InvTempCabinAlloc existCabinAlloc = existCabinAllocIt.next();
					if (existCabinAlloc.getInvTempCabinAllocID().equals(newCcAllocDTO.getInvTempCabinAllocID())) {
						existCabinAlloc.setAdultAllocation(newCcAllocDTO.getAdultAllocation());
						existCabinAlloc.setInfantAllocation(newCcAllocDTO.getInfantAllocation());
						auditList.add(existCabinAlloc);
						AirInventoryModuleUtils.getInventoryTemplateDAO().updateExistingInvTemplateCabinAlloc(existCabinAlloc);
						break;
					}
				}
			}
		}
		return auditList;
	}

	public boolean validateDuplicateTemp(String airCraftModel, String segment) {
		boolean isDuplicate = false;
		ArrayList<InventoryTemplate> existingTemplates;
		existingTemplates = AirInventoryModuleUtils.getInventoryTemplateDAO().getExistingTemplateList(airCraftModel, segment);
		
		Iterator<InventoryTemplate> iterator = existingTemplates.iterator();
		while (iterator.hasNext()) {
			InventoryTemplate exist = iterator.next();
			if (exist.getAirCraftModel().equalsIgnoreCase(airCraftModel) && exist.getSegment().equalsIgnoreCase(segment)) {
				isDuplicate = true;
				break;
			}
		}
		return isDuplicate;
	}

	public boolean validateDefaultDuplicateTemp(String airCraftModel) {
		boolean isDuplicate = false;
		ArrayList<InventoryTemplate> existingTemplates;
		existingTemplates = AirInventoryModuleUtils.getInventoryTemplateDAO().getExistingTemplateList(airCraftModel, null);

		Iterator<InventoryTemplate> iterator = existingTemplates.iterator();
		while (iterator.hasNext()) {
			InventoryTemplate exist = iterator.next();
			if (exist.getAirCraftModel().equalsIgnoreCase(airCraftModel) && exist.getSegment() == null) {
				isDuplicate = true;
				break;
			}
		}
		return isDuplicate;
	}
	private InventoryTemplateDAO getInventoryTemplateDAO() {
		InventoryTemplateDAO inventryTemplateDAO = (InventoryTemplateDAO) AirInventoryUtil.getInstance().getLocalBean(
				"InventoryTemplateDAOProxy");
		return inventryTemplateDAO;
	}

	public Set<InvTempCabinBCAlloc> getEditedBcAllocSet() {
		return editedOrDeletedBcAllocSet;
	}

	public void setEditedBcAllocSet(Set<InvTempCabinBCAlloc> editedBcAllocSet) {
		this.editedOrDeletedBcAllocSet = editedBcAllocSet;
	}


}