/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on September 27, 2005
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.remoting.ejb;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria;
import com.isa.thinair.airinventory.api.dto.inventory.BCDetailDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassJDBCDAO;
import com.isa.thinair.airinventory.core.service.bd.BookingClassBDImpl;
import com.isa.thinair.airinventory.core.service.bd.BookingClassBDLocalImpl;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

@Stateless
@RemoteBinding(jndiBinding = "BookingClassService.remote")
@LocalBinding(jndiBinding = "BookingClassService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class BookingClassServiceBean extends PlatformBaseSessionBean implements BookingClassBDImpl, BookingClassBDLocalImpl {

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * 
	 * @param bookingCode
	 * @return BookingClass
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BookingClass getBookingClass(String bookingCode) {
		return getBookingClassDAO().getBookingClass(bookingCode);
	}

	/**
	 * Note - only basic fields will be filled up.
	 * 
	 * @param bookingCode
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BookingClass getLightWeightBookingClass(String bookingCode) {
		return getBookingClassDAO().getLightWeightBookingClass(bookingCode);
	}

	/**
	 * 
	 * @param bookingCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BookingClassDTO getBookingClassDTO(String bookingCode) throws ModuleException {
		// return getBookingClassJdbcDAO().getBookingClassDTO(bookingCode);
		return getBookingClassDAO().getBookingClassDTO(bookingCode);
	}

	/**
	 * 
	 * @param cabinClassCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Object[]> getBookingCodesWithFlags(String cabinClassCode) throws ModuleException {
		return getBookingClassDAO().getBookingCodesWithFlags(cabinClassCode);
	}

	/**
	 * 
	 * @param cabinClassCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String> getStandbyBookingClasses() throws ModuleException {
		return getBookingClassDAO().getStandbyBookingClasses();
	}

	/**
	 * 
	 * @param logicalCCCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public Collection<Object[]> getBookingCodesWithFlagsForLogicalCC(Collection<String> logicalCCCodes) throws ModuleException {
		Collection<String> bookingCodes = AirinventoryUtils.getAirInventoryConfig()
				.getGoshowBCListForAgentBookingsWithinCutoffTime();
		return getBookingClassDAO().getBookingCodesWithFlagsForLogicalCC(logicalCCCodes, bookingCodes);
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getBookingClassDTOs(SearchBCsCriteria criteria) throws ModuleException {
		return getBookingClassDAO().getBookingClassDTOs(criteria);
	}

	/**
	 * 
	 * @param bookingClassDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createBookingClass(BookingClassDTO bookingClassDTO) throws ModuleException {
		try {
			AuditAirinventory.doAuditSaveUpdateBookingClass(bookingClassDTO.getBookingClass(), getUserPrincipal());
			getBookingClassDAO().createBookingClass(bookingClassDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Saving booking class failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Saving booking class failed.", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}

	}

	/**
	 * 
	 * @param bookingClassDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateBookingClass(BookingClassDTO bookingClassDTO) throws ModuleException {
		try {
			AuditAirinventory.doAuditSaveUpdateBookingClass(bookingClassDTO.getBookingClass(), getUserPrincipal());
			getBookingClassDAO().updateBookingClass(bookingClassDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Updating booking class failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Updating booking class failed.", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * 
	 * @param bookingClassCode
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteBookingClass(String bookingClassCode) throws ModuleException {
		try {
			AuditAirinventory.doAuditDeleteBookingClass(bookingClassCode, getUserPrincipal());
			getBookingClassDAO().deleteBookingClass(bookingClassCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Deleting booking class failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Deleting booking class failed.", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}

	}

	/**
	 * 
	 * @param bookingCodes
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public HashMap<String, BookingClass> getBookingClassesMap(Collection<String> bookingCodes) throws ModuleException {
		try {
			return getBookingClassDAO().getBookingClassesMap(bookingCodes);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading booking classes failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param gdsId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<String> getSingleCharBookingClassesForGDS(int gdsId) throws ModuleException {
		try {
			return getBookingClassDAO().getSingleCharBookingClassesForGDS(gdsId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading booking classes failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * returns the booking class codes for given gds ids
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<String, Collection<String>> getBookingClassCodes(Collection<Integer> gdsIds) throws ModuleException {
		try {
			return getBookingClassJdbcDAO().getBookingClassCodes(gdsIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Retrieving booking class type file", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	private BookingClassDAO getBookingClassDAO() {
		return (BookingClassDAO) AirInventoryUtil.getInstance().getLocalBean("BookingClassDAOImplProxy");
	}

	@Override
	public List<BCDetailDTO> getBCsForGrouping() {
		return getBookingClassJdbcDAO().getBCsForGrouping();
	}

	@Override
	public void updateBCGroupIds(List<String> bcCodes, int groupId) {
		getBookingClassDAO().updateBCGroupIds(bcCodes, groupId);
	}

	private BookingClassJDBCDAO getBookingClassJdbcDAO() {
		return (BookingClassJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("bookingClassJdbcDAO");
	}

	public Object[] getMinimumPublicFare(int flightId, String segmentCode, String logicalCabinClassCode) {
		return getBookingClassJdbcDAO().getMinimumPublicFare(flightId, segmentCode, logicalCabinClassCode);
	}

	/**
	 * 
	 * @param bookingCodes
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getCabinClassForBookingClass(String bookingClass) throws ModuleException {
		try {
			String cabinClass = getBookingClassDAO().getCabinClassForBookingClass(bookingClass);
			if (cabinClass == null) {
				log.error("Invalid bookin class found" + bookingClass);
				throw new ModuleException("invalid booking class");
			}
			return cabinClass;
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading booking classes failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	public Collection<BookingClass> getGoshoBCList() {
		Collection<String> bookingCodes = AirinventoryUtils.getAirInventoryConfig()
				.getGoshowBCListForAgentBookingsWithinCutoffTime();

		return getBookingClassDAO().getGoshowBCListForAgentBookingsWithinCutoffTime(bookingCodes);
	}

	@Override
	public Collection<BookingClass> getBookingClasses() {
		return getBookingClassDAO().getBookingClasses();
	}
}
