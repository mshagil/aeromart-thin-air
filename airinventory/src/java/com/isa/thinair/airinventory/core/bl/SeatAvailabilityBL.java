package com.isa.thinair.airinventory.core.bl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresBCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableBCAllocationSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airinventory.api.dto.seatavailability.BucketFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChangeFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.DepAPFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.HRTAvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OnDFareSegChargesRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTOBuilder;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.PaxTypewiseFareONDChargeInfo;
import com.isa.thinair.airinventory.api.dto.seatavailability.ReconcilePassengersTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentsFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airinventory.api.util.FarePercentageCalculator;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airinventory.api.util.FareRulesUtil;
import com.isa.thinair.airinventory.api.util.FareSummaryUtil;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airinventory.core.bl.IFareCalculator.FareCalType;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.LogicalCabinClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatAvailabilityDAOJDBC;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.dto.FlexiQuoteCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.QuotedONDChargesDTO;
import com.isa.thinair.airpricing.api.dto.TransitInfoDTO;
import com.isa.thinair.airpricing.api.service.AirPricingBD;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.FlightSegmentStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;

/**
 * Fare Quote/Charge quote routines
 * 
 * @author Nasly
 * 
 */
public class SeatAvailabilityBL {

	private final Log log = LogFactory.getLog(getClass());

	private BundledFareBL bundledFareBL;

	AvailableFlightSegment singleFlightOnewayAvailabilitySearch(AvailableFlightSegment availableFlightSegment,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		if (availableFlightSegment.getFilteredBucketsDTOForSegmentFares() == null) {
			availableFlightSearchDTO = availableFlightSearchDTO.clone();
			// availableFlightSearchDTO.setReturnFlag(false);
			try {
				FareQuoteUtil.setSearchCriteriaForModify(availableFlightSearchDTO, availableFlightSegment.getFlightSegmentDTO(),
						false);
			} catch (Exception exception) {
				log.error(exception);
			}

			BucketFilteringCriteria bucketFilteringCriteria = new BucketFilteringCriteria(
					availableFlightSegment.getFlightSegmentDTO(), availableFlightSearchDTO,
					availableFlightSegment.getFlightSegmentDTO().getSegmentCode(),
					availableFlightSearchDTO.getExternalBookingClasses(),
					availableFlightSearchDTO.getPaxTypewiseFareOndChargeInfo(availableFlightSegment.getOndSequence(),
							availableFlightSegment.getOndCode()),
					availableFlightSegment.getOndSequence());
			availableFlightSegment = getSeatAvailabilityDAO().flightSegmentAvailabilitySearch(availableFlightSegment,
					bucketFilteringCriteria);
			if (availableFlightSegment.isWaitListed()) {
				injectWaitListedStatusToSegment(availableFlightSegment);
			}
		}
		return availableFlightSegment;
	}

	AvailableConnectedFlight connectedFlightOnewayAvailabilitySearchForSegmentFares(
			AvailableConnectedFlight availableConnectedFlight, AvailableFlightSearchDTO availableFlightSearchDTO)
			throws ModuleException {
		for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
			singleFlightOnewayAvailabilitySearch(availableFlightSegment, availableFlightSearchDTO);
		}
		return availableConnectedFlight;
	}

	AvailableConnectedFlight connectedFlightOnewayAvailabilitySearch(AvailableConnectedFlight availableConnectedFlight,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		availableFlightSearchDTO = availableFlightSearchDTO.clone();
		// availableFlightSearchDTO.setReturnFlag(false);

		try {
			Date departureDate = null;
			Date arrivalDate = null;
			String fromAirport = null;
			String toAirport = null;
			for (AvailableFlightSegment availableFltSeg : availableConnectedFlight.getAvailableFlightSegments()) {
				if (departureDate == null) {
					departureDate = availableFltSeg.getDepartureDate();
					fromAirport = availableFltSeg.getFlightSegmentDTO().getDepartureAirportCode();
				}
				toAirport = availableFltSeg.getFlightSegmentDTO().getArrivalAirportCode();
				arrivalDate = availableFltSeg.getArrivalDate();
			}

			FareQuoteUtil.setSearchCriteriaForModify(availableFlightSearchDTO, departureDate, arrivalDate, fromAirport, toAirport,
					false);
		} catch (Exception e) {
			log.error(e);
		}

		filterBucketsInFlightSegments(availableConnectedFlight.getAvailableFlightSegments(), availableFlightSearchDTO,
				availableConnectedFlight.getOndCode());
		return availableConnectedFlight;
	}

	private FilteredBucketsDTO singleFlightHalfReturnAvailabilitySearch(AvailableFlightSegment availableFlightSegment,
			HRTAvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {

		try {
			FareQuoteUtil.setSearchCriteriaForModify(availableFlightSearchDTO, availableFlightSegment.getFlightSegmentDTO(),
					true);
		} catch (Exception e) {
			log.error(e);
		}

		BucketFilteringCriteria bucketFilteringCriteria = new BucketFilteringCriteria(
				availableFlightSegment.getFlightSegmentDTO(), availableFlightSearchDTO,
				availableFlightSearchDTO.getExternalBookingClasses(),
				availableFlightSearchDTO.getPaxTypewiseFareOndChargeInfo(availableFlightSegment.getOndSequence(),
						availableFlightSearchDTO.getOndCode()),
				availableFlightSegment.getOndSequence(), availableFlightSearchDTO.getPreferredLanguage());
		bucketFilteringCriteria.setIgnoreStayOverTime(availableFlightSearchDTO.isIgnoreStayOverTime());

		FilteredBucketsDTO filteredBucketsDTO = getSeatAvailabilityDAO().fileterBucketsForFlightSegment(bucketFilteringCriteria,
				availableFlightSegment);

		if (availableFlightSegment.isWaitListed()) {
			injectWaitListedStatusToSegment(availableFlightSegment);
		}
		return filteredBucketsDTO;
	}

	/**
	 * Method to rebuild the Collection<OndFareDTO> form the given criteria.
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> recreateFareSegCharges(OndRebuildCriteria criteria) throws ModuleException {
		List<OndFareDTO> ondFareDTOs = new ArrayList<OndFareDTO>();
		Collection<OnDFareSegChargesRebuildCriteria> ondCriterias = criteria.getOndCriteria();

		if (log.isDebugEnabled()) {
			log.debug("Rebulding the OndFareDTOs form OndRebuildCriteria ond Col size =[" + ondCriterias.size() + "]");
		}
		Map<String, Double> totalSurcharges = new HashMap<String, Double>();
		totalSurcharges.put(PaxTypeTO.ADULT, new Double(0));
		totalSurcharges.put(PaxTypeTO.CHILD, new Double(0));
		totalSurcharges.put(PaxTypeTO.INFANT, new Double(0));

		for (OnDFareSegChargesRebuildCriteria ondCriteria : ondCriterias) {
			preProcessFareNChgsForTotalSurcharge(criteria, ondCriteria);
		}

		for (Integer subJourneyGroup : criteria.getTotalSurChargeByJourneyMap().keySet()) {
			Map<String, Double> subJourneyCharges = criteria.getTotalSurChargeByJourneyMap().get(subJourneyGroup);
			if (subJourneyCharges != null) {
				totalSurcharges.put(PaxTypeTO.ADULT,
						totalSurcharges.get(PaxTypeTO.ADULT) + subJourneyCharges.get(PaxTypeTO.ADULT));
				totalSurcharges.put(PaxTypeTO.CHILD,
						totalSurcharges.get(PaxTypeTO.CHILD) + subJourneyCharges.get(PaxTypeTO.CHILD));
				totalSurcharges.put(PaxTypeTO.INFANT,
						totalSurcharges.get(PaxTypeTO.INFANT) + subJourneyCharges.get(PaxTypeTO.INFANT));
			}
		}
		criteria.setTotalSurchage(totalSurcharges);
		for (OnDFareSegChargesRebuildCriteria ondCriteria : ondCriterias) {
			OndFareDTO dto = buildOndFareDto(criteria, ondCriteria);
			ondFareDTOs.add(dto);
		}
		return ondFareDTOs;
	}

	/**
	 * This will check for the segment and BC inventory availability.
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public boolean checkInventoryAvailability(OndRebuildCriteria criteria) throws ModuleException {
		Collection<OnDFareSegChargesRebuildCriteria> ondCriterias = criteria.getOndCriteria();
		for (OnDFareSegChargesRebuildCriteria ondCriteria : ondCriterias) {

			Integer totalAdultChlidCount = criteria.getTotlaAdults() + criteria.getTotalChildrens();
			Integer totalInfants = criteria.getTotalInfants();
			boolean isBlockSeat = criteria.hasBlockSeats();
			LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fSegBCAlloc = ondCriteria.getFsegBCAlloc();

			if (fSegBCAlloc != null && !fSegBCAlloc.isEmpty()) {
				for (Entry<Integer, LinkedList<BookingClassAlloc>> entry : fSegBCAlloc.entrySet()) {

					Integer fltSegId = entry.getKey();
					Collection<Integer> flightSegIds = new ArrayList<Integer>();
					flightSegIds.add(fltSegId);
					Collection<FlightSegmentDTO> flightSegments = getFlightBD().getFlightSegments(flightSegIds);
					// Should get one record for given flight segment id
					FlightSegmentDTO flightSegmentDTO = flightSegments.iterator().next();
					boolean isSameBookedFlightCabinSearch = isSameBookedFlightCabinSearch(entry.getValue());
					boolean isFlownOnd = isFlownOnd(entry.getValue());
					// check for segment availability
					if (!flightSegmentDTO.getFlightSegmentStatus().equals(FlightSegmentStatusEnum.OPEN.getCode())) {
						if (!isSameBookedFlightCabinSearch && !isFlownOnd) {
							return false;
						}
					}

					// checking for flight status
					Flight flight = getFlightBD().getFlight(flightSegmentDTO.getFlightId());
					if (!isFlownOnd && !(flight != null && flight.getStatus() != null
							&& flight.getStatus().equals(FlightStatusEnum.ACTIVE.getCode()))) {
						return false;
					}

					LinkedList<BookingClassAlloc> bcAllocList = entry.getValue();
					int count = 0;
					if (bcAllocList != null) {
						for (BookingClassAlloc bcAlloc : bcAllocList) {

							Integer noSeatsPerBC = bcAlloc.getNoOfAdults();
							String bcCode = bcAlloc.getBookingClassCode();
							BookingClass bookingClass = getBookingClassDAO().getLightWeightBookingClass(bcCode);
							String bcType = bookingClass.getBcType();
							boolean isFixedBC = bookingClass.getFixedFlag();
							boolean isOverbook = bcAlloc.isOverbook();

							// CHECKING some error conditions
							if (bcAlloc.getCabinClassCode() == null
									|| !bcAlloc.getCabinClassCode().equals(bookingClass.getCabinClassCode())) {
								log.error("INVALID BOOKING CLASS =================> bcAlloc.getCcCode()="
										+ bcAlloc.getCabinClassCode()
										+ (bcAlloc.getCabinClassCode() != null
												? ", cAlloc.getbCCode()=" + bcAlloc.getCabinClassCode()
														+ ", bookingClass.getCabinClassCode()=" + bookingClass.getCabinClassCode()
												: ""));
								throw new ModuleException("airinventory.segment.inventory.invalidbookingclass",
										AirinventoryCustomConstants.INV_MODULE_CODE);
							}

							// segment inventory check
							if (!isBlockSeat && !isOverbook && !BookingClassUtil.byPassSegInvUpdate(bcType) && count == 0
									&& !isSameBookedFlightCabinSearch) {
								String logicalCabinClass = bookingClass.getLogicalCCCode();
								// skipping the segInventory avail for "early block seats",standby and O/R
								List<Integer> availableSeats = getSeatAvailabilityDAO()
										.getAvailableNonFixedAndFixedSeatsCounts(flightSegmentDTO.getFlightId(),
												flightSegmentDTO.getSegmentCode(), bookingClass.getCabinClassCode(),
												logicalCabinClass, bookingClass.getBookingCode())
										.get(logicalCabinClass);
								if (!isFixedBC) {
									int totalNonFixedSeats = availableSeats.get(0) - availableSeats.get(1);
									if (totalNonFixedSeats < totalAdultChlidCount) {
										return false;
									}
								} else { // fixed BC
									if (availableSeats.get(1) < totalAdultChlidCount) {
										return false;
									}
								}
								if (availableSeats.get(2) < totalInfants) {
									return false;
								}
							}

							// BC inventory check
							FCCSegBCInventory bcInventory = getFlightInventoryDAO().getFCCSegBCInventory(fltSegId, bcCode); // BC
																															// Inventory
							if (bcInventory == null) {
								return false;
							}

							if (!isBlockSeat && !isOverbook && !BookingClassUtil.byPassSegInvUpdate(bcType)
									&& !isSameBookedFlightCabinSearch) {
								// skipping BC inventory for "early block seats and O/R"

								if (bcInventory.getStatus().equals(FCCSegBCInventory.Status.CLOSED)) {
									return false;
								}

								if (bcInventory.getSeatsAvailable() < noSeatsPerBC) {
									return false;
								}
							}
							count++;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Rebuild a OndFareDTO form the criteria given. 1 ) building faraSummaryDTO 2 ) build segment seat distribution
	 * 
	 * @param criteria
	 * @param ondCriteria
	 * @param ondIteration
	 * @return
	 * @throws ModuleException
	 */
	private OndFareDTO buildOndFareDto(OndRebuildCriteria criteria, OnDFareSegChargesRebuildCriteria ondCriteria)
			throws ModuleException {
		// setting flags
		OndFareDTOBuilder ondBuilder = new OndFareDTOBuilder(criteria, ondCriteria);
		// building faraSummaryDTO
		if (ondCriteria.getFareTO() == null) {
			Collection<Integer> fareIdCol = new ArrayList<Integer>();
			fareIdCol.add(ondCriteria.getFareID());
			Map<Integer, FareTO> fareTOMap = getFareBD().getRefundableStatuses(fareIdCol, null, null).getFareTOs();
			FareTO fareTO = fareTOMap.get(ondCriteria.getFareID());
			ondBuilder.setFareSummaryDTO(fareTO);
		} else {
			ondBuilder.setFareSummaryDTO(ondCriteria.getFareTO());
		}

		// check for inventory availability and build seat distribution
		Collection<SegmentSeatDistsDTO> seatDistsDTOs = buildSegmentSeatDis(ondBuilder);

		ondBuilder.setSegmentSeatDistsDTOs(seatDistsDTOs);

		// build segment Map
		Collection<FlightSegmentDTO> flightSegmentDTOs = getFlightBD().getFlightSegments(ondBuilder.getFlightSegIds());
		ondBuilder.setSegmentsMap(new ArrayList<FlightSegmentDTO>(flightSegmentDTOs));

		// build chargers
		Integer[] pax = ondBuilder.getPaxCount();
		// TODO : We have quoted and effective charges in ondCriteria.getCharges(). But we can't use that because for
		// OND charges we have to change applicable to OND type to applicable to ADULT_CHILD. So in the second iteration
		// correct effective charge will not set properly. We have to parameterize the adjustChargesAmoungPassengers in
		// ChargeQuoteUtils not to change the fare applicability type.
		Collection<QuotedChargeDTO> charges = getChargeBD()
				.rebuildQuoteCharges(ondBuilder.getChargeRateIds(), ondBuilder.getQuoteChargesCriteria()).values();

		// Update bundled fare fee from charge defined in BundledFareDTO
		getBundledFareBL().updateOndEffectiveBundledFee(criteria, ondCriteria, charges);

		charges = ChargeQuoteUtils.getChargesWithEffectiveChargeValues(charges, ondBuilder.getFareSummaryDTO(), pax[0], pax[1],
				ondBuilder.getTotalJourneyFare(), criteria.getTotalSurcharge());

		ondBuilder.setAllCharges(charges);

		// build flexi
		if (ondBuilder.isFlexiQuote()) {
			Collection<FlexiRuleDTO> flexiCharges = getAirPricingBD()
					.rebuildFlexiCharges(ondBuilder.getFlexiRuleIds(), ondBuilder.getFlexiQuoteCriteria()).values();
			flexiCharges = ChargeQuoteUtils.getFlexiChargesWithEffectiveChargeValues(flexiCharges, ondBuilder.getFareSummaryDTO(),
					ondBuilder.getAllCharges());
			// Prorate the flexiChargeAmount.
			if (flexiCharges != null && !flexiCharges.isEmpty()) {
				int outboundRetPercentage = Integer.parseInt(
						CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.OUTBOUND_RETURN_FLEXI_CHARGE_PERCENTAGE));
				for (FlexiRuleDTO flexiChg : flexiCharges) {
					if (flexiChg.isForReturnJourney()) {
						if (!ondBuilder.isInBoundOnd() && !ondBuilder.isModifyInboundFlight()) { // TODO check this
							flexiChg.setEffectiveChargeAmount(PaxTypeTO.ADULT,
									(flexiChg.getEffectiveChargeAmount(PaxTypeTO.ADULT) * outboundRetPercentage / 100f));
							flexiChg.setEffectiveChargeAmount(PaxTypeTO.CHILD,
									(flexiChg.getEffectiveChargeAmount(PaxTypeTO.CHILD) * outboundRetPercentage / 100f));
							flexiChg.setEffectiveChargeAmount(PaxTypeTO.INFANT,
									(flexiChg.getEffectiveChargeAmount(PaxTypeTO.INFANT) * outboundRetPercentage / 100f));
						} else {
							flexiChg.setEffectiveChargeAmount(PaxTypeTO.ADULT,
									(flexiChg.getEffectiveChargeAmount(PaxTypeTO.ADULT) * (100 - outboundRetPercentage) / 100f));
							flexiChg.setEffectiveChargeAmount(PaxTypeTO.CHILD,
									(flexiChg.getEffectiveChargeAmount(PaxTypeTO.CHILD) * (100 - outboundRetPercentage) / 100f));
							flexiChg.setEffectiveChargeAmount(PaxTypeTO.INFANT,
									(flexiChg.getEffectiveChargeAmount(PaxTypeTO.INFANT) * (100 - outboundRetPercentage) / 100f));
						}
					}
				}
			}
			ondBuilder.setAllFlexiCharges(flexiCharges);
		}
		OndFareDTO ondFareDTO = ondBuilder.buildOndFareTOD();

		ondFareDTO.setSubJourneyGroup(ondCriteria.getSubJourneyGroup());
		if (AppSysParamsUtil.isShowFareDefInDepAirPCurr()) {
			Collection<FareSummaryDTO> fareSumColl = new ArrayList<FareSummaryDTO>();
			fareSumColl.add(ondFareDTO.getFareSummaryDTO());
			FareRulesUtil.injectDepAirportCurrFares(fareSumColl);
		}

		if (log.isDebugEnabled()) {
			log.debug(ondFareDTO.getSummary());
		}

		return ondFareDTO;
	}

	private void preProcessFareNChgsForTotalSurcharge(OndRebuildCriteria criteria, OnDFareSegChargesRebuildCriteria ondCriteria)
			throws ModuleException {
		// setting flags
		OndFareDTOBuilder ondBuilder = new OndFareDTOBuilder(criteria, ondCriteria);

		// building faraSummaryDTO
		Collection<Integer> fareIdCol = new ArrayList<Integer>();
		fareIdCol.add(ondCriteria.getFareID());
		Map<Integer, FareTO> fareTOMap = getFareBD().getRefundableStatuses(fareIdCol, null, null).getFareTOs();
		FareTO fareTO = fareTOMap.get(ondCriteria.getFareID());
		ondBuilder.setFareSummaryDTO(fareTO);

		// build chargers
		Integer[] pax = ondBuilder.getPaxCount();
		Collection<QuotedChargeDTO> charges = getChargeBD()
				.rebuildQuoteCharges(ondBuilder.getChargeRateIds(), ondBuilder.getQuoteChargesCriteria()).values();

		// Update bundled fare fee from charge defined in BundledFareDTO
		getBundledFareBL().updateOndEffectiveBundledFee(criteria, ondCriteria, charges);

		charges = ChargeQuoteUtils.getChargesWithEffectiveChargeValues(charges, ondBuilder.getFareSummaryDTO(), pax[0], pax[1],
				ondBuilder.getTotalJourneyFare(), null);

		Map<String, Double> surChargeMap = null;
		if (criteria.getSubJourneyTotalSurCharge(ondCriteria.getSubJourneyGroup()) == null) {
			surChargeMap = new HashMap<String, Double>();
			surChargeMap.put(PaxTypeTO.ADULT, new Double(0));
			surChargeMap.put(PaxTypeTO.CHILD, new Double(0));
			surChargeMap.put(PaxTypeTO.INFANT, new Double(0));
			criteria.setTotalSurchage(surChargeMap);
		} else {
			surChargeMap = criteria.getSubJourneyTotalSurCharge(ondCriteria.getSubJourneyGroup());
		}
		Map<String, Double> ondSurcharges = ChargeQuoteUtils.getTotalSurcharges(charges);
		surChargeMap.put(PaxTypeTO.ADULT, surChargeMap.get(PaxTypeTO.ADULT) + ondSurcharges.get(PaxTypeTO.ADULT));
		surChargeMap.put(PaxTypeTO.CHILD, surChargeMap.get(PaxTypeTO.CHILD) + ondSurcharges.get(PaxTypeTO.CHILD));
		surChargeMap.put(PaxTypeTO.INFANT, surChargeMap.get(PaxTypeTO.INFANT) + ondSurcharges.get(PaxTypeTO.INFANT));

		criteria.addTotalSurChargeByJourneyMap(ondCriteria.getSubJourneyGroup(), surChargeMap);

		ondCriteria.setCharges(charges);
		ondCriteria.setFareTO(fareTO);

	}

	/**
	 * 1) Check for the segment and BC inventory availability 2) build segmentSeatDistribution from fltSegId and booking
	 * classes
	 * 
	 * @param ondBuilder
	 * @return
	 * @throws ModuleException
	 */
	private LinkedList<SegmentSeatDistsDTO> buildSegmentSeatDis(OndFareDTOBuilder ondBuilder) throws ModuleException {

		LinkedList<SegmentSeatDistsDTO> seatDistsDTOs = new LinkedList<SegmentSeatDistsDTO>();

		Integer[] paxCount = ondBuilder.getPaxCount();
		Integer totalAdultChlidCount = paxCount[0] + paxCount[1];
		Integer totalInfants = paxCount[2];
		// String cabinClassCode = ondBuilder.getCabinClass();
		String bookingType = ondBuilder.getBookingType();
		boolean isBlockSeat = ondBuilder.isBlockSeat();

		LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fSegBCAlloc = ondBuilder.getFsegBCAlloc();
		if (fSegBCAlloc != null && !fSegBCAlloc.isEmpty()) {
			for (Entry<Integer, LinkedList<BookingClassAlloc>> entry : fSegBCAlloc.entrySet()) {

				SegmentSeatDistsDTO segmentSeatDistsDTO = new SegmentSeatDistsDTO();

				Integer fltSegId = entry.getKey();

				Collection<Integer> flightSegIds = new ArrayList<Integer>();
				flightSegIds.add(fltSegId);
				Collection<FlightSegmentDTO> flightSegments = getFlightBD().getFlightSegments(flightSegIds);
				// Should get one record for given flight segment id
				FlightSegmentDTO flightSegmentDTO = flightSegments.iterator().next();

				boolean busSegment = AirScheduleCustomConstants.OperationTypes.BUS_SERVICE == flightSegmentDTO
						.getOperationTypeID();

				boolean isFlownOnd = isFlownOnd(entry.getValue());
				boolean isSameBookedFlightCabinSearch = isSameBookedFlightCabinSearch(entry.getValue());
				boolean isUntouchedOnd = isUntouchedOnd(entry.getValue());

				// check for segment availability
				if (!flightSegmentDTO.getFlightSegmentStatus().equals(FlightSegmentStatusEnum.OPEN.getCode())) {
					if (!isSameBookedFlightCabinSearch && !isFlownOnd) {
						throw new ModuleException("airinventory.segment.inventory.invalid",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
				}

				// checking for flight status
				Flight flight = getFlightBD().getFlight(flightSegmentDTO.getFlightId());
				if (!isFlownOnd && !isUntouchedOnd && !(flight != null && flight.getStatus() != null
						&& flight.getStatus().equals(FlightStatusEnum.ACTIVE.getCode()))) {
					// flight is not active
					throw new ModuleException("airinventory.segment.inventory.invalid",
							AirinventoryCustomConstants.INV_MODULE_CODE);
				}

				// Filling the segmentSeatDistsDTO. Availability need to be done at the BC level
				segmentSeatDistsDTO.setFlightSegId(fltSegId);
				segmentSeatDistsDTO.setFlightId(flightSegmentDTO.getFlightId());
				// TODO : Logical CC Change - Only COS is available, need to set the Logical CC

				segmentSeatDistsDTO.setSegmentCode(flightSegmentDTO.getSegmentCode());
				segmentSeatDistsDTO.setNoOfInfantSeats(totalInfants);
				// TODO verify other attributes with MN

				LinkedList<BookingClassAlloc> bcAllocList = entry.getValue();
				int count = 1;
				if (bcAllocList != null) {
					for (BookingClassAlloc bcAlloc : bcAllocList) {
						Integer noSeatsPerBC = bcAlloc.getNoOfAdults();
						String bcCode = bcAlloc.getBookingClassCode();
						BookingClass bookingClass = getBookingClassDAO().getLightWeightBookingClass(bcCode);
						boolean isFixedBC = bookingClass.getFixedFlag();

						segmentSeatDistsDTO.setCabinClassCode(bcAlloc.getCabinClassCode());
						segmentSeatDistsDTO.setLogicalCabinClass(
								busSegment ? bookingClass.getLogicalCCCode() : bcAlloc.getLogicaCabinClassCode());

						if (bookingClass.getBcType().equals(BookingClass.BookingClassType.OPEN_RETURN)) {
							// Why we add this? in avail search booking is normal type. But if he selects open return
							// system will serve it from OPEN RETURN. so we can't override that one from the avail
							// search value
							bookingType = BookingClass.BookingClassType.OPEN_RETURN;
						}

						// CHECKING some error conditions
						if (bcAlloc.getCabinClassCode() == null && bcAlloc.getLogicaCabinClassCode() == null) {
							throw new ModuleException("airinventory.segment.inventory.invalidbookingclass",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}

						if ((bcAlloc.getCabinClassCode() != null && bookingClass.getCabinClassCode() != null
								&& !bcAlloc.getCabinClassCode().equals(bookingClass.getCabinClassCode()))
								|| (!busSegment
										&& (bcAlloc.getLogicaCabinClassCode() != null && bookingClass.getLogicalCCCode() != null
												&& !bcAlloc.getLogicaCabinClassCode().equals(bookingClass.getLogicalCCCode())))) {
							log.error("INVALID BOOKING CLASS =================> bcAlloc.getCcCode()="
									+ bcAlloc.getCabinClassCode()
									+ (bcAlloc.getCabinClassCode() != null
											? ", cAlloc.getCcCode()=" + bcAlloc.getCabinClassCode()
													+ ", bookingClass.getCabinClassCode()=" + bookingClass.getCabinClassCode()
											: ""));
							throw new ModuleException("airinventory.segment.inventory.invalidbookingclass",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}
						if ((segmentSeatDistsDTO.getCabinClassCode() != null
								&& !bookingClass.getCabinClassCode().equals(segmentSeatDistsDTO.getCabinClassCode()))
								|| (!busSegment && (segmentSeatDistsDTO.getLogicalCabinClass() != null && !bookingClass
										.getLogicalCCCode().equals(segmentSeatDistsDTO.getLogicalCabinClass())))) {
							log.error("INVALID BOOKING CLASS =================> segmentSeatDistsDTO.getCabinClassCode()="
									+ segmentSeatDistsDTO.getCabinClassCode() + ", bookingClass.getCabinClassCode()="
									+ bookingClass.getCabinClassCode());
							throw new ModuleException("airinventory.segment.inventory.invalidbookingclass",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}

						// segment inventory check
						if (!isBlockSeat // skipping the segInventory avail for "early block seats"
								&& ((!bcAlloc.isOverbook() && !flightSegmentDTO.isFlightWithinCutoff())
										|| (!ondBuilder.isAllowOverbookAfterCutoffTime()
												&& flightSegmentDTO.isFlightWithinCutoff())) // if
								// overbook
								&& !BookingClassUtil.byPassSegInvUpdate(bookingType) // standby and O/R
								&& count == 1 && !isSameBookedFlightCabinSearch && !bcAlloc.isWaitListed()) { // Seg
																												// availability
																												// check
																												// is
																												// done
																												// once.
							String logicalCabinClass = bookingClass.getLogicalCCCode();
							List<Integer> availableSeats = getSeatAvailabilityDAO()
									.getAvailableNonFixedAndFixedSeatsCounts(flightSegmentDTO.getFlightId(),
											flightSegmentDTO.getSegmentCode(), bookingClass.getCabinClassCode(),
											logicalCabinClass, bookingClass.getBookingCode())
									.get(logicalCabinClass);
							if (!isFixedBC) {
								int totalNonFixedSeats = availableSeats.get(0) - availableSeats.get(1);
								if (totalNonFixedSeats < totalAdultChlidCount) {
									throw new ModuleException("airinventory.segment.inventory.unavailable",
											AirinventoryCustomConstants.INV_MODULE_CODE);
								}
							} else { // fixed BC
								if (availableSeats.get(1) < totalAdultChlidCount) {
									throw new ModuleException("airinventory.segment.inventory.unavailable",
											AirinventoryCustomConstants.INV_MODULE_CODE);
								}
							}
							if (availableSeats.get(2) < totalInfants) {
								if (!bcAlloc.isBookedFlightCabinBeingSearched()) {
									throw new ModuleException("airinventory.segment.inventory.unavailable",
											AirinventoryCustomConstants.INV_MODULE_CODE);
								}
							}
						}
						segmentSeatDistsDTO.setFixedQuotaSeats(isFixedBC); // nesting is not applied for fixed BC

						if (!ondBuilder.isOpenReturn()) {
							// BC inventory check
							FCCSegBCInventory bcInventory = getFlightInventoryDAO().getFCCSegBCInventory(fltSegId, bcCode); // BC
																															// Inventory
							if (bcInventory == null) {
								throw new ModuleException("airinventory.booking.class.inventory.invalid",
										AirinventoryCustomConstants.INV_MODULE_CODE);
							}

							if (!isBlockSeat && !BookingClassUtil.byPassSegInvUpdate(bookingType) && !bcAlloc.isOverbook()
									&& !isSameBookedFlightCabinSearch && !bcAlloc.isWaitListed() && !isFlownOnd) {
								// skipping BC inventory for "early block seats and O/R"

								if (bcInventory.getStatus().equals(FCCSegBCInventory.Status.CLOSED)) {
									log.error(" Booking class closed for flightSegId - " + fltSegId + " booking class " + bcCode);
									throw new ModuleException("airinventory.booking.class.inventory.unavilable",
											AirinventoryCustomConstants.INV_MODULE_CODE);
								}

								if (bcInventory.getSeatsAvailable() < noSeatsPerBC) {
									log.error(" Available seats less than the requred for flightSegId - " + fltSegId
											+ " booking class " + bcCode);
									throw new ModuleException("airinventory.booking.class.inventory.unavilable",
											AirinventoryCustomConstants.INV_MODULE_CODE);
								}
							}
						}

						// Adding the SeatDistribution if all goes correctly
						SeatDistribution seatDistribution = new SeatDistribution();
						seatDistribution.setBookingClassCode(bcCode);
						seatDistribution.setBookingClassType(bookingType);
						seatDistribution.setNoOfSeats(noSeatsPerBC);
						seatDistribution.setOnholdRestricted(bookingClass.getOnHold());
						seatDistribution.setOverbook(bcAlloc.isOverbook());
						seatDistribution.setSameBookingClassAsExisting(bcAlloc.isSameBookingClassAsExisting());
						seatDistribution.setBookedFlightCabinBeingSearched(bcAlloc.isBookedFlightCabinBeingSearched());
						seatDistribution.setFlownOnd(bcAlloc.isFlownOnd());
						seatDistribution.setWaitListed(bcAlloc.isWaitListed());

						boolean isNested = (bcAllocList.size() > 1) && count < bcAllocList.size();
						seatDistribution.setNestedSeats(isNested);
						count++;

						segmentSeatDistsDTO.addSeatDistribution(seatDistribution);
					}
				}

				seatDistsDTOs.add(segmentSeatDistsDTO);
			}
		}
		return seatDistsDTOs;
	}

	private boolean isFlownOnd(LinkedList<BookingClassAlloc> bcAllocs) {
		if (bcAllocs != null) {
			for (BookingClassAlloc alloc : bcAllocs) {
				if (alloc.isFlownOnd()) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isUntouchedOnd(LinkedList<BookingClassAlloc> bcAllocs) {
		if (bcAllocs != null) {
			for (BookingClassAlloc alloc : bcAllocs) {
				if (alloc.isUntouchedOnd()) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isSameBookedFlightCabinSearch(LinkedList<BookingClassAlloc> bcAllocs) {
		if (bcAllocs != null) {
			for (BookingClassAlloc alloc : bcAllocs) {
				if (alloc.isBookedFlightCabinBeingSearched()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> quoteGoshowFares(ReconcilePassengersTO goshowFQCriteria) throws ModuleException {

		Collection<OndFareDTO> ondFareDTOs = new ArrayList<OndFareDTO>();

		// Get goshow book
		BookingClass goshowBC = getBookingClassDAO().getGoShowBookingClass(goshowFQCriteria.getCabinClassCode(),
				goshowFQCriteria.getPaxCategoryCode());

		if (goshowBC == null) {
			log.error("No goshow booking class defined. [cabinclass =" + goshowFQCriteria.getCabinClassCode()
					+ ", paxCategoryCode =" + goshowFQCriteria.getPaxCategoryCode() + "]");
			throw new ModuleException("airinventory.masterdata.goshow.notdefined", AirinventoryCustomConstants.INV_MODULE_CODE);
		}

		Object[] goshowFareArr = getSeatAvailabilityDAO().quoteGoshowFare(goshowFQCriteria);

		OndFareDTO ondFareDTO = new OndFareDTO();

		if (goshowFareArr == null) {
			throw new ModuleException("airinventory.masterdata.goshowfare.notdefined",
					AirinventoryCustomConstants.INV_MODULE_CODE);
		} else {
			// quoting charges
			Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();

			// FIXME : Setting transit tax new behavior only for interline fares
			boolean hasInwardCxnFlight = false;
			boolean hasOutwardCxnFlight = false;

			long inwardTransitDuration = 0;
			long outwardTransitDuration = 0;

			// Sales channel is TravelAgent for GOSHO bookings, Should apply all the travel agent charges.
			int salesChannel = 3;

			// single segment goshow charge quote is assumed
			// assume no connection flights or interline flights for GOSHOW
			criteriaCollection.add(createChargeQuoteCriteria(goshowFQCriteria.getSegmentCode(), goshowFQCriteria.getPosStation(),
					goshowFQCriteria.getDepartureDateTimeLocal(), AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS,
					AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION, null, QuoteChargesCriteria.FIRST_AND_LAST_OND,
					false, goshowFQCriteria.getAgentCode(), hasInwardCxnFlight, hasOutwardCxnFlight, null, inwardTransitDuration,
					outwardTransitDuration, null, goshowFQCriteria.getCabinClassCode(), null, null, null, salesChannel, null,
					null, null));

			// assume no connection flights or interline
			// flights for GOSHOW
			HashMap<String, HashMap<String, QuotedChargeDTO>> charges = getChargeBD().quoteCharges(criteriaCollection);

			HashMap<String, QuotedChargeDTO> ondCharges = charges.get(goshowFQCriteria.getSegmentCode());

			boolean isBusSegment = AirInventoryModuleUtils.getAirportBD().isBusSegment(goshowFQCriteria.getSegmentCode());

			Collection<QuotedChargeDTO> chargesCollection = filterChargesForChargeGroups(
					getApplicableChargeGroupsForBookingClass(goshowBC.getBookingCode(), isBusSegment),
					AirinventoryUtils.getQuotedChargeDTOList(ondCharges));

			FareSummaryDTO fareSummaryDTO = new FareSummaryDTO();
			fareSummaryDTO.setFareId((Integer) goshowFareArr[0]);
			fareSummaryDTO.setAdultFare((Double) goshowFareArr[1]);
			fareSummaryDTO.setBookingClassCode((String) goshowFareArr[2]);
			fareSummaryDTO.setChildFare((Double) goshowFareArr[3]);
			fareSummaryDTO.setChildFareType((String) goshowFareArr[4]);
			fareSummaryDTO.setInfantFare((Double) goshowFareArr[5]);
			fareSummaryDTO.setInfantFareType((String) goshowFareArr[6]);

			FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
			flightSegmentDTO.setFlightId(goshowFQCriteria.getFlightId());
			flightSegmentDTO.setSegmentCode(goshowFQCriteria.getSegmentCode());
			flightSegmentDTO.setFlightNumber(goshowFQCriteria.getFlightNumber());
			flightSegmentDTO.setFromAirport(goshowFQCriteria.getDepartureAirport());
			flightSegmentDTO.setDepartureDateTime(goshowFQCriteria.getDepartureDateTimeLocal());
			flightSegmentDTO.setDepartureDateTimeZulu(goshowFQCriteria.getDepartureDateTimeZulu());
			flightSegmentDTO.setArrivalDateTime(goshowFQCriteria.getArrivalDateTimeLocal());
			flightSegmentDTO.setArrivalDateTimeZulu(goshowFQCriteria.getArrivalDateTimeZulu());
			flightSegmentDTO.setSegmentId(goshowFQCriteria.getFlightSegId());

			Map<String, Double> journeyFareMap = new HashMap<String, Double>();
			journeyFareMap.put(PaxTypeTO.ADULT, fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
			journeyFareMap.put(PaxTypeTO.CHILD, fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD));
			journeyFareMap.put(PaxTypeTO.INFANT, fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT));
			// TODO check weather need to include applicability
			ondFareDTO.setFareSummaryDTO(fareSummaryDTO);
			ondFareDTO.addSegment(flightSegmentDTO.getSegmentId(), flightSegmentDTO);
			Collection<QuotedChargeDTO> chargesForSurCal = ChargeQuoteUtils.getChargesWithEffectiveChargeValues(chargesCollection,
					fareSummaryDTO, goshowFQCriteria.getNoOfAdults(), goshowFQCriteria.getNoOfChildren(), journeyFareMap, null);
			Map<String, Double> totalSurcharge = ChargeQuoteUtils.getTotalSurcharges(chargesForSurCal);
			ondFareDTO.setAllCharges(ChargeQuoteUtils.getChargesWithEffectiveChargeValues(chargesCollection, fareSummaryDTO,
					goshowFQCriteria.getNoOfAdults(), goshowFQCriteria.getNoOfChildren(), journeyFareMap, totalSurcharge));
		}

		SegmentSeatDistsDTO segSeatsDist = new SegmentSeatDistsDTO();
		// TODO : Logical CC Change - Only COS is available
		segSeatsDist.setCabinClassCode(goshowFQCriteria.getCabinClassCode());
		segSeatsDist.setFixedQuotaSeats(false);
		segSeatsDist.setFlightId(goshowFQCriteria.getFlightId());
		segSeatsDist.setFlightSegId(goshowFQCriteria.getFlightSegId());
		segSeatsDist.setNoOfInfantSeats(0);
		segSeatsDist.setSegmentCode(goshowFQCriteria.getSegmentCode());
		segSeatsDist.setPnrSegId(goshowFQCriteria.getPnrSegmentId());

		SeatDistribution seatDistribution = new SeatDistribution();
		seatDistribution.setBookingClassCode(goshowBC.getBookingCode());
		seatDistribution.setBookingClassType(getBookingClassJDBCDAO().getBookingClassType(goshowBC.getBookingCode()));
		seatDistribution.setNoOfSeats(goshowFQCriteria.getNoOfAdults() + goshowFQCriteria.getNoOfChildren());
		seatDistribution.setNestedSeats(false);

		segSeatsDist.addSeatDistribution(seatDistribution);

		// Prepare segment seat distribution
		Collection<SegmentSeatDistsDTO> segSeatsDists = new ArrayList<SegmentSeatDistsDTO>();
		segSeatsDists.add(segSeatsDist);

		ondFareDTO.setSegmentSeatDistsDTOs(segSeatsDists);

		ondFareDTOs.add(ondFareDTO);

		return ondFareDTOs;
	}

	public OndFareDTO quoteGDSFares(ReconcilePassengersTO gdsReconcilePassengersTO) throws ModuleException {

		// Get goshow book
		BookingClass gdsBookingClass = getBookingClassDAO().getBookingClass(gdsReconcilePassengersTO.getBookingClassCode());

		if (gdsBookingClass == null) {
			log.error("No gds booking class defined. [cabinclass =" + gdsReconcilePassengersTO.getBookingClassCode() + "]");
			throw new ModuleException("airinventory.masterdata.gds.bc.notdefined", AirinventoryCustomConstants.INV_MODULE_CODE);
		}

		OndFareDTO ondFareDTO = new OndFareDTO();

		FareSummaryDTO fareSummaryDTO = getSeatAvailabilityDAO().quoteFareForExternalBookingChannel(gdsReconcilePassengersTO,
				gdsBookingClass);

		if (fareSummaryDTO == null) {
			throw new ModuleException("airinventory.masterdata.gds.fare.notdefined", AirinventoryCustomConstants.INV_MODULE_CODE);
		}

		// we only consider segment fare
		fareSummaryDTO.setFarePercentage(100);
		// adding fareSummary
		ondFareDTO.setFareSummaryDTO(fareSummaryDTO);

		// quoting charges
		Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();

		boolean hasInwardCxnFlight = false;
		boolean hasOutwardCxnFlight = false;

		long inwardTransitDuration = 0;
		long outwardTransitDuration = 0;

		// single segment charge quote is assumed for all gds bookings assume no connection flights or interline flights
		// for gds bookings
		criteriaCollection.add(createChargeQuoteCriteria(gdsReconcilePassengersTO.getSegmentCode(),
				gdsReconcilePassengersTO.getPosStation(), gdsReconcilePassengersTO.getDepartureDateTimeLocal(),
				AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS, AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION,
				null, QuoteChargesCriteria.FIRST_AND_LAST_OND, false, gdsReconcilePassengersTO.getAgentCode(), hasInwardCxnFlight,
				hasOutwardCxnFlight, null, inwardTransitDuration, outwardTransitDuration, null,
				gdsBookingClass.getCabinClassCode(), null, null, null, -1, null, null, null));
		HashMap<String, HashMap<String, QuotedChargeDTO>> charges = getChargeBD().quoteCharges(criteriaCollection);
		HashMap<String, QuotedChargeDTO> ondCharges = charges.get(gdsReconcilePassengersTO.getSegmentCode());

		boolean isBusSegment = AirInventoryModuleUtils.getAirportBD().isBusSegment(gdsReconcilePassengersTO.getSegmentCode());
		Collection<QuotedChargeDTO> chargesCollection = filterChargesForChargeGroups(
				getApplicableChargeGroupsForBookingClass(gdsBookingClass.getBookingCode(), isBusSegment),
				AirinventoryUtils.getQuotedChargeDTOList(ondCharges));

		Map<String, Double> journeyFareMap = new HashMap<String, Double>();
		journeyFareMap.put(PaxTypeTO.ADULT, fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
		journeyFareMap.put(PaxTypeTO.CHILD, fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD));
		journeyFareMap.put(PaxTypeTO.INFANT, fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT));

		Collection<QuotedChargeDTO> chargesForSurCal = ChargeQuoteUtils.getChargesWithEffectiveChargeValues(chargesCollection,
				fareSummaryDTO, gdsReconcilePassengersTO.getNoOfAdults(), gdsReconcilePassengersTO.getNoOfChildren(),
				journeyFareMap, null);
		Map<String, Double> totalSurcharge = ChargeQuoteUtils.getTotalSurcharges(chargesForSurCal);

		// calculation effective chargers
		Collection<QuotedChargeDTO> chargesWithEffectiveChargeValues = ChargeQuoteUtils.getChargesWithEffectiveChargeValues(
				chargesCollection, fareSummaryDTO, gdsReconcilePassengersTO.getNoOfAdults(),
				gdsReconcilePassengersTO.getNoOfChildren(), journeyFareMap, totalSurcharge);
		// adding all charges
		ondFareDTO.setAllCharges(chargesWithEffectiveChargeValues);

		// Adding flight segment Map
		FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
		flightSegmentDTO.setFlightId(gdsReconcilePassengersTO.getFlightId());
		flightSegmentDTO.setSegmentCode(gdsReconcilePassengersTO.getSegmentCode());
		flightSegmentDTO.setFlightNumber(gdsReconcilePassengersTO.getFlightNumber());
		flightSegmentDTO.setFromAirport(gdsReconcilePassengersTO.getDepartureAirport());
		flightSegmentDTO.setDepartureDateTime(gdsReconcilePassengersTO.getDepartureDateTimeLocal());
		flightSegmentDTO.setDepartureDateTimeZulu(gdsReconcilePassengersTO.getDepartureDateTimeZulu());
		ondFareDTO.addSegment(flightSegmentDTO.getSegmentId(), flightSegmentDTO);

		// Prepare the seat distribution
		SegmentSeatDistsDTO segSeatsDist = new SegmentSeatDistsDTO();
		// TODO : Logical CC Change - Only COS is available
		segSeatsDist.setCabinClassCode(gdsReconcilePassengersTO.getCabinClassCode());
		segSeatsDist.setFixedQuotaSeats(false);
		segSeatsDist.setFlightId(gdsReconcilePassengersTO.getFlightId());
		segSeatsDist.setFlightSegId(gdsReconcilePassengersTO.getFlightSegId());
		segSeatsDist.setNoOfInfantSeats(0);
		segSeatsDist.setSegmentCode(gdsReconcilePassengersTO.getSegmentCode());
		segSeatsDist.setPnrSegId(gdsReconcilePassengersTO.getPnrSegmentId());

		SeatDistribution seatDistribution = new SeatDistribution();
		seatDistribution.setBookingClassCode(gdsBookingClass.getBookingCode());
		seatDistribution.setBookingClassType(getBookingClassJDBCDAO().getBookingClassType(gdsBookingClass.getBookingCode()));
		seatDistribution.setNoOfSeats(gdsReconcilePassengersTO.getNoOfAdults() + gdsReconcilePassengersTO.getNoOfChildren());
		seatDistribution.setNestedSeats(false);
		segSeatsDist.addSeatDistribution(seatDistribution);

		Collection<SegmentSeatDistsDTO> segSeatsDists = new ArrayList<SegmentSeatDistsDTO>();
		segSeatsDists.add(segSeatsDist);
		ondFareDTO.setSegmentSeatDistsDTOs(segSeatsDists);

		return ondFareDTO;
	}

	private void setChargeRatios(List<List<AvailableIBOBFlightSegment>> ondAvailFlightSegments,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {

		if (ondAvailFlightSegments == null || ondAvailFlightSegments.size() == 0) {
			return;
		}
		int maxAvailableCombinationsForOnds = 0;
		for (List<AvailableIBOBFlightSegment> ondFlightsPerOnd : ondAvailFlightSegments) {
			if (ondFlightsPerOnd.size() == 0)
				if (AppSysParamsUtil.showHalfReturnFaresInIBECalendar() && availableFlightSearchDTO.isIncludeHRTFares()) {
					continue;
				} else {
					return;
				}
			if (ondFlightsPerOnd.size() > maxAvailableCombinationsForOnds) {
				maxAvailableCombinationsForOnds = ondFlightsPerOnd.size();
			}
		}
		Map<Integer, Map<String, PaxTypewiseFareONDChargeInfo>> unifiedOndChargeInfoMap = new HashMap<Integer, Map<String, PaxTypewiseFareONDChargeInfo>>();
		Map<Integer, AvailableIBOBFlightSegment> lastOndFlights = new HashMap<Integer, AvailableIBOBFlightSegment>();
		for (int flightIndex = 0; flightIndex < maxAvailableCombinationsForOnds; flightIndex++) {
			List<AvailableIBOBFlightSegment> ondFlightsForPriceQuote = new ArrayList<AvailableIBOBFlightSegment>();
			ContextOndFlightsTO contextOndFltInfo = new ContextOndFlightsTO();
			// int count = 0;
			int ondSequence = 0;
			for (List<AvailableIBOBFlightSegment> ondFlightsPerOnd : ondAvailFlightSegments) {
				AvailableIBOBFlightSegment availableFlightSegment = null;

				if (CollectionUtils.isEmpty(ondFlightsPerOnd) && AppSysParamsUtil.showHalfReturnFaresInIBECalendar()
						&& availableFlightSearchDTO.isIncludeHRTFares()) {
					ondSequence++;
					continue;
				}

				if (ondFlightsPerOnd.size() > flightIndex) {
					availableFlightSegment = ondFlightsPerOnd.get(flightIndex);
					lastOndFlights.put(ondSequence, availableFlightSegment);
				} else {
					availableFlightSegment = lastOndFlights.get(ondSequence);
				}
				if (!availableFlightSearchDTO.getOndSequenceReturnMap().isEmpty()) {
					availableFlightSegment
							.setInboundFlightSegment(availableFlightSearchDTO.getOndSequenceReturnMap().get(ondSequence));
				}
				ondFlightsForPriceQuote.add(availableFlightSegment);
				contextOndFltInfo.addOndFlight(availableFlightSegment,
						AppSysParamsUtil.showHalfReturnFaresInIBECalendar() && availableFlightSearchDTO.isIncludeHRTFares());
				ondSequence++;
			}
			CategoryChargeQuoteDTO ccqOnd = new CategoryChargeQuoteDTO();
			int index = 1;
			for (AvailableIBOBFlightSegment ondFlight : ondFlightsForPriceQuote) {
				if (!ondFlight.isDirectFlight()) {
					HashMap<String, HashMap<String, QuotedChargeDTO>> segmentCharges = segmentChargeQuote(ondFlight,
							availableFlightSearchDTO, contextOndFltInfo);
					HashMap<String, HashMap<String, QuotedChargeDTO>> ondCharges = ondChargeQuote(ondFlight,
							availableFlightSearchDTO, contextOndFltInfo);
					setServiceTaxInformation(ondFlight, availableFlightSearchDTO, mapUnoin(segmentCharges, ondCharges));

					ccqOnd.addOndCharges(ondFlight, segmentCharges);
					ccqOnd.addOndCharges(ondFlight, ondCharges);
				} else {
					HashMap<String, HashMap<String, QuotedChargeDTO>> ondChargeQuotes = segmentChargeQuote(ondFlight,
							availableFlightSearchDTO, contextOndFltInfo);
					setServiceTaxInformation(ondFlight, availableFlightSearchDTO, ondChargeQuotes);
					ccqOnd.addOndCharges(ondFlight, ondChargeQuotes);
				}
				if (ondFlightsForPriceQuote.size() == index) {
					ccqOnd.calculateTotals();
				}
				index++;
			}

			Map<Integer, Map<String, PaxTypewiseFareONDChargeInfo>> chargeInfoMap = ccqOnd.getChargeInfo();
			for (Integer ondId : chargeInfoMap.keySet()) {
				if (!unifiedOndChargeInfoMap.containsKey(ondId)) {
					unifiedOndChargeInfoMap.put(ondId, chargeInfoMap.get(ondId));
				} else {
					for (String ondCode : chargeInfoMap.get(ondId).keySet()) {
						if (!unifiedOndChargeInfoMap.get(ondId).containsKey(ondCode)) {
							unifiedOndChargeInfoMap.get(ondId).put(ondCode, chargeInfoMap.get(ondId).get(ondCode));
						}
					}
				}
			}
		}
		availableFlightSearchDTO.setOndChargeInfo(unifiedOndChargeInfoMap);
	}

	private static <T, V> HashMap<T, V> mapUnoin(HashMap<T, V> first, HashMap<T, V> second) {
		HashMap<T, V> union = new HashMap<>();
		if (!CollectionUtils.isEmpty(first)) {
			union.putAll(first);
		}

		if (!CollectionUtils.isEmpty(second)) {
			union.putAll(second);
		}
		return union;
	}

	private HashMap<String, HashMap<String, QuotedChargeDTO>> ondChargeQuote(AvailableIBOBFlightSegment availableIBOBFlight,
			AvailableFlightSearchDTO availableFlightSearchDTO, ContextOndFlightsTO contextOndFltInfo) throws ModuleException {
		boolean hasInwardCxnFlight = false;
		boolean hasOutwardCxnFlight = false;

		long inwardTransitDuration = 0;
		long outwardTransitDuration = 0;
		Collection<Long> ondTransitDurList = new ArrayList<Long>();

		Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();

		Collection<FlightSegmentDTO> allFlightSegments = new ArrayList<FlightSegmentDTO>();
		allFlightSegments.addAll(availableIBOBFlight.getFlightSegmentDTOs());
		TransitInfoDTO transitInfoDTO = getTransitInfoDTO(allFlightSegments,
				contextOndFltInfo.getConnectingFlightSegment(availableIBOBFlight.getOndSequence()));

		hasInwardCxnFlight = false;
		hasOutwardCxnFlight = false;
		AvailableFlightSegment flightSegment = ((AvailableConnectedFlight) availableIBOBFlight).getAvailableFlightSegments()
				.iterator().next();

		ondTransitDurList = transitInfoDTO.getTransitDurationList();

		OriginDestinationInfoDTO ondInfo = availableFlightSearchDTO.getOndInfo(availableIBOBFlight.getOndSequence());

		// Setting transit tax new behavior only for interline fares
		if (availableFlightSearchDTO.isInterlineFareQuoted()) {
			if (ondInfo.isHasInwardCxnFlight()) {
				hasInwardCxnFlight = true;
				inwardTransitDuration = flightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu().getTime()
						- ondInfo.getInwardCxnFlightArriTime().getTime();
			}
			if (ondInfo.isHasOutwardCxnFlight()) {
				hasOutwardCxnFlight = true;
				outwardTransitDuration = ondInfo.getOutwardCxnFlightDepTime().getTime()
						- flightSegment.getFlightSegmentDTO().getArrivalDateTimeZulu().getTime();
			}
		}

		String cabinClassCode = ondInfo.getPreferredClassOfService();
		// If logical cabin class selected instead of cabin class, derive cabin class code from logical cabin
		// class
		if (cabinClassCode == null || "".equals(cabinClassCode)) {
			String logicalCCCode = ondInfo.getPreferredLogicalCabin();
			cabinClassCode = getLogicalCabinClassDAO().getCabinClass(logicalCCCode);
		}

		criteriaCollection.add(createChargeQuoteCriteria(availableIBOBFlight.getOndCode(),
				availableFlightSearchDTO.getPosAirport(), flightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu(),
				AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS, AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION,
				null, // Effective charge amount for percentage charge will be calculated when charges
						// retrieved at
						// later stage
				getONDPosition(availableIBOBFlight, availableFlightSearchDTO, null), transitInfoDTO.isQualifyForShortTransit(),
				availableFlightSearchDTO.getAgentCode(), hasInwardCxnFlight, hasOutwardCxnFlight, null, inwardTransitDuration,
				outwardTransitDuration, ondTransitDurList, cabinClassCode, transitInfoDTO.getTransitAirports(),
				transitInfoDTO.getTransitAirports(),
				availableFlightSearchDTO.getEffectiveLastFQDate(flightSegment.getFlightSegmentDTO().getSegmentId()),
				availableFlightSearchDTO.getChannelCode(), availableFlightSearchDTO.getExcludedCharges(),
				availableFlightSearchDTO.getFlightSearchGap(),
				getBundledFareBL().getApplicableBundledFareChargeCode(availableIBOBFlight)));

		HashMap<String, HashMap<String, QuotedChargeDTO>> quotedChargesMap = getChargeBD().quoteCharges(criteriaCollection);
		return quotedChargesMap;
	}

	private HashMap<String, HashMap<String, QuotedChargeDTO>> segmentChargeQuote(
			AvailableIBOBFlightSegment availableIBOBFlightSegment, AvailableFlightSearchDTO availableFlightSearchDTO,
			ContextOndFlightsTO contextOndFltInfo) throws ModuleException {
		boolean hasInwardCxnFlight = false;
		boolean hasOutwardCxnFlight = false;

		long inwardTransitDuration = 0;
		long outwardTransitDuration = 0;

		List<AvailableFlightSegment> availableFlights = new ArrayList<AvailableFlightSegment>();

		if (availableIBOBFlightSegment.isDirectFlight()) {
			availableFlights.add((AvailableFlightSegment) availableIBOBFlightSegment);
		} else {
			availableFlights = ((AvailableConnectedFlight) availableIBOBFlightSegment).getAvailableFlightSegments();
		}

		// FlightSegmentDTO fltSegment = availableIBOBFlightSegment.getFlightSegmentDTO();
		int count = 1;

		Collection<FlightSegmentDTO> allFlightSements = new ArrayList<FlightSegmentDTO>();
		allFlightSements.addAll(availableIBOBFlightSegment.getFlightSegmentDTOs());

		TransitInfoDTO transitInfoDTO = getTransitInfoDTO(allFlightSements,
				contextOndFltInfo.getConnectingFlightSegment(availableIBOBFlightSegment.getOndSequence()));

		Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();

		for (AvailableFlightSegment availableFlightSegment : availableFlights) {

			OriginDestinationInfoDTO ondInfo = availableFlightSearchDTO.getOndInfo(availableIBOBFlightSegment.getOndSequence());

			String localOndPosition = QuoteChargesCriteria.INBETWEEN_OND;
			if (count == 1) {
				localOndPosition = QuoteChargesCriteria.FIRST_OND;

				// Setting transit tax new behavior only for interline fares
				if (availableFlightSearchDTO.isInterlineFareQuoted()) {
					if (ondInfo.isHasInwardCxnFlight()) {
						hasInwardCxnFlight = true;
						inwardTransitDuration = availableFlightSegment.getDepartureDateZulu().getTime()
								- ondInfo.getInwardCxnFlightArriTime().getTime();
					}
					if (ondInfo.isHasOutwardCxnFlight()) {
						hasOutwardCxnFlight = true;
						outwardTransitDuration = ondInfo.getOutwardCxnFlightDepTime().getTime()
								- availableFlightSegment.getFlightSegmentDTO().getArrivalDateTimeZulu().getTime();
					}
				}
			} else if (count == availableFlights.size()) {
				localOndPosition = QuoteChargesCriteria.LAST_OND;

				// Setting transit tax new behavior only for interline fares
				if (availableFlightSearchDTO.isInterlineFareQuoted()) {
					if (availableIBOBFlightSegment.isInboundFlightSegment() && ondInfo.isHasOutwardCxnFlight()) {
						hasOutwardCxnFlight = true;
						outwardTransitDuration = ondInfo.getOutwardCxnFlightDepTime().getTime()
								- availableFlightSegment.getFlightSegmentDTO().getArrivalDateTimeZulu().getTime();
					}
				}
			}

			String cabinClassCode = ondInfo.getPreferredClassOfService();
			// If logical cabin class selected instead of cabin class, derive cabin class code from logical
			// cabin
			// class
			if (cabinClassCode == null || "".equals(cabinClassCode)) {
				String logicalCCCode = ondInfo.getPreferredLogicalCabin();
				cabinClassCode = getLogicalCabinClassDAO().getCabinClass(logicalCCCode);
			}

			criteriaCollection.add(createChargeQuoteCriteria(availableFlightSegment.getFlightSegmentDTO().getSegmentCode(),
					availableFlightSearchDTO.getPosAirport(),
					availableFlightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu(),
					AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS, AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION,
					null, // Effective charge amount for percentage charge will be calculated when charges
							// retrieved
							// at later stage
					getONDPosition(availableIBOBFlightSegment, availableFlightSearchDTO, localOndPosition),
					transitInfoDTO
							.isQualifyForShortTransit(availableFlightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu()),
					availableFlightSearchDTO.getAgentCode(), hasInwardCxnFlight, hasOutwardCxnFlight, null, inwardTransitDuration,
					outwardTransitDuration,
					transitInfoDTO.getTransitDurationList(
							availableFlightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu()),
					cabinClassCode,
					transitInfoDTO.getTransitAirports(availableFlightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu()),
					transitInfoDTO.getDepArrExcludeAirports(
							availableFlightSegment.getFlightSegmentDTO().getDepartureAirportCode(),
							availableFlightSegment.getFlightSegmentDTO().getArrivalAirportCode()),
					availableFlightSearchDTO.getEffectiveLastFQDate(availableFlightSegment.getFlightSegmentDTO().getSegmentId()),
					availableFlightSearchDTO.getChannelCode(), availableFlightSearchDTO.getExcludedCharges(),
					availableFlightSearchDTO.getFlightSearchGap(),
					getBundledFareBL().getApplicableBundledFareChargeCode(availableIBOBFlightSegment)));
			count++;

		}

		HashMap<String, HashMap<String, QuotedChargeDTO>> chargesPerSegmentPerGroup = getChargeBD()
				.quoteCharges(criteriaCollection);

		return chargesPerSegmentPerGroup;
	}

	public SelectedFlightDTO searchAvailableFlightsSeatAvailability(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote) throws ModuleException {
		// TODO this need to set ond wise not globally if so bus, change fares and multicity need to verify
		boolean isSplitOperationForBusInvoked = false;
		boolean attemptSplitQuote = AppSysParamsUtil.isEnableOndSplitForBusFareQuote();
		if (availableFlightSearchDTO.isOwnSearch() && attemptSplitQuote
				&& availableFlightSearchDTO.getOriginDestinationInfoDTOs().size() > 1) {
			Map<Integer, Boolean> attempOndSplit = new HashMap<>();

			// isSplitOperationForBusInvoked = true; AEROMART-2921

			List<String> matchingCombinationKeysTemp = new ArrayList<>();
			List<String> matchingCombinationKeys = new ArrayList<>();
			int ondFlightOptionIndex = 0;

			Map<AvailableIBOBFlightSegment, AvailableIBOBFlightSegment> map = new HashMap<>();
			Map<AvailableIBOBFlightSegment, List<AvailableIBOBFlightSegment>> matchingBusOptions = new HashMap<>();

			MatchMaker mm = new MatchMaker(this, availableFlightSearchDTO.getJourneyType());
			for (List<AvailableIBOBFlightSegment> ondFlightOptions : selectedFlightDTO.getOndWiseSelectedDateAllFlights()) {
				if (!mm.processOndFlights(ondFlightOptions)) {
					break;
				}
			}
			List<MatchingCombination> mcList = new ArrayList<>();

			if (mm.isValid()) {
				outer: for (MatchingCombination combination : mm.getMatchingCombinationList()) {
					MatchingCombination mcFltCombination = new MatchingCombination(this,
							availableFlightSearchDTO.getJourneyType(), true);
					boolean combinationHasBus = false;
					for (AvailableIBOBFlightSegment ondFlightOption : combination.getMatchingFlightList()) {

						if (matchingCombinationKeysTemp.isEmpty()) {// AEROMART-3832
							matchingCombinationKeysTemp.addAll(ondFlightOption.getMatchingCombinationKeys());
						} else {
							for (String key : ondFlightOption.getMatchingCombinationKeys()) {
								if (matchingCombinationKeysTemp.contains(key)) {
									matchingCombinationKeys.add(key);
								} else {
									matchingCombinationKeysTemp.add(key);
								}
							}

						}

						boolean firstTime = false;
						if (!attempOndSplit.containsKey(ondFlightOption.getOndSequence())) {
							attempOndSplit.put(ondFlightOption.getOndSequence(), false);
							firstTime = true;
						}

						if (!ondFlightOption.isDirectFlight()) {
							boolean hasBus = false;

							boolean hasBusInsideOnd = hasBusInsideOnd(ondFlightOption);
							if (!hasBusInsideOnd) {
								mcFltCombination.addFlightSegment(ondFlightOption);
							} else {
								for (AvailableFlightSegment afs : ((AvailableConnectedFlight) ondFlightOption)
										.getAvailableFlightSegments()) {
									if (!afs.isBusSegment()) {
										map.put(afs, ondFlightOption);

										if (!matchingBusOptions.containsKey(afs)) {
											matchingBusOptions.put(afs, new ArrayList<>());
										}
										matchingBusOptions.get(afs).add(ondFlightOption);
										mcFltCombination.addFlightSegment(afs);
									} else {
										hasBus = true;
										combinationHasBus = true;
									}
								}
							}

							if (firstTime && hasBus) {
								attemptSplitQuote &= true;
								attempOndSplit.put(ondFlightOption.getOndSequence(), true);
							} else if (!firstTime) {
								if (hasBus && !attempOndSplit.get(ondFlightOption.getOndSequence())
										|| !hasBus && attempOndSplit.get(ondFlightOption.getOndSequence())) {
									attemptSplitQuote = false;
									break outer;
								}
							}
						} else if (!((AvailableFlightSegment) ondFlightOption).isBusSegment()) {
							mcFltCombination.addFlightSegment(ondFlightOption);
							if (attempOndSplit.get(ondFlightOption.getOndSequence())) {
								attemptSplitQuote = false;
								break outer;
							}
						} else {
							if (((AvailableFlightSegment) ondFlightOption).isBusSegment() && !firstTime
									&& !attempOndSplit.get(ondFlightOption.getOndSequence())) {
								attemptSplitQuote = false;
								break outer;
							}
							// combinationHasBus = true; //isSplitOperationForBusInvoked = true;
						}
					}
					if (combinationHasBus) {
						mcList.add(mcFltCombination);
					}
				}

				matchingCombinationKeysTemp.clear();
			}

			if (attemptSplitQuote && mcList.size() > 0) {
				AvailableFlightSearchDTO airSearchDTO = availableFlightSearchDTO.clone();
				AvailableFlightSearchDTO busSearchDTO = availableFlightSearchDTO.clone();
				isSplitOperationForBusInvoked = true;
				List<List<AvailableIBOBFlightSegment>> selectedOndFlights = new ArrayList<>();

				for (MatchingCombination mc : mcList) {
					int ondSequence = 0;
					for (AvailableIBOBFlightSegment ondOption : mc.getMatchingFlightList()) {
						if (selectedOndFlights.size() == ondSequence) {
							selectedOndFlights.add(new ArrayList<>());
						}
						selectedOndFlights.get(ondSequence).add(ondOption);
						ondOption.setOndSequence(ondSequence);
						ondSequence++;
					}
				}

				List<AvailableIBOBFlightSegment> ondBestOptions = getBestOndFlightCombination(selectedOndFlights, airSearchDTO,
						mcList);
				Map<Integer, String> selectedCos = new HashMap<Integer, String>();
				if (ondBestOptions.size() > 0) {
					List<AvailableIBOBFlightSegment> bestOptionForChargeQuote = new ArrayList<>();
					Set<String> combinationKeys = null;
					for (AvailableIBOBFlightSegment airOption : ondBestOptions) {
						if (combinationKeys == null) {
							combinationKeys = new HashSet<>();
							combinationKeys.addAll(airOption.getMatchingCombinationKeys());
						} else {
							combinationKeys.retainAll(airOption.getMatchingCombinationKeys());
						}
						for (FlightSegmentDTO seg : airOption.getFlightSegmentDTOs()) {
							selectedCos.put(seg.getSegmentId(), airOption.getSelectedLogicalCabinClass());
						}
					}

					for (String key : combinationKeys) {// AEROMART-3832
						if (matchingCombinationKeys.contains(key)) {
							matchingCombinationKeysTemp.add(key);
						}
					}

					combinationKeys.clear();
					combinationKeys.addAll(matchingCombinationKeysTemp);

					if (combinationKeys != null && combinationKeys.size() == 1) {
						String combinationKey = combinationKeys.iterator().next();
						MatchingCombination combination = mm.getMatchingCombinationFor(combinationKey);
						List<List<AvailableIBOBFlightSegment>> busFlts = new ArrayList<>();
						int ondSequence = 0;
						for (AvailableIBOBFlightSegment ondFlightOption : combination.getMatchingFlightList()) {

							if (!ondFlightOption.isDirectFlight()) {

								if (hasBusInsideOnd(ondFlightOption)) {

									for (AvailableFlightSegment afs : ((AvailableConnectedFlight) ondFlightOption)
											.getAvailableFlightSegments()) {
										if (afs.isBusSegment()) {
											List<AvailableIBOBFlightSegment> list = new ArrayList<>();
											isSplitOperationForBusInvoked = true;
											list.add(afs);
											busFlts.add(list);

											afs.setOndSequence(ondSequence++);
										} else {
											for (String logicalCCtype : afs.getAvailableLogicalCabinClasses()) {
												ondFlightOption.setSeatsAvailable(logicalCCtype,
														afs.isSeatsAvailable(logicalCCtype));
												ondFlightOption.setFareType(logicalCCtype, FareTypes.PER_FLIGHT_FARE);
											}
										}
										String cos = ondFlightOption.getSelectedLogicalCabinClass();
										if (ondFlightOption.getFlightSegmentDTOs() != null
												&& ondFlightOption.getFlightSegmentDTOs().size() > 0
												&& ondFlightOption.getSelectedLogicalCabinClass() == null) {
											for (FlightSegmentDTO segDTO : ondFlightOption.getFlightSegmentDTOs()) {
												if (cos == null) {
													cos = selectedCos.get(segDTO.getSegmentId());
												}
											}
											ondFlightOption.setSelectedLogicalCabinClass(cos);
										}
									}
								}

							} else if (((AvailableFlightSegment) ondFlightOption).isBusSegment()) {
								String cos = ondFlightOption.getSelectedLogicalCabinClass();
								if (ondFlightOption.getFlightSegmentDTOs() != null
										&& ondFlightOption.getFlightSegmentDTOs().size() > 0) {
									cos = selectedCos
											.get(ondFlightOption.getFlightSegmentDTOs().iterator().next().getSegmentId());
								}
								ondFlightOption.setSelectedLogicalCabinClass(cos);
								List<AvailableIBOBFlightSegment> list = new ArrayList<>();
								list.add(ondFlightOption);
								busFlts.add(list);
								ondFlightOption.setOndSequence(ondSequence++);
							}
						}

						getBestOndFlightCombination(busFlts, busSearchDTO, null);
						ondSequence = 0;
						for (AvailableIBOBFlightSegment ondFlightOption : combination.getMatchingFlightList()) {
							ondFlightOption.setOndSequence(ondSequence++);
							ondFlightOption.setSplitOperationForBusInvoked(isSplitOperationForBusInvoked);
							bestOptionForChargeQuote.add(ondFlightOption);
						}
						selectedFlightChargeQuote(selectedFlightDTO, availableFlightSearchDTO, skipChargeQuote,
								bestOptionForChargeQuote, bestOptionForChargeQuote, new HashMap<Integer, BundledFareDTO>());
					}
				} else {
					for (AvailableIBOBFlightSegment fltSeg : selectedFlightDTO.getOndWiseMinimumSegFareFlights()) {// TODO
																													// clean
																													// this
						fltSeg.setSplitOperationForBusInvoked(isSplitOperationForBusInvoked);
					}
					for (List<AvailableIBOBFlightSegment> availableIBOBFlightSegments : selectedFlightDTO
							.getOndWiseSelectedDateAllFlights()) {
						for (AvailableIBOBFlightSegment fltSeg : availableIBOBFlightSegments) {
							fltSeg.setSplitOperationForBusInvoked(isSplitOperationForBusInvoked);
						}
					}
					selectedFlightDTO = actualSelectedQuote(selectedFlightDTO, availableFlightSearchDTO, skipChargeQuote);
				}
			} else {
				for (AvailableIBOBFlightSegment fltSeg : selectedFlightDTO.getOndWiseMinimumSegFareFlights()) {
					fltSeg.setSplitOperationForBusInvoked(isSplitOperationForBusInvoked);
				}
				for (List<AvailableIBOBFlightSegment> availableIBOBFlightSegments : selectedFlightDTO
						.getOndWiseSelectedDateAllFlights()) {
					for (AvailableIBOBFlightSegment fltSeg : availableIBOBFlightSegments) {
						fltSeg.setSplitOperationForBusInvoked(isSplitOperationForBusInvoked);
					}
				}
				selectedFlightDTO = actualSelectedQuote(selectedFlightDTO, availableFlightSearchDTO, skipChargeQuote);
			}
		} else {
			selectedFlightDTO = actualSelectedQuote(selectedFlightDTO, availableFlightSearchDTO, skipChargeQuote);
		}
		return selectedFlightDTO;
	}

	private boolean hasBusInsideOnd(AvailableIBOBFlightSegment ondFlightOption) {
		boolean hasBusSegment = false;

		for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) ondFlightOption)
				.getAvailableFlightSegments()) {

			if (availableFlightSegment.isBusSegment()) {
				hasBusSegment = true;
				break;
			}
		}

		return hasBusSegment;
	}

	private void setServiceTaxInformation(AvailableIBOBFlightSegment availableIBOBFlightSegment,
			AvailableFlightSearchDTO availableFlightSearchDTO,
			HashMap<String, HashMap<String, QuotedChargeDTO>> chargesPerSegmentPerGroup) throws ModuleException {
		Collection<QuoteChargesCriteria> criteriaCollection = generateCriteriaCollectionForTicketingRevenue(
				availableIBOBFlightSegment, availableFlightSearchDTO);
		HashMap<String, HashMap<String, QuotedChargeDTO>> serviceTaxes = getChargeBD().quoteCharges(criteriaCollection);
		Map<String, List<String>> serviceTaxWiseExcludableChargeCodes = null;

		if (serviceTaxes != null && !serviceTaxes.isEmpty() && !serviceTaxes.values().isEmpty()) {
			Collection<String> quotedServiceTaxCodesForFirstOnd = null;
			for (Entry<String, HashMap<String, QuotedChargeDTO>> ondWiseQuotedChargesEntry : serviceTaxes.entrySet()) {
				quotedServiceTaxCodesForFirstOnd = ondWiseQuotedChargesEntry.getValue().keySet();
				break;
			}

			HashSet<String> reservationChargeCodes = new HashSet<>();
			for (Map<String, QuotedChargeDTO> charges : chargesPerSegmentPerGroup.values()) {
				reservationChargeCodes.addAll(charges.keySet());
			}

			if (quotedServiceTaxCodesForFirstOnd != null && !quotedServiceTaxCodesForFirstOnd.isEmpty()
					&& !reservationChargeCodes.isEmpty()) {
				serviceTaxWiseExcludableChargeCodes = getChargeJdbcDAO()
						.getServiceTaxWiseExcludableChargeCodes(quotedServiceTaxCodesForFirstOnd, reservationChargeCodes);
			}
			availableIBOBFlightSegment.setServiceTaxes(serviceTaxes.entrySet().iterator().next().getValue());
			availableIBOBFlightSegment.setServiceTaxExcludedCharges(serviceTaxWiseExcludableChargeCodes);
		}
	}

	private ChargeJdbcDAO getChargeJdbcDAO() {
		return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
	}

	private Collection<QuoteChargesCriteria> generateCriteriaCollectionForTicketingRevenue(
			AvailableIBOBFlightSegment availableIBOBFlightSegment, AvailableFlightSearchDTO availableFlightSearchDTO) {
		String pointOfEmbarkationCountry = null;
		String pointOfEmbarkationState = null;

		Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<>();
		for (FlightSegmentDTO flightSegmentDTO : availableIBOBFlightSegment.getFlightSegmentDTOs()) {
			if (AppSysParamsUtil.getDefaultCarrierCode().equals(flightSegmentDTO.getCarrierCode())) {
				QuoteChargesCriteria criterion = new QuoteChargesCriteria();
				criterion.setServiceTaxQuote(true);
				String firstSegment = flightSegmentDTO.getSegmentCode();
				criterion.setOnd(firstSegment);
				if (firstSegment != null) {
					String pointOfEmbarkationAirport = availableFlightSearchDTO.getFirstDepartingAirport();
					if (pointOfEmbarkationAirport != null) {
						Object[] pointOfEmbarkationInfo = CommonsServices.getGlobalConfig()
								.retrieveAirportInfo(pointOfEmbarkationAirport);

						if (pointOfEmbarkationInfo == null) {
							try {
								Map<String, CachedAirportDTO> mapAirports = AirInventoryModuleUtils.getAirportBD()
										.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(pointOfEmbarkationAirport)));

								if (mapAirports != null && !mapAirports.isEmpty()
										&& mapAirports.get(pointOfEmbarkationAirport) != null) {
									pointOfEmbarkationCountry = mapAirports.get(pointOfEmbarkationAirport).getCountryCode();
								}

							} catch (ModuleException e) {
								log.info("Airport details not found for : " + pointOfEmbarkationAirport);
							}
						} else {
							pointOfEmbarkationCountry = pointOfEmbarkationInfo[3].toString();

							if (pointOfEmbarkationInfo[5] != null) {
								pointOfEmbarkationState = pointOfEmbarkationInfo[5].toString();
							}
						}
					}
				}
				if (CalendarUtil.isLessThan(flightSegmentDTO.getDepartureDateTimeZulu(),
						CalendarUtil.getCurrentSystemTimeInZulu())) {
					criterion.setDepatureDate(CalendarUtil.getCurrentSystemTimeInZulu());
				} else {
					criterion.setDepatureDate(flightSegmentDTO.getDepartureDateTimeZulu());
				}
				criterion.setCabinClassCode(availableIBOBFlightSegment.getCabinClassCode());
				criterion.setChargeQuoteDate(new Date());
				criterion.setChannelId(availableFlightSearchDTO.getChannelCode());
				criteriaCollection.add(criterion);
				break;
			}
		}
		for (QuoteChargesCriteria criterion : criteriaCollection) {
			criterion.setServiceTaxDepositeStateCode(pointOfEmbarkationState);
			criterion.setServiceTaxDepositeCountryCode(pointOfEmbarkationCountry);
			criterion.setServiceTaxPlaceOfSupplyStateCode(pointOfEmbarkationState);
			criterion.setServiceTaxPlaceOfSupplyCountryCode(pointOfEmbarkationCountry);
		}
		return criteriaCollection;
	}

	public SelectedFlightDTO actualSelectedQuote(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote) throws ModuleException {

		ContextOndFlightsTO contextOndFltInfo = new ContextOndFlightsTO();
		List<AvailableIBOBFlightSegment> availableIBOBFlightSegments = getBestOndFlightCombination(
				selectedFlightDTO.getOndWiseSelectedDateAllFlights(), availableFlightSearchDTO, null);

		if (availableIBOBFlightSegments.size() > 0) {
			selectedFlightDTO.getSelectedOndFlights().clear();
			int ondSequence = 0;
			for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableIBOBFlightSegments) {
				selectedFlightDTO.setSelectedOndFlight(ondSequence, availableIBOBFlightSegment);
				contextOndFltInfo.addOndFlight(availableIBOBFlightSegment);
				ondSequence++;
			}
		}
		boolean isIbeBooking = AirinventoryUtils.isIbeBooking(availableFlightSearchDTO.getAppIndicator());
		boolean isReturnFareSelected = AirinventoryUtils.isReturnFareSelected(availableIBOBFlightSegments);

		// Prepare flight for charge quote. XBE only selected minimum fare flight. IBE selected date all flights
		Collection<AvailableIBOBFlightSegment> availableFlightSegments = new ArrayList<AvailableIBOBFlightSegment>();
		if (availableFlightSearchDTO.isFareCalendarSearch()) {
			for (List<AvailableIBOBFlightSegment> ondFlights : selectedFlightDTO.getOndWiseSelectedDateAllFlights()) {
				availableFlightSegments.addAll(ondFlights);
			}
		} else {
			availableFlightSegments.addAll(availableIBOBFlightSegments);
		}
		Map<Integer, BundledFareDTO> ondSelectedBundledFare = new HashMap<Integer, BundledFareDTO>();
		selectedFlightDTO = selectedFlightChargeQuote(selectedFlightDTO, availableFlightSearchDTO, skipChargeQuote,
				availableIBOBFlightSegments, availableFlightSegments, ondSelectedBundledFare);

		// Segment fares related to the selected return fares
		List<AvailableIBOBFlightSegment> selectedOndFlightsWithSegFares = null;
		// If it's an IBE booking and return fares are selected, get the related segment fares
		if (availableFlightSearchDTO.isQuoteReturnSegmentDiscount() && isIbeBooking && isReturnFareSelected
				&& AppSysParamsUtil.enableIBEFakeDiscounts() && !AppSysParamsUtil.showHalfReturnFaresInIBECalendar()) {

			AvailableFlightSearchDTO clonedAvailSearchDto = availableFlightSearchDTO.clone();
			for (OriginDestinationInfoDTO ondInfoDto : clonedAvailSearchDto.getOriginDestinationInfoDTOs()) {
				ondInfoDto.setPreferredBundledFarePeriodId(null);
				ondInfoDto.setPreferredBookingClass(null);
				ondInfoDto.setSegmentBookingClass(null);
			}

			selectedFlightDTO.setReturnFareQuote(true);
			List<AvailableIBOBFlightSegment> newAvailableFlightSegList = new ArrayList<AvailableIBOBFlightSegment>();
			for (AvailableIBOBFlightSegment selectedOndFlightSeg : selectedFlightDTO.getSelectedOndFlights()) {
				AvailableIBOBFlightSegment flightSegment = selectedOndFlightSeg.getCleanedInstance();
				flightSegment.setServiceTaxes(selectedOndFlightSeg.getServiceTaxes());
				flightSegment.setServiceTaxExcludedCharges(selectedOndFlightSeg.getServiceTaxExcludedCharges());
				newAvailableFlightSegList.add(flightSegment);
			}
			selectedOndFlightsWithSegFares = getAvailableIBOBFlightSegments(clonedAvailSearchDto,
					AirinventoryUtils.getElementsAsList(newAvailableFlightSegList), true, true, null);

			List<List<AvailableIBOBFlightSegment>> ondFlightList = new ArrayList<List<AvailableIBOBFlightSegment>>();
			for (AvailableIBOBFlightSegment seg : selectedOndFlightsWithSegFares) {
				List<AvailableIBOBFlightSegment> segList = new ArrayList<AvailableIBOBFlightSegment>();
				segList.add(seg);
				ondFlightList.add(segList);
			}
			if (selectedOndFlightsWithSegFares.size() > 0) {
				populateEligibleReturnFareForCalendar(availableFlightSearchDTO, ondFlightList);
				selectedFlightDTO.getOndWiseMinimumSegFareFlights().clear();
				int ondSequence = 0;
				for (AvailableIBOBFlightSegment availableIBOBFlightSegment : selectedOndFlightsWithSegFares) {
					selectedFlightDTO.setSelectedOndSegFlight(ondSequence, availableIBOBFlightSegment);
					ondSequence++;
				}
			}

			if (!skipChargeQuote) {
				getBundledFareBL().getApplicableBundledFares(selectedOndFlightsWithSegFares, clonedAvailSearchDto);
				for (AvailableIBOBFlightSegment availableIBOBFlightSegment : selectedOndFlightsWithSegFares) {
					availableIBOBFlightSegment
							.setSelectedBundledFare(ondSelectedBundledFare.get(availableIBOBFlightSegment.getOndSequence()));
					ContextOndFlightsTO contextOndSegFltInfo = new ContextOndFlightsTO();
					contextOndSegFltInfo.addOndFlight(availableIBOBFlightSegment, 0);
					SelectedFlightDTO selectedFlight = availableIBOBFlightSegment.getSelectedFlightDTO();
					availableIBOBFlightSegment.setSelectedFlightDTO(null);
					setChargesForSelectedFlight(availableIBOBFlightSegment, clonedAvailSearchDto, contextOndSegFltInfo);
					getBundledFareBL().updateBundledFareFee(availableIBOBFlightSegment);
					getBundledFareBL().calculateTaxPortionForBundledFee(availableIBOBFlightSegment);
					for (String logicalCC : availableIBOBFlightSegment.getAvailableLogicalCabinClasses()) {
						availableIBOBFlightSegment.getTotalCharges(logicalCC);
					}
					availableIBOBFlightSegment.setSelectedFlightDTO(selectedFlight);
				}
			}

		}

		return selectedFlightDTO;
	}

	private SelectedFlightDTO selectedFlightChargeQuote(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote,
			List<AvailableIBOBFlightSegment> bestOndFlightSegments,
			Collection<AvailableIBOBFlightSegment> ondFlightsForChargeQuote, Map<Integer, BundledFareDTO> ondSelectedBundledFare)
			throws ModuleException {
		ContextOndFlightsTO contextOndFltInfo = new ContextOndFlightsTO();
		if (bestOndFlightSegments.size() > 0) {
			selectedFlightDTO.getSelectedOndFlights().clear();
			int ondSequence = 0;
			for (AvailableIBOBFlightSegment availableIBOBFlightSegment : bestOndFlightSegments) {
				selectedFlightDTO.setSelectedOndFlight(ondSequence, availableIBOBFlightSegment);
				contextOndFltInfo.addOndFlight(availableIBOBFlightSegment);
				ondSequence++;
			}
		}
		if (!skipChargeQuote) {
			setFlightSearchGap(contextOndFltInfo, availableFlightSearchDTO);
			getBundledFareBL().getApplicableBundledFares(ondFlightsForChargeQuote, availableFlightSearchDTO);

			for (AvailableIBOBFlightSegment availableIBOBFlightSegment : ondFlightsForChargeQuote) {
				getBundledFareBL().setPreferredBundledFareDetails(availableIBOBFlightSegment, availableFlightSearchDTO, true);
				setChargesForSelectedFlight(availableIBOBFlightSegment, availableFlightSearchDTO, contextOndFltInfo);
				getBundledFareBL().updateBundledFareFee(availableIBOBFlightSegment);
				getBundledFareBL().removeAvailableBundledFaresForFlown(availableIBOBFlightSegment);
				if (availableFlightSearchDTO.isFareCalendarSearch()) {
					getBundledFareBL().calculateTaxPortionForBundledFee(availableIBOBFlightSegment);
				}

				ondSelectedBundledFare.put(availableIBOBFlightSegment.getOndSequence(),
						availableIBOBFlightSegment.getSelectedBundledFare());
			}
		}

		if (selectedFlightDTO.isOpenReturn()) {
			selectedFlightDTO = injectOpenReturnBookingClass(selectedFlightDTO);
		}
		return selectedFlightDTO;
	}

	private List<AvailableIBOBFlightSegment> getBestOndFlightCombination(List<List<AvailableIBOBFlightSegment>> ondFlightOptions,
			AvailableFlightSearchDTO availableFlightSearchDTO, List<MatchingCombination> mcList) throws ModuleException {
		FareQuoteUtil.setHalfReturnFareQuoteOption(availableFlightSearchDTO, availableFlightSearchDTO.isModifyBooking());
		if (AppSysParamsUtil.isEnableTotalPriceBasedMinimumFareQuote()) {
			setChargeRatios(ondFlightOptions, availableFlightSearchDTO);
		}
		List<AvailableIBOBFlightSegment> availableIBOBFlightSegments = getAvailableIBOBFlightSegments(availableFlightSearchDTO,
				ondFlightOptions, true, false, mcList);
		return availableIBOBFlightSegments;
	}

	public SelectedFlightDTO chargeQuoteSelectedFlight(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		ContextOndFlightsTO contextOndFltInfo = new ContextOndFlightsTO();
		for (AvailableIBOBFlightSegment ondFlight : selectedFlightDTO.getSelectedOndFlights()) {
			if (ondFlight != null) {
				contextOndFltInfo.addOndFlight(ondFlight);
			}
		}

		setFlightSearchGap(contextOndFltInfo, availableFlightSearchDTO);
		getBundledFareBL().getApplicableBundledFares(selectedFlightDTO.getSelectedOndFlights(), availableFlightSearchDTO);
		Map<Integer, BundledFareDTO> ondSelectedBundledFare = new HashMap<Integer, BundledFareDTO>();
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : selectedFlightDTO.getSelectedOndFlights()) {
			getBundledFareBL().setPreferredBundledFareDetails(availableIBOBFlightSegment, availableFlightSearchDTO, false);
			setChargesForSelectedFlight(availableIBOBFlightSegment, availableFlightSearchDTO, contextOndFltInfo);
			getBundledFareBL().updateBundledFareFee(availableIBOBFlightSegment);
			getBundledFareBL().removeAvailableBundledFaresForFlown(availableIBOBFlightSegment);
			if (availableFlightSearchDTO.isFareCalendarSearch()) {
				getBundledFareBL().calculateTaxPortionForBundledFee(availableIBOBFlightSegment);
			}

			ondSelectedBundledFare.put(availableIBOBFlightSegment.getOndSequence(),
					availableIBOBFlightSegment.getSelectedBundledFare());
		}

		getBundledFareBL().getApplicableBundledFares(selectedFlightDTO.getOndWiseMinimumSegFareFlights(),
				availableFlightSearchDTO);
		for (AvailableIBOBFlightSegment segFareOndFlight : selectedFlightDTO.getOndWiseMinimumSegFareFlights()) {
			segFareOndFlight.setSelectedBundledFare(ondSelectedBundledFare.get(segFareOndFlight.getOndSequence()));
			ContextOndFlightsTO contextOndSegFltInfo = new ContextOndFlightsTO();
			contextOndSegFltInfo.addOndFlight(segFareOndFlight, 0);
			segFareOndFlight.setSelectedFlightDTO(null);
			setChargesForSelectedFlight(segFareOndFlight, availableFlightSearchDTO, contextOndSegFltInfo);
			getBundledFareBL().updateBundledFareFee(segFareOndFlight);
			if (availableFlightSearchDTO.isFareCalendarSearch()) {
				getBundledFareBL().calculateTaxPortionForBundledFee(segFareOndFlight);
			}
			for (String logicalCC : segFareOndFlight.getAvailableLogicalCabinClasses()) {
				segFareOndFlight.getTotalCharges(logicalCC);
			}
			segFareOndFlight.setSelectedFlightDTO(selectedFlightDTO);
		}
		return selectedFlightDTO;
	}

	private void chargeDebug(Collection<AvailableIBOBFlightSegment> ondFlights) {
		if (log.isDebugEnabled() && ondFlights != null && ondFlights.size() > 0) {
			StringBuffer debugInfo = new StringBuffer();
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMMHH:mm");
			for (AvailableIBOBFlightSegment availableIBOBFlightSegment : ondFlights) {
				debugInfo.append("CHG_DEBUG," + availableIBOBFlightSegment.getOndCode());
				debugInfo.append("(" + availableIBOBFlightSegment.toString() + ")(");
				for (FlightSegmentDTO y : availableIBOBFlightSegment.getFlightSegmentDTOs()) {
					debugInfo.append(y.getFlightNumber() + ":" + sdf.format(y.getDepartureDateTime()) + ",");
				}
				debugInfo.append(")");
				if (availableIBOBFlightSegment != null
						&& availableIBOBFlightSegment.getAvailableLogicalCabinClasses().size() > 0) {
					for (String logicalCC : availableIBOBFlightSegment.getAvailableLogicalCabinClasses()) {
						debugInfo.append(logicalCC + "[" + availableIBOBFlightSegment.getTotalCharges(logicalCC) + " ==> "
								+ availableIBOBFlightSegment.getCharges(logicalCC) + "]");

					}
				}
				debugInfo.append("\n");
			}
			log.debug(debugInfo);
		}
	}

	public List<FilteredBucketsDTO> getSingleFlightOneWayCheapestFare(AvailableFlightSegment availableFlightSegment,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		List<FilteredBucketsDTO> clonedFilteredBucketsDTOs = new ArrayList<FilteredBucketsDTO>();
		List<AvailableIBOBFlightSegment> availableIBOBFlightSegments = new ArrayList<AvailableIBOBFlightSegment>();
		availableIBOBFlightSegments.add(availableFlightSegment);

		List<Set<String>> ondWiseAvailableLogicalCabinClasses = new ArrayList<Set<String>>();
		ondWiseAvailableLogicalCabinClasses.add(0, new HashSet<String>());
		boolean isOpenJaw = (JourneyType.OPEN_JAW == availableFlightSearchDTO.getJourneyType()) ? true : false;
		singleFlightOnewayAvailabilitySearch(availableFlightSegment, availableFlightSearchDTO);
		GroupedOndFlightsFareCalculator groupedOndFlightsFareCalculator = new GroupedOndFlightsFareCalculator(
				availableFlightSearchDTO, availableIBOBFlightSegments, getSystemSupportedFareTypes(
						availableFlightSearchDTO.isModifyBooking(), availableFlightSearchDTO.isOpenReturnSearch(), isOpenJaw));
		FilteredBucketsDTO filteredBucketsDTO = AirInventoryDataExtractUtil
				.getLastFilteredBucketsDTO(groupedOndFlightsFareCalculator.getMinimumFareBCAllocationSummaryDTO(
						Arrays.asList(availableFlightSegment.getFilteredBucketsDTOForSegmentFares()),
						availableFlightSegment.getCabinClassCode(), ondWiseAvailableLogicalCabinClasses));
		if (filteredBucketsDTO != null) {
			clonedFilteredBucketsDTOs.add(filteredBucketsDTO);
		}

		return clonedFilteredBucketsDTOs;
	}

	public AvailableFlightDTO nonSelectedDatesFareQuoteForIBE(AvailableFlightDTO availableFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean includeSelectedDate) throws ModuleException {
		if (AppSysParamsUtil.isEnableTotalPriceBasedMinimumFareQuote()) {
			List<List<AvailableIBOBFlightSegment>> ondFlightList = new ArrayList<List<AvailableIBOBFlightSegment>>();
			for (Integer ondSequence : availableFlightDTO.getOndSequenceIds()) {
				List<AvailableIBOBFlightSegment> availONDFlightSegs = availableFlightDTO.getAvailableOndFlights(ondSequence);
				List<AvailableIBOBFlightSegment> ondList = new ArrayList<AvailableIBOBFlightSegment>();
				if (!availONDFlightSegs.isEmpty()) {
					ondList.add(availONDFlightSegs.get(0)); // Setting only the first flight
				}
				ondFlightList.add(ondList);
			}
			if (ondFlightList.size() > 0) {
				setChargeRatios(ondFlightList, availableFlightSearchDTO);
			}
		}

		List<List<AvailableIBOBFlightSegment>> ondFlightList = new ArrayList<List<AvailableIBOBFlightSegment>>();
		for (Integer ondSequence : availableFlightDTO.getOndSequenceIds()) {
			List<AvailableIBOBFlightSegment> availONDFlightSegs = availableFlightDTO.getAvailableOndFlights(ondSequence);
			ondFlightList.add(availONDFlightSegs);
		}
		populateEligibleReturnFareForCalendar(availableFlightSearchDTO, ondFlightList);

		for (Integer ondSequence : availableFlightDTO.getOndSequenceIds()) {
			List<AvailableIBOBFlightSegment> availONDFlightSegs = availableFlightDTO.getAvailableOndFlights(ondSequence);
			if (!availONDFlightSegs.isEmpty()) {
				Map<String, List<AvailableIBOBFlightSegment>> dateWiseAvailableFlightSegments = null;
				if (availableFlightDTO.getSelectedFlight() != null
						&& availableFlightDTO.getSelectedFlight().getSelectedOndFlight(ondSequence) != null) {
					AvailableIBOBFlightSegment ondFlight = availableFlightDTO.getSelectedFlight()
							.getSelectedOndFlight(ondSequence);
					dateWiseAvailableFlightSegments = AirinventoryUtils.getDateWiseAvailableFlightSegments(ondFlight,
							availONDFlightSegs, includeSelectedDate);
				} else {

					dateWiseAvailableFlightSegments = AirinventoryUtils.getDateWiseAvailableFlightSegments(availONDFlightSegs);
				}
				for (String formattedDate : dateWiseAvailableFlightSegments.keySet()) {
					dateWiseAvailableFlightSegments.get(formattedDate)
							.removeAll(getAvailableIBOBFlightSegments(availableFlightSearchDTO,
									Arrays.asList(dateWiseAvailableFlightSegments.get(formattedDate)), false, true, null));
				}

				if (AppSysParamsUtil.showTotalFareInCalendar()) {
					for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availONDFlightSegs) {
						if (availableIBOBFlightSegment.getAvailableBundledFares() != null) {
							availableIBOBFlightSegment.setAvailableBundledFares(new HashMap<String, List<BundledFareLiteDTO>>());
						}
					}

					// Recall available bundled fares with correct booking class for non selected flights
					getBundledFareBL().getApplicableBundledFares(availONDFlightSegs, availableFlightSearchDTO);

					for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availONDFlightSegs) {

						// Charge ratios were set only for the first flight for approximations
						// Hence service taxes in first flight is set to other flights
						if (!CollectionUtils.isEmpty(ondFlightList) && !CollectionUtils.isEmpty(ondFlightList.get(ondSequence))) {
							availableIBOBFlightSegment.setServiceTaxes(ondFlightList.get(ondSequence).get(0).getServiceTaxes());
							availableIBOBFlightSegment.setServiceTaxExcludedCharges(
									ondFlightList.get(ondSequence).get(0).getServiceTaxExcludedCharges());
						}
						Date prefferedDate = availableFlightSearchDTO.getOndInfo(availableIBOBFlightSegment.getOndSequence())
								.getPreferredDateTimeStart();
						Date ondFlightDate = availableIBOBFlightSegment.getDepartureDate();
						boolean recalculateAvailableBundles = BundledFareBL.recalculateAvailableBundledFare(prefferedDate,
								ondFlightDate);

						ContextOndFlightsTO contextOndFltInfo = new ContextOndFlightsTO();
						contextOndFltInfo.addOndFlight(availableIBOBFlightSegment, 0);

						if (recalculateAvailableBundles) {
							getBundledFareBL().setPreferredBundledFareDetails(availableIBOBFlightSegment,
									availableFlightSearchDTO, false);
						}

						if (availableIBOBFlightSegment.getSelectedFlightDTO() == null) {
							setChargesForSelectedFlight(availableIBOBFlightSegment, availableFlightSearchDTO, contextOndFltInfo);
						}

						if (recalculateAvailableBundles) {
							getBundledFareBL().updateBundledFareFee(availableIBOBFlightSegment);
							getBundledFareBL().removeAvailableBundledFaresForFlown(availableIBOBFlightSegment);
							getBundledFareBL().calculateTaxPortionForBundledFee(availableIBOBFlightSegment);
						}
					}
					if (log.isDebugEnabled()) {
						chargeDebug(availONDFlightSegs);
					}
				}
			}
		}

		return availableFlightDTO;
	}

	private void setFlightSearchGap(ContextOndFlightsTO contextOndFltInfo, AvailableFlightSearchDTO availableFlightSearchDTO) {
		Double fareAmount = 0.00;
		int dateGap = 0;
		if (AppSysParamsUtil.isMatchOfferedFareForCookieBasedSurcharges()) {
			for (AvailableIBOBFlightSegment flightList : contextOndFltInfo.ondFlights) {
				if (!flightList.isSeatsAvailable(flightList.getSelectedLogicalCabinClass())) {
					return;
				}
				fareAmount += flightList.getFareAmount(flightList.getSelectedLogicalCabinClass(), PaxTypeTO.ADULT)
						* availableFlightSearchDTO.getAdultCount()
						+ flightList.getFareAmount(flightList.getSelectedLogicalCabinClass(), PaxTypeTO.CHILD)
								* availableFlightSearchDTO.getChildCount()
						+ flightList.getFareAmount(flightList.getSelectedLogicalCabinClass(), PaxTypeTO.INFANT)
								* availableFlightSearchDTO.getInfantCount();
			}

			List<Integer> dateGapList = availableFlightSearchDTO.getFlightDateFareMap().get(fareAmount);
			if (dateGapList != null) {
				Collections.sort(dateGapList);
				if (AirinventoryUtils.getAirInventoryConfig().isUseNearestSearchDateForApplyCookieCharges()) {
					dateGap = dateGapList.get(0);
				} else {
					dateGap = dateGapList.get(dateGapList.size() - 1);
				}
			}
		} else {

			for (List<Integer> dategaps : availableFlightSearchDTO.getFlightDateFareMap().values()) {
				if (dategaps.size() > 0) {
					Collections.sort(dategaps);
					if (AirinventoryUtils.getAirInventoryConfig().isUseNearestSearchDateForApplyCookieCharges()) {
						if (dateGap == 0 || dateGap > dategaps.get(0)) {
							dateGap = dategaps.get(0);
						}
					} else {
						if (dateGap == 0 || dateGap < dategaps.get(0)) {
							dateGap = dategaps.get(dategaps.size() - 1);
						}
					}

				}
			}
		}
		availableFlightSearchDTO.setFlightSearchGap(dateGap);
	}

	private Collection<GroupedOndFlightsFareCalculator> getGroupedOndFareCalculators(
			AvailableFlightSearchDTO availableFlightSearchDTO, List<List<AvailableIBOBFlightSegment>> ondAvailFlightSegments,
			List<MatchingCombination> mcList) {
		Collection<GroupedOndFlightsFareCalculator> groupedOndFlightsFareCalculators = new ArrayList<GroupedOndFlightsFareCalculator>();

		boolean isOpenJaw = (JourneyType.OPEN_JAW == availableFlightSearchDTO.getJourneyType()) ? true : false;
		List<IFareCalculator.FareCalType> systemSupportedFareTypes = getSystemSupportedFareTypes(
				availableFlightSearchDTO.isModifyBooking(), availableFlightSearchDTO.isOpenReturnSearch(), isOpenJaw);

		if (mcList != null && mcList.size() > 0) {
			for (MatchingCombination mc : mcList) {
				if (mc.isValid()) {
					groupedOndFlightsFareCalculators.add(new GroupedOndFlightsFareCalculator(availableFlightSearchDTO,
							mc.getMatchingFlightList(), systemSupportedFareTypes));
				}
			}
		} else if (ondAvailFlightSegments != null) {

			MatchMaker matchMaker = new MatchMaker(this, availableFlightSearchDTO.getJourneyType());
			for (List<AvailableIBOBFlightSegment> fltSegments : ondAvailFlightSegments) {
				if (!matchMaker.processOndFlights(fltSegments)) {
					break;
				}
			}

			if (matchMaker.isValid()) {
				for (MatchingCombination mc : matchMaker.getMatchingCombinationList()) {
					if (mc.isValid()) {
						groupedOndFlightsFareCalculators.add(new GroupedOndFlightsFareCalculator(availableFlightSearchDTO,
								mc.getMatchingFlightList(), systemSupportedFareTypes));
					}
				}
			}
		}
		return groupedOndFlightsFareCalculators;

	}

	/**
	 * @param takeSEGFareIfAvailable
	 *            boolean switch to enable retreival of Segment Fare's if available and skip Return fares.
	 * @param mcList
	 */
	private List<AvailableIBOBFlightSegment> getAvailableIBOBFlightSegments(AvailableFlightSearchDTO availableFlightSearchDTO,
			List<List<AvailableIBOBFlightSegment>> ondAvailFlightSegments, boolean returnMinFareONDOnly,
			boolean takeSEGFareIfAvailable, List<MatchingCombination> mcList) throws ModuleException {

		Collection<GroupedOndFlightsFareCalculator> groupedOndFlightsFareCalculators = getGroupedOndFareCalculators(
				availableFlightSearchDTO, ondAvailFlightSegments, mcList);
		PriceBasedMinimumCal pbmc = new PriceBasedMinimumCal();

		for (GroupedOndFlightsFareCalculator groupedOndFlightsFareCalculator : groupedOndFlightsFareCalculators) {
			List<AvailableIBOBFlightSegment> minimumFareavailableIBOBFlightSegments = null;
			if (takeSEGFareIfAvailable) {
				FareCalType[] fareCalTypes = AppSysParamsUtil.showHalfReturnFaresInIBECalendar()
						&& availableFlightSearchDTO.isIncludeHRTFares()
								? new FareCalType[] { FareCalType.SEG, FareCalType.CON, FareCalType.HRT }
								: new FareCalType[] { FareCalType.SEG, FareCalType.CON };
				minimumFareavailableIBOBFlightSegments = groupedOndFlightsFareCalculator.minimumFareAvailableFlight(pbmc,
						fareCalTypes);
				if (minimumFareavailableIBOBFlightSegments == null || minimumFareavailableIBOBFlightSegments.isEmpty()) {
					groupedOndFlightsFareCalculator.minimumFareAvailableFlight(pbmc);
				}
			} else {
				minimumFareavailableIBOBFlightSegments = groupedOndFlightsFareCalculator.minimumFareAvailableFlight(pbmc);
			}

			double groupedFlightsFare = AirInventoryDataExtractUtil.getFareAmount(minimumFareavailableIBOBFlightSegments,
					availableFlightSearchDTO);
			debugFareInfo(groupedOndFlightsFareCalculator, groupedFlightsFare);
		}
		List<AvailableIBOBFlightSegment> availableIBOBFlightSegments = new ArrayList<AvailableIBOBFlightSegment>();
		if (pbmc.hasMinimimFare()) {
			availableIBOBFlightSegments.addAll(pbmc.getMinimumGoffCal().getAvailableIBOBFlightSegments());
		}
		return availableIBOBFlightSegments;
	}

	private void populateEligibleReturnFareForCalendar(AvailableFlightSearchDTO availableFlightSearchDTO,
			List<List<AvailableIBOBFlightSegment>> ondAvailFlightSegments) throws ModuleException {

		if (ondAvailFlightSegments.size() == 2 && ondAvailFlightSegments.get(0).size() == 1) {

			Collection<GroupedOndFlightsFareCalculator> groupedOndFlightsFareCalculators = getGroupedOndFareCalculators(
					availableFlightSearchDTO, ondAvailFlightSegments, null);

			for (GroupedOndFlightsFareCalculator groupedOndFlightsFareCalculator : groupedOndFlightsFareCalculators) {
				groupedOndFlightsFareCalculator.updateMinimumFareByFareType(FareCalType.RT);
			}
		}
	}

	private void debugFareInfo(GroupedOndFlightsFareCalculator groupedOnd, double fare) {
		if (log.isDebugEnabled()) {
			log.debug(groupedOnd.toString() + "," + fare + "," + groupedOnd.debugFareInfo());
		}
	}

	private Map<Integer, FlightSegmentDTO> getSelectedFlightSegment(Map<Integer, List<FlightSegmentDTO>> ondWiseFltSegs,
			int selSegmentId) {
		Map<Integer, FlightSegmentDTO> ondWiseSelFlightSeg = new HashMap<Integer, FlightSegmentDTO>();
		for (Entry<Integer, List<FlightSegmentDTO>> entry : ondWiseFltSegs.entrySet()) {
			Integer ondSeq = entry.getKey();
			List<FlightSegmentDTO> lstFlightSegment = entry.getValue();
			for (FlightSegmentDTO flightSegmentDTO : lstFlightSegment) {
				if (flightSegmentDTO.getSegmentId().intValue() == selSegmentId) {
					ondWiseSelFlightSeg.put(ondSeq, flightSegmentDTO);
				}
			}
		}

		return ondWiseSelFlightSeg;
	}

	public AllFaresDTO getAllBucketFareCombinationsForSegment(AllFaresDTO allFaresDTO, FareFilteringCriteria criteria,
			AvailableFlightSearchDTO avlFltSearchDTO) throws ModuleException {
		/* bypassing selected agent code AARESAA-5289 */
		setSelectedAgent(avlFltSearchDTO);
		avlFltSearchDTO.setSkipInvCheckForFlown(true);
		List<FlightSegmentDTO> allFlightSegments = allFaresDTO.getAllFlightSegmentDTOList();
		Map<Integer, FlightSegmentDTO> ondWiseSelFlightSeg = getSelectedFlightSegment(allFaresDTO.getAllFlightSegmentDTOs(),
				allFaresDTO.getSelectedSegmentId());

		FlightSegmentDTO selectedFlightSegmentDTO = null;
		Integer selectedOndSeq = null;

		Set<Integer> colSelectedSeq = ondWiseSelFlightSeg.keySet();
		if (colSelectedSeq != null & !colSelectedSeq.isEmpty()) {
			selectedOndSeq = colSelectedSeq.iterator().next();
		}

		Collection<FlightSegmentDTO> colFltSeg = ondWiseSelFlightSeg.values();
		if (colFltSeg != null && !colFltSeg.isEmpty()) {
			selectedFlightSegmentDTO = colFltSeg.iterator().next();
		}

		boolean isSkipInvChkForFlown = AppSysParamsUtil.skipInvChkForFlownInRequote();

		if (selectedFlightSegmentDTO == null || selectedOndSeq == null) {
			throw new ModuleException("airinventory.arg.allfarequote.invalid", AirinventoryCustomConstants.INV_MODULE_CODE);
		}
		HRTAvailableFlightSearchDTO availableFlightSearchDTO = new HRTAvailableFlightSearchDTO(avlFltSearchDTO);
		try {
			FareQuoteUtil.setSearchCriteriaForModify(availableFlightSearchDTO, selectedFlightSegmentDTO, false);
		} catch (Exception e) {
			log.error(e);
		}

		boolean isOpenReturnSearch = availableFlightSearchDTO.isOpenReturnSearch();

		BucketFilteringCriteria bucketFilteringCriteria = new BucketFilteringCriteria(selectedFlightSegmentDTO, criteria,
				availableFlightSearchDTO, selectedOndSeq, isOpenReturnSearch);

		// set fares applicable for selected single flight segment
		boolean excludeSegFaresForInterlinedBookings = AirinventoryUtils.getAirInventoryConfig().isDisableSegFaresForInterlining()
				&& allFaresDTO
						.hasNonMarketingCarrierSegments(CommonsServices.getGlobalConfig().getActiveSysCarriersMap().keySet());

		boolean excludeSegmentFaresFromConOrRet = AppSysParamsUtil.isExcludeSegFareFromConAndRet()
				&& !allFaresDTO.isSingleSegmentFareQuote();

		boolean excludeOneWayFaresFromConRet = AppSysParamsUtil.isExcludeOneWayFareFromConRet()
				&& !allFaresDTO.isSingleSegmentFareQuote()
				&& (availableFlightSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP);

		if (excludeSegmentFaresFromConOrRet && AppSysParamsUtil.isCityBasedAvailabilityEnabled()) {
			String segmentCode = "";
			for (FlightSegmentDTO flightSegmentDTO : allFlightSegments) {
				if (!segmentCode.equals("") && !segmentCode.substring(segmentCode.length() - 3, segmentCode.length())
						.equals(flightSegmentDTO.getSegmentCode().substring(0, 3))) {
					excludeSegmentFaresFromConOrRet = false;
					break;
				}
				segmentCode = flightSegmentDTO.getSegmentCode();
			}
		}

		if ((!excludeSegmentFaresFromConOrRet && !excludeSegFaresForInterlinedBookings
				&& !availableFlightSearchDTO.isExcludeSegFares() && !isOpenReturnSearch)
				|| availableFlightSearchDTO.getJourneyType() == JourneyType.MULTI_SECTOR) {
			List<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
			flightSegmentDTOs.add(selectedFlightSegmentDTO);

			try {
				FareQuoteUtil.setSearchCriteriaForModify(availableFlightSearchDTO, selectedFlightSegmentDTO, false);
			} catch (Exception e) {
				log.error(e);
			}

			BucketFilteringCriteria criteriaForFilteringPerFlightFares = new BucketFilteringCriteria(selectedFlightSegmentDTO,
					criteria, availableFlightSearchDTO, selectedOndSeq, availableFlightSearchDTO.isOpenReturnSearch());

			criteriaForFilteringPerFlightFares.setReturnOnd(false);
			if (availableFlightSearchDTO.getModifiedOndFareType() != null
					&& availableFlightSearchDTO.getModifiedOndPaxFareAmount() != null
					&& availableFlightSearchDTO.getModifiedOndFareType() == 2 && avlFltSearchDTO.isRequoteFlightSearch()) {
				// get the prorated amount of actual fare amount for the modified seg
				BigDecimal ondFareAmount = AccelAeroCalculator
						.divide(AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(),
								FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE), 100);
				criteriaForFilteringPerFlightFares.setMinPaxFareAmount(ondFareAmount);
			}

			if (criteriaForFilteringPerFlightFares.isFlownOnd() && isSkipInvChkForFlown) {
				// criteriaForFilteringPerFlightFares.setAgentCode(null);
				criteriaForFilteringPerFlightFares.setCheckAvailability(false);
			}

			LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketFaresCombinationsForSegments = filterBuckets(
					flightSegmentDTOs, criteriaForFilteringPerFlightFares, false);
			if (allBucketFaresCombinationsForSegments != null) {
				List<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketFaresCombinationForSegmentsList = new ArrayList<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>();
				allBucketFaresCombinationForSegmentsList
						.add(allBucketFaresCombinationsForSegments.get(allFaresDTO.getSelectedSegmentId()));

				setCommonBookingCodeFareIdsCombinations(allBucketFaresCombinationForSegmentsList, false, false, 0);

				allFaresDTO.addSegmentFaresOfSpecifiedType(new Integer(FareTypes.PER_FLIGHT_FARE),
						allBucketFaresCombinationsForSegments);
				allFaresDTO.addOndCodeForFareType(new Integer(FareTypes.PER_FLIGHT_FARE),
						criteriaForFilteringPerFlightFares.getOndCode());
			}
		}

		List<FlightSegmentDTO> selOndFlightSegList = allFaresDTO.getAllFlightSegmentDTOs().get(selectedOndSeq);

		if (!isOpenReturnSearch && !selOndFlightSegList.isEmpty() && selOndFlightSegList.size() > 1
				&& !excludeOneWayFaresFromConRet) {
			String ondCode = prepareOndCode(selOndFlightSegList);

			// BucketFilteringCriteria criteriaForFilteringOndFares = bucketFilteringCriteria.clone();

			try {
				Date departureDate = null;
				Date arrivalDate = null;
				String fromAirport = null;
				String toAirport = null;
				for (FlightSegmentDTO fltSegment : selOndFlightSegList) {
					if (departureDate == null) {
						departureDate = fltSegment.getDepartureDateTime();
						fromAirport = fltSegment.getDepartureAirportCode();
					}
					toAirport = fltSegment.getArrivalAirportCode();
					arrivalDate = fltSegment.getArrivalDateTime();
				}

				FareQuoteUtil.setSearchCriteriaForModify(availableFlightSearchDTO, departureDate, arrivalDate, fromAirport,
						toAirport, false);
			} catch (Exception e) {
				log.error(e);
			}

			BucketFilteringCriteria criteriaForFilteringOndFares = new BucketFilteringCriteria(selectedFlightSegmentDTO, criteria,
					availableFlightSearchDTO, selectedOndSeq, availableFlightSearchDTO.isOpenReturnSearch());
			criteriaForFilteringOndFares.setOndCode(ondCode);
			criteriaForFilteringOndFares.setExcludeNonLowestPublicFare(false);

			if (availableFlightSearchDTO.getModifiedOndFareType() != null
					&& availableFlightSearchDTO.getModifiedOndPaxFareAmount() != null
					&& availableFlightSearchDTO.getModifiedOndFareType() == 2 && avlFltSearchDTO.isRequoteFlightSearch()) {
				// get the prorated amount of actual fare amount for the modified seg
				BigDecimal ondFareAmount = AccelAeroCalculator
						.divide(AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(),
								FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE), 100);
				criteriaForFilteringOndFares.setMinPaxFareAmount(ondFareAmount);
			} else if (availableFlightSearchDTO.getModifiedOndFareType() != null
					&& availableFlightSearchDTO.getModifiedOndPaxFareAmount() != null
					&& availableFlightSearchDTO.getModifiedOndFareType() == 1 && avlFltSearchDTO.isRequoteFlightSearch()) {
				criteriaForFilteringOndFares.setMinPaxFareAmount(availableFlightSearchDTO.getModifiedOndPaxFareAmount());
			} else if (availableFlightSearchDTO.getModifiedOndFareType() != null
					&& availableFlightSearchDTO.getModifiedOndPaxFareAmount() != null
					&& availableFlightSearchDTO.getModifiedOndFareType() == 0 && avlFltSearchDTO.isRequoteFlightSearch()) {
				// get the full amount of actual fare amount for the modified seg
				BigDecimal ondFareAmount = AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(),
						selOndFlightSegList.size());
				criteriaForFilteringOndFares.setMinPaxFareAmount(ondFareAmount);
			}

			LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketOndFaresCombinationsForSegments = filterBuckets(
					selOndFlightSegList, criteriaForFilteringOndFares, false);
			if (allBucketOndFaresCombinationsForSegments != null) {
				List<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketOndFaresCombinationForSegmentsList = new ArrayList<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>();
				allBucketOndFaresCombinationForSegmentsList
						.add(allBucketOndFaresCombinationsForSegments.get(allFaresDTO.getSelectedSegmentId()));
				for (Integer segmentId : allBucketOndFaresCombinationsForSegments.keySet()) {
					if (segmentId != allFaresDTO.getSelectedSegmentId()) {
						allBucketOndFaresCombinationForSegmentsList.add(allBucketOndFaresCombinationsForSegments.get(segmentId));
					}
				}

				setCommonBookingCodeFareIdsCombinations(allBucketOndFaresCombinationForSegmentsList,
						bucketFilteringCriteria.isExcludeNonLowestPublicFare(), false, 0);

				allFaresDTO.addSegmentFaresOfSpecifiedType(new Integer(FareTypes.OND_FARE),
						allBucketOndFaresCombinationsForSegments);
				allFaresDTO.addOndCodeForFareType(new Integer(FareTypes.OND_FARE), criteriaForFilteringOndFares.getOndCode());

			}

		}

		if (availableFlightSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP
				|| availableFlightSearchDTO.getJourneyType() == JourneyType.CIRCULAR
				|| availableFlightSearchDTO.getJourneyType() == JourneyType.OPEN_JAW) {
			LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketReturnFaresCombinationsForSegments = new LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>();
			// BucketFilteringCriteria criteriaForFilteringFares = bucketFilteringCriteria.clone();

			Map<Integer, Boolean> ondSequenceReturnMap = new HashMap<Integer, Boolean>();
			Integer openJawFirstInBoundSeq = 1;

			if (availableFlightSearchDTO.getJourneyType() == JourneyType.OPEN_JAW
					|| availableFlightSearchDTO.getJourneyType() == JourneyType.CIRCULAR) {
				ondSequenceReturnMap = avlFltSearchDTO.getOndSequenceReturnMap();
				for (Entry<Integer, Boolean> ondSeqEntry : ondSequenceReturnMap.entrySet()) {
					if (ondSeqEntry.getValue()) {
						openJawFirstInBoundSeq = ondSeqEntry.getKey();
						break;
					}
				}
			}

			// following ond-code is required to show the correct label on selected flights PRORATED RETURN fares when
			// journey is open-jaw/circular in All Booking Class Fares option
			String selectedFltOndCode = null;
			// get OnD wise return buckets
			for (Entry<Integer, List<FlightSegmentDTO>> ondEntry : allFaresDTO.getAllFlightSegmentDTOs().entrySet()) {
				int ondSequence = ondEntry.getKey();
				List<FlightSegmentDTO> fltSegList = ondEntry.getValue();

				// quote fares for ond with return
				String ondCode = prepareOndCode(fltSegList);

				try {
					Date departureDate = null;
					Date arrivalDate = null;
					String fromAirport = null;
					String toAirport = null;
					for (FlightSegmentDTO fltSegment : fltSegList) {
						if (departureDate == null) {
							departureDate = fltSegment.getDepartureDateTime();
							fromAirport = fltSegment.getDepartureAirportCode();
						}
						toAirport = fltSegment.getArrivalAirportCode();
						arrivalDate = fltSegment.getArrivalDateTime();
					}

					FareQuoteUtil.setSearchCriteriaForModify(availableFlightSearchDTO, departureDate, arrivalDate, fromAirport,
							toAirport, availableFlightSearchDTO.isHalfReturnFareQuote());
				} catch (Exception e) {
					log.error(e);
				}

				BucketFilteringCriteria criteriaForFilteringFares = new BucketFilteringCriteria(selectedFlightSegmentDTO,
						criteria, availableFlightSearchDTO, selectedOndSeq, availableFlightSearchDTO.isOpenReturnSearch());

				criteriaForFilteringFares.setRequestedLogicalCabinClassCode(
						availableFlightSearchDTO.getOrderedOriginDestinations().get(ondSequence).getPreferredLogicalCabin());

				// criteriaForFilteringFares.setReturnFlag(true);

				// departure datetime is set, so that stay overdays are calculated properly
				FlightSegmentDTO flightSegmentDTO = (fltSegList.iterator().next());
				criteriaForFilteringFares.setDepatureDateAndTime(flightSegmentDTO.getDepartureDateTime());
				criteriaForFilteringFares.setDepartureDateTimeZulu(flightSegmentDTO.getDepartureDateTimeZulu());
				criteriaForFilteringFares.setArrivalDateTime(flightSegmentDTO.getArrivalDateTime());
				criteriaForFilteringFares.setExcludeNonLowestPublicFare(false);
				criteriaForFilteringFares.setFlownOnd(availableFlightSearchDTO.isFlownOnd(ondSequence));
				criteriaForFilteringFares.setUnTouchedOnd(availableFlightSearchDTO.isUnTouchedOnd(ondSequence));

				if (availableFlightSearchDTO.isQuoteSameFareBucket() && criteriaForFilteringFares.isUnTouchedOnd()) {
					criteriaForFilteringFares.setQuoteSameFareBucket(true);
				} else {
					criteriaForFilteringFares.setQuoteSameFareBucket(false);
				}
				if (availableFlightSearchDTO.isSameFlightSearched(selectedOndSeq, selectedFlightSegmentDTO.getSegmentId())) {
					String bookedCC = availableFlightSearchDTO.getBookedFlightCabinClass(selectedFlightSegmentDTO.getSegmentId());
					criteriaForFilteringFares.setBookedFltCabinBeingSearched(
							availableFlightSearchDTO.isSameCabinClassSearched(selectedOndSeq, bookedCC));
				}

				if (availableFlightSearchDTO.isSameFlightSearched(ondSequence, flightSegmentDTO.getSegmentId())) {
					String bookedCC = availableFlightSearchDTO.getBookedFlightCabinClass(flightSegmentDTO.getSegmentId());
					criteriaForFilteringFares.setBookedFlightCabinClass(bookedCC);
				} else {
					criteriaForFilteringFares.setBookedFlightCabinClass(null);
				}

				if (availableFlightSearchDTO.isSameFlightSearched(ondSequence, flightSegmentDTO.getSegmentId())) {
					criteriaForFilteringFares.setBookedFlightBookingClass(
							availableFlightSearchDTO.getBookedFlightBookingClass(flightSegmentDTO.getSegmentId()));
				} else {
					criteriaForFilteringFares.setBookedFlightBookingClass(null);
				}

				// if journey is roundtrip, set return fare quote related filtering params
				if (availableFlightSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP) {
					if (availableFlightSearchDTO.getModifiedOndFareType() != null
							&& availableFlightSearchDTO.getModifiedOndPaxFareAmount() != null
							&& availableFlightSearchDTO.getEnforceFareCheckForModOnd()) {
						if (availableFlightSearchDTO.getOriginDestinationInfoDTOs() != null) {
							String ondFromSegment = getONDFromSegments(fltSegList);
							BigDecimal maxFare = availableFlightSearchDTO.getModifiedOndPaxFareAmount();
							for (OriginDestinationInfoDTO originDestinationInfoDTO : availableFlightSearchDTO
									.getOriginDestinationInfoDTOs()) {
								if (((ondFromSegment.endsWith(originDestinationInfoDTO.getOrigin())
										&& ondFromSegment.startsWith(originDestinationInfoDTO.getDestination()))
										|| (ondFromSegment.endsWith(originDestinationInfoDTO.getDestination())
												&& ondFromSegment.startsWith(originDestinationInfoDTO.getOrigin())))
										&& maxFare.longValue() < originDestinationInfoDTO.getOldPerPaxFare().longValue()) {
									// in half return fare quote we should get max fare from inbound and outbound to
									// get same or higher return fare
									maxFare = originDestinationInfoDTO.getOldPerPaxFare();
								}
							}
							if (availableFlightSearchDTO.getModifiedOndFareType() != 2) {
								// get the prorated full amount of actual fare amount for the modified seg
								maxFare = AccelAeroCalculator.divide(AccelAeroCalculator.multiply(maxFare, 100),
										FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE);
							}
							criteriaForFilteringFares.setMinPaxFareAmount(maxFare);
						}
					}

					// if (ondSequence == OndSequence.OUT_BOUND) {
					// If journey type is round trip, only two Onds exists. So, OndSequence 1 will be the return Ond
					List<FlightSegmentDTO> inboundFlightSegList = allFaresDTO.getAllFlightSegmentDTOs().get(OndSequence.IN_BOUND);
					if (inboundFlightSegList != null && inboundFlightSegList.iterator().hasNext()) {
						FlightSegmentDTO inBoundFlightSegmentDTO = (inboundFlightSegList.iterator().next());
						criteriaForFilteringFares.setInboundDate(inBoundFlightSegmentDTO.getDepartureDateTime());
						criteriaForFilteringFares.setInboundDateZulu(inBoundFlightSegmentDTO.getDepartureDateTimeZulu());

						if (inBoundFlightSegmentDTO.getDepartureDateTimeZulu() != null) {
							criteriaForFilteringFares.setStayOverTime(inBoundFlightSegmentDTO.getDepartureDateTimeZulu().getTime()
									- flightSegmentDTO.getDepartureDateTimeZulu().getTime());
						}
					} else if (isOpenReturnSearch) {
						Date maxDate = availableFlightSearchDTO.getReturnValidityPeriodTO()
								.getDerivedBoundaryDate(flightSegmentDTO.getDepartureDateTimeZulu());
						criteriaForFilteringFares
								.setStayOverTime(maxDate.getTime() - flightSegmentDTO.getDepartureDateTimeZulu().getTime());
					} else if (availableFlightSearchDTO.isHalfReturnFareQuote()) {
						criteriaForFilteringFares.setReturnFareRule(true);

						if (availableFlightSearchDTO.isInboundFareQuote()) {
							ondCode = ReservationApiUtils.getInverseOndCode(ondCode);
						}
					}
					criteriaForFilteringFares.setReturnOnd(false);
					// criteriaForFilteringFares.setInboundFlight(false);
					// } else
					if (ondSequence == OndSequence.IN_BOUND) {
						// criteriaForFilteringFares.setInboundFlight(true);

						List<FlightSegmentDTO> outboundFlightSegList = allFaresDTO.getAllFlightSegmentDTOs()
								.get(OndSequence.OUT_BOUND);
						if (outboundFlightSegList != null && outboundFlightSegList.iterator().hasNext()) {
							FlightSegmentDTO outBoundFlightSegmentDTO = (outboundFlightSegList.iterator().next());
							criteriaForFilteringFares.setDepatureDateAndTime(outBoundFlightSegmentDTO.getDepartureDateTime());
							criteriaForFilteringFares
									.setDepartureDateTimeZulu(outBoundFlightSegmentDTO.getDepartureDateTimeZulu());
							criteriaForFilteringFares.setArrivalDateTime(outBoundFlightSegmentDTO.getArrivalDateTime());

							if (flightSegmentDTO.getDepartureDateTimeZulu() != null) {
								criteriaForFilteringFares.setStayOverTime(flightSegmentDTO.getDepartureDateTimeZulu().getTime()
										- outBoundFlightSegmentDTO.getDepartureDateTimeZulu().getTime());
							}
						}

						ondCode = prepareOndCode(outboundFlightSegList);
						criteriaForFilteringFares.setReturnOnd(true);
					}
				} else if (availableFlightSearchDTO.getJourneyType() == JourneyType.OPEN_JAW
						|| availableFlightSearchDTO.getJourneyType() == JourneyType.CIRCULAR) {

					if (ondSequence == OndSequence.IN_BOUND) {
						List<FlightSegmentDTO> outboundFlightSegList = allFaresDTO.getAllFlightSegmentDTOs()
								.get(OndSequence.OUT_BOUND);
						if (outboundFlightSegList != null && outboundFlightSegList.iterator().hasNext()) {
							FlightSegmentDTO outBoundFlightSegmentDTO = (outboundFlightSegList.iterator().next());
							criteriaForFilteringFares.setDepatureDateAndTime(outBoundFlightSegmentDTO.getDepartureDateTime());
							criteriaForFilteringFares
									.setDepartureDateTimeZulu(outBoundFlightSegmentDTO.getDepartureDateTimeZulu());
							criteriaForFilteringFares.setArrivalDateTime(outBoundFlightSegmentDTO.getArrivalDateTime());
							if (flightSegmentDTO.getDepartureDateTimeZulu() != null) {
								criteriaForFilteringFares.setStayOverTime(flightSegmentDTO.getDepartureDateTimeZulu().getTime()
										- outBoundFlightSegmentDTO.getDepartureDateTimeZulu().getTime());
							}
						}
					}

					List<FlightSegmentDTO> inFlightSegList = allFaresDTO.getAllFlightSegmentDTOs().get(openJawFirstInBoundSeq);
					if (inFlightSegList != null && inFlightSegList.size() > 0) {
						FlightSegmentDTO inBoundFlightSegmentDTO = inFlightSegList.get(0);
						criteriaForFilteringFares.setInboundDate(inBoundFlightSegmentDTO.getDepartureDateTime());
						criteriaForFilteringFares.setInboundDateZulu(inBoundFlightSegmentDTO.getDepartureDateTimeZulu());
						if (flightSegmentDTO.getDepartureDateTimeZulu() != null) {
							criteriaForFilteringFares.setStayOverTime(inBoundFlightSegmentDTO.getDepartureDateTimeZulu().getTime()
									- flightSegmentDTO.getDepartureDateTimeZulu().getTime());
						}
					}

					if (allFaresDTO.getAllFlightSegmentDTOs().size() >= 2) {

						List<FlightSegmentDTO> inboundFlightSegList = allFaresDTO.getAllFlightSegmentDTOs().get(ondSequence);
						boolean isReturnSegment = ondSequenceReturnMap.get(ondSequence).booleanValue();

						if (isReturnSegment) {
							ondCode = prepareOndCode(inboundFlightSegList);
							ondCode = Util.reverseOndCode(ondCode);
							criteriaForFilteringFares.setReturnOnd(true);
						} else {
							ondCode = prepareOndCode(inboundFlightSegList);
							criteriaForFilteringFares.setReturnOnd(false);
						}

					}
				}
				if (ondSequence == selectedOndSeq) {
					selectedFltOndCode = ondCode;
				}
				criteriaForFilteringFares.setOndCode(ondCode);
				// skip inventory check for flown segments
				if (criteriaForFilteringFares.isFlownOnd() && isSkipInvChkForFlown) {
					criteriaForFilteringFares.setCheckAvailability(false);
				}

				if (availableFlightSearchDTO.getModifiedOndFareType() != null
						&& availableFlightSearchDTO.getModifiedOndPaxFareAmount() != null
						&& availableFlightSearchDTO.getEnforceFareCheckForModOnd()) {
					if (availableFlightSearchDTO.getModifiedOndFareType() != 2) {
						criteriaForFilteringFares.setMinPaxFareAmount(AccelAeroCalculator
								.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(), new BigDecimal(2)));
					}

				}

				LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> returnBuckets = filterBuckets(
						fltSegList, criteriaForFilteringFares, true);
				if (returnBuckets != null) {
					allBucketReturnFaresCombinationsForSegments.putAll(returnBuckets);
				} else {
					for (FlightSegmentDTO fltSeg : fltSegList) {
						allBucketReturnFaresCombinationsForSegments.put(fltSeg.getSegmentId(),
								new LinkedHashMap<String, AllFaresBCInventorySummaryDTO>());
					}
				}
			}

			List<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketOndFaresCombinationForSegmentsList = new ArrayList<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>();

			allBucketOndFaresCombinationForSegmentsList
					.add(allBucketReturnFaresCombinationsForSegments.get(allFaresDTO.getSelectedSegmentId()));
			for (Integer segmentId : allBucketReturnFaresCombinationsForSegments.keySet()) {
				if (segmentId.intValue() != allFaresDTO.getSelectedSegmentId().intValue()) {
					allBucketOndFaresCombinationForSegmentsList.add(allBucketReturnFaresCombinationsForSegments.get(segmentId));
				}
			}

			int paxCount = criteria.getNumberOfAdults() + criteria.getNumberOfChilds();
			setCommonBookingCodeFareIdsCombinations(allBucketOndFaresCombinationForSegmentsList,
					bucketFilteringCriteria.isExcludeNonLowestPublicFare(), true, paxCount);

			// set return fares only for round trip
			if (avlFltSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP) {
				allFaresDTO.addSegmentFaresOfSpecifiedType(new Integer(FareTypes.RETURN_FARE),
						allBucketReturnFaresCombinationsForSegments);
				allFaresDTO.addOndCodeForFareType(new Integer(FareTypes.RETURN_FARE), selectedFltOndCode);
			}

			if (availableFlightSearchDTO.isHalfReturnFareQuote()) {
				LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketHalfReturnFaresCombinationsForSegments = getAllBucketFareCombinationsForHalfReturns(
						allBucketReturnFaresCombinationsForSegments, selectedOndSeq, allFaresDTO,
						bucketFilteringCriteria.isExcludeNonLowestPublicFare());
				if (allBucketHalfReturnFaresCombinationsForSegments != null) {
					allFaresDTO.addSegmentFaresOfSpecifiedType(new Integer(FareTypes.HALF_RETURN_FARE),
							allBucketHalfReturnFaresCombinationsForSegments);

					allFaresDTO.addOndCodeForFareType(new Integer(FareTypes.HALF_RETURN_FARE), selectedFltOndCode);
				}
			}

		}

		return allFaresDTO;
	}

	private LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>
			getAllBucketFareCombinationsForHalfReturns(
					LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketReturnFaresCombinations,
					int selectedOndSequence, AllFaresDTO allFaresDTO, boolean excludeNonLowestPublicFare) {
		List<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketOndFaresCombinationForHalfReturnList = new ArrayList<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>();
		LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketHalfReturnFaresCombinationsForSegments = null;

		if (allBucketReturnFaresCombinations != null) {
			allBucketHalfReturnFaresCombinationsForSegments = new LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>();
			for (Integer segmentId : allBucketReturnFaresCombinations.keySet()) {
				LinkedHashMap<String, AllFaresBCInventorySummaryDTO> lHashMap = allBucketReturnFaresCombinations.get(segmentId);
				LinkedHashMap<String, AllFaresBCInventorySummaryDTO> lHashMapHalfRet = new LinkedHashMap<String, AllFaresBCInventorySummaryDTO>();

				for (String bookingCode : lHashMap.keySet()) {
					AllFaresBCInventorySummaryDTO allFBISDTO = lHashMap.get(bookingCode);
					AllFaresBCInventorySummaryDTO allFaresBCInventorySummaryDTO = allFBISDTO.clone();
					Collection<FareSummaryDTO> collFares = allFaresBCInventorySummaryDTO.getFareSummaryDTOs().values();

					for (FareSummaryDTO halfReturnFareSummaryDTO : collFares) {
						double journeyFareAmount = new Double(OndFareUtil.getTruncatedDecimalStr(
								halfReturnFareSummaryDTO.getFareAmount(PaxTypeTO.ADULT).doubleValue() / 2)).doubleValue();
						halfReturnFareSummaryDTO.setAdultFare(journeyFareAmount);
						allFaresBCInventorySummaryDTO.addFareSummaryDTO(halfReturnFareSummaryDTO);
					}
					lHashMapHalfRet.put(bookingCode, allFaresBCInventorySummaryDTO);
				}
				allBucketHalfReturnFaresCombinationsForSegments.put(segmentId, lHashMapHalfRet);
			}

			Collection<Integer> colSegmentsForCommonBCAlloc = allFaresDTO.getOndFlightSegIds(selectedOndSequence);
			for (Integer segmentId : allBucketHalfReturnFaresCombinationsForSegments.keySet()) {
				if (colSegmentsForCommonBCAlloc.contains(segmentId)) {
					allBucketOndFaresCombinationForHalfReturnList
							.add(allBucketHalfReturnFaresCombinationsForSegments.get(segmentId));
				}
			}

			setBookingCodeFareIdsCombinationsForHalfReturns(allBucketOndFaresCombinationForHalfReturnList,
					excludeNonLowestPublicFare);
		}
		return allBucketHalfReturnFaresCombinationsForSegments;
	}

	private boolean isFareTypeExists(Collection<Integer> fareTypes, int fareType) {
		for (Integer ondFare : fareTypes) {
			if (ondFare == fareType) {
				return true;
			}
		}
		return false;
	}

	private boolean isFareInAllOnds(Collection<Integer> fareTypes, int fareType) {
		for (Integer ondFare : fareTypes) {
			if (ondFare != fareType) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Recalculate charges for each OND after fare change
	 * 
	 * @param changeFaresDTO
	 * @return
	 * @throws ModuleException
	 */
	public ChangeFaresDTO recalculateChargesForFareChange(ChangeFaresDTO changeFaresDTO,
			AvailableFlightSearchDTO availableFlightSearchCriteria) throws ModuleException {

		ReservationApiUtils.splitPossibleOnDs(availableFlightSearchCriteria);

		// merge possible ONDs for possible connection FareQuote
		ReservationApiUtils.mergePossibleOnD(availableFlightSearchCriteria);

		int ondSequenceTemp = 0;
		// boolean isBusSegmentExists = false;
		Map<Integer, List<Integer>> fltSegByOndSequence = new HashMap<Integer, List<Integer>>();

		Map<Integer, Integer> newOldSequenceMap = new HashMap<>();

		// Map<Integer, Integer> sameTypeSegs = new HashMap<Integer, Integer>();
		for (OriginDestinationInfoDTO ondInfo : availableFlightSearchCriteria.getOrderedOriginDestinations()) {
			fltSegByOndSequence.put(ondSequenceTemp, ondInfo.getFlightSegmentIds());

			int ondSeq = getExistingOndSequence(changeFaresDTO.getOndSegmentIdMap(), ondInfo.getFlightSegmentIds(), -1);
			if (ondSeq != -1) {
				newOldSequenceMap.put(ondSequenceTemp, ondSeq);
			}
			// if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) {
			// sameTypeSegs.put(new Integer(ondSequenceTemp), ondInfo.getRequestedOndSequence());
			// if (sameTypeSegs.get(ondSequenceTemp) == AirScheduleCustomConstants.SegmentTypes.BUS_SEGMENT) {
			// isBusSegmentExists = true;
			// }
			// }
			ondSequenceTemp++;
		}

		List<AvailableFlightSearchDTO> availableFlightSearchDTOList = null;
		if (AppSysParamsUtil.isSubJourneyDetectionEnabled()
				&& availableFlightSearchCriteria.getOriginDestinationInfoDTOs().size() > 1
				&& !availableFlightSearchCriteria.isOpenReturnSearch()) {
			// all available sub-journey list{RETURN, CIRCULAR, OPEN-JAW}
			availableFlightSearchDTOList = ReservationApiUtils.getAvailableSubJourneyCollection(availableFlightSearchCriteria);
		} else {
			availableFlightSearchDTOList = new ArrayList<AvailableFlightSearchDTO>();
			availableFlightSearchDTOList.add(availableFlightSearchCriteria);
		}
		if (!changeFaresDTO.hasExistingFareOnds()) {
			populateRequiredChgFareData(changeFaresDTO);
		}

		validateRecalculateChargesForFareChange(changeFaresDTO);
		String outboundCabinClassCode = null;
		populateFareInfo(changeFaresDTO.getChangedSegmentFaresMap());
		Iterator<AvailableFlightSearchDTO> avifltSearchDTOListItr = availableFlightSearchDTOList.iterator();
		Map<Integer, Boolean> ondSequenceReturnFlag = new HashMap<Integer, Boolean>();
		int subJourneyGroup = 1;
		Map<Integer, BundledFareDTO> ondSelectedBundledFareDTOs = getBundledFareBL()
				.getOndWiseSelectedBundledFareDTOs(availableFlightSearchCriteria);
		Map<Integer, List<OndFareDTO>> subJourneyWiseOndFareDTOs = new HashMap<Integer, List<OndFareDTO>>();
		Map<Integer, Map<OndFareDTO, Collection<QuotedChargeDTO>>> subJourneyWiseOndWiseChargeTOs = new HashMap<Integer, Map<OndFareDTO, Collection<QuotedChargeDTO>>>();

		Map<String, Double> totalSurcharges = new HashMap<String, Double>();
		totalSurcharges.put(PaxTypeTO.ADULT, new Double(0));
		totalSurcharges.put(PaxTypeTO.CHILD, new Double(0));
		totalSurcharges.put(PaxTypeTO.INFANT, new Double(0));

		Map<String, Double> totalJourneyFareMap = new HashMap<String, Double>();
		totalJourneyFareMap.put(PaxTypeTO.ADULT, new Double(0));
		totalJourneyFareMap.put(PaxTypeTO.CHILD, new Double(0));
		totalJourneyFareMap.put(PaxTypeTO.INFANT, new Double(0));
		while (avifltSearchDTOListItr.hasNext()) {

			AvailableFlightSearchDTO avifltSearchDTODum = avifltSearchDTOListItr.next();
			Map<Integer, Boolean> returnMapTemp = avifltSearchDTODum.getOndSequenceReturnMap();

			int ondSequence = 0;
			List<OndFareDTO> allOndchangedFaresSeatsSegmentsDTOs = new ArrayList<OndFareDTO>();
			List<Integer> fareTypeList = new ArrayList<Integer>();
			for (OriginDestinationInfoDTO ondInfoTo : avifltSearchDTODum.getOrderedOriginDestinations()) {
				int existOndSequence = ondSequence;

				if (!avifltSearchDTODum.isOpenReturnSearch()) {// AEROMART-3861
					existOndSequence = getExistingOndSequence(changeFaresDTO.getOndSegmentIdMap(),
							ondInfoTo.getFlightSegmentIds(), ondSequence);
				}

				List<OndFareDTO> changedFaresSeatsSegmentsDTOs = prepareFaresSeatsSegmentsDTOs(changeFaresDTO, existOndSequence,
						avifltSearchDTODum.isOpenReturnSearch(), subJourneyGroup, ondInfoTo.getFlightSegmentIds());

				if (changedFaresSeatsSegmentsDTOs != null && !changedFaresSeatsSegmentsDTOs.isEmpty()) {
					fareTypeList.add(changedFaresSeatsSegmentsDTOs.get(0).getFareType());
					allOndchangedFaresSeatsSegmentsDTOs.addAll(changedFaresSeatsSegmentsDTOs);
				}

				boolean isIBFlight = false;
				if ((availableFlightSearchCriteria.isMultiCitySearch() || availableFlightSearchCriteria.isRequoteFlightSearch())
						&& returnMapTemp != null && returnMapTemp.size() > 1 && returnMapTemp.get(ondSequence) != null) {
					isIBFlight = returnMapTemp.get(ondSequence);
				}

				ondSequenceReturnFlag.put(existOndSequence, isIBFlight);
				ondSequence++;
			}

			if (changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs() != null
					&& changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs().size() > 0) {
				changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs().addAll(allOndchangedFaresSeatsSegmentsDTOs);
			} else {
				changeFaresDTO.setNewFareChargesSeatsSegmentsDTOs(allOndchangedFaresSeatsSegmentsDTOs);
			}

			if (isFareInAllOnds(fareTypeList, FareTypes.NO_FARE)) {
				changeFaresDTO.setChangedFareType(FareTypes.NO_FARE);
			} else {
				if (isFareTypeExists(fareTypeList, FareTypes.HALF_RETURN_FARE)) {
					if (isFareTypeExists(fareTypeList, FareTypes.PER_FLIGHT_FARE)) {
						throw new ModuleException("airinventory.arg.changefare.invalid",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					} else if (isFareTypeExists(fareTypeList, FareTypes.OND_FARE)) {
						throw new ModuleException("airinventory.arg.changefare.invalid",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
				}

				if (isFareInAllOnds(fareTypeList, FareTypes.HALF_RETURN_FARE)) {
					changeFaresDTO.setChangedFareType(FareTypes.HALF_RETURN_FARE);
				} else if (isFareInAllOnds(fareTypeList, FareTypes.PER_FLIGHT_FARE)) {
					changeFaresDTO.setChangedFareType(FareTypes.PER_FLIGHT_FARE);
				} else if (isFareInAllOnds(fareTypeList, FareTypes.OND_FARE)) {
					changeFaresDTO.setChangedFareType(FareTypes.OND_FARE);
				} else if (isFareInAllOnds(fareTypeList, FareTypes.RETURN_FARE)) {
					changeFaresDTO.setChangedFareType(FareTypes.RETURN_FARE);
				} else {
					changeFaresDTO.setChangedFareType(FareTypes.OTHER_FARE);
				}
			}

			// CHARGE QUOTING
			// Identify total fare for the journey
			Double adultTotJnyFare = 0d;
			Double childTotJnyFare = 0d;
			Double infantTotJnyFare = 0d;

			Map<Integer, FlightSegmentDTO> allFlightSegMap = new HashMap<Integer, FlightSegmentDTO>();
			Map<Integer, Boolean> ondwiseReturnFlag = new HashMap<Integer, Boolean>();

			for (OndFareDTO ondFareDTO : allOndchangedFaresSeatsSegmentsDTOs) {
				ondFareDTO.injectFarePercentage(changeFaresDTO.getChangedFareType());
				adultTotJnyFare += ondFareDTO.getFareSummaryDTO().getFareAmount(PaxTypeTO.ADULT);
				childTotJnyFare += ondFareDTO.getFareSummaryDTO().getFareAmount(PaxTypeTO.CHILD);
				infantTotJnyFare += ondFareDTO.getFareSummaryDTO().getFareAmount(PaxTypeTO.INFANT);

				ondwiseReturnFlag.put(ondFareDTO.getOndSequence(), ondFareDTO.isInBoundOnd());

				if (ondFareDTO.getSegmentsMap() != null && !ondFareDTO.getSegmentsMap().isEmpty()) {
					allFlightSegMap.putAll(ondFareDTO.getSegmentsMap());
				}

				String ondCabinClass = ondFareDTO.getSegmentSeatDistsDTO().iterator().next().getCabinClassCode();
				if (outboundCabinClassCode == null) {
					outboundCabinClassCode = ondCabinClass;
				} else if (!outboundCabinClassCode.equals(ondCabinClass) && avifltSearchDTODum.isOpenReturnSearch()) {
					throw new ModuleException("airinventory.arg.changefare.invalid", AirinventoryCustomConstants.INV_MODULE_CODE);
				}
			}

			Map<Integer, List<FlightSegmentDTO>> ondWiseFlightSegList = new HashMap<Integer, List<FlightSegmentDTO>>();
			ondSequence = 0;
			for (OriginDestinationInfoDTO ondInfoTo : avifltSearchDTODum.getOrderedOriginDestinations()) {
				if (ondInfoTo.getFlightSegmentIds() != null && !ondInfoTo.getFlightSegmentIds().isEmpty()) {
					int sequence = 0;
					if (!avifltSearchDTODum.isOpenReturnSearch()) {
						// ondSequence = getExistingOndSequence(fltSegByOndSequence, ondInfoTo.getFlightSegmentIds(),
						// ondSequence); //AEROMART-3861
						ondSequence = getExistingOndSequence(changeFaresDTO.getOndSegmentIdMap(), ondInfoTo.getFlightSegmentIds(),
								ondSequence);
					}
					sequence = ondSequence;
					for (Integer flightSegmentId : ondInfoTo.getFlightSegmentIds()) {
						if (allFlightSegMap.containsKey(flightSegmentId)) {
							// if (sameTypeSegs.get(ondSequence) != null && isBusSegmentExists) {
							// sequence = sameTypeSegs.get(ondSequence);
							// }
							if (!ondWiseFlightSegList.containsKey(sequence)) {
								ondWiseFlightSegList.put(sequence, new ArrayList<FlightSegmentDTO>());
							}

							ondWiseFlightSegList.get(sequence).add(allFlightSegMap.get(flightSegmentId));
						}
					}
				}
				ondSequence++;
			}

			totalJourneyFareMap.put(PaxTypeTO.ADULT, totalJourneyFareMap.get(PaxTypeTO.ADULT) + adultTotJnyFare);
			totalJourneyFareMap.put(PaxTypeTO.CHILD, totalJourneyFareMap.get(PaxTypeTO.CHILD) + childTotJnyFare);
			totalJourneyFareMap.put(PaxTypeTO.INFANT, totalJourneyFareMap.get(PaxTypeTO.INFANT) + infantTotJnyFare);

			// Build charge quote criteria
			Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();
			HashMap<String, HashMap<String, QuotedChargeDTO>> quotedChargesMap = new HashMap<String, HashMap<String, QuotedChargeDTO>>();
			FlightSegmentDTO previousFlightSegment = null;
			int count = 1;

			// this flag is required to quote the charges separately when same ond is duplicated, if we didn't handle it
			// separate, charges map get overwritten by the last OND charges map
			boolean isSameOnDExist = isOnDDuplicated(allOndchangedFaresSeatsSegmentsDTOs);
			ondSequence = 0;
			for (OndFareDTO ondFareDTO : allOndchangedFaresSeatsSegmentsDTOs) {
				QuoteChargesCriteria quoteChargesCriteria = ondFareDTO
						.createCriteriaForChargeQuoting(changeFaresDTO.getPosAirport(), false);

				String localOndPosition = null;
				String ondPosition = null;
				List<FlightSegmentDTO> ondFlightList = new ArrayList<FlightSegmentDTO>();
				if (ondFareDTO.getFareType() == FareTypes.PER_FLIGHT_FARE) {
					ondFlightList.addAll(ondFareDTO.getSegmentsMap().values());
				} else {
					ondFlightList = ondWiseFlightSegList.get(ondFareDTO.getOndSequence());
				}

				String totalJourneyOndPosition = getTotalJourneyONDPosition(avifltSearchDTODum, ondFlightList);
				if (avifltSearchDTODum != null && avifltSearchDTODum.isSubJourney() && totalJourneyOndPosition != null) {
					ondPosition = totalJourneyOndPosition;
				} else {
					if (count == 1) {
						if (allOndchangedFaresSeatsSegmentsDTOs.size() == 1) {
							localOndPosition = QuoteChargesCriteria.FIRST_AND_LAST_OND;
						} else {
							localOndPosition = QuoteChargesCriteria.FIRST_OND;
						}
					} else if (count == allOndchangedFaresSeatsSegmentsDTOs.size()) {
						localOndPosition = QuoteChargesCriteria.LAST_OND;
					} else {
						localOndPosition = QuoteChargesCriteria.INBETWEEN_OND;
					}
					ondPosition = localOndPosition;
					if (avifltSearchDTODum != null) {
						// ondPosition = getOndPosForModAddSeg(ondFareDTO.getOndDepartureDate(),
						// availableFlightSearchCriteria.getFirstDepartureDateTimeZulu(),
						// availableFlightSearchCriteria.getLastArrivalDateTimeZulu(), localOndPosition, null);
						ondPosition = getONDPosition(ondSequence == 0, ondSequence == ondWiseFlightSegList.keySet().size() - 1,
								localOndPosition);
					}
				}

				quoteChargesCriteria.setOndPosition(ondPosition);

				FlightSegmentDTO connectionFltSeg = null;
				if (ondSequence > 0 && ondSequence < ondWiseFlightSegList.size()) {
					boolean isCurrentOndInbound = ondFareDTO.isInBoundOnd();
					boolean isPreviousOndInbound = ondwiseReturnFlag.get(ondFareDTO.getOndSequence());
					if (isCurrentOndInbound && !isPreviousOndInbound) {
						connectionFltSeg = null;
					} else {
						List<FlightSegmentDTO> connFlts = ondWiseFlightSegList.get(ondFareDTO.getOndSequence());
						connectionFltSeg = connFlts.get(connFlts.size() - 1);
					}
				}
				TransitInfoDTO transitInfoDTO = getTransitInfoDTO(ondFlightList, connectionFltSeg);

				Collection<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();

				flightSegmentDTOs.addAll(ondFareDTO.getSegmentsMap().values());
				Set<String> transitAPList = new HashSet<String>();
				Set<String> excludeAPlist = new HashSet<String>();
				List<Long> transitDurList = new ArrayList<Long>();

				for (FlightSegmentDTO fltSeg : flightSegmentDTOs) {

					if (transitInfoDTO.getTransitAirports(fltSeg.getDepartureDateTimeZulu()) != null) {
						transitAPList.addAll(transitInfoDTO.getTransitAirports(fltSeg.getDepartureDateTimeZulu()));
					}
					if (transitInfoDTO.getDepArrExcludeAirports(fltSeg.getDepartureAirportCode(),
							fltSeg.getArrivalAirportCode()) != null) {
						excludeAPlist.addAll(transitInfoDTO.getDepArrExcludeAirports(fltSeg.getDepartureAirportCode(),
								fltSeg.getArrivalAirportCode()));
					}
					if (transitInfoDTO.getTransitDurationList(fltSeg.getDepartureDateTimeZulu()) != null) {
						transitDurList.addAll(transitInfoDTO.getTransitDurationList(fltSeg.getDepartureDateTimeZulu()));
					}
				}

				quoteChargesCriteria.setQualifyForShortTransit(transitInfoDTO.isQualifyForShortTransit());
				quoteChargesCriteria.setAgentCode(avifltSearchDTODum.getAgentCode());
				quoteChargesCriteria.setChannelId(avifltSearchDTODum.getChannelCode());

				// Adding excluded charge list
				if (getChargeBD().isValidChargeExclusion(avifltSearchDTODum.getExcludedCharges())) {
					quoteChargesCriteria.setExcludedCharges(avifltSearchDTODum.getExcludedCharges());
				}

				// Since connection fares cannot be created from multiple cabin classes, retrieve 1st segment's cabin
				// class
				FlightSegmentDTO flightSegmentDTO = BeanUtils.getFirstElement(ondFlightList);
				String cabinClassCode = null;
				if (ondFareDTO.getSegmentSeatDistsDTO() != null) {
					for (SegmentSeatDistsDTO seatDistsDTO : ondFareDTO.getSegmentSeatDistsDTO()) {
						if (seatDistsDTO.getFlightSegId() == flightSegmentDTO.getSegmentId()) {
							cabinClassCode = seatDistsDTO.getCabinClassCode();
						}
					}
				}

				quoteChargesCriteria.setCabinClassCode(cabinClassCode);
				quoteChargesCriteria.setOndTransitDurList(transitDurList);
				quoteChargesCriteria.setTransitAiports(transitAPList, excludeAPlist);
				quoteChargesCriteria
						.setChargeQuoteDate(avifltSearchDTODum.getEffectiveLastFQDate(flightSegmentDTO.getSegmentId()));

				/*
				 * Temporary hotfix. Root problem is AvailableFlightSearchDTO.originDestinationInfoList not getting the
				 * proper flightSegId's set. Same one is there in both. Must search and fix the problem as this may be
				 * having subtle sideeffects.
				 */
				quoteChargesCriteria.setFareSummaryDTO(ondFareDTO.getFareSummaryDTO());
				BundledFareDTO selectedBundledFare = ondSelectedBundledFareDTOs.get(ondFareDTO.getOndSequence());
				if (selectedBundledFare != null) {
					quoteChargesCriteria.getBundledFareChargeCodes().add(selectedBundledFare.getChargeCode());
				}

				if (isSameOnDExist) {
					criteriaCollection = new ArrayList<QuoteChargesCriteria>();
				}

				criteriaCollection.add(quoteChargesCriteria);
				if (isSameOnDExist) {
					HashMap<String, HashMap<String, QuotedChargeDTO>> quotedChargesMapTemp = getChargeBD()
							.quoteCharges(criteriaCollection);
					for (String chargeMapKey : quotedChargesMapTemp.keySet()) {
						String newKey = chargeMapKey + "_" + ondFareDTO.getOndSequence();
						quotedChargesMap.put(newKey, quotedChargesMapTemp.get(chargeMapKey));
					}
				}
				count++;
				ondSequence++;
			}

			if (!isSameOnDExist) {
				quotedChargesMap = getChargeBD().quoteCharges(criteriaCollection);
			}

			// set charges
			Map<OndFareDTO, Collection<QuotedChargeDTO>> ondFareWiseCharges = new HashMap<OndFareDTO, Collection<QuotedChargeDTO>>();

			ondSequence = 0;
			for (OndFareDTO ondFareDTO : allOndchangedFaresSeatsSegmentsDTOs) {
				String chargeMapKey = ondFareDTO.getOndCode();
				if (isSameOnDExist) {
					chargeMapKey = ondFareDTO.getOndCode() + "_" + ondFareDTO.getOndSequence();
				}

				HashMap<String, QuotedChargeDTO> chargesMap = quotedChargesMap.get(chargeMapKey);
				if (chargesMap != null && chargesMap.size() > 0) {
					boolean isBusSegment = AirInventoryModuleUtils.getAirportBD().isBusSegment(ondFareDTO.getOndCode());
					Collection<QuotedChargeDTO> charges = filterChargesForChargeGroups(
							getApplicableChargeGroupsForBookingClass(ondFareDTO.getBCForChargeQuote(), isBusSegment),
							chargesMap.values());

					Collection<QuotedChargeDTO> chargesCollection = ChargeQuoteUtils.getChargesWithEffectiveChargeValues(charges,
							ondFareDTO.getFareSummaryDTO(), avifltSearchDTODum.getAdultCount(),
							avifltSearchDTODum.getChildCount(), totalJourneyFareMap, null);
					ondFareWiseCharges.put(ondFareDTO, chargesCollection);
					Map<String, Double> currSurcharges = ChargeQuoteUtils.getTotalSurcharges(chargesCollection);
					totalSurcharges.put(PaxTypeTO.ADULT,
							totalSurcharges.get(PaxTypeTO.ADULT) + currSurcharges.get(PaxTypeTO.ADULT));
					totalSurcharges.put(PaxTypeTO.CHILD,
							totalSurcharges.get(PaxTypeTO.CHILD) + currSurcharges.get(PaxTypeTO.CHILD));
					totalSurcharges.put(PaxTypeTO.INFANT,
							totalSurcharges.get(PaxTypeTO.INFANT) + currSurcharges.get(PaxTypeTO.INFANT));
				}
				ondSequence++;
			}
			subJourneyWiseOndWiseChargeTOs.put(subJourneyGroup, ondFareWiseCharges);
			subJourneyWiseOndFareDTOs.put(subJourneyGroup, allOndchangedFaresSeatsSegmentsDTOs);

			if (avifltSearchDTODum.isOpenReturnSearch()) {
				if (outboundCabinClassCode == null) {
					// TODO fix with open return
					// outboundCabinClassCode = availableFlightSearchCriteria.getCabinClassCode();
				}
				injectOpenReturnBookingClass(allOndchangedFaresSeatsSegmentsDTOs);
			}
			subJourneyGroup++;
		}

		avifltSearchDTOListItr = availableFlightSearchDTOList.iterator();
		subJourneyGroup = 1;
		while (avifltSearchDTOListItr.hasNext()) {
			AvailableFlightSearchDTO avifltSearchDTODum = avifltSearchDTOListItr.next();
			List<OndFareDTO> allOndchangedFaresSeatsSegmentsDTOs = subJourneyWiseOndFareDTOs.get(subJourneyGroup);
			Map<OndFareDTO, Collection<QuotedChargeDTO>> ondFareWiseCharges = subJourneyWiseOndWiseChargeTOs.get(subJourneyGroup);

			for (OndFareDTO ondFareDTO : ondFareWiseCharges.keySet()) {
				if (ondFareWiseCharges.containsKey(ondFareDTO)) {
					Collection<QuotedChargeDTO> charges = ondFareWiseCharges.get(ondFareDTO);
					ondFareDTO.setAllCharges(ChargeQuoteUtils.getChargesWithEffectiveChargeValues(charges,
							ondFareDTO.getFareSummaryDTO(), avifltSearchDTODum.getAdultCount(),
							avifltSearchDTODum.getChildCount(), totalJourneyFareMap, totalSurcharges));
				}
			}

			if (avifltSearchDTODum.isFlexiQuote()) {
				// Quote Flexi
				Collection<FlexiQuoteCriteria> flexiCriteriaCollection = new ArrayList<FlexiQuoteCriteria>();
				for (OndFareDTO ondFareDTO : allOndchangedFaresSeatsSegmentsDTOs) {
					FlexiQuoteCriteria quoteFlexiCriteria = ondFareDTO.createCriteriaForFlexiQuoting();
					BundledFareDTO bundledFareDTO = ondSelectedBundledFareDTOs.get(ondFareDTO.getOndSequence());
					if (bundledFareDTO != null) {
						quoteFlexiCriteria.setOfferFlexiFreeOfCharge(
								bundledFareDTO.isServiceIncluded(EXTERNAL_CHARGES.FLEXI_CHARGES.toString()));
					}
					flexiCriteriaCollection.add(quoteFlexiCriteria);
				}

				HashMap<String, HashMap<String, FlexiRuleDTO>> quotedFlexiMap = getAirPricingBD()
						.quoteFlexiCharges(flexiCriteriaCollection);
				// set flexi charges
				for (OndFareDTO ondFareDTO : allOndchangedFaresSeatsSegmentsDTOs) {
					HashMap<String, FlexiRuleDTO> flexiMap = quotedFlexiMap.get(ondFareDTO.getOndCode());
					if (flexiMap != null && flexiMap.size() > 0) {
						Collection<FlexiRuleDTO> flexiCharges = flexiMap.values();
						ondFareDTO.setAllFlexiCharges(ChargeQuoteUtils.getFlexiChargesWithEffectiveChargeValues(flexiCharges,
								ondFareDTO.getFareSummaryDTO(), ondFareDTO.getAllCharges()));

						if (ondFareDTO.getFareType() == FareTypes.RETURN_FARE
								|| ondFareDTO.getFareType() == FareTypes.HALF_RETURN_FARE) {
							Collection<FlexiRuleDTO> effectiveFlexiCharges = ondFareDTO.getAllFlexiCharges();
							if (effectiveFlexiCharges != null) {
								for (FlexiRuleDTO flexiChg : effectiveFlexiCharges) {
									flexiChg.setEffectiveChargeAmount(PaxTypeTO.ADULT,
											(flexiChg.getEffectiveChargeAmount(PaxTypeTO.ADULT) / 2));
									flexiChg.setEffectiveChargeAmount(PaxTypeTO.CHILD,
											(flexiChg.getEffectiveChargeAmount(PaxTypeTO.CHILD) / 2));
									flexiChg.setEffectiveChargeAmount(PaxTypeTO.INFANT,
											(flexiChg.getEffectiveChargeAmount(PaxTypeTO.INFANT) / 2));
								}
							}
						}
					}
				}
			}
			subJourneyGroup++;
		}

		changeFaresDTO.setOndSequenceReturnMap(ondSequenceReturnFlag);
		changeFaresDTO.setOndSelectedBundledFares(ondSelectedBundledFareDTOs);

		if (changeFaresDTO != null) {
			if (availableFlightSearchCriteria.isFQOnLastFQDate()) {
				changeFaresDTO.setLastFQDate(availableFlightSearchCriteria.getLastFareQuotedDate());
				changeFaresDTO.setFQWithinValidity(availableFlightSearchCriteria.isFQWithinValidity());
			}

			getBundledFareBL().getApplicableBundledFares(changeFaresDTO, availableFlightSearchCriteria);

			ApplicablePromotionDetailsTO applicablePromotionDetails = getApplicablePromotion(
					changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs(), availableFlightSearchCriteria);
			changeFaresDTO.setApplicablePromotionDetails(applicablePromotionDetails);
			getBundledFareBL().updateBundledFareFee(changeFaresDTO);
		}

		return changeFaresDTO;
	}

	private ApplicablePromotionDetailsTO getApplicablePromotion(Collection<OndFareDTO> newOndFareDtos,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		if (availableFlightSearchDTO.isPromoApplicable() && !availableFlightSearchDTO.isModifyBooking()
				&& AppSysParamsUtil.isPromoCodeEnabled() && newOndFareDtos != null && !newOndFareDtos.isEmpty()
				&& !availableFlightSearchDTO.isRequoteFlightSearch()) {
			ServiceResponce promoResponce = AirInventoryModuleUtils.getPromotionManagementBD().pickApplicablePromotion(
					PromotionsUtils.buildPromoSelectionCriteria(newOndFareDtos, availableFlightSearchDTO));
			if (promoResponce.isSuccess()) {
				return (ApplicablePromotionDetailsTO) promoResponce
						.getResponseParam(CommandParamNames.APPLICABLE_PROMOTION_DETAILS);

			}
		}
		return null;
	}

	private void populateRequiredChgFareData(ChangeFaresDTO changeFaresDTO) throws ModuleException {
		Collection<Integer> flightSegmentIds = new HashSet<Integer>();
		for (SegmentsFareDTO fareSegDTO : changeFaresDTO.getChangedFaresSeatsSegmentsDTOs()) {
			flightSegmentIds.addAll(fareSegDTO.getSegmentRPHMap().keySet());
		}
		Collection<FlightSegmentDTO> fltSegs = getFlightBD().getFlightSegments(flightSegmentIds);
		changeFaresDTO.addFlightSegments(fltSegs);
	}

	/**
	 * Inject Open Return booking for the in bound segments
	 * 
	 * @param changedFaresSeatsSegmentsDTOs
	 * @param availableFlightSearchCriteria
	 */
	private void injectOpenReturnBookingClass(List<OndFareDTO> changedFaresSeatsSegmentsDTOs) {

		if (changedFaresSeatsSegmentsDTOs != null && changedFaresSeatsSegmentsDTOs.size() > 0) {
			Collection<SeatDistribution> colSeatDistribution;
			Collection<SeatDistribution> colClonedSeatDistribution;

			for (OndFareDTO ondFareDTO : changedFaresSeatsSegmentsDTOs) {

				if (ondFareDTO.isInBoundOnd()) {
					Collection<SegmentSeatDistsDTO> colSegmentSeatDistsDTO = ondFareDTO.getSegmentSeatDistsDTO();

					for (SegmentSeatDistsDTO segmentSeatDistsDTO : colSegmentSeatDistsDTO) {

						if (segmentSeatDistsDTO != null) {

							BookingClass openReturnBC = getOpenReturnBookingClass(segmentSeatDistsDTO.getLogicalCabinClass());
							String openReturnCabinClassWiseBCCode = openReturnBC.getBookingCode();
							String openReturnCabinClassWiseBCType = openReturnBC.getBcType();

							colSeatDistribution = segmentSeatDistsDTO.getSeatDistribution();
							colClonedSeatDistribution = new ArrayList<SeatDistribution>();
							for (SeatDistribution seatDistribution : colSeatDistribution) {
								seatDistribution = seatDistribution.clone();

								seatDistribution.setBookingClassCode(openReturnCabinClassWiseBCCode);
								seatDistribution.setBookingClassType(openReturnCabinClassWiseBCType);
								colClonedSeatDistribution.add(seatDistribution);
							}

							segmentSeatDistsDTO.setSeatDistributions(colClonedSeatDistribution);
						}
					}
				}
			}
		}
	}

	/**
	 * Validates recalculate charges for change fare request parameters
	 * 
	 * @param changeFaresDTO
	 * @throws ModuleException
	 */
	private void validateRecalculateChargesForFareChange(ChangeFaresDTO changeFaresDTO) throws ModuleException {
		Map<Integer, List<Integer>> ondWiseExistingFareType = new HashMap<Integer, List<Integer>>();
		Map<Integer, List<Integer>> ondGroupJourneyWise = new HashMap<Integer, List<Integer>>();
		Map<Integer, Integer> totalSegmentSizeJourneyWise = new HashMap<Integer, Integer>();
		Map<Integer, Integer> fareChangedSegCountJourneyWise = new HashMap<Integer, Integer>();
		int totalExistingSegments = 0; // Existing Total Flight Segments

		Map<Integer, SegmentFareDTO> fareChangedSegmentsMap = changeFaresDTO.getChangedSegmentFaresMap();

		if ((fareChangedSegmentsMap == null) || (fareChangedSegmentsMap.size() == 0)) {
			// When no changed fare information available
			throw new ModuleException("airinventory.arg.changefare.invalid", "Inconsistent data for change fare operation");
		}

		Map<Integer, Integer> ondWiseChangedFareType = new HashMap<Integer, Integer>();

		if (changeFaresDTO.hasExistingFareOnds()) {
			for (OndFareDTO ondFareDTO : changeFaresDTO.getExistingFareChargesSeatsSegmentsDTOs()) {
				int subJourneyGorupId = ondFareDTO.getSubJourneyGroup();
				if (ondGroupJourneyWise.get(subJourneyGorupId) == null) {
					ondGroupJourneyWise.put(subJourneyGorupId, new ArrayList<Integer>());
				}

				ondGroupJourneyWise.get(subJourneyGorupId).add(ondFareDTO.getOndSequence());
				int totalSegSize = 0;
				if (totalSegmentSizeJourneyWise.get(subJourneyGorupId) == null) {
					totalSegmentSizeJourneyWise.put(subJourneyGorupId, new Integer(0));
				}

				totalSegSize = totalSegmentSizeJourneyWise.get(subJourneyGorupId) + ondFareDTO.getSegmentsMap().size();
				totalSegmentSizeJourneyWise.put(subJourneyGorupId, totalSegSize);
				if (!AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) {
					if (ondWiseExistingFareType.get(ondFareDTO.getOndSequence()) == null) {
						ondWiseExistingFareType.put(ondFareDTO.getOndSequence(), new ArrayList<Integer>());
						ondWiseExistingFareType.get(ondFareDTO.getOndSequence()).add(ondFareDTO.getFareType());
					} else {
						if (isMixFareTypesDetectedInConnection(ondFareDTO, ondWiseExistingFareType)) {
							throw new ModuleException("airinventory.arg.changefare.invalid",
									"Inconsistent outbound OND data received");
						} else {
							ondWiseExistingFareType.get(ondFareDTO.getOndSequence()).add(ondFareDTO.getFareType());
						}
					}
				} else {
					if (ondWiseExistingFareType.get(ondFareDTO.getOndSequence()) == null) {
						ondWiseExistingFareType.put(ondFareDTO.getOndSequence(), new ArrayList<Integer>());
						ondWiseExistingFareType.get(ondFareDTO.getOndSequence()).add(ondFareDTO.getFareType());
					} else {
						ondWiseExistingFareType.get(ondFareDTO.getOndSequence()).add(ondFareDTO.getFareType());
					}
				}

				totalExistingSegments += ondFareDTO.getSegmentsMap().size();
				int fareChangedSegmentCount = 0;

				for (FlightSegmentDTO flightSegmentDTO : ondFareDTO.getSegmentsMap().values()) {
					if (fareChangedSegmentsMap.containsKey(flightSegmentDTO.getSegmentId())) {
						++fareChangedSegmentCount;

						// Validate changed fare type
						int currentSegmentFareType = fareChangedSegmentsMap.get(flightSegmentDTO.getSegmentId()).getFareType();

						if (!ondWiseChangedFareType.containsKey(ondFareDTO.getOndSequence())) {
							ondWiseChangedFareType.put(ondFareDTO.getOndSequence(), currentSegmentFareType);
						} else if (ondWiseChangedFareType.get(ondFareDTO.getOndSequence()) != currentSegmentFareType) {
							// A given OnD can have single fare type only
							throw new ModuleException("airinventory.arg.changefare.invalid",
									"Inconsistent outbound OND changed fare data receeived");
						}
					}
				}
				if (fareChangedSegCountJourneyWise.get(subJourneyGorupId) == null) {
					fareChangedSegCountJourneyWise.put(subJourneyGorupId, new Integer(0));
				}

				fareChangedSegCountJourneyWise.put(subJourneyGorupId,
						(fareChangedSegCountJourneyWise.get(subJourneyGorupId) + fareChangedSegmentCount));

				if ((fareChangedSegmentCount != 0) && (fareChangedSegmentCount != ondFareDTO.getSegmentsMap().size())) {
					// When changing fare for an OnD, fare for all the segments in an OnD should be changed
					throw new ModuleException("airinventory.arg.changefare.invalid", "Invalid change fare operation");
				}
			}
		} else {
			for (SegmentFareDTO segFare : fareChangedSegmentsMap.values()) {
				int currentSegmentFareType = segFare.getFareType();

				if (!ondWiseChangedFareType.containsKey(segFare.getOndSequence())) {
					ondWiseChangedFareType.put(segFare.getOndSequence(), currentSegmentFareType);
				} else if (ondWiseChangedFareType.get(segFare.getOndSequence()) != currentSegmentFareType) {
					// A given OnD can have single fare type only
					throw new ModuleException("airinventory.arg.changefare.invalid",
							"Inconsistent outbound OND changed fare data receeived");
				}
			}
		}

		for (Integer journeyId : ondGroupJourneyWise.keySet()) {
			Collection<Integer> existingFareTypes = new ArrayList<Integer>();
			Collection<Integer> changedFareTypes = new ArrayList<Integer>();

			List<Integer> ondSequenceList = ondGroupJourneyWise.get(journeyId);

			for (Integer ondSeq : ondWiseExistingFareType.keySet()) {
				if (ondSequenceList.contains(ondSeq)) {
					existingFareTypes.addAll(ondWiseExistingFareType.get(ondSeq));
				}
			}

			for (Integer ondSeq : ondWiseChangedFareType.keySet()) {
				if (ondSequenceList.contains(ondSeq)) {
					changedFareTypes.add(ondWiseChangedFareType.get(ondSeq));
				}
			}

			int journeySegmentSize = totalSegmentSizeJourneyWise.get(journeyId);
			int fareChangedSegmentSize = fareChangedSegCountJourneyWise.get(journeyId);

			if ((isReturnFareExists(existingFareTypes) && !isAllReturnFares(existingFareTypes))
					|| (isReturnFareExists(changedFareTypes) && !isAllReturnFares(changedFareTypes))) {
				// Return Fare Type must be present in all Onds or must not have all Onds
				throw new ModuleException("airinventory.arg.changefare.invalid", "Inconsistent OND fare data receeived");
			}

			if (isAllReturnFares(existingFareTypes) && isAllReturnFares(changedFareTypes)) {
				if (changeFaresDTO.hasExistingFareOnds() && fareChangedSegmentSize != journeySegmentSize) {
					// When existing fare is a Return Fare, fare for all the segments in the journey should be changed
					throw new ModuleException("airinventory.arg.changefare.invalid", "Invalid change return fare operation");
				}
			}
		}
	}

	private boolean isReturnFareExists(Collection<Integer> fareTypes) {
		for (Integer fareType : fareTypes) {
			if (fareType == FareTypes.RETURN_FARE) {
				return true;
			}
		}
		return false;
	}

	private boolean isAllReturnFares(Collection<Integer> fareTypes) {
		if (fareTypes != null && !fareTypes.isEmpty()) {
			for (Integer fareType : fareTypes) {
				if (fareType != FareTypes.RETURN_FARE) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Mapps with the fare information
	 * 
	 * @param changedSegmentFaresMap
	 * @return
	 * @throws ModuleException
	 */
	private void populateFareInfo(LinkedHashMap<Integer, SegmentFareDTO> changedSegmentFaresMap) throws ModuleException {
		Collection<Integer> colFareIds = new HashSet<Integer>();
		// Captures the Fare Ids(s)
		for (SegmentFareDTO changedSegmentFareDTO : changedSegmentFaresMap.values()) {
			colFareIds.add(changedSegmentFareDTO.getFareSummaryDTO().getFareId());
		}

		FaresAndChargesTO faresAndChargesTO = getFareBD().getRefundableStatuses(colFareIds, null, null);
		Map<Integer, FareTO> mapFareTO = faresAndChargesTO.getFareTOs();

		Collection<FareSummaryDTO> fareSumColl = new ArrayList<FareSummaryDTO>();

		for (SegmentFareDTO changedSegmentFareDTO : changedSegmentFaresMap.values()) {
			FareTO fareTO = mapFareTO.get(changedSegmentFareDTO.getFareSummaryDTO().getFareId());
			FareSummaryDTO fareSummaryDTO = new FareSummaryDTO(fareTO);
			changedSegmentFareDTO.setFareSummaryDTO(fareSummaryDTO);
			fareSumColl.add(fareSummaryDTO);
		}

		if (fareSumColl.size() > 0 && AppSysParamsUtil.isShowFareDefInDepAirPCurr()) {
			FareRulesUtil.injectDepAirportCurrFares(fareSumColl);
		}

	}

	private List<OndFareDTO> prepareFaresSeatsSegmentsDTOs(ChangeFaresDTO changeFareDTO, int ondSequence,
			boolean isOpenReturnSearch, int subJourneyGroup, List<Integer> flightSegId) throws ModuleException {
		List<OndFareDTO> changedFaresSeatsSegmentsDTOs = new ArrayList<OndFareDTO>();

		boolean splitOperationInvoked = isSplitOperationForBusInvoked(changeFareDTO.getExistingFareChargesSeatsSegmentsDTOs());

		LinkedHashMap<Integer, SegmentFareDTO> newSegmentFaresMap = changeFareDTO.getNewSegmentFaresMap(ondSequence);
		LinkedHashMap<Integer, SegmentFareDTO> changedSegmentFaresMap = changeFareDTO.getChangedSegmentFaresMap();
		if (newSegmentFaresMap != null && newSegmentFaresMap.size() > 0) {
			for (SegmentFareDTO segmentFareDTO : newSegmentFaresMap.values()) {
				if (flightSegId.contains(segmentFareDTO.getFlightSegId())) {

					if (segmentFareDTO.getFareType() == FareTypes.PER_FLIGHT_FARE) {
						OndFareDTO ondFareDTO = new OndFareDTO();

						if (ondSequence == OndSequence.IN_BOUND) {
							ondFareDTO.setInBoundOnd(true);
							ondFareDTO.setOpenReturn(isOpenReturnSearch);
						}
						ondFareDTO.setFareSummaryDTO(segmentFareDTO.getFareSummaryDTO());
						ondFareDTO.setFareType(segmentFareDTO.getFareType());
						ondFareDTO.setOndSequence(ondSequence);
						ondFareDTO.setRequestedOndSequence(segmentFareDTO.getRequestedOndSequence());
						// }
						ondFareDTO.setSubJourneyGroup(subJourneyGroup);
						ondFareDTO.setOndCode(segmentFareDTO.getOndCode());
						ondFareDTO.addSegment(new Integer(segmentFareDTO.getFlightSegId()), segmentFareDTO.getFlightSegmentDTO());
						ondFareDTO.addSegmentSeatDistsDTO(segmentFareDTO.getSegmentSeatDistsDTO());
						ondFareDTO.setSplitOperationForBusInvoked(splitOperationInvoked);
						changedFaresSeatsSegmentsDTOs.add(ondFareDTO);
					} else {
						// outbound flight has a through fare or return fare or half return fare
						if (segmentFareDTO.getFareType() != FareTypes.OND_FARE
								&& segmentFareDTO.getFareType() != FareTypes.RETURN_FARE
								&& segmentFareDTO.getFareType() != FareTypes.HALF_RETURN_FARE) {
							throw new ModuleException("airinventory.arg.changefare.invalid",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}

						if (changedFaresSeatsSegmentsDTOs.size() == 0) {
							OndFareDTO ondFareDTO = new OndFareDTO();
							ondFareDTO.setFareType(segmentFareDTO.getFareType());
							// int effectiveOndSequence = ondSequence;
							// if (sameTypeSegs != null && isBusSegmentExists) {
							// effectiveOndSequence = sameTypeSegs.get(ondSequence);
							// }

							ondFareDTO.setOndSequence(ondSequence);
							ondFareDTO.setRequestedOndSequence(segmentFareDTO.getRequestedOndSequence());
							ondFareDTO.setSubJourneyGroup(subJourneyGroup);
							ondFareDTO.setSplitOperationForBusInvoked(splitOperationInvoked);
							FareSummaryDTO fareSummaryDTOForJourney = segmentFareDTO.getFareSummaryDTO().clone();

							FareSummaryDTO totalFareSummaryDTO = segmentFareDTO.getFareSummaryDTO();
							if (segmentFareDTO.getFareType() == FareTypes.RETURN_FARE
									|| (segmentFareDTO.getFareType() == FareTypes.HALF_RETURN_FARE
											&& changedSegmentFaresMap.containsKey(segmentFareDTO.getFlightSegId()))) {

								if (ondSequence == OndSequence.OUT_BOUND) {
									FareSummaryUtil.setOutBoundFareSummaryDTO(fareSummaryDTOForJourney, 50);

								} else {
									FareSummaryUtil.setInBoundFareSummaryDTO(fareSummaryDTOForJourney, 50);

								}

								if (segmentFareDTO.getFareType() == FareTypes.RETURN_FARE) {
									ondFareDTO.setFareType(FareTypes.HALF_RETURN_FARE);
								}
								DepAPFareDTO depAPFareDTO = totalFareSummaryDTO.getDepartureAPFare();
								if (depAPFareDTO != null) {
									DepAPFareDTO depAPFare = new DepAPFareDTO();

									double journeyFareAmount = AccelAeroCalculator
											.divide(new BigDecimal(depAPFareDTO.getAdultFare()), 2).doubleValue();
									depAPFare.setAdultFare(journeyFareAmount);

									journeyFareAmount = AccelAeroCalculator.divide(new BigDecimal(depAPFareDTO.getChildFare()), 2)
											.doubleValue();
									depAPFare.setChildFare(journeyFareAmount);

									journeyFareAmount = AccelAeroCalculator
											.divide(new BigDecimal(depAPFareDTO.getInfantFare()), 2).doubleValue();
									depAPFare.setInfantFare(journeyFareAmount);

									fareSummaryDTOForJourney.setDepartureAPFare(depAPFare);
								}

							}

							if (ondSequence == OndSequence.IN_BOUND) {
								ondFareDTO.setInBoundOnd(true);
								ondFareDTO.setOpenReturn(isOpenReturnSearch);
							}
							ondFareDTO.setFareSummaryDTO(fareSummaryDTOForJourney);

							// FIXME: when bus fare split enabled [AIRLINE_296] following fix need to verified on all
							// combination
							ondFareDTO.setOndCode(segmentFareDTO.getOndCode());
							// ondFareDTO.setOndCode(segmentFareDTO.getSegmentCode());
							changedFaresSeatsSegmentsDTOs.add(ondFareDTO);

						}

						OndFareDTO ondFareDTO = changedFaresSeatsSegmentsDTOs.get(0);
						ondFareDTO.setSubJourneyGroup(subJourneyGroup);
						ondFareDTO.addSegment(new Integer(segmentFareDTO.getFlightSegId()), segmentFareDTO.getFlightSegmentDTO());
						ondFareDTO.addSegmentSeatDistsDTO(segmentFareDTO.getSegmentSeatDistsDTO());

					}
				}
			}
		}

		return changedFaresSeatsSegmentsDTOs;
	}

	/* Jira 4160 Start - To display Half Return Fares */
	private void setBookingCodeFareIdsCombinationsForHalfReturns(
			List<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketFaresCombinationForSegments,
			boolean includeOnlyMinPublicFare) {
		for (int i = 0; i < allBucketFaresCombinationForSegments.size(); i++) {
			LinkedHashMap<String, AllFaresBCInventorySummaryDTO> refSegment = allBucketFaresCombinationForSegments.get(i);
			Map<Integer, FareSummaryDTO> fareSummaryDTOsMap = null;
			FareSummaryDTO fareSummaryDTO = null;
			boolean minPublicFareFound = false;

			for (String bookingCode : refSegment.keySet()) {
				fareSummaryDTOsMap = refSegment.get(bookingCode).getFareSummaryDTOs();
				for (Integer fareId : fareSummaryDTOsMap.keySet()) {
					fareSummaryDTO = fareSummaryDTOsMap.get(fareId);

					if (includeOnlyMinPublicFare && minPublicFareFound && fareSummaryDTO.getVisibleChannels()
							.contains(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY))) {
						continue;
					}

					boolean isBCFareCombinationCommon = true;

					if (isBCFareCombinationCommon) {
						setBCFareCombinationForHalfReturns(allBucketFaresCombinationForSegments, bookingCode);
						if (!minPublicFareFound && includeOnlyMinPublicFare && fareSummaryDTO.getVisibleChannels()
								.contains(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY))) {
							minPublicFareFound = true;
						}
					}
				}
			}
		}
	}

	private void setBCFareCombinationForHalfReturns(
			List<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketFaresCombinationForSegments, String bookingCode) {
		int maxAvailableSeatsCommonToAllSegs = -1000;
		int maxAvailableNestedSeatsCommonToAllSegs = -1000;
		for (LinkedHashMap<String, AllFaresBCInventorySummaryDTO> segment : allBucketFaresCombinationForSegments) {
			AllFaresBCInventorySummaryDTO bcInv = segment.get(bookingCode);
			if (bcInv != null) {
				if (maxAvailableSeatsCommonToAllSegs == -1000
						|| bcInv.getBcInventorySummaryDTO().getAvailableSeats() < maxAvailableSeatsCommonToAllSegs) {
					maxAvailableSeatsCommonToAllSegs = bcInv.getBcInventorySummaryDTO().getAvailableSeats();
				}
				if (maxAvailableNestedSeatsCommonToAllSegs == -1000
						|| bcInv.getBcInventorySummaryDTO().getAvailableNestedSeats() < maxAvailableNestedSeatsCommonToAllSegs) {
					maxAvailableNestedSeatsCommonToAllSegs = bcInv.getBcInventorySummaryDTO().getAvailableHRTNestedSeats();
				}
			} else {
				maxAvailableSeatsCommonToAllSegs = 0;
				maxAvailableNestedSeatsCommonToAllSegs = 0;
				break;
			}
		}

		for (LinkedHashMap<String, AllFaresBCInventorySummaryDTO> segment : allBucketFaresCombinationForSegments) {
			AllFaresBCInventorySummaryDTO bcInv = segment.get(bookingCode);
			if (bcInv != null) {
				bcInv.getBcInventorySummaryDTO().setAvailableMaxSeats(maxAvailableSeatsCommonToAllSegs);
				bcInv.getBcInventorySummaryDTO().setAvailableMaxNestedSeats(maxAvailableNestedSeatsCommonToAllSegs);
			}
		}
	}

	/* Jira 4160 End - To display Half Return Fares */

	// TODO consider storing common bc/fareid combination with in allFaresDTO
	private void setCommonBookingCodeFareIdsCombinations(
			List<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketFaresCombinationForSegments,
			boolean includeOnlyMinPublicFare, boolean isReturnFareSearch, int paxCount) {
		LinkedHashMap<String, AllFaresBCInventorySummaryDTO> refSegment = allBucketFaresCombinationForSegments.get(0);
		Map<Integer, FareSummaryDTO> fareSummaryDTOsMap = null;
		FareSummaryDTO fareSummaryDTO = null;
		boolean minPublicFareFound = false;
		HashSet<String> commonBCs = new HashSet<String>();

		if (refSegment != null) {
			for (String bookingCode : refSegment.keySet()) {
				fareSummaryDTOsMap = refSegment.get(bookingCode).getFareSummaryDTOs();
				for (Integer fareId : fareSummaryDTOsMap.keySet()) {
					fareSummaryDTO = fareSummaryDTOsMap.get(fareId);
					if (includeOnlyMinPublicFare && minPublicFareFound && fareSummaryDTO.getVisibleChannels()
							.contains(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY))) {
						continue;
					}

					boolean isBCFareCombinationCommon = true;
					for (int i = 1; i < allBucketFaresCombinationForSegments.size(); i++) {
						LinkedHashMap<String, AllFaresBCInventorySummaryDTO> segment = allBucketFaresCombinationForSegments
								.get(i);
						if (segment.containsKey(bookingCode)) {
							if (!(segment.get(bookingCode)).containsFareId(fareId)) {
								isBCFareCombinationCommon = false;
							}
						} else {
							isBCFareCombinationCommon = false;
						}
					}

					if (isBCFareCombinationCommon) {
						commonBCs.add(bookingCode);
						setBCFareCombinationCommonFlag(allBucketFaresCombinationForSegments, bookingCode, fareId,
								isReturnFareSearch, paxCount);
						if (!minPublicFareFound && includeOnlyMinPublicFare && fareSummaryDTO.getVisibleChannels()
								.contains(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY))) {
							minPublicFareFound = true;
						}
					}
				}
			}
		}

		if (AppSysParamsUtil.showNestedAvailableSeats()) {
			for (LinkedHashMap<String, AllFaresBCInventorySummaryDTO> segment : allBucketFaresCombinationForSegments) {
				for (String bookingCode : segment.keySet()) {
					if (!commonBCs.contains(bookingCode)) {
						AllFaresBCInventorySummaryDTO bcInv = segment.get(bookingCode);
						bcInv.getBcInventorySummaryDTO()
								.setAvailableMaxSeats(bcInv.getBcInventorySummaryDTO().getAvailableSeats());
						bcInv.getBcInventorySummaryDTO()
								.setAvailableMaxNestedSeats(bcInv.getBcInventorySummaryDTO().getAvailableNestedSeats());
					}
				}
			}
		}
	}

	private void setBCFareCombinationCommonFlag(
			List<LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketFaresCombinationForSegments, String bookingCode,
			Integer fareId, boolean isReturnFareSearch, int paxCount) {
		int maxAvailableSeatsCommonToAllSegs = -1000;
		int maxAvailableNestedSeatsCommonToAllSegs = -1000;
		for (LinkedHashMap<String, AllFaresBCInventorySummaryDTO> segment : allBucketFaresCombinationForSegments) {
			AllFaresBCInventorySummaryDTO bcInv = segment.get(bookingCode);
			if (maxAvailableSeatsCommonToAllSegs == -1000
					|| bcInv.getBcInventorySummaryDTO().getAvailableSeats() < maxAvailableSeatsCommonToAllSegs) {

				if (isReturnFareSearch && paxCount > 0 && bcInv.getBcInventorySummaryDTO().isBookedFlightCabinBeingSearched()) {
					maxAvailableSeatsCommonToAllSegs = paxCount;
				} else {
					maxAvailableSeatsCommonToAllSegs = bcInv.getBcInventorySummaryDTO().getAvailableSeats();
				}

			}
			if (maxAvailableNestedSeatsCommonToAllSegs == -1000
					|| bcInv.getBcInventorySummaryDTO().getAvailableNestedSeats() < maxAvailableNestedSeatsCommonToAllSegs) {
				maxAvailableNestedSeatsCommonToAllSegs = bcInv.getBcInventorySummaryDTO().getAvailableNestedSeats();
			}

		}

		for (LinkedHashMap<String, AllFaresBCInventorySummaryDTO> segment : allBucketFaresCombinationForSegments) {
			AllFaresBCInventorySummaryDTO bcInv = segment.get(bookingCode);
			bcInv.setBCFareIdCommonFlag(fareId);
			bcInv.getBcInventorySummaryDTO().setAvailableMaxSeats(maxAvailableSeatsCommonToAllSegs);
			bcInv.getBcInventorySummaryDTO().setAvailableMaxNestedSeats(maxAvailableNestedSeatsCommonToAllSegs);
		}
	}

	/**
	 * Filters buckets having fares for each flightsegment
	 * 
	 * @param flightSegmentDTOs
	 * @param bucketFilteringCriteria
	 * @param skipDeptDate
	 * @return
	 */
	private LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> filterBuckets(
			Collection<FlightSegmentDTO> flightSegmentDTOs, BucketFilteringCriteria bucketFilteringCriteria, boolean skipDeptDate)
			throws ModuleException {
		boolean bucketsExists = false;
		LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketFaresCombinationsForSegments = new LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>();
		for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
			BucketFilteringCriteria fltSegFareFilteringCriteria = bucketFilteringCriteria.clone();
			fltSegFareFilteringCriteria.setFlightId(flightSegmentDTO.getFlightId().intValue());
			fltSegFareFilteringCriteria.setSegmentCode(flightSegmentDTO.getSegmentCode());
			fltSegFareFilteringCriteria.setFromAirport(flightSegmentDTO.getFromAirport());
			fltSegFareFilteringCriteria.setToAirport(flightSegmentDTO.getToAirport());
			if (!skipDeptDate) {
				fltSegFareFilteringCriteria.setDepatureDateAndTime(flightSegmentDTO.getDepartureDateTime());
				fltSegFareFilteringCriteria.setDepartureDateTimeZulu(flightSegmentDTO.getDepartureDateTimeZulu());
				fltSegFareFilteringCriteria.setArrivalDateTime(flightSegmentDTO.getArrivalDateTime());
			}
			fltSegFareFilteringCriteria.setNoOfAdults(bucketFilteringCriteria.getNoOfAdults());
			fltSegFareFilteringCriteria.setNoOfChilds(bucketFilteringCriteria.getNoOfChilds());
			fltSegFareFilteringCriteria.setNoOfInfants(bucketFilteringCriteria.getNoOfInfants());

			LinkedHashMap<String, AllFaresBCInventorySummaryDTO> allBucketOndFaresCombintationsForSegment = getSeatAvailabilityDAO()
					.getAllBucketFareCombinationsForSegment(fltSegFareFilteringCriteria);

			if (allBucketOndFaresCombintationsForSegment.size() == 0) {
				bucketsExists = false;
				break;
			}

			allBucketFaresCombinationsForSegments.put(flightSegmentDTO.getSegmentId(), allBucketOndFaresCombintationsForSegment);
			bucketsExists = true;

			FareRulesUtil.setApplicabilityForAllBucketOndFaresCombintationsForSegment(allBucketFaresCombinationsForSegments);
		}

		if (bucketsExists) {
			return allBucketFaresCombinationsForSegments;
		}
		return null;
	}

	private String prepareOndCode(Collection<FlightSegmentDTO> flightSegmentDTOs) {
		String ondCode = "";
		for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
			String segmentCode = flightSegmentDTO.getSegmentCode();
			String[] airports = StringUtils.split(segmentCode, "/");
			for (String airport : airports) {
				if (ondCode.indexOf(airport) == -1) {
					if (!ondCode.equals("")) {
						ondCode += "/";
					}
					ondCode += airport;
				}
			}
		}
		return ondCode;
	}

	// TODO set charges for one way flight only?? Re name the method name accordingly
	private void setChargesForSelectedFlight(AvailableIBOBFlightSegment availableFlightSegment,
			AvailableFlightSearchDTO availableFlightSearchDTO, ContextOndFlightsTO contextOndFltInfo) throws ModuleException {
		if (availableFlightSegment != null) {
			if (!availableFlightSegment.getAvailableLogicalCabinClasses().isEmpty()
					|| !availableFlightSegment.getAvailableExternalLogicalCabinClasses().isEmpty()) {
				if (availableFlightSegment.isDirectFlight()) {
					singleFlightChargeQuote((AvailableFlightSegment) availableFlightSegment, availableFlightSearchDTO,
							contextOndFltInfo);
					if (availableFlightSearchDTO.isFlexiQuote(availableFlightSegment.getOndSequence())) {
						singleFlightFlexiQuote((AvailableFlightSegment) availableFlightSegment, availableFlightSearchDTO);
					}

				} else {
					connectedFlightsChargeQuote((AvailableConnectedFlight) availableFlightSegment, availableFlightSearchDTO,
							availableFlightSegment.isInboundFlightSegment(), contextOndFltInfo);
					if (availableFlightSearchDTO.isFlexiQuote(availableFlightSegment.getOndSequence())) {
						connectedFlightFlexiQuote((AvailableConnectedFlight) availableFlightSegment, availableFlightSearchDTO);
					}
				}

			}
		}
	}

	private String getONDPosition(AvailableIBOBFlightSegment ondFlight, AvailableFlightSearchDTO restrictionsDTO,
			String localOndPosition) {
		boolean isFirstOnd = restrictionsDTO.isFirstOnd(ondFlight.getOndSequence());
		boolean isLastOnd = restrictionsDTO.isLastOnd(ondFlight.getOndSequence());
		if (!AppSysParamsUtil.isRequoteEnabled() && restrictionsDTO.isModifyBooking()) {
			if (isFirstOnd && restrictionsDTO.getFirstDepartureDateTimeZulu() != null
					&& ondFlight.getFirstDepartureDateZulu() != null
					&& ondFlight.getFirstDepartureDateZulu().after(restrictionsDTO.getFirstDepartureDateTimeZulu())) {
				isFirstOnd = false;
			}
			if (isLastOnd && restrictionsDTO.getLastArrivalDateTimeZulu() != null && ondFlight.getLastArrivalDateZulu() != null
					&& restrictionsDTO.getLastArrivalDateTimeZulu().after(ondFlight.getLastArrivalDateZulu())) {
				isLastOnd = false;
			}
		}
		String totalJourneyOndPosition = getTotalJourneyONDPosition(restrictionsDTO, ondFlight.getFlightSegmentDTOs());

		if (totalJourneyOndPosition != null) {
			return totalJourneyOndPosition;
		} else {
			return getONDPosition(isFirstOnd, isLastOnd, localOndPosition);
		}

	}

	String getONDPosition(boolean isFirstOnd, boolean isLastOnd, String localOndPosition) {
		String ondPosition = null;
		ondPosition = localOndPosition;
		if (isFirstOnd && isLastOnd) {
			ondPosition = (localOndPosition == null) ? QuoteChargesCriteria.FIRST_AND_LAST_OND : localOndPosition;
		} else if (isFirstOnd) {
			if (localOndPosition == null) {
				ondPosition = QuoteChargesCriteria.FIRST_OND;
			} else if (!localOndPosition.equals(QuoteChargesCriteria.FIRST_OND)) {
				ondPosition = QuoteChargesCriteria.INBETWEEN_OND;
			}
		} else if (isLastOnd) {
			if (localOndPosition == null) {
				ondPosition = QuoteChargesCriteria.LAST_OND;
			} else if (!localOndPosition.equals(QuoteChargesCriteria.LAST_OND)) {
				ondPosition = QuoteChargesCriteria.INBETWEEN_OND;
			}
		} else {
			ondPosition = QuoteChargesCriteria.INBETWEEN_OND;
		}
		return ondPosition;
	}

	private String getONDFromSegments(List<FlightSegmentDTO> fltSegList) {
		String ondFromSegments = "";
		if (fltSegList != null) {
			Iterator<FlightSegmentDTO> fltSegListIte = fltSegList.iterator();
			while (fltSegListIte.hasNext()) {
				ondFromSegments += fltSegListIte.next().getSegmentCode();
			}
		}

		return ondFromSegments;
	}

	/**
	 * Charge Quoting for connected flight
	 * 
	 * @param connectedFlights
	 * @param availableFlightSearchDTO
	 * @param contextOndFltInfo
	 * @throws ModuleException
	 */
	private void connectedFlightsChargeQuote(AvailableConnectedFlight connectedFlights,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean isInboundJourney, ContextOndFlightsTO contextOndFltInfo)
			throws ModuleException {
		boolean connectionFare = false;
		boolean segmentFares = false;
		String bookingCode = null;
		boolean hasInwardCxnFlight = false;
		boolean hasOutwardCxnFlight = false;

		long inwardTransitDuration = 0;
		long outwardTransitDuration = 0;
		Collection<Long> ondTransitDurList = new ArrayList<Long>();

		Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();

		Collection<FlightSegmentDTO> allConnectedFltSegs = new ArrayList<FlightSegmentDTO>();
		allConnectedFltSegs.addAll(connectedFlights.getFlightSegmentDTOs());
		TransitInfoDTO transitInfoDTO = getTransitInfoDTO(allConnectedFltSegs,
				contextOndFltInfo.getConnectingFlightSegment(connectedFlights.getOndSequence()));

		// boolean isIbeBooking = AirinventoryUtils.isIbeBooking(availableFlightSearchDTO.getAppIndicator());
		Set<String> logicalCabinClasses = new HashSet<String>();
		if (availableFlightSearchDTO.isFareCalendarSearch()) {
			logicalCabinClasses.addAll(connectedFlights.getAvailableLogicalCabinClasses());
		} else {
			logicalCabinClasses.add(connectedFlights.getSelectedLogicalCabinClass());
		}

		Set<String> bookingClasses = new HashSet<String>();
		Set<String> busBookingClasses = new HashSet<String>();
		for (String availableLogicalCabinClass : logicalCabinClasses) {
			if (connectedFlights.getFare(availableLogicalCabinClass) != null
					&& connectedFlights.getFareType(availableLogicalCabinClass) != FareTypes.PER_FLIGHT_FARE) {
				AvailableFlightSegment flightSegment = connectedFlights.getAvailableFlightSegments().iterator().next();
				if (flightSegment.getSeatsAvailability(availableLogicalCabinClass) != null) {

					boolean isBusSegment = AirInventoryModuleUtils.getAirportBD().isBusSegment(flightSegment.getOndCode());
					// get the highest nest ranked booking code, in case of nested seats
					SeatDistribution highestRankedSeatDistribution = (SeatDistribution) BeanUtils
							.getLastElement(flightSegment.getSeatsAvailability(availableLogicalCabinClass));
					bookingClasses.add(highestRankedSeatDistribution.getBookingClassCode());
					if (isBusSegment) {
						busBookingClasses.add(highestRankedSeatDistribution.getBookingClassCode());
					}
				}
			} else {
				for (AvailableFlightSegment flightSeg : connectedFlights.getAvailableFlightSegments()) {

					Collection<SeatDistribution> colSeatDistribution = flightSeg.getSeatsAvailability(availableLogicalCabinClass);

					boolean isBusSegment = AirInventoryModuleUtils.getAirportBD().isBusSegment(flightSeg.getOndCode());

					if (colSeatDistribution != null && !colSeatDistribution.isEmpty()) {
						SeatDistribution highestRankedSeatDistribution = (SeatDistribution) BeanUtils
								.getLastElement(colSeatDistribution);
						bookingClasses.add(highestRankedSeatDistribution.getBookingClassCode());
						if (isBusSegment) {
							busBookingClasses.add(highestRankedSeatDistribution.getBookingClassCode());
						}
					}

					for (String logicalCabinClass : flightSeg.getAvailableExternalLogicalCabinClasses()) {
						Collection<AvailableBCAllocationSummaryDTO> externalBookingClassAvailability = flightSeg
								.getFilteredBucketsDTOForSegmentFares().getExternalBookingClassAvailability(logicalCabinClass);
						for (AvailableBCAllocationSummaryDTO bcAllocSummary : externalBookingClassAvailability) {
							bookingClasses.add(bcAllocSummary.getBookingCode());
							if (isBusSegment) {
								busBookingClasses.add(bcAllocSummary.getBookingCode());
							}
						}
					}
				}
			}
		}

		Map<String, Collection<String>> bookingClassWiseChargeGroupCodes = getApplicableChargeGroups(bookingClasses,
				busBookingClasses);

		for (String availableLogicalCabinClass : logicalCabinClasses) {
			if (connectedFlights.getFare(availableLogicalCabinClass) != null
					&& connectedFlights.getFareType(availableLogicalCabinClass) != FareTypes.PER_FLIGHT_FARE) {
				hasInwardCxnFlight = false;
				hasOutwardCxnFlight = false;
				AvailableFlightSegment flightSegment = connectedFlights.getAvailableFlightSegments().iterator().next();
				if (flightSegment.getSeatsAvailability(availableLogicalCabinClass) != null) {
					// get the highest nest ranked booking code, in case of nested seats
					SeatDistribution highestRankedSeatDistribution = (SeatDistribution) BeanUtils
							.getLastElement(flightSegment.getSeatsAvailability(availableLogicalCabinClass));
					bookingCode = highestRankedSeatDistribution.getBookingClassCode();
				}

				OriginDestinationInfoDTO ondInfo = availableFlightSearchDTO.getOndInfo(connectedFlights.getOndSequence());

				// Setting transit tax new behavior only for interline fares
				if (availableFlightSearchDTO.isInterlineFareQuoted()) {
					if (availableFlightSearchDTO.isHasInwardCxnFlight()) {
						hasInwardCxnFlight = true;
						inwardTransitDuration = flightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu().getTime()
								- ondInfo.getInwardCxnFlightArriTime().getTime();
					}
					if (availableFlightSearchDTO.isHasOutwardCxnFlight()) {
						hasOutwardCxnFlight = true;
						outwardTransitDuration = ondInfo.getOutwardCxnFlightDepTime().getTime()
								- flightSegment.getFlightSegmentDTO().getArrivalDateTimeZulu().getTime();
					}
				}

				ondTransitDurList = transitInfoDTO.getTransitDurationList();

				String cabinClassCode = ondInfo.getPreferredClassOfService();
				// If logical cabin class selected instead of cabin class, derive cabin class code from logical cabin
				// class
				if (cabinClassCode == null || "".equals(cabinClassCode)) {
					String logicalCCCode = ondInfo.getPreferredLogicalCabin();
					cabinClassCode = getLogicalCabinClassDAO().getCabinClass(logicalCCCode);
				}

				criteriaCollection.add(createChargeQuoteCriteria(connectedFlights.getOndCode(),
						availableFlightSearchDTO.getPosAirport(), flightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu(),
						AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS,
						AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION, null, // Effective charge amount for
																							// percentage charge will be
																							// calculated when charges
																							// retrieved at
																							// later stage
						getONDPosition(connectedFlights, availableFlightSearchDTO, null),
						transitInfoDTO.isQualifyForShortTransit(), availableFlightSearchDTO.getAgentCode(), hasInwardCxnFlight,
						hasOutwardCxnFlight, null, inwardTransitDuration, outwardTransitDuration, ondTransitDurList,
						cabinClassCode, transitInfoDTO.getTransitAirports(), transitInfoDTO.getTransitAirports(),
						availableFlightSearchDTO.getEffectiveLastFQDate(flightSegment.getFlightSegmentDTO().getSegmentId()),
						availableFlightSearchDTO.getChannelCode(), availableFlightSearchDTO.getExcludedCharges(),
						availableFlightSearchDTO.getFlightSearchGap(),
						getBundledFareBL().getApplicableBundledFareChargeCode(connectedFlights)));
				connectionFare = true;
			} else if (connectedFlights.isSeatsAvailable(availableLogicalCabinClass)) {
				// FlightSegmentDTO previousFlightSegmentDTO = null;
				hasInwardCxnFlight = false;
				hasOutwardCxnFlight = false;

				inwardTransitDuration = 0;
				outwardTransitDuration = 0;

				int count = 1;
				for (AvailableFlightSegment flightSeg : connectedFlights.getAvailableFlightSegments()) {
					OriginDestinationInfoDTO ondInfo = availableFlightSearchDTO.getOndInfo(connectedFlights.getOndSequence());

					String localOndPosition = QuoteChargesCriteria.INBETWEEN_OND;
					if (count == 1) {
						localOndPosition = QuoteChargesCriteria.FIRST_OND;

						// Setting transit tax new behavior only for interline fares
						if (availableFlightSearchDTO.isInterlineFareQuoted()) {
							if (ondInfo.isHasInwardCxnFlight()) {
								hasInwardCxnFlight = true;
								inwardTransitDuration = flightSeg.getFlightSegmentDTO().getDepartureDateTimeZulu().getTime()
										- ondInfo.getInwardCxnFlightArriTime().getTime();
							}
						}
					} else if (count == connectedFlights.getAvailableFlightSegments().size()) {
						localOndPosition = QuoteChargesCriteria.LAST_OND;

						// Setting transit tax new behavior only for interline fares
						if (availableFlightSearchDTO.isInterlineFareQuoted()) {
							if (isInboundJourney && ondInfo.isHasOutwardCxnFlight()) {
								hasOutwardCxnFlight = true;
								outwardTransitDuration = ondInfo.getOutwardCxnFlightDepTime().getTime()
										- flightSeg.getFlightSegmentDTO().getArrivalDateTimeZulu().getTime();
							}
						}
					}

					// Collection<FlightSegmentDTO> flidghtSegmentDTOs = new ArrayList<FlightSegmentDTO>();
					// if (previousFlightSegmentDTO != null) {
					// flightSegmentDTOs.add(previousFlightSegmentDTO);
					// }
					// flightSegmentDTOs.add(flightSeg.getFlightSegmentDTO());

					String cabinClassCode = ondInfo.getPreferredClassOfService();
					// If logical cabin class selected instead of cabin class, derive cabin class code from logical
					// cabin
					// class
					if (cabinClassCode == null || "".equals(cabinClassCode)) {
						String logicalCCCode = ondInfo.getPreferredLogicalCabin();
						cabinClassCode = getLogicalCabinClassDAO().getCabinClass(logicalCCCode);
					}

					criteriaCollection.add(createChargeQuoteCriteria(flightSeg.getFlightSegmentDTO().getSegmentCode(),
							availableFlightSearchDTO.getPosAirport(), flightSeg.getFlightSegmentDTO().getDepartureDateTimeZulu(),
							AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS,
							AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION, null, // Effective charge amount
																								// for percentage charge
																								// will be calculated
																								// when charges
																								// retrieved
																								// at later stage
							getONDPosition(connectedFlights, availableFlightSearchDTO, localOndPosition),
							transitInfoDTO.isQualifyForShortTransit(flightSeg.getFlightSegmentDTO().getDepartureDateTimeZulu()),
							availableFlightSearchDTO.getAgentCode(), hasInwardCxnFlight, hasOutwardCxnFlight, null, // Effective
																													// charge
																													// amount
																													// for
																													// percentage
																													// charge
																													// will
																													// be
																													// calculated
																													// when
																													// charges
																													// retrieved
																													// at
																													// later
																													// stage
							inwardTransitDuration, outwardTransitDuration,
							transitInfoDTO.getTransitDurationList(flightSeg.getFlightSegmentDTO().getDepartureDateTimeZulu()),
							cabinClassCode,
							transitInfoDTO.getTransitAirports(flightSeg.getFlightSegmentDTO().getDepartureDateTimeZulu()),
							transitInfoDTO.getDepArrExcludeAirports(flightSeg.getFlightSegmentDTO().getDepartureAirportCode(),
									flightSeg.getFlightSegmentDTO().getArrivalAirportCode()),
							availableFlightSearchDTO.getEffectiveLastFQDate(flightSeg.getFlightSegmentDTO().getSegmentId()),
							availableFlightSearchDTO.getChannelCode(), availableFlightSearchDTO.getExcludedCharges(),
							availableFlightSearchDTO.getFlightSearchGap(),
							getBundledFareBL().getApplicableBundledFareChargeCode(connectedFlights)));
					// previousFlightSegmentDTO = flightSeg.getFlightSegmentDTO();
					count++;
				}
				segmentFares = true;
			}

			HashMap<String, HashMap<String, QuotedChargeDTO>> quotedChargesMap = getChargeBD().quoteCharges(criteriaCollection);

			if (connectionFare) {// set charges for non-fixed quota connection fare
				Collection<QuotedChargeDTO> charges = filterChargesForChargeGroups(
						bookingClassWiseChargeGroupCodes.get(bookingCode),
						AirinventoryUtils.getQuotedChargeDTOList(quotedChargesMap.get(connectedFlights.getOndCode())));
				connectedFlights.setCharges(availableLogicalCabinClass, charges);
			}

			if (segmentFares) {// set charges for non-fixed quota segment fares
				for (AvailableFlightSegment flightSeg : connectedFlights.getAvailableFlightSegments()) {

					Collection<QuotedChargeDTO> charges = null;

					// get the highest nest ranked booking code, in case of nested seats
					Collection<QuotedChargeDTO> chargesCol = AirinventoryUtils
							.getQuotedChargeDTOList(quotedChargesMap.get(flightSeg.getFlightSegmentDTO().getSegmentCode()));
					Collection<SeatDistribution> colSeatDist = flightSeg.getSeatsAvailability(availableLogicalCabinClass);

					if (colSeatDist != null) {
						SeatDistribution highestRankedSeatDistribution = (SeatDistribution) BeanUtils.getLastElement(colSeatDist);
						bookingCode = highestRankedSeatDistribution.getBookingClassCode();
						charges = filterChargesForChargeGroups(bookingClassWiseChargeGroupCodes.get(bookingCode), chargesCol);
						flightSeg.setCharges(availableLogicalCabinClass, charges);
					}

					// set charges for external booking classes
					for (String logicalCabinClass : flightSeg.getAvailableExternalLogicalCabinClasses()) {
						Collection<AvailableBCAllocationSummaryDTO> externalBookingClassAvailability = flightSeg
								.getFilteredBucketsDTOForSegmentFares().getExternalBookingClassAvailability(logicalCabinClass);
						for (AvailableBCAllocationSummaryDTO bcAllocSummary : externalBookingClassAvailability) {
							charges = filterChargesForChargeGroups(
									bookingClassWiseChargeGroupCodes.get(bcAllocSummary.getBookingCode()), chargesCol);
							flightSeg.setInterlineBookingClassCharge(logicalCabinClass, bcAllocSummary.getBookingCode(),
									ChargeQuoteUtils.calculateTotalCharges(charges));
						}
					}
				}
			}
			for (AvailableFlightSegment flightSeg : connectedFlights.getAvailableFlightSegments()) {
				flightSeg.setRequestedNoOfAdults(availableFlightSearchDTO.getAdultCount());
				flightSeg.setRequestedNoOfChildren(availableFlightSearchDTO.getChildCount());
				flightSeg.setNumberOfInfantSeats(availableFlightSearchDTO.getInfantCount());
			}
		}
	}

	/**
	 * Charge Quoting for single flight segment Flight segment code is taken as the OND code for charge quoting
	 * 
	 * @param availableFlightSegment
	 * @param availableFlightSearchDTO
	 * @param contextOndFltInfo
	 * @throws ModuleException
	 */
	private HashMap<String, QuotedChargeDTO> singleFlightChargeQuote(AvailableFlightSegment availableFlightSegment,
			AvailableFlightSearchDTO availableFlightSearchDTO, ContextOndFlightsTO contextOndFltInfo) throws ModuleException {
		String standardBookingCode = null;
		boolean hasInwardCxnFlight = false;
		boolean hasOutwardCxnFlight = false;

		long inwardTransitDuration = 0;
		long outwardTransitDuration = 0;

		FlightSegmentDTO fltSegment = availableFlightSegment.getFlightSegmentDTO();

		Collection<FlightSegmentDTO> allFltSegs = new ArrayList<FlightSegmentDTO>();
		allFltSegs.add(fltSegment);
		TransitInfoDTO transitInfoDTO = getTransitInfoDTO(allFltSegs,
				contextOndFltInfo.getConnectingFlightSegment(availableFlightSegment.getOndSequence()));

		availableFlightSegment.setRequestedNoOfAdults(availableFlightSearchDTO.getAdultCount());
		availableFlightSegment.setRequestedNoOfChildren(availableFlightSearchDTO.getChildCount());
		availableFlightSegment.setNumberOfInfantSeats(availableFlightSearchDTO.getInfantCount());

		Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();

		// String cabinClassCode =
		// FareQuoteUtil.getClassOfserviceFromFlightSegId(availableFlightSearchDTO.getCabinClassSelection(),
		// availableFlightSegment.getFlightSegmentDTO().getSegmentId());

		OriginDestinationInfoDTO ondInfo = availableFlightSearchDTO.getOndInfo(availableFlightSegment.getOndSequence());

		// Setting transit tax new behavior only for interline fares
		if (availableFlightSearchDTO.isInterlineFareQuoted()) {
			if (ondInfo.isHasInwardCxnFlight()) {
				hasInwardCxnFlight = true;
				inwardTransitDuration = fltSegment.getDepartureDateTimeZulu().getTime()
						- ondInfo.getInwardCxnFlightArriTime().getTime();
			}
			if (ondInfo.isHasOutwardCxnFlight()) {
				hasOutwardCxnFlight = true;
				outwardTransitDuration = ondInfo.getOutwardCxnFlightDepTime().getTime()
						- fltSegment.getArrivalDateTimeZulu().getTime();
			}
		}

		String cabinClassCode = ondInfo.getPreferredClassOfService();
		// If logical cabin class selected instead of cabin class, derive cabin class code from logical cabin class
		if (cabinClassCode == null || "".equals(cabinClassCode)) {
			String logicalCCCode = ondInfo.getPreferredLogicalCabin();
			// String logicalCCCode = FareQuoteUtil.getClassOfserviceFromFlightSegId(availableFlightSearchDTO
			// .getLogicalCabinClassSelection(), availableFlightSegment.getFlightSegmentDTO().getSegmentId());
			cabinClassCode = getLogicalCabinClassDAO().getCabinClass(logicalCCCode);
		}

		criteriaCollection.add(createChargeQuoteCriteria(fltSegment.getSegmentCode(), availableFlightSearchDTO.getPosAirport(),
				fltSegment.getDepartureDateTime(), AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS,
				AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION, null, // Effective charge amount for
																					// percentage charge will be
																					// calculated when charges retrieved
																					// at
																					// later stage
				getONDPosition(availableFlightSegment, availableFlightSearchDTO, null),
				transitInfoDTO.isQualifyForShortTransit(fltSegment.getDepartureDateTimeZulu()),
				availableFlightSearchDTO.getAgentCode(), hasInwardCxnFlight, hasOutwardCxnFlight, null,
				// Effective charge amount for percentage charge will be calculated when charges retrieved at
				// later
				// stage
				inwardTransitDuration, outwardTransitDuration,
				transitInfoDTO.getTransitDurationList(fltSegment.getDepartureDateTimeZulu()), cabinClassCode,
				transitInfoDTO.getTransitAirports(fltSegment.getDepartureDateTimeZulu()),
				transitInfoDTO.getDepArrExcludeAirports(fltSegment.getDepartureAirportCode(), fltSegment.getArrivalAirportCode()),
				availableFlightSearchDTO.getEffectiveLastFQDate(fltSegment.getSegmentId()),
				availableFlightSearchDTO.getChannelCode(), availableFlightSearchDTO.getExcludedCharges(),
				availableFlightSearchDTO.getFlightSearchGap(),
				getBundledFareBL().getApplicableBundledFareChargeCode(availableFlightSegment)));

		HashMap<String, HashMap<String, QuotedChargeDTO>> charges = getChargeBD().quoteCharges(criteriaCollection);
		HashMap<String, QuotedChargeDTO> ondCharges = charges.get(fltSegment.getSegmentCode());

		Collection<QuotedChargeDTO> chargeCollection = AirinventoryUtils.getQuotedChargeDTOList(ondCharges);

		// boolean isIbeBooking = AirinventoryUtils.isIbeBooking(availableFlightSearchDTO.getAppIndicator());

		Set<String> logicalCabinClasses = new HashSet<String>();

		if (availableFlightSearchDTO.isFareCalendarSearch()) {
			logicalCabinClasses.addAll(availableFlightSegment.getAvailableLogicalCabinClasses());
		} else {
			logicalCabinClasses.add(availableFlightSegment.getSelectedLogicalCabinClass());
		}

		Set<String> bookingClasses = new HashSet<String>();
		Set<String> busBookingClasses = new HashSet<String>();

		boolean isBusSegment = AirInventoryModuleUtils.getAirportBD().isBusSegment(availableFlightSegment.getOndCode());

		for (String logicalCabinClass : logicalCabinClasses) {
			if (availableFlightSegment.isSeatsAvailable(logicalCabinClass)) {
				Collection<SeatDistribution> seatDists = availableFlightSegment.getSeatsAvailability(logicalCabinClass);
				if (seatDists != null) {
					for (SeatDistribution seatDistribution : seatDists) {
						bookingClasses.add(seatDistribution.getBookingClassCode());
						if (isBusSegment) {
							busBookingClasses.add(seatDistribution.getBookingClassCode());
						}
					}
				}
			}
		}

		for (String logicalCabinClass : availableFlightSegment.getAvailableExternalLogicalCabinClasses()) {
			Collection<AvailableBCAllocationSummaryDTO> externalBookingClassAvailability = availableFlightSegment
					.getFilteredBucketsDTOForSegmentFares().getExternalBookingClassAvailability(logicalCabinClass);
			for (AvailableBCAllocationSummaryDTO bcAllocSummary : externalBookingClassAvailability) {
				bookingClasses.add(bcAllocSummary.getBookingCode());
				if (isBusSegment) {
					busBookingClasses.add(bcAllocSummary.getBookingCode());
				}
			}
		}

		Map<String, Collection<String>> bookingClassWiseChargeGroupCodes = getApplicableChargeGroups(bookingClasses,
				busBookingClasses);

		for (String logicalCabinClass : logicalCabinClasses) {
			if (availableFlightSegment.isSeatsAvailable(logicalCabinClass) && bookingClassWiseChargeGroupCodes != null) {
				Collection<SeatDistribution> seatDists = availableFlightSegment.getSeatsAvailability(logicalCabinClass);
				if (seatDists != null) {
					for (SeatDistribution seatDistribution : seatDists) {
						standardBookingCode = seatDistribution.getBookingClassCode();
					}
				}
				availableFlightSegment.setCharges(logicalCabinClass, filterChargesForChargeGroups(
						bookingClassWiseChargeGroupCodes.get(standardBookingCode), chargeCollection));
			}
		}

		// set charges for external booking classes
		for (String logicalCabinClass : availableFlightSegment.getAvailableExternalLogicalCabinClasses()) {
			Collection<AvailableBCAllocationSummaryDTO> externalBookingClassAvailability = availableFlightSegment
					.getFilteredBucketsDTOForSegmentFares().getExternalBookingClassAvailability(logicalCabinClass);
			for (AvailableBCAllocationSummaryDTO bcAllocSummary : externalBookingClassAvailability) {
				Collection<QuotedChargeDTO> chargesCol = filterChargesForChargeGroups(
						bookingClassWiseChargeGroupCodes.get(bcAllocSummary.getBookingCode()), chargeCollection);
				availableFlightSegment.setInterlineBookingClassCharge(logicalCabinClass, bcAllocSummary.getBookingCode(),
						ChargeQuoteUtils.calculateTotalCharges(chargesCol));
			}
		}

		return ondCharges;
	}

	private QuoteChargesCriteria createChargeQuoteCriteria(String ond, String posAirport, Date depatureDate,
			String[] chargeGroupCodes, String[] chargeCatCodes, FareSummaryDTO fareSummaryDTO, String ondPosition,
			boolean isOndQualifyForShortTransit, String agentCode, boolean hasInwardCxnFlight, boolean hasOutwardCxnFlight,
			Map<String, Double> totalJourneyFare, long inwardTransitDuration, long outwardTransitDuration,
			Collection<Long> ondTransitDurList, String cabinClassCode, Set<String> transitAirports, Set<String> excludeDepArrAPs,
			Date chargeQuoteDate, int channelId, List<String> excludedCharges, Integer lastSearchGapInDays,
			Set<String> bundledFareChargeCodes) throws ModuleException {
		QuoteChargesCriteria criterion = new QuoteChargesCriteria();
		criterion.setOnd(ond);
		criterion.setPosAirport(posAirport);
		criterion.setDepatureDate(depatureDate);
		criterion.setChargeGroups(chargeGroupCodes);
		criterion.setCabinClassCode(cabinClassCode);
		criterion.setChargeCatCodes(chargeCatCodes);
		criterion.setFareSummaryDTO(fareSummaryDTO);
		criterion.setOndPosition(ondPosition);
		criterion.setQualifyForShortTransit(isOndQualifyForShortTransit);
		criterion.setAgentCode(agentCode);
		criterion.setHasInwardTransit(hasInwardCxnFlight);
		criterion.setHasOutwardTransit(hasOutwardCxnFlight);
		criterion.setTotalJourneyFare(totalJourneyFare);
		criterion.setInwardTransitGap(inwardTransitDuration);
		criterion.setOutwardTransitGap(outwardTransitDuration);
		criterion.setOndTransitDurList(ondTransitDurList);
		criterion.setTransitAiports(transitAirports, excludeDepArrAPs);
		criterion.setChargeQuoteDate(chargeQuoteDate);
		criterion.setChannelId(channelId);
		if (getChargeBD().isValidChargeExclusion(excludedCharges)) {
			criterion.setExcludedCharges(excludedCharges);
		}
		criterion.setLastSearchGapInDays(lastSearchGapInDays);
		criterion.setBundledFareChargeCodes(bundledFareChargeCodes);
		return criterion;
	}

	/**
	 * Filter charges having specified charge groups. If no charge groups specified, no charges filtered and null is
	 * returned.
	 * 
	 * @param chargeGroups
	 * @param charges
	 * @return
	 */
	Collection<QuotedChargeDTO> filterChargesForChargeGroups(Collection<String> chargeGroups,
			Collection<QuotedChargeDTO> charges) {
		List<QuotedChargeDTO> filteredCharges = null;
		boolean includeBundleCharge = AppSysParamsUtil.isBundleFareToQuoteIndependentOfBCSurChargeExcluition();
		if (charges != null && chargeGroups != null) {
			filteredCharges = new ArrayList<QuotedChargeDTO>();
			for (QuotedChargeDTO quotedCharge : charges) {
				if (chargeGroups.contains(quotedCharge.getChargeGroupCode())
						|| (quotedCharge.isBundledFareCharge() && includeBundleCharge)) {
					filteredCharges.add(quotedCharge.clone());
				}
			}
		}
		return filteredCharges;
	}

	private Map<String, Collection<String>> getApplicableChargeGroups(Collection<String> bookingClasses,
			Collection<String> busBookingClasses) {
		if (!bookingClasses.isEmpty()) {
			return getBookingClassDAO().getBookingClassChargeGroups(bookingClasses, busBookingClasses);
		}
		return null;
	}

	Collection<String> getApplicableChargeGroupsForBookingClass(String bookingClass, boolean isBusBookingClass) {
		if (bookingClass != null) {
			return getBookingClassDAO().getBookingClassChargeGroups(Arrays.asList(bookingClass),
					(isBusBookingClass ? Arrays.asList(bookingClass) : new ArrayList<String>())).get(bookingClass);
		}
		return null;
	}

	private void addOndWiseFilteredBucketsDTOsForReturns(AvailableFlightSegment availableFlightSegment,
			HRTAvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		if (AppSysParamsUtil.isReturnBucketCacheEnabled()) {
			if (availableFlightSegment.getOndFilteredBucketsDTOsForReturns(availableFlightSearchDTO.getOndCode()) == null) {
				availableFlightSegment.addOndWiseFilteredBucketsDTOsForReturns(availableFlightSearchDTO.getOndCode(),
						singleFlightHalfReturnAvailabilitySearch(availableFlightSegment, availableFlightSearchDTO));
			}
		} else {
			availableFlightSegment.addOndWiseFilteredBucketsDTOsForReturns(availableFlightSearchDTO.getOndCode(),
					singleFlightHalfReturnAvailabilitySearch(availableFlightSegment, availableFlightSearchDTO));
		}
	}

	private void addOndWiseFilteredBucketsDTOsForReturns(List<FilteredBucketsDTO> filteredBucketsDTOs,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, HRTAvailableFlightSearchDTO availableFlightSearchDTO)
			throws ModuleException {

		if (!availableIBOBFlightSegment.isDirectFlight()) {
			for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableIBOBFlightSegment)
					.getAvailableFlightSegments()) {
				addOndWiseFilteredBucketsDTOsForReturns(availableFlightSegment, availableFlightSearchDTO);
				filteredBucketsDTOs
						.add(availableFlightSegment.getOndFilteredBucketsDTOsForReturns(availableFlightSearchDTO.getOndCode()));
			}
		} else {
			AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) availableIBOBFlightSegment;
			addOndWiseFilteredBucketsDTOsForReturns(availableFlightSegment, availableFlightSearchDTO);
			filteredBucketsDTOs
					.add(availableFlightSegment.getOndFilteredBucketsDTOsForReturns(availableFlightSearchDTO.getOndCode()));
		}
	}

	private AvailableIBOBFlightSegment availableFlightsReturnAvailabilitySearch(
			AvailableIBOBFlightSegment availableIBOBFlightSegment, HRTAvailableFlightSearchDTO availableFlightSearchDTO)
			throws ModuleException {
		// AvailableIBOBFlightSegment availableOutboundFlightSegment =
		// availableIBOBFlightSegment.get(OndSequence.OUT_BOUND);
		// AvailableIBOBFlightSegment availableInboundFlightSegment =
		// availableIBOBFlightSegment.get(OndSequence.IN_BOUND);
		List<FilteredBucketsDTO> filteredBucketsDTOs = new ArrayList<FilteredBucketsDTO>();
		// String outboundOndCode = availableFlightSearchDTO.getOndCode();
		// String inboundOndCode = availableInboundFlightSegment.getOndCode();

		addOndWiseFilteredBucketsDTOsForReturns(filteredBucketsDTOs, availableIBOBFlightSegment, availableFlightSearchDTO);
		// addOndWiseFilteredBucketsDTOsForReturns(filteredBucketsDTOs, availableInboundFlightSegment,
		// availableFlightSearchDTO,
		// inboundOndCode, null);

		return availableIBOBFlightSegment;
	}

	public AvailableIBOBFlightSegment singleFlightsHalfReturnAvailabilitySearch(
			AvailableIBOBFlightSegment availableIBOBFlightSegment, HRTAvailableFlightSearchDTO availableFlightSearchDTO)
			throws ModuleException {
		return availableFlightsReturnAvailabilitySearch(availableIBOBFlightSegment, availableFlightSearchDTO);
	}

	public AvailableIBOBFlightSegment connectedFlightsHalfReturnAvailabilitySearch(
			AvailableIBOBFlightSegment availableIBOBFlightSegment, HRTAvailableFlightSearchDTO availableFlightSearchDTO)
			throws ModuleException {

		return availableFlightsReturnAvailabilitySearch(availableIBOBFlightSegment, availableFlightSearchDTO);
	}

	private List<FilteredBucketsDTO> filterBucketsInFlightSegments(Collection<AvailableFlightSegment> availableFlightSegments,
			AvailableFlightSearchDTO availableFlightSearchDTO, String ondCode) throws ModuleException {
		List<FilteredBucketsDTO> flightWiseFilteredBucketsDTOs = new ArrayList<FilteredBucketsDTO>();

		for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
			if (availableFlightSegment.getOndFilteredBucketsDTOsForConnections(ondCode) == null) {
				BucketFilteringCriteria bucketFilteringCriteria = new BucketFilteringCriteria(
						availableFlightSegment.getFlightSegmentDTO(), availableFlightSearchDTO, ondCode,
						availableFlightSearchDTO.getExternalBookingClasses(), availableFlightSearchDTO
								.getPaxTypewiseFareOndChargeInfo(availableFlightSegment.getOndSequence(), ondCode),
						availableFlightSegment.getOndSequence());
				// filters buckets that matches the criteria
				availableFlightSegment.addOndWiseFilteredBucketsDTOsForConnections(ondCode,
						getSeatAvailabilityDAO().fileterBucketsForFlightSegment(bucketFilteringCriteria, availableFlightSegment));
				if (availableFlightSegment.isWaitListed()) {
					injectWaitListedStatusToSegment(availableFlightSegment);
				}
			}
			flightWiseFilteredBucketsDTOs.add(availableFlightSegment.getOndFilteredBucketsDTOsForConnections(ondCode));
		}

		return flightWiseFilteredBucketsDTOs;
	}

	/**
	 * Returns the fare id(s)
	 * 
	 * @param colOndFareDTO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<Integer> getFareIds(Collection<OndFareDTO> colOndFareDTO) throws ModuleException {
		Collection<Integer> colFareIds = new HashSet<Integer>();
		for (OndFareDTO ondFareDTO : colOndFareDTO) {
			colFareIds.add(ondFareDTO.getFareId());
		}
		return colFareIds;
	}

	/**
	 * Compose the infant quote
	 * 
	 * @param colOndFareDTO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<OndFareDTO> composeInfantQuote(Collection<OndFareDTO> colOndFareDTO, Map<Integer, FareTO> mapFareTO,
			String strStationCode, String agentCode) throws ModuleException {
		Collection<QuoteChargesCriteria> criteria = new ArrayList<QuoteChargesCriteria>();
		FareSummaryDTO fareSummaryDTO;
		FareTO fareTO;

		// Populate fare information into OndFareDTO
		for (OndFareDTO ondFareDTO : colOndFareDTO) {
			if (mapFareTO != null) {
				fareTO = mapFareTO.get(ondFareDTO.getFareId());
				// Building FareSummaryDTO
				fareSummaryDTO = new FareSummaryDTO(fareTO);
				if (ondFareDTO.getFareSummaryDTO() != null && ondFareDTO.getFareSummaryDTO().getFarePercentage() != null
						&& ondFareDTO.getFareSummaryDTO().getFarePercentage() > 0) {
					fareSummaryDTO.setInfantFare(AccelAeroCalculator.calculatePercentage(fareSummaryDTO.getInfantFare(),
							ondFareDTO.getFareSummaryDTO().getFarePercentage()).doubleValue());
				}
				// Add Fares to OND Fare
				ondFareDTO.setFareSummaryDTO(fareSummaryDTO);
			}

			// replace open return BC with originally fare quoted BC otherwise charges for the open return segment want
			// get updated
			if (ondFareDTO.getSegmentSeatDistsDTO() != null && ondFareDTO.getSegmentSeatDistsDTO().size() > 0) {
				for (SegmentSeatDistsDTO segmentSeatDistsDTO : ondFareDTO.getSegmentSeatDistsDTO()) {
					if (segmentSeatDistsDTO.getSeatDistribution() != null) {
						for (SeatDistribution seatDistribution : segmentSeatDistsDTO.getSeatDistribution()) {

							if (BookingClass.BookingClassType.OPEN_RETURN.equals(seatDistribution.getBookingClassType())) {
								injectOpenRetunQuotedBC(ondFareDTO.getFareId(), seatDistribution);
							}

						}
					}
				}
			}
		}

		// Identify fare for the complete journey
		Map<String, Double> totalJourneyFareMap = new HashMap<String, Double>();
		Double adultTotJnyFare = 0d;
		Double childTotJnyFare = 0d;
		Double infantTotJnyFare = 0d;

		Map<Integer, FlightSegmentDTO> allFlightSegMap = new HashMap<Integer, FlightSegmentDTO>();
		for (OndFareDTO ondFareDTO : colOndFareDTO) {
			adultTotJnyFare += ondFareDTO.getFareSummaryDTO().getFareAmount(PaxTypeTO.ADULT);
			childTotJnyFare += ondFareDTO.getFareSummaryDTO().getFareAmount(PaxTypeTO.CHILD);
			infantTotJnyFare += ondFareDTO.getFareSummaryDTO().getFareAmount(PaxTypeTO.INFANT);
			if (ondFareDTO.getSegmentsMap() != null && !ondFareDTO.getSegmentsMap().isEmpty()) {
				allFlightSegMap.putAll(ondFareDTO.getSegmentsMap());
			}
		}
		totalJourneyFareMap.put(PaxTypeTO.ADULT, adultTotJnyFare);
		totalJourneyFareMap.put(PaxTypeTO.CHILD, childTotJnyFare);
		totalJourneyFareMap.put(PaxTypeTO.INFANT, infantTotJnyFare);

		Set<String> bookingClasses = new HashSet<String>();
		Set<String> busBookingClasses = new HashSet<String>();
		for (OndFareDTO ondFareDTO : colOndFareDTO) {
			bookingClasses.add(ondFareDTO.getBCForChargeQuote());
			boolean isBusSegment = AirInventoryModuleUtils.getAirportBD().isBusSegment(ondFareDTO.getOndCode());
			if (isBusSegment) {
				busBookingClasses.add(ondFareDTO.getBCForChargeQuote());
			}
		}
		Map<String, Collection<String>> bookingClassWiseChargeGroupCodes = getApplicableChargeGroups(bookingClasses,
				busBookingClasses);

		// Regenerate flight segments from ondFareDTOs
		List<FlightSegmentDTO> segmentsList = new ArrayList<FlightSegmentDTO>();
		Map<String, Collection<String>> bcChgGroupMap = new HashMap<String, Collection<String>>();
		Map<Integer, List<FlightSegmentDTO>> ondWiseFlightSegList = new HashMap<Integer, List<FlightSegmentDTO>>();
		int lastOnd = -1;
		for (OndFareDTO ondFareDTO : colOndFareDTO) {
			segmentsList.addAll(ondFareDTO.getSegmentsMap().values());

			if (!bcChgGroupMap.containsKey(ondFareDTO.getBCForChargeQuote())) {
				Collection<String> chargeGroups = bookingClassWiseChargeGroupCodes.get(ondFareDTO.getBCForChargeQuote());
				bcChgGroupMap.put(ondFareDTO.getBCForChargeQuote(), chargeGroups);
			}
			if (lastOnd == -1 || lastOnd < ondFareDTO.getOndSequence()) {
				lastOnd = ondFareDTO.getOndSequence();
			}
			for (FlightSegmentDTO fltSeg : ondFareDTO.getSegmentsMap().values()) {

				if (allFlightSegMap.containsKey(fltSeg.getSegmentId())) {
					if (!ondWiseFlightSegList.containsKey(ondFareDTO.getOndSequence())) {
						ondWiseFlightSegList.put(ondFareDTO.getOndSequence(), new ArrayList<FlightSegmentDTO>());
					}
					ondWiseFlightSegList.get(ondFareDTO.getOndSequence()).add(allFlightSegMap.get(fltSeg.getSegmentId()));
				}
			}
		}
		Collections.sort(segmentsList);
		// Date firstDepature = segmentsList.get(0).getDepartureDateTimeZulu();
		// Date lastArrival = segmentsList.get(segmentsList.size() - 1).getArrivalDateTimeZulu();

		// Build charge quote criteria
		for (OndFareDTO ondFareDTO : colOndFareDTO) {

			List<FlightSegmentDTO> ondFlightList = new ArrayList<FlightSegmentDTO>();
			if (ondFareDTO.getFareType() == FareTypes.PER_FLIGHT_FARE) {
				ondFlightList.addAll(ondFareDTO.getSegmentsMap().values());
			} else {
				ondFlightList = ondWiseFlightSegList.get(ondFareDTO.getOndSequence());
			}
			TransitInfoDTO transitInfoDTO = getTransitInfoDTO(ondFlightList, null);
			List<FlightSegmentDTO> ondSegmentsList = new ArrayList<FlightSegmentDTO>();
			ondSegmentsList.addAll(ondFareDTO.getSegmentsMap().values());
			Collections.sort(ondSegmentsList);

			Set<String> transitAPList = new HashSet<String>();
			Set<String> excludeAPlist = new HashSet<String>();
			List<Long> transitDurList = new ArrayList<Long>();

			for (FlightSegmentDTO fltSeg : ondSegmentsList) {
				if (transitInfoDTO.getTransitAirports(fltSeg.getDepartureDateTimeZulu()) != null) {
					transitAPList.addAll(transitInfoDTO.getTransitAirports(fltSeg.getDepartureDateTimeZulu()));
				}
				if (transitInfoDTO.getDepArrExcludeAirports(fltSeg.getDepartureAirportCode(),
						fltSeg.getArrivalAirportCode()) != null) {
					excludeAPlist.addAll(transitInfoDTO.getDepArrExcludeAirports(fltSeg.getDepartureAirportCode(),
							fltSeg.getArrivalAirportCode()));
				}
				if (transitInfoDTO.getTransitDurationList(fltSeg.getDepartureDateTimeZulu()) != null) {
					transitDurList.addAll(transitInfoDTO.getTransitDurationList(fltSeg.getDepartureDateTimeZulu()));
				}
			}
			// getOndPosForAddInf(ondSegmentsList.get(0).getDepartureDateTimeZulu(), firstDepature,
			// lastArrival, ondSegmentsList.get(ondSegmentsList.size() - 1).getArrivalDateTimeZulu())
			String ondPosition = getONDPosition((ondFareDTO.getOndSequence() == 0), (ondFareDTO.getOndSequence() == lastOnd),
					null);

			QuoteChargesCriteria criterion = ondFareDTO.createCriteriaForChargeQuoting(strStationCode, true);
			criterion.setOndPosition(ondPosition);
			criterion.setOndTransitDurList(transitDurList);
			criterion.setTransitAiports(transitAPList, excludeAPlist);
			criterion.setAgentCode(agentCode);
			criteria.add(criterion);
		}

		QuotedONDChargesDTO quotedONDChargesDTO = getChargeBD().quoteONDCharges(criteria);
		return quotedONDChargesDTO.setInfantCharges(colOndFareDTO, bcChgGroupMap);
	}

	/**
	 * Is Any non fare infant applicability exist
	 * 
	 * @param colFareQuoteOfOndFareDTO
	 * @return
	 */
	private boolean isAnyNonFareInfantApplicabilityExist(Map<Integer, FareTO> mapFareTO) {
		for (FareTO fareTO : mapFareTO.values()) {
			if (!fareTO.isInfantApplyFare()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the infant fare(s) and charge(s)
	 * 
	 * @param colOndFareDTO
	 * @param strStationCode
	 * @param override
	 * @return
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> getInfantQuote(Collection<OndFareDTO> colOndFareDTO, String strStationCode, boolean override,
			String agentCode) throws ModuleException {
		Collection<Integer> colFareIds = getFareIds(colOndFareDTO);
		Map<Integer, FareTO> mapFareTO = getFareBD().getRefundableStatuses(colFareIds, null, null).getFareTOs();
		boolean isAnyNonFareInfantApplicabilityExist = isAnyNonFareInfantApplicabilityExist(mapFareTO);

		if (isAnyNonFareInfantApplicabilityExist) {
			if (override) {
				return composeInfantQuote(colOndFareDTO, null, strStationCode, agentCode);
			}
			throw new ModuleException("airinventory.logic.bl.fare.invalid");
		} else {
			return composeInfantQuote(colOndFareDTO, mapFareTO, strStationCode, agentCode);
		}
	}

	/**
	 * Retrieve overall seat availability, unsold fixed quota for agent, unsold fixed for other agents/non-agents
	 * 
	 * @param flightId
	 * @param segmentCode
	 * @param logicalCabinClass
	 * @return int [] element 0 - overall availability element 1 - available fixed quota for any agent element 2 -
	 *         available infant seats 3 - Allocated seats for the flight segment. element 4 - Oversell Seats element 5 -
	 *         Curtailed Seats element 6 - Sold Set element 7 - On hold seats
	 */
	public List<Integer> getAvailableNonFixedAndFixedSeatsCounts(int flightId, String segmentCode, String cabinClass,
			String logicalCabinClass) throws ModuleException {
		return getSeatAvailabilityDAO()
				.getAvailableNonFixedAndFixedSeatsCounts(flightId, segmentCode, cabinClass, logicalCabinClass, null)
				.get(logicalCabinClass);
	}

	// ////// Flexi Quoting
	// private void singleFlightsReturnFlexiQuote(SelectedFlightDTO selectedFlightDTO, AvailableFlightSearchDTO
	// restrictionsDTO)
	// throws ModuleException {
	//
	// if (restrictionsDTO.isOutboundFlexiQuote()) {
	// singleFlightFlexiQuote((AvailableFlightSegment) selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND),
	// restrictionsDTO);
	// }
	// if (restrictionsDTO.isInboundFlexiQuote()) {
	// singleFlightFlexiQuote((AvailableFlightSegment) selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND),
	// restrictionsDTO);
	// }
	// }
	//
	// private void connectedFlightsReturnFlexiQuote(SelectedFlightDTO selectedFlightDTO, AvailableFlightSearchDTO
	// restrictionsDTO)
	// throws ModuleException {
	//
	// if (restrictionsDTO.isOutboundFlexiQuote()) {
	// connectedFlightFlexiQuote((AvailableConnectedFlight)
	// selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND),
	// restrictionsDTO);
	// }
	// if (restrictionsDTO.isInboundFlexiQuote()) {
	// connectedFlightFlexiQuote((AvailableConnectedFlight)
	// selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND),
	// restrictionsDTO);
	// }
	// }

	// private void singleConnectedFlightsReturnFlexiQuote(SelectedFlightDTO selectedFlightDTO,
	// AvailableFlightSearchDTO restrictionsDTO) throws ModuleException {
	//
	// if (restrictionsDTO.isOutboundFlexiQuote()) {
	// singleFlightFlexiQuote((AvailableFlightSegment) selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND),
	// restrictionsDTO);
	// }
	//
	// if (restrictionsDTO.isInboundFlexiQuote()) {
	// connectedFlightFlexiQuote((AvailableConnectedFlight)
	// selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND),
	// restrictionsDTO);
	// }
	// }
	//
	// private void connectedSingleFlightsReturnFlexiQuote(SelectedFlightDTO selectedFlightDTO,
	// AvailableFlightSearchDTO restrictionsDTO) throws ModuleException {
	//
	// if (restrictionsDTO.isOutboundFlexiQuote()) {
	// connectedFlightFlexiQuote((AvailableConnectedFlight)
	// selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND),
	// restrictionsDTO);
	// }
	//
	// if (restrictionsDTO.isInboundFlexiQuote()) {
	// singleFlightFlexiQuote((AvailableFlightSegment) selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND),
	// restrictionsDTO);
	// }
	// }

	/**
	 * Flexi Charge Quoting for single flight segment Flight segment code is taken as the OND code for flexi charge
	 * quoting
	 * 
	 * @param outboundSingleFlight
	 * @param restrictionsDTO
	 * @throws ModuleException
	 */
	private void singleFlightFlexiQuote(AvailableFlightSegment availableFlightSegment, AvailableFlightSearchDTO restrictionsDTO)
			throws ModuleException {
		/*
		 * proration of flexi charge calculated based on this flag in SelectedFlightDTO.setEffectiveFlexiCharges()
		 */
		if (restrictionsDTO.isHalfReturnFareQuote() && restrictionsDTO.isInboundFareQuote()) {
			availableFlightSegment.setInboundFlightSegment(true);
		}

		if (availableFlightSegment.getAvailableLogicalCabinClasses() != null) {
			for (String availableLogicalCabinClass : availableFlightSegment.getAvailableLogicalCabinClasses()) {
				if (availableFlightSegment.isSeatsAvailable(availableLogicalCabinClass)
						&& availableFlightSegment.getFare(availableLogicalCabinClass) != null) {
					Collection<FlexiQuoteCriteria> criteriaCollection = new ArrayList<FlexiQuoteCriteria>();
					criteriaCollection.add(createFlexiQuoteCriteria(availableFlightSegment.getFare(availableLogicalCabinClass),
							availableFlightSegment.getFlightSegmentDTO().getDepartureDateTime(),
							availableFlightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu(),
							availableFlightSegment.getFlightSegmentDTO().getSegmentCode(),
							availableFlightSegment.isServiceIncludedInBundledFare(EXTERNAL_CHARGES.FLEXI_CHARGES.toString())));
					HashMap<String, HashMap<String, FlexiRuleDTO>> charges = getAirPricingBD()
							.quoteFlexiCharges(criteriaCollection);
					HashMap<String, FlexiRuleDTO> ondCharges = charges
							.get(availableFlightSegment.getFlightSegmentDTO().getSegmentCode());
					Collection<FlexiRuleDTO> flexiChargeCollection = AirinventoryUtils.getFlexiRuleDTOList(ondCharges);
					availableFlightSegment.setFlexiCharges(availableLogicalCabinClass, flexiChargeCollection);
				}
			}
		}

	}

	/**
	 * Flexi Charge Quoting for connected flight
	 * 
	 * @param connectedFlights
	 * @param restrictionsDTO
	 * @param contextOndFltInfo
	 * @throws ModuleException
	 */
	private void connectedFlightFlexiQuote(AvailableConnectedFlight connectedFlight, AvailableFlightSearchDTO restrictionsDTO)
			throws ModuleException {

		/*
		 * proration of flexi charge calculated based on this flag in SelectedFlightDTO.setEffectiveFlexiCharges()
		 */
		// FIXME DILAN
		if (restrictionsDTO.isHalfReturnFareQuote() && restrictionsDTO.isInboundFareQuote()) {
			for (Object element : connectedFlight.getAvailableFlightSegments()) {
				AvailableFlightSegment fltSeg = (AvailableFlightSegment) element;
				fltSeg.setInboundFlightSegment(true);
			}
		}

		if (connectedFlight.getAvailableLogicalCabinClasses() != null) {
			for (String availableLogicalCabinClass : connectedFlight.getAvailableLogicalCabinClasses()) {
				Integer fareType = connectedFlight.getFareType(availableLogicalCabinClass);

				if (fareType == FareTypes.HALF_RETURN_FARE || fareType == FareTypes.OND_FARE) {

					if (connectedFlight.isSeatsAvailable(availableLogicalCabinClass)
							&& connectedFlight.getFare(availableLogicalCabinClass) != null) {

						AvailableFlightSegment firstOutFltSeg = connectedFlight.getAvailableFlightSegments().iterator().next();
						Collection<FlexiQuoteCriteria> criteriaCollection = new ArrayList<FlexiQuoteCriteria>();
						criteriaCollection.add(createFlexiQuoteCriteria(connectedFlight.getFare(availableLogicalCabinClass),
								firstOutFltSeg.getFlightSegmentDTO().getDepartureDateTime(),
								firstOutFltSeg.getFlightSegmentDTO().getDepartureDateTimeZulu(), connectedFlight.getOndCode(),
								connectedFlight.isServiceIncludedInBundledFare(EXTERNAL_CHARGES.FLEXI_CHARGES.toString())));
						HashMap<String, HashMap<String, FlexiRuleDTO>> charges = getAirPricingBD()
								.quoteFlexiCharges(criteriaCollection);
						connectedFlight.setFlexiCharges(availableLogicalCabinClass,
								AirinventoryUtils.getFlexiRuleDTOList(charges.get(connectedFlight.getOndCode())));
					}

				} else if (fareType == FareTypes.PER_FLIGHT_FARE || fareType == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE
						|| fareType == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE) {

					if (connectedFlight.isSeatsAvailable(availableLogicalCabinClass)) {
						Collection<FlexiQuoteCriteria> criteriaCollection = new ArrayList<FlexiQuoteCriteria>();
						Collection<AvailableFlightSegment> allFlightSegments = new ArrayList<AvailableFlightSegment>();

						for (AvailableFlightSegment fltSeg : connectedFlight.getAvailableFlightSegments()) {
							boolean isFreeFlexiIncluded = false;
							if (!fltSeg.isBusSegment() && connectedFlight
									.isServiceIncludedInBundledFare(EXTERNAL_CHARGES.FLEXI_CHARGES.toString())) {
								isFreeFlexiIncluded = true;
							}
							if (fltSeg.getFare(availableLogicalCabinClass) != null) {
								criteriaCollection.add(createFlexiQuoteCriteria(fltSeg.getFare(availableLogicalCabinClass),
										fltSeg.getFlightSegmentDTO().getDepartureDateTime(),
										fltSeg.getFlightSegmentDTO().getDepartureDateTimeZulu(),
										fltSeg.getFlightSegmentDTO().getSegmentCode(), isFreeFlexiIncluded));
								allFlightSegments.add(fltSeg);
							}
						}
						HashMap<String, HashMap<String, FlexiRuleDTO>> charges = getAirPricingBD()
								.quoteFlexiCharges(criteriaCollection);
						for (AvailableFlightSegment fltSeg : allFlightSegments) {
							fltSeg.setFlexiCharges(availableLogicalCabinClass, AirinventoryUtils
									.getFlexiRuleDTOList(charges.get(fltSeg.getFlightSegmentDTO().getSegmentCode())));
						}
					}

				}
			}
		}

	}

	private FlexiQuoteCriteria createFlexiQuoteCriteria(FareSummaryDTO fareSummaryDTO, Date departureDate, Date departureZuluDate,
			String ondCode, boolean offerFlexiFreeOfCharge) {
		FlexiQuoteCriteria criterion = new FlexiQuoteCriteria(fareSummaryDTO, departureDate, departureZuluDate, ondCode,
				offerFlexiFreeOfCharge);
		return criterion;
	}

	private TransitInfoDTO getTransitInfoDTO(Collection<FlightSegmentDTO> quotedFltSegmentDTOs,
			FlightSegmentDTO connectingFltSegment) {

		TransitInfoDTO transitInfoDTO = new TransitInfoDTO();

		if (quotedFltSegmentDTOs != null && quotedFltSegmentDTOs.size() > 0) { // Connection
			Date previousSegmentArrivalTimeZulu = null;
			for (FlightSegmentDTO flightSegmentDTO : quotedFltSegmentDTOs) {
				long transitGap = -1;
				if (previousSegmentArrivalTimeZulu == null && connectingFltSegment != null) {
					Set<String> hubAirports = CommonsServices.getGlobalConfig().getHubAirports();
					String currentStartAirport = SegmentUtil.getFromAirport(flightSegmentDTO.getSegmentCode());
					String connectingEndAirport = SegmentUtil.getToAirport(connectingFltSegment.getSegmentCode());
					// if connecting segment make a connection with the current segment, and aipport is a hub
					if (currentStartAirport.equals(connectingEndAirport) && hubAirports.contains(currentStartAirport)) {
						transitGap = flightSegmentDTO.getDepartureDateTimeZulu().getTime()
								- connectingFltSegment.getArrivalDateTimeZulu().getTime();

					}

				} else if (previousSegmentArrivalTimeZulu != null) {
					transitGap = flightSegmentDTO.getDepartureDateTimeZulu().getTime() - previousSegmentArrivalTimeZulu.getTime();
				}
				if (transitGap > 0) {
					transitInfoDTO.addTransit(flightSegmentDTO.getDepartureDateTimeZulu(),
							flightSegmentDTO.getDepartureAirportCode(), transitGap);
					if (transitGap < AppSysParamsUtil.getTransitTimeThresholdInMillis()) {
						transitInfoDTO.setQualifyForShortTransit(flightSegmentDTO.getDepartureDateTimeZulu(), true);
					}
				}
				previousSegmentArrivalTimeZulu = flightSegmentDTO.getArrivalDateTimeZulu();
			}
			/*
			 * In own airline reservation modification , new adding segments should be check whether satisfy inward or
			 * outward.Here it is not checked.
			 */

			/*
			 * Reservation modification (Modify Segment or Add Segment)
			 */
		}

		return transitInfoDTO;
	}

	BookingClass getOpenReturnBookingClass(String logicalCabinClassCode) {
		return getBookingClassDAO().getOpenReturnBookingClass(logicalCabinClassCode);
	}

	public Map<Integer, List<Pair<String, Integer>>> getBcAvailabilityForMyIDTravel(List<Integer> flightSegIds,
			String agentCode) {
		return getSeatAvailabilityDAO().getBcAvailabilityForMyIDTravel(flightSegIds, agentCode);
	}

	private SeatAvailabilityDAOJDBC getSeatAvailabilityDAO() {
		return (SeatAvailabilityDAOJDBC) AirInventoryUtil.getInstance().getLocalBean("seatAvailabilityDAOJDBC");
	}

	private FareBD getFareBD() {
		return AirInventoryModuleUtils.getFareBD();
	}

	private FlightBD getFlightBD() {
		return AirInventoryModuleUtils.getFlightBD();
	}

	private ChargeBD getChargeBD() {
		return AirInventoryModuleUtils.getChargeBD();
	}

	private AirPricingBD getAirPricingBD() {
		return AirInventoryModuleUtils.getAirPricingBD();
	}

	private BookingClassDAO getBookingClassDAO() {
		return (BookingClassDAO) AirInventoryUtil.getInstance().getLocalBean("BookingClassDAOImplProxy");
	}

	private BookingClassJDBCDAO getBookingClassJDBCDAO() {
		return (BookingClassJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("bookingClassJdbcDAO");
	}

	private LogicalCabinClassDAO getLogicalCabinClassDAO() {
		return (LogicalCabinClassDAO) AirInventoryUtil.getInstance().getLocalBean("LogicalCabinClassDAOImplProxy");
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		FlightInventoryDAO flightInventoryDAO = (FlightInventoryDAO) AirInventoryUtil.getInstance()
				.getLocalBean("FlightInventoryDAOImplProxy");
		return flightInventoryDAO;
	}

	private void setSelectedAgent(AvailableFlightSearchDTO restrictionsDTO) {
		if (restrictionsDTO.getSelectedAgentCode() != null && !restrictionsDTO.getSelectedAgentCode().equals("")) {
			restrictionsDTO.setAgentCode(restrictionsDTO.getSelectedAgentCode());
		}
	}

	/**
	 * Inject open return booking class for the inbound segments
	 * 
	 * @param selectedFlightDTO
	 * @param restrictionsDTO
	 * @return
	 */
	private SelectedFlightDTO injectOpenReturnBookingClass(SelectedFlightDTO selectedFlightDTO) {

		if (selectedFlightDTO != null) {
			AvailableIBOBFlightSegment inboundFlight = selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND);

			if (inboundFlight != null) {
				if (inboundFlight.isDirectFlight()) {
					BookingClass openReturnCabinClassWiseBC = getBookingClassDAO()
							.getOpenReturnBookingClass(inboundFlight.getSelectedLogicalCabinClass());
					if (openReturnCabinClassWiseBC != null) {
						AvailableFlightSegment avaFlightSeg = (AvailableFlightSegment) inboundFlight;

						if (avaFlightSeg.getSegmentSeatDistributionMap(inboundFlight.getSelectedLogicalCabinClass()) != null) {
							LinkedHashMap<Integer, SegmentSeatDistsDTO> colSeatDistribution = avaFlightSeg
									.getSegmentSeatDistributionMap(inboundFlight.getSelectedLogicalCabinClass());
							SegmentSeatDistsDTO segmentSeatDistsDTO;
							for (Integer segId : colSeatDistribution.keySet()) {
								segmentSeatDistsDTO = colSeatDistribution.get(segId);
								segmentSeatDistsDTO.setSeatDistributions(cloneSeatDistributionForOpenRT(
										segmentSeatDistsDTO.getSeatDistribution(), openReturnCabinClassWiseBC));
							}

						}
					}
				} else {
					AvailableConnectedFlight inboundConnectedFlight = (AvailableConnectedFlight) inboundFlight;
					Iterator<AvailableFlightSegment> inboundFlightIt = inboundConnectedFlight.getAvailableFlightSegments()
							.iterator();
					while (inboundFlightIt.hasNext()) {
						AvailableFlightSegment availableSingleFlightSegment = inboundFlightIt.next();

						BookingClass openReturnCabinClassWiseBC = getBookingClassDAO()
								.getOpenReturnBookingClass(inboundFlight.getSelectedLogicalCabinClass());
						if (openReturnCabinClassWiseBC != null) {
							if (availableSingleFlightSegment
									.getSeatsAvailability(inboundFlight.getSelectedLogicalCabinClass()) != null) {
								Collection<SeatDistribution> colSeatDistribution = availableSingleFlightSegment
										.getSeatsAvailability(inboundFlight.getSelectedLogicalCabinClass());
								availableSingleFlightSegment.setSeatsAvailability(inboundFlight.getSelectedLogicalCabinClass(),
										cloneSeatDistributionForOpenRT(colSeatDistribution, openReturnCabinClassWiseBC));
							}
						}
					}
				}
			}
		}

		return selectedFlightDTO;
	}

	private Collection<SeatDistribution> cloneSeatDistributionForOpenRT(Collection<SeatDistribution> colSeatDistribution,
			BookingClass openReturnCabinClassWiseBC) {
		Collection<SeatDistribution> colClonedSeatDistribution = new ArrayList<SeatDistribution>();
		for (SeatDistribution seatDistribution : colSeatDistribution) {
			seatDistribution = seatDistribution.clone();
			seatDistribution.setBookingClassCode(openReturnCabinClassWiseBC.getBookingCode());
			seatDistribution.setBookingClassType(openReturnCabinClassWiseBC.getBcType());

			colClonedSeatDistribution.add(seatDistribution);

		}
		return colClonedSeatDistribution;
	}

	private void injectWaitListedStatusToSegment(AvailableFlightSegment flightSegment) {
		for (FlightSegmentDTO fsdto : flightSegment.getFlightSegmentDTOs()) {
			fsdto.setWaitListed(true);
		}
	}

	private boolean isOnDDuplicated(List<OndFareDTO> allOndchangedFaresSeatsSegmentsDTOs) throws ModuleException {
		ArrayList<String> ondList = new ArrayList<String>();
		for (OndFareDTO ondFareDTO : allOndchangedFaresSeatsSegmentsDTOs) {

			if (!StringUtil.isNullOrEmpty(ondFareDTO.getOndCode())) {

				if (ondList.contains(ondFareDTO.getOndCode())) {
					return true;
				}

				ondList.add(ondFareDTO.getOndCode());
			}

		}

		return false;
	}

	private void injectOpenRetunQuotedBC(int fareId, SeatDistribution seatDistribution) throws ModuleException {

		String bookingCode = null;

		if (fareId > 0 && seatDistribution != null) {
			FareDTO fareDTO = getFareBD().getFare(fareId);

			if (fareDTO != null && fareDTO.getFare() != null) {
				bookingCode = fareDTO.getFare().getBookingCode();
				if (bookingCode != null
						&& BookingClass.BookingClassType.OPEN_RETURN.equals(seatDistribution.getBookingClassType())) {
					seatDistribution.setBookingClassCode(bookingCode);
				}
			}
		}
	}

	private List<FareCalType> getSystemSupportedFareTypes(boolean isModifyBooking, boolean isOpenReturn, boolean isOpenJaw) {

		List<FareCalType> systemSupportedFareTypes = new ArrayList<FareCalType>();

		for (String fareType : AppSysParamsUtil.enabledFareTypes()) {
			if (FareCalType.SEG.toString().equals(fareType)) {
				systemSupportedFareTypes.add(FareCalType.SEG);
			} else if (FareCalType.CON.toString().equals(fareType)) {
				systemSupportedFareTypes.add(FareCalType.CON);
			} else if (FareCalType.RT.toString().equals(fareType)) {
				systemSupportedFareTypes.add(FareCalType.RT);
			} else if (FareCalType.HRT.toString().equals(fareType)) {
				if (AppSysParamsUtil.isAllowHalfReturnFaresForModification() && isModifyBooking && !isOpenJaw) {
					systemSupportedFareTypes.add(FareCalType.HRT);
				} else if (!isOpenJaw && !isOpenReturn && AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings()) {
					systemSupportedFareTypes.add(FareCalType.HRT);
				} else if (!isOpenJaw && isOpenReturn && AppSysParamsUtil.isAllowHalfReturnFaresForOpenReturnBookings()) {
					systemSupportedFareTypes.add(FareCalType.HRT);
				} else if (isOpenJaw && AppSysParamsUtil.isAllowHalfReturnFaresForOpenJawJourney()) {
					systemSupportedFareTypes.add(FareCalType.HRT);
				}
			}
		}
		return systemSupportedFareTypes;
	}

	// following function returns exact OND position from complete OND list
	private int getExistingOndSequence(Map<Integer, List<Integer>> fltSegByOndSequence, List<Integer> flightSegmentIds,
			int currentSeq) {

		for (Integer seq : fltSegByOndSequence.keySet()) {
			List<Integer> fltSegList = fltSegByOndSequence.get(seq);
			if (fltSegList.containsAll(flightSegmentIds)) {
				return seq;
			}
		}

		return currentSeq;
	}

	String getTotalJourneyONDPosition(AvailableFlightSearchDTO restrictionsDTO, List<FlightSegmentDTO> flightSegmentDTOList) {

		boolean isFirstOnd = false;
		boolean isLastOnd = false;
		String localOndPosition = null;

		Map<List<Integer>, Integer> totalJourneyOrderedOndSEQMap = restrictionsDTO.getTotalJourneyOrderedOndSEQMap();
		if (AppSysParamsUtil.isSubJourneyDetectionEnabled() && restrictionsDTO.isSubJourney()
				&& totalJourneyOrderedOndSEQMap != null && totalJourneyOrderedOndSEQMap.size() > 0 && flightSegmentDTOList != null
				&& flightSegmentDTOList.size() > 0) {

			if (flightSegmentDTOList != null && flightSegmentDTOList.size() > 0) {
				Iterator<FlightSegmentDTO> flightSegmentDTOsItr = flightSegmentDTOList.iterator();

				while (flightSegmentDTOsItr.hasNext()) {
					FlightSegmentDTO flightSegmentDTO = flightSegmentDTOsItr.next();
					for (List<Integer> fltSegIdList : totalJourneyOrderedOndSEQMap.keySet()) {
						int ondSequence = totalJourneyOrderedOndSEQMap.get(fltSegIdList);
						if (fltSegIdList.contains(flightSegmentDTO.getSegmentId())) {
							if (ondSequence == 0) {
								isFirstOnd = true;
							}

							if (ondSequence == (totalJourneyOrderedOndSEQMap.size() - 1)) {
								isLastOnd = true;
							}

							if (isFirstOnd && isLastOnd) {
								localOndPosition = QuoteChargesCriteria.FIRST_AND_LAST_OND;
							} else if (isFirstOnd) {
								localOndPosition = QuoteChargesCriteria.FIRST_OND;
							} else if (isLastOnd) {
								localOndPosition = QuoteChargesCriteria.LAST_OND;
							} else {
								localOndPosition = QuoteChargesCriteria.INBETWEEN_OND;
							}

						}
					}
				}
			}

		}
		return localOndPosition;
	}

	public boolean isValidConnectionRoute(String segmentCode) {
		return getSeatAvailabilityDAO().isValidConnectionRoute(segmentCode);
	}

	private BundledFareBL getBundledFareBL() {
		if (bundledFareBL == null) {
			bundledFareBL = new BundledFareBL();
		}

		return bundledFareBL;
	}

	private boolean isSplitOperationForBusInvoked(Collection<OndFareDTO> ondFares) {

		boolean splitOperationInvoked = false;
		for (OndFareDTO fare : ondFares) {
			if (fare.isSplitOperationForBusInvoked()) {
				splitOperationInvoked = fare.isSplitOperationForBusInvoked();
			}
		}
		return splitOperationInvoked;
	}

	private boolean isMixFareTypesDetectedInConnection(OndFareDTO ondFareDTO,
			Map<Integer, List<Integer>> ondWiseExistingFareType) {
		boolean isInvalidFareTypes = true;
		if (ondWiseExistingFareType != null && !ondWiseExistingFareType.isEmpty()
				&& ondWiseExistingFareType.containsKey(ondFareDTO.getOndSequence())) {

			List<Integer> fareTypes = ondWiseExistingFareType.get(ondFareDTO.getOndSequence());
			if (fareTypes != null && !fareTypes.isEmpty()) {
				isInvalidFareTypes = false;
				for (int fareType : fareTypes) {
					if (fareType != ondFareDTO.getFareType()) {
						return true;
					}
				}
			}

		}
		return isInvalidFareTypes;
	}
}
