package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import com.isa.thinair.airinventory.api.dto.inventory.LogicalCabinClassDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.LogicalCabinClassDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * 
 * @author manoji
 * @isa.module.dao-impl dao-name="LogicalCabinClassDAOImpl"
 */
public class LogicalCabinClassDAOImpl extends PlatformBaseHibernateDaoSupport implements LogicalCabinClassDAO {

	@Override
	public Page<LogicalCabinClass> getLogicalCabinClass(LogicalCabinClassDTO searchParams, int startRec, int noRecs) {
		String s[] = constructLogicalCabinClassHql(searchParams);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		@SuppressWarnings("unchecked")
		Collection<LogicalCabinClass> colList = qResults.list();
		return new Page<LogicalCabinClass>(count, startRec, startRec + noRecs - 1, colList);
	}

	private String[] constructLogicalCabinClassHql(LogicalCabinClassDTO searchParams) {
		String completeHql = null;
		String completeHqlCount = null;

		String hql = " Select logicalCabinClass from LogicalCabinClass logicalCabinClass ";
		String hqlCount = " Select count(*) from LogicalCabinClass logicalCabinClass ";
		String hqlWhere = " where 1=1 ";
		if (searchParams != null) {
			if (!searchParams.getStatus().equals("All")) {
				hqlWhere += "AND logicalCabinClass.status ='" + searchParams.getStatus() + "' ";
			}
			if (!searchParams.getLogicalCCCode().equals("All")) {
				hqlWhere += "AND logicalCabinClass.logicalCCCode  ='" + searchParams.getLogicalCCCode() + "' ";
			}
			if (!searchParams.getCabinClassCode().equals("All")) {
				hqlWhere += "AND logicalCabinClass.cabinClassCode ='" + searchParams.getCabinClassCode() + "' ";
			}
		}
		completeHql = hql + hqlWhere;
		completeHqlCount = hqlCount + hqlWhere;
		return new String[] { completeHql, completeHqlCount };
	}

	@Override
	public void saveTemplate(LogicalCabinClass logicalCabinClass, String chkNestShift) throws ModuleException {
		if (chkNestShift.equals("INA")) {
			String hqlNestRankCount = "Select count(logicalCabinClass.nestRank) from LogicalCabinClass logicalCabinClass "
					+ "where logicalCabinClass.nestRank ='" + logicalCabinClass.getNestRank()
					+ "' and logicalCabinClass.cabinClassCode = '" + logicalCabinClass.getCabinClassCode()
					+ "' and logicalCabinClass.logicalCCCode NOT IN ('" + logicalCabinClass.getLogicalCCCode() + "')";
			Query qCount = getSession().createQuery(hqlNestRankCount);
			int count = ((Long) qCount.uniqueResult()).intValue();
			if (count > 0) {
				throw new CommonsDataAccessException("airinventory.logic.dao.bc.nestrank.duplicate",
						AirinventoryConstants.MODULE_NAME);
			}
		}
		if (("INA").equals(logicalCabinClass.getStatus())) {
			if (!isAllowUpdateLogicalCabinClass(logicalCabinClass.getLogicalCCCode())) {
				throw new CommonsDataAccessException("airinventory.bookingclass.exist", AirinventoryConstants.MODULE_NAME);
			}
		}

		if (chkNestShift.equals("ACT")) {
			String hqlUpdate = "update LogicalCabinClass set nestRank = nestRank + 1 where nestRank >= :oldnestRank "
					+ "and cabinClassCode = :cabinClassCode";
			getSession().createQuery(hqlUpdate).setInteger("oldnestRank", logicalCabinClass.getNestRank())
					.setString("cabinClassCode", logicalCabinClass.getCabinClassCode()).executeUpdate();
		}
		hibernateSaveOrUpdate(logicalCabinClass);
	}

	public boolean isAllowUpdateLogicalCabinClass(String logicalCCCode) {
		String hqlCount = "Select count(*) from BookingClass bookingClass where bookingClass.status = 'ACT' and bookingClass.logicalCCCode = '"
				+ logicalCCCode + "'";
		Query qCount = getSession().createQuery(hqlCount);
		int count = ((Long) qCount.uniqueResult()).intValue();
		if (count > 0) {
			return false;
		}
		return true;
	}

	@Override
	public int deleteLogicalCabinClass(LogicalCabinClass logicalCabinClass) {
		String hqlCount = " Select count(*) from BookingClass bookingClass where bookingClass.logicalCCCode = '"
				+ logicalCabinClass.getLogicalCCCode() + "' AND bookingClass.bcType != '"
				+ BookingClass.BookingClassType.OPEN_RETURN + "'";
		Query qCount = getSession().createQuery(hqlCount);
		int bookingClassCount = ((Long) qCount.uniqueResult()).intValue();
		String logicalCCCode = logicalCabinClass.getLogicalCCCode();
		if (bookingClassCount == 0) {
			String hqlNestRankCount = "Select count(logicalCabinClass.nestRank) from LogicalCabinClass logicalCabinClass where "
					+ "logicalCabinClass.nestRank >=" + logicalCabinClass.getNestRank()
					+ " and logicalCabinClass.cabinClassCode = '" + logicalCabinClass.getCabinClassCode()
					+ "' and logicalCabinClass.logicalCCCode NOT IN ('" + logicalCabinClass.getLogicalCCCode() + "')";
			Query higherLogicalCCCountQuery = getSession().createQuery(hqlNestRankCount);

			if (((Long) higherLogicalCCCountQuery.uniqueResult()).intValue() > 0) {
				String hqlUpdate = "update LogicalCabinClass set nestRank = nestRank - 1 where nestRank >= :oldnestRank "
						+ "and cabinClassCode = :cabinClassCode";
				getSession().createQuery(hqlUpdate).setInteger("oldnestRank", logicalCabinClass.getNestRank())
						.setString("cabinClassCode", logicalCabinClass.getCabinClassCode()).executeUpdate();
			}

			// Remove unused open return logical cabin class
			try {
				BookingClassDAO bookingClassDAO = AirInventoryModuleUtils.getBookingClassDAO();
				BookingClass openRTBookingClass = bookingClassDAO.getOpenReturnBookingClass(logicalCCCode);
				if (openRTBookingClass != null) {
					bookingClassDAO.deleteBookingClass(openRTBookingClass.getBookingCode());
				}
			} catch (Exception e) {
				return -1;
			}

			LogicalCabinClass logicalCC = (LogicalCabinClass) get(LogicalCabinClass.class, logicalCCCode);
			delete(logicalCC);
			return 0;
		} else {
			return -1;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LogicalCabinClass> getLogicalCabinClasses(String cabinClass) {
		String hql = " SELECT LogCC FROM LogicalCabinClass LogCC WHERE LogCC.cabinClassCode=:cabinClass";
		return getSession().createQuery(hql).setString("cabinClass", cabinClass).list();
	}

	@Override
	public String getDefaultLogicalCabinClass(String cabinClass) {
		String hql = " SELECT LogCC.logicalCCCode FROM LogicalCabinClass LogCC WHERE LogCC.cabinClassCode=:cabinClass order by LogCC.nestRank";
		Query query = getSession().createQuery(hql);

		query.setFirstResult(0);
		query.setMaxResults(1);

		@SuppressWarnings("unchecked")
		List<String> logicalCCes = query.setString("cabinClass", cabinClass).list();
		if (logicalCCes != null && !logicalCCes.isEmpty()) {
			return logicalCCes.get(0).toString();
		} else {
			return null;
		}
	}

	@Override
	public LogicalCabinClass getLogicalCabinClass(String logicalCCCode) throws ModuleException {
		String hql = " SELECT LogCC FROM LogicalCabinClass LogCC WHERE LogCC.logicalCCCode=:logicalCCCode";
		Query query = getSession().createQuery(hql);

		return (LogicalCabinClass) query.setString("logicalCCCode", logicalCCCode).uniqueResult();
	}

	@Override
	public String getCabinClass(String logicalCCCode) {
		String hql = " SELECT LogCC.cabinClassCode FROM LogicalCabinClass LogCC WHERE LogCC.logicalCCCode=:logicalCCCode AND LogCC.status = 'ACT'";
		Query query = getSession().createQuery(hql);

		String cabinClass = (String) query.setString("logicalCCCode", logicalCCCode).uniqueResult();
		return cabinClass;
	}

	@Override
	public List<LogicalCabinClass> getAllLogicalCabinClasses() throws ModuleException {
		return find(
				"select LogicalCabinClass from LogicalCabinClass as LogicalCabinClass "
						+ "order by LogicalCabinClass.cabinClassCode, LogicalCabinClass.nestRank", LogicalCabinClass.class);
	}
}
