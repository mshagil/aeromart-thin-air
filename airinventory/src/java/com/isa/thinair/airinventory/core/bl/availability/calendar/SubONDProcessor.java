package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airschedules.api.dto.AvailableONDFlight;
import com.isa.thinair.airschedules.api.dto.SegmentInvAvailability;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

class SubONDProcessor {
	private List<String> subONDs;
	private CalendarDateFlightTO calDTO;
	private FareProcessor fareProc;
	private BigDecimal selectedFareAmout;
	private String ondCode;
	private InventoryProcessor invProc;

	public SubONDProcessor(String ondCode, List<String> subONDs, BigDecimal selectedFareAmout, FareProcessor fareProc,
			CalendarDateFlightTO calDTO, InventoryProcessor invProc) {
		this.subONDs = subONDs;
		this.selectedFareAmout = selectedFareAmout;
		this.fareProc = fareProc;
		this.calDTO = calDTO;
		this.ondCode = ondCode;
		this.invProc = invProc;
	}

	public CalendarOndFlight processSubONDs(IPaxCountAssembler paxAssmblr) {

		List<AvailableONDFlight> flights = calDTO.getFlightFor(ondCode, subONDs);
		CalendarOndFlight selectedOndFlight = null;
		for (AvailableONDFlight ondFlight : flights) {
			Map<FlightSegmentTO, LightFareDTO> fareMap = new HashMap<FlightSegmentTO, LightFareDTO>();
			BigDecimal totalFareAmount = null;
			for (String subOND : subONDs) {
				FlightSegmentTO flightSegment = ondFlight.getFlightSegmentFor(subOND);
				SegmentInvAvailability segInv = ondFlight.getSegmentInventoryAvailability(flightSegment);
				for (LightFareDTO fare : fareProc.getOrderedFaresFor(subOND)) {
					if (invProc.hasValidInventory(flightSegment, segInv, fare)) {
						fareMap.put(flightSegment, fare);
						if (totalFareAmount == null) {
							totalFareAmount = fare.getTotalFare(paxAssmblr);
						} else {
							totalFareAmount = AccelAeroCalculator.add(totalFareAmount, fare.getTotalFare(paxAssmblr));
						}
						break;
					}
				}

			}
			if (fareMap.keySet().size() == subONDs.size()) {
				if (totalFareAmount.compareTo(selectedFareAmout) < 0) {
					selectedOndFlight = new CalendarOndFlight(totalFareAmount, ondFlight, fareMap);
				}
				break;
			}
		}
		return selectedOndFlight;
	}
}