package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airinventory.api.service.SeatMapBD;

/**
 * 
 * @author BYORN
 */
@Remote
public interface SeatMapBDImpl extends SeatMapBD {

}
