package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airinventory.api.model.FlightSeat;

public interface FlightSeatDAO {

	public Collection<FlightSeat> getFlightSeats(Collection<Integer> flightAmseatIds);

	public void saveFlightSeats(Collection<FlightSeat> fs);

	public boolean isStatusEqualInInterceptingFccSegIvs(Collection<Integer> fccSegInvs, String status);

	public Collection<FlightSeat> getFlightSeatsWithFccInv(Collection<Integer> fccInv, Collection<Integer> amSeats);

	public Integer getFlightSeatID(Integer flightSegmentId, String seatCode);

	public Integer getFlightAmSeatID(Integer flightSegmentId, String seatCode);
	
	public void deleteFlightSeats(Collection<Integer> flightSegIds);
	
	public void deleteReservationSeats(Collection<Integer> flightAMSeatIds);
}
