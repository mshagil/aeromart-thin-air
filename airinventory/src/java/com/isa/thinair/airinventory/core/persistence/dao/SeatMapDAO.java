/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusRS;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author indika
 * 
 */
public interface SeatMapDAO {

	public Collection<FlightSeat> getFilghtSeats(int flightSegID) throws ModuleException;

	public Collection<FlightSeat> getFilghtSeatData(Collection<Integer> flightSegIDs, Collection<Integer> seatIds)
			throws ModuleException;

	public void saveSeatFares(Collection<FlightSeat> seatFares) throws ModuleException;

	public void deleteSeatFares(Collection<FlightSeat> seatFares) throws ModuleException;

	public void deleteExistingSeatFares(ArrayList<Integer> seatIdList, Integer flightSegId) throws ModuleException;

	public Map<String, FlightSeatStatusTO> getFlightSeatStatusDetails(Integer flightSegID, String seatCode)
			throws ModuleException;

	public FlightSeatStatusRS getFlightSeatStatuses(int flightSegmentId) throws ModuleException;

	public List<FlightSeat> getFlightSeatsInaRowChunck(Integer flightSegID, String cabinClass, String logicalCC, Integer rowGrp,
			Integer colId);

	public Collection<FlightSeat> getSelectedFlightSeats(int flightSegID) throws ModuleException;
	
	public Collection<SeatDTO> getFlightSeatasSeatDTOs(int flightSegID) throws ModuleException;
}