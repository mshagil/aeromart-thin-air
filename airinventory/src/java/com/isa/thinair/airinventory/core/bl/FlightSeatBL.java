package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.FlightSeatDAO;
import com.isa.thinair.airinventory.core.util.TODataExtractor;
import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * Fare Quote/Charge quote routines
 * 
 * @author Byorn
 */
public class FlightSeatBL {

	private static final Log log = LogFactory.getLog(FlightSeatBL.class);

	private FlightSeatDAO seatDAO = null;

	public FlightSeatBL() {
		if (seatDAO == null) {
			this.seatDAO = AirInventoryModuleUtils.getFlightSeatDAO();
		}
	}

	public void updatePaxTypes(Map<Integer, String> flightSeatIdPaxType, Map<Integer, Integer> paxFlightSeatSwapMap)
			throws ModuleException {
		Collection<Integer> flightSeatIds = flightSeatIdPaxType.keySet();
		Collection<FlightSeat> dbflightAmSeats = seatDAO.getFlightSeats(flightSeatIds);
		checkSeatingRules(dbflightAmSeats, flightSeatIdPaxType, paxFlightSeatSwapMap, null);

		if (dbflightAmSeats == null || dbflightAmSeats.size() < 1) {
			throw new ModuleException("airinventory.seats.notfound");
		}

		for (FlightSeat flightSeat : dbflightAmSeats) {
			flightSeat.setPaxType(flightSeatIdPaxType.get(flightSeat.getFlightAmSeatId()));
		}

		seatDAO.saveFlightSeats(dbflightAmSeats);
	}

	@SuppressWarnings("unchecked")
	public void updateFlightSeats(Map<Integer, String> flightSeatIdPaxType, String status, UserPrincipal userPrincipal,
			ReservationAudit reservationAudit) throws ModuleException {

		Collection<Integer> flightSeatIds = flightSeatIdPaxType.keySet();

		Collection<FlightSeat> dbflightAmSeats = seatDAO.getFlightSeats(flightSeatIds);
		if (dbflightAmSeats == null || dbflightAmSeats.size() < 1) {
			log.error("############## [airinventory seats not found for release ]:" + flightSeatIds.iterator().next());
			throw new ModuleException("airinventory.seats.notfound");
		}

		// in reserve seat operation check the seating rule
		if (status.equals(AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED)) {
			checkSeatingRules(dbflightAmSeats, flightSeatIdPaxType, null, null);
		}

		// check if possible to update
		Object[] segAllocsAndStatus = getFccSegInvIds(dbflightAmSeats);
		Collection<Integer> sourceFccSegInvIds = (Collection<Integer>) segAllocsAndStatus[0];
		String existingStatus = (String) segAllocsAndStatus[1];

		Collection<Integer> interceptingInvSegIds = AirInventoryModuleUtils.getFlightInventoryDAO()
				.getInterceptingFCCSegmentInventoryIds(sourceFccSegInvIds, true);

		// Collection<Integer> additionalIntercptingInvids = getAdditionalInterceptingSegInvIds(interceptingInvSegIds,
		// sourceFccSegInvIds);

		// BuUG- FIX adding Seat IDs to take only the relevent Seats to Block
		Collection<Integer> intSelSeat = new HashSet<Integer>();
		for (FlightSeat selSeat : dbflightAmSeats) {
			intSelSeat.add(selSeat.getSeatId());
		}
		// interceptingInvSegIds.addAll(additionalIntercptingInvids);

		if (interceptingInvSegIds != null && interceptingInvSegIds.size() > 0) {
			if (seatDAO.isStatusEqualInInterceptingFccSegIvs(interceptingInvSegIds, existingStatus)) {
				Collection<FlightSeat> interceptingSeats = seatDAO.getFlightSeatsWithFccInv(interceptingInvSegIds, intSelSeat);
				for (FlightSeat flightSeat : interceptingSeats) {
					flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.ACQUIRED);
				}
				dbflightAmSeats.addAll(interceptingSeats);
			}
		}

		List<Integer> vacatedSeatIds = new ArrayList<Integer>();

		for (FlightSeat dbflightSeat : dbflightAmSeats) {
			boolean isVacated = false;

			if (dbflightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE)) {
				// do nothing ?
			} else if (dbflightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.ACQUIRED)) {
				// This needs to make as vaccent if intercepting segs are there
				// If status is RES or BLK keep the title. Otherwise make it null
				if (status.equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)) {
					if (userPrincipal != null) {
						dbflightSeat.setUserDetails(userPrincipal);
					}
					dbflightSeat.setStatus(status);
					dbflightSeat.setTimestamp(new Date());
					dbflightSeat.setPaxType(null);
					isVacated = true;
				}
			} else {
				if (AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED.equals(status)
						&& (!AirinventoryCustomConstants.FlightSeatStatuses.VACANT.equals(dbflightSeat.getStatus()))) {
					throw new ModuleException("airinventory.seat.taken");
				}

				if (dbflightSeat.getStatus().equals(status)) {
					throw new ModuleException("airinventory.seat.taken");
				}
				if (userPrincipal != null) {
					dbflightSeat.setUserDetails(userPrincipal);
				}
				dbflightSeat.setStatus(status);
				dbflightSeat.setTimestamp(new Date());
				// If status is RES or BLK keep the title. Otherwise make it null
				if (status.equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)) {
					dbflightSeat.setPaxType(null);
					isVacated = true;
				} else {
					dbflightSeat.setPaxType(flightSeatIdPaxType.get(dbflightSeat.getFlightAmSeatId()));
				}
			}

			if (isVacated) {
				vacatedSeatIds.add(dbflightSeat.getSeatId());
			}

		}

		seatDAO.saveFlightSeats(dbflightAmSeats);

		if (reservationAudit != null && vacatedSeatIds.size() > 0) {
			String vacatedSeats = "";
			List<AirCraftModelSeats> vacatedSeatList = AirInventoryModuleUtils.getAircraftBD().getFlightSeatData(vacatedSeatIds);
			for (AirCraftModelSeats airCraftModelSeats : vacatedSeatList) {
				vacatedSeats += airCraftModelSeats.getSeatCode() + ", ";
			}

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.VACATED_SEATS, vacatedSeats);
		}

	}

	// Modify FlightSeat status to BLOCK and remove existing flightAMSeatIds since we are adding it again to exchange
	// segments.
	@SuppressWarnings("unchecked")
	public void updateExchangedFlightSeats(Map<Integer, String> flightSeatIdPaxType, String status) throws ModuleException {

		Collection<Integer> flightSeatIds = flightSeatIdPaxType.keySet();

		Collection<FlightSeat> dbflightAmSeats = seatDAO.getFlightSeats(flightSeatIds);
		if (dbflightAmSeats == null || dbflightAmSeats.size() < 1) {
			throw new ModuleException("airinventory.seats.notfound");
		}

		Collection<Integer> flightAMSeatIds = new ArrayList<Integer>();

		for (FlightSeat dbflightSeat : dbflightAmSeats) {
			dbflightSeat.setStatus(status);
			dbflightSeat.setTimestamp(new Date());
			dbflightSeat.setPaxType(flightSeatIdPaxType.get(dbflightSeat.getFlightAmSeatId()));
			flightAMSeatIds.add(dbflightSeat.getFlightAmSeatId());
		}
		if (!flightAMSeatIds.isEmpty()) {
			seatDAO.deleteReservationSeats(flightAMSeatIds);
		}

		seatDAO.saveFlightSeats(dbflightAmSeats);

	}

	private static Object[] getFccSegInvIds(Collection<FlightSeat> seats) {
		Collection<Integer> fccSegIvIds = new ArrayList<Integer>();
		String status = null;
		for (FlightSeat flightSeat : seats) {
			if (status == null) {
				status = flightSeat.getStatus();
			} else {
				if (!status.equals(flightSeat.getStatus())) {
					log.error("############## [SEAT MAP DATA INCOSISTANT]:" + flightSeat.getFlightAmSeatId());
				}
			}
			fccSegIvIds.add(flightSeat.getFlightSegInventoryId());
		}
		return new Object[] { fccSegIvIds, status };
	}

	/**
	 * @todo : Configure the rules in Spring Bean to be dynamicallly loaded.
	 * @param paxFlightSeatSwapMap
	 *            TODO
	 * @param excludeCheckSeatIDs
	 */
	private static void checkSeatingRules(Collection<FlightSeat> blockingSeats, Map<Integer, String> flightSeatIdPaxType,
			Map<Integer, Integer> paxFlightSeatSwapMap, Collection<Integer> excludeCheckSeatIDs) throws ModuleException {
		// Few Notes:
		// The term row represents the row in the ancillary airline image that is shown in the UI. It's not the airline
		// physical row.
		// The term column represents the column in the ancillary airline image that is shown in the UI. It's not the
		// airline physical column.

		// row group 1 2
		// colIDrowID 1A 1B 1C 1D 1E 1F

		FlightSeatsDTO flightSeatDto = AirInventoryModuleUtils.getSeatMapJDBCDAO().getFlightSeatsForCheck(
				TODataExtractor.getFlightAmSeatIds(blockingSeats));

		Collection<SeatDTO> flightSeats = flightSeatDto.getSeats();

		Collection<SeatDTO> parentSeatsToCompare = new ArrayList<SeatDTO>();
		Collection<String> rowGroupColId = new ArrayList<String>();

		for (FlightSeat fs : blockingSeats) {

			String paxType = flightSeatIdPaxType.get(fs.getFlightAmSeatId());
			int flightAmSeatId = fs.getFlightAmSeatId();
			SeatDTO seatDTO = TODataExtractor.find(fs.getFlightAmSeatId(), flightSeats);
			if (paxType != null && paxType.equals(ReservationInternalConstants.PassengerType.PARENT) && seatDTO != null) {
				// rule 1: if parent - can not be near exit seats
				parentSeatsToCompare.add(seatDTO);
				if (seatDTO.getSeatType() != null
						&& seatDTO.getSeatType().equals(AirinventoryCustomConstants.FlightSeatMap.EXIT_SEAT)) {
					throw new ModuleException("airinventory.parent.exitseat");

				}

				// rule 2: if in row group 0 (i.e. left section), then will have to be odd numbered column .
				// i.e. 1A, 3A, 5A, ...likewise.
				if (seatDTO.getRowGroupId() == AirinventoryCustomConstants.FlightSeatMap.LEFT_ROW_GROUP) {
					if (seatDTO.getColId() % 2 == 0) {
						throw new ModuleException("airinventory.parent.not.odd.seat");
					}
				}

				// Fix for - AARESAA-6663
				// when changing traveling WithAdult reference following additional condition should be added for the
				// hasParentInRowGroupAndColumn validation
				// ex: change is PA-PA & AD-PA same seat reference should be ignored
				if (paxFlightSeatSwapMap != null && paxFlightSeatSwapMap.size() > 0
						&& seatDTO.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)) {
					flightAmSeatId = paxFlightSeatSwapMap.get(flightAmSeatId);
				}

				// rule 3: in both row groups, in a single column there can be only one parent
				if (AirInventoryModuleUtils.getSeatMapJDBCDAO().hasParentInRowGroupAndColumn(seatDTO.getRowGroupId(),
						seatDTO.getColId(), seatDTO.getFlightSegmentID(), flightAmSeatId, excludeCheckSeatIDs)) {
					throw new ModuleException("airinventory.parent.exist");
				}

				if (rowGroupColId.contains(Integer.toString(seatDTO.getRowGroupId()) + Integer.toString(seatDTO.getColId()))) {
					throw new ModuleException("airinventory.parent.exist");
				} else {
					if (ReservationInternalConstants.PassengerType.PARENT.equals(seatDTO.getSeatType())) {
						rowGroupColId.add(Integer.toString(seatDTO.getRowGroupId()) + Integer.toString(seatDTO.getColId()));
					}
				}

			}

			// check if the blocking parents seats are together
		}
	}

	public void checkSeatingRules(Map<Integer, String> flightSeatIdPaxType, Collection<Integer> excludeCheckSeatIDs)
			throws ModuleException {
		Collection<Integer> flightSeatIds = flightSeatIdPaxType.keySet();

		if (flightSeatIds.size() > 0) {
			Collection<FlightSeat> dbflightAmSeats = seatDAO.getFlightSeats(flightSeatIds);
			if (dbflightAmSeats == null || dbflightAmSeats.size() < 1) {
				throw new ModuleException("airinventory.seats.notfound");
			}
			checkSeatingRules(dbflightAmSeats, flightSeatIdPaxType, null, excludeCheckSeatIDs);
		} else {
			log.debug("=====FLIGHT_SEAT_IDs EMPTY=====");
		}
	}

	public Integer getFlightSeatID(Integer flightSegmentId, String seatCode) {
		return seatDAO.getFlightSeatID(flightSegmentId, seatCode);
	}

	public void deleteFlightSeats(Collection<Integer> flightSegIds) throws ModuleException {
		seatDAO.deleteFlightSeats(flightSegIds);
	}
}
