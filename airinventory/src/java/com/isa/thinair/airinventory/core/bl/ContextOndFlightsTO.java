package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;

public class ContextOndFlightsTO {
	List<AvailableIBOBFlightSegment> ondFlights = new ArrayList<AvailableIBOBFlightSegment>();

	public ContextOndFlightsTO() {
	}

	public ContextOndFlightsTO(List<AvailableIBOBFlightSegment> ondFlights) {
		if (ondFlights != null) {
			this.ondFlights = ondFlights;
		}
	}

	public void addOndFlight(AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		ondFlights.add(availableIBOBFlightSegment.getOndSequence(), availableIBOBFlightSegment);
	}

	public void addOndFlight(AvailableIBOBFlightSegment availableIBOBFlightSegment, boolean fillRemaining) {
		if (fillRemaining) {
			for (int index = 0; index < availableIBOBFlightSegment.getOndSequence(); index++) {
				addOndFlight(new AvailableFlightSegment(), index);
			}
		}
		addOndFlight(availableIBOBFlightSegment);
	}

	public void addOndFlight(AvailableIBOBFlightSegment availableIBOBFlightSegment, int ondSequence) {
		ondFlights.add(ondSequence, availableIBOBFlightSegment);
	}

	public FlightSegmentDTO getConnectingFlightSegment(Integer ondSequence) {
		if (ondSequence > 0 && ondSequence < ondFlights.size()) {
			int prevSeq = ondSequence - 1;
			if ((ondFlights.get(ondSequence).isInboundFlightSegment() && !ondFlights.get(prevSeq).isInboundFlightSegment())) {
				return null;
			}

			List<FlightSegmentDTO> flts = ondFlights.get(prevSeq).getFlightSegmentDTOs();
			if (flts != null && flts.size() > 0) {
				return flts.get(flts.size() - 1); // return the last element
			}
		}
		return null;
	}
}