package com.isa.thinair.airsecurity;

import java.util.Collection;

import com.isa.thinair.airsecurity.api.model.PrivilegeCategory;
import com.isa.thinair.airsecurity.api.model.UserDomain;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.api.service.SecurityUtilBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class AirSecurityUtilBDTest extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testSecurityUtilBD() throws ModuleException {
		IModule securityMod = AirSecurityUtil.getInstance();
		assertNotNull("module was null", securityMod);
		SecurityUtilBD securityUtilBD = (SecurityUtilBD) securityMod.getServiceBD("security.util.service.remote");
		assertNotNull("returned BD was null", securityUtilBD);
		PrivilegeCategory privilegeCategory = securityUtilBD.getPrivilegeCategory("AIRCRAFT");
		assertNotNull("Privilege category is null",privilegeCategory);
		assertEquals("Privilege category name retrieved is not match","Aircraft Maintenance",privilegeCategory.getPrivilegeCategoryDesc() );		
		if (privilegeCategory != null)
			System.out.println("Privilege category name of AIRCRAFT is" + privilegeCategory.getPrivilegeCategoryDesc());
		
		UserDomain userDomain = securityUtilBD.getUserDomain(1);
		assertEquals("User Domain is not match","Test Domain Entered by Thejaka",userDomain.getDomainName() );
		if (userDomain != null)
			System.out.println("Retrieved user domain is " + userDomain.getDomainName());
		
		//		Test privilege categories
		System.out.print("Going to retrieve privilege categories!");
		Collection privCats = securityUtilBD.getPrivilegeCategories();
		assertTrue("No privilege categories are retrived.",privCats.size()>0);
		System.out.print("Number of privilege categories retrieved = " + String.valueOf(privCats.size()) );
		PrivilegeCategory privCat = (PrivilegeCategory) privCats.iterator().next();		
		System.out.print("One of the retrieved privilege category :" + privCat.getPrivilegeCategoryDesc());		
	}
}
