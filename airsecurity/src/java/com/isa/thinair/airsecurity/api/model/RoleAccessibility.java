/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:24:02
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.model;

import java.io.Serializable;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_AGENT_TYPE"
 * 
 *                  Privilege is the entity class to repResent a Privilege model
 * 
 */

public class RoleAccessibility implements Serializable {

	private static final long serialVersionUID = -9071758827775086798L;

	private String agentTypeCode;

	/**
	 * @hibernate.id column = "AGENT_TYPE_CODE" generator-class = "assigned"
	 * @return
	 */
	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

}
