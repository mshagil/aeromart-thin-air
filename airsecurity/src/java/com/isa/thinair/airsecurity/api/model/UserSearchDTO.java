package com.isa.thinair.airsecurity.api.model;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airsecurity.api.utils.Constants;

public class UserSearchDTO implements Serializable {

	private static final long serialVersionUID = 1306074283336752826L;

	private String userID;
	private String displayName;
	private String stationCode;
	private String travelAgentCode;
	private Collection<String> reportingAgentCodes;
	private String adminuserCreateStatus;
	private String normaluserCreateStatus;
	private int accessLevel;
	private String accTravelAgentCode;
	private String serviceChannel[] = { Constants.INTERNAL_CHANNEL, Constants.BOTH_CHANNELS, Constants.EXTERNAL_CHANNEL };
	private String status;

	public UserSearchDTO() {
		super();
	}

	public UserSearchDTO(String displayName, String stationCode, String travelAgentCode, String userID) {

		setDisplayName(displayName);
		setStationCode(stationCode);
		setTravelAgentCode(travelAgentCode);
		setUserID(userID);
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getTravelAgentCode() {
		return travelAgentCode;
	}

	public void setTravelAgentCode(String travelAgentCode) {
		this.travelAgentCode = travelAgentCode;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Collection<String> getReportingAgentCodes() {
		return reportingAgentCodes;
	}

	public void setReportingAgentCodes(Collection<String> reportingAgentCodes) {
		this.reportingAgentCodes = reportingAgentCodes;
	}

	public String getAdminuserCreateStatus() {
		return adminuserCreateStatus;
	}

	public void setAdminuserCreateStatus(String adminuserCreateStatus) {
		this.adminuserCreateStatus = adminuserCreateStatus;
	}

	public String getNormaluserCreateStatus() {
		return normaluserCreateStatus;
	}

	public void setNormaluserCreateStatus(String normaluserCreateStatus) {
		this.normaluserCreateStatus = normaluserCreateStatus;
	}

	public int getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(int accessLevel) {
		this.accessLevel = accessLevel;
	}

	public String getAccTravelAgentCode() {
		return accTravelAgentCode;
	}

	public void setAccTravelAgentCode(String accTravelAgentCode) {
		this.accTravelAgentCode = accTravelAgentCode;
	}

	public String[] getServiceChannel() {
		return serviceChannel;
	}

	public void setServiceChannel(String[] serviceChannel) {
		this.serviceChannel = serviceChannel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
