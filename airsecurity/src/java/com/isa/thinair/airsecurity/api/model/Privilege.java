/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:24:02
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.model;

import java.io.Serializable;

import com.isa.thinair.airsecurity.api.dto.external.PrivilegeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Util;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_PRIVILEGE"
 * 
 *                  Privilege is the entity class to repesent a Privilege model
 * 
 */

public class Privilege implements Serializable {

	private static final long serialVersionUID = 1830514606728816625L;

	public static final String STATUS_ACTIVE = "ACT";
	public static final String STATUS_INACTIVE = "INA";

	public static final int STATUS_VISIBLE = 1;
	public static final int STATUS_NOTVISIBLE = 0;

	private String privilegeId;

	private String privilege;

	private String categoryID;

	private String status;

	private int visibility;

	public String serviceChannel;

	public Privilege() {

	}

	public Privilege(PrivilegeTO privilegeTO) throws ModuleException {
		Util.copyProperties(this, privilegeTO);
	}

	/**
	 * returns the privilegeId
	 * 
	 * @return Returns the privilegeId.
	 * 
	 * @hibernate.id column = "PRIVILEGE_ID" generator-class = "assigned"
	 */
	public String getPrivilegeId() {
		return privilegeId;
	}

	/**
	 * sets the privilegeId
	 * 
	 * @param privilegeId
	 *            The privilegeId to set.
	 */
	public void setPrivilegeId(String privilegeId) {
		this.privilegeId = privilegeId.intern();
	}

	/**
	 * returns the privilege
	 * 
	 * @return Returns the privilege.
	 * 
	 * @hibernate.property column = "PRIVILEGE"
	 */
	public String getPrivilege() {
		return privilege;
	}

	/**
	 * sets the privilege
	 * 
	 * @param privilege
	 *            The privilege to set.
	 */
	public void setPrivilege(String privilege) {
		this.privilege = privilege.intern();
	}

	/**
	 * returns the privilege
	 * 
	 * @return Returns the privilege category id.
	 * 
	 * @hibernate.property column = "PRIVILEGE_CATEGORY_ID"
	 */
	public String getPrivilegeCategoryID() {
		return categoryID;
	}

	/**
	 * sets the privilege category id
	 * 
	 * @param privilege
	 *            The privilege to set.
	 */
	public void setPrivilegeCategoryID(String categoryID) {
		this.categoryID = categoryID.intern();
	}

	/**
	 * Create PrivilegeTO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public PrivilegeTO toPrivilegeTO() throws ModuleException {
		PrivilegeTO privilegeTO = new PrivilegeTO();
		Util.copyProperties(privilegeTO, this);
		return privilegeTO;
	}

	/**
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status.intern();
	}

	/**
	 * @return Returns the visibility.
	 * 
	 * @hibernate.property column = "VISIBILITY"
	 */
	public int getVisibility() {
		return visibility;
	}

	/**
	 * @param visibility
	 *            The visibility to set.
	 */
	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	/**
	 * returns the privilege
	 * 
	 * @return Returns the privilege.
	 * 
	 * @hibernate.property column = "SERVICE_CHANNEL_CODE"
	 */
	public String getServiceChannel() {
		return serviceChannel;
	}

	/**
	 * sets the service channel
	 * 
	 * @param serviceChannel
	 *            The serviceChannel to set.
	 */
	public void setServiceChannel(String serviceChannel) {
		this.serviceChannel = serviceChannel.intern();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Privilege) {
			Privilege privilege = (Privilege) o;
			if (this.privilegeId.equals(privilege.privilegeId)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (this.getPrivilegeId() != null && this.getPrivilege() != null && this.getPrivilegeCategoryID() != null) {
			return this.getPrivilegeId().hashCode() + this.getPrivilege().hashCode() + this.getPrivilegeCategoryID().hashCode();
		} else {
			return (int) Math.round(Math.random());
		}
	}

}
