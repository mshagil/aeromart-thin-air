package com.isa.thinair.airsecurity.api.model;

import java.io.Serializable;

public class ParamSearchDTO implements Serializable {

	private static final long serialVersionUID = 1306074283336752826L;

	private String parameterKey;

	private String parameterName;

	private String carrierCode;

	public ParamSearchDTO() {
		super();
	}

	public ParamSearchDTO(String carrierCode, String parameterKey, String parameterName) {
		this.setCarrierCode(carrierCode);
		this.setParameterKey(parameterKey);
		this.setParameterName(parameterName);
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getParameterKey() {
		return parameterKey;
	}

	public void setParameterKey(String parameterKey) {
		this.parameterKey = parameterKey;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

}
