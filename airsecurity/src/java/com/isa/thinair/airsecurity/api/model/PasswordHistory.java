package com.isa.thinair.airsecurity.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @hibernate.class table = "t_user_password_history"
 * 
 */
public class PasswordHistory implements Serializable {

	private static final long serialVersionUID = 1344738096611246230L;

	private long passwordHistoryID;

	private String userId;

	private String password;

	private Date passwordExpiryDate;

	/**
	 * returns the passwordHistoryID.
	 * 
	 * @return Returns the passwordHistoryID.
	 * @hibernate.id column = "PASSWORD_HISTORY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "s_user_password_history"
	 */
	public long getPasswordHistoryID() {
		return passwordHistoryID;
	}

	public void setPasswordHistoryID(long passwordHistoryID) {
		this.passwordHistoryID = passwordHistoryID;
	}

	/**
	 * returns the userId
	 * 
	 * 
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * returns the password
	 * 
	 * @return Returns the password.
	 * @hibernate.property column= "PASSWORD"
	 */
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * returns the passwordExpiryDate
	 * 
	 * @return Returns the passwordExpiryDate.
	 * @hibernate.property column = "EXPIRY_DATE"
	 */
	public Date getPasswordExpiryDate() {
		return passwordExpiryDate;
	}

	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}

}
