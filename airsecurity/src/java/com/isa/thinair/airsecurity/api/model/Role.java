/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:27:01
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airsecurity.api.dto.external.RoleTO;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * 
 * @author : Code Generation Tool and Thejaka
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_ROLE"
 * 
 *                  Role is the entity class to repesent a Role model
 */
public class Role extends Tracking {

	private static final long serialVersionUID = -690217668194201079L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	public static final String STATUS_DELETE = "DEL";

	public static final String OWN_ROLE = "OWN";

	public static final String INCLUDE_RLOE = "INC";

	public static final String EXCLUDE_RLOE = "EXC";

	public static final String UNEDITABLE_ROLE_ID = "0";

	public static final String UNEDITABLE_ROLE_ID2 = "-1";

	public static final String UNEDITABLE_ROLE_ID3 = "-2";

	private String serviceChannel;

	private String roleId;

	private String roleName;

	private String remarks;

	private Set<Privilege> privileges;

	private String status;

	/**
	 * Holds the airline code the privilege is applied this airline code will not be equal to default airline code iff
	 * the privilege is applied to dry users. For all the other cases the airline code should be equal to the default
	 * airline of the system,
	 */
	private String airlineCode;

	/**
	 * Can be Null,INC,EXC Only applicable for dry users. Dry user privileges are compose as following 1. Get the normal
	 * Privileges assied to that user form the MC 2. Exclude any privileges define in Exclude privileges define for that
	 * OC 2. Include any privileges define in Include privileges define for that OC
	 */
	private String includeExcludeFlag;

	private Set<RoleVisibility> agentTypeVisibilities;

	private Set<RoleAccessibility> agentTypeAccessiblity;

	private String lccPublishStatus;

	public Role() {

	}

	/**
	 * Create a Role Object
	 * 
	 * @param roleTO
	 * @throws ModuleException
	 */
	public Role(RoleTO roleTO) throws ModuleException {

		this();
		String channel = Constants.EXTERNAL_CHANNEL;
		roleTO.setServiceChannel(channel);

		Util.copyProperties(this, roleTO);
		Collection<String> colPrivilegeIds = roleTO.getColPrivilegeIds();

		if (colPrivilegeIds != null && colPrivilegeIds.size() > 0) {
			Collection<Privilege> colPrivilege = AirSecurityUtil.getSecurityBD().getPrivileges(colPrivilegeIds,
					Constants.EXTERNAL_CHANNEL);
			Privilege privilege;

			for (Iterator<Privilege> iter = colPrivilege.iterator(); iter.hasNext();) {
				privilege = (Privilege) iter.next();
				this.addPrivilege(privilege);
			}
		}
	}

	/**
	 * Create a RoleTO object
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public RoleTO toRoleTO() throws ModuleException {

		RoleTO roleTO = new RoleTO();
		Util.copyProperties(roleTO, this);
		roleTO.setEditable(this.isEditable());
		Collection<Privilege> colPrivilege = this.getPrivileges();

		if (colPrivilege != null && colPrivilege.size() > 0) {
			for (Iterator<Privilege> iter = colPrivilege.iterator(); iter.hasNext();) {
				Privilege privilege = (Privilege) iter.next();
				roleTO.addPrivilegeId(privilege.getPrivilegeId());
			}
		}

		return roleTO;
	}

	/**
	 * returns the roleId
	 * 
	 * @return Returns the roleId.
	 * 
	 * @hibernate.id column = "ROLE_ID" generator-class = "assigned"
	 * 
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * sets the roleId
	 * 
	 * @param roleId
	 *            The roleId to set.
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	/**
	 * returns the roleName
	 * 
	 * @return Returns the roleName.
	 * 
	 * @hibernate.property column = "ROLE_NAME"
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * sets the roleName
	 * 
	 * @param roleName
	 *            The roleName to set.
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @hibernate.set table="T_ROLE_PRIVILEGE" cascade="none" readonly="true"
	 * @hibernate.collection-key column="ROLE_ID"
	 * 
	 * @hibernate.collection-many-to-many class="com.isa.thinair.airsecurity.api.model.Privilege" column="PRIVILEGE_ID"
	 * 
	 */
	public Set<Privilege> getPrivileges() {
		return this.privileges;
	}

	/**
	 * Add a Privilege
	 * 
	 * @param privilege
	 */
	public void addPrivilege(Privilege privilege) {
		if (this.getPrivileges() == null) {
			this.setPrivileges(new HashSet<Privilege>());
		}

		this.getPrivileges().add(privilege);
	}

	/**
	 * 
	 * @param privilege
	 *            The privilege to set.
	 */
	public void setPrivileges(Set<Privilege> set) {
		this.privileges = set;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status.intern();
	}

	public boolean isEditable() {
		String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		return !(roleId.equals(defaultCarrier + UNEDITABLE_ROLE_ID) && roleId.equals(defaultCarrier + UNEDITABLE_ROLE_ID2) && roleId
				.equals(defaultCarrier + UNEDITABLE_ROLE_ID3));
	}

	/**
	 * @hibernate.set table="T_ROLE_VISIBLE_AGENT_TYPE"
	 * @hibernate.collection-key column="ROLE_ID" cascade="none" readonly="true"
	 * 
	 * @hibernate.collection-many-to-many class="com.isa.thinair.airsecurity.api.model.RoleVisibility"
	 *                                    column="agent_type_code"
	 * 
	 */
	public Set<RoleVisibility> getAgentTypeVisibilities() {
		return agentTypeVisibilities;
	}

	public void setAgentTypeVisibilities(Set<RoleVisibility> agentTypeVisibilities) {
		this.agentTypeVisibilities = agentTypeVisibilities;
	}

	/**
	 * @hibernate.set table="T_ROLE_ASSIGNABLE_AGENT_TYPE" lazy="false"
	 * @hibernate.collection-key column="ROLE_ID" cascade="none" readonly="true"
	 * 
	 * @hibernate.collection-many-to-many class="com.isa.thinair.airsecurity.api.model.RoleAccessibility"
	 *                                    column="agent_type_code"
	 * 
	 */
	public Set<RoleAccessibility> getAgentTypeAccessiblity() {
		return agentTypeAccessiblity;
	}

	public void setAgentTypeAccessiblity(Set<RoleAccessibility> agentTypeAccessiblity) {
		this.agentTypeAccessiblity = agentTypeAccessiblity;
	}

	/**
	 * @returns the serviceChannel
	 * 
	 * @hibernate.property column = "SERVICE_CHANNEL_CODE"
	 * 
	 */
	public String getServiceChannel() {
		return serviceChannel;
	}

	/**
	 * sets the serviceChannel
	 * 
	 * @param remarks
	 *            The serviceChannel to set.
	 */
	public void setServiceChannel(String serviceChannel) {
		this.serviceChannel = serviceChannel.intern();
	}

	/**
	 * @return the airlineCode
	 * @hibernate.property column = "AIRLINE_CODE"
	 */
	public String getAirlineCode() {
		return airlineCode;
	}

	/**
	 * @param airlineCode
	 *            the airlineCode to set
	 */
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode.intern();
	}

	/**
	 * @return the includeExcludeFlag
	 * @hibernate.property column = "INCLUDE_EXCLUDE"
	 */
	public String getIncludeExcludeFlag() {
		return includeExcludeFlag;
	}

	/**
	 * @param includeExcludeFlag
	 *            the includeExcludeFlag to set
	 */
	public void setIncludeExcludeFlag(String includeExcludeFlag) {
		if (includeExcludeFlag != null) {
			this.includeExcludeFlag = includeExcludeFlag.intern();
		} else {
			this.includeExcludeFlag = includeExcludeFlag;
		}
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(getRoleId());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Role role = (Role) obj;
		return EqualsBuilder.reflectionEquals(this.getRoleId(), role.getRoleId());
	}

	/**
	 * @hibernate.property column = "LCC_PUBLISH_STATUS"
	 */
	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	/**
	 * @param lccPublishStatus
	 *            the lccPublishStatus to set
	 */

	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus.intern();
	}

}
