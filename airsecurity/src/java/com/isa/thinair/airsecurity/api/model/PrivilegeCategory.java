/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:24:17
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.model;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_PRIVILEGE_CATEGORY"
 * 
 *                  PrivilegeCategory is the entity class to repesent a PrivilegeCategory model
 * 
 */

public class PrivilegeCategory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1691734713150246162L;

	private String privilegeCategoryId;

	private String privilegeCategoryDesc;

	private Set<Privilege> privileges;

	/**
	 * returns the privilegeCategoryId
	 * 
	 * @return Returns the privilegeCategoryId.
	 * 
	 * @hibernate.id column = "PRIVILEGE_CATEGORY_ID" generator-class = "assigned"
	 */
	public String getPrivilegeCategoryId() {
		return privilegeCategoryId;
	}

	/**
	 * sets the privilegeCategoryId
	 * 
	 * @param privilegeCategoryId
	 *            The privilegeCategoryId to set.
	 */
	@SuppressWarnings("unused")
	private void setPrivilegeCategoryId(String privilegeCategoryId) {
		this.privilegeCategoryId = privilegeCategoryId;
	}

	/**
	 * returns the privilegeCategoryDesc
	 * 
	 * @return Returns the privilegeCategoryDesc.
	 * 
	 * @hibernate.property column = "PRIVILEGE_CATEGORY_DESC"
	 */
	public String getPrivilegeCategoryDesc() {
		return privilegeCategoryDesc;
	}

	/**
	 * sets the privilegeCategoryDesc
	 * 
	 * @param privilegeCategoryDesc
	 *            The privilegeCategoryDesc to set.
	 */
	@SuppressWarnings("unused")
	private void setPrivilegeCategoryDesc(String privilegeCategoryDesc) {
		this.privilegeCategoryDesc = privilegeCategoryDesc;
	}

	/**
	 * @hibernate.set lazy="false" cascade="none"
	 * 
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airsecurity.api.model.Privilege"
	 * 
	 * @hibernate.collection-key column="PRIVILEGE_CATEGORY_ID"
	 */
	public Set<Privilege> getPrivileges() {
		return privileges;
	}

	/**
   * 
   */
	@SuppressWarnings("unused")
	private void setPrivileges(Set<Privilege> set) {
		this.privileges = set;
	}

}
