/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:58
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airsecurity.api.dto.external.RoleTO;
import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * 
 * @author : Code Generation Tool and Thejaka
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_USER"
 * 
 *                  User is the entity class to repesent a User model
 * 
 */
public class User extends Tracking {

	private static final long serialVersionUID = -6250311399755529891L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INVALID = "INA";

	public static final String STATUS_BLOCKED = "BLK";

	public static final String UNEDITABLE_USER_IDS = "SYSTEM,SCHEDULER";

	public static final String SCHEDULER_USER_ID = "SCHEDULER";

	public static final String PASSWORD_RESETTED_YES = "Y";

	public static final String PASSWORD_RESETTED_NO = "N";

	public static final String THEME_CODE_DEFAULT = "default";
	
	public static final String SCHEDULER_USER_DISPLAY_NAME= "Scheduler User";

	private String userId;

	private String password;

	private String displayName;

	private String emailId;

	private String status;

	private String agentCode;

	private Set<Role> roles;

	private Set<Role> inactiveRoles;

	private Set<String> priviledgeIDs;

	private Set<String> carriers;

	private String defaultCarrierCode;

	private String airlineCode;

	private String adminUserCreateStatus;

	private String normalUserCreateStatus;

	private String serviceChannel = Constants.INTERNAL_CHANNEL;

	private String passwordReseted = PASSWORD_RESETTED_NO;

	private String themeCode;

	private String dashboardPreference;

	private String newMessageExist = "N";

	private String ffpNumber;

	private boolean assignRoleChangesToOtherUsers = false;

	private String myIDCarrierCode;
	
	private Date passwordExpiryDate;
	
	private Integer loginAttempts;
	
	private Date accountLockReleaseTime;

	private String lccPublishStatus;
	
	private Integer maxAdultAllowed;

	private Set<PasswordHistory> passwordHistories;

	public User() {

	}

	public User(UserTO userTO) throws ModuleException {
		this();
		boolean isPrivilegesExist = false;

		try {
			userTO.getPriviledgeIDs();
			isPrivilegesExist = true;
		} catch (ModuleException e) {
			// Remove the exception when privilege list is empty
			userTO.setPrivitedgeIDs(new HashSet<String>());
		}

		Util.copyProperties(this, userTO);

		if (!isPrivilegesExist) {
			this.setPrivitedgeIDs(null);
		}

		Collection<String> colRoleIds = new ArrayList<String>();
		if (userTO.getRoleTOs() != null) {
			for (Iterator<RoleTO> iterRoleTOs = userTO.getRoleTOs().iterator(); iterRoleTOs.hasNext();) {
				RoleTO roleTO = (RoleTO) iterRoleTOs.next();
				colRoleIds.add(roleTO.getRoleId());
			}
		}

		if (colRoleIds.size() > 0) {
			Collection<Role> colRole = AirSecurityUtil.getSecurityBD().getRoles(colRoleIds);
			for (Iterator<Role> iter = colRole.iterator(); iter.hasNext();) {
				Role role = (Role) iter.next();
				this.addRole(role);
			}
		}

		this.setAdminUserCreateStatus("N");
		this.setNormalUserCreateStatus("N");
		this.setServiceChannel(Constants.EXTERNAL_CHANNEL);

		String defaultCarrierCode = PlatformUtiltiies.nullHandler(userTO.getDefaultCarrierCode());

		if (defaultCarrierCode != null) {
			this.setDefaultCarrierCode(defaultCarrierCode);
		} else {
			this.setDefaultCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		}

		if (userTO.getUserCarriers() != null && userTO.getUserCarriers().size() > 0) {
			this.setCarriers(userTO.getUserCarriers());
		} else {
			String defCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			Set<String> carrierCodes = new HashSet<String>();
			carrierCodes.add(defCarrierCode);
			this.setCarriers(carrierCodes);
		}
	}

	/**
	 * Create a UserTO Object
	 * 
	 * @param user
	 * @return
	 * @throws ModuleException
	 */
	public UserTO toUserTO() throws ModuleException {
		UserTO userTO = new UserTO();
		boolean isPrivilegesExist = false;

		try {
			this.getPrivitedgeIDs();
			isPrivilegesExist = true;
		} catch (ModuleException e) {
			// Remove the exception when privilege list is empty
			this.setPrivitedgeIDs(new HashSet<String>());
		}

		Util.copyProperties(userTO, this);

		if (!isPrivilegesExist) {
			userTO.setPrivitedgeIDs(null);
		}

		if (this.getRoles() != null) {
			Set<RoleTO> roleTOs = new HashSet<RoleTO>();
			Role role;
			for (Iterator<Role> iterRoles = this.getRoles().iterator(); iterRoles.hasNext();) {
				role = (Role) iterRoles.next();
				if (BeanUtils.nullHandler(role.getServiceChannel()).equals(Constants.EXTERNAL_CHANNEL)) {
					roleTOs.add(role.toRoleTO());
				}
			}

			userTO.setRoleTOs(roleTOs);
		}

		return userTO;
	}

	/**
	 * returns the userId
	 * 
	 * @return Returns the userId.
	 * @hibernate.id column = "USER_ID" generator-class = "assigned"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * sets the userId
	 * 
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * returns the password
	 * 
	 * @return Returns the password.
	 * @hibernate.property column = "PASSWORD"
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * sets the password
	 * 
	 * @param password
	 *            The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * returns the displayName
	 * 
	 * @return Returns the displayName.
	 * @hibernate.property column = "DISPLAY_NAME"
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * sets the displayName
	 * 
	 * @param displayName
	 *            The displayName to set.
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * returns the emailId
	 * 
	 * @return Returns the emailId.
	 * @hibernate.property column = "EMAIL_ID"
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * sets the emailId
	 * 
	 * @param emailId
	 *            The emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the user agent code
	 * 
	 * @return Returns the user domain id.
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * sets agentcode
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @hibernate.set table="T_USER_ROLE" cascade="none" readonly="true"
	 *                where="Exists (select * from T_ROLE where ROLE_ID = T_ROLE.ROLE_ID )"
	 * @hibernate.collection-key column="USER_ID"
	 * @hibernate.collection-many-to-many class="com.isa.thinair.airsecurity.api.model.Role" column="ROLE_ID"
	 * 
	 */
	public Set<Role> getRoles() {
		return this.roles;
	}

	/**
	 * @param role
	 *            The role to set.
	 */
	public void setRoles(Set<Role> set) {
		this.roles = set;
	}

	/**
	 * @hibernate.set table="T_USER_ROLE" cascade="none" readonly="true"
	 *                where="Exists (select * from T_ROLE where T_ROLE.STATUS = 'INA' and ROLE_ID = T_ROLE.ROLE_ID )"
	 * @hibernate.collection-key column="USER_ID"
	 * @hibernate.collection-many-to-many class="com.isa.thinair.airsecurity.api.model.Role" column="ROLE_ID"
	 */
	public Set<Role> getInactiveRoles() {
		return inactiveRoles;
	}

	/**
	 * @param inactiveRoles
	 *            the inactiveRoles to set
	 */
	public void setInactiveRoles(Set<Role> inactiveRoles) {
		this.inactiveRoles = inactiveRoles;
	}

	/**
	 * Privilege IDs are set externally
	 * 
	 * @param priviledgeIDs
	 */
	public void setPrivitedgeIDs(Set<String> priviledgeIDs) {
		this.priviledgeIDs = priviledgeIDs;
	}

	public Set<String> getPrivitedgeIDs() throws ModuleException {
		if (priviledgeIDs == null) {
			throw new ModuleException("airsecurity.arg.dao.user.privilegesnotloaded");
		}
		return priviledgeIDs;
	}

	public boolean isEditable() {
		String[] uneditableUsers = UNEDITABLE_USER_IDS.split(",");
		for (int i = 0; i < uneditableUsers.length; i++) {
			if (userId.equals(uneditableUsers[i])) {
				return false;
			}
		}
		return true;
	}

	public static Collection<String> getUneditableUserIDs() {
		return Arrays.asList(UNEDITABLE_USER_IDS.split(","));
	}

	/**
	 * Adds a role
	 */
	public void addRole(Role role) {
		if (this.getRoles() == null) {
			this.setRoles(new HashSet<Role>());
		}

		this.getRoles().add(role);
	}

	/**
	 * @hibernate.set table="t_user_carrier" cascade="all"
	 * @hibernate.collection-key column="USER_ID"
	 * @hibernate.collection-element type="string" column="carrier_code"
	 */
	public Set<String> getCarriers() {
		return carriers;
	}

	public void setCarriers(Set<String> carriers) {
		this.carriers = carriers;
	}

	/**
	 * @hibernate.property column = "default_carrier_code"
	 */
	public String getDefaultCarrierCode() {
		return defaultCarrierCode;
	}

	public void setDefaultCarrierCode(String defaultCarrierCode) {
		this.defaultCarrierCode = defaultCarrierCode;
	}

	/**
	 * @return the airlineCode
	 * @hibernate.property column = "AIRLINE_CODE"
	 */
	public String getAirlineCode() {
		return airlineCode;
	}

	/**
	 * @param airlineCode
	 *            the airlineCode to set
	 */
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	/**
	 * @hibernate.property column = "ADMIN_USER_CREATE_STATUS"
	 */
	public String getAdminUserCreateStatus() {
		return adminUserCreateStatus;
	}

	public void setAdminUserCreateStatus(String adminUserCreateStatus) {
		this.adminUserCreateStatus = adminUserCreateStatus;
	}

	/**
	 * @hibernate.property column = "NORMAL_USER_CREATE_STATUS"
	 */
	public String getNormalUserCreateStatus() {
		return normalUserCreateStatus;
	}

	public void setNormalUserCreateStatus(String normalUserCreateStatus) {
		this.normalUserCreateStatus = normalUserCreateStatus;
	}

	/**
	 * @hibernate.property column = "SERVICE_CHANNEL_CODE"
	 */
	public String getServiceChannel() {
		return serviceChannel;
	}

	/**
	 * sets serviceChannel
	 */
	public void setServiceChannel(String serviceChannel) {
		this.serviceChannel = serviceChannel;
	}

	/**
	 * @hibernate.property column = "PASSWORD_RESETED"
	 */
	public String getPasswordReseted() {
		return passwordReseted;
	}

	/**
	 * @param passwordReseted
	 *            The passwordReseted to set.
	 */
	public void setPasswordReseted(String passwordReseted) {
		this.passwordReseted = passwordReseted;
	}

	/**
	 * @hibernate.property column = "UI_THEME_CODE"
	 */
	public String getThemeCode() {
		return themeCode;
	}

	/**
	 * @param themeCode
	 *            The themeCode to set.
	 */
	public void setThemeCode(String themeCode) {
		this.themeCode = themeCode;
	}

	/**
	 * @hibernate.property column = "DASHBOARD_PREF"
	 */
	public String getDashboardPreference() {
		return dashboardPreference;
	}

	public void setDashboardPreference(String dashboardPreference) {
		this.dashboardPreference = dashboardPreference;
	}

	/**
	 * @hibernate.property column = "NEW_MESSAGE_EXIST"
	 * @return the newMessageExist
	 */
	public String getNewMessageExist() {
		return newMessageExist;
	}

	/**
	 * @param newMessageExist
	 *            the newMessageExist to set
	 */
	public void setNewMessageExist(String newMessageExist) {
		this.newMessageExist = newMessageExist;
	}

	/**
	 * @hibernate.property column = "FFP_NUMBER"
	 * @return the ffpNumber
	 */
	public String getFfpNumber() {
		return ffpNumber;
	}

	/**
	 * @param ffpNumber
	 *            the ffpNumber to set
	 */
	public void setFfpNumber(String ffpNumber) {
		this.ffpNumber = ffpNumber;
	}

	/**
	 * @return the assignRoleChangesToOtherUsers
	 */
	public boolean isAssignRoleChangesToOtherUsers() {
		return assignRoleChangesToOtherUsers;
	}

	/**
	 * @param assignRoleChangesToOtherUsers
	 *            the assignRoleChangesToOtherUsers to set
	 */
	public void setAssignRoleChangesToOtherUsers(boolean assignRoleChangesToOtherUsers) {
		this.assignRoleChangesToOtherUsers = assignRoleChangesToOtherUsers;
	}

	/**
	 * @hibernate.property column = "MYID_CARRIER_CODE"
	 */
	public String getMyIDCarrierCode() {
		return myIDCarrierCode;
	}

	public void setMyIDCarrierCode(String myIDCarrierCode) {
		this.myIDCarrierCode = myIDCarrierCode;
	}
	/**
	 * @return the carrierCode
	 * @hibernate.property column = "LCC_PUBLISH_STATUS"
	 */
	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	/**
	 * @param lccPublishStatus
	 *            the lccPublishStatus to set
	 */

	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus.intern();
	}
	/**
	 * @hibernate.property column = "PASSWORD_EXPIRY_DATE"
	 */
	public Date getPasswordExpiryDate() {
		return passwordExpiryDate;
	}

	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}
	/**
	 * @hibernate.property column = "LOGIN_ATTEMPTS"
	 */
	public Integer getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(Integer loginAttempts) {
		this.loginAttempts = loginAttempts;
	}
	/**
	 * @hibernate.property column = "ACCOUNT_LOCK_RELEASE_DATE "
	 */
	public Date getAccountLockReleaseTime() {
		return accountLockReleaseTime;
	}

	public void setAccountLockReleaseTime(Date accountLockReleaseTime) {
		this.accountLockReleaseTime = accountLockReleaseTime;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airsecurity.api.model.PasswordHistory"
	 * @hibernate.collection-key column="USER_ID"
	 * 
	 */
	public Set<PasswordHistory> getPasswordHistories() {
		return passwordHistories;
	}

	public void setPasswordHistories(Set<PasswordHistory> passwordHistories) {
		this.passwordHistories = passwordHistories;
	}

	/**
	 * @return the maxAdultAllowed
	 * @hibernate.property column = "MAX_ADULT_COUNT"
	 */
	public Integer getMaxAdultAllowed() {
		return maxAdultAllowed;
	}

	/**
	 * @param maxAdultAllowed the maxAdultAllowed to set
	 */
	public void setMaxAdultAllowed(Integer maxAdultAllowed) {
		this.maxAdultAllowed = maxAdultAllowed;
	}
	
}
