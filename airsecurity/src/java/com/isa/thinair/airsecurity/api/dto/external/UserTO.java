/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on April 24, 2007 10:56:07 
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.dto.external;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Tracking;

/**
 * User is the entity class to repesent a User Transfer Information
 */
public class UserTO extends Tracking implements Serializable {

	private static final long serialVersionUID = -6650903331739883058L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INVALID = "INA";

	public static final String STATUS_BLOCKED = "BLK";

	public static final String UNEDITABLE_USER_IDS = "SYSTEM,SCHEDULER";

	public static final String PASSWORD_RESETTED_YES = "Y";

	public static final String PASSWORD_RESETTED_NO = "N";

	private String userId;

	private String password;

	private String displayName;

	private String emailId;

	private String status;

	private String agentCode;

	private String agentName;

	private Set<RoleTO> roleTOs;

	private Set<String> priviledgeIDs;

	private Set<String> userCarriers;

	private String defaultCarrierCode;

	private String adminUserCreateStatus;

	private String normalUserCreateStatus;

	private String themeCode;

	private String lccPublishStatus;

	private String serviceChannel = Constants.INTERNAL_CHANNEL;

	private String passwordReseted = PASSWORD_RESETTED_NO;

	private String airlineCode;

	private String myIDCarrierCode;

	public String getDefaultCarrierCode() {
		return defaultCarrierCode;
	}

	public void setDefaultCarrierCode(String defaultCarrierCode) {
		this.defaultCarrierCode = defaultCarrierCode;
	}

	/**
	 * returns the userId
	 * 
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * sets the userId
	 * 
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * returns the password
	 * 
	 * @return Returns the password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * sets the password
	 * 
	 * @param password
	 *            The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * returns the displayName
	 * 
	 * @return Returns the displayName.
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * sets the displayName
	 * 
	 * @param displayName
	 *            The displayName to set.
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * returns the emailId
	 * 
	 * @return Returns the emailId.
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * sets the emailId
	 * 
	 * @param emailId
	 *            The emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the user agent code
	 * 
	 * @return Returns the user domain id.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * sets agentcode
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Set<RoleTO> getRoleTOs() {
		return this.roleTOs;
	}

	/**
	 * @param role
	 *            The role to set.
	 */
	public void setRoleTOs(Set<RoleTO> set) {
		this.roleTOs = set;
	}

	/**
	 * Privilege IDs are set externally
	 * 
	 * @param priviledgeIDs
	 */
	public void setPrivitedgeIDs(Set<String> priviledgeIDs) {
		this.priviledgeIDs = priviledgeIDs;
	}

	public Set<String> getPriviledgeIDs() throws ModuleException {
		if (priviledgeIDs == null) {
			throw new ModuleException("airsecurity.arg.dao.user.privilegesnotloaded");
		}
		return priviledgeIDs;
	}

	public boolean isEditable() {
		String[] uneditableUsers = UNEDITABLE_USER_IDS.split(",");
		for (int i = 0; i < uneditableUsers.length; i++) {
			if (userId.equals(uneditableUsers[i])) {
				return false;
			}
		}
		return true;
	}

	public static Collection<String> getUneditableUserIDs() {
		return Arrays.asList(UNEDITABLE_USER_IDS.split(","));
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAdminUserCreateStatus() {
		return adminUserCreateStatus;
	}

	public void setAdminUserCreateStatus(String adminUserCreateStatus) {
		this.adminUserCreateStatus = adminUserCreateStatus;
	}

	public String getNormalUserCreateStatus() {
		return normalUserCreateStatus;
	}

	public void setNormalUserCreateStatus(String normalUserCreateStatus) {
		this.normalUserCreateStatus = normalUserCreateStatus;
	}

	public String getThemeCode() {
		return themeCode;
	}

	public void setThemeCode(String themeCode) {
		this.themeCode = themeCode;
	}

	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus;
	}

	public String getServiceChannel() {
		return serviceChannel;
	}

	public void setServiceChannel(String serviceChannel) {
		this.serviceChannel = serviceChannel;
	}

	public String getPasswordReseted() {
		return passwordReseted;
	}

	public void setPasswordReseted(String passwordReseted) {
		this.passwordReseted = passwordReseted;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getMyIDCarrierCode() {
		return myIDCarrierCode;
	}

	public void setMyIDCarrierCode(String myIDCarrierCode) {
		this.myIDCarrierCode = myIDCarrierCode;
	}

	public Set<String> getUserCarriers() {
		return userCarriers;
	}

	public void setUserCarriers(Set<String> userCarriers) {
		this.userCarriers = userCarriers;
	}

}
