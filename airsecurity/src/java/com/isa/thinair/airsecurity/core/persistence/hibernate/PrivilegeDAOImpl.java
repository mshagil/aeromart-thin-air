/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:24:17
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airsecurity.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.PrivilegeCategory;
import com.isa.thinair.airsecurity.api.model.PrivilegeDependantsDTO;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airsecurity.core.persistence.dao.PrivilegeDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * BHive: PrivilegeCategory module
 * 
 * @author This is a autogenerated file
 * @isa.module.dao-impl dao-name="PrivilegeDAOImpl"
 * 
 *                      PrivilegeCategoryDAOImpl is the PrivilegeCategorytDAO implementatiion for hibernate
 * 
 */
public class PrivilegeDAOImpl extends PlatformHibernateDaoSupport implements PrivilegeDAO {

	// private Log log = LogFactory.getLog(PrivilegeDAO.class);


	public Collection<PrivilegeCategory> getPrivilegeCategories() {
		return find("from PrivilegeCategory", PrivilegeCategory.class);
	}

	@SuppressWarnings("rawtypes")
	public Page getPrivilegeCategories(int startRec, int pageSize) {
		try {
			return new Page(getPrivilegeCategories().size(), startRec, startRec + pageSize, (Collection) getSession()
					.createCriteria(PrivilegeCategory.class).setFirstResult(startRec).setMaxResults(startRec + pageSize).list());
		} catch (HibernateException he) {
			throw new CommonsDataAccessException(he, "airsecurity.arg.dao.invalidrecordsrange");
		}
	}

	public PrivilegeCategory getPrivilegeCategory(String id) {
		return (PrivilegeCategory) get(PrivilegeCategory.class, id);
	}

	public Privilege getPrivilege(String privilegeId) {
		return (Privilege) get(Privilege.class, privilegeId);
	}

	public Privilege getPrivilege(String privilegeId, String channel) {
		if (channel != null)
			return (Privilege) find(
					"from Privilege p where p.privilegeId = '" + privilegeId + "' and p.serviceChannel = '" + channel + "' ", Privilege.class);
		else
			return (Privilege) find(
					"from Privilege p where p.privilegeId = '" + privilegeId + "' and p.serviceChannel = '"
							+ Constants.EXTERNAL_CHANNEL + "' ", Privilege.class);
	}

	public Collection<Privilege> getPrivileges() {
		return find("from Privilege", Privilege.class);
	}

	public Collection<Privilege> getPrivileges(String channel) {
		return find("from Privilege p where p.serviceChannel = '" + channel + "' ", Privilege.class);
	}

	public Collection<Privilege> getActiveVisiblePrivileges(String channel) {
		return find(
				"from Privilege p where p.status = '" + Privilege.STATUS_ACTIVE + "' and p.visibility = '"
						+ Privilege.STATUS_VISIBLE + "' and p.serviceChannel = '" + channel + "'", Privilege.class);
	}

	public Collection<Privilege> getActiveVisiblePrivileges() {
		return find(
				"from Privilege p where p.status = '" + Privilege.STATUS_ACTIVE + "' and p.visibility = '"
						+ Privilege.STATUS_VISIBLE + "'", Privilege.class);
	}
	public Collection<Privilege> getActivePrivileges() {
		return find("from Privilege p where p.status = ?", Privilege.STATUS_ACTIVE, Privilege.class);
	}

	public Collection<Privilege> getActivePrivileges(String channel) {
		return find("from Privilege p where p.serviceChannel = '" + channel + "' and p.status = ?",
				Privilege.STATUS_ACTIVE, Privilege.class);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public PrivilegeDependantsDTO getPrivilegeDependencies(String previlegeId) {
		PrivilegeDependantsDTO privilegeDependantsDTO = new PrivilegeDependantsDTO();
		JdbcTemplate jt = new JdbcTemplate(getDatasource());
		List rows = jt.query("select ANCESTOR_PRIVILEGE_ID from T_PRIVILEGE_DEPENDENCY where PRIVILEGE_ID = '" + previlegeId
				+ "'", new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				// if result set is not null
				if (rs != null) {
					return rs.getString("ANCESTOR_PRIVILEGE_ID");
				}
				return null;
			}
		});

		privilegeDependantsDTO.setPrivilegeId(previlegeId);
		privilegeDependantsDTO.setDependantPrevilegeIds((Collection) rows);
		return privilegeDependantsDTO;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public PrivilegeDependantsDTO getPrivilegeDependencies(String previlegeId, String channel) {
		PrivilegeDependantsDTO privilegeDependantsDTO = new PrivilegeDependantsDTO();
		JdbcTemplate jt = new JdbcTemplate(getDatasource());
		List rows = jt.query("select ANCESTOR_PRIVILEGE_ID from T_PRIVILEGE_DEPENDENCY where PRIVILEGE_ID = '" + previlegeId
				+ "'", new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				// if result set is not null
				if (rs != null) {
					return rs.getString("ANCESTOR_PRIVILEGE_ID");
				}
				return null;
			}
		});

		privilegeDependantsDTO.setPrivilegeId(previlegeId);
		privilegeDependantsDTO.setDependantPrevilegeIds((Collection) rows);
		return privilegeDependantsDTO;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, List<String>> getPrivilegeDependencies(Collection<String> previlegeIds) {

		JdbcTemplate jt = new JdbcTemplate(getDatasource());
		List<String[]> rows = jt.query("select PRIVILEGE_ID, ANCESTOR_PRIVILEGE_ID from T_PRIVILEGE_DEPENDENCY where PRIVILEGE_ID in  ("
				+ Util.buildStringInClauseContent(previlegeIds) + ") order by PRIVILEGE_ID, ANCESTOR_PRIVILEGE_ID",
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						// if result set is not null
						if (rs != null) {
							String[] privs = { rs.getString("PRIVILEGE_ID"), rs.getString("ANCESTOR_PRIVILEGE_ID") };
							return privs;
						}
						return null;
					}
				});
		List<String> ansPrivs = new ArrayList<String>();
		String privId = "";
		HashMap<String, List<String>> privHashMap = new HashMap<String, List<String>>();
		for (Iterator<String[]> iterPriv = rows.iterator(); iterPriv.hasNext();) {
			String[] privArr = (String[]) iterPriv.next();
			if (!((String) privArr[0]).equals(privId)) {
				if (!privId.equals(""))
					privHashMap.put(privId, ansPrivs);
				privId = (String) privArr[0];
				ansPrivs = new ArrayList<String>();
			}
			ansPrivs.add((String) privArr[1]);
		}
		if (!privId.equals(""))
			privHashMap.put(privId, ansPrivs);

		return privHashMap;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, List<String>> getPrivilegeDependencies(Collection<String> previlegeIds, String channel) {

		JdbcTemplate jt = new JdbcTemplate(getDatasource());
		List<String[]> rows = jt.query("select PRIVILEGE_ID, ANCESTOR_PRIVILEGE_ID from T_PRIVILEGE_DEPENDENCY where PRIVILEGE_ID in  ("
				+ Util.buildStringInClauseContent(previlegeIds) + ") order by PRIVILEGE_ID, ANCESTOR_PRIVILEGE_ID",
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						// if result set is not null
						if (rs != null) {
							String[] privs = { rs.getString("PRIVILEGE_ID"), rs.getString("ANCESTOR_PRIVILEGE_ID") };
							return privs;
						}
						return null;
					}
				});
		List<String> ansPrivs = new ArrayList<String>();
		String privId = "";
		HashMap<String,List<String>> privHashMap = new HashMap<String,List<String>>();
		for (Iterator<String[]> iterPriv = rows.iterator(); iterPriv.hasNext();) {
			String[] privArr = (String[]) iterPriv.next();
			if (!((String) privArr[0]).equals(privId)) {
				if (!privId.equals(""))
					privHashMap.put(privId, ansPrivs);
				privId = (String) privArr[0];
				ansPrivs = new ArrayList<String>();
			}
			ansPrivs.add((String) privArr[1]);
		}
		if (!privId.equals(""))
			privHashMap.put(privId, ansPrivs);

		return privHashMap;
	}

	public Collection<Privilege> getPrivileges(Collection<String> colPrivilegeId, String channel) {
		return find(
				"from Privilege as privilege where privilege.privilegeId IN (" + Util.buildStringInClauseContent(colPrivilegeId)
						+ ") and privilege.serviceChannel = '" + channel + "' order by privilege.privilegeId ", Privilege.class);
	}

	private DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

}
