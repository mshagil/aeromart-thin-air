/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:58
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airsecurity.api.model.PasswordHistory;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.model.UserNotes;
import com.isa.thinair.airsecurity.api.model.UserSearchDTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * BHive Commons: User module
 * 
 * @author This is a autogenerated file
 * 
 *         UserDAO is business delegate interface for the user service apis
 * 
 */

public interface UserDAO {
	public Collection<User> getUsers();

	public Collection<User> getActiveUsers();

	public Page getUsers(int sartIndex, int noRecs);

	public Page getUsers(int sartIndex, int noRecs, List<String> orderByFieldList);

	public Collection<String> getAgentUserIDs(String agentCode);

	public Collection<User> getAgentUsers(String agentCode);

	public User getUser(String userId);

	public User getUserBasicDetails(String userId);

	public User getUser(String userId, String[] channels);

	public User getUser(String userId, boolean loadPrivileges);

	public User getUser(String userId, boolean loadPrivileges, String[] channel);

	public Collection<User> getUsers(Collection<String> colUserIds, boolean loadPrivileges);

	public void saveUser(User user);

	public void removeUser(String userId);

	public void removeUser(User user);

	public Page searchUsers(UserSearchDTO userSearchDTO, int startIndex, int noRecs);

	public Page searchUsersWithAirlineSelection(UserSearchDTO userSearchDTO, int startIndex, int noRecs);

	public Collection<User> searchUsers(List<ModuleCriterion> criteria);

	public Collection<User> searchUsers(List<ModuleCriterion> criteria, List<String> orderByFieldList);

	public Page searchUsers(List<ModuleCriterion> criteria, int startIndex, int noRecs);

	public Page searchUsers(List<ModuleCriterion> criteria, int startIndex, int noRecs, List<String> orderByFieldList);

	public User authenticate(String userId, String password) throws ModuleException;

	public Map<String, Collection<String>> getUserIds(Collection<String> colPrivilege);

	public void evictUser(User user);

	public Map<String, Collection<UserTO>> getAgentUserSortedMap(Collection<String> agentCodes);

	public Collection<String[]> getDistinctRolesForUsers(Collection<String> userCodes);

	public Collection<String> getReportingAgentCodes(String gsaCode);

	public void saveOrUpdate(Collection<User> users);

	public Collection<User> getAgentUsers(Collection<String> agentCodes);

	public void saveUserNotes(UserNotes uNotes) throws ModuleException;

	public Collection<UserNotes> getUserNotes(String userNoteType, String user) throws ModuleException;

	public Collection<UserNotes> getCheckinNotes(String userNoteType, String strFlightSegId) throws ModuleException;

	public String getDryUserEnable(String defCarrierCode) throws ModuleException;

	public Set<String> getIncludeExcludePrivilegesForDryUser(String userId, String airlineCode, String includeExculde)
			throws ModuleException;

	public void setUserNewMessageStatus(Set<String> userIds, Set<String> agentCodes, Set<String> agentTypeCodes,
			Set<String> agentPos);

	public void updateUserNewMessageStatus(String userId, String newMessageStatus);

	public boolean isNewDashBoardMsgsExists(String userId);

	/**
	 * Gets the GMT time difference saved in the database for each airport that the given user is assigned to.
	 * 
	 * @param userName
	 *            User whose GMT time difference need to be retrieved.
	 * @return the GMT time difference in minutes, returning integer can be + or - as defined in the database entry.
	 * @throws ModuleException
	 *             If any underlying operations throw an exception
	 */
	public int getGMTTimeDifferenceInMinutes(String userID);

	public String[] getMyIDUserForCarrier(String myIDCarrierCode) throws ModuleException;

	public List<User> getLCCUnpublishedUsers() throws ModuleException;

	public void updateUserVersion(String userId, long version) throws ModuleException;

	public void updateUserPublishStatus(String userId, String lccPublished);
	
	public void saveUserPasswordHistory(PasswordHistory passwordHistory);
	
	public boolean isPasswordUsed(String userID, String password) throws ModuleException;
	
	public String getUserAccountLockStatus(String userID, boolean clearLock)  throws ModuleException;
	
	public boolean isCaptchaEnableForUser(String userIP) throws ModuleException;
	
	public Collection<String> getReportingAgentCodesIncludeSubAgentLevels(String gsaCode, String agentStatus,
			boolean isPrivilegedAgent);

	public Collection<String> getAllReportingAgentCodes(String startingAgent, boolean isRetrieveAllReportingAgents);
	
	public Integer getMaxAdultCount(String userID);
	

}
