/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:58
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.ibm.icu.util.Calendar;
import com.isa.thinair.airmaster.api.util.MasterDataPublishUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airsecurity.api.dto.external.PrivilegeTO;
import com.isa.thinair.airsecurity.api.dto.external.RoleTO;
import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airsecurity.api.model.PasswordHistory;
import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.PrivilegeDependantsDTO;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.model.UserNotes;
import com.isa.thinair.airsecurity.api.model.UserSearchDTO;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airsecurity.core.audit.AuditAirSecurity;
import com.isa.thinair.airsecurity.core.bl.CheckRolesBL;
import com.isa.thinair.airsecurity.core.bl.SecurityExternalBL;
import com.isa.thinair.airsecurity.core.persistence.dao.PrivilegeDAO;
import com.isa.thinair.airsecurity.core.persistence.dao.RoleDAO;
import com.isa.thinair.airsecurity.core.persistence.dao.UserDAO;
import com.isa.thinair.airsecurity.core.service.bd.SecurityServiceDelegateImpl;
import com.isa.thinair.airsecurity.core.service.bd.SecurityServiceLocalDelegateImpl;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author Thejaka
 */
@Stateless
@RemoteBinding(jndiBinding = "SecurityService.remote")
@LocalBinding(jndiBinding = "SecurityService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SecurityServiceBean extends PlatformBaseSessionBean implements SecurityServiceDelegateImpl,
		SecurityServiceLocalDelegateImpl {

	private final Log log = LogFactory.getLog(getClass());

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Privilege getPrivilege(String key) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivilege(key);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Privilege getPrivilege(String key, String channel) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivilege(key, channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Role> getRolesByServiceChannels(Collection<String> channels) throws ModuleException {
		try {
			return lookupRoleDAO().getRolesByServiceChannels(channels);
		} catch (CommonsDataAccessException e) {
			log.error("Getting roles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Role> getDryExcludeRolesByServiceChannels(Collection<String> channels) throws ModuleException {
		try {
			return lookupRoleDAO().getDryExcludeRolesByServiceChannels(channels);
		} catch (CommonsDataAccessException e) {
			log.error("Getting DryExcludeRoles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Role> getRoles(String channel) throws ModuleException {
		try {
			return lookupRoleDAO().getRoles(channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting roles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Role> getRoles(Collection<String> colRoleIds) throws ModuleException {
		try {
			return lookupRoleDAO().getRoles(colRoleIds);
		} catch (CommonsDataAccessException e) {
			log.error("Getting roles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Role> getEditableRoles() throws ModuleException {
		try {
			return lookupRoleDAO().getEditableRoles();
		} catch (CommonsDataAccessException e) {
			log.error("Getting editable roles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Role> getActiveRoles() throws ModuleException {
		try {
			return lookupRoleDAO().getActiveRoles();
		} catch (CommonsDataAccessException e) {
			log.error("Getting active roles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page getRoles(int startIndex, int noRecs) throws ModuleException {
		try {
			return lookupRoleDAO().getRoles(startIndex, noRecs);
		} catch (CommonsDataAccessException e) {
			log.error("Getting roles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Role> searchRoles(List<ModuleCriterion> criteria) throws ModuleException {
		try {
			return lookupRoleDAO().searchRoles(criteria);
		} catch (CommonsDataAccessException e) {
			log.error("Search roles for criteria is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page searchRoles(List<ModuleCriterion> criteria, int startIndex, int noRecs) throws ModuleException {
		try {
			return lookupRoleDAO().searchRoles(criteria, startIndex, noRecs, null);
		} catch (CommonsDataAccessException e) {
			log.error("Search roles for criteria and start index is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Role getRole(String roleID) throws ModuleException {
		try {
			return lookupRoleDAO().getRole(roleID);
		} catch (CommonsDataAccessException e) {
			log.error("Getting role is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Role saveRole(Role role, Map<String, String[]> mapCustomContents) throws ModuleException {
		try {
			role = (Role) setUserDetails(role);
			lookupRoleDAO().saveRole(role);
			if (mapCustomContents != null) {
				AuditAirSecurity.doAudit(role, getUserPrincipal(), mapCustomContents);
				if ((mapCustomContents.get("granted_privileges").length > 0)
						|| (mapCustomContents.get("revoked_privileges").length > 0)) {
					mapCustomContents.put("role", new String[] { role.getRoleName() });
					CheckRolesBL.alertUserPrivilegeChanges(mapCustomContents, getUserPrincipal());
				}
			}
			return role;
		} catch (CommonsDataAccessException e) {
			log.error("Saving role is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeRole(String roleID) throws ModuleException {
		try {
			lookupRoleDAO().removeRole(roleID);
			AuditAirSecurity.doAudit(String.valueOf(roleID), AuditAirSecurity.ROLE, getUserPrincipal());

		} catch (CommonsDataAccessException e) {
			log.error("Removing role for role id is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeRole(String roleID, String channel) throws ModuleException {
		try {
			lookupRoleDAO().removeRole(roleID, channel);
			AuditAirSecurity.doAudit(String.valueOf(roleID), AuditAirSecurity.ROLE, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			log.error("Removing role for role object is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeRole(Role role) throws ModuleException {
		try {
			lookupRoleDAO().removeRole(role);
			AuditAirSecurity.doAudit(String.valueOf(role.getRoleId()), AuditAirSecurity.ROLE, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			log.error("Removing role for role object is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<User> getUsers() throws ModuleException {
		try {
			return lookupUserDAO().getUsers();
		} catch (CommonsDataAccessException e) {
			log.error("Getting users is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<User> getBasicUserInformation(Collection<String> userIDs) throws ModuleException {
		try {
			return lookupUserDAO().getUsers(userIDs, false);
		} catch (CommonsDataAccessException e) {
			log.error("Getting users is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<User> getActiveUsers() throws ModuleException {
		try {
			return lookupUserDAO().getActiveUsers();
		} catch (CommonsDataAccessException e) {
			log.error("Getting active users is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page getUsers(int sartIndex, int noRecs) throws ModuleException {
		try {
			return lookupUserDAO().getUsers(sartIndex, noRecs);
		} catch (CommonsDataAccessException e) {
			log.error("Getting users for start index is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<String> getAgentUserIDs(String agentCode) throws ModuleException {
		try {
			return lookupUserDAO().getAgentUserIDs(agentCode);
		} catch (CommonsDataAccessException e) {
			log.error("Getting user IDs for an agent code is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<User> getAgentUsers(String agentCode) throws ModuleException {
		try {
			return lookupUserDAO().getAgentUsers(agentCode);
		} catch (CommonsDataAccessException e) {
			log.error("Getting users for an agent code is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public User getUser(String userId) throws ModuleException {
		try {
			return lookupUserDAO().getUser(userId);
		} catch (CommonsDataAccessException e) {
			log.error("Getting users for a user ID is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public User getUserBasicDetails(String userId) throws ModuleException {
		try {
			return lookupUserDAO().getUserBasicDetails(userId);
		} catch (CommonsDataAccessException e) {
			log.error("Getting users for a user ID is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public User getUser(String userId, boolean loadPrivileges) throws ModuleException {
		try {
			return lookupUserDAO().getUser(userId, loadPrivileges);
		} catch (CommonsDataAccessException e) {
			log.error("Getting users for a user ID is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public UserTO getUserTO(String userId, boolean loadPrivileges) throws ModuleException {
		try {
			return SecurityExternalBL.getUserTO(userId, loadPrivileges);
		} catch (CommonsDataAccessException e) {
			log.error("Getting users for a user ID is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@SuppressWarnings("rawtypes")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveUser(User user) throws ModuleException {
		try {
			User existingUser = lookupUserDAO().getUser(user.getUserId());
			HashMap<String, Object> userData = CheckRolesBL.checkUser(user, getUserPrincipal().getUserId());

			user = (User) setUserDetails(user);
			
			boolean changePassword = (Boolean)userData.get("blnChangePass");
			
			if (user.getVersion() == -1) {
				/** Set User Account expiry data **/
				setUserLoginExpiryData(user);
				user.setServiceChannel(Constants.INTERNAL_CHANNEL);
				lookupUserDAO().saveUser(user);
			} else {
				Set<PasswordHistory> passwordHistories = existingUser.getPasswordHistories();
				if (changePassword) {
					setUserLoginExpiryData(user);
					clearUserLockData(user);
					
					if (AppSysParamsUtil.isEnablePasswordHistory()) {
						PasswordHistory passwordHistory = new PasswordHistory();
						passwordHistory.setUserId(user.getUserId());
						passwordHistory.setPassword((String)(userData.get("oldpasswd")));
						passwordHistory.setPasswordExpiryDate(new Date());						
						passwordHistories.add(passwordHistory);						
					}
					
				}
				user.setPasswordHistories(passwordHistories);
				lookupUserDAO().saveUser(user);
			}

			Map<String, String[]> mapCustomContents = null;

			if (!"".equals((String) userData.get("changedContent"))) {
				String[] arr = (userData.get("changedContent").toString()).split(",");
				mapCustomContents = new LinkedHashMap<String, String[]>();
				mapCustomContents.put("changedContent", arr);
			}

			AuditAirSecurity.doAudit(user, getUserPrincipal(), mapCustomContents);
			// AuditAirSecurity.doAudit(user, getUserPrincipal());
			if (changePassword) {
				AuditAirSecurity.doAudit(user, getUserPrincipal(), (String) userData.get("oldpasswd"));				
			}
			if (existingUser != null) {
				Collection[] colRoleChanges = CheckRolesBL.getRoleChanges(existingUser.getRoles(), user.getRoles());
				AuditAirSecurity.doAudit(user, getUserPrincipal(), colRoleChanges);

				/**
				 * AARESAA-11108 - Nafly Azhar *If roles changes should be assigned to other users as well* 1. Get all
				 * the users from the agent code 2. Create a list of roles which needs to be there including added and
				 * excluding removed 3. add the new role list to the user and save the user. 4. Call audit function
				 */

				if (user.isAssignRoleChangesToOtherUsers()) {

					if (colRoleChanges[0] != null || colRoleChanges[1] != null) {
						// Role changes exists,load the users for the agent
						Collection<User> sameAgentUsers = getAgentUsers(user.getAgentCode());
						Collection<Role> removedRoles = new ArrayList<Role>();
						Collection<Role> addedRoles = new ArrayList<Role>();
						Set<Role> assignableRoles = new HashSet<Role>();
						Set<Role> exsistingRoles = new HashSet<Role>();

						if (colRoleChanges[0] != null) {
							removedRoles.addAll(colRoleChanges[0]);
						}

						if (colRoleChanges[1] != null) {
							addedRoles.addAll(colRoleChanges[1]);
						}

						for (User sameAgentUser : sameAgentUsers) {
							if (!user.getUserId().equals(sameAgentUser.getUserId())) {
								exsistingRoles.clear();
								if (sameAgentUser.getRoles() != null && !sameAgentUser.getRoles().isEmpty()) {
									exsistingRoles.addAll(sameAgentUser.getRoles());
								}

								User exsistingUser = sameAgentUser;
								exsistingUser.getRoles().clear();

								// Add all existing roles of this user to a set
								assignableRoles.clear();
								assignableRoles.addAll(exsistingRoles);

								// If one of the removed roles are available in exsistingRoles, remove it from the set
								for (Role role : exsistingRoles) {
									for (Role removedRole : removedRoles) {
										if (removedRole.getRoleId().equals(role.getRoleId())) {
											assignableRoles.remove(role);
											break;
										}
									}

								}

								// If user does not have a newly added role. Add it to the list
								for (Role addedRole : addedRoles) {
									boolean isAdded = true;
									for (Role exsistingRole : exsistingRoles) {
										if (exsistingRole.getRoleId().equals(addedRole.getRoleId())) { // Role already
																										// available for
																										// this user
											isAdded = false;
											break;
										}
									}

									if (isAdded) {
										assignableRoles.add(addedRole);
									}
								}

								exsistingUser.getRoles().addAll(assignableRoles);
								exsistingUser = (User) setUserDetails(exsistingUser);

								lookupUserDAO().saveUser(exsistingUser);

								Collection[] sameAgendtRoleChanges = CheckRolesBL.getRoleChanges(exsistingRoles, assignableRoles);
								AuditAirSecurity.doAudit(exsistingUser, getUserPrincipal(), sameAgendtRoleChanges);
							}
						}
					}
				}
			}
		} catch (CommonsDataAccessException e) {
			log.error("Saving user is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveDashboardPref(String userId, String pref) throws ModuleException {
		try {
			User user = lookupUserDAO().getUser(userId);
			user.setDashboardPreference(pref);
			lookupUserDAO().saveUser(user);

		} catch (CommonsDataAccessException e) {
			log.error("Saving user is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Save or Update User Used only for LCC data sync. Otherwise need to encrypt password checkUser(user,
	 * getUserPrincipal().getUserId())
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveOrUpdateUser(UserTO userTO) throws ModuleException {
		try {
			String userId = userTO.getUserId();
			User existUser = lookupUserDAO().getUser(userId);
			String channel = "";

			if (existUser != null) {
				channel = BeanUtils.nullHandler(existUser.getServiceChannel());
			}

			User user = null;

			if (!channel.isEmpty()) {
				user = SecurityExternalBL.getUserForSaveOrUpdate(userTO, channel);
			} else {
				user = SecurityExternalBL.getUserForSaveOrUpdate(userTO);
			}

			// CheckRolesBL.checkUser(user, getUserPrincipal().getUserId());
			user = (User) setUserDetails(user);
			lookupUserDAO().saveUser(user);
			AuditAirSecurity.doAudit(user, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			log.error("Saving user is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Save or Update Role
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveOrUpdateRole(RoleTO roleTO) throws ModuleException {
		try {
			Role role = SecurityExternalBL.getRoleForSaveOrUpdate(roleTO);
			String serviceChannel = Constants.EXTERNAL_CHANNEL;
			role.setServiceChannel(serviceChannel);
			/**
			 * Note :Roles applying for dry users will be added to the Db manually All the roles define by the Admin UI
			 * will be applicable to normal users only
			 */
			role.setAirlineCode(AppSysParamsUtil.getDefaultAirlineIdentifierCode());
			role = (Role) setUserDetails(role);
			lookupRoleDAO().saveRole(role);
			AuditAirSecurity.doAudit(role, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			log.error("Saving or Updating role failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeUser(String userId) throws ModuleException {
		try {
			lookupUserDAO().removeUser(userId);
			AuditAirSecurity.doAudit(userId, AuditAirSecurity.USER, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			log.error("Removing user for user id is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeUser(User user) throws ModuleException {
		try {
			lookupUserDAO().removeUser(user);
			AuditAirSecurity.doAudit(user.getUserId(), AuditAirSecurity.USER, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			log.error("Removing user for user object is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Removes a user from the system
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeUser(String userId, long version) throws ModuleException {
		try {
			SecurityExternalBL.removeUser(userId, version, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			log.error("Removing user for user object is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Remove role by role id and version
	 * 
	 * @param roleID
	 * @param version
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeRole(String roleId, long version) throws ModuleException {
		try {
			SecurityExternalBL.removeRole(roleId, version, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			log.error("Removing user for user object is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page searchUsers(UserSearchDTO userSearchDTO, int startIndex, int noRecs) throws ModuleException {
		try {
			String agentCode = this.getUserPrincipal().getAgentCode();

			if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
				boolean isAirlineAgent = AppSysParamsUtil.getCarrierAgent().equals(this.getUserPrincipal().getAgentTypeCode());
				if(!isAirlineAgent){
					Collection<String> agentCodes = lookupUserDAO().getReportingAgentCodesIncludeSubAgentLevels(agentCode, "ACT", true);
					userSearchDTO.setReportingAgentCodes(agentCodes);
				}				
			} else if (AgentType.GSA.equals(this.getUserPrincipal().getAgentTypeCode())
					&& userSearchDTO.getTravelAgentCode() == null) {
				Collection<String> agentCodes = lookupUserDAO().getReportingAgentCodes(agentCode);
				// userSearchDTO.setTravelAgentCode(travelAgentCode)
				if (userSearchDTO.getReportingAgentCodes() != null && !userSearchDTO.getReportingAgentCodes().isEmpty()) {
					agentCodes.addAll(userSearchDTO.getReportingAgentCodes());
				}
				userSearchDTO.setReportingAgentCodes(agentCodes);
			}

			return lookupUserDAO().searchUsers(userSearchDTO, startIndex, noRecs);
		} catch (CommonsDataAccessException e) {
			log.error("Searching users for userSearchDTO is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page searchUsersWithAirlineSelection(UserSearchDTO userSearchDTO, int startIndex, int noRecs) throws ModuleException {
		try {
			String agentCode = this.getUserPrincipal().getAgentCode();			
			
			if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
				boolean isAirlineAgent = AppSysParamsUtil.getCarrierAgent().equals(this.getUserPrincipal().getAgentTypeCode());
				if(!isAirlineAgent){
					Collection<String> agentCodes = lookupUserDAO().getReportingAgentCodesIncludeSubAgentLevels(agentCode, "ACT", true);
					userSearchDTO.setReportingAgentCodes(agentCodes);
				}
			} else if (AgentType.GSA.equals(this.getUserPrincipal().getAgentTypeCode())
					&& userSearchDTO.getTravelAgentCode() == null) {
				Collection<String> agentCodes = lookupUserDAO().getReportingAgentCodes(agentCode);
				// userSearchDTO.setTravelAgentCode(travelAgentCode)
				if (userSearchDTO.getReportingAgentCodes() != null && !userSearchDTO.getReportingAgentCodes().isEmpty()) {
					agentCodes.addAll(userSearchDTO.getReportingAgentCodes());
				}
				userSearchDTO.setReportingAgentCodes(agentCodes);
			}

			return lookupUserDAO().searchUsersWithAirlineSelection(userSearchDTO, startIndex, noRecs);
		} catch (CommonsDataAccessException e) {
			log.error("Searching users for userSearchDTO is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<User> searchUsers(List<ModuleCriterion> criteria) throws ModuleException {
		try {
			return lookupUserDAO().searchUsers(criteria);
		} catch (CommonsDataAccessException e) {
			log.error("Searching user for criteria is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page searchUsers(List<ModuleCriterion> criteria, int sartIndex, int noRecs) throws ModuleException {
		try {
			return lookupUserDAO().searchUsers(criteria, sartIndex, noRecs);
		} catch (CommonsDataAccessException e) {
			log.error("Searching user for criteria and start index is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page searchUsers(List<ModuleCriterion> criteria, int startIndex, int noRecs, List<String> orderByFieldList)
			throws ModuleException {
		try {
			return lookupUserDAO().searchUsers(criteria, startIndex, noRecs, orderByFieldList);
		} catch (CommonsDataAccessException e) {
			log.error("Searching user for criteria, start index and order by list is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Privilege> getPrivileges(String channel) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivileges(channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privileges is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Privilege> getPrivileges(Collection<String> colPrivilegeIds, String channel) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivileges(colPrivilegeIds, channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Privilege> getActivePrivileges() throws ModuleException {
		try {
			return lookupPrivilegeDAO().getActivePrivileges();
		} catch (CommonsDataAccessException e) {
			log.error("Getting active privileges is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Privilege> getActivePrivileges(String channel) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getActivePrivileges(channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting active privileges is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Privilege> getActiveVisiblePrivileges() throws ModuleException {
		try {
			return lookupPrivilegeDAO().getActiveVisiblePrivileges();
		} catch (CommonsDataAccessException e) {
			log.error("Getting active privileges is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Privilege> getActiveVisiblePrivileges(String channel) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getActiveVisiblePrivileges(channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting active privileges is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public User authenticate(String userId, String password) throws ModuleException {
		try {
			return lookupUserDAO().authenticate(userId, password);
		} catch (CommonsDataAccessException e) {
			log.error("Authenticating user is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Privilege> getPrivilegesForRole(String roleId) throws ModuleException {
		try {
			return lookupRoleDAO().getPrivilegesForRole(roleId);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privileges for a role id is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Privilege> getPrivilegesForRole(String roleId, String channel) throws ModuleException {
		try {
			return lookupRoleDAO().getPrivilegesForRole(roleId, channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privileges for a role id is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<String, Collection<Privilege>> getPrivilegesForRoleIds(Collection<String> roleIdCollection, String channel)
			throws ModuleException {
		try {
			return lookupRoleDAO().getPrivilegesForRoleIds(roleIdCollection, channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privileges for a role id collection failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<String[]> getDistinctPrivilegesForRoles(Collection<String> roleIdCollection, String channel)
			throws ModuleException {
		try {
			return lookupRoleDAO().getDistinctPrivilegesForRoles(roleIdCollection, channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privileges for a role id collection failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public PrivilegeDependantsDTO getPrivilegeDependencies(String previlegeId, String channel) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivilegeDependencies(previlegeId, channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege dependancies for a privilege id is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HashMap<String, List<String>> getPrivilegeDependencies(Collection<String> previlegeIds, String channel)
			throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivilegeDependencies(previlegeIds, channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege dependancies for a privilege IDs is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public PrivilegeDependantsDTO getPrivilegeDependencies(String previlegeId) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivilegeDependencies(previlegeId);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege dependancies for a privilege id is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HashMap<String, List<String>> getPrivilegeDependencies(Collection<String> previlegeIds) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivilegeDependencies(previlegeIds);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege dependancies for a privilege IDs is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Return PrivilegeTOs Map
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Collection<PrivilegeTO>> getPrivilegeTOsForRoleIDs(Collection<String> colRoleIds) throws ModuleException {
		try {
			return SecurityExternalBL.getPrivilegeTOsForRoleIDs(colRoleIds);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privileges for a role id collection failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Return UserTO(s)
	 * 
	 * @param colPrivilegeIds
	 * @param loadPrivileges
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Collection<UserTO>> getUserTOs(Collection<String> colPrivilegeIds, boolean loadPrivileges)
			throws ModuleException {
		try {
			return SecurityExternalBL.getUserTOs(colPrivilegeIds, loadPrivileges);
		} catch (CommonsDataAccessException e) {
			log.error("Getting getUserTOs failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Return Collections of UserTO(s)
	 * 
	 * @param colAgentCode
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Collection<UserTO>> getAgentUserSortedMap(Collection<String> colAgentCode) throws ModuleException {
		try {
			return lookupUserDAO().getAgentUserSortedMap(colAgentCode);
		} catch (CommonsDataAccessException e) {
			log.error("Getting getAgentUserSortedMap failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Return Collection of RoleTO(s)
	 * 
	 * @param colUserCodes
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String[]> getUserRoleSortedMap(Collection<String> colUserCodes) throws ModuleException {
		try {
			return lookupUserDAO().getDistinctRolesForUsers(colUserCodes);
		} catch (CommonsDataAccessException e) {
			log.error("Getting getUserRoleSortedMap failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}

	}

	/**
	 * Return RoleTO(s)
	 * 
	 * @param criteria
	 * @param sartIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page searchRoleTOs(List<ModuleCriterion> criteria, int sartIndex, int noRecs, List<String> orderByFieldList)
			throws ModuleException {
		try {
			return SecurityExternalBL.searchRoleTOs(criteria, sartIndex, noRecs, orderByFieldList);
		} catch (CommonsDataAccessException e) {
			log.error("Search roles for criteria and start index is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Search UserTO(s)
	 * 
	 * @param userSearchDTO
	 * @param startIndex
	 * @param noRecs
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page searchUserTOs(UserSearchDTO userSearchDTO, int startIndex, int noRecs) throws ModuleException {
		try {
			return SecurityExternalBL.searchUserTOs(userSearchDTO, startIndex, noRecs);
		} catch (CommonsDataAccessException e) {
			log.error("Searching users for userSearchDTO is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Get all the active RoleTOs
	 * 
	 * @return Role collection
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<RoleTO> getActiveRoleTOs() throws ModuleException {

		String channel = Constants.EXTERNAL_CHANNEL;
		try {
			return SecurityExternalBL.getActiveRoleTOs(channel);
		} catch (CommonsDataAccessException e) {
			log.error("Getting active roles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Return RoleTO(s)
	 * 
	 * @param criteria
	 * @param sartIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public void blockUsers(Collection<String> agentCodes) throws ModuleException {
		try {
			log.debug("Going to block users");
			Collection<User> users = lookupUserDAO().getAgentUsers(agentCodes);
			if (users != null && users.size() > 0) {
				Iterator<User> userIterator = users.iterator();
				while (userIterator.hasNext()) {
					User user = (User) userIterator.next();
					user.setStatus(User.STATUS_BLOCKED);
				}
			}

			lookupUserDAO().saveOrUpdate(users);
			if (log.isDebugEnabled()) {
				log.debug("finished blocking users");
			}

		} catch (CommonsDataAccessException e) {
			log.error("blockUsers", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Save User Notes
	 * 
	 * @param uNotes
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveUserNotes(UserNotes uNotes) throws ModuleException {
		try {
			uNotes.setUserId(getUserPrincipal().getUserId());
			uNotes.setCreatedDate(new java.util.Date());
			lookupUserDAO().saveUserNotes(uNotes);
		} catch (CommonsDataAccessException e) {
			log.error("Saving user notes is failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Get user notes
	 * 
	 * @param user
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<UserNotes> getUserNotes(String userNoteType, String user) throws ModuleException {
		try {
			return lookupUserDAO().getUserNotes(userNoteType, user);
		} catch (CommonsDataAccessException e) {
			log.error("Getting user notes is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Get checkin notes
	 * 
	 * @param user
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<UserNotes> getCheckinNotes(String userNoteType, String strFlightSegId) throws ModuleException {
		try {
			return lookupUserDAO().getCheckinNotes(userNoteType, strFlightSegId);
		} catch (CommonsDataAccessException e) {
			log.error("Getting user notes is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * Return all PrivilegeTO(s)
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PrivilegeTO> getPrivilegeTOs() throws ModuleException {
		try {
			return SecurityExternalBL.getPrivilegeTOs();
		} catch (CommonsDataAccessException e) {
			log.error("Getting privileges is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	private PrivilegeDAO lookupPrivilegeDAO() {
		return (PrivilegeDAO) AirSecurityUtil.getInstance().getLocalBean("PrivilegeDAOImplProxy");
	}

	private RoleDAO lookupRoleDAO() {
		return (RoleDAO) AirSecurityUtil.getInstance().getLocalBean("RoleDAOImplProxy");
	}

	private UserDAO lookupUserDAO() {
		return (UserDAO) AirSecurityUtil.getInstance().getLocalBean("UserDAOImplProxy");
	}

	/**
	 * Return dry selling state
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getDryUserEnable(String defCarrierCode) throws ModuleException {
		try {
			return lookupUserDAO().getDryUserEnable(defCarrierCode);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieveing dry selling state is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public Collection<User> getUsersByID(Collection<String> userIDList) throws ModuleException {
		return lookupUserDAO().getUsers(userIDList, false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getExcludePrivilegesForDryUser(String userId, String opCarrierAirlineCode) throws ModuleException {
		try {
			return lookupUserDAO().getIncludeExcludePrivilegesForDryUser(userId, opCarrierAirlineCode, Role.EXCLUDE_RLOE);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieveing ExcludePrivilegesForDryUser failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getIncludePrivilegesForDryUser(String userId, String opCarrierAirlineCode) throws ModuleException {
		try {
			return lookupUserDAO().getIncludeExcludePrivilegesForDryUser(userId, opCarrierAirlineCode, Role.INCLUDE_RLOE);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieveing IncludePrivilegesForDryUser failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	public String getNextSequenceForRole() throws ModuleException {
		try {
			return SecurityExternalBL.getNextSeqNoForRole();
		} catch (CommonsDataAccessException e) {
			log.error("Retrieveing Sequence No failed ", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public Map<String, Set<String>> getAssignedAgentTypesForRoles(Collection<String> roleIds) throws ModuleException {
		try {
			return lookupRoleDAO().getAssignedAgentTypesForRoles(roleIds);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieveing Assigned agent types for role" + roleIds + "failed", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public void setUserNewMessageStatus(Set<String> userIds, Set<String> agentCodes, Set<String> agentTypeCodes,
			Set<String> agentPos) throws ModuleException {
		try {
			lookupUserDAO().setUserNewMessageStatus(userIds, agentCodes, agentTypeCodes, agentPos);
		} catch (CommonsDataAccessException e) {
			log.error("Setting new user message status failed", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public void updateUserNewMessageStatus(String userId, String newMessageStatus) throws ModuleException {
		try {
			lookupUserDAO().updateUserNewMessageStatus(userId, newMessageStatus);
		} catch (CommonsDataAccessException e) {
			log.error("Updating new user message status failed", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public boolean isNewDashBoardMsgsExists(String userId) throws ModuleException {
		try {
			return lookupUserDAO().isNewDashBoardMsgsExists(userId);
		} catch (CommonsDataAccessException e) {
			log.error("isNewDashBoardMsgsExists", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getUserGMTTimeDifference(String userName) throws ModuleException {
		try {
			return lookupUserDAO().getGMTTimeDifferenceInMinutes(userName);
		} catch (CommonsDataAccessException e) {
			log.error("getUserGMTTimeDifference", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@PermitAll
	@Override
	public String[] getMyIDUserForCarrier(String myIDCarrierCode) throws ModuleException {

		try {
			return lookupUserDAO().getMyIDUserForCarrier(myIDCarrierCode);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving MyIDUser for carrier" + myIDCarrierCode + " failed", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}

	}
	
	private void clearUserLockData(User user) {
		if (AppSysParamsUtil.isEnableAccountLock()) {
			user.setLoginAttempts(0);
			user.setAccountLockReleaseTime(null);
		}
	}
	
	private void setUserLoginExpiryData(User user) {
		if (AppSysParamsUtil.isEnablePasswordExpiry()) {
			Calendar currentTime = Calendar.getInstance();
			currentTime.add(Calendar.DAY_OF_MONTH, AppSysParamsUtil.getPasswordExpiryDays());
			user.setPasswordExpiryDate(currentTime.getTime());
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isPasswordUsed(String userId, String password) throws ModuleException {
		try {
			return lookupUserDAO().isPasswordUsed(userId, password);
		} catch (CommonsDataAccessException e) {
			log.error("Getting users account is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getUserAccountLockStatus(String userID, boolean clearLock)  throws ModuleException {
		try {
			return lookupUserDAO().getUserAccountLockStatus(userID, clearLock);
		} catch (CommonsDataAccessException e) {
			log.error("Getting user account is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<User> getLCCUnpublishedUsers() throws ModuleException {
		try {
			return lookupUserDAO().getLCCUnpublishedUsers();
		} catch (CommonsDataAccessException e) {
			log.error("Getting LCC unpublished users is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Role> getLCCUnpublishedRoles() throws ModuleException {
		try {
			return lookupRoleDAO().getLCCUnpublishedRoles();
		} catch (CommonsDataAccessException e) {
			log.error("Getting LCC unpublished Roles is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateUserVersion(String userId, long version) throws ModuleException {
		try {
			lookupUserDAO().updateUserVersion(userId, version);
		} catch (CommonsDataAccessException e) {
			log.error("Update user version is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateRoleVersion(String roleId, long version) throws ModuleException {
		try {
			lookupRoleDAO().updateRoleVersion(roleId, version);
		} catch (CommonsDataAccessException e) {
			log.error("Update role version is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public void updateRoleLCCPublishStatus(List<Role> originalRoles, List<String> updatedRoleIds) throws ModuleException {
		for (Iterator<String> iterator = updatedRoleIds.iterator(); iterator.hasNext();) {
			String roleId = iterator.next();			
			Object tmpRole = MasterDataPublishUtil.retrieveMasterDataFromUnpublishedData(roleId, originalRoles);			
			if (tmpRole != null) {
				Role previousRole = (Role) tmpRole;
				
				if (!previousRole.getStatus().equals(Role.STATUS_DELETE)) {
					Role role = lookupRoleDAO().getRole(roleId);
					if (role.getVersion() == previousRole.getVersion()) {
						lookupRoleDAO().updateRolePublishStatus(roleId, Util.LCC_PUBLISHED);
					}
				}
			}

		}
	}

	@Override
	public void updateUserLCCPublishStatus(List<User> originalUsers, List<String> updatedUserIds) throws ModuleException {
		for (Iterator<String> iterator = updatedUserIds.iterator(); iterator.hasNext();) {
			String userId = iterator.next();
			User user = lookupUserDAO().getUser(userId);
			Object tmpUser = MasterDataPublishUtil.retrieveMasterDataFromUnpublishedData(userId, originalUsers);
			if (tmpUser != null) {
				User previousUser = (User) tmpUser;
				if (user.getVersion() == previousUser.getVersion()) {
					lookupUserDAO().updateUserPublishStatus(userId, Util.LCC_PUBLISHED);
				}
			}

		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isCaptchaEnableForUser(String userIP) throws ModuleException {
		try {
			return lookupUserDAO().isCaptchaEnableForUser(userIP);
		} catch (CommonsDataAccessException e) {
			log.error("Captcha enable is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public Collection<String> getReportingAgentCodesIncludeSubAgentLevels(String agentCode, String agentStatus,
			boolean isPrivilegedAgent) throws ModuleException {

		try {
			return lookupUserDAO().getReportingAgentCodesIncludeSubAgentLevels(agentCode, agentStatus,
					isPrivilegedAgent);
		} catch (CommonsDataAccessException e) {
			log.error("Error @ getReportingAgentCodesIncludeSubAgentLevels .", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public Collection<String> getAllReportingAgentCodes(String agentCode, boolean isRetrieveAllReportingAgents) throws ModuleException {

		try {
			return lookupUserDAO().getAllReportingAgentCodes(agentCode, isRetrieveAllReportingAgents);
		} catch (CommonsDataAccessException e) {
			log.error("Error @ getReportingAgentCodesIncludeSubAgentLevels .", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}
	
	@Override
	public Integer getMaxAdultCount(String userID){
		
		try{
		
			return lookupUserDAO().getMaxAdultCount(userID);
			
		}catch(Exception e){
			
			log.error("Getting max adult count failed.", e);
			
		}
		
		return null;
		
	}

}