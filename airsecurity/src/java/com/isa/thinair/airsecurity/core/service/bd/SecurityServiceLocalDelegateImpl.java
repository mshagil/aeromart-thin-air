/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.airsecurity.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.service.SecurityBDExternal;

/**
 * @author Thejaka
 */
@Local
public interface SecurityServiceLocalDelegateImpl extends SecurityBD, SecurityBDExternal {

}
