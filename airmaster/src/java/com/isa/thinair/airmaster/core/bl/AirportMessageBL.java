package com.isa.thinair.airmaster.core.bl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.AirportMessage;
import com.isa.thinair.airmaster.api.model.AirportMsgFlight;
import com.isa.thinair.airmaster.api.model.AirportMsgOND;
import com.isa.thinair.airmaster.api.model.AirportMsgSalesChannel;
import com.isa.thinair.airmaster.api.model.I18nMessage;
import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.to.AirportMessageTO;
import com.isa.thinair.airmaster.core.persistence.dao.AirportMessageDAO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.api.util.I18nTranslationUtil.I18nMessageCategory;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author eric
 * 
 */
public class AirportMessageBL {

	private AirportMessageDAO airportMessageDao;

	private CommonMasterBD commonBD;

	private ChargeBD chargeBD;

	public AirportMessageBL() {

		this.airportMessageDao = AirmasterDAOUtil.getAirportMessageDAO();
		this.commonBD = AirmasterUtils.getCommonMasterBD();
		this.chargeBD = AirmasterUtils.getChargeBD();

	}

	/**
	 * @param airportMessageTO
	 * @param userId
	 * @throws ModuleException
	 */
	public void saveAirportMessage(AirportMessageTO airportMessageTO, String userId) throws ModuleException {
		AirportMessage airportMessage;
		if (airportMessageTO.getMessageId() != null) {
			airportMessage = AirmasterDAOUtil.getAirportMessageDAO().getAirportMessage(airportMessageTO.getMessageId());
		} else {
			airportMessage = new AirportMessage();
		}
		// save new onds to avoid the foreign key violation
		Set newOnds = this.getSelectedOndSet(airportMessageTO);
		if (newOnds != null && !newOnds.isEmpty()) {
			this.chargeBD.saveONDs(newOnds);
		}

		// update the message
		this.updateMessage(airportMessage, airportMessageTO, userId);
		// save message
		Long messageId = this.airportMessageDao.saveAirportMessage(airportMessage);

		// update message id
		airportMessageTO.setMessageId(messageId);
		// save default translations
		this.saveAirportMessageLanguageTranslations(airportMessageTO);

	}

	/**
	 * @param airportMessageTO
	 * @throws ModuleException
	 */
	public void saveAirportMessageLanguageTranslations(AirportMessageTO airportMessageTO) throws ModuleException {
		// Create the key
		String i18nMsgKey = I18nTranslationUtil.getI18nAirportMessageKey(airportMessageTO.getMessageId());

		// load airport Message
		AirportMessage airportMessage = AirmasterDAOUtil.getAirportMessageDAO()
				.getAirportMessage(airportMessageTO.getMessageId());

		// get i18n message key
		I18nMessageKey i18nMessageKey = airportMessage.getI18nMessageKey();

		if (i18nMessageKey == null) {// new record

			// since already saved messages don't have translations
			i18nMessageKey = new I18nMessageKey();
			i18nMessageKey.setI18nMsgKey(i18nMsgKey);
			i18nMessageKey.setMessageCategory(I18nMessageCategory.AIRPORT_MSG.toString());
			// set English message to all translations
			if (airportMessageTO.getMessage() != null) { //
				this.createTranlatedMessages(airportMessageTO, i18nMessageKey);
			} else {
				// this is to avoid exception when updating languages for already saved messages
				this.updateTranlatedMessages(airportMessageTO, i18nMessageKey);
			}

			// set message key to airport message
			airportMessage.setI18nMessageKey(i18nMessageKey);

		} else {
			// update message translations
			this.updateTranlatedMessages(airportMessageTO, i18nMessageKey);
		}
		// save message
		this.airportMessageDao.saveAirportMessage(airportMessage);

	}

	/**
	 * @param airportMessage
	 * @param airportMessageTO
	 * @param userId
	 */
	private void updateMessage(AirportMessage airportMessage, AirportMessageTO airportMessageTO, String userId) {
		if (airportMessage.getMessageId() == null) {
			airportMessage.setCreatedDate(new Date());
			airportMessage.setCreatedBy(userId);
		} else {
			airportMessage.setModifiedDate(new Date());
			airportMessage.setModifiedBy(userId);
		}

		if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getAirport())) {
			airportMessage.setAirport(airportMessageTO.getAirport());
		}
		if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getDepArrType())) {
			airportMessage.setDepArrType(airportMessageTO.getDepArrType());
		}
		if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getMessageId())) {
			airportMessage.setMessageId(airportMessageTO.getMessageId());
		}
		if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getMessage())) {
			airportMessage.setMessage(airportMessageTO.getMessage());
		}
		if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getMessageType())) {
			airportMessage.setMessageType(airportMessageTO.getMessageType());
		}
		if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getVersion())) {
			airportMessage.setVersion(Long.valueOf(airportMessageTO.getVersion()).longValue());
		}
		try {
			SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			SimpleDateFormat validDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getDisplayFrom())) {
				airportMessage.setDisplayFrom(displayDateFormat.parse(airportMessageTO.getDisplayFrom() + " 00:00:00"));
			}
			if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getDispalyTo())) {
				airportMessage.setDisplayTo(displayDateFormat.parse(airportMessageTO.getDispalyTo() + " 23:59:59"));
			}

			if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getValidFrom())) {
				airportMessage.setValidFrom(validDateFormat.parse(airportMessageTO.getValidFrom()));
			}

			if (PlatformUtiltiies.isNotEmptyString(airportMessageTO.getValidTo())) {
				airportMessage.setValidTo(validDateFormat.parse(airportMessageTO.getValidTo()));
			}

		} catch (ParseException e) {
			throw new RuntimeException("Exception occured while parsing Date");
		}
		this.updateFlightNumbers(airportMessage, airportMessageTO);
		this.updateSalesChannels(airportMessage, airportMessageTO);
		this.updateOnds(airportMessage, airportMessageTO);

	}

	/**
	 * @param airportMessage
	 * @param airportMessageTO
	 */
	private void updateFlightNumbers(AirportMessage airportMessage, AirportMessageTO airportMessageTO) {

		Map<String, AirportMsgFlight> savedFlightsMap = new HashMap<String, AirportMsgFlight>();
		for (Iterator<AirportMsgFlight> iterator = airportMessage.getFlights().iterator(); iterator.hasNext();) {
			AirportMsgFlight airportMsgFlight = iterator.next();
			savedFlightsMap.put(airportMsgFlight.getFlightNo(), airportMsgFlight);
		}

		if (airportMessageTO.getSelectedflights() != null && !airportMessageTO.getSelectedflights().isEmpty()) {
			String[] splitedCodes = airportMessageTO.getSelectedflights().split(",");
			// update new flight numbers
			for (int i = 0; i < splitedCodes.length; i++) {

				AirportMsgFlight airportMsgFlight = savedFlightsMap.remove(splitedCodes[i]);
				if (airportMsgFlight == null) {
					airportMsgFlight = new AirportMsgFlight();
					airportMsgFlight.setFlightNo(splitedCodes[i]);
					airportMsgFlight.setApplyStatus(airportMessageTO.getFlightApplyStatus());
					airportMessage.addFlightNumber(airportMsgFlight);
				}
				if (!airportMsgFlight.getApplyStatus().equals(airportMessageTO.getFlightApplyStatus())) {
					airportMsgFlight.setApplyStatus(airportMessageTO.getFlightApplyStatus());
				}
			}
		}
		// remove flight numbers
		for (Iterator<AirportMsgFlight> iterator = savedFlightsMap.values().iterator(); iterator.hasNext();) {
			AirportMsgFlight airportMsgFlight = iterator.next();
			airportMessage.removeFlightNumber(airportMsgFlight);
		}

	}

	/**
	 * @param airportMessage
	 * @param airportMessageTO
	 */
	private void updateSalesChannels(AirportMessage airportMessage, AirportMessageTO airportMessageTO) {

		Map<Integer, AirportMsgSalesChannel> savedSalesChannelMap = new HashMap<Integer, AirportMsgSalesChannel>();
		for (Iterator<AirportMsgSalesChannel> iterator = airportMessage.getSalesChannels().iterator(); iterator.hasNext();) {
			AirportMsgSalesChannel salesChannel = iterator.next();
			savedSalesChannelMap.put(salesChannel.getSalesChannelId(), salesChannel);
		}

		if (airportMessageTO.getSelectedsalesChannels() != null && !airportMessageTO.getSelectedsalesChannels().isEmpty()) {
			String[] splitedCodes = airportMessageTO.getSelectedsalesChannels().split(",");
			for (int i = 0; i < splitedCodes.length; i++) {

				AirportMsgSalesChannel salesChannel = savedSalesChannelMap.remove(Integer.valueOf(splitedCodes[i]));

				if (salesChannel == null) {
					AirportMsgSalesChannel airportMsgSalesChannel = new AirportMsgSalesChannel();
					airportMsgSalesChannel.setSalesChannelId(Integer.valueOf(splitedCodes[i]));
					airportMessage.addSalesChannel(airportMsgSalesChannel);
				}
			}
		}

		// remove sales channels
		for (Iterator<AirportMsgSalesChannel> iterator = savedSalesChannelMap.values().iterator(); iterator.hasNext();) {
			AirportMsgSalesChannel salesChannel = iterator.next();
			airportMessage.removeSalesChannel(salesChannel);
		}

	}

	/**
	 * @param airportMessage
	 * @param airportMessageTO
	 */
	private void updateOnds(AirportMessage airportMessage, AirportMessageTO airportMessageTO) {

		Map<String, AirportMsgOND> ondMap = new HashMap<String, AirportMsgOND>();
		for (Iterator<AirportMsgOND> iterator = airportMessage.getOnds().iterator(); iterator.hasNext();) {
			AirportMsgOND airportMsgOND = iterator.next();
			ondMap.put(airportMsgOND.getOndCode(), airportMsgOND);
		}

		if (airportMessageTO.getSelectedonds() != null && !airportMessageTO.getSelectedonds().isEmpty()) {
			String[] splitedCodes = airportMessageTO.getSelectedonds().split(",");
			for (int i = 0; i < splitedCodes.length; i++) {

				AirportMsgOND airportMsgOND = ondMap.remove(splitedCodes[i]);

				if (airportMsgOND == null) {
					airportMsgOND = new AirportMsgOND();

					airportMsgOND.setOndCode(splitedCodes[i]);
					airportMsgOND.setApplyStatus(airportMessageTO.getOndApplyStatus());
					airportMessage.addOND(airportMsgOND);
				}
				if (!airportMsgOND.getApplyStatus().equals(airportMessageTO.getOndApplyStatus())) {
					airportMsgOND.setApplyStatus(airportMessageTO.getOndApplyStatus());
				}
			}
		}

		// remove ONDs
		for (Iterator<AirportMsgOND> iterator = ondMap.values().iterator(); iterator.hasNext();) {
			AirportMsgOND airportMsgOND = iterator.next();
			airportMessage.removeOnd(airportMsgOND);
		}
	}

	/**
	 * @param airportMessageTO
	 * @param i18nMessageKey
	 * @throws ModuleException
	 */
	private void createTranlatedMessages(AirportMessageTO airportMessageTO, I18nMessageKey i18nMessageKey) throws ModuleException {
		Collection<Language> langauges = commonBD.getLanguages();

		for (Language language : langauges) {
			I18nMessage i18nMessage = new I18nMessage();
			i18nMessage.setMessageLocale(language.getLanguageCode()); // Locale.ENGLISH.getLanguage()
			String hexString = StringUtil.convertToHex(airportMessageTO.getMessage().trim());
			i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
			i18nMessageKey.addI18nMessage(i18nMessage);
		}
	}

	/**
	 * @param airportMessageTO
	 * @param i18nMessageKey
	 * @throws ModuleException
	 */
	@SuppressWarnings("unused")
	private void updateTranlatedMessages(AirportMessageTO airportMessageTO, I18nMessageKey i18nMessageKey) throws ModuleException {
		Map<String, I18nMessage> savedI18nMessages = new HashMap<String, I18nMessage>();
		for (Iterator<I18nMessage> iterator = i18nMessageKey.getI18nMessages().iterator(); iterator.hasNext();) {
			I18nMessage i18nMessage = iterator.next();
			savedI18nMessages.put(i18nMessage.getMessageLocale(), i18nMessage);
		}

		if (airportMessageTO.getSelectedLanguageTranslations() != null
				&& !airportMessageTO.getSelectedLanguageTranslations().isEmpty()) {
			String[] splitedCodes = airportMessageTO.getSelectedLanguageTranslations().split(",");
			// update language translations
			for (int i = 0; i < splitedCodes.length; i++) {
				String[] lan_Translation = splitedCodes[i].split("\\^");

				String hexString = lan_Translation[1].trim();// StringUtil.convertToHex(lan_Translation[1].trim())

				I18nMessage i18nMessage = savedI18nMessages.remove(lan_Translation[0].trim());
				if (i18nMessage == null) {
					i18nMessage = new I18nMessage();
					i18nMessage.setMessageLocale(lan_Translation[0].trim()); // Locale.ENGLISH.getLanguage()
					i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
					i18nMessageKey.addI18nMessage(i18nMessage);
				} else {
					i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
				}
			}
		} else if (airportMessageTO.getMessage() != null) { // Update english translation message when the message is
															// updated
			I18nMessage i18nMessage = savedI18nMessages.remove(Locale.ENGLISH.getLanguage());
			if (i18nMessage != null) {
				String hexString = StringUtil.convertToHex(airportMessageTO.getMessage().trim());
				i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
			}
		}
		// remove language translation,but messages are not deleted from translation table.
		for (Iterator<I18nMessage> iterator = savedI18nMessages.values().iterator(); iterator.hasNext();) {
			I18nMessage i18nMessage = iterator.next();
			// i18nMessageKey.removeI18nMessage(i18nMessage);
		}
	}

	private Set<String> getSelectedOndSet(AirportMessageTO airportMessageTO) {
		Set<String> selectedOndSet = new HashSet<String>();
		if (airportMessageTO.getSelectedonds() != null && !airportMessageTO.getSelectedonds().isEmpty()) {
			String[] splitedCodes = airportMessageTO.getSelectedonds().split(",");
			for (int i = 0; i < splitedCodes.length; i++) {
				selectedOndSet.add(splitedCodes[i]);
			}
		}
		return selectedOndSet;
	}

}
