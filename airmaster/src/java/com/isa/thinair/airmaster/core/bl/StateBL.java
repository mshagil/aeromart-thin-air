/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.core.bl;

import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airmaster.core.persistence.dao.CommonMasterDAO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class StateBL {
	
	private CommonMasterDAO commonMasterDAO;
	
	public StateBL() {
		commonMasterDAO = AirmasterDAOUtil.getCommonMasterDAO();
	}
	
	public void saveOrUpdateState(State state) throws ModuleException {
		commonMasterDAO.saveStates(state);
	}

}
