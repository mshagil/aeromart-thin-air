package com.isa.thinair.airmaster.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airmaster.api.service.SsrBD;

/**
 * @author Dhanya
 */
@Local
public interface SsrServiceLocalDelegateImpl extends SsrBD {
}
