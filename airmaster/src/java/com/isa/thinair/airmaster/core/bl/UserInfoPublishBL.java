package com.isa.thinair.airmaster.core.bl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airproxy.api.model.userInfoPublish.UserInfoPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.userInfoPublish.UserInfoPublishRSDataTO;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;

public class UserInfoPublishBL {

	private static final Log log = LogFactory.getLog(UserInfoPublishBL.class);

	public void publishUserInfoUpdates() throws ModuleException {
		List<User> unPublishedUsers = AirmasterUtils.getSecurityBD().getLCCUnpublishedUsers();
		List<Role> unPublishedRoles = AirmasterUtils.getSecurityBD().getLCCUnpublishedRoles();
		List<Agent> unPublishedAgents = AirmasterUtils.getTravelAgentBD().getLCCUnpublishedAgents();

		if ((unPublishedAgents != null && unPublishedAgents.size() > 0)
				|| (unPublishedUsers != null && unPublishedUsers.size() > 0)
				|| (unPublishedRoles != null && unPublishedRoles.size() > 0)) {
			UserInfoPublishRQDataTO userInfoPublishRQDataTO = new UserInfoPublishRQDataTO();

			if (unPublishedAgents != null && unPublishedAgents.size() > 0) {
				userInfoPublishRQDataTO.getPublishAgents().addAll(unPublishedAgents);
			}
			if (unPublishedUsers != null && unPublishedUsers.size() > 0) {
				userInfoPublishRQDataTO.getPublishUsers().addAll(unPublishedUsers);
			}
			if (unPublishedRoles != null && unPublishedRoles.size() > 0) {
				userInfoPublishRQDataTO.getPublishRoles().addAll(unPublishedRoles);
			}
			UserInfoPublishRSDataTO userInfoPublishRSDataTO = doWSCall(userInfoPublishRQDataTO);

			updateLocalInfo(userInfoPublishRSDataTO, unPublishedUsers, unPublishedRoles, unPublishedAgents);
		}
	}

	@SuppressWarnings("hiding")
	public <T> void publishSpecificUserDataUpdate(T unpublishedUserData) throws ModuleException {
		UserInfoPublishRQDataTO userInfoPublishRQDataTO = new UserInfoPublishRQDataTO();
		List<User> unPublishedUsers = null;
		List<Role> unPublishedRoles = null;
		List<Agent> unPublishedAgents = null;
		if (unpublishedUserData instanceof User) {
			User tmpUser = (User) unpublishedUserData;
			unPublishedUsers = new ArrayList<User>();
			unPublishedUsers.add(tmpUser);
			userInfoPublishRQDataTO.getPublishUsers().add(tmpUser);
		} else if (unpublishedUserData instanceof Role) {
			Role tmpRole = (Role) unpublishedUserData;
			unPublishedRoles = new ArrayList<Role>();
			unPublishedRoles.add(tmpRole);
			userInfoPublishRQDataTO.getPublishRoles().add(tmpRole);
		} else if (unpublishedUserData instanceof Agent) {
			Agent tmpAgent = (Agent) unpublishedUserData;
			unPublishedAgents = new ArrayList<Agent>();
			unPublishedAgents.add(tmpAgent);
			userInfoPublishRQDataTO.getPublishAgents().add(tmpAgent);
		}

		UserInfoPublishRSDataTO userInfoPublishRSDataTO = doWSCall(userInfoPublishRQDataTO);

		updateLocalInfo(userInfoPublishRSDataTO, unPublishedUsers, unPublishedRoles, unPublishedAgents);
	}

	private UserInfoPublishRSDataTO doWSCall(UserInfoPublishRQDataTO userInfoPublishRQDataTO) throws ModuleException {
		try {
			return AirmasterUtils.getLCCMasterDataPublisherBD().publishUserInfoToLCC(userInfoPublishRQDataTO);
		} catch (Exception exception) {
			log.error("Failed while publishing User Info");
			throw new ModuleException(exception, "lccclient.userinfo.invocation.error");
		}
	}

	private void processErrors(UserInfoPublishRSDataTO userInfoPublishRSDataTO) {
		if (userInfoPublishRSDataTO.getErrors().size() > 0) {
			if (log.isDebugEnabled())
				log.debug("############# Errors Present in LCC User Info Publishing - Displaying- ##################");
			for (Iterator<String> iterator = userInfoPublishRSDataTO.getErrors().iterator(); iterator.hasNext();) {
				String error = iterator.next();
				log.error("Error returned from LCC " + error);
			}
			if (log.isDebugEnabled())
				log.debug("############# End Displaying Errors in LCC User Info Publishing ##################");
		}
	}

	private void updateLocalInfo(UserInfoPublishRSDataTO userInfoPublishRSDataTO, List<User> unPublishedUsers,
			List<Role> unPublishedRoles, List<Agent> unPublishedAgents) throws ModuleException {
		if (userInfoPublishRSDataTO.getUpdatedAgentCodes() != null && userInfoPublishRSDataTO.getUpdatedAgentCodes().size() > 0) {
			AirmasterUtils.getTravelAgentBD().updateAgentLCCPublishStatus(unPublishedAgents,
					userInfoPublishRSDataTO.getUpdatedAgentCodes());
		}

		if (userInfoPublishRSDataTO.getUpdatedRoleIds() != null && userInfoPublishRSDataTO.getUpdatedRoleIds().size() > 0) {
			AirmasterUtils.getSecurityBD().updateRoleLCCPublishStatus(unPublishedRoles,
					userInfoPublishRSDataTO.getUpdatedRoleIds());
		}
		if (userInfoPublishRSDataTO.getUpdatedUserIds() != null && userInfoPublishRSDataTO.getUpdatedUserIds().size() > 0) {
			AirmasterUtils.getSecurityBD().updateUserLCCPublishStatus(unPublishedUsers,
					userInfoPublishRSDataTO.getUpdatedUserIds());
		}

		processErrors(userInfoPublishRSDataTO);

	}
}
