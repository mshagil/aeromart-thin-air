/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:20:13
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.dto.PaxTitleDTO;
import com.isa.thinair.airmaster.api.dto.StationDTO;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.BusinessSystemParameter;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.FamilyRelationship;
import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.airmaster.api.model.ItineraryAdvertisement;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airmaster.api.model.MealCategory;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.model.SpyRecord;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airsecurity.api.model.ParamSearchDTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.model.IpToCountry;

/**
 * BHive Commons: Nationality module
 * 
 * @author This is a autogenerated file
 */
public interface CommonMasterDAO {

	public List<Nationality> getNationalitys();

	public Page getNationalitys(int StartRec, int NumOfRecs) throws CommonsDataAccessException;

	public Nationality getNationality(int NationalityCode);

	public Nationality getNationality(String isCode);

	public RouteInfo getRouteInfo(String modelRouteId);

	/**
	 * This method is used to get Route Info information by givning the Domain Object from the Database.
	 * 
	 * @param routeInfo
	 * @return RouteInfo
	 * @author Dhanusha
	 * @since 22 Feb, 2008
	 */
	public RouteInfo getRouteInfo(RouteInfo routeInfo);

	public List<RouteInfo> getRouteInfo(String fromAirport, String toAirport);

	public void saveRouteInfo(RouteInfo object);

	public Page getRouteInfo(List<ModuleCriterion> criteria, int startRec, int noRecs);

	public void removeRouteInfo(String routeCode);

	public void removeRouteInfo(RouteInfo info);

	/**
	 * Remove RouteInfo Records
	 * 
	 * @param routIdList
	 * @author Dhanushka
	 * @since 13 Mar, 2008
	 */
	public void removeRouteInfo(Collection<Integer> routeIdList);

	public BusinessSystemParameter getBusinessSystemParameter(String carriorCode, String paramKey);

	public List<BusinessSystemParameter> getBusinessSystemParameters();

	public Page searchBusinessSystemParameters(ParamSearchDTO paramSearchDTO, int startIndex, int noRecs);

	public void saveBusinessSystemParameters(Collection<BusinessSystemParameter> parameters);

	public void saveBusinessSystemParameter(BusinessSystemParameter parameter);
	
	public void addBusinessSystemParameter(BusinessSystemParameter parameter);

	public boolean isRoutInUse(RouteInfo routeInfo);

	/**
	 * Gets the no: of valid Leg Count
	 * 
	 * @param ondCodes
	 * @return Integer
	 * @author Dhanusha
	 * @since 23 Feb, 2008
	 */
	public Integer getValidLegCount(Collection<String> ondCodes);

	/**
	 * Gets the direct routes based on the airport being either origin or destination
	 * 
	 * @param airportCode
	 * @param colkey
	 * @return Collection
	 * @author Dhanusha
	 * @since 28 Feb, 2008
	 */
	public Collection<RouteInfo> getDirectRoutes(String airportCode, String colkey);

	/**
	 * Gets two leg routes, based on the hub being either origin or destination
	 * 
	 * @param airportCode
	 * @param isOrigin
	 * @return Collection
	 * @author Dhanusha
	 * @since 28 Feb, 2008
	 */
	public Collection<RouteInfo> getTwoLegRoutes(String airportCode, boolean isOrigin);

	/**
	 * Saves all the generated routes
	 * 
	 * @param generatedRoutes
	 * @author Dhanusha
	 * @since 28 Feb, 2008
	 */
	public void saveGeneratedRoutes(Collection<RouteInfo> generatedRoutes);

	/**
	 * Gets the direct route usage count
	 * 
	 * @param routeCode
	 * @return Integer
	 * @author Dhanusha
	 * @since 28 Feb, 2008
	 */
	public Integer getDirectRouteUsageCount(String routeCode);

	/**
	 * Gets the direct route usage object collection
	 * 
	 * @param routeCode
	 * @return Collection
	 * @author Dhanushka
	 * @since 13 Mar, 2008
	 */
	public Collection<RouteInfo> getDirectRouteUsageList(String routeCode);

	/**
	 * Gets the GDS codes
	 */
	public HashMap<Integer, String> getGdsCodes();

	public int getAIGDefaultSelected(String dest);// Haider

	public String getAIGTCSuffix(String dest);// Haider

	public boolean isAIGCountryCodeActive(String airportCode);

	public void saveMeal(Meal meal) throws ModuleException;

	public void deleteMeal(Integer mealId) throws ModuleException;

	public Page<Meal> getMeals(int startrec, int numofrecs) throws ModuleException;

	public Page<Meal> getMeals(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	public Meal getMeal(String mealName) throws ModuleException;

	public Collection<Meal> getMeals() throws ModuleException;

	public Meal getMeal(Integer mealId) throws ModuleException;

	public Integer saveMealCategory(MealCategory meal) throws ModuleException;

	public void deleteMealCategory(Integer mealCategoryId) throws ModuleException;

	public Page getMealCategories(int startrec, int numofrecs) throws ModuleException;

	public Page getMealCategories(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	public MealCategory getMealCategory(String mealCategoryName) throws ModuleException;

	public Collection<MealCategory> getMealCategories() throws ModuleException;

	public MealCategory getMealCategory(Integer mealCategoryId) throws ModuleException;

	public Page getBSPReports(int startrec, int numofrecs) throws ModuleException;

	public Page getBSPReports(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	/**
	 * Retrives UnPublished Routes for LCC
	 * 
	 * @return
	 */
	public List<RouteInfo> getLCCUnpublishedRouteInfo();

	/**
	 * Retrives RouteInfo By ID
	 * 
	 * @param routeInfoID
	 * @return
	 */
	public RouteInfo getRouteInfoByID(Long routeInfoID);

	public void saveAllRouteInfo(List<RouteInfo> routeInfoList);

	public Country getCountryByCode(String countryCode);

	public String getCountryCodeByAirportCode(String airportCode);

	public void saveSpyMessages(Collection<SpyRecord> colSpyRecord);

	public Collection<SpyRecord> getSpyMessages(String auditCode);

	public String getCurrencyCodeForAirportCountry(String airportCode);

	public String getCurrencyCodeForStation(String stationCode);

	public Collection<Meal> getMeals(Collection<Integer> mealIds);

	public Collection<Meal> getMealsByMealCharge(Collection<Integer> mealChargeIds);

	public Country getCountryFromIPAddress(long originIp);

	public String getMarketingCarrierFromCountry(String countryCode);

	public Page<Baggage> getBaggages(int startrec, int numofrecs) throws ModuleException;

	public Page<Baggage> getBaggages(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	public Baggage getBaggage(Integer baggageId) throws ModuleException;

	public Integer saveBaggage(Baggage baggage, boolean isResetCOS) throws ModuleException;

	public Baggage getBaggage(String baggageName) throws ModuleException;

	public void deleteBaggage(Integer baggageId) throws ModuleException;

	public void deleteBaggageTranslation(String i18nMsgKey) throws ModuleException;

	public Collection<Baggage> getBaggages(Collection<Integer> baggageIds);

	public String getCountryByIpAddress(String ipAddress) throws ModuleException;

	public Collection<Language> getLanguages();

	public Collection<Baggage> getBaggages() throws ModuleException;

	public Collection<Baggage> getBaggagesCOS(String cabinClass, String logicalCabinClass) throws ModuleException;

	public Collection<Country> getActiveCountries();

	public Collection<Country> getBSPEnabledCountries();

	public Collection<Meal> getAllMeals(String cabinClass, String logicalCabinClass) throws ModuleException;

	/**
	 * Loads the I18nMessageKey classes form the passed primary key(ID) Set and deletes them if the exist.
	 * 
	 * @param msgKeys
	 *            The Set<String> of I18nMessageKey primary keys(IDs) (i18n_message_key)
	 */
	public void deleteI18nMessages(Set<String> msgKeys);

	/**
	 * Save or update the passed I18nMessageKey set.
	 * 
	 * @param msgKeys
	 *            The Set<I18nMessageKey> of I18nMessageKey classes to be saved
	 */
	public void saveOrUpdateI18nMessageKeys(Collection<I18nMessageKey> msgKeys);

	/**
	 * Retrieves and returns a I18nMessageKey from the given primary key(ID)
	 * 
	 * @param messageKey
	 *            The primary key (ID) of the I18nMessageKey to be retrieved
	 * @return The retrieved I18nMessageKey or null if the I18nMessageKey with specific ID doesn't exist.
	 */
	public I18nMessageKey getI18nMessageKey(String messageKey);

	/**
	 * Returns translated messages in one language for the passed Set of i18n message keys.
	 * 
	 * @param language
	 *            Language the translations are required
	 * @param keySet
	 *            Set of keys to which the translations are required
	 * @return Results containing the translations in the form of Map<i18n message key,translated message>
	 * @throws CommonsDataAccessException
	 *             If any of the underlying operations throws an exception
	 */
	public Map<String, String> getTranslatedMessages(String language, Set<String> keySet) throws CommonsDataAccessException;

	/**
	 * Returns all the language translations for LogicalCabinClass
	 * 
	 * @return Map that will hold the translations in the format of Map<i18n message key,Map<language,message content>>
	 */
	public Map<String, Map<String, String>> getTranslatedMessagesForLCC();

	/**
	 * Returns all the language translations for the given message keys
	 * 
	 * @param messageKeys
	 *            The i18n message keys whose translations need to be retrieved
	 * 
	 * @return Map that will hold the translations in the format of Map<i18n message key,Map<language,message content>>
	 */
	public Map<String, Map<String, String>> getTranslatedMessagesForKeys(Collection<String> messageKeys);

	/**
	 * Returns airport codes for a given country
	 * 
	 * @param countryCode
	 * @return
	 */
	public Collection<String> getAirportCodesForCountry(String countryCode);

	/**
	 * Returns all the language translations based on the category
	 * 
	 * @param category
	 * @return
	 */
	public Map<String, Map<String, String>> getTranslatedMessages(String category);

	public Collection<Country> getPnrGovEnabledCountries();

	public Map<String, String> getFlightNumbers() throws ModuleException;

	public Map<String, String> getBookingClasses() throws ModuleException;

	public Map<String, String> getAgents() throws ModuleException;

	public Map<String, String> getBookingClassTypes(Collection<String> bcCodes) throws ModuleException;
	
	public Territory getTerritory(String territoryCode) throws ModuleException;

	public List<IpToCountry> getUnsynedIPToCountryData() throws ModuleException;

	public void saveOrUpdateAllIPToCountries(List<IpToCountry> ipToCountries);
	
	public List<PaxTitleDTO> getPaxTitleList() throws ModuleException;

	public Map<String, List<PaxTitleDTO>> getPaxTypeWiseTitleList() throws ModuleException;

	public void saveAdvertisement(ItineraryAdvertisement advertisement) throws ModuleException;
	
	public void deleteAdvertisement(Integer advertisementId) throws ModuleException;
	
	public void deleteAdvertisementOND(Integer advertisementId) throws ModuleException;
	
	public String getAdvertisementSegmentWise(String ONDsegment) throws ModuleException;
	
	public String getAdvertisementImage(String ONDsegment, String Language) throws ModuleException;
	
	public ItineraryAdvertisement getAdvertisement(String advertisementTitle, String advertisementLanguage) throws ModuleException;
	
	public ItineraryAdvertisement getAdvertisement(Integer advertisementId) throws ModuleException;
	
	public Page<ItineraryAdvertisement> getAdvertisements(int startrec, int numofrecs) throws ModuleException;

	public Page<ItineraryAdvertisement> getAdvertisements(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;
	
	public Page<State> getStates(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;
	
	public Page<State> getStates(int startrec, int numofrecs) throws ModuleException;
	
	public State getState(Integer stateId) throws ModuleException;
	
	public State getState(String stateCode) throws ModuleException;
	
	public List<State> getStates(String countryCode) throws ModuleException;
	
	public void saveStates(State state) throws ModuleException;
	
	public void deleteState(Integer stateId) throws ModuleException;
	
	public Map<String,List<State>> getCountryWiseStates() throws ModuleException;
	
	public StationDTO getStationInfo(String stateCode) throws ModuleException;
	
	public State getTaxApplicableState(int chargeRateID) throws ModuleException;
	
	public ChargeRate getChargeRateValue(int chargeRateID) throws ModuleException;

	public String getCurrencyCodeForCityCountry(String airportCode) throws ModuleException;
	
	public Page<City> getCities(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;
	
	public Page<City> getCities(int startrec, int numofrecs) throws ModuleException;
	
	public City getCity(Integer cityId) throws ModuleException;
	
	public List<City> getCities(String countryCode) throws ModuleException;
	
	public void saveOrUpdateCity(City city) throws ModuleException;
	
	public void deleteCity(Integer cityId) throws ModuleException;
	
	public Map<String,List<City>> getCountryWiseCities() throws ModuleException;
	
	public City getCity(String cityCode, String countryCode) throws ModuleException;

	public List<FamilyRelationship> getFamilyRelationships();
}