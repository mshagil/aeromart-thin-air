package com.isa.thinair.airmaster.core.publisher;

import com.isa.thinair.airmaster.api.model.EventProfile;

public interface Publisher {
	
	public void publish(EventProfile eventProfile);

}
