/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:36
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airmaster.api.dto.CountryDetailsDTO;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**********
 * 
 * BHive Commons: Country module
 * 
 * @author This is a autogenerated file
 * 
 *         CountryDAO is business delegate interface for the country service apis
 * 
 * 
 *********/

public interface CountryDAO {
	public List<Country> getCountrys();

	public Page getCountrys(int StartRec, int NumOfRecs, String countryCode) throws CommonsDataAccessException;

	public Country getCountry(String CountryCode);

	public void saveCountry(Country country);

	public void removeCountry(String CountryCode);

	public void removeCountry(Country country);

	public List<Country> getActiveCountries();

	public List<Country> getCountryCodesForRegions(List<String> regionCodes);

	public Country getCountryByIsoCode(String isoCode);
	
	public List<CountryDetailsDTO> getCountryDetailsList() throws ModuleException;

    public CountryDetailsDTO getCountryByPhoneCode(String phoneCode);


}
