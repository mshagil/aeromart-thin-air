/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.airmaster.core.persistence.dao.HubDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ondpublish.OndLocationDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/*******************************************************************************
 * Description : Hub Info. Hibernate DAO Implementation
 * 
 * @author : Nadika Dhanusha Mahatantila
 * @since : 25 Feb, 2008
 * @isa.module.dao-impl dao-name="HubDAO"
 * 
 ******************************************************************************/

public class HubDAOImpl extends PlatformBaseHibernateDaoSupport implements HubDAO {

	private static Log log = LogFactory.getLog(HubDAOImpl.class);

	/**
	 * This method is used to search Hub Info. information from the Database.
	 * 
	 * @param searchCriteria
	 * @param startIndex
	 * @param pageSize
	 * @return Page
	 */
	public Page getHubDetails(List<ModuleCriterion> searchCriteria, int startIndex, int pageSize) {

		List<String> orderByList = new ArrayList<String>();

		try {

			orderByList.add("airportCode");

			return getPagedData(searchCriteria, startIndex, pageSize, HubInfo.class, orderByList);

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/**
	 * This method is used to Save/Update information to the Database.
	 * 
	 * @param hubInfo
	 */
	public void saveHubInfo(HubInfo hubInfo) {
		hibernateSaveOrUpdate(hubInfo);
		log.debug("Saving Hub Info. " + hubInfo.getAirportCode());
	}

	/**
	 * This method is used to Delete information from the Database.
	 * 
	 * @param hubInfo
	 */
	public void removeHubInfo(HubInfo hubInfo) {
		delete(hubInfo);
	}

	/**
	 * This method is used to get Hub Info. information by givning the Domain Object from the Database.
	 * 
	 * @param hubInfo
	 * @return HubInfo
	 */
	public HubInfo getHubInfo(HubInfo hubInfo) {
		return (HubInfo) get(HubInfo.class, hubInfo.getHubId());
	}

	/**
	 * Gets the Valid Hub codes
	 * 
	 * @param airportCodes
	 * @return Collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> getValidHubCodes(Collection<String> airportCodes) {
		String hql = "SELECT DISTINCT(airportCode) " + "FROM 	HubInfo " + "WHERE 	airportCode IN ( :airportCodes ) ";

		return getSession().createQuery(hql).setParameterList("airportCodes", airportCodes).list();
	}

	public HubInfo getHubInfo(String airportCode, String ibCarrier, String obCarrier) {
		Object result = null;
		if (ibCarrier.equals(obCarrier)) {
			String qry = "from HubInfo where airportCode=? and ibCarrierCode is null and obCarrierCode is null";
			result = getSession().createQuery(qry).setString(0, airportCode).uniqueResult();
		} else {
			String qry = "from HubInfo where airportCode=? and ibCarrierCode=? and obCarrierCode=?";
			result = getSession().createQuery(qry).setString(0, airportCode).setString(1, ibCarrier).setString(2, obCarrier)
					.uniqueResult();
		}

		if (result != null) {
			return (HubInfo) result;
		} else {
			return null;
		}
	}

	/**
	 * Gets the Active Hub codes
	 * 
	 * @return Collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> getActiveHubCodes() {
		String hql = "SELECT DISTINCT(airportCode) " + "FROM 	HubInfo " + "WHERE 	status = 'ACT' ";

		return getSession().createQuery(hql).list();
	}
	
	/**
	 * Gets the Active Hub codes
	 * 
	 * @return Collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<OndLocationDTO> getActiveHubDetails() {
		List<OndLocationDTO> hubDetails = new ArrayList<>();
		String hql = "SELECT DISTINCT(ci.cityCode) FROM HubInfo hi,  City ci, Airport ai "
				+ "WHERE 	hi.status = 'ACT' AND hi.airportCode = ai.airportCode AND ai.cityId = ci.cityId "
				+ "AND  hi.airportCode in (:hubList)";
		Collection<String> airportHubCodes = getActiveHubCodes();
		Collection<String> cityHubCodes = getSession().createQuery(hql).setParameterList("hubList", airportHubCodes).list();
		for (String airportHubCode : airportHubCodes) {
			hubDetails.add(new OndLocationDTO(airportHubCode, false));
		}
		for (String cityHubCode : cityHubCodes) {
			hubDetails.add(new OndLocationDTO(cityHubCode, true));
		}
		return hubDetails;
	}

}
