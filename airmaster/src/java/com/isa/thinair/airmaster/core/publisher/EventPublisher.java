package com.isa.thinair.airmaster.core.publisher;

import com.isa.thinair.airmaster.api.model.EventProfile;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.EventAuditor;

public class EventPublisher extends AbstractEventPublisher {

	private static final String OPERATION_CODE = "PUBLISHER_EVENT";

	@Override
	public void publish(EventProfile eventProfile) {

		boolean eventPublishable = AppSysParamsUtil.isEnableEventPublication() && isEnabledEventPublish();

		if (eventPublishable) {
			try {
				getEventPublisherTemplate().convertAndSend(eventProfile.getTopicName(), eventProfile.getEvent());
			} catch (Exception ex) {
				EventAuditor.audit(OPERATION_CODE, ex);
			}
		}
	}
}
