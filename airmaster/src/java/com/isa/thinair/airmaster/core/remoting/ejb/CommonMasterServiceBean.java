/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airmaster.api.criteria.DashBoardMsgSearchCriteria;
import com.isa.thinair.airmaster.api.dto.CurrencyExchangeRateDTO;
import com.isa.thinair.airmaster.api.dto.MealCategoryDTO;
import com.isa.thinair.airmaster.api.dto.PaxTitleDTO;
import com.isa.thinair.airmaster.api.dto.StationDTO;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.BusinessSystemParameter;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.airmaster.api.model.FamilyRelationship;
import com.isa.thinair.airmaster.api.model.ItineraryAdvertisement;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airmaster.api.model.MealCategory;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.model.SpyRecord;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.api.to.BaggageTO;
import com.isa.thinair.airmaster.api.to.DashboardMsgTO;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airmaster.core.bl.AirmasterDAOUtil;
import com.isa.thinair.airmaster.core.bl.BaggageBusinessLayer;
import com.isa.thinair.airmaster.core.bl.CommonMasterBL;
import com.isa.thinair.airmaster.core.bl.CurrencyBL;
import com.isa.thinair.airmaster.core.bl.ItineraryAdvertisementBL;
import com.isa.thinair.airmaster.core.bl.MasterDataPublishBL;
import com.isa.thinair.airmaster.core.bl.MealBL;
import com.isa.thinair.airmaster.core.bl.OndPublishBL;
import com.isa.thinair.airmaster.core.bl.StateBL;
import com.isa.thinair.airmaster.core.bl.UserInfoPublishBL;
import com.isa.thinair.airmaster.core.bl.exrateautomation.ExRateAutomaterBL;
import com.isa.thinair.airmaster.core.service.bd.CommonMasterServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.CommonMasterServiceLocalDelegateImpl;
import com.isa.thinair.airmaster.core.util.RouteInfoPublishUtil;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airsecurity.api.model.ParamSearchDTO;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.dto.AirportFlightDetail;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishedTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Mohamed Nasly
 * @author Byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "CommonMasterService.remote")
@LocalBinding(jndiBinding = "CommonMasterService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class CommonMasterServiceBean extends PlatformBaseSessionBean implements CommonMasterServiceDelegateImpl,
		CommonMasterServiceLocalDelegateImpl {

	private static Log log = LogFactory.getLog(CommonMasterServiceBean.class);

	MealBL mealBL;

	/**
	 * @throws ModuleException
	 */
	@Override
	public void saveCurrency(Currency currency) throws ModuleException {
		try {
			currency = (Currency) setUserDetails(currency);

			if (currency.getExchangeRates() != null) {
				CurrencyExchangeRate curExRate = null;
				for (CurrencyExchangeRate currencyExchangeRate : currency.getExchangeRates()) {
					curExRate = currencyExchangeRate;
					if (curExRate.getIsModified() || curExRate.getVersion() < 0) {
						setUserDetails(curExRate);
					}
				}
			}

			AuditAirMaster.doAudit(currency, getUserPrincipal());
			getCurrencyBL().saveOrUpdateCurrency(currency);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Saving currency [" + currency != null ? currency.getCurrencyCode() : "" + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				String exceptionCode = ((((CommonsDataAccessException) ex).getExceptionCode() != null))
						? (((CommonsDataAccessException) ex).getExceptionCode())
						: "module.dataaccess.exception";
				throw new ModuleException(ex, exceptionCode, "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public void deleteCurrency(String currencyCode) throws ModuleException {
		try {
			getCurrencyBL().deleteCurrency(currencyCode);
			AuditAirMaster.doAudit(currencyCode, AuditAirMaster.CURRENCY, getUserPrincipal());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Deleting currency [" + currencyCode + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public void deleteCurrency(Currency currency) throws ModuleException {
		try {
			getCurrencyBL().deleteCurrency(currency);
			AuditAirMaster.doAudit(currency.getCurrencyCode(), AuditAirMaster.CURRENCY, getUserPrincipal());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Deleting currency [" + currency != null ? currency.getCurrencyCode() : "" + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public List<Currency> getCurrencies() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCurrencyDAO().getCurrencies();
		} catch (Exception ex) {
			log.error("Retrieving currencies failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public List<Currency> getActiveCurrencies() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCurrencyDAO().getActiveCurrencies();
		} catch (Exception ex) {
			log.error("Retrieving active currencies failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	public Page getCurrencies(int startrec, int numofrecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCurrencyDAO().getCurrencies(startrec, numofrecs);
		} catch (Exception ex) {
			log.error("Retrieving currencies failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public Page getCurrencies(List<ModuleCriterion> list, int startRec, int noRecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCurrencyDAO().getCurrencies(list, startRec, noRecs);
		} catch (Exception ex) {
			log.error("Retrieving currencies failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public Collection<Currency> getCurrencies(boolean bookVisibility) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCurrencyDAO().getCurrencies(bookVisibility);
		} catch (Exception ex) {
			log.error("Retrieving currencyies failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public Currency getCurrency(String currencyCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCurrencyDAO().getCurrency(currencyCode);
		} catch (Exception ex) {
			log.error("Retrieving currency [" + currencyCode + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * Returns the currency exchange rate
	 * 
	 * @param currencyCode
	 * @param exRateEffectiveDate
	 * @return {@link CurrencyExchangeRate}
	 * @throws ModuleException
	 */
	@Override
	public CurrencyExchangeRate getExchangeRate(String currencyCode, Date exRateEffectiveDate) throws ModuleException {
		try {
			CurrencyExchangeRate currencyExchangeRate = AirmasterDAOUtil.getCurrencyDAO().getExchangeRate(currencyCode,
					exRateEffectiveDate);

			if (currencyExchangeRate == null) {
				throw new ModuleException("airmaster.exchangerate.undefined");
			}

			return currencyExchangeRate;
		} catch (Exception ex) {
			log.error("Retrieving exchange rate [" + currencyCode + "," + exRateEffectiveDate + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * Returns the exchange rate
	 * 
	 * @param fromCurrCode
	 * @param toCurrCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public BigDecimal getExchangeRateValue(String fromCurrCode, String toCurrCode, Date effectiveDate, Boolean isPurchaseExRate)
			throws ModuleException {
		try {
			BigDecimal exchangeRate = getCurrencyBL().getExchangeRateValue(fromCurrCode, toCurrCode, effectiveDate,
					isPurchaseExRate);

			if (exchangeRate == null) {
				throw new ModuleException("airmaster.exchangerate.undefined");
			}

			return exchangeRate;
		} catch (Exception ex) {
			log.error("Retrieving exchange rate [" + fromCurrCode + "," + toCurrCode + "," + effectiveDate + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public Nationality getNationality(int nationalityId) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getNationality(nationalityId);
		} catch (Exception ex) {
			log.error("Retrieving nationality [" + nationalityId + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public Nationality getNationality(String isoCode) throws ModuleException {
		try {
			return isoCode == null ? null : AirmasterDAOUtil.getCommonMasterDAO().getNationality(isoCode);
		} catch (Exception ex) {
			log.error("Retrieving nationality [" + isoCode + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public List<Nationality> getNationalities() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getNationalitys();
		} catch (Exception ex) {
			log.error("Retrieving nationalities failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public Page getNationalities(int startrec, int numofrecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getNationalitys(startrec, numofrecs);
		} catch (Exception ex) {
			log.error("Retrieving nationalities failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Page getRouteInfo(List<ModuleCriterion> criteria, int startRec, int noRecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getRouteInfo(criteria, startRec, noRecs);
		} catch (Exception ex) {
			log.error("Retrieving route information failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "airmaster.route.routenotfound", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public RouteInfo getRouteInfo(String citypairId) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getRouteInfo(citypairId);
		} catch (Exception ex) {
			log.error("Retrieving route information [" + citypairId + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "airmaster.route.routenotfound", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public List<RouteInfo> getRouteInfo(String fromAirport, String toAirport) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getRouteInfo(fromAirport, toAirport);
		} catch (Exception ex) {
			log.error("Retrieving route information [" + fromAirport + "," + toAirport + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "airmaster.route.routenotfound", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void saveRouteInfo(RouteInfo routeInfo) throws ModuleException {
		try {
			AuditAirMaster.doAudit(routeInfo, getUserPrincipal());
			getCommonMasterBL().saveRouteInfo(routeInfo, getUserPrincipal().getUserId());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Saving route information [" + routeInfo != null
					? routeInfo.getFromAirportCode()
					: "" + "," + routeInfo != null ? routeInfo.getToAirportCode() : "" + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void removeRouteInfo(String routeCode) throws ModuleException {
		try {
			AuditAirMaster.doAudit(routeCode, AuditAirMaster.ROUTE_INFO, getUserPrincipal());
			getCommonMasterBL().removeRouteInfo(routeCode);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Removing route information [" + routeCode + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void removeRouteInfo(RouteInfo routeInfo) throws ModuleException {
		try {
			AuditAirMaster.doAudit(routeInfo.getRouteCode(), AuditAirMaster.ROUTE_INFO, getUserPrincipal());
			getCommonMasterBL().removeRouteInfo(routeInfo);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Removing route information [" + routeInfo != null ? routeInfo.getRouteCode() : "" + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public BusinessSystemParameter getBusinessSystemParameter(String carriorCode, String paramKey) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getBusinessSystemParameter(carriorCode, paramKey);
		} catch (Exception ex) {
			log.error("Retrieving parameter [" + carriorCode + "," + paramKey + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public List<BusinessSystemParameter> getBusinessSystemParameters() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getBusinessSystemParameters();
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Page searchBusinessSystemParameters(ParamSearchDTO paramSearchDTO, int startIndex, int noRecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().searchBusinessSystemParameters(paramSearchDTO, startIndex, noRecs);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void saveBusinessSystemParameter(BusinessSystemParameter parameter, String appCode) throws ModuleException {
		try {
			BusinessSystemParameter oldParameter = AirmasterDAOUtil.getCommonMasterDAO().getBusinessSystemParameter(
					parameter.getCarrierCode(), parameter.getParamKey());

			Audit audit = new Audit();
			audit.setTimestamp(new Date());
			LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
			UserPrincipal principal = getUserPrincipal();
			audit.setAppCode(appCode);

			if (principal != null) {
				audit.setUserId(principal.getUserId());
			} else {
				audit.setUserId("");
			}
			
			if(oldParameter == null && parameter.getVersion() < 0) {
				contents.put("param key", parameter.getParamKey());
				contents.put("param value", parameter.getParamValue());	
				contents.put("description", parameter.getDescription());
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_APP_PARAM));
				AirmasterDAOUtil.getCommonMasterDAO().addBusinessSystemParameter(parameter);
			} else {
				contents.put("param key", parameter.getParamKey());
				contents.put("old param value", oldParameter.getParamValue());
				contents.put("new param value", parameter.getParamValue());
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_APP_PARAM));
				AirmasterDAOUtil.getCommonMasterDAO().saveBusinessSystemParameter(parameter);
			}
			AirmasterUtils.getAuditorBD().audit(audit, contents);

			if (CommonsServices.getGlobalConfig().isCacheAppParamsInCachingDB()
					&& CommonsServices.getAerospikeCachingService().isCachingDBEnabled()) {
				CommonsServices.getAerospikeCachingService().syncAppParameterToCache(parameter.getParamKey(),
						parameter.getParamValue(), parameter.getCarrierCode());
			}

		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Saving parameter [" + parameter != null ? parameter.getParamId() : "" + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void saveBusinessSystemParameters(Collection<BusinessSystemParameter> parameters) throws ModuleException {
		try {
			AirmasterDAOUtil.getCommonMasterDAO().saveBusinessSystemParameters(parameters);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Saving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public HashMap<Integer, String> getGdsCodes() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getGdsCodes();
		} catch (Exception ex) {
			log.error("Retrieving GDS codes failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * Generates the routes, if there is a hub in the leg
	 * 
	 * @param routeInfo
	 * @throws ModuleException
	 * 
	 * @author Dhanusha
	 * @since 28 Feb, 2008
	 */
	@Override
	@TransactionTimeout(1000)
	public void generateRoutes(RouteInfo routeInfo) throws ModuleException {
		try {
			getCommonMasterBL().generateRoutes(routeInfo, getUserPrincipal().getUserId());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Generating routes failed [" + routeInfo != null ? routeInfo.getRouteCode() : "" + "] failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Collection<CurrencyExchangeRate> getAllUpdatedCurr(Date date) {
		return getCurrencyBL().getAllUpdatedCurr(date);
	}

	@Override
	public Collection<CurrencyExchangeRate> getAllUpdatedCurrForCharges(Date date) throws ModuleException {
		return getCurrencyBL().getAllUpdatedCurrForCharges(date);
	}

	@Override
	public Collection<CurrencyExchangeRate> getAllUpdatedCurrFares(Date date) throws ModuleException {
		return getCurrencyBL().getAllUpdatedCurrFares(date);
	}

	@Override
	public void saveOrUpdateCurrencyExRate(Collection<CurrencyExchangeRate> colCurrExRate) throws ModuleException {
		try {
			getCurrencyBL().saveOrUpdateCurrencyExRate(colCurrExRate);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("saveOrUpdateCurrencyExRate Failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	private CurrencyBL getCurrencyBL() {
		return new CurrencyBL();
	}

	private CommonMasterBL getCommonMasterBL() {
		return new CommonMasterBL();
	}

	/**
	 * @author Haider
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 */
	@Override
	public int getAIGDefaultSelected(String dest) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getAIGDefaultSelected(dest);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @author Haider
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 */
	@Override
	public String getAIGTCSuffix(String dest) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getAIGTCSuffix(dest);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(10000)
	public void saveMeal(Meal meal) throws ModuleException {
		try {
			meal = (Meal) setUserDetails(meal);
			AuditAirMaster.doAudit(meal, getUserPrincipal());
			new MealBL().saveOrUpdateMeal(meal);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void deleteMeal(Integer mealId) throws ModuleException {
		try {
			AuditAirMaster.doAudit(mealId.toString(), AuditAirMaster.MEAL, getUserPrincipal());
			AirmasterDAOUtil.getCommonMasterDAO().deleteMeal(mealId);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Page<Meal> getMeals(int startrec, int numofrecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getMeals(startrec, numofrecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Page<Meal> getMeals(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getMeals(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Collection<Meal> getMeals() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getMeals();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Collection<Meal> getAllMeals(String cabinClass, String logicalCabinClass) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getAllMeals(cabinClass, logicalCabinClass);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Meal getMeal(String mealName) throws ModuleException {
		try {

			return AirmasterDAOUtil.getCommonMasterDAO().getMeal(mealName);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Integer saveMealCategory(MealCategory mealCategory) throws ModuleException {
		try {
			if ((mealCategory.getMealCategoryId() != null) && (mealCategory.getMealCategoryId() > 0)) {
				MealCategory existMealCat = AirmasterDAOUtil.getCommonMasterDAO().getMealCategory(
						mealCategory.getMealCategoryId());
				mealCategory.setI18nMessageKey(existMealCat.getI18nMessageKey());
			}
			mealCategory = (MealCategory) setUserDetails(mealCategory);
			AuditAirMaster.doAudit(mealCategory, getUserPrincipal());
			return AirmasterDAOUtil.getCommonMasterDAO().saveMealCategory(mealCategory);

		} catch (Exception ex) {
			log.error("Saving meal category failed", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void deleteMealCategory(Integer mealCategoryId) throws ModuleException {
		try {
			// TODO - airadmin auditing.
			AuditAirMaster.doAudit(mealCategoryId.toString(), AuditAirMaster.MEAL_CATEGORY, getUserPrincipal());
			AirmasterDAOUtil.getCommonMasterDAO().deleteMealCategory(mealCategoryId);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Page getMealCategories(int startrec, int numofrecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getMealCategories(startrec, numofrecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Page getMealCategories(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getMealCategories(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Collection<MealCategory> getMealCategories() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getMealCategories();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public MealCategory getMealCategory(String mealCategoryName) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getMealCategory(mealCategoryName);
		} catch (Exception ex) {
			log.error("Retrieving meal categories failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public List<RouteInfo> getLCCUnPublishedRouteInfo() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getLCCUnpublishedRouteInfo();
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public RouteInfo getRouteInfoByID(Long routeInfoID) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getRouteInfoByID(routeInfoID);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void updateRouteInfoLCCPublishStatus(List<RouteInfo> originalRI, List<String> updatedRouteInfoIDList)
			throws ModuleException {
		String updatedRIID = null;
		RouteInfo previousRI = null;
		try {
			for (String string : updatedRouteInfoIDList) {
				updatedRIID = string;
				try {
					RouteInfo rfInDB = this.getRouteInfoByID(Long.valueOf(updatedRIID));
					previousRI = RouteInfoPublishUtil.retriveObjectFromUnPublishedRouteInfo(updatedRIID, originalRI);

					// Check for the version of the recently retrieved RouteInfo
					// obj and the RouteInfo obj retrieved
					// before the WS call.
					if (previousRI.getVersion() == rfInDB.getVersion()) {
						// if the versions are the same the object can be flag
						// as "LCC PublisheD" otherwise can be
						// ignored
						rfInDB.setIsLCCPubilshed(RouteInfo.LCC_PUBLISHED_TRUE);
						AirmasterDAOUtil.getCommonMasterDAO().saveRouteInfo(rfInDB);
					}
				} catch (Exception e) {
					// ignore error and try to update the other RouteInfo objs
					log.error("Error while publishing LCC Status");
				}
			}
		} catch (Exception e) {
			throw new ModuleException(e, "module.genericexception", "airmaster.desc");
		}
	}

	@Override
	public boolean isAIGCountryCodeActive(String airportCode) throws ModuleException {
		try {
			// tune provide insurance for all countries
			if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.TUNE)) {
				return true;
			} else {
				return AirmasterDAOUtil.getCommonMasterDAO().isAIGCountryCodeActive(airportCode);
			}
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Country getCountryByName(String countryCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCountryByCode(countryCode);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public String getCountryCodeByAirportCode(String airportCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCountryCodeByAirportCode(airportCode);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}


	@Override
	public Collection<Country> getActiveCountries() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getActiveCountries();
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Collection<Country> getBSPCountries() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getBSPEnabledCountries();
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Country getBSPCountryByCode(String countryCode) throws ModuleException {
		try {
			Country country = AirmasterDAOUtil.getCommonMasterDAO().getCountryByCode(countryCode);
			if (country != null && Country.STATUS_ACTIVE.equals(country.getStatus()) && "Y".equals(country.getBspEnabled())) {
				return country;
			}
			return null;
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Page getDashBoardMessages(DashBoardMsgSearchCriteria dashBoardMsgSearchCriteria, int start, int totalRec)
			throws ModuleException {
		try {
			return AirmasterDAOUtil.getDashboardMessageDAO().searchDashboardMessages(dashBoardMsgSearchCriteria, start, totalRec);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void saveDashboardMsg(DashboardMsgTO dashboardMessageTO) throws ModuleException {
		try {
			DashboardMessage current = null;
			if (dashboardMessageTO.getMessageID() != null) {
				current = AirmasterDAOUtil.getDashboardMessageDAO().getDashboardMessage(dashboardMessageTO.getMessageID());
			}

			// set new message exists if dashboard message add/edit from admin interface
			dashboardMessageTO.setNewMessageExist(DashboardMessage.NEW_MESSAGE);

			if (dashboardMessageTO.getMessageID() == null) {
				Integer key = AirmasterDAOUtil.getDashboardMessageDAO().saveMessage(
						dashboardMessageTO.parsePprimitiveDomainObj(current));
				DashboardMessage sentObj = dashboardMessageTO.parseDomainObj(current, key);

				AirmasterDAOUtil.getDashboardMessageDAO().saveAgentsForMessage(sentObj.getAgents());
				AirmasterDAOUtil.getDashboardMessageDAO().saveAgentTypesForMessage(sentObj.getAgentType());
				AirmasterDAOUtil.getDashboardMessageDAO().savePOSForMessage(sentObj.getPos());
				AirmasterDAOUtil.getDashboardMessageDAO().saveUsersForMessage(sentObj.getUser());

			} else {
				AirmasterDAOUtil.getDashboardMessageDAO().saveMessage(
						dashboardMessageTO.parseDomainObj(current, current.getMessageID()));
			}
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public List<DashboardMessage> getNewDashbrdMsgIDs() throws ModuleException {
		return AirmasterDAOUtil.getDashboardMessageDAO().getNewDashbrdMsgIDs();
	}

	@Override
	public void updateDashboardNewMessageStatus(List<Integer> msgIds, String msgStatus) throws ModuleException {
		AirmasterDAOUtil.getDashboardMessageDAO().updateDashboardNewMessageStatus(msgIds, msgStatus);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveSpyMessages(Collection<SpyRecord> colSpyRecord) throws ModuleException {
		try {
			Collection<SpyRecord> toSaveSpyRecords = new ArrayList<SpyRecord>();

			if (colSpyRecord != null && colSpyRecord.size() > 0) {
				for (SpyRecord spyRecord : colSpyRecord) {
					String spyCode = PlatformUtiltiies.nullHandler(spyRecord.getAuditCode());
					if (spyCode.length() > 0) {
						Collection<SpyRecord> relavantSpyRecords = AirmasterDAOUtil.getCommonMasterDAO().getSpyMessages(spyCode);
						int size = (relavantSpyRecords != null) ? relavantSpyRecords.size() : 0;

						if (spyRecord.getMaximumAttempts() > size) {
							spyRecord.setCurrentAttempt(size + 1);
							toSaveSpyRecords.add(spyRecord);
						}
					}
				}
			}

			if (toSaveSpyRecords.size() > 0) {
				AirmasterDAOUtil.getCommonMasterDAO().saveSpyMessages(toSaveSpyRecords);
			}
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public String getCurrencyCodeForAirportCountry(String airportCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCurrencyCodeForAirportCountry(airportCode);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
	
	@Override
	public String getCurrencyCodeForCityCountry(String cityCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCurrencyCodeForCityCountry(cityCode);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public String getCurrencyCodeForStation(String stationCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCurrencyCodeForStation(stationCode);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	// exchange rate automation call
	@Override
	public void updateExchangeRate() throws ModuleException {
		ExRateAutomaterBL exRateAutomaterBL = new ExRateAutomaterBL();
		exRateAutomaterBL.executeAutomateExRate(getUserPrincipal());
	}

	// Writing generated javascript string in frontend to the backend
	@Override
	public void publishOndListAsJavascript(OndPublishedTO ondList) throws ModuleException {

		OndPublishBL ondPublishBL = new OndPublishBL(false);
		ondPublishBL.generateJavascriptForOndPublish(ondList);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public OndPublishedTO getOndListForSystem() throws ModuleException {
		OndPublishBL ondPublishBL = new OndPublishBL(true);
		return ondPublishBL.getOndListForSystem();
	}

	@Override
	public void publishMasterDataUpdates() throws ModuleException {

		MasterDataPublishBL masterDataPublishBL = new MasterDataPublishBL();
		masterDataPublishBL.publishMasterDataUpdates();
	}

	@Override
	public void publishUserInfoUpdates() throws ModuleException {

		UserInfoPublishBL userInfoPublishBL = new UserInfoPublishBL();
		userInfoPublishBL.publishUserInfoUpdates();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public <T> void publishSpecificUserDataUpdate(T unpublishedUserData) throws ModuleException {
		UserInfoPublishBL userInfoPublishBL = new UserInfoPublishBL();
		userInfoPublishBL.publishSpecificUserDataUpdate(unpublishedUserData);
	}

	@Override
	public Collection<Language> getLanguages() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getLanguages();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Collection<Meal> getMeals(Collection<Integer> mealIds) throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getMeals(mealIds);
	}

	@Override
	public Collection<Meal> getMealsByMealCharge(Collection<Integer> mealChargeIds) throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getMealsByMealCharge(mealChargeIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Country getCountryFromIPAddress(long originIp) throws ModuleException {
		return new CommonMasterBL().getCountryByIpAddress(originIp);
	}

	@Override
	public String getMarketingCarrierFromCountry(String countryCode) throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getMarketingCarrierFromCountry(countryCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getCountryByIpAddress(String ipAddress) throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getCountryByIpAddress(ipAddress);
	}

	@Override
	public Page<Baggage> getBaggages(int startrec, int numofrecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getBaggages(startrec, numofrecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Page<Baggage> getBaggages(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getBaggages(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveBaggage(Baggage baggage, BaggageTO baggageTO) throws ModuleException {
		try {
			baggage = (Baggage) setUserDetails(baggage);
			AuditAirMaster.doAudit(baggage, getUserPrincipal());
			new BaggageBusinessLayer().saveOrUpdateBaggage(baggage, baggageTO);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Baggage getBaggage(String baggageName) throws ModuleException {
		try {

			return AirmasterDAOUtil.getCommonMasterDAO().getBaggage(baggageName);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void deleteBaggage(Integer baggageId) throws ModuleException {
		try {
			AuditAirMaster.doAudit(baggageId.toString(), AuditAirMaster.BAGGAGE, getUserPrincipal());

			Baggage baggage = AirmasterDAOUtil.getCommonMasterDAO().getBaggage(baggageId);

			AirmasterDAOUtil.getCommonMasterDAO().deleteBaggage(baggageId);
			if (baggage.getI18nMessageKey() != null) {
				String i18nMsgKey = baggage.getI18nMessageKey().getI18nMsgKey();
				AirmasterDAOUtil.getCommonMasterDAO().deleteBaggageTranslation(i18nMsgKey);
			}
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Collection<Baggage> getBaggages(Collection<Integer> baggageIds) throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getBaggages(baggageIds);
	}

	@Override
	public Collection<Baggage> getBaggages() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getBaggages();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Collection<Baggage> getBaggagesCOS(String cabinClass, String logicalCabinClass) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getBaggagesCOS(cabinClass, logicalCabinClass);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public void saveMealCatLanTranslation(MealCategoryDTO mealCatDTO) throws ModuleException {
		try {
			getMealBL().saveMealCatLanguageTranslation(mealCatDTO);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	private MealBL getMealBL() {
		if (mealBL == null) {
			mealBL = new MealBL();
		}
		return mealBL;
	}

	@Override
	public void saveDashboardMsg(List<DashboardMsgTO> dashboardMessageTOs) throws ModuleException {

		for (DashboardMsgTO dashboardMsgTO : dashboardMessageTOs) {

			try {
				DashboardMessage current = null;
				if (dashboardMsgTO.getMessageID() != null) {
					current = AirmasterDAOUtil.getDashboardMessageDAO().getDashboardMessage(dashboardMsgTO.getMessageID());
				}

				// set new message exists if dashboard message add/edit from admin interface
				dashboardMsgTO.setNewMessageExist(DashboardMessage.NEW_MESSAGE);

				if (dashboardMsgTO.getMessageID() == null) {
					Integer key = AirmasterDAOUtil.getDashboardMessageDAO().saveMessage(
							dashboardMsgTO.parsePprimitiveDomainObj(current));
					DashboardMessage sentObj = dashboardMsgTO.parseDomainObj(current, key);

					AirmasterDAOUtil.getDashboardMessageDAO().saveAgentsForMessage(sentObj.getAgents());
					AirmasterDAOUtil.getDashboardMessageDAO().saveAgentTypesForMessage(sentObj.getAgentType());
					AirmasterDAOUtil.getDashboardMessageDAO().savePOSForMessage(sentObj.getPos());
					AirmasterDAOUtil.getDashboardMessageDAO().saveUsersForMessage(sentObj.getUser());

				} else {
					AirmasterDAOUtil.getDashboardMessageDAO().saveMessage(
							dashboardMsgTO.parseDomainObj(current, current.getMessageID()));
				}
			} catch (Exception ex) {
				this.sessionContext.setRollbackOnly();
				if (ex instanceof CommonsDataAccessException) {
					throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
				} else {
					throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
				}
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteI18nMessages(LogicalCabinClass lcc) throws ModuleException {
		try {
			getCommonMasterBL().deleteI18nMessages(lcc);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveI18nMessageKeys(LogicalCabinClass lcc) throws ModuleException {
		try {
			getCommonMasterBL().saveI18nMessageKeys(lcc);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateI18nMessageKeys(LogicalCabinClass lcc) throws ModuleException {
		try {
			getCommonMasterBL().updateI18nMessageKeys(lcc);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getTranslatedMessages(String language, Set<String> keySet) throws ModuleException {
		try {
			return getCommonMasterBL().getTranslatedMessages(language, keySet);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Map<String, String>> getTranslatedMessagesForLCC() throws ModuleException {
		try {
			return getCommonMasterBL().getTranslatedMessagesForLCC();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Map<String, String>> getTranslatedMessagesForKeys(Collection<String> messageyKeys) throws ModuleException {
		try {
			return getCommonMasterBL().getTranslatedMessagesForKeys(messageyKeys);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveTranslations(Map<String, Map<String, String>> translations) throws ModuleException {
		try {
			getCommonMasterBL().saveTranslations(translations);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveTranslations(Map<String, String> translations, String i18nMessageKey, String i18nMessageCategory)
			throws ModuleException {
		try {
			getCommonMasterBL().saveTranslations(translations, i18nMessageKey, i18nMessageCategory);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public void saveTranslationWithoutMessageKey(long id, Map<String, String> messageKeys) throws ModuleException {
		try {
			getCommonMasterBL().saveTranslationWithoutMessageKey(id, messageKeys);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public Map<String, Map<String, String>> getTranslations(String category) throws ModuleException {
		try {
			return getCommonMasterBL().getTranslatedMessages(category);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public Collection<String> getAirportCodesForCountry(String countryCode) throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getAirportCodesForCountry(countryCode);
	}

	@Override
	public Collection<Country> getPnrGovEnabledCountries() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getPnrGovEnabledCountries();
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Map<String, String> getFlightNumbers() throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getFlightNumbers();
	}

	@Override
	public Map<String, String> getBookingClasses() throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getBookingClasses();
	}

	@Override
	public Map<String, String> getBookingClassTypes(Collection<String> bcCodes) throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getBookingClassTypes(bcCodes);
	}

	@Override
	public Map<String, String> getAgents() throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getAgents();
	}

	@Override
	public Territory getTerritory(String territoryCode) throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getTerritory(territoryCode);
	}
	
	public List<CurrencyExchangeRateDTO> getExchangeRates() throws ModuleException{
		return AirmasterDAOUtil.getCurrencyDAO().getExchangeRates();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SYSTEM getSearchSystem(List<OriginDestinationInformationTO> ondInfoTOList, String requestingCarrier) throws ModuleException  {
		return getCommonMasterBL().getSearchSystem(ondInfoTOList, requestingCarrier);
	}

	public List<PaxTitleDTO> getPaxTitleList() throws ModuleException{
		return AirmasterDAOUtil.getCommonMasterDAO().getPaxTitleList();
	}

	public Map<String, List<PaxTitleDTO>> getPaxTypeWiseTitleList() throws ModuleException {
		return AirmasterDAOUtil.getCommonMasterDAO().getPaxTypeWiseTitleList();
	}

	public List<AirportMessageDisplayDTO> airportMessages(List<AirportFlightDetail> airportFlightDetails) {
		return CommonsServices.getGlobalConfig().airportMessages(airportFlightDetails);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveAdvertisement(ItineraryAdvertisement advertisement) throws ModuleException {
		try {
			advertisement = (ItineraryAdvertisement) setUserDetails(advertisement);
			AuditAirMaster.doAudit(advertisement, getUserPrincipal());
			new ItineraryAdvertisementBL().saveOrUpdateAdvertisement(advertisement);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
	
	@Override
	public String getAdvertisementSegmentWise(String OND) throws ModuleException {
		try {

			return AirmasterDAOUtil.getCommonMasterDAO().getAdvertisementSegmentWise(OND);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
	
	@Override
	public ItineraryAdvertisement getAdvertisement(Integer advertisementId) throws ModuleException {
		try {

			return AirmasterDAOUtil.getCommonMasterDAO().getAdvertisement(advertisementId);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
	
	@Override
	public ItineraryAdvertisement getAdvertisement(String advertisementTitle, String advertisementLanguage) throws ModuleException {
		try {

			return AirmasterDAOUtil.getCommonMasterDAO().getAdvertisement(advertisementTitle,advertisementLanguage);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
	
	@Override
	public Page<ItineraryAdvertisement> getAdvertisements(int startrec, int numofrecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getAdvertisements(startrec, numofrecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Page<ItineraryAdvertisement> getAdvertisements(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getAdvertisements(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}
	
	@Override
	public void deleteAdvertisement(Integer advertisementId) throws ModuleException {
		try {
			AuditAirMaster.doAudit(advertisementId.toString(), AuditAirMaster.ITINERARY_ADVERTISEMENT, getUserPrincipal());
			AirmasterDAOUtil.getCommonMasterDAO().deleteAdvertisement(advertisementId);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
	
	@Override
	public String getAdvertisementImage(String ONDsegment, String Language) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getAdvertisementImage(ONDsegment, Language);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Page<State> getStates(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getStates(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public State getState(Integer stateId) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getState(stateId);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public List<State> getStates(String countryCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getStates(countryCode);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void saveStates(State state) throws ModuleException {
		try {
			state = (State) setUserDetails(state);
			AuditAirMaster.doAudit(state, getUserPrincipal());
			new StateBL().saveOrUpdateState(state);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void deleteState(Integer stateId) throws ModuleException {
		try {
			AuditAirMaster.doAudit(stateId.toString(), AuditAirMaster.STATE, getUserPrincipal());
			AirmasterDAOUtil.getCommonMasterDAO().deleteState(stateId);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}

	}

	@Override
	public Page<State> getStates(int startrec, int numofrecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getStates(startrec, numofrecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public State getState(String stateCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getState(stateCode);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public State getTaxApplicableState(int chargeRateID) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getTaxApplicableState(chargeRateID);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public ChargeRate getChargeRateValue(int chargeRateID) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getChargeRateValue(chargeRateID);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Map<String, List<State>> getCountryWiseStates() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCountryWiseStates();
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public StationDTO getStationInfo(String stateCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getStationInfo(stateCode);
		} catch (Exception ex) {
			log.error("Retrieving currencies failed", ex);
			throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
		}
	}
	
	@Override
	public void saveCity(City city) throws ModuleException {
		try {
			city = (City) setUserDetails(city);
			AuditAirMaster.doAudit(city, getUserPrincipal());
			AirmasterDAOUtil.getCommonMasterDAO().saveOrUpdateCity(city);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void deleteCity(Integer cityId) throws ModuleException {
		try {
			AuditAirMaster.doAudit(cityId.toString(), AuditAirMaster.CITY, getUserPrincipal());
			AirmasterDAOUtil.getCommonMasterDAO().deleteCity(cityId);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Page<City> getCities(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCities(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Page<City> getCities(int startrec, int numofrecs) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCities(startrec, numofrecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public City getCity(Integer cityId) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCity(cityId);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public List<City> getCities(String countryCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCities(countryCode);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public Map<String, List<City>> getCountryWiseCities() throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCountryWiseCities();
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
	
	@Override
	public City getCity(String cityCode, String countryCode) throws ModuleException {
		try {
			return AirmasterDAOUtil.getCommonMasterDAO().getCity(cityCode, countryCode);
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
	
	/**
	 * @throws ModuleException
	 */
	@Override
	public List<FamilyRelationship> getFamilyRelationships() throws ModuleException {
		try {

			return AirmasterDAOUtil.getCommonMasterDAO().getFamilyRelationships();
		} catch (Exception ex) {
			log.error("Retrieving family relationships failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airmaster.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}
}