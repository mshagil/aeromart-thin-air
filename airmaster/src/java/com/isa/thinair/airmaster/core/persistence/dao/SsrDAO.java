package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.List;
import java.util.Set;

import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.SSRPaxSegmentDTO;
import com.isa.thinair.airmaster.api.dto.SSRSearchDTO;
import com.isa.thinair.airmaster.api.dto.SSRTemplateSearchCriteria;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRConstraints;
import com.isa.thinair.airmaster.api.model.SSRSubCategory;
import com.isa.thinair.airmaster.api.model.SSRTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**********
 * 
 * BHive: SSR module
 * 
 * @author Dhanya
 * 
 *         SsrDAO is business delegate interface for the SSR service apis
 * 
 * 
 *********/

public interface SsrDAO {

	public List<SSR> getSSRs();

	public Page getSSRs(int StartRec, int NumOfRecs, int ssrCategoryId) throws CommonsDataAccessException;

	public SSR getSSR(int id);

	public Integer saveSSR(SSR ssr);

	public void saveSSRs(List<Integer> ssrIds, boolean isIncrement);

	public void removeSSR(int id);

	public void removeSSR(SSR ssr);

	public List<SSR> getActiveSSRs();

	public List<SSR> getActiveSSRs(String cabinClass, String logicalCabinClass, Integer ssrCategoryId);

	public List<SSR> getSSR(String ssrCode);

	public List<SpecialServiceRequestDTO> getActiveSSRsByFltSegId(Integer flightSegId, String segmentCode, String ssrCategory,
			String salesChannelCode, boolean checkSSRInventory, String selectedLanguage, String cabinClass,
			String logicalCabinClass, boolean isModifyAnci, int cutoverTime, boolean skipCutoverValidation, boolean isAddEditAnci,
			boolean hasPrivModifySSRWithinBuffer, boolean isUserDefinedSsrOnly);

	public List<SSRPaxSegmentDTO> getSSRPaxSegmentList(String pnr);

	public List<SpecialServiceRequestDTO> getAvailableSSRs(Integer flightSegmentId, String flightSegment, String salesChannel,
			boolean checkInventory, boolean isModifyAnci, int cutoverTime, boolean skipCutoverValidation);

	public List<AirportServiceDTO> getAvailableAirportServices(String airport, String airportType, AppIndicatorEnum appIndicator,
			int cutoverTime, int ssrCategory, String selectedLanguage, String cabinClass, String logicalCabinClass,
			Integer bundledServicePeriodId, Set<Integer> freeApsIds, boolean isAddEditAnci);

	public List<AirportServiceDTO> getSelectedAirportServices(String airport, long cutovertime, int ssrCategory,
			boolean skipCutoverValidation);

	public SSRSubCategory getSSRSubCategory(int id);

	public SSRConstraints getSSRConstraint(int id);

	public void removeApplicableAirports(int appConsId);

	public void removeApplicableOnds(int appConsId) throws CommonsDataAccessException;

	public Page getSSRs(SSRSearchDTO ssrSearchDTO, int startRec, int noRecs);

	public int getSSRCount(int ssrCategory);

	public Integer getSSRCategoryFromSSrId(Integer ssrId);

	/*
	 * This method will return list of SSRs having >= provided sortOrder value
	 */
	public List<SSR> getSSRList(int startSortOrder, int endSortOrder);

	// ############ SSR Template Related methods############

	public void saveTemplate(SSRTemplate ssrTemplate) throws ModuleException;

	public SSRTemplate loadSSRTemplate(int templateId) throws ModuleException;

	public void deleteTemplate(SSRTemplate ssrTemplate) throws ModuleException;

	public Page<SSRTemplate> getSSRTemplates(SSRTemplateSearchCriteria criteria, int startRec, int noRecs);

	public Integer getTemplateIdForFlightSegment(int flightSegId);
	
	public boolean isUserDefinedSSR(String ssrCode);
}
