/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.dto.CurrencyExchangeRateDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.core.persistence.dao.CurrencyDAO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * @author Mohamed Nasly
 * @author Byorn
 * 
 * @isa.module.dao-impl dao-name="CurrencyDAO"
 */
public class CurrencyDAOImpl extends PlatformBaseHibernateDaoSupport implements CurrencyDAO {

	private Log log = LogFactory.getLog(CurrencyDAOImpl.class);

	/**
	 * Retrieve all the currencies.
	 */
	public List<Currency> getCurrencies() {
		return find("from Currency", Currency.class);
	}

	/**
	 * Retrieve currencies, paged method.
	 */
	public Page getCurrencies(int startRecIndex, int numOfRecs) {
		List<String> orderByList = new ArrayList<String>();
		orderByList.add("currencyCode");
		orderByList.add("currencyDescriprion");
		return getPagedData(null, startRecIndex, numOfRecs, Currency.class, orderByList);
	}

	/**
	 * Retrieve currencies filtered by criteria, paged method.
	 */
	public Page getCurrencies(List<ModuleCriterion> criteria, int startRecIndex, int numOfRecs) {
		List<String> orderByList = new ArrayList<String>();
		orderByList.add("currency.currencyCode");
		orderByList.add("currency.currencyDescriprion");
		String[] ssls = getHQLForCurrencies(criteria, orderByList);

		String hql = ssls[0];
		String hqlCount = ssls[1];

		Query query = getSession().createQuery(hql).setFirstResult(startRecIndex).setMaxResults(numOfRecs);

		Query queryCount = getSession().createQuery(hqlCount);

		try {
			if (criteria != null) {
				Iterator<ModuleCriterion> siter = criteria.iterator();
				ModuleCriterion mc = null;
				while (siter.hasNext()) {
					mc = (ModuleCriterion) siter.next();
					if (mc != null) {
						query.setParameter(mc.getFieldName(), mc.getValue().get(0));
						queryCount.setParameter(mc.getFieldName(), mc.getValue().get(0));
					}
				}
			}

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}
		log.debug("Page getCharges with criteria");

		/** The duplicate records will be filtered out **/
		/** HIBERNATE WONT return distinct records when using distinct keyword with left outer join **/
		// Set distinctCount = new HashSet(queryCount.list());

		// Set distinctResults = new HashSet(query.list());

		return new Page(queryCount.list().size(), startRecIndex, startRecIndex + numOfRecs, query.list());

		// return getPagedData(criteria, startRecIndex, numOfRecs, Currency.class, orderByList);
	}

	/**
	 * Retrieve unique currency by CurrencyCode.
	 */
	public Currency getCurrency(String currencyCode) {
		return (Currency) get(Currency.class, currencyCode);
	}

	/**
	 * Save currency.
	 */
	public void saveCurrency(Currency currency) {
		currency.setCurrencyCode(currency.getCurrencyCode().toUpperCase());
		hibernateSaveOrUpdate(currency);
	}

	/**
	 * Delete a unique currency by CurrencyCode
	 */
	public void removeCurrency(String currencyCode) {
		log.warn("Removing currency [" + currencyCode + "]");
		Object currency = load(Currency.class, currencyCode);
		delete(currency);

	}

	/**
	 * Retrieve all the active currencies.
	 */
	public List<Currency> getActiveCurrencies() {
		return find("from Currency curr where curr.status = ?", Currency.STATUS_ACTIVE, Currency.class);
	}

	/**
	 * Delete a currency specifying Currency instance.
	 */
	public void removeCurrency(Currency currency) {
		delete(currency);
	}

	/**
	 * Retrieve all the currencies visible (or not visible) for bookings.
	 */
	public List<Currency> getCurrencies(boolean bookVisibility) {
		Integer bookVisibilityFlag = bookVisibility == true ? new Integer(Currency.BO0K_VISIBILITY_TRUE) : new Integer(
				Currency.BO0K_VISIBILITY_FALSE);
		return find("from Currency curr where curr.bookVisibililty = ?", bookVisibilityFlag, Currency.class);
	}

	/**
	 * Save/Update list of exchange rates
	 * 
	 * @param currencyExRates
	 */
	public void saveOrUpdateCurrencyExRates(Collection<CurrencyExchangeRate> currencyExRates) {
		hibernateSaveOrUpdateAll(currencyExRates);
	}

	/**
	 * Returns exchange rate for the specified date and currency. Returns null if no applicable exchange rate available.
	 * 
	 * @param currencyCode
	 *            currency code
	 * @param date
	 *            exchange rate effective date
	 * @return {@link BigDecimal} the exchange rate
	 */
	@SuppressWarnings("unchecked")
	public BigDecimal getExchangeRateValue(String currencyCode, Date date, Boolean isPurchaseExRate) {
		// String hql = "SELECT exRate.exchangeRate FROM CurrencyExchangeRate exRate "
		String hql = "SELECT " + (isPurchaseExRate ? " exRate.exrateCurToBaseNumber " : " exRate.exrateBaseToCurNumber ")
				+ " FROM CurrencyExchangeRate exRate "/** JIRA : 3087 - Lalanthi */
				+ " WHERE :effDate BETWEEN exRate.effectiveFrom AND " + " exRate.effectiveTo " + " AND exRate.status = :status "
				+ " AND exRate.currency.currencyCode = :currencyCode " + " AND exRate.currency.status = :currStatus ";
		Query hQuery = getSession().createQuery(hql);
		hQuery.setParameter("currencyCode", currencyCode);
		hQuery.setString("status", "ACT");
		hQuery.setTimestamp("effDate", date);
		hQuery.setString("currStatus", "ACT");

		List<BigDecimal> resultsList = hQuery.list();

		BigDecimal exchangeRate = null;
		if (resultsList != null && resultsList.size() == 1) {
			exchangeRate = (BigDecimal) resultsList.get(0);
		}
		return exchangeRate;
	}

	/**
	 * Returns CurrencyExchangeRate for the specified date and currency. Return null if no applicable exchange rate
	 * available.
	 * 
	 * @param currencyCode
	 * @param exRateEffectiveDate
	 * @return {@link CurrencyExchangeRate}
	 */
	@SuppressWarnings("unchecked")
	public CurrencyExchangeRate getExchangeRate(String currencyCode, Date exRateEffectiveDate) {
		String hql = "SELECT exRate FROM CurrencyExchangeRate exRate " + " WHERE :effDate BETWEEN exRate.effectiveFrom AND "
				+ " exRate.effectiveTo " + " AND exRate.status = :status " + " AND exRate.currency.currencyCode = :currencyCode "
				+ " AND exRate.currency.status = :currencyStatus ";
		// NOTE : will not set xbe ibe visibility conditions here as this can be used in back end modules as well
		Query hQuery = getSession().createQuery(hql);
		hQuery.setParameter("currencyCode", currencyCode);
		hQuery.setString("status", "ACT");
		hQuery.setTimestamp("effDate", exRateEffectiveDate);
		hQuery.setString("currencyStatus", Currency.STATUS_ACTIVE);

		List<CurrencyExchangeRate> resultsList = hQuery.list();
		CurrencyExchangeRate currencyExchangeRate = null;

		if (resultsList != null && resultsList.size() == 1) {
			currencyExchangeRate = (CurrencyExchangeRate) resultsList.get(0);
			/*
			 * FIXME following is done to force loading currency intance hibernatedoclet shipped with xdoclet 1.2.3 does
			 * not support lazy attribute for many-to-one mapping
			 */
			currencyExchangeRate.getCurrency().getCurrencyDescriprion();
		}

		return currencyExchangeRate;
	}

	/**
	 * Method will return (1) the Main HQL with the Where statement (2) the HQL for extracting the total count (3) a
	 * boolean "y/n" to see if date search has been selected String[]
	 * 
	 * @param SearchCriteria
	 * @param orderbyfields
	 * @return
	 */
	private String[] getHQLForCurrencies(List<ModuleCriterion> criteria, List<String> orderbyfields) {
		String hqlCount = "select distinct currency.currencyCode from Currency currency left join currency.exchangeRates currencyExchangeRate";

		String hql = "select distinct currency from Currency currency left join currency.exchangeRates currencyExchangeRate";

		String criteriaHql = "";// where chargerates.chargeCode=charge.chargeCode ";

		String hasDateSearch = "n";
		/** --> NOTE: if user leaves from-date and to-date null, i.e no search selection for dates **/

		boolean blnboth = false;
		boolean blnFrom = false;
		boolean blnTo = false;

		if (criteria != null) {
			Iterator<ModuleCriterion> siter = criteria.iterator();
			ModuleCriterion mc = null;
			while (siter.hasNext()) {
				mc = (ModuleCriterion) siter.next();
				if (mc != null) {
					if (mc.getFieldName().equals("effectiveFrom")) {
						blnFrom = true;
						hasDateSearch = "y";
					}
					if (mc.getFieldName().equals("effectiveTo")) {
						blnTo = true;
						hasDateSearch = "y";
					}
				}
			}
		}
		if (blnTo && blnFrom) {
			blnboth = true;
		}

		if (criteria != null) {
			Iterator<ModuleCriterion> siter = criteria.iterator();
			ModuleCriterion mc = null;
			while (siter.hasNext()) {
				mc = (ModuleCriterion) siter.next();
				if (mc != null) {
					if (!mc.getFieldName().equals("effectiveFrom") && !mc.getFieldName().equals("effectiveTo")) {
						if (criteriaHql.equals("")) {
							criteriaHql = criteriaHql + " where currency." + mc.getFieldName() + " like :" + mc.getFieldName();
						} else {
							criteriaHql = criteriaHql + " and  currency." + mc.getFieldName() + " like :" + mc.getFieldName();
						}
					} else if (blnboth) {
						if (criteriaHql.equals("")) {
							criteriaHql = criteriaHql
									+ " where ((currencyExchangeRate.effectiveFrom>=:effectiveFrom and "
									+ " currencyExchangeRate.effectiveTo>=:effectiveTo and currencyExchangeRate.effectiveFrom<=:effectiveTo) or "
									+ " (currencyExchangeRate.effectiveFrom<=:effectiveFrom and "
									+ " currencyExchangeRate.effectiveTo<=:effectiveTo and currencyExchangeRate.effectiveTo>=:effectiveFrom) or"
									+ " (currencyExchangeRate.effectiveFrom<=:effectiveFrom and "
									+ " currencyExchangeRate.effectiveTo>=:effectiveTo) or"
									+ " (currencyExchangeRate.effectiveFrom>=:effectiveFrom and "
									+ " currencyExchangeRate.effectiveTo<=:effectiveTo)) ";
						} else {
							criteriaHql = criteriaHql
									+ " and ((currencyExchangeRate.effectiveFrom>=:effectiveFrom and "
									+ " currencyExchangeRate.effectiveTo>=:effectiveTo and currencyExchangeRate.effectiveFrom<=:effectiveTo) or "
									+ " (currencyExchangeRate.effectiveFrom<=:effectiveFrom and "
									+ " currencyExchangeRate.effectiveTo<=:effectiveTo and currencyExchangeRate.effectiveTo>=:effectiveFrom) or"
									+ " (currencyExchangeRate.effectiveFrom<=:effectiveFrom and "
									+ " currencyExchangeRate.effectiveTo>=:effectiveTo) or"
									+ " (currencyExchangeRate.effectiveFrom>=:effectiveFrom and "
									+ " currencyExchangeRate.effectiveTo<=:effectiveTo)) ";
						}
					} else if (blnFrom) {
						if (criteriaHql.equals("")) {
							criteriaHql = criteriaHql + " where ((currencyExchangeRate.effectiveFrom>=:effectiveFrom or "
									+ " currencyExchangeRate.effectiveTo>=:effectiveFrom)) ";
						} else {
							criteriaHql = criteriaHql + " and ((currencyExchangeRate.effectiveFrom>=:effectiveFrom or "
									+ " currencyExchangeRate.effectiveTo>=:effectiveFrom)) ";
						}
						hasDateSearch = "y";

					} else if (blnTo) {
						if (criteriaHql.equals("")) {
							criteriaHql = criteriaHql + " where ((currencyExchangeRate.effectiveFrom<=:effectiveTo or "
									+ " currencyExchangeRate.effectiveTo<=:effectiveTo)) ";
						} else {
							criteriaHql = criteriaHql + " and ((chargerates.effectiveFrom<=:effectiveTo or "
									+ " currencyExchangeRate.effectiveTo<=:effectiveTo)) ";
						}
						hasDateSearch = "y";
					}
				}

			}
		}

		hql = hql + criteriaHql;
		hqlCount = hqlCount + criteriaHql;
		/** check for order by list **/

		if (orderbyfields != null) {
			String orderbyHql = "";
			Iterator<String> iter = orderbyfields.iterator();
			while (iter.hasNext()) {
				if (orderbyHql.equals(""))
					orderbyHql = " order by " + iter.next().toString();
				else
					orderbyHql = orderbyHql + ", " + iter.next().toString();
			}
			hql = hql + " " + orderbyHql + " asc";
		}

		return new String[] { hql, hqlCount, hasDateSearch };
	}

	/**
	 * get all active Currencies with creditLimitUpdateStatus as P OR F
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Collection<CurrencyExchangeRate> getAllUpdatedCurr(Date effFrom) {
		String hql = "SELECT exRate FROM CurrencyExchangeRate exRate " + " WHERE :effDate BETWEEN exRate.effectiveFrom AND "
				+ " exRate.effectiveTo "
				+ " AND (exRate.creditLimitUpdateStatus = :statusP OR exRate.creditLimitUpdateStatus = :statusF)";

		Query hQuery = getSession().createQuery(hql);

		hQuery.setParameter("statusP", CommonsConstants.CreditLimitUpdateStatus.PENDING.code());
		hQuery.setParameter("statusF", CommonsConstants.CreditLimitUpdateStatus.FAILED.code());
		hQuery.setTimestamp("effDate", effFrom);

		List<CurrencyExchangeRate> resultsList = hQuery.list();
		Collection<CurrencyExchangeRate> colCurr = new ArrayList<CurrencyExchangeRate>();

		if (resultsList.size() > 0) {
			Iterator<CurrencyExchangeRate> iterator = resultsList.iterator();
			CurrencyExchangeRate currencyExchangeRate;
			while (iterator.hasNext()) {
				currencyExchangeRate = (CurrencyExchangeRate) iterator.next();

				// Done in order to load the currency object
				currencyExchangeRate.getCurrency().getCurrencyCode();
				colCurr.add(currencyExchangeRate);
			}
		}

		return colCurr;
	}

	/**
	 * get all active Currencies with creditLimitUpdateStatus as P OR F
	 * 
	 */
	public Collection<CurrencyExchangeRate> getAllUpdatedCurrFares(Date effFrom) {
		String hql = "SELECT exRate FROM CurrencyExchangeRate exRate " + " WHERE :effDate BETWEEN exRate.effectiveFrom AND "
				+ " exRate.effectiveTo " + " AND (exRate.faresInLocalCurrUpdateStatus = :statusP OR "
				+ " exRate.faresInLocalCurrUpdateStatus = :statusF)";

		Query hQuery = getSession().createQuery(hql);

		hQuery.setParameter("statusP", CommonsConstants.CreditLimitUpdateStatus.PENDING.code());
		hQuery.setParameter("statusF", CommonsConstants.CreditLimitUpdateStatus.FAILED.code());
		hQuery.setTimestamp("effDate", effFrom);

		@SuppressWarnings("unchecked")
		List<CurrencyExchangeRate> resultsList = hQuery.list();
		Collection<CurrencyExchangeRate> colCurr = new ArrayList<CurrencyExchangeRate>();

		if (resultsList.size() > 0) {
			Iterator<CurrencyExchangeRate> iterator = resultsList.iterator();
			CurrencyExchangeRate currencyExchangeRate;
			while (iterator.hasNext()) {
				currencyExchangeRate = (CurrencyExchangeRate) iterator.next();

				// Done in order to load the currency object
				currencyExchangeRate.getCurrency().getCurrencyCode();
				colCurr.add(currencyExchangeRate);
			}
		}

		return colCurr;
	}

	/**
	 * get all active Currencies with creditLimitUpdateStatus as P OR F
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Collection<CurrencyExchangeRate> getAllUpdatedCurrForCharges(Date effFrom) {
		String hql = "SELECT exRate FROM CurrencyExchangeRate exRate " + " WHERE :effDate BETWEEN exRate.effectiveFrom AND "
				+ " exRate.effectiveTo " + " AND (exRate.chargesInLocalCurrUpdateStatus = :statusP OR "
				+ " exRate.chargesInLocalCurrUpdateStatus = :statusF)";

		Query hQuery = getSession().createQuery(hql);

		hQuery.setParameter("statusP", CommonsConstants.CreditLimitUpdateStatus.PENDING.code());
		hQuery.setParameter("statusF", CommonsConstants.CreditLimitUpdateStatus.FAILED.code());
		hQuery.setTimestamp("effDate", effFrom);

		List<CurrencyExchangeRate> resultsList = hQuery.list();
		Collection<CurrencyExchangeRate> colCurr = new ArrayList<CurrencyExchangeRate>();

		if (resultsList.size() > 0) {
			Iterator<CurrencyExchangeRate> iterator = resultsList.iterator();
			CurrencyExchangeRate currencyExchangeRate;
			while (iterator.hasNext()) {
				currencyExchangeRate = (CurrencyExchangeRate) iterator.next();

				// Done in order to load the currency object
				currencyExchangeRate.getCurrency().getCurrencyCode();
				colCurr.add(currencyExchangeRate);
			}
		}

		return colCurr;
	}

	public Collection<Currency> getExchangeRateAutomatedCurrencies() throws ModuleException {
		return find("from Currency where autoExRateEnabled = ?", Currency.AUTOMATED_EXRATE_ENABLED, Currency.class);

	}

	/**
	 * 
	 * @param currencyCode
	 * @param exRateEffectiveDate
	 * @return List of Currency Exchange Rates
	 */
	public List<CurrencyExchangeRate> getExchangeRatesForAutomatedFareUpdate(Date exRateEffectiveDate) {
		String hql = "SELECT exRate FROM CurrencyExchangeRate exRate " + " WHERE :effDate BETWEEN exRate.effectiveFrom AND "
				+ " exRate.effectiveTo " + " AND exRate.status = :status "
				+ " AND exRate.currency.status = :currencyStatus AND exRate.faresUpdatedForExchangeRateChange =:fareUpdateStatus";
		Query hQuery = getSession().createQuery(hql);
		hQuery.setString("status", "ACT");
		hQuery.setTimestamp("effDate", exRateEffectiveDate);
		hQuery.setString("currencyStatus", Currency.STATUS_ACTIVE);
		hQuery.setParameter("fareUpdateStatus", Currency.AUTOMATED_EXRATE_DISABLED);
		List resultsList = hQuery.list();
		ArrayList<CurrencyExchangeRate> currencyExchangeRates = new ArrayList();

		if (resultsList.size() > 0) {
			Iterator iterator = resultsList.iterator();
			CurrencyExchangeRate currencyExchangeRate;
			while (iterator.hasNext()) {
				currencyExchangeRate = (CurrencyExchangeRate) iterator.next();

				// Done in order to load the currency object
				currencyExchangeRate.getCurrency().getExchangeRates().isEmpty();
				currencyExchangeRates.add(currencyExchangeRate);
			}
		}
		return currencyExchangeRates;

	}
	
	@Override
	public List<CurrencyExchangeRate> getExchangeRatesForAutomatedUpdate(Date exRateEffectiveDate, String option) {
		  
		  String hql = "SELECT exRate FROM CurrencyExchangeRate exRate "
				+ " WHERE :effDate BETWEEN exRate.effectiveFrom AND "
				+ " exRate.effectiveTo "
				+ " AND exRate.status = :status "
				+ " AND exRate.currency.status = :currencyStatus "
				+ " AND "+option+" =:updateStatus";

		  /*Option defines in InternalConstants.java
			 */
		Query hQuery = getSession().createQuery(hql);
		hQuery.setString("status", "ACT");
		hQuery.setTimestamp("effDate", exRateEffectiveDate);
		hQuery.setString("currencyStatus", Currency.STATUS_ACTIVE);
		hQuery.setParameter("updateStatus", Currency.AUTOMATED_EXRATE_DISABLED);
		List resultsList = hQuery.list();
		ArrayList<CurrencyExchangeRate> currencyExchangeRates = new ArrayList();

		if (resultsList.size() > 0) {
			Iterator iterator = resultsList.iterator();
			CurrencyExchangeRate currencyExchangeRate;
			while (iterator.hasNext()) {
				currencyExchangeRate = (CurrencyExchangeRate) iterator.next();

				// Done in order to load the currency object
				currencyExchangeRate.getCurrency().getExchangeRates().isEmpty();
				currencyExchangeRates.add(currencyExchangeRate);
			}
		}
		return currencyExchangeRates;

		
	}
	
	public List<CurrencyExchangeRateDTO> getExchangeRates() throws ModuleException{
		
		try {
			
			LookupService lookup = LookupServiceFactory.getInstance();
			DataSource source = (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);

			JdbcTemplate jdbcTemplate = new JdbcTemplate(source);
			
			String sql = "SELECT cer.currency_code, cer.exrate_base_to_cur, cur.decimal_places "
					+ "FROM t_currency_exchange_rate cer, t_currency cur " + "WHERE cer.status='ACT' "
					+ " AND cur.CURRENCY_CODE = cer.CURRENCY_CODE AND cur.STATUS = 'ACT' " + " AND '"
					+ new SimpleDateFormat("dd-MMM-YY").format(new Date()) + "'"
					+ " BETWEEN TO_DATE(cer.effective_from, 'dd-MM-YY') AND TO_DATE(cer.effective_to, 'dd-MM-YY')";

			
			@SuppressWarnings({ "unchecked", "rawtypes" })
			List<CurrencyExchangeRateDTO> currencyExchangeRateList = (List<CurrencyExchangeRateDTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<CurrencyExchangeRateDTO> currencyExchangeRateList = new ArrayList<CurrencyExchangeRateDTO>();
					CurrencyExchangeRateDTO currencyExchangeRate = null;
					while (rs.next()) {
						currencyExchangeRate = new CurrencyExchangeRateDTO();
						
						currencyExchangeRate.setCurrencyCode(rs.getString("CURRENCY_CODE"));
						currencyExchangeRate.setDecimalPlaces(rs.getInt("DECIMAL_PLACES"));
						currencyExchangeRate.setBaseToCurrExRate(rs.getDouble("EXRATE_BASE_TO_CUR"));
						
						currencyExchangeRateList.add(currencyExchangeRate);
					}

					return currencyExchangeRateList;
				}

			});
			
			return currencyExchangeRateList;

		} catch (Exception e) {
			log.error("Error is: " + e.getLocalizedMessage());
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}
}
