package com.isa.thinair.airmaster.core.email;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;

/**
 * Class to implement the bussiness logic related to the Flight
 * 
 * @author Byorn
 */
public abstract class AirMail {

	private static Log log = LogFactory.getLog(AirMail.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void sendEmail(FlightAlertDTO alertDTO) throws ModuleException {

		MessageProfile profile = new MessageProfile();

		UserMessaging user = new UserMessaging();
		user.setFirstName(alertDTO.getEmailTo());
		// user.setLastName(alertDTO.get
		user.setToAddres(alertDTO.getEmailTo());
		List<UserMessaging> messageList = new ArrayList<UserMessaging>();
		messageList.add(user);
		profile.setUserMessagings(messageList);

		Topic topic = new Topic();
		HashMap map = new HashMap();
		map.put("user", user);

		topic.setTopicParams(map);
		topic.setTopicName("dst_change");
		profile.setTopic(topic);

		List<MessageProfile> list = new ArrayList<MessageProfile>();
		list.add(profile);
		try {
			AirmasterUtils.getMessagingBD().sendMessages(list);
		} catch (Exception e) {
			log.error("Error occured when sending email in airmaster");

		}
		if (log.isDebugEnabled()) {
			log.debug("send email successfull");
		}
	}
}
