package com.isa.thinair.airmaster.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.model.EventProfile;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.core.publisher.Publisher;
import com.isa.thinair.airmaster.core.service.bd.EventServiceLocal;
import com.isa.thinair.airmaster.core.service.bd.EventServiceRemote;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

@Stateless
@RemoteBinding(jndiBinding = "EventService.remote")
@LocalBinding(jndiBinding = "EventService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class EventServiceBean extends PlatformBaseSessionBean implements EventServiceLocal, EventServiceRemote {
	
	public static String EVENT_PUBLISHER_BEAN = "eventPublisher";

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void publishEvent(EventProfile eventProfile) {
		Publisher  publiser = AirmasterUtils.getBeanByName(EVENT_PUBLISHER_BEAN);
		publiser.publish(eventProfile);
	}

}
