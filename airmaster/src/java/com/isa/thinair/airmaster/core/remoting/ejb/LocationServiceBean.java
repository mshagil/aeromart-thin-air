/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.criteria.StationSearchCriteria;
import com.isa.thinair.airmaster.api.dto.CountryDetailsDTO;
import com.isa.thinair.airmaster.api.dto.external.StationTO;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.api.util.MasterDataPublishUtil;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airmaster.core.bl.HubInfoBL;
import com.isa.thinair.airmaster.core.persistence.dao.CityDAO;
import com.isa.thinair.airmaster.core.persistence.dao.CountryDAO;
import com.isa.thinair.airmaster.core.persistence.dao.HubDAO;
import com.isa.thinair.airmaster.core.persistence.dao.StationDAO;
import com.isa.thinair.airmaster.core.persistence.dao.TerritoryDAO;
import com.isa.thinair.airmaster.core.service.bd.LocationServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.LocationServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ondpublish.OndLocationDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Byorn
 */
/**
 * @author suneth
 *
 */
@Stateless
@RemoteBinding(jndiBinding = "LocationService.remote")
@LocalBinding(jndiBinding = "LocationService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LocationServiceBean extends PlatformBaseSessionBean implements LocationServiceDelegateImpl,
		LocationServiceLocalDelegateImpl {

	/**
	 * @throws ModuleException
	 */
	public void saveCountry(Country obj) throws ModuleException {
		try {
			obj = (Country) setUserDetails(obj);
			AuditAirMaster.doAudit(obj, getUserPrincipal());
			lookupCountryDAO().saveCountry(obj);

		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	public void saveTerritory(Territory obj) throws ModuleException {
		try {
			obj = (Territory) setUserDetails(obj);
			AuditAirMaster.doAudit(obj, getUserPrincipal());
			lookupTerritoryDAO().saveTerritory(obj);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	public void updateTerritoryVersion(String territoryCode, long version) throws ModuleException {
		try {
			lookupTerritoryDAO().updateTerritoryVersion(territoryCode, version);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public void deleteCountry(String key) throws ModuleException {
		try {
			lookupCountryDAO().removeCountry(key);
			AuditAirMaster.doAudit(key, AuditAirMaster.COUNTRY, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public void deleteCountry(Country country) throws ModuleException {
		try {
			lookupCountryDAO().removeCountry(country);
			AuditAirMaster.doAudit(country.getCountryCode(), AuditAirMaster.COUNTRY, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public void deleteTerritory(String key) throws ModuleException {
		try {
			lookupTerritoryDAO().removeTerritory(key);
			AuditAirMaster.doAudit(key, AuditAirMaster.TERRITORY, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */

	public void deleteTerritory(Territory territory) throws ModuleException {
		try {
			lookupTerritoryDAO().removeTerritory(territory);
			AuditAirMaster.doAudit(territory.getTerritoryCode(), AuditAirMaster.TERRITORY, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 * 
	 */
	public List<Country> getCountries() throws ModuleException {
		try {
			return lookupCountryDAO().getCountrys();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public Country getCountry(String countryId) throws ModuleException {
		try {
			return lookupCountryDAO().getCountry(countryId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public Territory getTerritory(String territoryId) throws ModuleException {
		try {
			return lookupTerritoryDAO().getTerritory(territoryId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public Page getCountries(int startRec, int noRecs, String countryCode) throws ModuleException {
		try {

			return lookupCountryDAO().getCountrys(startRec, noRecs, countryCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public List<Territory> getTerritories() throws ModuleException {
		try {
			return lookupTerritoryDAO().getTerritorys();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public Page getTerritories(int startRec, int noRecs) throws ModuleException {
		try {
			return lookupTerritoryDAO().getTerritorys(startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public Page getTerritorys(int startRec, int noRecs, String territoryCode, String territoryDesc) throws ModuleException {
		try {
			return lookupTerritoryDAO().getTerritorys(startRec, noRecs, territoryCode, territoryDesc);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public Page getStations(StationSearchCriteria criteria, int startRec, int pageSize) throws ModuleException {
		try {
			return lookupStationDAO().getStations(criteria, startRec, pageSize);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public void saveStation(Station station) throws ModuleException {
		try {

			station = (Station) setUserDetails(station);
			AuditAirMaster.doAudit(station, getUserPrincipal());
			lookupStationDAO().saveStation(station);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}
	
	public void saveCity(City city) throws ModuleException {
		try {

			city = (City) setUserDetails(city);
			AuditAirMaster.doAudit(city, getUserPrincipal());
			lookupCityDAO().saveCity(city);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}


	/**
	 * @throws ModuleException
	 */
	public void updateStationVersion(String stationCode, long version) throws ModuleException {
		try {

			lookupStationDAO().updateStationVersion(stationCode, version);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public void removeStation(String stationCode) throws ModuleException {
		try {
			lookupStationDAO().removeStation(stationCode);
			AuditAirMaster.doAudit(stationCode, AuditAirMaster.STATION, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public void removeStation(Station station) throws ModuleException {
		try {
			lookupStationDAO().removeStation(station);
			AuditAirMaster.doAudit(station.getStationCode(), AuditAirMaster.STATION, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public List<Country> getActiveCountries() throws ModuleException {
		try {
			return lookupCountryDAO().getActiveCountries();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public List<Territory> getActiveTerritories() throws ModuleException {
		try {
			return lookupTerritoryDAO().getActiveTerritories();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public City getCity(String cityCode) throws ModuleException {
		try {
			return lookupCityDAO().getCity(cityCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}
	
	public List<City> getLCCUnPublishedCities() throws ModuleException{
		try {
			return lookupCityDAO().getLCCUnPublishedCities();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	
	/**
	 * @throws ModuleException
	 */
	public Station getStation(String stationCode) throws ModuleException {
		try {
			return lookupStationDAO().getStation(stationCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	public Collection<String> getTerritoriesForStations(Collection<String> stationCodes) throws ModuleException {
		try {
			return lookupStationDAO().getTerritoriesForStations(stationCodes);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public Map<String, String> getStationContactDetails(String airportCode) throws ModuleException {
		try {
			return lookupStationDAO().getStationContactDetails(airportCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	public List<String> getStationDetails(String stCode, boolean isGSA) throws ModuleException {
		try {
			return lookupStationDAO().getStationDetails(stCode, isGSA);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	/**
	 * Return All Stations
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<StationTO> getAllStations() throws ModuleException {
		try {
			Collection<Station> colStation = lookupStationDAO().getAllStations();
			Collection<StationTO> colStationTO = new ArrayList<StationTO>();
			Station station;

			for (Iterator<Station> itColStation = colStation.iterator(); itColStation.hasNext();) {
				station = (Station) itColStation.next();
				colStationTO.add(station.toStationTO());
			}

			return colStationTO;
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * This method is used to search Hub Details information from the Database.
	 * 
	 * @param searchCriteria
	 * @param startRec
	 * @param pageSize
	 * @return Page
	 * @throws ModuleException
	 * @author Dhanusha
	 * @since 25 Feb, 2008
	 */
	public Page getHubDetails(List<ModuleCriterion> searchCriteria, int startRec, int pageSize) throws ModuleException {
		try {
			return lookupHubDAO().getHubDetails(searchCriteria, startRec, pageSize);

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * This method is used to Save/Update Hub Info. information to the Database.
	 * 
	 * @param hubInfo
	 * @throws ModuleException
	 * @author Dhanusha
	 * @since 26 Feb, 2008
	 */
	public void saveHubInfo(HubInfo hubInfo) throws ModuleException {
		try {
			AuditAirMaster.doAudit(hubInfo, getUserPrincipal());
			new HubInfoBL().saveHubInfo(hubInfo, getUserPrincipal().getUserId());

		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * This method is used to get valid hubs
	 */
	public Collection<String> getValidHubCodes(Collection<String> airportCodes) throws ModuleException {
		try {
			return lookupHubDAO().getValidHubCodes(airportCodes);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * This method is used to Delete information from the Database.
	 * 
	 * @param hubInfo
	 * @throws ModuleException
	 * @author Dhanusha
	 * @since 25 Feb, 2008
	 */
	public void removeHubInfo(HubInfo hubInfo) throws ModuleException {
		try {
			lookupHubDAO().removeHubInfo(hubInfo);
			AuditAirMaster.doAudit(hubInfo.getAirportCode(), AuditAirMaster.HUB_INFO, getUserPrincipal());

		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	public HubInfo getHubInfo(String airportCode, String ibCarrier, String obCarrier) throws ModuleException {
		try {
			return lookupHubDAO().getHubInfo(airportCode, ibCarrier, obCarrier);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * This method is used to get Active hubs
	 */
	public Collection<String> getActiveHubCodes() throws ModuleException {
		try {
			return lookupHubDAO().getActiveHubCodes();

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}
	
	/**
	 * This method is used to get Active hubs
	 */
	public Collection<OndLocationDTO> getActiveHubDetails() throws ModuleException {
		try {
			return lookupHubDAO().getActiveHubDetails();

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	private CountryDAO lookupCountryDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (CountryDAO) lookupService.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
	}

	private TerritoryDAO lookupTerritoryDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (TerritoryDAO) lookupService.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
	}

	private StationDAO lookupStationDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (StationDAO) lookupService.getBean("isa:base://modules/airmaster?id=StationDAOProxy");
	}
	
	private CityDAO lookupCityDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (CityDAO) lookupService.getBean("isa:base://modules/airmaster?id=CityDAOProxy");
	}

	/**
	 * Getting the Hub DAO object
	 * 
	 * @return HubDAO
	 */
	private HubDAO lookupHubDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (HubDAO) lookupService.getBean("isa:base://modules/airmaster?id=HubDAOProxy");
	}

	public List<Country> getCountryCodesForRegions(List<String> regionCodes) throws ModuleException {
		try {
			return lookupCountryDAO().getCountryCodesForRegions(regionCodes);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	public String getNameChangeThresholTimePerStation(String station) throws ModuleException {
		return lookupStationDAO().getNameChangeThresholTimePerStation(station);
	}

	public List<Station> getLCCUnPublishedAirports() throws ModuleException {
		try {
			return lookupStationDAO().getLCCUnpublisedStations();
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	public List<Territory> getLCCUnPublishedTerritories() throws ModuleException {
		try {
			return lookupTerritoryDAO().getLCCUnpublishedTerritories();
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public void updateStationLCCPublishStatus(List<Station> originalStations, List<String> updatedStationCodes)
			throws ModuleException {
		for (Iterator<String> iterator = updatedStationCodes.iterator(); iterator.hasNext();) {
			String stationCode = iterator.next();
			Object tmpStation = MasterDataPublishUtil.retrieveMasterDataFromUnpublishedData(stationCode, originalStations);
			if (tmpStation != null) {
				Station station = lookupStationDAO().getStation(stationCode);
				Station previousStation = (Station) tmpStation;
				if (station.getVersion() == previousStation.getVersion()) {
					station.setLccPublishStatus(Util.LCC_PUBLISHED);
					lookupStationDAO().saveStation(station);
				}
			}

		}
	}

	@Override
	public void updateTerritoryLCCPublishStatus(List<Territory> originalTerritories, List<String> updatedTerritoryCodes)
			throws ModuleException {
		for (Iterator<String> iterator = updatedTerritoryCodes.iterator(); iterator.hasNext();) {
			String territoryCode = iterator.next();
			Object tmpTerritory = MasterDataPublishUtil.retrieveMasterDataFromUnpublishedData(territoryCode, originalTerritories);
			if (tmpTerritory != null) {
				Territory territory = lookupTerritoryDAO().getTerritory(territoryCode);
				Territory previousTerritory = (Territory) tmpTerritory;
				if (territory.getVersion() == previousTerritory.getVersion()) {
					territory.setLccPublishStatus(Util.LCC_PUBLISHED);
					lookupTerritoryDAO().saveTerritory(territory);
				}
			}

		}
	}
	
	@Override
	public void updateCityLCCPublishStatus(List<City> originalCities, List<String> updatedCityCodes)
			throws ModuleException {
		for (String cityCode : updatedCityCodes) {
			Object tmpCity = MasterDataPublishUtil.retrieveMasterDataFromUnpublishedData(cityCode, originalCities);
			if (tmpCity != null) {
				City city = lookupCityDAO().getCity(cityCode);
				City previousCity = (City) tmpCity;
				if (city.getVersion() == previousCity.getVersion()) {
					city.setLccPublishStatus(Util.LCC_PUBLISHED);
					lookupCityDAO().saveCity(city);
				}
			}

		}
	}

	public Country getCountryByIsoCode(String isoCode) throws ModuleException {
		try {
			return lookupCountryDAO().getCountryByIsoCode(isoCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}
	
	public List<CountryDetailsDTO> getCountryDetailsList() throws ModuleException{
		try {
			return lookupCountryDAO().getCountryDetailsList();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}
	public boolean isValidCityStationCombination(String stationCode, String cityId) throws ModuleException{
		try {
			return lookupStationDAO().isValidCityStationCombination(stationCode, cityId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public List<String> getCityCodes() throws ModuleException {
		try {
			return lookupCityDAO().getCityCodes();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

    public CountryDetailsDTO getCountryByPhoneCode(String phoneCode) throws ModuleException{
        try {
            return lookupCountryDAO().getCountryByPhoneCode(phoneCode);
        } catch (CommonsDataAccessException e) {
            throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
        }
    }
}
