package com.isa.thinair.airmaster.core.bl;

import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airmaster.core.persistence.dao.AirportDAO;
import com.isa.thinair.airmaster.core.persistence.dao.AirportMessageDAO;
import com.isa.thinair.airmaster.core.persistence.dao.CommonMasterDAO;
import com.isa.thinair.airmaster.core.persistence.dao.CurrencyDAO;
import com.isa.thinair.airmaster.core.persistence.dao.DashboardMessageDAO;
import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * Utility for looking up Airmaster DAO instances.
 * 
 * @since 23-Dec-2008 09:59
 */
public class AirmasterDAOUtil {

	private static final String CURRENCY_DAO_PROXY = "CurrencyDAOProxy";
	private static final String COMMON_MASTER_DAO_PROXY = "CommonMasterDAOProxy";
	private static final String AIRPORT_DAO_PROXY = "AirportDAOProxy";
	private static final String AIRPORT_MESSAGE_DAO_PROXY = "AirportMessageDAOProxy";
	private static final String DASHBOARD_MESSAGE_DAO_PROXY = "DashboardMessageDAOProxy";

	public static CurrencyDAO getCurrencyDAO() {
		return (CurrencyDAO) ModuleFramework.getInstance().getBean(AirmasterConstants.MODULE_NAME, CURRENCY_DAO_PROXY);
	}

	public static CommonMasterDAO getCommonMasterDAO() {
		return (CommonMasterDAO) ModuleFramework.getInstance().getBean(AirmasterConstants.MODULE_NAME, COMMON_MASTER_DAO_PROXY);
	}

	public static AirportDAO getAirportDAO() {
		return (AirportDAO) ModuleFramework.getInstance().getBean(AirmasterConstants.MODULE_NAME, AIRPORT_DAO_PROXY);
	}

	public static DashboardMessageDAO getDashboardMessageDAO() {
		return (DashboardMessageDAO) ModuleFramework.getInstance().getBean(AirmasterConstants.MODULE_NAME,
				DASHBOARD_MESSAGE_DAO_PROXY);
	}

	public static AirportMessageDAO getAirportMessageDAO() {
		return (AirportMessageDAO) ModuleFramework.getInstance().getBean(AirmasterConstants.MODULE_NAME,
				AIRPORT_MESSAGE_DAO_PROXY);
	}

}
