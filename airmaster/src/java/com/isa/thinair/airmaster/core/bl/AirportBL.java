package com.isa.thinair.airmaster.core.bl;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.dto.SegmentCarrierDTO;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportCheckInTime;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.AirportDSTHistory;
import com.isa.thinair.airmaster.api.model.AirportTerminal;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.api.to.AirportDstTO;
import com.isa.thinair.airmaster.api.to.OperationStatusTO;
import com.isa.thinair.airmaster.core.persistence.dao.AirportDAO;
import com.isa.thinair.airmaster.core.util.LCCAirportDAO;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * Class to implement the bussiness logic related to the Flight
 * 
 * @author Byorn
 */
public class AirportBL {

	private AirportDAO airportdao;

	private LCCAirportDAO lccAirportDAO;

	private Log log = LogFactory.getLog(AirportBL.class);

	public AirportBL() {
		LookupService lookup = LookupServiceFactory.getInstance();
		airportdao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
		lccAirportDAO = (LCCAirportDAO) lookup.getBean("isa:base://modules/airmaster?id=LCCAirportDAO");

	}

	/**
	 * assumption: from front-end you can not manualy inactivate a dst...change the status to N asumption: from
	 * front-end you can not edit a date range that is in the past
	 * 
	 * @param newAirportDST
	 * @param alertDTO
	 * @throws ModuleException
	 */
	public OperationStatusTO addNewAirportDST(AirportDST newAirportDST, FlightAlertDTO alertDTO) throws ModuleException {
		try {
			boolean editMode = newAirportDST.getVersion() < 0 ? false : true;

			if (log.isDebugEnabled()) {
				if (editMode)
					log.debug("Updating DST  for airport=" + newAirportDST.getAirportCode() + " ID=" + newAirportDST.getDstCode()
							+ " new start=" + newAirportDST.getDstStartDateTime() + " new end="
							+ newAirportDST.getDstEndDateTime() + " new DST hours=" + newAirportDST.getDstAdjustTime());
				else
					log.debug("Adding DST for airport=" + newAirportDST.getAirportCode() + " start="
							+ newAirportDST.getDstStartDateTime() + " end=" + newAirportDST.getDstEndDateTime() + " DST hours="
							+ newAirportDST.getDstAdjustTime());
			}

			checkIfDSTOverlaps(newAirportDST);

			AirportDST oldAirportDST = null;

			if (editMode) {
				// get the existing DST
				oldAirportDST = airportdao.getDST(newAirportDST.getDstCode());
			}

			newAirportDST.setDSTStatus(AirportDST.CURRENT_DST);

			airportdao.saveAirportDST(newAirportDST);
			AirportDSTHistory airportDSTHistory = composeAirportDSTHistorySaveOrUpdate(newAirportDST, oldAirportDST);			

			Collection<AirportDST> changedAirportDSTs = new ArrayList<AirportDST>();
			if (editMode) {// identifying the period(s) for which flights to be updated and alerts need to be generated
				if (oldAirportDST.getDstAdjustTime() == newAirportDST.getDstAdjustTime()) {
					if (isDateEqual(oldAirportDST.getDstStartDateTime(), newAirportDST.getDstStartDateTime())) {
						if (isDateAfter(oldAirportDST.getDstEndDateTime(), newAirportDST.getDstEndDateTime())) {
							AirportDST changedDST = new AirportDST();

							GregorianCalendar startDate = new GregorianCalendar();
							startDate.setTime(newAirportDST.getDstEndDateTime());
							startDate.add(Calendar.MINUTE, 1);

							changedDST.setDstCode(newAirportDST.getDstCode());
							changedDST.setAirportCode(newAirportDST.getAirportCode());
							changedDST.setDstStartDateTime(startDate.getTime());
							changedDST.setDstEndDateTime(oldAirportDST.getDstEndDateTime());
							changedDST.setDstAdjustTime(newAirportDST.getDstAdjustTime());

							changedAirportDSTs.add(changedDST);

						} else if (isDateBefore(oldAirportDST.getDstEndDateTime(), newAirportDST.getDstEndDateTime())) {
							AirportDST changedDST = new AirportDST();

							GregorianCalendar startDate = new GregorianCalendar();
							startDate.setTime(oldAirportDST.getDstEndDateTime());
							startDate.add(Calendar.MINUTE, 1);

							changedDST.setDstCode(newAirportDST.getDstCode());
							changedDST.setAirportCode(newAirportDST.getAirportCode());
							changedDST.setDstStartDateTime(startDate.getTime());
							changedDST.setDstEndDateTime(newAirportDST.getDstEndDateTime());
							changedDST.setDstAdjustTime(newAirportDST.getDstAdjustTime());

							changedAirportDSTs.add(changedDST);
						} else {
							// nothing to apply
						}
					} else if (isDateEqual(oldAirportDST.getDstEndDateTime(), newAirportDST.getDstEndDateTime())) {
						if (isDateAfter(oldAirportDST.getDstStartDateTime(), newAirportDST.getDstStartDateTime())) {
							AirportDST changedDST = new AirportDST();

							GregorianCalendar endDate = new GregorianCalendar();
							endDate.setTime(oldAirportDST.getDstStartDateTime());
							endDate.add(Calendar.MINUTE, -1);

							changedDST.setDstCode(newAirportDST.getDstCode());
							changedDST.setAirportCode(newAirportDST.getAirportCode());
							changedDST.setDstStartDateTime(newAirportDST.getDstStartDateTime());
							changedDST.setDstEndDateTime(endDate.getTime());
							changedDST.setDstAdjustTime(newAirportDST.getDstAdjustTime());

							changedAirportDSTs.add(changedDST);

						} else if (isDateBefore(oldAirportDST.getDstStartDateTime(), newAirportDST.getDstStartDateTime())) {
							AirportDST changedDST = new AirportDST();

							GregorianCalendar endDate = new GregorianCalendar();
							endDate.setTime(newAirportDST.getDstStartDateTime());
							endDate.add(Calendar.MINUTE, -1);

							changedDST.setDstCode(newAirportDST.getDstCode());
							changedDST.setAirportCode(newAirportDST.getAirportCode());
							changedDST.setDstStartDateTime(oldAirportDST.getDstStartDateTime());
							changedDST.setDstEndDateTime(endDate.getTime());
							changedDST.setDstAdjustTime(newAirportDST.getDstAdjustTime());

							changedAirportDSTs.add(changedDST);
						} else {
							// nothing to apply
						}
					} else {
						// TODO: Need to hadle situtation where both start and end dates are changed
						throw new ModuleException("airmaster.dst.operation.notsupported", "airmaster.desc");
					}
				} else {
					if ((isDateAfter(oldAirportDST.getDstStartDateTime(), newAirportDST.getDstEndDateTime()))
							|| (isDateBefore(oldAirportDST.getDstEndDateTime(), newAirportDST.getDstStartDateTime()))) {
						// TODO: Need to handle situation
						throw new ModuleException("airmaster.dst.operation.notsupported", "airmaster.desc");
					} else {
						changedAirportDSTs.add(setEffectiveDateRanges(newAirportDST, oldAirportDST));
					}
				}
			} else {// add DST
				changedAirportDSTs.add(newAirportDST);
			}

			Iterator<AirportDST> changedAirportDSTsIt = changedAirportDSTs.iterator();
			OperationStatusTO operationStatusTO = null;
			while (changedAirportDSTsIt.hasNext()) {
				operationStatusTO = applyDSTChanges((AirportDST) changedAirportDSTsIt.next(), alertDTO, airportDSTHistory);
			}

			// FIXME need to send a collection of operationStatusDTO if DST is updated for multiple discrete periods
			return operationStatusTO;

		} catch (CommonsDataAccessException e) {
			log.error("Error occured in saving/updating DST", e);
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * 
	 * 
	 * @param newAirportCIT
	 * @throws ModuleException
	 */
	public void addNewAirportCIT(AirportCheckInTime newAirportCIT) throws ModuleException {
		try {
			boolean editMode = newAirportCIT.getVersion() < 0 ? false : true;

			if (log.isDebugEnabled()) {
				if (editMode)
					log.debug("Updating CIT  for airport=" + newAirportCIT.getAirportCode() + " ID="
							+ newAirportCIT.getAirportCheckInTime() + " new carrier code=" + newAirportCIT.getCarrierCode()
							+ " new flight no=" + newAirportCIT.getFlightNumber() + " new check in time gap="
							+ newAirportCIT.getCheckInGap() + " new check in closing time="
							+ newAirportCIT.getCheckInClosingTime());
				else
					log.debug("Adding CIT for airport=" + newAirportCIT.getAirportCode() + " new carrier code="
							+ newAirportCIT.getCarrierCode() + " new flight no=" + newAirportCIT.getFlightNumber()
							+ " new check in time gap=" + newAirportCIT.getCheckInGap() + " new check in closing time="
							+ newAirportCIT.getCheckInClosingTime());
			}

			airportdao.saveAirportCIT(newAirportCIT);

		} catch (CommonsDataAccessException e) {
			log.error("Error occured in saving/updating CIT", e);
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * 
	 * 
	 * @param newAirportCIT
	 * @throws ModuleException
	 */
	public void addNewAirportTerminal(AirportTerminal newAirportTerm) throws ModuleException {
		try {
			boolean editMode = newAirportTerm.getVersion() < 0 ? false : true;

			if (log.isDebugEnabled()) {
				if (editMode)
					log.debug("Updating CIT  for airport=" + newAirportTerm.getAirportCode() + " ID="
							+ newAirportTerm.getTerminalId() + " new carrier code=" + newAirportTerm.getTerminalCode()
							+ " new flight no=" + newAirportTerm.getTerminalShortName() + " new check in time gap="
							+ newAirportTerm.getDefaultTerminal());
				else
					log.debug("Adding CIT for airport=" + newAirportTerm.getAirportCode() + " new carrier code="
							+ newAirportTerm.getTerminalCode() + " new flight no=" + newAirportTerm.getTerminalShortName()
							+ " new check in time gap=" + newAirportTerm.getDefaultTerminal());
			}

			airportdao.saveAirportTerminal(newAirportTerm);

		} catch (CommonsDataAccessException e) {
			log.error("Error occured in saving/updating Terminal", e);
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * 
	 * 
	 * @param airportCIT
	 * @throws ModuleException
	 */
	public void deleteAirportCIT(AirportCheckInTime airportCIT) throws ModuleException {
		try {

			if (log.isDebugEnabled()) {

				log.debug("Deleting CIT  for airport=" + airportCIT.getAirportCode() + " ID="
						+ airportCIT.getAirportCheckInTime() + " new carrier code=" + airportCIT.getCarrierCode()
						+ " new flight no=" + airportCIT.getFlightNumber() + " new check in time gap="
						+ airportCIT.getCheckInGap());

			}

			airportdao.deleteAirportCIT(airportCIT);

		} catch (CommonsDataAccessException e) {
			log.error("Error occured in deleting CIT", e);
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * 
	 * 
	 * @param airportTerminal
	 * @throws ModuleException
	 */
	public void deleteAirportTerminal(AirportTerminal airportTerminal) throws ModuleException {
		try {

			if (log.isDebugEnabled()) {

				log.debug("Deleting CIT  for airport=" + airportTerminal.getAirportCode() + " ID="
						+ airportTerminal.getTerminalId() + " new terminal code=" + airportTerminal.getTerminalCode()
						+ " new terminal name=" + airportTerminal.getTerminalShortName() + " new default="
						+ airportTerminal.getDefaultTerminal());

			}

			airportdao.deleteAirportTerminal(airportTerminal);

		} catch (CommonsDataAccessException e) {
			log.error("Error occured in deleting CIT", e);
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * 
	 * 
	 * @param airportCode
	 * @throws ModuleException
	 */
	public void deleteAirportCIT(String airportCode) throws ModuleException {
		try {

			if (log.isDebugEnabled()) {

				log.debug("Deleting CITs  for airport=" + airportCode);

			}

			airportdao.deleteAirportCIT(airportCode);

		} catch (CommonsDataAccessException e) {
			log.error("Error occured in deleting CITs", e);
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * 
	 * @param airportdst
	 * @throws ModuleException
	 */
	public OperationStatusTO disbleAirportDST(AirportDST airportdst, FlightAlertDTO alertDTO) throws ModuleException {
		try {
			if (log.isDebugEnabled()) {
				log.debug("going to disable an airport dst" + airportdst == null ? "null" : airportdst.toString());
			}

			// if(airportdst==null){
			// log.error("Technical failure -- front end sending a null airportdst");
			// throw new ModuleException("airmaster.technical.error");
			// }

			if (log.isDebugEnabled()) {
				log.debug("Going to disable and Rollback");
				log.debug("AirportDST adjust time before negating" + airportdst.toString());

			}

			if (log.isDebugEnabled()) {
				log.debug("Going to disable and Rollback");
				log.debug("AirportDST adjust time after negating" + airportdst.toString());

			}

			checkIfDSTOverlaps(airportdst);
			if (log.isDebugEnabled()) {
				log.debug("DST Does NOT overlapp");
			}

			airportdst.setDSTStatus(AirportDST.INACTIVE_DST);

			airportdao.saveAirportDST(airportdst);
			AirportDSTHistory airportDSTHistory = composeAirportDSTHistoryForDisable(airportdst.getDstCode());
			log.debug("updating airportdst successfull");

			if (log.isDebugEnabled()) {
				log.debug("going to apply dst changes");
			}
			airportdst.setDstAdjustTime(airportdst.getDstAdjustTime() * -1);
			return applyDSTChanges(airportdst, alertDTO, airportDSTHistory);

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * Apply the DST change to flights and generate alerts
	 * 
	 * @param airportDST
	 * @param alertDTO
	 * @param airportDSTHistory
	 * @return
	 * @throws ModuleException
	 */
	private OperationStatusTO applyDSTChanges(AirportDST airportDST, FlightAlertDTO alertDTO, AirportDSTHistory airportDSTHistory) throws ModuleException {

		OperationStatusTO operationStatusTO = new OperationStatusTO();

		Airport airport = airportdao.getAirport(airportDST.getAirportCode());

		Collection<AirportDST> dsttos = airportdao.getEffectiveDSTs(airport.getAirportCode(), airportDST.getDstStartDateTime(),
				airportDST.getDstEndDateTime());

		try {
			if (containsArrivalFlights(airportDST) || containsDepartureFlights(airportDST)) {
				log.warn("Going update flights' dep/arr times for DST change [airport=" + airportDST.getAirportCode() + " start="
						+ airportDST.getDstStartDateTime() + " end=" + airportDST.getDstEndDateTime() + "]");
				
				if (airportDSTHistory!=null) {
					airportdao.saveAirportDSTHistory(airportDSTHistory);
				}				

				DefaultServiceResponse responce = (DefaultServiceResponse) AirmasterUtils.getFlightBD().recalculateLocalTime(
						airport, airportDST.getDstStartDateTime(), airportDST.getDstEndDateTime(), dsttos, alertDTO);

				if (responce != null) {
					if (responce.getResponseCode().equals(ResponceCodes.SEND_ALLERTY_SUCCESSFULL)) {
						operationStatusTO.setAlertSent(true);
						operationStatusTO.setEmailSent(true);
					}
				}
				publishGDSUpdateSchedules(airport.getAirportCode());
			}
		} catch (ModuleException e) {
			log.error("Error occured in updating flights for DST change", e);
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		}
		return operationStatusTO;
	}

	/**
	 * Sets the maximum period
	 * 
	 * @param airportDST
	 * @param prevDST
	 * @return
	 * @throws ModuleException
	 */
	private AirportDST setEffectiveDateRanges(AirportDST airportDST, AirportDST prevDST) throws ModuleException {

		if (prevDST == null) {
			log.error("Error in DST update for ID=" + airportDST.getDstCode());
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}

		try {
			Date prevStartDate = prevDST.getDstStartDateTime();
			Date prevEndDate = prevDST.getDstEndDateTime();

			Date newStartDate = airportDST.getDstStartDateTime();
			Date newEndDate = airportDST.getDstEndDateTime();

			if (newStartDate.before(prevStartDate)) {
				airportDST.setDstStartDateTime(newStartDate);
			} else {
				airportDST.setDstStartDateTime(prevStartDate);
			}

			if (newEndDate.after(prevEndDate)) {
				airportDST.setDstEndDateTime(newEndDate);
			} else {
				airportDST.setDstEndDateTime(prevEndDate);
			}
		} catch (Exception e) {
			log.error("Error in DST update for ID=" + airportDST.getDstCode());
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
		return airportDST;
	}

	/**
	 * for a single airport there cant be overlapping dst date ranges.
	 * 
	 * @param airportDST
	 * @throws ModuleException
	 */
	private void checkIfDSTOverlaps(AirportDST airportDST) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Checking if DST overlaps" + airportDST.getDstCode());
		}
		// new dst
		if (airportDST.getVersion() < 0) {
			if (!(airportdao.dstDoesNotOverlap(airportDST.getAirportCode(), airportDST.getDstStartDateTime(),
					airportDST.getDstEndDateTime()))) {
				log.error("Dst is Overlapping...Will Not proceed");
				throw new ModuleException("airmaster.dst.overlaps");
			}
		}
		// updating the dst
		else {
			if (!(airportdao.dstDoesNotOverlap(airportDST.getAirportCode(), airportDST.getDstCode(),
					airportDST.getDstStartDateTime(), airportDST.getDstEndDateTime()))) {
				log.error("Dst is Overlapping...Will Not proceed");
				throw new ModuleException("airmaster.dst.overlaps");
			}
		}
	}

	@SuppressWarnings("unchecked")
	public Collection<AirportDstTO> getAirportDSTs(String airportCode) throws ModuleException {
		Collection<AirportDstTO> airportDSTTOs = new ArrayList<AirportDstTO>();
		try {
			List<AirportDST> airportDsts = airportdao.getDSTs(airportCode);
			Iterator<AirportDST> iterator = airportDsts.iterator();
			while (iterator.hasNext()) {
				AirportDST airportDST = (AirportDST) iterator.next();
				AirportDstTO airportDstTO = new AirportDstTO();
				airportDstTO.setAirportDST(airportDST);
				if (containsArrivalFlights(airportDST) || containsDepartureFlights(airportDST)) {

					airportDstTO.setContainsFlights(AirportDstTO.CONTAINS_FLIGHTS_YES);
				} else {
					airportDstTO.setContainsFlights(AirportDstTO.CONTAINS_FLIGHTS_NO);

				}
				airportDSTTOs.add(airportDstTO);
			}
			Collections.sort((List<AirportDstTO>) airportDSTTOs, new LatestFirstSortComparator());
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			log.error("Error is :" + e.getLocalizedMessage());
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
		return airportDSTTOs;
	}

	/**
	 * @param airportCode
	 * @return
	 * @throws ModuleException
	 */
	public Collection<AirportCheckInTime> getAirportCITs(String airportCode) throws ModuleException {
		Collection<AirportCheckInTime> airportCITs = new ArrayList<AirportCheckInTime>();
		try {

			airportCITs = airportdao.getCITs(airportCode);

			// do we need sorting
			// Collections.sort((List) airportCITs, new LatestFirstSortComparator());
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			log.error("Error is :" + e.getLocalizedMessage());
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
		return airportCITs;
	}

	/**
	 * @param airportCode
	 * @return
	 * @throws ModuleException
	 */
	public Collection<AirportTerminal> getAirportTerminals(String airportCode, boolean isActive) throws ModuleException {
		Collection<AirportTerminal> airportTerminals = new ArrayList<AirportTerminal>();
		try {

			airportTerminals = airportdao.getTerminals(airportCode, isActive);

			// do we need sorting
			// Collections.sort((List) airportCITs, new LatestFirstSortComparator());
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			log.error("Error is :" + e.getLocalizedMessage());
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
		return airportTerminals;
	}

	private boolean containsDepartureFlights(AirportDST airportDST) throws ModuleException {

		try {
			String segmentCode = airportDST.getAirportCode() + "/%";
			// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			Date gmtDstEffectiveFromDate = airportDST.getDstStartDateTime();
			Date gmtDstEffectiveToDate = airportDST.getDstEndDateTime();

			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

			String sql = " select count(*) as cnt from t_Flight_segment f" + " where ( f.segment_code like ? "
					+ " and 	f.est_time_departure_zulu between ? and ? )";

			Object[] o = new Object[] { segmentCode, gmtDstEffectiveFromDate, gmtDstEffectiveToDate };
			Integer count = (Integer) jdbcTemplate.query(sql, o, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Integer count = null;
					while (rs.next()) {
						count = new Integer(rs.getInt("cnt"));
					}

					return count;
				}

			});

			if (count.intValue() == 0) {

				return false;
			} else {

				return true;
			}
		} catch (Exception e) {
			log.error("Error is: " + e.getLocalizedMessage());
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}

	}

	private boolean containsArrivalFlights(AirportDST airportDST) throws ModuleException {

		try {
			String segmentCode = "%/" + airportDST.getAirportCode();
			// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			Date gmtDstEffectiveFromDate = airportDST.getDstStartDateTime();
			Date gmtDstEffectiveToDate = airportDST.getDstEndDateTime();

			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

			String sql = " select count(*) as cnt from t_Flight_segment f" + " where ( f.segment_code like ? "
					+ " and 	f.est_time_arrival_zulu between ? and ? )";

			Object[] o = new Object[] { segmentCode, gmtDstEffectiveFromDate, gmtDstEffectiveToDate };
			Integer count = (Integer) jdbcTemplate.query(sql, o,

			new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Integer count = null;
					while (rs.next()) {
						count = new Integer(rs.getInt("cnt"));
					}

					return count;
				}

			});
			if (count.intValue() == 0) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {

			log.error("Error is: " + e.getLocalizedMessage());
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}

	}

	public AirportDST getCurrentAirportDST(String airportCode) throws ModuleException {
		Airport airport = airportdao.getAirport(airportCode);
		AirportDST airportDST = null;
		if (airport != null) {
			airportDST = airportdao.getEffectiveDST(airportCode, new Date());
			if (airportDST == null) {
				airportDST = airportdao.getNextEffectiveDST(airportCode);
				return airportDST;
			}
			return airportDST;
		} else {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@SuppressWarnings("rawtypes")
	private class LatestFirstSortComparator implements Comparator {

		public int compare(Object first, Object second) {

			AirportDstTO f = (AirportDstTO) first;
			AirportDstTO s = (AirportDstTO) second;

			return (f.getAirportDST().getDstStartDateTime() == null || s.getAirportDST().getDstStartDateTime() == null) ? 0 : f
					.getAirportDST().getDstStartDateTime().compareTo(s.getAirportDST().getDstStartDateTime())
					* -1;
		}
	}

	public static boolean isDateEqual(Date dateOne, Date dateTwo) {
		GregorianCalendar calDateOne = new GregorianCalendar();
		calDateOne.setTime(dateOne);

		GregorianCalendar calDateTwo = new GregorianCalendar();
		calDateTwo.setTime(dateTwo);

		return ((calDateOne.get(Calendar.MINUTE) == calDateTwo.get(Calendar.MINUTE))
				&& (calDateOne.get(Calendar.HOUR_OF_DAY) == calDateTwo.get(Calendar.HOUR_OF_DAY))
				&& (calDateOne.get(Calendar.DATE) == calDateTwo.get(Calendar.DATE))
				&& (calDateOne.get(Calendar.MONTH) == calDateTwo.get(Calendar.MONTH)) && (calDateOne.get(Calendar.YEAR) == calDateTwo
				.get(Calendar.YEAR)));
	}

	public static boolean isDateBefore(Date dateOne, Date dateTwo) {
		GregorianCalendar calDateOne = new GregorianCalendar();
		calDateOne.setTime(dateOne);

		GregorianCalendar calDateTwo = new GregorianCalendar();
		calDateTwo.setTime(dateTwo);

		GregorianCalendar calDatePartOne = new GregorianCalendar();
		calDatePartOne.set(Calendar.MINUTE, calDateOne.get(Calendar.MINUTE));
		calDatePartOne.set(Calendar.HOUR_OF_DAY, calDateOne.get(Calendar.HOUR_OF_DAY));
		calDatePartOne.set(Calendar.DATE, calDateOne.get(Calendar.DATE));
		calDatePartOne.set(Calendar.MONTH, calDateOne.get(Calendar.MONTH));
		calDatePartOne.set(Calendar.YEAR, calDateOne.get(Calendar.YEAR));

		GregorianCalendar calDatePartTwo = new GregorianCalendar();
		calDatePartTwo.set(Calendar.MINUTE, calDateTwo.get(Calendar.MINUTE));
		calDatePartTwo.set(Calendar.HOUR_OF_DAY, calDateTwo.get(Calendar.HOUR_OF_DAY));
		calDatePartTwo.set(Calendar.DATE, calDateTwo.get(Calendar.DATE));
		calDatePartTwo.set(Calendar.MONTH, calDateTwo.get(Calendar.MONTH));
		calDatePartTwo.set(Calendar.YEAR, calDateTwo.get(Calendar.YEAR));

		return calDatePartOne.before(calDatePartTwo);
	}

	public static boolean isDateAfter(Date dateOne, Date dateTwo) {
		GregorianCalendar calDateOne = new GregorianCalendar();
		calDateOne.setTime(dateOne);

		GregorianCalendar calDateTwo = new GregorianCalendar();
		calDateTwo.setTime(dateTwo);

		GregorianCalendar calDatePartOne = new GregorianCalendar();
		calDatePartOne.set(Calendar.MINUTE, calDateOne.get(Calendar.MINUTE));
		calDatePartOne.set(Calendar.HOUR_OF_DAY, calDateOne.get(Calendar.HOUR_OF_DAY));
		calDatePartOne.set(Calendar.DATE, calDateOne.get(Calendar.DATE));
		calDatePartOne.set(Calendar.MONTH, calDateOne.get(Calendar.MONTH));
		calDatePartOne.set(Calendar.YEAR, calDateOne.get(Calendar.YEAR));

		GregorianCalendar calDatePartTwo = new GregorianCalendar();
		calDatePartTwo.set(Calendar.MINUTE, calDateTwo.get(Calendar.MINUTE));
		calDatePartTwo.set(Calendar.HOUR_OF_DAY, calDateTwo.get(Calendar.HOUR_OF_DAY));
		calDatePartTwo.set(Calendar.DATE, calDateTwo.get(Calendar.DATE));
		calDatePartTwo.set(Calendar.MONTH, calDateTwo.get(Calendar.MONTH));
		calDatePartTwo.set(Calendar.YEAR, calDateTwo.get(Calendar.YEAR));

		return calDatePartOne.after(calDatePartTwo);
	}

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	// public class PrepStatCreater implements PreparedStatementCreator{
	//
	// private String sql;
	// private Date effectiveFromDate;
	// private Date effectiveToDate;
	// private String segmentCode;
	// public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
	// PreparedStatement ps = conn.prepareStatement(sql);
	//
	// ps.setString(0, segmentCode);
	// ps.setDate(1, new java.sql.Date(effectiveFromDate.getTime()));
	// ps.setDate(2, new java.sql.Date(effectiveToDate.getTime()));
	// return ps;
	// }
	// public PrepStatCreater(String query, Date parseDateArrival, Date parseDateDeparture, String segmentCode) {
	// this.effectiveToDate = parseDateArrival;
	// this.effectiveFromDate=parseDateDeparture;
	// this.segmentCode=segmentCode;
	// this.sql = query;
	// }
	// }


	public SegmentCarrierDTO getSegmentCarrierInfo(String origin, String destination, String carrierCode) throws ModuleException {
		return lccAirportDAO.getSegmentCarrierInfo(origin, destination, carrierCode);
	}

	
	public Set<String> getAirportCodeSet() throws ModuleException {

		Collection<Airport> airports = null;
		Set<String> airportCodeSet = new HashSet<String>();
		airports = AirmasterUtils.getAirportBD().getAirports();

		for (Airport airport : airports) {
			airportCodeSet.add(airport.getAirportCode());
		}

		return airportCodeSet;

	}
	
	private void publishGDSUpdateSchedules(String airportCode) {
		try {
			if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {
				GDSPublishingBD gdsPublishingBD = AirmasterUtils.getGDSPublishingBD();
				GDSScheduleEventCollector sGdsEvents = new GDSScheduleEventCollector();
				sGdsEvents.addAction(GDSScheduleEventCollector.AIRPORT_DST_CHANGED);
				sGdsEvents.addAirports(airportCode);

				Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_DST_AMMEND_SCHEDULE);
				env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, sGdsEvents);

				gdsPublishingBD.publishMessage(env);
			}
		} catch (Exception e) {
			log.error("ERROR publishGDSScheduleUpdates :: " + e.getMessage());
		}

	}
	
	private AirportDSTHistory composeAirportDSTHistorySaveOrUpdate(AirportDST newAirportDST, AirportDST oldAirportDST) {
		AirportDSTHistory airportDSTHistory = new AirportDSTHistory();

		Date startDateTime = newAirportDST.getDstStartDateTime();
		Date endDateTime = newAirportDST.getDstEndDateTime();
		int adjustTime = 0;

		String createdBy = newAirportDST.getCreatedBy();
		Date createdDate = newAirportDST.getCreatedDate();

		if (oldAirportDST != null) {
			startDateTime = oldAirportDST.getDstStartDateTime();
			endDateTime = oldAirportDST.getDstEndDateTime();
			adjustTime = oldAirportDST.getDstAdjustTime();
			airportDSTHistory.setModifiedBy(newAirportDST.getModifiedBy());
			airportDSTHistory.setModifiedDate(newAirportDST.getModifiedDate());
			if (createdBy == null) {
				createdBy = oldAirportDST.getCreatedBy();
			}
			if (createdDate == null) {
				createdDate = oldAirportDST.getCreatedDate();
			}
		}

		airportDSTHistory.setCreatedBy(createdBy);
		airportDSTHistory.setCreatedDate(createdDate);

		airportDSTHistory.setAirportCode(newAirportDST.getAirportCode());
		airportDSTHistory.setDstStartDateTime(newAirportDST.getDstStartDateTime());
		airportDSTHistory.setDstEndDateTime(newAirportDST.getDstEndDateTime());
		airportDSTHistory.setDstAdjustTime(newAirportDST.getDstAdjustTime());
		airportDSTHistory.setDSTStatus(AirportDSTHistory.NOT_PROCESSED);
		airportDSTHistory.setDstStartDateTimeOld(startDateTime);
		airportDSTHistory.setDstEndDateTimeOld(endDateTime);
		airportDSTHistory.setDstAdjustTimeOld(adjustTime);
		
		return airportDSTHistory;
	}

	private AirportDSTHistory composeAirportDSTHistoryForDisable(Integer dstCode) {
		AirportDSTHistory airportDSTHistory = null;
		if (dstCode != null) {
			AirportDST airportDST = airportdao.getDST(dstCode);
			airportDSTHistory = new AirportDSTHistory();

			airportDSTHistory.setAirportCode(airportDST.getAirportCode());
			airportDSTHistory.setDstStartDateTime(airportDST.getDstStartDateTime());
			airportDSTHistory.setDstEndDateTime(airportDST.getDstEndDateTime());
			airportDSTHistory.setDstAdjustTime(0);
			airportDSTHistory.setDSTStatus(AirportDSTHistory.NOT_PROCESSED);
			airportDSTHistory.setDstStartDateTimeOld(airportDST.getDstStartDateTime());
			airportDSTHistory.setDstEndDateTimeOld(airportDST.getDstEndDateTime());
			airportDSTHistory.setDstAdjustTimeOld(airportDST.getDstAdjustTime());

			airportDSTHistory.setCreatedBy(airportDST.getCreatedBy());
			airportDSTHistory.setCreatedDate(airportDST.getCreatedDate());
			airportDSTHistory.setModifiedBy(airportDST.getModifiedBy());
			airportDSTHistory.setModifiedDate(airportDST.getModifiedDate());
		}
		return airportDSTHistory;

	}
}
