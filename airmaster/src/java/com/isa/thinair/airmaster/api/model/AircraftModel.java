/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:44
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_AIRCRAFT_MODEL"
 * 
 *                  AircraftModel is the entity class to repesent a AircraftModel model
 * 
 */
public class AircraftModel extends Persistent {

	private static final long serialVersionUID = -526121017202649792L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	private String modelNumber;

	private String status;

	private String description;

	private String masterModelCode;

	private String iataAircraftType;

	private Set<AircraftCabinCapacity> aircraftcapacitys;

	private Set<AircraftCabinCapacity> oldaircraftcapacitys;

	private Integer ssrTemplateId;

	private String itineraryDesc;

	private String aircraftTypeCode;
	
	private String defaultIataAircraft = "N";

	/**
	 * returns the modelNumber
	 * 
	 * @return Returns the modelNumber.
	 * 
	 * @hibernate.id column = "MODEL_NUMBER" generator-class = "assigned"
	 */
	public String getModelNumber() {
		return modelNumber;
	}

	/**
	 * sets the modelNumber
	 * 
	 * @param modelNumber
	 *            The modelNumber to set.
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param des
	 *            The description to set.
	 */
	public void setDescription(String des) {
		this.description = des;
	}

	public String getMasterModelCode() {
		return masterModelCode;
	}

	public void setMasterModelCode(String masterModelCode) {
		this.masterModelCode = masterModelCode;
	}

	/**
	 * returns the IATA aircraft type of the aircraft
	 * 
	 * @hibernate.property column = "IATA_AIRCRAFT_TYPE"
	 */
	public String getIataAircraftType() {
		return iataAircraftType;
	}

	public void setIataAircraftType(String iataAircraftType) {
		this.iataAircraftType = iataAircraftType;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true" order-by="ACCC_ID"
	 * @hibernate.collection-key column="MODEL_NUMBER"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.AircraftCabinCapacity"
	 */
	public Set<AircraftCabinCapacity> getAircraftCabinCapacitys() {
		return aircraftcapacitys;
	}

	/**
	 * 
	 */
	public void setAircraftCabinCapacitys(Set<AircraftCabinCapacity> set) {
		this.aircraftcapacitys = set;
	}

	/**
	 * @return the ssrTemplateId
	 * 
	 * @hibernate.property column = "SSR_TEMPLATE_ID"
	 */
	public Integer getSsrTemplateId() {
		return ssrTemplateId;
	}

	/**
	 * @param ssrTemplateId
	 *            the ssrTemplateId to set
	 */
	public void setSsrTemplateId(Integer ssrTemplateId) {
		this.ssrTemplateId = ssrTemplateId;
	}

	/**
	 * returns the itineraryDesc
	 * 
	 * @return Returns the itineraryDesc.
	 * 
	 * @hibernate.property column = "ITIN_DESCRIPTION"
	 */
	public String getItineraryDesc() {
		return itineraryDesc;
	}

	/**
	 * sets the itineraryDesc
	 * 
	 * @param des
	 *            The itineraryDesc to set.
	 */
	public void setItineraryDesc(String itineraryDesc) {
		this.itineraryDesc = itineraryDesc;
	}

	/**
	 * returns the aircraftTypeCode
	 * 
	 * @return Returns the aircraftTypeCode
	 * 
	 * @hibernate.property column = "AIRCRAFT_TYPE_CODE"
	 */
	public String getAircraftTypeCode() {
		return aircraftTypeCode;
	}

	/**
	 * sets the aircraftTypeCode
	 * 
	 * @param des
	 *            The aircraftTypeCode to set.
	 */
	public void setAircraftTypeCode(String aircraftTypeCode) {
		this.aircraftTypeCode = aircraftTypeCode;
	}

	public Set<AircraftCabinCapacity> getOldaircraftcapacitys() {
		return oldaircraftcapacitys;
	}

	public void setOldaircraftcapacitys(Set<AircraftCabinCapacity> oldaircraftcapacitys) {
		this.oldaircraftcapacitys = oldaircraftcapacitys;
	}

	/**
	 * 
	 * @return Returns the defaultIataAircraft
	 * 
	 * @hibernate.property column = "DEFAULT_IATA_AIRCRAFT"
	 */
	public String getDefaultIataAircraft() {
		return defaultIataAircraft;
	}

	public void setDefaultIataAircraft(String defaultIataAircraft) {
		this.defaultIataAircraft = defaultIataAircraft;
	}
	
}
