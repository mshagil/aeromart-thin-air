package com.isa.thinair.airmaster.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.SSRPaxSegmentDTO;
import com.isa.thinair.airmaster.api.dto.SSRSearchDTO;
import com.isa.thinair.airmaster.api.dto.SSRTemplateSearchCriteria;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRConstraints;
import com.isa.thinair.airmaster.api.model.SSRSubCategory;
import com.isa.thinair.airmaster.api.model.SSRTemplate;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**
 * @author Dhanya
 */
public interface SsrBD {

	public static final String SERVICE_NAME = "SsrService";

	public Integer saveSSR(SSR obj) throws ModuleException;

	public void saveSSRs(List<Integer> ssrIds, boolean isIncrement) throws ModuleException;

	public void deleteSSR(int key) throws ModuleException;

	public void deleteSSR(SSR ssr) throws ModuleException;

	public Collection<SSR> getSSRs() throws ModuleException;

	public Page getSSRs(int startrec, int numofrecs, int ssrCategoryId) throws ModuleException;

	public SSR getSSR(int id) throws ModuleException;

	public Collection<SSR> getActiveSSRs() throws ModuleException;

	public List<SSR> getActiveSSRs(String cabinClass, String logicalCabinClass, Integer ssrCategoryId) throws ModuleException;

	public List<SSR> getSSR(String ssrCode) throws ModuleException;

	public List<SpecialServiceRequestDTO> getActiveSSRsByFltSegIds(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			String ssrCategory, String salesChannel, boolean checkSSRInventory, String selectedLanguage, boolean isModifyAnci,
			boolean isAddEditAnci, boolean hasPrivModifySSRWithinBuffer, boolean isUserDefinedSsrOnly) throws ModuleException;

	public List<SSRPaxSegmentDTO> getSSRPaxSegmentList(String pnr);

	public Map<Integer, List<SpecialServiceRequestDTO>> getAvailableSSRs(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos, String salesChannel,
			boolean checkInventory, boolean isModifyAnci, boolean skipeCutoverValidation) throws ModuleException;

	/*
	 * Airport services related methods
	 */
	public Map<AirportServiceKeyTO, List<AirportServiceDTO>> getAvailableAirportServices(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap, AppIndicatorEnum appIndicator, int ssrCategory,
			String selectedLangugae, Map<String, Integer> segmentBundledFareInclusion, boolean isAddEditAnci) throws ModuleException;

	/**
	 * 
	 * @param airportFltSegMap
	 *            Integer --> Flight Segment ID List<String> --> Airport Code List
	 * @param skipCutoverValidation
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, List<AirportServiceDTO>> getAvailableAirportServices(Map<Integer, List<String>> airportFltSegMap,
			boolean skipCutoverValidation) throws ModuleException;

	public void deleteApplicableAirports(int appConsId) throws ModuleException;

	public void deleteApplicableOnds(int appConsId) throws ModuleException;

	public Page getSSRs(SSRSearchDTO ssrSearchDTO, int startRec, int noRecs) throws ModuleException;

	public SSRSubCategory getSSRSubCategory(int id) throws ModuleException;

	public SSRConstraints getSSRConstraint(int id) throws ModuleException;

	public void saveSSRLanguageTranslation(SSRInfoDTO ssrInfoDTO) throws ModuleException;

	public int getSSRCount(int ssrCategory) throws ModuleException;

	public List<SSR> getSSRList(int startSortOrder, int endSortOrder) throws ModuleException;

	public void saveTemplate(SSRTemplate ssrTemplate) throws ModuleException;

	public void deleteTemplate(int templateId) throws ModuleException;

	public Page<SSRTemplate> getSSRTemplates(SSRTemplateSearchCriteria criteria, int startRec, int noRecs) throws ModuleException;

	public Integer getTemplateIdForFlightSegment(int flightSegId) throws ModuleException;

	public boolean isAirportServicesAvailable(Map<AirportServiceKeyTO, FlightSegmentDTO> airportWiseSegMap, String appCode,
			int ssrCategory, String selectedLangugae, String cabinClass, String logicalCabinClass) throws ModuleException;

	public Integer getSSRCategoryFromSSrId(Integer ssrId) throws ModuleException;
	
	public boolean isUserDefinedSSR(String ssrCode) throws ModuleException;

}
