package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * @author eric
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_AIRPORT_MSG_OND"
 */
public class AirportMsgOND implements Serializable {

	private static final long serialVersionUID = 5578646361357435031L;
	private Long id;
	private String ondCode;
	private String applyStatus;
	private AirportMessage airportMessage;

	/**
	 * @return Returns the Id.
	 * @hibernate.id column = "AIRPORT_MSG_OND_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_MSG_OND"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return ondCode.
	 * @hibernate.property column = "OND_CODE"
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return applyStatus.
	 * @hibernate.property column = "APPLY_STATUS"
	 */
	public String getApplyStatus() {
		return applyStatus;
	}

	/**
	 * @param applyStatus
	 */
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

	/**
	 * @return airportMessage.
	 * @hibernate.many-to-one column="AIRPORT_MSG_ID" class="com.isa.thinair.airmaster.api.model.AirportMessage"
	 * 
	 */
	public AirportMessage getAirportMessage() {
		return airportMessage;
	}

	/**
	 * @param airportMessage
	 */
	public void setAirportMessage(AirportMessage airportMessage) {
		this.airportMessage = airportMessage;
	}

}
