package com.isa.thinair.airmaster.api.criteria;

import java.io.Serializable;

public class AdminFeeAgentSearchCriteria implements Serializable {

	private static final long serialVersionUID = 9056788547125423658L;

	private String agentCode;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

}
