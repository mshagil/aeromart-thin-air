/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:28:40
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_AIRPORT_DST_HISTORY"
 */
public class AirportDSTHistory extends Tracking {

	private static final long serialVersionUID = -3794911866479711664L;

	private int dstCode;

	private Date dstStartDateTime;

	private Date dstEndDateTime;

	private int dstAdjustTime;

	private String airportCode;

	private String status;

	private Date dstStartDateTimeOld;

	private Date dstEndDateTimeOld;

	private int dstAdjustTimeOld;
	
	public static final String NOT_PROCESSED = "N";
	public static final String PROCESSED = "P";
	public static final String ERROR = "E";

	/**
	 * returns the dstCode
	 * 
	 * @return Returns the dstCode.
	 * 
	 * @hibernate.id column = "DST_HISTORY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_DST_HISTORY"
	 */

	public int getDstCode() {
		return dstCode;
	}

	/**
	 * sets the dstCode
	 * 
	 * @param dstCode
	 *            The dstCode to set.
	 */
	public void setDstCode(int dstCode) {
		this.dstCode = dstCode;
	}

	/**
	 * returns the dstStartDateTime
	 * 
	 * @return Returns the dstStartDateTime.
	 * 
	 * @hibernate.property column = "DST_START_DATE_TIME"
	 */
	public Date getDstStartDateTime() {
		return dstStartDateTime;
	}

	/**
	 * sets the dstStartDateTime
	 * 
	 * @param dstStartDateTime
	 *            The dstStartDateTime to set.
	 */
	public void setDstStartDateTime(Date dstStartDateTime) {
		this.dstStartDateTime = dstStartDateTime;
	}

	/**
	 * returns the dstEndDateTime
	 * 
	 * @return Returns the dstEndDateTime.
	 * 
	 * @hibernate.property column = "DST_END_DATE_TIME"
	 */
	public Date getDstEndDateTime() {
		return dstEndDateTime;
	}

	/**
	 * sets the dstEndDateTime
	 * 
	 * @param dstEndDateTime
	 *            The dstEndDateTime to set.
	 */
	public void setDstEndDateTime(Date dstEndDateTime) {
		this.dstEndDateTime = dstEndDateTime;
	}

	/**
	 * returns the dstAdjustTime
	 * 
	 * @return Returns the dstAdjustTime.
	 * 
	 * @hibernate.property column = "DST_ADJUST_TIME"
	 */
	public int getDstAdjustTime() {
		return dstAdjustTime;
	}

	/**
	 * sets the dstAdjustTime
	 * 
	 * @param dstAdjustTime
	 *            The dstAdjustTime to set.
	 */
	public void setDstAdjustTime(int dstAdjustTime) {
		this.dstAdjustTime = dstAdjustTime;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 * 
	 */
	public String getDSTStatus() {
		return status;
	}

	/**
	 * @hibernate.property column = "AIRPORT_CODE"
	 * 
	 */
	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
   * 
   */
	public void setDSTStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the dstStartDateTimeOld
	 * 
	 * @return Returns the dstStartDateTimeOld.
	 * 
	 * @hibernate.property column = "DST_START_DATE_TIME_EXISTING"
	 */
	public Date getDstStartDateTimeOld() {
		return dstStartDateTimeOld;
	}

	/**
	 * returns the dstEndDateTimeOld
	 * 
	 * @return Returns the dstEndDateTimeOld.
	 * 
	 * @hibernate.property column = "DST_END_DATE_TIME_EXISTING"
	 */
	public Date getDstEndDateTimeOld() {
		return dstEndDateTimeOld;
	}

	/**
	 * returns the dstAdjustTimeOld
	 * 
	 * @return Returns the dstAdjustTimeOld.
	 * 
	 * @hibernate.property column = "DST_ADJUST_TIME_EXISTING"
	 */
	public int getDstAdjustTimeOld() {
		return dstAdjustTimeOld;
	}

	public void setDstStartDateTimeOld(Date dstStartDateTimeOld) {
		this.dstStartDateTimeOld = dstStartDateTimeOld;
	}

	public void setDstEndDateTimeOld(Date dstEndDateTimeOld) {
		this.dstEndDateTimeOld = dstEndDateTimeOld;
	}

	public void setDstAdjustTimeOld(int dstAdjustTimeOld) {
		this.dstAdjustTimeOld = dstAdjustTimeOld;
	}

}
