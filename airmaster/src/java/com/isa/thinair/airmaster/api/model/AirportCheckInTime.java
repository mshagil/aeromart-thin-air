package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author sanjaya
 * 
 * @hibernate.class table = "T_AIRPORT_CHECKIN_TIMES"
 * 
 *                  The entity class representing an airport check in time value.
 */
public class AirportCheckInTime extends Persistent {

	private static final long serialVersionUID = -4335416011579454922L;

	/* Primary Key */
	private Integer airportCheckInTime;

	/* The airport code */
	private String airportCode;

	/* Carrier code */
	private String carrierCode;

	/* Flight number */
	private String flightNumber;

	/* The airport check-in gap in minutes */
	private Integer checkInGap;

	/* The airport check-in closing time in minutes */
	private Integer checkInClosingTime;

	/**
	 * TODO : we need to make this auto-generated. to be done when the airport check-in times ui functionality is
	 * provided.
	 * 
	 * @return Returns the airport check-in time id.
	 * 
	 * @hibernate.id column = "AIRPORT_CHECKIN_TIMES_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_CHECKIN_TIMES"
	 */
	public Integer getAirportCheckInTime() {
		return airportCheckInTime;
	}

	/**
	 * @param airportCheckinTime
	 */
	public void setAirportCheckInTime(Integer airportCheckInTime) {
		this.airportCheckInTime = airportCheckInTime;
	}

	/**
	 * @return Returns the airport code.
	 * 
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            : The airport code to set.
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the carrier code.
	 * 
	 * @hibernate.property column = "CARRIER_CODE"
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrier code to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the flight number.
	 * 
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flight number to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the check in gap in minutes
	 * 
	 * @hibernate.property column = "CHECKIN_GAP_MINS"
	 */
	public Integer getCheckInGap() {
		return checkInGap;
	}

	/**
	 * @param checkInGap
	 *            The check in gap in minutes to set.
	 */
	public void setCheckInGap(Integer checkInGap) {
		this.checkInGap = checkInGap;
	}

	/**
	 * @return the check in closing time in minutes
	 * 
	 * @hibernate.property column = "CHECKIN_CLOSING_TIME_MINS "
	 */
	public Integer getCheckInClosingTime() {
		return checkInClosingTime;
	}

	/**
	 * @param checkInClosingTime
	 *            The check in closing time in minutes to set.
	 */
	public void setCheckInClosingTime(Integer checkInClosingTime) {
		this.checkInClosingTime = checkInClosingTime;
	}
}
