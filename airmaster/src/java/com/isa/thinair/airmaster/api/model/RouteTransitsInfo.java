package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * @author : Nadika Dhanusha Mahatantila
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_ROUTE_TRANSITS"
 */
public class RouteTransitsInfo implements Serializable {

	private static final long serialVersionUID = 8908675068761090101L;

	private RouteInfo routeInfo;

	private long routeTransitId;

	private String transitAirportCode;

	private int seqNo;

	public RouteTransitsInfo() {

	}

	public RouteTransitsInfo(String transitAirportCode, int seqNo) {
		this.transitAirportCode = transitAirportCode;
		this.seqNo = seqNo;
	}

	/**
	 * @return Returns the routeInfo.
	 * 
	 * @hibernate.many-to-one cascade = "none" class = "com.isa.thinair.airmaster.api.model.RouteInfo" column="ROUTE_ID"
	 */
	public RouteInfo getRouteInfo() {
		return routeInfo;
	}

	/**
	 * @param routeInfo
	 *            The routeInfo to set.
	 */
	public void setRouteInfo(RouteInfo routeInfo) {
		this.routeInfo = routeInfo;
	}

	/**
	 * @return Returns the routeTransitId.
	 * 
	 * @hibernate.id column="ROUTE_TRANSIT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_ROUTE_TRANSITS"
	 */
	public long getRouteTransitId() {
		return routeTransitId;
	}

	/**
	 * @param routeTransitId
	 *            The routeTransitId to set.
	 */
	public void setRouteTransitId(long routeTransitId) {
		this.routeTransitId = routeTransitId;
	}

	/**
	 * @return Returns the transitAirportCode.
	 * 
	 * @hibernate.property column = "TRANSIT_AIRPORT_CODE"
	 */
	public String getTransitAirportCode() {
		return transitAirportCode;
	}

	/**
	 * @param transitAirportCode
	 *            The transitAirportCode to set.
	 */
	public void setTransitAirportCode(String transitAirportCode) {
		this.transitAirportCode = transitAirportCode;
	}

	/**
	 * @return Returns the seqNo.
	 * 
	 * @hibernate.property column = "SEQ"
	 */
	public int getSeqNo() {
		return seqNo;
	}

	/**
	 * @param seqNo
	 *            The seqNo to set.
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

}
