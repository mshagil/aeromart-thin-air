package com.isa.thinair.airmaster.api.service;

import java.util.Collection;

import com.isa.thinair.airmaster.api.dto.SmartSearchDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface SmartSearchBD {

	public static final String SERVICE_NAME = "SearchService";

	public Collection<String> search(SmartSearchDTO search) throws ModuleException;

}
