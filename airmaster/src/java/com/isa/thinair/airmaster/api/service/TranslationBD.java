/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.service;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.CityForDisplay;
import com.isa.thinair.airmaster.api.model.CurrencyForDisplay;
import com.isa.thinair.airmaster.api.model.MealForDisplay;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Haider
 */
public interface TranslationBD {

	public static final String SERVICE_NAME = "TranslationService";

	public List<AirportForDisplay> getAirports() throws ModuleException;

	public List<AirportForDisplay> getAirportsByLanguageCode(String languageCode) throws ModuleException;

	public List<AirportForDisplay> getAirportsByAirportCode(String airportCode) throws ModuleException;

	public Collection<AirportForDisplay> getAirport(Collection<String> airportCode, String languageCode) throws ModuleException;

	public Collection<CityForDisplay> getCity(Collection<String> cityCode, String languageCode) throws ModuleException;
	
	public void saveOrUpdateAirport(AirportForDisplay airport) throws ModuleException;

	public void saveOrUpdateAirports(Collection<AirportForDisplay> col) throws ModuleException;

	public void removeAirport(String airportCode, boolean isMealNotifyEnable, int mealNotifyId) throws ModuleException;

	public void removeAirport(int id) throws ModuleException;

	public List<CurrencyForDisplay> getCurrencies() throws ModuleException;

	public List<CurrencyForDisplay> getCurrenciesByLanguageCode(String languageCode) throws ModuleException;

	public List<CurrencyForDisplay> getCurrenciesByCurrencyCode(String currencyCode) throws ModuleException;

	public CurrencyForDisplay getCurrency(String currencyCode, String languageCode) throws ModuleException;

	public void saveOrUpdateCurrency(CurrencyForDisplay currency) throws ModuleException;

	public void removeCurrency(String currencyCode) throws ModuleException;

	public void removeCurrency(int id) throws ModuleException;

	public List<MealForDisplay> getMeals() throws ModuleException;

	public void saveOrUpdateMeals(Collection<MealForDisplay> meals) throws ModuleException;

	public void removeMeals(int mealId) throws ModuleException;
	
	public List<CityForDisplay> getCities() throws ModuleException;

	public void saveOrUpdateCities(Collection<CityForDisplay> obj) throws ModuleException;

	public void removeCities(int cityId) throws ModuleException;
}
