package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 * @author : M.Rikaz
 * @hibernate.class table = "T_SSR_APPLICABILITY_CONTAINTS" lazy="false"
 */
public class SSRConstraints implements Serializable {

	private static final long serialVersionUID = 1238956196375999768L;

	private Integer appConsId;

	private SSR ssrInfo;

	/**
	 * @hibernate.many-to-one column="SSR_ID" class="com.isa.thinair.airmaster.api.model.SSR"
	 * @return Returns the ssrInfo.
	 */
	public SSR getSsrInfo() {
		return ssrInfo;
	}

	public void setSsrInfo(SSR ssrInfo) {
		this.ssrInfo = ssrInfo;
	}

	private String applyAdult;

	private String applyChild;

	private String applyInfant;

	private String applySegment;

	private String applyArrival;

	private String applyDeparture;

	private String applyTransit;

	private Integer minTransitTimeMins;

	private Integer maxTransitTimeMins;

	private Set<SSRApplicableAirports> applicableAirports;

	private Set<SSRApplicableOND> applicableOnds;

	/**
	 * @return Returns the appConsId.
	 * 
	 * @hibernate.id column="SSR_APP_CON_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_APPLICABILITY_CONTAINTS"
	 */
	public Integer getAppConsId() {
		return appConsId;
	}

	public void setAppConsId(Integer appConsId) {
		this.appConsId = appConsId;
	}

	/**
	 * @return the applyAdult
	 * @hibernate.property column = "APPLY_ADT"
	 */
	public String getApplyAdult() {
		return applyAdult;
	}

	public void setApplyAdult(String applyAdult) {
		this.applyAdult = applyAdult;
	}

	/**
	 * @return theapplyChild
	 * @hibernate.property column = "APPLY_CHD"
	 */
	public String getApplyChild() {
		return applyChild;
	}

	public void setApplyChild(String applyChild) {
		this.applyChild = applyChild;
	}

	/**
	 * @return the applyInfant
	 * @hibernate.property column = "APPLY_INF"
	 */
	public String getApplyInfant() {
		return applyInfant;
	}

	public void setApplyInfant(String applyInfant) {
		this.applyInfant = applyInfant;
	}

	/**
	 * @return the applySegment
	 * @hibernate.property column = "APPLY_SEGMENT"
	 */
	public String getApplySegment() {
		return applySegment;
	}

	public void setApplySegment(String applySegment) {
		this.applySegment = applySegment;
	}

	/**
	 * @return the applyArrival
	 * @hibernate.property column = "APPLY_ARRIVAL"
	 */
	public String getApplyArrival() {
		return applyArrival;
	}

	public void setApplyArrival(String applyArrival) {
		this.applyArrival = applyArrival;
	}

	/**
	 * @return the applyDeparture
	 * @hibernate.property column = "APPLY_DEPARTURE"
	 */
	public String getApplyDeparture() {
		return applyDeparture;
	}

	public void setApplyDeparture(String applyDeparture) {
		this.applyDeparture = applyDeparture;
	}

	/**
	 * @return the applyTransit
	 * @hibernate.property column = "APPLY_TRANSIT"
	 */
	public String getApplyTransit() {
		return applyTransit;
	}

	public void setApplyTransit(String applyTransit) {
		this.applyTransit = applyTransit;
	}

	/**
	 * @return the minTransitTimeMins
	 * @hibernate.property column = "MIN_TRANSIT_TIME_IN_MINS"
	 */
	public Integer getMinTransitTimeMins() {
		return minTransitTimeMins;
	}

	public void setMinTransitTimeMins(Integer minTransitTimeMins) {
		this.minTransitTimeMins = minTransitTimeMins;
	}

	/**
	 * @return the maxTransitTimeMins
	 * @hibernate.property column = "MAX_TRANSIT_TIME_IN_MINS"
	 */
	public Integer getMaxTransitTimeMins() {
		return maxTransitTimeMins;
	}

	public void setMaxTransitTimeMins(Integer maxTransitTimeMins) {
		this.maxTransitTimeMins = maxTransitTimeMins;
	}

	/**
	 * @return Returns the applicableOnds collection.
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="SSR_APP_CON_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.SSRApplicableOND"
	 */
	public Set<SSRApplicableOND> getApplicableOnds() {
		return applicableOnds;
	}

	public void setApplicableOnds(Set<SSRApplicableOND> applicableOnds) {
		this.applicableOnds = applicableOnds;
	}

	/**
	 * @return Returns the applicableAirports collection.
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="SSR_APP_CON_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.SSRApplicableAirports"
	 */
	public Set<SSRApplicableAirports> getApplicableAirports() {
		return applicableAirports;
	}

	public void setApplicableAirports(Set<SSRApplicableAirports> applicableAirports) {
		this.applicableAirports = applicableAirports;
	}

	public boolean equals(Object obj) {
		if (obj instanceof SSRConstraints) {
			SSRConstraints castObj = (SSRConstraints) obj;
			if (castObj.getAppConsId() != null && this.appConsId != null) {
				if (castObj.getAppConsId().intValue() == this.appConsId.intValue()) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.appConsId).toHashCode();
	}

}
