package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */

public class AirportServiceKeyTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String airport;
	private String airportType;
	private long departureDateTime;
	private String segmentCode;

	public AirportServiceKeyTO(String airport, String airportType, long departureDateTime, String segmentCode) {
		this.airport = airport;
		this.airportType = airportType;
		this.departureDateTime = departureDateTime;
		this.segmentCode = segmentCode;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((airport == null) ? 0 : airport.hashCode());
		result = prime * result + ((airportType == null) ? 0 : airportType.hashCode());
		result = prime * result + (int) (departureDateTime ^ (departureDateTime >>> 32));
		result = prime * result + ((segmentCode == null) ? 0 : segmentCode.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (obj instanceof AirportServiceKeyTO) {
			AirportServiceKeyTO other = (AirportServiceKeyTO) obj;
			if (airport == null) {
				if (other.airport != null)
					return false;
			} else if (!airport.equals(other.airport))
				return false;
			if (airportType == null) {
				if (other.airportType != null)
					return false;
			} else if (!airportType.equals(other.airportType))
				return false;
			if (departureDateTime != other.departureDateTime)
				return false;
			if (segmentCode == null) {
				if (other.segmentCode != null)
					return false;
			} else if (!segmentCode.equals(other.segmentCode))
				return false;
			return true;
		} else {
			return false;
		}
	}

	public String getAirport() {
		return airport;
	}

	public void setAirport(String airport) {
		this.airport = airport;
	}

	public String getAirportType() {
		return airportType;
	}

	public void setAirportType(String airportType) {
		this.airportType = airportType;
	}

	public long getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(long departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
}
