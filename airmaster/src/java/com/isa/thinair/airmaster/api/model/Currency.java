/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.math.BigDecimal;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Byorn
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_CURRENCY"
 * 
 *                  Currency is the entity class to repesent a Currency model
 * 
 */
public class Currency extends Tracking {

	private static final long serialVersionUID = 1L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	public static final int BO0K_VISIBILITY_TRUE = 1;

	public static final int BO0K_VISIBILITY_FALSE = 0;

	public static final String AUTOMATED_EXRATE_ENABLED = "Y";

	public static final String AUTOMATED_EXRATE_DISABLED = "N";
	
	public static final String ITINERARY_BREAKDOWN_ENABLED = "Y";
	
	public static final String ITINERARY_BREAKDOWN_DISABLED = "N";

	private String currencyCode;

	private String currencyDescriprion;

	private String status;

	private BigDecimal boundryValue;

	private BigDecimal breakPoint;

	private int ibeVisibility = 0;

	private int xbeVisibility = 0;

	private int cardPaymentVisibility = 0;

	private Set<CurrencyExchangeRate> exchangeRates = null;

	private Integer defaultIbePGId = null;

	private Integer defaultXbePGId = null;

	private String autoExRateEnabled = "N";
	
	private String ItineraryFareBreakDownEnabled = "Y";

	private BigDecimal exRateUpdateVariance = null;
	
	private int decimalPlaces = 0;

	private String fareRoundingUnits;

	private String taxesChargesRoundingUnits;

	/**
	 * @return the ItineraryFareBreakDownEnabled
	 * @hibernate.property column = "SHOW_ITINERARY_BREAKDOWN"
	 */
	public String getItineraryFareBreakDownEnabled() {
		return ItineraryFareBreakDownEnabled;
	}

	/**
	 * @param itineraryFareBreakDownEnabled
	 *            , whether the Itinerary fare break down has been enabled
	 */
	public void setItineraryFareBreakDownEnabled(String itineraryFareBreakDownEnabled) {
		this.ItineraryFareBreakDownEnabled = itineraryFareBreakDownEnabled.intern();
	}

	/**
	 * returns the currencyCode
	 * 
	 * @return Returns the currencyCode.
	 * 
	 * @hibernate.id column = "CURRENCY_CODE" generator-class = "assigned"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * sets the currencyCode
	 * 
	 * @param currencyCode
	 *            The currencyCode to set.
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode.intern();
	}

	/**
	 * returns the currencyDescriprion
	 * 
	 * @return Returns the currencyDescriprion.
	 * 
	 * @hibernate.property column = "CURRENCY_DESCRIPRION"
	 */
	public String getCurrencyDescriprion() {
		return currencyDescriprion;
	}

	/**
	 * sets the currencyDescriprion
	 * 
	 * @param currencyDescriprion
	 *            The currencyDescriprion to set.
	 */
	public void setCurrencyDescriprion(String currencyDescriprion) {
		this.currencyDescriprion = currencyDescriprion;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status.intern();
	}

	/**
	 * @return Returns the boundryValue.
	 * @hibernate.property column = "BOUNDRY_VALUE"
	 * 
	 */
	public BigDecimal getBoundryValue() {
		return boundryValue;
	}

	/**
	 * @param boundryValue
	 *            The boundryValue to set.
	 */
	public void setBoundryValue(BigDecimal boundryValue) {
		this.boundryValue = boundryValue;
	}

	/**
	 * @return Returns the breakPoint.
	 * @hibernate.property column = "BREAK_POINT"
	 * 
	 */
	public BigDecimal getBreakPoint() {
		return breakPoint;
	}

	/**
	 * @param breakPoint
	 *            The breakPoint to set.
	 * 
	 */
	public void setBreakPoint(BigDecimal breakPoint) {
		this.breakPoint = breakPoint;
	}

	/**
	 * @return Returns the ibeVisibility.
	 * @hibernate.property column = "IBE_VISIBLITY"
	 */
	public int getIBEVisibility() {
		return ibeVisibility;
	}

	/**
	 * @param bookVisibililty
	 *            The bookVisibility to set.
	 */

	public void setIBEVisibility(int ibeVisibility) {
		this.ibeVisibility = ibeVisibility;
	}

	/**
	 * @return Returns the xbeVisibility.
	 * @hibernate.property column = "XBE_VISIBLITY"
	 */

	public int getXBEVisibility() {
		return xbeVisibility;
	}

	/**
	 * @param bookVisibililty
	 *            The bookVisibility to set.
	 */

	public void setXBEVisibility(int xbeVisibility) {
		this.xbeVisibility = xbeVisibility;
	}

	/**
	 * @return the cardPaymentVisibility
	 * @hibernate.property column = "CC_PAYMENT_VISIBILITY"
	 */
	public int getCardPaymentVisibility() {
		return cardPaymentVisibility;
	}

	/**
	 * @param cardPaymentVisibility
	 *            the cardPaymentVisibility to set
	 */
	public void setCardPaymentVisibility(int cardPaymentVisibility) {
		this.cardPaymentVisibility = cardPaymentVisibility;
	}

	/**
	 * @return the exchangeRates
	 * 
	 * @hibernate.set lazy="false" cascade="all" inverse="true" order-by="EFFECTIVE_FROM"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.CurrencyExchangeRate"
	 * @hibernate.collection-key column="CURRENCY_CODE"
	 */
	public Set<CurrencyExchangeRate> getExchangeRates() {
		return exchangeRates;
	}

	/**
	 * @param exchangeRates
	 *            the exchangeRates to set
	 */
	public void setExchangeRates(Set<CurrencyExchangeRate> exchangeRates) {
		this.exchangeRates = exchangeRates;
	}

	/**
	 * Overrided to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getCurrencyCode()).toHashCode();
	}

	/**
	 * 
	 * @return Returns the defaultIbePGId.
	 * @hibernate.property column = "DEFAULT_IBE_PAYMENT_GATEWAY_ID"
	 */
	public Integer getDefaultIbePGId() {
		return defaultIbePGId;
	}

	/**
	 * 
	 * @return Returns the defaultXbePGId.
	 * @hibernate.property column = "DEFAULT_XBE_PAYMENT_GATEWAY_ID"
	 */
	public Integer getDefaultXbePGId() {
		return defaultXbePGId;
	}

	public void setDefaultIbePGId(Integer defaultIbePGId) {
		this.defaultIbePGId = defaultIbePGId;
	}

	public void setDefaultXbePGId(Integer defaultXbePGId) {
		this.defaultXbePGId = defaultXbePGId;
	}

	/**
	 * @return the autoExRateEnabled
	 * @hibernate.property column = "IS_AUTO_EXRATE_ENABLED"
	 */
	public String getAutoExRateEnabled() {
		return autoExRateEnabled;
	}

	/**
	 * @param autoExRateEnabled
	 *            the autoExRateEnabled to set
	 */
	public void setAutoExRateEnabled(String autoExRateEnabled) {
		this.autoExRateEnabled = autoExRateEnabled.intern();
	}

	/**
	 * @return the exRateUpdateVariance
	 * @hibernate.property column = "EXRATE_UPDATE_VARIANCE"
	 */
	public BigDecimal getExRateUpdateVariance() {
		return exRateUpdateVariance;
	}

	/**
	 * @param exRateUpdateVariance
	 *            the exRateUpdateVariance to set
	 */
	public void setExRateUpdateVariance(BigDecimal exRateUpdateVariance) {
		this.exRateUpdateVariance = exRateUpdateVariance;
	}

	/**
	 * @return the decimalPlaces
	 * @hibernate.property column = "DECIMAL_PLACES"
	 */
	public int getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(int decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

	/**
	 * @hibernate.property column = "FARE_ROUND_UNITS"
	 */
	public String getFareRoundingUnits() {
		return fareRoundingUnits;
	}

	public void setFareRoundingUnits(String fareRoundingUnits) {
		this.fareRoundingUnits = fareRoundingUnits;
	}

	/**
	 * @hibernate.property column = "TAXES_CHARGES_ROUND_UNITS"
	 */
	public String getTaxesChargesRoundingUnits() {
		return taxesChargesRoundingUnits;
	}

	public void setTaxesChargesRoundingUnits(String taxesChargesRoundingUnits) {
		this.taxesChargesRoundingUnits = taxesChargesRoundingUnits;
	}
}
