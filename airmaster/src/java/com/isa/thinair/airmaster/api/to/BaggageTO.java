package com.isa.thinair.airmaster.api.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airmaster.api.model.I18nMessageKey;

public class BaggageTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer baggageId;

	private String baggageName;

	private String description;

	private String iataCode;

	private String status;

	private String cabinClassCode;

	private Integer sortOrder;

	private BigDecimal baggageWeight;

	private String version;
	private String createdDate;
	private String createdBy;
	private String modifiedDate;
	private String modifiedBy;

	private I18nMessageKey i18nMessageKey;

	private String languageList;
	private String selectedLanguageTranslations;

	private boolean updateLanguageTranslations;

	public Integer getBaggageId() {
		return baggageId;
	}

	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	public String getBaggageName() {
		return baggageName;
	}

	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIataCode() {
		return iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public BigDecimal getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(BigDecimal baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	public I18nMessageKey getI18nMessageKey() {
		return i18nMessageKey;
	}

	public void setI18nMessageKey(I18nMessageKey i18nMessageKey) {
		this.i18nMessageKey = i18nMessageKey;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getLanguageList() {
		return languageList;
	}

	public void setLanguageList(String languageList) {
		this.languageList = languageList;
	}

	public String getSelectedLanguageTranslations() {
		return selectedLanguageTranslations;
	}

	public void setSelectedLanguageTranslations(String selectedLanguageTranslations) {
		this.selectedLanguageTranslations = selectedLanguageTranslations;
	}

	public boolean isUpdateLanguageTranslations() {
		return updateLanguageTranslations;
	}

	public void setUpdateLanguageTranslations(boolean updateLanguageTranslations) {
		this.updateLanguageTranslations = updateLanguageTranslations;
	}
}
