package com.isa.thinair.airmaster.api.criteria;

import java.io.Serializable;

/**
 * @author aravinth.r
 *
 */
public class AutomaticCheckinTemplatesSearchCriteria implements Serializable {

	private static final long serialVersionUID = 1827236273815824227L;

	private String airportCode;
	private String status;

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
