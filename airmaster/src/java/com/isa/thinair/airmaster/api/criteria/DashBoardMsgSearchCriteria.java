package com.isa.thinair.airmaster.api.criteria;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Search Criteria for Dashboard Messages
 * 
 * @author Navod Ediriweera
 * @since 04Aug 2010
 */
public class DashBoardMsgSearchCriteria implements Serializable {

	private static final long serialVersionUID = 5036250819815955762L;
	private String fromDate;
	private String toDate;
	private String user;
	private String messageContent;
	private String messageType;

	public String getMessageType() {
		if ("".equals(messageType))
			return null;
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getFromDate() {
		if ("".equals(fromDate))
			return null;
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		if ("".equals(toDate))
			return null;
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getUser() {
		if ("".equals(user))
			return null;
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getMessageContent() {
		if ("".equals(messageContent))
			return null;
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public Date getFromDateAsDateTime() {
		if (this.fromDate == null)
			return null;
		if ("".equals(this.fromDate))
			return null;
		try {
			return CalendarUtil.getParsedTime(this.fromDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss");
		} catch (Exception e) {
			return null;
		}
	}

	public Date getToDateAsDateTime() {
		if (this.toDate == null)
			return null;
		if ("".equals(this.toDate))
			return null;
		try {
			return CalendarUtil.getParsedTime(this.toDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss");
		} catch (Exception e) {
			return null;
		}
	}

}
