package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @hibernate.class table = "ML_T_MEAL_COS"
 * 
 */
public class MealCOS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 674944252343415949L;

	private Integer mealCOSId;

	private Meal meal;

	private String cabinClassCode;

	private String logicalCCCode;

	/**
	 * @return the mealCOSId
	 * 
	 * @hibernate.id column="MEAL_COS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_MEAL_COS"
	 */
	public Integer getMealCOSId() {
		return mealCOSId;
	}

	/**
	 * @param mealCOSId
	 *            the mealCOSId to set
	 */
	public void setMealCOSId(Integer mealCOSId) {
		this.mealCOSId = mealCOSId;
	}

	/**
	 * @return the cabinClassCode
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the meal
	 * @hibernate.many-to-one column="MEAL_ID" class="com.isa.thinair.airmaster.api.model.Meal"
	 * 
	 */
	public Meal getMeal() {
		return meal;
	}

	/**
	 * @param meal
	 *            the meal to set
	 */
	public void setMeal(Meal meal) {
		this.meal = meal;
	}

	/**
	 * @return the logicalCCCode
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}
}
