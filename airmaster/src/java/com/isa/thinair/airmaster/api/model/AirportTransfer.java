package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author rimaz
 * @hibernate.class table = "T_AIRPORT_TRANSFER"
 */
public class AirportTransfer extends Tracking implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer airportTransferId;
	private String airportCode;
	private String providerName;
	private String providerAddess;
	private String providerNumber;
	private String providerEmail;
	private String status;

	/**
	 * @hibernate.id column = "AIRPORT_TRANFER_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_AIRPORT_TRANSFER"
	 */
	public Integer getAirportTransferId() {
		return airportTransferId;
	}

	public void setAirportTransferId(Integer airportTransferId) {
		this.airportTransferId = airportTransferId;
	}

	/**
	 * @return
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return
	 * @hibernate.property column = "SERVICE_PROVIDER_NAME"
	 */
	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	/**
	 * @return
	 * @hibernate.property column = "SERVICE_PROVIDER_ADDRESS"
	 */
	public String getProviderAddess() {
		return providerAddess;
	}

	public void setProviderAddess(String providerAddess) {
		this.providerAddess = providerAddess;
	}

	/**
	 * @return
	 * @hibernate.property column = "SERVICE_PROVIDER_NUMBER"
	 */
	public String getProviderNumber() {
		return providerNumber;
	}

	public void setProviderNumber(String providerNumber) {
		this.providerNumber = providerNumber;
	}

	/**
	 * @return
	 * @hibernate.property column = "SERVICE_PROVIDER_EMAIL"
	 */
	public String getProviderEmail() {
		return providerEmail;
	}

	public void setProviderEmail(String providerEmail) {
		this.providerEmail = providerEmail;
	}

	/**
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
