/*
 * ==============================================================================
 * JKCS Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:36
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_COUNTRY"
 * 
 *                  Country is the entity class to repesent a Country model
 * 
 */
public class Country extends Tracking {

	private static final long serialVersionUID = -5840520982318935094L;

	private String countryCode;

	private String countryName;

	private String remarks;

	private String status;

	private String currencyCode;

	private String regionCode;

	private String marketingCarrier;

	private String onholdEnabled;

	private String bspEnabled = "N"; // TODO : remove the default value once admin ready
	
	private String pnrGovInboundEnabled;
	
	private String pnrGovOutboundEnabled;
	
	private String pnrGovTiming;
	
	private String icaoCode;

	private boolean showExtraChargeDetails;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	/**
	 * returns the countryCode
	 * 
	 * @return Returns the countryCode.
	 * 
	 * @hibernate.id column = "COUNTRY_CODE" generator-class = "assigned"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * sets the countryCode
	 * 
	 * @param countryCode
	 *            The countryCode to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * returns the countryName
	 * 
	 * @return Returns the countryName.
	 * 
	 * @hibernate.property column = "COUNTRY_NAME"
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * sets the countryName
	 * 
	 * @param countryName
	 *            The countryName to set.
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "CURRENCY_CODE"
	 */

	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
   * 
   */
	public void setCurrencyCode(String curCode) {
		this.currencyCode = curCode;
	}

	/**
	 * returns the region code
	 * 
	 * @return Returns the region code.
	 * 
	 * @hibernate.property column = "REGION_CODE"
	 */
	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	/**
	 * @return the marketingCarrier
	 * 
	 * @hibernate.property column = "MARKETING_CARRIER_CODE"
	 * 
	 */
	public String getMarketingCarrier() {
		return marketingCarrier;
	}

	/**
	 * @param marketingCarrier
	 *            the marketingCarrier to set
	 */
	public void setMarketingCarrier(String marketingCarrier) {
		this.marketingCarrier = marketingCarrier;
	}

	/**
	 * @return the onholdEnabled
	 * 
	 * @hibernate.property column = "OHD_ENABLED"
	 * 
	 */
	public String getOnholdEnabled() {
		return onholdEnabled;
	}

	/**
	 * @param onholdEnabled
	 *            the onholdEnabled to set
	 */
	public void setOnholdEnabled(String onholdEnabled) {
		this.onholdEnabled = onholdEnabled;
	}

	/**
	 * @return the onholdEnabled
	 * 
	 * @hibernate.property column = "BSP_ENABLED"
	 * 
	 */
	public String getBspEnabled() {
		return bspEnabled;
	}

	/**
	 * @param bspEnabled
	 *            the bspEnabled to set
	 */
	public void setBspEnabled(String bspEnabled) {
		this.bspEnabled = bspEnabled;
	}

	/**
	 * @return the showExtraChargeDetails
	 * 
	 * @hibernate.property column="SHOW_EXTRA_CHARGES" type="yes_no" insert="false" update="false"
	 */
	public boolean isShowExtraChargeDetails() {
		return showExtraChargeDetails;
	}

	/**
	 * @param showExtraChargeDetails
	 *            the showExtraChargeDetails to set
	 */
	public void setShowExtraChargeDetails(boolean showExtraChargeDetails) {
		this.showExtraChargeDetails = showExtraChargeDetails;
	}

	/**
	 * @return the onholdEnabled
	 * 
	 * @hibernate.property column = "PNRGOV_INBOUND_ENABLED"
	 * 
	 */
	public String getPnrGovInboundEnabled() {
		return pnrGovInboundEnabled;
	}

	public void setPnrGovInboundEnabled(String pnrGovInboundEnabled) {
		this.pnrGovInboundEnabled = pnrGovInboundEnabled;
	}

	/**
	 * @return the onholdEnabled
	 * 
	 * @hibernate.property column = "PNRGOV_OUTBOUND_ENABLED"
	 * 
	 */
	public String getPnrGovOutboundEnabled() {
		return pnrGovOutboundEnabled;
	}

	public void setPnrGovOutboundEnabled(String pnrGovOutboundEnabled) {
		this.pnrGovOutboundEnabled = pnrGovOutboundEnabled;
	}

	/**
	 * @return the onholdEnabled
	 * 
	 * @hibernate.property column = "PNRGOV_TIMING"
	 * 
	 */
	public String getPnrGovTiming() {
		return pnrGovTiming;
	}

	public void setPnrGovTiming(String pnrGovTiming) {
		this.pnrGovTiming = pnrGovTiming;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "ICAO_CODE"
	 */
	
	public String getIcaoCode(){
		return icaoCode;
	}
	
	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

}

	
