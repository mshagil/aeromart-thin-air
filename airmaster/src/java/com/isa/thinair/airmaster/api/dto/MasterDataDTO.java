package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;
import java.util.Map;

public class MasterDataDTO implements Serializable {
	private Map<String, String> bookingClasses;
	private Map<String, String> agents;
	private Map<String, String> flightNumbers;

	public Map<String, String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(Map<String, String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public Map<String, String> getAgents() {
		return agents;
	}

	public void setAgents(Map<String, String> agents) {
		this.agents = agents;
	}

	public Map<String, String> getFlightNumbers() {
		return flightNumbers;
	}

	public void setFlightNumbers(Map<String, String> flightNumbers) {
		this.flightNumbers = flightNumbers;
	}
}
