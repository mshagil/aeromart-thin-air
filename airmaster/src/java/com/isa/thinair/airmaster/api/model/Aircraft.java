/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_AIRCRAFT"
 * 
 *                  Aircraft is the entity class to repesent a Aircraft model
 * 
 */
public class Aircraft extends Persistent {

	private static final long serialVersionUID = -2040726433444014889L;

	private int aircraftId;

	private String modelNumber;

	private String aircraftTailNumber;

	private String serialNumber;

	private String status;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	/**
	 * returns the aircraftId
	 * 
	 * @return Returns the aircraftId.
	 * 
	 * @hibernate.id column = "AIRCRAFT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRCRAFT_AIRCRAFT_ID"
	 * 
	 * 
	 */
	public int getAircraftId() {
		return aircraftId;
	}

	/**
	 * sets the aircraftId
	 * 
	 * @param aircraftId
	 *            The aircraftId to set.
	 */
	public void setAircraftId(int aircraftId) {
		this.aircraftId = aircraftId;
	}

	/**
	 * returns the modelNumber
	 * 
	 * @return Returns the modelNumber.
	 * 
	 * @hibernate.property column = "MODEL_NUMBER"
	 */
	public String getModelNumber() {
		return modelNumber;
	}

	/**
	 * sets the modelNumber
	 * 
	 * @param modelNumber
	 *            The modelNumber to set.
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	/**
	 * returns the aircraftTailNumber
	 * 
	 * @return Returns the aircraftTailNumber.
	 * 
	 * @hibernate.property column = "AIRCRAFT_TAIL_NUMBER"
	 */
	public String getAircraftTailNumber() {
		return aircraftTailNumber;
	}

	/**
	 * sets the aircraftTailNumber
	 * 
	 * @param aircraftTailNumber
	 *            The aircraftTailNumber to set.
	 */
	public void setAircraftTailNumber(String aircraftTailNumber) {
		this.aircraftTailNumber = aircraftTailNumber;
	}

	/**
	 * returns the serialNumber
	 * 
	 * @return Returns the serialNumber.
	 * 
	 * @hibernate.property column = "SERIAL_NUMBER"
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * sets the serialNumber
	 * 
	 * @param serialNumber
	 *            The serialNumber to set.
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
