package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @author Suneth
 * @hibernate.class table = "T_AIRPORT_TRANSFER_TEMPLATE_BC"
 */
public class AirportTransferTemplateBC implements Serializable{

	
	private Integer airportTransferTempBcId;
	
	private AirportTransferTemplate airportTransferTemplate;
	
	private String bookingClassCode;

	/**
	 * @return Returns the Airport Transfer Template Booking Class ID.
	 * 
	 * @hibernate.id column="AIRPORT_TRANS_TEMPLATE_BC_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "s_airport_transfer_template_bc"
	 */
	public Integer getAirportTransferTempBcId() {
		return airportTransferTempBcId;
	}

	public void setAirportTransferTempBcId(Integer airportTransferTempBcId) {
		this.airportTransferTempBcId = airportTransferTempBcId;
	}
		
	/**
	 * @return Airport Transfer Template 
	 * @hibernate.many-to-one column="AIRPORT_TRANSFER_TEMPLATE_ID" class="com.isa.thinair.airmaster.api.model.AirportTransferTemplate"
	 */
	public AirportTransferTemplate getAirportTransferTemplate() {
		return airportTransferTemplate;
	}

	public void setAirportTransferTemplate(
			AirportTransferTemplate airportTransferTemplate) {
		this.airportTransferTemplate = airportTransferTemplate;
	}

	/**
	 * @return the booking class code
	 * @hibernate.property column="BOOKING_CODE"
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}
	
	
	
}
