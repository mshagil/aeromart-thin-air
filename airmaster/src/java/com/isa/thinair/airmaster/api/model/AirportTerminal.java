/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2010 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author sanjaya
 * 
 * @hibernate.class table = "T_AIRPORT_TERMINAL"
 * 
 *                  The entity class representing an airport terminal
 */
public class AirportTerminal extends Persistent {

	private static final long serialVersionUID = -2192638517213272289L;

	/** The terminal id. */
	private Integer terminalId;

	/** The airport code. */
	private String airportCode;

	/** Flag indicating whether this is the default terminal for a given airport code */
	private String defaultTerminal;

	/** The terminal short name */
	private String terminalShortName;

	/** The terminal description */
	private String terminalDescription;

	/** The terminal code */
	private String terminalCode;

	private String status;

	/**
	 * TODO : we need to make this auto-generated. to be done when the airport terminals are allowed to be saved with
	 * the airports.
	 * 
	 * @return Returns the airport terminal id.
	 * 
	 * @hibernate.id column = "AIRPORT_TERMINAL_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_TERMINAL"
	 */
	public Integer getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId
	 *            The terminal id to set.
	 */
	public void setTerminalId(Integer terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @return Returns the airport code.
	 * 
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            The airport code to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return Returns the whether this is the default terminal. values : {"Y" : Yes, "N" : No}
	 * 
	 * @hibernate.property column = "DEFAULT_TERMINAL"
	 */
	public String getDefaultTerminal() {
		return defaultTerminal;
	}

	/**
	 * @param defaultTerminal
	 *            Whether this is the default terminal. values : {"Y" : Yes, "N" : No}
	 */
	public void setDefaultTerminal(String defaultTerminal) {
		this.defaultTerminal = defaultTerminal;
	}

	/**
	 * @return Returns the terminal short name used for display.
	 * 
	 * @hibernate.property column = "TERMINAL_SHORTNAME"
	 */
	public String getTerminalShortName() {
		return terminalShortName;
	}

	/**
	 * @param terminalShortName
	 *            : The terminal short name to set.
	 */
	public void setTerminalShortName(String terminalShortName) {
		this.terminalShortName = terminalShortName;
	}

	/**
	 * @return Returns the terminal description.
	 * 
	 * @hibernate.property column = "TERMINAL_DESC"
	 */
	public String getTerminalDescription() {
		return terminalDescription;
	}

	/**
	 * @param terminalDescription
	 *            The terminal description to set.
	 */
	public void setTerminalDescription(String terminalDescription) {
		this.terminalDescription = terminalDescription;
	}

	/**
	 * @return Returns the terminal code.
	 * 
	 * @hibernate.property column = "TERMINAL_CODE"
	 */
	public String getTerminalCode() {
		return terminalCode;
	}

	/**
	 * @param terminalDescription
	 *            The terminal code to set.
	 */
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
