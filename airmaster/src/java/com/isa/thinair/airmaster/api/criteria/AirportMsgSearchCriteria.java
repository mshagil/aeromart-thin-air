package com.isa.thinair.airmaster.api.criteria;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * @author eric
 * 
 */
public class AirportMsgSearchCriteria implements Serializable {

	private static final long serialVersionUID = -6312101742016052207L;
	private String airport;
	private String depArrType;
	private String displayFromDate;
	private String displayToDate;
	private String validFromDate;
	private String validToDate;
	private String messageContent;

	public String getAirport() {
		return airport;
	}

	public void setAirport(String airport) {
		this.airport = airport;
	}

	public String getDepArrType() {
		return depArrType;
	}

	public void setDepArrType(String depArrType) {
		this.depArrType = depArrType;
	}

	public String getDisplayFromDate() {
		return displayFromDate;
	}

	public void setDisplayFromDate(String displayFromDate) {
		this.displayFromDate = displayFromDate;
	}

	public String getDisplayToDate() {
		return displayToDate;
	}

	public void setDisplayToDate(String displayToDate) {
		this.displayToDate = displayToDate;
	}

	public String getValidFromDate() {
		return validFromDate;
	}

	public void setValidFromDate(String validFromDate) {
		this.validFromDate = validFromDate;
	}

	public String getValidToDate() {
		return validToDate;
	}

	public void setValidToDate(String validToDate) {
		this.validToDate = validToDate;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public Date getDisplayFromDateAsDateTime() {
		if (this.displayFromDate == null)
			return null;
		if ("".equals(this.displayFromDate))
			return null;
		try {
			return CalendarUtil.getParsedTime(this.displayFromDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss");
		} catch (Exception e) {
			return null;
		}
	}

	public Date getDisplayToDateAsDateTime() {
		if (this.displayToDate == null)
			return null;
		if ("".equals(this.displayToDate))
			return null;
		try {
			return CalendarUtil.getParsedTime(this.displayToDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss");
		} catch (Exception e) {
			return null;
		}
	}

	public Date getValidFromDateAsDate() {
		if (this.validFromDate == null)
			return null;
		if ("".equals(this.validFromDate))
			return null;
		try {
			return CalendarUtil.getParsedTime(this.validFromDate, "dd/MM/yyyy");
		} catch (Exception e) {
			return null;
		}
	}

	public Date getValidToDateAsDate() {
		if (this.validToDate == null)
			return null;
		if ("".equals(this.validToDate))
			return null;
		try {
			return CalendarUtil.getParsedTime(this.validToDate, "dd/MM/yyyy");
		} catch (Exception e) {
			return null;
		}
	}
}
