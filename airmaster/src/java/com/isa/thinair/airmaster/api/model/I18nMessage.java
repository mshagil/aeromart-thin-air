package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.sql.Clob;

/**
 * @author eric
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_I18N_MESSAGE"
 */
public class I18nMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long messageId;
	private String messageLocale;
	private Clob messageContent;
	private I18nMessageKey I18nMessageKey;

	// not a table field
	private String _msgContent;

	/**
	 * @return Returns the messageId.
	 * @hibernate.id column = "I18N_MESSAGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_I18N_MESSAGE"
	 */
	public Long getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId
	 */
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return messageLocale
	 * @hibernate.property column = "MESSAGE_LOCALE" sql-type="CLOB"
	 */
	public String getMessageLocale() {
		return messageLocale;
	}

	/**
	 * @param messageLocale
	 */
	public void setMessageLocale(String messageLocale) {
		this.messageLocale = messageLocale;
	}

	/**
	 * @return messageContent
	 * @hibernate.property column = "MESSAGE_CONTENT"
	 */
	public Clob getMessageContent() {
		return messageContent;
	}

	/**
	 * @param messageContent
	 */
	public void setMessageContent(Clob messageContent) {
		this.messageContent = messageContent;
	}

	/**
	 * @return
	 * @hibernate.many-to-one column="I18N_MESSAGE_KEY" class="com.isa.thinair.airmaster.api.model.I18nMessageKey"
	 */
	public I18nMessageKey getI18nMessageKey() {
		return I18nMessageKey;
	}

	/**
	 * @param i18nMessageKey
	 */
	public void setI18nMessageKey(I18nMessageKey i18nMessageKey) {
		I18nMessageKey = i18nMessageKey;
	}

	/**
	 * @return
	 */
	public String _getMsgContent() {
		return _msgContent;
	}

	/**
	 * @param msgContent
	 */
	public void _setMsgContent(String msgContent) {
		this._msgContent = msgContent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof I18nMessage)) {
			return false;
		} else {
			return this.I18nMessageKey.getI18nMsgKey().equals(((I18nMessage) obj).I18nMessageKey.getI18nMsgKey())
					&& this.messageLocale.equals(((I18nMessage) obj).messageLocale);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int hash = 1;
		hash = 31 * hash + (this.I18nMessageKey.getI18nMsgKey() == null ? 0 : this.I18nMessageKey.getI18nMsgKey().hashCode());
		hash = 31 * hash + (this.messageLocale == null ? 0 : this.messageLocale.hashCode());
		return hash;
	}
}
