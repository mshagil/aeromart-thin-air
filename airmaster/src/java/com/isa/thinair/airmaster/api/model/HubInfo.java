/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:13:50
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/*******************************************************************************
 * Description : Hub Info. domain class
 * 
 * @author : Nadika Dhanusha Mahatantila
 * @since : 25 Feb, 2008
 * @hibernate.class table = "T_HUB"
 * 
 ******************************************************************************/

public class HubInfo extends Tracking {

	private static final long serialVersionUID = -8136521831968633169L;

	private long hubId;

	private String airportCode;

	private String ibCarrierCode;

	private String obCarrierCode;

	private String minConnectionTime;

	private String maxConnectionTime;

	private String status;

	private String editable;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	public static final String EDITABLE_TRUE = "Y";

	public static final String EDITABLE_FALSE = "N";

	/**
	 * @return Returns the airportCode.
	 * 
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            The airportCode to set.
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return Returns the hubId.
	 * 
	 * @hibernate.id column="HUB_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_HUB"
	 */
	public long getHubId() {
		return hubId;
	}

	/**
	 * @param hubId
	 *            The hubId to set.
	 */
	public void setHubId(long hubId) {
		this.hubId = hubId;
	}

	/**
	 * @return Returns the ibCarrierCode.
	 * 
	 * @hibernate.property column = "IB_CARRIER_CODE"
	 */
	public String getIbCarrierCode() {
		return ibCarrierCode;
	}

	/**
	 * @param ibCarrierCode
	 *            The ibCarrierCode to set.
	 */
	public void setIbCarrierCode(String ibCarrierCode) {
		this.ibCarrierCode = ibCarrierCode;
	}

	/**
	 * @return Returns the maxConnectionTime.
	 * 
	 * @hibernate.property column = "MAX_CXN_TIME"
	 */
	public String getMaxConnectionTime() {
		return maxConnectionTime;
	}

	/**
	 * @param maxConnectionTime
	 *            The maxConnectionTime to set.
	 */
	public void setMaxConnectionTime(String maxConnectionTime) {
		this.maxConnectionTime = maxConnectionTime;
	}

	/**
	 * @return Returns the minConnectionTime.
	 * 
	 * @hibernate.property column = "MIN_CXN_TIME"
	 */
	public String getMinConnectionTime() {
		return minConnectionTime;
	}

	/**
	 * @param minConnectionTime
	 *            The minConnectionTime to set.
	 */
	public void setMinConnectionTime(String minConnectionTime) {
		this.minConnectionTime = minConnectionTime;
	}

	/**
	 * @return Returns the obCarrierCode.
	 * 
	 * @hibernate.property column = "OB_CARRIER_CODE"
	 */
	public String getObCarrierCode() {
		return obCarrierCode;
	}

	/**
	 * @param obCarrierCode
	 *            The obCarrierCode to set.
	 */
	public void setObCarrierCode(String obCarrierCode) {
		this.obCarrierCode = obCarrierCode;
	}

	/**
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the editable.
	 * 
	 * @hibernate.property column = "EDITABLE"
	 */
	public String getEditable() {
		return editable;
	}

	/**
	 * @param editable
	 *            The editable to set.
	 */
	public void setEditable(String editable) {
		this.editable = editable;
	}

}
