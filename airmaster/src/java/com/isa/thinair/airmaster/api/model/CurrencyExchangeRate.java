/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Mohamed Nasly
 * @since 22-Dec-2008 15:03
 * 
 * @hibernate.class table = "T_CURRENCY_EXCHANGE_RATE"
 */
public class CurrencyExchangeRate extends Tracking implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long exchangeRateId;

	private Currency currency;

	private Date effectiveFrom;

	private Date effectiveTo;

	private String status;

	private Boolean isModified = new Boolean(false);

	private String creditLimitUpdateStatus = "P"; // FIXME converted to string
													// because no time to map enum

	private String chargesInLocalCurrUpdateStatus = "P";

	private String faresInLocalCurrUpdateStatus = "P";

	/** JIRA : AARESAA-3087 -Lalanthi */
	private BigDecimal exrateBaseToCurNumber; // salesExRate
	private BigDecimal exrateCurToBaseNumber; // purchaseExRate

	private String faresUpdatedForExchangeRateChange = "Y";
	
	private String chargesUpdatedForExchangeRateChange = "Y";
	
	private String anciChargesUpdatedForExchangeRateChange ="Y";

	/**
	 * @hibernate.property column = "EXRATE_BASE_TO_CUR"
	 * @return
	 */
	public BigDecimal getExrateBaseToCurNumber() {
		return exrateBaseToCurNumber;
	}

	public void setExrateBaseToCurNumber(BigDecimal exrateBaseToCurNumber) {
		this.exrateBaseToCurNumber = exrateBaseToCurNumber;
	}

	/**
	 * @hibernate.property column = "EXRATE_CUR_TO_BASE"
	 * @return
	 */
	public BigDecimal getExrateCurToBaseNumber() {
		return exrateCurToBaseNumber;
	}

	public void setExrateCurToBaseNumber(BigDecimal exrateCurToBaseNumber) {
		this.exrateCurToBaseNumber = exrateCurToBaseNumber;
	}

	/**
	 * @return the exchangeRateId
	 * @hibernate.id column = "CER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CURRENCY_EXCHANGE_RATE"
	 * 
	 */
	public Long getExchangeRateId() {
		return exchangeRateId;
	}

	/**
	 * @param exchangeRateId
	 *            the exchangeRateId to set
	 */
	public void setExchangeRateId(Long exchangeRateId) {
		this.exchangeRateId = exchangeRateId;
	}

	/**
	 * @return the currency
	 * 
	 * @hibernate.many-to-one column="CURRENCY_CODE" cascade="all" class="com.isa.thinair.airmaster.api.model.Currency"
	 * 
	 *                        FIXME hibernatedoclet shipped with xdoclet 1.2.3 does not support lazy attribute for
	 *                        many-to-one mapping
	 * 
	 */
	public Currency getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/**
	 * @return the effectiveFrom
	 * @hibernate.property column = "EFFECTIVE_FROM"
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	/**
	 * @param effectiveFrom
	 *            the effectiveFrom to set
	 */
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return the effectiveTo
	 * @hibernate.property column = "EFFECTIVE_TO"
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	/**
	 * @param effectiveTo
	 *            the effectiveTo to set
	 */
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * Returns the multiplying exchange rate
	 * 
	 * @return
	 */
	public BigDecimal getMultiplyingExchangeRate() {
		if (this.getExrateBaseToCurNumber() != null) {
			return getMultiplyingExchangeRate(new BigDecimal("1"), this.getExrateBaseToCurNumber());// JIRA :
																									// AARESAA-3087
		} else {
			return null;
		}
	}

	public static BigDecimal getMultiplyingExchangeRate(BigDecimal fromCurrExchangeRate, BigDecimal toCurrExchangeRate) {
		return AccelAeroCalculator.divide(fromCurrExchangeRate, toCurrExchangeRate, 15, RoundingMode.DOWN);
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status.intern();
	}

	/**
	 * @return the isModified
	 */
	public Boolean getIsModified() {
		return isModified;
	}

	/**
	 * @param isModified
	 *            the isModified to set
	 */
	public void setIsModified(Boolean isModified) {
		this.isModified = isModified;
	}

	/**
	 * @hibernate.property column = "CREDIT_LIMIT_UPDATE_STATUS"
	 */
	public String getCreditLimitUpdateStatus() {
		return creditLimitUpdateStatus;
	}

	/**
	 * @param creditLimitUpdateStatus
	 *            The creditLimitUpdateStatus to set.
	 */
	public void setCreditLimitUpdateStatus(String creditLimitUpdateStatus) {
		this.creditLimitUpdateStatus = creditLimitUpdateStatus.intern();
	}

	/**
	 * @return Returns the chargesInLocalCurrUpdateStatus.
	 * @hibernate.property column = "CHG_IN_LOCAL_CUR_UPD_STATUS"
	 */
	public String getChargesInLocalCurrUpdateStatus() {
		return chargesInLocalCurrUpdateStatus;
	}

	/**
	 * @param chargesInLocalCurrUpdateStatus
	 *            The chargesInLocalCurrUpdateStatus to set.
	 */
	public void setChargesInLocalCurrUpdateStatus(String chargesInLocalCurrUpdateStatus) {
		this.chargesInLocalCurrUpdateStatus = chargesInLocalCurrUpdateStatus.intern();
	}

	/**
	 * @return Returns the faresInLocalCurrUpdateStatus.
	 * @hibernate.property column = "FARES_IN_LOCAL_CUR_UPD_STATUS"
	 */
	public String getFaresInLocalCurrUpdateStatus() {
		return faresInLocalCurrUpdateStatus;
	}

	/**
	 * @param faresInLocalCurrUpdateStatus
	 *            The faresInLocalCurrUpdateStatus to set.
	 */
	public void setFaresInLocalCurrUpdateStatus(String faresInLocalCurrUpdateStatus) {
		this.faresInLocalCurrUpdateStatus = faresInLocalCurrUpdateStatus.intern();
	}

	public Object clone() {
		CurrencyExchangeRate currencyExchangeRate = new CurrencyExchangeRate();

		currencyExchangeRate.setChargesInLocalCurrUpdateStatus(this.getChargesInLocalCurrUpdateStatus());
		currencyExchangeRate.setCreatedBy(this.getCreatedBy());
		currencyExchangeRate.setCreatedDate(this.getCreatedDate());
		currencyExchangeRate.setCreditLimitUpdateStatus(this.getCreditLimitUpdateStatus());
		currencyExchangeRate.setCurrency(this.getCurrency());
		currencyExchangeRate.setEffectiveFrom(this.getEffectiveFrom());
		currencyExchangeRate.setEffectiveTo(this.getEffectiveTo());
		// currencyExchangeRate.setExchangeRateId(this.getExchangeRateId());
		currencyExchangeRate.setExrateBaseToCurNumber(this.getExrateBaseToCurNumber());
		currencyExchangeRate.setExrateCurToBaseNumber(this.getExrateCurToBaseNumber());
		currencyExchangeRate.setFaresInLocalCurrUpdateStatus(this.getFaresInLocalCurrUpdateStatus());
		currencyExchangeRate.setIsModified(this.getIsModified());
		currencyExchangeRate.setModifiedBy(this.getModifiedBy());
		currencyExchangeRate.setModifiedDate(this.getModifiedDate());
		currencyExchangeRate.setStatus(this.getStatus());
		// currencyExchangeRate.setVersion(this.getVersion());
		currencyExchangeRate.setFaresUpdatedForExchangeRateChange(this.getFaresUpdatedForExchangeRateChange());
		currencyExchangeRate.setChargesUpdatedForExchangeRateChange(this.getChargesUpdatedForExchangeRateChange());
		currencyExchangeRate.setAnciChargesUpdatedForExchangeRateChange(this.getAnciChargesUpdatedForExchangeRateChange());
		return currencyExchangeRate;
	}

	/**
	 * @return the faresUpdatedForExchangeRateChange
	 * @hibernate.property column = "FARE_UPDATED_FOR_EXRATE_CHANGE"
	 */
	public String getFaresUpdatedForExchangeRateChange() {
		return faresUpdatedForExchangeRateChange;
	}

	/**
	 * @param faresUpdatedForExchangeRateChange
	 *            the faresUpdatedForExchangeRateChange to set
	 */
	public void setFaresUpdatedForExchangeRateChange(String faresUpdatedForExchangeRateChange) {
		this.faresUpdatedForExchangeRateChange = faresUpdatedForExchangeRateChange;
	}
	
	/**
	 * @return the chargesUpdatedForExchangeRateChange
	 * @hibernate.property column = "CHARGE_UPDATED_FOR_EXRATE_CHG"
	 */
	public String getChargesUpdatedForExchangeRateChange() {
		return chargesUpdatedForExchangeRateChange;
	}

	/**
	 * @param chargesUpdatedForExchangeRateChange
	 *            the chargesUpdatedForExchangeRateChange to set
	 */
	public void setChargesUpdatedForExchangeRateChange(String chargesUpdatedForExchangeRateChange) {
		this.chargesUpdatedForExchangeRateChange = chargesUpdatedForExchangeRateChange;
	}
	
	/**
	 * @return the anciChargesUpdatedForExchangeRateChange
	 * @hibernate.property column = "ANCI_CHG_UPD_FOR_EXRATE_CHG"
	 */
	public String getAnciChargesUpdatedForExchangeRateChange() {
		return anciChargesUpdatedForExchangeRateChange;
	}
	
	/**
	 * @param anciChargesUpdatedForExchangeRateChange
	 *            the anciChargesUpdatedForExchangeRateChange to set
	 */
	public void setAnciChargesUpdatedForExchangeRateChange(String anciChargesUpdatedForExchangeRateChange) {
		this.anciChargesUpdatedForExchangeRateChange = anciChargesUpdatedForExchangeRateChange;
	}

}
