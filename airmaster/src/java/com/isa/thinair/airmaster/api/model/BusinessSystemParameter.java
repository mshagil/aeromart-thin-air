/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:13:50
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_APP_PARAMETER"
 * 
 *                  BusinessSystemParameter is the entity class to repesent BusinessSystemParameter model
 * 
 */
public class BusinessSystemParameter extends Persistent {

	private static final long serialVersionUID = -6605128246567645553L;

	public static final String EDITABLE_CODE_Y = "Y";
	public static final String EDITABLE_CODE_N = "N";

	private long paramId;
	private String paramKey;
	private String description;
	private String paramValue;
	private String editable;
	private String paramType;
	private int maxLengthParamVal;
	private String carrierCode;

	/**
	 * returns the paramId
	 * 
	 * @return Returns the paramId.
	 * 
	 * @hibernate.id column = "PARAM_ID" generator-class = "assigned"
	 */
	public long getParamId() {
		return paramId;
	}

	/**
	 * sets the paramId
	 * 
	 * @param paramKey
	 *            The paramId to set.
	 * 
	 */
	public void setParamId(long paramId) {
		this.paramId = paramId;
	}

	/**
	 * returns the Type
	 * 
	 * @return Returns the type.
	 * 
	 * @hibernate.property column ="PARAM_TYPE" update="false"
	 */
	public String getParamType() {
		return paramType;
	}

	/**
	 * sets the paramType
	 * 
	 * @param paramType
	 */
	public void setParamType(String paramType) {

		this.paramType = paramType;
	}

	/**
	 * returns the paramKey
	 * 
	 * @return Returns the paramKey.
	 * 
	 * @hibernate.property column = "PARAM_KEY" update="false"
	 */
	public String getParamKey() {
		return paramKey;
	}

	/**
	 * sets the paramKey
	 * 
	 * @param paramKey
	 *            The paramKey to set.
	 */
	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}

	/**
	 * returns the carrierCode
	 * 
	 * @return Returns the carrierCode.
	 * 
	 * @hibernate.property column = "CARRIER_CODE"
	 * @hibernate.property update = "false"
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * sets the carrierCode
	 * 
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 * @hibernate.property update = "false"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * returns the paramValue
	 * 
	 * @return Returns the paramValue.
	 * 
	 * @hibernate.property column = "PARAM_VALUE"
	 */
	public String getParamValue() {
		return paramValue;
	}

	/**
	 * sets the paramValue
	 * 
	 * @param paramValue
	 *            The paramValue to set.
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	/**
	 * returns the editable
	 * 
	 * @return Returns the editable.
	 * 
	 * @hibernate.property column ="EDITABLE" update="false"
	 */
	public String getEditable() {
		return editable;
	}

	/**
	 * sets the editable
	 * 
	 * @param editable
	 *            .
	 */
	public void setEditable(String editable) {
		this.editable = editable;
	}

	/**
	 * returns the Maximum length of the parameter value
	 * 
	 * @return Returns the maxLengthParamVal.
	 * 
	 * @hibernate.property column ="MAX_LENGTH_PARAM_VALUE" update="false"
	 */
	public int getMaxLengthParamVal() {
		return maxLengthParamVal;
	}

	/**
	 * sets the maxLengthParamVal
	 * 
	 * @param maxLengthParamVal
	 *            The Maximum length of the parameter value to set.
	 */
	public void setMaxLengthParamVal(int maxLengthParamVal) {
		this.maxLengthParamVal = maxLengthParamVal;
	}
}
