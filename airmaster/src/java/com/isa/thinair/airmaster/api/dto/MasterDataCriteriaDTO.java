package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class MasterDataCriteriaDTO implements Serializable {
	private boolean loadFlightNumbers;
	private boolean loadAgents;
	private boolean loadBookingClasses;

	public boolean isLoadFlightNumbers() {
		return loadFlightNumbers;
	}

	public void setLoadFlightNumbers(boolean loadFlightNumbers) {
		this.loadFlightNumbers = loadFlightNumbers;
	}

	public boolean isLoadAgents() {
		return loadAgents;
	}

	public void setLoadAgents(boolean loadAgents) {
		this.loadAgents = loadAgents;
	}

	public boolean isLoadBookingClasses() {
		return loadBookingClasses;
	}

	public void setLoadBookingClasses(boolean loadBookingClasses) {
		this.loadBookingClasses = loadBookingClasses;
	}
}
