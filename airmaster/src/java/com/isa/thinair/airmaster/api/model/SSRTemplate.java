package com.isa.thinair.airmaster.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author rumesh
 * @hibernate.class table = "T_SSR_TEMPLATE"
 * 
 */

public class SSRTemplate extends Tracking {
	private static final long serialVersionUID = 3182130582747508269L;

	private Integer templateId;
	private String templateCode;
	private String description;
	private String status;
	private Set<SSRTemplateCabinQuantity> ssrTemplateCabinQtySet;

	/**
	 * @return the templateId
	 * 
	 * @hibernate.id column="SSR_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_TEMPLATE"
	 */
	public Integer getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the templateCode
	 * 
	 * @hibernate.property column = "SSR_TEMPLATE_CODE"
	 * 
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            the templateCode to set
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * @return the description
	 * 
	 * @hibernate.property column = "SSR_TEMPLATE_DESC"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the ssrTemplateCabinQtySet
	 * 
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="SSR_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.SSRTemplateCabinQuantity"
	 */
	public Set<SSRTemplateCabinQuantity> getSsrTemplateCabinQtySet() {
		return ssrTemplateCabinQtySet;
	}

	/**
	 * @param ssrTemplateCabinQtySet
	 *            the ssrTemplateCabinQtySet to set
	 */
	public void setSsrTemplateCabinQtySet(Set<SSRTemplateCabinQuantity> ssrTemplateCabinQtySet) {
		this.ssrTemplateCabinQtySet = ssrTemplateCabinQtySet;
	}

}
