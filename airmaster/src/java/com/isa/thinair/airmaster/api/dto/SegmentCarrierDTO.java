/**
 * 
 */
package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;

/**
 * @author nafly
 *
 */
public class SegmentCarrierDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private SYSTEM system;

	private String segment;

	/**
	 * @return the system
	 */
	public SYSTEM getSystem() {
		return system;
	}

	/**
	 * @param system
	 *            the system to set
	 */
	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	/**
	 * @return the segment
	 */
	public String getSegment() {
		return segment;
	}

	/**
	 * @param segment
	 *            the segment to set
	 */
	public void setSegment(String segment) {
		this.segment = segment;
	}

}
