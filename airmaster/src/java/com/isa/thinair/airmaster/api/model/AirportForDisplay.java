/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:44
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Haider
 * 
 *         05Mar09
 * 
 * @hibernate.class table = "T_AIRPORT_FOR_DISPLAY"
 * 
 *                  AirportForDisplay is the entity class to represent the translation of airport in other language
 * 
 */
public class AirportForDisplay extends Persistent {

	private static final long serialVersionUID = -3652972751473858233L;
	private Integer airportForDisplayId;
	private String airportCode;

	private String languageCode;

	private String airportCodeOl;

	private String airportNameOl;

	/**
	 * returns the airportForDisplayId
	 * 
	 * @return Returns the airportForDisplayId.
	 * 
	 * @hibernate.id column = "AIRPORT_FOR_DISPLAY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_FOR_DISPLAY"
	 * 
	 * 
	 */
	public Integer getId() {
		return airportForDisplayId;
	}

	public void setId(Integer id) {
		this.airportForDisplayId = id;
	}

	/**
	 * returns the airportCode
	 * 
	 * @return Returns the airportCode.
	 * 
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * sets the airportCode
	 * 
	 * @param airportCode
	 *            The airportCode to set.
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * returns the airportNameOl
	 * 
	 * @return Returns the airportCodeOl.
	 * 
	 * @hibernate.property column = "AIRPORT_CODE_OL"
	 */
	public String getAirportCodeOl() {
		return airportCodeOl;
	}

	/**
	 * sets the airportNameOl
	 * 
	 * @param airportCodeOl
	 *            The airportNameOl to set.
	 */
	public void setAirportCodeOl(String airportCodeOl) {
		this.airportCodeOl = airportCodeOl;
	}

	/**
	 * returns the languageCode
	 * 
	 * @return Returns the languageCode.
	 * 
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * sets the languageCode
	 * 
	 * @param languageCode
	 *            The languageCode to set.
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return the airportNameOl
	 * @hibernate.property column = "AIRPORT_NAME_OL"
	 */
	public String getAirportNameOl() {
		return airportNameOl;
	}

	/**
	 * @param airportNameOl
	 *            the airportNameOl to set
	 */
	public void setAirportNameOl(String airportNameOl) {
		this.airportNameOl = airportNameOl;
	}
}
