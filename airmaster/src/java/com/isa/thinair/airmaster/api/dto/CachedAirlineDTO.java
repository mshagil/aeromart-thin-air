package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

/**
 * Lightweight airport dto which is cached by the system
 *
 */
public class CachedAirlineDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String airlineCode;

	private String countryCode;

	public String getAirlineCode() {
		return airlineCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
