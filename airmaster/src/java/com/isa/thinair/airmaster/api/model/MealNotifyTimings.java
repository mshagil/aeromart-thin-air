/**
 * 
 */
package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author asapuge
 * @hibernate.class table = "ML_T_MEAL_NOTIFY_TIMINGS"
 * 
 *                  MealNotifyTimings is the entry class to represent a mealNotificationTiming model
 * 
 */
public class MealNotifyTimings implements Serializable {

	private static final long serialVersionUID = 1L;

	private int mealNotifyTimingId;
	private String airportCode;
	private String notifyStart;
	private int notifyGap;
	private String lastNotify;
	private String status;
	private String flightNo;
	private String applyToAll;
	private Date zuluStartDate;
	private Date zuluEndDate;
	private int version;

	/**
	 * @return the mealNotifyTimingId
	 * @hibernate.id column = "MEAL_NOTIFY_TIMING_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_MEAL_NOTIFY_TIMINGS"
	 */
	public int getMealNotifyTimingId() {
		return mealNotifyTimingId;
	}

	/**
	 * @param mealNotifyTimingId
	 *            the mealNotifyTimingId to set
	 */
	public void setMealNotifyTimingId(int mealNotifyTimingId) {
		this.mealNotifyTimingId = mealNotifyTimingId;
	}

	/**
	 * @return the airportCode
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the notifyStart
	 * @hibernate.property column = "NOTIFY_START"
	 */
	public String getNotifyStart() {
		return notifyStart;
	}

	/**
	 * @param notifyStart
	 *            the notifyStart to set
	 */
	public void setNotifyStart(String notifyStart) {
		this.notifyStart = notifyStart;
	}

	/**
	 * @return the notifyGap
	 * @hibernate.property column = "NOTIFY_GAP"
	 */
	public int getNotifyGap() {
		return notifyGap;
	}

	/**
	 * @param notifyGap
	 *            the notifyGap to set
	 */
	public void setNotifyGap(int notifyGap) {
		this.notifyGap = notifyGap;
	}

	/**
	 * @return the lastNotify
	 * @hibernate.property column = "LAST_NOTIFY"
	 */
	public String getLastNotify() {
		return lastNotify;
	}

	/**
	 * @param lastNotify
	 *            the lastNotify to set
	 */
	public void setLastNotify(String lastNotify) {
		this.lastNotify = lastNotify;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the flightNo
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return the applyToAll
	 * @hibernate.property column = "APPLY_TO_ALL"
	 */
	public String getApplyToAll() {
		return applyToAll;
	}

	/**
	 * @param applyToAll
	 *            the applyToAll to set
	 */
	public void setApplyToAll(String applyToAll) {
		this.applyToAll = applyToAll;
	}

	/**
	 * @return the zuluStartDate
	 * @hibernate.property column = "ZULU_START_DATE"
	 */
	public Date getZuluStartDate() {
		return zuluStartDate;
	}

	/**
	 * @param zuluStartDate
	 *            the zuluStartDate to set
	 */
	public void setZuluStartDate(Date zuluStartDate) {
		this.zuluStartDate = zuluStartDate;
	}

	/**
	 * @return the zuluEndDate
	 * @hibernate.property column = "ZULU_END_DATE"
	 */
	public Date getZuluEndDate() {
		return zuluEndDate;
	}

	/**
	 * @param zuluEndDate
	 *            the zuluEndDate to set
	 */
	public void setZuluEndDate(Date zuluEndDate) {
		this.zuluEndDate = zuluEndDate;
	}

	/**
	 * @return the version
	 * @hibernate.property column = "VERSION"
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(int version) {
		this.version = version;
	}
}
