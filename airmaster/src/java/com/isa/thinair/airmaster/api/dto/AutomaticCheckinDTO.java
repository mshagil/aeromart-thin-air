package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

/**
 * @author aravinth.r
 */
public class AutomaticCheckinDTO implements Serializable {

	private static final long serialVersionUID = 3478201662835565037L;

	private String airportCode;

	private BigDecimal amount;

	private Integer automaticCheckinTemplateId;

	private String status;

	private FlightSegmentTO flightSegmentTo;

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the automaticCheckinTemplateId
	 */
	public Integer getAutomaticCheckinTemplateId() {
		return automaticCheckinTemplateId;
	}

	/**
	 * @param automaticCheckinTemplateId
	 *            the automaticCheckinTemplateId to set
	 */
	public void setAutomaticCheckinTemplateId(Integer automaticCheckinTemplateId) {
		this.automaticCheckinTemplateId = automaticCheckinTemplateId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the flightSegmentTo
	 */
	public FlightSegmentTO getFlightSegmentTo() {
		return flightSegmentTo;
	}

	/**
	 * @param flightSegmentTo
	 *            the flightSegmentTo to set
	 */
	public void setFlightSegmentTo(FlightSegmentTO flightSegmentTo) {
		this.flightSegmentTo = flightSegmentTo;
	}

}
