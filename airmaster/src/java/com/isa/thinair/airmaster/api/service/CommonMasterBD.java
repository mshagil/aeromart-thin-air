/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airmaster.api.criteria.DashBoardMsgSearchCriteria;
import com.isa.thinair.airmaster.api.dto.CurrencyExchangeRateDTO;
import com.isa.thinair.airmaster.api.dto.MealCategoryDTO;
import com.isa.thinair.airmaster.api.dto.PaxTitleDTO;
import com.isa.thinair.airmaster.api.dto.StationDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishedTO;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.FamilyRelationship;
import com.isa.thinair.airmaster.api.model.ItineraryAdvertisement;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.BusinessSystemParameter;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airmaster.api.model.MealCategory;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.model.SpyRecord;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.api.to.BaggageTO;
import com.isa.thinair.airmaster.api.to.DashboardMsgTO;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airsecurity.api.model.ParamSearchDTO;
import com.isa.thinair.commons.api.dto.AirportFlightDetail;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Mohamed Nasly
 * @author Byorn
 */
public interface CommonMasterBD {

	public static final String SERVICE_NAME = "CommonMasterService";

	public void saveCurrency(Currency currency) throws ModuleException;

	public void deleteCurrency(String currencyCode) throws ModuleException;

	public void deleteCurrency(Currency currency) throws ModuleException;

	public Currency getCurrency(String currencyCode) throws ModuleException;

	public Collection<Currency> getCurrencies(boolean bookVisibility) throws ModuleException;

	public Collection<Currency> getCurrencies() throws ModuleException;

	public Collection<Currency> getActiveCurrencies() throws ModuleException;
	
	public Collection<FamilyRelationship> getFamilyRelationships() throws ModuleException;

	public Page getCurrencies(List<ModuleCriterion> moduleCriterionList, int startRecIndex, int numberOfRecs)
			throws ModuleException;

	/**
	 * Returns CurrencyExchangeRate (with Currency included) for the requested currency and effective date.
	 * 
	 * @param currencyCode
	 *            CurrencyCode
	 * @param exRateEffectiveDate
	 *            Exchange Rate Effective Date
	 * @return CurrencyExchangeRate
	 * @throws ModuleException
	 */
	public CurrencyExchangeRate getExchangeRate(String currencyCode, Date exRateEffectiveDate) throws ModuleException;

	/**
	 * Returns exchange rate of toCurrCode with respect to the fromCurrCode for the specified effective date.
	 * 
	 * @param fromCurrCode
	 * @param toCurrCode
	 * @param exRateEffectiveDate
	 * @param isPurchaseExRate
	 * @return
	 * @throws ModuleException
	 */
	public BigDecimal getExchangeRateValue(String fromCurrCode, String toCurrCode, Date exRateEffectiveDate,
			Boolean isPurchaseExRate) throws ModuleException;

	public Nationality getNationality(int nationalityId) throws ModuleException;

	/**
	 * Retrieves the nationality by the provided ISO Nationality Code
	 * 
	 * @param isoCode
	 *            The ISO Code of the nationality to be loaded.
	 * @return Nationality for the passed ISO Code or null if the passed ISO Code is null or doesn't exist in the
	 *         system.
	 * @throws ModuleException
	 *             If any of the underlying operations throws an exception
	 */
	public Nationality getNationality(String isoCode) throws ModuleException;

	public Collection<Nationality> getNationalities() throws ModuleException;

	public Page getNationalities(int startRecIndex, int numOfRecs) throws ModuleException;

	public Page getRouteInfo(List<ModuleCriterion> criteria, int startRec, int noOfRecs) throws ModuleException;

	public RouteInfo getRouteInfo(String RouteCode) throws ModuleException;

	public Collection<RouteInfo> getRouteInfo(String fromAirport, String toAirport) throws ModuleException;

	public void saveRouteInfo(RouteInfo object) throws ModuleException;

	public void removeRouteInfo(String routeCode) throws ModuleException;

	public void removeRouteInfo(RouteInfo routeInfo) throws ModuleException;

	public BusinessSystemParameter getBusinessSystemParameter(String carriorCode, String paramKey) throws ModuleException;

	public Collection<BusinessSystemParameter> getBusinessSystemParameters() throws ModuleException;

	public Page searchBusinessSystemParameters(ParamSearchDTO paramSearchDTO, int startIndex, int noOfRecs)
			throws ModuleException;

	public void saveBusinessSystemParameters(Collection<BusinessSystemParameter> parameters) throws ModuleException;

	public void saveBusinessSystemParameter(BusinessSystemParameter parameter, String appCode) throws ModuleException;

	/**
	 * Generates the routes, if there is a hub in the leg
	 * 
	 * @param routeInfo
	 * @throws ModuleException
	 * @author Dhanusha
	 * @since 28 Feb, 2008
	 */
	public void generateRoutes(RouteInfo routeInfo) throws ModuleException;

	public HashMap<Integer, String> getGdsCodes() throws ModuleException;

	public Collection<CurrencyExchangeRate> getAllUpdatedCurr(Date date) throws ModuleException;

	public void saveOrUpdateCurrencyExRate(Collection<CurrencyExchangeRate> colCurrExRate) throws ModuleException;

	public int getAIGDefaultSelected(String dest) throws ModuleException; // Haider

	public String getAIGTCSuffix(String dest) throws ModuleException; // Haider
																		// get
																		// terms
																		// and
																		// condition
																		// link
																		// sufix

	public boolean isAIGCountryCodeActive(String airportCode) throws ModuleException;

	public void saveMeal(Meal meal) throws ModuleException;

	public void deleteMeal(Integer mealId) throws ModuleException;

	public Page<Meal> getMeals(int startrec, int numofrecs) throws ModuleException;

	public Meal getMeal(String mealName) throws ModuleException;

	public Collection<Meal> getMeals() throws ModuleException;

	public Collection<Meal> getAllMeals(String cabinClass, String logicalCabinClass) throws ModuleException;

	public Page<Meal> getMeals(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	public Integer saveMealCategory(MealCategory mealCategory) throws ModuleException;

	public void deleteMealCategory(Integer mealCategoryId) throws ModuleException;

	public Page getMealCategories(int startrec, int numofrecs) throws ModuleException;

	public MealCategory getMealCategory(String mealCategoryName) throws ModuleException;

	public Collection<MealCategory> getMealCategories() throws ModuleException;

	public Page getMealCategories(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	public Collection<CurrencyExchangeRate> getAllUpdatedCurrForCharges(Date date) throws ModuleException;

	public Collection<CurrencyExchangeRate> getAllUpdatedCurrFares(Date date) throws ModuleException;

	/**
	 * Retrives LCC Unpublished RouteInfo - RouteInfo with is_lcc_published status set to
	 * {@link RouteInfo.LCC_PUBLISHED_FALSE}
	 * 
	 * @throws ModuleException
	 * @since 20 Oct 2009
	 */
	public List<RouteInfo> getLCCUnPublishedRouteInfo() throws ModuleException;

	public RouteInfo getRouteInfoByID(Long routeInfoID) throws ModuleException;

	/**
	 * Sets the LCC publish status of RouteInfo Original Objects to {@link RouteInfo.LCC_PUBLISHED_TRUE} if the versions
	 * are the same
	 * 
	 * @param originalRI
	 *            the original RouteInfo objects sent to LCC
	 * @param updatedRI
	 *            the updated RouteInfo objectts sent from LCC WS call after publishing
	 * @throws ModuleException
	 */
	public void updateRouteInfoLCCPublishStatus(List<RouteInfo> originalRI, List<String> updatedRI) throws ModuleException;

	public Country getCountryByName(String countryCode) throws ModuleException;

	public String getCountryCodeByAirportCode(String countryCode) throws ModuleException;

	public Collection<Country> getActiveCountries() throws ModuleException;

	public Page getDashBoardMessages(DashBoardMsgSearchCriteria dashBoardMsgSearchCriteria, int start, int totalRec)
			throws ModuleException;

	public void saveDashboardMsg(DashboardMsgTO dashboardMessageTO) throws ModuleException;

	public List<DashboardMessage> getNewDashbrdMsgIDs() throws ModuleException;

	public void updateDashboardNewMessageStatus(List<Integer> msgIds, String msgStatus) throws ModuleException;

	public void saveSpyMessages(Collection<SpyRecord> colSpyRecord) throws ModuleException;

	public void updateExchangeRate() throws ModuleException;

	public String getCurrencyCodeForAirportCountry(String airportCode) throws ModuleException;

	public String getCurrencyCodeForStation(String stationCode) throws ModuleException;

	public void publishOndListAsJavascript(OndPublishedTO ondList) throws ModuleException;

	public void publishMasterDataUpdates() throws ModuleException;

	public void publishUserInfoUpdates() throws ModuleException;

	public <T> void publishSpecificUserDataUpdate(T unpublishedUserData) throws ModuleException;

	public OndPublishedTO getOndListForSystem() throws ModuleException;

	public Collection<Meal> getMeals(Collection<Integer> mealIds) throws ModuleException;

	public Collection<Meal> getMealsByMealCharge(Collection<Integer> mealChargeIds) throws ModuleException;

	public Country getCountryFromIPAddress(long originIp) throws ModuleException;

	public String getMarketingCarrierFromCountry(String countryCode) throws ModuleException;

	public Page<Baggage> getBaggages(int startrec, int numofrecs) throws ModuleException;

	public Page<Baggage> getBaggages(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	public void saveBaggage(Baggage baggage, BaggageTO baggageTo) throws ModuleException;

	public Baggage getBaggage(String baggageName) throws ModuleException;

	public void deleteBaggage(Integer baggageId) throws ModuleException;

	public Collection<Baggage> getBaggages(Collection<Integer> baggageIds) throws ModuleException;

	public Collection<Language> getLanguages() throws ModuleException;

	public String getCountryByIpAddress(String ipAddress) throws ModuleException;
	
	public SYSTEM getSearchSystem(List<OriginDestinationInformationTO> ondInfoTOList, String requestingCarrier) throws ModuleException;

	public Collection<Baggage> getBaggages() throws ModuleException;

	public Collection<Baggage> getBaggagesCOS(String cabinClass, String logicalCabinClass) throws ModuleException;

	public Collection<Country> getBSPCountries() throws ModuleException;

	public Country getBSPCountryByCode(String countryCode) throws ModuleException;

	public void saveMealCatLanTranslation(MealCategoryDTO mealCatDTO) throws ModuleException;

	public void saveDashboardMsg(List<DashboardMsgTO> dashboardMessageTOs) throws ModuleException;

	/**
	 * Deletes {@link I18nMessageKey}s associated with the LogicalCabinClass
	 * 
	 * @param lcc
	 *            LogicalCabinClass whose associated I18nMessageKeys need to be deleted
	 * @throws ModuleException
	 *             If any underlying code throws an exception
	 */
	public void deleteI18nMessages(LogicalCabinClass lcc) throws ModuleException;

	/**
	 * Creates {@link I18nMessageKey}s associated with the LogicalCabinClass
	 * 
	 * @param lcc
	 *            LogicalCabinClass whose associated I18nMessageKeys need to be created and the data that must be put in
	 *            the created I18nMessageKeys
	 * @throws ModuleException
	 *             If any underlying code throws an exception
	 */
	public void saveI18nMessageKeys(LogicalCabinClass lcc) throws ModuleException;

	/**
	 * Updates {@link I18nMessageKey}s associated with the LogicalCabinClass. Updating for English translations only
	 * 
	 * @param lcc
	 *            LogicalCabinClass whose associated I18nMessageKeys need to be updated and the data that must be put in
	 *            the updated I18nMessageKeys
	 * @throws ModuleException
	 *             If any underlying code throws an exception
	 */
	public void updateI18nMessageKeys(LogicalCabinClass lcc) throws ModuleException;

	/**
	 * Returns translated messages in one language for the passed Set of i18n message keys.
	 * 
	 * @param language
	 *            Language the translations are required
	 * @param keySet
	 *            Set of keys to which the translations are required
	 * @return Results containing the translations in the form of Map<i18n message key,translated message>
	 * @throws ModuleException
	 *             If any of the underlying operations throw an exception.
	 */
	public Map<String, String> getTranslatedMessages(String language, Set<String> keySet) throws ModuleException;

	/**
	 * Returns all the language translations for LogicalCabinClass
	 * 
	 * @return Map that will hold the translations in the format of Map<i18n message key,Map<language,message content>>
	 * @throws ModuleException
	 *             If any of the underlying operations throw an exception.
	 */
	public Map<String, Map<String, String>> getTranslatedMessagesForLCC() throws ModuleException;

	/**
	 * Returns all the language translations for the provided i18n message keys
	 * 
	 * @param messageyKeys
	 *            Collection of i18n message keys whose translations need to be retrieved.
	 * 
	 * @return Map that will hold the translations in the format of Map<i18n message key,Map<language,message content>>
	 * 
	 * @throws ModuleException
	 *             If any of the underlying operations throw an exception.
	 */
	public Map<String, Map<String, String>> getTranslatedMessagesForKeys(Collection<String> messageyKeys) throws ModuleException;

	/**
	 * Loads I18nMessageKey and I18nMessage classes and updates message content. The I18nMessageKey need to be
	 * pre-existing. For new I18nMessageKeys consider {@link #saveTranslations(Map, String, String)}
	 * 
	 * @param translations
	 *            A map in the form of Map<MessageKey,Map<Locale,Translated Messages>>
	 * @throws ModuleException
	 *             If any of the underlying operations throw an exception.
	 */
	public void saveTranslations(Map<String, Map<String, String>> translations) throws ModuleException;

	/**
	 * Loads I18nMessageKey and I18nMessage classes and updates message content. If the keys doesn't exist it will be
	 * created with the provided category.
	 * 
	 * @param translations
	 *            A map in the form of Map<MessageKey,Map<Locale,Translated Messages>>
	 * 
	 * @param i18nMessageKey
	 *            The message key of the translation data.
	 * @param i18nMessageCategory
	 *            Category of the message key provided.
	 * @throws ModuleException
	 *             If any of the underlying operations throw an exception.
	 */
	public void saveTranslations(Map<String, String> translations, String i18nMessageKey, String i18nMessageCategory)
			throws ModuleException;

	public Collection<String> getAirportCodesForCountry(String countryCode) throws ModuleException;

	public void saveTranslationWithoutMessageKey(long id, Map<String, String> translations) throws ModuleException;

	public Map<String, Map<String, String>> getTranslations(String category) throws ModuleException;

	public Collection<Country> getPnrGovEnabledCountries() throws ModuleException;

	public Map<String, String> getFlightNumbers() throws ModuleException;

	public Map<String, String> getBookingClasses() throws ModuleException;

	public Map<String, String> getAgents() throws ModuleException;

	public Map<String, String> getBookingClassTypes(Collection<String> bcCodes) throws ModuleException;

	public Territory getTerritory(String territoryCode) throws ModuleException;
	
	public List<CurrencyExchangeRateDTO> getExchangeRates() throws ModuleException;
	
	public List<PaxTitleDTO> getPaxTitleList() throws ModuleException;
	
	public Map<String, List<PaxTitleDTO>> getPaxTypeWiseTitleList() throws ModuleException;

	public List<AirportMessageDisplayDTO> airportMessages(List<AirportFlightDetail> airportFlightDetails) throws ModuleException;
	
	public void deleteAdvertisement(Integer advertisementId) throws ModuleException;
	
	public String getAdvertisementSegmentWise(String OND) throws ModuleException;
	
	public String getAdvertisementImage(String ONDsegment, String Language) throws ModuleException;
	
	public void saveAdvertisement(ItineraryAdvertisement advertisement) throws ModuleException;
	
	public ItineraryAdvertisement getAdvertisement(Integer advertisementId) throws ModuleException;
	
	public ItineraryAdvertisement getAdvertisement(String advertisementTitle, String advertisementLanguage) throws ModuleException;
	
	public Page<ItineraryAdvertisement> getAdvertisements(int startrec, int numofrecs) throws ModuleException;
	
	public Page<ItineraryAdvertisement> getAdvertisements(int startrec, int numofrecs, List<ModuleCriterion> criteria)
			throws ModuleException;

	public void saveStates(State state) throws ModuleException;

	public void deleteState(Integer stateId) throws ModuleException;

	public Page<State> getStates(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	public Page<State> getStates(int startrec, int numofrecs) throws ModuleException;

	public State getState(Integer stateId) throws ModuleException;

	public State getState(String stateCode) throws ModuleException;

	public List<State> getStates(String countryCode) throws ModuleException;

	public Map<String, List<State>> getCountryWiseStates() throws ModuleException;

	public StationDTO getStationInfo(String stateCode) throws ModuleException;

	public State getTaxApplicableState(int chargeRateID) throws ModuleException;

	public ChargeRate getChargeRateValue(int chargeRateID) throws ModuleException;

	public String getCurrencyCodeForCityCountry(String cityCode) throws ModuleException;
	
	public void saveCity(City city) throws ModuleException;

	public void deleteCity(Integer cityId) throws ModuleException;

	public Page<City> getCities(int startrec, int numofrecs, List<ModuleCriterion> criteria) throws ModuleException;

	public Page<City> getCities(int startrec, int numofrecs) throws ModuleException;

	public City getCity(Integer cityId) throws ModuleException;

	public List<City> getCities(String countryCode) throws ModuleException;

	public Map<String, List<City>> getCountryWiseCities() throws ModuleException;
	
	public City getCity(String cityCode, String countryCode) throws ModuleException;
}
