/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.bl;

import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.core.persistence.dao.AirportDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author byorn
 *
 */
public class TestAirportBL extends PlatformTestCase{

	
//	public void testGetDSTS() throws Exception
//	{
//		AirportBL airportBL = new AirportBL();
//		Collection tos = airportBL.getAirportDSTs("SHJ");
//		Iterator iterator = tos.iterator();
//		while(iterator.hasNext()){
//			AirportDstTO airportDstTO = (AirportDstTO) iterator.next();
//			AirportDST airportDST = airportDstTO.getAirportDST();
//			System.out.println(airportDST.toString());
//			System.out.println(airportDstTO.getContainsFlights());
//		}	
//	}
	
	
//	public void testGetSitaAddresses() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//        AirportDAO airportdao= (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//     
//        SitaSearchCriteria criteria = new SitaSearchCriteria();
//        criteria.setAirportId("CMB");
//        criteria.setSitaAddress("o");
//        criteria.setActiveStatus(SITAAddress.ACTIVE_STATUS_YES);
//        
//        System.out.println(criteria.toString());
//        Page p = airportdao.getSITAAddresses(criteria, 0, 10);
//	Collection c = p.getPageData();
//	System.out.println(p.getTotalNoOfRecords());
//	System.out.println("SIZE:" + p.getPageData().size());
//	Iterator iterator = c.iterator();
//	while(iterator.hasNext()){
//		System.out.println(iterator.next().toString());
//	}
	
	
//	public void testSaveNewAirportDST() throws Exception{
//
//
//		
//        LookupService lookup = LookupServiceFactory.getInstance();
//        AirportDAO airportdao= (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//        
//        
//        AirportBL airportBL = new AirportBL();
//        
//        List list = airportdao.getSITAAddresses("CMB");
//      		
//        SITAAddress address = new SITAAddress();
//        address.setActiveStatus(address.ACTIVE_STATUS_YES);
//        address.setAddressType(address.ADDRESS_TYPE_SECONDARY);
//        address.setAirportCode("AA3");
//        address.setStatus(address.STATUS_ACTIVE);
//        address.setSitaLocation("byorn");
//        address.setSitaAddress("AFAS");
//        try{
//        airportBL.saveSITAAddress(address);
//        }
//        catch(ModuleException e)
//        {
//            System.out.println(e.getExceptionCode());
//        }
//        
//        
//		
//	}
//	
	private AirportDAO getAirportDAO(){
		LookupService lookup = LookupServiceFactory.getInstance();
		return (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
	}
	private AirportBD getAirportService(){
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule airmasterModule = lookup.getModule("airmaster");
		System.out.println("Lookup successful...");
		AirportBD delegate = null;
		try {
			delegate = (AirportBD)airmasterModule.getServiceBD("airport.service.remote");
			
			System.out.println("Delegate creation successful...");
			return delegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return delegate;
	}

	public void testSaveAndRemoveSITAaddress() throws Exception{


		        
        AirportSITABL airportSITABL = new AirportSITABL();
        
        SITAAddress address = getAirportDAO().getSITAAddress(new Integer(106));
        address.setActiveStatus(address.ACTIVE_STATUS_NO);
        address.setAirportCode("CMB");
        address.setSitaLocation("byorn");
        address.setSitaAddress("qq");
        try{
        	airportSITABL.saveSITAAddress(address);
        	//getAirportService().saveSITAAddress(address);
        }
        catch(ModuleException e)
        {
            System.out.println(e.getExceptionCode());
        }
        
        
		
	}


}
