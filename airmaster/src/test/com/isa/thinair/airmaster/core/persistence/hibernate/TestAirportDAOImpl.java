/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.airmaster.core.persistence.dao.AirportDAO;
import junit.framework.TestCase;

/**
 * @author byorn
 *
 */
public class TestAirportDAOImpl extends TestCase {

	public TestAirportDAOImpl() {
		super();
	}

	public TestAirportDAOImpl(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		//initialize framework
		ModuleFramework.startup();
	}
	
//	public void testSaveAndRemoveSITAaddress() throws Exception{
//
//				
//		LookupService lookup = LookupServiceFactory.getInstance();
//
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		
//      Collection list = (Collection) dao.getSITAAddresses("SHJ");
//       java.util.Iterator iterator = list.iterator();
//        while(iterator.hasNext())
//        {
//        SITAAddress address = (SITAAddress) iterator.next();
//
//			
//		address.setActiveStatus(address.ACTIVE_STATUS_NO);
//		address.setAddressType(address.ADDRESS_TYPE_SECONDARY);
//		address.setSitaAddress("asfd");
//		//address.setSITAId(0);
//
//		address.setSitaLocation("afasfsd");
//
//
//		address.setAirportCode("AA3");
//		address.setStatus(address.STATUS_ACTIVE);
//
//		dao.saveSITAAddress(address);
//        }
//        //dao.removeSITAAddress(2);
//		//assertNull(dao.getSITAAddresses("AA1"));
//		
//	}
//
//	public void testGetSITAaddressPageing() throws Exception{
//	
//			
//			LookupService lookup = LookupServiceFactory.getInstance();
//			AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//			SitaSearchCriteria criteria = new SitaSearchCriteria();
//			//criteria.setActiveStatus("Y");
//			//criteria.setSitaAddress("AAAA   ");
//			//criteria.setAirportId("AA1");
//			Page page = dao.getSITAAddresses(criteria,0,10);
//			assertTrue("no recs", page.getPageData().size()>0);
//			
//			
//		}
//	
//	public void testSaveSITAaddress() throws Exception{
//
//		
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		SITAAddress address = new SITAAddress();
//		address.setActiveStatus(address.ACTIVE_STATUS_NO);
//		address.setAddressType(address.ADDRESS_TYPE_PRIMARY);
//		address.setSitaAddress("asfd");
//		//address.setSITAId(0);
//		address.setSitaLocation("afasfsd");
//		address.setAirportCode("AA1");
//		address.setStatus(address.STATUS_ACTIVE);
//		try{
//		dao.saveSITAAddress(address);}catch(CommonsDataAccessException e)
//		{System.out.println(e.getExceptionCode());}
//		//dao.removeSITAAddress(1);
//		//assertNull(dao.getPrimarySITAAdress("AA1"));
//		
//	}
//	
//	public void testGetActiveSITAs() throws Exception{
//		
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		assertTrue("no records", dao.getActiveSITAAddresses("AA1").size()>0);
//	}
	
//	public void testGetPrimarySITAAddress() throws Exception{
//		
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		assertNotNull(dao.getPrimarySITAAdress("AA1"));
//	}
//	
//	public void testHasPrimarySITAAddress() throws Exception{
//        
//        LookupService lookup = LookupServiceFactory.getInstance();
//        AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//        assertTrue(dao.hasPrimarySITAAdress("AA1"));
//    }
//		
//	public void testHasPrimarySITAAddress1() throws Exception{
//        
//        LookupService lookup = LookupServiceFactory.getInstance();
//        AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//        assertTrue(dao.hasPrimarySITAAdress("AA1"));
//    }
//	public void testSaveAndRemoveAirport() throws Exception{
//		
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		
//		assertNotNull(dao);
//		Airport obj = new Airport();
//		
//		
//		
//		obj.setAirportCode("NNN");
//		obj.setAirportName("AAAAAAAAAA");
//		obj.setStatus(obj.STATUS_ACTIVE);
//		obj.setClosestAirport("AAA");
//		obj.setContact("sfsfsf");
//		obj.setFax("12112");
//		obj.setGmtOffsetAction("+");
//		obj.setGmtOffsetHours(1112);
//		obj.setLatitude("1212");
//		obj.setLatitudeNorthSouth(obj.LATITUDE_NORTH_SOUTH_N);
//		obj.setLongitude("1212");
//		obj.setLongitudeEastWest(obj.LONGITUDE_EAST_WEST_E);
//		obj.setMinConnectionTime(1121);
//		obj.setMinStopoverTime(3323);
//		obj.setOnlineStatus(obj.ONLINE_STATUS_ACTIVE);
//		obj.setRemarks("asfasfa");
//		obj.setTelephone("23423");
//		obj.setTerritoryCode("aaa");
//		
//		
//		//obj.setCountryCode(dao1.getCountry("555").getCountryCode());
//		
//		//obj.setCurrency()
//		
//		dao.saveAirport(obj);
//		dao.removeAirport("NNN");
//		
//		assertNull(dao.getAirport("NNN"));
//		
//		
//	}
//	
//	
//	
//	
//public void testLoadAndUpdate() throws Exception{
//		
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		//CountryDAO dao1 = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
//		assertNotNull(dao);
//		Airport obj = dao.getAirport("AA1");
//		String oldairportname = obj.getAirportName();
//		String newairportname= "new airport name";
//		obj.setAirportName(newairportname);
//		dao.saveAirport(obj);
//		assertNotSame(oldairportname, dao.getAirport("AA1"));
//		
//		//assertNull(dao.getAirport("FFF"));
//		
//		
//	}
//	
//	
//	public void testSaveDsts() throws Exception{
//		
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		//CountryDAO dao1 = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
//		assertNotNull(dao);
//		AirportDST obj = new AirportDST();
//		obj.setAirportCode("AA3");
//		obj.setDstAdjustTime(12);
//		obj.setDstEndDateTime(new Date());
//		obj.setDstStartDateTime(new Date());
//		obj.setDstCode(56);
//		
//			
//		dao.saveAirportDST(obj);
//		dao.removeAirportDST("56","AAA");
//	}
//	
//	
//	public void testGetAirportDSTs() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		System.out.println("Getting DSTS for airport AAA");
//		List l = dao.getDSTs("AAA");
//		assertNotNull(l);
//		//System.out.println(l);
//	}
//	
//	public void testGetAirport() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		System.out.println("Getting Airport AAA ");
//		Airport s = dao.getAirport("AAA");
//		assertNotNull(s);
//	//	System.out.println(s);
//	}
//	
//	public void testGetPaging() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		//System.out.println("Getting Airports Paging ");
//		Page p  = dao.getAirports(1,3);
//		
//	}
//	
//	public void testGetAirports() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		//System.out.println("Getting List of Airports");
//		List l = dao.getAirports();
//		assertNotNull(l);
//		//System.out.println(l);
//	}
//	
//	public void testGetCurrentDST() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		
//		AirportDST dst=null;
//		System.out.println("Getting current DST for Airport code AAA");
//		try{
//		dst = dao.getCurrentDST("AAA");
//		}
//		catch(CommonsDataAccessException e1)
//		{
//			System.out.println("EXCEPTION CODE CAUGHT: -" + e1.getExceptionCode());
//		}
//		assertNotNull(dst);
//		System.out.println(dst.toString());
//	
//	}
//	
//	
	public void testGetAirportsForCountry() throws Exception{
		LookupService lookup = LookupServiceFactory.getInstance();
		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
		System.out.println("Getting airports for country ww");
		
		List l = dao.getAirportsForCountry("AE");
		System.out.println(l.size());
		assertNotNull(l.get(0));
		//System.out.println(l.toString());
	
	}     
//	
//	public void testGetOfflineAirports() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//	
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		
//		System.out.println("Getting Offline Airports");
//		Page  p = dao.getOfflineAirports(1,3);
//		assertNotNull(p);
//		
//	}
//	
//	public void testGetOnlineAirports() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		
//		System.out.println("Getting Online Airports");
//		Page  p = dao.getOnlineAirports(1,3);
//		assertNotNull(p);
//		
//	}
//	
//	public void testRemoveAirportDST() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		
//		System.out.println("Removing AirportDST");
//		dao.removeAirportDST("111", "AAA");
//		//assertNotNull(l);
//		//System.out.println(l);
//	}
//
//	public void testGetActiveAirports()  throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AirportDAO dao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
//		
//		System.out.println("Removing AirportDST");
//		List l = dao.getActiveAirports();
//		assertTrue("No records", l.size()>0);
//	}
//	
	
	protected void tearDown(){	
	}

}
