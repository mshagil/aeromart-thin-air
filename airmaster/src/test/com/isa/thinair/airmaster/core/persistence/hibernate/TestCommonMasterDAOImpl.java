/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.math.BigDecimal;
import java.util.Collection;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;


import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.core.controller.ModuleFramework;


import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.RouteInfo;

import com.isa.thinair.airmaster.core.persistence.dao.CommonMasterDAO;

//import com.isa.thinair.airmaster.api.model.CommonMasterModel;
//import com.isa.thinair.airmaster.api.model.CommonMasterCcCapacity;
import com.isa.thinair.airmaster.api.model.BusinessSystemParameter;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

import junit.framework.TestCase;

/**
 * @author Byorn
 *
 */

public class TestCommonMasterDAOImpl extends TestCase {

	Nationality nationality = null;
	public TestCommonMasterDAOImpl() {
		super();
	}

	public TestCommonMasterDAOImpl(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		//initialize framework
		ModuleFramework.startup();
	}
	
	public void testSaveModelRouteInfo() throws ModuleException 
	{
		
		/*LookupService lookup = LookupServiceFactory.getInstance();
		
		CommonMasterDAO dao = (CommonMasterDAO) lookup.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
			
		assertNotNull(dao);
		
		RouteInfo mri = new RouteInfo();
			mri.setDistance(new BigDecimal(34.22));
			mri.setDuration(12);
			mri.setDurationTolerance(new BigDecimal(3.33));
			mri.setFromAirportCode("AAA");
			mri.setToAirportCode("AAA");
			//mri.set
			//mri.setModelNumber("AAA");
			dao.saveRouteInfo(mri);
		*/
	}
	
	public void testGetModelRouteInfoForCityPairID() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		CommonMasterDAO dao = (CommonMasterDAO) lookup.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
			
		assertNotNull(dao);
		
		RouteInfo mrs =  dao.getRouteInfo("AAA/AAA");
		assertNotNull(mrs);
	
	}
	public void testGetNationalities() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		CommonMasterDAO dao = (CommonMasterDAO) lookup.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
			
		assertNotNull(dao);
		
		List l = dao.getNationalitys();
		assertNotNull(l);
		
		for(Iterator iter = l.iterator();iter.hasNext();){
	        nationality = (Nationality)iter.next();
	        
	        assertNotNull(nationality);
	        System.out.println("**********************************************************");
	        System.out.println("Nationality ID:" + nationality.getNationalityCode() + "   Description: " + nationality.getDescription());
	        	        
		}
	
		
		
		
	}

	public void testGetModelRouteInfoForBetweenAirports(String fromAirport, String toAirport) 
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		CommonMasterDAO dao = (CommonMasterDAO) lookup.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
			
		assertNotNull(dao);
		
		Collection l  =  dao.getRouteInfo("AAA", "CCC");
		assertTrue("No Records in Collection", l.size() > 0);
	}

	
	
	public void testGetNationalitiesPaging() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		CommonMasterDAO dao = (CommonMasterDAO) lookup.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
			
		assertNotNull(dao);
		
		Page p  = dao.getNationalitys(1,10);
		Collection l = p.getPageData();
		System.out.println("Get Nationalities Paging");
		for(Iterator iter = l.iterator();iter.hasNext();){
	        nationality = (Nationality)iter.next();
	        assertNotNull(nationality);
	        System.out.println("**********************************************************");
	        System.out.println("Nationality ID:" + nationality.getNationalityCode() + "   Description: " + nationality.getDescription());
	        };
		
	}
	
	public void testGetNationality() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		CommonMasterDAO dao = (CommonMasterDAO) lookup.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
			
		assertNotNull(dao);
		
		Nationality n = dao.getNationality(1);
		
		System.out.println("Get Nationality for code 1");
		System.out.println(n.toString());
		
	}
	

	
	public void testSaveBusinessSystemParameters() throws Exception {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonMasterDAO dao = (CommonMasterDAO) lookup.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
		assertNotNull(dao);
		BusinessSystemParameter businessSystemParameter = null;
		Collection col = new ArrayList();
		businessSystemParameter = new BusinessSystemParameter();
		businessSystemParameter.setParamKey("SALES_2");
		businessSystemParameter.setDescription("Payment Terms1");
		businessSystemParameter.setEditable("N");	
		businessSystemParameter.setParamValue("aaaaaaaaaa");
		businessSystemParameter.setVersion(Long.parseLong("4"));
		col.add(businessSystemParameter);
		businessSystemParameter = new BusinessSystemParameter();		
		businessSystemParameter.setParamKey("RES_4");
		businessSystemParameter.setDescription("On-hold Duration1");
		businessSystemParameter.setEditable("N");	
		businessSystemParameter.setParamValue("bbbbbbbbbb");
		businessSystemParameter.setVersion(Long.parseLong("3"));
		col.add(businessSystemParameter);
		dao.saveBusinessSystemParameters(col);
	}	
	
	public void testGetBusinessSystemParameters() throws Exception {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonMasterDAO dao = (CommonMasterDAO) lookup.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
		BusinessSystemParameter businessSystemParameter = null;
		assertNotNull(dao);
		List lst =  dao.getBusinessSystemParameters();
		assertNotNull(lst);		
		for (Iterator iter = lst.iterator(); iter.hasNext();) {
			businessSystemParameter = (BusinessSystemParameter) iter.next();
	        assertNotNull(businessSystemParameter);
	        System.out.println("PARAM_KEY   = " + businessSystemParameter.getParamKey());
	        System.out.println("DESCRIPTION = " + businessSystemParameter.getDescription());
	        System.out.println("PARAM_VALUE = " + businessSystemParameter.getParamValue());
	        //System.out.println("EDITABLE    = " + businessSystemParameter.getEditable());
	        System.out.println("VERSION     = " + businessSystemParameter.getVersion());
		}	
	}	
	
	protected void tearDown(){	
	}

}
