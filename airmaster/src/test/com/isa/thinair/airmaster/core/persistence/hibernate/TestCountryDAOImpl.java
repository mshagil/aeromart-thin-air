/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.airmaster.api.model.Aircraft;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.core.persistence.dao.CommonMasterDAO;
import com.isa.thinair.airmaster.core.persistence.dao.CountryDAO;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.core.persistence.dao.CurrencyDAO;

import junit.framework.TestCase;

/**
 * @author Byorn
 *
 */
public class TestCountryDAOImpl extends TestCase {

	Country country = null;
	//Territory territory = null;
	public TestCountryDAOImpl() {
		super();
	}

	public TestCountryDAOImpl(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		//initialize framework
		ModuleFramework.startup();
	}
	
	
//	public void testSaveAndRemove() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		CountryDAO dao = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
//		//CurrencyDAO cdao = (CurrencyDAO) lookup.getBean("isa:base://modules/airmaster?id=CurrencyDAOProxy");
//		
//		
//		
//		assertNotNull(dao);
//		Country obj = new Country();
//		
//		String countrycode = "TT";
//		String countryname = "sri lanka";
//			
//		
//		obj.setCountryCode(countrycode);
//		obj.setCountryName(countryname);
//		obj.setCurrencyCode("AAA");
//		obj.setStatus(obj.STATUS_ACTIVE);
//		obj.setRemarks("sdfsf");
//		
//		//obj.setVersion(-2);
//		//obj.setCurrency()
//		dao.saveCountry(obj);
//		dao.removeCountry("TT");
//		assertNull(dao.getCountry("TT"));
//		//following should not throw an exception if deletion successful
//	}
//	
//	
//	public void testSave() throws Exception{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		CountryDAO dao = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
//		//CurrencyDAO cdao = (CurrencyDAO) lookup.getBean("isa:base://modules/airmaster?id=CurrencyDAOProxy");
//		
//		
//		
//		assertNotNull(dao);
//		Country obj = new Country();
//		
//		String countrycode = "KK";
//		String countryname = "sri lanka";
//		//Currency c = cdao.getCurrency("AAA");
//		//String currency = c.getCurrencyCode();
//		
//		
//		
//		
//		
//		
//		obj.setCountryCode(countrycode);
//		obj.setCountryName(countryname);
//		obj.setCurrencyCode("AAA");
//		obj.setStatus(obj.STATUS_ACTIVE);
//		obj.setRemarks("sdfsf");
//		obj.setVersion(-1);
//		//obj.setVersion(-2);
//		//obj.setCurrency()
//		dao.saveCountry(obj);
//		
//		assertNotNull(dao.getCountry("KK"));
//		//following should not throw an exception if deletion successful
//	}
	
	public void testLoadAndUpdate() throws Exception{
		LookupService lookup = LookupServiceFactory.getInstance();
		CountryDAO dao = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
		//CurrencyDAO cdao = (CurrencyDAO) lookup.getBean("isa:base://modules/airmaster?id=CurrencyDAOProxy");
		
		
		
		assertNotNull(dao);
		
		Country obj = dao.getCountry("YE");		
			
		String oldcountryname = obj.getCountryName();		
		String newCountryname = "new country name";
				
		
		obj.setCountryName(newCountryname);
		dao.saveCountry(obj);
	//	assertNotSame(oldcountryname, dao.getCountry("aa").getCountryName());
		
	}
	
	
//	public void testGetCountries() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		CountryDAO dao = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
//			
//		assertNotNull(dao);
//		
//		List l = dao.getCountrys();
//		for(Iterator iter = l.iterator();iter.hasNext();){
//	        country = (Country)iter.next();
//	        assertNotNull(country);
//	        System.out.println("**********************************************************");
//	        System.out.println("code:" + country.getCountryCode() + "||| name:" + country.getCountryName() + "currency code:" + country.getCurrencyCode() + "|||status: " + country.getStatus() + "|||remarks: " + country.getRemarks() + "|||Version :" + country.getVersion() ); }
//	
//		
//	}
//	 
//	
//	public void testGetCountriesPaging() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		CountryDAO dao = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
//			
//		assertNotNull(dao);
//		
//		Page p = dao.getCountrys(1, 3);
//		Collection l = p.getPageData();
//		assertTrue("No records found", l.size()>0);
//		//System.out.println("Get Countries Paging");
//		//for(Iterator iter = l.iterator();iter.hasNext();){
//	     //   country = (Country)iter.next();
//	      //  assertNotNull(country);
//	      //  System.out.println("**********************************************************");
//	      //  System.out.println("code:" + country.getCountryCode() + "||| name:" + country.getCountryName() + "currency code:" + country.getCurrencyCode() + "|||status: " + country.getStatus() + "|||remarks: " + country.getRemarks() + "|||Version :" + country.getVersion() ); }
//	
//		
//	}
//	
//	public void testGetActiveCountrys() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		CountryDAO dao = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
//			
//		assertNotNull(dao);
//		
//		List l = dao.getActiveCountries();
//		assertTrue("No records found", l.size()>0);
//		
//		
//	}
//	
//
//	public void testGetCountry() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		CountryDAO dao = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
//			
//		assertNotNull(dao);
//		
//		Country n = dao.getCountry("aa");
//		assertNotNull(n);
//		//as
//		System.out.println("Get Country for code aa");
//		System.out.println(n.toString());
//		
//	}
	
	protected void tearDown(){	
	}

}
