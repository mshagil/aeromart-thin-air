package com.isa.thinair.airreservation.api.utils;

public interface PNLConstants {

	public interface PnlPassengerStates {
		public final String NOT_UPDATED = "N";
		public final String UPDATED = "Y";
	}

	/** Holds the values of the sita msg types **/
	public interface MessageTypes {
		public final String PNL = "PNL";
		public final String ADL = "ADL";
		public final String PAL = "PAL";
		public final String CAL = "CAL";
	}

	public interface PnlAdlFactory {
		public final String PNLADL_MASTER_DATA = "MASTER_DATA";
		public final String PNLADL_ADDITIONAL_DATA = "ADDITIONAL_DATA";
	}
	
	
	/** Holds the values the sending method **/
	public interface SendingMethod {
		public final String SCHEDSERVICE = "SS";
		public final String MANUALLY = "MA";
		public final String RESENDER = "RE";
	}

	/** Holds PNL ADL status Codes **/
	public interface PNLADLStatusCode {
		public final static String HK1 = "HK1";
	}

	/** Holds PNL ADL Elements **/
	public interface PNLADLElements {

		public final int MAX_SINGLE_PAX_FULLNAME_WITH_TITLE_AND_TOURID_LENGTH = 55;
		public final int MAX_LINE_LENGTH_IATA = 64;
		public final String REMARK = ".R/";
		public final String PASSPORT = "PSPT";
		public final String WAITLIST = ".WL/";
		public final String CHILD = "CHD";
		public final String INFANT = "INF";
		public final String SEATMAP = ".R/RQST";
		public final String EPAY = ".R/EPAY";
		public final String ID2N2 = ".ID2/ID00N2";
		public final String SPACE = " ";
		public final String B_SLASH = "/";
		public final String ADL_DEL = "DEL";
		public final String ADL_ADD = "ADD";
		public final String ADL_CHG = "CHG";
		public final String END_ADL = "ENDADL";
		public final String EXTRA_SEAT = ".R/EXST";
		public final String EXST = "EXST";
		public final String INFANT2 = "INFT";
		public final String CHLD = "CHLD";
	}

	/* @TODO : Pnl Generation related constants should be kept here */
	public interface PNLGeneration {
		public final String PNL = "PNL";
		public final String PART_ONE = "PART1";
		public final String RBD = "RBD";

	}

	/** Holds ADL elements **/
	public interface ADLGeneration {
		public final String ADL = "ADL";

		public final String PART_ONE = "PART1";

	}

	/** Holds Error Codes **/
	public interface ERROR_CODES {
		public final String UNEXPECTED_ERROR_CODE = "airreservations.pnladl.unexpected";
		public final String EMPTY_ADL_ERROR_CODE = "reservationauxilliary.adl.noreservations";
		public final String ADL_GENERATION_ERROR_CODE = "airreservations.adl.generation";
		public final String PNL_GENERATION_ERROR_CODE = "airreservations.pnl.generation";
		public final String FLIGHT_CLOSED_ADLERROR_CODE = "airreservations.auxilliary.adltimeexceeded";
		public final String FLIGHT_CLOSED_PNLERROR_CODE = "airreservations.auxilliary.pnltimeexceeded";
		public final String PNL_COUNT_NOT_FOUND_FOR_ADL = "airreservations.auxilliary.pnlcountnotfound";
		public final String EMPTY_PNRSEGS_PAXIDS_ERROR = "airreservations.pnlbl.pnrsegpaxids";
		public final String SITA_STATUS_ERROR = "airreservations.sitastatus.error";
		public final String EMPTY_SITA_ADDRESS = "airreservations.pnladl.emptysita";
		
	//	public final String UNEXPECTED_ERROR_CODE = "airreservations.pnladl.unexpected";
		public final String EMPTY_CAL_ERROR_CODE = "reservationauxilliary.cal.noreservations";
		public final String CAL_GENERATION_ERROR_CODE = "airreservations.cal.generation";
		public final String PAL_GENERATION_ERROR_CODE = "airreservations.pal.generation";
		public final String FLIGHT_CLOSED_CALERROR_CODE = "airreservations.auxilliary.caltimeexceeded";
		public final String FLIGHT_CLOSED_PALERROR_CODE = "airreservations.auxilliary.paltimeexceeded";
		public final String PAL_COUNT_NOT_FOUND_FOR_CAL = "airreservations.auxilliary.palcountnotfound";
		public final String PAL_EMPTY_PNRSEGS_PAXIDS_ERROR = "airreservations.palbl.pnrsegpaxids";
	//	public final String SITA_STATUS_ERROR = "airreservations.sitastatus.error";
	//	public final String EMPTY_SITA_ADDRESS = "airreservations.pnladl.emptysita";

	}

	/** Holds the Flight Status in T_FLIGHT table in DB * */
	public interface FlightStatus {
		public final String FLIGHT_CANCELED = "CNX";
	}

	/** Hold the flight segment validity information * */
	public interface FightSegmentValidity {
		public final String VALID_SEGMENT = "Y";
	}

	/** Hold Sita Message Transmission Details **/
	public interface SITAMsgTransmission {
		public final String SUCCESS = "Y";

		public final String ADL_EMPTY = "E";

		public final String FAIL = "N";

		public final String PARTIALLY = "P";
	}

	/** Denotes the PNLTransmission Record Status */
	public interface PaxPnlAdlStates {
		/** PNL Sent */
		public final String PNL_PNL_STAT = "P";

		/** PNL Not Sent */
		public final String PNL_RES_STAT = "N";

		public final String ADL_ADDED_STAT = "D";

		/** ADL change for ADL sending eligibility */
		public final String ADL_CHANGED_STAT = "C";

		public final String ADL_CANCEL_STAT = "X";

		/** USED IN T_PNL_PASSENGER **/
		public final String REMOVE_PAX = "R";

	}

	/** Denotes the PnlAdlTimings Record Status */
	public static interface PnlAdlTimings {

		public static final String ACTIVE = "ACT";

		public static final String INACTIVE = "INA";

		public static final String APPLY_TO_ALL_YES = "Y";

		public static final String APPLY_TO_ALL_NO = "N";

	}

	public enum AdlActions {
		// TODO remove unnecassary status
		A, D, C, P, X, R, N;
	}
	
	public static interface PAlCALSendingStatus {
		public static final String SEND ="Y";
		public static final String NOT_SEND ="N";
	}
	
	public static interface LogTemplateName {
		public static final String PNL_ADL_LOG ="pnl_adl_log";
		public static final String PAL_CAL_LOG ="pal_cal_log";
	}
}
