package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;
import java.util.Date;

public class SegmentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5264389012757464937L;

	private String fareClass;

	private String departureAirport;

	private String arrivalAirport;

	private Date departureDateTime;

	private Date departureDateTimeZulu;

	private Date arrivalDateTime;

	private Date arrivalDateTimeZulu;

	private String carrier;

	private String flightNumber;

	private String fareBasis;

	private String departureAirportCountry;

	private String departureAirportTimeZone;

	public SegmentDTO() {
		super();
	}

	public String getFareClass() {
		return fareClass;
	}

	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	/**
	 * @return the departureDateTimeZulu
	 */
	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	/**
	 * @param departureDateTimeZulu
	 *            the departureDateTimeZulu to set
	 */
	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	/**
	 * @return the arrivalDateTimeZulu
	 */
	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	/**
	 * @param arrivalDateTimeZulu
	 *            the arrivalDateTimeZulu to set
	 */
	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFareBasis() {
		return fareBasis;
	}

	public void setFareBasis(String fareBasis) {
		this.fareBasis = fareBasis;
	}

	public String getDepartureAirportCountry() {
		return departureAirportCountry;
	}

	public void setDepartureAirportCountry(String departureAirportCountry) {
		this.departureAirportCountry = departureAirportCountry;
	}

	public String getDepartureAirportTimeZone() {
		return departureAirportTimeZone;
	}

	public void setDepartureAirportTimeZone(String departureAirportTimeZone) {
		this.departureAirportTimeZone = departureAirportTimeZone;
	}

}
