package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItineraryAirportTransferTO implements Serializable {

	private static final long serialVersionUID = -8358463517593000252L;

	private String ssrCode;
	private String description;
	private String airportCode;

	private String transferAddress;
	private String transferDate; 
	private String transferType;

	private String providerName;
	private String providerEmail;
	private String providerContact;

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getProviderEmail() {
		return providerEmail;
	}

	public void setProviderEmail(String providerEmail) {
		this.providerEmail = providerEmail;
	}

	public String getProviderContact() {
		return providerContact;
	}

	public void setProviderContact(String providerContact) {
		this.providerContact = providerContact;
	}

	public String getTransferAddress() {
		return transferAddress;
	}

	public void setTransferAddress(String transferAddress) {
		this.transferAddress = transferAddress;
	}

}
