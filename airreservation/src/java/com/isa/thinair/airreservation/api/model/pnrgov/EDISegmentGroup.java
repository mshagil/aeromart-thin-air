package com.isa.thinair.airreservation.api.model.pnrgov;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

public abstract class EDISegmentGroup extends MessageSegment {

	private ArrayList<MessageSegment> segments = new ArrayList<MessageSegment>();

	private int segmentCount = 0;

	public EDISegmentGroup(String segmentName) {
		super(segmentName);
	}

	@Override
	public String getEDIFmtData() throws PNRGOVException {
		if (!isValidDataFormat()) {
			throw new PNRGOVException("Mandatory Data element not found for" + segmentName);
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < segments.size(); i++) {
			MessageSegment segment = segments.get(i);
			String ediFormatData = segment.getEDIFmtData();
			if (!StringUtils.isEmpty(ediFormatData)) {
				if (i < segments.size() - 1) {
					sb.append(ediFormatData).append("\n");
				} else {
					sb.append(ediFormatData);
				}
			}
		}
		return sb.toString();
	}

	public void addMessageSegmentIfNotEmpty(MessageSegment messageSegment) {
		if (messageSegment != null) {
			segments.add(messageSegment.build());
			this.segmentCount = this.segmentCount + messageSegment.getSegmentCount();
		}
	}

	public void addMessageSegmentsIfNotEmpty(ArrayList<? extends MessageSegment> messageSegments) {
		if (messageSegments != null && !messageSegments.isEmpty()) {
			for (MessageSegment messageSegment : messageSegments) {
				segments.add(messageSegment.build());
				this.segmentCount = this.segmentCount + messageSegment.getSegmentCount();
			}
		}
	}

	@Override
	public boolean isValidDataFormat() {
		return true;
	}

	public int getSegmentCount() {
		return segmentCount;
	}

}
