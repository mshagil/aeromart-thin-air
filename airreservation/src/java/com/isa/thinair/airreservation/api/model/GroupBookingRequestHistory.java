package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Gayan
 * 
 * @hibernate.class table="t_group_booking_request_hst"
 */

public class GroupBookingRequestHistory extends Persistent {

	private static final long serialVersionUID = 1L;

	private Long historyID;
	private Long groupBookingRequestID;
	private String groupBookingDetails;
	private Integer previosuStatusID;
	private Integer newStatusID;
	private String addedBy;
	private Date addedDate;

	/**
	 * @hibernate.id column = "GROUP_BOOKING_REQ_HIS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_GROUP_BOOKING_REQUEST_HST"
	 */
	public Long getHistoryID() {
		return historyID;
	}

	public void setHistoryID(Long historyID) {
		this.historyID = historyID;
	}

	/**
	 * @hibernate.property column= "GROUP_BOOKING_REQUEST_ID"
	 * @return requestDate
	 */
	public Long getGroupBookingRequestID() {
		return groupBookingRequestID;
	}

	public void setGroupBookingRequestID(Long groupBookingRequestID) {
		this.groupBookingRequestID = groupBookingRequestID;
	}

	/**
	 * @hibernate.property column= "GROUP_BOOKING_DETAILS"
	 * @return requestDate
	 */
	public String getGroupBookingDetails() {
		return groupBookingDetails;
	}

	public void setGroupBookingDetails(String groupBookingDetails) {
		this.groupBookingDetails = groupBookingDetails;
	}

	/**
	 * @hibernate.property column= "PREV_GRP_BKG_REQ_OPERATION_ID"
	 * @return requestDate
	 */
	public Integer getPreviosuStatusID() {
		return previosuStatusID;
	}

	public void setPreviosuStatusID(Integer previosuStatusID) {
		this.previosuStatusID = previosuStatusID;
	}

	/**
	 * @hibernate.property column= "GRP_BKG_REQ_OPERATION_ID"
	 * @return requestDate
	 */
	public Integer getNewStatusID() {
		return newStatusID;
	}

	public void setNewStatusID(Integer newStatusID) {
		this.newStatusID = newStatusID;
	}

	/**
	 * @hibernate.property column= "CREATED_BY"
	 * @return requestDate
	 */
	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	/**
	 * @hibernate.property column= "CREATED_DATE"
	 * @return requestDate
	 */
	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

}
