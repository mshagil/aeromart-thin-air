/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

/**
 * PNR Number Generator interface
 * 
 * @author Nilindra Fernando
 * @since Sep 12, 2008
 */
public interface PNRNumberGenerator {
	/**
	 * Generate the PNR Sequence Number from the given PNR Number
	 * 
	 * @param pnrNumber
	 * @return
	 */
	public long generateSequenceNo(String pnrNumber);

	/**
	 * Generate the PNR Number from the given PNR Sequence Number
	 * 
	 * @param sequenceNo
	 * @return
	 */
	public String generatePNRNumber(long sequenceNo);
}
