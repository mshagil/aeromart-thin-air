/**
 * 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nafly
 *
 */
public class OnHoldBookingInfoDTO implements Serializable {

	private static final long serialVersionUID = -7098299897995908777L;

	private String pnr;

	private String groupPnr;

	private Date releaseTime;

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the groupPnr
	 */
	public String getGroupPnr() {
		return groupPnr;
	}

	/**
	 * @param groupPnr
	 *            the groupPnr to set
	 */
	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}

	/**
	 * @return the releaseTime
	 */
	public Date getReleaseTime() {
		return releaseTime;
	}

	/**
	 * @param releaseTime
	 *            the releaseTime to set
	 */
	public void setReleaseTime(Date releaseTime) {
		this.releaseTime = releaseTime;
	}

}
