package com.isa.thinair.airreservation.api.dto.eticket;

import java.io.Serializable;

/**
 * @author eric
 * 
 */
public class EticketTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int eticketId;

	private Integer pnrPaxId;

	private Integer pnrSegId;

	private Integer pnrPaxFareSegId;

	private String eticketNumber;

	private Integer couponNo;
	
	private String externalEticketNumber;

	private Integer externalCouponNo;

	private String ticketStatus;

	private int eTicketSequnce;

	private String paxStatus;

	private Integer flightSegId;

	private String externalCouponStatus;

	private String externalCouponControl;

	public int getEticketId() {
		return eticketId;
	}

	public void setEticketId(int eticketId) {
		this.eticketId = eticketId;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public Integer getPnrPaxFareSegId() {
		return pnrPaxFareSegId;
	}

	public void setPnrPaxFareSegId(Integer pnrPaxFareSegId) {
		this.pnrPaxFareSegId = pnrPaxFareSegId;
	}

	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	public Integer getCouponNo() {
		return couponNo;
	}

	public void setCouponNo(Integer couponNo) {
		this.couponNo = couponNo;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public int geteTicketSequnce() {
		return eTicketSequnce;
	}

	public void seteTicketSequnce(int eTicketSequnce) {
		this.eTicketSequnce = eTicketSequnce;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * @return the externalEticketNumber
	 */
	public String getExternalEticketNumber() {
		return externalEticketNumber;
	}

	/**
	 * @param externalEticketNumber the externalEticketNumber to set
	 */
	public void setExternalEticketNumber(String externalEticketNumber) {
		this.externalEticketNumber = externalEticketNumber;
	}

	/**
	 * @return the externalCouponNo
	 */
	public Integer getExternalCouponNo() {
		return externalCouponNo;
	}

	/**
	 * @param externalCouponNo the externalCouponNo to set
	 */
	public void setExternalCouponNo(Integer externalCouponNo) {
		this.externalCouponNo = externalCouponNo;
	}

	public String getExternalCouponStatus() {
		return externalCouponStatus;
	}

	public void setExternalCouponStatus(String externalCouponStatus) {
		this.externalCouponStatus = externalCouponStatus;
	}

	public String getExternalCouponControl() {
		return externalCouponControl;
	}

	public void setExternalCouponControl(String externalCouponControl) {
		this.externalCouponControl = externalCouponControl;
	}
}
