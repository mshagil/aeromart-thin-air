/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To keep track of blocked LMS credit info for on-going bookings
 * 
 * @author M.Rikaz
 * @since Sep, 2017
 * @hibernate.class table = "T_LMS_CREDIT_BLOCKED_INFO"
 */
public class LmsBlockedCredit extends Persistent {

	private static final long serialVersionUID = -1151200450895185077L;

	public static final String PENDING = "PENDING";
	public static final String SUCCESS = "SUCCESS";
	public static final String FAIL = "FAIL";
	public static final String ISSUE = "ISSUE";
	public static final String CANCEL = "CANCEL";

	private Integer lmsBlockedId;

	private Date paymentTimeStamp;

	private String memberId;

	private Integer tempTnxId;

	private BigDecimal blockedCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	@Deprecated
	private BigDecimal totalPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Integer salesChannelCode;

	private String lmsCreditPaymentInfo;

	private Set<String> lmsRewardIds;

	private String remarks;

	private Integer lmsCancelAttempts = 0;

	private String creditUtilizedStatus;
	
	private String pnr;

	/**
	 * @return Returns the lmsBlockedId.
	 * @hibernate.id column = "LMS_CREDIT_BLOCKED_INFO_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_LMS_CREDIT_BLOCKED_INFO"
	 */
	public Integer getLmsBlockedId() {
		return lmsBlockedId;
	}

	public void setLmsBlockedId(Integer lmsBlockedId) {
		this.lmsBlockedId = lmsBlockedId;
	}

	/**
	 * @return Returns the paymentTimeStamp.
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getPaymentTimeStamp() {
		return paymentTimeStamp;
	}

	public void setPaymentTimeStamp(Date paymentTimeStamp) {
		this.paymentTimeStamp = paymentTimeStamp;
	}

	/**
	 * @return Returns the memberId.
	 * @hibernate.property column = "EMAIL_ID"
	 */
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	/**
	 * @return Returns the tempTnxId.
	 * @hibernate.property column = "TPT_ID"
	 */
	public Integer getTempTnxId() {
		return tempTnxId;
	}

	public void setTempTnxId(Integer tempTnxId) {
		this.tempTnxId = tempTnxId;
	}

	/**
	 * @return Returns the blockedCreditAmount.
	 * @hibernate.property column = "BLOCKED_CREDIT_AMOUNT"
	 */
	public BigDecimal getBlockedCreditAmount() {
		return blockedCreditAmount;
	}

	public void setBlockedCreditAmount(BigDecimal blockedCreditAmount) {
		this.blockedCreditAmount = blockedCreditAmount;
	}

	/**
	 * @return Returns the totalPaymentAmount.
	 * @hibernate.property column = "TOTAL_PAYMENT_AMOUNT"
	 */
	@Deprecated
	public BigDecimal getTotalPaymentAmount() {
		return totalPaymentAmount;
	}

	@Deprecated
	public void setTotalPaymentAmount(BigDecimal totalPaymentAmount) {
		this.totalPaymentAmount = totalPaymentAmount;
	}

	/**
	 * @return Returns the salesChannelCode.
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the lmsCreditPaymentInfo.
	 * @hibernate.property column = "LMS_CREDIT_PAYMENT_INFO"
	 */
	public String getLmsCreditPaymentInfo() {
		return lmsCreditPaymentInfo;
	}

	public void setLmsCreditPaymentInfo(String lmsCreditPaymentInfo) {
		this.lmsCreditPaymentInfo = lmsCreditPaymentInfo;
	}

	/**
	 * returns the gds ids related with flight schedule
	 * 
	 * @hibernate.set lazy="false" cascade="all" table="T_LMS_BLOCKED_REWARD_IDS"
	 * @hibernate.collection-element column="LMS_REWARD_REF" type="string"
	 * @hibernate.collection-key column="LMS_CREDIT_BLOCKED_INFO_ID"
	 */
	public Set<String> getLmsRewardIds() {
		return lmsRewardIds;
	}

	public void setLmsRewardIds(Set<String> lmsRewardIds) {
		this.lmsRewardIds = lmsRewardIds;
	}

	public void addLmsRewardId(String lmsRewardId) {
		if (this.lmsRewardIds == null)
			this.lmsRewardIds = new HashSet<String>();
		lmsRewardIds.add(lmsRewardId);
	}

	/**
	 * @return Returns the remarks.
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return Returns the lmsCancelAttempts.
	 * @hibernate.property column = "LMS_CANCEL_ATTEMPTS"
	 */
	public Integer getLmsCancelAttempts() {
		return lmsCancelAttempts;
	}

	public void setLmsCancelAttempts(Integer lmsCancelAttempts) {
		this.lmsCancelAttempts = lmsCancelAttempts;
	}

	/**
	 * @return Returns the creditUtilizedStatus.
	 * @hibernate.property column = "UTILIZED_STATUS"
	 */
	public String getCreditUtilizedStatus() {
		return creditUtilizedStatus;
	}

	public void setCreditUtilizedStatus(String creditUtilizedStatus) {
		this.creditUtilizedStatus = creditUtilizedStatus;
	}

	@Override
	public String toString() {
		return "LmsBlockedCredit [lmsBlockedId=" + lmsBlockedId + ", paymentTimeStamp=" + paymentTimeStamp + ", memberId="
				+ memberId + ", tempTnxId=" + tempTnxId + ", blockedCreditAmount=" + blockedCreditAmount + ", totalPaymentAmount="
				+ totalPaymentAmount + ", salesChannelCode=" + salesChannelCode + ", lmsRewardIds=" + lmsRewardIds + ", remarks="
				+ remarks + ", lmsCancelAttempts=" + lmsCancelAttempts + ", creditUtilizedStatus=" + creditUtilizedStatus + "]";
	}


	/**
	 * @return Returns the PNR.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	

	
	
}
