/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model.assembler;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.LMSPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;

/**
 * PaymentAssembler is the main assembling utility for the payments
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PaymentAssembler implements Serializable, IPayment {

	private static final long serialVersionUID = 703498304937426456L;

	/**
	 * If no payment carrier is specified the default carrier is taken as the payment carrier
	 */
	private static final String DEFALT_PAYMENT_CARRIER = AppSysParamsUtil.getDefaultCarrierCode();

	/** Holds types of PaymentInfo Objects */
	private final Collection<PaymentInfo> payments;

	/** Holds the total payment amount */
	private BigDecimal totalPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds whether a payment exist or not */
	private boolean paymentExist = false;

	/** Holds the total charge amount */
	private BigDecimal totalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds whether or not the external charges are consumed or not */
	private boolean isExternalChargesConsumed = false;

	/** Holds passenger credits */
	private BigDecimal totalPaxCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds per passenger external charges */
	private final Collection<ExternalChgDTO> perPaxExternalCharges;

	private Date zuluOhdReleaseTime;

	/**
	 * Construct Reservation Object
	 */
	public PaymentAssembler() {
		this.payments = new ArrayList<PaymentInfo>();
		this.perPaxExternalCharges = new ArrayList<ExternalChgDTO>();
	}

	/**
	 * Captures the external charges
	 * 
	 * @param colExternalCharges
	 */

	@Override
	public void addExternalCharges(Collection<ExternalChgDTO> colExternalChgDTO) {
		if (colExternalChgDTO != null && colExternalChgDTO.size() > 0) {
			this.perPaxExternalCharges.addAll(colExternalChgDTO);
		}
	}

	public void addExternalCharges(Map<EXTERNAL_CHARGES, ExternalChgDTO> colExternalChgDTO) {
		if (colExternalChgDTO != null && colExternalChgDTO.size() > 0) {
			Collection<ExternalChgDTO> colExternalChgDTO_list = new ArrayList<ExternalChgDTO>(colExternalChgDTO.values());
			this.perPaxExternalCharges.addAll(colExternalChgDTO_list);
		}
	}

	/**
	 * Returns the total per passenger external charges amount
	 * 
	 * @return
	 */
	private BigDecimal getTotalExternalChargesAmount() {
		BigDecimal totalServiceChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (!this.isExternalChargesConsumed && this.perPaxExternalCharges != null && this.perPaxExternalCharges.size() > 0) {
			for (ExternalChgDTO externalChgDTO2 : this.perPaxExternalCharges) {
				ExternalChgDTO externalChgDTO = externalChgDTO2;
				if (!externalChgDTO.isAmountConsumedForPayment()) {
					totalServiceChargeAmount = AccelAeroCalculator.add(totalServiceChargeAmount, externalChgDTO.getAmount());
				}
			}
			this.isExternalChargesConsumed = true;
		}

		return totalServiceChargeAmount;
	}

	@Override
	public BigDecimal getTotalExternalChargesAmountWithoutConsume() {
		BigDecimal totalServiceChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.perPaxExternalCharges != null && this.perPaxExternalCharges.size() > 0) {
			for (ExternalChgDTO externalChgDTO2 : this.perPaxExternalCharges) {
				ExternalChgDTO externalChgDTO = externalChgDTO2;
				if (!externalChgDTO.isAmountConsumedForPayment()) {
					totalServiceChargeAmount = AccelAeroCalculator.add(totalServiceChargeAmount, externalChgDTO.getAmount());
				}
			}
		}
		return totalServiceChargeAmount;
	}

	/**
	 * Add card payment information
	 * 
	 * @param cardType
	 * @param eDate
	 * @param number
	 * @param name
	 * @param address
	 * @param securityCode
	 * @param amount
	 * @param appIndicatorEnum
	 * @param tnxModeEnum
	 * @param oldCCRecordId
	 * @param paymentGateway
	 */
	@Override
	public void addCardPayment(int cardType, String eDate, String number, String name, String address, String securityCode,
			BigDecimal amount, AppIndicatorEnum appIndicatorEnum, TnxModeEnum tnxModeEnum, Integer oldCCRecordId,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, String authorizationId, Integer paymentBrokerRefNo, Integer paymentTnxId) {

		addCardPayment(cardType, eDate, number, name, address, securityCode, amount, appIndicatorEnum, tnxModeEnum,
				oldCCRecordId, ipgIdentificationParamsDTO, null, null, paymentCarrier, payCurrencyDTO, lccUniqueTnxId,
				authorizationId, paymentBrokerRefNo, null, paymentTnxId);
	}

	@Override
	public void addCardPayment(int cardType, String eDate, String number, String name, String address, String securityCode,
			BigDecimal amount, AppIndicatorEnum appIndicatorEnum, TnxModeEnum tnxModeEnum, Integer oldCCRecordId,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, String paymentRef, Integer payMode, String paymentCarrier,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, String authorizationId, Integer paymentBrokerRefNo,
			UserInputDTO userInputDTO, Integer paymentTnxId) {

		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, getTotalExternalChargesAmount());
		CardPaymentInfo cardPaymentInfo = new CardPaymentInfo();

		// paypal
		if (userInputDTO != null) {
			cardPaymentInfo.setUserInputDTO(userInputDTO);
		}

		cardPaymentInfo.setType(cardType);
		cardPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);

		// defaulting to the default carrier if no payment carrier is specified
		cardPaymentInfo.setPayCarrier((paymentCarrier == null ? DEFALT_PAYMENT_CARRIER : paymentCarrier));

		cardPaymentInfo.setLccUniqueId(lccUniqueTnxId);

		cardPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));

		cardPaymentInfo.setAppIndicator(appIndicatorEnum);
		cardPaymentInfo.setTnxMode(tnxModeEnum);
		cardPaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		if (paymentRef != null || payMode != null) {
			PaymentReferenceTO paymentReferanceTO = getPayReferenceTO(paymentRef, payMode);
			cardPaymentInfo.setPaymentReferanceTO(paymentReferanceTO);
		}
		/*
		 * Add generic card payment information This means we don't know the type of the credit card we simply make it
		 * Generic
		 */
		if (PaymentType.CARD_GENERIC.getTypeValue() != cardType) {
			cardPaymentInfo.setEDate(eDate);
			cardPaymentInfo.setNo(number);
			cardPaymentInfo.setName(name);
			cardPaymentInfo.setAddress(address);
			cardPaymentInfo.setSecurityCode(securityCode);

			// If any old payment broker reference passed
			if (oldCCRecordId != null) {
				cardPaymentInfo.setOldCCRecordId(oldCCRecordId.intValue());
			}
		} else {
			if (number != null && !number.isEmpty()) {
				cardPaymentInfo.setNo(number);
			}
			if (eDate != null) {
				cardPaymentInfo.setEDate(eDate);
			}

			if (name != null) {
				cardPaymentInfo.setName(name);
			}

			if (securityCode != null) {
				cardPaymentInfo.setSecurityCode(securityCode);
			}
			// If any old payment broker reference passed
			if (oldCCRecordId != null) {
				cardPaymentInfo.setOldCCRecordId(oldCCRecordId.intValue());
			}
		}
		cardPaymentInfo.setAuthorizationId(authorizationId);
		if (paymentBrokerRefNo != null) {
			cardPaymentInfo.setPaymentBrokerRefNo(paymentBrokerRefNo);
		}
		cardPaymentInfo.setPaymentTnxId(paymentTnxId);

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(cardPaymentInfo);
	}

	/**
	 * Agent Credit Payment with payment reference
	 * 
	 * @param agentCode
	 * @param amount
	 * @param paymentReferenceTO
	 * @param paymentCarrier
	 * @param payCurrencyDTO
	 *            TODO
	 * @param lccUniqueTnxId
	 * @param paymentTnxId
	 *            TODO
	 */
	private void addAgentCreditPaymentWithPaymentRef(String agentCode, BigDecimal amount, PaymentReferenceTO paymentReferenceTO,
			String paymentCarrier, PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId) {
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, getTotalExternalChargesAmount());

		AgentCreditInfo agentCreditInfo = new AgentCreditInfo(agentCode, totalPayAmountWithServiceCharges,
				getNullSafePayCurrencyDTO(payCurrencyDTO), paymentReferenceTO, (paymentCarrier == null
						? DEFALT_PAYMENT_CARRIER
						: paymentCarrier), lccUniqueTnxId, paymentTnxId);
		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(agentCreditInfo);
	}

	/**
	 * Add agent credit information
	 * 
	 * @param amount
	 * @param agentCode
	 */
	@Override
	public void addLoyaltyCreditPayment(String agentCode, BigDecimal amount, String loyaltyAccountNo, String paymentCarrier,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId) {
		PaymentReferenceTO paymentReferenceTO = new PaymentReferenceTO();
		paymentReferenceTO.setPaymentRefType(PAYMENT_REF_TYPE.LOYALTY);
		paymentReferenceTO.setPaymentRef(loyaltyAccountNo);

		addAgentCreditPaymentWithPaymentRef(agentCode, amount, paymentReferenceTO, paymentCarrier, payCurrencyDTO,
				lccUniqueTnxId, paymentTnxId);
	}

	/**
	 * Add Agent Credit Payment
	 * 
	 * @param agentCode
	 * @param amount
	 */
	@Override
	public void addAgentCreditPayment(String agentCode, BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, Integer paymentTnxId) {
		addAgentCreditPaymentWithPaymentRef(agentCode, amount, null, paymentCarrier, payCurrencyDTO, lccUniqueTnxId, paymentTnxId);
	}

	/* 
	 * 
	 */
	@Override
	public void addAgentCreditPayment(String agentCode, BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, String paymentMethod, Integer paymentTnxId) {
		PaymentReferenceTO payReferenceTO = getPayReferenceTO(null, null, paymentMethod);
		addAgentCreditPaymentWithPaymentRef(agentCode, amount, payReferenceTO, paymentCarrier, payCurrencyDTO, lccUniqueTnxId,
				paymentTnxId);
	}

	/**
	 * Add Agent Credit Payment with reference
	 * 
	 * @param agentCode
	 * @param amount
	 * @param paymentRef
	 */

	@Override
	public void addAgentCreditPayment(String agentCode, BigDecimal amount, String paymentRef, Integer payMode,
			String paymentCarrier, PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId) {
		PaymentReferenceTO payReferenceTO = getPayReferenceTO(paymentRef, payMode);
		addAgentCreditPaymentWithPaymentRef(agentCode, amount, payReferenceTO, paymentCarrier, payCurrencyDTO, lccUniqueTnxId,
				paymentTnxId);
	}

	/*
	 * 
	 */
	@Override
	public void addAgentCreditPayment(String agentCode, BigDecimal amount, String paymentRef, Integer payMode,
			String paymentCarrier, PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, String paymentMethod,
			Integer paymentTnxId) {
		PaymentReferenceTO payReferenceTO = getPayReferenceTO(paymentRef, payMode, paymentMethod);
		addAgentCreditPaymentWithPaymentRef(agentCode, amount, payReferenceTO, paymentCarrier, payCurrencyDTO, lccUniqueTnxId,
				paymentTnxId);
	}

	/**
	 * Add Cash Payment
	 * 
	 * @param amount
	 */
	@Override
	public void addCashPayment(BigDecimal amount, String paymentRef, Integer payMode, String paymentCarrier,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId) {
		PaymentReferenceTO payReferanceTO = getPayReferenceTO(paymentRef, payMode);
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, getTotalExternalChargesAmount());

		CashPaymentInfo cashPaymentInfo = new CashPaymentInfo();
		cashPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);

		cashPaymentInfo.setPayCarrier((paymentCarrier == null ? DEFALT_PAYMENT_CARRIER : paymentCarrier));
		cashPaymentInfo.setLccUniqueId(lccUniqueTnxId);

		cashPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		cashPaymentInfo.setPaymentReferanceTO(payReferanceTO);
		cashPaymentInfo.setPaymentTnxId(paymentTnxId);
		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(cashPaymentInfo);
	}

	/**
	 * 
	 * TODO : // Merge with addCashPayment(BigDecimal amount, String paymentRef, Integer payMode)
	 */
	@Override
	public void addCashPayment(BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId,
			Integer paymentTnxId) {

		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, getTotalExternalChargesAmount());

		CashPaymentInfo cashPaymentInfo = new CashPaymentInfo();
		cashPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);

		cashPaymentInfo.setPayCarrier((paymentCarrier == null ? DEFALT_PAYMENT_CARRIER : paymentCarrier));
		cashPaymentInfo.setLccUniqueId(lccUniqueTnxId);

		cashPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		cashPaymentInfo.setPaymentTnxId(paymentTnxId);

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(cashPaymentInfo);
	}

	/**
	 * Set Credit Payments Initially this method was used only to trac credit payments. They are kind of dummy payments
	 * because we just records the amount and actual Credit BF record are captured in backend But now we are using this
	 * method for these actual BF methods as otehr carrier credit payments will call this one. So we need expiredate,
	 * resetExistingPaxCredit - whether to reset the pax list or not
	 * 
	 * @param balance
	 * @param debitPaxId
	 * @param paymentCarrier
	 *            carrier code the pax credit is belongs to
	 * @param paxCreditId
	 *            - this should be set only after the CF has taken place. because this should be an existing pax credit
	 *            record primary key
	 */
	@Override
	public void addCreditPayment(BigDecimal balance, String debitPaxId, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, Integer paxSequence, Date expiryDate, String pnr, String paxCreditId,
			String creditUtilizingPnr, Integer paymentTnxId) {
		PaxCreditInfo paxCreditInfo = new PaxCreditInfo();
		PaxCreditDTO paxCreditDTO = new PaxCreditDTO();
		paxCreditDTO.setBalance(balance);
		paxCreditDTO.setDebitPaxId(debitPaxId);
		paxCreditDTO.setPayCarrier((paymentCarrier == null ? DEFALT_PAYMENT_CARRIER : paymentCarrier));
		paxCreditDTO.setLccUniqueId(lccUniqueTnxId);
		paxCreditDTO.setPaxSequence(paxSequence);
		paxCreditDTO.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		paxCreditDTO.setDateOfExpiry(expiryDate);
		paxCreditDTO.setPnr(pnr);
		paxCreditDTO.setCreditUtilizingPnr(creditUtilizingPnr);
		paxCreditDTO.setPaxCreditId(paxCreditId);
		paxCreditInfo.setPaxCredit(paxCreditDTO);
		paxCreditInfo.setTotalAmount(balance);
		paxCreditInfo.setPayCarrier(paymentCarrier);
		paxCreditInfo.setPaymentTnxId(paymentTnxId);

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, balance);
		totalPaxCredit = AccelAeroCalculator.add(totalPaxCredit, balance);

		this.payments.add(paxCreditInfo);
	}

	@Override
	public void addLMSPayment(String loyaltyMemberAccountId, String[] rewardIDs, BigDecimal amount, String paymentCarrier,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId) {

		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, getTotalExternalChargesAmount());

		LMSPaymentInfo lmsPaymentInfo = new LMSPaymentInfo();
		lmsPaymentInfo.setLoyaltyMemberAccountId(loyaltyMemberAccountId);
		lmsPaymentInfo.setRewardIDs(rewardIDs);
		lmsPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);
		lmsPaymentInfo.setPayCarrier((paymentCarrier == null ? DEFALT_PAYMENT_CARRIER : paymentCarrier));
		lmsPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		lmsPaymentInfo.setLccUniqueId(lccUniqueTnxId);
		lmsPaymentInfo.setPaymentTnxId(paymentTnxId);

		this.trackPaymentExistOrNot();

		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(lmsPaymentInfo);

	}


	@Override
	public void addVoucherPayment(VoucherDTO voucherDTO, BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, Integer paymentTnxId) {
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, getTotalExternalChargesAmount());
		VoucherPaymentInfo voucherPaymentInfo = new VoucherPaymentInfo();
		voucherPaymentInfo.setVoucherDTO(voucherDTO);
		voucherPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);
		voucherPaymentInfo.setPayCarrier((paymentCarrier == null ? DEFALT_PAYMENT_CARRIER : paymentCarrier));
		PayCurrencyDTO voucherPayCurrency = (PayCurrencyDTO) payCurrencyDTO.clone();
		voucherPayCurrency.setTotalPayCurrencyAmount(AccelAeroCalculator.multiplyDefaultScale(totalPayAmountWithServiceCharges,
				payCurrencyDTO.getPayCurrMultiplyingExchangeRate()));
		voucherPaymentInfo.setPayCurrencyDTO(voucherPayCurrency);
		voucherPaymentInfo.setLccUniqueId(lccUniqueTnxId);
		voucherPaymentInfo.setPaymentTnxId(paymentTnxId);

		this.trackPaymentExistOrNot();

		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(voucherPaymentInfo);

	}
	
	
	

	public void addOfflinePayment(BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, Integer paymentTnxId) {
		OfflinePaymentInfo offlinePaymentInfo = new OfflinePaymentInfo();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, getTotalExternalChargesAmount());
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);
		offlinePaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);
		offlinePaymentInfo.setPaymentTnxId(paymentTnxId);
		offlinePaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		offlinePaymentInfo.setPayCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		offlinePaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		payCurrencyDTO.setTotalPayCurrencyAmount(AccelAeroCalculator.multiplyDefaultScale(totalPayAmountWithServiceCharges,
				payCurrencyDTO.getPayCurrMultiplyingExchangeRate()));
		offlinePaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		this.payments.add((PaymentInfo) offlinePaymentInfo);
	}

	/**
	 * Track Payment Exist or not
	 * 
	 */
	private void trackPaymentExistOrNot() {
		if (!isPaymentExist()) {
			setPaymentExist(true);
		}
	}

	/**
	 * Return Payments
	 * 
	 * @return
	 */
	public Collection<PaymentInfo> getPayments() {
		return this.payments;
	}

	/**
	 * @return Returns the totalPayAmount.
	 */
	public BigDecimal getTotalPayAmount() {
		return totalPayAmount;
	}

	/**
	 * Added this method to set the amount when we remove a payment from assembler
	 * 
	 * @param totalPayAmount
	 */
	public void setTotalPayAmount(BigDecimal totalPayAmount) {
		this.totalPayAmount = totalPayAmount;
	}

	/**
	 * @return Returns the totalChargeAmount.
	 */
	public BigDecimal getTotalChargeAmount() {
		return totalChargeAmount;
	}

	/**
	 * @param totalChargeAmount
	 *            The totalChargeAmount to set.
	 */
	public void setTotalChargeAmount(BigDecimal totalChargeAmount) {
		this.totalChargeAmount = totalChargeAmount;
	}

	/**
	 * @return Returns the paymentExist.
	 */
	public boolean isPaymentExist() {
		return paymentExist;
	}

	/**
	 * @param paymentExist
	 *            The paymentExist to set.
	 */
	private void setPaymentExist(boolean paymentExist) {
		this.paymentExist = paymentExist;
	}

	/**
	 * @return Returns the perPaxExternalCharges.
	 */
	public Collection<ExternalChgDTO> getPerPaxExternalCharges(EXTERNAL_CHARGES externalCharges) {
		Collection<ExternalChgDTO> tmpExternalChgDTO = new ArrayList<ExternalChgDTO>();

		for (ExternalChgDTO externalChgDTO : perPaxExternalCharges) {

			if (externalChgDTO != null && externalChgDTO.getExternalChargesEnum() == externalCharges) {
				tmpExternalChgDTO.add(externalChgDTO);
			}
		}

		return tmpExternalChgDTO;
	}

	/**
	 * Returns per pax cc and handling external charges
	 * 
	 * @return
	 */
	public Collection<ExternalChgDTO> getPerPaxGenericExternalCharges() {
		Collection<ExternalChgDTO> allExtenralChgs = new ArrayList<ExternalChgDTO>();
		allExtenralChgs.addAll(getPerPaxExternalCharges(EXTERNAL_CHARGES.CREDIT_CARD));
		allExtenralChgs.addAll(getPerPaxExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE));
		allExtenralChgs.addAll(getPerPaxExternalCharges(EXTERNAL_CHARGES.INSURANCE));
		allExtenralChgs.addAll(getPerPaxExternalCharges(EXTERNAL_CHARGES.PROMOTION_REWARD));
		allExtenralChgs.addAll(getPerPaxExternalCharges(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE));
		allExtenralChgs.addAll(getPerPaxExternalCharges(EXTERNAL_CHARGES.BSP_FEE));
		return allExtenralChgs;
	}

	private PayCurrencyDTO getNullSafePayCurrencyDTO(PayCurrencyDTO dto) {
		if (dto == null) {
			return new PayCurrencyDTO(AppSysParamsUtil.getBaseCurrency(), BigDecimal.ONE);
		}
		return (PayCurrencyDTO) dto.clone();
	}

	/**
	 * Fill Payment Reference Data
	 * 
	 * @param paymentRef
	 * @param payMode
	 * @return
	 */
	private PaymentReferenceTO getPayReferenceTO(String paymentRef, Integer payMode) {
		paymentRef = BeanUtils.nullHandler(paymentRef);

		if (paymentRef != null || payMode != null) {
			PaymentReferenceTO payRefTo = new PaymentReferenceTO();
			payRefTo.setActualPaymentMode(payMode);
			payRefTo.setPaymentRef(paymentRef);
			return payRefTo;
		}

		return null;
	}

	/**
	 * @param paymentRef
	 * @param payMode
	 * @return
	 */
	private PaymentReferenceTO getPayReferenceTO(String paymentRef, Integer payMode, String paymentRefType) {
		paymentRef = BeanUtils.nullHandler(paymentRef);

		if (paymentRef != null || payMode != null || paymentRefType != null) {
			PaymentReferenceTO payRefTo = new PaymentReferenceTO();
			payRefTo.setActualPaymentMode(payMode);
			payRefTo.setPaymentRef(paymentRef);
			payRefTo.setPaymentRefType(PaymentReferenceTO.getPaymentRefType(paymentRefType));
			return payRefTo;
		}

		return null;
	}

	/**
	 * Get the toal pax credit
	 * 
	 * @return totalPaxCredit
	 */
	@Override
	public BigDecimal getTotalPaxCredit() {
		return this.totalPaxCredit;
	}

	public void removePerPaxExternalCharge(EXTERNAL_CHARGES extCharge) {
		ExternalChgDTO existingExtObj = null;
		for (ExternalChgDTO externalChgDTO : perPaxExternalCharges) {
			if (externalChgDTO.getExternalChargesEnum() == extCharge) {
				existingExtObj = externalChgDTO;
				break;
			}
		}

		if (existingExtObj != null) {
			perPaxExternalCharges.remove(existingExtObj);
		}
	}

	public Date getZuluOhdReleaseTime() {
		return zuluOhdReleaseTime;
	}

	public void setZuluOhdReleaseTime(Date zuluOhdReleaseTime) {
		this.zuluOhdReleaseTime = zuluOhdReleaseTime;
	}
}
