/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.utils;

/**
 * @author Lasantha Pambagoda
 */
public abstract class ResponseCodes {

	public static final String TRANSFER_RESERVATION_SEGMENT_SUCCESSFULL = "airreservation.logic.bl.reservation.segment.transfer.succss";

	public static final String TRANSFERED_RESERVATION_SEGMENTS = "airreservation.logic.bl.reservation.segment.transfer.segment";

	public static final String ROLLFORWARD_TRANSFERS_SUCCESSFULL = "airreservation.logic.bl.reservation.transfer.rollforward.succss";

	public static final String TRANSFER_SEATS_SUCCESSFULL = "airreservation.logic.bl.reservation.seat.transfer.succss";

	public static final String TRANSFER_SEATS_UNSUCCESSFULL = "airreservation.logic.bl.reservation.seat.transfer.unsuccss";

	public static final String EFFECTED_ERROR_FLIGHT_SEGMENT_INFORMATION = "airreservation.logic.bl.reservation.alertreservations.errorflightsegments";

	public static final String STANDARD_PAYMENT_BROKER_ERROR = "airreservations.arg.cc.error";
	
	public static final String PAYMENT_BROKER_CONFIRMATION_WAITING = "airreservations.arg.cc.waiting";

	public static final String SEAT_BLOCK_ERROR = "seat.block.error";

	public static final String DUPLICATE_NAMES_IN_FLIGHT_SEGMENT = "airreservations.arg.duplicateNamesExist";

	public static final String TRANSFER_SEAT_NUMBER = "airreservation.logic.bl.reservation.seat.transfer.number";

	public static final String FLIGHT_REPROTECT_CANCELED_SEATS = "airreservation.logic.bl.reservation.seat.transfer.canceled.seats";

	public static final String FLIGHT_REPROTECT_CANCELED_MEALS = "airreservation.logic.bl.reservation.seat.transfer.canceled.meals";
}
