/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Panchatcharam.S
 *
 */
public class PassengerInfo implements Serializable {

	private static final long serialVersionUID = -2311413874330138659L;
	private Collection<PassengerBookingInfo> bookingInfo;

	public Collection<PassengerBookingInfo> getBookingInfo() {
		return bookingInfo;
	}

	public void setBookingInfo(Collection<PassengerBookingInfo> bookingInfo) {
		this.bookingInfo = bookingInfo;
	}

}
