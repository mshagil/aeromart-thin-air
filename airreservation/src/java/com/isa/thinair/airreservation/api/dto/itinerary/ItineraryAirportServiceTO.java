package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */

public class ItineraryAirportServiceTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String description;
	private String ssrCode;
	private String airportCode;
	private String ssrText;

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the ssrCode
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            the ssrCode to set
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getSsrText() {
		return ssrText;
	}

	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

}
