/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pfs;

/**
 * @author Rikaz
 *
 */
public class CSPaxBookingCount {
	private String segmentCode;
	private String bookingClass;
	private int gdsId;
	private int paxCount;

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	public int getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(int paxCount) {
		this.paxCount = paxCount;
	}

}
