/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.AirportCheckInTime;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.SimplifiedChargeDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedFlightSegmentDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedPaxDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.FLEXI_RULE_FLEXIBILITY_TYPE;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxCnxModPenChargeForServiceTaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.PaxChargesTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.assembler.OndConvertAssembler;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.BasicChargeDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.ChargeRateDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.InsuranceSegExtChgDTO;
import com.isa.thinair.airreservation.api.dto.OndGroupDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferAssembler;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.AirportTransferAdaptor;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.segment.FlownAssitUnit;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateJourneyType;
import com.isa.thinair.commons.api.dto.ChargeRateBasis.ChargeBasisEnum;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.AccelAeroClients;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * Holds Reservation business object specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationApiUtils {
	/** Holds the global configurations */
	private static GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();

	private static Log log = LogFactory.getLog(ReservationApiUtils.class);
	
	private final static String YES = "Y";
	private final static String NO = "N";
	
	/*
	 * To identify the reservation type based on segments
	 */
	private final static String RES_ONE_WAY = "RES_ONE_WAY";
	private final static String RES_RETURN = "RES_RETURN";
	private final static String RES_COMPLEX = "RES_COMPLEX";

	private ReservationApiUtils() {

	}

	/**
	 * Find whether infant exist
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isInfantType(ReservationPax reservationPax) {
		return ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType());
	}

	/**
	 * Find whether child exist or not
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isChildType(ReservationPax reservationPax) {
		return ReservationInternalConstants.PassengerType.CHILD.equals(reservationPax.getPaxType());
	}

	/**
	 * Find whether adult exist or not
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isAdultType(ReservationPax reservationPax) {
		return ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType());
	}

	/**
	 * Find whether parent exist
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isParentAfterSave(ReservationPax reservationPax) {
		return isAdultType(reservationPax) && reservationPax.getAccompaniedPaxId() != null;
	}

	/**
	 * Find whether parent exist before save
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isParentBeforeSave(ReservationPax reservationPax) {
		return isAdultType(reservationPax) && reservationPax.getIndexId() != null;
	}

	/**
	 * Find whether adult exist
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isSingleAfterSave(ReservationPax reservationPax) {
		return isAdultType(reservationPax) && reservationPax.getAccompaniedPaxId() == null;
	}

	/**
	 * Find whether adult exist before save
	 * 
	 * @param reservationPax
	 * @return
	 */
	private static boolean isSingleBeforeSave(ReservationPax reservationPax) {
		return isAdultType(reservationPax) && reservationPax.getIndexId() == null;
	}

	// START

	/**
	 * Find whether infant exist
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isInfantType(LCCClientReservationPax reservationPax) {
		return ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType());
	}

	/**
	 * Find whether child exist or not
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isChildType(LCCClientReservationPax reservationPax) {
		return ReservationInternalConstants.PassengerType.CHILD.equals(reservationPax.getPaxType());
	}

	/**
	 * Find whether adult exist or not
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isAdultType(LCCClientReservationPax reservationPax) {
		return ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType());
	}

	/**
	 * Find whether parent exist
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isParentAfterSave(LCCClientReservationPax reservationPax) {
		return isAdultType(reservationPax) && reservationPax.getParent() != null;
	}

	/**
	 * Find whether parent exist before save
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isParentBeforeSave(LCCClientReservationPax reservationPax) {
		return isAdultType(reservationPax) && reservationPax.getInfants() != null;
	}

	/**
	 * Find whether adult exist
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static boolean isSingleAfterSave(LCCClientReservationPax reservationPax) {
		return isAdultType(reservationPax) && reservationPax.getParent() == null;
	}

	/**
	 * Find whether adult exist before save
	 * 
	 * @param reservationPax
	 * @return
	 */
	private static boolean isSingleBeforeSave(LCCClientReservationPax reservationPax) {
		return isAdultType(reservationPax) && reservationPax.getInfants() == null;
	}

	/**
	 * Return the decimals
	 * 
	 * @return
	 * @throws ModuleException
	 */
	protected static String getDecimalPoints() throws ModuleException {
		int intDecimalPoints = AppSysParamsUtil.getNumberOfDecimalPoints();
		String strDecimalFormat = "0";
		for (int i = 1; i <= intDecimalPoints; i++) {
			if (i == 1) {
				strDecimalFormat += ".0";
			} else {
				strDecimalFormat += "0";
			}
		}

		return strDecimalFormat;
	}

	/**
	 * Return the company address information
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getCompanyAddressInformation(String carrierCode) throws ModuleException {
		String companyAddress = "";
		if (AccelAeroClients.AIR_ARABIA.equals(globalConfig.getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))
				|| AccelAeroClients.AIR_ARABIA_GROUP
						.equals(globalConfig.getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))) {
			companyAddress += " <br>" + globalConfig.getBizParam(SystemParamKeys.COMP_TEL, carrierCode) + " <br>"
					+ globalConfig.getBizParam(SystemParamKeys.COMP_EMAIL, carrierCode);
		} else {
			companyAddress += " <br>" + globalConfig.getBizParam(SystemParamKeys.COMP_ADD1, carrierCode) + " <br>"
					+ globalConfig.getBizParam(SystemParamKeys.COMP_ADD2, carrierCode) + " <br>"
					+ globalConfig.getBizParam(SystemParamKeys.COMP_TEL, carrierCode) + " <br>"
					+ globalConfig.getBizParam(SystemParamKeys.COMP_EMAIL, carrierCode);
		}
		return companyAddress;
	}

	/**
	 * Returns the ond code
	 * 
	 * @param ondCodes
	 * @return
	 */
	public static String getOndCode(Collection<String> ondCodes) {
		Iterator<String> itOndCodes = ondCodes.iterator();
		String accumulateOndCode = "";
		String ondCode;

		while (itOndCodes.hasNext()) {
			ondCode = itOndCodes.next();
			accumulateOndCode = ondComposer(accumulateOndCode, ondCode);
		}

		return accumulateOndCode;
	}

	/**
	 * Returns the inverse of the ond code e.g CMB/SHJ/MCT Returns MCT/SHJ/CMB
	 * 
	 * @param ondCode
	 * @return
	 */
	public static String getInverseOndCode(String ondCode) {
		StringTokenizer st = new StringTokenizer(ondCode, "/", false);
		LinkedList<String> stack = new LinkedList<String>();
		StringBuffer inverseOndCode = new StringBuffer();
		String segment;

		while (st.hasMoreElements()) {
			stack.addFirst((String) st.nextElement());
		}

		for (String string : stack) {
			segment = string;

			if (inverseOndCode.length() == 0) {
				inverseOndCode.append(segment);
			} else {
				inverseOndCode.append("/" + segment);
			}
		}

		return inverseOndCode.toString();
	}

	/**
	 * Return the ond code
	 * 
	 * @param ReservationPaxFare
	 * @return
	 * @throws ModuleException
	 */
	public static String getOndCode(ReservationPaxFare reservationPaxFare) throws ModuleException {
		if (reservationPaxFare.getReservationPax().getReservation().getSegmentsView() == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}

		Collection<Integer> colPnrSegIds = new ArrayList<Integer>();
		Collection<String> colPnrSegmentCodes = new ArrayList<String>();
		Map<Integer, String> mapPnrSegmentCodes = new TreeMap<Integer, String>();

		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = itReservationPaxFareSegment.next();
			colPnrSegIds.add(reservationPaxFareSegment.getSegment().getPnrSegId());
		}

		Reservation reservation = reservationPaxFare.getReservationPax().getReservation();
		Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservation.getSegmentsView().iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		while (itReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itReservationSegmentDTO.next();

			if (colPnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
				mapPnrSegmentCodes.put(reservationSegmentDTO.getSegmentSeq(), reservationSegmentDTO.getSegmentCode());
			}
		}

		colPnrSegmentCodes.addAll(mapPnrSegmentCodes.values());

		return ReservationApiUtils.getOndCode(colPnrSegmentCodes);
	}

	/**
	 * @param reservationPaxFare
	 * @return
	 * @throws ModuleException
	 */
	public static Date getFirstDepartureDate(ReservationPaxFare reservationPaxFare) throws ModuleException {
		if (reservationPaxFare.getReservationPax().getReservation().getSegmentsView() == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}

		ReservationPaxFareSegment reservationPaxFareSegment;
		ReservationSegmentDTO reservationSegmentDTO;
		Iterator<ReservationSegmentDTO> itReservationSegmentDTO;
		Reservation reservation = reservationPaxFare.getReservationPax().getReservation();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

		List<Date> departureDates = new ArrayList<Date>();

		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = itReservationPaxFareSegment.next();
			itReservationSegmentDTO = reservation.getSegmentsView().iterator();
			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = itReservationSegmentDTO.next();
				if (reservationPaxFareSegment.getPnrSegId().equals(reservationSegmentDTO.getPnrSegId())) {
					departureDates.add(reservationSegmentDTO.getDepartureDate());
				}
			}
		}
		Collections.sort(departureDates);

		return departureDates.isEmpty() ? null : departureDates.get(0);
	}

	public static ReservationSegmentDTO getFirstConfirmedSegment(Collection<ReservationSegmentDTO> reservationSegmentDTOs)
			throws ModuleException {
		if (reservationSegmentDTOs == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}
		ReservationSegmentDTO reservationSegmentDTO = null;
		List<ReservationSegmentDTO> lstReservationSegmentDTOs = (List<ReservationSegmentDTO>) reservationSegmentDTOs;
		Collections.sort(lstReservationSegmentDTOs);

		for (ReservationSegmentDTO sortedReservationSegmentDTO : lstReservationSegmentDTOs) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(sortedReservationSegmentDTO.getStatus())) {
				reservationSegmentDTO = sortedReservationSegmentDTO;
				break;
			}
		}

		return reservationSegmentDTO;
	}

	public static List<ReservationSegmentDTO> getConfirmedSegments(Collection<ReservationSegmentDTO> reservationSegmentDTOs) {
		List<ReservationSegmentDTO> lstReservationSegmentDTOs = new ArrayList<>();

		if (reservationSegmentDTOs != null) {
			for (ReservationSegmentDTO reservationSegmentDTO : reservationSegmentDTOs) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())) {
					lstReservationSegmentDTOs.add(reservationSegmentDTO);
				}
			}
			Collections.sort(lstReservationSegmentDTOs);
		}
		return lstReservationSegmentDTOs;
	}
	
	public static Set<ReservationSegment> getConfirmedSegments(Set<ReservationSegment> reservationSegments) {
		Set<ReservationSegment> lstReservationSegments = new HashSet<>();

		if (reservationSegments != null) {
			for (ReservationSegment reservationSegment : reservationSegments) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())) {
					lstReservationSegments.add(reservationSegment);
				}
			}
		}
		return lstReservationSegments;
	}

	/**
	 * Compose the ond code
	 * 
	 * @param accumulateOndCode
	 * @param ondCode
	 * @return
	 */
	private static String ondComposer(String accumulateOndCode, String ondCode) {
		StringBuffer finalOndCode = new StringBuffer();

		if (accumulateOndCode.length() != 0) {
			if (accumulateOndCode.substring(accumulateOndCode.length() - 3).equals(ondCode.substring(0, 3))) {
				finalOndCode.append(accumulateOndCode).append("/").append(ondCode.substring(4));
			} else {
				finalOndCode.append(accumulateOndCode).append("/").append(ondCode);
			}
		} else {
			finalOndCode.append(ondCode);
		}

		return finalOndCode.toString();
	}

	/**
	 * Get the total amount paid by passenger.
	 * 
	 * @param ondFareDTOS
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getPayedAmountFromOndFareDto(Collection<OndFareDTO> colOndFareDTO, String paxType)
			throws ModuleException {
		// Iterate through the ond fare dtos.
		Iterator<OndFareDTO> itColOndFareDTO = colOndFareDTO.iterator();
		BigDecimal totalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		OndFareDTO ondFareDTO;

		while (itColOndFareDTO.hasNext()) {
			ondFareDTO = itColOndFareDTO.next();

			if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
				if (ReservationApiUtils.isFareExist(ondFareDTO.getAdultFare())) {
					totalPayment = AccelAeroCalculator.add(totalPayment,
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare()),
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges()[0]));
				} else {
					totalPayment = AccelAeroCalculator.add(totalPayment,
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges()[0]));
				}
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
				if (ReservationApiUtils.isFareExist(ondFareDTO.getChildFare())) {
					totalPayment = AccelAeroCalculator.add(totalPayment,
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare()),
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges()[2]));
				} else {
					totalPayment = AccelAeroCalculator.add(totalPayment,
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges()[2]));
				}
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
				if (ReservationApiUtils.isFareExist(ondFareDTO.getInfantFare())) {
					totalPayment = AccelAeroCalculator.add(totalPayment,
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare()),
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges()[1]));
				} else {
					totalPayment = AccelAeroCalculator.add(totalPayment,
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges()[1]));
				}
			} else {
				throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
			}
		}

		return totalPayment;
	}

	public static Date getCheckInDate(Date departureDate, String airportCode, String flightNumber) throws ModuleException {
		if (departureDate != null) {
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(departureDate);

			int checkInDiff = AppSysParamsUtil.getTotalMinutes(AppSysParamsUtil.getCheckInTimeDifference());

			AirportBD airportBD = ReservationModuleUtils.getAirportBD();

			AirportCheckInTime checkInTime = airportBD.getAirportCheckInTime(airportCode, flightNumber);

			if (checkInTime != null) {
				if (checkInTime.getCheckInGap() > 0) {
					checkInDiff = checkInTime.getCheckInGap();
				}
			}

			calendar.add(Calendar.MINUTE, -checkInDiff);
			return calendar.getTime();
		} else {
			return departureDate;
		}
	}

	public static Date getCheckInClosingDate(Date departureDate, String airportCode, String flightNumber) throws ModuleException {
		if (departureDate != null) {
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(departureDate);

			int checkInClosingDiff = AppSysParamsUtil.getTotalMinutes(AppSysParamsUtil.getCheckInClosingTime());
			// FIXME - should be moved to core utils if this is necessary
			// 4May11 KD
			AirportBD airportBD = ReservationModuleUtils.getAirportBD();

			AirportCheckInTime checkInTime = airportBD.getAirportCheckInTime(airportCode, flightNumber);

			if (checkInClosingDiff > 0 && checkInTime != null) {
				if (checkInTime.getCheckInClosingTime() > 0) {
					checkInClosingDiff = checkInTime.getCheckInClosingTime();
				}
			}

			calendar.add(Calendar.MINUTE, -checkInClosingDiff);
			return calendar.getTime();
		} else {
			return departureDate;
		}
	}

	/**
	 * Return charge type amounts
	 * 
	 * Holds the following information in the array <br />
	 * 
	 * 0 --> Fare <br />
	 * 1 --> Tax Charge <br />
	 * 2 --> Surcharge Charge <br />
	 * 3 --> Cancellation Charge <br />
	 * 4 --> Modification Charge <br />
	 * 5 --> Adjustment Charge Charge <br />
	 * 6 --> Penalty <br />
	 */
	public static BigDecimal[] getChargeTypeAmounts() {
		return new BigDecimal[] { AccelAeroCalculator.getDefaultBigDecimalZero(), AccelAeroCalculator.getDefaultBigDecimalZero(),
				AccelAeroCalculator.getDefaultBigDecimalZero(), AccelAeroCalculator.getDefaultBigDecimalZero(),
				AccelAeroCalculator.getDefaultBigDecimalZero(), AccelAeroCalculator.getDefaultBigDecimalZero(),
				AccelAeroCalculator.getDefaultBigDecimalZero(), AccelAeroCalculator.getDefaultBigDecimalZero() };
	}

	/**
	 * Return converted date
	 * 
	 * @param zuluTime
	 * @param localTime
	 * @param dateFormat
	 * @param displayConversion
	 * @return
	 */
	public static String getConvertedDate(Date zuluTime, Date localTime, String dateFormat, boolean displayConversion) {
		if (localTime != null) {
			if (displayConversion) {
				return BeanUtils.parseDateFormat(localTime, dateFormat) + ReservationTemplateUtils.LOCAL_TIME;
			} else {
				return BeanUtils.parseDateFormat(localTime, dateFormat);
			}
		} else {
			if (displayConversion) {
				return BeanUtils.parseDateFormat(zuluTime, dateFormat) + ReservationTemplateUtils.ZULU_TIME;
			} else {
				return BeanUtils.parseDateFormat(zuluTime, dateFormat);
			}
		}
	}

	/**
	 * Check Passenger confirmation
	 * 
	 * @param reservationPax
	 * @return
	 * @throws ModuleException
	 */
	public static boolean checkPassengerConfirmationForAFreshBooking(ReservationPax reservationPax) throws ModuleException {
		BigDecimal totalPayAmount = reservationPax.getTotalPaidAmount();
		BigDecimal totalChargeAmount = reservationPax.getTotalChargeAmount();

		double dblTotalPayAmount = (totalPayAmount != null) ? Math.ceil(totalPayAmount.doubleValue()) : 0;
		double dblTotalChgAmount = Math.ceil((totalChargeAmount.doubleValue()));

		// Introduced [dblTotalPayAmount -1] because we may have a scenario that other carrier payments might lead
		// do decimal point deferences. Since we are getting ceilling value it should be always 1
		// Payment amount should not be less than at any time
		if (dblTotalPayAmount - 1 > dblTotalChgAmount && !reservationPax.hasVoucherPayment()) {
			log.error("Booking creation failure. Payment is more than charges. " + "[ Payments = " + totalPayAmount.doubleValue()
					+ ", Charges = " + totalChargeAmount.doubleValue() + ", Rounded Up Payments = " + dblTotalPayAmount
					+ ", Rounded Up Charges = " + dblTotalChgAmount + "]");
			throw new ModuleException("airreservations.arg.paymentAlreadyCompleted");
		}

		if (totalPayAmount != null && dblTotalPayAmount >= dblTotalChgAmount) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return if the passenger is confirmed or not for a fresh booking
	 * 
	 * @param reservationPax
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isPassengerConfirmedForAFreshBooking(ReservationPax reservationPax) throws ModuleException {
		// For infant
		// This is inorder to support infant zero charges. Because if the infant charge is zero he/she
		// can not become confirm since if he/she parent is not confirmed
		if (isInfantType(reservationPax)) {
			return checkPassengerConfirmationForAFreshBooking(reservationPax.getParent());
		}
		// For Adult or Parent
		else {
			return checkPassengerConfirmationForAFreshBooking(reservationPax);
		}
	}

	/**
	 * Return if the passenger is confirm or not for a modify booking
	 * 
	 * @param reservationPax
	 * @param passengerPayment
	 * @param acceptMorePaymentsThanItsCharges
	 * @param isPaymentForAccuirCredit
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isPassengerConfirmedForAModifyBooking(ReservationPax reservationPax,
			Map<Integer, IPayment> passengerPayment, boolean acceptMorePaymentsThanItsCharges, boolean isPaymentForAccuirCredit)
			throws ModuleException {
		// For infant
		// This is in order to support infant zero charges. Because if the infant charge is zero he/she
		// can not become confirm since if he/she parent is not confirmed
		if (isInfantType(reservationPax)) {
			return checkPassengerConfirmationForAModifyBooking(reservationPax.getParent(), passengerPayment,
					acceptMorePaymentsThanItsCharges, isPaymentForAccuirCredit);
		}
		// For Adult or Parent
		else {
			return checkPassengerConfirmationForAModifyBooking(reservationPax, passengerPayment, acceptMorePaymentsThanItsCharges,
					isPaymentForAccuirCredit);
		}
	}

	/**
	 * Checks the passenger status as confirm
	 * 
	 * @param reservationPax
	 * @param pnrPaxIdAndPayments
	 * @param acceptMorePaymentsThanItsCharges
	 * @param isPaymentForAcquirCredit
	 * @return
	 * @throws ModuleException
	 */
	public static boolean checkPassengerConfirmationForAModifyBooking(ReservationPax reservationPax,
			Map<Integer, IPayment> pnrPaxIdAndPayments, boolean acceptMorePaymentsThanItsCharges,
			boolean isPaymentForAcquirCredit) throws ModuleException {
		PaymentAssembler paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());
		BigDecimal totalChargeAmount = paymentAssembler != null
				? paymentAssembler.getTotalChargeAmount()
				: AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal dueAmount = AccelAeroCalculator.add(reservationPax.getTotalAvailableBalance(), totalChargeAmount);

		double dblDueAmount = Math.ceil(dueAmount.doubleValue());
		double dblPayAmount = (paymentAssembler != null)
				? Math.ceil(paymentAssembler.getTotalPayAmount().doubleValue())
				: AccelAeroCalculator.getDefaultBigDecimalZero().doubleValue();

		if (paymentAssembler != null && dblPayAmount != 0 && (dblPayAmount - dblDueAmount) > 1.00
				&& !acceptMorePaymentsThanItsCharges) {
			log.error("PAY_AMOUNT_MISMATCH : pnr" + reservationPax.getReservation().getPnr() + ", dueAmt:" + dblDueAmount
					+ ", payAmt :" + dblPayAmount + ", (total_charges: " + totalChargeAmount + ", pax_balance:"
					+ reservationPax.getTotalAvailableBalance() + "), pax_id : " + reservationPax.getPnrPaxId());
			throw new ModuleException("airreservations.arg.paymentAlreadyCompleted");
		}

		/*
		 * we don't want to change the pax status of the reservation for payments done for acquire pax credit.If that is
		 * the case status will remain unchanged. See AARESAA-3932 Issue 37
		 */
		if (dblDueAmount <= dblPayAmount && !isPaymentForAcquirCredit) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return booking classes These booking classes will be ordered for pnr segment
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, BookingClass> getBookingClasses(Reservation reservation) throws ModuleException {

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		Iterator<ReservationPaxFare> itReservationPaxFare;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;

		Map<Integer, Collection<String>> mapPnrSegIdAndBookingCode = new HashMap<Integer, Collection<String>>();
		Collection<String> bookingCodes;

		// If it an adult or a parent
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			// Parent or adult (Infant's won't have any booking codes)
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

				while (itReservationPaxFare.hasNext()) {
					reservationPaxFare = itReservationPaxFare.next();

					itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

					while (itReservationPaxFareSegment.hasNext()) {
						reservationPaxFareSegment = itReservationPaxFareSegment.next();

						// Keeping the booking code specific to pnr segment
						if (mapPnrSegIdAndBookingCode.containsKey(reservationPaxFareSegment.getPnrSegId())) {
							bookingCodes = mapPnrSegIdAndBookingCode.get(reservationPaxFareSegment.getPnrSegId());
							bookingCodes.add(reservationPaxFareSegment.getBookingCode());
						} else {
							bookingCodes = new HashSet<String>();
							bookingCodes.add(reservationPaxFareSegment.getBookingCode());
							mapPnrSegIdAndBookingCode.put(reservationPaxFareSegment.getPnrSegId(), bookingCodes);
						}
					}
				}
			}
		}

		BookingClassBD bookingClassBD = ReservationModuleUtils.getBookingClassBD();
		Map<Integer, BookingClass> mapPnrSegIdAndBookingClass = new TreeMap<Integer, BookingClass>();

		// If any records exist (Means Adult or Parent Exist)
		if (mapPnrSegIdAndBookingCode.size() > 0) {
			Collection<String> colAllBookingCodes = new HashSet<String>();
			Integer pnrSegId;
			BookingClass bookingClass;

			// Summing all the booking codes and validating
			for (Integer integer : mapPnrSegIdAndBookingCode.keySet()) {
				pnrSegId = integer;
				bookingCodes = mapPnrSegIdAndBookingCode.get(pnrSegId);
				colAllBookingCodes.addAll(bookingCodes);
			}

			Map<String, BookingClass> mapBookingCodeAndBookingClass = bookingClassBD.getBookingClassesMap(colAllBookingCodes);

			// Creating the final pnr segment and booking type mapping
			for (Integer integer : mapPnrSegIdAndBookingCode.keySet()) {
				pnrSegId = integer;
				bookingCodes = mapPnrSegIdAndBookingCode.get(pnrSegId);
				bookingClass = getBookingClass(bookingCodes, mapBookingCodeAndBookingClass);

				mapPnrSegIdAndBookingClass.put(pnrSegId, bookingClass);
			}
			// Only infant exist in the reservation
		} else {
			Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator();
			ReservationSegment reservationSegment;

			while (itReservationSegment.hasNext()) {
				reservationSegment = itReservationSegment.next();
				// If only an infant exist in the reservation infant can not perform any actions at all.
				// So in that case showing it as "" for that we need to send null
				mapPnrSegIdAndBookingClass.put(reservationSegment.getPnrSegId(), null);
			}
		}

		return mapPnrSegIdAndBookingClass;
	}

	/**
	 * Return the booking class
	 * 
	 * @param colBookingCodes
	 * @param mapBookingCodeAndBookingClass
	 * @return
	 * @throws ModuleException
	 */
	private static BookingClass getBookingClass(Collection<String> colBookingCodes,
			Map<String, BookingClass> mapBookingCodeAndBookingClass) throws ModuleException {

		// START Commented the validation
		// as Air Arabia migrated reservations DO NOT adhere to one booking class per segment rule. 20-May-2007 15:52

		/*
		 * Collection colBookingClasses = new HashSet(); String bookingCode; BookingClass bookingClass;
		 * 
		 * for (Iterator itBookingCodes = colBookingCodes.iterator(); itBookingCodes.hasNext();) { bookingCode =
		 * (String) itBookingCodes.next(); bookingClass = (BookingClass) mapBookingCodeAndBookingClass.get(bookingCode);
		 * 
		 * colBookingClasses.add(bookingClass); }
		 * 
		 * // Two booking classes can not exist for a reservation segment if (colBookingClasses.size() == 1) {
		 * bookingClass = (BookingClass) BeanUtils.getFirstElement(colBookingClasses);
		 * 
		 * if (bookingClass == null) { throw new ModuleException("airreservations.arg.InvalidBookingType"); }
		 * 
		 * return bookingClass; } else { throw new ModuleException("airreservations.arg.InvalidBookingType"); }
		 */
		// END 20-May-2007 15:52

		BookingClass bookingClass = mapBookingCodeAndBookingClass.get(colBookingCodes.iterator().next());

		if (bookingClass == null) {
			throw new ModuleException("airreservations.arg.InvalidBookingType");
		}

		return bookingClass;

	}

	/**
	 * Return release time based on the current GMT time. All Servers will be running on GMT time
	 * 
	 * @param onHoldDurationHHMM
	 * 
	 * @return the new onhold release time
	 * @throws ModuleException
	 */
	protected static Date calculateReleaseTime(String onHoldDurationHHMM) throws ModuleException {
		return BeanUtils.extendByMinutes(new Date(), AppSysParamsUtil.getTotalMinutes(onHoldDurationHHMM));
	}

	/**
	 * Calculates the release time in modification buffer time
	 * 
	 * @return
	 * @throws ModuleException
	 */
	protected static Date calculateReleaseTimeInModBufTime(String agentcode) throws ModuleException {
		return BeanUtils.extendByMinutes(new Date(), AppSysParamsUtil.getOnholdDurationWithinBufferTime(agentcode));
	}

	protected static Date calculateReleaseTimeInBufTime(String onHoldDurationHHMM) throws ModuleException {
		return BeanUtils.extendByMinutes(new Date(), AppSysParamsUtil.getTotalMinutes(onHoldDurationHHMM));
	}

	/**
	 * Returns the Cabin Classes Legend
	 * 
	 * @return the cabin classes legend
	 * @throws ModuleException
	 */
	public static String getCabinClassesLegend(boolean includeLegend) throws ModuleException {
		Map<String, String> colCabinClass = globalConfig.getActiveCabinClassesMap();
		StringBuffer legend = new StringBuffer();
		if (includeLegend) {
			for (String cabinClass : colCabinClass.keySet()) {
				if (legend.toString().equals("")) {
					legend.append(" " + cabinClass + " - " + colCabinClass.get(cabinClass));
				} else {
					legend.append(", " + cabinClass + " - " + colCabinClass.get(cabinClass));
				}
			}
		}
		return legend.toString();
	}

	/**
	 * Return All Logical Cabin Classes
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, LogicalCabinClass> getLogicalCabinClasses() throws ModuleException {
		LogicalCabinClassBD logicalCabinClassBD = ReservationModuleUtils.getLogicalCabinClassBD();
		return logicalCabinClassBD.getAllLogicalCabinClasses();
	}

	/**
	 * Update Tempory Payment to Success
	 * 
	 * @param temporyPaymentMap
	 */
	public static void updateTemporyPaymentStates(Map<Integer, CardPaymentInfo> temporyPaymentMap) {
		if (temporyPaymentMap != null && temporyPaymentMap.size() > 0) {
			CardPaymentInfo cardPaymentInfo;
			for (CardPaymentInfo cardPaymentInfo2 : temporyPaymentMap.values()) {
				cardPaymentInfo = cardPaymentInfo2;
				cardPaymentInfo.setSuccess(true);
			}
		}
	}

	/**
	 * Return total charge amount
	 * 
	 * @param totalCharges
	 * @return
	 */
	public static BigDecimal getTotalChargeAmount(BigDecimal[] totalCharges) {
		return AccelAeroCalculator.add(totalCharges);
	}

	/**
	 * Return updated totals
	 * 
	 * @param source
	 * @param target
	 * @param isSum
	 * @return
	 */
	public static BigDecimal[] getUpdatedTotals(BigDecimal[] source, BigDecimal[] target, boolean isSum) {
		BigDecimal[] values = new BigDecimal[source.length];

		if (isSum) {
			for (int i = 0; i < source.length; i++) {
				values[i] = AccelAeroCalculator.add(source[i], target[i]);
			}
		} else {
			for (int i = 0; i < source.length; i++) {
				values[i] = AccelAeroCalculator.subtract(source[i], target[i]);
			}
		}

		return values;
	}

	/**
	 * Capture Segment status change information
	 * 
	 * @param reservationSegment
	 * @param credentialsDTO
	 */
	public static void captureSegmentStatusChgInfo(ReservationSegment reservationSegment, CredentialsDTO credentialsDTO) {
		reservationSegment.setStatusModifiedDate(new Date());
		reservationSegment.setStatusModifiedUserId(credentialsDTO.getUserId());
		reservationSegment.setStatusModifiedCustomerId(credentialsDTO.getCustomerId());
		reservationSegment.setStatusModifiedAgentCode(credentialsDTO.getAgentCode());
		reservationSegment.setStatusModifiedSalesChannelCode(credentialsDTO.getSalesChannelCode());
	}

	/**
	 * Capture Segment Marketing change information
	 * 
	 * @param reservationSegment
	 * @param credentialsDTO
	 */
	public static void captureSegmentMarketingInfo(ReservationSegment reservationSegment, CredentialsDTO credentialsDTO) {

		if (SalesChannelsUtil.isSalesChannelWebOrMobile(credentialsDTO.getSalesChannelCode())) {

			if (credentialsDTO.getTrackInfoDTO() != null) {
				reservationSegment.setMarketingCarrierCode(credentialsDTO.getTrackInfoDTO().getCarrierCode());
			}

		} else if (credentialsDTO.getTrackInfoDTO() != null
				&& credentialsDTO.getSalesChannelCode() == SalesChannelsUtil.SALES_CHANNEL_LCC) {
			TrackInfoDTO trackInfoDTO = credentialsDTO.getTrackInfoDTO();
			reservationSegment.setMarketingCarrierCode(trackInfoDTO.getCarrierCode());
			reservationSegment.setMarketingAgentCode(trackInfoDTO.getMarketingAgentCode());
			reservationSegment.setMarketingStationCode(trackInfoDTO.getMarketingAgentStationCode());
			reservationSegment.setMarketingUserID(trackInfoDTO.getMarketingUserId());
		} else {
			reservationSegment.setMarketingCarrierCode(credentialsDTO.getDefaultCarrierCode());
			reservationSegment.setMarketingAgentCode(credentialsDTO.getAgentCode());
			reservationSegment.setMarketingStationCode(credentialsDTO.getAgentStation());
			reservationSegment.setMarketingUserID(credentialsDTO.getUserId());
		}

	}

	/**
	 * Returns Fare TO relavant to the pnr segment id
	 * 
	 * @param reservation
	 * @param prefferedLanguage
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, FareTO> getFareTOs(Reservation reservation, String prefferedLanguage) throws ModuleException {
		Map<Integer, Integer> pnrSegIdAndFareId = getFareIdsForSegments(reservation, false);

		// Remember infants could can have fare id(s). If it's a infant left
		// over case we need to load based on the fare id.
		if (pnrSegIdAndFareId.size() == 0) {
			pnrSegIdAndFareId = getFareIdsForSegments(reservation, true);
		}

		Map<Integer, FareTO> pnrSegIdAndFareTO = new HashMap<Integer, FareTO>();
		if (pnrSegIdAndFareId.size() > 0) {
			Map<String, FareCategoryTO> mapFareCategory = ReservationModuleUtils.getGlobalConfig().getFareCategories();
			Collection<Integer> colFareIds = new ArrayList<Integer>(pnrSegIdAndFareId.values());
			FaresAndChargesTO faresAndChargesTO = ReservationModuleUtils.getFareBD().getRefundableStatuses(colFareIds, null,
					prefferedLanguage);
			Map<Integer, FareTO> fareTOs = faresAndChargesTO.getFareTOs();

			Iterator<Integer> it = pnrSegIdAndFareId.keySet().iterator();
			Integer pnrSegId;
			Integer fareId;
			FareTO fareTO;
			while (it.hasNext()) {
				pnrSegId = it.next();
				fareId = pnrSegIdAndFareId.get(pnrSegId);
				fareTO = fareTOs.get(fareId);
				fareTO.setFareCategoryTO(mapFareCategory.get(fareTO.getFareCategoryCode()));
				pnrSegIdAndFareTO.put(pnrSegId, fareTO);
			}
		}

		return pnrSegIdAndFareTO;
	}

	/**
	 * Search passengers for fare id(s)
	 * 
	 * @param reservation
	 * @param isInfant
	 */
	private static Map<Integer, Integer> getFareIdsForSegments(Reservation reservation, boolean isInfant) {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;

		Map<Integer, Integer> pnrSegIdAndFareId = new HashMap<Integer, Integer>();

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			// Only taking adult / parents cos only they have fare id(s)
			// Every one will be going in the same route
			if (!isInfant && !ReservationApiUtils.isInfantType(reservationPax)) {
				findFareIds(reservationPax, pnrSegIdAndFareId);
				break;
			} else if (isInfant && ReservationApiUtils.isInfantType(reservationPax)) {
				findFareIds(reservationPax, pnrSegIdAndFareId);
				break;
			}
		}

		return pnrSegIdAndFareId;
	}

	/**
	 * Find the fare Id(s)
	 * 
	 * @param reservationPax
	 * @param pnrSegIdAndFareId
	 */
	private static void findFareIds(ReservationPax reservationPax, Map<Integer, Integer> pnrSegIdAndFareId) {
		Iterator<ReservationPaxFare> itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();
			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();

				if (reservationPaxFare.getFareId() != null) {
					pnrSegIdAndFareId.put(reservationPaxFareSegment.getPnrSegId(), reservationPaxFare.getFareId());
				}
			}
		}
	}

	/**
	 * Returns the quoted charges transparent to the reservation
	 * 
	 * @param chargeDescription
	 * @param chgRateId
	 * @param externalChgs
	 * @param isRatioValueInPercentage
	 * @param ratioValue
	 * @param boundryValue
	 * @param breakPoint
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static ExternalChgDTO getExternalChgDTO(String chagreCode, String chargeDescription, int chgRateId,
			EXTERNAL_CHARGES externalChgs, boolean isRatioValueInPercentage, BigDecimal ratioValue, ExternalChgDTO externalChgDTO,
			BigDecimal boundryValue, BigDecimal breakPoint) throws ModuleException {
		externalChgDTO.setChargeCode(chagreCode);
		externalChgDTO.setChargeDescription(chargeDescription);
		externalChgDTO.setChgRateId(chgRateId);
		externalChgDTO.setExternalChargesEnum(externalChgs);
		externalChgDTO.setRatioValue(ratioValue);
		externalChgDTO.setRatioValueInPercentage(isRatioValueInPercentage);
		externalChgDTO.setBoundryValue(boundryValue);
		externalChgDTO.setBreakPoint(breakPoint);

		if (externalChgs == EXTERNAL_CHARGES.CREDIT_CARD || externalChgs == EXTERNAL_CHARGES.HANDLING_CHARGE
				|| externalChgs == EXTERNAL_CHARGES.NO_SHORE || externalChgs == EXTERNAL_CHARGES.REFUND
				|| externalChgs == EXTERNAL_CHARGES.SEAT_MAP || externalChgs == EXTERNAL_CHARGES.INSURANCE
				|| externalChgs == EXTERNAL_CHARGES.MEAL || externalChgs == EXTERNAL_CHARGES.REF_ADJ
				|| externalChgs == EXTERNAL_CHARGES.NON_REF_ADJ || externalChgs == EXTERNAL_CHARGES.AIRPORT_SERVICE
				|| externalChgs == EXTERNAL_CHARGES.FLEXI_CHARGES || externalChgs == EXTERNAL_CHARGES.BAGGAGE
				|| externalChgs == EXTERNAL_CHARGES.INFLIGHT_SERVICES || externalChgs == EXTERNAL_CHARGES.PROMOTION_REWARD
				|| externalChgs == EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE || externalChgs == EXTERNAL_CHARGES.AIRPORT_TRANSFER
				|| externalChgs == EXTERNAL_CHARGES.NAME_CHANGE_CHARGE || externalChgs == EXTERNAL_CHARGES.AUTOMATIC_CHECKIN
				|| externalChgs == EXTERNAL_CHARGES.BSP_FEE) {
			externalChgDTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.SUR);
		} else if (externalChgs == EXTERNAL_CHARGES.JN_TAX || externalChgs == EXTERNAL_CHARGES.JN_ANCI
				|| externalChgs == EXTERNAL_CHARGES.JN_OTHER || externalChgs == EXTERNAL_CHARGES.SERVICE_TAX) {
			// Make charge group to TAX if charges are JN tax related ESUs
			externalChgDTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.TAX);
		} else if (externalChgs == EXTERNAL_CHARGES.ANCI_PENALTY) {
			// Make charge group to PENALTY if charges are Ancillary Penalty related ESUs
			externalChgDTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.PEN);
		} else {
			throw new ModuleException("airreservations.externalcharge.unidentifiedExtChgFound");
		}

		return externalChgDTO;
	}

	/**
	 * Returns the quoted charges transparent to the reservation
	 * 
	 * @param chargeDescription
	 * @param chgRateId
	 * @param externalChgs
	 * @param isRatioValueInPercentage
	 * @param ratioValue
	 * @param boundryValue
	 * @param breakPoint
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static BasicChargeDTO getBasicChgDTO(String chagreCode, String chargeDescription, int chgRateId,
			boolean isRatioValueInPercentage, BigDecimal ratioValue, BasicChargeDTO basicChgDto, BigDecimal boundryValue,
			BigDecimal breakPoint) throws ModuleException {
		basicChgDto.setChargeCode(chagreCode);
		basicChgDto.setChargeDescription(chargeDescription);
		basicChgDto.setChgRateId(chgRateId);
		basicChgDto.setRatioValue(ratioValue);
		basicChgDto.setRatioValueInPercentage(isRatioValueInPercentage);
		basicChgDto.setBoundryValue(boundryValue);
		basicChgDto.setBreakPoint(breakPoint);

		return basicChgDto;
	}

	public static ExternalChgDTO getExternalChgDTO(List<QuotedChargeDTO> quotedChargeDTOs, ExternalChgDTO externalChgDTO,
			EXTERNAL_CHARGES externalChgs) throws ModuleException {

		if (!quotedChargeDTOs.isEmpty()) {

			if (isValidExternalCharge(quotedChargeDTOs.get(0), externalChgs)) {

				externalChgDTO = createExternalChgDTO(quotedChargeDTOs.get(0), externalChgDTO, externalChgs);

				for (QuotedChargeDTO quotedChargeDTO : quotedChargeDTOs) {
					externalChgDTO.addChargeRate(createExternalChgRateDTO(quotedChargeDTO, externalChgs));
				}

			}
		}
		return externalChgDTO;
	}

	private static boolean isValidExternalCharge(QuotedChargeDTO quotedChargeDTO, EXTERNAL_CHARGES externalChgs)
			throws ModuleException {
		boolean valid = false;
		switch (externalChgs) {
		case HANDLING_CHARGE:
			if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_RESERVATION
					|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
				valid = true;
			}
			break;
		case CREDIT_CARD:
			break;
		case JN_TAX:
			if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_OND) {
				valid = true;
			}
			break;
		}

		if (!valid) {
			throw new ModuleException("airreservations.externalcharge.InvalidAppliesToType");
		}
		return true;

	}

	private static ExternalChgDTO createExternalChgDTO(QuotedChargeDTO quotedChargeDTO, ExternalChgDTO externalChgDTO,
			EXTERNAL_CHARGES externalChgs) throws ModuleException {

		externalChgDTO.setChargeCode(quotedChargeDTO.getChargeCode());
		externalChgDTO.setChargeDescription(quotedChargeDTO.getChargeDescription());
		externalChgDTO.setChgRateId(quotedChargeDTO.getChargeRateId());
		externalChgDTO.setExternalChargesEnum(externalChgs);
		if (externalChgs == EXTERNAL_CHARGES.JN_TAX) {
			externalChgDTO.setRatioValue(new BigDecimal(Double.toString(quotedChargeDTO.getChargeValuePercentage())));
		} else {
			externalChgDTO.setRatioValue(AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getChargeValuePercentage()));
		}
		externalChgDTO.setRatioValueInPercentage(quotedChargeDTO.isChargeValueInPercentage());

		externalChgDTO.setAppliesTo(quotedChargeDTO.getApplicableTo());
		externalChgDTO.setChargeValueMin(quotedChargeDTO.getChargeValueMin());
		externalChgDTO.setChargeValueMax(quotedChargeDTO.getChargeValueMax());
		externalChgDTO.setJourneyType(quotedChargeDTO.getChargeRateJourneyType());
		externalChgDTO.setBoundryValue(quotedChargeDTO.getBoundryValue());
		externalChgDTO.setBreakPoint(quotedChargeDTO.getBreakPoint());

		if (externalChgs == EXTERNAL_CHARGES.CREDIT_CARD || externalChgs == EXTERNAL_CHARGES.HANDLING_CHARGE
				|| externalChgs == EXTERNAL_CHARGES.NO_SHORE || externalChgs == EXTERNAL_CHARGES.REFUND
				|| externalChgs == EXTERNAL_CHARGES.SEAT_MAP || externalChgs == EXTERNAL_CHARGES.INSURANCE
				|| externalChgs == EXTERNAL_CHARGES.MEAL || externalChgs == EXTERNAL_CHARGES.REF_ADJ
				|| externalChgs == EXTERNAL_CHARGES.NON_REF_ADJ || externalChgs == EXTERNAL_CHARGES.AIRPORT_SERVICE
				|| externalChgs == EXTERNAL_CHARGES.FLEXI_CHARGES || externalChgs == EXTERNAL_CHARGES.BAGGAGE
				|| externalChgs == EXTERNAL_CHARGES.INFLIGHT_SERVICES || externalChgs == EXTERNAL_CHARGES.PROMOTION_REWARD
				|| externalChgs == EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE || externalChgs == EXTERNAL_CHARGES.AIRPORT_TRANSFER
				|| externalChgs == EXTERNAL_CHARGES.BSP_FEE || externalChgs == EXTERNAL_CHARGES.AUTOMATIC_CHECKIN) {
			externalChgDTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.SUR);
		} else if (externalChgs == EXTERNAL_CHARGES.JN_TAX) {
			// Make charge group to TAX if charges are JN tax related ESUs
			externalChgDTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.TAX);
		} else {
			throw new ModuleException("airreservations.externalcharge.unidentifiedExtChgFound");
		}

		return externalChgDTO;
	}

	private static ChargeRateDTO createExternalChgRateDTO(QuotedChargeDTO quotedChargeDTO, EXTERNAL_CHARGES externalChgs)
			throws ModuleException {

		ChargeRateDTO chargeRateDto = new ChargeRateDTO();

		chargeRateDto.setChgRateId(quotedChargeDTO.getChargeRateId());
		if (externalChgs == EXTERNAL_CHARGES.JN_TAX) {
			chargeRateDto.setRatioValue(new BigDecimal(Double.toString(quotedChargeDTO.getChargeValuePercentage())));
		} else {
			chargeRateDto.setRatioValue(AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getChargeValuePercentage()));
		}
		chargeRateDto.setChargeValueMin(quotedChargeDTO.getChargeValueMin());
		chargeRateDto.setChargeValueMax(quotedChargeDTO.getChargeValueMax());
		chargeRateDto.setJourneyType(quotedChargeDTO.getChargeRateJourneyType());
		chargeRateDto.setCabinClassCode(quotedChargeDTO.getCabinClassCode());

		return chargeRateDto;
	}

	/**
	 * @param agent
	 * @param externalChgDTO
	 * @throws ModuleException
	 */
	public static void updateHandlingFeeFromAgent(Agent agent, ExternalChgDTO externalChgDTO) throws ModuleException {

		for (ChargeRateDTO chargeRateDTO : externalChgDTO.getChargeRates()) {

			BigDecimal ratioValue = BigDecimal.ZERO;
			switch (chargeRateDTO.getJourneyType()) {
			case ChargeRateJourneyType.ANY:
				break;
			case ChargeRateJourneyType.ONEWAY:
				if (agent.getHandlingFeeOnewayAmount() != null) {
					ratioValue = agent.getHandlingFeeOnewayAmount();
				}
				break;
			case ChargeRateJourneyType.RETURN:
				if (agent.getHandlingFeeReturnAmount() != null) {
					ratioValue = agent.getHandlingFeeReturnAmount();
				}
				break;
			}

			chargeRateDTO.setChargeValueMin(agent.getHandlingFeeMinAmount() != null ? agent.getHandlingFeeMinAmount() : null);

			chargeRateDTO.setChargeValueMax(agent.getHandlingFeeMaxAmount() != null ? agent.getHandlingFeeMaxAmount() : null);

			chargeRateDTO.setRatioValue(ratioValue);
			chargeRateDTO.setChargeBasis(ChargeBasisEnum.getChargeBasis(agent.getHandlingFeeChargeBasisCode()));
			chargeRateDTO.setValid(agent.getHandlingFeeEnable().equalsIgnoreCase("Y"));

		}
		externalChgDTO.setAppliesTo(agent.getHandlingFeeAppliesTo());

	}

	/**
	 * Is Fare Exist or not
	 * 
	 * @param fareAmount
	 * @return
	 */
	public static boolean isFareExist(Object fareAmount) {
		return (fareAmount != null) ? true : false;
	}

	/**
	 * Find out whether the fare is refundable or not
	 * 
	 * @param fareTO
	 * @param paxType
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isRefundable(FareTO fareTO, String paxType) throws ModuleException {
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			return fareTO.isAdultFareRefundable();
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			return fareTO.isChildFareRefundable();
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			return fareTO.isInfantFareRefundable();
		} else {
			throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
		}
	}

	/**
	 * Returns the if the passenger types exist or not
	 * 
	 * [0] --> Adult [1] --> Child [2] --> Infant
	 * 
	 * @param colReservationPax
	 * @return
	 */
	public static boolean[] isPaxTypesExist(Collection<ReservationPax> colReservationPax) {
		boolean isAdultsExist = false;
		boolean isChildExist = false;
		boolean isInfantExist = false;
		ReservationPax reservationPax;

		for (ReservationPax reservationPax2 : colReservationPax) {
			reservationPax = reservationPax2;

			// Adult
			if (ReservationApiUtils.isAdultType(reservationPax)) {
				isAdultsExist = true;
				// Child
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				isChildExist = true;
				// Infant
			} else if (ReservationApiUtils.isInfantType(reservationPax)) {
				isInfantExist = true;
			}

			// Optimized break for all passenger types
			// Cause that's the only combination we are expecting fast
			if (isAdultsExist && isChildExist && isInfantExist) {
				break;
			}
		}

		return new boolean[] { isAdultsExist, isChildExist, isInfantExist };
	}

	/**
	 * Returns the pnr segment id(s)
	 * 
	 * @param colReservationSegmentDTO
	 * @return
	 */
	public static Collection<Integer> getPnrSegIds(Collection<ReservationSegmentDTO> colReservationSegmentDTO) {
		Collection<Integer> pnrSegIds = new HashSet<Integer>();
		ReservationSegmentDTO reservationSegmentDTO;
		for (ReservationSegmentDTO reservationSegmentDTO2 : colReservationSegmentDTO) {
			reservationSegmentDTO = reservationSegmentDTO2;
			pnrSegIds.add(reservationSegmentDTO.getPnrSegId());
		}

		return pnrSegIds;
	}

	/**
	 * Returns the reservation segment dto(s)
	 * 
	 * @param colReservationSegmentDTO
	 * @param pnrSegIds
	 * @return
	 */
	public static Collection<ReservationSegmentDTO>
			getReservationSegmentDTOs(Collection<ReservationSegmentDTO> colReservationSegmentDTO, Collection<Integer> pnrSegIds) {
		Collection<ReservationSegmentDTO> selectedReservationSegmentDTO = new ArrayList<ReservationSegmentDTO>();

		if (pnrSegIds != null && pnrSegIds.size() > 0) {
			for (ReservationSegmentDTO reservationSegmentDTO : colReservationSegmentDTO) {

				if (pnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
					selectedReservationSegmentDTO.add(reservationSegmentDTO);
				}
			}
		}

		return selectedReservationSegmentDTO;
	}

	/**
	 * Returns the OND Group Id(s)
	 * 
	 * @param colReservationSegmentDTO
	 * @return
	 */
	public static Collection<Integer> getONDGroupIds(Collection<ReservationSegmentDTO> colReservationSegmentDTO) {
		Collection<Integer> ondGroupIds = new HashSet<Integer>();
		ReservationSegmentDTO reservationSegmentDTO;
		for (ReservationSegmentDTO reservationSegmentDTO2 : colReservationSegmentDTO) {
			reservationSegmentDTO = reservationSegmentDTO2;
			ondGroupIds.add(reservationSegmentDTO.getFareGroupId());
		}

		return ondGroupIds;
	}

	/**
	 * Format carrier description legend
	 * 
	 * @param carrierCodeAndFlightsMap
	 * @return
	 */
	public static String formatCarrierLegend(Map<String, Collection<String>> carrierCodeAndFlightsMap) {
		StringBuilder carrierCodeLegend = new StringBuilder("");

		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_OPERATING_CARRIER_LEGEND))) {
			String carrierCode;
			Collection<String> flights;

			for (String string : carrierCodeAndFlightsMap.keySet()) {
				carrierCode = string;
				flights = carrierCodeAndFlightsMap.get(carrierCode);

				if (carrierCodeLegend.length() == 0) {
					carrierCodeLegend.append(
							BeanUtils.constructINStringForInts(flights) + " - " + AppSysParamsUtil.getCarrierDesc(carrierCode));
				} else {
					carrierCodeLegend.append(" / " + BeanUtils.constructINStringForInts(flights) + " - "
							+ AppSysParamsUtil.getCarrierDesc(carrierCode));
				}
			}
		}

		return carrierCodeLegend.toString();
	}

	/**
	 * Returns per passenger external charges
	 * 
	 * @param externalChgDTO
	 * @param numberOfConfirmedAdults
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<ExternalChgDTO> getPerPaxExternalCharges(ExternalChgDTO externalChgDTO,
			int numberOfConfirmedAdults) {

		BigDecimal[] bdEffectiveChgAmount = AccelAeroCalculator.roundAndSplit(externalChgDTO.getAmount(),
				numberOfConfirmedAdults);
		Collection<ExternalChgDTO> colExternalChgDTO = new ArrayList<ExternalChgDTO>();

		ExternalChgDTO tmpExternalChgDTO;
		for (int i = 0; i < numberOfConfirmedAdults; i++) {
			tmpExternalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
			tmpExternalChgDTO.setAmount(bdEffectiveChgAmount[i]);

			colExternalChgDTO.add(tmpExternalChgDTO);
		}

		return colExternalChgDTO;
	}

	/**
	 * Copies the collection
	 * 
	 * @param externalCharges
	 * @param colExternalChgDTO
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Collection[] copyToCollectionArray(Collection[] externalCharges, Collection colExternalChgDTO) {
		Iterator itColExternalChgDTO = colExternalChgDTO.iterator();
		ExternalChgDTO externalChgDTO;
		Collection colHolderExternalChgDTO;
		Collection holder = new ArrayList();

		while (itColExternalChgDTO.hasNext()) {
			externalChgDTO = (ExternalChgDTO) itColExternalChgDTO.next();

			colHolderExternalChgDTO = new ArrayList();
			colHolderExternalChgDTO.add(externalChgDTO);

			holder.add(colHolderExternalChgDTO);
		}

		if (externalCharges != null) {
			int index = 0;
			for (Iterator iter = holder.iterator(); iter.hasNext();) {
				Collection first = (Collection) iter.next();
				Collection second = externalCharges[index];

				if (second != null) {
					first.addAll(second);
				}
				index++;
			}
		}

		return (Collection[]) holder.toArray(new Collection[holder.size()]);
	}

	/**
	 * Create a Stack
	 * 
	 * @param externalCharges
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static LinkedList createLinkedList(Collection[] externalCharges) {
		LinkedList stack = new LinkedList();

		for (Collection externalCharge : externalCharges) {
			stack.addLast(externalCharge);
		}

		return stack;
	}

	/**
	 * Checks the Segment compatibility
	 * 
	 * @param colOndFareDTOs
	 * @param colFrontEndFlgSegIds
	 * @param skipDuplicateCheck
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<OndFareDTO> checkSegmentsCompatibility(Collection<OndFareDTO> colOndFareDTOs,
			Collection<Integer> colFrontEndFlgSegIds, Reservation res, boolean skipDuplicateCheck) throws ModuleException {
		Iterator<OndFareDTO> itColOndFareDTO = colOndFareDTOs.iterator();
		OndFareDTO ondFareDTO;
		SegmentSeatDistsDTO segmentSeatDistsDTO;
		Iterator<SegmentSeatDistsDTO> itSegmentSeatDistsDTO;
		Collection<Integer> colInvFlgSegIds = new ArrayList<Integer>();

		while (itColOndFareDTO.hasNext()) {
			ondFareDTO = itColOndFareDTO.next();
			itSegmentSeatDistsDTO = ondFareDTO.getSegmentSeatDistsDTO().iterator();

			while (itSegmentSeatDistsDTO.hasNext()) {
				segmentSeatDistsDTO = itSegmentSeatDistsDTO.next();
				colInvFlgSegIds.add(new Integer(segmentSeatDistsDTO.getFlightSegId()));
			}
		}

		if (colFrontEndFlgSegIds.size() == colInvFlgSegIds.size()) {
			if (colFrontEndFlgSegIds.containsAll(colInvFlgSegIds)) {

				if (res != null) {
					checkEndValidations(colFrontEndFlgSegIds, res.getPassengers(), skipDuplicateCheck, null);
				}

				return colOndFareDTOs;
			}
		}

		// This means that the front end flight segment ids
		// does not match with the inventory flight segment ids
		log.error("airreservations.arg.InvalidFareInformation:" + colInvFlgSegIds.toString());
		throw new ModuleException("airreservations.arg.InvalidFareInformation");
	}

	/**
	 * Get pnr pax ids from reservation
	 * 
	 * @param reservation
	 * @return
	 */
	// public static Collection<Integer> getPnrPaxIds(Reservation reservation) {
	// Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
	// Iterator<ReservationPax> iter = reservation.getPassengers().iterator();
	// while (iter.hasNext()) {
	// ReservationPax resPax = (ReservationPax) iter.next();
	// pnrPaxIds.add(resPax.getPnrPaxId());
	// }
	// return pnrPaxIds;
	// }

	public static void checkEndValidations(Collection<Integer> fltSegmentIds, Set<ReservationPax> resPaxs, String pnr)
			throws ModuleException {
		checkEndValidations(fltSegmentIds, resPaxs, false, pnr);
	}

	/**
	 * Method Can be use to validate reservation at end . eg: duplicate name check , etc etc
	 * 
	 * @param pnr
	 * @param res
	 * 
	 * @throws ModuleException
	 *             with relevant key which needs to be handle at each application accordingly.
	 */
	public static void checkEndValidations(Collection<Integer> fltSegmentIds, Set<ReservationPax> resPaxs,
			boolean skipDuplicateCheck, String pnr) throws ModuleException {

		ArrayList<String> adultNameList = new ArrayList<String>();
		ArrayList<String> childNameList = new ArrayList<String>();
		Collection<Integer> paxIds = new ArrayList<Integer>();

		if (fltSegmentIds == null || fltSegmentIds.isEmpty()) {
			return;
		}

		// VALIDATION No 1 : AARESAA-1953
		if (AppSysParamsUtil.isDuplicateNameCheckEnabled() && !skipDuplicateCheck) {

			for (ReservationPax reservationPax : resPaxs) {
				ReservationPax resPax = reservationPax;
				if (resPax != null) {
					if (isAdultType(resPax)) {
						String fullName = BeanUtils.nullHandler(resPax.getTitle()) + ReservationInternalConstants.DLIM
								+ BeanUtils.nullHandler(resPax.getFirstName()) + ReservationInternalConstants.DLIM
								+ BeanUtils.nullHandler(resPax.getLastName());
						fullName = fullName.toUpperCase();
						if (fullName.length() > 0 && !fullName.contains("T B A")) {
							adultNameList.add(fullName);
						}
					} else if (isChildType(resPax)) {
						String fullName = BeanUtils.nullHandler(resPax.getTitle()) + ReservationInternalConstants.DLIM
								+ BeanUtils.nullHandler(resPax.getFirstName()) + ReservationInternalConstants.DLIM
								+ BeanUtils.nullHandler(resPax.getLastName());
						fullName = fullName.toUpperCase();
						if (fullName.length() > 0 && !fullName.contains("T B A")) {
							childNameList.add(fullName);
						}
					}
					if (resPax.getPnrPaxId() != null) {
						paxIds.add(resPax.getPnrPaxId());
					}
				}
			}

			if ((adultNameList.isEmpty() && childNameList.isEmpty())) {
				return;
			}

			Collection<String> dupNameCollection = ReservationModuleUtils.getReservationQueryBD().getDuplicateNameReservations(
					fltSegmentIds, adultNameList, childNameList, pnr, paxIds);

			if (!dupNameCollection.isEmpty()) {
				log.error("airreservations.arg.duplicateNamesExist:" + dupNameCollection.toString());
				throw new ModuleException(ResponseCodes.DUPLICATE_NAMES_IN_FLIGHT_SEGMENT, dupNameCollection);
			}

			// Validate the duplicate names in the current setup
			Set<String> currentNameSet = new HashSet<String>();
			currentNameSet.addAll(adultNameList);
			currentNameSet.addAll(childNameList);

			if (currentNameSet.size() < adultNameList.size() + childNameList.size()) {
				List<String> duplicateNames = new ArrayList<String>();
				List<String> nameCollection = new ArrayList<String>();
				nameCollection.addAll(adultNameList);
				nameCollection.addAll(childNameList);
				for (String name : currentNameSet) {
					int count = 0;
					for (String allName : nameCollection) {
						if (name.equalsIgnoreCase(allName)) {
							count++;
							if (count > 1) {
								duplicateNames.add(name);
								break;
							}
						}
					}

				}
				throw new ModuleException(ResponseCodes.DUPLICATE_NAMES_IN_FLIGHT_SEGMENT, duplicateNames);
			}

		}
	}

	/**
	 * @param reservationPax
	 * @return
	 */
	public static boolean isTbaPassenger(ReservationPax reservationPax) {

		String fullName = BeanUtils.nullHandler(reservationPax.getTitle()) + ReservationInternalConstants.DLIM
				+ BeanUtils.nullHandler(reservationPax.getFirstName()) + ReservationInternalConstants.DLIM
				+ BeanUtils.nullHandler(reservationPax.getLastName());
		fullName = fullName.toUpperCase();
		if (fullName.contains("T B A")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param colTnxIds
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static String getElementsInCollection(Collection colTnxIds) {
		String result = "TPT_IDs : ";
		for (Object tptId : colTnxIds) {
			result += ((Integer) tptId).toString() + "-";
		}
		return result;
	}

	/**
	 * Returns the Unique Segment Key
	 * 
	 * @param flightNo
	 * @param segmentCode
	 * @param departureDate
	 * @return
	 */
	public static String getUniqueSegmentKey(String flightNo, String segmentCode, Date departureDate, String cabinClass,
			String status) {
		return BeanUtils.nullHandler(flightNo).toUpperCase() + BeanUtils.nullHandler(segmentCode)
				+ BeanUtils.parseDateFormat(departureDate, "yyMMddHHmm") + BeanUtils.nullHandler(cabinClass)
				+ BeanUtils.nullHandler(status);
	}

	/**
	 * Return PNR validity.
	 * 
	 * @param pnr
	 * @return
	 */
	public static boolean isPNRValid(String pnr) {
		boolean isValid = false;

		if (pnr != null && !"".equals(pnr)) {
			pnr = pnr.trim();
			Map<String, String> pnrValidationRules = ReservationModuleUtils.getAirReservationConfig().getPnrValidationRulesMap();
			int maxLength = Integer.parseInt(pnrValidationRules.get("maxLength").toString());
			int minLength = Integer.parseInt(pnrValidationRules.get("minLength").toString());
			if (pnr.length() >= minLength && pnr.length() <= maxLength) {
				String regEx = pnrValidationRules.get("javaRegEx").toString() + "{" + pnr.length() + "}";
				Pattern regExPattern = Pattern.compile(regEx);
				Matcher regExMatcher = regExPattern.matcher(pnr);
				if (regExMatcher.find()) {
					isValid = true;
				}
			}
		}

		return isValid;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Collection getSerializableValuesCollection(Map map) {
		Collection serializableCollection = null;
		if (map != null && map.size() > 0) {
			serializableCollection = new ArrayList();
			for (Iterator it = map.keySet().iterator(); it.hasNext();) {
				serializableCollection.add(map.get(it.next()));
			}
		}
		return serializableCollection;
	}

	/**
	 * dep time less than current time
	 * 
	 * @param reservationCutoffTime
	 * @param estDepartureZulu
	 * @return
	 */
	public static boolean hasReachedFlightClosure(String reservationCutoffTime, Collection<Date> estDepartureZulu) {
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();

		for (Date depDate : estDepartureZulu) {
			if (log.isDebugEnabled()) {
				log.debug("----------------------------------------------------------");
				log.debug("Flight Departure Time :" + String.valueOf(depDate.getTime()));
				log.debug("Current System Time   :" + String.valueOf(currentDate.getTime()));
				log.debug("----------------------------------------------------------");
			}
			if ((depDate.getTime() - getClosureInMilliseconds(reservationCutoffTime)) <= currentDate.getTime()) {
				log.debug("hasReachedFlightClosure");
				return true;
			}
		}

		return false;
	}

	private static Long getClosureInMilliseconds(String cutoffTimeStr) {
		String hours = cutoffTimeStr.substring(0, cutoffTimeStr.indexOf(":"));
		String mins = cutoffTimeStr.substring(cutoffTimeStr.indexOf(":") + 1);
		return (Long.parseLong(hours) * 60 + Long.parseLong(mins)) * 60 * 1000;
	}

	/**
	 * Update modification states
	 * 
	 * @param reservation
	 * @param lastModificationTimeStamp
	 * @param lastCurrencyCode
	 */
	public static void updateModificationStates(Reservation reservation, Date lastModificationTimeStamp, String lastCurrencyCode,
			CredentialsDTO credentialsDTO) {

		if (lastModificationTimeStamp != null) {
			reservation.setLastModificationTimestamp(lastModificationTimeStamp);
		} else {
			reservation.setLastModificationTimestamp(new Date());
		}

		if (BeanUtils.nullHandler(lastCurrencyCode).length() > 0) {
			reservation.setLastCurrencyCode(lastCurrencyCode);
		} else {
			if (BeanUtils.nullHandler(credentialsDTO.getAgentCurrencyCode()).length() > 0) {
				reservation.setLastCurrencyCode(credentialsDTO.getAgentCurrencyCode());
			} else {
				reservation.setLastCurrencyCode(AppSysParamsUtil.getBaseCurrency());
			}
		}
	}

	/**
	 * Has Payment or not
	 * 
	 * @param tnxns
	 * @return
	 */
	public static boolean hasPayment(Collection<ReservationTnx> colReservationTnx) {
		for (ReservationTnx reservationTnx : colReservationTnx) {
			// if positive payment has been done. i.e. check if > 0
			if (reservationTnx.getAmount().negate().compareTo(new BigDecimal(0)) > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the confirmed inverse segments for cancellation e.g. If the users pass departure reservation segment ids
	 * it will return the corresponding return reservation segment ids which are CONFIRMED if you includeAllSegments
	 * make false
	 * 
	 * @param reservation
	 * @param includeAllSegments
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Collection<Integer>, Collection<Integer>> getInverseSegments(Reservation reservation,
			boolean includeAllSegments) throws ModuleException {

		if (reservation == null || reservation.getSegmentsView() == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}

		Map<Collection<Integer>, Integer> mapPnrSegIdsAndFareIds = ReservationApiUtils.getFareIdsForPnrSegIds(reservation);
		Map<Collection<Integer>, Integer> mapPnrSegIdsAndReturnOndGroupIds = ReservationApiUtils
				.getReturnOndGroupIdsForPnrSegIds(reservation);

		Collection<Integer> colFareIds = new HashSet<Integer>();
		colFareIds.addAll(mapPnrSegIdsAndFareIds.values());

		Collection<Integer> colReturnOndGroupIds = new HashSet<Integer>();
		colReturnOndGroupIds.addAll(mapPnrSegIdsAndReturnOndGroupIds.values());
		if (colReturnOndGroupIds.contains(null)) {
			colReturnOndGroupIds.remove(null);
		}

		Map<Collection<Integer>, Collection<Integer>> mapPnrSegIdsAndInversePnrSegIds;
		Collection<Integer> colSetPnrSegIds;

		// Doing a easy comparison to check inverse segments exist or not
		if (mapPnrSegIdsAndReturnOndGroupIds.values().size() == colReturnOndGroupIds.size() && colReturnOndGroupIds.size() > 0) {
			mapPnrSegIdsAndInversePnrSegIds = new HashMap<Collection<Integer>, Collection<Integer>>();

			for (Collection<Integer> collection : mapPnrSegIdsAndReturnOndGroupIds.keySet()) {
				colSetPnrSegIds = collection;
				mapPnrSegIdsAndInversePnrSegIds.put(colSetPnrSegIds, new HashSet<Integer>());
			}

			return mapPnrSegIdsAndInversePnrSegIds;
		}
		if (colReturnOndGroupIds.size() == 0 && mapPnrSegIdsAndFareIds.values().size() == colFareIds.size()) {
			mapPnrSegIdsAndInversePnrSegIds = new HashMap<Collection<Integer>, Collection<Integer>>();

			for (Collection<Integer> collection : mapPnrSegIdsAndFareIds.keySet()) {
				colSetPnrSegIds = collection;
				mapPnrSegIdsAndInversePnrSegIds.put(colSetPnrSegIds, new HashSet<Integer>());
			}

			return mapPnrSegIdsAndInversePnrSegIds;
		}

		// This means that Same Fare Id exist and we need to drill down more to find the exact match
		Map<Integer, Collection<ReservationSegmentDTO>> mapGrpSegments = ReservationApiUtils
				.getSegSeqOrderedGrpSegments(reservation.getSegmentsView());
		ReservationSegmentDTO reservationSegmentDTO;
		Map<Collection<Integer>, OndGroupDTO> mapPnrSegIdsAndOndGrpDTO = new LinkedHashMap<Collection<Integer>, OndGroupDTO>();
		Integer ondGroupId;
		Collection<ReservationSegmentDTO> colSegments;
		Collection<String> colOndCodes;
		Collection<String> colReturnFlags;
		Collection<Integer> colReturnGroupId;
		OndGroupDTO ondGroupDTO;

		for (Integer integer : mapGrpSegments.keySet()) {
			ondGroupId = integer;
			colSegments = mapGrpSegments.get(ondGroupId);

			colSetPnrSegIds = new HashSet<Integer>();
			colOndCodes = new ArrayList<String>();
			colReturnFlags = new HashSet<String>();
			colReturnGroupId = new HashSet<Integer>();

			for (ReservationSegmentDTO reservationSegmentDTO2 : colSegments) {
				reservationSegmentDTO = reservationSegmentDTO2;

				// Picking only confirmed and wait listed segments cos we only need them for cancellation
				if (includeAllSegments
						|| ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
								.equals(reservationSegmentDTO.getStatus())
						|| ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING
								.equals(reservationSegmentDTO.getStatus())) {
					colSetPnrSegIds.add(reservationSegmentDTO.getPnrSegId());
					colOndCodes.add(reservationSegmentDTO.getSegmentCode());
					colReturnFlags.add(reservationSegmentDTO.getReturnFlag());
					colReturnGroupId.add(reservationSegmentDTO.getReturnGroupId());
				}
			}

			// If no pnr segments no need to track them
			if (colSetPnrSegIds.size() > 0) {
				// Checking for consistency
				if (colReturnFlags.size() > 1) {
					throw new ModuleException("airreservations.arg.invalidOnd");
				}

				ondGroupDTO = new OndGroupDTO();
				ondGroupDTO.setOndGroupId(ondGroupId);
				ondGroupDTO.setOndCode(getOndCode(colOndCodes));
				ondGroupDTO.setReturnFlag(BeanUtils.getFirstElement(colReturnFlags));
				ondGroupDTO.setReturnGroupId(BeanUtils.getFirstElement(colReturnGroupId));

				mapPnrSegIdsAndOndGrpDTO.put(colSetPnrSegIds, ondGroupDTO);
			}
		}

		mapPnrSegIdsAndInversePnrSegIds = new HashMap<Collection<Integer>, Collection<Integer>>();

		for (Collection<Integer> collection : mapPnrSegIdsAndOndGrpDTO.keySet()) {
			colSetPnrSegIds = collection;
			ondGroupDTO = mapPnrSegIdsAndOndGrpDTO.get(colSetPnrSegIds);

			mapPnrSegIdsAndInversePnrSegIds.put(colSetPnrSegIds,
					ReservationApiUtils.locateInversePnrSegIds(mapPnrSegIdsAndOndGrpDTO, colSetPnrSegIds, ondGroupDTO,
							mapPnrSegIdsAndFareIds, mapPnrSegIdsAndReturnOndGroupIds));
		}

		return mapPnrSegIdsAndInversePnrSegIds;
	}

	/**
	 * Returns fare ids for pnr segment ids
	 * 
	 * @param reservation
	 * @return
	 */
	private static Map<Collection<Integer>, Integer> getFareIdsForPnrSegIds(Reservation reservation) {
		Map<Collection<Integer>, Integer> mapPnrSegIdsAndFareIds = new HashMap<Collection<Integer>, Integer>();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> colSetPnrSegIds;

		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			reservationPax = reservationPax2;

			// If it's Parent of Adult
			if (!isInfantType(reservationPax)) {
				for (ReservationPaxFare reservationPaxFare2 : reservationPax.getPnrPaxFares()) {
					reservationPaxFare = reservationPaxFare2;

					colSetPnrSegIds = new HashSet<Integer>();

					for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
						reservationPaxFareSegment = reservationPaxFareSegment2;
						colSetPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
					}

					mapPnrSegIdsAndFareIds.put(colSetPnrSegIds, reservationPaxFare.getFareId());
				}
				break;
			}
		}

		return mapPnrSegIdsAndFareIds;
	}

	/**
	 * Returns inverse pnr segment ids
	 * 
	 * @param mapPnrSegIdsAndOndGrpDTO
	 * @param colSetPnrSegIds
	 * @param ondGroupDTO
	 * @param mapPnrSegIdsAndFareIds
	 * @return
	 */
	private static Collection<Integer> locateInversePnrSegIds(Map<Collection<Integer>, OndGroupDTO> mapPnrSegIdsAndOndGrpDTO,
			Collection<Integer> colSetPnrSegIds, OndGroupDTO ondGroupDTO,
			Map<Collection<Integer>, Integer> mapPnrSegIdsAndFareIds,
			Map<Collection<Integer>, Integer> mapPnrSegIdsAndReturnOndGroupIds) {
		Collection<Integer> colInversePnrSegIds = new HashSet<Integer>();
		Collection<Integer> targetColSetPnrSegIds;
		OndGroupDTO targetOndGroupDTO;

		for (Collection<Integer> collection : mapPnrSegIdsAndOndGrpDTO.keySet()) {
			targetColSetPnrSegIds = collection;
			targetOndGroupDTO = mapPnrSegIdsAndOndGrpDTO.get(targetColSetPnrSegIds);

			// Return Flags should not be equal cos return/return or departure/departure will not occure
			if (!ondGroupDTO.getReturnFlag().equals(targetOndGroupDTO.getReturnFlag())
					|| ondGroupDTO.getReturnGroupId().intValue() == targetOndGroupDTO.getReturnGroupId().intValue()) {

				if (mapPnrSegIdsAndReturnOndGroupIds.size() > 0) { // Compare ond return group ids, to find out return
																	// bookings with different fare ids
					Integer tempTargetColSetPnrSegIds = mapPnrSegIdsAndReturnOndGroupIds.get(targetColSetPnrSegIds);
					Integer tempColSetPnrSegIds = mapPnrSegIdsAndReturnOndGroupIds.get(colSetPnrSegIds);
					if (tempTargetColSetPnrSegIds != null && tempColSetPnrSegIds != null) {
						if (tempColSetPnrSegIds.intValue() == tempTargetColSetPnrSegIds.intValue()) {
							// This means going ond code is equal to the arrival ond code
							String ondCode = ondGroupDTO.getOndCode();
							String targetOndCode = getInverseOndCode(targetOndGroupDTO.getOndCode());

							if (getFirstAndLastOndCode(ondCode).equals(getFirstAndLastOndCode(targetOndCode))) {
								colInversePnrSegIds.addAll(targetColSetPnrSegIds);
								break;
							}
						}
					}
				}

				// Return Ond Group not present, compare fare ids
				if ((ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE.equals(ondGroupDTO.getReturnFlag())
						&& (ondGroupDTO.getOndGroupId().intValue() < targetOndGroupDTO.getOndGroupId().intValue()))
						|| ((ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(ondGroupDTO.getReturnFlag())
								|| (ondGroupDTO.getReturnGroupId().intValue() == targetOndGroupDTO.getReturnGroupId().intValue()))
								&& (targetOndGroupDTO.getOndGroupId().intValue() < ondGroupDTO.getOndGroupId().intValue()))) {

					Integer tempColSetPnrSegIds = mapPnrSegIdsAndFareIds.get(colSetPnrSegIds);
					Integer tempTargetColSetPnrSegIds = mapPnrSegIdsAndFareIds.get(targetColSetPnrSegIds);

					if (tempColSetPnrSegIds != null && tempTargetColSetPnrSegIds != null) {
						if (tempColSetPnrSegIds.intValue() == tempTargetColSetPnrSegIds.intValue()) {

							// This means going ond code is equal to the arrival ond code
							String ondCode = ondGroupDTO.getOndCode();
							String targetOndCode = getInverseOndCode(targetOndGroupDTO.getOndCode());

							if (getFirstAndLastOndCode(ondCode).equals(getFirstAndLastOndCode(targetOndCode))) {
								colInversePnrSegIds.addAll(targetColSetPnrSegIds);
								break;
							}
						}
					}
				}
			}
		}

		return colInversePnrSegIds;
	}

	private static String getFirstAndLastOndCode(String ondCode) {
		ondCode = BeanUtils.nullHandler(ondCode);

		if (ondCode.length() > 0) {
			String[] segments = ondCode.split("/");
			return segments[0] + "/" + segments[segments.length - 1];
		} else {
			return "";
		}
	}

	public static boolean isReturnSegmentExist(Collection<ReservationSegment> colReservationSegment) {
		ReservationSegment reservationSegment;

		for (ReservationSegment reservationSegment2 : colReservationSegment) {
			reservationSegment = reservationSegment2;

			if (ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(reservationSegment.getReturnFlag())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks the Segment compatibility
	 * 
	 * @param colOndFareDTOs
	 * @param colFrontEndFlgSegIds
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<OndFareDTO> checkSegmentsCompatibilityOnly(Collection<OndFareDTO> colOndFareDTOs,
			Collection<Integer> colFrontEndFlgSegIds) throws ModuleException {
		Iterator<OndFareDTO> itColOndFareDTO = colOndFareDTOs.iterator();
		OndFareDTO ondFareDTO;
		SegmentSeatDistsDTO segmentSeatDistsDTO;
		Iterator<SegmentSeatDistsDTO> itSegmentSeatDistsDTO;
		Collection<Integer> colInvFlgSegIds = new ArrayList<Integer>();

		while (itColOndFareDTO.hasNext()) {
			ondFareDTO = itColOndFareDTO.next();
			itSegmentSeatDistsDTO = ondFareDTO.getSegmentSeatDistsDTO().iterator();

			while (itSegmentSeatDistsDTO.hasNext()) {
				segmentSeatDistsDTO = itSegmentSeatDistsDTO.next();
				colInvFlgSegIds.add(new Integer(segmentSeatDistsDTO.getFlightSegId()));
			}
		}

		if (colFrontEndFlgSegIds.size() == colInvFlgSegIds.size()) {
			if (colFrontEndFlgSegIds.containsAll(colInvFlgSegIds)) {
				return colOndFareDTOs;
			}
		}

		// This means that the front end flight segment ids
		// does not match with the inventory flight segment ids
		log.error("airreservations.arg.InvalidFareInformation:" + colInvFlgSegIds.toString());
		throw new ModuleException("airreservations.arg.InvalidFareInformation");
	}

	/**
	 * Return segment sequence ordered group segments
	 * 
	 * @param colReservationSegmentDTO
	 */
	public static Map<Integer, Collection<ReservationSegmentDTO>>
			getSegSeqOrderedGrpSegments(Collection<ReservationSegmentDTO> colReservationSegmentDTO) {
		Map<Integer, Map<Integer, Collection<ReservationSegmentDTO>>> mapUnOrderedOndGrpSegments = new HashMap<Integer, Map<Integer, Collection<ReservationSegmentDTO>>>();
		ReservationSegmentDTO reservationSegmentDTO;
		Map<Integer, Collection<ReservationSegmentDTO>> mapSegSeqAndColSegments;
		Collection<ReservationSegmentDTO> colSegments;

		// Capturing the segment information in order to analyse
		for (ReservationSegmentDTO reservationSegmentDTO2 : colReservationSegmentDTO) {
			reservationSegmentDTO = reservationSegmentDTO2;

			if (mapUnOrderedOndGrpSegments.containsKey(reservationSegmentDTO.getFareGroupId())) {
				mapSegSeqAndColSegments = mapUnOrderedOndGrpSegments.get(reservationSegmentDTO.getFareGroupId());

				if (mapSegSeqAndColSegments.containsKey(reservationSegmentDTO.getSegmentSeq())) {
					colSegments = mapSegSeqAndColSegments.get(reservationSegmentDTO.getSegmentSeq());
					colSegments.add(reservationSegmentDTO);
				} else {
					colSegments = new ArrayList<ReservationSegmentDTO>();
					colSegments.add(reservationSegmentDTO);

					mapSegSeqAndColSegments.put(reservationSegmentDTO.getSegmentSeq(), colSegments);
				}
			} else {
				colSegments = new ArrayList<ReservationSegmentDTO>();
				colSegments.add(reservationSegmentDTO);

				mapSegSeqAndColSegments = new TreeMap<Integer, Collection<ReservationSegmentDTO>>();
				mapSegSeqAndColSegments.put(reservationSegmentDTO.getSegmentSeq(), colSegments);

				mapUnOrderedOndGrpSegments.put(reservationSegmentDTO.getFareGroupId(), mapSegSeqAndColSegments);
			}
		}

		// Getting the segment information organized for our purpose
		Map<Integer, Collection<ReservationSegmentDTO>> mapGrpSegments = new TreeMap<Integer, Collection<ReservationSegmentDTO>>();
		Integer ondGroupId;
		Collection<ReservationSegmentDTO> colSegSeqSegments;

		for (Integer integer : mapUnOrderedOndGrpSegments.keySet()) {
			ondGroupId = integer;
			mapSegSeqAndColSegments = mapUnOrderedOndGrpSegments.get(ondGroupId);
			colSegSeqSegments = new ArrayList<ReservationSegmentDTO>();

			for (Collection<ReservationSegmentDTO> collection : mapSegSeqAndColSegments.values()) {
				colSegments = collection;
				colSegSeqSegments.addAll(colSegments);
			}

			mapGrpSegments.put(ondGroupId, colSegSeqSegments);
		}

		return mapGrpSegments;
	}

	public static Map<Integer, Collection<Integer>> getOndGrpWiseSegments(Collection<ReservationSegment> colReservationSegment) {
		Map<Integer, Collection<Integer>> ondGrpWiseSegments = new HashMap<Integer, Collection<Integer>>();
		ReservationSegment reservationSegment;

		for (ReservationSegment reservationSegment2 : colReservationSegment) {
			reservationSegment = reservationSegment2;

			Collection<Integer> pnrSegIds = ondGrpWiseSegments.get(reservationSegment.getOndGroupId());

			if (pnrSegIds != null) {
				pnrSegIds.add(reservationSegment.getPnrSegId());
			} else {
				pnrSegIds = new HashSet<Integer>();
				pnrSegIds.add(reservationSegment.getPnrSegId());

				ondGrpWiseSegments.put(reservationSegment.getOndGroupId(), pnrSegIds);
			}
		}

		return ondGrpWiseSegments;
	}

	public static Collection<Integer> getPNRSegmentIds(Collection<ReservationSegment> pnrSegments) {
		Collection<Integer> pnrSegmentIds = null;

		if (pnrSegments != null) {
			pnrSegmentIds = new ArrayList<Integer>();

			for (ReservationSegment reservationSegment : pnrSegments) {
				pnrSegmentIds.add(reservationSegment.getPnrSegId());
			}

		}

		return pnrSegmentIds;
	}

	public static Collection<Integer> getConfirmedPnrSegIds(Collection<ReservationSegment> pnrSegments) {
		Collection<ReservationSegment> confirmedResSegments = new ArrayList<ReservationSegment>();

		if (pnrSegments != null && !pnrSegments.isEmpty()) {
			for (ReservationSegment pnrSegment : pnrSegments) {
				if (pnrSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					confirmedResSegments.add(pnrSegment);
				}
			}
		}
		return getPNRSegmentIds(confirmedResSegments);
	}

	public static List<Integer> getForcedConfirmedPaxSeqs(Reservation reservation) {
		List<Integer> paxSeqs = new ArrayList<Integer>();

		if (reservation.getStatus().equals(ReservationStatus.CONFIRMED) && reservation.getPassengers() != null) {
			boolean infantPaymentSeparated = reservation.isInfantPaymentRecordedWithInfant();
			for (ReservationPax resPax : reservation.getPassengers()) {
				if (resPax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1) {
					if (!isInfantType(resPax) || infantPaymentSeparated) {
						paxSeqs.add(resPax.getPaxSequence());
					}

					if (isParentAfterSave(resPax) && !infantPaymentSeparated) {
						paxSeqs.add(resPax.getAccompaniedSequence());
					}
				}
			}
		}
		return paxSeqs;
	}

	// Since Insurance are calculated for whole reservations there are no force confirm passengers when insurance are
	// added on anci modification flow
	public static List<Integer> getPaxSeqForInsuranceModify(Reservation reservation) {
		List<Integer> paxSeqs = new ArrayList<Integer>();

		if (reservation.getStatus().equals(ReservationStatus.CONFIRMED) && reservation.getPassengers() != null) {
			for (ReservationPax resPax : reservation.getPassengers()) {
				if (!isInfantType(resPax)) {
					paxSeqs.add(resPax.getPaxSequence());
					if (isParentAfterSave(resPax)) {
						paxSeqs.add(resPax.getAccompaniedSequence());
					}
				}
			}
		}
		return paxSeqs;
	}

	public static List<Integer> getPaxSeqs(Reservation reservation) {
		List<Integer> paxSeqs = new ArrayList<Integer>();

		if (reservation.getPassengers() != null) {
			for (ReservationPax resPax : reservation.getPassengers()) {
				paxSeqs.add(resPax.getPaxSequence());
			}
		}
		return paxSeqs;
	}

	public static List<Integer> getPaxSeqs(Collection<ReservationPax> passengers) {
		List<Integer> paxSeqs = new ArrayList<Integer>();

		if (passengers != null) {
			for (ReservationPax resPax : passengers) {
				paxSeqs.add(resPax.getPaxSequence());
			}
		}
		return paxSeqs;
	}

	public static Collection<ReservationPax> getRemainingPax(Reservation reservation, Collection<ReservationPax> passengers)
			throws ModuleException {
		if (passengers.isEmpty()) {
			return reservation.getPassengers();
		} else {
			Collection<ReservationPax> remainingPax = new ArrayList<ReservationPax>();
			List<Integer> paxSeqs = getPaxSeqs(passengers);
			Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
			ReservationPax reservationPax;
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				if (!paxSeqs.contains(reservationPax.getPaxSequence())) {
					remainingPax.add(reservationPax);
				}
			}
			return remainingPax;
		}
	}

	public static Collection<ReservationPax> getConfirmedPaxByOperationUsingForceCNFSeqs(
			List<Integer> forcedConfirmedPaxSeqsBeforeAdj, List<Integer> forcedConfirmedPaxSeqsAfterAdj,
			Reservation reservation) {
		Collection<ReservationPax> etGenerationEligiblePax = new ArrayList<ReservationPax>();
		for (Integer paxSeq : forcedConfirmedPaxSeqsBeforeAdj) {
			if (!forcedConfirmedPaxSeqsAfterAdj.contains(paxSeq)) {
				for (ReservationPax resPax : reservation.getPassengers()) {
					if (resPax.getPaxSequence().equals(paxSeq)) {
						etGenerationEligiblePax.add(resPax);
					}
				}
			}
		}
		return etGenerationEligiblePax;
	}

	public static Collection<ReservationPax> getConfirmedPaxByOperation(List<Integer> forcedConfirmedPaxSeqsBeforeAdj,
			List<Integer> confirmedPaxSeqsAfterAdj, Reservation reservation) {
		Collection<ReservationPax> etGenerationEligiblePax = new ArrayList<ReservationPax>();
		for (Integer paxSeq : forcedConfirmedPaxSeqsBeforeAdj) {
			if (confirmedPaxSeqsAfterAdj.contains(paxSeq)) {
				for (ReservationPax resPax : reservation.getPassengers()) {
					if (resPax.getPaxSequence().equals(paxSeq)) {
						etGenerationEligiblePax.add(resPax);
					}
				}
			}
		}
		return etGenerationEligiblePax;
	}

	public static Collection<Integer> getPNRPaxIds(Collection<ReservationPax> colReservationPax) {
		Collection<Integer> pnrPaxIds = null;

		if (colReservationPax != null) {
			pnrPaxIds = new ArrayList<Integer>();

			for (ReservationPax reservationSegment : colReservationPax) {
				pnrPaxIds.add(reservationSegment.getPnrPaxId());
			}

		}

		return pnrPaxIds;
	}

	public static Date getDepartureDate(String pnrNo) throws ModuleException {
		Date depDate = null;
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnrNo);
		pnrModesDTO.setLoadSegView(true);

		Reservation reservation = ReservationModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
		Collection<ReservationSegmentDTO> colSegments = reservation.getSegmentsView();

		for (ReservationSegmentDTO reservationSegmentDTO2 : colSegments) {
			ReservationSegmentDTO reservationSegmentDTO = reservationSegmentDTO2;

			if (depDate == null || depDate.before(reservationSegmentDTO.getZuluDepartureDate())) {
				depDate = reservationSegmentDTO.getZuluDepartureDate();
			}

		}

		return depDate;
	}

	public static Collection<FlightSegmentDTO> getFlightSegments(Reservation reservation) throws ModuleException {
		Collection<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
		Collection<ReservationSegmentDTO> segmentDTOs = reservation.getSegmentsView();

		for (ReservationSegmentDTO sementDTO : segmentDTOs) {
			FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();

			flightSegmentDTO.setSegmentCode(sementDTO.getSegmentCode());
			flightSegmentDTO.setDepartureDateTimeZulu(sementDTO.getZuluDepartureDate());
			flightSegmentDTO.setArrivalDateTimeZulu(sementDTO.getZuluArrivalDate());

			flightSegmentDTOs.add(flightSegmentDTO);
		}

		return flightSegmentDTOs;
	}

	public static String getOndCode(Reservation reservation) throws ModuleException {
		Collection<ReservationSegmentDTO> segmentDTOs = reservation.getSegmentsView();
		String ondCode = null;

		for (Iterator<ReservationSegmentDTO> itSegmentDTOs = segmentDTOs.iterator(); itSegmentDTOs.hasNext();) {
			ReservationSegmentDTO segmentDTO = itSegmentDTOs.next();
			String segCode = segmentDTO.getSegmentCode();

			if (itSegmentDTOs.hasNext()) {
				ReservationSegmentDTO nextSegmentDTO = itSegmentDTOs.next();
				String nextSegCode = nextSegmentDTO.getSegmentCode();

				if (ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE.equals(nextSegmentDTO.getReturnFlag())
						&& segCode.substring(4).equals(nextSegCode.subSequence(0, 3))) {
					ondCode = segCode.substring(0, 3) + "/" + nextSegCode.substring(4);
				} else {
					ondCode = segCode;
				}
			} else {
				ondCode = segCode;
			}
		}

		return ondCode;
	}

	public static Map<Integer, Integer> loadSegmentSeq(Reservation reservation) throws ModuleException {
		Collection<ReservationSegmentDTO> segmentDTOs = reservation.getSegmentsView();

		Map<Integer, Integer> segmentSeq = new HashMap<Integer, Integer>();
		for (ReservationSegmentDTO reservationSegmentDTO : segmentDTOs) {
			ReservationSegmentDTO segmentDTO = reservationSegmentDTO;
			segmentSeq.put(segmentDTO.getFlightSegId(), segmentDTO.getSegmentSeq());
		}

		return segmentSeq;
	}

	/**
	 * Returns the passenger name
	 * 
	 * @param paxTypeDescription
	 * @param title
	 * @param firstName
	 * @param lastName
	 * @param includePaxType
	 * @return
	 */
	public static String getPassengerName(String paxTypeDescription, String title, String firstName, String lastName,
			boolean includePaxType) {
		String passengerName = "";
		title = BeanUtils.nullHandler(title);

		if (includePaxType) {
			paxTypeDescription = paxTypeDescription + " ";
		} else {
			paxTypeDescription = "";
		}

		// Setting the passenger name
		if (title.equals("")) {
			passengerName = paxTypeDescription + BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(firstName)) + " "
					+ BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(lastName));
		} else {
			passengerName = paxTypeDescription + BeanUtils.makeFirstLetterCapital(title) + " "
					+ BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(firstName)) + " "
					+ BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(lastName));
		}

		return passengerName;
	}

	// added by Haider 4feb09
	// remove stop over seg for ex. SHJ/BAH/ALY will be SHJ/ALY
	public static String hideStopOverSeg(String strOndCode) {
		String newOnd = "";
		String[] ondArr = strOndCode.split("/");
		for (int i = 0; i < ondArr.length; i++) {
			if (ondArr.length > 2 && i > 0 && i < (ondArr.length - 1)) {
				continue;
			}
			if (i > 0) {
				newOnd += "/";
			}
			newOnd += ondArr[i];
		}
		return newOnd;
	}

	public static String getReservationPaxFareSegmentStatus(String status) {
		status = BeanUtils.nullHandler(status);

		if (ReservationInternalConstants.PaxFareSegmentTypes.FLOWN.equals(status)) {
			return "/FLOWN";
		} else if (ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE.equals(status)) {
			return "/NO SHOW";
		} else if (ReservationInternalConstants.PaxFareSegmentTypes.NO_REC.equals(status)) {
			return "/NO REC";
		} else if (ReservationInternalConstants.PaxFareSegmentTypes.GO_SHORE.equals(status)) {
			return "/GO SHOW";
		}

		return "";
	}

	/**
	 * Returns return ond ids for pnr segment ids
	 * 
	 * @param reservation
	 * @return
	 */
	private static Map<Collection<Integer>, Integer> getReturnOndGroupIdsForPnrSegIds(Reservation reservation) {
		Map<Collection<Integer>, Integer> mapPnrSegIdsAndReturnOndGroupIds = new HashMap<Collection<Integer>, Integer>();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		ReservationSegmentDTO segDTO;
		Collection<Integer> colSetPnrSegIds;

		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			reservationPax = reservationPax2;

			// If it's Parent of Adult
			if (!isInfantType(reservationPax)) {
				for (ReservationPaxFare reservationPaxFare2 : reservationPax.getPnrPaxFares()) {
					reservationPaxFare = reservationPaxFare2;

					colSetPnrSegIds = new HashSet<Integer>();

					for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
						reservationPaxFareSegment = reservationPaxFareSegment2;
						colSetPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
					}

					for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
						segDTO = reservationSegmentDTO;
						if (colSetPnrSegIds.contains(segDTO.getPnrSegId())) {
							mapPnrSegIdsAndReturnOndGroupIds.put(colSetPnrSegIds, segDTO.getReturnOndGroupId());
							break;
						}
					}

				}
				break;
			}
		}

		return mapPnrSegIdsAndReturnOndGroupIds;
	}

	/**
	 * Returns the flexibility information for audit
	 * 
	 * @param reservationFlexibilitiesMap
	 * @return
	 */
	public static String
			getFlexibilityInformation(Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> reservationFlexibilitiesMap) {
		StringBuffer flexibilities = new StringBuffer();

		if (reservationFlexibilitiesMap != null && reservationFlexibilitiesMap.keySet().size() > 0) {
			Integer key = reservationFlexibilitiesMap.keySet().iterator().next();
			Collection<ReservationPaxOndFlexibilityDTO> ondFlexibilities = reservationFlexibilitiesMap.get(key);
			if (ondFlexibilities != null) {
				Iterator<ReservationPaxOndFlexibilityDTO> itFlexi = ondFlexibilities.iterator();
				while (itFlexi.hasNext()) {
					ReservationPaxOndFlexibilityDTO flexibilityDTO = itFlexi.next();
					if (flexibilityDTO.getAvailableCount() > 0) {
						if (flexibilities.length() > 0) {
							flexibilities.append(",");
						}
						flexibilities.append(flexibilityDTO.getDescription()).append("-")
								.append(flexibilityDTO.getAvailableCount());
					}
				}
			}
		}

		return flexibilities.toString();
	}

	/**
	 * Returns return remaining flexibilities for pnr segment ids
	 * 
	 * @param reservation
	 *            , pnrSegmentIds
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static HashMap<String, ReservationPaxOndFlexibilityDTO> getRemainingFlexibilitiesForOnd(Reservation reservation,
			Collection<Integer> pnrSegmentIds) throws ModuleException {
		HashMap<String, ReservationPaxOndFlexibilityDTO> flexibilities = new HashMap<String, ReservationPaxOndFlexibilityDTO>();

		if (!ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())
				&& !ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())
				&& !ReservationApiUtils.isOpenReturnSegmentsExist(pnrSegmentIds, reservation)) {
			Map flexiruleFlexibilityTypeMap = AirpricingUtils.getAirpricingConfig().getFlexiruleFlexibilityTypeMap();
			Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> reservationFlexibilities = reservation
					.getPaxOndFlexibilities();
			Iterator<ReservationPax> iterResPassengers = reservation.getPassengers().iterator();
			ReservationPaxFare reservationPaxFareTarget = null;
			String flexibilityTypeID;

			if (pnrSegmentIds != null && !reservationFlexibilities.isEmpty()) {
				while (iterResPassengers.hasNext()) {
					ReservationPax reservationPax = iterResPassengers.next(); // need to check only for
																				// the first pax
					// TODO
					if (!ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType())) {
						reservationPaxFareTarget = getRelativePnrPaxFare(reservationPax.getPnrPaxFares(), pnrSegmentIds);
					}
					// above condition applied for avoid infants, because infant may not have all the pnr segements
					// which reservation exists.
					// ex : cancelled segments which were cancelled before add infant

					if (reservationPaxFareTarget != null) {
						Collection<ReservationPaxOndFlexibilityDTO> paxFareFlexibilites = reservationFlexibilities
								.get(reservationPaxFareTarget.getPnrPaxFareId());

						if (paxFareFlexibilites != null) {
							Iterator<ReservationPaxOndFlexibilityDTO> paxFareFlexiIter = paxFareFlexibilites.iterator();

							while (paxFareFlexiIter.hasNext()) {
								ReservationPaxOndFlexibilityDTO flexiChgDTO = paxFareFlexiIter.next();
								flexibilityTypeID = String.valueOf(flexiChgDTO.getFlexibilityTypeId());

								if (flexibilityTypeID
										.equals(BeanUtils.nullHandler(
												flexiruleFlexibilityTypeMap.get(FLEXI_RULE_FLEXIBILITY_TYPE.MODIFY_OND.name())))
										&& flexiChgDTO.getStatus().equals(ReservationPaxOndFlexibility.STATUS_ACTIVE)) {
									flexibilities.put(FLEXI_RULE_FLEXIBILITY_TYPE.MODIFY_OND.name(), flexiChgDTO);
								} else if (flexibilityTypeID
										.equals(BeanUtils.nullHandler(
												flexiruleFlexibilityTypeMap.get(FLEXI_RULE_FLEXIBILITY_TYPE.CANCEL_OND.name())))
										&& flexiChgDTO.getStatus().equals(ReservationPaxOndFlexibility.STATUS_ACTIVE)) {
									flexibilities.put(FLEXI_RULE_FLEXIBILITY_TYPE.CANCEL_OND.name(), flexiChgDTO);
								}
							}
							break; // get available balance flexibilities for first pax, will be same for all other pax
									// in reservation for the same ond
						}
					}
				}
			}
		}

		return flexibilities;
	}

	public static Object[] getFlexiModifyCutOverBuffer(Reservation reservation, Collection<Integer> pnrSegmentIds)
			throws ModuleException {
		Long cutOverBufferInMillis = new Long(0);
		Boolean modificationAvailable = new Boolean(false);
		ReservationPaxOndFlexibilityDTO flexiDTO = null;
		HashMap<String, ReservationPaxOndFlexibilityDTO> flexibilitiesMap = getRemainingFlexibilitiesForOnd(reservation,
				pnrSegmentIds);

		if (flexibilitiesMap != null && flexibilitiesMap.size() > 0) {
			if (flexibilitiesMap.containsKey(FLEXI_RULE_FLEXIBILITY_TYPE.MODIFY_OND.name())) {
				flexiDTO = flexibilitiesMap.get(FLEXI_RULE_FLEXIBILITY_TYPE.MODIFY_OND.name());
				modificationAvailable = new Boolean(true);
			} else if (flexibilitiesMap.containsKey(FLEXI_RULE_FLEXIBILITY_TYPE.CANCEL_OND.name())) {
				flexiDTO = flexibilitiesMap.get(FLEXI_RULE_FLEXIBILITY_TYPE.CANCEL_OND.name());
			}

			if (flexiDTO != null) {
				cutOverBufferInMillis = flexiDTO.getCutOverBufferInMins() * 60 * 1000;
			} else {
				cutOverBufferInMillis = new Long(-1);
			}
		} else {
			cutOverBufferInMillis = new Long(-1);
		}

		// Assumption that both cancel and modify cutover will be same, returning only one value
		return new Object[] { cutOverBufferInMillis, modificationAvailable };
	}

	private static String getCombinedOnd(Set<String> segmentCodes) {
		LinkedList<String> linkedSegments = new LinkedList<String>();
		for (String segmentCode : segmentCodes) {
			if (linkedSegments.size() == 0) {
				linkedSegments.add(segmentCode.split("/")[0]);
				linkedSegments.add(segmentCode.split("/")[1]);
			} else {
				String[] stations = segmentCode.split("/");
				if (linkedSegments.indexOf(stations[1]) != -1 && linkedSegments.indexOf(stations[0]) != -1) {
					continue;
				} else if (linkedSegments.indexOf(stations[0]) != -1) {
					if (linkedSegments.indexOf(stations[1]) == -1) {
						linkedSegments.add(linkedSegments.indexOf(stations[0]) + 1, stations[1]);
					}
				} else if (linkedSegments.indexOf(stations[1]) != -1) {
					if (linkedSegments.indexOf(stations[0]) == -1) {
						linkedSegments.add(linkedSegments.indexOf(stations[1]), stations[0]);
					}
				} else {
					linkedSegments.add(segmentCode.split("/")[0]);
					linkedSegments.add(segmentCode.split("/")[1]);
				}
			}
		}
		String ondCode = linkedSegments.getFirst();
		for (int i = 1; i < linkedSegments.size(); i++) {
			ondCode += "/" + linkedSegments.get(i);
		}
		return ondCode;
	}

	/**
	 * Returns the ReservationPaxFare instance which belongs to the pnr segment id(s) in ReservationPaxFareSegment
	 * 
	 * @param pnrPaxFares
	 * @param pnrSegmentIds
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationPaxFare getRelativePnrPaxFare(Set<ReservationPaxFare> pnrPaxFares,
			Collection<Integer> pnrSegmentIds) throws ModuleException {
		Iterator<ReservationPaxFare> itrPaxFare = pnrPaxFares.iterator();
		Iterator<ReservationPaxFareSegment> itrPaxFareSeg;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFare reservationPaxFareLocated = null;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> currentSegIds;

		while (itrPaxFare.hasNext()) {
			reservationPaxFare = itrPaxFare.next();
			itrPaxFareSeg = reservationPaxFare.getPaxFareSegments().iterator();

			currentSegIds = new ArrayList<Integer>();

			while (itrPaxFareSeg.hasNext()) {
				reservationPaxFareSegment = itrPaxFareSeg.next();

				if (reservationPaxFareSegment.getPnrSegId() != null) {
					currentSegIds.add(reservationPaxFareSegment.getPnrSegId());
				}
			}

			// If relative record is found.
			if (currentSegIds.containsAll(pnrSegmentIds)) {
				reservationPaxFareLocated = reservationPaxFare;
				break;
			}
		}

		// If this happens it means front end had sent invalid segment ids
		// so why not throw an exception to them :-)
		if (reservationPaxFareLocated == null) {
			throw new ModuleException("airreservations.arg.invalidOnd");
		}

		return reservationPaxFareLocated;
	}

	public static boolean isServiceAvailable(String caller, Date flightDeptTimeZulu) {
		long diffMils = 3600000;

		long lngBufferTime = AppSysParamsUtil.getInternationalBufferDurationInMillis();
		Calendar currentTime = Calendar.getInstance();

		if (caller.equals(ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP)) {
			diffMils = getTimeInMiliSeconds(globalConfig.getBizParam(SystemParamKeys.IBE_SEATMAP_CUTTOFF));
		} else if (caller.equals(ReservationInternalConstants.SERVICE_CALLER.MEAL)) {
			diffMils = getTimeInMiliSeconds(globalConfig.getBizParam(SystemParamKeys.MEAL_CUTOVERTIME));
		} else if (caller.equals(ReservationInternalConstants.SERVICE_CALLER.BAGGAGE)) {
			diffMils = getTimeInMiliSeconds(globalConfig.getBizParam(SystemParamKeys.BAGGAGE_CUTOVERTIME));
		} else if (caller.equals(ReservationInternalConstants.SERVICE_CALLER.AIRPORT_SERVICES)) {
			diffMils = getTimeInMiliSeconds(globalConfig.getBizParam(SystemParamKeys.SSR_SELECTION_CUTOVER_TIME));
		} else if (caller.equals(ReservationInternalConstants.SERVICE_CALLER.INSURANCE)) {
			diffMils = lngBufferTime;
		}

		if (flightDeptTimeZulu.getTime() - diffMils < currentTime.getTimeInMillis()) {
			return false;
		} else {
			return true;
		}
	}

	private static Integer getTimeInMiliSeconds(String cutOver) {
		Integer diffMils = null;
		String strHrs = null;
		String strMins = "0";

		if (cutOver.indexOf(":") > 0) {
			strHrs = cutOver.substring(0, cutOver.indexOf(":"));
			if (cutOver.indexOf(":") + 1 > 0) {
				strMins = cutOver.substring(cutOver.indexOf(":") + 1);
			}
		} else {
			strHrs = cutOver;
		}

		diffMils = (Integer.parseInt(strHrs) * 60 + Integer.parseInt(strMins)) * 60 * 1000;
		return diffMils;
	}

	public static String checkSeatAvalibilityForFlightSeg(Integer flightSegmentID, Date flightDeptTimeZulu)
			throws ModuleException {
		SeatMapBD seatMapBD = ReservationModuleUtils.getSeatMapBD();
		String seatAvailable = null;
		Collection<SeatDTO> seatCollection = null;
		Map<Integer, FlightSeatsDTO> availableSeatMap = new HashMap<Integer, FlightSeatsDTO>();
		List<Integer> flightSegIds = new ArrayList<Integer>();
		flightSegIds.add(flightSegmentID);

		availableSeatMap = seatMapBD.getFlightSeats(flightSegIds, null, false);
		FlightSeatsDTO flightSeatsDTO = availableSeatMap.get(flightSegmentID);
		if (flightSeatsDTO != null) {
			seatCollection = flightSeatsDTO.getSeats();
		}

		if (seatCollection != null && seatCollection.size() > 0) {
			seatAvailable = isServiceAvailable(ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP, flightDeptTimeZulu)
					? "Y"
					: null;
		}

		return seatAvailable;
	}

	public static String checkBaggageAvalibilityForFlightSeg(Integer flightSegmentID, Date flightDeptTimeZulu)
			throws ModuleException {
		BaggageBusinessDelegate baggageBD = ReservationModuleUtils.getBaggageBD();
		String baggageAvailable = null;
		Collection<FlightBaggageDTO> baggageCollection = null;
		Map<Integer, List<FlightBaggageDTO>> availableBaggageMap = new HashMap<Integer, List<FlightBaggageDTO>>();

		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		flightSegIdWiseCos.put(flightSegmentID, null);

		availableBaggageMap = baggageBD.getBaggages(flightSegIdWiseCos, false, false);
		baggageCollection = availableBaggageMap.get(flightSegmentID);

		if (baggageCollection != null && baggageCollection.size() > 0) {
			baggageAvailable = isServiceAvailable(ReservationInternalConstants.SERVICE_CALLER.BAGGAGE, flightDeptTimeZulu)
					? "Y"
					: null;
		}

		return baggageAvailable;
	}

	// private static BigDecimal[] getSplittedArray(Double amount, int number) {
	// BigDecimal[] splittedArray = new BigDecimal[number];
	// BigDecimal firstAmount = AccelAeroCalculator.divide(new BigDecimal(amount), new BigDecimal(number));
	// BigDecimal lastAmount = AccelAeroCalculator.subtract(new BigDecimal(amount),
	// AccelAeroCalculator.multiply(firstAmount, new BigDecimal(number - 1)));
	// for (int i = 0; i < number - 1; i++) {
	// splittedArray[i] = firstAmount;
	// }
	// splittedArray[number - 1] = lastAmount;
	// return splittedArray;
	// }

	public static String getFlexiInfoForAudit(Collection<FlexiExternalChgDTO> flexiCharges, Reservation reservation,
			Collection<Integer> newSegmentSeqNos) throws ModuleException {
		String ondWiseFlexi = "";
		// Ond group wise flight segments
		Map<Integer, Collection<Integer>> ondFlightSegMap = new HashMap<Integer, Collection<Integer>>();
		boolean includeSegmentForAudit = true;
		for (ReservationSegment reservationSegment : reservation.getSegments()) {
			// audit to be done only for new segments added in case of Add Segment
			includeSegmentForAudit = (newSegmentSeqNos != null)
					? newSegmentSeqNos.contains(reservationSegment.getSegmentSeq())
					: true;

			if (includeSegmentForAudit) {
				if (ondFlightSegMap.get(reservationSegment.getOndGroupId()) == null) {
					Collection<Integer> flightSegIds = new ArrayList<Integer>();
					flightSegIds.add(reservationSegment.getFlightSegId());
					ondFlightSegMap.put(reservationSegment.getOndGroupId(), flightSegIds);
				} else {
					Collection<Integer> flightSegIds = ondFlightSegMap.get(reservationSegment.getOndGroupId());
					flightSegIds.add(reservationSegment.getFlightSegId());
				}
			}
		}

		Map<Integer, String> flightSegmentFlexies = new HashMap<Integer, String>();
		for (FlexiExternalChgDTO flexiExternalChgDTO : flexiCharges) {
			String flexiMap = "";
			if (flightSegmentFlexies.get(flexiExternalChgDTO.getFlightSegId()) == null) {
				for (ReservationPaxOndFlexibilityDTO ondFlexibilityDTO : flexiExternalChgDTO
						.getReservationPaxOndFlexibilityDTOs()) {
					flexiMap += ondFlexibilityDTO.getAvailableCount() + " - " + ondFlexibilityDTO.getDescription() + " ";
				}
				flightSegmentFlexies.put(flexiExternalChgDTO.getFlightSegId(), flexiMap);
			}
		}

		for (Integer ondGroupId : ondFlightSegMap.keySet()) {
			Collection<Integer> flightSegIds = ondFlightSegMap.get(ondGroupId);
			Collection<FlightSegmentDTO> flightSegments = ReservationModuleUtils.getFlightBD().getFlightSegments(flightSegIds);
			Integer flightSegId = null;
			Set<String> segmentCodes = new HashSet<String>();
			for (FlightSegmentDTO flightSegmentDTO : flightSegments) {
				segmentCodes.add(flightSegmentDTO.getSegmentCode());
				flightSegId = flightSegmentDTO.getSegmentId();
			}
			if (flightSegmentFlexies.get(flightSegId) != null) {
				ondWiseFlexi += getCombinedOnd(segmentCodes) + " - " + flightSegmentFlexies.get(flightSegId) + " ";
			}
		}

		return ondWiseFlexi;
	}

	private static boolean isOpenReturnSegmentsExist(Collection<Integer> modifiedPnrSegIds, Reservation reservation)
			throws ModuleException {

		if (modifiedPnrSegIds == null || modifiedPnrSegIds.size() == 0) {
			return false;
		}

		Collection<String> colBookingTypes = new HashSet<String>();
		ReservationSegmentDTO reservationSegmentDTO;
		if (reservation.getSegmentsView() != null) {
			for (ReservationSegmentDTO reservationSegmentDTO2 : reservation.getSegmentsView()) {
				reservationSegmentDTO = reservationSegmentDTO2;

				if (modifiedPnrSegIds.contains(reservationSegmentDTO.getPnrSegId())
						&& reservationSegmentDTO.isOpenReturnSegment()) {
					colBookingTypes.add(reservationSegmentDTO.getBookingType());
				}
			}
		}

		if (colBookingTypes.size() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public static void setHalfReturnSearchCriteria(ReservationPaxFareSegment modifiedPaxFareSegment, FareTO fareTO,
			Integer modifiedReturnOndGrpId, AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {

		Integer modifiedOndGrpId = modifiedPaxFareSegment.getSegment().getOndGroupId();
		Collection<ReservationSegmentDTO> reservationSegments = modifiedPaxFareSegment.getReservationPaxFare().getReservationPax()
				.getReservation().getSegmentsView();
		boolean isInboundSegmentModified = "Y".equals(modifiedPaxFareSegment.getSegment().getReturnFlag()) ? true : false;

		List<FlightSegmentDTO> colInverseFlightSegments = new ArrayList<FlightSegmentDTO>();
		String inverseOndCode = "";

		for (ReservationSegmentDTO reservationSegmentDTO : reservationSegments) {
			ReservationSegmentDTO resDTO = reservationSegmentDTO;
			if ((resDTO.getReturnOndGroupId() != null && resDTO.getReturnOndGroupId().equals(modifiedReturnOndGrpId))) {
				if (!resDTO.getFareGroupId().equals(modifiedOndGrpId)
						&& !resDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					// inverse ond which is having same Return Group with a return fare

					FlightSegmentDTO fSeg;
					Collection<FlightSegmentDTO> existingFlightSegments = availableFlightSearchDTO.getExistingFlightSegments();
					for (FlightSegmentDTO flightSegmentDTO : existingFlightSegments) {
						fSeg = flightSegmentDTO;
						if (resDTO.getFlightSegId().equals(fSeg.getSegmentId())) {
							colInverseFlightSegments.add(fSeg);
							inverseOndCode = inverseOndCode + (inverseOndCode.equals("")
									? fSeg.getSegmentCode()
									: fSeg.getSegmentCode().substring(fSeg.getSegmentCode().indexOf("/")));
							break;
						}

					}
				}
			}
		}
		if (colInverseFlightSegments.size() > 0) { // Inverse Segment exist for modified segment
			Date departureDate = null;
			Date returnDate = null;
			if (isInboundSegmentModified) {
				departureDate = (colInverseFlightSegments.get(0)).getDepartureDateTime(); // dep.
																							// date
																							// of
																							// first
																							// inverse
																							// seg
																							// in
																							// case
																							// of
																							// connection
																							// flight
				returnDate = availableFlightSearchDTO.getDepatureDateTimeEnd(); // till end of day
			} else {
				departureDate = availableFlightSearchDTO.getDepatureDateTimeStart(); // from start of day
				returnDate = (colInverseFlightSegments.get(0)).getDepartureDateTime();
			}
			if (departureDate.compareTo(returnDate) < 0) { // Half return fare applicable only if the modified booking
															// is similar to existing return booking with change in
															// dates only
				availableFlightSearchDTO.setInverseSegmentsOfModifiedFlightSegment(colInverseFlightSegments);
				availableFlightSearchDTO.setHalfReturnFareQuote(true);
				availableFlightSearchDTO.setModifiedOndReturnGroupId(modifiedReturnOndGrpId);
				availableFlightSearchDTO.setInboundFareQuote(isInboundSegmentModified);
				availableFlightSearchDTO.setInverseOndCode(inverseOndCode);
			}
		}
	}

	/**
	 * Collect the ground segment IDs <groundSegment> linked to a <ReservationSegment>
	 * 
	 * @param colReservationSegments
	 * @return
	 */
	public static Map<Integer, Integer> getLinkedGroundSegments(Set<ReservationSegment> colReservationSegments) {
		Map<Integer, Integer> mapResSegment = new HashMap<Integer, Integer>();
		for (ReservationSegment reservationSegment : colReservationSegments) {
			if (reservationSegment.getGroundSegment() != null && !ReservationInternalConstants.ReservationSegmentStatus.CANCEL
					.equals(reservationSegment.getGroundSegment().getStatus())) {
				mapResSegment.put(reservationSegment.getPnrSegId(), reservationSegment.getGroundSegment().getPnrSegId());
			}
		}
		return mapResSegment;
	}

	public static boolean isOtherCarrierPaymentsExist(CommonReservationAssembler commonReservationAssembler) {
		boolean otherCarrierPayments = false;
		OTHER_CARRIER_PAYMENTS: for (LCCClientReservationPax lccClientReservationPax : commonReservationAssembler
				.getLccreservation().getPassengers()) {
			if (!lccClientReservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
				for (LCCClientPaymentInfo lccClientPaymentInfo : lccClientReservationPax.getLccClientPaymentAssembler()
						.getPayments()) {
					if (!lccClientPaymentInfo.getCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
						otherCarrierPayments = true;
						break OTHER_CARRIER_PAYMENTS;
					}
				}
			}
		}
		return otherCarrierPayments;
	}

	public static boolean isOtherCarrierPaymentsExist(Collection<LCCClientPaymentAssembler> lccClientPaymentAssemblers) {
		boolean otherCarrierPayments = false;
		OTHER_CARRIER_PAYMENTS: for (LCCClientPaymentAssembler lccClientPaymentAssembler : lccClientPaymentAssemblers) {
			for (LCCClientPaymentInfo lccClientPaymentInfo : lccClientPaymentAssembler.getPayments()) {
				if (!lccClientPaymentInfo.getCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					otherCarrierPayments = true;
					break OTHER_CARRIER_PAYMENTS;
				}
			}
		}
		return otherCarrierPayments;
	}

	/**
	 * Returns caller credentials information
	 * 
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public static CredentialsDTO getCallerCredentials(TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal)
			throws ModuleException {
		if (userPrincipal == null || userPrincipal.getName() == null) {
			throw new ModuleException("module.credential.error");
		}

		CredentialsDTO credentialsDTO = new CredentialsDTO();
		credentialsDTO.setTrackInfoDTO(trackInfoDTO);
		if (trackInfoDTO != null && trackInfoDTO.getOriginUserId() != null && trackInfoDTO.getOriginAgentCode() != null
				&& trackInfoDTO.getOriginChannelId() != null) {
			// In here, we set only the userID, agentCode and sales channel. We didn't override user principal.
			credentialsDTO.setUserId(trackInfoDTO.getOriginUserId());
			credentialsDTO.setAgentCode(trackInfoDTO.getOriginAgentCode());
			credentialsDTO.setSalesChannelCode(trackInfoDTO.getOriginChannelId());
		} else {
			credentialsDTO.setUserId(userPrincipal.getUserId());
			credentialsDTO.setAgentCode(userPrincipal.getAgentCode());
			credentialsDTO.setSalesChannelCode(new Integer(userPrincipal.getSalesChannel()));
		}

		if (trackInfoDTO != null) {
			credentialsDTO.setDirectBillId(trackInfoDTO.getDirectBillId());
			
			credentialsDTO.setMarketingBookingChannel(trackInfoDTO.getMarketingBookingChannel() != 0 ? 
					  trackInfoDTO.getMarketingBookingChannel() : 
					  credentialsDTO.getSalesChannelCode());
		}

		credentialsDTO.setPassword(userPrincipal.getPassword());
		credentialsDTO.setCustomerId(userPrincipal.getCustomerId());
		credentialsDTO.setAgentCurrencyCode(userPrincipal.getAgentCurrencyCode());
		credentialsDTO.setAgentStation(userPrincipal.getAgentStation());
		credentialsDTO.setColUserDST(userPrincipal.getColUserDST());
		credentialsDTO.setDefaultCarrierCode(userPrincipal.getDefaultCarrierCode());
		if (userPrincipal.getUserId() != null) {
			credentialsDTO.setDisplayName(
					ReservationModuleUtils.getSecurityBD().getUserBasicDetails(userPrincipal.getUserId()).getDisplayName());
		}
		return credentialsDTO;
	}

	public static String getDisplayName(CredentialsDTO credentialsDTO) {
		if (credentialsDTO.getDisplayName() == null && credentialsDTO.getUserId() != null) {
			try {
				credentialsDTO.setDisplayName(
						ReservationModuleUtils.getSecurityBD().getUserBasicDetails(credentialsDTO.getUserId()).getDisplayName());
			} catch (ModuleException e) {
				log.error("Error fetching user display name", e);
			}
		}
		return credentialsDTO.getDisplayName();
	}

	/**
	 * Gets the set of flight segments for a given set of fareDTOs.
	 * 
	 * @param fareDTOs
	 * 
	 * @return
	 */
	public static Set<FlightSegmentDTO> getFlightSegmentsForOnDFare(Collection<OndFareDTO> fareDTOs) {
		Set<FlightSegmentDTO> flightSegmentSet = new HashSet<FlightSegmentDTO>();
		if (fareDTOs != null) {
			for (OndFareDTO fareDTO : fareDTOs) {
				if (fareDTO.getSegmentsMap() != null) {
					flightSegmentSet.addAll(fareDTO.getSegmentsMap().values());
				}
			}
		}
		return flightSegmentSet;
	}

	/**
	 * Returns the segment information formatted in a single string for presentation purposes in the reservation's audit
	 * log.
	 * 
	 * @param flightSegmentDTOs
	 * @param openRTFltSegIds
	 *            TODO
	 * @return : The formatted flight segment string.
	 */
	@SuppressWarnings("unchecked")
	public static String getSegmentInformationForReservation(List<FlightSegmentDTO> flightSegmentDTOs,
			List<Integer> openRTFltSegIds) {
		StringBuffer segmentInformation = new StringBuffer();
		if (flightSegmentDTOs != null && flightSegmentDTOs.size() > 0) {
			Collections.sort(flightSegmentDTOs);

			for (FlightSegmentDTO flightSegment : flightSegmentDTOs) {
				if (StringUtils.isNotEmpty(segmentInformation.toString())) {
					segmentInformation.append(", ");
				}
				if (openRTFltSegIds != null && openRTFltSegIds.contains(flightSegment.getSegmentId())) {
					segmentInformation.append("OPENRT ");
					segmentInformation.append("-");
					segmentInformation.append(flightSegment.getSegmentCode());
				} else {
					segmentInformation.append(flightSegment.getFlightNumber());
					segmentInformation.append("-");
					segmentInformation.append(flightSegment.getSegmentCode());
					segmentInformation.append("-");
					SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
					segmentInformation.append(df.format(flightSegment.getDepartureDateTime()));
				}
			}
		}
		return segmentInformation.toString();
	}

	public static ResOnholdValidationDTO getOnHoldValidationDTO(Collection<FlightSegmentDTO> flightSegmentDTOs,
			List<ReservationPax> paxList, String contactEmail, int validationType, String ipAddress, int salesChannel) {
		ResOnholdValidationDTO onholdValidationDTO = new ResOnholdValidationDTO();
		onholdValidationDTO.setIpAddress(ipAddress);
		onholdValidationDTO.setValidationType(validationType);
		onholdValidationDTO.setPaxList(paxList);
		onholdValidationDTO.setContactEmail(contactEmail);
		onholdValidationDTO.setSalesChannel(salesChannel);

		for (FlightSegmentDTO flightSegDTO : (List<FlightSegmentDTO>) flightSegmentDTOs) {
			onholdValidationDTO.addFlightSegId(flightSegDTO.getSegmentId());
			onholdValidationDTO.addDepartureDate(flightSegDTO.getDepartureDateTimeZulu());
		}

		return onholdValidationDTO;
	}

	public static PayCurrencyDTO getPayCurrencyDTOFromBaseCurrency(String baseCurrency) throws ModuleException {
		CurrencyExchangeRate exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
				.getCurrencyExchangeRate(baseCurrency);
		Currency currency = exchangeRate.getCurrency();
		return new PayCurrencyDTO(currency.getCurrencyCode(), exchangeRate.getExrateBaseToCurNumber(), currency.getBoundryValue(),
				currency.getBreakPoint());
	}

	public static int getCreditCardTypeId(Integer id) {
		int cardTypeId = 0;
		if (id != null) {
			switch (id) {
			case 1:
				cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.MASTER;
				break;
			case 2:
				cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.VISA;
				break;
			case 3:
				cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.AMEX;
				break;
			case 4:
				cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.DINERS;
				break;
			case 5:
				cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.GENERIC;
				break;
			case 6:
				cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.CMI;
				break;
			default:
				cardTypeId = 0;
			}
		}
		return cardTypeId;
	}

	public static String getSelectedCabinClass(Collection<ReservationSegment> resSegments, Integer flightSegId)
			throws ModuleException {

		if (resSegments != null) {
			for (ReservationSegment reservationSegment : resSegments) {
				if (reservationSegment.getFlightSegId() != null && flightSegId != null
						&& reservationSegment.getFlightSegId().intValue() == flightSegId.intValue()
						&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {
					return reservationSegment.getCabinClassCode();
				}
			}
		}

		return null;
	}

	public static String getSelectedLogicalCabinClass(Collection<ReservationSegment> resSegments, Integer flightSegId)
			throws ModuleException {

		if (resSegments != null) {
			for (ReservationSegment reservationSegment : resSegments) {
				if (reservationSegment.getFlightSegId() != null && flightSegId != null
						&& reservationSegment.getFlightSegId().intValue() == flightSegId.intValue()
						&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {
					return reservationSegment.getLogicalCCCode();
				}
			}
		}

		return null;
	}

	/**
	 * @param reservation
	 * @param unflownPnrSegmentIds
	 * @return
	 */
	public static Collection<ReservationSegment> getUnflownSegments(Reservation reservation,
			Collection<Integer> unflownPnrSegmentIds) {
		Collection<ReservationSegment> unflownSegements = new ArrayList<ReservationSegment>();

		Iterator<Integer> segmentIdIterator = unflownPnrSegmentIds.iterator();

		while (segmentIdIterator.hasNext()) {
			int segmentId = segmentIdIterator.next();
			Iterator<ReservationSegment> iteSegments = reservation.getSegments().iterator();
			while (iteSegments.hasNext()) {
				ReservationSegment resSegment = iteSegments.next();
				if (segmentId == resSegment.getPnrSegId()) {
					unflownSegements.add(resSegment);
					break;
				}
			}
		}
		return unflownSegements;
	}

	/**
	 * @param unflownSegmentList
	 */
	public static void removeBusSegments(List<ReservationSegment> unflownSegmentList) {

		List<ReservationSegment> busSegments = new ArrayList<ReservationSegment>();

		for (ReservationSegment segment : unflownSegmentList) {
			if (segment.getSubStationShortName() != null && !segment.getSubStationShortName().isEmpty()) {
				busSegments.add(segment);
			}
		}
		unflownSegmentList.removeAll(busSegments);
	}

	/**
	 * @param colPaymentInfo
	 * @return
	 */
	public static boolean isBSPpayment(Collection<PaymentInfo> colPaymentInfo) {

		if (colPaymentInfo != null) {
			Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();

			while (itColPaymentInfo.hasNext()) {
				PaymentInfo paymentInfo = itColPaymentInfo.next();
				if (paymentInfo instanceof AgentCreditInfo) {
					AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
					if (agentCreditInfo.getPaymentReferenceTO() != null
							&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType() != null
							&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Returns Pnr Segment Ids and Inverse Pnr Segment Ids Map
	 * 
	 * @param mapSetPSegIdsAndSetInvPSegIds
	 * @param pnrSegIds
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Integer> getInversePnrSegIds(
			Map<Collection<Integer>, Collection<Integer>> mapSetPSegIdsAndSetInvPSegIds, Collection<Integer> pnrSegIds)
			throws ModuleException {
		if (pnrSegIds.size() > 0) {
			Collection<Integer> colSetPnrSegIds = new HashSet<Integer>();
			colSetPnrSegIds.addAll(pnrSegIds);

			if (mapSetPSegIdsAndSetInvPSegIds.containsKey(colSetPnrSegIds)) {
				return mapSetPSegIdsAndSetInvPSegIds.get(colSetPnrSegIds);
			} else {
				throw new ModuleException("airreservations.arg.invalidOnd");
			}
		} else {
			return new ArrayList<Integer>();
		}
	}

	public static boolean isTriggerForceConfirm(Integer paymentTypes) {
		boolean status = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(paymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
						.equals(paymentTypes);

		return status;

	}

	public static Map<Long, List<ReservationPaxOndPayment>>
			getTransactionWisePaymentMap(List<ReservationPaxOndPayment> paymentList) {

		Map<Long, List<ReservationPaxOndPayment>> txnWisePaymentList = new HashMap<Long, List<ReservationPaxOndPayment>>();

		for (ReservationPaxOndPayment payment : paymentList) {
			if (txnWisePaymentList.get(payment.getPaymentTnxId()) == null) {
				txnWisePaymentList.put(payment.getPaymentTnxId(), new ArrayList<ReservationPaxOndPayment>());
			}
			txnWisePaymentList.get(payment.getPaymentTnxId()).add(payment);
		}

		return txnWisePaymentList;
	}

	public static BigDecimal getPaymentListTotal(List<ReservationPaxOndPayment> paymentList) {

		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (paymentList != null) {
			for (ReservationPaxOndPayment payment : paymentList) {
				totalAmount = AccelAeroCalculator.add(totalAmount, payment.getAmount());
			}
		}

		return totalAmount;
	}

	public static BigDecimal getTxnWisePaymentMapTotal(Map<Long, List<ReservationPaxOndPayment>> txnWisePaymentList) {

		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (Entry<Long, List<ReservationPaxOndPayment>> entry : txnWisePaymentList.entrySet()) {
			BigDecimal paymentListTotal = getPaymentListTotal(entry.getValue());
			totalAmount = AccelAeroCalculator.add(totalAmount, paymentListTotal);
		}

		return totalAmount;
	}

	public static long getTotalStayOverMins(Date firstDepDate, long stayOverMonths, long stayOverMins) {

		long totalStayOverMins = 0l;

		if (firstDepDate != null && (stayOverMonths > 0 || stayOverMins > 0)) {
			totalStayOverMins = CalendarUtil.getTimeDifferenceInMinutes(firstDepDate,
					CalendarUtil.add(firstDepDate, 0, (int) stayOverMonths, 0, 0, (int) stayOverMins, 0));
		}

		return totalStayOverMins;
	}

	public static Date addMinimumMinsToFirstDepDate(List<Long> ticketValidityColl, List<Date> segmentDepDateColl) {
		if (ticketValidityColl != null && segmentDepDateColl != null && ticketValidityColl.size() > 0
				&& segmentDepDateColl.size() > 0) {
			Collections.sort(ticketValidityColl);
			Collections.sort(segmentDepDateColl);

			Date firstDepDate = segmentDepDateColl.get(0);
			// Date currentDate = new Date();

			Long ticketValidityInMins = ticketValidityColl.get(0);

			return CalendarUtil.addMinutes(firstDepDate, ticketValidityInMins.intValue());
		}
		return null;

	}

	/**
	 * 
	 * @param colOndFareDTOs
	 * @param mostRestMinStayOver
	 * @param mostRestMaxStayOver
	 * @param extFirstDeptDate
	 * @param validityFltSegIdColl
	 *            TODO
	 * @return [0] --> Max/Ticket Validity Till , [1] --> min Stay over From
	 */
	public static Date[] calculateValidities(Collection<OndFareDTO> colOndFareDTOs, Long mostRestMinStayOver,
			Long mostRestMaxStayOver, Date extFirstDeptDate, Collection<Integer> validityFltSegIdColl) {
		Date arr[] = new Date[2];
		if (colOndFareDTOs != null) {

			if (validityFltSegIdColl == null) {
				validityFltSegIdColl = new ArrayList<Integer>();
			}

			Iterator<OndFareDTO> ondFareDTOItr = colOndFareDTOs.iterator();

			List<Long> ticketValidityColl = new ArrayList<Long>();

			List<Long> miniStayOverColl = new ArrayList<Long>();

			List<Date> segmentDepDateColl = new ArrayList<Date>();

			boolean checkTicketValidity = false;

			if (mostRestMinStayOver != null) {
				miniStayOverColl.add(mostRestMinStayOver);
			}
			if (mostRestMaxStayOver != null) {
				ticketValidityColl.add(mostRestMaxStayOver);
			}
			if (extFirstDeptDate != null) {
				segmentDepDateColl.add(extFirstDeptDate);
			}

			while (ondFareDTOItr.hasNext()) {
				OndFareDTO ondFareDTO = ondFareDTOItr.next();
				FareSummaryDTO fareSummaryDTO = ondFareDTO.getFareSummaryDTO();

				// long minStayOverMins =
				// ReservationApiUtils.getTotalStayOverMins(fareSummaryDTO.getMinimumStayOverInMonths(),
				// fareSummaryDTO.getMinimumStayOverInMins());

				LinkedHashMap<Integer, FlightSegmentDTO> segmentMap = ondFareDTO.getSegmentsMap();

				int fareType = ondFareDTO.getFareType();

				if (fareType != FareTypes.RETURN_FARE && fareType != FareTypes.HALF_RETURN_FARE) {
					if (AppSysParamsUtil.isTicketValidityForSubJourney()) {
						continue;
					} else {
						checkTicketValidity = false;
						break;
					}
				}

				for (Integer segmentId : segmentMap.keySet()) {
					FlightSegmentDTO flightSegmentDTO = segmentMap.get(segmentId);
					segmentDepDateColl.add(flightSegmentDTO.getArrivalDateTimeZulu());
					validityFltSegIdColl.add(segmentId);
				}

				Collections.sort(segmentDepDateColl);

				long maxStayOverMins = ReservationApiUtils.getTotalStayOverMins(segmentDepDateColl.get(0),
						fareSummaryDTO.getMaximumStayOverInMonths(), fareSummaryDTO.getMaximumStayOverInMins());

				if (maxStayOverMins > 0)
					ticketValidityColl.add(maxStayOverMins);

				checkTicketValidity = true;

			}
			Date ticketValidity = null;
			Date minimumStayOver = null;
			if (checkTicketValidity) {
				if (ticketValidityColl != null && ticketValidityColl.size() > 0)
					ticketValidity = ReservationApiUtils.addMinimumMinsToFirstDepDate(ticketValidityColl, segmentDepDateColl);

				if (miniStayOverColl != null && miniStayOverColl.size() > 0)
					minimumStayOver = ReservationApiUtils.addMinimumMinsToFirstDepDate(miniStayOverColl, segmentDepDateColl);
			}
			arr[0] = ticketValidity;
			arr[1] = minimumStayOver;
		}
		return arr;
	}

	public static boolean checkAllPaxNoShowInTheSegment(Collection<ReservationPax> colReservationPax, Integer pnrSegId) {
		int i = 0;
		for (ReservationPax reservationPax : colReservationPax) {
			if (hasNoSHOWPax(reservationPax, pnrSegId)) {
				i++;
			}
		}

		if (i == colReservationPax.size()) {
			return true;
		} else {
			return false;
		}
	}

	public static List<ExternalChgDTO> populateExternalCharges(List<LCCClientExternalChgDTO> lccExternalChgList) {
		List<ExternalChgDTO> externalChgDTOs = new ArrayList<ExternalChgDTO>();

		Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();

		for (LCCClientExternalChgDTO paxExtChgDTO : lccExternalChgList) {
			ExternalChgDTO externalChgDTO = null;
			EXTERNAL_CHARGES extChg = paxExtChgDTO.getExternalCharges();

			switch (extChg) {
			case SEAT_MAP:
				externalChgDTO = new SMExternalChgDTO();
				((SMExternalChgDTO) externalChgDTO).setSeatNumber(paxExtChgDTO.getCode());
				break;
			case MEAL:
				externalChgDTO = new MealExternalChgDTO();
				((MealExternalChgDTO) externalChgDTO).setMealCode(paxExtChgDTO.getCode());
				break;
			case AIRPORT_TRANSFER:
				externalChgDTO = new AirportTransferExternalChgDTO();
				((AirportTransferExternalChgDTO) externalChgDTO).setTransferAddress(paxExtChgDTO.getTransferAddress());
				((AirportTransferExternalChgDTO) externalChgDTO).setFlightRPH(paxExtChgDTO.getFlightRefNumber());
				((AirportTransferExternalChgDTO) externalChgDTO).setTransferContact(paxExtChgDTO.getTransferContact());
				((AirportTransferExternalChgDTO) externalChgDTO).setTransferDate(paxExtChgDTO.getTransferDate());
				((AirportTransferExternalChgDTO) externalChgDTO).setTransferType(paxExtChgDTO.getTransferType());
				((AirportTransferExternalChgDTO) externalChgDTO).setAirportTransferId(paxExtChgDTO.getOfferedAnciTemplateID());
				((AirportTransferExternalChgDTO) externalChgDTO).setAirportCode(paxExtChgDTO.getAirportCode());
				((AirportTransferExternalChgDTO) externalChgDTO).setTransferType(paxExtChgDTO.getTransferType());
				break;
			case AIRPORT_SERVICE:
			case INFLIGHT_SERVICES:
				externalChgDTO = new SSRExternalChargeDTO();
				((SSRExternalChargeDTO) externalChgDTO).setAirportCode(paxExtChgDTO.getAirportCode());
				((SSRExternalChargeDTO) externalChgDTO).setFlightRPH(paxExtChgDTO.getFlightRefNumber());
				((SSRExternalChargeDTO) externalChgDTO).setSegmentSequece(paxExtChgDTO.getSegmentSequence());
				((SSRExternalChargeDTO) externalChgDTO).setApplyOn((paxExtChgDTO.getApplyOn() == null)
						? ReservationPaxSegmentSSR.APPLY_ON_SEGMENT
						: paxExtChgDTO.getApplyOn());
				((SSRExternalChargeDTO) externalChgDTO).setSSRText(paxExtChgDTO.getUserNote());
				((SSRExternalChargeDTO) externalChgDTO).setChargeCode(externalChargesMap.get(extChg.toString()));
				((SSRExternalChargeDTO) externalChgDTO)
						.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(paxExtChgDTO.getFlightRefNumber()));
				int ssrId = SSRUtil.getSSRId(paxExtChgDTO.getCode());
				((SSRExternalChargeDTO) externalChgDTO).setSSRId(ssrId);
				break;
			case FLEXI_CHARGES:
				externalChgDTO = new FlexiExternalChgDTO();
				LCCClientFlexiExternalChgDTO paxFlexiChg = (LCCClientFlexiExternalChgDTO) paxExtChgDTO;
				((FlexiExternalChgDTO) externalChgDTO).setChargeCode(paxFlexiChg.getCode());
				for (FlexiInfoTO flexibility : paxFlexiChg.getFlexiBilities()) {
					ReservationPaxOndFlexibilityDTO flexi = new ReservationPaxOndFlexibilityDTO();
					flexi.setAvailableCount(flexibility.getAvailableCount());
					flexi.setStatus(ReservationPaxOndFlexibility.STATUS_ACTIVE);
					flexi.setUtilizedCount(0);
					flexi.setFlexiRateId(flexibility.getFlexiRateId());
					flexi.setFlexibilityTypeId(flexibility.getFlexibilityTypeId());
					((FlexiExternalChgDTO) externalChgDTO).addToReservationFlexibilitiesList(flexi);
				}
				break;
			case BAGGAGE:
				externalChgDTO = new BaggageExternalChgDTO();
				((BaggageExternalChgDTO) externalChgDTO).setBaggageCode(paxExtChgDTO.getCode());
				break;
			case INSURANCE:
				externalChgDTO = new InsuranceSegExtChgDTO(new ExternalChgDTO());
				break;
			case JN_ANCI:
			case JN_OTHER:
			case JN_TAX:
				continue;
			default:
				externalChgDTO = new ExternalChgDTO();
				break;
			}
			
			if (EXTERNAL_CHARGES.HANDLING_CHARGE == paxExtChgDTO.getExternalCharges()) {
				externalChgDTO.setOperationMode(paxExtChgDTO.getOperationMode());
				externalChgDTO.setChargeCode(paxExtChgDTO.getCode());
				externalChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.HANDLING_CHARGE);
			}

			externalChgDTO.setApplicableChargeRate(paxExtChgDTO.getJourneyType());
			externalChgDTO.setAmountConsumedForPayment(paxExtChgDTO.isAmountConsumedForPayment());
			externalChgDTO.setAmount(paxExtChgDTO.getAmount());
			externalChgDTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChgDTO.getFlightRefNumber()));
			externalChgDTOs.add(externalChgDTO);
		}

		return externalChgDTOs;
	}
	
	public static Collection<ReservationPaxSegAirportTransfer> cancelSegmentApts(Reservation reservation,
			Collection<Integer> pnrSegmentIds, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxSeq)
			throws ModuleException {
		AirportTransferAssembler ipaxAirportTansfer = new AirportTransferAssembler(false);
		Collection<PaxAirportTransferTO> airportTransfers = reservation.getAirportTransfers();
		if (airportTransfers != null && !airportTransfers.isEmpty()) {
			for (PaxAirportTransferTO airportTransfer : airportTransfers) {
				if (pnrSegmentIds.contains(airportTransfer.getPnrSegId())) {
					ipaxAirportTansfer.removePaxSegmentApt(airportTransfer.getPnrPaxId(), airportTransfer.getPnrSegId(),
							airportTransfer.getPpfId(), airportTransfer.getChargeAmount(), airportTransfer.getAirportCode(),
							credentialsDTO.getTrackInfoDTO());
				}
			}
		}
		AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);
		if (chgTnxSeq == null) {
			chgTnxSeq = new ChargeTnxSeqGenerator(reservation.getPnr());
		}
		Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferToRemove = ipaxAirportTansfer
				.getPaxAirportTransferTORemove();
		if (!paxAirportTransferToRemove.isEmpty()) {
			AirportTransferAdaptor.addPassengerAptChargeAndAdjustCredit(adjustCreditBO, reservation, paxAirportTransferToRemove,
					null, credentialsDTO.getTrackInfoDTO(), credentialsDTO, chgTnxSeq);
		}
		return AirportTransferAdaptor.cancelPaxApts(paxAirportTransferToRemove, reservation);
	}

	private static boolean hasNoSHOWPax(ReservationPax reservationPax, Integer pnrSegId) {
		Set<ReservationPaxFare> colResPaxFares = reservationPax.getPnrPaxFares();
		for (ReservationPaxFare reservationPaxFare : colResPaxFares) {
			Set<ReservationPaxFareSegment> colPaxFareSegments = reservationPaxFare.getPaxFareSegments();
			for (ReservationPaxFareSegment reservationPaxFareSegment : colPaxFareSegments) {
				if (reservationPaxFareSegment.getPnrSegId().equals(pnrSegId) && reservationPaxFareSegment.getStatus()
						.equals(ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE)) {
					return true;
				}
			}
		}

		return false;
	}

	public static PnrChargesDTO getInfantChargesDTO(Collection<PnrChargesDTO> pnrChargesDTOs, int pnrPaxId) {

		for (PnrChargesDTO pnrChargesDTO : pnrChargesDTOs) {
			if (pnrChargesDTO.getPnrPaxId() == pnrPaxId) {
				return pnrChargesDTO;
			}
		}

		return null;
	}

	public static BigDecimal getNonRefundableTotal(Collection<ChargeMetaTO> nonRefundableChargeMetaAmounts) {
		BigDecimal nonRefundableTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (nonRefundableChargeMetaAmounts != null && !nonRefundableChargeMetaAmounts.isEmpty()) {
			for (ChargeMetaTO chargeMetaTO : nonRefundableChargeMetaAmounts) {
				nonRefundableTotal = AccelAeroCalculator.add(nonRefundableTotal, chargeMetaTO.getChargeAmount());
			}
		}

		return nonRefundableTotal;
	}

	/**
	 * Build key - This is not correct key
	 * 
	 * @param cardPaymentInfo
	 * @return
	 */
	/**
	 * TODO Build unique key
	 */
	public static String getPaymentKey(CardPaymentInfo cardPaymentInfo) {
		return cardPaymentInfo.getPaymentBrokerRefNo() + "_" + StringUtil.getNotNullString(cardPaymentInfo.getNo());
	}

	public static int getPayablePaxCount(Collection<ReservationPax> colPax) {
		int payablePaxCount = 0;
		if (colPax != null) {
			for (ReservationPax reservationPax : colPax) {
				if (!isInfantType(reservationPax)) {
					payablePaxCount++;
				}
			}
		}
		return payablePaxCount;
	}

	/**
	 * required to capture the reservation search data when reverse payment is executed
	 * 
	 */
	public static String getBasicReservationInfo(ReservationAssembler rAssembler) {
		StringBuffer sb = new StringBuffer();
		try {

			if (rAssembler != null) {

				sb.append("\n=======================================================================================\n");
				sb.append("=======================================================================================\n");
				sb.append("==========================  Reservation Info:: reversePayment =========================\n");
				sb.append("=======================================================================================\n");
				sb.append("=======================================================================================\n");
				if (rAssembler.getDummyReservation() != null) {
					sb.append("PAX_INFO:- " + rAssembler.getDummyReservation().getTotalPaxAdultCount() + " AD,"
							+ rAssembler.getDummyReservation().getTotalPaxChildCount() + " CH,"
							+ rAssembler.getDummyReservation().getTotalPaxInfantCount() + " IN \n");
				}

				if (rAssembler.getInventoryFares() != null) {
					Collection<OndFareDTO> colOndFareDTOs = rAssembler.getInventoryFares();

					if (!colOndFareDTOs.isEmpty() && colOndFareDTOs.size() > 0) {
						Iterator<OndFareDTO> itrOndFareDTOs = colOndFareDTOs.iterator();

						while (itrOndFareDTOs.hasNext()) {
							OndFareDTO ondFareDTO = itrOndFareDTOs.next();
							LinkedHashMap<Integer, FlightSegmentDTO> segmentsMap = ondFareDTO.getSegmentsMap();

							if (ondFareDTO.getFareSummaryDTO() != null) {
								sb.append("FARE:- FARE_ID: " + ondFareDTO.getFareSummaryDTO().getFareId() + " FARE_BASIS:"
										+ ondFareDTO.getFareSummaryDTO().getFareBasisCode() + " FARE_RULE:"
										+ ondFareDTO.getFareSummaryDTO().getFareRuleID() + "\n");
							}

							for (Integer flightSegId : segmentsMap.keySet()) {
								FlightSegmentDTO fltSegmentDTO = segmentsMap.get(flightSegId);
								sb.append("FLIGHT INFO:- FLT_NO: " + fltSegmentDTO.getFlightNumber());
								sb.append(" SEGMENT_CODE: " + fltSegmentDTO.getSegmentCode() + "\n");
								sb.append(" DEP: "
										+ CalendarUtil.formatForSQL(fltSegmentDTO.getDepartureDateTime(), CalendarUtil.PATTERN5));
								sb.append(" ARR: "
										+ CalendarUtil.formatForSQL(fltSegmentDTO.getArrivalDateTime(), CalendarUtil.PATTERN5)
										+ "\n");

							}
						}
					}

				}

				sb.append("=======================================================================================\n");
				sb.append("=======================================================================================\n");
			}

		} catch (Exception e) {
			sb.append("Exception @ getBasicReservationInfo");
		}

		return sb.toString();
	}

	public static boolean isRetOrHalfRetFareType(String fareType) {

		ArrayList<String> includeFareTypeList = new ArrayList<String>();
		includeFareTypeList.add(Integer.toString(FareTypes.RETURN_FARE));
		includeFareTypeList.add(Integer.toString(FareTypes.HALF_RETURN_FARE));

		if (includeFareTypeList.contains(fareType)) {
			return true;
		}

		return false;
	}

	public static List<AvailableFlightSearchDTO> getAvailableSubJourneyCollection(AvailableFlightSearchDTO avifltSearchDTO) {

		// recording total journey OND sequence order for charge quote
		Integer ondSequence = 0;
		Map<List<Integer>, Integer> totalJourneyOrderedOndSEQMap = new HashMap<List<Integer>, Integer>();
		for (OriginDestinationInfoDTO ondInfoDTOTemp : avifltSearchDTO.getOrderedOriginDestinations()) {
			totalJourneyOrderedOndSEQMap.put(ondInfoDTOTemp.getFlightSegmentIds(), ondSequence);
			ondSequence++;
		}

		if (totalJourneyOrderedOndSEQMap != null && totalJourneyOrderedOndSEQMap.size() > 0) {
			avifltSearchDTO.setTotalJourneyOrderedOndSEQMap(totalJourneyOrderedOndSEQMap);
		}

		Map<Integer, List<Integer>> existRetGropMap = avifltSearchDTO.getExistRetGropMap();
		List<AvailableFlightSearchDTO> avifltSearchDTOList = new ArrayList<AvailableFlightSearchDTO>();

		boolean isExistingRetGroupOndsRemoved = false;
		List<OriginDestinationInfoDTO> removeOndList = new ArrayList<OriginDestinationInfoDTO>();

		// Existing untouched Return group Journey
		if (existRetGropMap != null && existRetGropMap.size() > 0) {
			for (Integer retGroupId : existRetGropMap.keySet()) {
				ArrayList<Integer> pnrSegIds = (ArrayList<Integer>) existRetGropMap.get(retGroupId);

				if (pnrSegIds != null && pnrSegIds.size() > 1) {
					List<OriginDestinationInfoDTO> groupOndList = new ArrayList<OriginDestinationInfoDTO>();
					Iterator<OriginDestinationInfoDTO> ondDTOInfoItr = avifltSearchDTO.getOrderedOriginDestinations().iterator();
					List<OriginDestinationInfoDTO> subJourneyRemoveONDList = new ArrayList<OriginDestinationInfoDTO>();
					while (ondDTOInfoItr.hasNext()) {
						OriginDestinationInfoDTO ondInfo = ondDTOInfoItr.next();

						if (ondInfo.getOldPerPaxFareTO() != null) {
							boolean isRetHalf = ReservationApiUtils
									.isRetOrHalfRetFareType(ondInfo.getOldPerPaxFareTO().getFareType());

							if (isRetHalf && ondInfo.getExistingFlightSegIds() != null
									&& !ondInfo.getExistingFlightSegIds().isEmpty()
									&& pnrSegIds.containsAll(ondInfo.getExistingFlightSegIds())) {
								groupOndList.add(ondInfo.clone());
								subJourneyRemoveONDList.add(ondInfo);
							}
						}

					}

					if (groupOndList != null && groupOndList.size() > 1) {
						AvailableFlightSearchDTO avifltSearchDTOCopy = avifltSearchDTO.deepCopy(groupOndList);

						if (avifltSearchDTOCopy.getJourneyType() == JourneyType.ROUNDTRIP
								|| avifltSearchDTOCopy.getJourneyType() == JourneyType.CIRCULAR) {
							removeOndList.addAll(subJourneyRemoveONDList);
							avifltSearchDTO.getOriginDestinationInfoDTOs().removeAll(subJourneyRemoveONDList);
							avifltSearchDTOList.add(avifltSearchDTOCopy);
							isExistingRetGroupOndsRemoved = true;

						}

					}

				}

			}
		}

		// Identifying sub-journey within available remaining ONDs
		if (avifltSearchDTO.getOriginDestinationInfoDTOs().size() > 0) {
			List<OriginDestinationInfoDTO> remainOndList = new ArrayList<OriginDestinationInfoDTO>();
			for (OriginDestinationInfoDTO ondInfo : avifltSearchDTO.getOriginDestinationInfoDTOs()) {
				remainOndList.add(ondInfo.clone());
			}

			AvailableFlightSearchDTO avifltSearchDTOCopy = avifltSearchDTO.deepCopy(remainOndList);

			Collection<AvailableFlightSearchDTO> subJouneyFltSearchColl = getReGroupNewSubJourney(avifltSearchDTOCopy);

			avifltSearchDTOList.addAll(subJouneyFltSearchColl);
		}

		if (isExistingRetGroupOndsRemoved) {
			avifltSearchDTO.getOriginDestinationInfoDTOs().addAll(removeOndList);
		}

		ReservationApiUtils.processApplyLastFareQuoteForJourney(avifltSearchDTOList);

		return avifltSearchDTOList;
	}

	private static Collection<AvailableFlightSearchDTO> getReGroupNewSubJourney(AvailableFlightSearchDTO avifltSearchDTO) {

		// all ONDs waiting to be re-organized
		List<OriginDestinationInfoDTO> allOndList = avifltSearchDTO.getOrderedOriginDestinations();
		// collection contains the sub-journey OND lists
		List<List<OriginDestinationInfoDTO>> allJourneyList = new ArrayList<List<OriginDestinationInfoDTO>>();
		// grouped ONDs to be removed from the main list
		List<OriginDestinationInfoDTO> removedOndList = new ArrayList<OriginDestinationInfoDTO>();

		// Identifying the RETURN JOURNEY
		if (allOndList != null && allOndList.size() > 1) {
			for (OriginDestinationInfoDTO ondInfo1 : allOndList) {
				for (OriginDestinationInfoDTO ondInfo2 : allOndList) {
					if (!ondInfo1.equals(ondInfo2) && !removedOndList.contains(ondInfo2) && !removedOndList.contains(ondInfo1)) {

						if (ondInfo1.getDestination().equals(ondInfo2.getOrigin())
								&& ondInfo1.getOrigin().equals(ondInfo2.getDestination())) {

							// RETURN JOURNEY
							List<OriginDestinationInfoDTO> returnList = new ArrayList<OriginDestinationInfoDTO>();

							returnList.add(ondInfo1.clone());
							returnList.add(ondInfo2.clone());

							allJourneyList.add(returnList);

							removedOndList.add(ondInfo1);
							removedOndList.add(ondInfo2);
							break;
						}
					}

				}

			}

			// REMOVE identified return journey ONDs
			allOndList.removeAll(removedOndList);
		}

		if (allOndList != null && allOndList.size() > 1) {
			// Identifying the CIRCULAR JOURNEY
			for (OriginDestinationInfoDTO ondInfo1 : allOndList) {

				if (isEligibleForCircularJourney(allOndList, ondInfo1, true) && !removedOndList.contains(ondInfo1)) {
					OriginDestinationInfoDTO prevOnd = null;
					Set<OriginDestinationInfoDTO> cycleList = new HashSet<OriginDestinationInfoDTO>();
					boolean matchedOndRecord = false;
					for (OriginDestinationInfoDTO ondInfo2 : allOndList) {

						if (ondInfo2.equals(ondInfo1)) {
							matchedOndRecord = true;
							continue;
						}

						if (matchedOndRecord) {
							if (prevOnd == null) {
								prevOnd = ondInfo1;
							}

							if (!ondInfo1.equals(ondInfo2)
									&& (isEligibleForCircularJourney(allOndList, ondInfo2, false)
											|| ondInfo1.getOrigin().equals(ondInfo2.getDestination()))
									&& !removedOndList.contains(ondInfo2)
									&& prevOnd.getDestination().equals(ondInfo2.getOrigin())) {

								cycleList.add(prevOnd);
								cycleList.add(ondInfo2);

								if (ondInfo1.getOrigin().equals(ondInfo2.getDestination())) {
									List<OriginDestinationInfoDTO> returnList = getSubJourneyToList(cycleList, removedOndList);

									if (returnList != null && returnList.size() > 1) {
										allJourneyList.add(returnList);
									}
									break;
								}

								prevOnd = ondInfo2;

							}
						}

					}
				}

			}

			// REMOVE identified circular journey ONDs
			allOndList.removeAll(removedOndList);
		}

		if (allOndList != null && allOndList.size() > 1) {
			// Identifying the OPEN-JAW JOURNEY
			if (AppSysParamsUtil.isOpenJawMultiCitySearchEnabled()) {
				// OPEN-JAW formed by more than 2 ONDs, starts & ends at same point
				if (isSingleOpenJawExist(allOndList)) {

					List<OriginDestinationInfoDTO> openJawOndList = new ArrayList<OriginDestinationInfoDTO>();
					for (OriginDestinationInfoDTO ondInfo : allOndList) {
						removedOndList.add(ondInfo);
						openJawOndList.add(ondInfo.clone());
					}
					allJourneyList.add(openJawOndList);
					allOndList.removeAll(removedOndList);

				}

				if (allOndList != null && allOndList.size() > 1) {
					for (OriginDestinationInfoDTO ondInfo1 : allOndList) {
						boolean matchedOndRecord = false;
						if (!removedOndList.contains(ondInfo1)) {
							for (OriginDestinationInfoDTO ondInfo2 : allOndList) {
								if (ondInfo2.equals(ondInfo1)) {
									matchedOndRecord = true;
									continue;
								}
								if (!ondInfo1.equals(ondInfo2) && !removedOndList.contains(ondInfo2)
										&& isValidOpenJawOndMatch(ondInfo1, ondInfo2) && matchedOndRecord) {

									removedOndList.add(ondInfo1);
									removedOndList.add(ondInfo2);

									List<OriginDestinationInfoDTO> openJawOndList = new ArrayList<OriginDestinationInfoDTO>();
									openJawOndList.add(ondInfo1.clone());
									openJawOndList.add(ondInfo2.clone());

									allJourneyList.add(openJawOndList);
									break;
								}

							}

						}
					}

					// REMOVE identified OPEN-JAW JOURNEY ONDs
					allOndList.removeAll(removedOndList);
				}

			}
		}

		// Add remaining ONDs
		if (allOndList != null && allOndList.size() > 0) {
			allJourneyList.add(allOndList);
		}

		Collection<AvailableFlightSearchDTO> subJouneyFltSearchColl = new ArrayList<AvailableFlightSearchDTO>();
		if (allJourneyList != null && allJourneyList.size() > 0) {

			Iterator<List<OriginDestinationInfoDTO>> allJourneyItr = allJourneyList.iterator();
			while (allJourneyItr.hasNext()) {
				List<OriginDestinationInfoDTO> subJourneyList = allJourneyItr.next();

				AvailableFlightSearchDTO avifltSearchDTOCopy = avifltSearchDTO.deepCopy(subJourneyList);
				subJouneyFltSearchColl.add(avifltSearchDTOCopy);

			}
		}

		return subJouneyFltSearchColl;

	}

	private static List<OriginDestinationInfoDTO> getSubJourneyToList(Set<OriginDestinationInfoDTO> cycleList,
			List<OriginDestinationInfoDTO> removedOndList) {
		List<OriginDestinationInfoDTO> returnList = new ArrayList<OriginDestinationInfoDTO>();
		if (cycleList != null && cycleList.size() > 1) {
			Iterator<OriginDestinationInfoDTO> cycleItr = cycleList.iterator();
			while (cycleItr.hasNext()) {
				OriginDestinationInfoDTO ondInfo = cycleItr.next();
				returnList.add(ondInfo.clone());

				removedOndList.add(ondInfo);
			}

		}
		return returnList;
	}

	private static boolean isEligibleForCircularJourney(List<OriginDestinationInfoDTO> allOndList,
			OriginDestinationInfoDTO ondInfo2, boolean isFirstOnd) {
		String airport = ondInfo2.getDestination();

		boolean inExist = true;
		boolean outExist = false;
		boolean isValidFirstOnD = false;
		boolean matchOnDRecord = false;
		String lastAirport = ondInfo2.getDestination();
		for (OriginDestinationInfoDTO ondInfo : allOndList) {

			if (ondInfo2.equals(ondInfo)) {
				matchOnDRecord = true;
				continue;
			}

			if (!ondInfo.getOrigin().equals(lastAirport)) {
				continue;
			}

			if (matchOnDRecord) {
				lastAirport = ondInfo.getDestination();

				if (airport.equals(ondInfo.getOrigin())) {
					outExist = true;
				}

				if (inExist && outExist && ondInfo2.getOrigin().equals(ondInfo.getDestination())) {
					isValidFirstOnD = true;
				}

				if (isFirstOnd && isValidFirstOnD) {
					return true;
				} else if (!isFirstOnd && outExist && inExist) {
					return true;
				}
			}

		}
		return false;
	}

	public static PaxDiscountInfoDTO updatePaxOndExternalChargeDiscountInfo(PaxDiscountInfoDTO paxDiscInfo,
			ExternalChgDTO externalChgDTO, Collection<DiscountChargeTO> discountChargeTOs, Integer paxSequence) {
		BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		boolean isDiscountApplied = false;
		if (paxDiscInfo != null && externalChgDTO != null) {
			String chargeCode = getChargeCode(externalChgDTO);
			if (discountChargeTOs != null && !discountChargeTOs.isEmpty()) {
				Integer rateId = externalChgDTO.getChgRateId();
				for (DiscountChargeTO discountChargeTO : discountChargeTOs) {
					if (discountChargeTO.getFlightSegmentIds().contains(externalChgDTO.getFlightSegId())
							&& discountChargeTO.getRateId().equals(rateId) && discountChargeTO.getChargeCode().equals(chargeCode)
							&& discountChargeTO.getDiscountAmount().doubleValue() > 0 && !AccelAeroCalculator
									.isGreaterThan(discountChargeTO.getDiscountAmount(), externalChgDTO.getAmount())
							&& !discountChargeTO.isConsumed()) {

						discount = discountChargeTO.getDiscountAmount().negate();
						discountChargeTO.setConsumed(true);
						isDiscountApplied = true;
						break;
					}
				}
			}

			paxDiscInfo.setDiscountApplied(isDiscountApplied);
			paxDiscInfo.setDiscount(discount);
		}

		return paxDiscInfo;
	}

	/**
	 * In LCC flows some generic charges comes without flightSegId following function will detect those external charges
	 * & pass respective flightSegId, in-order to identify applied discount amount against same charge
	 */
	public static PaxDiscountInfoDTO updatePaxOndExternalGenericChargeDiscountInfo(ReservationPaxFare reservationPaxFare,
			PaxDiscountInfoDTO paxDiscInfo, ExternalChgDTO externalChgDTO, Collection<DiscountChargeTO> discountChargeTOs) {
		BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		boolean isDiscountApplied = false;
		if (paxDiscInfo != null && externalChgDTO != null) {
			Collection<Integer> flightSegId = new ArrayList<Integer>();
			if (reservationPaxFare != null) {
				Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();
				if (paxFareSegments != null && !paxFareSegments.isEmpty()) {
					for (ReservationPaxFareSegment paxFareSegment : paxFareSegments) {
						if (paxFareSegment.getSegment() != null && paxFareSegment.getSegment().getFlightSegId() != null) {
							flightSegId.add(paxFareSegment.getSegment().getFlightSegId());
						}
					}
				}
			}

			String chargeCode = getChargeCode(externalChgDTO);
			if (discountChargeTOs != null && !discountChargeTOs.isEmpty()) {
				Integer rateId = externalChgDTO.getChgRateId();
				for (DiscountChargeTO discountChargeTO : discountChargeTOs) {
					if ((CollectionUtils.containsAny(discountChargeTO.getFlightSegmentIds(), flightSegId))
							&& discountChargeTO.getRateId().equals(rateId) && discountChargeTO.getChargeCode().equals(chargeCode)
							&& discountChargeTO.getDiscountAmount().doubleValue() > 0 && !AccelAeroCalculator
									.isGreaterThan(discountChargeTO.getDiscountAmount(), externalChgDTO.getAmount())
							&& !discountChargeTO.isConsumed()) {

						discount = discountChargeTO.getDiscountAmount().negate();
						discountChargeTO.setConsumed(true);
						isDiscountApplied = true;
						break;
					}
				}
			}

			paxDiscInfo.setDiscountApplied(isDiscountApplied);
			paxDiscInfo.setDiscount(discount);
		}

		return paxDiscInfo;
	}

	public static BigDecimal getApplicableChargeAmount(String paxType, QuotedChargeDTO quotedChargeDTO) {

		BigDecimal paxChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (PassengerType.ADULT.equals(paxType)) {
			if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
					|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
					|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
					|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {

				paxChargeAmount = AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT));

			}
		} else if (PassengerType.CHILD.equals(paxType)) {
			if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY
					|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
					|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {

				paxChargeAmount = AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD));

			}
		} else if (PassengerType.INFANT.equals(paxType)) {
			if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
					|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
					|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT) {

				paxChargeAmount = AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT));

			}
		}

		return paxChargeAmount;
	}

	public static void processApplyLastFareQuoteForJourney(List<AvailableFlightSearchDTO> avifltSearchDTOList) {
		if (avifltSearchDTOList != null && avifltSearchDTOList.size() > 0) {
			Iterator<AvailableFlightSearchDTO> avifltSearchDTOItr = avifltSearchDTOList.iterator();

			boolean isSubJourneyDetected = false;

			if (avifltSearchDTOList.size() > 1) {
				isSubJourneyDetected = true;
			}

			while (avifltSearchDTOItr.hasNext()) {
				AvailableFlightSearchDTO avifltSearchDTO = avifltSearchDTOItr.next();

				if (!isLastFareQuoteDateApplicableForJourney(avifltSearchDTO)) {
					avifltSearchDTO.setLastFareQuotedDate(null);
					avifltSearchDTO.setFQOnLastFQDate(false);
					avifltSearchDTO.setFQWithinValidity(false);
				}

				avifltSearchDTO.setSubJourney(isSubJourneyDetected);
			}

		}

	}

	private static boolean isLastFareQuoteDateApplicableForJourney(AvailableFlightSearchDTO availFltSearchDTO) {
		boolean isApplicable = false;
		if (availFltSearchDTO != null) {
			List<OriginDestinationInfoDTO> groupOndList = availFltSearchDTO.getOrderedOriginDestinations();
			if (groupOndList != null && groupOndList.size() > 0
					&& (availFltSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP)) {
				Iterator<OriginDestinationInfoDTO> groupOndItr = groupOndList.iterator();
				while (groupOndItr.hasNext()) {
					OriginDestinationInfoDTO originDestinationInfoDTO = groupOndItr.next();
					com.isa.thinair.airproxy.api.model.reservation.commons.FareTO fareTO = originDestinationInfoDTO
							.getOldPerPaxFareTO();
					if (fareTO != null) {
						if (ReservationApiUtils.isRetOrHalfRetFareType(fareTO.getFareType())) {
							isApplicable = true;
						} else if (isApplicable && !ReservationApiUtils.isRetOrHalfRetFareType(fareTO.getFareType())) {
							isApplicable = false;
							break;
						}
					}

				}
			}
		}

		return isApplicable;
	}

	/**
	 * when minus charge adjustment is applied following function validates entered amount is valid minus amount
	 */
	public static Map<Integer, BigDecimal> isValidChargeAdjustment(ReservationPaxFare reservationPaxFare, BigDecimal amount,
			BigDecimal totalPlusAdjCharge) throws ModuleException {
		Map<Integer, BigDecimal> adjustedChargeAmount = null;
		if (reservationPaxFare != null && amount.doubleValue() < 0
		// When other carrier amounts are converted, there can be a scenario where it will not match due to
		// decimal conversions.I am using floor because a case like 3284.0 and 3284.01 will not match if we use ceil
				&& (Math.floor(reservationPaxFare.getTotalChargeAmount().doubleValue()) <= Math
						.floor((amount.doubleValue() * -1)))) {

			if (adjustedChargeAmount == null) {
				adjustedChargeAmount = new HashMap<Integer, BigDecimal>();
			}

			amount = AccelAeroCalculator.multiply(amount, -1);
			Integer fltSegId = null;
			BigDecimal balanceAmount = amount;
			Iterator<ReservationPaxFareSegment> paxFareSegItr = reservationPaxFare.getPaxFareSegments().iterator();
			while (paxFareSegItr.hasNext()) {
				ReservationPaxFareSegment paxFareSeg = paxFareSegItr.next();
				fltSegId = paxFareSeg.getSegment().getFlightSegId();
			}

			// Passenger adjustment amount
			if (reservationPaxFare.getTotalChargeAmount().doubleValue() > 0) {
				balanceAmount = AccelAeroCalculator.subtract(amount, reservationPaxFare.getTotalChargeAmount());
				// Required when plus & minus adjustment happens at the same time.[Ex:GDS Ticket issue]
				if (totalPlusAdjCharge != null && totalPlusAdjCharge.doubleValue() > 0) {
					if (totalPlusAdjCharge.doubleValue() >= balanceAmount.doubleValue()) {
						adjustedChargeAmount.put(reservationPaxFare.getPnrPaxFareId(), amount.negate());
						balanceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {
						BigDecimal effectiveTotalCharge = AccelAeroCalculator.add(reservationPaxFare.getTotalChargeAmount(),
								totalPlusAdjCharge);
						adjustedChargeAmount.put(reservationPaxFare.getPnrPaxFareId(), effectiveTotalCharge.negate());
						balanceAmount = AccelAeroCalculator.subtract(balanceAmount, effectiveTotalCharge);
					}

				} else {
					adjustedChargeAmount.put(reservationPaxFare.getPnrPaxFareId(),
							reservationPaxFare.getTotalChargeAmount().negate());
				}
			} else if (reservationPaxFare.getTotalChargeAmount().doubleValue() < 0 && totalPlusAdjCharge != null
					&& totalPlusAdjCharge.doubleValue() > 0 && totalPlusAdjCharge.doubleValue() >= amount.doubleValue()) {
				// this is possible only via GDS Ticket issue
				adjustedChargeAmount.put(reservationPaxFare.getPnrPaxFareId(), amount.negate());
				balanceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			}

			balanceAmount = adjustExchangedONDCharges(reservationPaxFare, balanceAmount, fltSegId, adjustedChargeAmount);

			if (balanceAmount.doubleValue() > 0) {
				if (ReservationApiUtils.isParentAfterSave(reservationPaxFare.getReservationPax())) {

					List<ReservationPaxFare> infantResPaxFareCol = getInfantPaxFareRecord(reservationPaxFare, fltSegId);

					if (infantResPaxFareCol.size() == 0 && balanceAmount.doubleValue() > 0) {
						throw new ModuleException("airreservations.charge.adj.amount.invalid");
					}

					for (ReservationPaxFare infantResPaxFare : infantResPaxFareCol) {
						BigDecimal infantTotalCharge = infantResPaxFare.getTotalChargeAmount();
						// Infant adjustment amount
						if (balanceAmount.doubleValue() > infantTotalCharge.doubleValue()) {
							adjustedChargeAmount.put(infantResPaxFare.getPnrPaxFareId(), infantTotalCharge.negate());
							balanceAmount = AccelAeroCalculator.subtract(balanceAmount, infantTotalCharge);
							balanceAmount = adjustExchangedONDCharges(infantResPaxFare, balanceAmount, fltSegId,
									adjustedChargeAmount);
						} else {
							adjustedChargeAmount.put(infantResPaxFare.getPnrPaxFareId(), balanceAmount.negate());
							balanceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
					}

					if (balanceAmount.doubleValue() > 0) {
						throw new ModuleException("airreservations.charge.adj.amount.invalid");
					}

				} else {
					throw new ModuleException("airreservations.charge.adj.amount.invalid");
				}
			}

		}

		return adjustedChargeAmount;

	}

	/**
	 * when minus charge adjustment is applied following function validates entered amount is valid minus amount and
	 * same time if its a parent record then give the correct charge portion of the parent and infant for selected OND
	 */
	public static Map<Integer, BigDecimal> getAdjustedAmountForPassenger(Reservation reservation, Integer pnrPaxFareId,
			BigDecimal amount, Map<Integer, BigDecimal> totalPlusAdjByPaxFareId) throws ModuleException {

		Map<Integer, BigDecimal> adjustedChargeAmount = null;

		if (reservation != null && reservation.getPassengers() != null && !reservation.getPassengers().isEmpty()) {
			Iterator<ReservationPax> reservationPaxItr = reservation.getPassengers().iterator();

			while (reservationPaxItr.hasNext()) {
				ReservationPax reservationPax = reservationPaxItr.next();

				for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {

					if (reservationPaxFare != null && reservationPaxFare.getPnrPaxFareId().equals(pnrPaxFareId)) {
						BigDecimal totalPlusAdjCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (totalPlusAdjByPaxFareId != null && !totalPlusAdjByPaxFareId.isEmpty()
								&& totalPlusAdjByPaxFareId.get(pnrPaxFareId) != null) {
							totalPlusAdjCharge = totalPlusAdjByPaxFareId.get(pnrPaxFareId);
						}
						adjustedChargeAmount = isValidChargeAdjustment(reservationPaxFare, amount, totalPlusAdjCharge);
					}
				}

			}
		}
		return adjustedChargeAmount;

	}

	public static ClassOfServiceDTO getClassOfServiceDTOForReservedSegment(Integer pnrSegId, Reservation reservation)
			throws ModuleException {

		ClassOfServiceDTO classOfServiceDTO = new ClassOfServiceDTO();
		Set<ReservationSegment> segments = reservation.getSegments();
		for (ReservationSegment segment : segments) {
			if (segment.getPnrSegId().equals(pnrSegId)) {
				classOfServiceDTO.setCabinClassCode(segment.getCabinClassCode());
				classOfServiceDTO.setLogicalCCCode(segment.getLogicalCCCode());
				break;
			}
		}

		return classOfServiceDTO;
	}

	private static List<ReservationPaxFare> getInfantPaxFareRecord(ReservationPaxFare reservationPaxFare, Integer fltSegId) {

		List<ReservationPaxFare> infantResPaxFareCol = new ArrayList<ReservationPaxFare>();

		if (reservationPaxFare.getReservationPax() != null && reservationPaxFare.getReservationPax().getInfants() != null
				&& fltSegId != null) {
			Iterator<ReservationPax> paxInfantItr = reservationPaxFare.getReservationPax().getInfants().iterator();
			while (paxInfantItr.hasNext()) {
				ReservationPax infantPax = paxInfantItr.next();
				Iterator<ReservationPaxFare> infantResPaxFareItr = infantPax.getPnrPaxFares().iterator();
				while (infantResPaxFareItr.hasNext()) {
					ReservationPaxFare infantResPaxFare = infantResPaxFareItr.next();
					Iterator<ReservationPaxFareSegment> infantResPaxFareSegItr = infantResPaxFare.getPaxFareSegments().iterator();
					while (infantResPaxFareSegItr.hasNext()) {
						ReservationPaxFareSegment infantPaxFareSeg = infantResPaxFareSegItr.next();

						if (infantPaxFareSeg.getSegment().getFlightSegId().equals(fltSegId)) {
							infantResPaxFareCol.add(infantResPaxFare);
						}
					}

				}

			}
		}

		return infantResPaxFareCol;
	}

	/**
	 * validates whether two ONDs can form a valid open-jaw journey
	 * 
	 */
	private static boolean isValidOpenJawOndMatch(OriginDestinationInfoDTO ondInfo1, OriginDestinationInfoDTO ondInfo2) {
		// ONDs not domestic
		// journey :
		// 1.starts & ends @ same country
		// TODO:2.when more than 2 ONDs starts & ends at same point
		boolean isJourneyStartAndEndAtSameCountry = false;
		boolean isNotDomestic = false;
		boolean isOverlapAirportExist = false;
		boolean isJouneryStartAndEndAtSamePoint = false;

		Object[] startOndOriginAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(ondInfo1.getOrigin(), false);
		Object[] startOndDestAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(ondInfo1.getDestination(), false);

		Object[] endOndOriginAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(ondInfo2.getOrigin(), false);
		Object[] endOndDestAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(ondInfo2.getDestination(), false);

		if (startOndOriginAirportInfo != null && endOndDestAirportInfo != null) {
			isJourneyStartAndEndAtSameCountry = startOndOriginAirportInfo[3].equals(endOndDestAirportInfo[3]);
		}

		if (ondInfo1.getOrigin().equals(ondInfo2.getDestination())) {
			isJouneryStartAndEndAtSamePoint = true;
		}

		if (!startOndOriginAirportInfo[3].equals(startOndDestAirportInfo[3])
				&& !endOndOriginAirportInfo[3].equals(endOndDestAirportInfo[3])) {
			isNotDomestic = true;
		}
		List<String> connectingPorts = new ArrayList<String>();
		List<OriginDestinationInfoDTO> ondList = new ArrayList<OriginDestinationInfoDTO>();
		ondList.add(ondInfo1);
		ondList.add(ondInfo2);

		boolean isSameAirportExist = ReservationApiUtils.processFlightsConnectionAirports(ondList, connectingPorts);
		boolean returningSameCountryArray[] = ReservationApiUtils.isOpenJawReturningFromSameCountry(ondInfo1, ondInfo2);

		if (!isSameAirportExist) {
			if (connectingPorts.contains(ondInfo1.getOrigin()) || connectingPorts.contains(ondInfo1.getDestination())
					|| connectingPorts.contains(ondInfo2.getOrigin()) || connectingPorts.contains(ondInfo2.getDestination())) {
				isOverlapAirportExist = true;
			}

			boolean isValid = false;

			if (!isOverlapAirportExist && isNotDomestic && isJourneyStartAndEndAtSameCountry) {
				isValid = true;
			}

			if (isValid && !ondInfo1.getDestination().equals(ondInfo2.getOrigin())) {
				if (isJouneryStartAndEndAtSamePoint && returningSameCountryArray[0]) {
					return true;
				} else if (!isJouneryStartAndEndAtSamePoint && isJourneyStartAndEndAtSameCountry
						&& returningSameCountryArray[1]) {
					return true;
				}
			} else if (isValid) {
				return true;
			}
		}

		return false;
	}

	private static boolean isSingleOpenJawExist(List<OriginDestinationInfoDTO> originDestinationInfoList) {
		boolean inBoundAllDomestic = true;
		boolean outBoundAllDomestic = true;
		boolean allDomestic = true;
		boolean isJourneyStartAndEndAtSameCountry = false;
		boolean secondSectorStartAtSamePoint = false;
		boolean isOpenJaw = false;
		boolean isOverlapAirportExist = false;
		if (originDestinationInfoList != null && originDestinationInfoList.size() >= 2) {

			int ondListSize = originDestinationInfoList.size();
			int countinousSectors = 1;
			OriginDestinationInfoDTO prevOnd = null;
			OriginDestinationInfoDTO outBoundLastOnd = null;
			OriginDestinationInfoDTO inBoundFirstOnd = null;
			int count = 1;

			boolean isJouneryStartAndEndAtSamePoint = (originDestinationInfoList.get(0).getOrigin()
					.equals(originDestinationInfoList.get(originDestinationInfoList.size() - 1).getDestination()));

			String startCountry = originDestinationInfoList.get(0).getOrigin();
			String endCountry = originDestinationInfoList.get(ondListSize - 1).getDestination();
			Object[] startOndAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(startCountry, false);
			Object[] endOndAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(endCountry, false);

			if (startOndAirportInfo != null && endOndAirportInfo != null) {
				isJourneyStartAndEndAtSameCountry = startOndAirportInfo[3].equals(endOndAirportInfo[3]);
			}

			List<String> connectingPorts = new ArrayList<String>();

			boolean isSameAirportExist = processFlightsConnectionAirports(originDestinationInfoList, connectingPorts);

			if (!isSameAirportExist) {
				for (OriginDestinationInfoDTO ondInfo : originDestinationInfoList) {

					Object[] originAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(ondInfo.getOrigin(), false);
					Object[] destinationOndAirportInfo = CommonsServices.getGlobalConfig()
							.getAirportInfo(ondInfo.getDestination(), false);

					if (!originAirportInfo[3].equals(destinationOndAirportInfo[3])) {
						if (countinousSectors == 1) {
							outBoundAllDomestic = false;
						}
						allDomestic = false;
					}

					if (prevOnd != null) {
						if (!ondInfo.getOrigin().equals(prevOnd.getDestination())) {
							countinousSectors++;
							outBoundLastOnd = prevOnd;
							inBoundFirstOnd = ondInfo;
						}
					}
					prevOnd = ondInfo;

					if (connectingPorts.contains(ondInfo.getOrigin()) || connectingPorts.contains(ondInfo.getDestination())) {
						isOverlapAirportExist = true;
					}

					if (countinousSectors == 2) {

						if (!originAirportInfo[3].equals(destinationOndAirportInfo[3])) {
							inBoundAllDomestic = false;
						}

						if (ondInfo.getOrigin().equals(originDestinationInfoList.get(0).getOrigin())) {
							secondSectorStartAtSamePoint = true;
						}

					} else if (ondListSize == 2 && countinousSectors == 1 && count == 2) {
						if (!originAirportInfo[3].equals(destinationOndAirportInfo[3])) {
							inBoundAllDomestic = false;
						}
					}
					count++;
				}
			}

			boolean returningSameCountryArray[] = ReservationApiUtils.isOpenJawReturningFromSameCountry(outBoundLastOnd,
					inBoundFirstOnd);

			if (!isOverlapAirportExist && !allDomestic && !inBoundAllDomestic && !outBoundAllDomestic) {
				if (ondListSize == 2 && countinousSectors == 1 && !isJouneryStartAndEndAtSamePoint
						&& isJourneyStartAndEndAtSameCountry) {
					isOpenJaw = true;
				} else if (!secondSectorStartAtSamePoint && isJouneryStartAndEndAtSamePoint && countinousSectors == 2
						&& returningSameCountryArray[0]) {
					isOpenJaw = true;
				} else if (ondListSize == 2 && countinousSectors == 2 && !isJouneryStartAndEndAtSamePoint
						&& isJourneyStartAndEndAtSameCountry && returningSameCountryArray[1]) {
					isOpenJaw = true;
				}
			}
		}

		return isOpenJaw;
	}

	/**
	 * process the ONDs & identify whether any connection flights exist. returns true if overlap connecting airports
	 * exist.
	 * 
	 * required to identify open-jaw journey in following cases: when RES_184=Y CASE 1: 1. DXB/IKA/DUS & DEL/IKA/DXB is
	 * NOT AN OPEN-JAW 2. DXB/IKA/DUS & DEL/MHD/DXB is AN OPEN-JAW 3. DXB/DUS & DEL/DXB is AN OPEN-JAW 4. DXB/IKA/DUS &
	 * IKA/DXB is NOT AN OPEN-JAW
	 * 
	 * when RES_184=N CASE 2: 1. DXB/IKA/DUS & DEL/IKA/DXB is OPEN-JAW 2. DXB/IKA/DUS & DEL/MHD/DXB is AN OPEN-JAW 3.
	 * DXB/DUS & DEL/DXB is AN OPEN-JAW 4. DXB/IKA/DUS & IKA/DXB is NOT AN OPEN-JAW
	 * 
	 */
	public static boolean processFlightsConnectionAirports(List<OriginDestinationInfoDTO> originDestinationInfoList,
			List<String> connectingPorts) {
		try {
			// don't filter ondInfo.getFlightSegmentIds().size() > 1 because multi-legs sectors mights get
			// skipped
			if (originDestinationInfoList != null && originDestinationInfoList.size() > 0) {
				for (OriginDestinationInfoDTO ondInfo : originDestinationInfoList) {
					if (ondInfo != null && ondInfo.getFlightSegmentIds() != null && !ondInfo.getFlightSegmentIds().isEmpty()) {
						Collection<FlightSegmentDTO> flightSegments = ReservationModuleUtils.getFlightBD()
								.getFlightSegments(ondInfo.getFlightSegmentIds());
						List<String> newPortsAdded = new ArrayList<String>();
						for (FlightSegmentDTO flightSegmentDTO : flightSegments) {
							String segmentCode = flightSegmentDTO.getSegmentCode();
							if (segmentCode != null) {
								String[] airports = StringUtils.split(segmentCode, "/");
								for (int i = 0; i < airports.length; i++) {
									if (!ondInfo.getOrigin().equals(airports[i]) && !ondInfo.getDestination().equals(airports[i])
											&& !connectingPorts.contains(airports[i])) {
										connectingPorts.add(airports[i]);
										newPortsAdded.add(airports[i]);

									}
									if (AppSysParamsUtil.isValidateConnectionFlightForOpenJaw()) {
										if (connectingPorts.contains(airports[i]) && !newPortsAdded.contains(airports[i])) {
											return true;
										}
									}

								}
							}

						}

					}
				}
			}

		} catch (Exception e) {
			log.error("Error @ processFlightsConnectionAirports");
		}
		return false;
	}

	public static void splitPossibleOnDs(AvailableFlightSearchDTO avifltSearchDTO) throws ModuleException {
		if (avifltSearchDTO.isOwnSearch() &&AppSysParamsUtil.isEnableOndSplitForBusFareQuote() && avifltSearchDTO != null
				&& avifltSearchDTO.getOrderedOriginDestinations() != null
				&& avifltSearchDTO.getOrderedOriginDestinations().size() > 1) {
			int originSequence = 0;
			boolean isOnDSplitOperationInvoked = false;
			List<OriginDestinationInfoDTO> newOndList = new ArrayList<OriginDestinationInfoDTO>();
			for (OriginDestinationInfoDTO ondInfo : avifltSearchDTO.getOrderedOriginDestinations()) {
				if (ondInfo.isFlownOnd()) {
					newOndList.add(ondInfo);
					ondInfo.setRequestedOndSequence(originSequence);
				} else {
					CachedAirportDTO originAirport = ReservationModuleUtils.getAirportBD().getCachedAirport(ondInfo.getOrigin());

					CachedAirportDTO destinationAirport = ReservationModuleUtils.getAirportBD()
							.getCachedAirport(ondInfo.getDestination());

					if (originAirport.isSurfaceSegment() && !destinationAirport.isSurfaceSegment()) {
						String connectingAirport = originAirport.getConnectingAirport();
						if (connectingAirport != null && !ondInfo.getDestination().equals(connectingAirport)) {
							isOnDSplitOperationInvoked = true;
							OriginDestinationInfoDTO firstOnd = ondInfo.clone();
							OriginDestinationInfoDTO secondOnd = ondInfo.clone();

							firstOnd.setDestination(connectingAirport);
							firstOnd.setGroundSegmentOnly(true);
							secondOnd.setOrigin(connectingAirport);

							Collection<FlightSegmentDTO> flightSegments = ReservationModuleUtils.getFlightBD()
									.getFlightSegments(ondInfo.getFlightSegmentIds());
							firstOnd.setFlightSegmentIds(new ArrayList<>());
							secondOnd.setFlightSegmentIds(new ArrayList<>());
							firstOnd.setRequestedOndSequence(originSequence);
							secondOnd.setRequestedOndSequence(originSequence);
							for (FlightSegmentDTO fltSegmentDTO : flightSegments) {
								if (firstOnd.getOrigin().equals(fltSegmentDTO.getFromAirport())
										&& firstOnd.getDestination().equals(fltSegmentDTO.getToAirport())) {
									firstOnd.getFlightSegmentIds().add(fltSegmentDTO.getSegmentId());
								} else if (secondOnd.getOrigin().equals(fltSegmentDTO.getFromAirport())
										&& secondOnd.getDestination().equals(fltSegmentDTO.getToAirport())) {
									secondOnd.getFlightSegmentIds().add(fltSegmentDTO.getSegmentId());
								}

							}
							// TODO split the flt segments, pnr segments, etc

							newOndList.add(firstOnd);
							newOndList.add(secondOnd);
						} else {
							newOndList.add(ondInfo);
						}
					} else if (!originAirport.isSurfaceSegment() && destinationAirport.isSurfaceSegment()) {
						String connectingAirport = destinationAirport.getConnectingAirport();
						if (connectingAirport != null && !ondInfo.getOrigin().equals(connectingAirport)) {
							isOnDSplitOperationInvoked = true;
							OriginDestinationInfoDTO firstOnd = ondInfo.clone();
							OriginDestinationInfoDTO secondOnd = ondInfo.clone();

							firstOnd.setDestination(connectingAirport);
							secondOnd.setOrigin(connectingAirport);
							secondOnd.setGroundSegmentOnly(false);

							Collection<FlightSegmentDTO> flightSegments = ReservationModuleUtils.getFlightBD()
									.getFlightSegments(ondInfo.getFlightSegmentIds());
							firstOnd.setFlightSegmentIds(new ArrayList<>());
							secondOnd.setFlightSegmentIds(new ArrayList<>());
							firstOnd.setRequestedOndSequence(originSequence);
							secondOnd.setRequestedOndSequence(originSequence);
							for (FlightSegmentDTO fltSegmentDTO : flightSegments) {
								if (firstOnd.getOrigin().equals(fltSegmentDTO.getFromAirport())
										&& firstOnd.getDestination().equals(fltSegmentDTO.getToAirport())) {
									firstOnd.getFlightSegmentIds().add(fltSegmentDTO.getSegmentId());
								}
								if (secondOnd.getOrigin().equals(fltSegmentDTO.getFromAirport())
										&& secondOnd.getDestination().equals(fltSegmentDTO.getToAirport())) {
									secondOnd.getFlightSegmentIds().add(fltSegmentDTO.getSegmentId());
								}

							}
							// TODO split the flt segments, pnr segments, etc

							newOndList.add(firstOnd);
							newOndList.add(secondOnd);
						} else {
							newOndList.add(ondInfo);
						}
					} else {
						newOndList.add(ondInfo);
					}

				}
				originSequence++;
			}

			avifltSearchDTO.getOriginDestinationInfoDTOs().clear();
			avifltSearchDTO.getOriginDestinationInfoDTOs().addAll(newOndList);
			avifltSearchDTO.setOndSplitOperationInvoked(isOnDSplitOperationInvoked);
		}
	}

	public static void updateSplitONDListOnAvailRQ(AvailableFlightSearchDTO avifltSearchDTO, FareAvailRQ fareAvailRQ)
			throws ModuleException {
		if (fareAvailRQ != null && avifltSearchDTO != null && fareAvailRQ.getOriginDestinationInformationList() != null
				&& avifltSearchDTO.getOriginDestinationInfoDTOs() != null) {
			if (fareAvailRQ.getOrderedOriginDestinationInformationList().size() < avifltSearchDTO.getOrderedOriginDestinations()
					.size()) {

				List<OriginDestinationInfoDTO> fltList = new ArrayList<>(avifltSearchDTO.getOrderedOriginDestinations());

				Iterator<OriginDestinationInfoDTO> availRQondItr = null;
				List<OriginDestinationInformationTO> ondNewList = new ArrayList<>();
				List<OriginDestinationInformationTO> nonMatchedList = new ArrayList<>();
				for (OriginDestinationInformationTO ondInfo : fareAvailRQ.getOrderedOriginDestinationInformationList()) {
					availRQondItr = fltList.iterator();
					boolean found = false;
					while (availRQondItr.hasNext()) {
						OriginDestinationInfoDTO ondOption = availRQondItr.next();
						if (ondInfo.getOrigin().equals(ondOption.getOrigin())
								&& ondInfo.getDestination().equals(ondOption.getDestination())
								&& ondOption.getDepartureDateTimeStart().compareTo(ondInfo.getDepartureDateTimeStart()) >= 0
								&& ondOption.getDepartureDateTimeStart().compareTo(ondInfo.getDepartureDateTimeEnd()) <= 0) {
							ondNewList.add(ondInfo);
							availRQondItr.remove();
							found = true;
							break;
						}
					}

					if (!found) {
						nonMatchedList.add(ondInfo);
					}

				}
				List<OriginDestinationInfoDTO> selectedFlts = new ArrayList<>(avifltSearchDTO.getOrderedOriginDestinations());

				for (OriginDestinationInfoDTO ondOption : selectedFlts) {
					for (OriginDestinationInformationTO ondInfo : nonMatchedList) {
						if ((ondInfo.getOrigin().equals(ondOption.getOrigin())
								|| ondInfo.getDestination().equals(ondOption.getDestination()))
								&& ondOption.getDepartureDateTimeStart().compareTo(ondInfo.getDepartureDateTimeStart()) >= 0
								&& ondOption.getDepartureDateTimeEnd().compareTo(ondInfo.getDepartureDateTimeEnd()) <= 0) {
							OriginDestinationInformationTO newOnd = ondInfo.cloneForSplit();

							newOnd.setOrigin(ondOption.getOrigin());
							newOnd.setDestination(ondOption.getDestination());
							List<Integer> fltSegIds = ondOption.getFlightSegmentIds();

							for (OriginDestinationOptionTO option : ondInfo.getOrignDestinationOptions()) {
								OriginDestinationOptionTO newOption = new OriginDestinationOptionTO();
								for (FlightSegmentTO fltSeg : option.getFlightSegmentList()) {

									Integer fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltSeg.getFlightRefNumber());
									if (fltSegIds.contains(fltSegId)) {
										newOption.getFlightSegmentList().add(fltSeg);
										newOption.getFlightSegIdList().add(fltSegId);
									}
								}
								if (newOption.getFlightSegmentList().size() > 0) {
									newOnd.getOrignDestinationOptions().add(newOption);
								}
							}
							ondNewList.add(newOnd);
							break;
						}
					}
				}

				if (ondNewList.size() == avifltSearchDTO.getOrderedOriginDestinations().size()) {
					fareAvailRQ.getOriginDestinationInformationList().clear();
					fareAvailRQ.getOriginDestinationInformationList().addAll(ondNewList);
					fareAvailRQ.getOrderedOriginDestinationInformationList();
				}
			}
		}

	}

	public static void mergePossibleOnD(AvailableFlightSearchDTO avifltSearchDTO) throws ModuleException {

		if (AppSysParamsUtil.isConnRouteOnDMergeEnabled() && avifltSearchDTO != null
				&& avifltSearchDTO.getOrderedOriginDestinations() != null
				&& avifltSearchDTO.getOrderedOriginDestinations().size() > 1) {
			OriginDestinationInfoDTO prevOnd = null;
			Set<String> hubAirports = CommonsServices.getGlobalConfig().getHubAirports();
			List<OriginDestinationInfoDTO> updatedOndList = new ArrayList<OriginDestinationInfoDTO>();
			List<OriginDestinationInfoDTO> removeOndList = new ArrayList<OriginDestinationInfoDTO>();
			Iterator<OriginDestinationInfoDTO> orderedOnDItr = avifltSearchDTO.getOrderedOriginDestinations().iterator();
			boolean isAnyOndMerged = false;
			while (orderedOnDItr.hasNext()) {
				OriginDestinationInfoDTO ondInfo = orderedOnDItr.next();

				if (!ondInfo.isFlownOnd()) {
					if (prevOnd != null) {
						if (ondInfo.getOrigin().equals(prevOnd.getDestination())
								&& !ondInfo.getDestination().equals(prevOnd.getOrigin())
								&& hubAirports.contains(ondInfo.getOrigin()) && isOnDPreferenceAreSame(prevOnd, ondInfo)) {

							ArrayList<Integer> arrFlightSegId = new ArrayList<Integer>();
							arrFlightSegId.addAll(prevOnd.getFlightSegmentIds());
							arrFlightSegId.addAll(ondInfo.getFlightSegmentIds());

							Collection<FlightSegmentDTO> flightSegments = ReservationModuleUtils.getFlightBD()
									.getFlightSegments(arrFlightSegId);
							FlightSegmentDTO prevFlightSegmentDTO = null;
							FlightSegmentDTO currentFlightSegmentDTO = null;
							for (FlightSegmentDTO fltSegmentDTO : flightSegments) {

								if (prevOnd.getFlightSegmentIds().contains(fltSegmentDTO.getSegmentId())) {

									if (prevFlightSegmentDTO == null || prevFlightSegmentDTO != null
											&& CalendarUtil.isGreaterThan(fltSegmentDTO.getArrivalDateTimeZulu(),
													prevFlightSegmentDTO.getArrivalDateTimeZulu())) {
										prevFlightSegmentDTO = fltSegmentDTO;
									}

								} else if (ondInfo.getFlightSegmentIds().contains(fltSegmentDTO.getSegmentId())) {

									if (currentFlightSegmentDTO == null || currentFlightSegmentDTO != null
											&& CalendarUtil.isLessThan(fltSegmentDTO.getArrivalDateTimeZulu(),
													currentFlightSegmentDTO.getArrivalDateTimeZulu())) {
										currentFlightSegmentDTO = fltSegmentDTO;
									}
								}
							}

							String[] minMaxTransitDurations = CommonsServices.getGlobalConfig()
									.getMixMaxTransitDurations(ondInfo.getOrigin(), null, null);

							String[] minTransitHoursMin = minMaxTransitDurations[0].split(":");
							GregorianCalendar minCal = new GregorianCalendar();
							minCal.setTime(prevFlightSegmentDTO.getArrivalDateTime());
							minCal.add(GregorianCalendar.HOUR, Integer.parseInt(minTransitHoursMin[0]));
							minCal.add(GregorianCalendar.MINUTE, Integer.parseInt(minTransitHoursMin[1]));

							GregorianCalendar nextSegDep = new GregorianCalendar();
							nextSegDep.setTime(currentFlightSegmentDTO.getDepartureDateTime());

							if (nextSegDep.getTimeInMillis() >= minCal.getTimeInMillis()) {
								// possible for OND merge
								// calculate stop over days??

								String segmentCode = prevOnd.getOrigin() + "/" + ondInfo.getOrigin() + "/"
										+ ondInfo.getDestination();

								boolean isValidRoute = ReservationModuleUtils.getFlightInventoryResBD()
										.isValidConnectionRoute(segmentCode);

								if (isValidRoute) {
									OriginDestinationInfoDTO mergedOndInfo = prevOnd.clone();
									mergedOndInfo.setDestination(ondInfo.getDestination());
									mergedOndInfo.setUnTouchedOnd(false);

									Map<String, Integer> hubDetailMap = new HashMap<String, Integer>();

									int stopOverDays = CalendarUtil.daysUntil(prevOnd.getDepartureDateTimeEnd(),
											ondInfo.getDepartureDateTimeEnd());

									hubDetailMap.put(ondInfo.getOrigin(), stopOverDays);
									mergedOndInfo.getFlightSegmentIds().addAll(ondInfo.getFlightSegmentIds());
									mergedOndInfo.setHubTimeDetailMap(hubDetailMap);

									updatedOndList.add(mergedOndInfo);

									removeOndList.add(prevOnd);
									removeOndList.add(ondInfo);

									isAnyOndMerged = true;
								}

							}

						}
					}
					prevOnd = ondInfo;
				}

			}

			if (isAnyOndMerged) {
				avifltSearchDTO.getOriginDestinationInfoDTOs().addAll(updatedOndList);
				avifltSearchDTO.getOriginDestinationInfoDTOs().removeAll(removeOndList);
			}

		}

	}

	public static void updateMergedONDListOnAvailRQ(AvailableFlightSearchDTO avifltSearchDTO, FareAvailRQ fareAvailRQ)
			throws ModuleException {
		if (fareAvailRQ != null && avifltSearchDTO != null && fareAvailRQ.getOriginDestinationInformationList() != null
				&& avifltSearchDTO.getOriginDestinationInfoDTOs() != null && AppSysParamsUtil.isConnRouteOnDMergeEnabled()) {
			if (fareAvailRQ.getOrderedOriginDestinationInformationList().size() != avifltSearchDTO.getOrderedOriginDestinations()
					.size()) {

				List<OriginDestinationInformationTO> updatedOndList = new ArrayList<OriginDestinationInformationTO>();
				List<OriginDestinationInformationTO> removeOndList = new ArrayList<OriginDestinationInformationTO>();
				boolean isAnyOndMerged = false;
				for (OriginDestinationInfoDTO ondInfoDTO : avifltSearchDTO.getOrderedOriginDestinations()) {

					if (ondInfoDTO.getHubTimeDetailMap() != null && ondInfoDTO.getHubTimeDetailMap().size() > 0) {
						int i = 0;
						OriginDestinationInformationTO nextOndTO = null;
						for (OriginDestinationInformationTO ondInfoTO : fareAvailRQ
								.getOrderedOriginDestinationInformationList()) {
							i++;
							List<Integer> fltSegIdList = OndConvertAssembler
									.extractSegIdsFromOnds(ondInfoTO.getOrignDestinationOptions());

							if (ondInfoDTO.getFlightSegmentIds().containsAll(fltSegIdList)
									&& ondInfoDTO.getFlightSegmentIds().size() > fltSegIdList.size()
									&& !removeOndList.contains(ondInfoTO)
									&& i < fareAvailRQ.getOrderedOriginDestinationInformationList().size()) {

								if (fareAvailRQ.getOrderedOriginDestinationInformationList().get(i) != null) {
									nextOndTO = fareAvailRQ.getOrderedOriginDestinationInformationList().get(i);

									OriginDestinationInformationTO mergedOndInfo = ondInfoTO.ondMerge(nextOndTO);
									mergedOndInfo.setHubTimeDetailMap(ondInfoDTO.getHubTimeDetailMap());

									updatedOndList.add(mergedOndInfo);

									removeOndList.add(nextOndTO);
									removeOndList.add(ondInfoTO);

									isAnyOndMerged = true;
									nextOndTO = null;
									break;
								}

							}

						}
					}

				}

				if (isAnyOndMerged) {
					fareAvailRQ.getOriginDestinationInformationList().addAll(updatedOndList);
					fareAvailRQ.getOriginDestinationInformationList().removeAll(removeOndList);
				}

			}
		}
	}

	private static boolean isOnDPreferenceAreSame(OriginDestinationInfoDTO prevOnd, OriginDestinationInfoDTO ondInfo) {
		if (((prevOnd.getPreferredClassOfService() != null
				&& prevOnd.getPreferredClassOfService().equals(ondInfo.getPreferredClassOfService()))
				|| (prevOnd.getPreferredClassOfService() == null && ondInfo.getPreferredClassOfService() == null))
				&& ((prevOnd.getPreferredBookingClass() != null
						&& prevOnd.getPreferredBookingClass().equals(ondInfo.getPreferredBookingClass()))
						|| (prevOnd.getPreferredBookingClass() == null && ondInfo.getPreferredBookingClass() == null))
				&& ((prevOnd.getPreferredLogicalCabin() != null
						&& prevOnd.getPreferredLogicalCabin().equals(ondInfo.getPreferredLogicalCabin()))
						|| (prevOnd.getPreferredLogicalCabin() == null && ondInfo.getPreferredLogicalCabin() == null))) {
			return true;
		}
		return false;
	}

	public static boolean isChargeReferenceRequired(String chargeGroupCode, String chargeCode) {
		if (!StringUtil.isNullOrEmpty(chargeGroupCode)) {
			if (!StringUtil.isNullOrEmpty(chargeCode) && (ReservationInternalConstants.ChargeGroup.ADJ.equals(chargeGroupCode)
					&& ChargeCodes.FARE_ADJUSTMENTS.equals(chargeCode))) {
				return true;
			} else if (ReservationInternalConstants.ChargeGroup.DISCOUNT.equals(chargeGroupCode)) {
				return true;
			}
		}
		return false;
	}

	public static void mapStationsBySegments(Collection<ReservationSegment> pnrSegments,
			Map<String, String> groundSegmentByAirFlightSegmentID) throws NumberFormatException, ModuleException {
		Calendar airSegmentArrivalDateTime = Calendar.getInstance();
		Calendar groundSegmentArrivalDateTime = Calendar.getInstance();
		if (!AppSysParamsUtil.isGroundServiceEnabled()) {
			return;
		}

		Collection<Integer> flightSegmentIds = new ArrayList<Integer>();

		for (ReservationSegment reservationSegment : pnrSegments) {
			flightSegmentIds.add(reservationSegment.getFlightSegId());
		}

		Collection<FlightSegmentDTO> flightSegmentDetails = ReservationModuleUtils.getFlightBD()
				.getFlightSegments(flightSegmentIds);

		List<FlightSegmentDTO> filteredGroundSegments = new ArrayList<FlightSegmentDTO>();
		List<FlightSegmentDTO> filteredAirSegments = new ArrayList<FlightSegmentDTO>();

		for (FlightSegmentDTO flightSegmentDTO : flightSegmentDetails) {
			Collection<String> airports = Arrays.asList(flightSegmentDTO.getSegmentCode().split("/"));
			if (ReservationModuleUtils.getAirportBD().isContainGroundSegment(airports)) {
				filteredGroundSegments.add(flightSegmentDTO);
			} else {
				filteredAirSegments.add(flightSegmentDTO);
			}
		}

		if (!filteredGroundSegments.isEmpty() && !filteredAirSegments.isEmpty()) {
			Collections.sort(filteredAirSegments);
			Collections.sort(filteredGroundSegments);
			for (FlightSegmentDTO airSegmentDTO : filteredAirSegments) {
				Iterator<FlightSegmentDTO> groundSegIte = filteredGroundSegments.iterator();
				if (groundSegIte.hasNext()) {
					FlightSegmentDTO groundSegmentDTO = groundSegIte.next();
					airSegmentArrivalDateTime.setTime(airSegmentDTO.getArrivalDateTimeZulu());
					groundSegmentArrivalDateTime.setTime(groundSegmentDTO.getArrivalDateTimeZulu());
					airSegmentArrivalDateTime.add(Calendar.HOUR, 24);
					groundSegmentArrivalDateTime.add(Calendar.HOUR, 24);

					if (!groundSegmentByAirFlightSegmentID.isEmpty()
							&& groundSegmentByAirFlightSegmentID.containsValue(groundSegmentDTO.getSegmentId().toString())) {
						groundSegIte.remove();
						continue;

					}

					if (!filteredGroundSegments.isEmpty() && ((airSegmentDTO.getDepartureDateTimeZulu()
							.after(groundSegmentDTO.getArrivalDateTimeZulu())
							&& airSegmentDTO.getDepartureAirportCode().equals(groundSegmentDTO.getArrivalAirportCode())
							&& groundSegmentArrivalDateTime.getTime().after(airSegmentDTO.getDepartureDateTimeZulu()))
							|| (groundSegmentDTO.getDepartureDateTimeZulu().after(airSegmentDTO.getArrivalDateTimeZulu())
									&& groundSegmentDTO.getDepartureAirportCode().equals(airSegmentDTO.getArrivalAirportCode())
									&& airSegmentArrivalDateTime.getTime().after(groundSegmentDTO.getDepartureDateTimeZulu())))) {
						groundSegmentByAirFlightSegmentID.put(airSegmentDTO.getSegmentId().toString(),
								groundSegmentDTO.getSegmentId().toString());
						groundSegIte.remove();
					}
				}
			}
		}
	}

	public static FlightSegmentDTO[] sortFlightSegmentByDepDate(Collection<FlightSegmentDTO> setFlightSegments) {
		int size = setFlightSegments.size();
		FlightSegmentDTO[] arrayToSort = setFlightSegments.toArray(new FlightSegmentDTO[size]);
		Arrays.sort(arrayToSort, new Comparator<FlightSegmentDTO>() {
			@Override
			public int compare(FlightSegmentDTO rs1, FlightSegmentDTO rs2) {
				return rs1.getDepartureDateTimeZulu().compareTo(rs2.getDepartureDateTimeZulu());
			}
		});

		return arrayToSort;
	}

	private static String returnGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (Iterator<CachedAirportDTO> iterator = airports.iterator(); iterator.hasNext();) {
			CachedAirportDTO airport = iterator.next();
			if (airport.isSurfaceSegment()) {
				return airport.getAirportCode();
			}
		}
		return null;
	}

	public static void updateGroundSegmentReference(Collection<ReservationSegment> pnrSegments,
			Map<String, String> groundFltSegByFltSegIdMap) throws ModuleException {

		mapStationsBySegments(pnrSegments, groundFltSegByFltSegIdMap);

		if (pnrSegments != null && pnrSegments.size() > 0 && groundFltSegByFltSegIdMap != null
				&& groundFltSegByFltSegIdMap.size() > 0) {
			for (String key : groundFltSegByFltSegIdMap.keySet()) {

				if (!StringUtil.isNullOrEmpty(key)) {
					String busSegId = groundFltSegByFltSegIdMap.get(key);
					if (!StringUtil.isNullOrEmpty(busSegId)) {
						ReservationSegment flightSegment = getNewReservationSegmentByFltSegId(pnrSegments, new Integer(key));
						ReservationSegment busSegment = getNewReservationSegmentByFltSegId(pnrSegments, new Integer(busSegId));

						if (flightSegment != null && busSegment != null) {
							String subStation = null;

							ArrayList<Integer> arrFlightSegId = new ArrayList<Integer>();
							arrFlightSegId.add(busSegment.getFlightSegId());

							Collection<FlightSegmentDTO> flightSegments = ReservationModuleUtils.getFlightBD()
									.getFlightSegments(arrFlightSegId);

							FlightSegmentDTO fltSegDTO = flightSegments.iterator().next();

							String airports[] = fltSegDTO.getSegmentCode().split("/");

							Collection<String> airportCodeColl = new ArrayList<String>();
							airportCodeColl.add(airports[0]);
							if (ReservationModuleUtils.getAirportBD().isContainGroundSegment(airportCodeColl)) {
								subStation = airports[0];
							}

							if (subStation == null) {
								airportCodeColl = new ArrayList<String>();
								airportCodeColl.add(airports[airports.length - 1]);
								if (ReservationModuleUtils.getAirportBD().isContainGroundSegment(airportCodeColl)) {
									subStation = airports[airports.length - 1];
								}
							}

							if (flightSegment != null && busSegment != null && subStation != null) {
								flightSegment.setGroundSegment(busSegment);
								busSegment.setSubStationShortName(subStation);
							}
						}

					}
				}

			}
		}
	}

	private static ReservationSegment getNewReservationSegmentByFltSegId(Collection<ReservationSegment> pnrSegments,
			Integer flightSegId) {
		for (ReservationSegment resSegment : pnrSegments) {
			if (resSegment.getFlightSegId().equals(flightSegId)) {
				return resSegment;
			}
		}
		return null;
	}

	public static void updateQuoteOndFlexi(FareAvailRQ fareAvailRQ, int removedOndSequence) {

		if (fareAvailRQ != null && fareAvailRQ.getAvailPreferences() != null) {

			if (fareAvailRQ.getAvailPreferences().getQuoteOndFlexi() != null
					&& fareAvailRQ.getAvailPreferences().getQuoteOndFlexi().size() > 0) {
				Map<Integer, Boolean> quoteOndFlexi = fareAvailRQ.getAvailPreferences().getQuoteOndFlexi();
				boolean isOndRemoved = false;
				if (quoteOndFlexi.get(removedOndSequence) != null) {
					quoteOndFlexi.remove(removedOndSequence);
					isOndRemoved = true;
				}

				int newOndSequence = 0;
				Map<Integer, Boolean> updatedQuoteOndFlexi = new HashMap<Integer, Boolean>();

				for (Integer oldOndSequence : quoteOndFlexi.keySet()) {
					updatedQuoteOndFlexi.put(newOndSequence, quoteOndFlexi.get(oldOndSequence));
					newOndSequence++;
				}
				if (isOndRemoved) {
					fareAvailRQ.getAvailPreferences().setQuoteOndFlexi(updatedQuoteOndFlexi);
				}

			}

			if (fareAvailRQ.getAvailPreferences().getOndFlexiSelected() != null
					&& fareAvailRQ.getAvailPreferences().getOndFlexiSelected().size() > 0) {
				Map<Integer, Boolean> ondFlexiSelected = fareAvailRQ.getAvailPreferences().getOndFlexiSelected();
				boolean isOndRemoved = false;
				if (ondFlexiSelected.get(removedOndSequence) != null) {
					ondFlexiSelected.remove(removedOndSequence);
					isOndRemoved = true;
				}
				int newOndSequence = 0;
				Map<Integer, Boolean> updatedOndFlexiSelected = new HashMap<Integer, Boolean>();
				for (Integer oldOndSequence : ondFlexiSelected.keySet()) {
					updatedOndFlexiSelected.put(newOndSequence, ondFlexiSelected.get(oldOndSequence));
					newOndSequence++;
				}
				if (isOndRemoved) {
					fareAvailRQ.getAvailPreferences().setOndFlexiSelected(updatedOndFlexiSelected);
				}

			}

		}

	}

	public static boolean isGdsReservation(Reservation reservation) {
		return reservation.getExternalRecordLocator() != null && !reservation.getExternalRecordLocator().isEmpty();
	}

	public static boolean[] isOpenJawReturningFromSameCountry(OriginDestinationInfoDTO outBoundLastOnd,
			OriginDestinationInfoDTO inBoundFirstOnd) {
		boolean returningSameCountryArray[] = new boolean[2];
		boolean isSingleOJReturnFromSameCountry = true;
		boolean isDoubleOJReturnFromSameCountry = true;
		if (outBoundLastOnd != null && inBoundFirstOnd != null) {
			boolean returningFromSameCountry = false;
			Object[] jourStopInfo = CommonsServices.getGlobalConfig().getAirportInfo(outBoundLastOnd.getDestination(), false);
			Object[] jourStartInfo = CommonsServices.getGlobalConfig().getAirportInfo(inBoundFirstOnd.getOrigin(), false);

			if (jourStopInfo != null && jourStartInfo != null) {
				returningFromSameCountry = jourStopInfo[3].equals(jourStartInfo[3]);
			}

			if (AppSysParamsUtil.isReturnFromSameCountryDoubleOpenJaw()) {
				isDoubleOJReturnFromSameCountry = false;
				if (returningFromSameCountry) {
					isDoubleOJReturnFromSameCountry = true;
				}
			}

			if (AppSysParamsUtil.isReturnFromSameCountrySingleOpenJaw()) {
				isSingleOJReturnFromSameCountry = false;
				if (returningFromSameCountry) {
					isSingleOJReturnFromSameCountry = true;
				}
			}

		}

		returningSameCountryArray[0] = isSingleOJReturnFromSameCountry;
		returningSameCountryArray[1] = isDoubleOJReturnFromSameCountry;

		return returningSameCountryArray;
	}

	public static void validateSkipSameFareQuote(AvailableFlightSearchDTO avifltSearchDTO) throws ModuleException {

		if (avifltSearchDTO != null && avifltSearchDTO.isRequoteFlightSearch()
				&& !StringUtil.isNullOrEmpty(avifltSearchDTO.getPnr())) {
			boolean isGroundServiceEnabled = AppSysParamsUtil.isGroundServiceEnabled();
			Collection<ReservationSegmentDTO> pnrSegmentColl = ReservationModuleUtils.getSegmentBD()
					.getSegmentsView(avifltSearchDTO.getPnr());

			if (pnrSegmentColl != null && pnrSegmentColl.size() > 0) {

				int currentOndCount = 0;
				boolean foundNewRoute = false;
				boolean foundNewFlight = false;
				boolean ondOrderChanged = false;
				// when all air segments are untouched, action is only with ground segment[Add Bus Segment/Cancel Bus
				// segment]
				// should re-quote same fare for the air segments.
				boolean quoteBusSegmentOnly = false;
				boolean foundModifiedAirSegment = false;
				List<String> segCodeList = new ArrayList<String>();
				List<Integer> fltSegIdList = new ArrayList<Integer>();
				List<Integer> airSegOnlyIdList = new ArrayList<Integer>();
				List<Integer> quoteFltSegIdList = new ArrayList<Integer>();
				List<Integer> groundFltIdList = new ArrayList<Integer>();
				Map<Integer, List<ReservationSegmentDTO>> tmpOndGroupCNFSegmentMap = new HashMap<Integer, List<ReservationSegmentDTO>>();

				Iterator<ReservationSegmentDTO> pnrSegmentItr = pnrSegmentColl.iterator();
				while (pnrSegmentItr.hasNext()) {
					ReservationSegmentDTO resSegDTO = pnrSegmentItr.next();

					if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSegDTO.getStatus())
							&& StringUtil.isNullOrEmpty(resSegDTO.getSubStatus())) {
						segCodeList.add(resSegDTO.getSegmentCode());
						fltSegIdList.add(resSegDTO.getFlightSegId());

						if (isGroundServiceEnabled && !isGroundSegment(resSegDTO.getOperationTypeID())) {
							airSegOnlyIdList.add(resSegDTO.getFlightSegId());
						}

						if (tmpOndGroupCNFSegmentMap.containsKey(resSegDTO.getFareGroupId())) {
							tmpOndGroupCNFSegmentMap.get(resSegDTO.getFareGroupId()).add(resSegDTO);
						} else {
							List<ReservationSegmentDTO> segments = new ArrayList<ReservationSegmentDTO>();
							segments.add(resSegDTO);
							tmpOndGroupCNFSegmentMap.put(resSegDTO.getFareGroupId(), segments);
						}
					} else {
						pnrSegmentItr.remove();
					}

				}

				Map<Integer, List<ReservationSegmentDTO>> ondGroupCNFSegmentMap = new HashMap<Integer, List<ReservationSegmentDTO>>();
				int dummyIndex = 0;
				for (Integer fareGroupId : tmpOndGroupCNFSegmentMap.keySet()) {
					ondGroupCNFSegmentMap.put(dummyIndex, tmpOndGroupCNFSegmentMap.get(fareGroupId));
					dummyIndex++;
				}

				if (pnrSegmentColl != null && pnrSegmentColl.size() > 0) {
					currentOndCount = ondGroupCNFSegmentMap.size();

					int currentIndex = 0;
					for (OriginDestinationInfoDTO ondInfoTo : avifltSearchDTO.getOrderedOriginDestinations()) {
						String segCode = ondInfoTo.getOrigin() + "/" + ondInfoTo.getDestination();
						if (ondInfoTo.getFlightSegmentIds().size() == 1 && !segCodeList.contains(segCode)) {
							foundNewRoute = true;
						} else if (ondInfoTo.getFlightSegmentIds().size() > 1 && !checkRouteInSegCodes(segCodeList, segCode)) {
							foundNewRoute = true;
						}

						if (!fltSegIdList.containsAll(ondInfoTo.getFlightSegmentIds())) {
							foundNewFlight = true;
						}

						if (!ondOrderChanged && avifltSearchDTO.getOrderedOriginDestinations().size() == currentOndCount
								&& foundNewFlight) {
							List<ReservationSegmentDTO> pnrSegments = ondGroupCNFSegmentMap.get(currentIndex);
							ReservationSegmentDTO resSegDTO = pnrSegments.get(0);
							int segmentCount = pnrSegments.size();
							if (segmentCount == 1 && !segCode.equals(resSegDTO.getSegmentCode())) {
								ondOrderChanged = true;
							} else if (segmentCount > 1
									&& !(segCode.startsWith(pnrSegments.get(0).getOrigin()) && segCode.endsWith(pnrSegments.get(
											segmentCount - 1).getDestination()))) {
								ondOrderChanged = true;
							}

						}

						if (isGroundServiceEnabled) {
							// Check is only Ground Segment should be quoted
							boolean isBusSegment = false;
							quoteFltSegIdList.addAll(ondInfoTo.getFlightSegmentIds());

							if (ondInfoTo.getFlightSegmentIds() != null && ondInfoTo.getFlightSegmentIds().size() == 1) {
								Collection<FlightSegmentDTO> flightSegments = ReservationModuleUtils.getFlightBD()
										.getFlightSegments(ondInfoTo.getFlightSegmentIds());

								for (FlightSegmentDTO flightSegmentDTO : flightSegments) {

									if (isGroundSegment(flightSegmentDTO.getOperationTypeID())) {
										isBusSegment = true;
										ondInfoTo.setGroundSegmentOnly(true);
										groundFltIdList.add(flightSegmentDTO.getFlightId());
									}

								}
							}

							if (!isBusSegment && !ondInfoTo.isUnTouchedOnd()) {
								foundModifiedAirSegment = true;
							}
						}
						currentIndex++;
					}
				}

				if (isGroundServiceEnabled && !foundModifiedAirSegment && quoteFltSegIdList.containsAll(airSegOnlyIdList)
						&& groundFltIdList.size() > 0) {
					quoteBusSegmentOnly = true;
				}

				if (!quoteBusSegmentOnly && !avifltSearchDTO.isFlownOnDExist()) {
					int ondListSize = avifltSearchDTO.getOrderedOriginDestinations().size();
					if ((ondListSize > currentOndCount) || (ondListSize <= currentOndCount && foundNewRoute)
							|| (ondListSize < currentOndCount && foundNewFlight)
							|| (ondListSize == currentOndCount && ondOrderChanged)) {
						avifltSearchDTO.setSkipSameFareBucketQuote(true);
					}
				}
			}

		}

	}
	
	public static Date getNonDepartedFirstSegmentDate(Reservation reservation) throws ModuleException {

		Date departingTimeZulu = null;
		Collection<ReservationSegmentDTO> segments = reservation.getSegmentsView();

		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();

		Set<ExternalPnrSegment> externalPnrSegments = reservation.getExternalReservationSegment();

		if (segments == null && externalPnrSegments == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}

		if (segments != null) {
			Collections.sort((List<ReservationSegmentDTO>) segments);

			for (ReservationSegmentDTO segment : segments) {
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())
						&& segment.getZuluDepartureDate().after(currentDate)) {
					departingTimeZulu = segment.getZuluDepartureDate();
					break;
				}
			}
		}

		if (externalPnrSegments != null && !externalPnrSegments.isEmpty()) {
			List<ExternalFlightSegment> externalFlightSegments = new ArrayList<ExternalFlightSegment>();

			for (ExternalPnrSegment externalPnrSegment : externalPnrSegments) {
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(externalPnrSegment.getStatus())
						&& externalPnrSegment.getExternalFlightSegment().getEstTimeDepatureZulu().after(currentDate)) {
					externalFlightSegments.add(externalPnrSegment.getExternalFlightSegment());
				}
			}

			if (!externalFlightSegments.isEmpty()) {
				Collections.sort(externalFlightSegments);
				if (departingTimeZulu == null
						|| (departingTimeZulu != null && departingTimeZulu.after(externalFlightSegments.get(0)
								.getEstTimeDepatureZulu()))) {
					departingTimeZulu = externalFlightSegments.get(0).getEstTimeDepatureZulu();
				}
			}
		}

		return departingTimeZulu;
	}

	private static boolean checkRouteInSegCodes(List<String> segCodeList, String segCode) {
		boolean foundOrigin = false;
		boolean foundDest = false;
		for (String tmpSegCode : segCodeList) {
			if (tmpSegCode.startsWith(segCode.substring(0, 3))) {
				foundOrigin = true;
			}
			if (tmpSegCode.endsWith(segCode.substring(4, segCode.length()))) {
				foundDest = true;
			}
		}
		if (foundDest && foundOrigin) {
			return true;
		}
		return false;
	}

	public static GDSStatusTO getGDSShareCarrierTO(Integer gdsId) {
		GDSStatusTO gdsStatusTO = null;
		if (gdsId != null) {
			Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				gdsStatusTO = gdsStatusMap.get(gdsId.toString());
			}
		}
		return gdsStatusTO;
	}

	public static boolean isCodeShareReservation(Integer gdsId) {
		boolean codeShareReservation = false;
		if (gdsId != null) {
			Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				codeShareReservation = gdsStatusMap.get(gdsId.toString()).isCodeShareCarrier();
			}
		}
		return codeShareReservation;
	}

	public static String getMarketingFlightData(String csFlightNo, String csBookingClass, String segmentCode,
			Date departuerDate) {
		StringBuilder marketingFlightInfo = new StringBuilder();
		if (!StringUtil.isNullOrEmpty(csFlightNo) && !StringUtil.isNullOrEmpty(csBookingClass)
				&& !StringUtil.isNullOrEmpty(segmentCode) && departuerDate != null && segmentCode.length() > 6) {

			segmentCode = segmentCode.substring(0, 3) + "" + segmentCode.substring(segmentCode.length() - 3);

			SimpleDateFormat df = new SimpleDateFormat("dd");
			SimpleDateFormat tf = new SimpleDateFormat("HHmm");
			marketingFlightInfo.append(csFlightNo);
			marketingFlightInfo.append(csBookingClass);
			marketingFlightInfo.append(df.format(departuerDate));
			marketingFlightInfo.append(segmentCode);
			marketingFlightInfo.append(tf.format(departuerDate));
			marketingFlightInfo.append("HK");

		}

		return marketingFlightInfo.toString();
	}

	private static boolean isGroundSegment(Integer operationTypeID) {
		if (operationTypeID != null && AirScheduleCustomConstants.OperationTypes.BUS_SERVICE == operationTypeID) {
			return true;
		}
		return false;
	}

	public static Collection<PaxChargesTO> transformPaxList(Collection<ReservationPax> paxCollection,
			DiscountedFareDetails discFareDetailsDTO) {
		Collection<PaxChargesTO> paxExtChargesList = new ArrayList<PaxChargesTO>();
		if (paxCollection != null && !paxCollection.isEmpty()) {

			for (ReservationPax resPaxTO : paxCollection) {

				String paxType = resPaxTO.getPaxType();
				if (PaxTypeTO.INFANT.equals(paxType)) {
					continue;
				}

				if (PaxTypeTO.PARENT.equals(paxType)) {
					paxType = PaxTypeTO.ADULT;
				}

				PaxChargesTO paxExternalChargesTO = new PaxChargesTO();
				paxExternalChargesTO.setPaxTypeCode(resPaxTO.getPaxType());
				paxExternalChargesTO.setPaxSequence(resPaxTO.getPaxSequence());

				if ((resPaxTO.getInfants() != null && !resPaxTO.getInfants().isEmpty())
						|| (resPaxTO.getIndexId() != null && resPaxTO.getIndexId() > 0)) {
					paxExternalChargesTO.setParent(true);
				}

				paxExtChargesList.add(paxExternalChargesTO);
			}
		}
		return paxExtChargesList;
	}

	public static PaxDiscountInfoDTO getPaxDiscountInfoDTO(boolean isCreditPromotion,
			Collection<DiscountChargeTO> discountChargeTOs, Integer rateId, String discountCode, double discountPercentage,
			String chargeCode, BigDecimal chargeAmount) {

		PaxDiscountInfoDTO paxDiscInfo = new PaxDiscountInfoDTO();
		paxDiscInfo.setCreditPromotion(isCreditPromotion);

		if (rateId != null && discountChargeTOs != null && !discountChargeTOs.isEmpty()) {

			for (DiscountChargeTO discountChargeTO : discountChargeTOs) {

				if (discountChargeTO.getRateId().equals(rateId) && discountChargeTO.getChargeCode().equals(chargeCode)
						&& discountChargeTO.getDiscountAmount().doubleValue() > 0
						&& !AccelAeroCalculator.isGreaterThan(discountChargeTO.getDiscountAmount(), chargeAmount)
						&& !discountChargeTO.isConsumed()) {
					paxDiscInfo.setDiscountApplied(true);
					paxDiscInfo.setDicountCode(discountCode);
					paxDiscInfo.setDiscount(discountChargeTO.getDiscountAmount().negate());
					paxDiscInfo.setDiscountPercentage(discountPercentage);
					discountChargeTO.setConsumed(true);
					break;

				}

			}

		}

		return paxDiscInfo;

	}

	public static String getChargeCode(ExternalChgDTO externalChgDTO) {
		String chargeCode = null;
		if (externalChgDTO != null) {
			if (externalChgDTO instanceof SMExternalChgDTO) {
				chargeCode = ((SMExternalChgDTO) externalChgDTO).getSeatNumber();
			} else if (externalChgDTO instanceof MealExternalChgDTO) {
				chargeCode = ((MealExternalChgDTO) externalChgDTO).getMealCode();
			} else if (externalChgDTO instanceof BaggageExternalChgDTO) {
				chargeCode = ((BaggageExternalChgDTO) externalChgDTO).getBaggageCode();
			} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
				// chargeCode = ((SSRExternalChargeDTO) externalChgDTO).getChargeCode();
				Integer ssrId = ((SSRExternalChargeDTO) externalChgDTO).getSSRId();
				chargeCode = SSRUtil.getSSRCode(ssrId);
			} else if (externalChgDTO instanceof InsuranceSegExtChgDTO
					|| externalChgDTO.getExternalChargesEnum() == EXTERNAL_CHARGES.INSURANCE) {
				chargeCode = "INS";
			} else {
				chargeCode = externalChgDTO.getChargeCode();
			}
		}

		return chargeCode;
	}

	public static ReservationDiscountDTO getApplicableDiscount(DiscountRQ promotionRQ, FareSegChargeTO fareSegChargeTO,
			BaseAvailRQ baseAvailRQ, SYSTEM searchSystem, String transactionIdentifier, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		ReservationDiscountDTO resDiscountDTO = null;

		if (promotionRQ != null && baseAvailRQ != null && promotionRQ.getDiscountInfoDTO() != null && searchSystem != null
				&& (SYSTEM.AA == searchSystem && fareSegChargeTO != null) || (SYSTEM.AA != searchSystem)) {

			QuotedFareRebuildDTO fareInfo = null;
			if (fareSegChargeTO != null) {
				fareInfo = new QuotedFareRebuildDTO(fareSegChargeTO, baseAvailRQ, promotionRQ.getDiscountInfoDTO());
			}

			if (StringUtil.isNullOrEmpty(transactionIdentifier)
					&& !StringUtil.isNullOrEmpty(baseAvailRQ.getTransactionIdentifier())) {
				transactionIdentifier = baseAvailRQ.getTransactionIdentifier();
			}

			promotionRQ.setSystem(searchSystem);
			promotionRQ.setQuotedFareRebuildDTO(fareInfo);
			promotionRQ.setTransactionIdentifier(transactionIdentifier);

			resDiscountDTO = ReservationModuleUtils.getAirproxyReservationBD().calculatePromotionDiscount(promotionRQ,
					trackInfoDTO);
		}
		if (resDiscountDTO == null) {
			resDiscountDTO = new ReservationDiscountDTO();
			resDiscountDTO.setTotalDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		return resDiscountDTO;
	}

	/**
	 * function to apply minus adjustment for exchanged ONDs when adjustment amount is > selected total OND charges
	 */
	private static BigDecimal adjustExchangedONDCharges(ReservationPaxFare reservationPaxFare, BigDecimal balanceAmount,
			Integer fltSegId, Map<Integer, BigDecimal> adjustedChargeAmount) {
		if (reservationPaxFare != null && balanceAmount != null && fltSegId != null && adjustedChargeAmount != null) {
			ReservationPax reservationPax = reservationPaxFare.getReservationPax();
			if (balanceAmount.doubleValue() > 0 && reservationPax != null && reservationPax.getPnrPaxFares() != null
					&& !reservationPax.getPnrPaxFares().isEmpty()) {
				Set<ReservationPaxFare> pnrPaxFares = reservationPax.getPnrPaxFares();
				for (ReservationPaxFare pnrPaxFare : pnrPaxFares) {
					if (balanceAmount.doubleValue() > 0) {
						if (reservationPaxFare.getPnrPaxFareId().equals(pnrPaxFare.getPnrPaxFareId())) {
							continue;
						}
						Set<ReservationPaxFareSegment> paxFareSegments = pnrPaxFare.getPaxFareSegments();
						for (ReservationPaxFareSegment paxFareSegment : paxFareSegments) {
							if (fltSegId.equals(paxFareSegment.getSegment().getFlightSegId())
									&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED
											.equals(paxFareSegment.getSegment().getSubStatus())) {
								BigDecimal totChargeTemp = pnrPaxFare.getTotalChargeAmount();
								if (totChargeTemp.doubleValue() > 0) {
									if (totChargeTemp.doubleValue() >= balanceAmount.doubleValue()) {
										adjustedChargeAmount.put(pnrPaxFare.getPnrPaxFareId(), balanceAmount.negate());
										balanceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
									} else {
										balanceAmount = AccelAeroCalculator.subtract(balanceAmount,
												pnrPaxFare.getTotalChargeAmount());
										adjustedChargeAmount.put(pnrPaxFare.getPnrPaxFareId(),
												pnrPaxFare.getTotalChargeAmount().negate());
									}
								}

							}
						}
					}

				}
			}
		}
		return balanceAmount;
	}

	public static boolean allowAddModifyAncillaryForGDS(Integer gdsId) {
		if (gdsId != null) {
			Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				return gdsStatusMap.get(gdsId.toString()).isAddAncillary();
			}
		}
		return false;
	}

	public static boolean allowExternalTicketExchange(Integer gdsId) {
		if (gdsId != null) {
			Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				boolean codeShareReservation = gdsStatusMap.get(gdsId.toString()).isCodeShareCarrier();
				if (!codeShareReservation) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isCodeShareCarrier(String carrierCode) {
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
			for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
				if (gdsStatusTO != null && gdsStatusTO.isCodeShareCarrier() && carrierCode.equals(gdsStatusTO.getCarrierCode())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isAccelAeroEtsIntegratedCSCarrier(String carrierCode) {
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		if (!StringUtil.isNullOrEmpty(carrierCode) && gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
			for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
				if (gdsStatusTO != null && carrierCode.equals(gdsStatusTO.getCarrierCode()) && gdsStatusTO.isCodeShareCarrier()) {

					return gdsStatusTO.isUseAeroMartETS();
				}
			}
		}
		return false;
	}
	
	/**
	 * used to regenerate discount info
	 * 
	 * can be used only, when invoking refund operation where actually we don't know which charge is being refunded
	 * following function will give given charge discount info
	 */
	public static PaxDiscountInfoDTO getAppliedDiscountInfoForRefund(ReservationPaxFare reservationPaxFare,
			ExternalChgDTO externalChgDTO, Collection<Integer> consumedPaxOndChgIds) {
		BigDecimal appliedDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		PaxDiscountInfoDTO paxDiscInfo = null;
		if (externalChgDTO != null && externalChgDTO.getAmount().doubleValue() < 0 && reservationPaxFare != null
				&& reservationPaxFare.getCharges() != null && !reservationPaxFare.getCharges().isEmpty()) {

			if (reservationPaxFare.getTotalDiscount() != null && reservationPaxFare.getTotalDiscount().doubleValue() > 0) {

				Set<ReservationPaxOndCharge> charges = reservationPaxFare.getCharges();
				if (consumedPaxOndChgIds == null) {
					consumedPaxOndChgIds = new ArrayList<Integer>();
				}

				int chargeRateId = externalChgDTO.getChgRateId();
				BigDecimal chargeAmount = externalChgDTO.getAmount();

				BigDecimal totalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
				Collection<ReservationPaxOndCharge> filteredCharges = new ArrayList<ReservationPaxOndCharge>();
				for (ReservationPaxOndCharge paxOndCharge : charges) {
					if (paxOndCharge.getChargeRateId() != null && paxOndCharge.getChargeRateId().equals(chargeRateId)
							&& paxOndCharge.getAmount().abs().doubleValue() == chargeAmount.abs().doubleValue()
							&& paxOndCharge.getDiscount() != null && paxOndCharge.getDiscount().doubleValue() != 0) {

						totalCharge = AccelAeroCalculator.add(totalCharge, paxOndCharge.getAmount());
						totalDiscount = AccelAeroCalculator.add(totalDiscount, paxOndCharge.getDiscount());

						filteredCharges.add(paxOndCharge);

					}

				}

				if (totalDiscount.doubleValue() < 0) {

					boolean isValidCharge = true;
					for (ReservationPaxOndCharge filteredCharge : filteredCharges) {
						if (filteredCharge.getAmount().doubleValue() > 0) {
							for (ReservationPaxOndCharge paxOndCharge : filteredCharges) {
								if (paxOndCharge.getAmount().doubleValue() < 0) {
									if (AccelAeroCalculator.add(filteredCharge.getAmount(),
											paxOndCharge.getAmount()) == AccelAeroCalculator.getDefaultBigDecimalZero()) {
										// already refunded
										isValidCharge = false;
									} else {
										isValidCharge = true;
									}
								}

							}
						}

						// re-check already refunded for another charges
						if (isValidCharge && !consumedPaxOndChgIds.contains(filteredCharge.getPnrPaxOndChgId())) {
							consumedPaxOndChgIds.add(filteredCharge.getPnrPaxOndChgId());
							appliedDiscount = filteredCharge.getDiscount().abs();
							break;
						}

					}

					if (appliedDiscount.doubleValue() > 0) {

						paxDiscInfo = new PaxDiscountInfoDTO();
						paxDiscInfo.setCreditPromotion(false);
						paxDiscInfo.setDiscountApplied(true);
						paxDiscInfo.setDicountCode(reservationPaxFare.getDiscountCode());
						paxDiscInfo.setDiscount(appliedDiscount);

					}

				}

			}

		}

		return paxDiscInfo;

	}

	public static boolean hasInfantOnlyAmountDue(LCCClientReservationPax pax, boolean isInfantPaymentSeparated) {

		if (isInfantPaymentSeparated && pax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) <= 0
				&& pax.getPaxType().equals(PaxTypeTO.ADULT) && pax.getInfants() != null && pax.getInfants().size() > 0) {
			for (LCCClientReservationPax infantPax : pax.getInfants()) {
				if (infantPax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) > 0) {
					return true;
				}
			}
		}

		return false;
	}

	public static BigDecimal getAmountDueWhenOnlyInfantHasAmountDue(LCCClientReservationPax pax,
			boolean isInfantPaymentSeparated) {
		// only infant has Balance to pay
		BigDecimal amountDue = pax.getTotalAvailableBalance();
		if (hasInfantOnlyAmountDue(pax, isInfantPaymentSeparated)) {
			for (LCCClientReservationPax infantPax : pax.getInfants()) {
				if (infantPax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) > 0) {
					amountDue = infantPax.getTotalAvailableBalance();
				}
			}
		}

		return amountDue;
	}
	
	public static ServiceTaxQuoteForTicketingRevenueRQ createPaxWiseTicketingServiceTaxQuoteRQForPenalty(BigDecimal penaltyCharge, FlownAssitUnit flownAsstUnit,
			ReservationPax reservationPax, boolean isOnlyCancellation) throws ModuleException {
		ServiceTaxQuoteForTicketingRevenueRQ request = new ServiceTaxQuoteForTicketingRevenueRQ();

		Reservation reservation = reservationPax.getReservation();

		List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = new ArrayList<SimplifiedFlightSegmentDTO>();
		for (OndFareDTO ondFare : flownAsstUnit.getOndFares()) {
			Map<Integer, SegmentSeatDistsDTO> fltSegIdSD = new HashMap<>();
			for (SegmentSeatDistsDTO segSeatDist : ondFare.getSegmentSeatDistsDTO()) {
				fltSegIdSD.put(segSeatDist.getFlightSegId(), segSeatDist);
			}
			boolean returnFlag = ondFare.getOndSequence() == OndSequence.IN_BOUND;
			for (FlightSegmentDTO flightSegmentTO : ondFare.getSegmentsMap().values()) {
				SegmentSeatDistsDTO segSeatDist = fltSegIdSD.get(flightSegmentTO.getSegmentId());
				if (segSeatDist != null) {
					SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
					simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
					simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
					simplifiedFlightSegmentDTO.setOndSequence(ondFare.getOndSequence());
					simplifiedFlightSegmentDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
					simplifiedFlightSegmentDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
					simplifiedFlightSegmentDTO.setLogicalCabinClassCode(segSeatDist.getLogicalCabinClass());
					simplifiedFlightSegmentDTO.setReturnFlag(returnFlag);
					simplifiedFlightSegmentDTO.setOperatingAirline(flightSegmentTO.getCarrierCode());
					simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
				}
			}
		}
		Collections.sort(simplifiedFlightSegmentDTOs);
		int segmentSequence = 0;
		for (SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO : simplifiedFlightSegmentDTOs) {
			simplifiedFlightSegmentDTO.setSegmentSequence(segmentSequence);
			segmentSequence++;
		}

		Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges = null;
		paxWiseCharges = new HashMap<SimplifiedPaxDTO, List<SimplifiedChargeDTO>>();

		Map<Integer, SimplifiedPaxDTO> paxSeqSimplifiedPaxDTO = new HashMap<>();

		Integer paxSequence = reservationPax.getPaxSequence();
		List<SimplifiedChargeDTO> chargesList = new ArrayList<SimplifiedChargeDTO>();
		SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
		paxDTO.setTotalAmountIncludingServiceTax(isOnlyCancellation);
		paxDTO.setPaxSequence(paxSequence);
		paxDTO.setPaxType(reservationPax.getPaxType());

		SimplifiedFlightSegmentDTO flightSegmentTO = simplifiedFlightSegmentDTOs.get(0);
		SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
		chargeDTO.setAmount(penaltyCharge);
		chargeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.PEN);
		chargeDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		chargeDTO.setChargeCode(ReservationInternalConstants.ChargeGroup.PEN);
		chargeDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
		chargeDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
		chargeDTO.setSegmentSequence(flightSegmentTO.getSegmentSequence());

		chargesList.add(chargeDTO);

		paxSeqSimplifiedPaxDTO.put(paxSequence, paxDTO);
		paxWiseCharges.put(paxDTO, chargesList);

		request.setPaxWiseExternalCharges(paxWiseCharges);

		request.setSimplifiedFlightSegmentDTOs(simplifiedFlightSegmentDTOs);
		request.setPaxState(reservation.getContactInfo().getState());
		request.setPaxCountryCode(reservation.getContactInfo().getCountryCode());
		request.setPaxTaxRegistered(
				reservation.getContactInfo().getTaxRegNo() != null && !"".equals(reservation.getContactInfo().getTaxRegNo()));

		return request;
	}
	
	public static ServiceTaxQuoteCriteriaForNonTktDTO createPaxWiseNonTicketingServiceTaxQuoteRQForPenalty(
			BigDecimal identifiedPenaltyCharge, ReservationPax reservationPax, boolean isOnlyCancellation) {
		ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForNonTktDTO();
		Map<Integer, PaxCnxModPenChargeForServiceTaxDTO> paxWiseCharges = new HashMap<>();
		PaxCnxModPenChargeForServiceTaxDTO paxCnxModPenChargeForServiceTaxDTO = new PaxCnxModPenChargeForServiceTaxDTO();

		LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
		chgDTO.setCode(ReservationInternalConstants.ChargeGroup.PEN);
		chgDTO.setAmount(identifiedPenaltyCharge);

		List<LCCClientExternalChgDTO> chgs = new ArrayList<>();
		chgs.add(chgDTO);

		paxCnxModPenChargeForServiceTaxDTO.setTotalAmountIncludingServiceTax(isOnlyCancellation);
		paxCnxModPenChargeForServiceTaxDTO.setCharges(chgs);
		paxWiseCharges.put(1, paxCnxModPenChargeForServiceTaxDTO);

		serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(null);
		serviceTaxQuoteCriteriaDTO.setPaxCountryCode(reservationPax.getReservation().getContactInfo().getCountryCode());
		serviceTaxQuoteCriteriaDTO.setPaxState(reservationPax.getReservation().getContactInfo().getState());
		serviceTaxQuoteCriteriaDTO.setPaxWiseCharges(paxWiseCharges);
		serviceTaxQuoteCriteriaDTO.setPrefSystem(ProxyConstants.SYSTEM.AA);

		return serviceTaxQuoteCriteriaDTO;
	}


	public static boolean isOpenReturnBooking(Reservation reservation) {
		boolean isOpenReturn = false;
		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			try {
				if (reservationSegmentDTO.isOpenReturnSegment()
						&& !ReservationConstants.SegmentStatus.CANCELED.equals(reservationSegmentDTO.getStatus())) {
					isOpenReturn = true;
					break;
				}
			} catch (ModuleException e) {
				isOpenReturn = false;
				log.error("ERROR :: Error while identifying the open-return segment");
			}
		}
		return isOpenReturn;
	}
	
	public static void replaceOnDInformationForFareQuoteAfterCityBaseSearch(BaseAvailRQ baseAvailRQ) throws Exception {
		if (AppSysParamsUtil.enableCityBasesFunctionality()) {
			for (OriginDestinationInformationTO ondInfo : baseAvailRQ.getOrderedOriginDestinationInformationList()) {

				if ((ondInfo.isDepartureCitySearch() || ondInfo.isArrivalCitySearch())
						&& ondInfo.getOrignDestinationOptions() != null && !ondInfo.getOrignDestinationOptions().isEmpty()) {

					List<FlightSegmentTO> ondFlights = ondInfo.getOrignDestinationOptions().get(0).getFlightSegmentList();
					if (ondFlights != null && !ondFlights.isEmpty()) {
						SortUtil.sortFlightSegByDepDate(ondFlights);
						setOriginAirportCode(ondInfo, ondFlights);
						setDestinationAirportCode(ondInfo, ondFlights);
					}

				}

			}
		}

	}
	
	public static void validateTotalJourneyFareQuoteForBusFare(SelectedFlightDTO selectedFlightDTO) {

		if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote() && selectedFlightDTO != null
				&& selectedFlightDTO.getSelectedOndFlights() != null
				&& selectedFlightDTO.getOndWiseSelectedDateAllFlights() != null
				&& selectedFlightDTO.getSelectedOndFlights().size() > 0) {

			if (selectedFlightDTO.getOndWiseSelectedDateAllFlights().size() != selectedFlightDTO.getSelectedOndFlights().size()) {
				selectedFlightDTO.getSelectedOndFlights().clear();
			} else {
				ONDFLT: for (AvailableIBOBFlightSegment fltIBOBSeg : selectedFlightDTO.getSelectedOndFlights()) {
					if (fltIBOBSeg.isSplitOperationForBusInvoked() && fltIBOBSeg instanceof AvailableConnectedFlight) {
						for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) fltIBOBSeg)
								.getAvailableFlightSegments()) {
							if (availableFlightSegment != null) {
								String selLcc = availableFlightSegment.getSelectedLogicalCabinClass() != null
										? availableFlightSegment.getSelectedLogicalCabinClass()
										: fltIBOBSeg.getSelectedLogicalCabinClass();
								if (!availableFlightSegment.isSeatsAvailable(selLcc)
										&& fltIBOBSeg.isSeatsAvailable(fltIBOBSeg.getSelectedLogicalCabinClass())) {
									selectedFlightDTO.getSelectedOndFlights().clear();
									break ONDFLT;
								}
							}

						}

					}

				}

			}

		}

	}
	
	private static String extractSegmentCode(FlightSegmentTO flightSegment) {
		String segmentCode = flightSegment.getSegmentCode();
		if (StringUtil.isNullOrEmpty(segmentCode)) {
			segmentCode = FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightSegment.getFlightRefNumber());
		}
		return segmentCode;
	}

	private static void setOriginAirportCode(OriginDestinationInformationTO ondInfo, List<FlightSegmentTO> ondFlights) {
		FlightSegmentTO firstFlightSegment = ondFlights.get(0);
		String firstFlightOndCode = extractSegmentCode(firstFlightSegment);
		if (!StringUtil.isNullOrEmpty(firstFlightOndCode)) {
			String origin = firstFlightOndCode.substring(0, firstFlightOndCode.indexOf("/"));
			if (ondInfo.isDepartureCitySearch() && !ondInfo.getOrigin().equals(origin)) {
				ondInfo.setOrigin(origin);
			}
		}
	}

	private static void setDestinationAirportCode(OriginDestinationInformationTO ondInfo, List<FlightSegmentTO> ondFlights) {
		FlightSegmentTO lastFlightSegment = ondFlights.get(ondFlights.size() - 1);
		String lastFlightOndCode = extractSegmentCode(lastFlightSegment);
		if (!StringUtil.isNullOrEmpty(lastFlightOndCode)) {
			String destination = lastFlightOndCode.substring(lastFlightOndCode.lastIndexOf("/") + 1);
			if (ondInfo.isArrivalCitySearch() && !ondInfo.getDestination().equals(destination)) {
				ondInfo.setDestination(destination);
			}
		}
	}
	public static void updateHandlingFeeFromAgentOnModification(Agent agent, ExternalChgDTO externalChgDTO, int operationalMode)
			throws ModuleException {

		if (Agent.STATUS_HANDLING_FEE_MOD_Y.equalsIgnoreCase(agent.getHandlingFeeModificationEnabled())) {

			TravelAgentBD travelAgentBD = ReservationModuleUtils.getTravelAgentBD();

			Collection<AgentHandlingFeeModification> modificationHandlingFees = travelAgentBD
					.getAgentActiveHandlingFeeDefinedForMOD(agent.getAgentCode());

			BigDecimal ratioValue = BigDecimal.ZERO;
			String chargeBasisCode = null;
			if (modificationHandlingFees != null && !modificationHandlingFees.isEmpty()) {
				for (AgentHandlingFeeModification handlingFee : modificationHandlingFees) {

					if (Agent.STATUS_ACTIVE.equalsIgnoreCase(handlingFee.getStatus())
							&& handlingFee.getOperation().intValue() == operationalMode) {
						ratioValue = handlingFee.getAmount();
						chargeBasisCode = handlingFee.getChargeBasisCode();
						break;
					}

				}
				externalChgDTO.setAmount(ratioValue);

				if (ratioValue.doubleValue() > 0) {
					for (ChargeRateDTO chargeRateDTO : externalChgDTO.getChargeRates()) {
						chargeRateDTO.setRatioValue(ratioValue);
						chargeRateDTO.setChargeBasis(ChargeBasisEnum.getChargeBasis(chargeBasisCode));
						chargeRateDTO.setValid(true);
					}
				}

			}

		}

		externalChgDTO.setAppliesTo(agent.getHandlingFeeModificationApllieTo());

	}
	
	public static Collection<ExternalChgDTO> getFilteredHandlingFeeChargesForModification(Collection<ExternalChgDTO> extChgs,
			ExternalChgDTO externalChgDTO) {
		Collection<ExternalChgDTO> externalCharges = new ArrayList<ExternalChgDTO>();
		if (extChgs != null && !extChgs.isEmpty() && externalChgDTO != null) {
			for (ExternalChgDTO charge : extChgs) {
				if (EXTERNAL_CHARGES.HANDLING_CHARGE == charge.getExternalChargesEnum()) {
					ExternalChgDTO clonnedCharge = (ExternalChgDTO) externalChgDTO.clone();
					clonnedCharge.setAmountConsumedForPayment(charge.isAmountConsumedForPayment());
					clonnedCharge.setAmount(charge.getAmount());
					clonnedCharge.setFlightSegId(charge.getFlightSegId());
					clonnedCharge.setOperationMode(charge.getOperationMode());

					externalCharges.add(clonnedCharge);
				}
			}
		}
		return externalCharges;
	}

	public static boolean shouldReprotectedAncillariesRemoved(Integer oldSegmentBundleFareId, Integer newSegmentBundleFareId) {
		boolean removeReprotectedAncillaries = false;
		/*
		 * Reprotected Ancillaries should be removed if bundle changed to Bundle_1 -> Basic, Bundle_1 -> Bundle_2
		 * bundleFareId will be null if it is Basic
		 */
		if ((oldSegmentBundleFareId != null && newSegmentBundleFareId != null && !oldSegmentBundleFareId
				.equals(newSegmentBundleFareId)) || (oldSegmentBundleFareId != null && newSegmentBundleFareId == null)) {
			removeReprotectedAncillaries = true;
		}
		if ((oldSegmentBundleFareId != null && oldSegmentBundleFareId == -1)
				|| (newSegmentBundleFareId != null && newSegmentBundleFareId == -1)) {
			removeReprotectedAncillaries = false;
		}
		return removeReprotectedAncillaries;
	}

	public static Map<String, String> getSegmentReturnMap(List<LCCClientReservationSegment> resSegmentList) {
		Map<String, String> segmentReturnMap = new HashMap<String, String>();
		List<LCCClientReservationSegment> nonCanceledAirSegmentList = getNonCanceledAirSegmentList(resSegmentList);
		if (nonCanceledAirSegmentList != null && !nonCanceledAirSegmentList.isEmpty()) {
			Collections.sort(nonCanceledAirSegmentList);
			resetReturnFlagForSegments(nonCanceledAirSegmentList);
			Map<String, Integer> airportCountMap = getAirportCountMap(nonCanceledAirSegmentList);
			segmentReturnMap = createSegmentReturnMap(nonCanceledAirSegmentList, airportCountMap);
		}
		return segmentReturnMap;
	}
	
	public static String getReservationType(List<LCCClientReservationSegment> resSegmentList) {
		String reservationType = RES_COMPLEX;

		List<LCCClientReservationSegment> nonCanceledAirSegmentList = getNonCanceledAirSegmentList(resSegmentList);
		if (nonCanceledAirSegmentList != null && !nonCanceledAirSegmentList.isEmpty()) {
			Collections.sort(nonCanceledAirSegmentList);
			boolean hasReturn = isReturnReservation(nonCanceledAirSegmentList);
			boolean oneWay = isOneWayReservation(nonCanceledAirSegmentList);

			if (hasReturn) {
				reservationType = RES_RETURN;
			} else if (oneWay) {
				reservationType = RES_ONE_WAY;
			} else {
				reservationType = RES_COMPLEX;
			}
		}
		return reservationType;
	}
	
	public static String getReturnFlag(Map<String, String> segmentReturnMap, LCCClientReservationSegment resSegment) {
		if (ReservationConstants.SegmentStatus.CANCELED.equals(resSegment.getStatus())) {
			return NO;
		} else {
			return segmentReturnMap.get(resSegment.getFlightSegmentRefNumber()) != null ? segmentReturnMap.get(resSegment
					.getFlightSegmentRefNumber()) : NO;
		}
	}

	private static Map<String, String> createSegmentReturnMap(List<LCCClientReservationSegment> nonCanceledAirSegmentList,
			Map<String, Integer> airportCountMap) {
		Map<String, String> segmentReturnMap = new HashMap<String, String>();
		boolean hasReturn = isReturnReservation(nonCanceledAirSegmentList);
		
		if (hasReturn) {
			int airportCountMapSize = airportCountMap.size();
			for (LCCClientReservationSegment resSeg : nonCanceledAirSegmentList.subList(nonCanceledAirSegmentList.size()
					- (airportCountMapSize - 1), nonCanceledAirSegmentList.size())) {
				resSeg.setReturnFlag(YES);
			}
		}

		for (LCCClientReservationSegment resSegment : nonCanceledAirSegmentList) {
			if (resSegment.getFlightSegmentRefNumber() != null) {
				segmentReturnMap.put(resSegment.getFlightSegmentRefNumber(), resSegment.getReturnFlag());
			} else if (resSegment.getPnrSegID() != null) {
				segmentReturnMap.put(resSegment.getPnrSegID().toString(), resSegment.getReturnFlag());
			}
		}

		return segmentReturnMap;
	}

	private static boolean isFirstAndLastAirportSame(List<LCCClientReservationSegment> nonCanceledAirSegmentList) {
		boolean isFirstAndLastAirportSame = false;

		LCCClientReservationSegment firstSegment = nonCanceledAirSegmentList.get(0);
		LCCClientReservationSegment lastSegment = nonCanceledAirSegmentList.get(nonCanceledAirSegmentList.size() - 1);

		String departureAirport = getDepartureAirportCode(firstSegment.getSegmentCode());
		String arrivalAirport = getArrivalAirportCode(lastSegment.getSegmentCode());

		isFirstAndLastAirportSame = departureAirport.equals(arrivalAirport);
		return isFirstAndLastAirportSame;

	}

	private static Map<String, Integer> getAirportCountMap(List<LCCClientReservationSegment> resSegmentList) {
		Map<String, Integer> airportCountMap = new HashMap<String, Integer>();

		for (LCCClientReservationSegment resSegment : resSegmentList) {
			String departureAirport = getDepartureAirportCode(resSegment.getSegmentCode());
			String arrivalAirport = getArrivalAirportCode(resSegment.getSegmentCode());
			addToAriportCountMap(airportCountMap, departureAirport, arrivalAirport);
		}

		return airportCountMap;
	}

	private static List<LCCClientReservationSegment>
			getNonCanceledAirSegmentList(List<LCCClientReservationSegment> resSegmentList) {
		List<LCCClientReservationSegment> nonCanceledAirSegmentList = new ArrayList<>();
		for (LCCClientReservationSegment resSegment : resSegmentList) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSegment.getStatus())
					&& !(resSegment.getSubStationShortName() != null && !resSegment.getSubStationShortName().isEmpty())) {
				nonCanceledAirSegmentList.add(resSegment);
			}
		}
		return nonCanceledAirSegmentList;
	}

	private static void resetReturnFlagForSegments(List<LCCClientReservationSegment> resSegmentList) {
		for (LCCClientReservationSegment resSegment : resSegmentList) {
			resSegment.setReturnFlag(NO);
		}
	}

	private static boolean isSegmentsContinuous(List<LCCClientReservationSegment> resSegmentList) {
		boolean isSegmentsContinuous = true;
		for (int i = 0; i < resSegmentList.size() - 1; i++) {
			String arrivalAirport = getArrivalAirportCode(resSegmentList.get(i).getSegmentCode());
			String nextDepartureAirport = getDepartureAirportCode(resSegmentList.get(i + 1).getSegmentCode());
			if (!arrivalAirport.equals(nextDepartureAirport)) {
				isSegmentsContinuous = false;
				break;
			}
		}
		return isSegmentsContinuous;
	}

	private static void addToAriportCountMap(Map<String, Integer> airportCountMap, String... airports) {
		for (String airport : airports) {
			if (airportCountMap.get(airport) != null) {
				int count = airportCountMap.get(airport);
				airportCountMap.put(airport, ++count);
			} else {
				int count = 0;
				airportCountMap.put(airport, ++count);
			}
		}
	}

	private static String getArrivalAirportCode(String segCode) {
		if (segCode != null) {
			String arr[] = segCode.split("/");
			return arr[arr.length - 1];
		}
		return null;
	}

	private static String getDepartureAirportCode(String segCode) {
		if (segCode != null) {
			return segCode.split("/")[0];
		}
		return null;
	}
	
	private static boolean isReturnReservation(List<LCCClientReservationSegment> nonCanceledAirSegmentList) {
		return isFirstAndLastAirportSame(nonCanceledAirSegmentList) && isSegmentsContinuous(nonCanceledAirSegmentList);
	}

	private static boolean isOneWayReservation(List<LCCClientReservationSegment> nonCanceledAirSegmentList) {
		return !isReturnReservation(nonCanceledAirSegmentList) && isSegmentsContinuous(nonCanceledAirSegmentList);
	}
}
