package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To Show in the screen the total amount generate/and what is existing in interface tables.
 * 
 * @author Byorn
 */
public class CreditCardSalesStatusDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5412376532871651805L;

	private BigDecimal totAmtGenerated = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totAmtTransfered = AccelAeroCalculator.getDefaultBigDecimalZero();

	private boolean hasEXDB;
	private ArrayList<CCSalesHistoryDTO> creditSales;

	/**
	 * @return Returns the amountSatus.
	 */
	public String getAmountSatus() {
		NumberFormat formatter = new DecimalFormat("#,###,###.00");
		if (this.totAmtGenerated != null) {

			if (this.totAmtTransfered != null) {

				if (Math.round(this.totAmtGenerated.doubleValue()) == Math.round(this.totAmtTransfered.doubleValue())) {
					return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue())
							+ " Is Generated and Transfered";
				} else {
					if (Math.round(this.totAmtGenerated.doubleValue()) == 0) {
						return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue())
								+ " Is Generated but NOT Transfered";
					} else {
						return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue())
								+ " Is Generated but Transfered Valued is :"
								+ formatter.format(this.totAmtTransfered.doubleValue());
					}

				}
			} else {
				if (hasEXDB) {
					return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue())
							+ " Is Generated but Not Transfered";
				} else {
					return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue()) + " Is Generated.";
				}
			}
		}

		return "No Credit Card Sales have been generated for the selected Period ";
	}

	/**
	 * @return Returns the creditSales.
	 */
	public ArrayList<CCSalesHistoryDTO> getCreditSales() {
		return creditSales;
	}

	/**
	 * @param creditSales
	 *            The creditSales to set.
	 */
	public void setCreditSales(ArrayList<CCSalesHistoryDTO> creditSales) {
		this.creditSales = creditSales;
	}

	/**
	 * @return Returns the hasEXDB.
	 */
	public boolean isHasEXDB() {
		return hasEXDB;
	}

	/**
	 * @param hasEXDB
	 *            The hasEXDB to set.
	 */
	public void setHasEXDB(boolean hasEXDB) {
		this.hasEXDB = hasEXDB;
	}

	/**
	 * @param totAmtGenerated
	 *            The totAmtGenerated to set.
	 */
	public void setTotAmtGenerated(BigDecimal totAmtGenerated) {
		this.totAmtGenerated = totAmtGenerated;
	}

	/**
	 * @param totAmtTransfered
	 *            The totAmtTransfered to set.
	 */
	public void setTotAmtTransfered(BigDecimal totAmtTransfered) {
		this.totAmtTransfered = totAmtTransfered;
	}

}
