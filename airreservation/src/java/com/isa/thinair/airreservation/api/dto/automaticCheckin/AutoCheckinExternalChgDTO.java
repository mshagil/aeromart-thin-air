package com.isa.thinair.airreservation.api.dto.automaticCheckin;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

/**
 * To hold reservation external charges data transfer information
 * 
 * @author aravinth.r
 *
 */
public class AutoCheckinExternalChgDTO extends ExternalChgDTO {

	private static final long serialVersionUID = 1L;

	private Integer paxSequenceNo;

	private Integer flightSeatId;

	private BigDecimal autoCheckinCharge;

	private Integer pnrPaxId;

	private Integer pnrPaxFareId;

	private String paxType;

	private Integer pnrSegId;

	private String flightRPH;

	private Integer autoCheckinId;

	private String seatTypePreference;

	private String email;

	private String seatCode;

	/**
	 * @return the paxSequenceNo
	 */
	public Integer getPaxSequenceNo() {
		return paxSequenceNo;
	}

	/**
	 * @param paxSequenceNo
	 *            the paxSequenceNo to set
	 */
	public void setPaxSequenceNo(Integer paxSequenceNo) {
		this.paxSequenceNo = paxSequenceNo;
	}

	/**
	 * @return the flightSeatId
	 */
	public Integer getFlightSeatId() {
		return flightSeatId;
	}

	/**
	 * @param flightSeatId
	 *            the flightSeatId to set
	 */
	public void setFlightSeatId(Integer flightSeatId) {
		this.flightSeatId = flightSeatId;
	}

	/**
	 * @return the autoCheckinCharge
	 */
	public BigDecimal getAutoCheckinCharge() {
		return autoCheckinCharge;
	}

	/**
	 * @param autoCheckinCharge
	 *            the autoCheckinCharge to set
	 */
	public void setAutoCheckinCharge(BigDecimal autoCheckinCharge) {
		this.autoCheckinCharge = autoCheckinCharge;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the pnrPaxFareId
	 */
	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	/**
	 * @param pnrPaxFareId
	 *            the pnrPaxFareId to set
	 */
	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the pnrSegId
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return the flightRPH
	 */
	public String getFlightRPH() {
		return flightRPH;
	}

	/**
	 * @param flightRPH
	 *            the flightRPH to set
	 */
	public void setFlightRPH(String flightRPH) {
		this.flightRPH = flightRPH;
	}

	/**
	 * @return the autoCheckinId
	 */
	public Integer getAutoCheckinId() {
		return autoCheckinId;
	}

	/**
	 * @param autoCheckinId
	 *            the autoCheckinId to set
	 */
	public void setAutoCheckinId(Integer autoCheckinId) {
		this.autoCheckinId = autoCheckinId;
	}

	/**
	 * @return the seatTypePreference
	 */
	public String getSeatTypePreference() {
		return seatTypePreference;
	}

	/**
	 * @param seatTypePref
	 *            the seatTypePref to set
	 */
	public void setSeatTypePreference(String seatTypePreference) {
		this.seatTypePreference = seatTypePreference;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		AutoCheckinExternalChgDTO clone = new AutoCheckinExternalChgDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());

		clone.setPaxSequenceNo(this.getPaxSequenceNo());
		clone.setFlightSegId(this.getFlightSegId());
		clone.setFlightSeatId(this.getFlightSeatId());
		clone.setAutoCheckinId(this.autoCheckinId);
		clone.setSeatTypePreference(this.seatTypePreference);
		clone.setEmail(this.email);
		clone.setSeatCode(this.seatCode);
		return clone;
	}

	/**
	 * @return the seatCode
	 */
	public String getSeatCode() {
		return seatCode;
	}

	/**
	 * @param seatCode
	 *            the seatCode to set
	 */
	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}
}
