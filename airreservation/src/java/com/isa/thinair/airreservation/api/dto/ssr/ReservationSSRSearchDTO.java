package com.isa.thinair.airreservation.api.dto.ssr;

import java.util.Collection;
import java.util.Date;

public class ReservationSSRSearchDTO {
	private String pNRNo;
	private Collection<Integer> pNRPaxIds;
	private Collection<Integer> pNRSegmentIds;
	private Collection<Integer> pNRPaxSegmentSSRIds;

	private Date fromDepartureDate;
	private Date toDepartureDate;

	private boolean loadCancelledSSRs;
	private Integer ssrCategoryId;

	public String getPNRNo() {
		return this.pNRNo;
	}

	public void setPNRNo(String no) {
		this.pNRNo = no;
	}

	public Date getFromDepartureDate() {
		return this.fromDepartureDate;
	}

	public void setFromDepartureDate(Date fromDepartureDate) {
		this.fromDepartureDate = fromDepartureDate;
	}

	public Date getToDepartureDate() {
		return this.toDepartureDate;
	}

	public void setToDepartureDate(Date toDepartureDate) {
		this.toDepartureDate = toDepartureDate;
	}

	public Collection<Integer> getPNRPaxIds() {
		return this.pNRPaxIds;
	}

	public void setPNRPaxIds(Collection<Integer> paxIds) {
		this.pNRPaxIds = paxIds;
	}

	public Collection<Integer> getPNRSegmentIds() {
		return this.pNRSegmentIds;
	}

	public void setPNRSegmentIds(Collection<Integer> pnrSegmentIds) {
		this.pNRSegmentIds = pnrSegmentIds;
	}

	public Collection<Integer> getpNRPaxSegmentSSRIds() {
		return this.pNRPaxSegmentSSRIds;
	}

	public void setpNRPaxSegmentSSRIds(Collection<Integer> pNRPaxSegmentSSRIds) {
		this.pNRPaxSegmentSSRIds = pNRPaxSegmentSSRIds;
	}

	public boolean isLoadCancelledSSRs() {
		return this.loadCancelledSSRs;
	}

	public void setLoadCancelledSSRs(boolean loadCancelledSSRs) {
		this.loadCancelledSSRs = loadCancelledSSRs;
	}

	public Integer getSsrCategoryId() {
		return ssrCategoryId;
	}

	public void setSsrCategoryId(Integer ssrCategoryId) {
		this.ssrCategoryId = ssrCategoryId;
	}

}
