package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * Created by degorov on 6/14/17.
 */
public class PaymentMethodDetailsDTO implements Serializable {
    public PaymentMethodDetailsDTO() {
    }

    private String firstSegmentONDCode = null;
    private String countryCode;
    private boolean enableOffline;

    public PaymentMethodDetailsDTO setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public PaymentMethodDetailsDTO setEnableOffline(boolean enableOffline) {
        this.enableOffline = enableOffline;
        return this;
    }

    public PaymentMethodDetailsDTO setFirstSegmentONDCode(String ondCode) {
        this.firstSegmentONDCode = ondCode;
        return this;
    }

    public String getFirstSegmentONDCode() {
        return firstSegmentONDCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public boolean isEnableOffline() {
        return enableOffline;
    }
}
