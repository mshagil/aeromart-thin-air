package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Nilindra Fernando
 * @since August 5, 2010
 * @hibernate.class table = "T_PNR_PAX_OND_PAYMENTS"
 */
public class ReservationPaxOndPayment extends Persistent {

	private static final long serialVersionUID = -3390515359192080307L;

	private Long paxOndPaymentId;

	private String pnrPaxId;

	private Long pnrPaxOndChgId;

	private Long paymentTnxId;

	private BigDecimal amount;

	private Integer nominalCode;

	private String remarks;

	private String chargeGroupCode;

	private Long originalPaxOndPaymentId;
	
	private Integer paxOndRefundId;
	
	private String chargeCode;

	private Integer transactionSeq;

	/**
	 * @return the paxOndPaymentId
	 * @hibernate.id column = "PPOP_ID " generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_OND_PAYMENTS"
	 */
	public Long getPaxOndPaymentId() {
		return paxOndPaymentId;
	}

	/**
	 * @param paxOndPaymentId
	 *            the paxOndPaymentId to set
	 */
	public void setPaxOndPaymentId(Long paxOndPaymentId) {
		this.paxOndPaymentId = paxOndPaymentId;
	}

	/**
	 * @return the pnrPaxOndChgId
	 * @hibernate.property column = "PFT_ID"
	 */
	public Long getPnrPaxOndChgId() {
		return pnrPaxOndChgId;
	}

	/**
	 * @param pnrPaxOndChgId
	 *            the pnrPaxOndChgId to set
	 */
	public void setPnrPaxOndChgId(Long pnrPaxOndChgId) {
		this.pnrPaxOndChgId = pnrPaxOndChgId;
	}

	/**
	 * @return the paymentTnxId
	 * @hibernate.property column = "PAYMENT_TXN_ID"
	 */
	public Long getPaymentTnxId() {
		return paymentTnxId;
	}

	/**
	 * @param paymentTnxId
	 *            the paymentTnxId to set
	 */
	public void setPaymentTnxId(Long paymentTnxId) {
		this.paymentTnxId = paymentTnxId;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the nominalCode
	 * @hibernate.property column = "NOMINAL_CODE"
	 */
	public Integer getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            the nominalCode to set
	 */
	public void setNominalCode(Integer nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return the remarks
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the chargeGroupCode
	 * @hibernate.property column = "CHARGE_GROUP_CODE"
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            the chargeGroupCode to set
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return the pnrPaxId
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public String getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(String pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the originalPaxOndPaymentId
	 * @hibernate.property column = "ORIGINAL_PPOP_ID"
	 */
	public Long getOriginalPaxOndPaymentId() {
		return originalPaxOndPaymentId;
	}

	/**
	 * @param originalPaxOndPaymentId
	 */
	public void setOriginalPaxOndPaymentId(Long originalPaxOndPaymentId) {
		this.originalPaxOndPaymentId = originalPaxOndPaymentId;
	}

	public ReservationPaxOndPayment clone() {
		ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
		reservationPaxOndPayment.setPnrPaxId(this.getPnrPaxId());
		reservationPaxOndPayment.setPnrPaxOndChgId(this.getPnrPaxOndChgId());
		reservationPaxOndPayment.setPaymentTnxId(this.getPaymentTnxId());
		reservationPaxOndPayment.setAmount(this.getAmount());
		reservationPaxOndPayment.setNominalCode(this.getNominalCode());
		reservationPaxOndPayment.setRemarks(this.getRemarks());
		reservationPaxOndPayment.setChargeGroupCode(this.getChargeGroupCode());
		reservationPaxOndPayment.setTransactionSeq(this.getTransactionSeq());
		reservationPaxOndPayment.setChargeCode(this.getChargeCode());
		return reservationPaxOndPayment;
	}

	/**
	 * @return the transactionSeq
	 * @hibernate.property column = "TRANSACTION_SEQ"
	 */
	public Integer getTransactionSeq() {
		return transactionSeq;
	}

	/**
	 * @param transactionSeq
	 *            the transactionSeq to set
	 */
	public void setTransactionSeq(Integer transactionSeq) {
		this.transactionSeq = transactionSeq;
	}

	/**
	 * @return the paxOndRefundId
	 * @hibernate.property column = "REFUND_TXN_ID"
	 */
	public Integer getPaxOndRefundId() {
		return paxOndRefundId;
	}

	/**
	 * @param paxOndRefundId the paxOndRefundId to set
	 */
	public void setPaxOndRefundId(Integer paxOndRefundId) {
		this.paxOndRefundId = paxOndRefundId;
	}
	
	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public ReservationPaxPaymentCredit transformToPaymentCredit() {

		ReservationPaxPaymentCredit paxPaymentCredit = new ReservationPaxPaymentCredit();
		paxPaymentCredit.setPaxOndPaymentId(this.getPaxOndPaymentId());
		// reservationPaxOndPayment.setOriginalPaxOndPaymentId(this.getPnrPaxOndChgId());
		paxPaymentCredit.setAmount(this.getAmount());
		paxPaymentCredit.setRemarks(this.getRemarks());
		paxPaymentCredit.setChargeGroupCode(this.getChargeGroupCode());
		paxPaymentCredit.setChargeCode(this.getChargeCode());
		return paxPaymentCredit;

	}

}
