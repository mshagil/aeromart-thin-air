/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.util.List;

/**
 * @author Nilindra Fernando
 */
public class LccReservationTnxNominalCode {

	private int code;

	private LccReservationTnxNominalCode(int code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public boolean equals(LccReservationTnxNominalCode lccReservationTnxNominalCode) {
		return lccReservationTnxNominalCode.toString().equals(String.valueOf(this.code));
	}

	public int getCode() {
		return code;
	}

	public static final LccReservationTnxNominalCode CREDIT_CF = new LccReservationTnxNominalCode(
			ReservationTnxNominalCode.CREDIT_CF.getCode());
	public static final LccReservationTnxNominalCode CREDIT_BF = new LccReservationTnxNominalCode(
			ReservationTnxNominalCode.CREDIT_BF.getCode());

	public static final LccReservationTnxNominalCode CASH_PAYMENT = new LccReservationTnxNominalCode(
			ReservationTnxNominalCode.CASH_PAYMENT.getCode());
	public static final LccReservationTnxNominalCode ONACCOUNT_PAYMENT = new LccReservationTnxNominalCode(
			ReservationTnxNominalCode.ONACCOUNT_PAYMENT.getCode());

	public static final LccReservationTnxNominalCode REFUND_CASH = new LccReservationTnxNominalCode(
			ReservationTnxNominalCode.REFUND_CASH.getCode());
	public static final LccReservationTnxNominalCode REFUND_ONACCOUNT = new LccReservationTnxNominalCode(
			ReservationTnxNominalCode.REFUND_ONACCOUNT.getCode());

	public static List<Integer> getVisaTypeNominalCodes() {
		return ReservationTnxNominalCode.getVisaTypeNominalCodes();
	}

	public static List<Integer> getMasterTypeNominalCodes() {
		return ReservationTnxNominalCode.getMasterTypeNominalCodes();
	}

	public static List<Integer> getAmexTypeNominalCodes() {
		return ReservationTnxNominalCode.getAmexTypeNominalCodes();
	}

	public static List<Integer> getGenericTypeNominalCodes() {
		return ReservationTnxNominalCode.getGenericTypeNominalCodes();
	}

	public static List<Integer> getCMITypeNominalCodes() {
		return ReservationTnxNominalCode.getCMITypeNominalCodes();
	}

	public static List<Integer> getDinersTypeNominalCodes() {
		return ReservationTnxNominalCode.getDinersTypeNominalCodes();
	}

}