package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 * @hibernate.class table = "T_PAX_TXN_BREAKDOWN_SUMMARY"
 */
public class ReservationPaxTnxBreakdown extends Persistent {

	private static final long serialVersionUID = -8815444665329127246L;

	private Long pnrPaxTnxBreakdownId;

	private Integer txnId;

	private Date timestamp;

	private String paymentCurrencyCode;

	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalPriceInPayCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal fareAmountInPayCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal taxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal taxAmountInPayCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal surchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal surchargeAmountInPayCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal modAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal modAmountInPayCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

	/**
	 * @return the pnrPaxTnxBreakdownId
	 * @hibernate.id column = "PTBS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PAX_TXN_BREAKDOWN_SUMMARY"
	 */
	public Long getPnrPaxTnxBreakdownId() {
		return pnrPaxTnxBreakdownId;
	}

	/**
	 * @param pnrPaxTnxBreakdownId
	 *            the pnrPaxTnxBreakdownId to set
	 */
	public void setPnrPaxTnxBreakdownId(Long pnrPaxTnxBreakdownId) {
		this.pnrPaxTnxBreakdownId = pnrPaxTnxBreakdownId;
	}

	/**
	 * @return the txnId
	 * @hibernate.property column = "PAX_TXN_ID"
	 */
	public Integer getTxnId() {
		return txnId;
	}

	/**
	 * @param txnId
	 *            the txnId to set
	 */
	public void setTxnId(Integer txnId) {
		this.txnId = txnId;
	}

	/**
	 * @return the timestamp
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the paymentCurrencyCode
	 * @hibernate.property column = "PAYMENT_CURRENCY_CODE"
	 */
	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	/**
	 * @param paymentCurrencyCode
	 *            the paymentCurrencyCode to set
	 */
	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	/**
	 * @return the totalPrice
	 * @hibernate.property column = "TOTAL_AMOUNT"
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *            the totalPriceAmount to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the totalPriceInPayCurrency
	 * @hibernate.property column = "TOTAL_AMOUNT_PAYCUR"
	 */
	public BigDecimal getTotalPriceInPayCurrency() {
		return totalPriceInPayCurrency;
	}

	/**
	 * @param totalPriceInPayCurrency
	 *            the totalPriceInPayCurrency to set
	 */
	public void setTotalPriceInPayCurrency(BigDecimal totalPriceInPayCurrency) {
		this.totalPriceInPayCurrency = totalPriceInPayCurrency;
	}

	/**
	 * @return the fareAmount
	 * @hibernate.property column = "TOTAL_FARE_AMOUNT"
	 */
	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	/**
	 * @param fareAmount
	 *            the fareAmount to set
	 */
	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * @return the fareAmountInPayCurrency
	 * @hibernate.property column = "TOTAL_FARE_AMOUNT_PAYCUR"
	 */
	public BigDecimal getFareAmountInPayCurrency() {
		return fareAmountInPayCurrency;
	}

	/**
	 * @param fareAmountInPayCurrency
	 *            the fareAmountInPayCurrency to set
	 */
	public void setFareAmountInPayCurrency(BigDecimal fareAmountInPayCurrency) {
		this.fareAmountInPayCurrency = fareAmountInPayCurrency;
	}

	/**
	 * @return the taxAmount
	 * @hibernate.property column = "TOTAL_TAX_AMOUNT"
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	/**
	 * @param taxAmount
	 *            the taxAmount to set
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * @return the taxAmountInPayCurrency
	 * @hibernate.property column = "TOTAL_TAX_AMOUNT_PAYCUR"
	 */
	public BigDecimal getTaxAmountInPayCurrency() {
		return taxAmountInPayCurrency;
	}

	/**
	 * @param taxAmountInPayCurrency
	 *            the taxAmountInPayCurrency to set
	 */
	public void setTaxAmountInPayCurrency(BigDecimal taxAmountInPayCurrency) {
		this.taxAmountInPayCurrency = taxAmountInPayCurrency;
	}

	/**
	 * @return the surchargeAmount
	 * @hibernate.property column = "TOTAL_SUR_AMOUNT"
	 */
	public BigDecimal getSurchargeAmount() {
		return surchargeAmount;
	}

	/**
	 * @param surchargeAmount
	 *            the surchargeAmount to set
	 */
	public void setSurchargeAmount(BigDecimal surchargeAmount) {
		this.surchargeAmount = surchargeAmount;
	}

	/**
	 * @return the surchargeAmountInPayCurrency
	 * @hibernate.property column = "TOTAL_SUR_AMOUNT_PAYCUR"
	 */
	public BigDecimal getSurchargeAmountInPayCurrency() {
		return surchargeAmountInPayCurrency;
	}

	/**
	 * @param surchargeAmountInPayCurrency
	 *            the surchargeAmountInPayCurrency to set
	 */
	public void setSurchargeAmountInPayCurrency(BigDecimal surchargeAmountInPayCurrency) {
		this.surchargeAmountInPayCurrency = surchargeAmountInPayCurrency;
	}

	/**
	 * @return the modAmount
	 * @hibernate.property column = "TOTAL_MOD_AMOUNT"
	 */
	public BigDecimal getModAmount() {
		return modAmount;
	}

	/**
	 * @param modAmount
	 *            the modAmount to set
	 */
	public void setModAmount(BigDecimal modAmount) {
		this.modAmount = modAmount;
	}

	/**
	 * @return the modAmountInPayCurrency
	 * @hibernate.property column = "TOTAL_MOD_AMOUNT_PAYCUR"
	 */
	public BigDecimal getModAmountInPayCurrency() {
		return modAmountInPayCurrency;
	}

	/**
	 * @param modAmountInPayCurrency
	 *            the modAmountInPayCurrency to set
	 */
	public void setModAmountInPayCurrency(BigDecimal modAmountInPayCurrency) {
		this.modAmountInPayCurrency = modAmountInPayCurrency;
	}
}
