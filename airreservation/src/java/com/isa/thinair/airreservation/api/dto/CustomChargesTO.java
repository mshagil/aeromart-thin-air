/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

/**
 * To hold custom charges data transfer information for the reservation module
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class CustomChargesTO implements Serializable {

	private static final long serialVersionUID = -3112671456950849010L;

	/** Holds the custom adult cancellation charge */
	private BigDecimal customAdultCCharge;

	/** Holds the custom child cancellation charge */
	private BigDecimal customChildCCharge;

	/** Holds the custom infant cancellation charge */
	private BigDecimal customInfantCCharge;

	/** Holds the custom adult modification charge */
	private BigDecimal customAdultMCharge;

	/** Holds the custom child modification charge */
	private BigDecimal customChildMCharge;

	/** Holds the custom infant modification charge */
	private BigDecimal customInfantMCharge;

	private BigDecimal defaultCustomAdultCharge;

	private BigDecimal defaultCustomChildCharge;

	private BigDecimal defaultCustomInfantCharge;

	private PnrChargeDetailTO customAdultChargeTO;

	private PnrChargeDetailTO customChildChargeTO;

	private PnrChargeDetailTO customInfantChargeTO;

	private boolean isAbsoluteValuesPassed = false;

	/** Holds whether to compare with existing modification charges to get effective cancellation charge */
	private boolean compareWithExistingModificationCharges = false;

	/** Holds total modification charge applied due to modify segment operations for adult */
	private BigDecimal existingModificationChargeAdult;

	/** Holds total modification charge applied due to modify segment operations for child */
	private BigDecimal existingModificationChargeChild;

	/** Holds total modification charge applied due to modify segment operations for infant */
	private BigDecimal existingModificationChargeInfant;

	/** Holds the ond wise custom adult cancellation charge */
	private Map<String, BigDecimal> ondWiseCustomAdultCharge;

	/** Holds the ond wise custom child cancellation charge */
	private Map<String, BigDecimal> ondWiseCustomChildCharge;

	/** Holds the ond wise custom infant cancellation charge */
	private Map<String, BigDecimal> ondWiseCustomInfantCharge;

	private String routeType;

	/** Default constructor. Can use to apply default system behavior with out overridden charges */
	public CustomChargesTO() {
	}

	/** Construct the CustomChargesTO object */
	public CustomChargesTO(BigDecimal customAdultCCharge, BigDecimal customChildCCharge, BigDecimal customInfantCCharge,
			BigDecimal customAdultMCharge, BigDecimal customChildMCharge, BigDecimal customInfantMCharge) {
		this.customAdultCCharge = customAdultCCharge;
		this.customChildCCharge = customChildCCharge;
		this.customInfantCCharge = customInfantCCharge;

		this.customAdultMCharge = customAdultMCharge;
		this.customChildMCharge = customChildMCharge;
		this.customInfantMCharge = customInfantMCharge;
	}

	public CustomChargesTO(BigDecimal customAdultCCharge, BigDecimal customChildCCharge, BigDecimal customInfantCCharge,
			BigDecimal customAdultMCharge, BigDecimal customChildMCharge, BigDecimal customInfantMCharge,
			PnrChargeDetailTO customAdultChargeTO, PnrChargeDetailTO customChildChargeTO, PnrChargeDetailTO customInfantChargeTO,
			boolean isAbsoluteValuesPassed) {
		this.customAdultCCharge = customAdultCCharge;
		this.customChildCCharge = customChildCCharge;
		this.customInfantCCharge = customInfantCCharge;

		this.customAdultMCharge = customAdultMCharge;
		this.customChildMCharge = customChildMCharge;
		this.customInfantMCharge = customInfantMCharge;

		this.customAdultChargeTO = customAdultChargeTO;
		this.customChildChargeTO = customChildChargeTO;
		this.customInfantChargeTO = customInfantChargeTO;

		this.isAbsoluteValuesPassed = isAbsoluteValuesPassed;
	}

	public CustomChargesTO(BigDecimal customAdultCCharge, BigDecimal customChildCCharge, BigDecimal customInfantCCharge,
			BigDecimal customAdultMCharge, BigDecimal customChildMCharge, BigDecimal customInfantMCharge,
			PnrChargeDetailTO customAdultChargeTO, PnrChargeDetailTO customChildChargeTO, PnrChargeDetailTO customInfantChargeTO,
			boolean isAbsoluteValuesPassed, Map<String, BigDecimal> ondWiseCustomAdultCharge,
			Map<String, BigDecimal> ondWiseCustomChildCharge, Map<String, BigDecimal> ondWiseCustomInfantCharge) {
		this.customAdultCCharge = customAdultCCharge;
		this.customChildCCharge = customChildCCharge;
		this.customInfantCCharge = customInfantCCharge;

		this.customAdultMCharge = customAdultMCharge;
		this.customChildMCharge = customChildMCharge;
		this.customInfantMCharge = customInfantMCharge;

		this.customAdultChargeTO = customAdultChargeTO;
		this.customChildChargeTO = customChildChargeTO;
		this.customInfantChargeTO = customInfantChargeTO;
		this.isAbsoluteValuesPassed = isAbsoluteValuesPassed;

		this.ondWiseCustomAdultCharge = ondWiseCustomAdultCharge;
		this.ondWiseCustomChildCharge = ondWiseCustomChildCharge;
		this.ondWiseCustomInfantCharge = ondWiseCustomInfantCharge;
	}

	public PnrChargeDetailTO getPaxCustomChargeDetail(String paxType) {
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			return customAdultChargeTO;
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			return customChildChargeTO;
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			return customInfantChargeTO;
		}
		return null;
	}

	/**
	 * Has a user defined cancelation charge Value, and user is expecting to override with his / her abosolute value
	 * 
	 * @param paxType
	 * @return
	 */
	public boolean hasUserDefinedCNXCharge(String paxType) {
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			return (customAdultCCharge != null && isAbsoluteValuesPassed && !isOverrideWithPercentage(paxType, true));
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			return (customChildCCharge != null && isAbsoluteValuesPassed && !isOverrideWithPercentage(paxType, true));
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			return (customInfantCCharge != null && isAbsoluteValuesPassed && !isOverrideWithPercentage(paxType, true));
		}
		return false;
	}

	/**
	 * Has a user defined modification charge Value, and user is expecting to override with his / her abosolute value
	 * 
	 * @param paxType
	 * @return
	 */
	public boolean hasUserDefinedModCharge(String paxType) {
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			return (customAdultMCharge != null && isAbsoluteValuesPassed && !isOverrideWithPercentage(paxType, false));
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			return (customChildMCharge != null && isAbsoluteValuesPassed && !isOverrideWithPercentage(paxType, false));
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			return (customInfantMCharge != null && isAbsoluteValuesPassed && !isOverrideWithPercentage(paxType, false));
		}
		return false;
	}

	/**
	 * Check if user has edited the actual value instead of the percentage parameters
	 * 
	 * @param paxType
	 * @param isCancelation
	 * @return
	 */
	private boolean isOverrideWithPercentage(String paxType, boolean isCancelation) {
		if (getPaxCustomChargeDetail(paxType) != null) {
			if (isCancelation) {
				if (getPaxCustomChargeDetail(paxType).getCancellationChargeType() != null
						&& !getPaxCustomChargeDetail(paxType).getCancellationChargeType().trim().equals("")) {
					return true;
				}
			} else {
				if (getPaxCustomChargeDetail(paxType).getModificationChargeType() != null
						&& !getPaxCustomChargeDetail(paxType).getModificationChargeType().trim().equals("")) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return Returns the customAdultMCharge.
	 */
	public BigDecimal getCustomAdultMCharge() {
		return customAdultMCharge;
	}

	/**
	 * @param customAdultMCharge
	 *            The customAdultMCharge to set.
	 */
	public void setCustomAdultMCharge(BigDecimal customAdultMCharge) {
		this.customAdultMCharge = customAdultMCharge;
	}

	/**
	 * @return Returns the customChildCCharge.
	 */
	public BigDecimal getCustomChildCCharge() {
		return customChildCCharge;
	}

	/**
	 * @param customChildCCharge
	 *            The customChildCCharge to set.
	 */
	public void setCustomChildCCharge(BigDecimal customChildCCharge) {
		this.customChildCCharge = customChildCCharge;
	}

	/**
	 * @return Returns the customChildMCharge.
	 */
	public BigDecimal getCustomChildMCharge() {
		return customChildMCharge;
	}

	/**
	 * @param customChildMCharge
	 *            The customChildMCharge to set.
	 */
	public void setCustomChildMCharge(BigDecimal customChildMCharge) {
		this.customChildMCharge = customChildMCharge;
	}

	/**
	 * @return Returns the customInfantCCharge.
	 */
	public BigDecimal getCustomInfantCCharge() {
		return customInfantCCharge;
	}

	/**
	 * @param customInfantCCharge
	 *            The customInfantCCharge to set.
	 */
	public void setCustomInfantCCharge(BigDecimal customInfantCCharge) {
		this.customInfantCCharge = customInfantCCharge;
	}

	/**
	 * @return Returns the customInfantMCharge.
	 */
	public BigDecimal getCustomInfantMCharge() {
		return customInfantMCharge;
	}

	/**
	 * @param customInfantMCharge
	 *            The customInfantMCharge to set.
	 */
	public void setCustomInfantMCharge(BigDecimal customInfantMCharge) {
		this.customInfantMCharge = customInfantMCharge;
	}

	/**
	 * @return Returns the customAdultCCharge.
	 */
	public BigDecimal getCustomAdultCCharge() {
		return customAdultCCharge;
	}

	/**
	 * @param customAdultCCharge
	 *            The customAdultCCharge to set.
	 */
	public void setCustomAdultCCharge(BigDecimal customAdultCCharge) {
		this.customAdultCCharge = customAdultCCharge;
	}

	/**
	 * @return the customAdultChargeTO
	 */
	public PnrChargeDetailTO getCustomAdultChargeTO() {
		return customAdultChargeTO;
	}

	/**
	 * @param customAdultChargeTO
	 *            the customAdultChargeTO to set
	 */
	public void setCustomAdultChargeTO(PnrChargeDetailTO customAdultChargeTO) {
		this.customAdultChargeTO = customAdultChargeTO;
	}

	/**
	 * @return the customChildChargeTO
	 */
	public PnrChargeDetailTO getCustomChildChargeTO() {
		return customChildChargeTO;
	}

	/**
	 * @param customChildChargeTO
	 *            the customChildChargeTO to set
	 */
	public void setCustomChildChargeTO(PnrChargeDetailTO customChildChargeTO) {
		this.customChildChargeTO = customChildChargeTO;
	}

	/**
	 * @return the customInfantChargeTO
	 */
	public PnrChargeDetailTO getCustomInfantChargeTO() {
		return customInfantChargeTO;
	}

	/**
	 * @param customInfantChargeTO
	 *            the customInfantChargeTO to set
	 */
	public void setCustomInfantChargeTO(PnrChargeDetailTO customInfantChargeTO) {
		this.customInfantChargeTO = customInfantChargeTO;
	}

	/**
	 * @return the isAbosoluteValuesPassed
	 */
	public boolean isAbsoluteValuesPassed() {
		return isAbsoluteValuesPassed;
	}

	/**
	 * @param isAbosoluteValuesPassed
	 *            the isAbosoluteValuesPassed to set
	 */
	public void setAbsoluteValuesPassed(boolean isAbsoluteValuesPassed) {
		this.isAbsoluteValuesPassed = isAbsoluteValuesPassed;
	}

	public boolean hasCustomCharge(String paxType) {
		if (PaxTypeTO.ADULT.equals(paxType) && getCustomAdultChargeTO() != null) {
			return true;
		} else if (PaxTypeTO.CHILD.equals(paxType) && getCustomChildChargeTO() != null) {
			return true;
		} else if (PaxTypeTO.INFANT.equals(paxType) && getCustomInfantChargeTO() != null) {
			return true;
		}
		return false;
	}

	/**
	 * @return the compareWithExistingModificationCharges
	 */
	public boolean isCompareWithExistingModificationCharges() {
		return compareWithExistingModificationCharges;
	}

	/**
	 * @param compareWithExistingModificationCharges
	 *            the compareWithExistingModificationCharges to set
	 */
	public void setCompareWithExistingModificationCharges(boolean compareWithExistingModificationCharges) {
		this.compareWithExistingModificationCharges = compareWithExistingModificationCharges;
	}

	/**
	 * @return the existingModificationChargeAdult
	 */
	public BigDecimal getExistingModificationChargeAdult() {
		return existingModificationChargeAdult;
	}

	/**
	 * @param existingModificationChargeAdult
	 *            the existingModificationChargeAdult to set
	 */
	public void setExistingModificationChargeAdult(BigDecimal existingModificationChargeAdult) {
		this.existingModificationChargeAdult = existingModificationChargeAdult;
	}

	/**
	 * @return the existingModificationChargeChild
	 */
	public BigDecimal getExistingModificationChargeChild() {
		return existingModificationChargeChild;
	}

	/**
	 * @param existingModificationChargeChild
	 *            the existingModificationChargeChild to set
	 */
	public void setExistingModificationChargeChild(BigDecimal existingModificationChargeChild) {
		this.existingModificationChargeChild = existingModificationChargeChild;
	}

	/**
	 * @return the existingModificationChargeInfant
	 */
	public BigDecimal getExistingModificationChargeInfant() {
		return existingModificationChargeInfant;
	}

	/**
	 * @param existingModificationChargeInfant
	 *            the existingModificationChargeInfant to set
	 */
	public void setExistingModificationChargeInfant(BigDecimal existingModificationChargeInfant) {
		this.existingModificationChargeInfant = existingModificationChargeInfant;
	}

	public void setOndWiseCustomAdultCharge(Map<String, BigDecimal> ondWiseCustomAdultCharge) {
		this.ondWiseCustomAdultCharge = ondWiseCustomAdultCharge;
	}

	public Map<String, BigDecimal> getOndWiseCustomAdultCharge() {
		return ondWiseCustomAdultCharge;
	}

	public void setOndWiseCustomChildCharge(Map<String, BigDecimal> ondWiseCustomChildCharge) {
		this.ondWiseCustomChildCharge = ondWiseCustomChildCharge;
	}

	public Map<String, BigDecimal> getOndWiseCustomChildCharge() {
		return ondWiseCustomChildCharge;
	}

	public void setOndWiseCustomInfantCharge(Map<String, BigDecimal> ondWiseCustomInfantCharge) {
		this.ondWiseCustomInfantCharge = ondWiseCustomInfantCharge;
	}

	public Map<String, BigDecimal> getOndWiseCustomInfantCharge() {
		return ondWiseCustomInfantCharge;
	}

	public BigDecimal getDefaultCustomAdultCharge() {
		return defaultCustomAdultCharge;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public void setDefaultCustomAdultCharge(BigDecimal defaultCustomAdultCharge) {
		this.defaultCustomAdultCharge = defaultCustomAdultCharge;
	}

	public BigDecimal getDefaultCustomChildCharge() {
		return defaultCustomChildCharge;
	}

	public void setDefaultCustomChildCharge(BigDecimal defaultCustomChildCharge) {
		this.defaultCustomChildCharge = defaultCustomChildCharge;
	}

	public BigDecimal getDefaultCustomInfantCharge() {
		return defaultCustomInfantCharge;
	}

	public void setDefaultCustomInfantCharge(BigDecimal defaultCustomInfantCharge) {
		this.defaultCustomInfantCharge = defaultCustomInfantCharge;
	}

}
