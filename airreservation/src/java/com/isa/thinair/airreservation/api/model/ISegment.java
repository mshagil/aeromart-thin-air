/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.util.Date;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airreservation.api.dto.AutoCancellationDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;

/**
 * Segment interface where a client can use to store main segments information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ISegment extends IResSegment {
	/**
	 * Add outgoing segment
	 * 
	 * @param journeySeq
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 * @param returnOndGroupId
	 * @param bundledFarePeriodId
	 */
	public void addOutgoingSegment(int journeySeq, int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			Integer baggageOndGrpId, Integer bundledFarePeriodId, String codeShareFlightNumber, String codeShareBookingClass);

	/**
	 * Add return segment
	 * 
	 * @param journeySeq
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 * @param openRtConfirmBefore
	 * @param returnOndGroupId
	 * @param bundledFarePeriodId
	 */
	public void addReturnSegment(int journeySeq, int segmentSeq, int flightSegId, Integer ondGroupId, Date openRtConfirmBefore,
			Integer returnOndGroupId, Integer baggageOndGrpId, Integer bundledFarePeriodId, String codeShareFlightNumber,
			String codeShareBookingClass);

	/**
	 * Add passenger payment NOTE: Pass only parent and adult passenger ids
	 * 
	 * @param pnrPaxId
	 * @param iPayment
	 */
	public void addPassengerPayments(Integer pnrPaxId, IPayment iPayment);

	/**
	 * Over ride the zulu release time stamp Note: Pass Null if you don't want to over ride the release time stamp
	 * 
	 * @param zuluReleaseTimeStamp
	 */
	public void overrideZuluReleaseTimeStamp(Date zuluReleaseTimeStamp);

	/**
	 * Add Tempory Payment Entries
	 * 
	 * @param mapTnxIds
	 */
	public void addTemporyPaymentEntries(Map<Integer, CardPaymentInfo> mapTnxIds);

	public void setLastCurrencyCode(String onholdCurrencyCode);

	public void setLastModificationTimeStamp(Date lastModTimestamp);

	// Haider 19Apr09
	public void addInsurance(IInsuranceRequest insurance);

	public void setFareDiscount(float fareDiscount, String notes);

	public void setFareDiscountInfo(DiscountedFareDetails discountedFareDetails);

	public void setUserNote(String userNote);

	public void setExistingValidityInfo(Date firstDepDate, Long mostRestrMaxStayOver);

	public void addAutoCancellationInfo(AutoCancellationDTO autoCancellationDTO);

	public void setAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo);

	public void setApplicableAgentCommissions(AgentCommissionDetails agentCommissions);

}
