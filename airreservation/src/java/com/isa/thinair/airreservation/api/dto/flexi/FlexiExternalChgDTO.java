package com.isa.thinair.airreservation.api.dto.flexi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

public class FlexiExternalChgDTO extends ExternalChgDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer pnrSegId;

	private String flightRPH;

	private Collection<ReservationPaxOndFlexibilityDTO> reservationPaxOndFlexibilityDTOs = new ArrayList<ReservationPaxOndFlexibilityDTO>();

	public Collection<ReservationPaxOndFlexibilityDTO> getReservationPaxOndFlexibilityDTOs() {
		return reservationPaxOndFlexibilityDTOs;
	}

	public void setReservationPaxOndFlexibilityDTOs(Collection<ReservationPaxOndFlexibilityDTO> reservationPaxOndFlexibilityDTOs) {
		this.reservationPaxOndFlexibilityDTOs = reservationPaxOndFlexibilityDTOs;
	}

	public void addToReservationFlexibilitiesList(ReservationPaxOndFlexibilityDTO reservationPaxOndFlexibilityDTO) {
		this.reservationPaxOndFlexibilityDTOs.add(reservationPaxOndFlexibilityDTO);
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public String getFlightRPH() {
		return flightRPH;
	}

	public void setFlightRPH(String flightRPH) {
		this.flightRPH = flightRPH;
	}

	@Override
	public Object clone() {
		FlexiExternalChgDTO clone = new FlexiExternalChgDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		for (ReservationPaxOndFlexibilityDTO reservationPaxOndFlexibilityDTO : this.getReservationPaxOndFlexibilityDTOs()) {
			clone.addToReservationFlexibilitiesList((ReservationPaxOndFlexibilityDTO) reservationPaxOndFlexibilityDTO.clone());
		}
		clone.setFlightSegId(this.getFlightSegId());
		clone.setPnrSegId(this.getPnrSegId());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());
		clone.setFlightRPH(this.getFlightRPH());
		return clone;
	}
}
