/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.adl;

import java.util.ArrayList;

/**
 * This DTO will ecapsulate the PassengerCountRecord
 * 
 * @author isuru
 */
public class PassengerCountRecordDTO {
	private String destination;

	private int totalpassengers;

	private String fareClass;

	private ArrayList<ADLRecordDTO> addpnlrecords;

	private ArrayList<ADLRecordDTO> delpnlrecords;

	private ArrayList<ADLRecordDTO> chgRecords;

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getFareClass() {
		return fareClass;
	}

	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}

	public int getTotalpassengers() {
		return totalpassengers;
	}

	public void setTotalpassengers(int totalpassengers) {
		if (totalpassengers < 0) {
			this.totalpassengers = 0;
		} else {
			this.totalpassengers = totalpassengers;
		}

	}

	public ArrayList<ADLRecordDTO> getAddpnlrecords() {
		return addpnlrecords;
	}

	public void setAddpnlrecords(ArrayList<ADLRecordDTO> addpnlrecords) {
		this.addpnlrecords = addpnlrecords;
	}

	public ArrayList<ADLRecordDTO> getDelpnlrecords() {
		return delpnlrecords;
	}

	public void setDelpnlrecords(ArrayList<ADLRecordDTO> delpnlrecords) {
		this.delpnlrecords = delpnlrecords;
	}

	/**
	 * @return Returns the chgRecords.
	 */
	public ArrayList<ADLRecordDTO> getChgRecords() {
		return chgRecords;
	}

	/**
	 * @param chgRecords
	 *            The chgRecords to set.
	 */
	public void setChgRecords(ArrayList<ADLRecordDTO> chgRecords) {
		this.chgRecords = chgRecords;
	}

}
