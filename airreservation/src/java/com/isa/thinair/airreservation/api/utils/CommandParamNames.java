/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.utils;

/**
 * @author Lasantha Pambagoda
 */
public class CommandParamNames {

	public static final String PNR_PAX_ID = "pnrPaxId";
	public static final String PAX_TXN_ID = "paxTxnId";
	public static final String PNR_PAX_ID_TO = "pnrPaxIdTo";
	public static final String PNR_PAX_IDS = "pnrPaxIds";
	public static final String PNR_PAX_IDS_AND_PAYMENTS = "pnrPaxIdsAndPayments";
	public static final String IS_RESCHEDULE_FLIGHT = "isRescheduleFlight";
	public static final String IS_CANCEL_FLIGHT = "isCancelFlight";
	public static final String PNR_FLIGHT_SEG_MAP = "pnrSegFlightSegIdMap";

	public static final String CREDIT_TOTAL = "creditTotal";
	public static final String CANCEL_TOTAL = "cancelTotal";
	public static final String ADDED_TOTAL = "addedTotal";
	public static final String SEGMENT_TOTAL = "segmentTotal";
	public static final String TOTAL_PAYMENT_AMOUNT = "totalPaymentAmount";
	public static final String CHARGE_AMOUNT = "chargeAmount";
	public static final String CUSTOM_CHARGES = "customCharges";
	public static final String RAC_NEW_CREDIT_BALANCE = "newCreditBalance";
	public static final String MODIFY_AMOUNT = "modifyAmount";
	public static final String AMOUNT = "amount";
	public static final String PAYMENT_LIST = "paymentList";
	public static final String PAYMENT_INFO = "paymentInfoCol";
	public static final String PASSENGER_REVENUE_MAP = "modifyPerPaxSegTotal";
	public static final String PAX_TYPE_REVENUE_MAP = "PAX_TYPE_REVENUE_MAP";

	public static final String CANCEL_SEGMENT_INFORMATION = "cancelSegmentInformation";
	public static final String CANCEL_RESERVATION_SEGMENTS = "cancelReservationSegments";
	public static final String VACATED_SEATS = "vacatedSeats";
	public static final String VACATED_MEALS = "vacatedMeals";
	public static final String ADD_SEGMENT_INFORMATION = "addSegmentInformation";
	public static final String FLIGHT_ALERT_DTO = "flightAlertDTO";
	public static final String CREDIT_ID = "creditId";
	public static final String EXPIRY_DATE = "expiryDate";
	public static final String NOTE = "note";
	public static final String PAYMENT_AUTHID_MAP = "paymentRefNos";
	public static final String PAYMENT_BROKER_REF_MAP = "paymentBrokerRefNos";
	public static final String IS_CAPTURE_PAYMENT = "isCapturePayment";

	public static final String REFUND_ENTIRE_BOOKING = "refundEntireBooking";

	public static final String OTHER_CARRIER_CREDIT_USED = "isOtherCarrierPaxCredituse";

	public static final String IS_DUMMY_RESERVATION = "isDummyReservation";

	public static final String IS_FROM_NAME_CHANGE = "isFromNameChange";
	public static final String IS_TBA_NAME_CHANGE = "isTBANameChange";

	public static final String IS_CANCEL_CHG_OPERATION = "isCancelChargeOperation";
	public static final String IS_MODIFY_CHG_OPERATION = "isModifiyChargeOperation";

	public static final String FLIGHT_SEGMENT_IDS = "flightSegIds";

	public static final String IS_VOID_OPERATION = "isVoidOperation";

	public static final String UTILIZED_REFUNDABLES = "utilizeAllRefundables";
	public static final String IS_OPEN_RETURN_CONF_SEG_MODIFICATION = "isORConfimSegmentModifiation";

	public static final String IS_ADD_OND_OPERATION = "isAddOndOperation";
	public static final String IS_CANCEL_OND_OPERATION = "isCancelOndOperation";
	public static final String IS_MODIFY_OND_OPERATION = "isModifyOndOperation";

	public static final String APPLY_CANCEL_CHARGE = "applyCancelCharge";
	public static final String APPLY_MODIFY_CHARGE = "applyModifyCharge";

	public static final String HAS_EXT_CARRIER_PAYMENTS = "hasExtCarrierPayments";

	public static final String IS_CNX_FLOWN_SEGS = "isCnxFlownSegments";
	public static final String PNR_PAYMENT_TYPES = "pnrPaymentTypes";
	public static final String TRIGGER_BLOCK_SEATS = "triggerBlockSeats";
	public static final String TRIGGER_PAID_AMOUNT_ERROR = "triggerPaidAmountError";

	public static final String TRIGGER_PNR_FORCE_CONFIRMED = "triggerPnrForceConfirmed";
	public static final String ACCEPT_MORE_PAYMENTS_THAN_ITS_CHARGES = "acceptMorePaymentsThanItsCharges";
	public static final String IS_PAYMENT_FOR_ACQUIRE_CREDITS = "isPaymentForAcquireCredits";
	public static final String TRIGGER_ISOLATED_INFANT_MOVE = "triggerIsolatedInfantMove";
	public static final String NO_OF_CONFIRM_ADULT_SEATS = "noOfConfirmAdultSeats";
	public static final String NO_OF_CONFIRM_INFANT_SEATS = "noOfConfirmInfantSeats";

	public static final String ACTION_NC = "actionNC";
	public static final String CHARGE_NC = "chargeNC";

	public static final String PNR = "pnr";
	public static final String PNR_LIST = "pnrList";
	public static final String FIRST_PASSENGER_LAST_NAME = "firstPassengerLastName";
	public static final String OLD_PNR = "oldPnr";
	public static final String MOVING_PASSENGERS = "movingPassengers";
	public static final String REMOVE_INFANT_BC_MAP = "removeInfantBCMap";
	public static final String FLIGHT_RESERVATION_ALERT_DTOS = "flightReservationAlertDTOs";
	public static final String FLIGHT_RESERVATION_ALERT_DTOS_SEAT_LOST = "flightReservationAlertDTOseatLost";
	public static final String ON_HOLD_PNR = "onholdPnr";
	public static final String CONFIRM_PNR = "confirmPnr";
	public static final String RESERVATION = "reservation";
	public static final String OHD_RESERVATION = "ohdReservation";
	public static final String INFANTS = "infants";
	public static final String USER_NOTES = "userNotes";
	public static final String USER_NOTE_TYPE = "userNoteType";
	public static final String ENDORSEMENT_NOTES = "endorsementNotes";
	public static final String ORIGINAL_PAYMENT_ID = "originalPaymentId";
	public static final String SEG_OBJECTS_MAP = "segmentObjectsMap";
	public static final String RESERVATION_AUDIT_COLLECTION = "reservationAuditCollection";
	public static final String RESERVATION_REQUOTE_AUDIT_COLLECTION = "reservationRequoteAuditCollection";
	public static final String RECIEPT_NUMBER = "recieptNumber";
	public static final String PAYMENT_GATEWAY_BILL_NUMBER = "pgwBillNumber";
	public static final String PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY = "pgwBillNumberLabelKey";
	public static final String SEAT_NO_REPROTECT_No_RETURN = "noReprotectNoReturn";
	public static final String PAX_CREDIT_PAYMENTS = "paxCreditPayments";
	public static final String IS_DUPLICATE_NAME_SKIP = "duplicateNameSkip";
	public static final String EXT_RECORD_LOCATOR = "extRecordLocator";
	public static final String GDS_NOTIFY_ACTION = "gdsNotifyAction";
	public static final String IS_REFUND_ONLY_NOSHOWPAX = "refundOnlyNSPax";
	public static final String IS_SEGMENT_TRANSFERRED_BY_NOREC_PROCESS = "isSegmentTransferredByNorecProcess";
	public static final String REASON_FOR_ALLOW_BLACKLISTED_PAX = "reasonForAllowBlPax";
	public static final String REPROTECT_FLT_SEG_IDS = "repretectFltSegIds";
	public static final String VISIBLE_IBE_ONLY = "visibleIBEonly";
	public static final String TRANSFER_FLT_SEG = "transferFltSeg";

	public static final String OWNER_AGENT_CODE = "ownerAgentCode";
	public static final String RESERVATION_CONTACT_INFO = "reservationContactInfo";
	public static final String PNR_SEGMENT_IDS = "pnrSegmentIds";
	public static final String CREDENTIALS_DTO = "credentialsDTO";
	public static final String PREFERRED_REFUND_ORDER = "prefferedRefundOrder";
	public static final String PNR_PAX_OND_CHARGE_IDS = "pnrPaxOndChgIds";
	public static final String EXT_PAX_TNX_MAP = "externalPaxTxnMap";
	public static final String USER_ID = "userId";
	public static final String PNR_PAX_CHARGES = "pnrPaxCharges";

	public static final String IS_VOID_BOOKING = "isVoidBooking";

	public static final String RESERVATION_PAX_PAYMENT_META_TO = "reservationPaxPaymentMetaTO";
	public static final String IS_PAX_CANCEL = "isPaxCancel";
	public static final String IS_PAX_SPLIT = "isPaxSplit";
	public static final String IS_REMOVE_PAX = "isRemovePax";
	public static final String RESERVATION_SEGMENTS = "reservationSegments";
	public static final String RESERVATION_EXTERNAL_SEGMENTS = "reservationExternalSegments";
	public static final String RESERVATION_PAX_FARE_FOR_ADULT = "reservationPaxFareForAdult";
	public static final String RESERVATION_PAX_FARE_FOR_CHILD = "reservationPaxFareForChild";
	public static final String RESERVATION_PAX_FARE_FOR_INFANT = "reservationPaxFareForInfant";
	public static final String RESERVATION_OTHER_AIRLINE_SEGMENTS = "reservationOtherAirlineSegments";

	public static final String TRANSFER_PAX_DTO = "transferSeatDTO";
	public static final String TRANSFER_SEAT_MSG = "transferSeatMessage";

	public static final String OND_FARE_DTOS = "inventoryFares";
	public static final String BLOCK_KEY_IDS = "blockKey";
	public static final String TEMPORY_PAYMENT_IDS = "colTnxIds";
	public static final String OVERRIDDEN_ZULU_RELEASE_TIMESTAMP = "overriddenZuluReleaseTimeStamp";
	public static final String ORDERED_NEW_FLIGHT_SEGMENT_IDS = "orderedNewFlightSegmentIds";
	public static final String FARE_DISCOUNT_DTO = "fareDiscountInfo";
	public static final String FARE_DISCOUNT_NOTE = "fareDiscountNote";
	public static final String FARE_DISCOUNT_AUDIT = "fareDiscountAudit";
	public static final String FARE_DISCOUNT_CODE = "fareDiscountCode";
	public static final String IS_MANUAL_REFUND = "isManualRefund";
	// contains the object
	public static final String FARE_DISCOUNT_INFO = "fareDiscountDTOInfo";

	public static final String AGENT_COMMISSION_DTO = "agentCommissionDTO";
	public static final String REFUNDED_PAX_OND_CHG_IDS = "refundedPaxOndChgIds";
	public static final String REMOVE_AGENT_COMMISSION = "removeAgentCommission";
	public static final String REMOVE_AGENT_COM_PNR_PAX_IDS = "removeAgentComPnrPaxIds";

	public static final String ONHOLD_OR_FORCE_CONFIRM = "onHoldOrForceConfirm";
	public static final String IS_ALEART = "isAleart";
	public static final String OVER_ALLOC_ENUM = "overAllocFlag";
	public static final String OVER_ALLOC_SEATS = "overAllocSeats";
	public static final String ROLLFORWARD_FROM_DATE = "rollFromDate";
	public static final String ROLLFORWARD_TO_DATE = "rollToDate";
	public static final String ROLLFORWARD_INCLUDE_FREQUENCY = "rollIncludeFrequency";
	public static final String INCLUDE_CLS_FLIGHTS = "includeClsFlights";
	public static final String OUTPUT_SERVICE_RESPONSE = "outPutServiceResponse";
	public static final String VERSION = "version";
	public static final String IS_ROLLFORWARD = "isRollforward";
	public static final String PFS_ID = "pfsId";
	public static final String XA_PNL_ID = "xaPnlId";
	public static final String ETL_ID = "etlId";
	public static final String PRL_ID = "prlId";
	public static final String ORIGINAL_SEGMENT_TOTAL = "originalSegmentTotal";
	public static final String FARE_CHANGES = "fareChanges";
	public static final String TRIGGER_EXTERNAL_PAY_TX_UPDATE = "triggerExtPayTxUpdate";
	public static final String EXTERNAL_PAY_TX_INFO = "extPayTxInfo";
	public static final String ONACC_AGENTCODE_FOR_EXT_PAY = "extPayAgentCode";
	public static final String TRIGGER_EXTERNAL_PAY_TX_MANUAL_RECON = "triggerExtPayTxManualRecon";
	public static final String E_TICKET_MAP = "eTicketMap";

	public static final String RESERVATION_PAYMENT_META_TO = "reservationPaymentMetaTO";
	public static final String REFUNDABLE_CHARGES = "refundableCharges";

	public static final String INSURANCE_INFO = "insuranceInfo";
	public static final String INSURANCE_RES = "insuranceRes";
	public static final String INSURANCE_HOLD = "insuranceHold";
	public static final String INSURANCE_CHK_HOLD = "insuranceCheckIfOnholdInsuranceExist";
	public static final String NEW_ORIGINATOR_PNR = "newOriginatorPNR";
	public static final String NEW_CARRIER_PNR = "newCarrierPNR";
	public static final String FLIGHT_SEAT_IDS_AND_PAX_TYPES = "flightSeatIdsAndPaxTypes";
	public static final String REMOVE_INFANT_PARENT_PAX_FARE_IDS = "removeInfantParentPaxFareIds";
	public static final String IS_CHECKIN_PROCESS = "isCheckinProcess";
	public static final String IS_PROCESS_PFS = "isProcessPFS";
	public static final String PFS_PAX_STATUS = "pfsPaxStatus";

	public static final String LAST_MODIFICATION_TIMESTAMP = "lastModificationTimeStamp";
	public static final String ZULU_BOOKING_TIMESTAMP = "zuluBookingTimestamp";
	public static final String LAST_CURRENCY_CODE = "lastCurrencyCode";
	public static final String FLIGHT_MEAL_IDS = "flightMealIds";
	public static final String FLIGHT_AIRPORT_TRANSFER_IDS = "airportTransferIds";
	public static final String ENABLE_TRANSACTION_GRANULARITY = "enableTransactionGranularity";
	public static final String ORIGINAL_PAYMENT_TNX_ID = "originalPaymentTnxID";
	public static final String BOOKING_TYPE = "bookingType";

	public static final String OPENRT_SEGMENT_IDS = "openReturnSegmentIds";

	public static final String LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS = "paxProductRedeemedAmounts";
	public static final String LOYALTY_PRODUCT_REDEEMED_AMOUNTS = "lmsProductRedeemedAmounts";
	
	public static final String SALES_CHANNEL_KEY = "salesCahnnelKey";

	public static final String CHECK_FOR_NEW_TRANSACTION = "checkForNewTransaction";

	/**
	 * For baggage
	 */
	public static final String FLIGHT_BAGGAGE_IDS = "flightBaggageIds";

	/**
	 * For AutoCheckin Ancillary
	 */
	public static final String FLIGHT_AUTO_CHECKIN_IDS = "flightAutoCheckinIds";

	public static final String PAX_SEGMENT_SSR_MAPS = "paxSegmentSSRMaps";
	public static final String CANCELLED_PAX_SEGMENT_SSR_MAP = "cancelledPaxSegmentSSRMap";
	public static final String NEW_SEGMENT_SEQUENCE_NUMBERS = "newSegmentSequenceNos";
	public static final String NEW_NONSTB_SEGMENT_SEQUENCE_NUMBERS = "newNonSTBSegmentSequenceNos";
	public static final String CHECK_DUPLICATE_NAMES = "duplicateNameCheck";
	public static final String APPLY_NAME_CHANGE_CHARGE = "applyNameChangeCharge";
	public static final String NAME_CHANGE_PAX_MAP = "nameChangePaxMap";
	public static final String UPDATED_PAX_FLEXIBILITIES_MAP = "updatedPaxFlexibilitiesMap";
	public static final String FLEXI_RETAIN_DTO = "flexiRetainDTO";
	public static final String UPDATED_FLEXIBILITIES_INFORMATION = "updatedFlexibilitiesInformation";

	public static final String CURRENT_TIMESTAMP = "currentTimestamp";
	public static final String RESERVATION_ADMIN_INFO_TO = "reservationAdminInfoTO";

	public static final String PAX_CREDIT_TO = "paxCreditTO";

	public static final String CC_FRAUD_ENABLE = "creditCaradFraudCheckEnable";
	public static final String RESERVATIN_TRACINFO = "reservationTracInfo";

	public static final String FLEXI_INFO = "resFlexiInfo";
	public static final String IS_FLEXI_SELECTED = "isFlexiSelected";
	public static final String RESERVATION_TYPE = "reservationType";
	public static final String CANCELLED_SURFACE_SEGMENT_LIST = "cnxSurfaceSegList";
	public static final String CONNECTING_FLT_SEG_ID = "connectingPnrSeg";
	public static final String SET_PREVIOUS_CREDIT_CARD_PAYMENT = "setPreviousCreditCardPayment";

	public static final String IS_GOSHOW_PROCESS = "isGoshowProcess";
	public static final String PAYMENT_CARD_CONFIG_DATA = "paymentCardConfigData";
	public static final String PFS_CHECKIN_METHOD = "pfsCheckinMethod";
	public static final String IS_CONSIDER_FUTURE_DATE_VALIDATION = "isConsiderFutureDateValidation";

	public static final String FLIGHT_SEGMENT_DTO_SET = "flightSegmentDTOs";

	public static final String PAYPAL_REQUEST_DTO = "paypalRequestDTO";
	public static final String BOOKING_DEPARTURE_DATE_TIME = "bookingDepartureDateTime";

	public static final String RES_FLIGHT_SEGMENT_DTO = "reservationFlightSegmentDTO";
	public static final String ROLL_FORWARD_FLIGHT_LIST = "rollForwardFlightList";
	public static final String AVILABLE_FLIGHT_SEARCH_DTO = "AvailableFlightSearchDTO";

	public static final String TRACK_INFO_DTO = "trackInfoDTO";
	/**
	 * Requote related parameters
	 */
	public static final String BALANCE_QUERY_DTO = "BALANCE_QUERY_DTO";
	public static final String BALANCE_SUMMARY_DTO = "BALANCE_SUMMARY_DTO";
	public static final String REQUOTE_MODIFY_RQ = "REQUOTE_MODIFY_RQ";
	public static final String EXCHANGE_PNR_SEGMENT_IDS = "EXCHANGE_PNR_SEGMENT_IDS";
	public static final String EXCHANGE_SEG_INV_IDS = "EXCHANGE_SEG_INV_IDS";
	public static final String IS_REQUOTE = "IS_REQUOTE";
	public static final String FLOWN_ASSIT_UNIT = "FLOWN_ASSIT_UNIT";
	public static final String UPDATE_FLOWN_VALIDITY = "UPDATE_FLOWN_VALIDITY";
	public static final String TICKET_VALID_TILL_DATE = "TICKET_VALID_TILL_DATE";
	public static final String TICKET_VALID_FLT_SEG_IDS = "TICKET_VALID_FLT_SEG_IDS";
	public static final String PENALTY_AMOUNT = "PENALTY_AMOUNT";
	public static final String BC_NON_CHANGED_FLT_SEG_IDS = "BC_NON_CHANGED_FLT_SEG_IDS";
	public static final String DO_REV_PURCHASE = "DO_REV_PURCHASE";
	public static final String FLOWN_PNR_SEGMENT_IDS = "FLOWN_PNR_SEGMENT_IDS";
	public static final String TYPE_B_REQUEST = "TYPE_B_REQUEST";
	public static final String CS_TYPE_B_REQUEST = "CS_TYPE_B_REQUEST";

	public static final String EXCHG_REPROTECTED_EXTERNAL_CHARGES = "EXCHG_REPROTECTED_EXTERNAL_CHARGES";
	public static final String NEW_AND_REPROTECT_EXT_CHARGES = "NEW_AND_REPROTECT_EXT_CHARGES";
	public static final String EXCHG_REPROTD_SSR_FLTSEG_INVENT = "EXCHG_REPROTD_SSR_FLTSEG_INVENT";
	public static final String EFFECTIVE_PAX_TAX_CHARGES = "EFFECTIVE_PAX_TAX_CHARGES";

	public static final String AUDIT_ACTION_PROCESS = "PROCESS";
	public static final String AUDIT_ACTION_STATUS_ERROR = "ERROR";
	public static final String AUDIT_ACTION_STATUS_SUCCESS = "SUCCESS";

	public static final String PAX_REFUND_TYPE = "paxRefundType";
	public static final String PAX_EXTERNAL_CHARGE_MAP = "paxExtChgMap";
	public static final String PAX_TAXING_EXT_CHARGE_MAP = "paxTaxingExtChgMap";

	public static final String ALLOW_CREATE_RESERVATION_AFTER_CUT_OFF_TIME = "isAllowReservationAfterCutOffTime";

	public static final String AUDIT_DETAILS = "auditDetails";
	public static final String AUTO_CANCELLATION_INFO = "autoCancellationInfo";

	public static final String REQUOTE_SEGMENT_MAP = "requoteSegmentMap";
	public static final String IS_ACTUAL_PAYMENT = "isActualPayment";
	public static final String IS_NO_BALANCE_TO_PAY = "isNoBalanceToPay";
	public static final String IS_FIRST_PAYMENT = "isFirstPayment";
	public static final String TRANSACTION_SEQ = "transactionSeq";

	public static final String APPLICABLE_PROMOTION_DETAILS = "applicablePromotionDetails";
	public static final String USED_PROMOTION_CREDITS = "usedPromotionCredits";

	public static final String FARE_ADJUSTMENT_VALID = "fareAdjustmentValid";
	public static final String IS_OHD_RELEASE = "isOnholdRelease";

	public static final String IS_CNX_MOD_FEE_CHANGE_AUDIT_NEED = "isCNXMODAuditNeed";
	public static final String CUSTOM_CHARGE_TO = "customChargeTo";

	public static final String GROUP_BOOKING_REQUEST_ID = "groupBookingRequestID";
	public static final String PAX_PAY_CREDIT_UTILIZED_MAP_BY_TNX = "paymentCreditUtilizedMapByTnx";
	public static final String SELECTED_ANCI_MAP = "anciMap";
	
	public static final String RESERVATION_STATUS_BEFORE_UPDATE = "reservationStatusBeforeUpdate";

	/*
	 * Group booking response key
	 */
	public static final String RESPONSE_KEY = "responseKey";
	public static final String GROUND_SEG_BY_AIR_SEG_ID_MAP = "groundFltSegByFltSegId";

	public static final String PAX_ADJUSTMENT_AMOUNT_MAP = "paxWiseAdjustmentAmountMap";
	public static final String OND_WISE_PAX_DUE_AMOUNT_MAP = "ondWisePaxDueAdjMap";

	public static final String SPLITTED_PAX_IDS = "splittedPaxIds";

	public static final String ALLOWED_SUB_OPERATIONS = "allowedSubOperations";
	public static final String GDS_CHARGE_BY_PAX = "gdsChargesByPax";

	public static final String OWNER_CHANNEL_ID = "ownerChannelId";

	public static final String PROMO_CALCULATOR_RQ = "promoCalculatorRQ";
	public static final String RESERVATION_DISCOUNT_DTO = "reservationDiscountDTO";

	public static final String IS_GDS_SALE = "isGdsSale";

	/**
	 * GDS Coupon exchange related
	 */
	public static final String FLOWN_PNR_SEG_ID_LIST = "flownPnrSegIdList";
	public static final String EXCHANGE_PNR_SEG_ID_LIST = "exchangePnrSegIdList";
	public static final String EXCHANGE_FLT_SEG_ID_LIST = "exchangeFltSegIdList";
	public static final String CNX_PNR_SEG_ID_LIST = "cnxPnrSegIdList";
	public static final String EXCHANGE_PAX_FARE_MAP = "exchangePaxFareMap";
	public static final String EXCHANGE_PNR_SEGMENT_MAP = "exchangePnrSegmentMap";
	public static final String EXCHANGE_PNR_SEG_ID_MAP = "exchangePnrSegIdMap";
	public static final String EXCHANGE_TIKCET_TRANSITION_MAP = "exchangeTicketTransitionMap";

	public static final String ETICKET_HAVING_PASSENGERS = "eticketHavingPassengers";

	/**
	 * For PNL ADL
	 */
	public static final String PNL_DATA_CONTEXT = "baseDataContext";
	public static final String PNL_ADDITIONAL_DATA_CONTEXT = "additionalDataContext";
	public static final String PNL_PASSENGER_COLLECTION = "passengerCollection";
	public static final String PNL_MESSAGES_PARTS = "messageParts";
	public static final String PNL_MESSAGE_RESPONSE_ADDITIONALS = "messageBuilderResponseAdditionals";

	/**
	 * For ResModification Detector
	 */
	public static final String RES_MODIFY_DETECT_DATA_CONTEXT = "resModificationDataContext";
	public static final String RES_MODIFY_OPTIMIZED_RULE_LIST = "resModificationOptRules";
	public static final String  MODIFY_DETECTOR_RES_BEFORE_MODIFY = "resBeforeModify";
	public static final String  MODIFY_DETECTOR_RES_AFTER_MODIFY = "resAfterModify";
	
	public static final String  MODIFY_DETECTOR_IS_GROUP_MODIFY = "resIsGroupModify";
	public static final String  MODIFY_DETECTOR_RES_LIST_BEFORE_MODIFY = "resListBeforeModify";
	public static final String  MODIFY_DETECTOR_PNR_LIST = "pnrList";

	/**
	 * Transaction related parameters
	 */
	public static final String TRNX_SEGMENTS = "transactionSegmentIds";

	/**
	 * Charge Reverser
	 */
	public static final String REVERSABLE_CHARGE_GROUPS = "reversableChargeGroups";
	public static final String REVERSABLE_CHARGE_CODES = "reversableChargeCodes";
	public static final String IS_NO_SHOW_TAX_REVERSAL = "isNoShowTaxReversal";
	
	public static final String TAX_INVOICE_NUMBERS = "taxInvoiceNumbers";

	public static final String USER_PRINCIPAL = "userPrincipal";
	public static final String PAX_ADJ_ASSEMBLER = "paxAdjAssembler";
	public static final String CHG_TRNX_GEN = "chgTnxGen";
	public static final String CMI_FATOURATI_REF = "fatoRef";

	
}
