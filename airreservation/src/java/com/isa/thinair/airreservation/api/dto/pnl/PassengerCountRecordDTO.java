/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.pnl;

import java.util.ArrayList;

/**
 * @author isuru This class will wrap the PassengerCountRecordDTO DTO convention may be violated due to template file
 *         dependencies
 */
public class PassengerCountRecordDTO {
	private String destination;

	private int totalpassengers;

	private String fareClass;

	private ArrayList<PNLRecordDTO> pnlrecords;

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getFareClass() {
		return fareClass;
	}

	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}

	public int getTotalpassengers() {
		return totalpassengers;
	}

	public void setTotalpassengers(int totalpassengers) {
		this.totalpassengers = totalpassengers;
	}

	public ArrayList<PNLRecordDTO> getPnlrecords() {
		return pnlrecords;
	}

	public void setPnlrecords(ArrayList<PNLRecordDTO> pnlrecords) {
		this.pnlrecords = pnlrecords;
	}

}
