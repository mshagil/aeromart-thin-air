package com.isa.thinair.airreservation.api.dto.flexi;

import java.io.Serializable;

public class ReservationPaxOndFlexibilityDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer flexiRuleID;

	private int ppOndFlxId;

	private int ppfId;

	private int flexiRateId;

	private int flexibilityTypeId;

	private int availableCount;

	private int utilizedCount;

	private long cutOverBufferInMins;

	private String status;

	private String description;

	public int getPpOndFlxId() {
		return ppOndFlxId;
	}

	public void setPpOndFlxId(int ppOndFlxId) {
		this.ppOndFlxId = ppOndFlxId;
	}

	public int getPpfId() {
		return ppfId;
	}

	public void setPpfId(int ppfId) {
		this.ppfId = ppfId;
	}

	public int getFlexiRateId() {
		return flexiRateId;
	}

	public void setFlexiRateId(int flexiRateId) {
		this.flexiRateId = flexiRateId;
	}

	public int getFlexibilityTypeId() {
		return flexibilityTypeId;
	}

	public void setFlexibilityTypeId(int flexibilityTypeId) {
		this.flexibilityTypeId = flexibilityTypeId;
	}

	public int getAvailableCount() {
		return availableCount;
	}

	public void setAvailableCount(int availableCount) {
		this.availableCount = availableCount;
	}

	public int getUtilizedCount() {
		return utilizedCount;
	}

	public void setUtilizedCount(int utilizedCount) {
		this.utilizedCount = utilizedCount;
	}

	public long getCutOverBufferInMins() {
		return cutOverBufferInMins;
	}

	public void setCutOverBufferInMins(long cutOverBufferInMins) {
		this.cutOverBufferInMins = cutOverBufferInMins;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object clone() {
		ReservationPaxOndFlexibilityDTO paxOndFlexibilityDTO = new ReservationPaxOndFlexibilityDTO();
		paxOndFlexibilityDTO.setAvailableCount(this.getAvailableCount());
		paxOndFlexibilityDTO.setCutOverBufferInMins(this.getCutOverBufferInMins());
		paxOndFlexibilityDTO.setFlexibilityTypeId(this.getFlexibilityTypeId());
		paxOndFlexibilityDTO.setFlexiRateId(this.getFlexiRateId());
		paxOndFlexibilityDTO.setPpfId(this.getPpfId());
		paxOndFlexibilityDTO.setPpOndFlxId(this.getPpOndFlxId());
		paxOndFlexibilityDTO.setStatus(this.getStatus());
		paxOndFlexibilityDTO.setUtilizedCount(this.getUtilizedCount());
		paxOndFlexibilityDTO.setDescription(this.getDescription());
		return paxOndFlexibilityDTO;
	}

	/**
	 * @return the flexiRuleID
	 */
	public Integer getFlexiRuleID() {
		return flexiRuleID;
	}

	/**
	 * @param flexiRuleID
	 *            the flexiRuleID to set
	 */
	public void setFlexiRuleID(Integer flexiRuleID) {
		this.flexiRuleID = flexiRuleID;
	}

}
