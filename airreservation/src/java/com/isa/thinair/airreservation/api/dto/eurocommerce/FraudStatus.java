package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;

public enum FraudStatus implements Serializable {

	APPROVED("Approved"), REVIEW("Review"), DECLINED("Declined"), OTHER("Other"), ERROR("Error");

	private String status;

	private FraudStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public static FraudStatus getStatus(String status) {
		for (FraudStatus fraudStatus : FraudStatus.values()) {
			if (fraudStatus.getStatus().equals(status)) {
				return FraudStatus.valueOf(status.toUpperCase());
			}
		}
		return FraudStatus.OTHER;
	}
}
