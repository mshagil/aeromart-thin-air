/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * The model to keep track of credit promotion details, since it will not applied to the same reservation we might need this
 * details for future usage to track how the promotion was derived from original Reservation
 * 
 * @author M.Rikaz
 * @hibernate.class table = "T_PNR_PAX_OND_CREDIT_PROMOTION"
 */
public class ReservationPaxOndCreditPromotion extends Persistent implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds Reservation passenger ond charge id */
	private Integer pnrPaxOndChgCredPromoId;

	/** Holds the ond fare id */
	private Integer fareId;

	/** Holds the charge rate id */
	private Integer chargeRateId;

	/** Holds charge amount */
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds charge date */
	private Date zuluChargeDate;

	/** Holds charge group code */
	private String chargeGroupCode;

	/** Holds origin agent code */
	private String agentCode;

	/** Holds origin user id */
	private String userId;

	private BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public ReservationPaxOndCreditPromotion() {

	}

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the chargeRateId.
	 * @hibernate.property column = "CHARGE_RATE_ID"
	 */
	public Integer getChargeRateId() {
		return chargeRateId;
	}

	/**
	 * @param chargeRateId
	 *            The chargeRateId to set.
	 */
	public void setChargeRateId(Integer chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	/**
	 * @return Returns the chargeGroupCode.
	 * @hibernate.property column = "CHARGE_GROUP_CODE"
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            The chargeGroupCode to set.
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return Returns the fareId.
	 * @hibernate.property column = "FARE_ID"
	 */
	public Integer getFareId() {
		return fareId;
	}

	/**
	 * @param fareId
	 *            The fareId to set.
	 */
	public void setFareId(Integer fareId) {
		this.fareId = fareId;
	}

	/**
	 * @return Returns the pnrPaxOndChgId.
	 * @hibernate.id column = "PPOCP_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_OND_CREDIT_PROMOTION"
	 */
	public Integer getPnrPaxOndChgCredPromoId() {
		return pnrPaxOndChgCredPromoId;
	}

	/**
	 * @param pnrPaxOndChgId
	 *            The pnrPaxOndChgId to set.
	 */
	public void setPnrPaxOndChgCredPromoId(Integer pnrPaxOndChgCredPromoId) {
		this.pnrPaxOndChgCredPromoId = pnrPaxOndChgCredPromoId;
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPnrPaxOndChgCredPromoId()).toHashCode();
	}
	
	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationPaxOndCreditPromotion) {
			ReservationPaxOndCreditPromotion castObject = (ReservationPaxOndCreditPromotion) o;
			if (castObject.getPnrPaxOndChgCredPromoId() == null || this.getPnrPaxOndChgCredPromoId() == null) {
				return false;
			} else if (castObject.getPnrPaxOndChgCredPromoId().intValue() == this.getPnrPaxOndChgCredPromoId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * @return Returns the zuluChargeDate.
	 * @hibernate.property column = "CHARGE_DATE"
	 */
	public Date getZuluChargeDate() {
		return zuluChargeDate;
	}

	/**
	 * @param zuluChargeDate
	 *            The zuluChargeDate to set.
	 */
	public void setZuluChargeDate(Date zuluChargeDate) {
		this.zuluChargeDate = zuluChargeDate;
	}

	/**
	 * @return the agentCode
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the discount.
	 * @hibernate.property column = "DISCOUNT"
	 */
	public BigDecimal getDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            The discount to set.
	 */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
}
