/**
 * 	mano
	May 10, 2011 
	2011
 */
package com.isa.thinair.airreservation.api.dto.baggage;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author mano
 * 
 */
public class PassengerBaggageAssembler implements Serializable, IPassengerBaggage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, PaxBaggageTO> paxBaggageInfo = new HashMap<String, PaxBaggageTO>();

	// pnrPaxId|pnrPaxFareId|flightSegId
	private Map<String, PaxBaggageTO> paxBaggageTOAdd = new HashMap<String, PaxBaggageTO>();
	private Map<String, PaxBaggageTO> paxBaggageTORemove = new HashMap<String, PaxBaggageTO>();

	private Map<Integer, Integer> flightBaggageIdPnrSegId = new HashMap<Integer, Integer>();
	private Map<String, Collection<Integer>> statusAndFlightBaggageIdMap = new HashMap<String, Collection<Integer>>();

	private IPassenger passenger;

	private boolean isForceConfirmed;

	private PaxAdjAssembler adjAssembler;

	public PassengerBaggageAssembler(IPassenger passenger, boolean isForceConfirmed) {
		this(passenger, null, isForceConfirmed);
	}

	public PassengerBaggageAssembler(IPassenger passenger, PaxAdjAssembler adjAssembler, boolean isForceConfirmed) {
		this.passenger = passenger;
		this.isForceConfirmed = isForceConfirmed;
		this.adjAssembler = adjAssembler;
	}

	public Map<String, Collection<Integer>> getStatusFlightBaggageIdsMap() {
		return statusAndFlightBaggageIdMap;
	}

	public Map<Integer, Integer> getFlightBaggageIdPnrSegIdsMap() {
		return flightBaggageIdPnrSegId;
	}

	/**
	 * @return the paxBaggageTOAdd
	 */
	public Map<String, PaxBaggageTO> getPaxBaggageTOAdd() {
		return paxBaggageTOAdd;
	}

	/**
	 * @param paxBaggageTOAdd
	 *            the paxBaggageTOAdd to set
	 */
	public void setPaxBaggageTOAdd(Map<String, PaxBaggageTO> paxBaggageTOAdd) {
		this.paxBaggageTOAdd = paxBaggageTOAdd;
	}

	/**
	 * @return the paxBaggageTORemove
	 */
	public Map<String, PaxBaggageTO> getPaxBaggageTORemove() {
		return paxBaggageTORemove;
	}

	/**
	 * @param paxBaggageTORemove
	 *            the paxBaggageTORemove to set
	 */
	public void setPaxBaggageTORemove(Map<String, PaxBaggageTO> paxBaggageTORemove) {
		this.paxBaggageTORemove = paxBaggageTORemove;
	}

	/**
	 * @return the passenger
	 */
	public IPassenger getPassenger() {
		return passenger;
	}

	/**
	 * @param passenger
	 *            the passenger to set
	 */
	public void setPassenger(IPassenger passenger) {
		this.passenger = passenger;
	}

	/**
	 * @return the isForceConfirmed
	 */
	public boolean isForceConfirmed() {
		return isForceConfirmed;
	}

	/**
	 * @param isForceConfirmed
	 *            the isForceConfirmed to set
	 */
	public void setForceConfirmed(boolean isForceConfirmed) {
		this.isForceConfirmed = isForceConfirmed;
	}

	@Override
	public void addBaggageInfo(Integer pnrPaxId, Integer pnrPaxFareId, Integer flightBaggageId, Integer pnrSegId,
			BigDecimal chargeAmount, Integer baggageId, Integer baggageChrgId, String baggageOndGroupId, Integer autoCnxId,
			TrackInfoDTO trackInfo) throws ModuleException {

		PaxBaggageTO paxBaggageTO = new PaxBaggageTO();
		paxBaggageTO.setPnrPaxId(pnrPaxId);
		paxBaggageTO.setPnrSegId(pnrSegId);
		paxBaggageTO.setChgDTO(getExternalChgDTOForBaggage(chargeAmount, trackInfo));
		paxBaggageTO.setSelectedFlightBaggageId(flightBaggageId);
		paxBaggageTO.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED);
		paxBaggageTO.setPnrPaxFareId(pnrPaxFareId);
		paxBaggageTO.setBaggageId(baggageId);
		paxBaggageTO.setBaggageChrgId(baggageChrgId);
		paxBaggageTO.setBaggageOndGroupId(baggageOndGroupId);

		if (chargeAmount != null
				&& AccelAeroCalculator.isGreaterThan(chargeAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			paxBaggageTO.setAutoCancellationId(autoCnxId);
		}

		String uniqueId = BaggageUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);
		paxBaggageInfo.put(uniqueId, paxBaggageTO);
		flightBaggageIdPnrSegId.put(pnrPaxFareId, pnrSegId);
		if (adjAssembler != null) {
			adjAssembler.addCharge(pnrPaxId, chargeAmount);
		}

	}

	private ExternalChgDTO getExternalChgDTOForBaggage(BigDecimal charge, TrackInfoDTO trackInfo) throws ModuleException {
		if (charge == null) {
			return null;
		}

		Collection<ReservationInternalConstants.EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.BAGGAGE);
		Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationModuleUtils
				.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, trackInfo, null);
		ExternalChgDTO externalChgDTO = (ExternalChgDTO) ((ExternalChgDTO) mapExternalChgs.get(EXTERNAL_CHARGES.BAGGAGE)).clone();
		externalChgDTO.setAmount(charge);
		return externalChgDTO;
	}

	public void removePassengerFromFlightBaggage(Integer pnrPaxId, Integer pnrPaxFareId, Integer pnrSegId,
			Integer flightBaggageId, BigDecimal chargeAmount, Integer baggageId, String paxName, TrackInfoDTO trackInfo)
			throws ModuleException {

		PaxBaggageTO paxBaggageTO = new PaxBaggageTO();
		paxBaggageTO.setPnrPaxId(pnrPaxId);
		paxBaggageTO.setPnrSegId(pnrSegId);
		paxBaggageTO.setChgDTO(getExternalChgDTOForBaggage(chargeAmount.negate(), trackInfo));
		paxBaggageTO.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.PAX_CNX);
		paxBaggageTO.setSelectedFlightBaggageId(flightBaggageId);
		paxBaggageTO.setPnrPaxFareId(pnrPaxFareId);
		paxBaggageTO.setBaggageId(baggageId);
		paxBaggageTO.setPaxName(paxName);
		paxBaggageTO.setAutoCancellationId(null);
		String uniqueId = BaggageUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);

		paxBaggageTORemove.put(uniqueId, paxBaggageTO);
		putBaggage(flightBaggageId, AirinventoryCustomConstants.FlightBaggageStatuses.VACANT);

		if (adjAssembler != null) {
			adjAssembler.removeCharge(pnrPaxId, chargeAmount);
		}

	}

	private void putBaggage(Integer flightBaggageId, String status) {
		Collection<Integer> flightBaggageIds = statusAndFlightBaggageIdMap.get(status);
		if (flightBaggageIds == null) {
			flightBaggageIds = new ArrayList<Integer>();
			flightBaggageIds.add(flightBaggageId);
			statusAndFlightBaggageIdMap.put(status, flightBaggageIds);
		} else {
			flightBaggageIds.add(flightBaggageId);
		}
	}

	public void addPassengertoNewFlightBaggage(Integer pnrPaxId, Integer pnrPaxFareId, Integer pnrSegId, Integer flightBaggageId,
			BigDecimal chargeAmount, String baggageName, Integer baggageId, String paxName, Integer baggageChrgId,
			String baggageOndGroupId, Integer autoCnxId, TrackInfoDTO trackInfo) throws ModuleException {

		PaxBaggageTO paxBaggageTO = new PaxBaggageTO();
		paxBaggageTO.setChgDTO(getExternalChgDTOForBaggage(chargeAmount, trackInfo));
		paxBaggageTO.setPnrPaxId(pnrPaxId);
		paxBaggageTO.setPnrSegId(pnrSegId);
		paxBaggageTO.setStatus(AirinventoryCustomConstants.FlightMealStatuses.RESERVED);
		paxBaggageTO.setSelectedFlightBaggageId(flightBaggageId);
		paxBaggageTO.setPnrPaxFareId(pnrPaxFareId);
		paxBaggageTO.setBaggageName(baggageName);
		paxBaggageTO.setBaggageId(baggageId);
		paxBaggageTO.setPaxName(paxName);
		paxBaggageTO.setBaggageChrgId(baggageChrgId);
		paxBaggageTO.setBaggageOndGroupId(baggageOndGroupId);
		if (chargeAmount != null
				&& AccelAeroCalculator.isGreaterThan(chargeAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			paxBaggageTO.setAutoCancellationId(autoCnxId);
		}
		String uniqueId = BaggageUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);
		paxBaggageTOAdd.put(uniqueId, paxBaggageTO);

		putBaggage(flightBaggageId, AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED);

		if (adjAssembler != null) {
			adjAssembler.addCharge(pnrPaxId, chargeAmount);
		}
	}

	/**
	 * @return the paxBaggageInfo
	 */
	public Map<String, PaxBaggageTO> getPaxBaggageInfo() {
		return paxBaggageInfo;
	}

}
