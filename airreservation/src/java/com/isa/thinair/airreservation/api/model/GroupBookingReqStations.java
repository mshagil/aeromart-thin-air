package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

public class GroupBookingReqStations extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Long groupBookingReqID;

	private String stattionCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGroupBookingReqID() {
		return groupBookingReqID;
	}

	public void setGroupBookingReqID(Long groupBookingReqID) {
		this.groupBookingReqID = groupBookingReqID;
	}

	public String getStattionCode() {
		return stattionCode;
	}

	public void setStattionCode(String stattionCode) {
		this.stattionCode = stattionCode;
	}

}
