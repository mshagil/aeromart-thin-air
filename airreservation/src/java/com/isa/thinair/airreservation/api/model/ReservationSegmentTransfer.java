/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * ReservationSegmentTransfer is the entity class to represent a Reservation Segment Transfer for code share model
 * 
 * @author Nilindra Fernando
 * @since 2.0
 * @hibernate.class table = "T_PNR_SEGMENT_TRANSFER"
 */
public class ReservationSegmentTransfer extends Persistent {

	private static final long serialVersionUID = -5912589257478761204L;

	private int pnrSegTrnsId;

	private int pnrSegId;

	private String transferPnr;

	private String carrierCode;

	private String processDesc;

	private Date executionTimeStamp;

	private String status;

	/**
	 * Create the ReservationSegmentTransfer
	 */
	public ReservationSegmentTransfer() {

	}

	/**
	 * Create the ReservationSegmentTransfer
	 * 
	 * @param pnrSegId
	 * @param carrierCode
	 * @param executionTimeStamp
	 * @param status
	 */
	public ReservationSegmentTransfer(int pnrSegId, String carrierCode, Date executionTimeStamp, String status) {
		this();
		setPnrSegId(pnrSegId);
		setCarrierCode(carrierCode);
		setExecutionTimeStamp(executionTimeStamp);
		setStatus(status);
	}

	/**
	 * @return Returns the pnrSegTrnsId.
	 * @hibernate.id column = "PNR_SEG_TRNS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_SEGMENT_TRANSFER"
	 */
	public int getPnrSegTrnsId() {
		return pnrSegTrnsId;
	}

	/**
	 * @param pnrSegTrnsId
	 *            The pnrSegTrnsId to set.
	 */
	public void setPnrSegTrnsId(int pnrSegTrnsId) {
		this.pnrSegTrnsId = pnrSegTrnsId;
	}

	/**
	 * @return Returns the carrierCode.
	 * @hibernate.property column = "CARRIER_CODE"
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return Returns the pnrSegId.
	 * @hibernate.property column = "PNR_SEG_ID"
	 */
	public int getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            The pnrSegId to set.
	 */
	public void setPnrSegId(int pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return Returns the processDesc.
	 * @hibernate.property column = "PROCESS_DESCRIPTION"
	 */
	public String getProcessDesc() {
		return processDesc;
	}

	/**
	 * @param processDesc
	 *            The processDesc to set.
	 */
	public void setProcessDesc(String processDesc) {
		this.processDesc = processDesc;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the transferPnr.
	 * @hibernate.property column = "TRANSFER_PNR"
	 */
	public String getTransferPnr() {
		return transferPnr;
	}

	/**
	 * @param transferPnr
	 *            The transferPnr to set.
	 */
	public void setTransferPnr(String transferPnr) {
		this.transferPnr = transferPnr;
	}

	/**
	 * @return Returns the executionTimeStamp.
	 * @hibernate.property column = "EXECUTED_DATE"
	 */
	public Date getExecutionTimeStamp() {
		return executionTimeStamp;
	}

	/**
	 * @param executionTimeStamp
	 *            The executionTimeStamp to set.
	 */
	public void setExecutionTimeStamp(Date executionTimeStamp) {
		this.executionTimeStamp = executionTimeStamp;
	}
}
