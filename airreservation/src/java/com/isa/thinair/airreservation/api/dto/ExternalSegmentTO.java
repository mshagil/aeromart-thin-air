/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReturnTypes;

/**
 * ExternalSegmentTO is the data transfer class to call it's entity classes
 * 
 * @author Nilindra Fernando
 * @since 8/25/2008
 */
public class ExternalSegmentTO implements Serializable, Comparable<ExternalSegmentTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6649102461225929117L;

	/** Holds flight number */
	private String flightNo;

	/** Holds departure date */
	private Date departureDate;

	/** Holds departure date Zulu */
	private Date departureDateZulu;

	/** Holds arrival date */
	private Date arrivalDate;

	/** Holds arrival date Zulu */
	private Date arrivalDateZulu;

	/** Holds segment code */
	private String segmentCode;

	/** Holds the flightRefNumber */
	private String flightRefNumber;

	/** Holds the externalFlightSegId */
	private Integer externalFlightSegId;

	/** Holds fltSeg status */
	private String fltSegStatus;

	/** Holds the cabin class code */
	private String cabinClassCode;

	/** Holds the cabin class code */
	private String logicalCabinClassCode;

	/** Holds the operatingAirline */
	private String operatingAirline;

	/** Holds the returnFlag */
	private String returnFlag;

	/** Holds the externalPnrSegId */
	private Integer externalPnrSegId;

	/** Holds pnrFltSeg status */
	private String pnrFltSegStatus;

	/** The marketing agent code */
	private String marketingAgentCode;

	/** The marketing station code */
	private String marketingStationCode;

	/** The marketing carrier code */
	private String marketingCarrierCode;

	/** The status modified date */
	private Date statusModifiedDate;

	/** The status modified channel code */
	private Integer statusModifiedChannelCode;

	private Integer segmentSequence;
	
	private String subStatus;

	public ExternalSegmentTO() {

	}

	public ExternalSegmentTO(ExternalPnrSegment reservationExternalSegment) {
		this();

		ExternalFlightSegment externalFlightSegment = reservationExternalSegment.getExternalFlightSegment();
		setFlightNo(externalFlightSegment.getFlightNumber());
		setDepartureDate(externalFlightSegment.getEstTimeDepatureLocal());
		setDepartureDateZulu(externalFlightSegment.getEstTimeDepatureZulu());
		setArrivalDate(externalFlightSegment.getEstTimeArrivalLocal());
		setArrivalDateZulu(externalFlightSegment.getEstTimeArrivalZulu());
		setSegmentCode(externalFlightSegment.getSegmentCode());
		setFltSegStatus(externalFlightSegment.getStatus());

		setCabinClassCode(reservationExternalSegment.getCabinClassCode());
		setLogicalCabinClassCode(reservationExternalSegment.getLogicalCabinClassCode());
		setOperatingAirline(reservationExternalSegment.getExternalCarrierCode());
		setStatus(reservationExternalSegment.getStatus());
		setReturnFlag(reservationExternalSegment.getReturnFlag());
		setSegmentSequence(reservationExternalSegment.getSegmentSeq());
		setSubStatus(reservationExternalSegment.getSubStatus());

		setExternalFlightSegId(reservationExternalSegment.getExternalFltSegId());
	}

	/**
	 * @return Returns the arrivalDate.
	 */
	public Date getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * @param arrivalDate
	 *            The arrivalDate to set.
	 */
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	/**
	 * @param logicalCabinClassCode
	 */
	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return Returns the departureDate.
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            The departureDate to set.
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the flightNo.
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            The flightNo to set.
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return Returns the segmentCode.
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return Returns the pnrFltSegStatus.
	 */
	public String getStatus() {
		return pnrFltSegStatus;
	}

	/**
	 * @param pnrFltSegStatus
	 *            The status to set.
	 */
	public void setStatus(String pnrFltSegStatus) {
		this.pnrFltSegStatus = pnrFltSegStatus;
	}

	public String getFltSegStatus() {
		return fltSegStatus;
	}

	public void setFltSegStatus(String fltSegStatus) {
		this.fltSegStatus = fltSegStatus;
	}

	/**
	 * @return the flightRefNumber
	 */
	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	/**
	 * @param flightRefNumber
	 *            the flightRefNumber to set
	 */
	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	/**
	 * @return the externalFlightSegId
	 */
	public Integer getExternalFlightSegId() {
		return externalFlightSegId;
	}

	/**
	 * @param externalFlightSegId
	 *            the externalFlightSegId to set
	 */
	public void setExternalFlightSegId(Integer externalFlightSegId) {
		this.externalFlightSegId = externalFlightSegId;
	}

	/**
	 * @return the departureDateZulu
	 */
	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	/**
	 * @param departureDateZulu
	 *            the departureDateZulu to set
	 */
	public void setDepartureDateZulu(Date departureDateZulu) {
		this.departureDateZulu = departureDateZulu;
	}

	/**
	 * @return the arrivalDateZulu
	 */
	public Date getArrivalDateZulu() {
		return arrivalDateZulu;
	}

	/**
	 * @param arrivalDateZulu
	 *            the arrivalDateZulu to set
	 */
	public void setArrivalDateZulu(Date arrivalDateZulu) {
		this.arrivalDateZulu = arrivalDateZulu;
	}

	/**
	 * @return the externalPnrSegId
	 */
	public Integer getExternalPnrSegId() {
		return externalPnrSegId;
	}

	/**
	 * @param externalPnrSegId
	 *            the externalPnrSegId to set
	 */
	public void setExternalPnrSegId(Integer externalPnrSegId) {
		this.externalPnrSegId = externalPnrSegId;
	}

	/**
	 * @return the pnrFltSegStatus
	 */
	public String getPnrFltSegStatus() {
		return pnrFltSegStatus;
	}

	/**
	 * @param pnrFltSegStatus
	 *            the pnrFltSegStatus to set
	 */
	public void setPnrFltSegStatus(String pnrFltSegStatus) {
		this.pnrFltSegStatus = pnrFltSegStatus;
	}

	/**
	 * @return : The marketing agent code.
	 */
	public String getMarketingAgentCode() {
		return marketingAgentCode;
	}

	/**
	 * @param marketingAgentCode
	 *            : The marketing agent code.
	 */
	public void setMarketingAgentCode(String marketingAgentCode) {
		this.marketingAgentCode = marketingAgentCode;
	}

	/**
	 * @return : The marketing station code.
	 */
	public String getMarketingStationCode() {
		return marketingStationCode;
	}

	/**
	 * @param marketingStationCode
	 *            : The marketing station code to set.
	 */
	public void setMarketingStationCode(String marketingStationCode) {
		this.marketingStationCode = marketingStationCode;
	}

	/**
	 * @return : The marketing carrier code.
	 */
	public String getMarketingCarrierCode() {
		return marketingCarrierCode;
	}

	/**
	 * @param marketingCarrierCode
	 *            : The marketing carrier code to set.
	 */
	public void setMarketingCarrierCode(String marketingCarrierCode) {
		this.marketingCarrierCode = marketingCarrierCode;
	}

	/**
	 * @return The status modifed date.
	 */
	public Date getStatusModifiedDate() {
		return statusModifiedDate;
	}

	/**
	 * @param statusModifiedDate
	 *            The status modified date to set.
	 */
	public void setStatusModifiedDate(Date statusModifiedDate) {
		this.statusModifiedDate = statusModifiedDate;
	}

	/**
	 * @return The status modified channel code.
	 */
	public Integer getStatusModifiedChannelCode() {
		return statusModifiedChannelCode;
	}

	/**
	 * @param statusModifiedChannelCode
	 *            The status modifed channel code to set.
	 */
	public void setStatusModifiedChannelCode(Integer statusModifiedChannelCode) {
		this.statusModifiedChannelCode = statusModifiedChannelCode;
	}

	public Integer getSegmentSequence() {
		return segmentSequence;
	}

	public void setSegmentSequence(Integer segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

	/**
	 * Returns the to string representation of this object
	 */
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append(" Flight Number: " + this.getFlightNo());
		stringBuilder.append(" Segment Code: " + this.getSegmentCode());
		stringBuilder.append(" Cabin Class: " + this.getCabinClassCode());
		stringBuilder.append(" Logical cabin class: " + this.getLogicalCabinClassCode());
		stringBuilder.append(" Departure Date: " + BeanUtils.parseDateFormat(this.getDepartureDate(), "E, dd MMM yyyy HH:mm:ss"));
		stringBuilder.append(" Arrival Date: " + BeanUtils.parseDateFormat(this.getArrivalDate(), "E, dd MMM yyyy HH:mm:ss"));
		stringBuilder.append(" Flight Ref No: " + this.getFlightRefNumber());
		stringBuilder.append(" Carrier code: " + this.getOperatingAirline());
		stringBuilder.append(" ExternalFlt Seg Id: " + this.getExternalFlightSegId());
		stringBuilder.append(" Pnr flt seg Status : " + this.getStatus());
		stringBuilder.append(" Flt seg Status : " + this.getStatus());
		stringBuilder.append(" Segment Sequence : " + this.getSegmentSequence());

		return stringBuilder.toString();
	}

	/**
	 * Transforms ExternalSegmentTO object to ExternalPnrSegment
	 * 
	 * @return
	 */
	public ExternalPnrSegment toExternalPnrSegment() {
		ExternalPnrSegment externalPnrSegment = new ExternalPnrSegment();

		ExternalFlightSegment externalFlightSegment = new ExternalFlightSegment();
		externalFlightSegment.setFlightNumber(this.getFlightNo());
		externalFlightSegment.setEstTimeDepatureLocal(this.getDepartureDate());
		externalFlightSegment.setEstTimeDepatureZulu(this.getDepartureDateZulu());
		externalFlightSegment.setEstTimeArrivalLocal(this.getArrivalDate());
		externalFlightSegment.setEstTimeArrivalZulu(this.getArrivalDateZulu());
		externalFlightSegment.setSegmentCode(this.getSegmentCode());
		externalFlightSegment.setStatus(this.getFltSegStatus());
		externalPnrSegment.setExternalFlightSegment(externalFlightSegment);

		externalPnrSegment.setStatus(this.getStatus());
		externalPnrSegment.setCabinClassCode(this.getCabinClassCode());
		externalPnrSegment.setLogicalCabinClassCode(this.getLogicalCabinClassCode());
		externalPnrSegment.setSegmentSeq(this.getSegmentSequence());
		externalPnrSegment.setExternalCarrierCode(this.getOperatingAirline());
		if (this.getReturnFlag() != null) {
			externalPnrSegment.setReturnFlag(this.getReturnFlag());
		} else {
			externalPnrSegment.setReturnFlag(ReturnTypes.RETURN_FLAG_FALSE);
		}
		externalPnrSegment.setExternalFltSegId(this.getExternalFlightSegId());
		externalPnrSegment.setMarketingAgentCode(this.getMarketingAgentCode());
		externalPnrSegment.setMarketingCarrierCode(this.getMarketingCarrierCode());
		externalPnrSegment.setMarketingStationCode(this.getMarketingStationCode());
		externalPnrSegment.setStatusModifiedDate(this.getStatusModifiedDate());
		externalPnrSegment.setStatusModifiedSalesChannelCode(this.getStatusModifiedChannelCode());

		return externalPnrSegment;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	
	public int compareTo(ExternalSegmentTO o) {
		return this.getSegmentSequence().compareTo(o.getSegmentSequence());
	}
}
