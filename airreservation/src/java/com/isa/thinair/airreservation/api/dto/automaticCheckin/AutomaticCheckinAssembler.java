package com.isa.thinair.airreservation.api.dto.automaticCheckin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AutomaticCheckinAssembler implements Serializable, IPassengerAutomaticCheckin {

	private static final long serialVersionUID = 6910743505682178781L;

	private Map<String, Collection<PaxAutomaticCheckinTO>> paxAutoCheckinInfo = new HashMap<String, Collection<PaxAutomaticCheckinTO>>();
	private Map<String, Collection<PaxAutomaticCheckinTO>> paxAutomaticChecinTORemove = new HashMap<String, Collection<PaxAutomaticCheckinTO>>();

	private Map<Integer, Integer> flightAutoCheckinIdPnrSegId = new HashMap<Integer, Integer>();

	private IPassenger passenger;

	private final boolean isForceConfirmed;

	private PaxAdjAssembler adjAssembler;

	public AutomaticCheckinAssembler(IPassenger passenger, boolean isForceConfirmed) {
		this(passenger, null, isForceConfirmed);
	}

	public AutomaticCheckinAssembler(IPassenger passenger, PaxAdjAssembler adjAssembler, boolean isForceConfirmed) {
		this.isForceConfirmed = isForceConfirmed;
		this.adjAssembler = adjAssembler;
		this.passenger = passenger;
	}

	/**
	 * This is used to add autocheckin details for all pax
	 */
	@Override
	public void addPaxSegmentAutoCheckin(Integer pnrPaxId, Integer flightSeatId, Integer pnrSegId, Integer pnrPaxFareId,
			BigDecimal chargeAmount, Integer autoCheckinId, String seatPref, String email, TrackInfoDTO trackInfo)
			throws ModuleException {

		PaxAutomaticCheckinTO paxAutomaticCheckinTO = new PaxAutomaticCheckinTO();
		paxAutomaticCheckinTO.setAmount(chargeAmount);
		paxAutomaticCheckinTO.setPnrPaxId(pnrPaxId);
		paxAutomaticCheckinTO.setPnrSegId(pnrSegId);
		paxAutomaticCheckinTO.setPpfId(pnrPaxFareId);
		paxAutomaticCheckinTO.setAutoCheckinTemplateId(autoCheckinId);
		paxAutomaticCheckinTO.setSeatTypePreference(seatPref);
		paxAutomaticCheckinTO.setFlightAmSeatId(flightSeatId);
		paxAutomaticCheckinTO.setEmail(email);
		paxAutomaticCheckinTO.setChgDTO(getExternalChgDTOForAutoCkn(chargeAmount, trackInfo));
		String uniqueId = AutoCheckinUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);
		if (paxAutoCheckinInfo.get(uniqueId) == null)
			paxAutoCheckinInfo.put(uniqueId, new ArrayList<PaxAutomaticCheckinTO>());
		paxAutoCheckinInfo.get(uniqueId).add(paxAutomaticCheckinTO);

		flightAutoCheckinIdPnrSegId.put(pnrPaxFareId, pnrSegId);

		if (adjAssembler != null) {
			adjAssembler.addCharge(pnrPaxId, chargeAmount);
		}
	}

	/**
	 * This is used to remove autocheckin details for all pax
	 */
	public void removePaxSegmentAutoCheckin(Integer pnrPaxId, Integer flightSeatId, Integer pnrSegId, Integer pnrPaxFareId,
			BigDecimal chargeAmount, Integer autoCheckinId, String seatTypePref, String email, TrackInfoDTO trackInfo)
			throws ModuleException {

		PaxAutomaticCheckinTO paxAutomaticCheckinTO = new PaxAutomaticCheckinTO();
		paxAutomaticCheckinTO.setPnrPaxId(pnrPaxId);
		paxAutomaticCheckinTO.setPnrSegId(pnrSegId);
		paxAutomaticCheckinTO.setSeatTypePreference(seatTypePref);
		paxAutomaticCheckinTO.setFlightAmSeatId(flightSeatId);
		paxAutomaticCheckinTO.setEmail(email);
		paxAutomaticCheckinTO.setChgDTO(getExternalChgDTOForAutoCkn(chargeAmount.negate(), trackInfo));
		String uniqueId = AutoCheckinUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);
		if (paxAutomaticChecinTORemove.get(uniqueId) == null)
			paxAutomaticChecinTORemove.put(uniqueId, new ArrayList<PaxAutomaticCheckinTO>());
		paxAutomaticChecinTORemove.get(uniqueId).add(paxAutomaticCheckinTO);

		if (adjAssembler != null) {
			adjAssembler.removeCharge(pnrPaxId, chargeAmount);
		}
	}

	/**
	 * This is to get exxternal charge for auto checkin
	 * 
	 * @param charge
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	private ExternalChgDTO getExternalChgDTOForAutoCkn(BigDecimal charge, TrackInfoDTO trackInfo) throws ModuleException {
		if (charge == null) {
			return null;
		}

		Collection<ReservationInternalConstants.EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
		Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationModuleUtils
				.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, trackInfo, null);
		ExternalChgDTO externalChgDTO = (ExternalChgDTO) ((ExternalChgDTO) mapExternalChgs
				.get(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN)).clone();
		externalChgDTO.setAmount(charge);
		return externalChgDTO;
	}

	/**
	 * @return the paxAutoCheckinInfo
	 */
	public Map<String, Collection<PaxAutomaticCheckinTO>> getPaxAutoCheckinInfo() {
		return paxAutoCheckinInfo;
	}

	/**
	 * @param paxAutoCheckinInfo
	 *            the paxAutoCheckinInfo to set
	 */
	public void setPaxAutoCheckinInfo(Map<String, Collection<PaxAutomaticCheckinTO>> paxAutoCheckinInfo) {
		this.paxAutoCheckinInfo = paxAutoCheckinInfo;
	}

	/**
	 * @return the paxAutomaticChecinTORemove
	 */
	public Map<String, Collection<PaxAutomaticCheckinTO>> getPaxAutomaticChecinTORemove() {
		return paxAutomaticChecinTORemove;
	}

	/**
	 * @param paxAutomaticChecinTORemove
	 *            the paxAutomaticChecinTORemove to set
	 */
	public void setPaxAutomaticChecinTORemove(Map<String, Collection<PaxAutomaticCheckinTO>> paxAutomaticChecinTORemove) {
		this.paxAutomaticChecinTORemove = paxAutomaticChecinTORemove;
	}

	/**
	 * @return the flightAutoCheckinIdPnrSegId
	 */
	public Map<Integer, Integer> getFlightAutoCheckinIdPnrSegId() {
		return flightAutoCheckinIdPnrSegId;
	}

	/**
	 * @param flightAutoCheckinIdPnrSegId
	 *            the flightAutoCheckinIdPnrSegId to set
	 */
	public void setFlightAutoCheckinIdPnrSegId(Map<Integer, Integer> flightAutoCheckinIdPnrSegId) {
		this.flightAutoCheckinIdPnrSegId = flightAutoCheckinIdPnrSegId;
	}

	/**
	 * @return the adjAssembler
	 */
	public PaxAdjAssembler getAdjAssembler() {
		return adjAssembler;
	}

	/**
	 * @param adjAssembler
	 *            the adjAssembler to set
	 */
	public void setAdjAssembler(PaxAdjAssembler adjAssembler) {
		this.adjAssembler = adjAssembler;
	}

	/**
	 * @return the isForceConfirmed
	 */
	public boolean isForceConfirmed() {
		return isForceConfirmed;
	}

	/**
	 * @return the passenger
	 */
	public IPassenger getPassenger() {
		return passenger;
	}

	/**
	 * @param passenger the passenger to set
	 */
	public void setPassenger(IPassenger passenger) {
		this.passenger = passenger;
	}

}
