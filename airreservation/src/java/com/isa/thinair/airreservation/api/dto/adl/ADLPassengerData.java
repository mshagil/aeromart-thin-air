package com.isa.thinair.airreservation.api.dto.adl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ADLPassengerData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<PassengerInformation> addedPassengers;
	List<PassengerInformation> deletedPassengers;
	List<PassengerInformation> changedPassengers;

	public void addAddedPassengers(PassengerInformation passengerInfo) {
		if (addedPassengers == null) {
			addedPassengers = new ArrayList<PassengerInformation>();
		}
		addedPassengers.add(passengerInfo);
	}

	public void addDeletedPassengers(PassengerInformation passengerInfo) {
		if (deletedPassengers == null) {
			deletedPassengers = new ArrayList<PassengerInformation>();
		}
		deletedPassengers.add(passengerInfo);
	}

	public void addChangedPassengers(PassengerInformation passengerInfo) {
		if (changedPassengers == null) {
			changedPassengers = new ArrayList<PassengerInformation>();
		}
		changedPassengers.add(passengerInfo);
	}

	public List<PassengerInformation> getAddedPassengers() {
		if (addedPassengers == null) {
			addedPassengers = new ArrayList<PassengerInformation>();
		}
		return addedPassengers;
	}

	public void setAddedPassengers(List<PassengerInformation> addedPassengers) {
		this.addedPassengers = addedPassengers;
	}

	public List<PassengerInformation> getDeletedPassengers() {
		if (deletedPassengers == null) {
			deletedPassengers = new ArrayList<PassengerInformation>();
		}
		return deletedPassengers;
	}

	public void setDeletedPassengers(List<PassengerInformation> deletedPassengers) {
		this.deletedPassengers = deletedPassengers;
	}

	public List<PassengerInformation> getChangedPassengers() {
		if (changedPassengers == null) {
			changedPassengers = new ArrayList<PassengerInformation>();
		}
		return changedPassengers;
	}

	public void setChangedPassengers(List<PassengerInformation> changedPassengers) {
		this.changedPassengers = changedPassengers;
	}

}
