package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * To hold Loyalty credit data transfer information
 * 
 * @author harshaa
 */
public class LoyaltyCreditDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String customerID;

	private String loyaltyAccountNo;

	private Date dateOfExpiry;

	private Date dateOfEarn;

	private BigDecimal credit;

	/**
	 * @return Returns the customerID
	 */
	public String getCustomerID() {
		return customerID;
	}

	/**
	 * @param customerID
	 */
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	/**
	 * @return Returns the loyaltyAccountNo
	 */
	public String getLoyaltyAccountNo() {
		return loyaltyAccountNo;
	}

	/**
	 * @param loyaltyAccountNo
	 */
	public void setLoyaltyAccountNo(String loyaltyAccountNo) {
		this.loyaltyAccountNo = loyaltyAccountNo;
	}

	/**
	 * @return Returns the dateOfExpiry
	 */
	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	/**
	 * @param dateOfExpiry
	 */
	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	/**
	 * @return Returns the dateOfEarn
	 */
	public Date getDateOfEarn() {
		return dateOfEarn;
	}

	/**
	 * @param dateOfEarn
	 */
	public void setDateOfEarn(Date dateOfEarn) {
		this.dateOfEarn = dateOfEarn;
	}

	/**
	 * @return Returns the credit
	 */
	public BigDecimal getCredit() {
		return credit;
	}

	/**
	 * @param credit
	 */
	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

}
