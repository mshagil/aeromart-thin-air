/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.adl;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.RemarksElementDTO;

/*
 * author Isuru Description-PNL Record Bean
 */
@SuppressWarnings("rawtypes")
public class ADLRecordDTO implements Comparable {
	private Integer pnrSegId;

	private int numberPAD;

	private ArrayList<NameDTO> names;

	private String tourID;

	private int index;

	private int totalSeats;

	private String lastname;

	private String automatedPNR;

	private String airlineCode;

	private boolean duplicatedPNRaddress;

	private String osicode;

	private String infantString;

	private int key;

	private boolean invisible;

	private ArrayList<OnWardConnectionDTO> onwardconnectionlist;

	private String remarkStart = "";

	private String ssrCode = "";

	private String remarksText = "";

	private String inboundFlightNumber = "";

	private String inboundFareClass = "";

	private String inboundDate = "";

	private String inboundDepartureStation = "";

	private String waitingListStart = "";

	private String standByIDpax = "";

	private String inboundStart = "";

	private boolean hasInbound = false;

	private boolean hasRemarks = false;

	private boolean hasWaitingList = false;

	private boolean hasSeatSelection = false;

	private boolean hasIDpax = false;

	private String seatRequestCode = "";

	private String seatNumbers = "";

	private boolean hasCCDetails = false;
	private String CCDetailsRequestCode = "";
	private String CCDetailsStatusCode = "";
	private String CCDetails = "";

	private Integer pnrPaxId;

	private String mealRequestCode;

	private String mealName;

	private boolean hasMealSelection = false;

	private Collection<RemarksElementDTO> requests;
	
	private boolean multipleSeatsSelected;

	/**
	 * @return the hasMealSelection
	 */
	public boolean isHasMealSelection() {
		return hasMealSelection;
	}

	/**
	 * @param hasMealSelection
	 *            the hasMealSelection to set
	 */
	public void setHasMealSelection(boolean hasMealSelection) {
		this.hasMealSelection = hasMealSelection;
	}

	/**
	 * @return the mealRequestCode
	 */
	public String getMealRequestCode() {
		return mealRequestCode;
	}

	/**
	 * @param mealRequestCode
	 *            the mealRequestCode to set
	 */
	public void setMealRequestCode(String mealRequestCode) {
		this.mealRequestCode = mealRequestCode;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getAutomatedPNR() {
		return automatedPNR;
	}

	public void setAutomatedPNR(String automatedPNR) {
		this.automatedPNR = automatedPNR;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public int getNumberPAD() {
		return numberPAD;
	}

	public void setNumberPAD(int numberPAD) {
		this.numberPAD = numberPAD;
	}

	public String getOsicode() {
		return osicode;
	}

	public void setOsicode(String osicode) {
		this.osicode = osicode;
	}

	public int getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}

	public String getTourID() {
		return tourID;
	}

	public void setTourID(String tourID) {
		this.tourID = tourID;
	}

	public boolean isDuplicatedPNRaddress() {
		return duplicatedPNRaddress;
	}

	public void setDuplicatedPNRaddress(boolean duplicatedPNRaddress) {
		this.duplicatedPNRaddress = duplicatedPNRaddress;
	}

	public ArrayList<OnWardConnectionDTO> getOnwardconnectionlist() {
		return onwardconnectionlist;
	}

	public void setOnwardconnectionlist(ArrayList<OnWardConnectionDTO> onwardconnectionlist) {
		this.onwardconnectionlist = onwardconnectionlist;
	}

	public ArrayList<NameDTO> getNames() {
		return names;
	}

	public void setNames(ArrayList<NameDTO> names) {
		this.names = names;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public boolean isInvisible() {
		return invisible;
	}

	public void setInvisible(boolean invisible) {
		this.invisible = invisible;
	}

	public int compareTo(Object arg0) {
		return this.lastname.compareTo(((ADLRecordDTO) arg0).lastname);
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean isHasInbound() {
		return hasInbound;
	}

	public void setHasInbound(boolean hasInbound) {
		this.hasInbound = hasInbound;
	}

	public boolean isHasRemarks() {
		return hasRemarks;
	}

	public void setHasRemarks(boolean hasRemarks) {
		this.hasRemarks = hasRemarks;
	}

	public String getInboundDate() {
		return inboundDate;
	}

	public void setInboundDate(String inboundDate) {
		this.inboundDate = inboundDate;
	}

	public String getInboundDepartureStation() {
		return inboundDepartureStation;
	}

	public void setInboundDepartureStation(String inboundDepartureStation) {
		this.inboundDepartureStation = inboundDepartureStation;
	}

	public String getInboundFareClass() {
		return inboundFareClass;
	}

	public void setInboundFareClass(String inboundFareClass) {
		this.inboundFareClass = inboundFareClass;
	}

	public String getInboundFlightNumber() {
		return inboundFlightNumber;
	}

	public void setInboundFlightNumber(String inboundFlightNumber) {
		this.inboundFlightNumber = inboundFlightNumber;
	}

	public String getInboundStart() {
		return inboundStart;
	}

	public void setInboundStart(String inboundStart) {
		this.inboundStart = inboundStart;
	}

	public String getRemarkStart() {
		return remarkStart;
	}

	public void setRemarkStart(String remarkStart) {
		this.remarkStart = remarkStart;
	}

	public String getRemarksText() {
		return remarksText;
	}

	public void setRemarksText(String remarksText) {
		this.remarksText = remarksText;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getInfantString() {
		return infantString;
	}

	public void setInfantString(String infantString) {
		this.infantString = infantString;
	}

	/**
	 * @return the hasWaitingList
	 */
	public boolean isHasWaitingList() {
		return hasWaitingList;
	}

	/**
	 * @param hasWaitingList
	 *            the hasWaitingList to set
	 */
	public void setHasWaitingList(boolean hasWaitingList) {
		this.hasWaitingList = hasWaitingList;
	}

	/**
	 * @return the waitingListStart
	 */
	public String getWaitingListStart() {
		return waitingListStart;
	}

	/**
	 * @param waitingListStart
	 *            the waitingListStart to set
	 */
	public void setWaitingListStart(String waitingListStart) {
		this.waitingListStart = waitingListStart;
	}

	/**
	 * @return the pnrSegId
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public boolean isHasSeatSelection() {
		return hasSeatSelection;
	}

	public void setHasSeatSelection(boolean hasSeatSelection) {
		this.hasSeatSelection = hasSeatSelection;
	}

	public String getSeatRequestCode() {
		return seatRequestCode;
	}

	public void setSeatRequestCode(String seatRequestCode) {
		this.seatRequestCode = seatRequestCode;
	}

	public String getSeatNumbers() {
		return seatNumbers;
	}

	public void setSeatNumbers(String seatNumbers) {
		this.seatNumbers = seatNumbers;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the mealName
	 */
	public String getMealName() {
		return mealName;
	}

	/**
	 * @param mealName
	 *            the mealName to set
	 */
	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	/**
	 * @return the hasCCDetails
	 */
	public boolean isHasCCDetails() {
		return hasCCDetails;
	}

	/**
	 * @param hasCCDetails
	 *            the hasCCDetails to set
	 */
	public void setHasCCDetails(boolean hasCCDetails) {
		this.hasCCDetails = hasCCDetails;
	}

	/**
	 * @return the cCDetailsRequestCode
	 */
	public String getCCDetailsRequestCode() {
		return CCDetailsRequestCode;
	}

	/**
	 * @param detailsRequestCode
	 *            the cCDetailsRequestCode to set
	 */
	public void setCCDetailsRequestCode(String detailsRequestCode) {
		CCDetailsRequestCode = detailsRequestCode;
	}

	/**
	 * @return the cCDetailsStatusCode
	 */
	public String getCCDetailsStatusCode() {
		return CCDetailsStatusCode;
	}

	/**
	 * @param detailsStatusCode
	 *            the cCDetailsStatusCode to set
	 */
	public void setCCDetailsStatusCode(String detailsStatusCode) {
		CCDetailsStatusCode = detailsStatusCode;
	}

	/**
	 * @return the cCDetails
	 */
	public String getCCDetails() {
		return CCDetails;
	}

	/**
	 * @param details
	 *            the cCDetails to set
	 */
	public void setCCDetails(String details) {
		CCDetails = details;
	}

	/**
	 * @return the requests
	 */
	public Collection<RemarksElementDTO> getRequests() {
		return requests;
	}

	/**
	 * @param requests
	 *            the requests to set
	 */
	public void addRequest(RemarksElementDTO request) {
		if (this.requests == null) {
			this.requests = new ArrayList<RemarksElementDTO>();
		}
		this.requests.add(request);
	}

	/**
	 * @return the hasIDpax
	 */
	public boolean isHasIDpax() {
		return hasIDpax;
	}

	/**
	 * @param hasIDpax
	 *            the hasIDpax to set
	 */
	public void setHasIDpax(boolean hasIDpax) {
		this.hasIDpax = hasIDpax;
	}

	/**
	 * @return the stanbyIDPax
	 */
	public String getStandByIDpax() {
		return standByIDpax;
	}

	/**
	 * @param stanbyIDPax
	 *            the stanbyIDPax to set
	 */
	public void setStandByIDPax(String standbyIDpax) {
		this.standByIDpax = standbyIDpax;
	}

	public boolean isMultipleSeatsSelected() {
		return multipleSeatsSelected;
	}

	public void setMultipleSeatsSelected(boolean multipleSeatsSelected) {
		this.multipleSeatsSelected = multipleSeatsSelected;
	}
	
}
