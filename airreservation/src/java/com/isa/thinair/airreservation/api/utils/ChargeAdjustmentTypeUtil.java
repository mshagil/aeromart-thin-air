package com.isa.thinair.airreservation.api.utils;

import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * Utility methods for custom charge adjustment types
 * 
 * @author sanjaya
 */
public class ChargeAdjustmentTypeUtil {

	/**
	 * Gets the associated charge rete if for the given Charge Adjustment Type.
	 * 
	 * @param chargeAdjustmentTypeDTO
	 *            : The charge adjustment type object.
	 * @param isRefundable
	 *            : Refundable non refundable flag.
	 * @return : The matching charge rate id.
	 * @throws ModuleException
	 */
	public static Integer getChargeRateIdForAdjustment(ChargeAdjustmentTypeDTO chargeAdjustmentTypeDTO, Boolean isRefundable)
			throws ModuleException {

		if (chargeAdjustmentTypeDTO == null || isRefundable == null) {
			throw new ModuleException("airreservations.custom.adjustment.charge.type.id.not.found");
		}

		String chargeCode = null;
		if (isRefundable) {
			chargeCode = chargeAdjustmentTypeDTO.getRefundableChargeCode();
		} else {
			chargeCode = chargeAdjustmentTypeDTO.getNonRefundableChargeCode();
		}

		return getExternalChgDTO(chargeCode).getChargeRateId();
	}

	/**
	 * Gets the {@link QuotedChargeDTO} object for the provided charge code.
	 * 
	 * @param chargeCode
	 *            : The charge Code.
	 * @return
	 * @throws ModuleException
	 */
	private static QuotedChargeDTO getExternalChgDTO(String chargeCode) throws ModuleException {

		if (chargeCode == null) {
			throw new ModuleException("airreservations.custom.adjustment.charge.code.not.found");
		}

		BigDecimal fare = AccelAeroCalculator.getDefaultBigDecimalZero();
		FareSummaryDTO fareSummaryDTO = new FareSummaryDTO();
		fareSummaryDTO.setAdultFare(fare.doubleValue());
		fareSummaryDTO.setChildFare(fare.doubleValue());
		fareSummaryDTO.setInfantFare(fare.doubleValue());
		fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
		fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V);

		QuotedChargeDTO quotedChargeDTO = ReservationModuleUtils.getChargeBD().getCharge(chargeCode, null, fareSummaryDTO, null,
				false);

		// even default charge rate is also not defined , throws exception.
		if (quotedChargeDTO == null) {
			throw new ModuleException("airreservations.custom.adjustment.charge.code.not.found");
		}

		return quotedChargeDTO;
	}

	/**
	 * Gets the {@link ChargeAdjustmentTypeDTO} object for the provided charge adjustment type id.
	 * 
	 * @param chargeAdjTypeId
	 *            : The charge adjustment type id.
	 * @return : The {@link ChargeAdjustmentTypeDTO} object.
	 */
	public static ChargeAdjustmentTypeDTO getChargeAdjustmentTypeDTO(Integer chargeAdjTypeId) {
		ChargeAdjustmentTypeDTO chargeAdjustmentTypeDTO = null;

		List<ChargeAdjustmentTypeDTO> globalChargeAdjustmentTypes = CommonsServices.getGlobalConfig().getChargeAdjustmentTypes();

		for (ChargeAdjustmentTypeDTO chargeAdjustment : globalChargeAdjustmentTypes) {
			if (chargeAdjustment.getChargeAdjustmentTypeId().equals(chargeAdjTypeId)) {
				chargeAdjustmentTypeDTO = chargeAdjustment;
				break;
			}
		}
		return chargeAdjustmentTypeDTO;
	}

	/**
	 * Gets the charge adjustment type used when doing automated adjustment after refunding TAX.
	 * 
	 * @return ChargeAdjustmentTypeDTO used to mark adjustments for tax refunds.
	 */
	/*
	 * If this method is returning null that means the specific charge adjustment is not created in the database or not
	 * creted in the config file. It's a configuration problem.
	 */
	public static ChargeAdjustmentTypeDTO getChargeAdjustTypeForTaxRefund() {
		ChargeAdjustmentTypeDTO taxRefundAdjustment = null;
		List<ChargeAdjustmentTypeDTO> globalChargeAdjustmentTypes = CommonsServices.getGlobalConfig().getChargeAdjustmentTypes();

		// Refundable and non refundable are the same for this type because it serves no purpose to have two.
		String refundableChargeAdjustmentTAXCode = ReservationModuleUtils.getAirReservationConfig().getChargeAdjustmentMap()
				.get(ReservationInternalConstants.CHARGE_ADJUSTMENTS.AUTOMATED_REFUND_TAX.toString());

		for (ChargeAdjustmentTypeDTO chargeAdjustment : globalChargeAdjustmentTypes) {
			if (chargeAdjustment.getRefundableChargeCode().equalsIgnoreCase(refundableChargeAdjustmentTAXCode)) {
				taxRefundAdjustment = chargeAdjustment;
				break;
			}
		}

		return taxRefundAdjustment;
	}
}
