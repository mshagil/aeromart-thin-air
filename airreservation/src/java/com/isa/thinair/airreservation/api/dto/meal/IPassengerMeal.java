package com.isa.thinair.airreservation.api.dto.meal;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.AncillaryAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface IPassengerMeal extends AncillaryAssembler {

	public void addMealInfo(Integer pnrPaxId, Integer pnrPaxFareId, Integer flightMealId, Integer pnrSegId,
			BigDecimal chargeAmount, Integer mealId, Integer allocatedQty, Integer autoCnxId,
			TrackInfoDTO trackInfo) throws ModuleException;
}
