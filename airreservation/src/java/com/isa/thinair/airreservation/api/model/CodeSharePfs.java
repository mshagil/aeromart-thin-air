/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of sent PFS information with code share carriers
 * 
 * @author M.Rikaz
 * @hibernate.class table = "T_CS_PFS"
 */
public class CodeSharePfs extends Persistent {

	private static final long serialVersionUID = -7185867895680222309L;

	private int csPfsId;

	private Date processedDate;

	private String toAddress;

	private String fromAirport;

	private String status;

	private String csFlightDesignator;

	private String csCarrierCode;

	private Date departureDate;

	private String pfsContent;

	private Integer refMessageID;

	/**
	 * @return Returns the ppId.
	 * @hibernate.id column = "CS_PFS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_CS_PFS"
	 */
	public int getCsPfsId() {
		return csPfsId;
	}

	public void setCsPfsId(int csPfsId) {
		this.csPfsId = csPfsId;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "PROCESSED_DATE"
	 */
	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "TO_ADDRESS"
	 */
	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "FROM_AIRPORT"
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "CS_FLIGHT_NUMBER"
	 */
	public String getCsFlightDesignator() {
		return csFlightDesignator;
	}

	public void setCsFlightDesignator(String csFlightDesignator) {
		this.csFlightDesignator = csFlightDesignator;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "CS_CARRIER_CODE"
	 */
	public String getCsCarrierCode() {
		return csCarrierCode;
	}

	public void setCsCarrierCode(String csCarrierCode) {
		this.csCarrierCode = csCarrierCode;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "DEPARTURE_DATE"
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "PFS_CONTENT"
	 */
	public String getPfsContent() {
		return pfsContent;
	}

	public void setPfsContent(String pfsContent) {
		this.pfsContent = pfsContent;
	}

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "REF_MESSAGE_ID"
	 */
	public Integer getRefMessageID() {
		return refMessageID;
	}

	public void setRefMessageID(Integer refMessageID) {
		this.refMessageID = refMessageID;
	}

}
