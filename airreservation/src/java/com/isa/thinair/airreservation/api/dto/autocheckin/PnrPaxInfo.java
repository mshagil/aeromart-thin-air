/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class PnrPaxInfo implements Serializable {

	private static final long serialVersionUID = 7074276407396494816L;
	private String email;
	private String onwardFlightIndicator;
	private String checkInGenderIndicator;
	private String paxType;
	private String preAssignSeatsIndicator;
	private String selectedSeat;
	private String selectedZone;
	private String travelWithInfant;
	private String allowBagThroughCheckin;
	private String allowThroughCheckin;
	private ReservationPaxAdditionalInfo reservationPaxAdditionalInfo;
	private PassengerName passengerName;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOnwardFlightIndicator() {
		return onwardFlightIndicator;
	}

	public void setOnwardFlightIndicator(String onwardFlightIndicator) {
		this.onwardFlightIndicator = onwardFlightIndicator;
	}

	public String getCheckInGenderIndicator() {
		return checkInGenderIndicator;
	}

	public void setCheckInGenderIndicator(String checkInGenderIndicator) {
		this.checkInGenderIndicator = checkInGenderIndicator;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getPreAssignSeatsIndicator() {
		return preAssignSeatsIndicator;
	}

	public void setPreAssignSeatsIndicator(String preAssignSeatsIndicator) {
		this.preAssignSeatsIndicator = preAssignSeatsIndicator;
	}

	public String getSelectedSeat() {
		return selectedSeat;
	}

	public void setSelectedSeat(String selectedSeat) {
		this.selectedSeat = selectedSeat;
	}

	public String getSelectedZone() {
		return selectedZone;
	}

	public void setSelectedZone(String selectedZone) {
		this.selectedZone = selectedZone;
	}

	public String getTravelWithInfant() {
		return travelWithInfant;
	}

	public void setTravelWithInfant(String travelWithInfant) {
		this.travelWithInfant = travelWithInfant;
	}

	public String getAllowBagThroughCheckin() {
		return allowBagThroughCheckin;
	}

	public void setAllowBagThroughCheckin(String allowBagThroughCheckin) {
		this.allowBagThroughCheckin = allowBagThroughCheckin;
	}

	public String getAllowThroughCheckin() {
		return allowThroughCheckin;
	}

	public void setAllowThroughCheckin(String allowThroughCheckin) {
		this.allowThroughCheckin = allowThroughCheckin;
	}

	public ReservationPaxAdditionalInfo getReservationPaxAdditionalInfo() {
		return reservationPaxAdditionalInfo;
	}

	public void setReservationPaxAdditionalInfo(ReservationPaxAdditionalInfo reservationPaxAdditionalInfo) {
		this.reservationPaxAdditionalInfo = reservationPaxAdditionalInfo;
	}

	public PassengerName getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(PassengerName passengerName) {
		this.passengerName = passengerName;
	}

}
