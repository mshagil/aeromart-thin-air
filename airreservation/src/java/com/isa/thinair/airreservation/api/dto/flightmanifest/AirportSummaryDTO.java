package com.isa.thinair.airreservation.api.dto.flightmanifest;

import java.io.Serializable;

public class AirportSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String airportCode;

	private Integer getInCount = 0;

	private Integer getDownCount = 0;

	public AirportSummaryDTO(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public Integer getGetInCount() {
		return getInCount;
	}

	public void setGetInCount(Integer getInCount) {
		this.getInCount = getInCount;
	}

	public Integer getGetDownCount() {
		return getDownCount;
	}

	public void setGetDownCount(Integer getDownCount) {
		this.getDownCount = getDownCount;
	}

}
