package com.isa.thinair.airreservation.api.dto.meal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * Used for Modify Meals
 */
public class PassengerMealAssembler implements Serializable, IPassengerMeal {

	private static final long serialVersionUID = 5034096180184643233L;

	// pnrPaxId|pnrPaxFareId|flightMealId
	private Map<String, Collection<PaxMealTO>> paxMealInfo = new HashMap<String, Collection<PaxMealTO>>();

	// pnrPaxId|pnrPaxFareId|flightSegId
	private Map<String, Collection<PaxMealTO>> paxMealTOAdd = new HashMap<String, Collection<PaxMealTO>>();
	private Map<String, Collection<PaxMealTO>> paxMealTORemove = new HashMap<String, Collection<PaxMealTO>>();

	private Map<Integer, Integer> flightMealIdPnrSegId = new HashMap<Integer, Integer>();
	// private Map<String, Collection<Integer>> statusAndFlightMealIdMap = new HashMap<String, Collection<Integer>>();

	private IPassenger passenger;
	private boolean isForceConfirmed;

	private PaxAdjAssembler adjAssembler;

	public PassengerMealAssembler(IPassenger passenger, boolean isForceConfirmed) {
		this(passenger, null, isForceConfirmed);
	}

	public PassengerMealAssembler(IPassenger passenger, PaxAdjAssembler adjAssembler, boolean isForceConfirmed) {
		this.passenger = passenger;
		this.isForceConfirmed = isForceConfirmed;
		this.adjAssembler = adjAssembler;
	}

	// public Map<String, Collection<Integer>> getStatusFlightMealIdsMap() {
	// return statusAndFlightMealIdMap;
	// }

	public Map<Integer, Integer> getFlightMealIdPnrSegIdsMap() {
		return flightMealIdPnrSegId;
	}

	/**
	 * Add to Meal
	 * 
	 * @throws ModuleException
	 */
	public void addMealInfo(Integer pnrPaxId, Integer pnrPaxFareId, Integer flightMealId, Integer pnrSegId,
			BigDecimal chargeAmount, Integer mealId, Integer allocatedQty, Integer autoCnxId, TrackInfoDTO trackInfo)
			throws ModuleException {

		PaxMealTO paxMealTO = new PaxMealTO();
		paxMealTO.setPnrPaxId(pnrPaxId);
		paxMealTO.setPnrSegId(pnrSegId);
		paxMealTO.setChgDTO(getExternalChgDTOForMeal(chargeAmount, trackInfo));
		paxMealTO.setSelectedFlightMealId(flightMealId);
		paxMealTO.setStatus(AirinventoryCustomConstants.FlightMealStatuses.RESERVED);
		paxMealTO.setPnrPaxFareId(pnrPaxFareId);
		paxMealTO.setMealId(mealId);
		paxMealTO.setAllocatedQty(allocatedQty);
		if (chargeAmount != null
				&& AccelAeroCalculator.isGreaterThan(chargeAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			paxMealTO.setAutoCancellationId(autoCnxId);
		}

		String uniqueId = MealUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, flightMealId);
		if (paxMealInfo.get(uniqueId) == null)
			paxMealInfo.put(uniqueId, new ArrayList<PaxMealTO>());
		paxMealInfo.get(uniqueId).add(paxMealTO);
		flightMealIdPnrSegId.put(flightMealId, pnrSegId);

		if (adjAssembler != null) {
			adjAssembler.addCharge(pnrPaxId, chargeAmount);
		}
	}

	/**
	 * Remove from Meal
	 * 
	 * @param autoCnxId
	 * 
	 * @throws ModuleException
	 */
	public void addPassengertoNewFlightMeal(Integer pnrPaxId, Integer pnrPaxFareId, Integer pnrSegId, Integer flightMealId,
			BigDecimal chargeAmount, String mealCode, Integer mealId, String paxName, Integer autoCnxId, TrackInfoDTO trackInfo)
			throws ModuleException {

		PaxMealTO paxMealTO = new PaxMealTO();
		paxMealTO.setChgDTO(getExternalChgDTOForMeal(chargeAmount, trackInfo));
		paxMealTO.setPnrPaxId(pnrPaxId);
		paxMealTO.setPnrSegId(pnrSegId);
		paxMealTO.setStatus(AirinventoryCustomConstants.FlightMealStatuses.RESERVED);
		paxMealTO.setSelectedFlightMealId(flightMealId);
		paxMealTO.setPnrPaxFareId(pnrPaxFareId);
		paxMealTO.setMealCode(mealCode);
		paxMealTO.setMealId(mealId);
		paxMealTO.setPaxName(paxName);
		if (chargeAmount != null
				&& AccelAeroCalculator.isGreaterThan(chargeAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			paxMealTO.setAutoCancellationId(autoCnxId);
		}
		String uniqueId = MealUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);
		if (paxMealTOAdd.get(uniqueId) == null)
			paxMealTOAdd.put(uniqueId, new ArrayList<PaxMealTO>());
		paxMealTOAdd.get(uniqueId).add(paxMealTO);
		if (adjAssembler != null) {
			adjAssembler.addCharge(pnrPaxId, chargeAmount);
		}
		// putMeal(flightMealId, AirinventoryCustomConstants.FlightMealStatuses.RESERVED);
	}

	/**
	 * Remove from Meal
	 * 
	 * @throws ModuleException
	 */
	public void removePassengerFromFlightMeal(Integer pnrPaxId, Integer pnrPaxFareId, Integer pnrSegId, Integer flightMealId,
			BigDecimal chargeAmount, Integer mealId, String paxName, TrackInfoDTO trackInfo, Integer quantity)
			throws ModuleException {

		PaxMealTO paxMealTO = new PaxMealTO();
		paxMealTO.setPnrPaxId(pnrPaxId);
		paxMealTO.setPnrSegId(pnrSegId);
		paxMealTO.setChgDTO(getExternalChgDTOForMeal(chargeAmount.negate(), trackInfo));
		paxMealTO.setStatus(AirinventoryCustomConstants.FlightMealStatuses.PAX_CNX);
		paxMealTO.setSelectedFlightMealId(flightMealId);
		paxMealTO.setPnrPaxFareId(pnrPaxFareId);
		paxMealTO.setMealId(mealId);
		paxMealTO.setPaxName(paxName);
		paxMealTO.setAllocatedQty(quantity);
		paxMealTO.setAutoCancellationId(null);
		String uniqueId = MealUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);
		if (paxMealTORemove.get(uniqueId) == null)
			paxMealTORemove.put(uniqueId, new ArrayList<PaxMealTO>());
		paxMealTORemove.get(uniqueId).add(paxMealTO);
		if (adjAssembler != null) {
			adjAssembler.removeCharge(pnrPaxId, chargeAmount);
		}
		// putMeal(flightMealId, AirinventoryCustomConstants.FlightMealStatuses.VACANT);

	}

	// private void putMeal(Integer flightMealId, String status) {
	// Collection<Integer> flightMealIds = statusAndFlightMealIdMap.get(status);
	// if (flightMealIds == null) {
	// flightMealIds = new ArrayList<Integer>();
	// flightMealIds.add(flightMealId);
	// statusAndFlightMealIdMap.put(status, flightMealIds);
	// } else {
	// flightMealIds.add(flightMealId);
	// }
	// }

	private ExternalChgDTO getExternalChgDTOForMeal(BigDecimal charge, TrackInfoDTO trackInfo) throws ModuleException {
		if (charge == null) {
			return null;
		}

		Collection<ReservationInternalConstants.EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.MEAL);
		Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationModuleUtils
				.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, trackInfo, null);
		ExternalChgDTO externalChgDTO = (ExternalChgDTO) ((ExternalChgDTO) mapExternalChgs.get(EXTERNAL_CHARGES.MEAL)).clone();
		externalChgDTO.setAmount(charge);
		return externalChgDTO;
	}

	/**
	 * @return the paxMealTOAdd
	 */
	public Map<String, Collection<PaxMealTO>> getPaxMealInfo() {
		return paxMealInfo;
	}

	/**
	 * @return the paxMealTOAdd
	 */
	public Map<String, Collection<PaxMealTO>> getpaxMealTOAdd() {
		return paxMealTOAdd;
	}

	/**
	 * @return the paxMealTORemove
	 */
	public Map<String, Collection<PaxMealTO>> getpaxMealTORemove() {
		return paxMealTORemove;
	}

	/**
	 * @return the passenger
	 */
	public IPassenger getPassenger() {
		return passenger;
	}

	/**
	 * @return the isForceConfirmed
	 */
	public boolean isForceConfirmed() {
		return isForceConfirmed;
	}
}
