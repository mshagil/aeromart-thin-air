package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.model.ReservationPax;

public class ResOnholdValidationDTO implements Serializable {

	private static final long serialVersionUID = 1377260759151541999L;

	public static final int COMBINE_VALIDATION = 1;
	public static final int OTHER_VALIDATION = 2;
	public static final int PAX_VALIDATION = 3;
	public static final int IBE_PAYMENT_VALIDATION = 4;
	public static final int IBE_OFFLINE_VALIDATION = 5;

	private String ipAddress;
	private List<Integer> flightSegIdList;
	private List<Date> depatureDateList;
	private List<ReservationPax> paxList;
	private String contactEmail;
	private int validationType;
	private int salesChannel;

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the flightSegIdList
	 */
	public List<Integer> getFlightSegIdList() {
		return flightSegIdList;
	}

	/**
	 * @param flightSegIdList
	 *            the flightSegIdList to set
	 */
	public void setFlightSegIdList(List<Integer> flightSegIdList) {
		this.flightSegIdList = flightSegIdList;
	}

	public void addFlightSegId(Integer flightSegId) {
		if (flightSegIdList == null) {
			flightSegIdList = new ArrayList<Integer>();
		}
		flightSegIdList.add(flightSegId);
	}

	/**
	 * @return the depatureDateList
	 */
	public List<Date> getDepatureDateList() {
		return depatureDateList;
	}

	/**
	 * @param depatureDateList
	 *            the depatureDateList to set
	 */
	public void setDepatureDateList(List<Date> depatureDateList) {
		this.depatureDateList = depatureDateList;
	}

	public void addDepartureDate(Date depDate) {
		if (depatureDateList == null) {
			depatureDateList = new ArrayList<Date>();
		}
		depatureDateList.add(depDate);
	}

	/**
	 * @return the paxList
	 */
	public List<ReservationPax> getPaxList() {
		return paxList;
	}

	/**
	 * @param paxList
	 *            the paxList to set
	 */
	public void setPaxList(List<ReservationPax> paxList) {
		this.paxList = paxList;
	}

	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * @param contactEmail
	 *            the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * @return the validationType
	 */
	public int getValidationType() {
		return validationType;
	}

	/**
	 * @param validationType
	 *            the validationType to set
	 */
	public void setValidationType(int validationType) {
		this.validationType = validationType;
	}

	/**
	 * @return the serviceChannel
	 */
	public int getSalesChannel() {
		return salesChannel;
	}

	/**
	 * @param salesChannel
	 *            the serviceChannel to set
	 */
	public void setSalesChannel(int salesChannel) {
		this.salesChannel = salesChannel;
	}
}
