package com.isa.thinair.airreservation.api.dto.revacc;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;

/**
 * @author Nilindra Fernando
 */
public class AmountProrateTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5260358346336158209L;

	private ReservationPaxOndCharge reservationPaxOndCharge;

	private Collection<ReservationPaxOndPayment> colReservationPaxOndPayment;

	/**
	 * @return the reservationPaxOndCharge
	 */
	public ReservationPaxOndCharge getReservationPaxOndCharge() {
		return reservationPaxOndCharge;
	}

	/**
	 * @param reservationPaxOndCharge
	 *            the reservationPaxOndCharge to set
	 */
	public void setReservationPaxOndCharge(ReservationPaxOndCharge reservationPaxOndCharge) {
		this.reservationPaxOndCharge = reservationPaxOndCharge;
	}

	/**
	 * @return the colReservationPaxOndPayment
	 */
	public Collection<ReservationPaxOndPayment> getColReservationPaxOndPayment() {
		return colReservationPaxOndPayment;
	}

	/**
	 * @param colReservationPaxOndPayment
	 *            the colReservationPaxOndPayment to set
	 */
	public void setColReservationPaxOndPayment(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		this.colReservationPaxOndPayment = colReservationPaxOndPayment;
	}

}
