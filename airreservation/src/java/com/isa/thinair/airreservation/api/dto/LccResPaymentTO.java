/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 */
public class LccResPaymentTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String groupPNR;

	private String contactPerson;

	private BigDecimal baseAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal paymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String paymentCurrencyCode;

	private String telephoneNumber;

	private String billableAgentCode;

	private String ccLast4Digits;

	private String tnxType;

	private Date tnxDate;

	private String ipAddress;

	private String mobileNumber;

	private Integer nominalCode;

	private Collection<LccPaxPaymentTO> colLCCPaxPaymentTO;

	private String lccUniqueTnxId;

	private Integer temporyPaymentId;

	private String authorizationId;

	/** For pax credit payment */
	private PaxCreditDTO paxCreditDTO;

	private String cardHoldersName;

	private String refundRemarks;

	private Integer lccPnrTnxId;

	private Integer pmtLccPnrTnxId;

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param groupPNR
	 *            the groupPNR to set
	 */
	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the contactPerson
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	/**
	 * @param contactPerson
	 *            the contactPerson to set
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * @return the baseAmount
	 */
	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	/**
	 * @param baseAmount
	 *            the baseAmount to set
	 */
	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	/**
	 * @return the telephoneNumber
	 */
	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	/**
	 * @param telephoneNumber
	 *            the telephoneNumber to set
	 */
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	/**
	 * @return the billableAgentCode
	 */
	public String getBillableAgentCode() {
		return billableAgentCode;
	}

	/**
	 * @param billableAgentCode
	 *            the billableAgentCode to set
	 */
	public void setBillableAgentCode(String billableAgentCode) {
		this.billableAgentCode = billableAgentCode;
	}

	/**
	 * @return the ccLast4Digits
	 */
	public String getCcLast4Digits() {
		return ccLast4Digits;
	}

	/**
	 * @param ccLast4Digits
	 *            the ccLast4Digits to set
	 */
	public void setCcLast4Digits(String ccLast4Digits) {
		this.ccLast4Digits = ccLast4Digits;
	}

	/**
	 * @return the tnxType
	 */
	public String getTnxType() {
		return tnxType;
	}

	/**
	 * @param tnxType
	 *            the tnxType to set
	 */
	public void setTnxType(String tnxType) {
		this.tnxType = tnxType;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the nominalCode
	 */
	public Integer getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            the nominalCode to set
	 */
	public void setNominalCode(Integer nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return the colLCCPaxPaymentTO
	 */
	public Collection<LccPaxPaymentTO> getColLCCPaxPaymentTO() {
		return colLCCPaxPaymentTO;
	}

	/**
	 * @param colLCCPaxPaymentTO
	 *            the colLCCPaxPaymentTO to set
	 */
	public void setColLCCPaxPaymentTO(Collection<LccPaxPaymentTO> colLCCPaxPaymentTO) {
		this.colLCCPaxPaymentTO = colLCCPaxPaymentTO;
	}

	public void addLccPaxPaymentTO(LccPaxPaymentTO lccPaxPaymentTO) {
		if (this.getColLCCPaxPaymentTO() == null) {
			this.setColLCCPaxPaymentTO(new ArrayList<LccPaxPaymentTO>());
		}

		lccPaxPaymentTO.setLccResPaymentTO(this);
		this.getColLCCPaxPaymentTO().add(lccPaxPaymentTO);
	}

	/**
	 * @return the tnxDate
	 */
	public Date getTnxDate() {
		return tnxDate;
	}

	/**
	 * @param tnxDate
	 *            the tnxDate to set
	 */
	public void setTnxDate(Date tnxDate) {
		this.tnxDate = tnxDate;
	}

	/**
	 * @return the paymentAmount
	 */
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * @param paymentAmount
	 *            the paymentAmount to set
	 */
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * @return the paymentCurrencyCode
	 */
	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	/**
	 * @param paymentCurrencyCode
	 *            the paymentCurrencyCode to set
	 */
	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	/**
	 * @return the lccUniqueTnxId
	 */
	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	/**
	 * @param lccUniqueTnxId
	 *            the lccUniqueTnxId to set
	 */
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	/**
	 * @return the temporyPaymentId
	 */
	public Integer getTemporyPaymentId() {
		return temporyPaymentId;
	}

	/**
	 * @param temporyPaymentId
	 *            the temporyPaymentId to set
	 */
	public void setTemporyPaymentId(Integer temporyPaymentId) {
		this.temporyPaymentId = temporyPaymentId;
	}

	/**
	 * @return the authorizationId
	 */
	public String getAuthorizationId() {
		return authorizationId;
	}

	/**
	 * @param authorizationId
	 *            the authorizationId to set
	 */
	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}

	/**
	 * @return the paxCreditDTO
	 */
	public PaxCreditDTO getPaxCreditDTO() {
		return paxCreditDTO;
	}

	/**
	 * @param paxCreditDTO
	 *            the paxCreditDTO to set
	 */
	public void setPaxCreditDTO(PaxCreditDTO paxCreditDTO) {
		this.paxCreditDTO = paxCreditDTO;
	}

	public String getCardHoldersName() {
		return cardHoldersName;
	}

	public void setCardHoldersName(String cardHoldersName) {
		this.cardHoldersName = cardHoldersName;
	}

	public String getRefundRemarks() {
		return refundRemarks;
	}

	public void setRefundRemarks(String refundRemarks) {
		this.refundRemarks = refundRemarks;
	}

	public Integer getLccPnrTnxId() {
		return lccPnrTnxId;
	}

	public void setLccPnrTnxId(Integer lccPnrTnxId) {
		this.lccPnrTnxId = lccPnrTnxId;
	}

	public Integer getPmtLccPnrTnxId() {
		return pmtLccPnrTnxId;
	}

	public void setPmtLccPnrTnxId(Integer pmtLccPnrTnxId) {
		this.pmtLccPnrTnxId = pmtLccPnrTnxId;
	}
}