/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @version $Id$
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto.external;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Isuru
 */
public class CreditCardPaymentFocusDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6643928009110007726L;
	private BigDecimal totalSales = AccelAeroCalculator.getDefaultBigDecimalZero();
	private Date dateOfSale;
	private int cardTypeID;
	private String transferStatus;

	/**
	 * @return Returns the cardTypeID.
	 */
	public int getCardTypeID() {
		return cardTypeID;
	}

	/**
	 * @param cardTypeID
	 *            The cardTypeID to set.
	 */
	public void setCardTypeID(int cardTypeID) {
		this.cardTypeID = cardTypeID;
	}

	/**
	 * @return Returns the dateOfSale.
	 */
	public java.util.Date getDateOfSale() {
		return dateOfSale;
	}

	/**
	 * @param dateOfSale
	 *            The dateOfSale to set.
	 */
	public void setDateOfSale(java.util.Date dateOfSale) {
		this.dateOfSale = dateOfSale;
	}

	/**
	 * @return Returns the transferStatus.
	 */
	public String getTransferStatus() {
		return transferStatus;
	}

	/**
	 * @param transferStatus
	 *            The transferStatus to set.
	 */
	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	/**
	 * @return Returns the totalSales.
	 */
	public BigDecimal getTotalSales() {
		return totalSales;
	}

	/**
	 * @param totalSales
	 *            The totalSales to set.
	 */
	public void setTotalSales(BigDecimal totalSales) {
		this.totalSales = totalSales;
	}

}