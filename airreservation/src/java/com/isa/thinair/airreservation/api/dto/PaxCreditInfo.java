/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold passenger credit data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PaxCreditInfo implements PaymentInfo, Serializable {

	private static final long serialVersionUID = -4565968597466502671L;

	/** Holds total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	/** Hold a collection of PaxCreditDTO */
	private PaxCreditDTO paxCredit;

	/** Holds the payment carrier code */
	private String payCarrier;
	
	/**
	 * Holds original paymentTnxId when the operation is a refund
	 */
	private Integer paymentTnxId;
	
	/**
	 * @return Returns the paxCredit.
	 */
	public PaxCreditDTO getPaxCredit() {
		return paxCredit;
	}

	/**
	 * @param paxCredit
	 *            The paxCredit to set.
	 */
	public void setPaxCredit(PaxCreditDTO paxCredit) {
		this.paxCredit = paxCredit;
	}

	/**
	 * @return Returns the totalAmount.
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            The totalAmount to set.
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	public String getPayCarrier() {
		return payCarrier;
	}

	public void setPayCarrier(String payCarrier) {
		this.payCarrier = payCarrier;
	}
	
	/**
	 * @param paymentTnxId
	 */
	public Integer getPaymentTnxId() {
		return this.paymentTnxId;
	}

	
	public void setPaymentTnxId(Integer paymentTnxId) {
		this.paymentTnxId = paymentTnxId;

	}

}
