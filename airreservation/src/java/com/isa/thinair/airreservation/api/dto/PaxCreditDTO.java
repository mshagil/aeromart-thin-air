/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold passenger credit data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PaxCreditDTO implements Serializable, CustomerPayment {

	private static final long serialVersionUID = 4882696090312064988L;

	/** Holds Passenger Id */
	private Integer paxId;

	/** Holds Passenger balance */
	private BigDecimal balance = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds date of expiry */
	private Date dateOfExpiry;

	/** Holds debit Passenger Id */
	private String debitPaxId;

	/** Holds reservation number of the credit source */
	private String pnr;

	/** Holds reservation originator pnr number of the credit source */
	private String originatorPnr;

	/**
	 * Holds the new pnr which the credit will be utilized form the credit source
	 */
	private String creditUtilizingPnr;

	/** Holds passenger title */
	private String title;

	/** Holds passenger first name */
	private String firstName;

	/** Holds passenger last name */
	private String lastName;

	/** Holds the passenger type */
	private String paxType;

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	private Integer paxSequence;

	/**
	 * carrier which the pax credit is belongs to. This was introduce with the pax credit Sharing across multiple
	 * carriers
	 */
	private String payCarrier;

	/** Holds the lcc unique Id for interline or dry booking */
	private String lccUniqueId;

	private String paxCreditId;

	public PaxCreditDTO() {
	}

	public PaxCreditDTO(String debitPaxId, BigDecimal balance, String payCarrier, PayCurrencyDTO payCurrencyDTO) {
		this.debitPaxId = debitPaxId;
		this.balance = balance;
		this.payCarrier = payCarrier;
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return Returns the paxId.
	 */
	public Integer getPaxId() {
		return paxId;
	}

	/**
	 * @param paxId
	 *            The paxId to set.
	 */
	public void setPaxId(Integer paxId) {
		this.paxId = paxId;
	}

	/**
	 * @return Returns the debitPaxId.
	 */
	public String getDebitPaxId() {
		return debitPaxId;
	}

	/**
	 * @param debitPaxId
	 *            The debitPaxId to set.
	 */
	public void setDebitPaxId(String debitPaxId) {
		this.debitPaxId = debitPaxId;
	}

	/**
	 * @return Returns the dateOfExpiry.
	 */
	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	/**
	 * @param dateOfExpiry
	 *            The dateOfExpiry to set.
	 */
	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the balance.
	 */
	public BigDecimal getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            The balance to set.
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	/**
	 * @return Returns the paxType.
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return the paxSequence
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            the paxSequence to set
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return Returns the originatorPnr.
	 */
	public String getOriginatorPnr() {
		return originatorPnr;
	}

	/**
	 * @param originatorPnr
	 *            the originatorPnr to set
	 */
	public void setOriginatorPnr(String originatorPnr) {
		this.originatorPnr = originatorPnr;
	}

	/**
	 * @return the creditUtilizingPnr
	 */
	public String getCreditUtilizingPnr() {
		return creditUtilizingPnr;
	}

	/**
	 * @param creditUtilizingPnr
	 *            the creditUtilizingPnr to set
	 */
	public void setCreditUtilizingPnr(String creditUtilizingPnr) {
		this.creditUtilizingPnr = creditUtilizingPnr;
	}

	/**
	 * @return the carrierCode
	 */
	public String getPayCarrier() {
		return payCarrier;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setPayCarrier(String carrierCode) {
		this.payCarrier = carrierCode;
	}

	@Override
	public String getLccUniqueId() {
		return this.lccUniqueId;
	}

	@Override
	public void setLccUniqueId(String lccUniqueId) {
		this.lccUniqueId = lccUniqueId;
	}

	public String getPaxCreditId() {
		return paxCreditId;
	}

	public void setPaxCreditId(String paxCreditId) {
		this.paxCreditId = paxCreditId;
	}

}
