/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * $Id$
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;

/**
 * To hold main reservation alert data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationAlertDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2494265744593515738L;

	/** Holds a collection of FlightReservationAlertDTO */
	private Collection<FlightReservationAlertDTO> colFlightReservationAlertDTO;

	/** Holds a type of FlightAlertDTO */
	private FlightAlertDTO flightAlertDTO;

	/**
	 * @return Returns the colFlightReservationAlertDTO.
	 */
	public Collection<FlightReservationAlertDTO> getColFlightReservationAlertDTO() {
		return colFlightReservationAlertDTO;
	}

	/**
	 * @param colFlightReservationAlertDTO
	 *            The colFlightReservationAlertDTO to set.
	 */
	public void setColFlightReservationAlertDTO(Collection<FlightReservationAlertDTO> colFlightReservationAlertDTO) {
		this.colFlightReservationAlertDTO = colFlightReservationAlertDTO;
	}

	/**
	 * @return Returns the flightAlertDTO.
	 */
	public FlightAlertDTO getFlightAlertDTO() {
		return flightAlertDTO;
	}

	/**
	 * @param flightAlertDTO
	 *            The flightAlertDTO to set.
	 */
	public void setFlightAlertDTO(FlightAlertDTO flightAlertDTO) {
		this.flightAlertDTO = flightAlertDTO;
	}

}
