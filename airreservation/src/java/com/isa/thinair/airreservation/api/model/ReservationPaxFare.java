/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To keep track of reservation fare for a passenger
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PNR_PAX_FARE"
 */
public class ReservationPaxFare extends Persistent implements Serializable, Comparable<ReservationPaxFare> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3539105687904959877L;

	/** Holds pnr passenger fare id */
	private Integer pnrPaxFareId;

	/** Holds fare master id */
	private Integer fareId;

	/** Holds the total fare applied to this level */
	private BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total surcharge applied to this level */
	private BigDecimal totalSurCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total tax charge applied to this level */
	private BigDecimal totalTaxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total cancel charge applied to this level */
	private BigDecimal totalCancelCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total modification charge applied to this level */
	private BigDecimal totalModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total adjustment charge applied to this level */
	private BigDecimal totalAdjustmentCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal penaltyCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds a set of ReservationPaxFareSegment */
	private Set<ReservationPaxFareSegment> paxFareSegments;

	/** Holds a set of ReservationPaxOndCharge */
	private Set<ReservationPaxOndCharge> charges;

	/** Holds a instance of reservation passenger */
	private ReservationPax reservationPax;
	
	private String discountCode;
	
	private Set<ReservationPaxOndCreditPromotion> chargesPromotionCredits;

	/**
	 * @return Returns the pnrPaxFareId.
	 * @hibernate.id column = "PPF_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_FARE"
	 */
	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	/**
	 * @param pnrPaxFareId
	 *            The pnrPaxFareId to set.
	 */
	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	/**
	 * @return Returns the fareId.
	 * @hibernate.property column = "FARE_ID"
	 */
	public Integer getFareId() {
		return fareId;
	}

	/**
	 * @param fareId
	 *            The fareId to set.
	 */
	public void setFareId(Integer fareId) {
		this.fareId = fareId;
	}

	/**
	 * @return Returns the paxFareSegments.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PPF_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment"
	 */
	public Set<ReservationPaxFareSegment> getPaxFareSegments() {
		return paxFareSegments;
	}

	/**
	 * @param paxFareSegments
	 *            The paxFareSegments to set.
	 */
	private void setPaxFareSegments(Set<ReservationPaxFareSegment> paxFareSegments) {
		this.paxFareSegments = paxFareSegments;
	}

	/**
	 * Add a ReservationPaxFareSegment
	 * 
	 * @param reservationPaxFareSegment
	 */
	public void addPaxFareSegment(ReservationPaxFareSegment reservationPaxFareSegment) {
		if (this.getPaxFareSegments() == null) {
			this.setPaxFareSegments(new HashSet<ReservationPaxFareSegment>());
		}

		reservationPaxFareSegment.setReservationPaxFare(this);
		this.getPaxFareSegments().add(reservationPaxFareSegment);
	}

	/**
	 * @return Returns the charges.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PPF_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge"
	 */
	public Set<ReservationPaxOndCharge> getCharges() {
		return charges;
	}

	/**
	 * @param charges
	 *            The charges to set.
	 */
	private void setCharges(Set<ReservationPaxOndCharge> charges) {
		this.charges = charges;
	}

	/**
	 * Add a charge
	 * 
	 * @param reservationPaxOndCharge
	 */
	public void addCharge(ReservationPaxOndCharge reservationPaxOndCharge) {
		if (this.getCharges() == null) {
			this.setCharges(new HashSet<ReservationPaxOndCharge>());
		}

		reservationPaxOndCharge.setReservationPaxFare(this);
		this.getCharges().add(reservationPaxOndCharge);
	}

	/**
	 * Removes a charge
	 * 
	 * @param reservationPaxOndCharge
	 */
	public void removeCharge(ReservationPaxOndCharge reservationPaxOndCharge) {
		if (this.getCharges() != null) {
			reservationPaxOndCharge.setReservationPaxFare(null);
			this.getCharges().remove(reservationPaxOndCharge);
		}
	}

	/**
	 * @return Returns the totalSurCharge.
	 * @hibernate.property column = "TOTAL_SURCHARGES"
	 */
	public BigDecimal getTotalSurCharge() {
		return totalSurCharge;
	}

	/**
	 * @param totalSurCharge
	 *            The totalSurCharge to set.
	 */
	public void setTotalSurCharge(BigDecimal totalSurCharge) {
		this.totalSurCharge = totalSurCharge;
	}

	/**
	 * @return Returns the totalTaxCharge.
	 * @hibernate.property column = "TOTAL_TAX"
	 */
	public BigDecimal getTotalTaxCharge() {
		return totalTaxCharge;
	}

	/**
	 * @param totalTaxCharge
	 *            The totalTaxCharge to set.
	 */
	public void setTotalTaxCharge(BigDecimal totalTaxCharge) {
		this.totalTaxCharge = totalTaxCharge;
	}

	/**
	 * @return Returns the totalFare.
	 * @hibernate.property column = "TOTAL_FARE"
	 */
	public BigDecimal getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            The totalFare to set.
	 */
	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	/**
	 * @return Returns the totalAdjustmentCharge.
	 * @hibernate.property column = "TOTAL_ADJ"
	 */
	public BigDecimal getTotalAdjustmentCharge() {
		return totalAdjustmentCharge;
	}

	/**
	 * @param totalAdjustmentCharge
	 *            The totalAdjustmentCharge to set.
	 */
	public void setTotalAdjustmentCharge(BigDecimal totalAdjustmentCharge) {
		this.totalAdjustmentCharge = totalAdjustmentCharge;
	}

	/**
	 * @return Returns the totalCancelCharge.
	 * @hibernate.property column = "TOTAL_CNX"
	 */
	public BigDecimal getTotalCancelCharge() {
		return totalCancelCharge;
	}

	/**
	 * @param totalCancelCharge
	 *            The totalCancelCharge to set.
	 */
	public void setTotalCancelCharge(BigDecimal totalCancelCharge) {
		this.totalCancelCharge = totalCancelCharge;
	}

	/**
	 * @return Returns the totalModificationCharge.
	 * @hibernate.property column = "TOTAL_MOD"
	 */
	public BigDecimal getTotalModificationCharge() {
		return totalModificationCharge;
	}

	/**
	 * @param totalModificationCharge
	 *            The totalModificationCharge to set.
	 */
	public void setTotalModificationCharge(BigDecimal totalModificationCharge) {
		this.totalModificationCharge = totalModificationCharge;
	}

	/**
	 * @return Returns the totalPenaltyCharge
	 * @hibernate.property column = "TOTAL_PEN"
	 */
	public BigDecimal getTotalPenaltyCharge() {
		return penaltyCharge;
	}

	public void setTotalPenaltyCharge(BigDecimal penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	/**
	 * Set the total charges amounts
	 * 
	 * @param totalCharges
	 */
	public void setTotalChargeAmounts(BigDecimal[] totalCharges) {
		this.setTotalFare(totalCharges[0]);
		this.setTotalTaxCharge(totalCharges[1]);
		this.setTotalSurCharge(totalCharges[2]);
		this.setTotalCancelCharge(totalCharges[3]);
		this.setTotalModificationCharge(totalCharges[4]);
		this.setTotalAdjustmentCharge(totalCharges[5]);
		this.setTotalPenaltyCharge(totalCharges[6]);
		this.setTotalDiscount(totalCharges[7]);
	}

	/**
	 * Return the total charges amounts
	 * 
	 * @return
	 */
	public BigDecimal[] getTotalChargeAmounts() {
		return new BigDecimal[] { this.getTotalFare(), this.getTotalTaxCharge(), this.getTotalSurCharge(),
				this.getTotalCancelCharge(), this.getTotalModificationCharge(), this.getTotalAdjustmentCharge(),
				this.getTotalPenaltyCharge(), this.getTotalDiscount().negate() };
	}

	/**
	 * Returns the total charges
	 * 
	 * @return
	 */
	public BigDecimal getTotalChargeAmount() {
		return AccelAeroCalculator.add(getTotalFare(), getTotalTaxCharge(), getTotalSurCharge(), getTotalCancelCharge(),
				getTotalModificationCharge(), getTotalAdjustmentCharge(), getTotalPenaltyCharge(), getTotalDiscount().negate());
	}

	/**
	 * @hibernate.many-to-one column="PNR_PAX_ID" class="com.isa.thinair.airreservation.api.model.ReservationPax"
	 * @return Returns the reservationPax.
	 */
	public ReservationPax getReservationPax() {
		return reservationPax;
	}

	/**
	 * @param reservationPax
	 *            The reservationPax to set.
	 */
	public void setReservationPax(ReservationPax reservationPax) {
		this.reservationPax = reservationPax;
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPnrPaxFareId()).toHashCode();
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationPaxFare) {
			ReservationPaxFare castObject = (ReservationPaxFare) o;
			if (castObject.getPnrPaxFareId() == null || this.getPnrPaxFareId() == null) {
				return false;
			} else if (castObject.getPnrPaxFareId().intValue() == this.getPnrPaxFareId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	public Object clone() {
		ReservationPaxFare reservationPaxFare = new ReservationPaxFare();
		reservationPaxFare.setFareId(this.getFareId());
		reservationPaxFare.setTotalAdjustmentCharge(this.getTotalAdjustmentCharge());
		reservationPaxFare.setTotalCancelCharge(this.getTotalCancelCharge());
		reservationPaxFare.setTotalChargeAmounts(this.getTotalChargeAmounts());
		reservationPaxFare.setTotalFare(this.getTotalFare());
		reservationPaxFare.setTotalModificationCharge(this.getTotalModificationCharge());
		reservationPaxFare.setTotalSurCharge(this.getTotalSurCharge());
		reservationPaxFare.setTotalPenaltyCharge(this.getTotalPenaltyCharge());
		reservationPaxFare.setTotalTaxCharge(this.getTotalTaxCharge());
		reservationPaxFare.setTotalDiscount(this.getTotalDiscount());
		reservationPaxFare.setDiscountCode(this.getDiscountCode());
		return reservationPaxFare;
	}

	/**
	 * @return
	 * @hibernate.property column = "TOTAL_DISCOUNT"
	 */
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {

		if (totalDiscount.doubleValue() < 0)
			totalDiscount = totalDiscount.negate();

		this.totalDiscount = totalDiscount;
	}

	/**
	 * @return
	 * @hibernate.property column = "DISCOUNT_CODE"
	 */
	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	
	/**
	 * @return Returns the charges.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="false"
	 * @hibernate.collection-key column="PPF_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPaxOndCreditPromotion"
	 */	
	public Set<ReservationPaxOndCreditPromotion> getChargesPromotionCredits() {
		return chargesPromotionCredits;
	}

	public void setChargesPromotionCredits(Set<ReservationPaxOndCreditPromotion> chargesPromotionCredits) {
		this.chargesPromotionCredits = chargesPromotionCredits;
	}

	public void addPromotionCredits(ReservationPaxOndCreditPromotion chargesPromotionCredit) {
		if (this.getChargesPromotionCredits() == null) {
			this.setChargesPromotionCredits(new HashSet<ReservationPaxOndCreditPromotion>());
		}
		this.getChargesPromotionCredits().add(chargesPromotionCredit);
	}
	
	public int compareTo(ReservationPaxFare reservationPaxFare) {
		return this.getPnrPaxFareId().compareTo(reservationPaxFare.getPnrPaxFareId());
	}
}
