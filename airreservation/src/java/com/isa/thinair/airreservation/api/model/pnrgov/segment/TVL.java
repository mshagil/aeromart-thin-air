package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class TVL extends EDISegment {

	private String e9916;
	private String e9918;
	private String e9920;
	private String e9922;
	private String e9954;
	private String e3225_1;
	private String e3225_2;
	private String e9906;
	private String e9908;

	public TVL() {
		super(EDISegmentTag.TVL);
	}

	public void setDepartureDate(String e9916) {
		this.e9916 = e9916;
	}

	public void setDepartureTime(String e9918) {
		this.e9918 = e9918;
	}

	public void setArrivalDate(String e9920) {
		this.e9920 = e9920;
	}

	public void setArrivalTime(String e9922) {
		this.e9922 = e9922;
	}

	public void setDateVariation(String e9954) {
		this.e9954 = e9954;
	}

	public void setDepartureAirport(String e3225_1) {
		this.e3225_1 = e3225_1;
	}

	public void setArrivalAirport(String e3225_2) {
		this.e3225_2 = e3225_2;
	}

	public void setOperatingAirline(String e9906) {
		this.e9906 = e9906;
	}

	public void setFlightNumber(String e9908) {
		this.e9908 = e9908;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C310 = new CompoundDataElement("C310", DE_STATUS.M);
		C310.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9916, e9916));
		C310.addBasicDataelement(new Numeric(EDI_ELEMENT.E9918, e9918));
		C310.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9920, e9920));
		C310.addBasicDataelement(new Numeric(EDI_ELEMENT.E9922, e9922));
		C310.addBasicDataelement(new Numeric(EDI_ELEMENT.E9954, e9954));

		CompoundDataElement C328_1 = new CompoundDataElement("C328_1", DE_STATUS.M);
		C328_1.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3225, e3225_1));

		CompoundDataElement C328_2 = new CompoundDataElement("C328_2", DE_STATUS.M);
		C328_2.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3225, e3225_2));

		CompoundDataElement C306 = new CompoundDataElement("C306", DE_STATUS.M);
		C306.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9906, e9906));

		CompoundDataElement C308 = new CompoundDataElement("C308", DE_STATUS.M);
		C308.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9908, e9908));

		addEDIDataElement(C310);
		addEDIDataElement(C328_1);
		addEDIDataElement(C328_2);
		addEDIDataElement(C306);
		addEDIDataElement(C308);
		return this;
	}

}
