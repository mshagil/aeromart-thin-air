package com.isa.thinair.airreservation.api.model.pnrgov;

public abstract class EDIFACTBasicMessage extends EDISegmentGroup implements EDIBuilder {

	private String unbRef;
	private String unhRef;
	private String ungRef;

	public EDIFACTBasicMessage(String segmentName) {
		super(segmentName);
		unbRef = new RefNumGenerator().nextSessionID(14);
		unhRef = new RefNumGenerator().nextSessionID(14);
		ungRef = new RefNumGenerator().nextSessionID(14);
	}

	public String getUnbRef() {
		return this.unbRef;
	}

	public String getUnhRef() {
		return this.unhRef;
	}

	public String getUngRef() {
		return this.ungRef;
	}

}
