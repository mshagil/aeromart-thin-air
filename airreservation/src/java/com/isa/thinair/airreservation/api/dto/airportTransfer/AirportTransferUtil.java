package com.isa.thinair.airreservation.api.dto.airportTransfer;


/**
 * @author Manoj Dhanushka
 * 
 */
public class AirportTransferUtil {	

	public static String makeUniqueIdForModifications(Integer paxId, Integer pnrPaxFareId, Integer pnrSegId) {
		return paxId.toString() + "|" + pnrPaxFareId.toString() + "|" + pnrSegId.toString();
	}
	
	public static String getPnrPaxId(String uniqueId) {
		return uniqueId.substring(0, uniqueId.indexOf("|"));
	}
}
