package com.isa.thinair.airreservation.api.dto.eticket;

import java.io.Serializable;
import java.util.Date;

/**
 * To be used in the E ticket update web service
 */
public class ETicketUpdateRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String flightNo;
	private Date departureDate;
	private String segmentCode;
	private String cabinClassCode;

	private String eTicketNo;
	private String couponNumber;
	private String status;
	private String title;

	private String unaccompaniedETicketNo;
	private String unaccompaniedStatus;
	private String unaccompaniedCouponNo;

	private String firstName;
	private String lastName;
	private String infantFirstName;
	private String infantLsatName;

	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return the departureDate
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            the departureDate to set
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the eTicketNo
	 */
	public String geteTicketNo() {
		return eTicketNo;
	}

	/**
	 * @param eTicketNo
	 *            the eTicketNo to set
	 */
	public void seteTicketNo(String eTicketNo) {
		this.eTicketNo = eTicketNo;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the unaccompaniedETicketNo
	 */
	public String getUnaccompaniedETicketNo() {
		return unaccompaniedETicketNo;
	}

	/**
	 * @param unaccompaniedETicketNo
	 *            the unaccompaniedETicketNo to set
	 */
	public void setUnaccompaniedETicketNo(String unaccompaniedETicketNo) {
		this.unaccompaniedETicketNo = unaccompaniedETicketNo;
	}

	/**
	 * @return the unaccompaniedStatus
	 */
	public String getUnaccompaniedStatus() {
		return unaccompaniedStatus;
	}

	/**
	 * @param unaccompaniedStatus
	 *            the unaccompaniedStatus to set
	 */
	public void setUnaccompaniedStatus(String unaccompaniedStatus) {
		this.unaccompaniedStatus = unaccompaniedStatus;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the infantFirstName
	 */
	public String getInfantFirstName() {
		return infantFirstName;
	}

	/**
	 * @param infantFirstName
	 *            the infantFirstName to set
	 */
	public void setInfantFirstName(String infantFirstName) {
		this.infantFirstName = infantFirstName;
	}

	/**
	 * @return the infantLsatName
	 */
	public String getInfantLsatName() {
		return infantLsatName;
	}

	/**
	 * @param infantLsatName
	 *            the infantLsatName to set
	 */
	public void setInfantLsatName(String infantLsatName) {
		this.infantLsatName = infantLsatName;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getUnaccompaniedCouponNo() {
		return unaccompaniedCouponNo;
	}

	public void setUnaccompaniedCouponNo(String unaccompaniedCouponNo) {
		this.unaccompaniedCouponNo = unaccompaniedCouponNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
}