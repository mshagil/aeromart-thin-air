/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pfs;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashain
 *
 */
public class PassengerCategoryToPFS {
	private String paxCategory;
	private int count;
	private String countDisplay;
	private List<PaxPFSTo> paxList = new ArrayList<PaxPFSTo>();
	
	
	/**
	 * @return the paxCategory
	 */
	public String getPaxCategory() {
		return paxCategory;
	}
	/**
	 * @param paxCategory the paxCategory to set
	 */
	public void setPaxCategory(String paxCategory) {
		this.paxCategory = paxCategory;
	}
	/**
	 * @return the paxList
	 */
	public List<PaxPFSTo> getPaxList() {
		return paxList;
	}
	/**
	 * @param paxList the paxList to set
	 */
	public void setPaxList(List<PaxPFSTo> paxList) {
		this.paxList = paxList;
	}
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return the countDisplay
	 */
	public String getCountDisplay() {
		return countDisplay;
	}
	/**
	 * @param countDisplay the countDisplay to set
	 */
	public void setCountDisplay(String countDisplay) {
		this.countDisplay = countDisplay;
	}
	

	
	
}
