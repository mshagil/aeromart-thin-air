package com.isa.thinair.airreservation.api.dto.automaticCheckin;

/**
 * @author aravinth.r
 *
 */
public class AutoCheckinUtil {

	private static final String SEPERATOR = "|";

	/**
	 * This is to identify unique modifications
	 * 
	 * @param inputParams
	 * @return
	 */
	public static String makeUniqueIdForModifications(Integer... inputParams) {
		String key = "";
		for (Integer input : inputParams) {
			key += input.toString() + SEPERATOR;
		}
		return key;
	}

	// get pnr-pax-id
	public static String getPnrPaxId(String uniqueId) {
		return uniqueId.substring(0, uniqueId.indexOf(SEPERATOR));
	}

}
