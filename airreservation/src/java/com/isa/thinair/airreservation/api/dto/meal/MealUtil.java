package com.isa.thinair.airreservation.api.dto.meal;

import java.util.Collection;

public class MealUtil {

	public static String makeUniqueIdForModifications(Integer paxId, Integer pnrPaxFareId, Integer pnrSegId) {
		return paxId.toString() + "|" + pnrPaxFareId.toString() + "|" + pnrSegId.toString();
	}

	// get pnr-pax-id
	public static String getPnrPaxId(String uniqueId) {
		return uniqueId.substring(0, uniqueId.indexOf("|"));
	}

	// get seat code string for audit
	public static String getMealAuditString(Collection<String> mealCodes) {
		StringBuilder sb = new StringBuilder();
		for (String string : mealCodes) {
			if (sb.length() < 1) {
				sb.append(string);
			} else {
				sb.append("," + string);
			}
		}
		return sb.toString();
	}

	/**
	 * Return collection of PnrPaxIds of the Reservation.
	 * 
	 * @param reservation
	 * @return
	 */
//	public static Collection<Integer> getPnrPaxIds(Reservation reservation) {
//		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
//		Iterator<ReservationPax> iter = reservation.getPassengers().iterator();
//		while (iter.hasNext()) {
//			ReservationPax resPax = (ReservationPax) iter.next();
//			pnrPaxIds.add(resPax.getPnrPaxId());
//		}
//		return pnrPaxIds;
//	}

}
