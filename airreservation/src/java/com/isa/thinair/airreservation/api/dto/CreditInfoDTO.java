/**
 * 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author indika
 * 
 */
public class CreditInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8861895394677721756L;

	public enum status {
		AVAILABLE, EXPIRED
	};

	private int creditId;

	private int txnId;

	private BigDecimal amount;

	private Date expireDate;

	private status enumStatus;

	private String userNote;

	private String carrierCode;
	
	private String currencyCode;
	
	private String lccUniqueTnxId;
	
	/** Amount with respect to Marketing carrier*/
	private BigDecimal mcAmount;
		
	private boolean nonRefundableCredit;
	/**
	 * @return Returns the expireDate.
	 */
	public Date getExpireDate() {
		return expireDate;
	}

	/**
	 * @param expireDate
	 *            The expireDate to set.
	 */
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	/**
	 * @return Returns the txn_id.
	 */
	public int getTxnId() {
		return txnId;
	}

	/**
	 * @param txn_id
	 *            The txn_id to set.
	 */
	public void setTxnId(int txnId) {
		this.txnId = txnId;
	}

	/**
	 * @return Returns the enumStatus.
	 */
	public status getEnumStatus() {
		return enumStatus;
	}

	/**
	 * @param enumStatus
	 *            The enumStatus to set.
	 */
	public void setEnumStatus(status enumStatus) {
		this.enumStatus = enumStatus;
	}

	/**
	 * @return Returns the creditId.
	 */
	public int getCreditId() {
		return creditId;
	}

	/**
	 * @param creditId
	 *            The creditId to set.
	 */
	public void setCreditId(int creditId) {
		this.creditId = creditId;
	}

	/**
	 * @return Returns the amount.
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the userNote
	 */
	public String getUserNote() {
		return userNote;
	}

	/**
	 * @param userNote
	 *            the userNote to set
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public BigDecimal getMcAmount() {
		return mcAmount;
	}

	public void setMcAmount(BigDecimal mcAmount) {
		this.mcAmount = mcAmount;
	}

	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	public boolean isNonRefundableCredit() {
		return nonRefundableCredit;
	}

	public void setNonRefundableCredit(boolean nonRefundableCredit) {
		this.nonRefundableCredit = nonRefundableCredit;
	}
	
}
