/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * ReservationCredit is the entity class to represent a ReservationCredit model
 * 
 * @author Lasantha Pambagoda
 * @hibernate.class table = "T_PAX_CREDIT"
 */
public class ReservationCredit extends Persistent {

	private static final long serialVersionUID = -1271274225121484982L;

	private int creditId;

	private String pnrPaxId;

	private long tnxId;

	private Date expiryDate;

	private BigDecimal balance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Integer carredForwardPaymentId;

	private String note;
	
	private Set<ReservationPaxPaymentCredit> payments;
	
	private Integer creditReInstatedTnxId;	
	/**
	 * @return Returns the note.
	 * 
	 * @hibernate.property column = "NOTE"
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            The note to set.
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return Returns the creditId.
	 * @hibernate.id column = "PAY_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PAX_CREDIT"
	 */
	public int getCreditId() {
		return creditId;
	}

	/**
	 * @param creditId
	 *            The creditId to set.
	 */
	public void setCreditId(int creditId) {
		this.creditId = creditId;
	}

	/**
	 * @return Returns the pnrPaxId.
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public String getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(String pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the balance.
	 * @hibernate.property column = "BALANCE"
	 */
	public BigDecimal getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            The balance to set.
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	/**
	 * @return Returns the carredForwardPaymentId.
	 * @hibernate.property column = "CF_FROM"
	 */
	public Integer getCarredForwardPaymentId() {
		return carredForwardPaymentId;
	}

	/**
	 * @param carredForwardPaymentId
	 *            The carredForwardPaymentId to set.
	 */
	public void setCarredForwardPaymentId(Integer carredForwardPaymentId) {
		this.carredForwardPaymentId = carredForwardPaymentId;
	}

	/**
	 * @return Returns the expiryDate.
	 * @hibernate.property column = "DATE_EXP"
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            The expiryDate to set.
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return Returns the tnxId.
	 * @hibernate.property column = "TXN_ID"
	 */
	public long getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            The tnxId to set.
	 */
	public void setTnxId(long tnxId) {
		this.tnxId = tnxId;
	}

	public ReservationCredit clone() {
		ReservationCredit reservationCredit = new ReservationCredit();
		reservationCredit.setBalance(this.getBalance());
		reservationCredit.setCarredForwardPaymentId(this.getCarredForwardPaymentId());
		reservationCredit.setExpiryDate(this.getExpiryDate());
		reservationCredit.setNote(this.getNote());
		reservationCredit.setPnrPaxId(this.getPnrPaxId());
		reservationCredit.setTnxId(this.getTnxId());

		return reservationCredit;
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getCreditId()).toHashCode();
	}

	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationCredit) {
			ReservationCredit castObject = (ReservationCredit) o;
			if (castObject.getCreditId() == 0 || this.getCreditId() == 0) {
				return false;
			} else if (castObject.getCreditId() == this.getCreditId()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * @return Returns the charges.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PAY_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPaxPaymentCredit"
	 */
	public Set<ReservationPaxPaymentCredit> getPayments() {
		return payments;
	}

	private void setPayments(Set<ReservationPaxPaymentCredit> payments) {
		this.payments = payments;
	}

	public void addPayments(ReservationPaxPaymentCredit paymentCredit) {
		if (this.getPayments() == null) {
			this.setPayments(new HashSet<ReservationPaxPaymentCredit>());
		}
		paymentCredit.setReservationCredit(this);
		this.getPayments().add(paymentCredit);
	}
	
	public BigDecimal getPaymentCreditTotal() {
		BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (this.getPayments() != null && this.getPayments().size() > 0) {

			for (ReservationPaxPaymentCredit paxPaymentCredit : this.getPayments()) {

				if (paxPaymentCredit != null) {

					totalCreditAmount = AccelAeroCalculator.add(totalCreditAmount, paxPaymentCredit.getAmount());

				}

			}

		}

		return totalCreditAmount;
	}
	
	/**
	 * @return Returns the creditReInstatedTnxId.
	 * @hibernate.property column = "REINSTATED_TNX_ID"
	 */
	public Integer getCreditReInstatedTnxId() {
		return creditReInstatedTnxId;
	}

	public void setCreditReInstatedTnxId(Integer creditReInstatedTnxId) {
		this.creditReInstatedTnxId = creditReInstatedTnxId;
	}
}
