package com.isa.thinair.airreservation.api.dto.flightmanifest;

import java.io.Serializable;

public class ManifestCountDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer maleCount = 0;

	private Integer femaleCount = 0;

	private Integer childCount = 0;

	private Integer infantCount = 0;

	private Integer unknownCount = 0;

	private Integer totalCount = 0;

	private Double totalExcessBaggageWeight = 0.0;

	public Integer getMaleCount() {
		return maleCount;
	}

	public Integer getFemaleCount() {
		return femaleCount;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public Integer getInfantCount() {
		return infantCount;
	}

	public Integer getUnknownCount() {
		return unknownCount;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setMaleCount(Integer maleCount) {
		this.maleCount = maleCount;
	}

	public void setFemaleCount(Integer femaleCount) {
		this.femaleCount = femaleCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	public void setUnknownCount(Integer unknownCount) {
		this.unknownCount = unknownCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Double getTotalExcessBaggageWeight() {
		return totalExcessBaggageWeight;
	}

	public void setTotalExcessBaggageWeight(Double totalExcessBaggageWeight) {
		this.totalExcessBaggageWeight = totalExcessBaggageWeight;
	}


}
