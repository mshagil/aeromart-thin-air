package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class ReservationPaxAdditionalInfo implements Serializable {

	private static final long serialVersionUID = 8380698668130244619L;
	private String passportNumber;
	private String passportIssuedCountry;
	private String passportExpiryDate;
	private String passportIssuedDate;
	private String passportFullName;
	private Integer visaType;
	private String visaNumber;
	private String visaIssuedCountry;
	private String visaIssuedDate;
	private String visExpiredDate;
	private String dateOfBirth;
	private String countryOfCitizenship;
	private String countryOfResidence;
	private String placeOfBirth;
	private String destinationAddressLine1;
	private String destinationAddressLine2;
	private String destinationCity;
	private String destinationZipCode;
	private String destinationState;
	private String destinationCountryCode;
	private String nationality;
	private String travelDocType;
	private String staffId;
	private String dateOfJoin;
	private String visaApplicableCountry;
	private String documentSubType;
	private String gender;

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getPassportIssuedCountry() {
		return passportIssuedCountry;
	}

	public void setPassportIssuedCountry(String passportIssuedCountry) {
		this.passportIssuedCountry = passportIssuedCountry;
	}

	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}

	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	public String getPassportIssuedDate() {
		return passportIssuedDate;
	}

	public void setPassportIssuedDate(String passportIssuedDate) {
		this.passportIssuedDate = passportIssuedDate;
	}

	public String getPassportFullName() {
		return passportFullName;
	}

	public void setPassportFullName(String passportFullName) {
		this.passportFullName = passportFullName;
	}

	public Integer getVisaType() {
		return visaType;
	}

	public void setVisaType(Integer visaType) {
		this.visaType = visaType;
	}

	public String getVisaNumber() {
		return visaNumber;
	}

	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}

	public String getVisaIssuedCountry() {
		return visaIssuedCountry;
	}

	public void setVisaIssuedCountry(String visaIssuedCountry) {
		this.visaIssuedCountry = visaIssuedCountry;
	}

	public String getVisaIssuedDate() {
		return visaIssuedDate;
	}

	public void setVisaIssuedDate(String visaIssuedDate) {
		this.visaIssuedDate = visaIssuedDate;
	}

	public String getVisExpiredDate() {
		return visExpiredDate;
	}

	public void setVisExpiredDate(String visExpiredDate) {
		this.visExpiredDate = visExpiredDate;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCountryOfCitizenship() {
		return countryOfCitizenship;
	}

	public void setCountryOfCitizenship(String countryOfCitizenship) {
		this.countryOfCitizenship = countryOfCitizenship;
	}

	public String getCountryOfResidence() {
		return countryOfResidence;
	}

	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getDestinationAddressLine1() {
		return destinationAddressLine1;
	}

	public void setDestinationAddressLine1(String destinationAddressLine1) {
		this.destinationAddressLine1 = destinationAddressLine1;
	}

	public String getDestinationAddressLine2() {
		return destinationAddressLine2;
	}

	public void setDestinationAddressLine2(String destinationAddressLine2) {
		this.destinationAddressLine2 = destinationAddressLine2;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDestinationZipCode() {
		return destinationZipCode;
	}

	public void setDestinationZipCode(String destinationZipCode) {
		this.destinationZipCode = destinationZipCode;
	}

	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public String getDestinationCountryCode() {
		return destinationCountryCode;
	}

	public void setDestinationCountryCode(String destinationCountryCode) {
		this.destinationCountryCode = destinationCountryCode;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getTravelDocType() {
		return travelDocType;
	}

	public void setTravelDocType(String travelDocType) {
		this.travelDocType = travelDocType;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(String dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}

	public String getDocumentSubType() {
		return documentSubType;
	}

	public void setDocumentSubType(String documentSubType) {
		this.documentSubType = documentSubType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
