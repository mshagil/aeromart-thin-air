package com.isa.thinair.airreservation.api.dto.seatmap;

import java.io.Serializable;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

public class PaxSeatTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4650454422307222213L;
	private Integer pkey;
	private Integer pnrPaxFareId;
	private Integer pnrPaxId;
	private Integer pnrSegId;
	private Integer selectedFlightSeatId;
	private String paxType;
	private String status;
	private String seatCode;
	private ExternalChgDTO chgDTO;
	private String paxName;
	private Integer autoCancellationId;
	private boolean reprotectedToAnotherSeat = false;

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public Integer getSelectedFlightSeatId() {
		return selectedFlightSeatId;
	}

	public void setSelectedFlightSeatId(Integer selectedFlightSeatId) {
		this.selectedFlightSeatId = selectedFlightSeatId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ExternalChgDTO getChgDTO() {
		return chgDTO;
	}

	public void setChgDTO(ExternalChgDTO chgDTO) {
		this.chgDTO = chgDTO;
	}

	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	public Integer getPkey() {
		return pkey;
	}

	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	public boolean isReprotectedToAnotherSeat() {
		return reprotectedToAnotherSeat;
	}

	public void setReprotectedToAnotherSeat(boolean reprotectedToAnotherSeat) {
		this.reprotectedToAnotherSeat = reprotectedToAnotherSeat;
	}

}
