package com.isa.thinair.airreservation.api.model.pnrgov;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;

public abstract class EDISegment extends MessageSegment {

	private ArrayList<EDIDataElement> ediDataElements = new ArrayList<EDIDataElement>();

	public EDISegment(EDISegmentTag segment) {
		super(segment.toString());
	}

	@Override
	public String getEDIFmtData() throws PNRGOVException {
		StringBuilder sb = new StringBuilder(segmentName);
		for (int i = 0; i < ediDataElements.size(); i++) {
			EDIDataElement element = ediDataElements.get(i);
			try {
				sb.append(ElementFactory.DATA_ELEMENT_SEPERATOR).append(element.getEDIFmtData());
			} catch (PNRGOVException e) {
				StringBuffer sbe = new StringBuffer("[" + segmentName + "] " + e.getMessage());
				throw new PNRGOVException(sbe.toString());
			}
		}

		String ediString = sb.toString();
		String processedStr = StringUtils.stripEnd(ediString, ElementFactory.DATA_ELEMENT_SEPERATOR);
		return processedStr + ElementFactory.SEGMENT_SEPERATOR;
	}

	@Override
	public boolean isValidDataFormat() {
		return true;
	}

	public void addEDIDataElement(EDIDataElement element) {
		if (element != null) {
			ediDataElements.add(element);
		}
	}

	@Override
	public int getSegmentCount() {
		return 1;
	}

}
