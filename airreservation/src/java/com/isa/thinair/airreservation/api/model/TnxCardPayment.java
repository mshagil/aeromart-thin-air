/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id $
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

/**
 * Holds the Transaction Credit Payment
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class TnxCardPayment extends TnxPayment {

	private static final long serialVersionUID = 1L;

	private String cardNumber;
	private String cardName;
	private String address;
	private String cardExpiryDate;
	private String secCode;
	private String pnr;
	private int paymentBrokerRefNumber;
	private String authorizationId;

	/**
	 * @return Returns the address.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            The address to set.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return Returns the cardExpiryDate.
	 */
	public String getCardExpiryDate() {
		return cardExpiryDate;
	}

	/**
	 * @param cardExpiryDate
	 *            The cardExpiryDate to set.
	 */
	public void setCardExpiryDate(String cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}

	/**
	 * @return Returns the cardName.
	 */
	public String getCardName() {
		return cardName;
	}

	/**
	 * @param cardName
	 *            The cardName to set.
	 */
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	/**
	 * @return Returns the cardNumber.
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber
	 *            The cardNumber to set.
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return Returns the secCode.
	 */
	public String getSecCode() {
		return secCode;
	}

	/**
	 * @param secCode
	 *            The secCode to set.
	 */
	public void setSecCode(String secCode) {
		this.secCode = secCode;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the paymentBrokerRefNumber.
	 */
	public int getPaymentBrokerRefNumber() {
		return paymentBrokerRefNumber;
	}

	/**
	 * @param paymentBrokerRefNumber
	 *            The paymentBrokerRefNumber to set.
	 */
	public void setPaymentBrokerRefNumber(int paymentBrokerRefNumber) {
		this.paymentBrokerRefNumber = paymentBrokerRefNumber;
	}

	public String getAuthorizationId() {
		return authorizationId;
	}

	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}

}
