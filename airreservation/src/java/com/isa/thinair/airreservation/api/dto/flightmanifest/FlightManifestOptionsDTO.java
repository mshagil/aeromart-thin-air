package com.isa.thinair.airreservation.api.dto.flightmanifest;

import java.io.Serializable;

import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * DTO for sending flight manifest options to backend
 * 
 * @author jpadukka
 * 
 */
public class FlightManifestOptionsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int flightId;

	private boolean checkInwardConnections;

	private boolean checkOutwardConnections;

	private String maxInwardConnectionTime;

	private String maxOutwardConnectionTime;

	private String outwardConnectionDestination;

	private String inwardConnectionOrigin;

	private String inwardBoardingAirport;

	private String outwardBoardingAirport;

	private String inwardFlightNo;

	private String outwardFlightNo;

	private String sortOption;

	private String cabinClass;

	private String logicalCabinClass;

	private String summaryMode;

	private String viewOption;

	private String summaryPosition;

	private boolean confirmedReservation;

	private boolean email = false;

	private boolean filterByCabinClass = false;

	private boolean filterByLogicalCabinClass = false;

	private String emailId;

	private String flightNo;

	private String flightDate;

	private String route;

	private String passportStatus;
	
	private String docsStatus;
	
	private boolean showPaxIndentification;

	private boolean showSsr;

	private boolean showSeatMap;

	private boolean showMeal;

	private boolean showBaggage;

	private boolean showCreditCardDetails;

	private boolean showAirportServices;

	private boolean searchTriggerdByDifferentCarrier = false;

	private String dataSourceMode;
	
	private boolean download = false;
	
	private boolean privilegedToShowAllBookings;

	private boolean loadByPnr;
	private UserPrincipal userPrincipal;

	public static final String INWARD_CONNECTIONS = "in";
	public static final String OUTWARD_CONNECTIONS = "out";
	public static final String BOTH_CONNECTIONS = "both";

	public static final String EMAIL = "email";
	public static final String DOWNLOAD = "download";
	public static final String VIEW_OPTIONS_DETAILED = "detailed";
	public static final String VIEW_OPTIONS_SUMMARY = "summary";
	public static final String SUMMARY_OPTIONS_SEGMENTWISE = "segment";
	public static final String SUMMARY_OPTIONS_INOUT = "inout";
	public static final String SUMMARY_POSITION_TOP = "top";
	public static final String SUMMARY_POSITION_BOTTOM = "bottom";

	public static final String GROUP_BY_CABIN_CLASS = "cos";
	public static final String GROUP_BY_LOGICAL_CABIN_CLASS = "lcc";

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public boolean isCheckInwardConnections() {
		return checkInwardConnections;
	}

	public void setCheckInwardConnections(boolean checkInwardConnections) {
		this.checkInwardConnections = checkInwardConnections;
	}

	public boolean isCheckOutwardConnections() {
		return checkOutwardConnections;
	}

	public void setCheckOutwardConnections(boolean checkOutwordConnections) {
		this.checkOutwardConnections = checkOutwordConnections;
	}

	public String getMaxInwardConnectionTime() {
		return maxInwardConnectionTime;
	}

	public void setMaxInwardConnectionTime(String maxInwardConnectionTime) {
		this.maxInwardConnectionTime = maxInwardConnectionTime;
	}

	public String getMaxOutwardConnectionTime() {
		return maxOutwardConnectionTime;
	}

	public void setMaxOutwardConnectionTime(String maxOutwardConnectionTime) {
		this.maxOutwardConnectionTime = maxOutwardConnectionTime;
	}

	public String getInwardConnectionOrigin() {
		return inwardConnectionOrigin;
	}

	public void setInwardConnectionOrigin(String inwardConnectionOrigin) {
		this.inwardConnectionOrigin = inwardConnectionOrigin;
	}

	public String getInwardBoardingAirport() {
		return inwardBoardingAirport;
	}

	public void setInwardBoardingAirport(String inwardBoardingAirport) {
		this.inwardBoardingAirport = inwardBoardingAirport;
	}

	public String getOutwardBoardingAirport() {
		return outwardBoardingAirport;
	}

	public void setOutwardBoardingAirport(String outwardBoardingAirport) {
		this.outwardBoardingAirport = outwardBoardingAirport;
	}

	public String getInwardFlightNo() {
		return inwardFlightNo;
	}

	public void setInwardFlightNo(String inwardFlightNo) {
		this.inwardFlightNo = inwardFlightNo;
	}

	public String getOutwardFlightNo() {
		return outwardFlightNo;
	}

	public void setOutwardFlightNo(String outwardFlightNo) {
		this.outwardFlightNo = outwardFlightNo;
	}

	public String getOutwardConnectionDestination() {
		return outwardConnectionDestination;
	}

	public void setOutwardConnectionDestination(String outwardConnectionDestination) {
		this.outwardConnectionDestination = outwardConnectionDestination;
	}

	public String getSortOption() {
		return sortOption;
	}

	public void setSortOption(String sortOption) {
		this.sortOption = sortOption;
	}

	public boolean isConfirmedReservation() {
		return confirmedReservation;
	}

	public void setConfirmedReservation(boolean confirmedReservation) {
		this.confirmedReservation = confirmedReservation;
	}

	public boolean isEmail() {
		return email;
	}

	public void setEmail(boolean email) {
		this.email = email;
	}

	public String getSummaryMode() {
		return summaryMode;
	}

	public String getViewOption() {
		return viewOption;
	}

	public String getSummaryPosition() {
		return summaryPosition;
	}

	public void setSummaryMode(String summaryMode) {
		this.summaryMode = summaryMode;
	}

	public void setViewOption(String viewOption) {
		this.viewOption = viewOption;
	}

	public void setSummaryPosition(String summaryPosition) {
		this.summaryPosition = summaryPosition;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public String getRoute() {
		return route;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public boolean isFilterByCabinClass() {
		return filterByCabinClass;
	}

	public void setFilterByCabinClass(boolean filterByCabinClass) {
		this.filterByCabinClass = filterByCabinClass;
	}

	public boolean isFilterByLogicalCabinClass() {
		return filterByLogicalCabinClass;
	}

	public void setFilterByLogicalCabinClass(boolean filterByLogicalCabinClass) {
		this.filterByLogicalCabinClass = filterByLogicalCabinClass;
	}

	public boolean isShowPaxIndentification() {
		return showPaxIndentification;
	}

	public void setShowPaxIndentification(boolean showPaxIndentification) {
		this.showPaxIndentification = showPaxIndentification;
	}

	public boolean isShowSsr() {
		return showSsr;
	}

	public void setShowSsr(boolean showSsr) {
		this.showSsr = showSsr;
	}

	public boolean isShowSeatMap() {
		return showSeatMap;
	}

	public void setShowSeatMap(boolean showSeatMap) {
		this.showSeatMap = showSeatMap;
	}

	public boolean isShowMeal() {
		return showMeal;
	}

	public void setShowMeal(boolean showMeal) {
		this.showMeal = showMeal;
	}

	public boolean isShowCreditCardDetails() {
		return showCreditCardDetails;
	}

	public void setShowCreditCardDetails(boolean showCreditCardDetails) {
		this.showCreditCardDetails = showCreditCardDetails;
	}

	public boolean isShowAirportServices() {
		return showAirportServices;
	}

	public void setShowAirportServices(boolean showAirportServices) {
		this.showAirportServices = showAirportServices;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public boolean isShowBaggage() {
		return showBaggage;
	}

	public void setShowBaggage(boolean showBaggage) {
		this.showBaggage = showBaggage;
	}

	public String getPassportStatus() {
		return passportStatus;
	}

	public void setPassportStatus(String passportStatus) {
		this.passportStatus = passportStatus;
	}

	public String getDocsStatus() {
		return docsStatus;
	}

	public void setDocsStatus(String docsStatus) {
		this.docsStatus = docsStatus;
	}

	public boolean isSearchTriggerdByDifferentCarrier() {
		return searchTriggerdByDifferentCarrier;
	}

	public void setSearchTriggerdByDifferentCarrier(boolean searchTriggerdByDifferentCarrier) {
		this.searchTriggerdByDifferentCarrier = searchTriggerdByDifferentCarrier;
	}

	public String getDataSourceMode() {
		return dataSourceMode;
	}

	public void setDataSourceMode(String dataSourceMode) {
		this.dataSourceMode = dataSourceMode;
	}

	public boolean isLoadByPnr() {
		return loadByPnr;
	}

	public void setLoadByPnr(boolean loadByPnr) {
		this.loadByPnr = loadByPnr;
	}

	public boolean isDownload() {
		return download;
	}

	public void setDownload(boolean download) {
		this.download = download;
	}

	public boolean isPrivilegedToShowAllBookings() {
		return privilegedToShowAllBookings;
	}

	public void setPrivilegedToShowAllBookings(boolean privilegedToShowAllBookings) {
		this.privilegedToShowAllBookings = privilegedToShowAllBookings;
	}
}
