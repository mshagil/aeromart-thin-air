package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;

/**
 * Using a Dto to populate itinerary depending on the service
 * 
 * @author Thushara
 * 
 */
public class PaxSSRInfoDTO implements Serializable {

	private static final long serialVersionUID = 4916628131074044994L;
	private Collection<PaxSSRDTO> ssrDto;
	private boolean hasSeats = false;
	private boolean hasMeals = false;
	private boolean hasAirportService = false;
	private boolean hasAdditionalSeats = false;

	public Collection<PaxSSRDTO> getSsrDto() {
		return ssrDto;
	}

	public void setSsrDto(Collection<PaxSSRDTO> ssrDto) {
		this.ssrDto = ssrDto;
	}

	public boolean isHasSeats() {
		return hasSeats;
	}

	public void setHasSeats(boolean hasSeats) {
		this.hasSeats = hasSeats;
	}

	public boolean isHasMeals() {
		return hasMeals;
	}

	public void setHasMeals(boolean hasMeals) {
		this.hasMeals = hasMeals;
	}

	/**
	 * @return Returns the hasAirportService.
	 */
	public boolean isHasAirportService() {
		return hasAirportService;
	}

	/**
	 * @param hasAirportService
	 *            The hasAirportService to set.
	 */
	public void setHasAirportService(boolean hasAirportService) {
		this.hasAirportService = hasAirportService;
	}

	/**
	 * @return the hasAdditionalSeats
	 */
	public boolean isHasAdditionalSeats() {
		return hasAdditionalSeats;
	}

	/**
	 * @param hasAdditionalSeats
	 *            the hasAdditionalSeats to set
	 */
	public void setHasAdditionalSeats(boolean hasAdditionalSeats) {
		this.hasAdditionalSeats = hasAdditionalSeats;
	}

}
