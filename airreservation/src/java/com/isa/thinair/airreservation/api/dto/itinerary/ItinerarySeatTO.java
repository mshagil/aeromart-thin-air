package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItinerarySeatTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String seatNumber;

	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

}
