package com.isa.thinair.airreservation.api.dto;

public class NameChangeExtChgDTO extends ExternalChgDTO {

	private static final long serialVersionUID = -3428414375607767076L;

	private Integer pnrPaxId;

	private Integer pnrPaxFareId;	

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}
}
