/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * Holds the when same flight modification COS information
 * 
 * @author M.Rikaz
 * @since 03/10/2012
 */
public class FlightCOSDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6732060126265578320L;

	private Integer flightSegId;

	private String newCabinClassCode;

	private Integer newRanking;

	private String oldCabinClassCode;

	private Integer oldRanking;

	private boolean isCOSUpperOrSame;

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public String getNewCabinClassCode() {
		return newCabinClassCode;
	}

	public void setNewCabinClassCode(String newCabinClassCode) {
		this.newCabinClassCode = newCabinClassCode;
	}

	public Integer getNewRanking() {
		return newRanking;
	}

	public void setNewRanking(Integer newRanking) {
		this.newRanking = newRanking;
	}

	public String getOldCabinClassCode() {
		return oldCabinClassCode;
	}

	public void setOldCabinClassCode(String oldCabinClassCode) {
		this.oldCabinClassCode = oldCabinClassCode;
	}

	public Integer getOldRanking() {
		return oldRanking;
	}

	public void setOldRanking(Integer oldRanking) {
		this.oldRanking = oldRanking;
	}

	public boolean isCOSUpperOrSame() {
		return isCOSUpperOrSame;
	}

	public void setCOSUpperOrSame(boolean isCOSUpperOrSame) {
		this.isCOSUpperOrSame = isCOSUpperOrSame;
	}

}
