/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.service;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Lasantha Pambagoda
 * 
 * @isa.module.bd-intf id="creditaccount.service"
 */
public interface CreditAccountBD {
	public ServiceResponce balanceCredit(String pnrPaxId, BigDecimal amount) throws ModuleException;

	public ServiceResponce carryForwardCredit(PaxCreditDTO paxCreditDTO, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity) throws ModuleException;

	public ServiceResponce broughtForwardCredit(PaxCreditDTO paxCreditDTO, CredentialsDTO credentialsDTO) throws ModuleException;

}
