package com.isa.thinair.airreservation.api.dto.pnrgov;

import java.util.Date;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;

public class PNRGOVSegmentDTO {

	private Integer pnrSegId;

	private Date departureDate;

	private Date arrivalDate;

	private String segmentCode;

	private String flightNumber;

	private String operatingAirlineCode;
	
	private String groundSegId;

	private Map<Integer, PaxSeatTO> bookedSeat;

	private Map<Integer, PaxMealTO> bookedMeal;

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getOperatingAirlineCode() {
		return operatingAirlineCode;
	}

	public void setOperatingAirlineCode(String operatingAirlineCode) {
		this.operatingAirlineCode = operatingAirlineCode;
	}

	public String getFlightNo() {
		if (this.getOperatingAirlineCode() != null && !this.getOperatingAirlineCode().equals("")) {
			return this.getFlightNumber().substring(this.getOperatingAirlineCode().length());
		} else {
			return this.getFlightNumber();
		}
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public Map<Integer, PaxSeatTO> getBookedSeat() {
		return bookedSeat;
	}

	public void setBookedSeat(Map<Integer, PaxSeatTO> bookedSeat) {
		this.bookedSeat = bookedSeat;
	}

	public Map<Integer, PaxMealTO> getBookedMeal() {
		return bookedMeal;
	}

	public void setBookedMeal(Map<Integer, PaxMealTO> bookedMeal) {
		this.bookedMeal = bookedMeal;
	}

	public String getGroundSegId() {
		return groundSegId;
	}

	public void setGroundSegId(String groundSegId) {
		this.groundSegId = groundSegId;
	}

}
