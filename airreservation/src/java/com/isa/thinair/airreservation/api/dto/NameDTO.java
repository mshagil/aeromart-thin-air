/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * @author isuru DTO will wrap the name and the title
 */
@SuppressWarnings("rawtypes")
public class NameDTO implements Comparable, Serializable {

	private String firstname;
	private String title;
	private String remarkText;
	private String pad;
	private String ssrCode;
	private String ssrText;
	private String lastName;
	private String ssrFirstName;
	private String ssrTitle;
	private String seatCode;
	private String seatNumbers;
	private Integer pnrPaxId;
	private String paxType;
	private String paxReference;
	private String FFID;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return this.firstname.compareTo(((NameDTO) arg0).getFirstname());
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSsrText() {
		return ssrText;
	}

	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public String getSsrFirstName() {
		return ssrFirstName;
	}

	public void setSsrFirstName(String ssrFirstName) {
		this.ssrFirstName = ssrFirstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastname) {
		this.lastName = lastname;
	}

	public String getSsrTitle() {
		return ssrTitle;
	}

	public void setSsrTitle(String ssrTitle) {
		this.ssrTitle = ssrTitle;
	}

	public String getPad() {
		return pad;
	}

	public void setPad(String pad) {
		this.pad = pad;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	public String getSeatNumbers() {
		return seatNumbers;
	}

	public void setSeatNumbers(String seatNumbers) {
		this.seatNumbers = seatNumbers;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getPaxReference() {
		return paxReference;
	}

	public void setPaxReference(String paxReference) {
		this.paxReference = paxReference;
	}

	public String getFFID() {
		return FFID;
	}

	public void setFFID(String fFID) {
		FFID = fFID;
	}

}
