package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

public class ResSegmentEticketInfoDTO implements Serializable {
	
	private static final long serialVersionUID = -4525153349807643231L;

	private String pnr;
	
	private int paxSequence;
	
	private String segmentCode;
	
	private Date departureDateZulu;
	
	private long eTicketNumber;
	
	private int segmentSequence;
	
	private String ticketStatus;
	
	private String segmentStatus;
	
	private String flightNumber;
	
	private String lccUniqueID;
	
	private Long transactionID;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	public void setDepartureDateZulu(Date departureDateZulu) {
		this.departureDateZulu = departureDateZulu;
	}

	public long geteTicketNumber() {
		return eTicketNumber;
	}

	public void seteTicketNumber(long eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}

	public int getSegmentSequence() {
		return segmentSequence;
	}

	public void setSegmentSequence(int segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getSegmentStatus() {
		return segmentStatus;
	}

	public void setSegmentStatus(String segmentStatus) {
		this.segmentStatus = segmentStatus;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getLccUniqueID() {
		return lccUniqueID;
	}

	public void setLccUniqueID(String lccUniqueID) {
		this.lccUniqueID = lccUniqueID;
	}

	public Long getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
}
