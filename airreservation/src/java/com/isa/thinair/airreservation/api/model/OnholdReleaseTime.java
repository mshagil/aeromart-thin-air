package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of OnholdReleaseTime
 * 
 * @author asiri
 * @hibernate.class table = "T_ONHOLD_RELEASE_TIME"
 * 
 */

public class OnholdReleaseTime extends Persistent {

	private static final long serialVersionUID = -5840520982318934L;

	private int relTimeId;

	private String ondCode;

	private String bookingCode;

	private char isDomestic;

	private String cabinClassCode;

	private String agentCode;

	private Integer startCutover;

	private Integer endCutover;

	private Integer releaseTime;

	private String moduleCode;

	private Integer ranking;

	private String relTimeWrt;
	
	private Integer fareRuleId;

	private String user;
	/**
	 * 
	 * @return
	 * @hibernate.property column = "OND_CODE"
	 * 
	 */
	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "BOOKING_CODE"
	 * 
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "IS_DOMESTIC"
	 * 
	 */
	public char getIsDomestic() {
		return isDomestic;
	}

	public void setIsDomestic(char isDomestic) {
		this.isDomestic = isDomestic;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 * 
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "AGENT_CODE"
	 * 
	 */
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "START_CUTOVER"
	 * 
	 */
	public Integer getStartCutover() {
		return startCutover;
	}

	public void setStartCutover(Integer startCutover) {
		this.startCutover = startCutover;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "END_CUTOVER"
	 * 
	 */
	public Integer getEndCutover() {
		return endCutover;
	}

	public void setEndCutover(Integer endCutover) {
		this.endCutover = endCutover;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "RELEASE_TIME"
	 * 
	 */
	public Integer getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(Integer releaseTime) {
		this.releaseTime = releaseTime;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MODULE_CODE"
	 * 
	 */
	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "RANKING"
	 * 
	 */
	public Integer getRanking() {
		return ranking;
	}

	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}

	/**
	 * 
	 * @return
	 * @hibernate.id column = "ONHOLD_RELEASE_TIME_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_ONHOLD_RELEASE_TIME"
	 * 
	 */
	public int getRelTimeId() {
		return relTimeId;
	}

	public void setRelTimeId(int relTimeId) {
		this.relTimeId = relTimeId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "REL_TIME_WRT"
	 * 
	 */
	public String getRelTimeWrt() {
		return relTimeWrt;
	}

	public void setRelTimeWrt(String relTimeWrt) {
		this.relTimeWrt = relTimeWrt;
	}
	
	/**
	 * 
	 * @return
	 * @hibernate.property column = "FARE_RULE_ID"
	 * 
	 */
	public Integer getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}
