package com.isa.thinair.airreservation.api.dto.seatmap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Byorn.
 * 
 *         Used for Modify Seats
 */
public class PassengerSeatingAssembler implements Serializable, IPassengerSeating {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3808293788357744983L;

	// pnrPaxId|pnrPaxFareId|flightSeatId
	private final Map<String, PaxSeatTO> paxSeatingInfo = new HashMap<String, PaxSeatTO>();

	// pnrPaxId|pnrPaxFareId|flightSegId
	private final Map<String, PaxSeatTO> paxSeatToAdd = new HashMap<String, PaxSeatTO>();
	private final Map<String, PaxSeatTO> paxSeatToRemove = new HashMap<String, PaxSeatTO>();

	private final Map<Integer, Integer> flightSeatIdPnrSegId = new HashMap<Integer, Integer>();
	private final Map<String, Collection<Integer>> statusAndFlightSeatIdMap = new HashMap<String, Collection<Integer>>();

	private final IPassenger passenger;
	private final boolean isForceConfirmed;

	private PaxAdjAssembler adjAssembler;

	public PassengerSeatingAssembler(IPassenger passenger, boolean isForceConfirmed) {
		this(passenger, null, isForceConfirmed);
	}

	public PassengerSeatingAssembler(IPassenger passenger, PaxAdjAssembler adjAssembler, boolean isForceConfirmed) {
		this.passenger = passenger;
		this.isForceConfirmed = isForceConfirmed;
		this.adjAssembler = adjAssembler;
	}

	public Map<String, Collection<Integer>> getStatusFlightSeatIdsMap() {
		return statusAndFlightSeatIdMap;
	}

	public Map<Integer, Integer> getFlightSeatIdPnrSegIdsMap() {
		return flightSeatIdPnrSegId;
	}

	/**
	 * Add to Seat
	 * 
	 * @throws ModuleException
	 */
	@Override
	public void addSeatInfo(Integer pnrPaxId, Integer pnrPaxFareId, Integer flightSeatId, Integer pnrSegId,
			BigDecimal chargeAmount, Integer autoCnxId, TrackInfoDTO trackInfo) throws ModuleException {

		PaxSeatTO paxSeatTO = new PaxSeatTO();
		paxSeatTO.setPnrPaxId(pnrPaxId);
		paxSeatTO.setPnrSegId(pnrSegId);
		paxSeatTO.setChgDTO(getExternalChgDTOForSeatMap(chargeAmount, trackInfo));
		paxSeatTO.setSelectedFlightSeatId(flightSeatId);
		paxSeatTO.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
		paxSeatTO.setPnrPaxFareId(pnrPaxFareId);
		if (chargeAmount != null
				&& AccelAeroCalculator.isGreaterThan(chargeAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			paxSeatTO.setAutoCancellationId(autoCnxId);
		}

		String uniqueId = SMUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId, flightSeatId);
		paxSeatingInfo.put(uniqueId, paxSeatTO);
		flightSeatIdPnrSegId.put(flightSeatId, pnrSegId);

		if (adjAssembler != null) {
			adjAssembler.addCharge(pnrPaxId, chargeAmount);
		}
	}

	/**
	 * Remove from Seat
	 * 
	 * @param autoCnxId
	 * 
	 * @throws ModuleException
	 */
	public void addPassengertoNewFlightSeat(Integer pnrPaxId, Integer pnrPaxFareId, Integer pnrSegId, Integer flightSeatId,
			BigDecimal chargeAmount, String seatCode, String paxType, Integer autoCnxId, TrackInfoDTO trackInfo)
			throws ModuleException {

		PaxSeatTO paxSeatTO = new PaxSeatTO();
		paxSeatTO.setChgDTO(getExternalChgDTOForSeatMap(chargeAmount, trackInfo));
		paxSeatTO.setPnrPaxId(pnrPaxId);
		paxSeatTO.setPnrSegId(pnrSegId);
		paxSeatTO.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
		paxSeatTO.setSelectedFlightSeatId(flightSeatId);
		paxSeatTO.setPnrPaxFareId(pnrPaxFareId);
		paxSeatTO.setSeatCode(seatCode);
		paxSeatTO.setPaxType(paxType);
		if (chargeAmount != null
				&& AccelAeroCalculator.isGreaterThan(chargeAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			paxSeatTO.setAutoCancellationId(autoCnxId);
		}
		String uniqueId = SMUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId, flightSeatId);
		paxSeatToAdd.put(uniqueId, paxSeatTO);

		putSeat(flightSeatId, AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);

		if (adjAssembler != null) {
			adjAssembler.removeCharge(pnrPaxId, chargeAmount);
		}

	}

	/**
	 * Remove from Seat
	 * 
	 * @throws ModuleException
	 */
	public void removePassengerFromFlightSeat(Integer pnrPaxId, Integer pnrPaxFareId, Integer pnrSegId, Integer flightSeatId,
			BigDecimal chargeAmount, TrackInfoDTO trackInfo) throws ModuleException {

		PaxSeatTO paxSeatTO = new PaxSeatTO();
		paxSeatTO.setPnrPaxId(pnrPaxId);
		paxSeatTO.setPnrSegId(pnrSegId);
		paxSeatTO.setChgDTO(getExternalChgDTOForSeatMap(chargeAmount.negate(), trackInfo));
		paxSeatTO.setSelectedFlightSeatId(flightSeatId);
		paxSeatTO.setPnrPaxFareId(pnrPaxFareId);
		paxSeatTO.setAutoCancellationId(null);
		String uniqueId = SMUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId, flightSeatId);

		paxSeatToRemove.put(uniqueId, paxSeatTO);
		putSeat(flightSeatId, AirinventoryCustomConstants.FlightSeatStatuses.VACANT);

	}

	private void putSeat(Integer flightSeatId, String status) {
		Collection<Integer> flightSeatIds = statusAndFlightSeatIdMap.get(status);
		if (flightSeatIds == null) {
			flightSeatIds = new ArrayList<Integer>();
			flightSeatIds.add(flightSeatId);
			statusAndFlightSeatIdMap.put(status, flightSeatIds);
		} else {
			flightSeatIds.add(flightSeatId);
		}
	}

	private ExternalChgDTO getExternalChgDTOForSeatMap(BigDecimal charge, TrackInfoDTO trackInfo) throws ModuleException {
		if (charge == null) {
			return null;
		}

		Collection<ReservationInternalConstants.EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SEAT_MAP);
		Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationModuleUtils
				.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, trackInfo, null);
		ExternalChgDTO externalChgDTO = (ExternalChgDTO) ((ExternalChgDTO) mapExternalChgs.get(EXTERNAL_CHARGES.SEAT_MAP))
				.clone();
		externalChgDTO.setAmount(charge);
		return externalChgDTO;
	}

	/**
	 * @return the paxSeatToAdd
	 */
	public Map<String, PaxSeatTO> getPaxSeatingInfo() {
		return paxSeatingInfo;
	}

	/**
	 * @return the paxSeatToAdd
	 */
	public Map<String, PaxSeatTO> getPaxSeatToAdd() {
		return paxSeatToAdd;
	}

	/**
	 * @return the paxSeatToRemove
	 */
	public Map<String, PaxSeatTO> getPaxSeatToRemove() {
		return paxSeatToRemove;
	}

	/**
	 * @return the passenger
	 */
	public IPassenger getPassenger() {
		return passenger;
	}

	/**
	 * @return the isForceConfirmed
	 */
	public boolean isForceConfirmed() {
		return isForceConfirmed;
	}
}
