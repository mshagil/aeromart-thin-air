package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;

public class NameValueDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8516931995422068394L;

	private String name;

	private String value;

	public NameValueDTO(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	public NameValueDTO() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
