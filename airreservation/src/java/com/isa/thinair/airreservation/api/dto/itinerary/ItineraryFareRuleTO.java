package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

/**
 * @author Dilan Anuruddha
 */
public class ItineraryFareRuleTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ondCode;

	private String fareCategoryDescription;

	private String fareRuleComments;

	private String segmentTravelExpiary;

	private String fareBasisCode;

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getFareCategoryDescription() {
		return fareCategoryDescription;
	}

	public void setFareCategoryDescription(String fareCategoryDescription) {
		this.fareCategoryDescription = fareCategoryDescription;
	}

	public String getFareRuleComments() {
		return fareRuleComments;
	}

	public void setFareRuleComments(String fareRuleComments) {
		this.fareRuleComments = fareRuleComments;
	}

	public String getSegmentTravelExpiary() {
		return segmentTravelExpiary;
	}

	public void setSegmentTravelExpiary(String segmentTravelExpiary) {
		this.segmentTravelExpiary = segmentTravelExpiary;
	}

	/**
	 * @return the fareBasisCode
	 */
	public String getFareBasisCode() {
		return fareBasisCode;
	}

	/**
	 * @param fareBasisCode
	 *            the fareBasisCode to set
	 */
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

}
