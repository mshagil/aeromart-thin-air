/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 * @version $Id$
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * CreditCardSales Sales is the entity class to repesent a CreditSales model
 * 
 * @author :byorn
 * @hibernate.class table = "T_CREDITCARD_SALES"
 */
public class CreditCardSalesFocus implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3968602299542524909L;
	private Integer id;
	private Date dateOfSale;
	private int cardTypeId;
	private BigDecimal totalDailySales = AccelAeroCalculator.getDefaultBigDecimalZero();
	private char transferStatus;
	private Date transferTimeStamp;

	/**
	 * returns the cardTypeId
	 * 
	 * @return Returns the cardTypeId
	 * @hibernate.property column = "CARD_TYPE_ID"
	 * 
	 */
	public int getCardTypeId() {
		return cardTypeId;
	}

	/**
	 * sets the cardTypeId
	 * 
	 * @param cardTypeId
	 *            The cardTypeId to set.
	 */
	public void setCardTypeId(int cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	/**
	 * returns the dateOfsales
	 * 
	 * @return Returns the dateOfsales.
	 * @hibernate.property column = "DATE_OF_SALE"
	 */
	public Date getDateOfSale() {
		return dateOfSale;
	}

	/**
	 * sets the dateofsales
	 * 
	 * @param dateOfSales
	 *            The dateOfSales to set.
	 */
	public void setDateOfSale(Date dateOfSale) {
		this.dateOfSale = dateOfSale;
	}

	/**
	 * returns the totalDailySales
	 * 
	 * @return Returns the totalDailySales
	 * @hibernate.property column = "TOTAL_DAILY_SALES"
	 */
	public BigDecimal getTotalDailySales() {
		return totalDailySales;
	}

	/**
	 * sets the totalDailySales
	 * 
	 * @param totalDailySales
	 *            The totalDailySales to set.
	 */
	public void setTotalDailySales(BigDecimal totalDailySales) {
		this.totalDailySales = totalDailySales;
	}

	/**
	 * returns the transferStatus
	 * 
	 * @return Returns the transferStatus
	 * @hibernate.property column = "TRANSFER_STATUS"
	 */
	public char getTransferStatus() {
		return transferStatus;
	}

	/**
	 * sets the transferStatus
	 * 
	 * @param transferStatus
	 *            The transferStatus to set.
	 */
	public void setTransferStatus(char transferStatus) {
		this.transferStatus = transferStatus;
	}

	/**
	 * returns the transferTimeStamp
	 * 
	 * @return Returns the transferTimeStamp
	 * @hibernate.property column = "TRANSFER_TIMESTAMP"
	 */
	public Date getTransferTimeStamp() {
		return transferTimeStamp;
	}

	/**
	 * sets the transferTimeStamp
	 * 
	 * @param transferTimeStamp
	 *            ThetransferTimeStamp.
	 */
	public void setTransferTimeStamp(Date transferTimeStamp) {
		this.transferTimeStamp = transferTimeStamp;
	}

	/**
	 * @return Returns the id.
	 * @hibernate.id column = "ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CREDITCARD_SALES"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}
}
