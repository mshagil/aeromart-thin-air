package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Nilindra Fernando
 * @since Oct 23, 2012
 * @hibernate.class table = "T_PNR_PROMOTION"
 */
public class PromotionAttribute extends Persistent implements Serializable {

	private Long promotionAttributeId;

	private String pnr;
	
	private String promotionType;

	private String attribute;

	private String value;

	/**
	 * @return the promotionAttributeId
	 * @hibernate.id column = "PNR_PROMOTION_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PNR_PROMOTION"
	 */
	public Long getPromotionAttributeId() {
		return promotionAttributeId;
	}

	/**
	 * @param promotionAttributeId
	 *            the promotionAttributeId to set
	 */
	public void setPromotionAttributeId(Long promotionAttributeId) {
		this.promotionAttributeId = promotionAttributeId;
	}

	/**
	 * @return the promotionType
	 * @hibernate.property column = "PROMOTION_TYPE"
	 */
	public String getPromotionType() {
		return promotionType;
	}

	/**
	 * @param promotionType
	 *            the promotionType to set
	 */
	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	/**
	 * @return the attribute
	 * @hibernate.property column = "PROMOTION_ATTRIBUTE"
	 */
	public String getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute
	 *            the attribute to set
	 */
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	/**
	 * @return the value
	 * @hibernate.property column = "VALUE"
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the pnr
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
