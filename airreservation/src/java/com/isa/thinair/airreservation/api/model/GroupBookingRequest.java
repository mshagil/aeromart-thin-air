package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Gayan
 * 
 * @hibernate.class table="t_group_booking_request"
 */
public class GroupBookingRequest extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6765219761980057897L;

	private Long requestID;

	private Date requestDate;
	private Integer adultAmount;
	private Integer childAmount;
	private Integer infantAmount;
	private String oalFare;
	private BigDecimal requestedFare = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal agreedFare = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal minPaymentRequired = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal paymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private Set<GroupBookingReqRoutes> groupBookingRoutes;
	private Date createdDate;
	private String craeteUserCode;
	private String approvalUserCode;
	private Date approvalDate;
	private String agentComments;
	private String approvalComments;
	private Long mstGroupBookingRequestId;

	/**
	 * status =
	 * [["0","Pending Approval"],["1","Approve"],["2","Reject"],["3","Re-Quote"],["4","Pending after Re-Quote"],
	 * ["5","Request Release Seat"],["6","Withdraw"],["7","Booked"]]
	 */
	private int statusID;
	private Date targetPaymentDate;
	private Date fareValidDate;
	private Set<GroupRequestStation> groupStations;

	private Reservation reservation;
	private String stationCode;
	private String userName;
	private String onds;
	private String requestedAgentCode;

	private Set<GroupBookingRequest> subRequests;

	private boolean isApprovedPercentage;

	/**
	 * @hibernate.id column = "GROUP_BOOKING_REQUEST_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="s_group_booking_request"
	 */
	public Long getRequestID() {
		return requestID;
	}

	public void setRequestID(Long requestID) {
		this.requestID = requestID;
	}

	/**
	 * @hibernate.property column= "requested_date"
	 * @return requestDate
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * @hibernate.property column="CHILD_AMOUNT"
	 * @return childAmount
	 */
	public Integer getChildAmount() {
		return childAmount;
	}

	public void setChildAmount(Integer childAmount) {
		this.childAmount = childAmount;
	}

	/**
	 * @hibernate.property column="ADULT_AMOUNT"
	 * @return adultAmount
	 */
	public Integer getAdultAmount() {
		return adultAmount;
	}

	public void setAdultAmount(Integer adultAmount) {
		this.adultAmount = adultAmount;
	}

	/**
	 * @hibernate.property column="INFANT_AMOUNT"
	 * @return
	 */
	public Integer getInfantAmount() {
		return infantAmount;
	}

	public void setInfantAmount(Integer infantAmount) {
		this.infantAmount = infantAmount;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-key column="GROUP_BOOKING_REQUEST_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.GroupBookingReqRoutes"
	 */
	public Set<GroupBookingReqRoutes> getGroupBookingRoutes() {
		return groupBookingRoutes;
	}

	public void setGroupBookingRoutes(Set<GroupBookingReqRoutes> list) {
		this.groupBookingRoutes = list;
	}

	/**
	 * @hibernate.property column="oal_fare"
	 * @return oalFare
	 */
	public String getOalFare() {
		return oalFare;
	}

	public void setOalFare(String oalFare) {
		this.oalFare = oalFare;
	}

	/**
	 * @hibernate.property column="requested_fare"
	 * @return requestedFare
	 */
	public BigDecimal getRequestedFare() {
		return requestedFare;
	}

	public void setRequestedFare(BigDecimal requestedFare) {
		this.requestedFare = requestedFare;
	}

	/**
	 * @hibernate.property column="agreed_fare"
	 * @return agreedFare
	 */
	public BigDecimal getAgreedFare() {
		return agreedFare;
	}

	public void setAgreedFare(BigDecimal agreedFare) {
		this.agreedFare = agreedFare;
	}

	/**
	 * @hibernate.property column="min_payment_required"
	 * @return minPaymentRequired
	 */
	public BigDecimal getMinPaymentRequired() {
		return minPaymentRequired;
	}

	public void setMinPaymentRequired(BigDecimal minPaymentRequired) {
		this.minPaymentRequired = minPaymentRequired;
	}

	/**
	 * @hibernate.property column="CREATED_DATE"
	 * @return createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @hibernate.property column="CREATED_BY"
	 * @return craeteUserCode
	 */
	public String getCraeteUserCode() {
		return craeteUserCode;
	}

	public void setCraeteUserCode(String craeteUserCode) {
		this.craeteUserCode = craeteUserCode;
	}

	/**
	 * @hibernate.property column="approved_user_id"
	 * @return approvalUserCode
	 */
	public String getApprovalUserCode() {
		return approvalUserCode;
	}

	public void setApprovalUserCode(String approvalUserCode) {
		this.approvalUserCode = approvalUserCode;
	}

	/**
	 * @hibernate.property column="approval_date"
	 * @return approvalDate
	 */
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	/**
	 * @hibernate.property column="agent_comments"
	 * @return agentComments
	 */
	public String getAgentComments() {
		return agentComments;
	}

	public void setAgentComments(String agentComments) {
		this.agentComments = agentComments;
	}

	/**
	 * @hibernate.property column="approval_comments"
	 * @return approvalComments
	 */
	public String getApprovalComments() {
		return approvalComments;
	}

	public void setApprovalComments(String approvalComments) {
		this.approvalComments = approvalComments;
	}

	/**
	 * @hibernate.property column="status"
	 * @return statusID
	 */
	public int getStatusID() {
		return statusID;
	}

	public void setStatusID(int statusID) {
		this.statusID = statusID;
	}

	/**
	 * @hibernate.property column="target_payment_date"
	 * @return targetPaymentDate
	 */
	public Date getTargetPaymentDate() {
		return targetPaymentDate;
	}

	public void setTargetPaymentDate(Date targetPaymentDate) {
		this.targetPaymentDate = targetPaymentDate;
	}

	/**
	 * @hibernate.property column="fare_valid_date"
	 * @return fareValidDate
	 */
	public Date getFareValidDate() {
		return fareValidDate;
	}

	public void setFareValidDate(Date fareValidDate) {
		this.fareValidDate = fareValidDate;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-key column="GROUP_BOOKING_REQUEST_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.GroupRequestStation"
	 */
	public Set<GroupRequestStation> getGroupStations() {
		return groupStations;
	}

	public void setGroupStations(Set<GroupRequestStation> groupStations) {
		this.groupStations = groupStations;
	}

	/**
	 * @hibernate.property column="PAYMENT_AMOUNT"
	 * @return paymentAmount
	 */
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @hibernate.property column="STATION_CODE"
	 * @return stationCode
	 */
	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOnds() {
		return onds;
	}

	public void setOnds(String onds) {
		this.onds = onds;
	}

	/**
	 * @return the subRequests
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-key column="MST_GROUP_BOOKING_REQUEST_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.GroupBookingRequest"
	 */
	public Set<GroupBookingRequest> getSubRequests() {
		return subRequests;
	}

	/**
	 * @param subRequests
	 *            the subRequests to set
	 */
	public void setSubRequests(Set<GroupBookingRequest> subRequests) {
		this.subRequests = subRequests;
	}

	/**
	 * @hibernate.property column="MST_GROUP_BOOKING_REQUEST_ID" update="false" insert ="false"
	 * @return mstGroupBookingRequestId
	 */
	public Long getMstGroupBookingRequestId() {
		return mstGroupBookingRequestId;
	}

	public void setMstGroupBookingRequestId(Long mstGroupBookingRequestId) {
		this.mstGroupBookingRequestId = mstGroupBookingRequestId;
	}

	/**
	 * @hibernate.property column="REQUESTED_AGENT"
	 * @return requestedAgentCode
	 */

	public String getRequestedAgentCode() {
		return requestedAgentCode;
	}

	public void setRequestedAgentCode(String requestedAgentCode) {
		this.requestedAgentCode = requestedAgentCode;
	}

	/**
	 * @hibernate.property column = "IS_PERCENTAGE" type = "yes_no"
	 * @return the isApprovedPercentage
	 */
	public boolean isApprovedPercentage() {
		return isApprovedPercentage;
	}

	/**
	 * @param isApprovedPercentage
	 *            the isApprovedPercentage to set
	 */
	public void setApprovedPercentage(boolean isApprovedPercentage) {
		this.isApprovedPercentage = isApprovedPercentage;
	}

}
