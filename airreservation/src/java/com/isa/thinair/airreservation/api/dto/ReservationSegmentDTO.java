/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.FlightType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * To hold reservation segment data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationSegmentDTO implements Serializable, Comparable<ReservationSegmentDTO> {

	private static final long serialVersionUID = -8105558386371192804L;

	/** Holds flight segment id */
	private Integer flightSegId;

	/** Holds flight segment baggage allowance */
	private Integer flightSegBagAllowance;

	/** Holds the flight Segment Status */
	private String flightSegStatus;

	/** Holds reservation segment sequence */
	private Integer segmentSeq;

	/** Holds reservation segment return flag */
	private String returnFlag;

	/** Holds flight number */
	private String flightNo;

	/** Holds departure date */
	private Date departureDate;

	/** Holds arrival date */
	private Date arrivalDate;

	/** Holds zulu departure date */
	private Date zuluDepartureDate;

	/** Holds zulu arrival date */
	private Date zuluArrivalDate;

	/** Holds segment code */
	private String segmentCode;

	/** Holds Segment description */
	private String segmentDescription;

	/** Holds reservation segment status */
	private String status;

	/** Holds the reservation segment display status */
	private String displayStatus;

	/** Holds the pnr */
	private String pnr;

	/** Holds reservation segment id */
	private Integer pnrSegId;

	/** Holds reservation fare group id */
	private Integer fareGroupId;

	/** Holds the flight id */
	private Integer flightId;

	/** Holds the flight status */
	private String flightStatus;

	/** Holds the alert id */
	private int alertFlag;

	/** Holds the booking type */
	private String bookingType;

	/** Holds the cabin class code */
	private String cabinClassCode;

	/** Holds the logical cabin class code */
	private String logicalCCCode;

	/** Holds whether or not to shown in the itinerary */
	private boolean shownInItinerary;

	/** Holds the fareTO */
	private FareTO fareTO;

	/** Holds Flight Operation Time */
	private Integer operationTypeID;

	/** Holds the segment expiry date [Loads via Itineray Only] */
	private Date segmentExpiryDateZulu;

	/** Holds the segment expiry date in local with the airport */
	private Date segmentExpiryDateLocal;

	/** Holds the return group id */
	private Integer returnGroupId;

	/** Holds the open return confirm before Zulu */
	private Date openRtConfirmBeforeZulu;

	/** Holds the open return confirm before Local */
	private Date openRtConfirmBeforeLocal;

	/** Holds the return ond group id */
	private Integer returnOndGroupId;

	/** Holds the Ground Segment Code if the segment is a Ground Segment */
	private String subStationShortName;

	private Integer groundStationPnrSegmentID;

	private String externalReference;

	/** The arrival terminal name. */
	private String arrivalTerminalName;

	/** The departure terminal name. */
	private String departureTerminalName;

	/** The check in Date */
	private Date checkInDate;

	/** Channel code of the last status modification */
	private Integer statusModifiedChannelCode;

	/** The date of the status modification */
	private Date statusModifiedDate;

	/** The marketing carrier code */
	private String marketingCarrierCode;

	/** The marketing agent code */
	private String marketingAgentCode;

	/** The marketing station code */
	private String marketingStationCode;

	/** Flight type DOM orINT */
	private String flightType;

	private String flightModelNumber;

	private String flightModelDescription;

	private String flightDuration;

	private String remarks;

	private Date lastFareQuoteDateZulu;

	/** Holds the baggage ond group id */
	private String baggageOndGroupId;

	private Date ticketValidTill;

	private Integer journeySeq;

	private String subStatus;

	private boolean alertAutoCancellation;

	private String modifiedFrom;

	private Integer flexiRuleID;

	private Integer bundledFarePeriodId;

	private String origin;

	private String destination;

	private String bookingClass;
	
	private String 	codeShareFlightNo;
	
	private String 	codeShareBc;
	
	private String csOcCarrierCode;
	
	private String csOcFlightNumber;
	
	private boolean flownSegment;

	public ReservationSegmentDTO() {

	}

	public ReservationSegmentDTO(int flightSegId, int segmentSeq, Integer ondGroupId, Integer returnOndGroupId,
			int journeySequence) {
		this.flightSegId = flightSegId;
		this.segmentSeq = segmentSeq;
		this.fareGroupId = ondGroupId;
		this.returnOndGroupId = returnOndGroupId;
		this.journeySeq = journeySequence;
	}

	/**
	 * @return Returns the departureDate.
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            The departureDate to set.
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the flightNo.
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            The flightNo to set.
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return Returns the segmentCode.
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the flightSegId.
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightSegId
	 *            The flightSegId to set.
	 */
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public String getFlightSegStatus() {
		return flightSegStatus;
	}

	public void setFlightSegStatus(String flightSegStatus) {
		this.flightSegStatus = flightSegStatus;
	}

	/**
	 * @return Returns the returnFlag.
	 */
	public String getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            The returnFlag to set.
	 */
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return Returns the segmentSeq.
	 */
	public Integer getSegmentSeq() {
		return segmentSeq;
	}

	/**
	 * @param segmentSeq
	 *            The segmentSeq to set.
	 */
	public void setSegmentSeq(Integer segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the arrivalDate.
	 */
	public Date getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * @param arrivalDate
	 *            The arrivalDate to set.
	 */
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * @return Returns the pnrSegId.
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            The pnrSegId to set.
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return Returns the fareGroupId.
	 */
	public Integer getFareGroupId() {
		return fareGroupId;
	}

	/**
	 * @param fareGroupId
	 *            The fareGroupId to set.
	 */
	public void setFareGroupId(Integer fareGroupId) {
		this.fareGroupId = fareGroupId;
	}

	/**
	 * @return Returns the flightId.
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * Return departure date in a format
	 * 
	 * @param dateFormat
	 * @return
	 */
	public String getDepartureStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getDepartureDate(), dateFormat);
	}

	/**
	 * Return arrival date in a format
	 * 
	 * @param dateFormat
	 * @return
	 */
	public String getArrivalStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getArrivalDate(), dateFormat);
	}

	/**
	 * Return the check in date
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Date getCheckInDate() throws ModuleException {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	/**
	 * Return the check in date in the given format
	 * 
	 * @param dateFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getCheckInStringDate(String dateFormat) throws ModuleException {
		return BeanUtils.parseDateFormat(this.getCheckInDate(), dateFormat);
	}

	/**
	 * @return Returns the alertFlag.
	 */
	public int getAlertFlag() {
		return alertFlag;
	}

	/**
	 * @param alertFlag
	 *            The alertFlag to set.
	 */
	public void setAlertFlag(int alertFlag) {
		this.alertFlag = alertFlag;
	}

	/**
	 * @return Returns the zuluArrivalDate.
	 */
	public Date getZuluArrivalDate() {
		return zuluArrivalDate;
	}

	/**
	 * @param zuluArrivalDate
	 *            The zuluArrivalDate to set.
	 */
	public void setZuluArrivalDate(Date zuluArrivalDate) {
		this.zuluArrivalDate = zuluArrivalDate;
	}

	/**
	 * @return Returns the zuluDepartureDate.
	 */
	public Date getZuluDepartureDate() {
		return zuluDepartureDate;
	}

	/**
	 * @param zuluDepartureDate
	 *            The zuluDepartureDate to set.
	 */
	public void setZuluDepartureDate(Date zuluDepartureDate) {
		this.zuluDepartureDate = zuluDepartureDate;
	}

	/**
	 * Compare zulu departure date time
	 * 
	 * @param o
	 * @return 1, 0, -1
	 */
	public int compareTo(ReservationSegmentDTO reservationSegmentDTO) {
		return this.getZuluDepartureDate().compareTo(reservationSegmentDTO.getZuluDepartureDate());
	}

	/**
	 * @return Returns the segmentDescription.
	 */
	public String getSegmentDescription() {
		return segmentDescription;
	}

	/**
	 * @param segmentDescription
	 *            The segmentDescription to set.
	 */
	public void setSegmentDescription(String segmentDescription) {
		this.segmentDescription = segmentDescription;
	}

	/**
	 * Returns the segment is a normal segment or not
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public boolean isNormalSegment() throws ModuleException {
		if (!BookingClassUtil.byPassSegInvUpdate(this.getBookingType())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the segment is a stand by segment or not
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public boolean isStandBySegment() throws ModuleException {
		if (BookingClass.BookingClassType.STANDBY.equals(this.getBookingType())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the segment is a open return segment or not
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public boolean isOpenReturnSegment() throws ModuleException {
		if (BookingClass.BookingClassType.OPEN_RETURN.equals(this.getBookingType())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the bookingType.
	 * @throws ModuleException
	 */
	public String getBookingType() throws ModuleException {
		if (this.bookingType == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}

		return this.bookingType;
	}

	/**
	 * @param bookingType
	 *            The bookingType to set.
	 */
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	/**
	 * @return Returns the shownInItinerary.
	 */
	public boolean isShownInItinerary() {
		return shownInItinerary;
	}

	/**
	 * @param shownInItinerary
	 *            The shownInItinerary to set.
	 */
	public void setShownInItinerary(boolean shownInItinerary) {
		this.shownInItinerary = shownInItinerary;
	}

	/**
	 * @return Returns the cabinClassCode.
	 * @throws ModuleException
	 */
	public String getCabinClassCode() throws ModuleException {
		if (this.cabinClassCode == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}

		return this.cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return Returns the fareTO.
	 * @throws ModuleException
	 */
	public FareTO getFareTO() throws ModuleException {
		if (this.fareTO == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}
		return fareTO;
	}

	/**
	 * @param fareTO
	 *            The fareTO to set.
	 */
	public void setFareTO(FareTO fareTO) {
		this.fareTO = fareTO;
	}

	/**
	 * Returns the to string representation of this object
	 */
	public String toSimpleString() {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append(this.getFlightNo() + " " + this.getSegmentCode() + " "
				+ BeanUtils.parseDateFormat(this.getDepartureDate(), "E, dd MMM yyyy HH:mm:ss") + " / "
				+ BeanUtils.parseDateFormat(this.getArrivalDate(), "E, dd MMM yyyy HH:mm:ss"));

		return stringBuilder.toString();
	}

	/**
	 * Returns the segment status to use in the itinerary
	 * 
	 * @throws ModuleException
	 */
	public String getEffectiveStatus(String reservationStatus) throws ModuleException {
		String status = "";
		if (BookingClass.BookingClassType.STANDBY.equals(getBookingType())) {
			status = ReservationInternalConstants.ReservationStatus.STAND_BY;
		} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)) {
			status = ReservationInternalConstants.ReservationStatus.ON_HOLD;
		} else {
			status = getStatus();
		}
		return status;
	}

	/**
	 * Returns the carrier code derived from the flight number
	 */
	public String getCarrierCode() {
		return AppSysParamsUtil.extractCarrierCode(getFlightNo());
	}

	/**
	 * @return the operationTypeID
	 */
	public Integer getOperationTypeID() {
		return operationTypeID;
	}

	/**
	 * @param operationTypeID
	 *            the operationTypeID to set
	 */
	public void setOperationTypeID(Integer operationTypeID) {
		this.operationTypeID = operationTypeID;
	}

	/**
	 * Returns the segment expiry date
	 * 
	 * @param dateFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getSegmentExpiryDate(String dateFormat) {
		String airportCode = getSegmentCode().split("/")[0];
		return BeanUtils.parseDateFormat(this.getSegmentExpiryDateLocal(), dateFormat) + " " + airportCode;
	}

	/**
	 * @return the returnGroupId
	 * @throws ModuleException
	 */
	public Integer getReturnGroupId() throws ModuleException {
		if (this.returnGroupId == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}

		return returnGroupId;
	}

	/**
	 * @param returnGroupId
	 *            the returnGroupId to set
	 */
	public void setReturnGroupId(Integer returnGroupId) {
		this.returnGroupId = returnGroupId;
	}

	/**
	 * Open return confirm before
	 * 
	 * @param dateFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getOpenRtConfirmBefore(String dateFormat) throws ModuleException {
		String airportCode = getSegmentCode().split("/")[0];
		return BeanUtils.parseDateFormat(this.getOpenRtConfirmBeforeLocal(), dateFormat) + " " + airportCode;
	}

	/**
	 * @return the openRtConfirmBeforeZulu
	 */
	public Date getOpenRtConfirmBeforeZulu() {
		return openRtConfirmBeforeZulu;
	}

	/**
	 * @param openRtConfirmBeforeZulu
	 *            the openRtConfirmBeforeZulu to set
	 */
	public void setOpenRtConfirmBeforeZulu(Date openRtConfirmBeforeZulu) {
		this.openRtConfirmBeforeZulu = openRtConfirmBeforeZulu;
	}

	/**
	 * @return the openRtConfirmBeforeLocal
	 */
	public Date getOpenRtConfirmBeforeLocal() {
		return openRtConfirmBeforeLocal;
	}

	/**
	 * @param openRtConfirmBeforeLocal
	 *            the openRtConfirmBeforeLocal to set
	 */
	public void setOpenRtConfirmBeforeLocal(Date openRtConfirmBeforeLocal) {
		this.openRtConfirmBeforeLocal = openRtConfirmBeforeLocal;
	}

	/**
	 * @return the segmentExpiryDateZulu
	 */
	public Date getSegmentExpiryDateZulu() {
		return segmentExpiryDateZulu;
	}

	/**
	 * @param segmentExpiryDateZulu
	 *            the segmentExpiryDateZulu to set
	 */
	public void setSegmentExpiryDateZulu(Date segmentExpiryDateZulu) {
		this.segmentExpiryDateZulu = segmentExpiryDateZulu;
	}

	/**
	 * @return the segmentExpiryDateLocal
	 */
	public Date getSegmentExpiryDateLocal() {
		return segmentExpiryDateLocal;
	}

	/**
	 * @param segmentExpiryDateLocal
	 *            the segmentExpiryDateLocal to set
	 */
	public void setSegmentExpiryDateLocal(Date segmentExpiryDateLocal) {
		this.segmentExpiryDateLocal = segmentExpiryDateLocal;
	}

	/**
	 * @return Returns the returnOndGroupId.
	 */
	public Integer getReturnOndGroupId() {
		return returnOndGroupId;
	}

	/**
	 * @param returnOndGroupId
	 *            The returnOndGroupId to set.
	 */
	public void setReturnOndGroupId(Integer returnOndGroupId) {
		this.returnOndGroupId = returnOndGroupId;
	}

	/**
	 * @return
	 */
	public String getOrigin() {
		if (origin != null) {
			return origin;
		} else {
			return segmentCode.substring(0, 3);
		}
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}


	/**
	 * @return
	 */
	public String getDestination() {
		if (destination != null) {
			return destination;
		} else {
			return segmentCode.substring(segmentCode.length() - 3);
		}
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSubStationShortName() {
		return subStationShortName;
	}

	public void setSubStationShortName(String subStationShortName) {
		this.subStationShortName = subStationShortName;
	}

	public Integer getGroundStationPnrSegmentID() {
		return groundStationPnrSegmentID;
	}

	public void setGroundStationPnrSegmentID(Integer groundStationPnrSegmentID) {
		this.groundStationPnrSegmentID = groundStationPnrSegmentID;
	}

	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @return The arrival terminal name for the segment.
	 */
	public String getArrivalTerminalName() {
		return arrivalTerminalName;
	}

	/**
	 * @param arrivalTerminalName
	 *            : the arrival terminal name for the segment to set.
	 */
	public void setArrivalTerminalName(String arrivalTerminalName) {
		this.arrivalTerminalName = arrivalTerminalName;
	}

	/**
	 * @return the departure terminal name for the segment.
	 */
	public String getDepartureTerminalName() {
		return departureTerminalName;
	}

	/**
	 * @param departureTerminalName
	 *            : The departure terminal name to set.
	 */
	public void setDepartureTerminalName(String departureTerminalName) {
		this.departureTerminalName = departureTerminalName;
	}

	/**
	 * @return the status modified channel code.
	 */
	public Integer getStatusModifiedChannelCode() {
		return statusModifiedChannelCode;
	}

	/**
	 * @param statusModifiedChannelCode
	 *            : the status modified channel code to set.
	 */
	public void setStatusModifiedChannelCode(Integer statusModifiedChannelCode) {
		this.statusModifiedChannelCode = statusModifiedChannelCode;
	}

	/**
	 * @return : The status modified date.
	 */
	public Date getStatusModifiedDate() {
		return statusModifiedDate;
	}

	/**
	 * @param statusModifiedDate
	 *            : The status modified date to set.
	 */
	public void setStatusModifiedDate(Date statusModifiedDate) {
		this.statusModifiedDate = statusModifiedDate;
	}

	/**
	 * @return : The marketing carrier code.
	 */
	public String getMarketingCarrierCode() {
		return marketingCarrierCode;
	}

	/**
	 * @param marketingCarrierCode
	 *            : The marketing carrier code to set.
	 */
	public void setMarketingCarrierCode(String marketingCarrierCode) {
		this.marketingCarrierCode = marketingCarrierCode;
	}

	/**
	 * @return : The marketing agent code.
	 */
	public String getMarketingAgentCode() {
		return marketingAgentCode;
	}

	/**
	 * @param marketingAgentCode
	 *            : The marketing agent code to set.
	 */
	public void setMarketingAgentCode(String marketingAgentCode) {
		this.marketingAgentCode = marketingAgentCode;
	}

	/**
	 * @return The marketing station code.
	 */
	public String getMarketingStationCode() {
		return marketingStationCode;
	}

	/**
	 * @param marketingStationCode
	 *            : The marketing station code to set.
	 */
	public void setMarketingStationCode(String marketingStationCode) {
		this.marketingStationCode = marketingStationCode;
	}

	/**
	 * @return
	 */
	public String getFlightType() {
		return flightType;
	}

	/**
	 * @param flightType
	 */
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public boolean isInternationalFlight() {
		return getFlightType().equalsIgnoreCase(FlightType.INTERNATIONL.getType());
	}

	public String getFlightModelNumber() {
		return flightModelNumber;
	}

	public void setFlightModelNumber(String flightModelNumber) {
		this.flightModelNumber = flightModelNumber;
	}

	public String getFlightModelDescription() {
		return flightModelDescription;
	}

	public void setFlightModelDescription(String flightModelDescription) {
		this.flightModelDescription = flightModelDescription;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	/**
	 * @return the displayStatus
	 */
	public String getDisplayStatus() {
		return displayStatus;
	}

	/**
	 * @param displayStatus
	 *            the displayStatus to set
	 */
	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	public void setLastFareQuoteDateZulu(Date lastFareQuoteDateZulu) {
		this.lastFareQuoteDateZulu = lastFareQuoteDateZulu;
	}

	public Date getLastFareQuoteDateZulu() {
		return lastFareQuoteDateZulu;
	}

	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	public Date getTicketValidTill() {
		return ticketValidTill;
	}

	public Integer getJourneySeq() {
		return journeySeq;
	}

	public void setJourneySeq(Integer journeySeq) {
		this.journeySeq = journeySeq;
	}

	public Integer getFlightSegBagAllowance() {
		return flightSegBagAllowance;
	}

	public void setFlightSegBagAllowance(Integer flightSegBagAllowance) {
		this.flightSegBagAllowance = flightSegBagAllowance;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	/**
	 * @return the baggageOndGroupId
	 */
	public String getBaggageOndGroupId() {
		return baggageOndGroupId;
	}

	/**
	 * @param baggageOndGroupId
	 *            the baggageOndGroupId to set
	 */
	public void setBaggageOndGroupId(String baggageOndGroupId) {
		this.baggageOndGroupId = baggageOndGroupId;
	}

	/**
	 * @return the modifiedFrom
	 */
	public String getModifiedFrom() {
		return modifiedFrom;
	}

	/**
	 * @param modifiedFrom
	 *            the modifiedFrom to set
	 */
	public void setModifiedFrom(String modifiedFrom) {
		this.modifiedFrom = modifiedFrom;
	}

	/**
	 * @return the flexiRuleID
	 */
	public Integer getFlexiRuleID() {
		return flexiRuleID;
	}

	/**
	 * @param flexiRuleID
	 *            the flexiRuleID to set
	 */
	public void setFlexiRuleID(Integer flexiRuleID) {
		this.flexiRuleID = flexiRuleID;
	}

	public Integer getBundledFarePeriodId() {
		return bundledFarePeriodId;
	}

	public void setBundledFarePeriodId(Integer bundledFarePeriodId) {
		this.bundledFarePeriodId = bundledFarePeriodId;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getCodeShareFlightNo() {
		return codeShareFlightNo;
	}

	public String getCodeShareBc() {
		return codeShareBc;
	}

	public void setCodeShareFlightNo(String codeShareFlightNo) {
		this.codeShareFlightNo = codeShareFlightNo;
	}

	public void setCodeShareBc(String codeShareBc) {
		this.codeShareBc = codeShareBc;
	}
	
	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}

	public String getCsOcFlightNumber() {
		return csOcFlightNumber;
	}

	public void setCsOcFlightNumber(String csOcFlightNumber) {
		this.csOcFlightNumber = csOcFlightNumber;
	}

	public boolean isFlownSegment() {
		return flownSegment;
	}

	public void setFlownSegment(boolean flownSegment) {
		this.flownSegment = flownSegment;
	}
	
}
