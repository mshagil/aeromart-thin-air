package com.isa.thinair.airreservation.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * The model class representing a terms and conditions template. This model is not designed to create new objects.
 * Rather the purpose is to edit objects that are initially inserted into the db manually. equals and hashCode methods
 * are overridden with this purpose in mind (only considering the ID field).
 * 
 * @author thihara
 * 
 * @hibernate.class table = "T_TERMS_TEMPLATE"
 */
public class TermsTemplate extends Persistent {

	private static final long serialVersionUID = 14654678945612347L;

	private Integer templateID;

	private String templateName;

	private String carriers;

	private String templateContent;

	private String languageCode;

	private Set<TermsTemplateAudit> auditHistory;

	/**
	 * @return The template ID
	 * @hibernate.id column = "TERMS_TEMPLATE_ID" generator-class = "assigned"
	 */
	public Integer getTemplateID() {
		return templateID;
	}

	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	/**
	 * @return The name of the terms and condition template. The name is unique.
	 * 
	 * @hibernate.property column = "TERMS_TEMPLATE_NAME "
	 */
	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return The carrier code this terms and conditions apply to.
	 * 
	 * @hibernate.property column = "CARRIERS"
	 */
	public String getCarriers() {
		return carriers;
	}

	public void setCarriers(String carriers) {
		this.carriers = carriers;
	}

	/**
	 * @return Terms and conditions stored in the database.
	 * 
	 * @hibernate.property column = "TERMS_TEMPLATE_CONTENT"
	 */
	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	/**
	 * @return The language code of the template.
	 * 
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return
	 * 
	 * @hibernate.set lazy="false" cascade="all" order-by="MODIFIED_DATE" inverse="true"
	 * @hibernate.collection-key column="TERMS_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.TermsTemplateAudit"
	 */
	public Set<TermsTemplateAudit> getAuditHistory() {
		return auditHistory;
	}

	public void setAuditHistory(Set<TermsTemplateAudit> auditHistory) {
		this.auditHistory = auditHistory;
	}

	/**
	 * Ignoring super class hashCode implementation. Reflective hashCode is bad for everyone's health. This class is
	 * inserted to the database manually. So ID is always guaranteed to be present. Therefore only ID field is
	 * considered.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((templateID == null) ? 0 : templateID.hashCode());
		return result;
	}

	/**
	 * Ignoring super class equals implementation. Reflective equals is bad for everyone's health. This class is
	 * inserted to the database manually. So ID is always guaranteed to be present. Therefore only ID field is
	 * considered.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof TermsTemplate)) {
			return false;
		}
		TermsTemplate other = (TermsTemplate) obj;
		if (templateID == null) {
			if (other.templateID != null) {
				return false;
			}
		} else if (!templateID.equals(other.templateID)) {
			return false;
		}
		return true;
	}
}
