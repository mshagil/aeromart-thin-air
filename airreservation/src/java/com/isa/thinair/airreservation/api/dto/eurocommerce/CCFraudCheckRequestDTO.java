package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds the CC fraud check transfer data. TODO this dto should be generalize later to be used for on account payment as
 * well.
 * 
 * @author mekanayake
 * 
 */
public class CCFraudCheckRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5051314762834514657L;

	private String paymentmethodCode;

	private String externalRef;

	private String externalId;

	private BigDecimal amount;

	private String originalCurrencyCode;

	private String accountNumber;

	private int expirationMonth;

	private int expirationYear;

	private List<DocumentSnapInDTO> documentSnapIns;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getOriginalCurrencyCode() {
		return originalCurrencyCode;
	}

	public void setOriginalCurrencyCode(String originalCurrencyCode) {
		this.originalCurrencyCode = originalCurrencyCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getExpirationMonth() {
		return expirationMonth;
	}

	public void setExpirationMonth(int expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	public int getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(int expirationYear) {
		this.expirationYear = expirationYear;
	}

	public String getExternalRef() {
		return externalRef;
	}

	public void setExternalRef(String externalRef) {
		this.externalRef = externalRef;
	}

	public List<DocumentSnapInDTO> getDocumentSnapInDto() {
		if (documentSnapIns == null) {
			documentSnapIns = new ArrayList<DocumentSnapInDTO>();
		}
		return documentSnapIns;
	}

	public void setDocumentSnapInDto(List<DocumentSnapInDTO> documentSnapIn) {
		this.documentSnapIns = documentSnapIn;
	}

	public void addDocumentSnapInDto(DocumentSnapInDTO documentSnapIn) {
		getDocumentSnapInDto().add(documentSnapIn);
	}

	public ItineraryDocumentDTO getItineraryDocumentSnapIn() {
		for (DocumentSnapInDTO snapIn : documentSnapIns) {
			if (snapIn instanceof ItineraryDocumentDTO) {
				return (ItineraryDocumentDTO) snapIn;
			}
		}
		return null;
	}

	public List<NameValueDocumentDTO> getNameValueDocumentSnapIn() {
		List<NameValueDocumentDTO> list = new ArrayList<NameValueDocumentDTO>();
		for (DocumentSnapInDTO snapIn : documentSnapIns) {
			if (snapIn instanceof NameValueDocumentDTO) {
				list.add((NameValueDocumentDTO) snapIn);
			}
		}
		return list;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getPaymentmethodCode() {
		return paymentmethodCode;
	}

	public void setPaymentmethodCode(String paymentmethodCode) {
		this.paymentmethodCode = paymentmethodCode;
	}

	@Override
	public String toString() {
		return "ProcessPaymentRequestDTO [accountNumber=" + accountNumber + ", amount=" + amount + ", expirationMonth="
				+ expirationMonth + ", expirationYear=" + expirationYear + "]";
	}

}
