/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.emirates;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Holds Passenger final sales Meta data transfer information
 * 
 * @author Byorn de Silva
 * @since 1.0
 */
public class EMPNLMetaDataDTO {

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the departure day e.g. 02 */
	private String day;

	/** Holds the departure month e.g. DEC */
	private String month;

	/** Holds the from airport */
	private String boardingAirport;

	/** Holds the part number */
	private String partNumber;

	/** Holds the day and month e.g 02DEC */
	private String dayMonth;

	/** Hold the pnlContent */
	private String pnlContent;

	/** Hold the date downloaded */
	private Date dateDownloaded;

	/** Hold the from address of Pfs */
	private String fromAddress;

	/** Hold the destination airport **/
	private String destinationAirport;

	/** Hold the Number of passengers Flown **/
	private String totNumberOfPax;

	private String desAirportNumPaxCCCode;

	/** Holds the real departure date and the timestamp **/
	private Date realDepartureDate;

	/** Holds the Cabin class code of the flight **/
	private String ccCode;

	/** Holds the EMPNLRecordDTO **/
	private Collection<EMPNLRecordDTO> passengerRecords;

	private Map<String,String> tourIdTicketNumberMap = new HashMap<String,String>();

	/**
	 * Add passenger
	 * 
	 * @param passengerRecordDTO
	 */
	public void addPassengerRecords(EMPNLRecordDTO empnlRecordDTO) {
		if (this.getPassengerRecords() == null) {
			this.setPassengerRecords(new ArrayList<EMPNLRecordDTO>());
		}

		this.getPassengerRecords().add(empnlRecordDTO);
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the pnlContent.
	 */
	public String getPnlContent() {
		return pnlContent;
	}

	/**
	 * @param pnlContent
	 *            The pnlContent to set.
	 */
	public void setPnlContent(String pnlContent) {
		this.pnlContent = pnlContent;
	}

	/**
	 * @return Returns the dateDownloaded.
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return Returns the fromAddress.
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress
	 *            The fromAddress to set.
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return Returns the dayMonth.
	 */
	public String getDayMonth() {
		return dayMonth;
	}

	/**
	 * @param dayMonth
	 *            The dayMonth to set.
	 */
	public void setDayMonth(String dayMonth) {
		this.dayMonth = dayMonth;

		this.setMonth(dayMonth.substring(dayMonth.length() - 3, dayMonth.length()));
		this.setDay(dayMonth.substring(0, dayMonth.length() - 3));
	}

	/**
	 * Returns the departure date
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Date getDepartureDate() throws ModuleException {
		Calendar calToday = new GregorianCalendar();
		int year = calToday.get(Calendar.YEAR);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd");
		String tmpdate = year + "-" + this.getMonth() + "-" + this.getDay();

		Date dateToCheck = null;
		try {
			dateToCheck = dateFormat.parse(tmpdate);
		} catch (ParseException e1) {
			throw new ModuleException("airreservations.pnl.invalidMonthAndDay");
		}

		if (CalendarUtil.isGreaterThan(new Date(), dateToCheck)) {
			year += 1;
			tmpdate = year + "-" + this.getMonth() + "-" + this.getDay();
		}

		try {
			return dateFormat.parse(tmpdate);
		} catch (ParseException e) {
			throw new ModuleException("airreservations.pnl.invalidMonthAndDay");
		}
	}

	/**
	 * @return Returns the boardingAirport.
	 */
	public String getBoardingAirport() {
		return boardingAirport;
	}

	/**
	 * @param boardingAirport
	 *            The boardingAirport to set.
	 */
	public void setBoardingAirport(String boardingAirport) {
		this.boardingAirport = boardingAirport;
	}

	/**
	 * @return Returns the day.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            The day to set.
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return Returns the partNumber.
	 */
	public String getPartNumber() {
		if (this.partNumber == null || this.partNumber.length() < 5) {
			return "";
		}
		return this.partNumber.substring(4);

	}

	/**
	 * @param partNumber
	 *            The partNumber to set.
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return Returns the realDepartureDate.
	 */
	public Date getRealDepartureDate() {
		return realDepartureDate;
	}

	/**
	 * @param realDepartureDate
	 *            The realDepartureDate to set.
	 */
	public void setRealDepartureDate(Date realDepartureDate) {
		this.realDepartureDate = realDepartureDate;
	}

	/**
	 * @return Returns the destinationAirport.
	 */
	public String getDestinationAirport() {
		return destinationAirport;
	}

	/**
	 * @param destinationAirport
	 *            The destinationAirport to set.
	 */
	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	/**
	 * @return Returns the totNumberOfPax.
	 */
	public String getTotNumberOfPax() {
		return totNumberOfPax;
	}

	/**
	 * @param totNumberOfPax
	 *            The totNumberOfPax to set.
	 */
	public void setTotNumberOfPax(String totNumberOfPax) {
		this.totNumberOfPax = totNumberOfPax;
	}

	/**
	 * @return Returns the passengerRecords.
	 */
	public Collection<EMPNLRecordDTO> getPassengerRecords() {
		return passengerRecords;
	}

	/**
	 * @param passengerRecords
	 *            The passengerRecords to set.
	 */
	public void setPassengerRecords(Collection<EMPNLRecordDTO> passengerRecords) {
		this.passengerRecords = passengerRecords;
	}

	/**
	 * @return Returns the desAirportNumPax.
	 */
	public String getDesAirportNumPaxCCCode() {
		return desAirportNumPaxCCCode;
	}

	/**
	 * @param desAirportNumPax
	 *            The desAirportNumPax to set.
	 */
	public void setDesAirportNumPaxCCCode(String desAirportNumPaxCCCode) {
		this.desAirportNumPaxCCCode = desAirportNumPaxCCCode;
		this.destinationAirport = desAirportNumPaxCCCode.substring(1, 4);
		this.totNumberOfPax = desAirportNumPaxCCCode.substring(4, desAirportNumPaxCCCode.length() - 1);
		this.ccCode = desAirportNumPaxCCCode.substring(desAirportNumPaxCCCode.length() - 1);
	}

	/**
	 * @return Returns the tourIdTicketNumberMap.
	 */
	public Map<String,String> getTourIdTicketNumberMap() {
		return tourIdTicketNumberMap;
	}

	/**
	 * @param tourIdTicketNumberMap
	 *            The tourIdTicketNumberMap to set.
	 */
	public void setTourIdTicketNumberMap(Map<String,String> tourIdTicketNumberMap) {
		this.tourIdTicketNumberMap = tourIdTicketNumberMap;
	}

	/**
	 * 
	 * @param tourId
	 * @return
	 */
	public String getXAPnrNumber(String tourId) {

		String tcktNumber = (String) getTourIdTicketNumberMap().get(tourId);
		if (tcktNumber == null) {
			return null;
		}
		String pnr = tcktNumber.substring(2);
		return pnr.replaceAll("/", "");
	}

	/**
	 * 
	 * @param tourId
	 * @param ticketNumber
	 */
	public void addTourIdTicketNumber(String tourId, String ticketNumber) {

		if (tourId != null && ticketNumber != null) {

			getTourIdTicketNumberMap().put(tourId, ticketNumber);

		}

	}

	/**
	 * @return Returns the ccCode.
	 */
	public String getCcCode() {
		return ccCode;
	}

	/**
	 * @param ccCode
	 *            The ccCode to set.
	 */
	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

}
