package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

public class IbeExitUserDetailsDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer ibeExitDetailsId;
	private String searchFlightType;
	private String paxEmail;
	private String paxFlightSearchFrom;
	private String paxFlightSearchTo;
	private Date paxFlihtSearchDepartureDate;
	private Date paxFlightSearchArrivalDate;
	private Integer paxFlightSearchNoOfInfants;
	private Integer paxFlightSearchNoAdults;
	private Integer paxFlightSearchNoChildren;
	private Integer exitStep;
	private Date searchTime;
	private boolean clickedExitDetailEmailLink;
	private String title;
	private String firstName;
	private String lastName;
	private String nationality;
	private String country;
	private String language;
	private String mobileNumber;
	private String travelMobileNumber;
	
	public String getSearchFlightType() {
		return searchFlightType;
	}
	public void setSearchFlightType(String searchFlightType) {
		this.searchFlightType = searchFlightType;
	}
	public String getPaxEmail() {
		return paxEmail;
	}
	public void setPaxEmail(String paxEmail) {
		this.paxEmail = paxEmail;
	}
	public String getPaxFlightSearchFrom() {
		return paxFlightSearchFrom;
	}
	public void setPaxFlightSearchFrom(String paxFlightSearchFrom) {
		this.paxFlightSearchFrom = paxFlightSearchFrom;
	}
	public String getPaxFlightSearchTo() {
		return paxFlightSearchTo;
	}
	public void setPaxFlightSearchTo(String paxFlightSearchTo) {
		this.paxFlightSearchTo = paxFlightSearchTo;
	}
	public Date getPaxFlihtSearchDepartureDate() {
		return paxFlihtSearchDepartureDate;
	}
	public void setPaxFlihtSearchDepartureDate(Date paxFlihtSearchDepartureDate) {
		this.paxFlihtSearchDepartureDate = paxFlihtSearchDepartureDate;
	}
	public Date getPaxFlightSearchArrivalDate() {
		return paxFlightSearchArrivalDate;
	}
	public void setPaxFlightSearchArrivalDate(Date paxFlightSearchArrivalDate) {
		this.paxFlightSearchArrivalDate = paxFlightSearchArrivalDate;
	}
	public Integer getPaxFlightSearchNoOfInfants() {
		return paxFlightSearchNoOfInfants;
	}
	public void setPaxFlightSearchNoOfInfants(Integer paxFlightSearchNoOfInfants) {
		this.paxFlightSearchNoOfInfants = paxFlightSearchNoOfInfants;
	}
	public Integer getPaxFlightSearchNoAdults() {
		return paxFlightSearchNoAdults;
	}
	public void setPaxFlightSearchNoAdults(Integer paxFlightSearchNoAdults) {
		this.paxFlightSearchNoAdults = paxFlightSearchNoAdults;
	}
	public Integer getPaxFlightSearchNoChildren() {
		return paxFlightSearchNoChildren;
	}
	public void setPaxFlightSearchNoChildren(Integer paxFlightSearchNoChildren) {
		this.paxFlightSearchNoChildren = paxFlightSearchNoChildren;
	}
	public Integer getExitStep() {
		return exitStep;
	}
	public void setExitStep(Integer exitStep) {
		this.exitStep = exitStep;
	}
	public Date getSearchTime() {
		return searchTime;
	}
	public void setSearchTime(Date searchTime) {
		this.searchTime = searchTime;
	}
	public Integer getIbeExitDetailsId() {
		return ibeExitDetailsId;
	}
	public void setIbeExitDetailsId(Integer ibeExitDetailsId) {
		this.ibeExitDetailsId = ibeExitDetailsId;
	}
	public boolean isClickedExitDetailEmailLink() {
		return clickedExitDetailEmailLink;
	}
	public void setClickedExitDetailEmailLink(boolean clickedExitDetailEmailLink) {
		this.clickedExitDetailEmailLink = clickedExitDetailEmailLink;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getTravelMobileNumber() {
		return travelMobileNumber;
	}
	public void setTravelMobileNumber(String travelMobileNumber) {
		this.travelMobileNumber = travelMobileNumber;
	}

}
