package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;

public class PassengerDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 998528950997080209L;

	private String title;

	private String firstName;

	private String lastName;

	private String loyaltyNumber;

	private String type;

	public PassengerDTO(String title, String firstName, String lastName, String loyaltyNumber, String type) {
		super();
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.loyaltyNumber = loyaltyNumber;
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLoyaltyNumber() {
		return loyaltyNumber;
	}

	public void setLoyaltyNumber(String loyaltyNumber) {
		this.loyaltyNumber = loyaltyNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
