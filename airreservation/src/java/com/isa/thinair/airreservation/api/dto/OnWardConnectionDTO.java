/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO will wrap the OnWardConnectionInformation The DTOconvention is violated due to a dependency n the template file
 */
public class OnWardConnectionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String flight;

	private String fareclass;

	private int day;

	private String boardingpoint;

	private String arrivalpoint;

	private String departuretime;
	
	private String arrivalTime;
	
	private int dateDifference;

	private String reservationstatus;

	private boolean onwardconnectionelement;
	
	private String segmentCode;

	private Date departureDate;

	public String getArrivalpoint() {
		return arrivalpoint;
	}

	public void setArrivalpoint(String arrivalpoint) {
		this.arrivalpoint = arrivalpoint;
	}

	public String getBoardingpoint() {
		return boardingpoint;
	}

	public void setBoardingpoint(String boardingpoint) {
		this.boardingpoint = boardingpoint;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public String getDeparturetime() {
		return departuretime;
	}

	public void setDeparturetime(String departuretime) {
		this.departuretime = departuretime;
	}

	public String getFareclass() {
		return fareclass;
	}

	public void setFareclass(String fareclass) {
		this.fareclass = fareclass;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public boolean isOnwardconnectionelement() {
		return onwardconnectionelement;
	}

	public void setOnwardconnectionelement(boolean onwardconnectionelement) {
		this.onwardconnectionelement = onwardconnectionelement;
	}

	public String getReservationstatus() {
		return reservationstatus;
	}

	public void setReservationstatus(String reservationstatus) {
		this.reservationstatus = reservationstatus;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getDateDifference() {
		return dateDifference;
	}

	public void setDateDifference(int dateDifference) {
		this.dateDifference = dateDifference;
	}

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
}
