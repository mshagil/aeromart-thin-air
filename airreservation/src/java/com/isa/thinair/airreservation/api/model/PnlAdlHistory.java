/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.Set;

/**
 * To keep track of PnlAdlHistory
 * 
 * @author Isuru
 * @since 1.0
 * @hibernate.class table = "T_PNLADL_TX_HISTORY"
 */
public class PnlAdlHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8632018630886969835L;

	private Integer id;

	private Integer flightID;

	private String airportCode;

	private String messageType;

	private Timestamp transmissionTimeStamp;

	private String transmissionStatus;

	private String sitaAddress;

	private Blob sitaMessage;

	private String errorDescription;

	private String sentMethod;

	private Set<PnlAdlPaxCount> ccPaxCountSet;

	private String email;
	
	private String userID ; 

	/**
	 * @return Returns the messageType
	 * @hibernate.property column = "MESSAGE_TYPE"
	 */
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return Returns the airportCode
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;

	}

	/**
	 * @return Returns the transmission status
	 * @hibernate.property column = "TRANSMISSION_STATUS"
	 */
	public String getTransmissionStatus() {
		return transmissionStatus;
	}

	public void setTransmissionStatus(String transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	/**
	 * @return Returns the flightID.
	 * @hibernate.property column = "SITA_ADDRESS"
	 */
	public String getSitaAddress() {
		return sitaAddress;
	}

	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	/**
	 * @return Returns the flightID.
	 * @hibernate.property column = "TRANSMISSION_TIMESTAMP"
	 */
	public Timestamp getTransmissionTimeStamp() {
		return transmissionTimeStamp;
	}

	public void setTransmissionTimeStamp(Timestamp transmissionTimeStamp) {
		this.transmissionTimeStamp = transmissionTimeStamp;
	}

	/**
	 * @return Returns the flightID.
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public Integer getFlightID() {
		return flightID;
	}

	/**
	 * @param flightID
	 *            The flightID to set.
	 */
	public void setFlightID(Integer flightID) {
		this.flightID = flightID;
	}

	/**
	 * @return Returns the id.
	 * @hibernate.id column = "ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PNLADL_TX_HISTORY"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Returns the blob.
	 * @hibernate.property column = "SENT_MESSAGE"
	 */
	public Blob getSitaMessage() {
		return sitaMessage;
	}

	/**
	 * @param sitaMessage
	 *            The sitaMessage to set.
	 */
	public void setSitaMessage(Blob sitaMessage) {
		this.sitaMessage = sitaMessage;
	}

	/**
	 * @return Returns the errorDescription.
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            The errorDescription to set.
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true" order-by="ID"
	 * @hibernate.collection-key column="HIS_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.PnlAdlPaxCount"
	 **/
	public Set<PnlAdlPaxCount> getCcPaxCountSet() {
		return ccPaxCountSet;
	}

	/**
	 * @param ccPaxCountSet
	 *            The ccPaxCountSet to set.
	 */
	public void setCcPaxCountSet(Set<PnlAdlPaxCount> ccPaxCountSet) {
		this.ccPaxCountSet = ccPaxCountSet;
	}

	/**
	 * @return Returns the errorDescription.
	 * @hibernate.property column = "SENT_METHOD"
	 */
	public String getSentMethod() {
		return sentMethod;
	}

	/**
	 * @param sentMethod
	 *            The sentMethod to set.
	 */
	public void setSentMethod(String sentMethod) {
		this.sentMethod = sentMethod;
	}

	/**
	 * @return Returns the email.
	 * @hibernate.property column = "EMAIL_ID"
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return Returns the User.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
