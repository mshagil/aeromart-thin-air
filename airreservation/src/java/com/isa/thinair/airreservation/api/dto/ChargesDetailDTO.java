/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Holds the charges specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ChargesDetailDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8469787935550598581L;

	/** Holds the charge date */
	private Date chargeDate;

	/** Holds the ond code */
	private String ondCode;

	/** Holds the ond description */
	private String ondDescription;

	/** Holds the charge rate id */
	private Integer chargeRateId;

	/** Holds the passenger id */
	private Integer pnrPaxId;

	/** Holds the passenger name */
	private String passengerName;

	/** Holds the charge code */
	private String chargeCode;

	/** Holds the charge description */
	private String chargeDescription;

	/** Holds the charge type */
	private String chargeType;

	/** Holds the charge amount */
	private BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the charge group code */
	private String chargeGroupCode;

	/** Holds the booking codes */
	private Collection<String> bookingCodes;

	private boolean bundleFareFee;

	/**
	 * Returns charge amount as a String format value
	 * 
	 * @param numberFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getChargeAmountAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getChargeAmount().doubleValue(), numberFormat);
	}

	/**
	 * @return Returns the chargeDate.
	 */
	public Date getChargeDate() {
		return chargeDate;
	}

	/**
	 * Return charge date in a format
	 * 
	 * @param dateFormat
	 * @return
	 */
	public String getChargeStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getChargeDate(), dateFormat);
	}

	/**
	 * @param chargeDate
	 *            The chargeDate to set.
	 */
	public void setChargeDate(Date chargeDate) {
		this.chargeDate = chargeDate;
	}

	/**
	 * @return Returns the chargeDescription.
	 */
	public String getChargeDescription() {
		return chargeDescription;
	}

	/**
	 * @param chargeDescription
	 *            The chargeDescription to set.
	 */
	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	/**
	 * @return Returns the chargeType.
	 */
	public String getChargeType() {
		return chargeType;
	}

	/**
	 * @param chargeType
	 *            The chargeType to set.
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	/**
	 * @return Returns the passengerName.
	 */
	public String getPassengerName() {
		return passengerName;
	}

	/**
	 * @param passengerName
	 *            The passengerName to set.
	 */
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the chargeRateId.
	 */
	public Integer getChargeRateId() {
		return chargeRateId;
	}

	/**
	 * @param chargeRateId
	 *            The chargeRateId to set.
	 */
	public void setChargeRateId(Integer chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	/**
	 * @return Returns the bookingCodes.
	 */
	public Collection<String> getBookingCodes() {
		return this.bookingCodes;
	}

	/**
	 * Returns the booking code
	 * 
	 * @return
	 */
	public String getBookingCode() {
		if (this.getBookingCodes() == null) {
			return "";
		} else {
			String bookingCode = "";
			for (Iterator<String> itBookingCode = this.getBookingCodes().iterator(); itBookingCode.hasNext();) {
				if (bookingCode.equals("")) {
					bookingCode = (String) itBookingCode.next();
				} else {
					bookingCode = bookingCode + "," + (String) itBookingCode.next();
				}
			}

			return bookingCode;
		}
	}

	/**
	 * @param bookingCodes
	 *            The bookingCodes to set.
	 */
	public void setBookingCodes(Collection<String> bookingCodes) {
		this.bookingCodes = bookingCodes;
	}

	/**
	 * @return Returns the chargeGroupCode.
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            The chargeGroupCode to set.
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return Returns the ondDescription.
	 */
	public String getOndDescription() {
		return ondDescription;
	}

	/**
	 * @param ondDescription
	 *            The ondDescription to set.
	 */
	public void setOndDescription(String ondDescription) {
		this.ondDescription = ondDescription;
	}

	/**
	 * @return Returns the ondCode.
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            The ondCode to set.
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the chargeCode.
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            The chargeCode to set.
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return Returns the chargeAmount.
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            The chargeAmount to set.
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return the bundleFareFee
	 */
	public boolean isBundleFareFee() {
		return bundleFareFee;
	}

	/**
	 * @param bundleFareFee
	 *            the bundleFareFee to set
	 */
	public void setBundleFareFee(boolean bundleFareFee) {
		this.bundleFareFee = bundleFareFee;
	}

}
