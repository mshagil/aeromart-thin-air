/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Holds Reservation templates specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationTemplateUtils {

	/** Holds the terms summary IBE template name */
	private static final String TERMS_SUMMARY_IBE_TEMPLATE = "TermsItinerary_IBE";

	/** Holds the terms summary XBE template name */
	private static final String TERMS_SUMMARY_XBE_TEMPLATE = "TermsItinerary_XBE";

	/** Holds the terms summary WS template name */
	private static final String TERMS_SUMMARY_WS_TEMPLATE = "TermsItinerary_WS";

	/** Holds the terms summary INT template name (interline) */
	private static final String TERMS_SUMMARY_INT_TEMPLATE = "TermsItinerary_INT";

	/** Holds the template types */
	public static enum TEMPLATE_TYPES {
		IBE_TEMPLATE, XBE_TEMPLATE, WS_TEMPLATE
	};

	/** Holds the audit force confirm text */
	public final static String AUDIT_FORCE_CONFIRM_TEXT = " Force Confirmed ";

	/** Holds the audit payment text */
	public final static String AUDIT_PAYMENT_TEXT = " PAYMENT ";

	/** Holds the audit refund text */
	public final static String AUDIT_REFUND_TEXT = " REFUND ";

	/** Holds the audit reverse text */
	public final static String AUDIT_REVERSE_TEXT = " REVERSE ";

	/** Holds the no response text */
	public final static String NO_RESPONSE_TEXT = " No response from Payment Broker ";

	/** Holds the flight cancellation */
	public final static String FLIGHT_CANCELLATION = " Cancellation ";

	/** Holds the reschedule flight */
	public final static String RESCHEDULE_FLIGHT = " Reschedule ";

	/** Holds the zulu time display name */
	public static final String ZULU_TIME = " GMT ";

	/** Holds the local time display name */
	protected static final String LOCAL_TIME = " LOCAL ";

	/** Holds the minutes display */
	public static final String MINUTES = " minutes ";

	/** Avoid instantiating the constructor */
	private ReservationTemplateUtils() {

	}

	/**
	 * Returns the Terms and Conditions
	 * 
	 * @param locale
	 * @return
	 * @throws ModuleException
	 */
	public static String getTermsNConditions(Locale locale, TEMPLATE_TYPES templateTypes) throws ModuleException {
		String templateName;

		// Identifying the application type
		if (templateTypes == TEMPLATE_TYPES.IBE_TEMPLATE) {
			templateName = TERMS_SUMMARY_IBE_TEMPLATE;
		} else if (templateTypes == TEMPLATE_TYPES.XBE_TEMPLATE) {
			templateName = TERMS_SUMMARY_XBE_TEMPLATE;
		} else {
			templateName = TERMS_SUMMARY_WS_TEMPLATE;
		}

		return getTermsNConditions(templateName, locale);
	}

	/**
	 * Retrieves terms and conditions for the default carrier.
	 * 
	 * @param templateName
	 *            The terms template name to be retrieved.
	 * @param locale
	 *            Locale of the terms template to be retrieved.
	 * @return The terms and conditions details according to the parameters given.
	 * @throws ModuleException
	 *             If retrieval of terms and conditions fails.
	 */
	public static String getTermsNConditions(String templateName, Locale locale) throws ModuleException {

		String termsNConditions = "";

		Map<String, Map<String, String>> tncMap = ReservationModuleUtils.getGlobalConfig().getTermsAndCOnditionsMap()
				.get(AppSysParamsUtil.getDefaultCarrierCode());

		if (tncMap != null) {
			termsNConditions = tncMap.get(templateName).get(locale.getLanguage());
		}
		
		return termsNConditions;
	}

	/**
	 * Returns the Terms and Conditions
	 * 
	 * @param locale
	 * @return
	 * @throws ModuleException
	 */
	public static String getInterlineTermsNConditions(Locale locale, String marketingCarrier, List<String> operatingCarriers,
			Boolean isFirstDepartingTNCEnabled, String firstDepartingCarrier, boolean isCnxRes) throws ModuleException {

		String carriersString = "";

		if (isFirstDepartingTNCEnabled != null && isFirstDepartingTNCEnabled == true && !isCnxRes) {
			carriersString = firstDepartingCarrier;
		} else {
			if (operatingCarriers.size() == 1) {
				/* Dry Sell (1 carrier) */
				// For dry selling w/ only 1 carrier, we take the T&C of that
				// carrier

				carriersString = operatingCarriers.get(0);
			} else {
				/* All other scenarios */
				// We take the T&C of the MKT carrier. Note that this is temporary
				// as we
				// are still awaiting updated T&C from the client. - Zaki
				// Mar.07.2010

				Collections.sort(operatingCarriers);

				carriersString = marketingCarrier;
				for (String operatingCarrier : operatingCarriers) {
					if (marketingCarrier.compareTo(operatingCarrier) != 0) {
						carriersString += "_" + operatingCarrier;
					}
				}
			}
		}

		String termsNConditions = "Terms And Conditions Not Found";

		Map<String, Map<String, String>> tncMap = ReservationModuleUtils.getGlobalConfig().getTermsAndCOnditionsMap()
				.get(carriersString);

		if (tncMap != null && tncMap.get(TERMS_SUMMARY_INT_TEMPLATE) != null
				&& tncMap.get(TERMS_SUMMARY_INT_TEMPLATE).get(locale.getLanguage()) != null) {
			termsNConditions = tncMap.get(TERMS_SUMMARY_INT_TEMPLATE).get(locale.getLanguage());
		}

		return termsNConditions;
	}

	/**
	 * Returns a map containing the error code and the error message
	 * 
	 * @param errorCode
	 * @param errorMsg
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map getStandardPaymentErrorMap(Object errorCode, Object errorMsg) throws ModuleException {
		Map errorMap = new HashMap();
		errorMap.put(BeanUtils.nullHandler(errorCode), BeanUtils.nullHandler(errorMsg));
		return errorMap;
	}
}
