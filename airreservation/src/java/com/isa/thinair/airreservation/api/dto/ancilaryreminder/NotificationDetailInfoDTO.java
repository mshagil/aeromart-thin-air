package com.isa.thinair.airreservation.api.dto.ancilaryreminder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.eurocommerce.PassengerDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class NotificationDetailInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4359927242292325665L;
	private String pnr;
	private String name;
	private String lastName;
	private String title;

	private String email;
	private String flightNo;

	private Integer flightSegmentId;
	private Integer fltSegNotifyEventId;

	private Integer pnrSegId;

	private String origin;
	private String originDescription;
	private String destination;
	private String destinationDescription;

	private String onlineCheckInURL;

	private Date flightDepartureTimeLocal;
	private Date flightDepartureTimeZulu;

	private String iteneraryLanguage;
	private String airportOnlineCheckin;

	private String insuranceStatus;
	private String insurancePolicyNum;

	private String seatSelectedStatus;
	private String mealSelectedStatus;
	private String baggageSelectedStatus;

	private String seatReminderReq;
	private String mealReminderReq;
	private String baggageReminderReq;
	private String insReminderReq;

	private String busReminderReq;
	private String isBusAtDeparture;
	private String busConnectingAirport;

	private Integer ancillaryCutOverStartTime;

	private boolean reminderRequired = true;
	private String ibeURL;
	private String ibePnrModificationLink;

	private String carBookingLink;
	private String hotelBookingLink;

	private Integer flightSegNotificationEventId;
	private Integer pnrSegNotificationEventId;

	private String qrtzJobName;
	private String qrtzJobGroupName;

	private Integer daysRemainingInflights;
	private Integer hourRemainingInflights;
	private Date firstSegFlightDepDateTime;

	private String insuranceApproxAmount;
	private String seatApproxAmount;

	private List<FlightSegReminderNotificationDTO> flightSegmentsList = new ArrayList<FlightSegReminderNotificationDTO>();
	private List<PassengerDTO> passangersDTOList = new ArrayList<PassengerDTO>();

	private boolean dynamicCarLink = false;
	
	public String getflightDepartureDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(flightDepartureTimeLocal, dateTimeFormat);
	}

	public String getFirstFlightSegDepDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(firstSegFlightDepDateTime, dateTimeFormat);
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the flightSegmentId
	 */
	public Integer getFlightSegmentId() {
		return flightSegmentId;
	}

	/**
	 * @param flightSegmentId
	 *            the flightSegmentId to set
	 */
	public void setFlightSegmentId(Integer flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	/**
	 * @return the fltSegNotifyEventId
	 */
	public Integer getFltSegNotifyEventId() {
		return fltSegNotifyEventId;
	}

	/**
	 * @param fltSegNotifyEventId
	 *            the fltSegNotifyEventId to set
	 */
	public void setFltSegNotifyEventId(Integer fltSegNotifyEventId) {
		this.fltSegNotifyEventId = fltSegNotifyEventId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the insuranceStatus
	 */
	public String getInsuranceStatus() {
		return insuranceStatus;
	}

	/**
	 * @param insuranceStatus
	 *            the insuranceStatus to set
	 */
	public void setInsuranceStatus(String insuranceStatus) {
		this.insuranceStatus = insuranceStatus;
	}

	/**
	 * @return the insurancePolicyNum
	 */
	public String getInsurancePolicyNum() {
		return insurancePolicyNum;
	}

	/**
	 * @param insurancePolicyNum
	 *            the insurancePolicyNum to set
	 */
	public void setInsurancePolicyNum(String insurancePolicyNum) {
		this.insurancePolicyNum = insurancePolicyNum;
	}

	/**
	 * @return the seatSelectedStatus
	 */
	public String getSeatSelectedStatus() {
		return seatSelectedStatus;
	}

	/**
	 * @param seatSelectedStatus
	 *            the seatSelectedStatus to set
	 */
	public void setSeatSelectedStatus(String seatSelectedStatus) {
		this.seatSelectedStatus = seatSelectedStatus;
	}

	/**
	 * @return the mealSelectedStatus
	 */
	public String getMealSelectedStatus() {
		return mealSelectedStatus;
	}

	/**
	 * @param mealSelectedStatus
	 *            the mealSelectedStatus to set
	 */
	public void setMealSelectedStatus(String mealSelectedStatus) {
		this.mealSelectedStatus = mealSelectedStatus;
	}

	/**
	 * @return the seatReminderReq
	 */
	public String getSeatReminderReq() {
		return seatReminderReq;
	}

	/**
	 * @param seatReminderReq
	 *            the seatReminderReq to set
	 */
	public void setSeatReminderReq(String seatReminderReq) {
		this.seatReminderReq = seatReminderReq;
	}

	/**
	 * @return the mealReminderReq
	 */
	public String getMealReminderReq() {
		return mealReminderReq;
	}

	/**
	 * @param mealReminderReq
	 *            the mealReminderReq to set
	 */
	public void setMealReminderReq(String mealReminderReq) {
		this.mealReminderReq = mealReminderReq;
	}

	/**
	 * @return the insReminderReq
	 */
	public String getInsReminderReq() {
		return insReminderReq;
	}

	/**
	 * @param insReminderReq
	 *            the insReminderReq to set
	 */
	public void setInsReminderReq(String insReminderReq) {
		this.insReminderReq = insReminderReq;
	}

	/**
	 * @return the flightSegmentsList
	 */
	public List<FlightSegReminderNotificationDTO> getFlightSegmentsList() {
		return flightSegmentsList;
	}

	/**
	 * @return the passangersDTOList
	 */
	public List<PassengerDTO> getPassangersDTOList() {
		return passangersDTOList;
	}

	/**
	 * @return the iteneraryLanguage
	 */
	public String getIteneraryLanguage() {
		return iteneraryLanguage;
	}

	/**
	 * @param iteneraryLanguage
	 *            the iteneraryLanguage to set
	 */
	public void setIteneraryLanguage(String iteneraryLanguage) {
		this.iteneraryLanguage = iteneraryLanguage;
	}

	/**
	 * @return the airportOnlineCheckin
	 */
	public String getAirportOnlineCheckin() {
		return airportOnlineCheckin;
	}

	/**
	 * @param airportOnlineCheckin
	 *            the airportOnlineCheckin to set
	 */
	public void setAirportOnlineCheckin(String airportOnlineCheckin) {
		this.airportOnlineCheckin = airportOnlineCheckin;
	}

	/**
	 * @return the reminderRequired
	 */
	public boolean isReminderRequired() {
		return reminderRequired;
	}

	/**
	 * @param reminderRequired
	 *            the reminderRequired to set
	 */
	public void setReminderRequired(boolean reminderRequired) {
		this.reminderRequired = reminderRequired;
	}

	/**
	 * @return the ancillaryCutOverStartTime
	 */
	public Integer getAncillaryCutOverStartTime() {
		return ancillaryCutOverStartTime;
	}

	/**
	 * @param ancillaryCutOverStartTime
	 *            the ancillaryCutOverStartTime to set
	 */
	public void setAncillaryCutOverStartTime(Integer ancillaryCutOverStartTime) {
		this.ancillaryCutOverStartTime = ancillaryCutOverStartTime;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the flightDepartureTimeLocal
	 */
	public Date getFlightDepartureTimeLocal() {
		return flightDepartureTimeLocal;
	}

	/**
	 * @param flightDepartureTimeLocal
	 *            the flightDepartureTimeLocal to set
	 */
	public void setFlightDepartureTimeLocal(Date flightDepartureTimeLocal) {
		this.flightDepartureTimeLocal = flightDepartureTimeLocal;
	}

	/**
	 * @return the flightDepartureTimeZulu
	 */
	public Date getFlightDepartureTimeZulu() {
		return flightDepartureTimeZulu;
	}

	/**
	 * @param flightDepartureTimeZulu
	 *            the flightDepartureTimeZulu to set
	 */
	public void setFlightDepartureTimeZulu(Date flightDepartureTimeZulu) {
		this.flightDepartureTimeZulu = flightDepartureTimeZulu;
	}

	/**
	 * @return the ibeURL
	 */
	public String getIbeURL() {
		return ibeURL;
	}

	/**
	 * @param ibeURL
	 *            the ibeURL to set
	 */
	public void setIbeURL(String ibeURL) {
		this.ibeURL = ibeURL;
	}

	/**
	 * @return the ibePnrModificationLink
	 */
	public String getIbePnrModificationLink() {
		return ibePnrModificationLink;
	}

	/**
	 * @param ibePnrModificationLink
	 *            the ibePnrModificationLink to set
	 */
	public void setIbePnrModificationLink(String ibePnrModificationLink) {
		this.ibePnrModificationLink = ibePnrModificationLink;
	}

	/**
	 * @return the originDescription
	 */
	public String getOriginDescription() {
		return originDescription;
	}

	/**
	 * @param originDescription
	 *            the originDescription to set
	 */
	public void setOriginDescription(String originDescription) {
		this.originDescription = originDescription;
	}

	/**
	 * @return the destinationDescription
	 */
	public String getDestinationDescription() {
		return destinationDescription;
	}

	/**
	 * @param destinationDescription
	 *            the destinationDescription to set
	 */
	public void setDestinationDescription(String destinationDescription) {
		this.destinationDescription = destinationDescription;
	}

	/**
	 * @return the flightSegNotificationEventId
	 */
	public Integer getFlightSegNotificationEventId() {
		return flightSegNotificationEventId;
	}

	/**
	 * @param flightSegNotificationEventId
	 *            the flightSegNotificationEventId to set
	 */
	public void setFlightSegNotificationEventId(Integer flightSegNotificationEventId) {
		this.flightSegNotificationEventId = flightSegNotificationEventId;
	}

	/**
	 * @return the pnrSegNotificationEventId
	 */
	public Integer getPnrSegNotificationEventId() {
		return pnrSegNotificationEventId;
	}

	/**
	 * @param pnrSegNotificationEventId
	 *            the pnrSegNotificationEventId to set
	 */
	public void setPnrSegNotificationEventId(Integer pnrSegNotificationEventId) {
		this.pnrSegNotificationEventId = pnrSegNotificationEventId;
	}

	/**
	 * @return the carBookingLink
	 */
	public String getCarBookingLink() {
		return carBookingLink;
	}

	/**
	 * @param carBookingLink
	 *            the carBookingLink to set
	 */
	public void setCarBookingLink(String carBookingLink) {
		this.carBookingLink = carBookingLink;
	}

	/**
	 * @return the hotelBookingLink
	 */
	public String getHotelBookingLink() {
		return hotelBookingLink;
	}

	/**
	 * @param hotelBookingLink
	 *            the hotelBookingLink to set
	 */
	public void setHotelBookingLink(String hotelBookingLink) {
		this.hotelBookingLink = hotelBookingLink;
	}

	/**
	 * @return the qrtzJobName
	 */
	public String getQrtzJobName() {
		return qrtzJobName;
	}

	/**
	 * @param qrtzJobName
	 *            the qrtzJobName to set
	 */
	public void setQrtzJobName(String qrtzJobName) {
		this.qrtzJobName = qrtzJobName;
	}

	/**
	 * @return the qrtzJobGroupName
	 */
	public String getQrtzJobGroupName() {
		return qrtzJobGroupName;
	}

	/**
	 * @param qrtzJobGroupName
	 *            the qrtzJobGroupName to set
	 */
	public void setQrtzJobGroupName(String qrtzJobGroupName) {
		this.qrtzJobGroupName = qrtzJobGroupName;
	}

	/**
	 * @return the daysRemainingInflights
	 */
	public Integer getDaysRemainingInflights() {
		return daysRemainingInflights;
	}

	/**
	 * @param daysRemainingInflights
	 *            the daysRemainingInflights to set
	 */
	public void setDaysRemainingInflights(Integer daysRemainingInflights) {
		this.daysRemainingInflights = daysRemainingInflights;
	}

	/**
	 * @return the hourRemainingInflights
	 */
	public Integer getHourRemainingInflights() {
		return hourRemainingInflights;
	}

	/**
	 * @param hourRemainingInflights
	 *            the hourRemainingInflights to set
	 */
	public void setHourRemainingInflights(Integer hourRemainingInflights) {
		this.hourRemainingInflights = hourRemainingInflights;
	}

	/**
	 * @return the firstSegFlightDepDateTime
	 */
	public Date getFirstSegFlightDepDateTime() {
		return firstSegFlightDepDateTime;
	}

	/**
	 * @param firstSegFlightDepDateTime
	 *            the firstSegFlightDepDateTime to set
	 */
	public void setFirstSegFlightDepDateTime(Date firstSegFlightDepDateTime) {
		this.firstSegFlightDepDateTime = firstSegFlightDepDateTime;
	}

	/**
	 * @return the insuranceApproxAmount
	 */
	public String getInsuranceApproxAmount() {
		return insuranceApproxAmount;
	}

	/**
	 * @param insuranceApproxAmount
	 *            the insuranceApproxAmount to set
	 */
	public void setInsuranceApproxAmount(String insuranceApproxAmount) {
		this.insuranceApproxAmount = insuranceApproxAmount;
	}

	/**
	 * @return the seatApproxAmount
	 */
	public String getSeatApproxAmount() {
		return seatApproxAmount;
	}

	/**
	 * @param seatApproxAmount
	 *            the seatApproxAmount to set
	 */
	public void setSeatApproxAmount(String seatApproxAmount) {
		this.seatApproxAmount = seatApproxAmount;
	}

	public String getBusReminderReq() {
		return busReminderReq;
	}

	public void setBusReminderReq(String busReminderReq) {
		this.busReminderReq = busReminderReq;
	}

	public String getIsBusAtDeparture() {
		return isBusAtDeparture;
	}

	public void setIsBusAtDeparture(String isBusAtDeparture) {
		this.isBusAtDeparture = isBusAtDeparture;
	}

	public String getBusConnectingAirport() {
		return busConnectingAirport;
	}

	public void setBusConnectingAirport(String busConnectingAirport) {
		this.busConnectingAirport = busConnectingAirport;
	}

	public String getBaggageSelectedStatus() {
		return baggageSelectedStatus;
	}

	public void setBaggageSelectedStatus(String baggageSelectedStatus) {
		this.baggageSelectedStatus = baggageSelectedStatus;
	}

	public String getBaggageReminderReq() {
		return baggageReminderReq;
	}

	public void setBaggageReminderReq(String baggageReminderReq) {
		this.baggageReminderReq = baggageReminderReq;
	}

	public boolean isDynamicCarLink() {
		return dynamicCarLink;
	}

	public void setDynamicCarLink(boolean dynamicCarLink) {
		this.dynamicCarLink = dynamicCarLink;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setOnlineCheckInURL(String onlineCheckInURL) {
		this.onlineCheckInURL = onlineCheckInURL;
	}

	public String getOnlineCheckInURL() {
		return onlineCheckInURL;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	
}
