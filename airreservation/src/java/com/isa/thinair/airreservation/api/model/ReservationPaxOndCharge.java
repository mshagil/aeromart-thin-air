/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO.ReservationPaxChargeMetaTO;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * The model to keep track of reservation fare level (OND) charges
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PNR_PAX_OND_CHARGES"
 */
public class ReservationPaxOndCharge extends Persistent implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds Reservation passenger ond charge id */
	private Integer pnrPaxOndChgId;

	/** Holds the ond fare id */
	private Integer fareId;

	/** Holds the charge rate id */
	private Integer chargeRateId;

	/** Holds charge amount */
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds charge date */
	private Date zuluChargeDate;

	/** Holds the local charge date */
	private Date localChargeDate;

	/** Holds charge group code */
	private String chargeGroupCode;

	/** Holds the reservation passenger fare */
	private ReservationPaxFare reservationPaxFare;

	/** Holds origin agent code */
	private String agentCode;

	/** Holds origin user id */
	private String userId;

	private String refundableOperation;

	private Integer discountValuePercentage;

	private Set<ReservationPaxOndChgCommission> resPaxOndChgCommission;

	private BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal adjustment = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String chargeCode;

	private BigDecimal taxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal nonTaxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String taxAppliedCategory;

	private String taxDepositStateCode;

	private String taxDepositCountryCode;

	public static String TAX_APPLIED_FOR_TICKETING_CATEGORY = "T";
	public static String TAX_APPLIED_FOR_NON_TICKETING_CATEGORY = "N";
	public static String TAX_APPLIED_FOR_OTHER_CATEGORY = "O";

	/**
	 * Holds the charge reference information, charge reference is required to derive the PAX_OND_PAYMENTS to denote for
	 * which fare this charge has been applied
	 */
	private Integer chargeReferenceId;

	private Integer transactionSeq;
	
	private Integer operationType;

	public ReservationPaxOndCharge() {

	}

	public ReservationPaxOndCharge cloneForNew(CredentialsDTO credentialsDTO, boolean isRefundableOperation) {
		ReservationPaxOndCharge reservationPaxOndCharge = new ReservationPaxOndCharge();

		reservationPaxOndCharge.setFareId(this.getFareId());
		reservationPaxOndCharge.setChargeRateId(this.getChargeRateId());
		reservationPaxOndCharge.setAmount(this.getAmount());
		reservationPaxOndCharge.setZuluChargeDate(this.getZuluChargeDate());
		reservationPaxOndCharge.setLocalChargeDate(this.getLocalChargeDate());
		reservationPaxOndCharge.setChargeGroupCode(this.getChargeGroupCode());
		reservationPaxOndCharge.setReservationPaxFare(null);

		reservationPaxOndCharge.setAgentCode(credentialsDTO.getAgentCode());
		reservationPaxOndCharge.setUserId(credentialsDTO.getUserId());
		reservationPaxOndCharge.setRefundableOperation(isRefundableOperation ? "Y" : "N");
		reservationPaxOndCharge.setDiscountValuePercentage(this.getDiscountValuePercentage());
		Set<ReservationPaxOndChgCommission> tempResPaxOndChgCom = this.getResPaxOndChgCommission();
		if (tempResPaxOndChgCom != null && tempResPaxOndChgCom.size() > 0) {
			reservationPaxOndCharge.setResPaxOndChgCommission(cloneResPaxOndChgCommission(tempResPaxOndChgCom));
		} else {
			reservationPaxOndCharge.setResPaxOndChgCommission(new HashSet<ReservationPaxOndChgCommission>());
		}
		reservationPaxOndCharge.setChargeReferenceId(this.getChargeReferenceId());
		reservationPaxOndCharge.setDiscount(this.getDiscount());
		reservationPaxOndCharge.setAdjustment(this.getAdjustment());
		reservationPaxOndCharge.setChargeCode(this.getChargeCode());
		reservationPaxOndCharge.setTaxableAmount(this.getTaxableAmount());
		reservationPaxOndCharge.setNonTaxableAmount(this.getNonTaxableAmount());
		reservationPaxOndCharge.setTaxAppliedCategory(this.getTaxAppliedCategory());
		reservationPaxOndCharge.setTaxDepositStateCode(this.getTaxDepositStateCode());
		reservationPaxOndCharge.setTaxDepositCountryCode(this.getTaxDepositCountryCode());
		reservationPaxOndCharge.setTransactionSeq(this.getTransactionSeq());
		reservationPaxOndCharge.setOperationType(this.getOperationType());		

		return reservationPaxOndCharge;
	}

	public ReservationPaxOndCharge cloneForShallowCopy() {
		ReservationPaxOndCharge reservationPaxOndCharge = new ReservationPaxOndCharge();

		reservationPaxOndCharge.setPnrPaxOndChgId(this.getPnrPaxOndChgId());
		reservationPaxOndCharge.setFareId(this.getFareId());
		reservationPaxOndCharge.setChargeRateId(this.getChargeRateId());
		reservationPaxOndCharge.setAmount(this.getAmount());
		reservationPaxOndCharge.setZuluChargeDate(this.getZuluChargeDate());
		reservationPaxOndCharge.setLocalChargeDate(this.getLocalChargeDate());
		reservationPaxOndCharge.setChargeGroupCode(this.getChargeGroupCode());
		reservationPaxOndCharge.setReservationPaxFare(null);

		reservationPaxOndCharge.setAgentCode(this.getAgentCode());
		reservationPaxOndCharge.setUserId(this.getUserId());
		reservationPaxOndCharge.setRefundableOperation(this.getRefundableOperation());
		reservationPaxOndCharge.setDiscountValuePercentage(this.getDiscountValuePercentage());
		Set<ReservationPaxOndChgCommission> tempResPaxOndChgCom = this.getResPaxOndChgCommission();
		if (tempResPaxOndChgCom != null && tempResPaxOndChgCom.size() > 0) {
			reservationPaxOndCharge.setResPaxOndChgCommission(cloneResPaxOndChgCommission(tempResPaxOndChgCom));
		} else {
			reservationPaxOndCharge.setResPaxOndChgCommission(new HashSet<ReservationPaxOndChgCommission>());
		}
		reservationPaxOndCharge.setChargeReferenceId(this.getChargeReferenceId());
		reservationPaxOndCharge.setDiscount(this.getDiscount());
		reservationPaxOndCharge.setAdjustment(this.getAdjustment());
		reservationPaxOndCharge.setChargeCode(this.getChargeCode());
		reservationPaxOndCharge.setTaxableAmount(this.getTaxableAmount());
		reservationPaxOndCharge.setNonTaxableAmount(this.getNonTaxableAmount());
		reservationPaxOndCharge.setTaxAppliedCategory(this.getTaxAppliedCategory());
		reservationPaxOndCharge.setTaxDepositStateCode(this.getTaxDepositStateCode());
		reservationPaxOndCharge.setTaxDepositCountryCode(this.getTaxDepositCountryCode());
		reservationPaxOndCharge.setTransactionSeq(this.getTransactionSeq());
		reservationPaxOndCharge.setOperationType(this.getOperationType());

		return reservationPaxOndCharge;
	}

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the chargeRateId.
	 * @hibernate.property column = "CHARGE_RATE_ID"
	 */
	public Integer getChargeRateId() {
		return chargeRateId;
	}

	/**
	 * @param chargeRateId
	 *            The chargeRateId to set.
	 */
	public void setChargeRateId(Integer chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	/**
	 * @return Returns the chargeGroupCode.
	 * @hibernate.property column = "CHARGE_GROUP_CODE"
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            The chargeGroupCode to set.
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return Returns the fareId.
	 * @hibernate.property column = "FARE_ID"
	 */
	public Integer getFareId() {
		return fareId;
	}

	/**
	 * @param fareId
	 *            The fareId to set.
	 */
	public void setFareId(Integer fareId) {
		this.fareId = fareId;
	}

	/**
	 * @return Returns the pnrPaxOndChgId.
	 * @hibernate.id column = "PFT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_OND_CHARGES"
	 */
	public Integer getPnrPaxOndChgId() {
		return pnrPaxOndChgId;
	}

	/**
	 * @param pnrPaxOndChgId
	 *            The pnrPaxOndChgId to set.
	 */
	public void setPnrPaxOndChgId(Integer pnrPaxOndChgId) {
		this.pnrPaxOndChgId = pnrPaxOndChgId;
	}

	/**
	 * @hibernate.many-to-one column="PPF_ID" class="com.isa.thinair.airreservation.api.model.ReservationPaxFare"
	 * @return Returns the reservationPaxFare.
	 */
	public ReservationPaxFare getReservationPaxFare() {
		return reservationPaxFare;
	}

	/**
	 * @param reservationPaxFare
	 *            The reservationPaxFare to set.
	 */
	public void setReservationPaxFare(ReservationPaxFare reservationPaxFare) {
		this.reservationPaxFare = reservationPaxFare;
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPnrPaxOndChgId()).toHashCode();
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationPaxOndCharge) {
			ReservationPaxOndCharge castObject = (ReservationPaxOndCharge) o;
			if (castObject.getPnrPaxOndChgId() == null || this.getPnrPaxOndChgId() == null) {
				return false;
			} else if (castObject.getPnrPaxOndChgId().intValue() == this.getPnrPaxOndChgId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * @return Returns the localChargeDate.
	 */
	public Date getLocalChargeDate() {
		return localChargeDate;
	}

	/**
	 * @param localChargeDate
	 *            The localChargeDate to set.
	 */
	public void setLocalChargeDate(Date localChargeDate) {
		this.localChargeDate = localChargeDate;
	}

	/**
	 * @return Returns the zuluChargeDate.
	 * @hibernate.property column = "CHARGE_DATE"
	 */
	public Date getZuluChargeDate() {
		return zuluChargeDate;
	}

	/**
	 * @param zuluChargeDate
	 *            The zuluChargeDate to set.
	 */
	public void setZuluChargeDate(Date zuluChargeDate) {
		this.zuluChargeDate = zuluChargeDate;
	}

	public ReservationPaxChargeMetaTO transformTOReservationPaxChargeMetaTO() {
		ReservationPaxChargeMetaTO reservationPaxChargeMetaTO = new ReservationPaxChargeMetaTO();
		reservationPaxChargeMetaTO.setPnrPaxOndChgId(this.getPnrPaxOndChgId());
		reservationPaxChargeMetaTO.setAmount(this.getEffectiveAmount());
		// reservationPaxChargeMetaTO.setAmount(this.getAmount());
		reservationPaxChargeMetaTO.setChargeGroupCode(this.getChargeGroupCode());
		reservationPaxChargeMetaTO.setChargeRateId(this.getChargeRateId());
		reservationPaxChargeMetaTO.setFareId(this.getFareId());
		reservationPaxChargeMetaTO.setZuluChargeDate(this.getZuluChargeDate());
		reservationPaxChargeMetaTO.setRemarks(null);
		reservationPaxChargeMetaTO.setChargeCode(this.getChargeCode());
		return reservationPaxChargeMetaTO;
	}

	/**
	 * @return the agentCode
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the refundableOperation
	 * @hibernate.property column = "IS_REFUNDABLE_OPERATION"
	 */
	public String getRefundableOperation() {
		return refundableOperation;
	}

	/**
	 * @param refundableOperation
	 *            the refundableOperation to set
	 */
	public void setRefundableOperation(String refundableOperation) {
		this.refundableOperation = refundableOperation;
	}

	/**
	 * @return the refundableOperation
	 * @hibernate.property column = "VALUE_PERCENTAGE"
	 */
	public Integer getDiscountValuePercentage() {
		return discountValuePercentage;
	}

	public void setDiscountValuePercentage(Integer discountValuePercentage) {
		this.discountValuePercentage = discountValuePercentage;
	}

	/**
	 * @return Returns the resPaxOndChgCommission.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PFT_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPaxOndChgCommission"
	 */
	public Set<ReservationPaxOndChgCommission> getResPaxOndChgCommission() {
		if (resPaxOndChgCommission == null) {
			resPaxOndChgCommission = new HashSet<ReservationPaxOndChgCommission>();
		}
		return resPaxOndChgCommission;
	}

	public void setResPaxOndChgCommission(Set<ReservationPaxOndChgCommission> resPaxOndChgCommission) {
		this.resPaxOndChgCommission = resPaxOndChgCommission;
	}

	/**
	 * @return the chargeReferenceId
	 * @hibernate.property column = "CHARGE_REFERENCE_ID"
	 */
	public Integer getChargeReferenceId() {
		return chargeReferenceId;
	}

	public void setChargeReferenceId(Integer chargeReferenceId) {
		this.chargeReferenceId = chargeReferenceId;
	}

	private Set<ReservationPaxOndChgCommission>
			cloneResPaxOndChgCommission(Set<ReservationPaxOndChgCommission> resPaxOndChgCommission) {
		Set<ReservationPaxOndChgCommission> cloneResPaxOndChgCom = new HashSet<ReservationPaxOndChgCommission>();
		Iterator<ReservationPaxOndChgCommission> resPaxOndChgComIte = resPaxOndChgCommission.iterator();
		while (resPaxOndChgComIte.hasNext()) {
			cloneResPaxOndChgCom.add((ReservationPaxOndChgCommission) resPaxOndChgComIte.next().clone());
		}
		return cloneResPaxOndChgCom;
	}

	/**
	 * @return Returns the discount.
	 * @hibernate.property column = "DISCOUNT"
	 */
	public BigDecimal getDiscount() {
		if (discount == null)
			discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		return discount;
	}

	/**
	 * @param discount
	 *            The discount to set.
	 */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	/**
	 * @return Returns the adjustment.
	 * @hibernate.property column = "ROE_ADJUSTMENT"
	 */
	public BigDecimal getAdjustment() {

		if (adjustment == null)
			adjustment = AccelAeroCalculator.getDefaultBigDecimalZero();

		return adjustment;
	}

	/**
	 * @param discount
	 *            The discount to set.
	 */
	public void setAdjustment(BigDecimal adjustment) {
		this.adjustment = adjustment;
	}

	/**
	 * @return Returns the effective charge amount. when discount/promotion/roe_adjustment given for the respective
	 *         charge following function returns the actual effective charge amount
	 */
	public BigDecimal getEffectiveAmount() {
		BigDecimal actualAmount = this.amount;

		if (this.discount != null && this.discount.doubleValue() != 0) {
			actualAmount = AccelAeroCalculator.add(actualAmount, this.discount);
		}

		if (this.adjustment != null && this.adjustment.doubleValue() != 0) {
			actualAmount = AccelAeroCalculator.add(actualAmount, this.adjustment);
		}

		return actualAmount;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return Returns the taxableAmount.
	 * @hibernate.property column = "TAXABLE_AMOUNT"
	 */
	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	/**
	 * @return Returns the nonTaxableAmount.
	 * @hibernate.property column = "NON_TAXABLE_AMOUNT"
	 */
	public BigDecimal getNonTaxableAmount() {
		return nonTaxableAmount;
	}

	public void setNonTaxableAmount(BigDecimal nonTaxableAmount) {
		this.nonTaxableAmount = nonTaxableAmount;
	}

	/**
	 * @return Returns the taxAppliedCategory.
	 * @hibernate.property column = "APPLIED_CATEGORY"
	 */
	public String getTaxAppliedCategory() {
		return taxAppliedCategory;
	}

	public void setTaxAppliedCategory(String taxAppliedCategory) {
		this.taxAppliedCategory = taxAppliedCategory;
	}

	/**
	 * @return Returns the taxDepositStateCode.
	 * @hibernate.property column = "TAX_DEPOSIT_STATE_CODE"
	 */
	public String getTaxDepositStateCode() {
		return taxDepositStateCode;
	}

	public void setTaxDepositStateCode(String taxDepositStateCode) {
		this.taxDepositStateCode = taxDepositStateCode;
	}

	/**
	 * @return Returns the taxDepositCountryCode.
	 * @hibernate.property column = "TAX_DEPOSIT_COUNTRY_CODE"
	 */
	public String getTaxDepositCountryCode() {
		return taxDepositCountryCode;
	}

	public void setTaxDepositCountryCode(String taxDepositCountryCode) {
		this.taxDepositCountryCode = taxDepositCountryCode;
	}

	/**
	 * @return the transactionSeq
	 * @hibernate.property column = "TRANSACTION_SEQ"
	 */
	public Integer getTransactionSeq() {
		return transactionSeq;
	}

	/**
	 * @param transactionSeq
	 *            the transactionSeq to set
	 */
	public void setTransactionSeq(Integer transactionSeq) {
		this.transactionSeq = transactionSeq;
	}

	/**
	 * @return the operationType
	 * @hibernate.property column = "OPERATION_TYPE"
	 */
	public Integer getOperationType() {
		return operationType;
	}

	public void setOperationType(Integer operationType) {
		this.operationType = operationType;
	}

}
