package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of reservation segment Notifications
 * 
 * @hibernate.class table = "T_PNR_SEGMENT_NOTIFICATION"
 */
public class ReservationSegmentNotification extends Persistent {

	public static final String NOTIFICATION_TYPE_PROMOTION = "PROMOTION";

	private Integer reservationSegNotificationId;
	private Integer reservationSegId;
	private String notificationType;
	private Integer noficationRefId;

	/**
	 * @hibernate.id column = "PNR_SEGMENT_NOTIFICATION_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_SEGMENT_NOTIFICATION"
	 */
	public Integer getReservationSegNotificationId() {
		return reservationSegNotificationId;
	}

	public void setReservationSegNotificationId(Integer reservationSegNotificationId) {
		this.reservationSegNotificationId = reservationSegNotificationId;
	}

	/**
	 * @hibernate.property column = "PNR_SEG_ID"
	 */
	public Integer getReservationSegId() {
		return reservationSegId;
	}

	public void setReservationSegId(Integer reservationSegId) {
		this.reservationSegId = reservationSegId;
	}

	/**
	 * @hibernate.property column = "NOTIFICATION_TYPE"
	 */
	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * @hibernate.property column = "NOTIFICATION_REF"
	 */
	public Integer getNoficationRefId() {
		return noficationRefId;
	}

	public void setNoficationRefId(Integer noficationRefId) {
		this.noficationRefId = noficationRefId;
	}

}
