/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author M.Rikaz
 * @since 1.0
 */
public class PaxDiscountInfoDTO implements Serializable {

	private static final long serialVersionUID = 5411176147064274436L;

	private BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private double discountPercentage = 0;

	private boolean discountApplied;

	private String dicountCode;

	private boolean creditPromotion;

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public boolean isDiscountApplied() {
		return discountApplied;
	}

	public void setDiscountApplied(boolean discountApplied) {
		this.discountApplied = discountApplied;
	}

	public String getDicountCode() {
		return dicountCode;
	}

	public void setDicountCode(String dicountCode) {
		this.dicountCode = dicountCode;
	}

	public boolean isCreditPromotion() {
		return creditPromotion;
	}

	public void setCreditPromotion(boolean creditPromotion) {
		this.creditPromotion = creditPromotion;
	}

}