/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.adl.comparator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;


/**
 * @author udithad
 *
 */
public class CompPassengerTitle implements Comparator<PassengerInformation> {
	List<String> titleList = new ArrayList<String> (Arrays.asList("MR","MRS","MS","MSTR","MISS"));
    @Override
    public int compare(PassengerInformation arg0, PassengerInformation arg1) {
    	int elf = titleList.indexOf(arg0.getTitle());
    	int ell = titleList.indexOf(arg1.getTitle());
    	int ret=0;
    	if((elf-ell)<0){
    		ret = -1;
    	}else if((elf-ell)> 0){
    		ret = 1;
    	}else if((elf-ell)==0){
    		ret = 0;
    	}
    	return ret;
    }
}
