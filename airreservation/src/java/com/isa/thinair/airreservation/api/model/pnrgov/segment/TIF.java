package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumericSpace;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class TIF extends EDISegment {

	private String e9936;
	private String e6353_1;
	private String e9942;
	private String e6353_2;
	private String e9944;
	private String e9946;

	public TIF() {
		super(EDISegmentTag.TIF);
	}

	public void setPassengerSurname(String e9936) {
		this.e9936 = e9936;
	}

	public void setNameQualifier(String e6353_1) {
		this.e6353_1 = e6353_1;
	}

	public void setPassengerFirstNameAndTitle(String e9942) {
		this.e9942 = e9942;
	}

	public void setPassengerType(String e6353_2) {
		this.e6353_2 = e6353_2;
	}

	public void setTravellerReferenceNumber(String e9944) {
		this.e9944 = e9944;
	}

	public void setTravellerWithInfant(String e9946) {
		this.e9946 = e9946;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C322 = new CompoundDataElement("C322", DE_STATUS.M);
		C322.addBasicDataelement(new AlphaNumericSpace(EDI_ELEMENT.E9936, e9936));
		C322.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E6353, e6353_1));

		CompoundDataElement C324 = new CompoundDataElement("C324", DE_STATUS.C);
		C324.addBasicDataelement(new AlphaNumericSpace(EDI_ELEMENT.E9942, e9942));
		C324.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E6353, e6353_2));
		C324.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9944, e9944));
		C324.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9946, e9946));

		addEDIDataElement(C322);
		addEDIDataElement(C324);

		return this;
	}

}
