package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author rumesh
 * @hibernate.class table = "T_PNR_PAX_ADDITIONAL_INFO"
 */
public class ReservationPaxAdditionalInfo extends Persistent implements Serializable {

	private static final long serialVersionUID = 8258627227977757923L;

	private Integer additionalInfoId;
	// private Integer pnrPaxId;
	private ReservationPax reservationPax;
	private String passportNo;
	private Date passportExpiry;
	private String passportIssuedCntry;
	private String employeeId;
	private Date dateOfJoin;
	private String idCategory;
	private String placeOfBirth; 
	private String travelDocumentType; 
	private String visaDocNumber;
	private String visaDocPlaceOfIssue ;
	private Date visaDocIssueDate; 
	private String visaApplicableCountry;
	private String nationalIDNo;
	private String arrivalIntlFlightNo;
	private Date intlFlightArrivalDate;
	private String departureIntlFlightNo;
	private Date intlFlightDepartureDate;
	private String groupId;
	private String ffid;

	/**
	 * @return the additionalInfoId
	 * @hibernate.id column = "PNR_PAX_ADDITIONAL_INFO_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="s_pnr_pax_additional_info"
	 */
	public Integer getAdditionalInfoId() {
		return additionalInfoId;
	}

	/**
	 * @param additionalInfoId
	 *            the additionalInfoId to set
	 */
	public void setAdditionalInfoId(Integer additionalInfoId) {
		this.additionalInfoId = additionalInfoId;
	}

	/**
	 * @return the passportNo
	 * @hibernate.property column = "PASSPORT_NUMBER"
	 */
	public String getPassportNo() {
		return passportNo;
	}

	/**
	 * @hibernate.many-to-one column="PNR_PAX_ID" class="com.isa.thinair.airreservation.api.model.ReservationPax"
	 * @return Returns the reservationPax.
	 */
	public ReservationPax getReservationPax() {
		return reservationPax;
	}

	/**
	 * @param reservationPax
	 *            the reservationPax to set
	 */
	public void setReservationPax(ReservationPax reservationPax) {
		this.reservationPax = reservationPax;
	}

	/**
	 * @param passportNo
	 *            the passportNo to set
	 */
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	/**
	 * @return the passportExpiry
	 * @hibernate.property column = "PASSPORT_EXPIRY"
	 */
	public Date getPassportExpiry() {
		return passportExpiry;
	}

	/**
	 * @param passportExpiry
	 *            the passportExpiry to set
	 */
	public void setPassportExpiry(Date passportExpiry) {
		this.passportExpiry = passportExpiry;
	}

	/**
	 * @return the passportIssuedCntry
	 * @hibernate.property column = "PASSPORT_ISSUED_CNTRY"
	 */
	public String getPassportIssuedCntry() {
		return passportIssuedCntry;
	}

	/**
	 * @param passportIssuedCntry
	 *            the passportIssuedCntry to set
	 */
	public void setPassportIssuedCntry(String passportIssuedCntry) {
		this.passportIssuedCntry = passportIssuedCntry;
	}

	/**
	 * @return the employeeId
	 * @hibernate.property column = "EMPLOYEE_ID"
	 */
	public String getEmployeeID() {
		return employeeId;
	}

	/**
	 * 
	 * @param employeeID
	 */
	public void setEmployeeID(String employeeID) {
		this.employeeId = employeeID;
	}

	/**
	 * @return the dateOfJoin
	 * @hibernate.property column = "DATE_OF_JOIN"
	 */
	public Date getDateOfJoin() {
		return dateOfJoin;
	}

	/**
	 * 
	 * @param dateOfJoin
	 */
	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	/**
	 * @return the idCategory
	 * @hibernate.property column = "ID_CATEGORY"
	 */
	public String getIdCategory() {
		return idCategory;
	}

	/**
	 * 
	 * @param idCategory
	 */
	public void setIdCategory(String idCategory) {
		this.idCategory = idCategory;
	}

	/**
	 * @return the placeOfBirth
	 * @hibernate.property column = "PLACE_OF_BIRTH"
	 */
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	/**
	 * @param placeOfBirth the placeOfBirth to set
	 */
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	/**
	 * @return the travelDocumentType
	 * @hibernate.property column = "TRAVEL_DOC_TYPE"
	 */
	public String getTravelDocumentType() {
		return travelDocumentType;
	}

	/**
	 * @param travelDocumentType the travelDocumentType to set
	 */
	public void setTravelDocumentType(String travelDocumentType) {
		this.travelDocumentType = travelDocumentType;
	}

	/**
	 * @return the visaDocNumber
	 * @hibernate.property column = "VISA_DOC_NUMBER"
	 */
	public String getVisaDocNumber() {
		return visaDocNumber;
	}

	/**
	 * @param visaDocNumber the visaDocNumber to set
	 */
	public void setVisaDocNumber(String visaDocNumber) {
		this.visaDocNumber = visaDocNumber;
	}

	/**
	 * @return the visaDocPlaceOfIssue
	 * @hibernate.property column = "VISA_DOC_PLACE_OF_ISSUE"
	 */
	public String getVisaDocPlaceOfIssue() {
		return visaDocPlaceOfIssue;
	}

	/**
	 * @param visaDocPlaceOfIssue the visaDocPlaceOfIssue to set
	 */
	public void setVisaDocPlaceOfIssue(String visaDocPlaceOfIssue) {
		this.visaDocPlaceOfIssue = visaDocPlaceOfIssue;
	}

	/**
	 * @return the visaDocIssueDate
	 * @hibernate.property column = "VISA_DOC_ISSUE_DATE"
	 */
	public Date getVisaDocIssueDate() {
		return visaDocIssueDate;
	}

	/**
	 * @param visaDocIssueDate the visaDocIssueDate to set
	 */
	public void setVisaDocIssueDate(Date visaDocIssueDate) {
		this.visaDocIssueDate = visaDocIssueDate;
	}

	/**
	 * @return the visaApplicableCountry
	 * @hibernate.property column = "VISA_APPLICABLE_COUNTRY"
	 */
	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	/**
	 * @param visaApplicableCountry the visaApplicableCountry to set
	 */
	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}

	/**
	 * @return the nationalIDNo
	 * @hibernate.property column = "NATIONALID_NO"
	 */
	public String getNationalIDNo() {
		return nationalIDNo;
	}

	public void setNationalIDNo(String nationalIDNo) {
		this.nationalIDNo = nationalIDNo;
	}

	/**
	 * @return the arrivalIntlFlightNo
	 * @hibernate.property column = "ARRIVAL_INTL_FLIGHTNO"
	 */
	public String getArrivalIntlFlightNo() {
		return arrivalIntlFlightNo;
	}

	/**
	 * @arrivalIntlFlightNo
	 * 
	 */
	public void setArrivalIntlFlightNo(String arrivalIntlFlightNo) {
		this.arrivalIntlFlightNo = arrivalIntlFlightNo;
	}

	/**
	 * @return the intlFlightArrivalDate
	 * @hibernate.property column = "ARRIVAL_DATE"
	 */
	public Date getIntlFlightArrivalDate() {
		return intlFlightArrivalDate;
	}

	/**
	 * @intlFlightArrivalDate
	 * 
	 */
	public void setIntlFlightArrivalDate(Date intlFlightArrivalDate) {
		this.intlFlightArrivalDate = intlFlightArrivalDate;
	}

	/**
	 * @return the departureIntlFlightNo
	 * @hibernate.property column = "DEPARTURE_INTL_FLIGHTNO"
	 */
	public String getDepartureIntlFlightNo() {
		return departureIntlFlightNo;
	}

	/**
	 * @departureIntlFlightNo
	 * 
	 */
	public void setDepartureIntlFlightNo(String departureIntlFlightNo) {
		this.departureIntlFlightNo = departureIntlFlightNo;
	}

	/**
	 * @return the intlFlightDepartureDate
	 * @hibernate.property column = "DEPARTURE_DATE"
	 */
	public Date getIntlFlightDepartureDate() {
		return intlFlightDepartureDate;
	}

	/**
	 * @intlFlightDepartureDate
	 * 
	 */
	public void setIntlFlightDepartureDate(Date intlFlightDepartureDate) {
		this.intlFlightDepartureDate = intlFlightDepartureDate;
	}

	/**
	 * @return the groupId
	 * @hibernate.property column = "GROUP_ID"
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @return the groupId
	 * 
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the ffid
	 * @hibernate.property column = "FFID"
	 */
	public String getFfid() {
		return ffid;
	}

	/**
	 * @param ffid the ffid to set
	 */
	public void setFfid(String ffid) {
		this.ffid = ffid;
	}
}
