/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * To hold reservation search data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationSearchDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds PNR Number */
	private String pnr;

	/** Holds the originator pnr */
	private String originatorPnr;
	
	/** Holds the external record locator */
	private String invoiceNumber;

	/* */
	private String externalRecordLocator;

	/** Holds first Name */
	private String firstName;

	/** Holds last Name */
	private String lastName;

	/** Holds from airport name */
	private String fromAirport;

	/** Holds to airport name */
	private String toAirport;

	/** Holds depature date */
	private Date departureDate;

	/** Holds early depature date */
	private Date earlyDepartureDate;

	/** Holds return date */
	private Date returnDate;

	/** Holds telephone number */
	private String telephoneNo;

	/** Holds customer id */
	private Integer customerId;

	/** Holds email */
	private String email;

	/** Holds flight number */
	private String flightNo;

	/** Holds owner channel id */
	private Integer ownerChannelId;

	/** Holds owner agent code */
	private String ownerAgentCode;

	/** Holds the origin channel id(s) */
	private Collection<Integer> originChannelIds;

	/** Holds origin agent code */
	private String originAgentCode;

	/** Holds origin user id */
	private String originUserId;

	/** Holds the reservation states */
	@SuppressWarnings("rawtypes")
	private Collection reservationStates;

	/** Holds the whether to filter zero credits */
	private boolean filterZeroCredits;

	/** Holds whether to include passenger information */
	private boolean includePaxInfo;

	/** Holds E Ticket Number */
	private String eTicket;

	/** Holds Passport */
	private String passport;

	/** Holds the booking class type */
	private String bookingClassType;

	/** Holds the xbe page number **/
	private int pageNo = 1;

	private String telephoneNoForSQL;

	private boolean isExactMatch;

	private boolean searchOtherAirlines;

	/** boolean to identify search own system or all systems **/
	private boolean isSearchAll;

	private Collection<String> bookingCategories;

	private boolean searchGSABookings;

	private boolean searchReportingAgentBookings;

	private boolean searchAllReportingAgentBookings;

	private boolean searchIBEBookings;

	private boolean departingTimeInclusive;
	
	private Collection<String> searchableCarriers;
	
	private String externalETicket;

	/**
	 * @return Returns the departureDate.
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            The departureDate to set.
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the fromAirport.
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param fromAirport
	 *            The fromAirport to set.
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the returnDate.
	 */
	public Date getReturnDate() {
		return returnDate;
	}

	/**
	 * @param returnDate
	 *            The returnDate to set.
	 */
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	/**
	 * @return Returns the telephoneNo.
	 */
	public String getTelephoneNo() {
		return telephoneNo;
	}

	/**
	 * @param telephoneNo
	 *            The telephoneNo to set.
	 */
	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}

	/**
	 * @return Returns the toAirport.
	 */
	public String getToAirport() {
		return toAirport;
	}

	/**
	 * @param toAirport
	 *            The toAirport to set.
	 */
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	/**
	 * @return Returns the customerId.
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return Returns the earlyDepartureDate.
	 */
	public Date getEarlyDepartureDate() {
		return earlyDepartureDate;
	}

	/**
	 * @param earlyDepartureDate
	 *            The earlyDepartureDate to set.
	 */
	public void setEarlyDepartureDate(Date earlyDepartureDate) {
		this.earlyDepartureDate = earlyDepartureDate;
	}

	/**
	 * @return the flight number
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            the flight number
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return Returns the originAgentCode.
	 */
	public String getOriginAgentCode() {
		return originAgentCode;
	}

	/**
	 * @param originAgentCode
	 *            The originAgentCode to set.
	 */
	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}

	/**
	 * @return Returns the originUserId.
	 */
	public String getOriginUserId() {
		return originUserId;
	}

	/**
	 * @param originUserId
	 *            The originUserId to set.
	 */
	public void setOriginUserId(String originUserId) {
		this.originUserId = originUserId;
	}

	/**
	 * @return Returns the ownerAgentCode.
	 */
	public String getOwnerAgentCode() {
		return ownerAgentCode;
	}

	/**
	 * @param ownerAgentCode
	 *            The ownerAgentCode to set.
	 */
	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	/**
	 * @return Returns the ownerChannelId.
	 */
	public Integer getOwnerChannelId() {
		return ownerChannelId;
	}

	/**
	 * @param ownerChannelId
	 *            The ownerChannelId to set.
	 */
	public void setOwnerChannelId(Integer ownerChannelId) {
		this.ownerChannelId = ownerChannelId;
	}

	/**
	 * @return Returns the filterZeroCredits.
	 */
	public boolean isFilterZeroCredits() {
		return filterZeroCredits;
	}

	/**
	 * @param filterZeroCredits
	 *            The filterZeroCredits to set.
	 */
	public void setFilterZeroCredits(boolean filterZeroCredits) {
		this.filterZeroCredits = filterZeroCredits;
	}

	/**
	 * @return Returns the reservationStates.
	 */
	@SuppressWarnings("rawtypes")
	public Collection getReservationStates() {
		return reservationStates;
	}

	/**
	 * @param reservationStates
	 *            The reservationStates to set.
	 */
	@SuppressWarnings("rawtypes")
	private void setReservationStates(Collection reservationStates) {
		this.reservationStates = reservationStates;
	}

	/**
	 * Add reservation status
	 * 
	 * @param reservationStatus
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addReservationStatus(String reservationStatus) {
		if (this.getReservationStates() == null) {
			this.setReservationStates(new ArrayList());
		}

		this.getReservationStates().add(reservationStatus);
	}

	/**
	 * Return whether to include pax info in the response.
	 * 
	 * @return
	 */
	public boolean isIncludePaxInfo() {
		return includePaxInfo;
	}

	/**
	 * Sets whether to include pax info in the response.
	 * 
	 * @param includePaxInfo
	 */
	public void setIncludePaxInfo(boolean includePaxInfo) {
		this.includePaxInfo = includePaxInfo;
	}

	/**
	 * @return Returns the originChannelIds.
	 */
	public Collection<Integer> getOriginChannelIds() {
		return originChannelIds;
	}

	/**
	 * @param originChannelIds
	 *            The originChannelIds to set.
	 */
	public void setOriginChannelIds(Collection<Integer> originChannelIds) {
		this.originChannelIds = originChannelIds;
	}

	/**
	 * @return Returns the eTicket.
	 */
	public String getETicket() {
		return eTicket;
	}

	/**
	 * @param eticket
	 *            The eTicket to set.
	 */
	public void setETicket(String eticket) {
		this.eTicket = eticket;
	}

	/**
	 * @return Returns the passport.
	 */
	public String getPassport() {
		return passport;
	}

	/**
	 * @param passport
	 *            The passport to set.
	 */
	public void setPassport(String passport) {
		this.passport = passport;
	}

	/**
	 * @return the originatorPnr
	 */
	public String getOriginatorPnr() {
		return originatorPnr;
	}

	/**
	 * @param originatorPnr
	 *            the originatorPnr to set
	 */
	public void setOriginatorPnr(String originatorPnr) {
		this.originatorPnr = originatorPnr;
	}

	/**
	 * @return the bookingClassType
	 */
	public String getBookingClassType() {
		return bookingClassType;
	}

	/**
	 * @param bookingClassType
	 *            the bookingClassType to set
	 */
	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String getTelephoneNoForSQL() {
		return telephoneNoForSQL;
	}

	public void setTelephoneNoForSQL(String telephoneNoForSQL) {
		this.telephoneNoForSQL = telephoneNoForSQL;
	}

	public boolean isExactMatch() {
		return isExactMatch;
	}

	public void setExactMatch(boolean isExactMatch) {
		this.isExactMatch = isExactMatch;
	}

	/**
	 * @return the searchOtherAirlines
	 */
	public boolean isSearchOtherAirlines() {
		return searchOtherAirlines;
	}

	/**
	 * @param searchOtherAirlines
	 *            the searchOtherAirlines to set
	 */
	public void setSearchOtherAirlines(boolean searchOtherAirlines) {
		this.searchOtherAirlines = searchOtherAirlines;
	}

	/**
	 * @return the isSearchAll
	 */
	public boolean isSearchAll() {
		return isSearchAll;
	}

	/**
	 * @param isSearchAll
	 *            the isSearchAll to set
	 */
	public void setSearchAll(boolean isSearchAll) {
		this.isSearchAll = isSearchAll;
	}

	/**
	 * @return
	 */
	public Collection<String> getBookingCategories() {
		return bookingCategories;
	}

	/**
	 * @param bookingCategories
	 */
	public void setBookingCategories(Collection<String> bookingCategories) {
		this.bookingCategories = bookingCategories;
	}

	/**
	 * @return
	 */
	public boolean isSearchGSABookings() {
		return searchGSABookings;
	}

	/**
	 * @param searchGSABookings
	 */
	public void setSearchGSABookings(boolean searchGSABookings) {
		this.searchGSABookings = searchGSABookings;
	}

	public boolean isSearchIBEBookings() {
		return searchIBEBookings;
	}

	public void setSearchIBEBookings(boolean searchIBEBookings) {
		this.searchIBEBookings = searchIBEBookings;
	}

	public boolean isDepartingTimeInclusive() {
		return departingTimeInclusive;
	}

	public void setDepartingTimeInclusive(boolean departingTimeInclusive) {
		this.departingTimeInclusive = departingTimeInclusive;
	}

	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	/**
	 * @return the searchableCarriers
	 */
	public Collection<String> getSearchableCarriers() {
		return searchableCarriers;
	}

	/**
	 * @param searchableCarriers the searchableCarriers to set
	 */
	public void setSearchableCarriers(Collection<String> searchableCarriers) {
		this.searchableCarriers = searchableCarriers;
	}

	public String getExternalETicket() {
		return externalETicket;
	}

	public void setExternalETicket(String externalETicket) {
		this.externalETicket = externalETicket;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public boolean isSearchReportingAgentBookings() {
		return searchReportingAgentBookings;
	}

	public void setSearchReportingAgentBookings(boolean searchReportingAgentBookings) {
		this.searchReportingAgentBookings = searchReportingAgentBookings;
	}

	public boolean isSearchAllReportingAgentBookings() {
		return searchAllReportingAgentBookings;
	}

	public void setSearchAllReportingAgentBookings(boolean searchAllReportingAgentBookings) {
		this.searchAllReportingAgentBookings = searchAllReportingAgentBookings;
	}
}