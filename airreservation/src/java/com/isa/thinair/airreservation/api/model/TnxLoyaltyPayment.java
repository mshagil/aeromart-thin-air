package com.isa.thinair.airreservation.api.model;

/**
 * Hold reservation transaction loyalty payments
 * 
 * @author rumesh
 * 
 */
public class TnxLoyaltyPayment extends TnxPayment {

	private static final long serialVersionUID = -6697459760185672927L;

	private String loyaltyMemberAccountId;

	private String[] rewardIDs;

	public String getLoyaltyMemberAccountId() {
		return loyaltyMemberAccountId;
	}

	public void setLoyaltyMemberAccountId(String loyaltyMemberAccountId) {
		this.loyaltyMemberAccountId = loyaltyMemberAccountId;
	}

	public String[] getRewardIDs() {
		return rewardIDs;
	}

	public void setRewardIDs(String[] rewardIDs) {
		this.rewardIDs = rewardIDs;
	}

}
