/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**
 * To hold track information data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class TrackInfoDTO extends BasicTrackInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Holds the ip address */
	private String ipAddress;

	/** Holds the origin ip number calculated from the ip address */
	private Long originIPNumber;

	private String carrierCode;

	/** Holds the marketing user id */
	private String marketingUserId;

	/** Holds the marketing agent code */
	private String marketingAgentCode;
	
	/** Holds the direct bill id */
	private String directBillId;
	
	/** Holds user Marketing Agent Station Code */
	private String marketingAgentStationCode;

	/** Holds the payment agent code */
	private String paymentAgent;

	/** Holds the application type */
	private AppIndicatorEnum appIndicator;
	
	/** Holds the origin user id */
	private String originUserId;

	/** Holds the origin agent code */
	private String originAgentCode;
	
	/** Holds origin channel id */
	private Integer originChannelId;
	
	/** Holds cuntomer Id */
	private String customerId;
	
	/** Holds actual channel code for lcc bookings eg XBE-3, IBE-4  **/
	private int marketingBookingChannel;

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the originIPNumber
	 */
	public Long getOriginIPNumber() {
		return originIPNumber;
	}

	/**
	 * @param originIPNumber
	 *            the originIPNumber to set
	 */
	public void setOriginIPNumber(Long originIPNumber) {
		this.originIPNumber = originIPNumber;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the marketingUserId
	 */
	public String getMarketingUserId() {
		return marketingUserId;
	}

	/**
	 * @param marketingUserId
	 *            the marketingUserId to set
	 */
	public void setMarketingUserId(String marketingUserId) {
		this.marketingUserId = marketingUserId;
	}

	/**
	 * @return the marketingAgentCode
	 */
	public String getMarketingAgentCode() {
		return marketingAgentCode;
	}

	/**
	 * @param marketingAgentCode
	 *            the marketingAgentCode to set
	 */
	public void setMarketingAgentCode(String marketingAgentCode) {
		this.marketingAgentCode = marketingAgentCode;
	}

	/**
	 * @return the marketingAgentStationCode
	 */
	public String getMarketingAgentStationCode() {
		return marketingAgentStationCode;
	}

	/**
	 * @param marketingAgentStationCode
	 *            the marketingAgentStationCode to set
	 */
	public void setMarketingAgentStationCode(String marketingAgentStationCode) {
		this.marketingAgentStationCode = marketingAgentStationCode;
	}

	/**
	 * @return
	 */
	public String getPaymentAgent() {
		return paymentAgent;
	}

	/**
	 * @param paymentAgent
	 */
	public void setPaymentAgent(String paymentAgent) {
		this.paymentAgent = paymentAgent;
	}

	/**
	 * @return the originUserId
	 */
	public String getOriginUserId() {
		return originUserId;
	}

	/**
	 * @param originUserId the originUserId to set
	 */
	public void setOriginUserId(String originUserId) {
		this.originUserId = originUserId;
	}

	/**
	 * @return the originAgentCode
	 */
	public String getOriginAgentCode() {
		return originAgentCode;
	}

	/**
	 * @param originAgentCode the originAgentCode to set
	 */
	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}

	/**
	 * @return
	 */
	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	/**
	 * @param appIndicator
	 */
	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	/**
	 * @param appIndicator : String value of appIndicator 
	 */
	public void setAppIndicator(String appIndicator){
		this.appIndicator = AppIndicatorEnum.fromValue(appIndicator);
	}
	/**
	 * @return the originChannelId
	 */
	public Integer getOriginChannelId() {
		return originChannelId;
	}

	/**
	 * @param originChannelId the originChannelId to set
	 */
	public void setOriginChannelId(Integer originChannelId) {
		this.originChannelId = originChannelId;
	}

	public String getDirectBillId() {
		return directBillId;
	}

	public void setDirectBillId(String directBillId) {
		this.directBillId = directBillId;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public int getMarketingBookingChannel() {
		return marketingBookingChannel;
	}

	public void setMarketingBookingChannel(int marketingBookingChannel) {
		this.marketingBookingChannel = marketingBookingChannel;
	}	
	
}