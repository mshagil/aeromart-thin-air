package com.isa.thinair.airreservation.api.dto.onlinecheckin;

public class PNRInfoDTO {

	private String pnr;
	private String firstName;
	private String lastName;
	private String email;
	private String prefferdLanguage;
	private String title;
	private Integer pnrSegId;
	private Integer pnrSegNotificationEventId;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPrefferdLanguage() {
		return prefferdLanguage;
	}

	public void setPrefferdLanguage(String prefferdLanguage) {
		this.prefferdLanguage = prefferdLanguage;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegNotificationEventId(Integer pnrSegNotificationEventId) {
		this.pnrSegNotificationEventId = pnrSegNotificationEventId;
	}

	public Integer getPnrSegNotificationEventId() {
		return pnrSegNotificationEventId;
	}

}
