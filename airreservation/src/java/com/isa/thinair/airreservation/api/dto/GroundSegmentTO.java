package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class GroundSegmentTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3446778454870059792L;

	private String selectedOnd;

	private boolean isAddGroundSegment;

	private boolean groupPNRNo;

	private String operatingCarrier;

	public String getSelectedOnd() {
		return selectedOnd;
	}

	public void setSelectedOnd(String selectedOnd) {
		this.selectedOnd = selectedOnd;
	}

	public boolean isAddGroundSegment() {
		return isAddGroundSegment;
	}

	public void setAddGroundSegment(boolean isAddGroundSegment) {
		this.isAddGroundSegment = isAddGroundSegment;
	}

	public boolean isGroupPNRNo() {
		return groupPNRNo;
	}

	public void setGroupPNRNo(boolean groupPNRNo) {
		this.groupPNRNo = groupPNRNo;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

}
