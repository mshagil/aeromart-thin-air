package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.Alpha;
import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.BasicDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class UNH extends EDISegment {

	private String E0062;
	private String E0065;
	private String E0052;
	private String E0054;
	private String E0051;
	private String E0057;
	private String E0068;
	private String E0070;
	private String E0073;

	public UNH() {
		super(EDISegmentTag.UNH);
	}

	public void setMessageReferenceNumber(String e0062) {
		E0062 = e0062;
	}

	public void setMessageType(String e0065) {
		E0065 = e0065;
	}

	public void setMessageVersionNumber(String e0052) {
		E0052 = e0052;
	}

	public void setMessageReleaseNumber(String e0054) {
		E0054 = e0054;
	}

	public void setControllingAgency(String e0051) {
		E0051 = e0051;
	}

	public void setAssociationAssignedCode(String e0057) {
		E0057 = e0057;
	}

	public void setCommonAccessReference(String e0068) {
		E0068 = e0068;
	}

	public void setSequenceOfTransfer(String e0070) {
		E0070 = e0070;
	}

	public void setFirstAndLastTransfer(String e0073) {
		E0073 = e0073;
	}

	@Override
	public MessageSegment build() {
		BasicDataElement V0062 = new AlphaNumeric(EDI_ELEMENT.E0062, E0062);

		CompoundDataElement S009 = new CompoundDataElement("S009", DE_STATUS.M);
		S009.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0065, E0065));
		S009.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0052, E0052));
		S009.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0054, E0054));
		S009.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0051, E0051));
		S009.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0057, E0057));

		BasicDataElement V0068 = new AlphaNumeric(EDI_ELEMENT.E0068, E0068);

		CompoundDataElement S010 = new CompoundDataElement("S010", DE_STATUS.C);
		S010.addBasicDataelement(new Numeric(EDI_ELEMENT.E0070, E0070));
		S010.addBasicDataelement(new Alpha(EDI_ELEMENT.E0073, E0073));

		addEDIDataElement(V0062);
		addEDIDataElement(S009);
		addEDIDataElement(V0068);
		addEDIDataElement(S010);

		return this;
	}
}
