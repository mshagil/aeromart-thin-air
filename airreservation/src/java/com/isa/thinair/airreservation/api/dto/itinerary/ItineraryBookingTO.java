package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class ItineraryBookingTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String GDSPNR;

	private String marketingCarrier;

	/** Map(AirlineCode, PNR) */
	private Map<String, String> PNR;

	/** Map(AirlineCode, AirlineName) */
	private Map<String, String> airlineNames;

	private String status; // STANDBY (if all segments are standby); CONFIRMED;
							// ONHOLD (if not one of the above two)

	private Date bookingDate;

	private Date dateOfIssue;

	private Date releaseDate;

	private String bookingDateInPersian;

	private String dateOfIssueInPersian;

	private String releaseDateTimeInPersian;

	private ItineraryFinancialTotalsTO baseCurrencyFinancials;

	private ItineraryFinancialTotalsTO paidCurrencyFinancials;

	private boolean hasInfants;

	private String barcode;

	private String locale;

	private String itineraryFareMask;

	private Date ticketExpiryDate;

	private String ticketExpiryDateInPersian;

	private boolean showTicketExpiryDate;

	private String itineraryFareMaskLabel;

	private boolean itinChargesMask;

	private boolean itinBallanceMask;

	private boolean itinPaidAmountMask;

	private boolean persianDatesSet;

	private String autoCancellationTime;
	
	private boolean isForced;

	public boolean isForced() {
		return isForced;
	}

	public void setForced(boolean isForced) {
		this.isForced = isForced;
	}

	public boolean isPersianDatesSet() {
		return persianDatesSet;
	}

	public void setIsPersianDatesSet(boolean usePersianDates) {
		this.persianDatesSet = usePersianDates;
	}

	public String getBookingDateInPersian() {
		return bookingDateInPersian;
	}

	public void setBookingDateInPersian(String bookingDateInPersian) {
		this.bookingDateInPersian = bookingDateInPersian;
	}

	public String getDateOfIssueInPersian() {
		return dateOfIssueInPersian;
	}

	public void setDateOfIssueInPersian(String dateOfIssueInPersian) {
		this.dateOfIssueInPersian = dateOfIssueInPersian;
	}

	public String getReleaseDateTimeInPersian() {
		return releaseDateTimeInPersian;
	}

	public void setReleaseDateTimeInPersian(String releaseDateTimeInPersian) {
		this.releaseDateTimeInPersian = releaseDateTimeInPersian;
	}

	public String getBookingDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(bookingDate, dateTimeFormat, locale);
	}

	public String getReleaseDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(releaseDate, dateTimeFormat, locale);
	}

	public String getTicketExpiryDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(ticketExpiryDate, dateTimeFormat, locale);
	}

	public String getDateOfIssue(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(dateOfIssue, dateTimeFormat, locale);
	}

	public String getTicketExpiryDateInPersian() {
		return ticketExpiryDateInPersian;
	}

	public void setTicketExpiryDateInPersian(String ticketExpiryDateInPersian) {
		this.ticketExpiryDateInPersian = ticketExpiryDateInPersian;
	}

	public boolean hasDateOfIssue() {
		return dateOfIssue != null;
	}

	/**
	 * @return the GDSPNR
	 */
	public String getGDSPNR() {
		return GDSPNR;
	}

	/**
	 * @param GDSPNR
	 *            the GDSPNR to set
	 */
	public void setGDSPNR(String GDSPNR) {
		this.GDSPNR = GDSPNR;
	}

	/**
	 * @return the pNR
	 */
	public Map<String, String> getPNR() {
		return PNR;
	}

	/**
	 * @param pNR
	 *            the pNR to set
	 */
	public void setPNR(Map<String, String> pNR) {
		PNR = pNR;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param bookingDate
	 *            the bookingDate to set
	 */
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 * @param bookingDate
	 *            the bookingDate to set
	 */
	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	/**
	 * @param releaseDate
	 *            the releaseDate to set
	 */
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * @return the baseCurrencyFinancials
	 */
	public ItineraryFinancialTotalsTO getBaseCurrencyFinancials() {
		return baseCurrencyFinancials;
	}

	/**
	 * @param baseCurrencyFinancials
	 *            the baseCurrencyFinancials to set
	 */
	public void setBaseCurrencyFinancials(ItineraryFinancialTotalsTO baseCurrencyFinancials) {
		this.baseCurrencyFinancials = baseCurrencyFinancials;
	}

	/**
	 * @return the paidCurrencyFinancials
	 */
	public ItineraryFinancialTotalsTO getPaidCurrencyFinancials() {
		return paidCurrencyFinancials;
	}

	/**
	 * @param paidCurrencyFinancials
	 *            the paidCurrencyFinancials to set
	 */
	public void setPaidCurrencyFinancials(ItineraryFinancialTotalsTO paidCurrencyFinancials) {
		this.paidCurrencyFinancials = paidCurrencyFinancials;
	}

	/**
	 * @return the hasInfants
	 */
	public boolean isHasInfants() {
		return hasInfants;
	}

	/**
	 * @param hasInfants
	 *            the hasInfants to set
	 */
	public void setHasInfants(boolean hasInfants) {
		this.hasInfants = hasInfants;
	}

	/**
	 * @return the airlineNames
	 */
	public Map<String, String> getAirlineNames() {
		return airlineNames;
	}

	/**
	 * @param airlineNames
	 *            the airlineNames to set
	 */
	public void setAirlineNames(Map<String, String> airlineNames) {
		this.airlineNames = airlineNames;
	}

	/**
	 * @return the marketingCarrier
	 */
	public String getMarketingCarrier() {
		return marketingCarrier;
	}

	/**
	 * @param marketingCarrier
	 *            the marketingCarrier to set
	 */
	public void setMarketingCarrier(String marketingCarrier) {
		this.marketingCarrier = marketingCarrier;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getItineraryFareMask() {
		return itineraryFareMask;
	}

	public void setItineraryFareMask(String itineraryFareMask) {
		this.itineraryFareMask = itineraryFareMask;
	}

	public void setTicketExpiryDate(Date ticketExpiryDate) {
		this.ticketExpiryDate = ticketExpiryDate;
	}

	public boolean isShowTicketExpiryDate() {
		return showTicketExpiryDate;
	}

	public void setShowTicketExpiryDate(boolean showTicketExpiryDate) {
		this.showTicketExpiryDate = showTicketExpiryDate;
	}

	public String getItineraryFareMaskLabel() {
		return itineraryFareMaskLabel;
	}

	public void setItineraryFareMaskLabel(String itineraryFareMaskLabel) {
		this.itineraryFareMaskLabel = itineraryFareMaskLabel;
	}

	public boolean isItinChargesMask() {
		return itinChargesMask;
	}

	public void setItinChargesMask(boolean itinChargesMask) {
		this.itinChargesMask = itinChargesMask;
	}

	public boolean isItinBallanceMask() {
		return itinBallanceMask;
	}

	public void setItinBallanceMask(boolean itinBallanceMask) {
		this.itinBallanceMask = itinBallanceMask;
	}

	public boolean isItinPaidAmountMask() {
		return itinPaidAmountMask;
	}

	public void setItinPaidAmountMask(boolean itinPaidAmountMask) {
		this.itinPaidAmountMask = itinPaidAmountMask;
	}

	public String getAutoCancellationTime() {
		return autoCancellationTime;
	}

	public void setAutoCancellationTime(String autoCancellationTime) {
		this.autoCancellationTime = autoCancellationTime;
	}

}