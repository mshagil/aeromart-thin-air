/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.utils;

import javax.sql.DataSource;

import com.isa.thinair.aircustomer.api.service.LoyaltyCreditServiceBD;
import com.isa.thinair.aircustomer.api.utils.AircustomerConstants;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.AirportTransferBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.GdsBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airproxy.api.service.AirproxyReservationBD;
import com.isa.thinair.airproxy.api.service.AirproxySegmentBD;
import com.isa.thinair.airproxy.api.utils.AirproxyConstants;
import com.isa.thinair.airreservation.api.service.BlacklistPAXBD;
import com.isa.thinair.airreservation.api.service.CreditAccountBD;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.RevenueAccountBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.core.bl.external.config.ExternalSystemsConfig;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.invoicing.api.service.InvoicingBD;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.lccclient.api.service.LCCPaxCreditBD;
import com.isa.thinair.lccclient.api.service.LCCReservationBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messagepasser.api.service.ETLBD;
import com.isa.thinair.messagepasser.api.utils.MessagepasserConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.msgbroker.api.service.ETSTicketGenerationServiceBD;
import com.isa.thinair.msgbroker.api.service.ETSTicketRefundServiceBD;
import com.isa.thinair.msgbroker.api.service.ETSTicketServiceBD;
import com.isa.thinair.msgbroker.api.service.ETSTicketVoidServiceBD;
import com.isa.thinair.msgbroker.api.service.MessageBrokerServiceBD;
import com.isa.thinair.msgbroker.api.service.PFSETLServiceBD;
import com.isa.thinair.msgbroker.api.service.PNLADLServiceBD;
import com.isa.thinair.msgbroker.api.service.TypeBServiceBD;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.promotion.api.service.AnciOfferBD;
import com.isa.thinair.promotion.api.service.BunldedFareBD;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.service.PromotionCriteriaAdminBD;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduler.api.service.SchedulerBD;
import com.isa.thinair.scheduler.api.utils.SchedulerConstants;
import com.isa.thinair.wsclient.api.service.DCSClientBD;
import com.isa.thinair.wsclient.api.service.InsuranceClientBD;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

/**
 * Air Reservation module specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationModuleUtils {

	/**
	 * Returns IModule instance for air reservation module
	 * 
	 * @return
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(AirreservationConstants.MODULE_NAME);
	}

	/**
	 * Returns the AirReservationConfig instance for air reservation module
	 * 
	 * @return
	 */
	public static AirReservationConfig getAirReservationConfig() {
		return (AirReservationConfig) getInstance().getModuleConfig();
	}

	/**
	 * Return the global confirguration
	 * 
	 * @return
	 */
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}

	/**
	 * Returns air reservation module data source
	 * 
	 * @return
	 */
	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}
		
	public static DataSource getDataSource(String type) {
		String dataSourceBean = null;
		if (!AppSysParamsUtil.isReportingDbAvailable() || type == null
				|| type.equals(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT)) {
			dataSourceBean = PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN;
		} else {
			dataSourceBean = PlatformBeanUrls.Commons.REPORT_DATA_SOURCE_BEAN;
		}
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(dataSourceBean);
	}

	/**
	 * Returns lcc data source.
	 * 
	 * @return
	 */
	public static DataSource getLCCDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_LCC_DATA_SOURCE_BEAN);
	}

	/**
	 * Returns the external data source
	 * 
	 * @return
	 */
	public static XDBConnectionUtil getExternalDBConnectionUtil() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (XDBConnectionUtil) lookup.getBean("isa:base://modules?id=myXDBDataSourceUtil");
	}

	/**
	 * Returns Bean for air reservation module
	 * 
	 * @param beanName
	 * @return
	 */
	public static Object getBean(String beanName) {
		return LookupServiceFactory.getInstance().getModule(AirreservationConstants.MODULE_NAME).getLocalBean(beanName);
	}

	/**
	 * Returns EJB2 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupEJB2ServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getAirReservationConfig(),
				"airreservation", "airreservations.config.dependencymap.invalid");
	}

	/**
	 * Returns EJB3 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirReservationConfig(), "airreservation",
				"airreservations.config.dependencymap.invalid");
	}

	/**
	 * Return transparent flight inventory business delegate
	 * 
	 * @return
	 */
	public static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) ReservationModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				FlightInventoryBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight business delegate
	 * 
	 * @return
	 */
	public static FlightBD getFlightBD() {
		return (FlightBD) ReservationModuleUtils.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	/**
	 * Return transparent revenue account delegate
	 * 
	 * @return
	 */
	public static RevenueAccountBD getRevenueAccountBD() {
		return (RevenueAccountBD) ReservationModuleUtils.lookupEJB2ServiceBD(AirreservationConstants.MODULE_NAME,
				AirreservationConstants.BDKeys.REVENUEACCOUNT_SERVICE);
	}

	/**
	 * Return transparent payment broker delegate
	 * 
	 * @return
	 */
	public static PaymentBrokerBD getPaymentBrokerBD() {
		return (PaymentBrokerBD) ReservationModuleUtils.lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME,
				PaymentBrokerBD.SERVICE_NAME);
	}

	/**
	 * Return transparent revenue account delegate
	 * 
	 * @return
	 */
	public static CreditAccountBD getCreditAccountBD() {
		return (CreditAccountBD) ReservationModuleUtils.lookupEJB2ServiceBD(AirreservationConstants.MODULE_NAME,
				AirreservationConstants.BDKeys.CREDITACCOUNT_SERVICE);
	}

	/**
	 * Return transparent flight inventory reservation delegate
	 * 
	 * @return
	 */
	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) ReservationModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				FlightInventoryResBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight inventory reservation delegate
	 * 
	 * @return
	 */
	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) ReservationModuleUtils.lookupEJB3Service(MessagingConstants.MODULE_NAME,
				MessagingServiceBD.SERVICE_NAME);
	}

	/**
	 * Return transparent Crypto delegate
	 * 
	 * @return
	 */
	public static CryptoServiceBD getCryptoServiceBD() {
		return (CryptoServiceBD) ReservationModuleUtils.lookupEJB2ServiceBD(CryptoConstants.MODULE_NAME,
				CryptoConstants.BDKeys.CRYPTO_SERVICE);
	}

	/**
	 * Return transparent travel agent finance delegate
	 * 
	 * @return
	 */
	public static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) ReservationModuleUtils.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
				TravelAgentFinanceBD.SERVICE_NAME);
	}

	/**
	 * Return transparent travel agent delegate
	 * 
	 * @return
	 */
	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) ReservationModuleUtils.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
				TravelAgentBD.SERVICE_NAME);
	}

	/**
	 * Return transparent fare delegate
	 * 
	 * @return
	 */
	public static FareBD getFareBD() {
		return (FareBD) ReservationModuleUtils.lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	/**
	 * Return transparent charge delegate
	 * 
	 * @return
	 */
	public static ChargeBD getChargeBD() {
		return (ChargeBD) ReservationModuleUtils.lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	/**
	 * Return transparent auditor delegate
	 * 
	 * @return
	 */
	public static AuditorBD getAuditorBD() {
		return (AuditorBD) ReservationModuleUtils.lookupEJB2ServiceBD(AuditorConstants.MODULE_NAME,
				AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	/**
	 * Return transparent booking class delegate
	 * 
	 * @return
	 */
	public static BookingClassBD getBookingClassBD() {
		return (BookingClassBD) ReservationModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BookingClassBD.SERVICE_NAME);
	}

	/**
	 * Return transparent airport class delegate
	 * 
	 * @return
	 */
	public static AirportBD getAirportBD() {
		return (AirportBD) ReservationModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}
	
	public static AirportTransferBD getAirportTransferBD() {
		return (AirportTransferBD) ReservationModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportTransferBD.SERVICE_NAME);
	}

	/**
	 * Return transparent TypeBService class delegate
	 * 
	 * @return
	 */
	public final static TypeBServiceBD getTypeBServiceBD() {
		return (TypeBServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, TypeBServiceBD.SERVICE_NAME);
	}
	
	/**
	 * Return transparent ETSTicketGenerationService class delegate
	 * 
	 * @return
	 */
	public final static ETSTicketGenerationServiceBD getETSTicketGenerationServiceBD() {
		return (ETSTicketGenerationServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, ETSTicketGenerationServiceBD.SERVICE_NAME);
	}
	
	/**
	 * Return transparent ETSTicketService class delegate
	 * 
	 * @return
	 */
	public final static ETSTicketServiceBD getETSTicketServiceBD() {
		return (ETSTicketServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, ETSTicketServiceBD.SERVICE_NAME);
	}

	/**
	 * Return transparent common master delegate
	 * 
	 * @return
	 */
	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) ReservationModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				CommonMasterBD.SERVICE_NAME);
	}

	/**
	 * Return transparent location class delegate
	 * 
	 * @return
	 */
	public static LocationBD getLocationBD() {
		return (LocationBD) ReservationModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent reservation business delegate
	 * 
	 * @return
	 */
	public static ReservationBD getReservationBD() {
		return (ReservationBD) ReservationModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent segment business delegate
	 * 
	 * @return
	 */
	public static SegmentBD getSegmentBD() {
		return (SegmentBD) ReservationModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent reservation query business delegate
	 * 
	 * @return
	 */
	public static ReservationQueryBD getReservationQueryBD() {
		return (ReservationQueryBD) ReservationModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent passenger business delegate
	 * 
	 * @return
	 */
	public static PassengerBD getPassengerBD() {
		return (PassengerBD) ReservationModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent aircraft business delegate
	 * 
	 * @return
	 */
	public static AircraftBD getAircraftBD() {
		return (AircraftBD) ReservationModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AircraftBD.SERVICE_NAME);
	}

	/**
	 * Return logical cabin class business delegate
	 * 
	 * @return
	 */
	public static LogicalCabinClassBD getLogicalCabinClassBD() {
		return (LogicalCabinClassBD) ReservationModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				LogicalCabinClassBD.SERVICE_NAME);
	}

	/**
	 * Return Reservation Auxilliary Business delegate
	 * 
	 * @return
	 */
	public static ReservationAuxilliaryBD getReservationAuxilliaryBD() {
		return (ReservationAuxilliaryBD) ReservationModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationAuxilliaryBD.SERVICE_NAME);
	}

	/**
	 * Return Alerting Business delegate
	 * 
	 * @return
	 */
	public static AlertingBD getAlertingBD() {
		return (AlertingBD) ReservationModuleUtils.lookupEJB2ServiceBD(AlertingConstants.MODULE_NAME,
				AlertingConstants.BDKeys.ALERTING_SERVICE);
	}

	/**
	 * Returns transparent Web Service Client business delegate
	 * 
	 * @return
	 */
	public static WSClientBD getWSClientBD() {
		return (WSClientBD) ReservationModuleUtils.lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}

	public static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) ReservationModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}

	public static MealBD getMealBD() {
		return (MealBD) ReservationModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, MealBD.SERVICE_NAME);
	}

	public static SsrBD getSsrServiceBD() {
		return (SsrBD) ReservationModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, SsrBD.SERVICE_NAME);
	}

	public static LoyaltyCreditServiceBD getLoyaltyCreditBD() {
		return (LoyaltyCreditServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME, LoyaltyCreditServiceBD.SERVICE_NAME);
	}

	public static InvoicingBD getInvoicingBD() {
		return (InvoicingBD) lookupEJB3Service(InvoicingConstants.MODULE_NAME, InvoicingBD.SERVICE_NAME);
	}

	public static LCCReservationBD getLCCReservationBD() {
		return (LCCReservationBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCReservationBD.SERVICE_NAME);
	}

	public static LCCPaxCreditBD getLCCPaxCreditBD() {
		return (LCCPaxCreditBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCPaxCreditBD.SERVICE_NAME);
	}

	public static SchedulerBD getSchedulerBD() {
		return (SchedulerBD) lookupEJB3Service(SchedulerConstants.MODULE_NAME, SchedulerBD.SERVICE_NAME);
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}
	
	public static BaggageBusinessDelegate getBaggageBD() {
		return (BaggageBusinessDelegate) ReservationModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);
	}

	/**
	 * @return : The GDS Services Business Delegate.
	 */
	public static GDSNotifyBD getGDSNotifyBD() {
		return (GDSNotifyBD) lookupEJB3Service(GdsservicesConstants.MODULE_NAME, GDSNotifyBD.SERVICE_NAME);
	}

	public final static GdsBD getGdsServiceBD() {
		return (GdsBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, GdsBD.SERVICE_NAME);
	}

	/**
	 * @return AirproxyReservationBD
	 */
	public static AirproxyReservationBD getAirproxyReservationBD() {
		return (AirproxyReservationBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxyReservationBD.SERVICE_NAME);
	}

	/**
	 * @return AirproxySegmentBD
	 */
	public static AirproxySegmentBD getAirproxySegmentBD() {
		return (AirproxySegmentBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxySegmentBD.SERVICE_NAME);
	}

	public static ExternalSystemsConfig getImplConfig() {
		return (ExternalSystemsConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/airreservation?id=externalSystemsImplConfig");
	}

	/**
	 * @return DCSClientBD
	 */
	public static DCSClientBD getDCSClientBD() {
		return (DCSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, DCSClientBD.SERVICE_NAME);
	}

	/**
	 * @return PNLADLServiceBD
	 */
	public static PNLADLServiceBD getPNLADLServiceBD() {
		return (PNLADLServiceBD) ReservationModuleUtils.lookupEJB3Service(MsgbrokerConstants.MODULE_NAME,
				PNLADLServiceBD.SERVICE_NAME);
	}

	public static FareRuleBD getFareRuleBD() {
		return (FareRuleBD) ReservationModuleUtils.lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareRuleBD.SERVICE_NAME);
	}

	/**
	 * Gets ETL Service
	 * 
	 * @return EtlBD
	 */
	public final static ETLBD getEtlBD() {
		return (ETLBD) ReservationModuleUtils.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, ETLBD.SERVICE_NAME);
	}

	public final static PromotionManagementBD getPromotionManagementBD() {
		return (PromotionManagementBD) ReservationModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionManagementBD.SERVICE_NAME);
	}

	public final static PromotionCriteriaAdminBD getPromotionCriteriaAdminBD() {
		return (PromotionCriteriaAdminBD) ReservationModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionCriteriaAdminBD.SERVICE_NAME);
	}

	public final static AnciOfferBD getAnciOfferBD() {
		return (AnciOfferBD) ReservationModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME, AnciOfferBD.SERVICE_NAME);
	}

	public static final BunldedFareBD getBundledFareBD() {
		return (BunldedFareBD) ReservationModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				BunldedFareBD.SERVICE_NAME);
	}
	
	public static InsuranceClientBD getInsuranceClientBD() {
		return (InsuranceClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, InsuranceClientBD.SERVICE_NAME);
	}

	public static MessageBrokerServiceBD getMessageBrokerServiceBD() {
		return (MessageBrokerServiceBD) ReservationModuleUtils.lookupEJB3Service(MsgbrokerConstants.MODULE_NAME,
				MessageBrokerServiceBD.SERVICE_NAME);
	}
	
	public static PFSETLServiceBD getPFSETLServiceBD() {
		return (PFSETLServiceBD) ReservationModuleUtils.lookupEJB3Service(MsgbrokerConstants.MODULE_NAME,
				PFSETLServiceBD.SERVICE_NAME);
	}

	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) ReservationModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				LoyaltyManagementBD.SERVICE_NAME);
	}
	
	public final static BlacklistPAXBD getBlacklisPAXBD() {
		return (BlacklistPAXBD) ReservationModuleUtils.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				BlacklistPAXBD.SERVICE_NAME);
	}

	public final static VoucherBD getVoucherBD() {
		return (VoucherBD) ReservationModuleUtils.lookupEJB3Service(PromotionConstants.MODULE_NAME, VoucherBD.SERVICE_NAME);
	}

	/**
	 * 
	 * @return
	 */
	public final static ETSTicketRefundServiceBD getETSTicketRefundServiceBD() {
		return (ETSTicketRefundServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, ETSTicketRefundServiceBD.SERVICE_NAME);
	}
	
	public final static ETSTicketVoidServiceBD getETSTicketVoidServiceBD(){
		return (ETSTicketVoidServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, ETSTicketVoidServiceBD.SERVICE_NAME);
	}
}