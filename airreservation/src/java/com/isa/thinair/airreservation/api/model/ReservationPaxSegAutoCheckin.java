package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author MOhamed Rizwan
 * @hibernate.class table = "T_PNR_PAX_SEG_AUTO_CHECKIN"
 */
public class ReservationPaxSegAutoCheckin extends Persistent {

	private static final long serialVersionUID = 1L;
	private Integer pnrPaxSegAutoCheckinId;
	private Integer pnrPaxId;
	private Integer pnrSegId;
	private Integer ppfId;
	private Integer flightAmSeatId;
	private Integer salesChannelCode;
	private String userId;
	private Integer customerId;
	private Integer autoCheckinTemplateId;
	private Date requestTimeStamp;
	private String status;
	private String dcsCheckinStatus;
	private String dcsResponseText;
	private Integer sitTogetherpnrPaxId;
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String email;
	private Integer noOfAttempts;
	private ReservationPaxSegAutoCheckinSeat reservationPaxSegAutoCheckinSeat;

	/**
	 * @hibernate.id column = "PPAC_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_SEG_AUTO_CHECKIN"
	 */
	public Integer getPnrPaxSegAutoCheckinId() {
		return pnrPaxSegAutoCheckinId;
	}

	/**
	 * @param pnrPaxSegAutoCheckinId
	 *            the pnrPaxSegAutoCheckinId to set
	 */
	public void setPnrPaxSegAutoCheckinId(Integer pnrPaxSegAutoCheckinId) {
		this.pnrPaxSegAutoCheckinId = pnrPaxSegAutoCheckinId;
	}

	/**
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @hibernate.property column = "PNR_SEG_ID"
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @hibernate.property column = "PPF_ID"
	 */
	public Integer getPpfId() {
		return ppfId;
	}

	/**
	 * @param ppfId
	 *            the ppfId to set
	 */
	public void setPpfId(Integer ppfId) {
		this.ppfId = ppfId;
	}

	/**
	 * @hibernate.property column = "FLIGHT_AM_SEAT_ID"
	 */
	public Integer getFlightAmSeatId() {
		return flightAmSeatId;
	}

	/**
	 * @param flightAmSeatId
	 *            the flightAmSeatId to set
	 */
	public void setFlightAmSeatId(Integer flightAmSeatId) {
		this.flightAmSeatId = flightAmSeatId;
	}

	/**
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            the salesChannelCode to set
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @hibernate.property column = "AUTOMATIC_CHECKIN_TEMPLATE_ID"
	 */
	public Integer getAutoCheckinTemplateId() {
		return autoCheckinTemplateId;
	}

	/**
	 * @param autoCheckinTemplateId
	 *            the autoCheckinTemplateId to set
	 */
	public void setAutoCheckinTemplateId(Integer autoCheckinTemplateId) {
		this.autoCheckinTemplateId = autoCheckinTemplateId;
	}

	/**
	 * @hibernate.property column = "REQUEST_TIMESTAMP"
	 */
	public Date getRequestTimeStamp() {
		return requestTimeStamp;
	}

	/**
	 * @param requestTimeStamp
	 *            the requestTimeStamp to set
	 */
	public void setRequestTimeStamp(Date requestTimeStamp) {
		this.requestTimeStamp = requestTimeStamp;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "DCS_CHECKIN_STATUS"
	 */
	public String getDcsCheckinStatus() {
		return dcsCheckinStatus;
	}

	/**
	 * @param dcsCheckinStatus
	 *            the dcsCheckinStatus to set
	 */
	public void setDcsCheckinStatus(String dcsCheckinStatus) {
		this.dcsCheckinStatus = dcsCheckinStatus;
	}

	/**
	 * @hibernate.property column = "DCS_RESPONSE_TEXT"
	 */
	public String getDcsResponseText() {
		return dcsResponseText;
	}

	/**
	 * @param dcsResponseText
	 *            the dcsResponseText to set
	 */
	public void setDcsResponseText(String dcsResponseText) {
		this.dcsResponseText = dcsResponseText;
	}

	/**
	 * @hibernate.property column = "SIT_TOGETHER_PNR_PAX_ID"
	 */
	public Integer getSitTogetherpnrPaxId() {
		return sitTogetherpnrPaxId;
	}

	/**
	 * @param sitTogetherpnrPaxId
	 *            the sitTogetherpnrPaxId to set
	 */
	public void setSitTogetherpnrPaxId(Integer sitTogetherpnrPaxId) {
		this.sitTogetherpnrPaxId = sitTogetherpnrPaxId;
	}

	/**
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @hibernate.property column = "EMAIL"
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @hibernate.property column = "NO_OF_ATTEMPTS"
	 */
	public Integer getNoOfAttempts() {
		return noOfAttempts;
	}

	/**
	 * @param noOfAttempts
	 *            the noOfAttempts to set
	 */
	public void setNoOfAttempts(Integer noOfAttempts) {
		this.noOfAttempts = noOfAttempts;
	}

	/**
	 * @return the reservationPaxSegAutoCheckinSeat
	 * @hibernate.one-to-one cascade ="save-update"
	 */
	public ReservationPaxSegAutoCheckinSeat getReservationPaxSegAutoCheckinSeat() {
		return reservationPaxSegAutoCheckinSeat;
	}

	public void setReservationPaxSegAutoCheckinSeat(ReservationPaxSegAutoCheckinSeat reservationPaxSegAutoCheckinSeat) {
		this.reservationPaxSegAutoCheckinSeat = reservationPaxSegAutoCheckinSeat;
	}
}
