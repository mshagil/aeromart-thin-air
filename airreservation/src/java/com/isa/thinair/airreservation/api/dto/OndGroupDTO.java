/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * To hold ond group specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class OndGroupDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8706119459653885687L;

	/** Holds the ond group id */
	private Integer ondGroupId;

	/** Holds the ond code */
	private String ondCode;

	/** Holds the return flag */
	private String returnFlag;

	private Integer returnGroupId;

	/**
	 * @return Returns the ondCode.
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            The ondCode to set.
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the ondGroupId.
	 */
	public Integer getOndGroupId() {
		return ondGroupId;
	}

	/**
	 * @param ondGroupId
	 *            The ondGroupId to set.
	 */
	public void setOndGroupId(Integer ondGroupId) {
		this.ondGroupId = ondGroupId;
	}

	/**
	 * @return Returns the returnFlag.
	 */
	public String getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            The returnFlag to set.
	 */
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public Integer getReturnGroupId() {
		return returnGroupId;
	}

	public void setReturnGroupId(Integer returnGroupId) {
		this.returnGroupId = returnGroupId;
	}
}
