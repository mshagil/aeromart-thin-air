/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.TnxTypes;

/**
 * @author Lasantha Pambagoda
 */
public class ReservationTnxNominalCode {

	private int code;

	private ReservationTnxNominalCode(int code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public boolean equals(ReservationTnxNominalCode reservationTnxNominalCode) {
		return reservationTnxNominalCode.toString().equals(String.valueOf(this.code));
	}

	public int getCode() {
		return code;
	}

	/**
	 * Returns the description
	 * 
	 * @param nominalCode
	 * @return
	 */
	public static String getDescription(int nominalCode) {
		String description = "";

		switch (nominalCode) {
		case 1:
			description = "TICKET PURCHASE";
			break;
		case 2:
			description = "CANCEL REFUND";
			break;
		case 3:
			description = "PASSENGER CANCELLATION";
			break;
		case 4:
			description = "CANCELLATION CHARGE";
			break;
		case 5:
			description = "PREVIOUS CREDIT CF";
			break;
		case 6:
			description = "PREVIOUS CREDIT BF";
			break;
		case 7:
			description = "MODIFICATION CHARGE";
			break;
		case 9:
			description = "AIRLINE CANCELLATION";
			break;
		case 11:
			description = "CREDIT ADJUST";
			break;
		case 12:
			description = "CREDIT ACQUIRE";
			break;
		case 15:
			description = "CARD PAYMENT - VISA";
			break;
		case 16:
			description = "CARD PAYMENT - MASTER";
			break;
		case 17:
			description = "CARD PAYMENT - DINERS";
			break;
		case 18:
			description = "CASH";
			break;
		case 19:
			description = "ONACCOUNT";
			break;
		case 22:
			description = "REFUND - CARD PAYMENT - VISA";
			break;
		case 23:
			description = "REFUND - CARD PAYMENT - MASTER";
			break;
		case 24:
			description = "REFUND - CARD PAYMENT - DINERS";
			break;
		case 25:
			description = "REFUND - ONACCOUNT";
			break;
		case 26:
			description = "REFUND - CASH";
			break;
		case 27:
			description = "TRANSFER INFANT";
			break;
		case 28:
			description = "CARD PAYMENT - AMEX";
			break;
		case 29:
			description = "REFUND - CARD PAYMENT - AMEX";
			break;
		case 30:
			description = "CARD PAYMENT - GENERIC";
			break;
		case 31:
			description = "REFUND - CARD PAYMENT - GENERIC";
			break;
		case 32:
			description = "CARD PAYMENT - CMI";
			break;
		case 33:
			description = "REFUND - CARD PAYMENT - CMI";
			break;
		case 36:
			description = "BSP PAYMENT";
			break;
		case 37:
			description = "REFUND - BSP";
			break;
		case 38:
			description = "PAYMENT - PAYPAL";
			break;
		case 39:
			description = "REFUND - PAYMENT - PAYPAL";
			break;
		case 41:
			description = "CARD PAYMENT - GENERIC DEBIT";
			break;
		case 45:
			description = "LOYALTY PAYMENT";
			break;
		case 46:
			description = "REFUND - LOYALTY PAYMENT";
			break;
		case 48:
			description = "VOUCHER_PAYMENT";
			break;
		}

		return description;
	}

	enum PAX_TRNX_NOMINAL_CODE {
		TICKET_PURCHASE(1, "TICKET PURCHASE", TnxTypes.DEBIT), ADDED_CHARGES(2, "ADDED CHARGES", TnxTypes.DEBIT), PAX_CANCEL(3,
				"PASSENGER CANCELLATION", TnxTypes.DEBIT), CANCEL_CHARGE(4, "CANCELLATION CHARGE",
						TnxTypes.DEBIT), CREDIT_CF(5, "PREVIOUS CREDIT CF", TnxTypes.DEBIT), CREDIT_BF(6, "PREVIOUS CREDIT BF",
								TnxTypes.DEBIT), ALTER_CHARGE(7, "XX", TnxTypes.DEBIT),
		// XX(8, "XX", TnxTypes.DEBIT),
		AIR_LINE_CANCEL(9, "XX", TnxTypes.DEBIT),
		// XX(10, "XX", TnxTypes.DEBIT),
		CREDIT_ADJUST(11, "XX", TnxTypes.DEBIT), CREDIT_ACQUIRE(12, "XX", TnxTypes.DEBIT),
		// XX(13, "XX", TnxTypes.DEBIT),
		// XX(14, "XX", TnxTypes.DEBIT),
		CARD_PAYMENT_VISA(15, "XX", TnxTypes.DEBIT), CARD_PAYMENT_MASTER(16, "XX", TnxTypes.DEBIT), CARD_PAYMENT_DINERS(17, "XX",
				TnxTypes.DEBIT), CASH_PAYMENT(18, "XX",
						TnxTypes.DEBIT), ONACCOUNT_PAYMENT(19, "XX", TnxTypes.DEBIT), CREDIT_ACCOUNT(20, "XX", TnxTypes.DEBIT),
		// XX(21, "XX", TnxTypes.DEBIT),
		REFUND_VISA(22, "XX", TnxTypes.DEBIT), REFUND_MASTER(23, "XX", TnxTypes.DEBIT), REFUND_DINERS(24, "XX",
				TnxTypes.DEBIT), REFUND_ONACCOUNT(25, "XX", TnxTypes.DEBIT), REFUND_CASH(26, "XX",
						TnxTypes.DEBIT), INFANT_TRANSFER(27, "XX", TnxTypes.DEBIT), CARD_PAYMENT_AMEX(28, "XX",
								TnxTypes.DEBIT), REFUND_AMEX(29, "XX", TnxTypes.DEBIT), CARD_PAYMENT_GENERIC(30, "XX",
										TnxTypes.DEBIT), REFUND_GENERIC(31, "XX", TnxTypes.DEBIT), CARD_PAYMENT_CMI(32, "XX",
												TnxTypes.DEBIT), REFUND_CMI(33, "XX", TnxTypes.DEBIT),
		// XX(34, "XX", TnxTypes.DEBIT),
		// XX(35, "XX", TnxTypes.DEBIT),
		BSP_PAYMENT(36, "XX", TnxTypes.DEBIT), REFUND_BSP(37, "XX", TnxTypes.DEBIT), PAYMENT_PAYPAL(38, "XX",
				TnxTypes.DEBIT), REFUND_PAYPAL(39, "XX", TnxTypes.DEBIT), PENALTY_CHARGE(40, "XX", TnxTypes.DEBIT), CREDIT_PAYOFF(
						41, "XX", TnxTypes.DEBIT), PAYMENT_CARRIER_CODE_G9(42, "XX", TnxTypes.DEBIT), PAYMENT_CARRIER_CODE_3O(43,
								"XX",
								TnxTypes.DEBIT), PAYMENT_CARRIER_CODE_E5(44, "XX", TnxTypes.DEBIT), CARD_PAYMENT_GENERIC_DEBIT(45,
										"XX", TnxTypes.DEBIT), LOYALTY_PAYMENT(46, "XX", TnxTypes.DEBIT), REFUND_LOYALTY_PAYMENT(
												47, "XX", TnxTypes.DEBIT), VOUCHER_PAYMENT(48, "XX", TnxTypes.DEBIT),
		// XX(49, "XX", TnxTypes.DEBIT),
		CHARGE_REVERSAL(50, "XX", TnxTypes.DEBIT);

		private String description;
		private Integer code;
		private String tnxType;

		PAX_TRNX_NOMINAL_CODE(Integer code, String description, String tnxType) {
			this.code = code;
			this.description = description;
			this.tnxType = tnxType;
		}

		public String getDescription() {
			return description;
		}

		public Integer getCode() {
			return code;
		}

		public String getTnxType() {
			return tnxType;
		}

	}

	public static final ReservationTnxNominalCode TICKET_PURCHASE = new ReservationTnxNominalCode(1);
	/** Cancellation amount */
	public static final ReservationTnxNominalCode ADDED_CHARGES = new ReservationTnxNominalCode(2);
	public static final ReservationTnxNominalCode PAX_CANCEL = new ReservationTnxNominalCode(3);
	public static final ReservationTnxNominalCode CANCEL_CHARGE = new ReservationTnxNominalCode(4);
	/** From Passenger's carry forward amount */
	public static final ReservationTnxNominalCode CREDIT_CF = new ReservationTnxNominalCode(5);
	/** To Passenger's carry forward amount */
	public static final ReservationTnxNominalCode CREDIT_BF = new ReservationTnxNominalCode(6);
	public static final ReservationTnxNominalCode ALTER_CHARGE = new ReservationTnxNominalCode(7);
	// Airline cancel is not used. To check the usefulness and get rid of it.
	public static final ReservationTnxNominalCode AIR_LINE_CANCEL = new ReservationTnxNominalCode(9);

	public static final ReservationTnxNominalCode CREDIT_ADJUST = new ReservationTnxNominalCode(11);
	public static final ReservationTnxNominalCode CREDIT_ACQUIRE = new ReservationTnxNominalCode(12);

	public static final ReservationTnxNominalCode CARD_PAYMENT_VISA = new ReservationTnxNominalCode(15);
	public static final ReservationTnxNominalCode CARD_PAYMENT_MASTER = new ReservationTnxNominalCode(16);
	public static final ReservationTnxNominalCode CARD_PAYMENT_DINERS = new ReservationTnxNominalCode(17);

	public static final ReservationTnxNominalCode CASH_PAYMENT = new ReservationTnxNominalCode(18);
	public static final ReservationTnxNominalCode ONACCOUNT_PAYMENT = new ReservationTnxNominalCode(19);
	/** @deprecated */
	public static final ReservationTnxNominalCode CREDIT_ACCOUNT = new ReservationTnxNominalCode(20);

	public static final ReservationTnxNominalCode REFUND_VISA = new ReservationTnxNominalCode(22);
	public static final ReservationTnxNominalCode REFUND_MASTER = new ReservationTnxNominalCode(23);
	public static final ReservationTnxNominalCode REFUND_DINERS = new ReservationTnxNominalCode(24);
	public static final ReservationTnxNominalCode REFUND_ONACCOUNT = new ReservationTnxNominalCode(25);
	public static final ReservationTnxNominalCode REFUND_CASH = new ReservationTnxNominalCode(26);
	public static final ReservationTnxNominalCode INFANT_TRANSFER = new ReservationTnxNominalCode(27);

	public static final ReservationTnxNominalCode CARD_PAYMENT_AMEX = new ReservationTnxNominalCode(28);
	public static final ReservationTnxNominalCode REFUND_AMEX = new ReservationTnxNominalCode(29);

	public static final ReservationTnxNominalCode CARD_PAYMENT_GENERIC = new ReservationTnxNominalCode(30);
	public static final ReservationTnxNominalCode REFUND_GENERIC = new ReservationTnxNominalCode(31);

	public static final ReservationTnxNominalCode CARD_PAYMENT_CMI = new ReservationTnxNominalCode(32);
	public static final ReservationTnxNominalCode REFUND_CMI = new ReservationTnxNominalCode(33);

	public static final ReservationTnxNominalCode BSP_PAYMENT = new ReservationTnxNominalCode(36);
	public static final ReservationTnxNominalCode REFUND_BSP = new ReservationTnxNominalCode(37);
	public static final ReservationTnxNominalCode PAYMENT_PAYPAL = new ReservationTnxNominalCode(38);
	public static final ReservationTnxNominalCode REFUND_PAYPAL = new ReservationTnxNominalCode(39);

	public static final ReservationTnxNominalCode PENALTY_CHARGE = new ReservationTnxNominalCode(40);
	public static final ReservationTnxNominalCode CREDIT_PAYOFF = new ReservationTnxNominalCode(41);

	// Following Nominal codes are only used in Mode of payment report. There are not db records for these nominal
	// codes.
	public static final ReservationTnxNominalCode PAYMENT_CARRIER_CODE_G9 = new ReservationTnxNominalCode(42);
	public static final ReservationTnxNominalCode PAYMENT_CARRIER_CODE_3O = new ReservationTnxNominalCode(43);
	public static final ReservationTnxNominalCode PAYMENT_CARRIER_CODE_E5 = new ReservationTnxNominalCode(44);

	public static final ReservationTnxNominalCode CARD_PAYMENT_GENERIC_DEBIT = new ReservationTnxNominalCode(45);

	public static final ReservationTnxNominalCode LOYALTY_PAYMENT = new ReservationTnxNominalCode(46);
	public static final ReservationTnxNominalCode REFUND_LOYALTY_PAYMENT = new ReservationTnxNominalCode(47);
	public static final ReservationTnxNominalCode VOUCHER_PAYMENT = new ReservationTnxNominalCode(48);
	public static final ReservationTnxNominalCode CHARGE_REVERSAL = new ReservationTnxNominalCode(50);

	/**
	 * Return Cash Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getCashTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CASH_PAYMENT.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_CASH.getCode()));

		return nominalCodes;
	}

	/**
	 * Return Visa Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getVisaTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_VISA.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_VISA.getCode()));

		return nominalCodes;
	}

	/**
	 * Return Master Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getMasterTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_MASTER.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_MASTER.getCode()));

		return nominalCodes;
	}

	/**
	 * Return Amex Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getAmexTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_AMEX.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_AMEX.getCode()));

		return nominalCodes;
	}

	/**
	 * Return Generic Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getGenericTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_GENERIC.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_GENERIC.getCode()));

		return nominalCodes;
	}

	/**
	 * Return CMI Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getCMITypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_CMI.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_CMI.getCode()));

		return nominalCodes;
	}

	/**
	 * Return Diners Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getDinersTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_DINERS.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_DINERS.getCode()));

		return nominalCodes;
	}

	/**
	 * Return On Account Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getOnAccountTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.ONACCOUNT_PAYMENT.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_ONACCOUNT.getCode()));

		return nominalCodes;
	}

	/**
	 * Return On Account Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getBSPAccountTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.BSP_PAYMENT.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_BSP.getCode()));

		return nominalCodes;
	}

	/**
	 * Return Credit Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getCreditTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CREDIT_BF.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CREDIT_CF.getCode()));

		return nominalCodes;
	}

	/**
	 * Return PayPal Type Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getPayPalTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.PAYMENT_PAYPAL.getCode()));

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_PAYPAL.getCode()));

		return nominalCodes;
	}

	/**
	 * Return DebitCard Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getDebitCardNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_GENERIC_DEBIT.getCode()));

		return nominalCodes;
	}

	public static List<Integer> getLoyaltyNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.LOYALTY_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_LOYALTY_PAYMENT.getCode()));

		return nominalCodes;
	}

	/**
	 * Return Voucher Payment Nominal Codes
	 * 
	 * @return
	 */
	public static List<Integer> getVoucherTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode()));

		return nominalCodes;
	}

	public static List<Integer> getVoucherNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();
		nominalCodes.add(new Integer(ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode()));
		return nominalCodes;
	}

	/**
	 * Return the payment nominal codes
	 */
	public static List<Integer> getPaymentAndRefundNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.addAll(getCashTypeNominalCodes());
		nominalCodes.addAll(getVisaTypeNominalCodes());
		nominalCodes.addAll(getMasterTypeNominalCodes());
		nominalCodes.addAll(getAmexTypeNominalCodes());
		nominalCodes.addAll(getDinersTypeNominalCodes());
		nominalCodes.addAll(getGenericTypeNominalCodes());
		nominalCodes.addAll(getCMITypeNominalCodes());
		nominalCodes.addAll(getOnAccountTypeNominalCodes());
		nominalCodes.addAll(getCreditTypeNominalCodes());
		nominalCodes.addAll(getBSPAccountTypeNominalCodes());
		nominalCodes.addAll(getPayPalTypeNominalCodes());
		nominalCodes.addAll(getDebitCardNominalCodes());
		nominalCodes.addAll(getLoyaltyNominalCodes());
		nominalCodes.addAll(getVoucherTypeNominalCodes());

		return nominalCodes;
	}

	public static List<Integer> getPaymentAndRefundAndCreditAcquireNominalCodes() {
		List<Integer> nominalCodes = getPaymentAndRefundNominalCodes();
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CREDIT_ACQUIRE.getCode()));
		return nominalCodes;
	}

	/**
	 * Returns All Credit Card Nominal Codes
	 */
	public static List<Integer> getCreditCardNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.addAll(getVisaTypeNominalCodes());
		nominalCodes.addAll(getMasterTypeNominalCodes());
		nominalCodes.addAll(getAmexTypeNominalCodes());
		nominalCodes.addAll(getDinersTypeNominalCodes());
		nominalCodes.addAll(getGenericTypeNominalCodes());
		nominalCodes.addAll(getCMITypeNominalCodes());
		nominalCodes.addAll(getPayPalTypeNominalCodes());

		return nominalCodes;
	}

	/**
	 * Reports specific payment type nominal codes
	 * 
	 * @return
	 */
	public static List<Integer> getReportsPaymentTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_VISA.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_MASTER.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_AMEX.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_DINERS.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_GENERIC.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_CMI.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.ONACCOUNT_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CASH_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.BSP_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.PAYMENT_PAYPAL.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_GENERIC_DEBIT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.LOYALTY_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode()));

		return nominalCodes;
	}

	/**
	 * Returns payment types nominal codes
	 */
	public static List<Integer> getPaymentTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_VISA.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_MASTER.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_AMEX.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_DINERS.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_GENERIC.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_CMI.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.ONACCOUNT_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CASH_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CREDIT_BF.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.BSP_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.PAYMENT_PAYPAL.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CARD_PAYMENT_GENERIC_DEBIT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.LOYALTY_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode()));
		return nominalCodes;
	}

	/**
	 * Returns refund types nominal codes
	 * 
	 * @return
	 */
	public static List<Integer> getRefundTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_VISA.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_MASTER.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_AMEX.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_DINERS.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_GENERIC.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_CMI.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_ONACCOUNT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_CASH.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CREDIT_CF.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_BSP.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_PAYPAL.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_LOYALTY_PAYMENT.getCode()));

		return nominalCodes;
	}

	public static List<Integer> getAdjModCnxNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(new Integer(ReservationTnxNominalCode.CREDIT_ADJUST.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.ALTER_CHARGE.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CANCEL_CHARGE.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CHARGE_REVERSAL.getCode()));

		return nominalCodes;
	}

	/**
	 * Returns Nominal Codes of the payments which cannot be refunded.
	 */
	public static List<Integer> getNonRefundableTypePaymentCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();
		nominalCodes.add(new Integer(ReservationTnxNominalCode.LOYALTY_PAYMENT.getCode()));
		return nominalCodes;
	}

}