package com.isa.thinair.airreservation.api.dto.adl;

import java.io.Serializable;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.AnciTypes;


public class AncillaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String description;
	AnciTypes anciType;
	private int count;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AnciTypes getAnciType() {
		return anciType;
	}

	public void setAnciType(AnciTypes anciType) {
		this.anciType = anciType;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
