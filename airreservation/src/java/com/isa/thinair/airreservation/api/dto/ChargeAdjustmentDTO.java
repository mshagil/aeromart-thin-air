package com.isa.thinair.airreservation.api.dto;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.CHARGE_ADJUSTMENTS;

/**
 * @author eric
 * 
 */
public class ChargeAdjustmentDTO extends BasicChargeDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CHARGE_ADJUSTMENTS chargeAdjustment;

	public CHARGE_ADJUSTMENTS getChargeAdjustment() {
		return chargeAdjustment;
	}

	public void setChargeAdjustment(CHARGE_ADJUSTMENTS chargeAdjustment) {
		this.chargeAdjustment = chargeAdjustment;
	}

}
