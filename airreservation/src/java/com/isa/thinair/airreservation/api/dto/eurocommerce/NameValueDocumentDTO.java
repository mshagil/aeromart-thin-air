package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.util.ArrayList;
import java.util.List;

public class NameValueDocumentDTO extends DocumentSnapInDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<NameValueDTO> iteamDTOs;

	public NameValueDocumentDTO(String name, String discription) {
		super(name, discription);
	}

	public NameValueDocumentDTO() {
		super();
	}

	public List<NameValueDTO> getIteamDTOs() {
		if (iteamDTOs == null) {
			iteamDTOs = new ArrayList<NameValueDTO>();
		}
		return iteamDTOs;
	}

	public void setIteamDTOs(List<NameValueDTO> iteamDTOs) {
		this.iteamDTOs = iteamDTOs;
	}

	public void addIteamDTOs(NameValueDTO iteam) {
		getIteamDTOs().add(iteam);
	}

}
