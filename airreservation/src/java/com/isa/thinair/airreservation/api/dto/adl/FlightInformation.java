package com.isa.thinair.airreservation.api.dto.adl;

import java.io.Serializable;
import java.util.Date;

public class FlightInformation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Integer flightId;
	String flightNumber;
	Date flightDate;
	String boardingAirport;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public String getBoardingAirport() {
		return boardingAirport;
	}

	public void setBoardingAirport(String boardingAirport) {
		this.boardingAirport = boardingAirport;
	}

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

}
