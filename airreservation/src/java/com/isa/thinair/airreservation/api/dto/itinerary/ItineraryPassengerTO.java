package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class ItineraryPassengerTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer paxSeq;

	private String paxType;

	private String paxDisplayName;

	private String infantDisplayName;

	private ItineraryFinancialTotalsTO baseCurrencyFinancials;

	private List<ItinerarySegmentAncillaryTO> ancillary;

	private String paxInsPolicyCode;

	// Payments by Currency
	private Map<ItineraryFinancialTotalsTO, List<ItineraryPaymentTO>> payments;

	private Date dateOfBirth;

	private String foidNumber;

	private String foidExpiry;

	private String foidPlace;

	private String dateOfBirthInPersian;

	public String getDateOfBirthInPersian() {
		return dateOfBirthInPersian;
	}

	public void setDateOfBirthInPersian(String dateOfBirthInPersian) {
		this.dateOfBirthInPersian = dateOfBirthInPersian;
	}

	private Date infantDateOfBirth;

	private String infantDateOfBirthInPersian;

	private String paxNationality;
	
	private String employeeId;

	private boolean persianDatesSet;

	private boolean alertAutoCancellation;

	public String getInfantDateOfBirthInPersian() {
		return infantDateOfBirthInPersian;
	}

	public void setInfantDateOfBirthInPersian(String infantDateOfBirthInPersian) {
		this.infantDateOfBirthInPersian = infantDateOfBirthInPersian;
	}

	public boolean isPersianDatesSet() {
		return persianDatesSet;
	}

	public void setIsPersianDatesSet(boolean usePersianDates) {
		this.persianDatesSet = usePersianDates;
	}

	/**
	 * @return the paxSeq
	 */
	public Integer getPaxSeq() {
		return paxSeq;
	}

	/**
	 * @param paxSeq
	 *            the paxSeq to set
	 */
	public void setPaxSeq(Integer paxSeq) {
		this.paxSeq = paxSeq;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the paxDisplayName
	 */
	public String getPaxDisplayName() {
		return paxDisplayName;
	}

	/**
	 * @param paxDisplayName
	 *            the paxDisplayName to set
	 */
	public void setPaxDisplayName(String paxDisplayName) {
		this.paxDisplayName = paxDisplayName;
	}

	/**
	 * @return the infantDisplayName
	 */
	public String getInfantDisplayName() {
		return infantDisplayName;
	}

	/**
	 * @param infantDisplayName
	 *            the infantDisplayName to set
	 */
	public void setInfantDisplayName(String infantDisplayName) {
		this.infantDisplayName = infantDisplayName;
	}

	/**
	 * @return the baseCurrencyFinancials
	 */
	public ItineraryFinancialTotalsTO getBaseCurrencyFinancials() {
		return baseCurrencyFinancials;
	}

	/**
	 * @param baseCurrencyFinancials
	 *            the baseCurrencyFinancials to set
	 */
	public void setBaseCurrencyFinancials(ItineraryFinancialTotalsTO baseCurrencyFinancials) {
		this.baseCurrencyFinancials = baseCurrencyFinancials;
	}

	/**
	 * @return the payments
	 */
	public Map<ItineraryFinancialTotalsTO, List<ItineraryPaymentTO>> getPayments() {
		return payments;
	}

	/**
	 * @param payments
	 *            the payments to set
	 */
	public void setPayments(Map<ItineraryFinancialTotalsTO, List<ItineraryPaymentTO>> payments) {
		this.payments = payments;
	}

	public List<ItinerarySegmentAncillaryTO> getAncillary() {
		return ancillary;
	}

	public void setAncillary(List<ItinerarySegmentAncillaryTO> ancillary) {
		this.ancillary = ancillary;
	}

	public String getPaxInsPolicyCode() {
		return paxInsPolicyCode;
	}

	public void setPaxInsPolicyCode(String paxInsPolicyCode) {
		this.paxInsPolicyCode = paxInsPolicyCode;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getDateOfBirth(String dateFmt) {
		return BeanUtils.parseDateFormat(this.dateOfBirth, dateFmt);
	}

	public void setFoidNumber(String foidNumber) {
		this.foidNumber = foidNumber;
	}

	public String getFoidNumber() {
		return foidNumber;
	}

	public String getFoidExpiry() {
		return foidExpiry;
	}

	public void setFoidExpiry(String foidExpiry) {
		this.foidExpiry = foidExpiry;
	}

	public String getFoidPlace() {
		return foidPlace;
	}

	public void setFoidPlace(String foidPlace) {
		this.foidPlace = foidPlace;
	}

	public void setInfantDateOfBirth(Date infantDateOfBirth) {
		this.infantDateOfBirth = infantDateOfBirth;
	}

	public String getInfantDateOfBirth(String dateFmt) {
		return BeanUtils.parseDateFormat(this.infantDateOfBirth, dateFmt);
	}

	public String getPsptExpiryFormattedDate(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = "";
		try {
			Date dt = sdf.parse(foidExpiry);
			formattedDate = BeanUtils.parseDateFormat(dt, format);
		} catch (ParseException e) {
			formattedDate = foidExpiry;
		}
		return formattedDate;
	}

	public String getPaxNationality() {
		return paxNationality;
	}

	public void setPaxNationality(String paxNationality) {
		this.paxNationality = paxNationality;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	
	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}	
}