package com.isa.thinair.airreservation.api.dto.baggage;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.AncillaryAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author mano
 * @since May 10, 2011 
 */
public interface IPassengerBaggage extends AncillaryAssembler {

	public void addBaggageInfo(Integer pnrPaxId, Integer pnrPaxFareId, Integer flightBaggageId, Integer pnrSegId,
			BigDecimal chargeAmount, Integer baggageId, Integer baggageChrgId, String baggageOndGroupId, Integer autoCnxId,
			TrackInfoDTO trackInfo) throws ModuleException;
}
