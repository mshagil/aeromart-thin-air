/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateJourneyType;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

/**
 * To hold reservation external charges data transfer information
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class ExternalChgDTO extends BasicChargeDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6659884370061434384L;

	/** Holds external charges enum */
	private EXTERNAL_CHARGES externalChargesEnum;

	/** Holds charge amount this will be calculated via isValueInPercentage and ratioValue paramters */
	private BigDecimal amount = BigDecimal.ZERO;

	/**
	 * to hold the fare type wise charge amounts
	 */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/**
	 * to hold the set of applicable charge rates.
	 */
	private List<ChargeRateDTO> chargeRates;

	private boolean valid;

	/** Holds whether or not the amount is consumed for payment */
	private boolean isAmountConsumedForPayment;
	
	private Integer flightSegId;
	
	/**
	 * CREATE_RES(1), MODIFY_SEGMENT(2), CANCEL_SEGMENT(3), ADD_SEGMENT(4), NAME_CHANGE(5), MODIFY_ANCI(6),
	 * REMOVE_PAX(7), ADD_INFANT(8)
	 */
	private Integer operationMode;

	/**
	 * @return Returns the externalChargesEnum.
	 */
	public EXTERNAL_CHARGES getExternalChargesEnum() {
		return externalChargesEnum;
	}

	/**
	 * @param externalChargesEnum
	 *            The externalChargesEnum to set.
	 */
	public void setExternalChargesEnum(EXTERNAL_CHARGES externalChargesEnum) {
		this.externalChargesEnum = externalChargesEnum;
	}

	/**
	 * @return Returns the amount.
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * @param valid
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	/**
	 * @param chargeRates
	 */
	public void setChargeRates(List<ChargeRateDTO> chargeRates) {
		this.chargeRates = chargeRates;
	}

	/**
	 * @return
	 */
	public List<ChargeRateDTO> getChargeRates() {
		if (chargeRates == null) {
			chargeRates = new ArrayList<ChargeRateDTO>();
		}
		return chargeRates;
	}

	/**
	 * @param externalChgDTO
	 */
	public void addChargeRate(ChargeRateDTO chargeRateDTO) {
		this.getChargeRates().add(chargeRateDTO);
	}

	/**
	 * @return
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return
	 */
	public boolean isAmountConsumedForPayment() {
		return isAmountConsumedForPayment;
	}

	/**
	 * @param isAmountConsumedForPayment
	 */
	public void setAmountConsumedForPayment(boolean isAmountConsumedForPayment) {
		this.isAmountConsumedForPayment = isAmountConsumedForPayment;
	}

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		ExternalChgDTO clone = new ExternalChgDTO();
		clone.setChargeCode(this.getChargeCode());
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setAppliesToInfant(this.isAppliesToInfant());
		clone.setAppliesTo(this.getAppliesTo());
		clone.setChargeValueMin(this.getChargeValueMin());
		clone.setChargeValueMax(this.getChargeValueMax());
		clone.setChargeBasis(this.getChargeBasis());
		clone.setJourneyType(this.getJourneyType());
		clone.setValid(this.isValid());
		clone.setChargeRates(this.getChargeRates());
		clone.setBreakPoint(this.getBreakPoint());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setFlightSegId(this.getFlightSegId());
		clone.setOperationMode(this.getOperationMode());
		return clone;
	}

	/**
	 * Calculates the Amount
	 * 
	 * @param baseAmount
	 */
	public void calculateAmount(BigDecimal baseAmount) {
		if (isRatioValueInPercentage()) {
			BigDecimal amount = AccelAeroCalculator.divide(AccelAeroCalculator.multiply(baseAmount, getRatioValue()),
					new BigDecimal(100));
			if (this.isChargeRateRoundingApplicable()) {
				BigDecimal roundedAmount = AccelAeroRounderPolicy.getRoundedValue(amount, this.getBoundryValue(),
						this.getBreakPoint());
				amount = roundedAmount;

			}
			this.setAmount(amount);
		} else {
			this.setAmount(getRatioValue());
		}
	}

	/**
	 * Calculates the total cc charge = no of payable pax * cc charge per pax per segment * no of segments
	 * 
	 * @param baseAmount
	 */
	public void calculateAmount(int noOfPayablePax, int noOfSegemnts) {
		if (!isRatioValueInPercentage()) {
			this.setAmount(AccelAeroCalculator.multiply(
					AccelAeroCalculator.multiply(new BigDecimal(noOfPayablePax), getRatioValue()), noOfSegemnts));
		}
	}

	public void calculateAmount(int noOfPayablePax) {
		if (!isRatioValueInPercentage()) {
			this.setAmount(AccelAeroCalculator.multiply(new BigDecimal(noOfPayablePax), getRatioValue()));
		}
	}

	/**
	 * @param journeyType
	 */
	public void setApplicableChargeRate(Integer journeyType) {
		if (journeyType != null && chargeRates != null && !chargeRates.isEmpty()) {
			boolean found = false;
			for (ChargeRateDTO chargeRateDTO : chargeRates) {
				if (journeyType.intValue() == chargeRateDTO.getJourneyType()) {
					this.updateChargeInfo(chargeRateDTO);
					found = true;
					break;
				}
			}

			if (!found && journeyType.intValue() != ChargeRateJourneyType.ANY) {
				for (ChargeRateDTO chargeRateDTO : chargeRates) {
					if (ChargeRateJourneyType.ANY == chargeRateDTO.getJourneyType()) {
						this.updateChargeInfo(chargeRateDTO);
						found = true;
						break;
					}
				}
			}
			this.setValid(this.isValid() && found);
		}
	}

	public boolean isChargeRateExist(Integer chargeRateID) {
		if (chargeRateID.intValue() == this.getChgRateId().intValue()) {
			return true;
		}
		if (chargeRates != null && !chargeRates.isEmpty()) {
			for (ChargeRateDTO chargeRateDTO : chargeRates) {
				if (chargeRateID.intValue() == chargeRateDTO.getChgRateId().intValue()) {
					updateChargeInfo(chargeRateDTO);
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @return : true if both boundary value and break point are both available in for the charge rate record.
	 */
	public boolean isChargeRateRoundingApplicable() {
		if (this.getBoundryValue() != null && this.getBoundryValue().compareTo(BigDecimal.ZERO) > 0
				&& this.getBreakPoint() != null && this.getBreakPoint().compareTo(BigDecimal.ZERO) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * @param chargeRateDTO
	 */
	private void updateChargeInfo(ChargeRateDTO chargeRateDTO) {

		this.setChgRateId(chargeRateDTO.getChgRateId());
		this.setRatioValueInPercentage(chargeRateDTO.isRatioInPercentage());
		this.setRatioValue(chargeRateDTO.getRatioValue());
		this.setChargeValueMin(chargeRateDTO.getChargeValueMin());
		this.setChargeValueMax(chargeRateDTO.getChargeValueMax());
		this.setJourneyType(chargeRateDTO.getJourneyType());
		this.setChargeBasis(chargeRateDTO.getChargeBasis());
		this.setValid(chargeRateDTO.isValid());

	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Integer getOperationMode() {
		return operationMode;
	}

	public void setOperationMode(Integer operationMode) {
		this.operationMode = operationMode;
	}	

}
