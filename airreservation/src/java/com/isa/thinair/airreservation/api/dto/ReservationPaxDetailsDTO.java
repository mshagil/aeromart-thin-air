/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold reservation passenger details data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
@SuppressWarnings("rawtypes")
public class ReservationPaxDetailsDTO implements Serializable, Comparable {

	private static final long serialVersionUID = 1L;

	/** Holds passenger status */
	private String status;

	/** Holds passenger sequence */
	private Integer paxSequence;

	/** Holds passenger type */
	private String paxType;

	/** Holds passenger pnr number */
	private String pnr;

	/** Holds passenger date of birth */
	private Date dateOfBirth;

	/** Holds passenger id */
	private Integer pnrPaxId;

	/** Holds passenger title */
	private String title;

	/** Holds passenger first name */
	private String firstName;

	/** Holds passenger last name */
	private String lastName;

	private String gender;

	/** Holds passenger nationality code */
	private Integer nationalityCode;

	private String nationalityIsoCode;
	
	private String eticketNumber;

	/** Holds adult id */
	private Integer adultId;

	/** Holds Pnl Stat Type */
	private String pnlStatType;

	/** Holds collection of PaxCreditDTO */
	private Collection<PaxCreditDTO> paxCredit;

	/** Holds Special Service Request */
	private String ssrCode = "";

	/** Holds Child First Name */
	private String childFirstName;

	/** Holds Child Last Name */
	private String childLastName;

	/** Holds Child Title */
	private String childTitle;

	/** Holds Child's Special Service Request */
	private String childSsrCode;

	/** Holds reservation passenger total fare */
	private BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds reservation passenger paid amount */
	private BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds reservation passenger total charge amount */
	private BigDecimal totalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds credit amount */
	private BigDecimal credit = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds Special Service Request Text */
	private String ssrText;

	/** Holds the pnr status */
	private String pnrStatus;

	/** Holds the PNR segment id **/
	private Integer pnrSegId;

	/** Holds if Waiting List Pax **/
	private boolean waitingListPax;

	/** Standby .ID2/ passengers **/
	private boolean IDPassenger;

	/** Holds FOID **/
	private String foidNumber;

	/** Holds expiry date of PSPT */
	private String foidExpiry;

	/** Holds two letter country code */
	private String foidPlace;
	
	private String placeOfBirth; 
	
	private String travelDocumentType; 
	
	private String visaDocNumber;
	
	private String visaDocPlaceOfIssue ;
	
	private String visaDocIssueDate; 
	
	private String visaApplicableCountry;

	private String FFID;

	private String returnFlag;

	private Integer journeySequence;

	private Integer flightSegId;

	/** voucher reprotected flight seg id **/
	private Integer voucherToFlightSegID;


	/** Holds DOB **/
	private String dob;

	/** Holds child FOID **/
	private String childFoid;

	/** Holds child DOB **/
	private String childDob;

	/** Holds Pnl Stat Type */
	private String groupId;

	/** Holds Last 4 Digits for CreditCard Payments */
	private String ccDigits;

	/** Store collection of SSR details */
	private Collection<PaxSSRDetailDTO> ssrDetails;

	/** Hold credit residing carrier amount for utilized amount calculations */
	private BigDecimal residingCarrierAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Collection<ResSegmentEticketInfoDTO> eTicketInfo;
	
	private String csFlightNo;
	
	private String csBookingClass;
	
	private Date csDepatureDateTime;
	
	private String csOrginDestination;
	
	private boolean codeShareFlight;
	
	private String externalPnr;

	private String nationalIDNo;
	/** Store External International Arrival Flight No */
	private String arrivalIntlFlightNo;
	
	/** Store External International Flight Arrival Date */
	private Date intlFlightArrivalDate;
	
	/** Store External International Departure Flight No */
	private String departureIntlFlightNo;
	
	/** Store External International Flight Departure Date */
	private Date intlFlightDepartureDate;
	
	/** Holds Pax group */
	private String pnrPaxGroupId;

	private List<AncillaryDTO> seats;
	private List<AncillaryDTO> meals;
	private List<AncillaryDTO> ssrs;
	private AncillaryDTO baggages;
	
	private String couponNumber;
	
	/**
	 * @return the ssrDetails
	 */
	public Collection<PaxSSRDetailDTO> getSsrDetails() {
		return ssrDetails;
	}

	/**
	 * @param ssrDetails
	 *            the ssrDetails to set
	 */
	public void setSsrDetails(Collection<PaxSSRDetailDTO> ssrDetails) {
		this.ssrDetails = ssrDetails;
	}

	/**
	 * @return Returns the adultId.
	 */
	public Integer getAdultId() {
		return adultId;
	}

	/**
	 * @param adultId
	 *            The adultId to set.
	 */
	public void setAdultId(Integer adultId) {
		this.adultId = adultId;
	}

	/**
	 * @return Returns the credit.
	 */
	public BigDecimal getCredit() {
		return credit;
	}

	/**
	 * Returns the credit as string amount
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public String getCreditAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getCredit().doubleValue(), numberFormat);
	}

	/**
	 * @param credit
	 *            The credit to set.
	 */
	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	/**
	 * @return Returns the dateOfBirth.
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            The dateOfBirth to set.
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the nationalityCode.
	 */
	public Integer getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode
	 *            The nationalityCode to set.
	 */
	public void setNationalityCode(Integer nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the paxCredit.
	 */
	public Collection<PaxCreditDTO> getPaxCredit() {
		return paxCredit;
	}

	/**
	 * @param paxCredit
	 *            The paxCredit to set.
	 */
	public void setPaxCredit(Collection<PaxCreditDTO> paxCredit) {
		this.paxCredit = paxCredit;
	}

	/**
	 * @return Returns the paxSequence.
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            The paxSequence to set.
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return Returns the paxType.
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the pnlStatType.
	 */
	public String getPnlStatType() {
		return pnlStatType;
	}

	/**
	 * @param pnlStatType
	 *            The pnlStatType to set.
	 */
	public void setPnlStatType(String pnlStatType) {
		this.pnlStatType = pnlStatType;
	}

	/**
	 * Sorts by the last Name
	 */
	public int compareTo(Object arg0) {
		return this.lastName.compareTo(((ReservationPaxDetailsDTO) arg0).getLastName());
	}

	/**
	 * @return Returns the Special Service Request Code
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            to set
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the totalChargeAmount.
	 */
	public BigDecimal getTotalChargeAmount() {
		return totalChargeAmount;
	}

	/**
	 * @param totalChargeAmount
	 *            The totalChargeAmount to set.
	 */
	public void setTotalChargeAmount(BigDecimal totalChargeAmount) {
		this.totalChargeAmount = totalChargeAmount;
	}

	/**
	 * @return Returns the totalFare.
	 */
	public BigDecimal getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            The totalFare to set.
	 */
	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	/**
	 * Returns the total fare as string amount
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public String getTotalFareAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getTotalFare().doubleValue(), numberFormat);
	}

	/**
	 * Returns the total charge as string amount
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public String getTotalChargeAmountAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getTotalChargeAmount().doubleValue(), numberFormat);
	}

	/**
	 * Returns the total charge as string amount
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public String getTotalPaidAmountAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getTotalPaidAmount().doubleValue(), numberFormat);
	}

	/**
	 * @return Returns the totalPaidAmount.
	 */
	public BigDecimal getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            The totalPaidAmount to set.
	 */
	public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	/**
	 * @return Returns the childFirstName.
	 */
	public String getChildFirstName() {
		return childFirstName;
	}

	/**
	 * @param childFirstName
	 *            The childFirstName to set.
	 */
	public void setChildFirstName(String childFirstName) {
		this.childFirstName = childFirstName;
	}

	/**
	 * @return Returns the childLastName.
	 */
	public String getChildLastName() {
		return childLastName;
	}

	/**
	 * @param childLastName
	 *            The childLastName to set.
	 */
	public void setChildLastName(String childLastName) {
		this.childLastName = childLastName;
	}

	/**
	 * @return Returns the childTitle.
	 */
	public String getChildTitle() {
		return childTitle;
	}

	/**
	 * @param childTitle
	 *            The childTitle to set.
	 */
	public void setChildTitle(String childTitle) {
		this.childTitle = childTitle;
	}

	/**
	 * utility method to check if a pax is a parent
	 * 
	 * @return
	 */
	public boolean isParent() {
		return (this.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT) && this.adultId != null);
	}

	/**
	 * Utility method to check if a pax is a Child
	 * 
	 * @return
	 */
	public boolean isChild() {
		return (this.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD));
	}

	/**
	 * @return Returns the childSsrCode.
	 */
	public String getChildSsrCode() {
		return childSsrCode;
	}

	/**
	 * @param childSsrCode
	 *            The childSsrCode to set.
	 */
	public void setChildSsrCode(String childSsrCode) {
		this.childSsrCode = childSsrCode;
	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		return ssrText;
	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

	/**
	 * @return Returns the pnrSegId.
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            The pnrSegId to set.
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return Returns the pnrStatus.
	 */
	public String getPnrStatus() {
		return pnrStatus;
	}

	/**
	 * @param pnrStatus
	 *            The pnrStatus to set.
	 */
	public void setPnrStatus(String pnrStatus) {
		this.pnrStatus = pnrStatus;
	}

	/**
	 * @return the waitingListPax
	 */
	public boolean isWaitingListPax() {
		return waitingListPax;
	}

	/**
	 * @param waitingListPax
	 *            the waitingListPax to set
	 */
	public void setWaitingListPax(boolean waitingListPax) {
		this.waitingListPax = waitingListPax;
	}

	/**
	 * @return Returns the foidNumber.
	 */
	public String getFoidNumber() {
		return foidNumber;
	}

	/**
	 * @param foidNumber
	 *            The foidNumber to set.
	 */
	public void setFoidNumber(String foidNumber) {
		this.foidNumber = foidNumber;
	}

	/**
	 * @return the foidExpiry
	 */
	public String getFoidExpiry() {
		return foidExpiry;
	}

	/**
	 * @param foidExpiry
	 *            the foidExpiry to set
	 */
	public void setFoidExpiry(String foidExpiry) {
		this.foidExpiry = foidExpiry;
	}

	/**
	 * @return the foidPlace
	 */
	public String getFoidPlace() {
		return foidPlace;
	}

	/**
	 * @param foidPlace
	 *            the foidPlace to set
	 */
	public void setFoidPlace(String foidPlace) {
		this.foidPlace = foidPlace;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getTravelDocumentType() {
		return travelDocumentType;
	}

	public void setTravelDocumentType(String travelDocumentType) {
		this.travelDocumentType = travelDocumentType;
	}

	public String getVisaDocNumber() {
		return visaDocNumber;
	}

	public void setVisaDocNumber(String visaDocNumber) {
		this.visaDocNumber = visaDocNumber;
	}

	public String getVisaDocPlaceOfIssue() {
		return visaDocPlaceOfIssue;
	}

	public void setVisaDocPlaceOfIssue(String visaDocPlaceOfIssue) {
		this.visaDocPlaceOfIssue = visaDocPlaceOfIssue;
	}

	public String getVisaDocIssueDate() {
		return visaDocIssueDate;
	}

	public void setVisaDocIssueDate(String visaDocIssueDate) {
		this.visaDocIssueDate = visaDocIssueDate;
	}

	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}

	/**
	 * @return Returns the dob.
	 */
	public String getDob() {
		return dob;
	}

	/**
	 * @param dob
	 *            The dob to set.
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}

	/**
	 * @return Returns the childFoid.
	 */
	public String getChildFoid() {
		return childFoid;
	}

	/**
	 * @param childFoid
	 *            The childFoid to set.
	 */
	public void setChildFoid(String childFoid) {
		this.childFoid = childFoid;
	}

	/**
	 * @return Returns the childDob.
	 */
	public String getChildDob() {
		return childDob;
	}

	/**
	 * @param childDob
	 *            The childDob to set.
	 */
	public void setChildDob(String childDob) {
		this.childDob = childDob;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the ccDigits
	 */
	public String getCcDigits() {
		return ccDigits;
	}

	/**
	 * @param ccDigits
	 *            the ccDigits to set
	 */
	public void setCcDigits(String ccDigits) {
		this.ccDigits = ccDigits;
	}

	/**
	 * @return the iDPassenger
	 */
	public boolean isIDPassenger() {
		return IDPassenger;
	}

	/**
	 * @param iDPassenger
	 *            the iDPassenger to set
	 */
	public void setIDPassenger(boolean iDPassenger) {
		IDPassenger = iDPassenger;
	}

	public BigDecimal getResidingCarrierAmount() {
		return residingCarrierAmount;
	}

	public void setResidingCarrierAmount(BigDecimal residingCarrierAmount) {
		this.residingCarrierAmount = residingCarrierAmount;
	}

	public String getNationalityIsoCode() {
		return nationalityIsoCode;
	}

	public void setNationalityIsoCode(String nationalityIsoCode) {
		this.nationalityIsoCode = nationalityIsoCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Collection<ResSegmentEticketInfoDTO> geteTicketInfo() {
		return eTicketInfo;
	}

	public void seteTicketInfo(Collection<ResSegmentEticketInfoDTO> eTicketInfo) {
		this.eTicketInfo = eTicketInfo;
	}

	public void addeTicketInfo(ResSegmentEticketInfoDTO eticketInfoDTO) {
		if(this.eTicketInfo == null) {
			eTicketInfo = new ArrayList<ResSegmentEticketInfoDTO>();
		}

		eTicketInfo.add(eticketInfoDTO);
	}

	/**
	 * @return the arrivalIntlFlightNo
	 */
	public String getArrivalIntlFlightNo() {
		return arrivalIntlFlightNo;
	}

	/**
	 * @param arrivalIntlFlightNo
	 *            the arrivalIntlFlightNo to set
	 */
	public void setArrivalIntlFlightNo(String arrivalIntlFlightNo) {
		this.arrivalIntlFlightNo = arrivalIntlFlightNo;
	}

	/**
	 * @return the intlFlightArrivalDate
	 */
	public Date getIntlFlightArrivalDate() {
		return intlFlightArrivalDate;
	}

	/**
	 * @param intlFlightArrivalDate
	 *            the intlFlightArrivalDate to set
	 */
	public void setIntlFlightArrivalDate(Date intlFlightArrivalDate) {
		this.intlFlightArrivalDate = intlFlightArrivalDate;
	}

	/**
	 * @return the departureIntlFlightNo
	 */
	public String getDepartureIntlFlightNo() {
		return departureIntlFlightNo;
	}

	/**
	 * @param departureIntlFlightNo
	 *            the departureIntlFlightNo to set
	 */
	public void setDepartureIntlFlightNo(String departureIntlFlightNo) {
		this.departureIntlFlightNo = departureIntlFlightNo;
	}

	/**
	 * @return the intlFlightDepartureDate
	 */
	public Date getIntlFlightDepartureDate() {
		return intlFlightDepartureDate;
	}

	/**
	 * @param intlFlightDepartureDate
	 *            the intlFlightDepartureDate to set
	 */
	public void setIntlFlightDepartureDate(Date intlFlightDepartureDate) {
		this.intlFlightDepartureDate = intlFlightDepartureDate;
	}

	public String getPnrPaxGroupId() {
		return pnrPaxGroupId;
	}

	public void setPnrPaxGroupId(String pnrPaxGroupId) {
		this.pnrPaxGroupId = pnrPaxGroupId;
	}

	public String getCsFlightNo() {
		return csFlightNo;
	}

	public void setCsFlightNo(String csFlightNo) {
		this.csFlightNo = csFlightNo;
	}

	public String getCsBookingClass() {
		return csBookingClass;
	}

	public void setCsBookingClass(String csBookingClass) {
		this.csBookingClass = csBookingClass;
	}

	public Date getCsDepatureDateTime() {
		return csDepatureDateTime;
	}

	public void setCsDepatureDateTime(Date csDepatureDateTime) {
		this.csDepatureDateTime = csDepatureDateTime;
	}

	public String getCsOrginDestination() {
		return csOrginDestination;
	}

	public void setCsOrginDestination(String csOrginDestination) {
		this.csOrginDestination = csOrginDestination;
	}

	public boolean isCodeShareFlight() {
		return codeShareFlight;
	}

	public void setCodeShareFlight(boolean codeShareFlight) {
		this.codeShareFlight = codeShareFlight;
	}

	public String getExternalPnr() {
		return externalPnr;
	}

	public void setExternalPnr(String externalPnr) {
		this.externalPnr = externalPnr;
	}

	public String getNationalIDNo() {
		return nationalIDNo;
	}

	public void setNationalIDNo(String nationalIDNo) {
		this.nationalIDNo = nationalIDNo;
	}

	public List<AncillaryDTO> getSeats() {
		return seats;
	}

	public void setSeats(List<AncillaryDTO> seats) {
		this.seats = seats;
	}

	public List<AncillaryDTO> getMeals() {
		return meals;
	}

	public void setMeals(List<AncillaryDTO> meals) {
		this.meals = meals;
	}

	public List<AncillaryDTO> getSsrs() {
		return ssrs;
	}

	public void setSsrs(List<AncillaryDTO> ssrs) {
		this.ssrs = ssrs;
	}

	public AncillaryDTO getBaggages() {
		return baggages;
	}

	public void setBaggages(AncillaryDTO baggages) {
		this.baggages = baggages;
	}

	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}
	public String getFFID() {
		return FFID;
	}

	public void setFFID(String fFID) {
		FFID = fFID;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public Integer getJourneySequence() {
		return journeySequence;
	}

	public void setJourneySequence(Integer journeySequence) {
		this.journeySequence = journeySequence;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Integer getVoucherToFlightSegID() {
		return voucherToFlightSegID;
	}

	public void setVoucherToFlightSegID(Integer voucherToFlightSegID) {
		this.voucherToFlightSegID = voucherToFlightSegID;
	}
	
	
}
