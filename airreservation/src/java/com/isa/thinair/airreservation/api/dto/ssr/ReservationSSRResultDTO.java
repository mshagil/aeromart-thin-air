package com.isa.thinair.airreservation.api.dto.ssr;

import java.io.Serializable;

public class ReservationSSRResultDTO implements Serializable {

	private static final long serialVersionUID = 6309539730957882133L;
	private Integer ppssId;
	private Integer ssrCategoryId;
	private Integer ssrSubCategoryId;
	private String carrierNotifyEmail;

	private String pnr;
	private String timeStamp;
	private String flightNo;
	private String title;
	private String firstName;
	private String lastName;
	private String contactNo;
	private String serviceTime;
	private String serviceDescription;

	/** param used by airport transfer **/
	private String airportCode;
	private String address;
	private String type;
	private String segmentCode;
	private String serviceDate;

	public Integer getPpssId() {
		return ppssId;
	}

	public void setPpssId(Integer ppssId) {
		this.ppssId = ppssId;
	}

	public Integer getSsrCategoryId() {
		return this.ssrCategoryId;
	}

	public void setSsrCategoryId(Integer ssrCategoryId) {
		this.ssrCategoryId = ssrCategoryId;
	}

	public Integer getSsrSubCategoryId() {
		return this.ssrSubCategoryId;
	}

	public void setSsrSubCategoryId(Integer subCategoryId) {
		this.ssrSubCategoryId = subCategoryId;
	}

	public String getCarrierNotifyEmail() {
		return carrierNotifyEmail;
	}

	public void setCarrierNotifyEmail(String carrierNotifyEmail) {
		this.carrierNotifyEmail = carrierNotifyEmail;
	}

	public String getTimeStamp() {
		return this.timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getPnr() {
		return this.pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNo() {
		return this.contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getServiceTime() {
		return this.serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}

	public String getServiceDescription() {
		return this.serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}

	@Override
	public String toString() {
		return "ReservationSSRResultDTO [pNRNo=" + pnr + ", timeStamp=" + timeStamp + ", flightNo=" + flightNo + ", title="
				+ title + ", firstName=" + firstName + ", lastName=" + lastName + ", contactNo=" + contactNo + ", serviceTime="
				+ serviceTime + ", serviceDescription=" + serviceDescription + ", airportCode=" + airportCode + ", address="
				+ address + ", type=" + type + ", segmentCode=" + segmentCode + ", serviceDate=" + serviceDate + "]";
	}

}
