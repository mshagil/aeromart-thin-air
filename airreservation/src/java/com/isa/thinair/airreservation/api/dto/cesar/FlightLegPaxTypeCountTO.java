package com.isa.thinair.airreservation.api.dto.cesar;

import java.io.Serializable;

public class FlightLegPaxTypeCountTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4897128015412302163L;
	private int flightLegId;
	private PaxTypeCountTO confirmedPaxSummary;
	private PaxTypeCountTO onholdPaxSummary;

	public int getFlightLegId() {
		return flightLegId;
	}

	public void setFlightLegId(int flightLegId) {
		this.flightLegId = flightLegId;
	}

	public PaxTypeCountTO getConfirmedPaxSummary() {
		return confirmedPaxSummary;
	}

	public void setConfirmedPaxSummary(PaxTypeCountTO confirmedPaxSummary) {
		this.confirmedPaxSummary = confirmedPaxSummary;
	}

	public PaxTypeCountTO getOnholdPaxSummary() {
		return onholdPaxSummary;
	}

	public void setOnholdPaxSummary(PaxTypeCountTO onholdPaxSummary) {
		this.onholdPaxSummary = onholdPaxSummary;
	}

}
