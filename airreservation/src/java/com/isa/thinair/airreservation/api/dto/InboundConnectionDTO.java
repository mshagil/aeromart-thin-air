package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class InboundConnectionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1437606842129788189L;

	private String flightNumber;

	private String fareClass;

	private String date;

	private String departureStation;

	private String arrivalTime;

	private String reservationStatus; // optional

	private String arrivalStation; // optional
	
	private String segmentCode;
	
	private int dateDifference;

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getFareClass() {
		return fareClass;
	}

	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getDateDifference() {
		return dateDifference;
	}

	public void setDateDifference(int dateDifference) {
		this.dateDifference = dateDifference;
	}

}
