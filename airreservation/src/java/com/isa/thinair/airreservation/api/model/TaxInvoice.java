package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author dilan
 * @hibernate.class table = "T_TAX_INVOICE"
 */
public class TaxInvoice extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer taxInvoiceId;
	private String invoiceType;
	private Integer tnxSeq;
	private Date dateOfIssue;
	private String taxRegistrationNumber;
	private String stateCode;
	private Integer taxRate1Id;
	private BigDecimal taxRate1Amount;
	private Integer taxRate2Id;
	private BigDecimal taxRate2Amount;
	private Integer taxRate3Id;
	private BigDecimal taxRate3Amount;
	private String originalPnr;
	private Integer originalTaxInvoiceId;
	private String currencyCode;
	private BigDecimal totalDiscount;
	private BigDecimal taxableAmount;
	private BigDecimal nonTaxableAmount;
	private String journeyOND;

	private Set<Reservation> reservations;
	
	private String nameOfTheAirline;
	private String airlineOfficeSTAddress1;
	private String airlineOfficeSTAddress2;
	private String airlineOfficeCity;
	private String airlineOfficeCountryCode;
	private String gstinForInvoiceState;
	private String nameOfTheRecipient;
	private String recipientStreetAddress1;
	private String recipientStreetAddress2;
	private String recipientCity;
	private String recipientCountryCode;
	private String gstinOfTheRecipient;
	private String stateOfTheRecipient;
	private String stateCodeOfTheRecipient;
	private String accountCodeOfService;
	private String descriptionOfService;
	// taxable + non taxable amount
	private BigDecimal totalValueOfService;
	private String placeOfSupply;
	private String placeOfSupplyState;
	private String digitalSignForInvoiceState;
	// tax rate 1 - cgst
	private double taxRate1Percentage;
	// tax rate 2 - sgst
	private double taxRate2Percentage;
	// tax rate 3 - igst
	private double taxRate3Percentage;
	// cgst + sgst || igst
	private BigDecimal totalTaxAmount = BigDecimal.ZERO;
	// for debit note
	private Date dateOfIssueOriginalTaxInvoice;

	private String taxInvoiceNumber;
	private String originalTaxInvoiceNumber;
	private String stateName;

	/**
	 * @hibernate.id column = "TI_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_TAX_INVOICE"
	 * 
	 */
	public Integer getTaxInvoiceId() {
		return taxInvoiceId;
	}

	public void setTaxInvoiceId(Integer taxInvoiceId) {
		this.taxInvoiceId = taxInvoiceId;
	}

	/**
	 * @hibernate.property column = "INVOICE_TYPE"
	 * 
	 */
	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	/**
	 * @hibernate.property column = "TNX_SEQ"
	 * 
	 */
	public Integer getTnxSeq() {
		return tnxSeq;
	}

	public void setTnxSeq(Integer tnxSeq) {
		this.tnxSeq = tnxSeq;
	}

	/**
	 * @hibernate.property column = "DATE_OF_ISSUE"
	 */
	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	/**
	 * @hibernate.property column = "TAX_REG_NO"
	 */
	public String getTaxRegistrationNumber() {
		return taxRegistrationNumber;
	}

	public void setTaxRegistrationNumber(String taxRegistrationNumber) {
		this.taxRegistrationNumber = taxRegistrationNumber;
	}

	/**
	 * @hibernate.property column = "STATE"
	 */
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	/**
	 * @hibernate.property column = "TAX1_RATE_ID"
	 */
	public Integer getTaxRate1Id() {
		return taxRate1Id;
	}

	public void setTaxRate1Id(Integer taxRate1Id) {
		this.taxRate1Id = taxRate1Id;
	}

	/**
	 * @hibernate.property column = "TAX1_AMOUNT"
	 */
	public BigDecimal getTaxRate1Amount() {
		return taxRate1Amount;
	}

	public void setTaxRate1Amount(BigDecimal taxRate1Amount) {
		this.taxRate1Amount = taxRate1Amount;
	}

	/**
	 * @hibernate.property column = "TAX2_RATE_ID"
	 */
	public Integer getTaxRate2Id() {
		return taxRate2Id;
	}

	public void setTaxRate2Id(Integer taxRate2Id) {
		this.taxRate2Id = taxRate2Id;
	}

	/**
	 * @hibernate.property column = "TAX2_AMOUNT"
	 */
	public BigDecimal getTaxRate2Amount() {
		return taxRate2Amount;
	}

	public void setTaxRate2Amount(BigDecimal taxRate2Amount) {
		this.taxRate2Amount = taxRate2Amount;
	}

	/**
	 * @hibernate.property column = "TAX3_RATE_ID"
	 */
	public Integer getTaxRate3Id() {
		return taxRate3Id;
	}

	public void setTaxRate3Id(Integer taxRate3Id) {
		this.taxRate3Id = taxRate3Id;
	}

	/**
	 * @hibernate.property column = "TAX3_AMOUNT"
	 */
	public BigDecimal getTaxRate3Amount() {
		return taxRate3Amount;
	}

	public void setTaxRate3Amount(BigDecimal taxRate3Amount) {
		this.taxRate3Amount = taxRate3Amount;
	}

	/**
	 * @hibernate.property column = "ORIGINAL_PNR"
	 */
	public String getOriginalPnr() {
		return originalPnr;
	}

	public void setOriginalPnr(String originalPnr) {
		this.originalPnr = originalPnr;
	}

	/**
	 * @hibernate.property column = "ORIGINAL_TI_ID"
	 */
	public Integer getOriginalTaxInvoiceId() {
		return originalTaxInvoiceId;
	}

	public void setOriginalTaxInvoiceId(Integer originalTaxInvoiceId) {
		this.originalTaxInvoiceId = originalTaxInvoiceId;
	}

	/**
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @hibernate.property column = "DISCOUNT"
	 */
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	/**
	 * @hibernate.property column = "TAXABLE_AMOUNT"
	 */
	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	/**
	 * @hibernate.property column = "NON_TAXABLE_AMOUNT"
	 */
	public BigDecimal getNonTaxableAmount() {
		return nonTaxableAmount;
	}

	public void setNonTaxableAmount(BigDecimal nonTaxableAmount) {
		this.nonTaxableAmount = nonTaxableAmount;
	}

	/**
	 * @hibernate.property column = "INVOICE_NUMBER"
	 */
	public String getTaxInvoiceNumber() {
		return taxInvoiceNumber;
	}

	public void setTaxInvoiceNumber(String taxInvoiceNumber) {
		this.taxInvoiceNumber = taxInvoiceNumber;
	}

	/**
	 * @hibernate.set table="T_PNR_TAX_INVOICE"
	 * @hibernate.collection-key column="TI_ID"
	 * @hibernate.collection-many-to-many class="com.isa.thinair.airreservation.api.model.Reservation" column="PNR"
	 * 
	 */
	public Set<Reservation> getReservations() {
		return this.reservations;
	}

	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}

	public void addReservation(Reservation reservation) {
		if (getReservations() == null) {
			setReservations(new HashSet<>());
		}
		getReservations().add(reservation);
	}

	public String getNameOfTheAirline() {
		return nameOfTheAirline;
	}

	public void setNameOfTheAirline(String nameOfTheAirline) {
		this.nameOfTheAirline = nameOfTheAirline;
	}

	public String getAirlineOfficeSTAddress1() {
		return airlineOfficeSTAddress1;
	}

	public void setAirlineOfficeSTAddress1(String airlineOfficeSTAddress1) {
		this.airlineOfficeSTAddress1 = airlineOfficeSTAddress1;
	}

	public String getAirlineOfficeSTAddress2() {
		return airlineOfficeSTAddress2;
	}

	public void setAirlineOfficeSTAddress2(String airlineOfficeSTAddress2) {
		this.airlineOfficeSTAddress2 = airlineOfficeSTAddress2;
	}

	public String getAirlineOfficeCity() {
		return airlineOfficeCity;
	}

	public void setAirlineOfficeCity(String airlineOfficeCity) {
		this.airlineOfficeCity = airlineOfficeCity;
	}

	public String getAirlineOfficeCountryCode() {
		return airlineOfficeCountryCode;
	}

	public void setAirlineOfficeCountryCode(String airlineOfficeCountryCode) {
		this.airlineOfficeCountryCode = airlineOfficeCountryCode;
	}

	public String getGstinForInvoiceState() {
		return gstinForInvoiceState;
	}

	public void setGstinForInvoiceState(String gstinForInvoiceState) {
		this.gstinForInvoiceState = gstinForInvoiceState;
	}

	public String getNameOfTheRecipient() {
		return nameOfTheRecipient;
	}

	public void setNameOfTheRecipient(String nameOfTheRecipient) {
		this.nameOfTheRecipient = nameOfTheRecipient;
	}

	public String getRecipientStreetAddress1() {
		return recipientStreetAddress1;
	}

	public void setRecipientStreetAddress1(String recipientStreetAddress1) {
		this.recipientStreetAddress1 = recipientStreetAddress1;
	}

	public String getRecipientStreetAddress2() {
		return recipientStreetAddress2;
	}

	public void setRecipientStreetAddress2(String recipientStreetAddress2) {
		this.recipientStreetAddress2 = recipientStreetAddress2;
	}

	public String getRecipientCity() {
		return recipientCity;
	}

	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}

	public String getRecipientCountryCode() {
		return recipientCountryCode;
	}

	public void setRecipientCountryCode(String recipientCountryCode) {
		this.recipientCountryCode = recipientCountryCode;
	}

	public String getGstinOfTheRecipient() {
		return gstinOfTheRecipient;
	}

	public void setGstinOfTheRecipient(String gstinOfTheRecipient) {
		this.gstinOfTheRecipient = gstinOfTheRecipient;
	}

	public String getStateOfTheRecipient() {
		return stateOfTheRecipient;
	}

	public void setStateOfTheRecipient(String stateOfTheRecipient) {
		this.stateOfTheRecipient = stateOfTheRecipient;
	}

	public String getStateCodeOfTheRecipient() {
		return stateCodeOfTheRecipient;
	}

	public void setStateCodeOfTheRecipient(String stateCodeOfTheRecipient) {
		this.stateCodeOfTheRecipient = stateCodeOfTheRecipient;
	}

	public String getAccountCodeOfService() {
		return accountCodeOfService;
	}

	public void setAccountCodeOfService(String accountCodeOfService) {
		this.accountCodeOfService = accountCodeOfService;
	}

	public String getDescriptionOfService() {
		return descriptionOfService;
	}

	public void setDescriptionOfService(String descriptionOfService) {
		this.descriptionOfService = descriptionOfService;
	}

	public BigDecimal getTotalValueOfService() {
		return totalValueOfService;
	}

	public void setTotalValueOfService(BigDecimal totalValueOfService) {
		this.totalValueOfService = totalValueOfService;
	}

	public String getPlaceOfSupply() {
		return placeOfSupply;
	}

	public void setPlaceOfSupply(String placeOfSupply) {
		this.placeOfSupply = placeOfSupply;
	}

	public String getPlaceOfSupplyState() {
		return placeOfSupplyState;
	}

	public void setPlaceOfSupplyState(String placeOfSupplyState) {
		this.placeOfSupplyState = placeOfSupplyState;
	}

	public String getDigitalSignForInvoiceState() {
		return digitalSignForInvoiceState;
	}

	public void setDigitalSignForInvoiceState(String digitalSignForInvoiceState) {
		this.digitalSignForInvoiceState = digitalSignForInvoiceState;
	}

	public double getTaxRate1Percentage() {
		return taxRate1Percentage;
	}

	public void setTaxRate1Percentage(double taxRate1Percentage) {
		this.taxRate1Percentage = taxRate1Percentage;
	}

	public double getTaxRate2Percentage() {
		return taxRate2Percentage;
	}

	public void setTaxRate2Percentage(double taxRate2Percentage) {
		this.taxRate2Percentage = taxRate2Percentage;
	}

	public double getTaxRate3Percentage() {
		return taxRate3Percentage;
	}

	public void setTaxRate3Percentage(double taxRate3Percentage) {
		this.taxRate3Percentage = taxRate3Percentage;
	}

	public BigDecimal getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	public Date getDateOfIssueOriginalTaxInvoice() {
		return dateOfIssueOriginalTaxInvoice;
	}

	public void setDateOfIssueOriginalTaxInvoice(Date dateOfIssueOriginalTaxInvoice) {
		this.dateOfIssueOriginalTaxInvoice = dateOfIssueOriginalTaxInvoice;
	}

	public String getOriginalTaxInvoiceNumber() {
		return originalTaxInvoiceNumber;
	}

	public void setOriginalTaxInvoiceNumber(String originalTaxInvoiceNumber) {
		this.originalTaxInvoiceNumber = originalTaxInvoiceNumber;
	}

	public String getJourneyOND() {
		return journeyOND;
	}

	public void setJourneyOND(String journeyOND) {
		this.journeyOND = journeyOND;
	}

}
