/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * The model to keep track of reservation passenger segment level charges
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PNR_PAX_SEG_CHARGES"
 */
public class ReservationPaxSegCharge extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3681241769764829271L;

	/** Holds Reservation fare segment charge id */
	private Integer pnrFareSegChgId;

	/** Holds Reservation passenger ond charge id */
	private Integer pnrPaxOndChgId;

	/** Holds Reservation passenger fare segment id */
	private Integer pnrPaxFareSegId;

	/** Holds charge amount */
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the pnrPaxFareSegId.
	 * @hibernate.property column = "PPFS_ID"
	 */
	public Integer getPnrPaxFareSegId() {
		return pnrPaxFareSegId;
	}

	/**
	 * @param pnrPaxFareSegId
	 *            The pnrPaxFareSegId to set.
	 */
	public void setPnrPaxFareSegId(Integer pnrPaxFareSegId) {
		this.pnrPaxFareSegId = pnrPaxFareSegId;
	}

	/**
	 * @return Returns the pnrFareSegChgId.
	 * @hibernate.id column = "PFST_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_SEG_CHARGES"
	 */
	public Integer getPnrFareSegChgId() {
		return pnrFareSegChgId;
	}

	/**
	 * @param pnrFareSegChgId
	 *            The pnrFareSegChgId to set.
	 */
	public void setPnrFareSegChgId(Integer pnrFareSegChgId) {
		this.pnrFareSegChgId = pnrFareSegChgId;
	}

	/**
	 * @return Returns the pnrPaxOndChgId.
	 * @hibernate.property column = "PFT_ID"
	 */
	public Integer getPnrPaxOndChgId() {
		return pnrPaxOndChgId;
	}

	/**
	 * @param pnrPaxOndChgId
	 *            The pnrPaxOndChgId to set.
	 */
	public void setPnrPaxOndChgId(Integer pnrPaxOndChgId) {
		this.pnrPaxOndChgId = pnrPaxOndChgId;
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationPaxSegCharge) {
			ReservationPaxSegCharge castObject = (ReservationPaxSegCharge) o;
			if (castObject.getPnrFareSegChgId() == null || this.getPnrFareSegChgId() == null) {
				return false;
			} else if (castObject.getPnrFareSegChgId().intValue() == this.getPnrFareSegChgId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPnrFareSegChgId()).toHashCode();
	}
}
