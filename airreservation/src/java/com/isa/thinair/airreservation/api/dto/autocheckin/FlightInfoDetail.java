/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class FlightInfoDetail implements Serializable {

	private static final long serialVersionUID = 1017346575726387287L;
	private FlightLegInfo flightLegInfo;

	public FlightLegInfo getFlightLegInfo() {
		return flightLegInfo;
	}

	public void setFlightLegInfo(FlightLegInfo flightLegInfo) {
		this.flightLegInfo = flightLegInfo;
	}

}
