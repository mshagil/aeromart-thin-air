package com.isa.thinair.airreservation.api.dto;

/**
 * Capture the customer payment related info
 * 
 * @author mekanayake
 * 
 */
public interface CustomerPayment {

	/**
	 * Set the payCarrier for the payment
	 * 
	 * @return
	 */
	public String getPayCarrier();

	/**
	 * Get the payCarrer associating the payment
	 * 
	 * @param payCarrier
	 */
	public void setPayCarrier(String payCarrier);

	/**
	 * @return the lccUniqueId
	 */
	public String getLccUniqueId();

	/**
	 * @param lccUniqueId
	 *            the lccUniqueId to set
	 */
	public void setLccUniqueId(String lccUniqueId);

}
