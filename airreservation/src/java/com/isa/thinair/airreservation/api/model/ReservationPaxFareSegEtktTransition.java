package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

/**
 * @author Manoj Dhanushka
 * @hibernate.class table = "T_PNR_PAX_FARE_SEG_ETKT_AUDIT"
 */
public class ReservationPaxFareSegEtktTransition implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer paxFareSegmentEticketTranId;

	private Integer paxFareSegmentEticketId;
	
	private String fromStatus;
	
	private String toStatus;
	
	private boolean inconsistentStatus;

	/**
	 * 
	 * @hibernate.id column = "PNR_PAX_FARE_SEG_ETKT_AUDIT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_FARE_SEG_ETKT_AUDIT"
	 */
	public Integer getPaxFareSegmentEticketTranId() {
		return paxFareSegmentEticketTranId;
	}

	/**
	 * @param paxFareSegmentEticketTranId the paxFareSegmentEticketTranId to set
	 */
	public void setPaxFareSegmentEticketTranId(Integer paxFareSegmentEticketTranId) {
		this.paxFareSegmentEticketTranId = paxFareSegmentEticketTranId;
	}

	/**
	 * @return the paxFareSegmentEticketId
	 * @hibernate.property column = "PNR_PAX_FARE_SEG_E_TICKET_ID"
	 */
	public Integer getPaxFareSegmentEticketId() {
		return paxFareSegmentEticketId;
	}

	/**
	 * @param paxFareSegmentEticketId the paxFareSegmentEticketId to set
	 */
	public void setPaxFareSegmentEticketId(Integer paxFareSegmentEticketId) {
		this.paxFareSegmentEticketId = paxFareSegmentEticketId;
	}

	/**
	 * @return the fromStatus
	 * @hibernate.property column = "FROM_STATUS"
	 */
	public String getFromStatus() {
		return fromStatus;
	}

	/**
	 * @param fromStatus the fromStatus to set
	 */
	public void setFromStatus(String fromStatus) {
		this.fromStatus = fromStatus;
	}

	/**
	 * @return the toStatus
	 * @hibernate.property column = "TO_STATUS"
	 */
	public String getToStatus() {
		return toStatus;
	}

	/**
	 * @param toStatus the toStatus to set
	 */
	public void setToStatus(String toStatus) {
		this.toStatus = toStatus;
	}

	/**
	 * @return the inconsistentStatus
	 * @hibernate.property column = "INCONSISTENT_TRANSITION" type="yes_no"
	 */
	public boolean getInconsistentStatus() {
		return inconsistentStatus;
	}

	/**
	 * @param inconsistentStatus the inconsistentStatus to set
	 */
	public void setInconsistentStatus(boolean inconsistentStatus) {
		this.inconsistentStatus = inconsistentStatus;
	}

}
