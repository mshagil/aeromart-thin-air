/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * Holds the pfs atomic record specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class RecordDTO {

	/** Holds the passenger number */
	private String paxNumber;

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the passenger first name with title */
	private String firstNameWithTitle;

	/** Holds the ticket number which includes .L/170781/5 format */
	private String ticketnumber;

	private String title;

	private String midName = "";
	
	/** Holds the marketing flight information which includes .M/MH2370F05IKADXB1810HK format */
	private String marketingFlight;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the pnr number
	 * 
	 * @return
	 */
	public String getPnrNumber() {
		if (this.getTicketnumber() != null && this.getTicketnumber().length() > 0) {
			String pnr = this.getTicketnumber().substring(2);
			return pnr.replaceAll("/", "").trim();
		} else {
			return null;
		}
	}

	/**
	 * Set the ticket number
	 * 
	 * @param ticketnumber
	 */
	public void setTicketnumber(String ticketnumber) {
		this.ticketnumber = BeanUtils.nullHandler(ticketnumber);
	}

	/**
	 * Add the first Name with title
	 * 
	 * @param fName
	 */
	public void addFName(String fName) {
		if (this.getFirstNameWithTitle() == null) {
			this.setFirstNameWithTitle(fName);
		} else if (isATitle(fName)) {
			this.setFirstNameWithTitle(this.getFirstNameWithTitle() + fName);
		} else {
			this.setFirstNameWithTitle(this.getFirstNameWithTitle() + " " + fName);
		}
	}

	/**
	 * @return Returns the paxNumber.
	 */
	public String getPaxNumber() {
		return paxNumber;
	}

	/**
	 * @param paxNumber
	 *            The paxNumber to set.
	 */
	public void setPaxNumber(String paxNumber) {
		this.paxNumber = paxNumber;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the ticketnumber.
	 */
	public String getTicketnumber() {
		return ticketnumber;
	}

	/**
	 * Returns the title in order to parse
	 * 
	 * @return
	 */
	public String getParseTitle() {
		if (!this.getMidName().isEmpty()) {
			return this.getMidName();
		} else if (this.getFirstNameWithTitle() == null) {
			return this.getLastName();
		} else {
			return this.getFirstNameWithTitle();
		}
	}

	/**
	 * Clones the RecordDTO object
	 */
	public Object clone() {
		RecordDTO recordDTO = new RecordDTO();
		recordDTO.setFirstNameWithTitle(this.getFirstNameWithTitle());
		recordDTO.setLastName(this.getLastName());
		recordDTO.setPaxNumber(this.getPaxNumber());
		recordDTO.setTicketnumber(this.getTicketnumber());
		recordDTO.setMarketingFlight(this.getMarketingFlight());

		return recordDTO;
	}

	/**
	 * @return Returns the firstNameWithTitle.
	 */
	public String getFirstNameWithTitle() {
		return firstNameWithTitle;
	}

	/**
	 * @param firstNameWithTitle
	 *            The firstNameWithTitle to set.
	 */
	public void setFirstNameWithTitle(String firstNameWithTitle) {
		this.firstNameWithTitle = firstNameWithTitle;
	}

	private boolean isATitle(String name) {

		if (name.equals("MR")) {
			return true;
		} else if (name.equals("MS")) {
			return true;
		} else if (name.equals("CHD")) {
			return true;
		} else if (name.equals("MRS")) {
			return true;
		} else if (name.equals("MISS")) {
			return true;
		} else if (name.equals("MSTR")) {
			return true;
		}
		return false;
	}

	public String getMidName() {
		return midName;
	}

	public void setMidName(String midName) {
		this.midName = midName;
	}

	public String getMarketingFlight() {
		if (this.marketingFlight != null && this.marketingFlight.length() > 0) {
			return this.marketingFlight.replaceAll(".M/", "").trim();
		} else {
			return null;
		}
	}

	public void setMarketingFlight(String marketingFlight) {
		this.marketingFlight = BeanUtils.nullHandler(marketingFlight);
	}
}
