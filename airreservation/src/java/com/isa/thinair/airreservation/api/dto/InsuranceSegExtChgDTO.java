package com.isa.thinair.airreservation.api.dto;

public class InsuranceSegExtChgDTO extends ExternalChgDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	

	public InsuranceSegExtChgDTO(ExternalChgDTO clone) {
		setChargeCode(clone.getChargeCode());
		setChargeDescription(clone.getChargeDescription());
		setChgGrpCode(clone.getChgGrpCode());
		setChgRateId(clone.getChgRateId());
		setExternalChargesEnum(clone.getExternalChargesEnum());
		setAmount(clone.getAmount());
		setRatioValueInPercentage(clone.isRatioValueInPercentage());
		setRatioValue(clone.getRatioValue());
		setAmountConsumedForPayment(clone.isAmountConsumedForPayment());
		setAppliesToInfant(clone.isAppliesToInfant());
		setAppliesTo(clone.getAppliesTo());
		setChargeValueMin(clone.getChargeValueMin());
		setChargeValueMax(clone.getChargeValueMax());
		setChargeBasis(clone.getChargeBasis());
		setJourneyType(clone.getJourneyType());
		setValid(clone.isValid());
		setChargeRates(clone.getChargeRates());
		setBreakPoint(clone.getBreakPoint());
		setBoundryValue(clone.getBoundryValue());
	}
}
