package com.isa.thinair.airreservation.api.dto.aig;

import java.io.Serializable;
import java.math.BigDecimal;

public class INSChargeTO implements Serializable {

	private static final long serialVersionUID = 8007848307089650714L;
	private Integer pkey;
	private Integer pnrPaxFareId;
	private Integer pnrPaxId;
	private Integer chargeRateId;
	private String chargeGroup;
	private BigDecimal amount;

	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	public Integer getChargeRateId() {
		return chargeRateId;
	}

	public void setChargeRateId(Integer chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	public String getChargeGroup() {
		return chargeGroup;
	}

	public void setChargeGroup(String chargeGroup) {
		this.chargeGroup = chargeGroup;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getPkey() {
		return pkey;
	}

	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

}
