/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * To hold reservation payment data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationPaymentDTO extends ReservationSearchDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds the last 4 digits of the credit card number */
	private String creditCardNo;

	/** Holds expiry Date HHMM format */
	private String eDate;

	/** Holds CC Authorization Code */
	private String ccAuthorCode;

	/**
	 * @return Returns the creditCardNo.
	 */
	public String getCreditCardNo() {
		return creditCardNo;
	}

	/**
	 * @param creditCardNo
	 *            The creditCardNo to set.
	 */
	public void setCreditCardNo(String creditCardNo) {
		this.creditCardNo = creditCardNo;
	}

	/**
	 * @return Returns the eDate.
	 */
	public String getEDate() {
		return eDate;
	}

	/**
	 * @param date
	 *            The eDate to set.
	 */
	public void setEDate(String date) {
		eDate = date;
	}

	/**
	 * @return Returns the ccAuthorCode.
	 */
	public String getCcAuthorCode() {
		return ccAuthorCode;
	}

	/**
	 * @param ccAuthorCode
	 *            The ccAuthorCode to set.
	 */
	public void setCcAuthorCode(String ccAuthorCode) {
		this.ccAuthorCode = ccAuthorCode;
	}

}
