/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

/**
 * To hold reservation external charges data transfer information
 * 
 * @author Rikaz
 */
public class ServiceTaxExtCharges extends ExternalChgDTO {

	private static final long serialVersionUID = -3428414375607766976L;

	private Collection<ServiceTaxExtChgDTO> serviceTaxes;

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		ServiceTaxExtCharges clone = new ServiceTaxExtCharges();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setFlightSegId(this.getFlightSegId());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());

		return clone;
	}

	public Collection<ServiceTaxExtChgDTO> getServiceTaxes() {
		if (this.serviceTaxes == null)
			this.serviceTaxes = new ArrayList<ServiceTaxExtChgDTO>();

		return this.serviceTaxes;
	}

	public void setServiceTaxes(Collection<ServiceTaxExtChgDTO> serviceTaxes) {
		this.serviceTaxes = serviceTaxes;
	}

	public void addServiceTaxes(ServiceTaxExtChgDTO serviceTax) {
		getServiceTaxes().add(serviceTax);
	}
	
	public ServiceTaxExtChgDTO getRespectiveServiceChgDTO(String chargeCode) {
		for (ServiceTaxExtChgDTO serviceTaxes : getServiceTaxes()) {
			if(serviceTaxes.getChargeCode().equalsIgnoreCase(chargeCode)){
				return serviceTaxes;
			}
			
		}
		return null;
	}

}
