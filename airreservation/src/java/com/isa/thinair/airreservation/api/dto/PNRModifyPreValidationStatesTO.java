package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * @author Nilindra Fernando
 * @since 11:50 AM 5/6/2008
 */
public class PNRModifyPreValidationStatesTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5854360424912132047L;

	private boolean isInterlinedModifiable;

	private boolean isCancelled;

	private boolean isRestricted;

	private boolean isFlown;

	private boolean isInBufferTime;

	private boolean hasFareRuleBuffer;

	private boolean hasNOSHOWPax;

	private boolean isSpecialFare;
	/* segment modification */
	private boolean allowModifySegDate;

	private boolean allowModifySegRoute;

	/**
	 * @return Returns the isCancelled.
	 */
	public boolean isCancelled() {
		return isCancelled;
	}

	/**
	 * @param isCancelled
	 *            The isCancelled to set.
	 */
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

	/**
	 * @return Returns the isFlown.
	 */
	public boolean isFlown() {
		return isFlown;
	}

	/**
	 * @param isFlown
	 *            The isFlown to set.
	 */
	public void setFlown(boolean isFlown) {
		this.isFlown = isFlown;
	}

	/**
	 * @return Returns the isInBufferTime.
	 */
	public boolean isInBufferTime() {
		return isInBufferTime;
	}

	/**
	 * @param isInBufferTime
	 *            The isInBufferTime to set.
	 */
	public void setInBufferTime(boolean isInBufferTime) {
		this.isInBufferTime = isInBufferTime;
	}

	/**
	 * @return Returns the isInterlinedModifiable.
	 */
	public boolean isInterlinedModifiable() {
		return isInterlinedModifiable;
	}

	/**
	 * @param isInterlinedModifiable
	 *            The isInterlinedModifiable to set.
	 */
	public void setInterlinedModifiable(boolean isInterlinedModifiable) {
		this.isInterlinedModifiable = isInterlinedModifiable;
	}

	/**
	 * @return Returns the isRestricted.
	 */
	public boolean isRestricted() {
		return isRestricted;
	}

	/**
	 * @param isRestricted
	 *            The isRestricted to set.
	 */
	public void setRestricted(boolean isRestricted) {
		this.isRestricted = isRestricted;
	}

	/**
	 * @return Returns the hasFareRuleBuffer.
	 */
	public boolean isHasFareRuleBuffer() {
		return hasFareRuleBuffer;
	}

	/**
	 * @param hasFareRuleBuffer
	 *            The hasFareRuleBuffer to set.
	 */
	public void setHasFareRuleBuffer(boolean hasFareRuleBuffer) {
		this.hasFareRuleBuffer = hasFareRuleBuffer;
	}

	/**
	 * @return Returns the hasNOSHOWPax.
	 */
	public boolean isHasNOSHOWPax() {
		return hasNOSHOWPax;
	}

	/**
	 * @param hasNOSHOWPax
	 *            The hasNOSHOWPax to set.
	 */
	public void setHasNOSHOWPax(boolean hasNOSHOWPax) {
		this.hasNOSHOWPax = hasNOSHOWPax;
	}

	/**
	 * @return Returns the isSpecialFare.
	 */
	public boolean isSpecialFare() {
		return isSpecialFare;
	}

	/**
	 * @param isSpecialFare
	 *            The isSpecialFare to set.
	 */
	public void setSpecialFare(boolean isSpecialFare) {
		this.isSpecialFare = isSpecialFare;
	}

	public boolean isAllowModifySegDate() {
		return allowModifySegDate;
	}

	public void setAllowModifySegDate(boolean allowModifySegDate) {
		this.allowModifySegDate = allowModifySegDate;
	}

	public boolean isAllowModifySegRoute() {
		return allowModifySegRoute;
	}

	public void setAllowModifySegRoute(boolean allowModifySegRoute) {
		this.allowModifySegRoute = allowModifySegRoute;
	}
}
