package com.isa.thinair.airreservation.api.dto.ssr;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.SsrPaxSegments;

/**
 * <h1>CommonMedicalSsrParamDTO</h1>
 * 
 * <p>
 * This contains the content of the medical ssr email.
 *
 * @author Shiluka Dharmasena
 * @since 2016-09-28
 */
public class CommonMedicalSsrParamDTO implements Serializable {

	private static final long serialVersionUID = 987654123456L;

	private List<SsrPaxSegments> paxWiseMedicalSsrSegments;
	private CommonReservationContactInfo contactInfo;
	private String linkToFillMedicalSSRInfo;
	private String medicalSSREmailLanguage;
	private String medicalSSREmailTopic;
	private String medicalSSREmailMsg;
	private String pnr;

	/**
	 * @return the paxWiseMedicalSsrSegments
	 */
	public List<SsrPaxSegments> getPaxWiseMedicalSsrSegments() {
		return paxWiseMedicalSsrSegments;
	}

	/**
	 * @param paxWiseMedicalSsrSegments
	 *            the paxWiseMedicalSsrSegments to set
	 */
	public void setPaxWiseMedicalSsrSegments(List<SsrPaxSegments> paxWiseMedicalSsrSegments) {
		this.paxWiseMedicalSsrSegments = paxWiseMedicalSsrSegments;
	}

	/**
	 * @return the contactInfo
	 */
	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the linkToFillMedicalSSRInfo
	 */
	public String getLinkToFillMedicalSSRInfo() {
		return linkToFillMedicalSSRInfo;
	}

	/**
	 * @param linkToFillMedicalSSRInfo
	 *            the linkToFillMedicalSSRInfo to set
	 */
	public void setLinkToFillMedicalSSRInfo(String linkToFillMedicalSSRInfo) {
		this.linkToFillMedicalSSRInfo = linkToFillMedicalSSRInfo;
	}

	/**
	 * @return the medicalSSREmailLanguage
	 */
	public String getMedicalSSREmailLanguage() {
		return medicalSSREmailLanguage;
	}

	/**
	 * @param medicalSSREmailLanguage
	 *            the medicalSSREmailLanguage to set
	 */
	public void setMedicalSSREmailLanguage(String medicalSSREmailLanguage) {
		this.medicalSSREmailLanguage = medicalSSREmailLanguage;
	}

	/**
	 * @return the medicalSSREmailTopic
	 */
	public String getMedicalSSREmailTopic() {
		return medicalSSREmailTopic;
	}

	/**
	 * @param medicalSSREmailTopic
	 *            the medicalSSREmailTopic to set
	 */
	public void setMedicalSSREmailTopic(String medicalSSREmailTopic) {
		this.medicalSSREmailTopic = medicalSSREmailTopic;
	}

	/**
	 * @return the medicalSSREmailMsg
	 */
	public String getMedicalSSREmailMsg() {
		return medicalSSREmailMsg;
	}

	/**
	 * @param medicalSSREmailMsg
	 *            the medicalSSREmailMsg to set
	 */
	public void setMedicalSSREmailMsg(String medicalSSREmailMsg) {
		this.medicalSSREmailMsg = medicalSSREmailMsg;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
