package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;

/**
 * @author mano
 */
public interface BaggageDAO {

	public Collection<PassengerBaggage> getFlightBaggagesForPnrPax(Collection<Integer> pnrPaxIds);

	public void saveOrUpdate(Collection<PassengerBaggage> baggages);

	public Collection<PaxBaggageTO> getReservationBaggages(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			boolean isOndBaggage, String preferredLanguage);

	public Map<Integer, String> getBaggageDetailNames(Collection<Integer> flightBaggageIds);

	public Collection<PassengerBaggage> getFlightBaggagesForPnrPaxFare(Collection<Integer> ppfIds);

	public Map<Integer, String> getReservationBaggagesForPNL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			String ssrCode);

	public Map<Integer, AncillaryDTO>
			getReservationBaggagesForPNLADL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds);

	public Map<Integer, String> getReservationBaggageWeightsForPNL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds);

	public String getBaggageSsrCodesToSendPNL(String airportCode);

	public Collection<PassengerBaggage> getPassengerBaggages(Collection<Integer> paxBaggageIds);

	public Integer getTemplateForSegmentId(int flightSegId);

	public Integer getONDChargeTemplateId(int pnrSegId);

	public Integer getONDBaggageId(long baggageChargeGroupId);
	
	public Integer getOndBaggageChargeTemplateId(Integer ondBaggageChargeId);

	/**
	 * Return expired baggage(s) based on auto cancellation flag
	 * 
	 * @param cancellationIds
	 * @param marketingCarrier
	 * @return
	 */
	public Map<ReservationLiteDTO, List<PaxBaggageTO>> getExpiredBaggages(Collection<Integer> cancellationIds,
			String marketingCarrier);

	/**
	 * Return expired lcc reservations due to baggage expire based on auto cancellation flag
	 * 
	 * @param marketingCarrier
	 * @param cancellationIds
	 * 
	 * @return
	 */
	public Map<String, Integer> getExpiredLccBaggagesInfo(String marketingCarrier, Collection<Integer> cancellationIds);
}
