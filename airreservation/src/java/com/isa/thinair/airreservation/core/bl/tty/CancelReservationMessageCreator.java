package com.isa.thinair.airreservation.core.bl.tty;


/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class CancelReservationMessageCreator extends TypeBReservationMessageCreator {
	
	public CancelReservationMessageCreator() {
		segmentsComposingStrategy = new SegmentsComposerForCancelAllSegments();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}
}
