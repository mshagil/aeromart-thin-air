/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.model.PnlPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PnlPassengerStates;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservarionPaxFareSegDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * For Basic Changes like Split Pax, Remove Pax, Name Changes, SSR Changes AFTER *PNL Was sent
 * 
 * @author Nilindra Fernando / Byorn John
 * @since 2.0
 */
public class ADLNotifyer {

	private static Log log = LogFactory.getLog(ADLNotifyer.class);

	public static void updateConnectedSegmentsWhenAddSegment(Reservation reservationOrg, Collection<Integer> addFlightSegIds)
			throws ModuleException {
		Set<ReservationPax> reservationPaxSet = reservationOrg.getPassengers();

		for (ReservationPax reservatinPax : reservationPaxSet) {
			performPNLADLStat(reservatinPax, PNLConstants.PaxPnlAdlStates.ADL_CHANGED_STAT, false, null, addFlightSegIds);
		}
	}

	public static void updateConnectedSegmentsWhenCNXSegment(Reservation reservationOrg, Collection<Integer> affectingPnrSegIds)
			throws ModuleException {
		Set<ReservationPax> reservationPaxSet = reservationOrg.getPassengers();

		for (ReservationPax reservatinPax : reservationPaxSet) {
			performPNLADLStat(reservatinPax, PNLConstants.PaxPnlAdlStates.ADL_CHANGED_STAT, false, affectingPnrSegIds, null);
		}
	}

	/**
	 * Tracks modifing reservations
	 * 
	 * @param reservationOrg
	 * @throws ModuleException
	 */
	public static void updateStatusToCHG(Reservation reservationOrg) throws ModuleException {
		if(!reservationOrg.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)){
			Set<ReservationPax> reservationPaxSet = reservationOrg.getPassengers();
			
			for (ReservationPax reservatinPax : reservationPaxSet) {
				performPNLADLStat(reservatinPax, PNLConstants.PaxPnlAdlStates.ADL_CHANGED_STAT, false, null, null);
			}			
		}
	}

	/**
	 * Tracks the original reservation
	 * 
	 * @param reservationOrg
	 * @throws ModuleException
	 */
	public static void recordOriginalReservation(Reservation reservationOrg) throws ModuleException {
		Set<ReservationPax> reservationPaxSet = reservationOrg.getPassengers();

		for (ReservationPax reservatinPax : reservationPaxSet) {
			performPNLADLStat(reservatinPax, PNLConstants.PaxPnlAdlStates.ADL_CHANGED_STAT, true, null, null);
		}
	}

	/**
	 * Tracks the original reservation
	 * 
	 * @param etGenerationEligiblePax
	 *            TODO
	 * @param originalReservationStatus
	 *            TODO
	 * @param reservationOrg
	 * 
	 * @throws ModuleException
	 */
	public static void recordReservation(Reservation reservation, AdlActions adlAction,
			Collection<ReservationPax> etGenerationEligiblePax, String originalReservationStatus) throws ModuleException {
		if (!reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
			recordADLActions(reservation, adlAction, null, etGenerationEligiblePax, originalReservationStatus);
		}

	}

	public static void updateADLActionsForRemovePaxScenario(Reservation reservation, String pnr) throws ModuleException {

		recordADLActions(reservation, AdlActions.D, pnr, null, null);
	}

	@Deprecated
	private static void updateADLActions(ReservationPax reservationPaxSrv, String adlAction, String pnr) throws ModuleException {

		if (pnr == null) {
			pnr = reservationPaxSrv.getReservation().getPnr();
		}
		Iterator<ReservationPaxFare> itReservationPaxFare = reservationPaxSrv.getPnrPaxFares().iterator();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		String pnlAdlStat;
		PnlPassenger pnlPassenger;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();
			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();

				pnlAdlStat = BeanUtils.nullHandler(reservationPaxFareSegment.getPnlStat());
				// Only adding if PNL is sent
				// if (!pnlAdlStat.equals(PNLConstants.PaxPnlAdlStates.PNL_RES_STAT)) {
				pnlPassenger = new PnlPassenger();
				pnlPassenger.setTitle(reservationPaxFare.getReservationPax().getTitle());
				pnlPassenger.setFirstName(reservationPaxFare.getReservationPax().getFirstName());
				pnlPassenger.setLastName(reservationPaxFare.getReservationPax().getLastName());
				pnlPassenger.setPnr(pnr);
				pnlPassenger.setPnlStatus(adlAction);
				pnlPassenger.setPnrPaxId(reservationPaxFare.getReservationPax().getPnrPaxId());
				pnlPassenger.setPaxType(reservationPaxFare.getReservationPax().getPaxType());
				pnlPassenger.setPnrSegId(reservationPaxFareSegment.getPnrSegId());
				pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);

				pnlPassenger.setGroupId(reservationPaxFareSegment.getGroupId());
				colPnlPassengers.add(pnlPassenger);
				// }
			}
		}

		// Save the pnl passengers
		if (colPnlPassengers.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);

		}
	}

	public static void recordADLActions(Reservation reservation, AdlActions adlAction, String pnr,
			Collection<ReservationPax> etGenerationEligiblePax, String originalReservationStatus) throws ModuleException {
		Set<ReservationPax> paxs = reservation.getPassengers();
		boolean isOldPnr = false;
		if (pnr == null) {
			pnr = reservation.getPnr();
			isOldPnr = true;
		}

		// handle anci modification and payment in OHD reservation
		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus()) && adlAction == AdlActions.C) {
			adlAction = AdlActions.A;
		}

		Collection<Integer> paxHavingBalanceToPay = new ArrayList<Integer>();
		if (etGenerationEligiblePax == null || etGenerationEligiblePax.isEmpty()) {
			paxHavingBalanceToPay = getPaxHavingBalanceToPay(reservation);
		}
		
		addDeleteParentForPnlAdlWhenTicketGeneratedOnlyForInfant(etGenerationEligiblePax, reservation, paxHavingBalanceToPay);
		
		HashMap<Integer, String> flightSegIdAction = new HashMap<Integer, String>();
		Collection<Integer> etGeneratedPassengerIds = getPassengerIds(etGenerationEligiblePax);

		PnlPassenger pnlPassenger;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();
		String validAdlAction;
		if (!(paxHavingBalanceToPay.isEmpty() && etGeneratedPassengerIds.isEmpty())) {
			OUTER: for (ReservationSegment resSeg : reservation.getSegments()) {
				if (!isValidSegmentToTriggerADL(resSeg, (AdlActions.A == adlAction))) {
					continue OUTER;
				}

				validAdlAction = adlAction.toString();
				// If there are multiple same flight CNX seg for DEL action
				if (colPnlPassengers.size() > 0) {
					if (flightSegIdAction.get(resSeg.getFlightSegId()) != null && AdlActions.D == adlAction) {
						if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
							for (PnlPassenger deletedPnlPassenger : colPnlPassengers) {
								if (deletedPnlPassenger.getPnlStatus().equals(AdlActions.D.toString())) {
									deletedPnlPassenger.setPnrSegId(resSeg.getPnrSegId());
								}
							}
						}
						continue OUTER;
					} else {
						flightSegIdAction.put(resSeg.getFlightSegId(), adlAction.toString());
					}
				} else {
					flightSegIdAction.put(resSeg.getFlightSegId(), adlAction.toString());
				}

				// Check whether this reservation contains added record to send with
				// ADL
				List<PnlPassenger> pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlPassegers(
						resSeg.getPnrSegId(), PNLConstants.PnlPassengerStates.NOT_UPDATED, true);

				if (!pnlPassengers.isEmpty()) {
					boolean isOnlyContainsAddedRecrods = true;
					boolean isAdlActionToBeADD = false;
					boolean isAdlActionToBeCHG = false;
					boolean hasDelRecords = false;
					if (pnlPassengers.get(0) != null) {
						for (PnlPassenger pnlPax : pnlPassengers) {
							if (pnlPax.getPnlStatus().equals(AdlActions.D.toString())) {
								hasDelRecords = true;
							}
							if (!pnlPax.getPnlStatus().equals(AdlActions.A.toString())) {
								isOnlyContainsAddedRecrods = false;
							}
						}
						for (PnlPassenger pax : pnlPassengers) {
							if ((paxHavingBalanceToPay.isEmpty() && etGeneratedPassengerIds.contains(pax.getPnrPaxId()))
									|| !paxHavingBalanceToPay.contains(pax.getPnrPaxId())) {
								if (etGeneratedPassengerIds.contains(pax.getPnrPaxId())) {
									if (pax.getPnlStatus().equals(AdlActions.D.toString()) && AdlActions.D == adlAction) {
										pax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
									} else if (pax.getPnlStatus().equals(adlAction.toString())) {
										pax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
									} else if (pax.getPnlStatus().equals(AdlActions.C.toString()) && AdlActions.A == adlAction) {
										pax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
										if (!hasDelRecords) {
											isAdlActionToBeCHG = true;
										} else {
											isAdlActionToBeADD = true;
										}
									} else if (pax.getPnlStatus().equals(AdlActions.A.toString()) && AdlActions.C == adlAction) {
										pax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
										isAdlActionToBeADD = true;
									}
								}

							}
						}
						ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(pnlPassengers);
					}

					if (isAdlActionToBeADD) {
						// No need to add CHG pax, since ADD not sent yet.So update
						// record action as ADD
						validAdlAction = AdlActions.A.toString();
					} else if (isAdlActionToBeCHG) {
						// No need to add pax, since CHG not sent yet.So update
						// record action as CHG
						validAdlAction = AdlActions.C.toString();
					}

				} else if (AdlActions.D == adlAction && !isOldPnr) {
					// No need to add DEL record for this old res
					continue OUTER;
				}
				INNER: for (ReservationPax pax : paxs) {

					if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)
							|| ((paxHavingBalanceToPay.isEmpty() && etGeneratedPassengerIds.isEmpty()))
							|| ((!paxHavingBalanceToPay.isEmpty() && paxHavingBalanceToPay.contains(pax.getPnrPaxId())) || (!etGeneratedPassengerIds
									.isEmpty() && !etGeneratedPassengerIds.contains(pax.getPnrPaxId())))) {
						continue INNER;
					}

					if(validAdlAction.equals(AdlActions.D.toString())){
						if(!isPnlMessagesDeliveredTo(pax.getPnrPaxId(),resSeg.getPnrSegId())){
							if(pnlPassengers != null && !pnlPassengers.isEmpty()){
								removeNotDeliveredAddActions(pnlPassengers, pax.getPnrPaxId(), resSeg.getPnrSegId());						
							}
							continue INNER;
						}
					}

					if (validAdlAction.equals(AdlActions.C.toString())) {
						validAdlAction = getValidAdlActionForAnciModification(pax, resSeg.getPnrSegId());
					}

					if (!("TBA").equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getFirstName())))
							&& !("TBA").equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getLastName())))) {
						pnlPassenger = new PnlPassenger();
						pnlPassenger.setTitle(pax.getTitle());
						pnlPassenger.setFirstName(pax.getFirstName());
						pnlPassenger.setLastName(pax.getLastName());
						pnlPassenger.setPnr(pnr);
						pnlPassenger.setPnlStatus(validAdlAction);
						pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
						pnlPassenger.setPaxType(pax.getPaxType());
						pnlPassenger.setPnrSegId(resSeg.getPnrSegId());
						pnlPassenger.setFlightSegId(resSeg.getFlightSegId());
						pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
						pnlPassenger.setBcType(resSeg.getBookingType());
						pnlPassenger.setBookingCode(resSeg.getBookingCode());
						pnlPassenger.setCabinClassCode(resSeg.getCabinClassCode());
						pnlPassenger.setLogicalCCCode(resSeg.getLogicalCCCode());
						colPnlPassengers.add(pnlPassenger);
					}
				}// recordExchangedSegForAdl(reservation, resSeg, originalReservationStatus, etGeneratedPassengerIds);
			}

			if (colPnlPassengers.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
			}
		}
	}

	private static boolean isPnlMessagesDeliveredTo(Integer pnrPaxId,Integer pnrSegId){
		return ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_SEG_DAO.isPaxAvailableFor(pnrPaxId, pnrSegId);
	}
	
	private static void removeNotDeliveredAddActions(List<PnlPassenger> pnlPassengers,Integer pnrPaxId,Integer pnrSegmentId){
		PnlPassenger passenger = getMatchingPnlPaxFor(pnrPaxId, pnrSegmentId, pnlPassengers);
		if(passenger != null){
			passenger.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(pnlPassengers);
		}
		
	}
	
	private static PnlPassenger getMatchingPnlPaxFor(Integer pnrPaxId,Integer pnrSegmentId,List<PnlPassenger> pnlPassengers){
		PnlPassenger matchingPnlPassenger = null;
		for(PnlPassenger passenger:pnlPassengers){
			if(passenger.getPnrPaxId().equals(pnrPaxId) && passenger.getPnrSegId().equals(pnrSegmentId)){
				matchingPnlPassenger = passenger;
				break;
			}
		}
		return matchingPnlPassenger;
	}
	
	private static String getValidAdlActionForAnciModification(ReservationPax pax, Integer pnrSegId) {
		for (ReservationPaxFare reservationPaxFare : pax.getPnrPaxFares()) {
			for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
				if (reservationPaxFareSegment.getPnrSegId().equals(pnrSegId)) {
					if (reservationPaxFareSegment.getPnlStat().equals(PNLConstants.PaxPnlAdlStates.PNL_RES_STAT)) {
						return AdlActions.A.toString();
					}
					break;
				}
			}
		}
		return AdlActions.C.toString();
	}


	private static PnlPassenger createPnlPassenger(String title,String firstName,String lastName,String pnr,
			String adlAction,Integer pnrPaxId,String paxType,Integer pnrSegmentId,Integer flightSegmentId,
			String status,String bookingType,String bookingCode,String cabinClassCode,String logicalCabinClassCode){
		PnlPassenger pnlPassenger = null;
		pnlPassenger = new PnlPassenger();
		pnlPassenger.setTitle(title);
		pnlPassenger.setFirstName(firstName);
		pnlPassenger.setLastName(lastName);
		pnlPassenger.setPnr(pnr);
		pnlPassenger.setPnlStatus(adlAction);
		pnlPassenger.setPnrPaxId(pnrPaxId);
		pnlPassenger.setPaxType(paxType);
		pnlPassenger.setPnrSegId(pnrSegmentId);
		pnlPassenger.setFlightSegId(flightSegmentId);
		pnlPassenger.setStatus(status);
		pnlPassenger.setBcType(bookingType);
		pnlPassenger.setBookingCode(bookingCode);
		pnlPassenger.setCabinClassCode(cabinClassCode);
		pnlPassenger.setLogicalCCCode(logicalCabinClassCode);
		return pnlPassenger;
		
	}
	
	private static boolean isValidSegmentToTriggerADL(ReservationSegment resSeg, boolean isAdd) throws ModuleException {
		if ((resSeg.getSubStatus() != null && resSeg.getSubStatus().equals(
				ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED))
				|| (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL) && isAdd)) {
			return false;
		} else {
			List<Integer> flightSegmentIds = new ArrayList<Integer>();
			flightSegmentIds.add(resSeg.getFlightSegId());
			Collection<FlightSegmentDTO> flightSegmentDTOs = ReservationModuleUtils.getFlightBD().getFlightSegments(
					flightSegmentIds);
			FlightSegmentDTO fsDTO = BeanUtils.getFirstElement(flightSegmentDTOs);
			if (!ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.hasAnyPnlHistory(fsDTO.getFlightNumber(),
					fsDTO.getDepartureDateTimeZulu(), fsDTO.getDepartureAirportCode())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param reservation
	 * @param affectingPnrSegIds
	 * @param etGenerationEligiblePax
	 *            TODO
	 */
	public static void recordCanceledSegmentsForAdl(Reservation reservation, Collection<Integer> affectingPnrSegIds,
			Collection<ReservationPax> etGenerationEligiblePax) throws ModuleException {
		Set<ReservationPax> paxs = reservation.getPassengers();
		String pnr = reservation.getPnr();

		PnlPassenger pnlPassenger;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();
		Collection<Integer> etGeneratedPassengerIds = new ArrayList<Integer>();
		if (etGenerationEligiblePax != null && !etGenerationEligiblePax.isEmpty()) {
			etGeneratedPassengerIds = getPassengerIds(etGenerationEligiblePax);
		} else {
			etGeneratedPassengerIds = getPassengerIds(paxs);
		}
		if (affectingPnrSegIds != null && affectingPnrSegIds.size() > 0) {
			OUTER: for (ReservationSegment resSeg : reservation.getSegments()) {
				if (affectingPnrSegIds.contains(resSeg.getPnrSegId())) {
					if (!isValidSegmentToTriggerADL(resSeg, false)) {
						continue OUTER;
					}
					// Check whether this reservation contains added record to send with ADL
					List<PnlPassenger> pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
							.getPnlPassegers(resSeg.getPnrSegId(), PNLConstants.PnlPassengerStates.NOT_UPDATED, false);
					if (!pnlPassengers.isEmpty()) {
						for (PnlPassenger pnlPax : pnlPassengers) {
							if (!pnlPax.getPnlStatus().equals(AdlActions.D.toString())) {
								if (!etGeneratedPassengerIds.isEmpty()) {
									if (etGeneratedPassengerIds.contains(pnlPax.getPnrPaxId())) {
										pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
									}
								} else {
									pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
								}
							}
						}
						ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(pnlPassengers);
						// Do need to add Del record for this segment as ADL not
						// sent
						continue OUTER;
					}
					INNER: for (ReservationPax pax : paxs) {
						if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)
								|| (!etGeneratedPassengerIds.isEmpty() && !etGeneratedPassengerIds.contains(pax.getPnrPaxId()))) {
							continue INNER;
						}
						if (!("TBA").equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getFirstName())))
								&& !("TBA").equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getLastName())))) {
							pnlPassenger = new PnlPassenger();
							pnlPassenger.setTitle(pax.getTitle());
							pnlPassenger.setFirstName(pax.getFirstName());
							pnlPassenger.setLastName(pax.getLastName());
							pnlPassenger.setPnr(pnr);
							pnlPassenger.setPnlStatus(AdlActions.D.toString());
							pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
							pnlPassenger.setPaxType(pax.getPaxType());
							pnlPassenger.setPnrSegId(resSeg.getPnrSegId());
							pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
							pnlPassenger.setBcType(resSeg.getBookingType());
							pnlPassenger.setBookingCode(resSeg.getBookingCode());
							pnlPassenger.setCabinClassCode(resSeg.getCabinClassCode());
							pnlPassenger.setLogicalCCCode(resSeg.getLogicalCCCode());
							// pnlPassenger.setGroupId();
							colPnlPassengers.add(pnlPassenger);
						}


					}
				}
			}
		}
		if (colPnlPassengers.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
		}
	}

	public static void recordAddedSegmentsForAdl(Reservation reservation, Collection<Integer> affectingFlightSegIds,
			String originalReservationStatus, Collection<ReservationPax> etGenerationEligiblePax,
			Collection<ReservationPax> passengersAlreadyHavingEticket) throws ModuleException {
		Set<ReservationPax> paxs = reservation.getPassengers();
		String pnr = reservation.getPnr();

		PnlPassenger pnlPassenger;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();
		Collection<Integer> etGeneratedPassengerIds = getPassengerIds(etGenerationEligiblePax);

		if (affectingFlightSegIds != null && affectingFlightSegIds.size() > 0) {
			OUTER: for (ReservationSegment resSeg : reservation.getSegments()) {
				if (!isValidSegmentToTriggerADL(resSeg, true)) {
					continue OUTER;
				}
				if (affectingFlightSegIds.contains(resSeg.getFlightSegId())) {
					// Check whether this reservation contains added record to send with ADL
					List<PnlPassenger> pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
							.getPnlPassegers(resSeg.getPnrSegId(), PNLConstants.PnlPassengerStates.NOT_UPDATED, false);
					if (!pnlPassengers.isEmpty()) {
						if (pnlPassengers.get(0) != null && pnlPassengers.get(0).getPnlStatus().equals(AdlActions.A.toString())) {
							for (PnlPassenger pnlPax : pnlPassengers) {
								if (etGeneratedPassengerIds.contains(pnlPax.getPnrPaxId())) {
									pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
								}
							}
							ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(pnlPassengers);

						}
					}
					if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
							&& resSeg.getSubStatus() == null) {
						INNER: for (ReservationPax pax : paxs) {
							if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)
									|| (!ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(originalReservationStatus) && (!etGeneratedPassengerIds.isEmpty() && !etGeneratedPassengerIds
											.contains(pax.getPnrPaxId())))) {
								continue INNER;
							}

							updateExistingRecordsToSent(pnr, AdlActions.A, resSeg.getPnrSegId());
							if (!("TBA").equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getFirstName())))
									&& !("TBA").equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getLastName())))) {
								pnlPassenger = new PnlPassenger();
								pnlPassenger.setTitle(pax.getTitle());
								pnlPassenger.setFirstName(pax.getFirstName());
								pnlPassenger.setLastName(pax.getLastName());
								pnlPassenger.setPnr(pnr);
								pnlPassenger.setPnlStatus(AdlActions.A.toString());
								pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
								pnlPassenger.setPaxType(pax.getPaxType());
								pnlPassenger.setPnrSegId(resSeg.getPnrSegId());
								pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
								pnlPassenger.setBcType(resSeg.getBookingType());
								pnlPassenger.setBookingCode(resSeg.getBookingCode());
								pnlPassenger.setCabinClassCode(resSeg.getCabinClassCode());
								pnlPassenger.setLogicalCCCode(resSeg.getLogicalCCCode());
								colPnlPassengers.add(pnlPassenger);
							}
							

						}
					}

					// Check if there matching exchange seg to send DEL record.
					recordExchangedSegForAdl(reservation, resSeg, originalReservationStatus, etGeneratedPassengerIds,
							passengersAlreadyHavingEticket);
				}
			}
		}
		if (colPnlPassengers.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
		}

	}

	private static void recordExchangedSegForAdl(Reservation reservation, ReservationSegment newSegment,
			String originalReservationStatus, Collection<Integer> etGeneratedPassengerIds,
			Collection<ReservationPax> passengersAlreadyHavingEticket) {
		boolean isOnlyContainsAddedRecrods = true;
		Collection<Integer> exchangedSegIds = new ArrayList<Integer>();
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();
		Collection<Integer> passengerIdsAlreadyHavingEticket = getPassengerIds(passengersAlreadyHavingEticket);
		PnlPassenger pnlPassenger;
		ReservationSegment exchangedSegment = null;
		for (ReservationSegment resSeg : reservation.getSegments()) {
			if (resSeg.getSubStatus() != null && resSeg.getFlightSegId().equals(newSegment.getFlightSegId())) {
				if (exchangedSegment == null) {
					exchangedSegment = resSeg;
				} else if (exchangedSegment.getSegmentSeq() < resSeg.getSegmentSeq()) {
					exchangedSegment = resSeg;
				}
			}
		}
		if (exchangedSegment != null) {
			exchangedSegIds.add(exchangedSegment.getPnrSegId());
		}

		if (exchangedSegIds.size() > 0) {
			List<PnlPassenger> pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
					.getPnlPassegers(exchangedSegIds);
			if (!pnlPassengers.isEmpty()) {
				for (PnlPassenger pnlPax : pnlPassengers) {
					if (!pnlPax.getPnlStatus().equals(AdlActions.A.toString())) {
						isOnlyContainsAddedRecrods = false;
					}
					if (pnlPax.getStatus().equals(PnlPassengerStates.UPDATED)
							&& pnlPax.getPnlStatus().equals(AdlActions.D.toString())) {
						exchangedSegIds.remove(pnlPax.getPnrSegId());
					} else if (pnlPax.getStatus().equals(PnlPassengerStates.NOT_UPDATED)
							&& (pnlPax.getPnlStatus().equals(AdlActions.D.toString()) || pnlPax.getPnlStatus().equals(
									AdlActions.C.toString()))) {
						pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
						colPnlPassengers.add(pnlPax);
						exchangedSegIds.remove(pnlPax.getPnrSegId());
					} else if (pnlPax.getStatus().equals(PnlPassengerStates.NOT_UPDATED)
							&& pnlPax.getPnlStatus().equals(AdlActions.A.toString())) {
						pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
						colPnlPassengers.add(pnlPax);
						exchangedSegIds.remove(pnlPax.getPnrSegId());
					}
				}
				ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(pnlPassengers);
			} else {
				isOnlyContainsAddedRecrods = false;
			}
		}

		// If there are new exchanged segments
		if (exchangedSegIds.size() > 0 && !isOnlyContainsAddedRecrods) {
			for (Integer exchangedPnrSegId : exchangedSegIds) {
				for (ReservationPax pax : reservation.getPassengers()) {
					if (!pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)
							&& !ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(originalReservationStatus)
							&& passengerIdsAlreadyHavingEticket.contains(pax.getPnrPaxId())) {
						pnlPassenger = new PnlPassenger();
						pnlPassenger.setTitle(pax.getTitle());
						pnlPassenger.setFirstName(pax.getFirstName());
						pnlPassenger.setLastName(pax.getLastName());
						pnlPassenger.setPnr(reservation.getPnr());
						pnlPassenger.setPnlStatus(AdlActions.D.toString());
						pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
						pnlPassenger.setPaxType(pax.getPaxType());
						pnlPassenger.setPnrSegId(exchangedPnrSegId);

						for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment paxFareSeg : paxFare.getPaxFareSegments()) {
								if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(paxFareSeg.getSegment()
										.getStatus())) {
									if (exchangedSegIds.contains(paxFareSeg.getPnrSegId())) {
										try {
											if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
												if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
													pnlPassenger.setLogicalCCCode(paxFareSeg.getSegment().getLogicalCCCode());
												} else {
													pnlPassenger.setBookingCode(paxFareSeg.getBookingCode());
												}
											} else {
												pnlPassenger.setCabinClassCode(paxFareSeg.getSegment().getCabinClassCode());
											}

										} catch (ModuleException ex) {

										}
									}
								}
							}
						}
						pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
						colPnlPassengers.add(pnlPassenger);
					}

				}
			}
		}

		if (colPnlPassengers.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
		}

	}

	// TODO : check pax Having balance to pay
	public static void recordAddedSegmentsForAdl(ReservationSegment resSeg) throws ModuleException {
		if (isValidSegmentToTriggerADL(resSeg, true)) {
			Set<ReservationPax> paxs = resSeg.getReservation().getPassengers();
			String pnr = resSeg.getReservation().getPnr();
			PnlPassenger pnlPassenger;
			Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();
			updateExistingRecordsToSent(pnr, AdlActions.A, resSeg.getPnrSegId());
			for (ReservationPax pax : paxs) {
				pnlPassenger = new PnlPassenger();
				pnlPassenger.setTitle(pax.getTitle());
				pnlPassenger.setFirstName(pax.getFirstName());
				pnlPassenger.setLastName(pax.getLastName());
				pnlPassenger.setPnr(pnr);
				pnlPassenger.setPnlStatus(AdlActions.A.toString());
				pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
				pnlPassenger.setPaxType(pax.getPaxType());
				pnlPassenger.setPnrSegId(resSeg.getPnrSegId());
				pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
				pnlPassenger.setBcType(resSeg.getBookingType());
				pnlPassenger.setBookingCode(resSeg.getBookingCode());
				pnlPassenger.setCabinClassCode(resSeg.getCabinClassCode());
				pnlPassenger.setLogicalCCCode(resSeg.getLogicalCCCode());
				// pnlPassenger.setGroupId();
				colPnlPassengers.add(pnlPassenger);

			}

			if (colPnlPassengers.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
			}
		}

	}

	private static void updateExistingRecordsToSent(String pnr, AdlActions adlAction, Integer pnrSegId) {
		Set<String> pnrs = new HashSet<String>();
		pnrs.add(pnr);
		List<String> adlActions = new ArrayList<String>();
		adlActions.add(adlAction.toString());
		List<PnlPassenger> passengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlPassegers(pnrs,
				adlActions, PNLConstants.PnlPassengerStates.NOT_UPDATED);
		if (!passengers.isEmpty()) {
			for (PnlPassenger passenger : passengers) {
				if (passenger.getPnrSegId() != null && passenger.getPnrSegId().intValue() == pnrSegId.intValue()) {
					passenger.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
				}
			}
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(passengers);
		}
	}

	/**
	 * Track any new reservations that getting created
	 * 
	 * @param reservationNew
	 * @throws ModuleException
	 */
	public static void ensureADLAddition(Reservation reservationNew) throws ModuleException {
		Set<ReservationPax> reservationPaxSet = reservationNew.getPassengers();

		for (ReservationPax reservatinPax : reservationPaxSet) {
			performPNLADLStat(reservatinPax, PNLConstants.PaxPnlAdlStates.PNL_RES_STAT, false, null, null);
		}
	}

	public static void ensureRemovePax(Reservation reservationNew, String oldPnr) throws ModuleException {
		Set<ReservationPax> reservationPaxSet = reservationNew.getPassengers();

		for (ReservationPax reservatinPax : reservationPaxSet) {
			savePNLPassengerForRemovePax(reservatinPax, oldPnr, true);
		}
	}

	private static void savePNLPassengerForRemovePax(ReservationPax reservationPaxSrv, String oldPnr, boolean savePnlPassenger)
			throws ModuleException {
		log.debug("Start of savePNLPassengerForRemovePax");

		Iterator<ReservationPaxFare> itReservationPaxFare = reservationPaxSrv.getPnrPaxFares().iterator();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		PnlPassenger pnlPassenger;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();
			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();

				String pnlAdlStat = BeanUtils.nullHandler(reservationPaxFareSegment.getPnlStat());

				if (!PNLConstants.PaxPnlAdlStates.ADL_CANCEL_STAT.equals(pnlAdlStat)) {
					boolean isPNLSent = false;

					if (reservationPaxFareSegment.getPnlStat().equalsIgnoreCase(PNLConstants.PaxPnlAdlStates.PNL_PNL_STAT)) {
						isPNLSent = true;
						reservationPaxFareSegment.setPnlStat(PNLConstants.PaxPnlAdlStates.ADL_CHANGED_STAT);
					}
				} else {
					savePnlPassenger = false;
				}
			}

		}
		if (savePnlPassenger) {
			savePnlPassengerRemovePax(colPnlPassengers);
		}

		log.debug("End of savePNLPassengerForRemovePax");
	}

	private static boolean isWithinTransitTime(Collection<Integer> addFlightSegIds, Integer affectingFltSegId) {
		Date affectingDepartureDate = null;
		Date affectingArrivalDate = null;

		Collection<Integer> flightSegmentIds = new ArrayList<Integer>();
		flightSegmentIds.addAll(addFlightSegIds);
		flightSegmentIds.add(affectingFltSegId);

		Map<Integer, Date[]> flightSegZuluTimeMap = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
				.getFlightSegmentDepTimeZuluMapForAddSegment(flightSegmentIds);

		if (flightSegZuluTimeMap != null) {

			Date dAffecting[] = flightSegZuluTimeMap.get(affectingFltSegId);
			affectingDepartureDate = dAffecting[0];
			affectingArrivalDate = dAffecting[1];

			Set<Integer> keys = flightSegZuluTimeMap.keySet();

			Iterator<Integer> iter = keys.iterator();
			while (iter.hasNext()) {

				Integer flt_seg_id = iter.next();

				if (flt_seg_id != affectingFltSegId) {

					Date d[] = flightSegZuluTimeMap.get(flt_seg_id);

					Date departreDateTime = d[0];
					Date arrivalDateTime = d[1];

					int timeDiffOutBound = CalendarUtil.getTimeDifferenceInHours(affectingArrivalDate, departreDateTime);

					int timeDiffInbound = CalendarUtil.getTimeDifferenceInHours(arrivalDateTime, affectingDepartureDate);

					String hours = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.MAX_TRANSIT_TIME);

					String hrs = hours.substring(0, 2);

					int intSysMaxTransitTime = Integer.parseInt(hrs);

					if ((timeDiffInbound > 0 && timeDiffInbound < intSysMaxTransitTime)
							|| (timeDiffOutBound > 0 && timeDiffOutBound < intSysMaxTransitTime)) {
						return true;
					}

				}

			}

		}
		return false;
	}

	private static boolean
			isWithinTransitTime(Collection<Integer> pnrSegIds, Integer affectingPnrSegId, Integer affectingFltSegId) {

		Date affectingDepartureDate = null;
		Date affectingArrivalDate = null;

		pnrSegIds.add(affectingPnrSegId);

		Map<Integer, Date[]> flightSegZuluTimeMap = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
				.getFlightSegmentDepTimeZuluMap(pnrSegIds);

		if (flightSegZuluTimeMap != null) {

			Date dAffecting[] = flightSegZuluTimeMap.get(affectingFltSegId);
			affectingDepartureDate = dAffecting[0];
			affectingArrivalDate = dAffecting[1];

			Set<Integer> keys = flightSegZuluTimeMap.keySet();

			Iterator<Integer> iter = keys.iterator();
			while (iter.hasNext()) {

				Integer flt_seg_id = iter.next();

				if (flt_seg_id != affectingFltSegId) {

					Date d[] = flightSegZuluTimeMap.get(flt_seg_id);

					Date departreDateTime = d[0];
					Date arrivalDateTime = d[1];

					int timeDiffOutBound = CalendarUtil.getTimeDifferenceInHours(affectingArrivalDate, departreDateTime);

					int timeDiffInbound = CalendarUtil.getTimeDifferenceInHours(arrivalDateTime, affectingDepartureDate);

					String hours = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.MAX_TRANSIT_TIME);

					String hrs = hours.substring(0, 2);

					int intSysMaxTransitTime = Integer.parseInt(hrs);

					if ((timeDiffInbound > 0 && timeDiffInbound < intSysMaxTransitTime)
							|| (timeDiffOutBound > 0 && timeDiffOutBound < intSysMaxTransitTime)) {
						return true;
					}
				}

			}

		}

		return false;
	}

	/**
	 * Update PNL/ADL Stat
	 * 
	 * @param reservationPaxSrv
	 * @param pnlAdlStatType
	 * @param recordInPNLPassenger
	 * @throws ModuleException
	 */
	private static void performPNLADLStat(ReservationPax reservationPaxSrv, String pnlAdlStatType, boolean recordInPNLPassenger,
			Collection<Integer> cancelPnrSegIds, Collection<Integer> addFltSegIds) throws ModuleException {
		log.debug("Start of performPNLADLStat");

		if (pnlAdlStatType == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		String pnr = reservationPaxSrv.getReservation().getPnr();
		Iterator<ReservationPaxFare> itReservationPaxFare = reservationPaxSrv.getPnrPaxFares().iterator();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		String pnlAdlStat;
		PnlPassenger pnlPassenger;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();
			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();

				pnlAdlStat = BeanUtils.nullHandler(reservationPaxFareSegment.getPnlStat());

				if (!pnlAdlStat.equals(PNLConstants.PaxPnlAdlStates.PNL_RES_STAT)) {
					// cancel segment - if there are cancelling pnrSegIds, that means status should not be set to 'C',
					// only the connected segments pnl status should be set to 'C'
					if (cancelPnrSegIds != null && cancelPnrSegIds.size() > 0) {
						if (!cancelPnrSegIds.contains(reservationPaxFareSegment.getPnrSegId())) {
							if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationPaxFareSegment
									.getSegment().getStatus())) {
								if (isWithinTransitTime(cancelPnrSegIds, reservationPaxFareSegment.getPnrSegId(),
										reservationPaxFareSegment.getSegment().getFlightSegId())) {
									reservationPaxFareSegment.setPnlStat(pnlAdlStatType);
									pnlPassenger = new PnlPassenger();
									pnlPassenger.setTitle(reservationPaxFare.getReservationPax().getTitle());
									pnlPassenger.setFirstName(reservationPaxFare.getReservationPax().getFirstName());
									pnlPassenger.setLastName(reservationPaxFare.getReservationPax().getLastName());
									pnlPassenger.setPnr(pnr);
									pnlPassenger.setPnlStatus(AdlActions.C.toString());
									pnlPassenger.setPnrPaxId(reservationPaxFare.getReservationPax().getPnrPaxId());
									pnlPassenger.setPaxType(reservationPaxFare.getReservationPax().getPaxType());
									pnlPassenger.setPnrSegId(reservationPaxFareSegment.getPnrSegId());
									pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);

									pnlPassenger.setGroupId(reservationPaxFareSegment.getGroupId());
									colPnlPassengers.add(pnlPassenger);
								}
							}
						}

					}// add segment
					else if (addFltSegIds != null && addFltSegIds.size() > 0) {
						if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationPaxFareSegment
								.getSegment().getStatus())) {
							if (isWithinTransitTime(addFltSegIds, reservationPaxFareSegment.getSegment().getFlightSegId())) {
								reservationPaxFareSegment.setPnlStat(pnlAdlStatType);
								pnlPassenger = new PnlPassenger();
								pnlPassenger.setTitle(reservationPaxFare.getReservationPax().getTitle());
								pnlPassenger.setFirstName(reservationPaxFare.getReservationPax().getFirstName());
								pnlPassenger.setLastName(reservationPaxFare.getReservationPax().getLastName());
								pnlPassenger.setPnr(pnr);
								pnlPassenger.setPnlStatus(AdlActions.C.toString());
								pnlPassenger.setPnrPaxId(reservationPaxFare.getReservationPax().getPnrPaxId());
								pnlPassenger.setPaxType(reservationPaxFare.getReservationPax().getPaxType());
								pnlPassenger.setPnrSegId(reservationPaxFareSegment.getPnrSegId());
								pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);

								pnlPassenger.setGroupId(reservationPaxFareSegment.getGroupId());
								colPnlPassengers.add(pnlPassenger);
							}
						}

					}
					// all
					else {

						if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationPaxFareSegment
								.getSegment().getStatus())) {
							reservationPaxFareSegment.setPnlStat(pnlAdlStatType);
						}

					}
				}
			}
		}

		// Save the pnl passengers
		if (colPnlPassengers.size() > 0) {
			if (log.isDebugEnabled()) {
				log.debug(colPnlPassengers.size() + " of Passenger(s) are TARGETED to be saved as PNL Passengers");
			}

			savePnlPassenger(colPnlPassengers);

			if (log.isDebugEnabled()) {
				log.debug(colPnlPassengers.size() + " Saved PNL Passenger Info Sucessfully");
			}
		}
	}

	private static void savePnlPassengerRemovePax(Collection<PnlPassenger> colPnlPassengers) {
		if (colPnlPassengers.size() > 0) {

			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
		}
	}

	/**
	 * Save PNL passengers with name changes
	 * 
	 * @param colPnlPassengers
	 */
	private static void savePnlPassenger(Collection<PnlPassenger> colPnlPassengers) {

		ifRecordsExistsRemoveThem(colPnlPassengers);
		if (colPnlPassengers.size() > 0) {

			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
		}
	}

	/**
	 * If original record already exists do no need to save it again.
	 * 
	 * @param pnlPassengers
	 */
	private static void ifRecordsExistsRemoveThem(Collection<PnlPassenger> pnlPassengers) {
		Collection<Integer> pnrSegIds = new HashSet<Integer>();
		Collection<Integer> pnrPaxIds = new HashSet<Integer>();

		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;

		for (PnlPassenger pnlPassenger : pnlPassengers) {
			pnrSegIds.add(pnlPassenger.getPnrSegId());
			pnrPaxIds.add(pnlPassenger.getPnrPaxId());
		}

		Collection<PnlPassenger> dbPnlPassengers = auxilliaryDAO.getPnlPassegers(pnrSegIds, pnrPaxIds,
				PNLConstants.PnlPassengerStates.NOT_UPDATED);
		Collection<PnlPassenger> toRemove = new ArrayList<PnlPassenger>();

		for (PnlPassenger dbPnlPassenger : dbPnlPassengers) {
			for (PnlPassenger pnlPassenger : pnlPassengers) {
				if (dbPnlPassenger.getPnrPaxId().equals(pnlPassenger.getPnrPaxId())
						&& dbPnlPassenger.getPnrSegId().equals(pnlPassenger.getPnrSegId())) {
					toRemove.add(pnlPassenger);
				}
			}
		}
		pnlPassengers.removeAll(toRemove);
	}

	public static void recordExchangedETicketForAdl(Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets,
			Reservation reservation, Collection<ReservationPax> etGenerationEligiblePaxForAdl) throws ModuleException {
		PnlPassenger pnlPassenger;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();
		Collection<ReservationPax> resPAx = reservation.getPassengers();
		Collection<Integer> pnrPaxIdList = null;
		List<PnlPassenger> pnlPassengers = null;
		Set<Integer> deletePaxList = null;
		Collection<Integer> etGeneratedIds = getPassengerIds(etGenerationEligiblePaxForAdl);

		for (ReservationPaxFareSegmentETicket eTicket : modifiedPaxfareSegmentEtickets) {
			OUTER: for (ReservationSegment resSeg : reservation.getSegments()) {
				if (!isValidSegmentToTriggerADL(resSeg, false)) {
					continue OUTER;
				}
				for (ReservationPax pax : resPAx) {
					if (eTicket.getReservationPaxETicket().getPnrPaxId().intValue() == pax.getPnrPaxId().intValue()) {
						for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment paxFareSeg : paxFare.getPaxFareSegments()) {
								if (paxFareSeg.getPnrPaxFareSegId().intValue() == eTicket.getPnrPaxFareSegId().intValue()
										&& paxFareSeg.getSegment().getPnrSegId() == resSeg.getPnrSegId()) {
									pnrPaxIdList = new ArrayList<Integer>();
									pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlPassegers(
											paxFareSeg.getPnrSegId(), PNLConstants.PnlPassengerStates.NOT_UPDATED, false);

									boolean isOnlyContainsAddedRecrods = true;
									if (!pnlPassengers.isEmpty()) {
										for (PnlPassenger pnlPax : pnlPassengers) {
											if (!pnlPax.getPnlStatus().equals(AdlActions.A.toString())) {
												isOnlyContainsAddedRecrods = false;
											}
										}
									}
									deletePaxList = getSegmentTransferredPaxIdList(pnlPassengers);

									if (!pnlPassengers.isEmpty()) {
										for (PnlPassenger pnlPax : pnlPassengers) {
											if ((pnlPax.getPnlStatus().equals(AdlActions.A.toString())
													&& pnlPax.getPnrPaxId().intValue() == pax.getPnrPaxId().intValue() && deletePaxList
														.contains(pnlPax.getPnrPaxId().intValue()))
													|| !etGeneratedIds.contains(pnlPax.getPnrPaxId())) {
												continue;
											}
											
											
											
											if (pnlPax.getPnlStatus().equals(AdlActions.D.toString())
													&& pnlPax.getPnrPaxId().intValue() == pax.getPnrPaxId().intValue()) {
												pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
											}
											if (pnlPax.getPnrPaxId().intValue() == eTicket.getReservationPaxETicket()
													.getPnrPaxId().intValue()) {
												if (!pnrPaxIdList.contains(pax.getPnrPaxId().intValue())) {
													pnlPassenger = new PnlPassenger();
													pnlPassenger.setTitle(pax.getTitle());
													pnlPassenger.setFirstName(pax.getFirstName());
													pnlPassenger.setLastName(pax.getLastName());
													pnlPassenger.setPnr(reservation.getPnr());
													pnlPassenger.setPnlStatus(AdlActions.D.toString());
													pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
													pnlPassenger.setPaxType(pax.getPaxType());
													pnlPassenger.setPnrSegId(paxFareSeg.getPnrSegId());
													pnlPassenger.setBookingCode(paxFareSeg.getBookingCode());
													pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
													pnrPaxIdList.add(pax.getPnrPaxId());
													colPnlPassengers.add(pnlPassenger);
												}
											}
										}
										ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
												.savePnlPassengers(pnlPassengers);
									}
								}
							}
						}
					}
				}
			}
		}

		if (colPnlPassengers.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
		}
	}

	private static List<Integer> getPnrPaxIds(Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets) {
		List<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (ReservationPaxFareSegmentETicket eTicket : modifiedPaxfareSegmentEtickets) {
			pnrPaxIds.add(eTicket.getReservationPaxETicket().getPnrPaxId());
		}
		return pnrPaxIds;
	}

	private static void updateExistingRecord(Integer pnrSegId,
			Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets) {
		List<PnlPassenger> pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlPassegers(pnrSegId,
				PNLConstants.PnlPassengerStates.NOT_UPDATED, false);
		if (!pnlPassengers.isEmpty()) {
			for (PnlPassenger pnlPax : pnlPassengers) {
				if (getPnrPaxIds(modifiedPaxfareSegmentEtickets).contains(pnlPax.getPnrPaxId())) {
					pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
				}
			}
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(pnlPassengers);
		}
	}
	
	public static void recordExchangedETicketForCodeShareBookings(
			Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets, Reservation reservation,
			Set<Integer> unrecordedPaxList) throws ModuleException {
		PnlPassenger pnlPassenger;
		List<PnlPassenger> pnlPassengers;
		Set<Integer> deletePaxList;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();
		Collection<ReservationPax> resPAx = reservation.getPassengers();

		for (ReservationPaxFareSegmentETicket eTicket : modifiedPaxfareSegmentEtickets) {
			for (ReservationPax pax : resPAx) {
				if (eTicket.getReservationPaxETicket().getPnrPaxId().intValue() == pax.getPnrPaxId().intValue()) {
					for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
						for (ReservationPaxFareSegment paxFareSeg : paxFare.getPaxFareSegments()) {
							if (isValidSegmentToTriggerADL(paxFareSeg.getSegment(), true)) {
								pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlPassegers(
										paxFareSeg.getPnrSegId(), PNLConstants.PnlPassengerStates.NOT_UPDATED, false);

								deletePaxList = getNonSegmentTransferredPaxIdList(pnlPassengers);
								deletePaxList.addAll(unrecordedPaxList);

								if (!pnlPassengers.isEmpty()) {
									for (PnlPassenger pnlPax : pnlPassengers) {
										if (pnlPax.getPnlStatus().equals(AdlActions.A.toString())
												&& pnlPax.getPnrPaxId().intValue() == pax.getPnrPaxId().intValue()
												&& deletePaxList.contains(pnlPax.getPnrPaxId().intValue())) {
											continue;
										}
										if (pnlPax.getPnlStatus().equals(AdlActions.D.toString())
												&& pnlPax.getPnrPaxId().intValue() == pax.getPnrPaxId().intValue()) {
											pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
										}

										if (paxFareSeg.getPnrPaxFareSegId().intValue() == eTicket.getPnrPaxFareSegId().intValue()) {
											pnlPassenger = new PnlPassenger();
											pnlPassenger.setTitle(pax.getTitle());
											pnlPassenger.setFirstName(pax.getFirstName());
											pnlPassenger.setLastName(pax.getLastName());
											pnlPassenger.setPnr(reservation.getPnr());
											pnlPassenger.setPnlStatus(AdlActions.D.toString());
											pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
											pnlPassenger.setPaxType(pax.getPaxType());
											pnlPassenger.setPnrSegId(paxFareSeg.getSegment().getPnrSegId());
											pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
											pnlPassenger.setBcType(paxFareSeg.getSegment().getBookingType());
											pnlPassenger.setBookingCode(paxFareSeg.getSegment().getBookingCode());
											pnlPassenger.setCabinClassCode(paxFareSeg.getSegment().getCabinClassCode());
											pnlPassenger.setLogicalCCCode(paxFareSeg.getSegment().getLogicalCCCode());
											colPnlPassengers.add(pnlPassenger);

											pnlPassenger = new PnlPassenger();
											pnlPassenger.setTitle(pax.getTitle());
											pnlPassenger.setFirstName(pax.getFirstName());
											pnlPassenger.setLastName(pax.getLastName());
											pnlPassenger.setPnr(reservation.getPnr());
											pnlPassenger.setPnlStatus(AdlActions.A.toString());
											pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
											pnlPassenger.setPaxType(pax.getPaxType());
											pnlPassenger.setPnrSegId(paxFareSeg.getSegment().getPnrSegId());
											pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
											pnlPassenger.setBcType(paxFareSeg.getSegment().getBookingType());
											pnlPassenger.setBookingCode(paxFareSeg.getSegment().getBookingCode());
											pnlPassenger.setCabinClassCode(paxFareSeg.getSegment().getCabinClassCode());
											pnlPassenger.setLogicalCCCode(paxFareSeg.getSegment().getLogicalCCCode());
											colPnlPassengers.add(pnlPassenger);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (colPnlPassengers.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
		}
	}

	private static Set<Integer> getNonSegmentTransferredPaxIdList(List<PnlPassenger> pnlPassengers) {
		Set<String> pnrList = new HashSet<String>();
		Set<Integer> deletePaxList = new HashSet<Integer>();
		;
		Collection<Integer> pnrSegId = new ArrayList<Integer>();
		List<String> adlActions = new ArrayList<String>();
		adlActions.add(AdlActions.D.toString());

		for (PnlPassenger pnlPax : pnlPassengers) {
			if (!pnrList.contains(pnlPax.getPnr())) {
				pnrList.add(pnlPax.getPnr());
			}
			if (!pnrSegId.contains(pnlPax.getPnrSegId())) {
				pnrSegId.add(pnlPax.getPnrSegId());
			}
		}

		List<PnlPassenger> deletePassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
				.getNonSegmentTransferredPnlPassegers(pnrList, adlActions, pnrSegId);
		for (PnlPassenger deletePax : deletePassengers) {
			deletePaxList.add(deletePax.getPnrPaxId());
		}
		return deletePaxList;
	}

	private static Set<Integer> getSegmentTransferredPaxIdList(List<PnlPassenger> pnlPassengers) {
		Set<String> pnrList = new HashSet<String>();
		Set<Integer> deletePaxList = new HashSet<Integer>();
		;
		Collection<Integer> pnrSegId = new ArrayList<Integer>();
		List<String> adlActions = new ArrayList<String>();
		adlActions.add(AdlActions.D.toString());

		for (PnlPassenger pnlPax : pnlPassengers) {
			if (!pnrList.contains(pnlPax.getPnr())) {
				pnrList.add(pnlPax.getPnr());
			}
			if (!pnrSegId.contains(pnlPax.getPnrSegId())) {
				pnrSegId.add(pnlPax.getPnrSegId());
			}
		}

		List<PnlPassenger> deletePassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
				.getNonSegmentTransferredPnlPassegers(pnrList, adlActions, pnrSegId);
		for (PnlPassenger deletePax : deletePassengers) {
			deletePaxList.add(deletePax.getPnrPaxId());
		}
		return deletePaxList;
	}

	public static Set<Integer> getUnrecordedPnrPaxIds(Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets,
			Reservation reservation) {
		Collection<ReservationPax> resPAx = reservation.getPassengers();
		Collection<Integer> pnrPaxIdList = null;
		Set<Integer> updatedPnrPaxIdList = new HashSet<Integer>();
		List<PnlPassenger> pnlPassengers = null;
		Set<Integer> deletePaxList = new HashSet<Integer>();

		for (ReservationPaxFareSegmentETicket eTicket : modifiedPaxfareSegmentEtickets) {
			for (ReservationPax pax : resPAx) {
				if (eTicket.getReservationPaxETicket().getPnrPaxId().intValue() == pax.getPnrPaxId().intValue()) {
					for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
						for (ReservationPaxFareSegment paxFareSeg : paxFare.getPaxFareSegments()) {
							if (paxFareSeg.getPnrPaxFareSegId().intValue() == eTicket.getPnrPaxFareSegId().intValue()) {
								pnrPaxIdList = new ArrayList<Integer>();
								pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlPassegers(
										paxFareSeg.getPnrSegId(), PNLConstants.PnlPassengerStates.NOT_UPDATED, false);

								Set<String> pnrList = new HashSet<String>();
								List<String> adlActions = new ArrayList<String>();
								adlActions.add(AdlActions.A.toString());
								for (PnlPassenger pnlPax : pnlPassengers) {
									if (!pnrList.contains(pnlPax.getPnr())) {
										pnrList.add(pnlPax.getPnr());
									}
									if (!pnrPaxIdList.contains(pnlPax.getPnrPaxId())) {
										pnrPaxIdList.add(pnlPax.getPnrPaxId());
									}
								}

								List<PnlPassenger> updatedPnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
										.getPnlPassegers(pnrList, adlActions, PNLConstants.PnlPassengerStates.UPDATED);

								for (PnlPassenger pnlPax : updatedPnlPassengers) {
									if (!updatedPnrPaxIdList.contains(pnlPax.getPnrPaxId())) {
										updatedPnrPaxIdList.add(pnlPax.getPnrPaxId());
									}
								}
								pnrPaxIdList.removeAll(updatedPnrPaxIdList);
								deletePaxList.addAll(pnrPaxIdList);
							}
						}
					}
				}
			}
		}
		return deletePaxList;
	}

	public static void recordReservationChanges(Reservation reservation, AdlActions adlAction) throws ModuleException {
		Set<ReservationPax> paxs = reservation.getPassengers();

		PnlPassenger pnlPassenger;
		Collection<PnlPassenger> colPnlPassengers = new ArrayList<PnlPassenger>();
		String validAdlAction = adlAction.toString();
		for (ReservationSegment resSeg : reservation.getSegments()) {
			if (!isValidSegmentToTriggerADL(resSeg, (AdlActions.A == adlAction))) {
				continue;
			}

			List<Integer> addRecordList = new ArrayList<Integer>();
			List<PnlPassenger> pnlPassengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlPassegers(
					resSeg.getPnrSegId(), PNLConstants.PnlPassengerStates.NOT_UPDATED, true);
			if (!pnlPassengers.isEmpty()) {
				if (pnlPassengers.get(0) != null) {
					for (PnlPassenger pnlPax : pnlPassengers) {
						if (pnlPax.getPnlStatus().equals(AdlActions.A.toString())) {
							addRecordList.add(pnlPax.getPnrPaxId());
						}
					}
					for (PnlPassenger pnlPax : pnlPassengers) {
						if (pnlPax.getPnlStatus().equals(adlAction.toString())) {
							pnlPax.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
						}
					}
					ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(pnlPassengers);
				}
			}
			for (ReservationPax pax : paxs) {

				if (!pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)
						&& (!("TBA").equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getFirstName()))) && !("TBA")
								.equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getLastName()))))) {
					if (AdlActions.D != adlAction || (!addRecordList.contains(pax.getPnrPaxId()) && AdlActions.D == adlAction)) {
						pnlPassenger = new PnlPassenger();
						pnlPassenger.setTitle(pax.getTitle());
						pnlPassenger.setFirstName(pax.getFirstName());
						pnlPassenger.setLastName(pax.getLastName());
						pnlPassenger.setPnr(pax.getReservation().getPnr());
						pnlPassenger.setPnlStatus(validAdlAction);
						pnlPassenger.setPnrPaxId(pax.getPnrPaxId());
						pnlPassenger.setPaxType(pax.getPaxType());
						pnlPassenger.setPnrSegId(resSeg.getPnrSegId());
						pnlPassenger.setFlightSegId(resSeg.getFlightSegId());
						pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.NOT_UPDATED);
						pnlPassenger.setBcType(resSeg.getBookingType());
						pnlPassenger.setBookingCode(resSeg.getBookingCode());
						pnlPassenger.setCabinClassCode(resSeg.getCabinClassCode());
						pnlPassenger.setLogicalCCCode(resSeg.getLogicalCCCode());
						colPnlPassengers.add(pnlPassenger);
					}
				}
			}
		}

		if (colPnlPassengers.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(colPnlPassengers);
		}

	}
	
	private static Collection<Integer> getPassengerIds(Collection<ReservationPax> passengers) {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		if (passengers != null && !passengers.isEmpty()) {
			for (ReservationPax pax : passengers) {
				pnrPaxIds.add(pax.getPnrPaxId());
			}
		}
		return pnrPaxIds;
	}

	private static Collection<Integer> getPaxHavingBalanceToPay(Reservation reservation) {
		Collection<Integer> paxHavingBalanceToPay = new ArrayList<Integer>();
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			pnrPaxIds.add(reservationPax.getPnrPaxId());
		}

		Map<Integer, BigDecimal> sortedMap = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getPNRPaxBalances(pnrPaxIds);
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			BigDecimal credit = sortedMap.get(reservationPax.getPnrPaxId());
			if (credit != null && credit.doubleValue() > 0) {
				paxHavingBalanceToPay.add(reservationPax.getPnrPaxId());
			}
		}
		return paxHavingBalanceToPay;
	}

	private static Collection<Integer> getPassengerIdsPerAction(Collection<PnlPassenger> pnlPassengers, String adlAction) {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (PnlPassenger pax : pnlPassengers) {
			if (adlAction == null) {
				pnrPaxIds.add(pax.getPnrPaxId());
			} else if (adlAction.equals(pax.getPnlStatus())) {
				pnrPaxIds.add(pax.getPnrPaxId());
			}
		}
		return pnrPaxIds;
	}
	
	
	/**
	 * Parent PAX already sent with PNL/ADL infant was not included due to pending payment. when respective payment is
	 * settled need to send the Infant details along with adult.
	 */
	private static void addDeleteParentForPnlAdlWhenTicketGeneratedOnlyForInfant(
			Collection<ReservationPax> etGenerationEligiblePax, Reservation reservation,
			Collection<Integer> paxHavingBalanceToPay) {

		if (reservation != null && reservation.isInfantPaymentRecordedWithInfant() && etGenerationEligiblePax != null
				&& !etGenerationEligiblePax.isEmpty()) {
			Collection<ReservationPax> parentPaxList = new ArrayList<>();
			Collection<Integer> etGenerationPaxIds = new ArrayList<>();
			Map<Integer, ReservationPax> reservationPaxMap = new HashMap<>();
			for (ReservationPax pax : reservation.getPassengers()) {
				if (PaxTypeTO.ADULT.equals(pax.getPaxType())) {
					reservationPaxMap.put(pax.getPnrPaxId(), pax);
				}
			}

			for (ReservationPax pax : etGenerationEligiblePax) {
				etGenerationPaxIds.add(pax.getPnrPaxId());
			}
			boolean isParentAdded = false;
			for (ReservationPax pax : etGenerationEligiblePax) {
				Integer pnrPaxId = pax.getPnrPaxId();

				if (PaxTypeTO.INFANT.equals(pax.getPaxType())
						&& (paxHavingBalanceToPay.isEmpty() || !paxHavingBalanceToPay.contains(pnrPaxId))
						&& etGenerationPaxIds.contains(pnrPaxId) && !etGenerationPaxIds.contains(pax.getAccompaniedPaxId())) {

					ReservationPax parentPax = reservationPaxMap.get(pax.getAccompaniedPaxId());
					if (parentPax != null) {
						parentPaxList.add(parentPax);
						isParentAdded = true;
					}

				}
			}

			if (isParentAdded) {
				etGenerationEligiblePax.addAll(parentPaxList);
			}

		}
	}

}
