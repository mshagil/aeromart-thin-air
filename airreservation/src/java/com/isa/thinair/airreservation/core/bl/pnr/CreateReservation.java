/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.airporttransfer.AirportTransferBL;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.ExternalAirportTransferChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalAutoCheckinChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalBaggageChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalFlexiChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalGenericChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalMealChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalSMChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalSSRChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalServiceTaxChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.airreservation.core.bl.ssr.SSRBL;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * Command for finalize the assembled reservation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="createReservation"
 */
public class CreateReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CreateReservation.class);

	/**
	 * Execute method of the CreateReservation command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		ReservationAdminInfoTO reservationAdminInfoTO = (ReservationAdminInfoTO) this
				.getParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Boolean triggerPaidAmountError = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR);
		Boolean triggerPnrForceConfirmed = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		Boolean isActualPayment = this.getParameter(CommandParamNames.IS_ACTUAL_PAYMENT, Boolean.class);
		Collection<OndFareDTO> colOndFareDTO = (Collection<OndFareDTO>) this.getParameter(CommandParamNames.OND_FARE_DTOS);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Boolean isGoShowProcess = (Boolean) this.getParameter(CommandParamNames.IS_GOSHOW_PROCESS);
		String bookingType = (String) this.getParameter(CommandParamNames.BOOKING_TYPE);
		Boolean isAllowReservationAfterCutOffTime = (Boolean) this
				.getParameter(CommandParamNames.ALLOW_CREATE_RESERVATION_AFTER_CUT_OFF_TIME);
		Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map) this
				.getParameter(CommandParamNames.PAX_CREDIT_PAYMENTS);
		Map<Integer, Map<Integer, EticketDTO>> eTicketInfo = (Map) this.getParameter(CommandParamNames.E_TICKET_MAP);

		Boolean otherCarrierPaxCreditUsed = (Boolean) this.getParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED);

		Collection<PaymentInfo> colPaymentInfo = (Collection<PaymentInfo>) this.getParameter(CommandParamNames.PAYMENT_INFO);
		AutoCancellationInfo autoCancellationInfo = (AutoCancellationInfo) this
				.getParameter(CommandParamNames.AUTO_CANCELLATION_INFO);

		DiscountedFareDetails discFareDetailsDTO = this.getParameter(CommandParamNames.FARE_DISCOUNT_INFO,
				DiscountedFareDetails.class);

		ReservationDiscountDTO reservationDiscountDTO = (ReservationDiscountDTO) this
				.getParameter(CommandParamNames.RESERVATION_DISCOUNT_DTO);

		Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps = (Map) this
				.getParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS);

		boolean isCreditPromotion = false;
		if (discFareDetailsDTO != null
				&& PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discFareDetailsDTO.getDiscountAs())) {
			isCreditPromotion = true;
		}

		// Checking params
		this.validateParams(reservation, credentialsDTO, triggerPaidAmountError, triggerPnrForceConfirmed, isGoShowProcess,
				colOndFareDTO);

		// Check the create reservation constraints
		this.checkConstraints(reservation);

		// Adds any external charges for confirmed passengers
		this.addExternalCharges(reservation, credentialsDTO, isCreditPromotion, reservationDiscountDTO);

		// Sets the passenger information
		Collection<ReservationPax>[] collections = this.setPassengerInformation(reservation, triggerPaidAmountError,
				triggerPnrForceConfirmed);
		Collection<ReservationPax> confirmAdults = collections[0];
		Collection<ReservationPax> confirmInfants = collections[1];
		Collection<ReservationPax> adults = collections[2];
		Collection<ReservationPax> infants = collections[3];

		int numberOfConfirmAdultSeats = confirmAdults.size();
		int numberOfConfirmInfantSeats = confirmInfants.size();

		// Check Constraints
		ValidationUtils.checkRestrictedSegmentConstraints(colOndFareDTO, reservation.getStatus(), triggerPnrForceConfirmed
				? ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED
				: ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT);

		// Sets the admin information
		this.setAdminInformation(reservation, reservationAdminInfoTO, credentialsDTO);

		this.setAutoCancellationInfo(reservation, autoCancellationInfo, credentialsDTO);

		// validate whether flight has reached closure time when the user complete the reservation
		this.validateSegAvailability(isGoShowProcess, reservation.getSegments(), reservation.getAdminInfo().getOriginChannelId(),
				isAllowReservationAfterCutOffTime, bookingType);

		ReservationApiUtils.updateModificationStates(reservation, reservation.getLastModificationTimestamp(),
				reservation.getLastCurrencyCode(), credentialsDTO);
		
		// integrated with AccelAero ETicket Server
		this.setAAETSServerEnabledWithCodeShareCarrier(reservation);		
		
		// Assign the ETicket numbers
		Collection<ReservationPax> confirmedPassengers = new ArrayList<ReservationPax>();
		confirmedPassengers.addAll(confirmAdults);
		confirmedPassengers.addAll(confirmInfants);
		reservation.setInfantPaymentSeparated(ReservationInternalConstants.InfantPaymentRecordedWithInfant.YES);
		// Saves the reservation
		ReservationProxy.saveReservation(reservation);

		//save in blacklistPaxReservation
		if(AppSysParamsUtil.isBlacklistpassengerCheckEnabled()){			
			ReservationBO.saveBlacklistedPnrPaxIfExist(reservation ,String.valueOf(this.getParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX)));
		}

		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
		if (csOCCarrirs != null) {
			// send Codeshare typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.setPaxSegmentSSRDTOMaps(paxSegmentSSRDTOMaps);
			typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CREATE_ONHOLD.getCode());
			TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
		}

		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			// Adding adl add record if pnl already sent
			ADLNotifyer.recordReservation(reservation, AdlActions.A, reservation.getPassengers(), null);
		}
		// check selected SSRs inventory available
		if (AppSysParamsUtil.isInventoryCheckForSSREnabled()) {
			ValidationUtils.checkSSRInventoryAvailability(reservation, null);
		}

		// Save E tickets for own booking
		if ((reservation.getOriginatorPnr() == null || (reservation.getOriginatorPnr() != null
				&& otherCarrierPaxCreditUsed != null && otherCarrierPaxCreditUsed.booleanValue()))
				&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {

			CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
			tktInfoDto.setBSPPayment(ReservationApiUtils.isBSPpayment(colPaymentInfo));

			if (eTicketInfo == null) {
				// We dont have the eticket info externally submitted. Generating eticket number internally
				if (isGoShowProcess) {
					eTicketInfo = ETicketBO.generateIATAETicketNumbersForGOSHOWBooking(confirmedPassengers, reservation,
							tktInfoDto);
				} else {
					if (!triggerPnrForceConfirmed || isActualPayment) {
						eTicketInfo = ETicketBO.generateIATAETicketNumbersForFreshBooking(confirmedPassengers, reservation,
								tktInfoDto);
					}
				}
			}
			if (eTicketInfo != null) {
				ETicketBO.saveETickets(eTicketInfo, confirmedPassengers, reservation);
			}
		}

		Map<Integer, LinkedList<Integer>> flgtSegmentMap = this.getFlightSegmentMap(reservation);

		// Retrieve pax ids with totals and set the necessary pnr pax id, pnr seg id, and pnr pax fare id
		Map<Integer, IPayment> paxIdsWithPayments = this.getPaxIdsWithPayments(reservation, paxCreditPayments);

		// Update the group booking request id if available //todo : add the paxCreditPayments
		ReservationCoreUtils.updateGrpBookingPaymentStatus(reservation, paxIdsWithPayments);

		// Saves the seat information
		Map<Integer, String> flightSeatIdsAndPaxTypes = ReservationBO.saveSeats(paxIdsWithPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		// Saves the meal information
		Collection[] flightMealInfo = ReservationBO.saveMeals(paxIdsWithPayments, flgtSegmentMap, reservation, credentialsDTO);

		// Saves baggage info
		Collection[] flightBaggageInfo = BaggageAdaptor.saveBaggages(paxIdsWithPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		// Saves flexi charges
		Collection flexiCharges = ReservationBO.saveFlexiCharges(paxIdsWithPayments, flgtSegmentMap, reservation, credentialsDTO);

		// Saves the airport transfer information
		Collection[] flightAirportTransferInfo = ReservationBO.saveAirportTransfers(paxIdsWithPayments, flgtSegmentMap,
				reservation, credentialsDTO);

		Collection[] flightAutoCheckinInfo = ReservationBO.saveAutoCheckins(paxIdsWithPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		// Save wait listing reservation priority
		if (bookingType != null && bookingType.equals(ReservationInternalConstants.ReservationType.WL.getDescription())) {

			ReservationBO.saveReservationWLPriority(reservation.getPnr(), adults.size(), infants.size(),
					this.getWaitListedSegments(reservation));
		}
		// send SSR add notifications
		SSRBL.sendSSRAddNotifications(reservation, null, credentialsDTO);
		AirportTransferBL.sendAirportTransferAddNotifications(reservation.getPnr(), null, credentialsDTO);

		// Updated Segments Ancillary Status
		ReservationCoreUtils.updateSegmentsAncillaryStatus(reservation);

		List<Integer> openRTSegmentIds = getOpenReturnSegmentIds(colOndFareDTO);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		Map<Integer, Map<String, BigDecimal>> lmsPaxProductRedeemedAmount = (Map<Integer, Map<String, BigDecimal>>) this
				.getParameter(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS);

		if (lmsPaxProductRedeemedAmount != null && lmsPaxProductRedeemedAmount.size() > 0) {
			Map<Integer, Map<String, BigDecimal>> lmsPnrPaxIdRedeemedAmounts = new HashMap<Integer, Map<String, BigDecimal>>();
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				if (lmsPaxProductRedeemedAmount.containsKey(reservationPax.getPaxSequence())) {
					lmsPnrPaxIdRedeemedAmounts.put(reservationPax.getPnrPaxId(),
							lmsPaxProductRedeemedAmount.get(reservationPax.getPaxSequence()));
				}
			}

			response.addResponceParam(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS, lmsPnrPaxIdRedeemedAmounts);
		}

		response.addResponceParam(CommandParamNames.PNR, reservation.getPnr());
		response.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		response.addResponceParam(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, paxIdsWithPayments);
		response.addResponceParam(CommandParamNames.NO_OF_CONFIRM_ADULT_SEATS, new Integer(numberOfConfirmAdultSeats));
		response.addResponceParam(CommandParamNames.NO_OF_CONFIRM_INFANT_SEATS, new Integer(numberOfConfirmInfantSeats));
		response.addResponceParam(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, reservation.isEnableTransactionGranularity());

		if (output == null) {
			output = new DefaultServiceResponse();
			output.addResponceParam(CommandParamNames.PNR, reservation.getPnr());
			output.addResponceParam(CommandParamNames.ZULU_BOOKING_TIMESTAMP, reservation.getZuluBookingTimestamp());
		} else {
			output.addResponceParam(CommandParamNames.PNR, reservation.getPnr());
			output.addResponceParam(CommandParamNames.ZULU_BOOKING_TIMESTAMP, reservation.getZuluBookingTimestamp());
		}
		
		output.addResponceParam(CommandParamNames.CMI_FATOURATI_REF, this.getParameter(CommandParamNames.CMI_FATOURATI_REF));

		ReservationPaymentMetaTO reservationPaymentMetaTO = TnxGranularityReservationUtils
				.getReservationPaymentMetaTO(reservation);

		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.FLIGHT_SEAT_IDS_AND_PAX_TYPES, flightSeatIdsAndPaxTypes);
		response.addResponceParam(CommandParamNames.RESERVATION_PAYMENT_META_TO, reservationPaymentMetaTO);
		response.addResponceParam(CommandParamNames.FLIGHT_MEAL_IDS, flightMealInfo);
		response.addResponceParam(CommandParamNames.FLIGHT_AIRPORT_TRANSFER_IDS, flightAirportTransferInfo);
		response.addResponceParam(CommandParamNames.FLIGHT_BAGGAGE_IDS, flightBaggageInfo);
		response.addResponceParam(CommandParamNames.FLIGHT_AUTO_CHECKIN_IDS, flightAutoCheckinInfo);
		response.addResponceParam(CommandParamNames.FLEXI_INFO, flexiCharges);
		response.addResponceParam(CommandParamNames.USER_NOTES, reservation.getUserNote());
		response.addResponceParam(CommandParamNames.OPENRT_SEGMENT_IDS, openRTSegmentIds);
		response.addResponceParam(CommandParamNames.CMI_FATOURATI_REF, this.getParameter(CommandParamNames.CMI_FATOURATI_REF));
		
		log.debug("Exit execute");
		return response;
	}	

	private List<Integer> getOpenReturnSegmentIds(Collection<OndFareDTO> colOndFareDTOs) {
		List<Integer> segIds = new ArrayList<Integer>();
		for (OndFareDTO ondFare : colOndFareDTOs) {
			if (ondFare.isOpenReturn() && ondFare.isInBoundOnd()) {
				for (FlightSegmentDTO seg : ondFare.getSegmentsMap().values()) {
					segIds.add(seg.getSegmentId());
				}
			}
		}
		return segIds;
	}

	/**
	 * Returns flight segment map to the pnr segment ids
	 * 
	 * @param reservation
	 * @return
	 */
	private Map<Integer, LinkedList<Integer>> getFlightSegmentMap(Reservation reservation) {
		Map<Integer, LinkedList<Integer>> flgtSegmentMap = new HashMap<Integer, LinkedList<Integer>>();

		ReservationSegment reservationSegment;
		LinkedList<Integer> pnrSegIds;
		for (Iterator<ReservationSegment> iterator = reservation.getSegments().iterator(); iterator.hasNext();) {
			reservationSegment = (ReservationSegment) iterator.next();

			pnrSegIds = flgtSegmentMap.get(reservationSegment.getFlightSegId());

			if (pnrSegIds == null) {
				pnrSegIds = new LinkedList<Integer>();
				pnrSegIds.add(reservationSegment.getPnrSegId());

				flgtSegmentMap.put(reservationSegment.getFlightSegId(), pnrSegIds);
			} else {
				pnrSegIds.add(reservationSegment.getPnrSegId());
			}
		}

		return flgtSegmentMap;
	}

	private List<Integer> getWaitListedSegments(Reservation reservation) {
		List<Integer> wlFlightSegments = new ArrayList<Integer>();

		ReservationSegment reservationSegment;
		for (Iterator<ReservationSegment> iterator = reservation.getSegments().iterator(); iterator.hasNext();) {
			reservationSegment = (ReservationSegment) iterator.next();
			if (reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationType.WL.toString())) {
				wlFlightSegments.add(reservationSegment.getFlightSegId());
			}

		}

		return wlFlightSegments;
	}

	/**
	 * Check the create reservation constraints
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private void checkConstraints(Reservation reservation) throws ModuleException {
		// Tempory Fix... Most probably permanent too.
		// Nili 5:29 PM 11/18/2008
		// Current Findings the EJB execution happens after 1+ hours time
		// This validation was added to prevent this from happening.
		long queryDRTimeDuration = AppSysParamsUtil.getTimeForQueryDROperation();
		Date currentTimestamp = new Date();
		Date zuluBookingTimestamp = reservation.getZuluBookingTimestamp();
		long diffMins = (currentTimestamp.getTime() - zuluBookingTimestamp.getTime()) / (60 * 1000);

		if (diffMins >= queryDRTimeDuration) {
			throw new ModuleException("airreservations.arg.cc.noresponse");
		}
	}

	/**
	 * Adds external charges
	 * 
	 * @param reservation
	 * @param discountCode
	 *            TODO
	 * @param isCreditPromotion
	 * @param reservationDiscountDTO
	 * @throws ModuleException
	 */
	private void addExternalCharges(Reservation reservation, CredentialsDTO credentialsDTO, boolean isCreditPromotion,
			ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {

		PaxDiscountInfoDTO paxDiscInfo = new PaxDiscountInfoDTO();
		if (reservationDiscountDTO != null) {
			paxDiscInfo.setDicountCode(reservationDiscountDTO.getDiscountCode());
		}

		paxDiscInfo.setCreditPromotion(isCreditPromotion);

		ExternalGenericChargesBL.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo,
				reservationDiscountDTO);
		ExternalSMChargesBL.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo, reservationDiscountDTO);
		ExternalMealChargesBL.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo,
				reservationDiscountDTO);

		ExternalBaggageChargesBL.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo,
				reservationDiscountDTO);

		ExternalSSRChargesBL
				.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo, reservationDiscountDTO);
		ExternalFlexiChargesBL.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo,
				reservationDiscountDTO);

		ExternalServiceTaxChargesBL.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo,
				reservationDiscountDTO);
		ExternalAirportTransferChargesBL.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo,
				reservationDiscountDTO);
		ExternalAutoCheckinChargesBL.reflectExternalChgsForAFreshBooking(reservation, credentialsDTO, paxDiscInfo,
				reservationDiscountDTO);
	}

	/**
	 * Return passenger ids with totals
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	private Map<Integer, IPayment> getPaxIdsWithPayments(Reservation reservation,
			Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		PaymentAssembler paymentAssembler;
		Map<Integer, IPayment> paxIdsWithTotals = new HashMap<Integer, IPayment>();

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			// Sending the pnr passenger id and the IPayment information per passenger
			paymentAssembler = (PaymentAssembler) reservationPax.getPayment();

			// Removing credit payments and link actual payments. Eg. Cash payment is done for the total available
			// credit but that payment may be of several payments
			for (Iterator<PaymentInfo> iterator = paymentAssembler.getPayments().iterator(); iterator.hasNext();) {
				PaymentInfo paymentInfo = iterator.next();
				if (paymentInfo.getPayCarrier().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					if (paymentInfo instanceof PaxCreditInfo)
						iterator.remove();
				}
			}

			// Adding payments.
			if (paxCreditPayments != null && paxCreditPayments.get(reservationPax.getPaxSequence()) != null) {
				for (TnxCreditPayment tnxCreditPayment : paxCreditPayments.get(reservationPax.getPaxSequence())) {
					paymentAssembler.addCreditPayment(tnxCreditPayment.getAmount(), tnxCreditPayment.getPnrPaxId(),
							tnxCreditPayment.getPaymentCarrier(), tnxCreditPayment.getPayCurrencyDTO(),
							tnxCreditPayment.getLccUniqueTnxId(), reservationPax.getPaxSequence(),
							tnxCreditPayment.getExpiryDate(), tnxCreditPayment.getPnr(), tnxCreditPayment.getPaxCreditId(),
							null, null);
				}
			}

			paymentAssembler.setTotalChargeAmount(reservationPax.getTotalChargeAmountWhenInfantHasPayment());
			paxIdsWithTotals.put(reservationPax.getPnrPaxId(), (IPayment) paymentAssembler);
		}

		return paxIdsWithTotals;
	}

	/**
	 * Sets the admin information
	 * 
	 * @param reservation
	 * @param reservationAdminInfoTO
	 * @param credentialsDTO
	 */
	private void setAdminInformation(Reservation reservation, ReservationAdminInfoTO reservationAdminInfoTO,
			CredentialsDTO credentialsDTO) {

		ReservationAdminInfo adminInfo = new ReservationAdminInfo();
		reservation.setAdminInfo(adminInfo);

		adminInfo.setOriginAgentCode(credentialsDTO.getAgentCode());
		adminInfo.setOriginChannelId(credentialsDTO.getSalesChannelCode());
		adminInfo.setOriginUserId(credentialsDTO.getUserId());
		adminInfo.setOwnerChannelId(credentialsDTO.getSalesChannelCode());
		adminInfo.setOwnerAgentCode(credentialsDTO.getAgentCode());

		if (credentialsDTO.getTrackInfoDTO() != null) {
			String carrierCode;

			carrierCode = BeanUtils.nullHandler(credentialsDTO.getTrackInfoDTO().getCarrierCode());
			if (carrierCode.length() > 0) {
				adminInfo.setOriginCarrierCode(carrierCode);
			}

			adminInfo.setOriginIPAddress(credentialsDTO.getTrackInfoDTO().getIpAddress());
			adminInfo.setOriginIPNumber(credentialsDTO.getTrackInfoDTO().getOriginIPNumber());
		}

		if (reservationAdminInfoTO != null) {
			adminInfo.setOriginSalesTerminal(reservationAdminInfoTO.getOriginSalesTerminal());
			adminInfo.setLastSalesTerminal(reservationAdminInfoTO.getLastSalesTerminal());

			if (reservationAdminInfoTO.isOverrideOriginAgentCode()) {
				adminInfo.setOriginAgentCode(reservationAdminInfoTO.getOriginAgentCode());
			}

			if (reservationAdminInfoTO.isOverrideOriginUserId()) {
				adminInfo.setOriginUserId(reservationAdminInfoTO.getOriginUserId());
			}

			if (reservationAdminInfoTO.isOverrideOwnerAgentCode()) {
				adminInfo.setOwnerAgentCode(reservationAdminInfoTO.getOwnerAgentCode());
			}

			if (reservationAdminInfoTO.isOverrideOwnerChannelId()) {
				adminInfo.setOwnerChannelId(reservationAdminInfoTO.getOwnerChannelId());
			}

			if (reservationAdminInfoTO.isOverrideOriginChannelId()) {
				adminInfo.setOriginChannelId(reservationAdminInfoTO.getOriginChannelId());
			}
		}

	}

	/**
	 * Set Passenger information
	 * 
	 * @param reservation
	 * @param triggerPaidAmountError
	 * @param triggerPnrForceConfirmed
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private Collection<ReservationPax>[] setPassengerInformation(Reservation reservation, Boolean triggerPaidAmountError,
			Boolean triggerPnrForceConfirmed) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		Collection<ReservationPax> confirmAdultsObjects = new ArrayList<ReservationPax>();
		Collection<ReservationPax> confirmInfantObjects = new ArrayList<ReservationPax>();

		Collection<ReservationPax> adultsObjects = new ArrayList<ReservationPax>();
		Collection<ReservationPax> infantObjects = new ArrayList<ReservationPax>();

		// Forcing the pnr to be confirmed if this is enabled
		if (triggerPnrForceConfirmed.booleanValue()) {
			// Updating the reservation passenger information
			while (itReservationPax.hasNext()) {
				reservationPax = (ReservationPax) itReservationPax.next();
				reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);

				if (ReservationApiUtils.isInfantType(reservationPax)) {
					confirmInfantObjects.add(reservationPax);
				} else {
					confirmAdultsObjects.add(reservationPax);
				}
			}
			adultsObjects.addAll(confirmAdultsObjects);
			infantObjects.addAll(confirmInfantObjects);
			reservation.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);

		} else {
			int i = 0;

			while (itReservationPax.hasNext()) {
				reservationPax = (ReservationPax) itReservationPax.next();

				if (reservation.getGroupBookingRequestID() != null
						|| ReservationApiUtils.checkPassengerConfirmationForAFreshBooking(reservationPax)) {
					reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
					i++;

					if (ReservationApiUtils.isInfantType(reservationPax)) {
						confirmInfantObjects.add(reservationPax);
					} else {
						confirmAdultsObjects.add(reservationPax);
					}
				} else {
					if (ReservationApiUtils.isInfantType(reservationPax)) {
						infantObjects.add(reservationPax);
					} else {
						adultsObjects.add(reservationPax);
					}
					if (triggerPaidAmountError.booleanValue()) {
						throw new ModuleException("airreservations.arg.paidAmountError");
					} else {
						reservationPax.setStatus(ReservationPaxStatus.ON_HOLD);
					}
				}
			}

			if (reservation.getPassengers().size() == i) {
				reservation.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);
			} else {
				reservation.setStatus(ReservationInternalConstants.ReservationStatus.ON_HOLD);
			}
		}

		return new Collection[] { confirmAdultsObjects, confirmInfantObjects, adultsObjects, infantObjects };
	}

	/**
	 * Validate Parameters
	 * 
	 * @param reservation
	 * @param credentialsDTO
	 * @param triggerPaidAmountError
	 * @param triggerPnrForceConfirmed
	 * @param isGoShowProcess
	 * @param colOndFareDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(Reservation reservation, CredentialsDTO credentialsDTO, Boolean triggerPaidAmountError,
			Boolean triggerPnrForceConfirmed, Boolean isGoShowProcess, Collection colOndFareDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (reservation == null || reservation.getContactInfo() == null || reservation.getSegments() == null
				|| reservation.getSegments().size() == 0 || reservation.getPassengers() == null
				|| reservation.getPassengers().size() == 0 || credentialsDTO == null || triggerPaidAmountError == null
				|| triggerPnrForceConfirmed == null || isGoShowProcess == null || colOndFareDTO == null
				|| colOndFareDTO.size() == 0) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	/**
	 * Validate whether flight segment\s have reached closure time during reservation process
	 * 
	 * @param resSeg
	 * @param originalSalesTerminal
	 * @param isAllowReservationAfterCutOffTime
	 *            TODO
	 * 
	 * @throws ModuleException
	 */
	private void validateSegAvailability(boolean isGoShowProcess, Collection<ReservationSegment> resSeg,
			int originalSalesTerminal, boolean isAllowReservationAfterCutOffTime, String bookingType) throws ModuleException {
		if (!isGoShowProcess) {
			log.debug("Inside validateSegAvailability");
			Collection<Integer> flightSegmentIds = new ArrayList<Integer>();
			Collection<Date> estDepartureZulu = new ArrayList<Date>();
			boolean firstSegmentIsDomestic = false;

			for (ReservationSegment reservationSegment : resSeg) {
				flightSegmentIds.add(reservationSegment.getFlightSegId());
			}

			Collection<FlightSegmentDTO> colFlightSegmentDetails = ReservationModuleUtils.getFlightBD().getFlightSegments(
					flightSegmentIds);

			for (FlightSegmentDTO flightSegmentDTO : colFlightSegmentDetails) {
				if (!flightSegmentDTO.getFlightStatus().equals(FlightStatusEnum.ACTIVE.getCode())) {
					throw new ModuleException("airreservations.arg.invalidSegCancel");
				}
			}
			Integer flightId = null;
			for (FlightSegmentDTO flightSegementDTO : colFlightSegmentDetails) {
				estDepartureZulu.add(flightSegementDTO.getDepartureDateTimeZulu());
				if (flightSegementDTO.isDomesticFlight()) {
					firstSegmentIsDomestic = true;
				}
				if (flightId == null) {
					flightId = flightSegementDTO.getFlightId();
				}
			}

			boolean isStandBy = false;
			if (BookingClass.BookingClassType.STANDBY.equals(bookingType)) {
				isStandBy = true;
			}

			boolean isFlightInClosure = false;
			if (!isAllowReservationAfterCutOffTime && !estDepartureZulu.isEmpty()) {
				if (originalSalesTerminal == SalesChannelsUtil.SALES_CHANNEL_AGENT) {
					if (isStandBy) {
						isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(CommonsServices.getGlobalConfig()
								.getBizParam(SystemParamKeys.STANDBY_AGENT_RESERVATION_CUTOFF_TIME), estDepartureZulu);
					} else {
						isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(
								FlightUtil.getApplicableResCutoffTime(flightId), estDepartureZulu);
					}
				} else if (SalesChannelsUtil.isSalesChannelWebOrMobile(originalSalesTerminal)) {
					if (firstSegmentIsDomestic) {
						isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(CommonsServices.getGlobalConfig()
								.getBizParam(SystemParamKeys.PUBLIC_RESERVATION_CUTOFF_TIME_FOR_DOMESTIC), estDepartureZulu);
					} else {
						isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(CommonsServices.getGlobalConfig()
								.getBizParam(SystemParamKeys.PUBLIC_RESERVATION_CUTOFF_TIME_FOR_INTERNATIONAL), estDepartureZulu);
					}
				} else if (originalSalesTerminal == SalesChannelsUtil.SALES_CHANNEL_GDS) {
					isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(CommonsServices.getGlobalConfig()
							.getBizParam(SystemParamKeys.GDS_RESERVATION_CUTOFF_TIME), estDepartureZulu);
				} else if (originalSalesTerminal == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES) {
					if (firstSegmentIsDomestic) {
						isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(CommonsServices.getGlobalConfig()
								.getBizParam(SystemParamKeys.API_RESERVATION_CUTOFF_TIME_FOR_DOMESTIC), estDepartureZulu);
					} else {
						isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(CommonsServices.getGlobalConfig()
								.getBizParam(SystemParamKeys.API_RESERVATION_CUTOFF_TIME_FOR_INTERNATIONAL), estDepartureZulu);
					}
				}
			}

			if (isFlightInClosure) {
				throw new ModuleException("airreservations.arg.flightInClosure");
			}
		}
	}

	private void setAutoCancellationInfo(Reservation reservation, AutoCancellationInfo autoCancellationInfo,
			CredentialsDTO credentialsDTO) throws ModuleException {
		if (autoCancellationInfo != null) {
			for (ReservationSegment resSeg : reservation.getSegments()) {
				resSeg.setAutoCancellationId(autoCancellationInfo.getAutoCancellationId());
			}
		}
	}
	
	private void setAAETSServerEnabledWithCodeShareCarrier(Reservation reservation) {
		// integrated with AccelAero ETicket Server
		boolean useAeroMartETS = false;
		if (reservation.getSegments() != null && !reservation.getSegments().isEmpty()) {
			for (ReservationSegment segment : reservation.getSegments()) {
				if (!StringUtil.isNullOrEmpty(segment.getCsOcCarrierCode())) {
					if(ReservationApiUtils.isAccelAeroEtsIntegratedCSCarrier(segment.getCsOcCarrierCode())){
						useAeroMartETS = true;
						break;
					}
				}

			}
		}

		if (useAeroMartETS) {
			reservation.setUseAeroMartETS(ReservationInternalConstants.UseAccelAeroETS.YES);
		} else {
			reservation.setUseAeroMartETS(ReservationInternalConstants.UseAccelAeroETS.NO);
		}
	}

}
