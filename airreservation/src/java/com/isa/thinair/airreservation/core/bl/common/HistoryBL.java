package com.isa.thinair.airreservation.core.bl.common;

import java.io.StringWriter;
import java.util.Collection;
import java.util.HashMap;

import com.isa.thinair.airproxy.api.dto.UserNoteHistoryTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.TemplateEngine;

/**
 * Reservation History Print related business logics
 * 
 * @author Manoji
 */
public class HistoryBL {

	private HistoryBL() {

	}

	public static String getHistoryForPrint(String pnr, Collection<UserNoteHistoryTO> colUserNoteHistoryTO)
			throws ModuleException {
		// Create data map
		HashMap<String, Object> historyDataMap = getHistoryDataMap(pnr, colUserNoteHistoryTO);
		String templateName = ReservationModuleUtils.getAirReservationConfig().getHistoryPrintTemplate();
		String localeSpecificTemplateName = templateName;

		// Use engine to generate page
		StringWriter writer = new StringWriter();
		try {
			new TemplateEngine().writeTemplate(historyDataMap, localeSpecificTemplateName, writer);
		} catch (Exception e) {
			throw new ModuleException("airreservations.history.errorCreatingFromTemplate", e);
		}

		return writer.toString();
	}

	private static HashMap<String, Object> getHistoryDataMap(String pnr, Collection<UserNoteHistoryTO> colUserNoteHistoryTO)
			throws ModuleException {

		HashMap<String, Object> historyDataMap = new HashMap<String, Object>();

		historyDataMap.put("pnr", pnr);
		historyDataMap.put("historyTO", colUserNoteHistoryTO);
		return historyDataMap;
	}

}
