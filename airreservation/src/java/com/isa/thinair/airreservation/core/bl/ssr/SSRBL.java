package com.isa.thinair.airreservation.core.bl.ssr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.model.SSRSubCategory;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.AncilaryNotificationDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.ssr.ReservationSSRResultDTO;
import com.isa.thinair.airreservation.api.dto.ssr.ReservationSSRSearchDTO;
import com.isa.thinair.airreservation.api.dto.ssr.CommonMedicalSsrParamDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.airreservation.core.util.SSRCommonUtil;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.CommonTemplatingDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

public class SSRBL {

	public static void sendSSRAddNotifications(Reservation reservation, Collection<ReservationSegment> pnrSegments,
			CredentialsDTO credentialsDTO) throws ModuleException {

		if (AppSysParamsUtil.isHalaInstantNotificationsEnabled()) {
			Collection<ReservationSSRResultDTO> ssrResultDTOs = loadReservationSSRs(reservation, null,
					ReservationApiUtils.getPNRSegmentIds(pnrSegments), null, false);
			if (ssrResultDTOs != null && ssrResultDTOs.size() > 0) {
				AncilaryNotificationDTO ssrNotificationDTO = new AncilaryNotificationDTO();
				Date depDate = ReservationApiUtils.getDepartureDate(reservation.getPnr());

				ssrNotificationDTO.setDepartureDate(depDate);
				ssrNotificationDTO.setNotificationType(AncilaryNotificationDTO.NotificationType.ADD);
				ssrNotificationDTO.setPnr(reservation.getPnr());

				sendSSRNotifications(ssrResultDTOs, ssrNotificationDTO, credentialsDTO);
			}
		}
	}

	/**
	 * Format and Fill <link>SSRNotificationDTO</link> <br>
	 * Last modified :by dumindaG for JIRA AARESAA-2678
	 * 
	 * @param ssrResultDTOs
	 * @param ssrNotificationDTO
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private static void sendSSRNotifications(Collection<ReservationSSRResultDTO> ssrResultDTOs,
			AncilaryNotificationDTO ssrNotificationDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		Collection<ReservationSSRResultDTO> mailSSRResultDTOs = new ArrayList<ReservationSSRResultDTO>();
		Integer lastSSRCatId = null;
		boolean isFirstMaas = true;
		Collection<ReservationSSRResultDTO> colMaasSSRResultDTO = new ArrayList<ReservationSSRResultDTO>();
		StringBuilder sbMAASPaxNames = new StringBuilder();
		String strMaasConnFlightNo = null;
		String strGrabFirstMaasFlight = null;
		for (Iterator<ReservationSSRResultDTO> iterator = ssrResultDTOs.iterator(); iterator.hasNext();) {
			ReservationSSRResultDTO ssrResultDTO = (ReservationSSRResultDTO) iterator.next();

			Integer ssrCategoryId = ssrResultDTO.getSsrCategoryId();

			if (lastSSRCatId != null && ssrCategoryId != lastSSRCatId) {
				ssrNotificationDTO.setSSRCatId(lastSSRCatId);
				sendSSRNotificationMail(mailSSRResultDTOs, ssrNotificationDTO, credentialsDTO);
				mailSSRResultDTOs = new ArrayList<ReservationSSRResultDTO>();
			}
			ReservationSSRResultDTO maasSSRResultDTO = null;
			if (SSRSubCategory.ID_MASS.equals(ssrResultDTO.getSsrSubCategoryId())) {

				if (strGrabFirstMaasFlight == null || ssrResultDTO.getFlightNo().equals(strGrabFirstMaasFlight)) {
					sbMAASPaxNames.append(ssrResultDTO.getTitle());
					sbMAASPaxNames.append(" ");
					sbMAASPaxNames.append(ssrResultDTO.getFirstName());
					sbMAASPaxNames.append(" ");
					sbMAASPaxNames.append(ssrResultDTO.getLastName());
					sbMAASPaxNames.append("<br>");
				}

				if (isFirstMaas) {
					maasSSRResultDTO = new ReservationSSRResultDTO();
					maasSSRResultDTO = ssrResultDTO;
					maasSSRResultDTO.setTitle("");
					maasSSRResultDTO.setFirstName("");
					maasSSRResultDTO.setLastName("");
					colMaasSSRResultDTO.add(maasSSRResultDTO);
					isFirstMaas = false;
				}

				if (strMaasConnFlightNo != null && !ssrResultDTO.getFlightNo().equals(strMaasConnFlightNo)) {
					maasSSRResultDTO = new ReservationSSRResultDTO();
					maasSSRResultDTO = ssrResultDTO;
					maasSSRResultDTO.setTitle("");
					maasSSRResultDTO.setFirstName("");
					maasSSRResultDTO.setLastName("");
					colMaasSSRResultDTO.add(maasSSRResultDTO);
				}
				strMaasConnFlightNo = ssrResultDTO.getFlightNo();
				lastSSRCatId = ssrCategoryId;

				if (strGrabFirstMaasFlight == null) {
					strGrabFirstMaasFlight = ssrResultDTO.getFlightNo();
				}
			} // end maas

			else {
				lastSSRCatId = ssrCategoryId;
				mailSSRResultDTOs.add(ssrResultDTO);
			}

		} // end for

		if (colMaasSSRResultDTO != null && !colMaasSSRResultDTO.isEmpty()) {
			for (ReservationSSRResultDTO resSSRResDTO : colMaasSSRResultDTO) {
				resSSRResDTO.setTitle(sbMAASPaxNames.toString());
				mailSSRResultDTOs.add(resSSRResDTO);

			}
		}
		if (mailSSRResultDTOs != null && !mailSSRResultDTOs.isEmpty()) {
			ssrNotificationDTO.setSSRCatId(lastSSRCatId);
			sendSSRNotificationMail(mailSSRResultDTOs, ssrNotificationDTO, credentialsDTO);
		}

	}

	private static Collection<ReservationSSRResultDTO> loadReservationSSRs(Reservation reservation,
			Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegmentIds, Collection<Integer> paxSegSSRIds,
			boolean loadCancelledSSRs) {
		ReservationSSRSearchDTO reservationSSRSearchDTO = new ReservationSSRSearchDTO();
		reservationSSRSearchDTO.setPNRNo(reservation.getPnr());
		reservationSSRSearchDTO.setPNRPaxIds(pnrPaxIds);
		reservationSSRSearchDTO.setPNRSegmentIds(pnrSegmentIds);
		reservationSSRSearchDTO.setPNRSegmentIds(pnrSegmentIds);
		reservationSSRSearchDTO.setpNRPaxSegmentSSRIds(paxSegSSRIds);
		reservationSSRSearchDTO.setLoadCancelledSSRs(loadCancelledSSRs);
		Collection<ReservationSSRResultDTO> ssrResultDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_SSR
				.getReservationSSR(reservationSSRSearchDTO);

		return ssrResultDTOs;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendSSRNotificationMail(Collection<ReservationSSRResultDTO> mailSSRResultDTOs,
			AncilaryNotificationDTO ssrNotificationDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

		ssrNotificationDTO.setMailSubject(getSSRNotificationSubject(ssrNotificationDTO));
		ssrNotificationDTO.setMailTitle(getSSRNotificationSubject(ssrNotificationDTO));
		ssrNotificationDTO.setFirstName("");
		ssrNotificationDTO.setLastName("");

		UserMessaging userMessaging = new UserMessaging();

		userMessaging.setFirstName(ssrNotificationDTO.getFirstName());
		userMessaging.setLastName(ssrNotificationDTO.getLastName());
		userMessaging.setToAddres(globalConfig.getSSRCategoryEMailMap().get(ssrNotificationDTO.getSSRCatId()));

		List messages = new ArrayList();
		messages.add(userMessaging);

		HashMap emailDataMap = (HashMap) getEMailDataMap(ssrNotificationDTO, mailSSRResultDTOs, credentialsDTO);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(emailDataMap);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.SSR_NOTIFICATION_EMAIL);
		topic.setLocale((ssrNotificationDTO.getLocale() == null) ? Locale.getDefault() : ssrNotificationDTO.getLocale());
		topic.setAttachMessageBody(true);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		messagingServiceBD.sendMessage(messageProfile);

	}

	private static String getSSRNotificationSubject(AncilaryNotificationDTO ssrNotificationDTO) {
		String subject = null;
		Integer catId = ssrNotificationDTO.getSSRCatId();

		if (AncilaryNotificationDTO.NotificationType.ADD.equals(ssrNotificationDTO.getNotificationType())) {
			if (SSRCategory.ID_AIRPORT_SERVICE.equals(catId)) {
				subject = "New Hala booking";
			} else {
				subject = "New Standard booking";
			}
		} else {
			if (SSRCategory.ID_AIRPORT_SERVICE.equals(catId)) {
				subject = "Hala booking Cancelled";
			} else {
				subject = "Standard booking Cancelled";
			}
		}

		return subject;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map getEMailDataMap(AncilaryNotificationDTO anciNotificationDTO,
			Collection<ReservationSSRResultDTO> ssrResultDTOs, CredentialsDTO credentialsDTO) throws ModuleException {

		CommonTemplatingDTO commonTemplatingDTO = AppSysParamsUtil.composeCommonTemplatingDTO(anciNotificationDTO
				.getCarrierCode());
		Map emailDataMap = new HashMap();
		emailDataMap.put("anciNotificationDTO", anciNotificationDTO); // ssrNotificationDTO
		emailDataMap.put("anciResultsDTO", ssrResultDTOs); // ssrResultDTOs

		if (commonTemplatingDTO != null) {
			emailDataMap.put("commonTemplatingDTO", commonTemplatingDTO);
		}

		return emailDataMap;
	}

	public static void sendSSRAddPaxNotifications(Reservation reservation, Collection<ReservationPax> colReservationPax,
			CredentialsDTO credentialsDTO) throws ModuleException {
		Collection<ReservationSSRResultDTO> ssrResultDTOs = loadReservationSSRs(reservation, null,
				ReservationApiUtils.getPNRPaxIds(colReservationPax), null, false);
		AncilaryNotificationDTO ssrNotificationDTO = new AncilaryNotificationDTO();

		ssrNotificationDTO.setPnr(reservation.getPnr());
		sendSSRNotifications(ssrResultDTOs, ssrNotificationDTO, credentialsDTO);

	}

	public static void sendSSRCancelNotifications(Reservation reservation,
			Map<Integer, Collection<ReservationPaxSegmentSSR>> cancelledSSRMap, CredentialsDTO credentialsDTO)
			throws ModuleException {
		Collection<Integer> paxSegSSRIds = SSRCommonUtil.getPaxSegmentSSRIds(cancelledSSRMap);
		Collection<ReservationSSRResultDTO> ssrResultDTOs = loadReservationSSRs(reservation, null, null, paxSegSSRIds, true);
		AncilaryNotificationDTO ssrNotificationDTO = new AncilaryNotificationDTO();

		ssrNotificationDTO.setPnr(reservation.getPnr());
		sendSSRNotifications(ssrResultDTOs, ssrNotificationDTO, credentialsDTO);
	}

	public static void modifySSRs(Reservation reservation, AdjustCreditBO adjustCreditBO, String pnr, Integer version,
			Map<Integer, SegmentSSRAssembler> ssrAssemblerMap, IPassenger iPassenger, String userNotes,
			EXTERNAL_CHARGES extChargeType, CredentialsDTO credentialsDTO, boolean cancelOnly) throws ModuleException {

		// TODO remove this
		ADLNotifyer.updateStatusToCHG(reservation);
		// Recording adl change operation according to new implemenatation
		ADLNotifyer.recordReservation(reservation, AdlActions.C, reservation.getPassengers(), null);

		ReservationProxy.saveReservation(reservation);
		List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

		if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null && credentialsDTO.getTrackInfoDTO()
				.getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
			// send typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.MODIFY_SSR.getCode());
			typeBRequestDTO.setSsrAssemblerMap(ssrAssemblerMap);
			TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
		}

		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
		if (csOCCarrirs != null) {
			// send Codeshare typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.MODIFY_SSR.getCode());
			typeBRequestDTO.setSsrAssemblerMap(ssrAssemblerMap);
			TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
		}

		// check selected SSRs inventory available
		if (AppSysParamsUtil.isInventoryCheckForSSREnabled()) {
			ValidationUtils.checkSSRInventoryAvailability(reservation, null);
		}

		adjustCreditBO.callRevenueAccountingAfterReservationSave();

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);

		Reservation newReservation = ReservationProxy.getReservation(pnrModesDTO);
		List<Integer> forcedConfirmedPaxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(newReservation);
		handleETGeneration(reservation, forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange, credentialsDTO);

		recordPaxWiseAuditHistoryForSSR(pnr, userNotes, ssrAssemblerMap, credentialsDTO);
	}

	private static void handleETGeneration(Reservation reservation, List<Integer> forcedConfirmedPaxSeqsBeforeChange,
			List<Integer> forcedConfirmedPaxSeqsAfterChange, CredentialsDTO credentialsDTO) throws ModuleException {

		if (reservation.getOriginatorPnr() == null) {
			Collection<ReservationPax> etGenerationEligiblePax = ReservationApiUtils.getConfirmedPaxByOperationUsingForceCNFSeqs(
					forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange, reservation);
			Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
			if (!etGenerationEligiblePax.isEmpty()) {
				CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
				modifiedPaxfareSegmentEtickets = ETicketBO.updateETickets(
						ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, tktInfoDto), etGenerationEligiblePax,
						reservation, true);

				if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
					Collection<Integer> affectingPnrSegIds = new ArrayList<Integer>();
					for (ReservationPax resPax : reservation.getPassengers()) {
						Iterator<ReservationPaxFare> itReservationPaxFare = resPax.getPnrPaxFares().iterator();
						Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
						ReservationPaxFare reservationPaxFare;
						ReservationPaxFareSegment reservationPaxFareSegment;

						while (itReservationPaxFare.hasNext()) {
							reservationPaxFare = itReservationPaxFare.next();
							itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

							while (itReservationPaxFareSegment.hasNext()) {
								reservationPaxFareSegment = itReservationPaxFareSegment.next();
								for (ReservationPaxFareSegmentETicket resPaxFareSegmentETicket : modifiedPaxfareSegmentEtickets) {
									if (resPaxFareSegmentETicket.getPnrPaxFareSegId().equals(
											reservationPaxFareSegment.getPnrPaxFareSegId())) {
										affectingPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
									}
								}
							}
						}
					}
					ADLNotifyer.recordCanceledSegmentsForAdl(reservation, affectingPnrSegIds, etGenerationEligiblePax);
				}
				ADLNotifyer.recordReservation(reservation, AdlActions.A, etGenerationEligiblePax, null);
			}
		}
	}

	private static void recordPaxWiseAuditHistoryForSSR(String pnr, String userNotes,
			Map<Integer, SegmentSSRAssembler> ssrAssemblerMap, CredentialsDTO credentialsDTO) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		// FIXME ME change to meal templates
		reservationAudit.setModificationType(AuditTemplateEnum.MODIFY_SSR.getCode());
		Set<Integer> paxSeqSet = ssrAssemblerMap.keySet();

		String addContent = "";
		String updateContent = "";
		String removeContent = "";

		boolean recordModification = false;
		for (Integer paxId : paxSeqSet) {
			SegmentSSRAssembler ssrAsm = ssrAssemblerMap.get(paxId);
			Map<Integer, Collection<PaxSegmentSSRDTO>> added = ssrAsm.getNewSegmentSSRDTOMap();
			if (added != null && added.size() > 0) {
				addContent += buildSSRAddedInfo(added);
				// reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SSRs.ADDED+paxId,
				// buildSSRAddedInfo(added));
				recordModification = true;
			}
			Map<Integer, PaxSegmentSSRDTO> updated = ssrAsm.getUpdatedSegmentSSRDTOMap();
			if (updated != null && updated.size() > 0) {
				addContent += buildSSRUpdatedInfo(updated);
				// reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SSRs.ADDED+paxId,
				// buildSSRUpdatedInfo(updated));
				recordModification = true;
			}
			Map<Integer, String> cancelled = ssrAsm.canceledSegemntSSRCodeMap();
			if (cancelled != null && cancelled.size() > 0) {
				removeContent += buildSSRCancelledInfo(cancelled);
				// reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SSRs.REMOVED+paxId,
				// buildSSRCancelledInfo(cancelled));
				recordModification = true;
			}

		}

		if (addContent != null && !addContent.equals("")) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SSRs.ADDED, addContent);
		}

		if (updateContent != null && !updateContent.equals("")) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SSRs.ADDED, addContent);
		}

		if (removeContent != null && !removeContent.equals("")) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SSRs.REMOVED, removeContent);
		}

		if (recordModification) {
			ReservationBO.recordModification(pnr, reservationAudit, userNotes, credentialsDTO);
		}

	}

	private static String buildSSRCancelledInfo(Map<Integer, String> cancelled) {
		StringBuilder builder = new StringBuilder("Cancelled ssrs=");
		Set<Integer> cSet = cancelled.keySet();
		boolean first = true;
		for (Integer key : cSet) {
			if (!first) {
				builder.append(",");
			} else {
				first = false;
			}
			builder.append(cancelled.get(key)); // TODO

		}
		return builder.toString();
	}

	private static String buildSSRUpdatedInfo(Map<Integer, PaxSegmentSSRDTO> updated) {
		Set<Integer> keySet = updated.keySet();
		StringBuilder builder = new StringBuilder("Updated ssrs=");
		boolean first = true;
		for (Integer key : keySet) {
			if (!first) {
				builder.append(",");
			} else {
				first = false;
			}
			PaxSegmentSSRDTO paxSSR = updated.get(key);
			String ssrText = "";
			if (ReservationPaxSegmentSSR.APPLY_ON_SEGMENT.equals(paxSSR.getApplyOn())) {
				// get Additional information for in-flight services
				if (paxSSR.getSSRText() != null) {
					ssrText = "-" + paxSSR.getSSRText();
				}
			} else {
				// get applicable airport for airport services
				if (paxSSR.getAirportCode() != null) {
					ssrText = "-" + paxSSR.getAirportCode();
				}
			}
			builder.append(paxSSR.getSsrCode() + ssrText);
		}
		return builder.toString();
	}

	private static String buildSSRAddedInfo(Map<Integer, Collection<PaxSegmentSSRDTO>> added) {
		Set<Integer> keySet = added.keySet();
		StringBuilder builder = new StringBuilder("  Added SSRs");
		for (Integer key : keySet) {
			boolean first = true;
			builder.append("Pax ::");
			for (PaxSegmentSSRDTO paxSSR : added.get(key)) {
				if (!first) {
					builder.append(",");
				} else {
					first = false;
				}
				String ssrText = "";
				if (ReservationPaxSegmentSSR.APPLY_ON_SEGMENT.equals(paxSSR.getApplyOn())) {
					// get Additional information for in-flight services
					if (paxSSR.getSSRText() != null) {
						ssrText = "-" + paxSSR.getSSRText();
					}
				} else {
					// get applicable airport for airport services
					if (paxSSR.getAirportCode() != null) {
						ssrText = "-" + paxSSR.getAirportCode();
					}
				}
				builder.append(paxSSR.getSsrCode() + ssrText);
			}
		}
		return builder.toString();
	}

	public static boolean checkSSRChargeAssignedInReservation(int ssrId) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_SSR.checkSSRChargeAssignedInReservation(ssrId);
	}

	/**
	 * Email the Medical ssr
	 * 
	 * @param medicalSsrDTO
	 * @throws ModuleException
	 */
	public static void sendEmailMedicalSsr(CommonMedicalSsrParamDTO medicalSsrDTO)
			throws ModuleException {

		// Check to see if the required contact details are present
		CommonReservationContactInfo contactInfo = medicalSsrDTO.getContactInfo();
		if (BeanUtils.nullHandler(contactInfo.getEmail()).length() == 0) {
			throw new ModuleException("airreservations.arg.invalidEmailOnlyUser");
		} else if (BeanUtils.nullHandler(contactInfo.getFirstName()).length() == 0
				|| BeanUtils.nullHandler(contactInfo.getLastName()).length() == 0) {
			throw new ModuleException("airreservations.arg.invalidEmailUser");
		}

		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

		// User Message
		UserMessaging userMessaging = new UserMessaging();
		userMessaging.setFirstName(contactInfo.getFirstName());
		userMessaging.setLastName(contactInfo.getLastName());
		userMessaging.setToAddres(contactInfo.getEmail());

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		HashMap<String, Object> itineraryDataMap = getMedicalSsrDataMap(medicalSsrDTO);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(itineraryDataMap);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.MEDICAL_SSR_EMAIL);
		topic.setLocale(new Locale(medicalSsrDTO.getMedicalSSREmailLanguage()));
		topic.setAttachMessageBody(false);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		messagingServiceBD.sendMessage(messageProfile);
	}

	private static HashMap<String, Object> getMedicalSsrDataMap(CommonMedicalSsrParamDTO medicalSsrDTO)
			throws ModuleException {

		HashMap<String, Object> interlineItineraryDataMap = new HashMap<String, Object>();

		interlineItineraryDataMap.put("paxContactInfo", medicalSsrDTO.getContactInfo());
		interlineItineraryDataMap.put("paxWiseMedicalSsrSegments", medicalSsrDTO.getPaxWiseMedicalSsrSegments());
		interlineItineraryDataMap.put("pnr",medicalSsrDTO.getPnr());
		interlineItineraryDataMap.put("medicalSSREmailMsg", medicalSsrDTO.getMedicalSSREmailMsg());
		interlineItineraryDataMap.put("medicalSSREmailTopic", medicalSsrDTO.getMedicalSSREmailTopic());
		interlineItineraryDataMap.put("linkToFillMedicalSSRInfo", medicalSsrDTO.getLinkToFillMedicalSSRInfo());
		interlineItineraryDataMap.put("medicalSSREmailLanguage", medicalSsrDTO.getMedicalSSREmailLanguage());
		
		return interlineItineraryDataMap;
	}

}
