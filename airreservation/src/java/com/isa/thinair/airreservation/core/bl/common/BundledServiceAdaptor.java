package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class BundledServiceAdaptor {

	public static BundledFareDTO refundBundledServiceChargeIfApplicable(AdjustCreditBO adjustCreditBO, Reservation res,
			Integer pnrSegId, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		BundledFareDTO bundledFareDTO = null;
		if (AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption()) {
			Set<Integer> affectedPnrSegIds = new HashSet<Integer>();
			affectedPnrSegIds.add(pnrSegId);
			Map<Integer, BundledFareDTO> pnrSegWiseSelectedBundles = getPnrSegWiseBundledFares(affectedPnrSegIds, res);
			Map<Integer, String> chargeRateCodeMapping = getChargeRateChargeCodeMapping(res);
			Map<String, ReservationPaxOndCharge> perPaxOndCharge = new HashMap<String, ReservationPaxOndCharge>();
			Set<Integer> bundledChgRefundedPnrSegIds = new HashSet<Integer>();

			for (ReservationPax reservationPax : res.getPassengers()) {
				Set<ReservationPaxFare> reservationPaxFares = reservationPax.getPnrPaxFares();
				for (ReservationPaxFare reservationPaxFare : reservationPaxFares) {

					Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();
					if (isPnrSegExists(affectedPnrSegIds, paxFareSegments)) {
						for (ReservationPaxFareSegment reservationPaxFareSegment : paxFareSegments) {
							Integer resPnrSegId = reservationPaxFareSegment.getPnrSegId();
							if (affectedPnrSegIds.contains(resPnrSegId) && pnrSegWiseSelectedBundles.containsKey(resPnrSegId)) {
								BundledFareDTO selBundledFare = pnrSegWiseSelectedBundles.get(resPnrSegId);

								String bundledChargeCode = selBundledFare.getChargeCode();

								Set<ReservationPaxOndCharge> charges = reservationPaxFare.getCharges();
								List<ReservationPaxOndCharge> refundableCharges = new ArrayList<ReservationPaxOndCharge>();
								for (ReservationPaxOndCharge paxOndCharge : charges) {
									String chargeCode = chargeRateCodeMapping.get(paxOndCharge.getChargeRateId());
									if (chargeCode != null && chargeCode.equals(bundledChargeCode)) {
										refundableCharges.add(paxOndCharge);
									}
								}

								if (!refundableCharges.isEmpty()) {
									for (ReservationPaxOndCharge paxOndCharge : refundableCharges) {
										ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
												.captureReservationPaxOndCharge(paxOndCharge.getAmount().negate(), null,
														paxOndCharge.getChargeRateId(), paxOndCharge.getChargeGroupCode(),
														reservationPaxFare, credentialsDTO, false, null, null, null,
														chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

										if (paxOndCharge.getDiscount() != null && paxOndCharge.getDiscount().doubleValue() < 0) {
											reservationPaxOndCharge.setDiscount(paxOndCharge.getDiscount().abs());
										}

										// rebuild unique id with flight seat id
										String uniqueId = SMUtil.makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
												reservationPaxFare.getPnrPaxFareId(), resPnrSegId);

										perPaxOndCharge.put(uniqueId, reservationPaxOndCharge);
										bundledChgRefundedPnrSegIds.add(resPnrSegId);
									}
								}

							}

						}
					}
				}
			}

			if (!perPaxOndCharge.isEmpty()) {
				// Set reservation and passenger total amounts
				ReservationCoreUtils.setPnrAndPaxTotalAmounts(res);

				for (String uniqueId : perPaxOndCharge.keySet()) {
					Integer pnrPaxId = Integer.parseInt(SMUtil.getPnrPaxId(uniqueId));
					ReservationPaxOndCharge paxOndCharge = perPaxOndCharge.get(uniqueId);
					if (paxOndCharge.getEffectiveAmount().compareTo(BigDecimal.ZERO) != 0) {
						adjustCreditBO.addAdjustment(res.getPnr(), pnrPaxId, "", paxOndCharge, credentialsDTO,
								res.isEnableTransactionGranularity());
					}
				}

				// Remove selected bundled fare from segment(s)
				if (!bundledChgRefundedPnrSegIds.isEmpty()) {
					for (ReservationSegment resSeg : res.getSegments()) {
						if (bundledChgRefundedPnrSegIds.contains(resSeg.getPnrSegId())) {
							resSeg.setBundledFarePeriodId(null);
						}
					}
				}

				// Save the reservation
				ReservationProxy.saveReservation(res);
			}

			bundledFareDTO = pnrSegWiseSelectedBundles.get(pnrSegId);
		}

		return bundledFareDTO;
	}

	public static boolean isServiceIncludedInBundle(BundledFareDTO bundledFareDTO, String serviceName) {
		if (bundledFareDTO == null) {
			return false;
		} else if (bundledFareDTO.isServiceIncluded(serviceName)) {
			return true;
		}

		return false;
	}

	private static boolean isPnrSegExists(Set<Integer> affectedPnrSegIds, Set<ReservationPaxFareSegment> paxFareSegments) {

		for (ReservationPaxFareSegment reservationPaxFareSegment : paxFareSegments) {

			Integer pnrSegId = reservationPaxFareSegment.getPnrSegId();
			if (pnrSegId != null && affectedPnrSegIds.contains(pnrSegId)) {
				return true;
			}

		}

		return false;
	}

	private static Map<Integer, BundledFareDTO> getPnrSegWiseBundledFares(Set<Integer> affectedPnrSegIds, Reservation res)
			throws ModuleException {
		Map<Integer, BundledFareDTO> pnrSegWiseSelectedBundles = null;
		if (affectedPnrSegIds != null && !affectedPnrSegIds.isEmpty()) {
			pnrSegWiseSelectedBundles = new HashMap<Integer, BundledFareDTO>();
			Map<Integer, Set<Integer>> bundledServicePnrSegIdMap = new HashMap<Integer, Set<Integer>>();
			Set<Integer> selectedBundledServicePeriodIds = new HashSet<Integer>();
			for (ReservationSegment resSeg : res.getSegments()) {
				if (resSeg.getBundledFarePeriodId() != null) {
					selectedBundledServicePeriodIds.add(resSeg.getBundledFarePeriodId());

					if (!bundledServicePnrSegIdMap.containsKey(resSeg.getBundledFarePeriodId())) {
						bundledServicePnrSegIdMap.put(resSeg.getBundledFarePeriodId(), new HashSet<Integer>());
					}

					Set<Integer> pnrSegIds = bundledServicePnrSegIdMap.get(resSeg.getBundledFarePeriodId());
					pnrSegIds.add(resSeg.getPnrSegId());
				}

			}

			if (!selectedBundledServicePeriodIds.isEmpty()) {
				List<BundledFareDTO> bundledFareDTOs = ReservationModuleUtils.getBundledFareBD()
						.getBundledFareDTOsByBundlePeriodIds(selectedBundledServicePeriodIds);
				if (bundledFareDTOs != null && !bundledFareDTOs.isEmpty()) {
					for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
						Integer bundledFarePeriodId = bundledFareDTO.getBundledFarePeriodId();
						if (bundledServicePnrSegIdMap.containsKey(bundledFarePeriodId)) {
							Set<Integer> pnrSegIds = bundledServicePnrSegIdMap.get(bundledFarePeriodId);
							if (pnrSegIds != null && !pnrSegIds.isEmpty()) {
								for (Integer pnrSegId : pnrSegIds) {
									pnrSegWiseSelectedBundles.put(pnrSegId, bundledFareDTO);
								}
							}
						}
					}
				}
			}
		}

		return pnrSegWiseSelectedBundles;
	}

	private static Map<Integer, String> getChargeRateChargeCodeMapping(Reservation res) {
		Map<Integer, String> chargeRateCodeMapping = new HashMap<Integer, String>();

		for (ReservationPax resPax : res.getPassengers()) {
			Collection<ChargesDetailDTO> ondChargesDetailDTOs = resPax.getOndChargesView();
			for (ChargesDetailDTO chargesDetailDTO : ondChargesDetailDTOs) {
				chargeRateCodeMapping.put(chargesDetailDTO.getChargeRateId(), chargesDetailDTO.getChargeCode());
			}
		}

		return chargeRateCodeMapping;
	}
}