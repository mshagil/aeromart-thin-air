package com.isa.thinair.airreservation.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareIdDetailDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.SortUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.ReturnValidityPeriodTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class OnholdRollForwardUtil {

	public static void populateFlightSegmentOnFare(List<ReservationSegmentDTO> reservationSegmentDTOs,
			List<ReservationSegmentDTO> segmentFareList, Map<Integer, List<ReservationSegmentDTO>> connectionFareMap,
			Map<Integer, List<ReservationSegmentDTO>> returnFareMap) throws ModuleException {
		if (reservationSegmentDTOs != null) {
			for (ReservationSegmentDTO outerObj : reservationSegmentDTOs) {
				boolean isReturn = false;
				boolean isConnection = false;
				for (ReservationSegmentDTO innerObj : reservationSegmentDTOs) {
					if (outerObj.getFlightSegId().intValue() != innerObj.getFlightSegId().intValue()
							&& outerObj.getSegmentSeq().intValue() != innerObj.getSegmentSeq().intValue()) {
						if (outerObj.getFareGroupId().intValue() == innerObj.getFareGroupId()) {
							isConnection = true;
							break;
						} else if (outerObj.getReturnOndGroupId().intValue() == innerObj.getReturnOndGroupId().intValue()) {
							isReturn = true;
							break;
						}
					}
				}

				if (isConnection) {
					if (!connectionFareMap.containsKey(outerObj.getFareGroupId())) {
						connectionFareMap.put(outerObj.getFareGroupId(), new ArrayList<ReservationSegmentDTO>());
					}
					connectionFareMap.get(outerObj.getFareGroupId()).add(outerObj);
				} else if (isReturn) {
					if (!returnFareMap.containsKey(outerObj.getReturnOndGroupId())) {
						returnFareMap.put(outerObj.getReturnOndGroupId(), new ArrayList<ReservationSegmentDTO>());
					}
					returnFareMap.get(outerObj.getReturnOndGroupId()).add(outerObj);
				} else {
					segmentFareList.add(outerObj);

				}
			}
		}
	}

	public static void populateAvailableFlightSearchDTO(List<LinkedHashMap<Integer, RollForwardFlightDTO>> inboundOutboundList,
			int availabilityRestriction, AvailableFlightSearchDTO availableFlightSearchDTO) {

		List<RollForwardFlightDTO> inboundOutboundFlts = new ArrayList<RollForwardFlightDTO>();
		Map<Integer, RollForwardFlightDTO> allFltMap = new HashMap<Integer, RollForwardFlightDTO>();
		for (LinkedHashMap<Integer, RollForwardFlightDTO> inboundOutboundMap : inboundOutboundList) {
			inboundOutboundFlts.addAll(inboundOutboundMap.values());
			for (Entry<Integer, RollForwardFlightDTO> entry : inboundOutboundMap.entrySet()) {
				allFltMap.put(entry.getKey(), entry.getValue());
			}
		}

		for (RollForwardFlightDTO rollForwardFlightDTO : inboundOutboundFlts) {
			if (rollForwardFlightDTO.isOpenReturn() && inboundOutboundFlts.size() > 0) {
				Integer validityPeriod = roundUpToValidityPeriod(rollForwardFlightDTO, inboundOutboundFlts.get(0));
				availableFlightSearchDTO.setReturnValidityPeriodId(validityPeriod);
			}
		}
		Collections.sort(inboundOutboundFlts);

		RollForwardFlightDTO firstOutSeg = BeanUtils.getFirstElement(inboundOutboundFlts);
		availableFlightSearchDTO.setBookingClassCode(firstOutSeg.getBookingClass());
		availableFlightSearchDTO.setFlightsPerOndRestriction(availabilityRestriction);
		setClassOfServiceSelection(availableFlightSearchDTO, inboundOutboundFlts);

		Map<Integer, FareIdDetailDTO> flightSegFareDetails = new HashMap<Integer, FareIdDetailDTO>();
		availableFlightSearchDTO.setFlightSegFareDetails(flightSegFareDetails);

		for (Entry<Integer, RollForwardFlightDTO> entry : allFltMap.entrySet()) {
			FareIdDetailDTO fareIdDetailDTO = new FareIdDetailDTO();
			RollForwardFlightDTO rfDTO = entry.getValue();
			fareIdDetailDTO.setFareRuleId(rfDTO.getFareRuleId());
			fareIdDetailDTO.setFareId(rfDTO.getFareId());
			fareIdDetailDTO.setCabinClassCode(rfDTO.getCabinClass());
			fareIdDetailDTO.setBookingClassCode(rfDTO.getBookingClass());
			flightSegFareDetails.put(entry.getKey(), fareIdDetailDTO);
		}

		availableFlightSearchDTO.getOriginDestinationInfoDTOs().clear();
		for (LinkedHashMap<Integer, RollForwardFlightDTO> inboundOutboundMap : inboundOutboundList) {
			RollForwardFlightDTO firstOutboundSeg = BeanUtils.getFirstElement(inboundOutboundMap.values());
			OriginDestinationInfoDTO ondInfoDTO = new OriginDestinationInfoDTO();
			ondInfoDTO.setOrigin(firstOutboundSeg.getSegmentCode().split("/")[0]);
			RollForwardFlightDTO lastOutboundSeg = (RollForwardFlightDTO) BeanUtils.getLastElement(inboundOutboundMap.values());
			String paramValues[] = lastOutboundSeg.getSegmentCode().split("/");
			ondInfoDTO.setDestination(paramValues[paramValues.length - 1]);
			Date prefDTStart = CalendarUtil.getStartTimeOfDate(firstOutboundSeg.getDepartureDateTimeZulu());
			Date prefDTEnd = CalendarUtil.getEndTimeOfDate(firstOutboundSeg.getDepartureDateTimeZulu());
			// if (prefDTStart.before(ondInfoTO.getDepartureDateTimeStart())) {
			// prefDTStart = ondInfoTO.getDepartureDateTimeEnd();
			// }
			ondInfoDTO.setPreferredDateTimeStart(prefDTStart);
			ondInfoDTO.setPreferredDateTimeEnd(prefDTEnd);
			ondInfoDTO.setDepartureDateTimeStart(prefDTStart);
			ondInfoDTO.setDepartureDateTimeEnd(prefDTEnd);
			// ondInfoDTO.setArrivalDateTimeStart(prefDTStart);
			// ondInfoDTO.setArrivalDateTimeEnd(CalendarUtil.add(prefDTEnd, 0, 0, 1, 0, 0, 0));

			List<Integer> segIds = new ArrayList<Integer>();
			segIds.addAll(allFltMap.keySet());
			ondInfoDTO.setFlightSegmentIds(segIds);
			ondInfoDTO.setPreferredClassOfService(firstOutboundSeg.getCabinClass());
			ondInfoDTO.setPreferredLogicalCabin(firstOutboundSeg.getCabinClass());
			ondInfoDTO.setPreferredBookingType(availableFlightSearchDTO.getBookingType());
			ondInfoDTO.setOpenOnd(firstOutboundSeg.isOpenReturn());

			availableFlightSearchDTO.addOriginDestination(ondInfoDTO);
		}
		if (inboundOutboundList.size() > 1) {
			availableFlightSearchDTO.setReturnFlag(true);
		}
	}

	private static Integer roundUpToValidityPeriod(RollForwardFlightDTO openReturnFlightDTO,
			RollForwardFlightDTO firstOutboundFlightDTO) {
		int differenceInDays = CalendarUtil.getTimeDifferenceInDays(firstOutboundFlightDTO.getDepartureDateTime(),
				openReturnFlightDTO.getDepartureDateTime());
		Map<Integer, ReturnValidityPeriodTO> returnValidityPeriods = ReservationModuleUtils.getGlobalConfig()
				.getReturnValidityPeriods();
		List<ReturnValidityPeriodTO> returnValidityPeriodTO = new ArrayList<ReturnValidityPeriodTO>(
				returnValidityPeriods.values());
		Collections.sort(returnValidityPeriodTO, new Comparator<ReturnValidityPeriodTO>() {
			@Override
			public int compare(ReturnValidityPeriodTO o1, ReturnValidityPeriodTO o2) {
				return o1.getValidityPeriod() - o2.getValidityPeriod();
			}
		});
		for (ReturnValidityPeriodTO returnValidityPeriod : returnValidityPeriods.values()) {
			if (differenceInDays <= returnValidityPeriod.getValidityPeriod() * 30) {
				return returnValidityPeriod.getValidityPeriod();
			}
		}
		return null;
	}

	private static void setClassOfServiceSelection(AvailableFlightSearchDTO availableFlightSearchDTO,
			List<RollForwardFlightDTO> allFlts) {
		Map<String, List<Integer>> cabinClassSelection = availableFlightSearchDTO.getCabinClassSelection();
		for (RollForwardFlightDTO rollForwardFlightDTO : allFlts) {
			if (!cabinClassSelection.containsKey(rollForwardFlightDTO.getCabinClass())) {
				cabinClassSelection.put(rollForwardFlightDTO.getCabinClass(), new ArrayList<Integer>());
			}
			cabinClassSelection.get(rollForwardFlightDTO.getCabinClass()).add(rollForwardFlightDTO.getFlightSegId());
		}
	}

	public static List<LinkedHashMap<Integer, RollForwardFlightDTO>> getOutboundInboundFlights(
			List<RollForwardFlightDTO> rollForwardFlightDTOs) {
		List<LinkedHashMap<Integer, RollForwardFlightDTO>> inboundOutboundList = new ArrayList<LinkedHashMap<Integer, RollForwardFlightDTO>>();

		Map<Integer, Integer> processedOrderMap = new HashMap<Integer, Integer>();
		if (rollForwardFlightDTOs != null) {
			for (RollForwardFlightDTO rollfwdDTO : rollForwardFlightDTOs) {
				rollfwdDTO.setSegmentSeq(rollfwdDTO.getSegmentSeq() - 1);
			}
			int index = 0;
			for (RollForwardFlightDTO dto : rollForwardFlightDTOs) {
				if (inboundOutboundList.size() < dto.getSegmentSeq() + 1) {
					inboundOutboundList.add(new LinkedHashMap<Integer, RollForwardFlightDTO>());
				}

				if (processedOrderMap.get(dto.getSegmentSeq()) == null) {
					processedOrderMap.put(dto.getSegmentSeq(), index);
					index++;
				}
				inboundOutboundList.get(processedOrderMap.get(dto.getSegmentSeq())).put(dto.getFlightSegId(), dto);
			}
		}
		return inboundOutboundList;
	}

	public static List<RollForwardFlightDTO> FilterMatchingRollForwardingFlights(
			List<RollForwardFlightDTO> allRollForwardFlightDTOs, List<ReservationSegmentDTO> reservationSegmentDTOs) {
		List<RollForwardFlightDTO> filteredFlights = new ArrayList<RollForwardFlightDTO>();

		for (ReservationSegmentDTO resSegDTO : reservationSegmentDTOs) {
			for (RollForwardFlightDTO rollFltDTO : allRollForwardFlightDTOs) {
				if (resSegDTO.getSegmentCode().equals(rollFltDTO.getSegmentCode())
						&& resSegDTO.getSegmentSeq().intValue() == rollFltDTO.getSegmentSeq()) {
					filteredFlights.add(rollFltDTO);
					break;
				}
			}
		}
		Collections.sort(filteredFlights);
		return filteredFlights;
	}

	public static Collection<OndFareDTO> getOndFareDTOs(List<SelectedFlightDTO> selectedFlightDTOs) {
		Collection<OndFareDTO> colOndFares = new ArrayList<OndFareDTO>();
		for (SelectedFlightDTO selectedFlightDTO : selectedFlightDTOs) {
			Collection<OndFareDTO> tempOndFares = selectedFlightDTO.getOndFareDTOs(false);
			if (tempOndFares != null && tempOndFares.size() > 0) {
				colOndFares.addAll(tempOndFares);
			}
		}

		return colOndFares;
	}

	public static IReservation getReservationAssembler(Collection<OndFareDTO> ondFareDTOs, Reservation sourceReservation,
			List<RollForwardFlightDTO> rollForwardFlightDTOs, Date onholdReleaseTime, Boolean isDuplicateNameSkip)
			throws ModuleException {

		setFareGroupingIds(sourceReservation.getSegmentsView(), rollForwardFlightDTOs);

		IReservation iReservation = null;
		iReservation = new ReservationAssembler(ondFareDTOs, null);
		iReservation.setBookingCategory(sourceReservation.getBookingCategory());
		iReservation.setOriginCountryOfCall(sourceReservation.getOriginCountryOfCall());
		iReservation.setItineraryFareMask(sourceReservation.getItineraryFareMaskFlag());

		iReservation.setChargesApplicability(ChargeRateOperationType.MAKE_ONLY);
		iReservation.setSkipDuplicateCheck(isDuplicateNameSkip);
		ReservationContactInfo contactInfo = sourceReservation.getContactInfo();

		iReservation.addContactInfo(sourceReservation.getUserNote(), contactInfo.clone(), null);
		populatePaxInformation(iReservation, sourceReservation.getPassengers());
		iReservation.setLastCurrencyCode(sourceReservation.getLastCurrencyCode());

		Map<Integer, String> groundStationBySegmentMap = new LinkedHashMap<Integer, String>();
		Map<Integer, Integer> groundSementFlightByAirFlightMap = new LinkedHashMap<Integer, Integer>();

		mapStationsBySegment(sourceReservation.getSegmentsView(), groundStationBySegmentMap, groundSementFlightByAirFlightMap,
				rollForwardFlightDTOs);

		iReservation.setPnrZuluReleaseTimeStamp(onholdReleaseTime);

		populateOndSegments(iReservation, ondFareDTOs, groundStationBySegmentMap, groundSementFlightByAirFlightMap,
				rollForwardFlightDTOs);

		// iReservation.setTicketValidity(ondFareDTOs);

		return iReservation;
	}

	public static void overrideBookingType(Collection<OndFareDTO> ondFareDTOs, String bookingType) {
		if (bookingType.equals(BookingClass.BookingClassType.OVERBOOK)) {
			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				for (SegmentSeatDistsDTO segmentSeatDistsDTO : ondFareDTO.getSegmentSeatDistsDTO()) {
					for (SeatDistribution seatDistribution : segmentSeatDistsDTO.getSeatDistribution()) {
						seatDistribution.setBookingClassType(BookingClass.BookingClassType.OVERBOOK);
					}
				}
			}
		}
	}

	private static void populatePaxInformation(IReservation iReservation, Set<ReservationPax> passengers) throws ModuleException {
		if (passengers != null && passengers.size() > 0) {
			for (ReservationPax resPax : (Set<ReservationPax>) passengers) {
				SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();

				PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
				String arrivalIntlFltNo = "";
				Date intlFltArrivalDate = null;
				String departureIntlFltNo = "";
				Date intlFltDepartureDate = null;
				String pnrPaxGroupId = "";

				if (resPax.getPaxAdditionalInfo() != null) {
					paxAdditionalInfoDTO.setPassportNo(resPax.getPaxAdditionalInfo().getPassportNo());
					paxAdditionalInfoDTO.setPassportExpiry(resPax.getPaxAdditionalInfo().getPassportExpiry());
					paxAdditionalInfoDTO.setPassportIssuedCntry(resPax.getPaxAdditionalInfo().getPassportIssuedCntry());
					paxAdditionalInfoDTO.setEmployeeId(resPax.getPaxAdditionalInfo().getEmployeeID());
					paxAdditionalInfoDTO.setDateOfJoin(resPax.getPaxAdditionalInfo().getDateOfJoin());
					paxAdditionalInfoDTO.setIdCategory(resPax.getPaxAdditionalInfo().getIdCategory());
					paxAdditionalInfoDTO.setPlaceOfBirth(resPax.getPaxAdditionalInfo().getPlaceOfBirth());
					paxAdditionalInfoDTO.setTravelDocumentType(resPax.getPaxAdditionalInfo().getTravelDocumentType());
					paxAdditionalInfoDTO.setVisaDocNumber(resPax.getPaxAdditionalInfo().getVisaDocNumber());
					paxAdditionalInfoDTO.setVisaDocPlaceOfIssue(resPax.getPaxAdditionalInfo().getVisaDocPlaceOfIssue());
					paxAdditionalInfoDTO.setVisaDocIssueDate(resPax.getPaxAdditionalInfo().getVisaDocIssueDate());
					paxAdditionalInfoDTO.setVisaApplicableCountry(resPax.getPaxAdditionalInfo().getVisaApplicableCountry());
					paxAdditionalInfoDTO.setFfid(resPax.getPaxAdditionalInfo().getFfid());
					intlFltArrivalDate = resPax.getPaxAdditionalInfo().getIntlFlightArrivalDate();
					departureIntlFltNo = resPax.getPaxAdditionalInfo().getDepartureIntlFlightNo();
					intlFltDepartureDate = resPax.getPaxAdditionalInfo().getIntlFlightDepartureDate();
					pnrPaxGroupId = resPax.getPaxAdditionalInfo().getGroupId();

				}

				if (ReservationInternalConstants.PassengerType.ADULT.equals(resPax.getPaxType())) {
					if (resPax.getAccompaniedSequence() != null && resPax.getAccompaniedSequence() > 0) {
						iReservation.addParent(resPax.getFirstName(), resPax.getLastName(), resPax.getTitle(),
								resPax.getDateOfBirth(), resPax.getNationalityCode(), resPax.getPaxSequence(),
								resPax.getAccompaniedSequence(), paxAdditionalInfoDTO, resPax.getPaxCategory(),
								new PaymentAssembler(), segmentSSRs, null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo,
								intlFltDepartureDate, pnrPaxGroupId);
					} else {
						iReservation.addSingle(resPax.getFirstName(), resPax.getLastName(), resPax.getTitle(),
								resPax.getDateOfBirth(), resPax.getNationalityCode(), resPax.getPaxSequence(),
								paxAdditionalInfoDTO, resPax.getPaxCategory(), new PaymentAssembler(), segmentSSRs, null, null,
								null, null, CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo,
								intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
					}
				} else if (ReservationInternalConstants.PassengerType.CHILD.equals(resPax.getPaxType())) {
					iReservation.addChild(resPax.getFirstName(), resPax.getLastName(), resPax.getTitle(),
							resPax.getDateOfBirth(), resPax.getNationalityCode(), resPax.getPaxSequence(), paxAdditionalInfoDTO,
							resPax.getPaxCategory(), new PaymentAssembler(), segmentSSRs, null, null, null, null,
							CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo,
							intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
				} else if (ReservationInternalConstants.PassengerType.INFANT.equals(resPax.getPaxType())) {
					iReservation.addInfant(resPax.getFirstName(), resPax.getLastName(), resPax.getTitle(),
							resPax.getDateOfBirth(), resPax.getNationalityCode(), resPax.getPaxSequence(),
							resPax.getAccompaniedSequence(), paxAdditionalInfoDTO, resPax.getPaxCategory(),
							new PaymentAssembler(), segmentSSRs, null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo,
							intlFltDepartureDate, pnrPaxGroupId);
				}
			}
		}
	}

	private static void mapStationsBySegment(Collection<ReservationSegmentDTO> segments, Map<Integer, String> segmentMap,
			Map<Integer, Integer> groundSegmentByAirFlightSegmentID, List<RollForwardFlightDTO> rollForwardFlightDTOs)
			throws ModuleException {
		if (!AppSysParamsUtil.isGroundServiceEnabled()) {
			return;
		}

		if (segmentMap == null) {
			segmentMap = new LinkedHashMap<Integer, String>();
		}
		if (groundSegmentByAirFlightSegmentID == null) {
			groundSegmentByAirFlightSegmentID = new LinkedHashMap<Integer, Integer>();
		}

		// FlightReF No : SegmentCode
		Map<Integer, String> airSegments = new LinkedHashMap<Integer, String>();
		// Airport Code of the Parent Ground Segment Connection
		Map<String, List<Integer>> groundSegments = new LinkedHashMap<String, List<Integer>>();
		boolean isGroundSegment = false;
		// Ordering Segment Sequence. For Local use
		Set<ReservationSegmentDTO> outBoundSegs = new LinkedHashSet<ReservationSegmentDTO>();
		Set<ReservationSegmentDTO> orderedSegSetReturn = new LinkedHashSet<ReservationSegmentDTO>();
		Set<ReservationSegmentDTO> orderedSegments = new LinkedHashSet<ReservationSegmentDTO>();

		for (ReservationSegmentDTO resSeg : segments) {
			if (ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(resSeg.getReturnFlag())) {
				orderedSegSetReturn.add(resSeg);
			} else {
				outBoundSegs.add(resSeg);
			}
		}

		orderedSegments.addAll(outBoundSegs);
		orderedSegments.addAll(orderedSegSetReturn);
		ReservationSegmentDTO[] sortedSegmentArray = SortUtil.sortByDepDate(orderedSegments);
		
		for (ReservationSegmentDTO reservationSegment : sortedSegmentArray) {
			isGroundSegment = false;

			RollForwardFlightDTO rollForwardFlightDTO = getRollForwardingSegInfo(reservationSegment, rollForwardFlightDTOs);

			if (rollForwardFlightDTO == null) {
				throw new ModuleException("airreservations.ohd.rollfoward.fail");
			}

			List<String> airportList = getAirportCollection(rollForwardFlightDTO.getSegmentCode());
			if (reservationSegment.getSubStationShortName() != null
					&& isContainGroundSegment(ReservationModuleUtils.getAirportBD().getCachedOwnAirportMap(airportList))) {
				String groundStation = null;
				if (reservationSegment.getSubStationShortName() != null) {
					groundStation = reservationSegment.getSubStationShortName();
				} else {
					groundStation = returnGroundSegment(ReservationModuleUtils.getAirportBD().getCachedOwnAirportMap(airportList));
				}

				segmentMap.put(rollForwardFlightDTO.getFlightSegId(), groundStation);
				isGroundSegment = true;
				List<Integer> existing = groundSegments.get(groundStation);

				if (existing == null) {
					existing = new ArrayList<Integer>();
					if (groundStation != null) {
						groundSegments.put(groundStation, existing);
					}
				}
				existing.add(rollForwardFlightDTO.getFlightSegId());
			}
			if (!isGroundSegment) {
				airSegments.put(rollForwardFlightDTO.getFlightSegId(), rollForwardFlightDTO.getSegmentCode());
			}
		}

		for (Entry<Integer, String> airSegmentMapEntry : airSegments.entrySet()) {
			for (Entry<String, List<Integer>> groundSegMapEntry : groundSegments.entrySet()) {
				if (SubStationUtil.isPrimaryStationBelongingToGroundStationPresent(airSegmentMapEntry.getValue(),
						groundSegMapEntry.getKey())) {
					List<Integer> existing = groundSegMapEntry.getValue();
					if (existing != null && existing.size() > 0) {
						groundSegmentByAirFlightSegmentID.put(existing.remove(0), airSegmentMapEntry.getKey());
					}
					break;
				}
			}
		}

	}

	private static boolean isContainGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (CachedAirportDTO airport : airports) {
			if (airport.isSurfaceSegment()) {
				return true;
			}
		}
		return false;
	}

	private static List<String> getAirportCollection(String segCode) {
		List<String> airports = Arrays.asList(segCode.split("/"));
		return airports;
	}

	private static String returnGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (CachedAirportDTO airport : airports) {
			if (airport.isSurfaceSegment()) {
				return airport.getAirportCode();
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static void populateOndSegments(IReservation iReservation, Collection<OndFareDTO> ondFareDTOs,
			Map<Integer, String> groundStationBySegmentMap, Map<Integer, Integer> linkedGroundSegmentFlightRefMap,
			List<RollForwardFlightDTO> rollForwardFlightDTOs) throws ModuleException {

		Object[] inOutboundSegments = extractNewResSegmentsInfo(ondFareDTOs, rollForwardFlightDTOs);

		Collection<ReservationSegmentDTO> outboundSegments = (Collection<ReservationSegmentDTO>) inOutboundSegments[0];
		Collection<ReservationSegmentDTO> inboundSegments = (Collection<ReservationSegmentDTO>) inOutboundSegments[1];

		Date openRTConfirmExpiry = OndFareUtil.getOpenReturnPeriods(ondFareDTOs).getConfirmBefore();

		if (outboundSegments != null && outboundSegments.size() > 0) {
			for (ReservationSegmentDTO reservationSegmentDTO : outboundSegments) {
				iReservation.addOndSegment(reservationSegmentDTO.getJourneySeq(), reservationSegmentDTO.getSegmentSeq(),
						reservationSegmentDTO.getFlightSegId(), reservationSegmentDTO.getFareGroupId(),
						reservationSegmentDTO.getReturnOndGroupId(), openRTConfirmExpiry, null, null, false, false,
						reservationSegmentDTO.getBundledFarePeriodId(), null, null, reservationSegmentDTO.getCsOcCarrierCode(),
						reservationSegmentDTO.getCsOcFlightNumber());
				iReservation.addGroundStationDataToPNRSegment(groundStationBySegmentMap, linkedGroundSegmentFlightRefMap);
			}
		}

		if (inboundSegments != null && inboundSegments.size() > 0) {
			for (ReservationSegmentDTO reservationSegmentDTO : inboundSegments) {
				iReservation.addOndSegment(reservationSegmentDTO.getJourneySeq(), reservationSegmentDTO.getSegmentSeq(),
						reservationSegmentDTO.getFlightSegId(), reservationSegmentDTO.getFareGroupId(),
						reservationSegmentDTO.getReturnOndGroupId(), openRTConfirmExpiry, null, null, false, true,
						reservationSegmentDTO.getBundledFarePeriodId(), null, null, reservationSegmentDTO.getCsOcCarrierCode(),
						reservationSegmentDTO.getCsOcFlightNumber());
				iReservation.addGroundStationDataToPNRSegment(groundStationBySegmentMap, linkedGroundSegmentFlightRefMap);
			}
			
		}
	}

	private static Object[] extractNewResSegmentsInfo(Collection<OndFareDTO> segsSeatsAndFaresCol,
			List<RollForwardFlightDTO> rollForwardFlightDTOs) throws ModuleException {

		Collection<ReservationSegmentDTO> obFlightSegments = new LinkedList<ReservationSegmentDTO>();
		Collection<ReservationSegmentDTO> ibFlightSegments = null;

		int ondSequence = 1;
		for (OndFareDTO ondFareDTO : segsSeatsAndFaresCol) {
			ondSequence = ondFareDTO.getOndSequence();
			for (FlightSegmentDTO flightSegmentDTO : ondFareDTO.getSegmentsMap().values()) {
				RollForwardFlightDTO rollForwardFlightDTO = getRollForwardingSegInfo(flightSegmentDTO, rollForwardFlightDTOs);

				if (rollForwardFlightDTO == null) {
					throw new ModuleException("airreservations.ohd.rollfoward.fail");
				}

				if (!ondFareDTO.isInBoundOnd()) {
					obFlightSegments.add(new ReservationSegmentDTO(flightSegmentDTO.getSegmentId(), rollForwardFlightDTO
							.getSegmentSeq() + 1, rollForwardFlightDTO.getFareGroupId(), rollForwardFlightDTO
							.getReturnOndGroupId(), ondSequence + 1));
				} else {
					if (ibFlightSegments == null) {
						ibFlightSegments = new LinkedList<ReservationSegmentDTO>();
					}
					ibFlightSegments.add(new ReservationSegmentDTO(flightSegmentDTO.getSegmentId(), rollForwardFlightDTO
							.getSegmentSeq() + 1, rollForwardFlightDTO.getFareGroupId(), rollForwardFlightDTO
							.getReturnOndGroupId(), ondSequence + 1));
				}
			}

		}

		return new Object[] { obFlightSegments, ibFlightSegments };
	}

	private static RollForwardFlightDTO getRollForwardingSegInfo(ReservationSegmentDTO resSegment,
			List<RollForwardFlightDTO> rollForwardFlightDTOs) {
		for (RollForwardFlightDTO rollForwardFlightDTO : rollForwardFlightDTOs) {
			if (rollForwardFlightDTO.getSegmentSeq() + 1 == resSegment.getSegmentSeq().intValue()
					&& rollForwardFlightDTO.getSegmentCode().equals(resSegment.getSegmentCode())) {
				return rollForwardFlightDTO;
			}
		}

		return null;
	}

	private static RollForwardFlightDTO getRollForwardingSegInfo(FlightSegmentDTO flightSegmentDTO,
			List<RollForwardFlightDTO> rollForwardFlightDTOs) {
		for (RollForwardFlightDTO rollForwardFlightDTO : rollForwardFlightDTOs) {
			if (rollForwardFlightDTO.getFlightSegId() == flightSegmentDTO.getSegmentId().intValue()
					&& rollForwardFlightDTO.getSegmentCode().equals(flightSegmentDTO.getSegmentCode())) {
				return rollForwardFlightDTO;
			}
		}

		return null;
	}

	private static void setFareGroupingIds(Collection<ReservationSegmentDTO> segments,
			List<RollForwardFlightDTO> rollForwardFlightDTOs) {
		if (segments != null) {
			for (ReservationSegmentDTO reservationSegmentDTO : segments) {
				RollForwardFlightDTO rollForwardFlightDTO = getRollForwardingSegInfo(reservationSegmentDTO, rollForwardFlightDTOs);
				if (rollForwardFlightDTO != null) {
					rollForwardFlightDTO.setFareGroupId(reservationSegmentDTO.getFareGroupId());
					try {
						rollForwardFlightDTO.setReturnGroupId(reservationSegmentDTO.getReturnGroupId());
					} catch (ModuleException e) {
						// If return group null set as default
					}
					rollForwardFlightDTO.setReturnOndGroupId(reservationSegmentDTO.getReturnOndGroupId());
				}
			}
		}
	}
}
