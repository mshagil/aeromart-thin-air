package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.PaxChargesTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.InsuranceSegExtChgDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;

/**
 * Command for calculate promotion & fare discount. For all existing API we need to make sure promotion or fare discount
 * is calculated only from this class. This will keep the calculation logic only in one central location & accuracy is
 * guaranteed through out all reservation flows.
 * 
 * @author M.Rikaz
 * 
 * @isa.module.command name="discountCalculator"
 */

public class DiscountCalculator extends DefaultBaseCommand {
	private static Log log = LogFactory.getLog(DiscountCalculator.class);
	private BigDecimal totalDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private Map<Integer, PaxDiscountDetailTO> paxDiscountDetails = null;
	private ReservationDiscountDTO reservationDiscountTO = new ReservationDiscountDTO();
	private Map<Integer, List<ExternalChgDTO>> paxExternalChargesMap = null;

	@Override
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside DiscountCalculator");

		DiscountRQ discountCalcRQ = (DiscountRQ) this.getParameter(CommandParamNames.PROMO_CALCULATOR_RQ);

		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		Reservation reservation = this.getParameter(CommandParamNames.RESERVATION, Reservation.class);

		validateParams(discountCalcRQ);

		DiscountedFareDetails discFareDetailsDTO = discountCalcRQ.getDiscountInfoDTO();
		Collection<PaxChargesTO> paxExtChargesColl = discountCalcRQ.getPaxChargesList();
		Collection<PassengerTypeQuantityTO> paxQtyList = discountCalcRQ.getPaxQtyList();
		boolean validateCriteria = discountCalcRQ.isValidateCriteria();

		DISCOUNT_METHOD actionType = discountCalcRQ.getActionType();

		if (paxQtyList != null && !paxQtyList.isEmpty() && (paxExtChargesColl == null || paxExtChargesColl.isEmpty())) {
			paxExtChargesColl = generateDummyPaxChargesTOs(paxQtyList);

			if (paxExtChargesColl == null || paxExtChargesColl.isEmpty()) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}
		}
		List<PaxChargesTO> paxExtChargesList = new ArrayList<PaxChargesTO>(paxExtChargesColl);
		Collections.sort(paxExtChargesList);
		
		if (discFareDetailsDTO != null) {

			Collection<OndFareDTO> ondFareDTOs = getQuotedOndFareDTOs(discountCalcRQ);

			if (ondFareDTOs == null || ondFareDTOs.isEmpty()) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}		
			
			if (discFareDetailsDTO.getPromotionId() == null) {

				if (DISCOUNT_METHOD.FARE_DISCOUNT == actionType) {

					reservationDiscountTO.setDiscountCode(ChargeCodes.FARE_DISCOUNT);
					calculateFareDiscount(ondFareDTOs, discFareDetailsDTO, paxExtChargesList, false);

				} else if (DISCOUNT_METHOD.DOM_FARE_DISCOUNT == actionType) {

					reservationDiscountTO.setDiscountCode(ChargeCodes.DOM_FARE_DISCOUNT);
					calculateFareDiscount(ondFareDTOs, discFareDetailsDTO, paxExtChargesList, true);

				} else {
					log.debug("Fare Discount method is not defined ");
				}
				reservationDiscountTO.setDiscountAs(DiscountApplyAsTypes.MONEY);
			} else {
				
				if (reservation != null) {
					this.paxExternalChargesMap = getPaxExternalChargesMap(reservation.getPassengers(), discFareDetailsDTO);
				}				

				if (validateCriteria) {

					if (reservation == null || credentialsDTO == null) {
						throw new ModuleException("airreservations.arg.invalid.null");
					}
					Set<ReservationPax> paxSet = reservation.getPassengers();
					List<ReservationPax> paxList = new ArrayList<ReservationPax>(paxSet);
					Collections.sort(paxList);

					// Validate promotion
					validateAppliedPromotion(discFareDetailsDTO, credentialsDTO, ondFareDTOs, paxList);

				}
				reservationDiscountTO.setDiscountAs(discFareDetailsDTO.getDiscountAs());
				calculateChargesPromotionDiscounts(ondFareDTOs, discFareDetailsDTO, paxExtChargesList);

				reservationDiscountTO.setPromotionId(discFareDetailsDTO.getPromotionId());

			}

		}

		reservationDiscountTO.setTotalDiscount(totalDiscountAmount);
		reservationDiscountTO.setPaxDiscountDetails(paxDiscountDetails);

		if (!discountCalcRQ.isCalculateTotalOnly()) {
			splitInfantDiscount(reservation);
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.USED_PROMOTION_CREDITS, totalDiscountAmount);
		response.addResponceParam(CommandParamNames.RESERVATION_DISCOUNT_DTO, reservationDiscountTO);
		log.info(reservationDiscountTO.toString());

		log.debug("Exit DiscountCalculator");
		return response;
	}

	private void calculateFareDiscount(Collection<OndFareDTO> ondFareDTOs, DiscountedFareDetails discFareDetailsDTO,
			Collection<PaxChargesTO> paxExtChargesList, boolean isDomesticFareDiscount) throws ModuleException {

		this.paxDiscountDetails = new HashMap<Integer, PaxDiscountDetailTO>();
		Collection<OndFareDTO> tmpOndFareDTOs = new ArrayList<OndFareDTO>(ondFareDTOs);

		Iterator<OndFareDTO> itrOndFareDTOS = tmpOndFareDTOs.iterator();

		while (itrOndFareDTOS.hasNext()) {
			if (itrOndFareDTOS.next().getFareSummaryDTO().getBookingClassCode().equals("BUS")) {
				itrOndFareDTOS.remove();
			}
		}

		BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		float discountPercentage = discFareDetailsDTO.getFarePercentage();
		Set<String> applicableOnds = discFareDetailsDTO.getApplicableOnds();
		String discountType = PromotionCriteriaConstants.DiscountTypes.PERCENTAGE;

		Map<String, Integer> applicablePaxCount = null;

		applicablePaxCount = new HashMap<String, Integer>();
		applicablePaxCount.put(PaxTypeTO.ADULT, 0);
		applicablePaxCount.put(PaxTypeTO.CHILD, 0);
		applicablePaxCount.put(PaxTypeTO.INFANT, 0);

		List<String> discountEnabledPaxType = AppSysParamsUtil.fareDiscountEnabledPaxTypes();

		if (isDomesticFareDiscount) {
			Integer domesticFareDiscPerc = AppSysParamsUtil.getDomesticFareDiscountPercentage();
			if (domesticFareDiscPerc == null || discFareDetailsDTO.getDomesticSegmentCodeList() == null) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}

			applicableOnds = new HashSet<String>(discFareDetailsDTO.getDomesticSegmentCodeList());
			discountPercentage = domesticFareDiscPerc;
		}

		if (discountPercentage > 0) {
			reservationDiscountTO.setDiscountPercentage(discountPercentage);
			for (PaxChargesTO reservationPaxTO : paxExtChargesList) {
				if (discountEnabledPaxType.contains(PaxTypeTO.ADULT) && PaxTypeTO.ADULT.equals(reservationPaxTO.getPaxTypeCode())) {
					applicablePaxCount.put(PaxTypeTO.ADULT, applicablePaxCount.get(PaxTypeTO.ADULT) + 1);
					if (reservationPaxTO.isParent() && discountEnabledPaxType.contains(PaxTypeTO.INFANT)) {
						applicablePaxCount.put(PaxTypeTO.INFANT, applicablePaxCount.get(PaxTypeTO.INFANT) + 1);
					}
				} else if (discountEnabledPaxType.contains(PaxTypeTO.CHILD)
						&& PaxTypeTO.CHILD.equals(reservationPaxTO.getPaxTypeCode())) {
					applicablePaxCount.put(PaxTypeTO.CHILD, applicablePaxCount.get(PaxTypeTO.CHILD) + 1);
				}
			}

			for (OndFareDTO ondFareDTO : tmpOndFareDTOs) {

				reservationDiscountTO.addSegmentCode(ondFareDTO.getFlightSegmentIds(), ondFareDTO.getOndCode());

				if (isDomesticFareDiscount && !StringUtil.isMatchingStringFound(applicableOnds, ondFareDTO.getOndCode())) {
					continue;
				}

				int adultItr = 0, childItr = 0, infantItr = 0;
				BigDecimal totAdult = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totChild = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totInfant = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (PaxChargesTO reservationPax : paxExtChargesList) {
					PaxDiscountDetailTO discChargeGroupTO = null;
					if (paxDiscountDetails.get(reservationPax.getPaxSequence()) == null) {
						discChargeGroupTO = new PaxDiscountDetailTO();
						discChargeGroupTO.setPaxSequence(reservationPax.getPaxSequence());
						paxDiscountDetails.put(reservationPax.getPaxSequence(), discChargeGroupTO);
						discChargeGroupTO.setPaxType(reservationPax.getPaxTypeCode());
					}

					discChargeGroupTO = paxDiscountDetails.get(reservationPax.getPaxSequence());

					if (PaxTypeTO.ADULT.equals(reservationPax.getPaxTypeCode())) {

						if (adultItr < applicablePaxCount.get(PaxTypeTO.ADULT)) {

							BigDecimal adultDiscount = applyPromotionToFare(ondFareDTO, discountPercentage, PaxTypeTO.ADULT,
									discountType, discChargeGroupTO)[1];

							totAdult = AccelAeroCalculator.add(totAdult, adultDiscount);

							adultItr++;
						}

						if (reservationPax.isParent()) {

							if (infantItr < applicablePaxCount.get(PaxTypeTO.INFANT)) {

								BigDecimal infantDiscount = applyPromotionToFare(ondFareDTO, discountPercentage,
										PaxTypeTO.INFANT, discountType, discChargeGroupTO)[1];

								totInfant = AccelAeroCalculator.add(totInfant, infantDiscount);

								infantItr++;
							}

						}

					} else if (PaxTypeTO.CHILD.equals(reservationPax.getPaxTypeCode())) {
						if (childItr < applicablePaxCount.get(PaxTypeTO.CHILD)) {

							BigDecimal childDiscount = applyPromotionToFare(ondFareDTO, discountPercentage, PaxTypeTO.CHILD,
									discountType, discChargeGroupTO)[1];

							totChild = AccelAeroCalculator.add(totChild, childDiscount);
							childItr++;
						}
					}

				}

				totalDiscount = AccelAeroCalculator.add(totalDiscount, totAdult, totChild, totInfant);
				totalDiscountAmount = AccelAeroCalculator.add(totalDiscountAmount, totAdult, totChild, totInfant);

			}
		}
	}

	private void calculateChargesPromotionDiscounts(Collection<OndFareDTO> ondFareDTOs, DiscountedFareDetails discFareDetailsDTO,
			Collection<PaxChargesTO> paxExtChargesList) throws ModuleException {
		this.paxDiscountDetails = new HashMap<Integer, PaxDiscountDetailTO>();
		Collection<OndFareDTO> tmpOndFareDTOs = new ArrayList<OndFareDTO>(ondFareDTOs);

		Iterator<OndFareDTO> itrOndFareDTOS = tmpOndFareDTOs.iterator();

		while (itrOndFareDTOS.hasNext()) {
			if (itrOndFareDTOS.next().getFareSummaryDTO().getBookingClassCode().equals("BUS")) {
				itrOndFareDTOS.remove();
			}
		}

		BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		int promoPaxCount = 0;
		Set<String> applicableOnds = discFareDetailsDTO.getApplicableOnds();
		int discountableOndCount = getDiscountApplicableOndCount(tmpOndFareDTOs, applicableOnds);
		int actualOndCount = tmpOndFareDTOs.size();
		String promoType = discFareDetailsDTO.getPromotionType();
		String discountType = discFareDetailsDTO.getDiscountType();

		int payablePaxCount = 0;
		if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(promoType)) {			
			for (PaxChargesTO pax : paxExtChargesList) {
				if (!pax.getPaxTypeCode().equals(PaxTypeTO.INFANT)) {
					payablePaxCount++;
				}
			}
		} else {
			for (PaxChargesTO paxChargesTO : paxExtChargesList) {
				payablePaxCount++;
				if (paxChargesTO.isParent()) {
					payablePaxCount++;
				}
			}
		}

		BigDecimal[] effectiveDiscAmountArr = PromotionsUtils.getEffectivePaxDiscountValueWithoutLoss(
				discFareDetailsDTO.getFarePercentage(), discFareDetailsDTO.getDiscountApplyTo(),
				discFareDetailsDTO.getDiscountType(), payablePaxCount, discountableOndCount);

		float effectiveDiscountValue;

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(promoType)) {
			Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
			List<String> applicableAncis = discFareDetailsDTO.getApplicableAncillaries();
			Set<Integer> paxSeqSet = new HashSet<Integer>();
			reservationDiscountTO.setDiscountCode(ChargeCodes.FREE_SERVICE_PROMO);
			if (applicableAncis != null && !applicableAncis.isEmpty()) {

				Map<Integer, BigDecimal> consumedDiscount = new HashMap<Integer, BigDecimal>();
				if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
					effectiveDiscAmountArr = PromotionsUtils.getPaxWiseTotalDiscount(effectiveDiscAmountArr, payablePaxCount);
				}

				int ondCount = 0;
				for (OndFareDTO ondFareDTO : tmpOndFareDTOs) {
					int count = 0;
					reservationDiscountTO.addSegmentCode(ondFareDTO.getFlightSegmentIds(), ondFareDTO.getOndCode());
					if (!StringUtil.isMatchingStringFound(applicableOnds, ondFareDTO.getOndCode())) {
						continue;
					}

					Map<String, BigDecimal> paxTypeWiseTot = new HashMap<String, BigDecimal>();
					Set<Integer> fltSegIds = ondFareDTO.getSegmentsMap().keySet();

					for (PaxChargesTO reservationPax : paxExtChargesList) {
						PaxDiscountDetailTO discChargeGroupTO = null;
						if (paxDiscountDetails.get(reservationPax.getPaxSequence()) == null) {
							discChargeGroupTO = new PaxDiscountDetailTO();
							discChargeGroupTO.setPaxSequence(reservationPax.getPaxSequence());
							paxDiscountDetails.put(reservationPax.getPaxSequence(), discChargeGroupTO);
							discChargeGroupTO.setPaxType(reservationPax.getPaxTypeCode());
						}

						discChargeGroupTO = paxDiscountDetails.get(reservationPax.getPaxSequence());

						if (!paxTypeWiseTot.containsKey(reservationPax.getPaxTypeCode())) {
							paxTypeWiseTot.put(reservationPax.getPaxTypeCode(), AccelAeroCalculator.getDefaultBigDecimalZero());
						}

						if (!consumedDiscount.containsKey(reservationPax.getPaxSequence())
								|| consumedDiscount.get(reservationPax.getPaxSequence()) == null) {
							consumedDiscount.put(reservationPax.getPaxSequence(), AccelAeroCalculator.getDefaultBigDecimalZero());
						}

						BigDecimal paxAnciTotalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						BigDecimal paxAnciDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

						List<ExternalChgDTO> paxExtChgs = null;

						if (this.paxExternalChargesMap != null && !this.paxExternalChargesMap.isEmpty()
								&& paxExternalChargesMap.get(reservationPax.getPaxSequence()) != null) {
							paxExtChgs = paxExternalChargesMap.get(reservationPax.getPaxSequence());
						} else if (reservationPax.getExtChgList() != null && !reservationPax.getExtChgList().isEmpty()) {
							paxExtChgs = ReservationApiUtils.populateExternalCharges(reservationPax.getExtChgList());
						}

						List<ExternalChgDTO> extChgs = null;
						if (paxExtChgs != null && !paxExtChgs.isEmpty()) {
							extChgs = getApplicableExternalCharges(applicableAncis, paxExtChgs, fltSegIds);
						}

						if (extChgs != null && !extChgs.isEmpty()) {							
							// guaranteed always discount is calculated on specified order
							List<EXTERNAL_CHARGES> processOrder = new ArrayList<EXTERNAL_CHARGES>();
							processOrder.add(EXTERNAL_CHARGES.BAGGAGE);
							processOrder.add(EXTERNAL_CHARGES.MEAL);
							processOrder.add(EXTERNAL_CHARGES.SEAT_MAP);
							processOrder.add(EXTERNAL_CHARGES.AIRPORT_SERVICE);
							processOrder.add(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
							processOrder.add(EXTERNAL_CHARGES.INSURANCE);
							
							paxSeqSet.add(reservationPax.getPaxSequence());

							BigDecimal paxConsumedDiscAmount = consumedDiscount.get(reservationPax.getPaxSequence());

							if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
								effectiveDiscountValue = (effectiveDiscAmountArr[0]).floatValue();
							} else {
								BigDecimal totPaxDiscountAmount = effectiveDiscAmountArr[count];
								BigDecimal remainingDiscountAmount = AccelAeroCalculator.subtract(totPaxDiscountAmount,
										paxConsumedDiscAmount);
								if (!AccelAeroCalculator.isGreaterThan(remainingDiscountAmount,
										AccelAeroCalculator.getDefaultBigDecimalZero())) {
									count++;
									continue;
								}
								effectiveDiscountValue = remainingDiscountAmount.floatValue();
							}

							float effectiveDiscountValueTemp = effectiveDiscountValue;
							
							for (EXTERNAL_CHARGES extChargEnum : processOrder) {

								for (ExternalChgDTO externalChgDTO : extChgs) {									
									
									if (extChargEnum == getExternalChargeEnum(externalChgDTO)) {

										BigDecimal chgAmount = externalChgDTO.getAmount();
										String chargeCode = ReservationApiUtils.getChargeCode(externalChgDTO);
										String externalChargeCode = getExternalChargeCode(externalChgDTO, externalChargesMap);

										if ((externalChgDTO instanceof InsuranceSegExtChgDTO)
												|| (externalChgDTO.getExternalChargesEnum() == EXTERNAL_CHARGES.INSURANCE)) {
											BigDecimal[] splittedInsChgArr = AccelAeroCalculator.roundAndSplit(chgAmount,
													actualOndCount);
											chgAmount = splittedInsChgArr[ondCount];
										}

										paxAnciTotalAmount = AccelAeroCalculator.add(paxAnciTotalAmount, chgAmount);

										BigDecimal chgDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
										if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
											chgDiscount = AccelAeroCalculator.calculatePercentage(chgAmount,
													effectiveDiscountValue, RoundingMode.UP);
										} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
											BigDecimal offeredDiscount = new BigDecimal(
													new Float(effectiveDiscountValueTemp).toString());
											if (AccelAeroCalculator.isGreaterThan(offeredDiscount, chgAmount)) {
												effectiveDiscountValueTemp = AccelAeroCalculator.subtract(offeredDiscount,
														chgAmount).floatValue();
												chgDiscount = chgAmount;
											} else {
												chgDiscount = offeredDiscount;
												effectiveDiscountValueTemp = 0.0f;
											}
										}

										if (chgDiscount.doubleValue() != 0) {
											discChargeGroupTO.addDiscountChargeTO(chargeCode,
													ReservationInternalConstants.ChargeGroup.SUR, ondFareDTO.getOndCode(),
													chgDiscount, false, externalChgDTO.getChgRateId(),
													ondFareDTO.getFlightSegmentIds(), externalChargeCode);
											totalDiscountAmount = AccelAeroCalculator.add(totalDiscountAmount, chgDiscount);
										}

									}
								}
							}

							if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
								paxAnciDiscount = AccelAeroCalculator.calculatePercentage(paxAnciTotalAmount,
										effectiveDiscountValue, RoundingMode.UP);
							} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
								BigDecimal offeredDiscount = new BigDecimal(new Float(effectiveDiscountValue).toString());
								if (AccelAeroCalculator.isGreaterThan(offeredDiscount, paxAnciTotalAmount)) {
									paxAnciDiscount = paxAnciTotalAmount;
								} else {
									paxAnciDiscount = offeredDiscount;
								}

								consumedDiscount.put(reservationPax.getPaxSequence(),
										AccelAeroCalculator.add(paxAnciDiscount, paxConsumedDiscAmount));
							}

							paxDiscountDetails.put(reservationPax.getPaxSequence(), discChargeGroupTO);

							paxTypeWiseTot
									.put(reservationPax.getPaxTypeCode(),
											AccelAeroCalculator.add(paxAnciDiscount,
													paxTypeWiseTot.get(reservationPax.getPaxTypeCode())));
						}
						count++;
					}

					ondCount++;
				}

				promoPaxCount = paxSeqSet.size();
			}

		} else {
			String chargeCode = null;
			Map<String, Integer> applicablePaxCount = null;

			if (PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET.equals(promoType)) {
				chargeCode = ChargeCodes.BUYANDGET_PROMO;
				applicablePaxCount = discFareDetailsDTO.getApplicablePaxCount();
			} else if (PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT.equals(promoType)) {
				chargeCode = ChargeCodes.DISCOUNT_PROMO;
				applicablePaxCount = new HashMap<String, Integer>();
				applicablePaxCount.put(PaxTypeTO.ADULT, 0);
				applicablePaxCount.put(PaxTypeTO.CHILD, 0);
				applicablePaxCount.put(PaxTypeTO.INFANT, 0);

				for (PaxChargesTO reservationPaxTO : paxExtChargesList) {
					if (PaxTypeTO.ADULT.equals(reservationPaxTO.getPaxTypeCode())) {
						applicablePaxCount.put(PaxTypeTO.ADULT, applicablePaxCount.get(PaxTypeTO.ADULT) + 1);
						if (reservationPaxTO.isParent()) {
							applicablePaxCount.put(PaxTypeTO.INFANT, applicablePaxCount.get(PaxTypeTO.INFANT) + 1);
						}
					} else if (PaxTypeTO.CHILD.equals(reservationPaxTO.getPaxTypeCode())) {
						applicablePaxCount.put(PaxTypeTO.CHILD, applicablePaxCount.get(PaxTypeTO.CHILD) + 1);
					}
				}
			}
			reservationDiscountTO.setDiscountCode(chargeCode);
			promoPaxCount = applicablePaxCount.get(PaxTypeTO.ADULT) + applicablePaxCount.get(PaxTypeTO.CHILD);
			int count = 0;
			for (OndFareDTO ondFareDTO : tmpOndFareDTOs) {
				reservationDiscountTO.addSegmentCode(ondFareDTO.getFlightSegmentIds(), ondFareDTO.getOndCode());
				if (!StringUtil.isMatchingStringFound(applicableOnds, ondFareDTO.getOndCode())) {
					continue;
				}

				int adultItr = 0, childItr = 0, infantItr = 0;
				BigDecimal totAdult = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totChild = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totInfant = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (PaxChargesTO reservationPax : paxExtChargesList) {
					PaxDiscountDetailTO discChargeGroupTO = null;
					if (paxDiscountDetails.get(reservationPax.getPaxSequence()) == null) {
						discChargeGroupTO = new PaxDiscountDetailTO();
						discChargeGroupTO.setPaxSequence(reservationPax.getPaxSequence());
						paxDiscountDetails.put(reservationPax.getPaxSequence(), discChargeGroupTO);
						discChargeGroupTO.setPaxType(reservationPax.getPaxTypeCode());
					}

					discChargeGroupTO = paxDiscountDetails.get(reservationPax.getPaxSequence());

					if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
						effectiveDiscountValue = (effectiveDiscAmountArr[0]).floatValue();
					} else {
						effectiveDiscountValue = (effectiveDiscAmountArr[count]).floatValue();
					}

					if (PaxTypeTO.ADULT.equals(reservationPax.getPaxTypeCode())) {

						if (adultItr < applicablePaxCount.get(PaxTypeTO.ADULT)) {

							BigDecimal adultDiscount = calculatePaxPromotionDiscount(ondFareDTO, PaxTypeTO.ADULT,
									effectiveDiscountValue, discFareDetailsDTO.getDiscountType(),
									discFareDetailsDTO.getDiscountApplyTo(), chargeCode, discChargeGroupTO);

							totAdult = AccelAeroCalculator.add(totAdult, adultDiscount);

							adultItr++;
						}

						if (reservationPax.isParent()) {

							if (infantItr < applicablePaxCount.get(PaxTypeTO.INFANT)) {
								count++;

								if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
									effectiveDiscountValue = (effectiveDiscAmountArr[0]).floatValue();
								} else {
									effectiveDiscountValue = (effectiveDiscAmountArr[count]).floatValue();
								}

								BigDecimal infantDiscount = calculatePaxPromotionDiscount(ondFareDTO, PaxTypeTO.INFANT,
										effectiveDiscountValue, discFareDetailsDTO.getDiscountType(),
										discFareDetailsDTO.getDiscountApplyTo(), chargeCode, discChargeGroupTO);

								totInfant = AccelAeroCalculator.add(totInfant, infantDiscount);

								infantItr++;
							}

						}

					} else if (PaxTypeTO.CHILD.equals(reservationPax.getPaxTypeCode())) {
						if (childItr < applicablePaxCount.get(PaxTypeTO.CHILD)) {

							BigDecimal childDiscount = calculatePaxPromotionDiscount(ondFareDTO, PaxTypeTO.CHILD,
									effectiveDiscountValue, discFareDetailsDTO.getDiscountType(),
									discFareDetailsDTO.getDiscountApplyTo(), chargeCode, discChargeGroupTO);
							totChild = AccelAeroCalculator.add(totChild, childDiscount);
							childItr++;
						}
					}

					count++;
				}

				totalDiscount = AccelAeroCalculator.add(totalDiscount, totAdult, totChild, totInfant);
				totalDiscountAmount = AccelAeroCalculator.add(totalDiscountAmount, totAdult, totChild, totInfant);

			}

		}
		reservationDiscountTO.setPromoPaxCount(promoPaxCount);
	}

	private void validateAppliedPromotion(DiscountedFareDetails discountedFareDetails, CredentialsDTO credentialsDTO,
			Collection<OndFareDTO> ondFareDTOs, List<ReservationPax> paxList) throws ModuleException {

		Set<Integer> fltSegIds = new HashSet<Integer>();
		Set<Integer> flightIds = new HashSet<Integer>();

		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			LinkedHashMap<Integer, FlightSegmentDTO> segMap = ondFareDTO.getSegmentsMap();
			if (segMap != null && !segMap.isEmpty()) {
				for (Entry<Integer, FlightSegmentDTO> segEntry : segMap.entrySet()) {
					FlightSegmentDTO flightSegmentDTO = segEntry.getValue();
					flightIds.add(flightSegmentDTO.getFlightId());
					fltSegIds.add(segEntry.getKey());
				}
			}

		}

		fltSegIds = PromotionsUtils.getFlightSegIdsWithInterceptingSegIds(fltSegIds);

		int paxCount = 0;
		for (ReservationPax reservationPax : paxList) {
			if (ReservationApiUtils.isChildType(reservationPax) || ReservationApiUtils.isAdultType(reservationPax)) {
				paxCount++;
			}
		}

		int promoAppliedPaxCount = paxCount;
		if (PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET.equals(discountedFareDetails.getPromotionType())) {
			Map<String, Integer> promoPaxTypeCount = discountedFareDetails.getApplicablePaxCount();
			promoAppliedPaxCount = promoPaxTypeCount.get(PaxTypeTO.ADULT) + promoPaxTypeCount.get(PaxTypeTO.CHILD);
		}

		ReservationModuleUtils.getPromotionManagementBD().checkPromotionConstraints(discountedFareDetails.getPromotionId(),
				new ArrayList<Integer>(flightIds), new ArrayList<Integer>(fltSegIds), credentialsDTO.getAgentCode(),
				credentialsDTO.getSalesChannelCode(), paxCount, promoAppliedPaxCount);

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT.equals(discountedFareDetails.getPromotionType())
				&& discountedFareDetails.isAllowSamePaxOnly() && discountedFareDetails.getOriginPnr() != null
				&& !"".equals(discountedFareDetails.getOriginPnr())) {
			// Load system generated promotion originated reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(discountedFareDetails.getOriginPnr());
			Reservation originRes = ReservationProxy.getReservation(pnrModesDTO);
			Set<ReservationPax> originResPaxs = originRes.getPassengers();
			for (ReservationPax reservationPax : paxList) {
				if (!checkPaxExists(originResPaxs, reservationPax)) {
					throw new ModuleException("promotion.pax.not.matching");
				}
			}

		}
	}

	private boolean checkPaxExists(Set<ReservationPax> originResPaxs, ReservationPax currentPax) {
		for (ReservationPax originResPax : originResPaxs) {
			String originPaxFName = BeanUtils.nullHandler(originResPax.getFirstName());
			String originPaxLName = BeanUtils.nullHandler(originResPax.getLastName());
			String currentPaxFName = BeanUtils.nullHandler(currentPax.getFirstName());
			String currentPaxLName = BeanUtils.nullHandler(currentPax.getLastName());
			if (originPaxFName.equalsIgnoreCase(currentPaxFName) && originPaxLName.equalsIgnoreCase(currentPaxLName)) {
				return true;
			}
		}
		return false;
	}

	private int getDiscountApplicableOndCount(Collection<OndFareDTO> ondFareDTOs, Set<String> applicableOnds)
			throws ModuleException {

		int ondCount = 0;
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			if (StringUtil.isMatchingStringFound(applicableOnds, ondFareDTO.getOndCode())) {

				ondCount++;
			}
		}

		return ondCount;
	}

	public BigDecimal calculatePaxPromotionDiscount(OndFareDTO ondFareDTO, String paxType, float discountValue,
			String discountType, int discountApplyTo, String chargeCode, PaxDiscountDetailTO discChargeGroupTO)
			throws ModuleException {

		BigDecimal fareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal chargesDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
			if (discountApplyTo == PromotionCriteriaConstants.DiscountApplyTo.FARE.ordinal()) {
				fareDiscount = applyPromotionToFare(ondFareDTO, discountValue, paxType, discountType, discChargeGroupTO)[1];
			} else if (discountApplyTo == PromotionCriteriaConstants.DiscountApplyTo.FARE_SURCHARGE.ordinal()) {

				fareDiscount = applyPromotionToFare(ondFareDTO, discountValue, paxType, discountType, discChargeGroupTO)[1];

				chargesDiscount = applyPromotionToCharges(ondFareDTO, discountValue, paxType, discountType, discountApplyTo,
						discChargeGroupTO);

			} else if (discountApplyTo == PromotionCriteriaConstants.DiscountApplyTo.TOTAL.ordinal()) {

				fareDiscount = applyPromotionToFare(ondFareDTO, discountValue, paxType, discountType, discChargeGroupTO)[1];

				chargesDiscount = applyPromotionToCharges(ondFareDTO, discountValue, paxType, discountType, discountApplyTo,
						discChargeGroupTO);
			}

		} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {

			BigDecimal[] response = applyPromotionToFare(ondFareDTO, discountValue, paxType, discountType, discChargeGroupTO);

			fareDiscount = response[1];

			chargesDiscount = applyPromotionToCharges(ondFareDTO, response[0].floatValue(), paxType, discountType,
					discountApplyTo, discChargeGroupTO);
		}

		BigDecimal totalDiscount = AccelAeroCalculator.add(fareDiscount, chargesDiscount);

		return totalDiscount;
	}

	private BigDecimal[] applyPromotionToFare(OndFareDTO ondFareDTO, float fareDiscountPercentage, String paxType,
			String discountType, PaxDiscountDetailTO discChargeGroupTO) throws ModuleException {

		BigDecimal balance = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal paxDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (ondFareDTO != null) {

			BigDecimal paxChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			boolean isInfant = false;

			if (PaxTypeTO.ADULT.equals(paxType)) {
				paxChargeAmount = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare());
			} else if (PaxTypeTO.CHILD.equals(paxType)) {
				paxChargeAmount = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare());				
			} else if (PaxTypeTO.INFANT.equals(paxType)) {
				paxChargeAmount = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare());				
				isInfant = true;
			}

			if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
				paxDiscountAmount = AccelAeroCalculator.calculatePercentage(paxChargeAmount, fareDiscountPercentage,
						RoundingMode.DOWN);
			} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
				Float fltDiscountVal = fareDiscountPercentage;
				BigDecimal offeredDiscount = new BigDecimal(fltDiscountVal.toString());
				// BigDecimal totalPrice = new BigDecimal(paxChargeAmount);
				if (AccelAeroCalculator.isGreaterThan(offeredDiscount, paxChargeAmount)) {
					balance = AccelAeroCalculator.subtract(offeredDiscount, paxChargeAmount);
					paxDiscountAmount = paxChargeAmount;
				} else {
					paxDiscountAmount = offeredDiscount;
					fareDiscountPercentage = 0.0f;
				}

			}

			if (discChargeGroupTO != null && paxDiscountAmount.doubleValue() != 0) {
				discChargeGroupTO.addDiscountChargeTO(ReservationInternalConstants.ChargeGroup.FAR,
						ReservationInternalConstants.ChargeGroup.FAR, ondFareDTO.getOndCode(), paxDiscountAmount, isInfant,
						ondFareDTO.getFareId(), ondFareDTO.getFlightSegmentIds(), null);
			}

		}
		BigDecimal[] response = { balance, paxDiscountAmount };

		return response;
	}

	private BigDecimal applyPromotionToCharges(OndFareDTO ondFareDTO, float fareDiscountPercentage, String paxType,
			String discountType, int discountApplyTo, PaxDiscountDetailTO discChargeGroupTO) throws ModuleException {
		Collection<QuotedChargeDTO> allCharges = ondFareDTO.getAllCharges();
		BigDecimal appliedDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (allCharges != null) {
			boolean isInfant = false;
			if (PaxTypeTO.INFANT.equals(paxType)) {
				isInfant = true;
			}

			Iterator<QuotedChargeDTO> allChargesItr = allCharges.iterator();
			while (allChargesItr.hasNext()) {
				QuotedChargeDTO quotedChargeDTO = allChargesItr.next();
				boolean isValidCharge = false;

				if (PassengerType.ADULT.equals(paxType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						isValidCharge = true;

					}
				} else if (PassengerType.CHILD.equals(paxType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						isValidCharge = true;

					}
				} else if (PassengerType.INFANT.equals(paxType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT) {
						isValidCharge = true;

					}
				}

				if (!isValidCharge) {
					// this charge is not applicable for selected PAX Type.
					continue;
				}

				if (discountApplyTo == PromotionCriteriaConstants.DiscountApplyTo.FARE_SURCHARGE.ordinal()) {
					if (PaxTypeTO.INFANT.equals(paxType)) {
						if (!ChargeGroups.SURCHARGE.equals(quotedChargeDTO.getChargeGroupCode())
								&& !ChargeGroups.INFANT_SURCHARGE.equals(quotedChargeDTO.getChargeGroupCode())) {
							continue;
						}
					} else if (!ChargeGroups.SURCHARGE.equals(quotedChargeDTO.getChargeGroupCode())) {
						continue;
					}
				}

				if (discountApplyTo == PromotionCriteriaConstants.DiscountApplyTo.TOTAL.ordinal()
						&& (!PaxTypeTO.INFANT.equals(paxType) && ChargeGroups.INFANT_SURCHARGE.equals(quotedChargeDTO
								.getChargeGroupCode()))) {
					continue;
				}

				BigDecimal paxDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal paxChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
					paxChargeAmount = AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(paxType));					 
				} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
					paxChargeAmount = ReservationApiUtils.getApplicableChargeAmount(paxType, quotedChargeDTO);
				}

				if (paxChargeAmount.doubleValue() > 0) {
					if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
						paxDiscountAmount = AccelAeroCalculator.calculatePercentage(paxChargeAmount, fareDiscountPercentage,
								RoundingMode.DOWN);
					} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
						Float fltDiscountVal = fareDiscountPercentage;
						BigDecimal offeredDiscount = new BigDecimal(fltDiscountVal.toString());
						BigDecimal totalPrice = paxChargeAmount;
						if (AccelAeroCalculator.isGreaterThan(offeredDiscount, totalPrice)) {
							fareDiscountPercentage = AccelAeroCalculator.subtract(offeredDiscount, totalPrice).floatValue();
							paxDiscountAmount = paxChargeAmount;
						} else {
							paxDiscountAmount = offeredDiscount;
							fareDiscountPercentage = 0.0f;
						}

					}

					if (discChargeGroupTO != null && paxDiscountAmount.doubleValue() != 0) {
						discChargeGroupTO.addDiscountChargeTO(quotedChargeDTO.getChargeCode(),
								quotedChargeDTO.getChargeGroupCode(), ondFareDTO.getOndCode(), paxDiscountAmount, isInfant,
								quotedChargeDTO.getChargeRateId(), ondFareDTO.getFlightSegmentIds(), null);
					}
					appliedDiscount = AccelAeroCalculator.add(appliedDiscount, paxDiscountAmount);
				}

			}

		}
		return appliedDiscount;
	}

	private List<ExternalChgDTO> getApplicableExternalCharges(List<String> applicableAncis, Collection<ExternalChgDTO> extChgs,
			Set<Integer> fltSegIds) {
		List<ExternalChgDTO> applicableExtChgs = new ArrayList<ExternalChgDTO>();

		if (extChgs != null && !extChgs.isEmpty() && applicableAncis != null && !applicableAncis.isEmpty()) {
			for (ExternalChgDTO externalChgDTO : extChgs) {
				Integer fltSegId = null;
				boolean anciApplicable = false;
				if (externalChgDTO instanceof SMExternalChgDTO
						&& isApplicableService(externalChgDTO, applicableAncis, EXTERNAL_CHARGES.SEAT_MAP.toString())) {
					fltSegId = ((SMExternalChgDTO) externalChgDTO).getFlightSegId();
					anciApplicable = fltSegIds.contains(fltSegId);
				} else if (externalChgDTO instanceof MealExternalChgDTO
						&& isApplicableService(externalChgDTO, applicableAncis, EXTERNAL_CHARGES.MEAL.toString())) {
					fltSegId = ((MealExternalChgDTO) externalChgDTO).getFlightSegId();
					anciApplicable = fltSegIds.contains(fltSegId);
				} else if (externalChgDTO instanceof BaggageExternalChgDTO
						&& isApplicableService(externalChgDTO, applicableAncis, EXTERNAL_CHARGES.BAGGAGE.toString())) {
					fltSegId = ((BaggageExternalChgDTO) externalChgDTO).getFlightSegId();
					anciApplicable = fltSegIds.contains(fltSegId);
				} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
					SSRExternalChargeDTO ssrExtChg = (SSRExternalChargeDTO) externalChgDTO;
					if (isApplicableService(externalChgDTO, applicableAncis, null)) {

						fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(ssrExtChg.getFlightRPH());
						anciApplicable = fltSegIds.contains(fltSegId);
					}
				} else if ((externalChgDTO instanceof InsuranceSegExtChgDTO || (externalChgDTO.getExternalChargesEnum() != null && EXTERNAL_CHARGES.INSURANCE == externalChgDTO
						.getExternalChargesEnum()))
						&& isApplicableService(externalChgDTO, applicableAncis, EXTERNAL_CHARGES.INSURANCE.toString())) {

					anciApplicable = true;
				}

				if (anciApplicable) {
					applicableExtChgs.add(externalChgDTO);
				}

			}
		}

		return applicableExtChgs;
	}

	private boolean isApplicableService(ExternalChgDTO externalChgDTO, List<String> applicableAncis, String externalCharge) {
		boolean anciApplicable = false;
		if (externalChgDTO != null && applicableAncis != null && !applicableAncis.isEmpty()) {
			if (externalChgDTO.getExternalChargesEnum() != null
					&& applicableAncis.contains(externalChgDTO.getExternalChargesEnum().toString())) {
				anciApplicable = true;
			} else if (externalCharge != null && applicableAncis.contains(externalCharge)) {
				anciApplicable = true;
			} else if (externalChgDTO.getChargeCode() != null && "SSR".equalsIgnoreCase(externalChgDTO.getChargeCode())
					&& applicableAncis.contains(EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString())) {
				anciApplicable = true;
			} else if (externalChgDTO.getChargeCode() != null && "HALA".equalsIgnoreCase(externalChgDTO.getChargeCode())
					&& applicableAncis.contains(EXTERNAL_CHARGES.AIRPORT_SERVICE.toString())) {
				anciApplicable = true;
			}
		}

		return anciApplicable;
	}

	private Map<Integer, Integer> getInfantMappingByPaxSequence(List<ReservationPax> paxList) {
		Map<Integer, Integer> infantMapByPaxSequence = new HashMap<Integer, Integer>();
		if (paxList != null && !paxList.isEmpty()) {
			for (ReservationPax pax : paxList) {
				if (PaxTypeTO.ADULT.equals(pax.getPaxType())) {
					Integer infantSeq = pax.getIndexId();
					if (infantSeq == null && pax.getInfants() != null && !pax.getInfants().isEmpty()) {
						INFANT: for (ReservationPax infant : pax.getInfants()) {
							infantSeq = infant.getPaxSequence();
							break INFANT;
						}
					}
					infantMapByPaxSequence.put(pax.getPaxSequence(), infantSeq);
				}
			}
		}

		return infantMapByPaxSequence;
	}

	private void splitInfantDiscount(Reservation reservation) {

		Map<Integer, PaxDiscountDetailTO> updatePaxDiscountDetails = new HashMap<Integer, PaxDiscountDetailTO>();
		boolean isInfantSplitted = false;

		Map<Integer, Integer> infantMapByPaxSequence = null;
		Map<Integer, PaxDiscountDetailTO> paxDiscountDetails = null;
		if (reservation != null && reservation.getPassengers() != null && reservationDiscountTO != null) {
			Set<ReservationPax> paxSet = reservation.getPassengers();
			List<ReservationPax> paxList = new ArrayList<ReservationPax>(paxSet);
			Collections.sort(paxList);

			infantMapByPaxSequence = getInfantMappingByPaxSequence(paxList);
			paxDiscountDetails = reservationDiscountTO.getPaxDiscountDetails();

			if (paxDiscountDetails != null && !paxDiscountDetails.isEmpty() && infantMapByPaxSequence != null
					&& !infantMapByPaxSequence.isEmpty()) {

				for (Integer paxSequence : paxDiscountDetails.keySet()) {
					PaxDiscountDetailTO discountDetailTO = paxDiscountDetails.get(paxSequence);
					if (discountDetailTO.isInfantChargesExist() && infantMapByPaxSequence.get(paxSequence) != null) {
						Integer infantPaxSequence = infantMapByPaxSequence.get(paxSequence);
						if (infantPaxSequence != null && infantPaxSequence.intValue() > 0) {
							isInfantSplitted = true;
							updatePaxDiscountDetails.put(infantPaxSequence, discountDetailTO.splitInfant(infantPaxSequence));
						}
					}

					updatePaxDiscountDetails.put(paxSequence, discountDetailTO);

				}
				if (isInfantSplitted) {
					reservationDiscountTO.setPaxDiscountDetails(updatePaxDiscountDetails);
				}

			}

		}

	}

	private void validateParams(DiscountRQ discountCalcRQ) throws ModuleException {

		log.debug("Inside validateParams");
		if (discountCalcRQ == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		Collection<PaxChargesTO> paxExtChargesList = discountCalcRQ.getPaxChargesList();
		Collection<PassengerTypeQuantityTO> paxQtyList = discountCalcRQ.getPaxQtyList();
		boolean inValidRequest = false;

		if (discountCalcRQ.getActionType() == null || discountCalcRQ.getDiscountInfoDTO() == null) {
			inValidRequest = true;
		} else if ((paxExtChargesList == null || paxExtChargesList.isEmpty()) && (paxQtyList == null || paxQtyList.isEmpty())) {
			inValidRequest = true;
		} else if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(discountCalcRQ.getDiscountInfoDTO()
				.getPromotionType()) && (paxExtChargesList == null || paxExtChargesList.isEmpty())) {
			inValidRequest = true;
		} else if (!discountCalcRQ.isCalculateTotalOnly() && (paxExtChargesList == null || paxExtChargesList.isEmpty())) {
			inValidRequest = true;
		}
		if (inValidRequest) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	private Collection<OndFareDTO> getQuotedOndFareDTOs(DiscountRQ discountCalcRQ) throws ModuleException {
		QuotedFareRebuildDTO quotedFareRebuildDTO = null;
		Collection<OndFareDTO> ondFareDTOs = discountCalcRQ.getOndFareDTOs();
		if ((ondFareDTOs == null || ondFareDTOs.isEmpty()) && discountCalcRQ.getQuotedFareRebuildDTO() != null) {
			quotedFareRebuildDTO = discountCalcRQ.getQuotedFareRebuildDTO();
			ondFareDTOs = new ArrayList<OndFareDTO>();
			for (OndRebuildCriteria ondRebuildCriteria : quotedFareRebuildDTO.getOndRebuildCriterias()) {
				ondFareDTOs.addAll(ReservationModuleUtils.getReservationQueryBD().recreateFareSegCharges(ondRebuildCriteria));

			}
		}
		return ondFareDTOs;
	}

	public static Map<Integer, List<ExternalChgDTO>> getPaxExternalChargesMap(Collection<ReservationPax> paxCollection,
			DiscountedFareDetails discFareDetailsDTO) {

		Map<Integer, List<ExternalChgDTO>> paxExternalChargesMap = new HashMap<Integer, List<ExternalChgDTO>>();
		if (paxCollection != null && !paxCollection.isEmpty()) {
			PaymentAssembler paymentAssembler;
			for (ReservationPax resPaxTO : paxCollection) {

				if (PaxTypeTO.INFANT.equals(resPaxTO.getPaxType())) {
					continue;
				}

				if (resPaxTO.getPayment() != null) {
					paymentAssembler = (PaymentAssembler) resPaxTO.getPayment();
					if (paymentAssembler != null) {
						List<String> applicableAncis = discFareDetailsDTO.getApplicableAncillaries();
						List<ExternalChgDTO> extChgs = new ArrayList<ExternalChgDTO>();
						if (applicableAncis != null && !applicableAncis.isEmpty()) {
							for (String anci : applicableAncis) {
								Collection<ExternalChgDTO> tmpExternalChgDTO = paymentAssembler
										.getPerPaxExternalCharges(EXTERNAL_CHARGES.valueOf(anci));
								if (tmpExternalChgDTO != null && !tmpExternalChgDTO.isEmpty()) {
									extChgs.addAll(tmpExternalChgDTO);
								}

							}

						}

						if (!extChgs.isEmpty())
							paxExternalChargesMap.put(resPaxTO.getPaxSequence(), extChgs);
					}

				}

			}
		}
		return paxExternalChargesMap;
	}

	private static Collection<PaxChargesTO> generateDummyPaxChargesTOs(Collection<PassengerTypeQuantityTO> paxQtyList) {
		Collection<PaxChargesTO> paxChargesTempList = null;
		if (paxQtyList != null && !paxQtyList.isEmpty()) {
			int paxSequence = 1;
			paxChargesTempList = new ArrayList<PaxChargesTO>();
			int infantPaxCount = 0;
			for (PassengerTypeQuantityTO paxTypeQty : paxQtyList) {
				if (PaxTypeTO.INFANT.equals(paxTypeQty.getPassengerType())) {
					infantPaxCount = paxTypeQty.getQuantity();
					break;
				}
			}

			for (PassengerTypeQuantityTO paxTypeQty : paxQtyList) {
				Integer paxCount = paxTypeQty.getQuantity();
				String paxType = paxTypeQty.getPassengerType();

				if (!PaxTypeTO.INFANT.equals(paxTypeQty.getPassengerType())) {
					for (int i = 0; i < paxCount; i++) {
						PaxChargesTO paxChargesTO = new PaxChargesTO();
						paxChargesTO.setPaxTypeCode(paxType);
						paxChargesTO.setPaxSequence(paxSequence);
						if (PaxTypeTO.ADULT.equals(paxType) && infantPaxCount > 0) {
							paxChargesTO.setParent(true);
							infantPaxCount--;
						}
						paxChargesTempList.add(paxChargesTO);
						paxSequence++;
					}
				}

			}
		}

		return paxChargesTempList;
	}
	
	private static String getExternalChargeCode(ExternalChgDTO externalChgDTO, Map<String, String> externalChargesMap) {

		String externalChargeCode = null;
		if (externalChargesMap != null && !externalChargesMap.isEmpty() && externalChgDTO != null) {
			String chargeCode = null;

			if (externalChgDTO.getExternalChargesEnum() != null) {
				externalChargeCode = externalChargesMap.get(externalChgDTO.getExternalChargesEnum().toString());
			}

			if (externalChargeCode == null) {
				if (externalChgDTO instanceof SMExternalChgDTO) {
					chargeCode = EXTERNAL_CHARGES.SEAT_MAP.toString();
				} else if (externalChgDTO instanceof MealExternalChgDTO) {
					chargeCode = EXTERNAL_CHARGES.MEAL.toString();
				} else if (externalChgDTO instanceof BaggageExternalChgDTO) {
					chargeCode = EXTERNAL_CHARGES.BAGGAGE.toString();
				} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
					if (externalChgDTO.getChargeCode() != null && "SSR".equalsIgnoreCase(externalChgDTO.getChargeCode())) {
						chargeCode = EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString();
					} else if (externalChgDTO.getChargeCode() != null && "HALA".equalsIgnoreCase(externalChgDTO.getChargeCode())) {
						chargeCode = EXTERNAL_CHARGES.AIRPORT_SERVICE.toString();
					}
				} else if (externalChgDTO instanceof InsuranceSegExtChgDTO) {
					chargeCode = EXTERNAL_CHARGES.INSURANCE.toString();
				} else {
					chargeCode = externalChgDTO.getChargeCode();
				}

				if (chargeCode != null) {
					externalChargeCode = externalChargesMap.get(chargeCode);
				}
			}

		}

		return externalChargeCode;
	}
	
	
	private static EXTERNAL_CHARGES getExternalChargeEnum(ExternalChgDTO externalChgDTO) {

		EXTERNAL_CHARGES enumCode = null;

		if (externalChgDTO != null) {
			enumCode = externalChgDTO.getExternalChargesEnum();

			if (enumCode == null) {
				if (externalChgDTO instanceof SMExternalChgDTO) {
					enumCode = EXTERNAL_CHARGES.SEAT_MAP;
				} else if (externalChgDTO instanceof MealExternalChgDTO) {
					enumCode = EXTERNAL_CHARGES.MEAL;
				} else if (externalChgDTO instanceof BaggageExternalChgDTO) {
					enumCode = EXTERNAL_CHARGES.BAGGAGE;
				} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
					if (externalChgDTO.getChargeCode() != null && "SSR".equalsIgnoreCase(externalChgDTO.getChargeCode())) {
						enumCode = EXTERNAL_CHARGES.INFLIGHT_SERVICES;
					} else if (externalChgDTO.getChargeCode() != null && "HALA".equalsIgnoreCase(externalChgDTO.getChargeCode())) {
						enumCode = EXTERNAL_CHARGES.AIRPORT_SERVICE;
					}
				} else if (externalChgDTO instanceof InsuranceSegExtChgDTO) {
					enumCode = EXTERNAL_CHARGES.INSURANCE;
				}

			}
		}

		return enumCode;
	}

}
