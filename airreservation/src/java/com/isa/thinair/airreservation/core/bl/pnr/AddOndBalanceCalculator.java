package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.dto.SegmentSummaryTOV2;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.core.bl.segment.FlownAssitUnit;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AddOndBalanceCalculator extends BaseBalanceCalculator implements IBalanceCalculator {

	private Map<Integer, PnrChargesDTO> paxModifyingPnrChargeMap;
	private Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseCurrCharges;
	private Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseExcludedCharges;
	private Reservation reservation;
	private Date quotedDate = null;
	private ONDFareChargeProcessor ondFareChargeProcessor;
	private ExternalChargeProcessor extChargeProcessor;

	public AddOndBalanceCalculator(Reservation reservation, Date quotedDate, Collection<OndFareDTO> ondFares,
			Collection<PnrChargesDTO> modifyingPnrCharges, Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseCurrCharges,
			FlownAssitUnit flownAssitUnit, Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges,
			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseExcludedCharges,
			Map<Integer, List<ExternalChgDTO>> paxEffectiveTax, CredentialsDTO credentialsDTO) {
		this.quotedDate = quotedDate;
		this.ondFareChargeProcessor = new ONDFareChargeProcessor(ondFares, this.quotedDate, flownAssitUnit, credentialsDTO, reservation);
		this.paxModifyingPnrChargeMap = AirProxyReservationUtil.indexOndChargesByPax(modifyingPnrCharges);
		this.mapPnrPaxIdWiseCurrCharges = mapPnrPaxIdWiseCurrCharges;
		this.mapPnrPaxIdWiseExcludedCharges = mapPnrPaxIdWiseExcludedCharges;
		this.extChargeProcessor = new ExternalChargeProcessor(quotedDate, reprotectedExternalCharges);
		this.reservation = reservation;
		this.paxEffectiveTax = paxEffectiveTax;
		this.infantPaymentSeparated = reservation.isInfantPaymentRecordedWithInfant();
	}

	public void calculate() {
		for (ReservationPax passenger : getReservationPassengers()) {
			if (!passenger.getPaxType().equals(PaxTypeTO.INFANT) || infantPaymentSeparated) {
				SegmentSummaryTOV2 balanceSummary = getSegmentSummary(passenger.getPnrPaxId());
				balanceSummary.addNewMetaCharges(ondFareChargeProcessor.getChargesPerPax(passenger.getPaxType()));
				if (!infantPaymentSeparated && passenger.hasInfant()) {
					balanceSummary.addNewMetaCharges(ondFareChargeProcessor.getChargesPerPax(PaxTypeTO.INFANT));
				}

				balanceSummary.addNewMetaCharges(extChargeProcessor.getPaxExternalCharges(passenger.getPnrPaxId()));
				if (!infantPaymentSeparated && passenger.hasInfant()) {
					balanceSummary.addNewMetaCharges(extChargeProcessor.getPaxExternalCharges(passenger.getAccompaniedPaxId()));
				}

				PnrChargesDTO modifyingCharges = paxModifyingPnrChargeMap.get(passenger.getPnrPaxId());
				if (modifyingCharges == null) {
					modifyingCharges = new PnrChargesDTO();
				}

				mapPnrPaxIdWiseCurrCharges.get(passenger.getPnrPaxId()).addAll(modifyingCharges.getExtraFeeChargeMetaAmounts());

				balanceSummary.addCurrentMetaCharges(mapPnrPaxIdWiseCurrCharges.get(passenger.getPnrPaxId()));
				balanceSummary.addCurrentNonRefundableMetaCharges(modifyingCharges.getNonRefundableChargeMetaAmounts());
				balanceSummary.addCurrentRefundableMetaCharges(modifyingCharges.getRefundableChargeMetaAmounts());

				if (ondFareChargeProcessor.hasPenaltyChargesPerPax(passenger.getPaxType())) {
					balanceSummary.setModificationPenalty(ondFareChargeProcessor.getPenaltyChargesPerPax(passenger.getPaxType()));
				} else if (modifyingCharges.getModificationPenalty().compareTo(BigDecimal.ZERO) > 0) {
					balanceSummary.setModificationPenalty(modifyingCharges.getModificationPenalty());
				}

				if (!infantPaymentSeparated && passenger.hasInfant() && ondFareChargeProcessor.hasPenaltyChargesPerPax(PaxTypeTO.INFANT)) {
					balanceSummary.setModificationPenalty(AccelAeroCalculator.add(balanceSummary.getModificationPenatly(),
							ondFareChargeProcessor.getPenaltyChargesPerPax(PaxTypeTO.INFANT)));
				}

				if (mapPnrPaxIdWiseExcludedCharges != null && !mapPnrPaxIdWiseExcludedCharges.isEmpty()) {
					balanceSummary.setExcludedSegCharge(mapPnrPaxIdWiseExcludedCharges.get(passenger.getPnrPaxId()));
				}

			}
		}
	}

	@Override
	protected Set<ReservationPax> getReservationPassengers() {
		return reservation.getPassengers();
	}

	@Override
	protected Map<Integer, Collection<ChargeMetaTO>> getPnrPaxIdWiseCurrentCharges() {
		return mapPnrPaxIdWiseCurrCharges;
	}

	protected Map<Integer, Collection<ChargeMetaTO>> getPnrPaxIdWiseExcludedCharges() {
		return mapPnrPaxIdWiseExcludedCharges;
	}
}
