/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealUtil;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.AirportTransferAdaptor;
import com.isa.thinair.airreservation.core.bl.common.AutomaticCheckinAdaptor;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.InsuranceHelper;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

/**
 * Command for creating a modification object for change segment
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cMForChgSegment"
 */
public class CMForChgSegment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CMForChgSegment.class);

	/**
	 * Execute method of the CMForChgSegment command
	 * 
	 * @throws ModuleException
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String cancelSegmentInfo = (String) this.getParameter(CommandParamNames.CANCEL_SEGMENT_INFORMATION);
		String vacatedSeats = (String) this.getParameter(CommandParamNames.VACATED_SEATS);
		String vacatedMeals = (String) this.getParameter(CommandParamNames.VACATED_MEALS);
		String fareChanges = (String) this.getParameter(CommandParamNames.FARE_CHANGES);
		String originalSegmentTotal = (String) this.getParameter(CommandParamNames.ORIGINAL_SEGMENT_TOTAL);
		String addSegmentInfo = (String) this.getParameter(CommandParamNames.ADD_SEGMENT_INFORMATION);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Boolean isForceConfirmed = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		List<InsuranceResponse> insuranceResponses = (List<InsuranceResponse>) this.getParameter(CommandParamNames.INSURANCE_RES);
		Collection[] flightMealIds = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_MEAL_IDS);
		Collection[] flightBaggageIds = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_BAGGAGE_IDS);
		Collection[] airportTransferInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_AIRPORT_TRANSFER_IDS);
		Map flightSeatIdsAndPaxTypes = (Map) this.getParameter(CommandParamNames.FLIGHT_SEAT_IDS_AND_PAX_TYPES);
		Collection[] flightAutoCheckinsInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_AUTO_CHECKIN_IDS);
		String updatedFlexibilitiesInfo = (String) this.getParameter(CommandParamNames.UPDATED_FLEXIBILITIES_INFORMATION);
		if (updatedFlexibilitiesInfo == null || "".equals(updatedFlexibilitiesInfo))
			updatedFlexibilitiesInfo = (String) this.getParameter(CommandParamNames.FLEXI_INFO);

		String fareDiscountAudit = (String) this.getParameter(CommandParamNames.FARE_DISCOUNT_AUDIT);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		String userNoteType = (String) this.getParameter(CommandParamNames.USER_NOTE_TYPE);
		Boolean isRequote = getParameter(CommandParamNames.IS_REQUOTE, Boolean.FALSE, Boolean.class);
//		Boolean isCNXMODChargeChange = getParameter(CommandParamNames.IS_CNX_MOD_FEE_CHANGE_AUDIT_NEED, Boolean.FALSE,
//				Boolean.class);
//		CustomChargesTO customChargesTO = (CustomChargesTO) this.getParameter(CommandParamNames.CUSTOM_CHARGES);

		// Checking params
		this.validateParams(pnr, cancelSegmentInfo, originalSegmentTotal, addSegmentInfo, isForceConfirmed, credentialsDTO,
				isRequote);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		
		// get insurance policy info for audit content
		String insuranceAudit = InsuranceHelper.getAudit(insuranceResponses);

		// Get the seat information
		String seatCodes = getSeatCodesForAudit(flightSeatIdsAndPaxTypes);

		// Get the meal information
		String mealCodes = null;
		if (flightMealIds != null && flightMealIds.length > 0) {
			mealCodes = getMealCodesForAudit(flightMealIds[1]);
		}
		
		// Get the Baggage Information
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
		Collection[] colResInfo = { reservation.getPassengers(), reservation.getSegmentsView() };

		String baggageNames = null;

		if (flightBaggageIds != null && flightBaggageIds.length > 0) {
			baggageNames = BaggageAdaptor.recordPaxWiseAuditHistoryForModifyBaggages(pnr, flightBaggageIds[0], credentialsDTO,
					null, colResInfo);
		}
		
		// Get the AutoCheckins for Audit
		String autoCheckins = null;
		if (flightAutoCheckinsInfo != null && flightAutoCheckinsInfo.length > 0) {
			autoCheckins = AutomaticCheckinAdaptor.recordPaxWiseAuditHistoryForModifyAutoCheckin(flightAutoCheckinsInfo[0]);
		}

		// Get the apt information
		String aptDetails = null;
		if (airportTransferInfo != null  && airportTransferInfo.length > 0) {
			aptDetails = AirportTransferAdaptor.recordPaxWiseAuditHistoryForModifyAirportTransfers(pnr,
					airportTransferInfo[0], credentialsDTO, null, colResInfo);
		}

		String bookingType = "";
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			bookingType = (String) this.getParameter(CommandParamNames.BOOKING_TYPE);
		}

		// Compose Modification object
		ReservationAudit reservationAudit = this.composeAudit(pnr, cancelSegmentInfo, fareChanges, originalSegmentTotal,
				addSegmentInfo, isForceConfirmed, credentialsDTO, insuranceAudit, seatCodes, mealCodes, aptDetails,
				updatedFlexibilitiesInfo, fareDiscountAudit, userNotes, vacatedSeats, vacatedMeals, baggageNames, bookingType,
				userNoteType, autoCheckins);

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);

		if (isRequote) {
			Collection<ReservationAudit> colRequoteReservationAudit = new ArrayList<ReservationAudit>();
			if (this.getParameter(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION) != null) {
				colRequoteReservationAudit = (Collection<ReservationAudit>) this
						.getParameter(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION);
			}
			colRequoteReservationAudit.addAll(colReservationAudit);
			response.addResponceParam(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION, colRequoteReservationAudit);
		}

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_IDS, this.getParameter(CommandParamNames.FLIGHT_SEGMENT_IDS));
		response.addResponceParam(CommandParamNames.PNR, this.getParameter(CommandParamNames.PNR));

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Compose Audit
	 * 
	 * @param pnr
	 * @param cancelSegmentInfo
	 * @param fareChanges
	 * @param originalSegmentTotal
	 * @param addSegmentInfo
	 * @param isForceConfirmed
	 * @param credentialsDTO
	 * @param vacatedSeats
	 * @param vacatedMeals
	 * @param bookingType
	 *            TODO
	 * @param userNoteType TODO
	 * @param autoCheckins
	 * @param insurancePolicy
	 * @return
	 */
	private ReservationAudit composeAudit(String pnr, String cancelSegmentInfo, String fareChanges, String originalSegmentTotal,
			String addSegmentInfo, Boolean isForceConfirmed, CredentialsDTO credentialsDTO, String insuranceStr,
			String seatCodes, String strMeals, String aptDetails, String updatedFlexibilitiesInfo, String fareDiscountInfo,
			String userNotes, String vacatedSeats, String vacatedMeals, String strBaggages, String bookingType,
			String userNoteType, String autoCheckins) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.MODIFIED_SEGMENT.getCode());
		reservationAudit.setUserNote(userNotes);
		reservationAudit.setUserNoteType(userNoteType);

		// Setting the cancel segment information
		if (cancelSegmentInfo != null && !"".equals(cancelSegmentInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.OLD_SEGMENT_INFORMATION,
					cancelSegmentInfo);
		} else if (addSegmentInfo != null && !"".equals(addSegmentInfo)) {// Same flight modification add old segment as
																			// original flight
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.OLD_SEGMENT_INFORMATION,
					addSegmentInfo);
		}

		// if (bookingType != null && bookingType.equals("OVERBOOK")) {
		// reservationAudit
		// .addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.BOOKING_TYPE, "OverBooking");
		// }

		// Capture vacated seats information of canceled segment
		if (vacatedSeats != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.VACATED_SEATS, vacatedSeats);
		}

		// Capture vacated meal information of canceled segment
		if (vacatedMeals != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.VACATED_MEALS, vacatedMeals);
		}

		// Capturing the fare changes if exist
		if (fareChanges != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.FARE_CHANGES, fareChanges);
		}
		if (originalSegmentTotal != null && !"".equals(originalSegmentTotal)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.ORIGINAL_SEGMENT_TOTAL,
					originalSegmentTotal);
		}

		// Setting the add segment information
		if (addSegmentInfo != null && !"".equals(addSegmentInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.NEW_SEGMENT_INFORMATION,
					addSegmentInfo);
		}

		// Setting the force confirm information
		if (isForceConfirmed.booleanValue()) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.FORCE_CONFIRM,
					ReservationTemplateUtils.AUDIT_FORCE_CONFIRM_TEXT);
		}

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ModifiedSegment.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}
		if (insuranceStr != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.INSURANCE, insuranceStr);
		}

		if (seatCodes != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Seats.SEATS, seatCodes);
		}

		if (strMeals != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Meals.MEALS, strMeals);
		}
		
		if (aptDetails != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AirportTransfers.AIRPORTTRANSFERS, aptDetails);
		}

		if (autoCheckins != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutomaticCheckins.AUTOMATICCHECKINS, autoCheckins);
		}

		if (updatedFlexibilitiesInfo != null && !"".equals(updatedFlexibilitiesInfo)) {
			reservationAudit
					.addContentMap(AuditTemplateEnum.TemplateParams.Flexibilities.FLEXIBILITIES, updatedFlexibilitiesInfo);
		}

		if (fareDiscountInfo != null && !"".equals(fareDiscountInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.FareDiscount.FARE_DISCOUNT_INFO, fareDiscountInfo);
		}

		if (strBaggages != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Baggages.BAGGAGES, strBaggages);
		}

		if (!bookingType.isEmpty() && bookingType.equals("OVERBOOK")) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.BOOKING_TYPE, bookingType);
		}

		return reservationAudit;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param cancelSegmentInfo
	 * @param originalSegmentTotal
	 * @param addSegmentInfo
	 * @param isForceConfirmed
	 * @param credentialsDTO
	 * @param isRequote
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, String cancelSegmentInfo, String originalSegmentTotal, String addSegmentInfo,
			Boolean isForceConfirmed, CredentialsDTO credentialsDTO, Boolean isRequote) throws ModuleException {
		log.debug("Inside validateParams");

		if (!isRequote
				&& (pnr == null || cancelSegmentInfo == null || originalSegmentTotal == null || addSegmentInfo == null
						|| isForceConfirmed == null || credentialsDTO == null)) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	/**
	 * Returns the Meal codes for audit
	 * 
	 * @param flightMealIds
	 * @return
	 */
	private String getMealCodesForAudit(Collection<Integer> flightMealIds) {
		String mealCodes = null;

		if (flightMealIds != null && flightMealIds.size() > 0) {
			Collection<String> mealCodesValues = ReservationDAOUtils.DAOInstance.MEAL_DAO.getMealCodes(flightMealIds);

			if (mealCodesValues != null && mealCodesValues.size() > 0) {
				mealCodes = MealUtil.getMealAuditString(mealCodesValues);
			}
		}

		return mealCodes;
	}

	/**
	 * Returns the seat codes for audit
	 * 
	 * @param flightSeatIdsAndPaxTypes
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getSeatCodesForAudit(Map flightSeatIdsAndPaxTypes) {
		String seatCodes = null;

		if (flightSeatIdsAndPaxTypes != null && flightSeatIdsAndPaxTypes.keySet().size() > 0) {
			Collection<String> seatCodesValues = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO
					.getSeatCodes(flightSeatIdsAndPaxTypes.keySet());

			if (seatCodesValues != null && seatCodesValues.size() > 0) {
				seatCodes = SMUtil.getSeatAuditString(seatCodesValues);
			}
		}

		return seatCodes;
	}
}
