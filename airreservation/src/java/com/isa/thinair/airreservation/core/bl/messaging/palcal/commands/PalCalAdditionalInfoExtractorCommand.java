package com.isa.thinair.airreservation.core.bl.messaging.palcal.commands;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PnlAdlFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdditionalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory.PnlAdlAbstractDataFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory.PnlAdlDataFactoryProducer;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @isa.module.command name="palCalAdditionalInfoExtractorCommand"
 * 
 */
public class PalCalAdditionalInfoExtractorCommand extends DefaultBaseCommand {

	/** Holds the logger instance ye */
	private static Log log = LogFactory.getLog(PalCalAdditionalInfoExtractorCommand.class);

	private PnlAdlAbstractDataFactory<BasePassengerCollection, BaseDataContext> dcsPaxMsgAbstractDataFactory;
	private MessageDataStructure<BasePassengerCollection, BaseDataContext> messageDataStructure;
	private PassengerCollection passengerCollection;
	private BaseDataContext baseDataContext;
	private String messageType;
	private DefaultServiceResponse response;

	@Override
	public ServiceResponce execute() throws ModuleException {
		createDataFactory();
		resolveCommandParameters();
		messageType = getMessageType();
		messageDataStructure = dcsPaxMsgAbstractDataFactory.createDataStructure(messageType);
		passengerCollection = (PassengerCollection) messageDataStructure.getDataStructure(createAdditionalDataContext());
		populateDataContextByPassengerCollection();
		createResponseObject(response);
		return response;
	}

	private void createDataFactory() {
		dcsPaxMsgAbstractDataFactory = PnlAdlDataFactoryProducer.getFactory(PnlAdlFactory.PNLADL_ADDITIONAL_DATA);
	}

	private void resolveCommandParameters() {
		baseDataContext = (BaseDataContext) this.getParameter(CommandParamNames.PNL_DATA_CONTEXT);
		response = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		passengerCollection = baseDataContext.getPassengerCollection();
	}

	private String getMessageType() {
		return baseDataContext.getMessageType();
	}

	private void populateDataContextByPassengerCollection() {
		baseDataContext.setPassengerCollection(passengerCollection);
	}

	private void createResponseObject(DefaultServiceResponse response) {
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, response);
		response.addResponceParam(CommandParamNames.PNL_DATA_CONTEXT, baseDataContext);
	}

	private PnlAdditionalDataContext createAdditionalDataContext() {
		PnlAdditionalDataContext additionalDataContext = new PnlAdditionalDataContext();
		additionalDataContext.setDepartureAirportCode(baseDataContext.getDepartureAirportCode());
		additionalDataContext.setPassengerCollection(passengerCollection);
		additionalDataContext.setFeaturePack(baseDataContext.getFeaturePack());
		additionalDataContext.setFlightId(baseDataContext.getFlightId());
		additionalDataContext.setDepartureAirportCode(baseDataContext.getDepartureAirportCode());
		additionalDataContext.setCarrierCode(baseDataContext.getCarrierCode());
		additionalDataContext.setFlightLocalDate(baseDataContext.getFlightLocalDate());
		additionalDataContext.setFlightNumber(baseDataContext.getFlightNumber());
		additionalDataContext.setPnlAdlDeliveryInformation(baseDataContext.getPnlAdlDeliveryInformation());
		return additionalDataContext;
	}
}
