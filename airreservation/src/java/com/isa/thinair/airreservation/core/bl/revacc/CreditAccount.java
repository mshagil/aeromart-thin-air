/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * commanad to credit account
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="creditAccount"
 */
public class CreditAccount extends DefaultBaseCommand {

	// Dao's
	private ReservationCreditDAO reservationCreditDao;
	private ReservationTnxDAO reservationTnxDao;

	/**
	 * constructor of the BalanceCredit command
	 */
	public CreditAccount() {

		// looking up daos
		reservationCreditDao = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	/**
	 * execute method of the BalanceCredit command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("deprecation")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal amount = (BigDecimal) this.getParameter(CommandParamNames.AMOUNT);

		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		// checking params
		this.checkParams(pnrPaxId, amount, credentialsDTO);

		if (0 < amount.doubleValue()) {

			ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, amount,
					ReservationTnxNominalCode.CREDIT_ACCOUNT.getCode(), credentialsDTO,
					CalendarUtil.getCurrentSystemTimeInZulu(), null, null, null, false, true);
			reservationTnxDao.saveTransaction(creditTnx);

			ReservationCredit credit = CreditFactory.getInstance(creditTnx.getTnxId().longValue(), pnrPaxId, amount, null);
			reservationCreditDao.saveReservationCredit(credit);

		} else {
			throw new ModuleException("revac.invalid.amount");
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(String pnrPaxId, BigDecimal amount, CredentialsDTO credentialsDTO) throws ModuleException {

		if (pnrPaxId == null || amount == null || credentialsDTO == null)// throw exception
			throw new ModuleException("airreservations.arg.invalid.null");
	}

}
