/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PnlAdlFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;

/**
 * @author udithad
 *
 */
public class PnlAdlDataFactoryProducer {

	public static PnlAdlAbstractDataFactory<BasePassengerCollection, BaseDataContext> getFactory(String factory){
		PnlAdlAbstractDataFactory<BasePassengerCollection, BaseDataContext> abstractDataFactory=null;
		if(factory.equals(PnlAdlFactory.PNLADL_MASTER_DATA)){
			abstractDataFactory = new PnlAdlMasterDataFactory<BasePassengerCollection, BaseDataContext>();
		}else if(factory.equals(PnlAdlFactory.PNLADL_ADDITIONAL_DATA)){
			abstractDataFactory = new PnlAdlAdditionalDataFactory<BasePassengerCollection, BaseDataContext>();
		}
		return abstractDataFactory;
	}
	
}
