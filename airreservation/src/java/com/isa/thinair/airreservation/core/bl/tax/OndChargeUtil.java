package com.isa.thinair.airreservation.core.bl.tax;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;

public class OndChargeUtil {

	public static void setChargeCodeInPaxCharges(Collection<ReservationPaxOndCharge> colReservationPaxOndCharges,
			Collection<Integer> ppocIds) {
		if (colReservationPaxOndCharges != null && !colReservationPaxOndCharges.isEmpty()) {
			if (ppocIds == null || ppocIds.isEmpty()) {
				ppocIds = new HashSet<>();
				for (ReservationPaxOndCharge paxOndCharge : colReservationPaxOndCharges) {
					if (paxOndCharge.getPnrPaxOndChgId() != null) {
						ppocIds.add(paxOndCharge.getPnrPaxOndChgId());
					}
				}
			}
			Map<Long, String> ppocChargeCodeMap = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getChargeCodesForPaxOndPayments(ppocIds);
			for (ReservationPaxOndCharge reservationPaxOndCharge : colReservationPaxOndCharges) {
				if (ppocChargeCodeMap.containsKey(reservationPaxOndCharge.getPnrPaxOndChgId().longValue())) {
					String chargeCode = ppocChargeCodeMap.get(reservationPaxOndCharge.getPnrPaxOndChgId().longValue());
					if (chargeCode != null) {
						reservationPaxOndCharge.setChargeCode(chargeCode);
					}
				}
			}
		}
	}

	public static void setChargeCodeInPaxCharges(Collection<ReservationPaxOndCharge> colReservationPaxOndCharges) {
		setChargeCodeInPaxCharges(colReservationPaxOndCharges, null);
	}

}
