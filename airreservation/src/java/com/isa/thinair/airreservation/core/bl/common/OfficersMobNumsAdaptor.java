package com.isa.thinair.airreservation.core.bl.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.OfficersMobileNumbers;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;

public class OfficersMobNumsAdaptor {

	private static Log log = LogFactory.getLog(OfficersMobNumsAdaptor.class);

	public static void auditForOfficersMobNumsConfigModification(UserPrincipal userPrinciple,
			OfficersMobileNumbers OfficersMobNums, Map<String, String[]> contMap, long versionFlg) {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userPrinciple.getUserId());

		if (versionFlg < 0) {
			audit.setTaskCode(TasksUtil.MASTER_ADD_OFFICERS_MOB_NUMS);
		} else {
			audit.setTaskCode(TasksUtil.MASTER_EDIT_OFFICERS_MOB_NUMS);
		}

		LinkedHashMap contents = new LinkedHashMap();
		contents.put("config_id", OfficersMobNums.getOfficerId());
		contents.put("modified by", userPrinciple.getUserId());

		if (versionFlg < 0) {
			OfficersMobNums.setUser(userPrinciple.getUserId());
			contents.put("new_config", createStrOnholdConfigData(OfficersMobNums));
		}

		if (contMap != null && contMap.size() > 0) {
			for (Entry<String, String[]> entry : contMap.entrySet()) {
				contents.put(entry.getKey(), entry.getValue());
			}
		}

		try {
			ReservationModuleUtils.getAuditorBD().audit(audit, contents);
		} catch (ModuleException e) {
			log.error("ModuleException::auditForOfficersMobNumsConfigModification", e);
		}

	}

	public static void auditForDeleteOfficersMobNumsConfigModification(UserPrincipal userPrinciple, int configId) {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userPrinciple.getUserId());
		audit.setTaskCode(TasksUtil.MASTER_DELETE_OFFICERS_MOB_NUMS);

		LinkedHashMap contents = new LinkedHashMap();
		contents.put("config_id", configId);
		contents.put("deletedBy:", userPrinciple.getUserId());

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Date today = new Date();
		contents.put("-deletedDate:", formatter.format(today));
		try {
			ReservationModuleUtils.getAuditorBD().audit(audit, contents);
		} catch (ModuleException e) {
			log.error("ModuleException::auditForDeleteOfficersMobNumsConfigModification", e);
		}

	}

	public static String createStrOnholdConfigData(OfficersMobileNumbers OfficersMobNums) {
		StringBuilder sb = new StringBuilder();
		sb.append("officerName:" + OfficersMobNums.getOfficerName());
		sb.append("-mobNumber:" + OfficersMobNums.getMobNumber());
		sb.append("-createdBy:" + OfficersMobNums.getUser());

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Date today = new Date();
		sb.append("-createdDate:" + formatter.format(today));
		return sb.toString();
	}
}
