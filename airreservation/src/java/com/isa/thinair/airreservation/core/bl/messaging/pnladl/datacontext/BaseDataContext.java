/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;

/**
 * @author udithad
 *
 */
public abstract class BaseDataContext {

	protected String flightNumber;
	protected Integer flightId;
	protected String flightOriginAirportCode;
	protected String flightDestinationAirportCode;
	protected String departureAirportCode;
	protected Date flightLocalDate;
	protected PnlAdlDeliveryInformation pnlAdlDeliveryInformation;
	protected FeaturePack featurePack;
	protected String carrierCode;
	protected PassengerCollection passengerCollection;
	protected boolean isWebserviceSuccess;
	protected Map<String, LinkedHashMap<String,String>> seatConfigurationElements;

	public abstract String getMessageType();

	public PnlAdlDeliveryInformation getPnlAdlDeliveryInformation() {
		return pnlAdlDeliveryInformation;
	}

	public void setPnlAdlDeliveryInformation(PnlAdlDeliveryInformation pnlAdlDeliveryInformation) {
		this.pnlAdlDeliveryInformation = pnlAdlDeliveryInformation;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public Date getFlightLocalDate() {
		return flightLocalDate;
	}

	public void setFlightLocalDate(Date flightLocalDate) {
		this.flightLocalDate = flightLocalDate;
	}

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public FeaturePack getFeaturePack() {
		return featurePack;
	}

	public void setFeaturePack(FeaturePack featurePack) {
		this.featurePack = featurePack;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public PassengerCollection getPassengerCollection() {
		return passengerCollection;
	}

	public void setPassengerCollection(PassengerCollection passengerCollection) {
		this.passengerCollection = passengerCollection;
	}

	public boolean isWebserviceSuccess() {
		return isWebserviceSuccess;
	}

	public void setWebserviceSuccess(boolean isWebserviceSuccess) {
		this.isWebserviceSuccess = isWebserviceSuccess;
	}

	public String getFlightOriginAirportCode() {
		return flightOriginAirportCode;
	}

	public void setFlightOriginAirportCode(String flightOriginAirportCode) {
		this.flightOriginAirportCode = flightOriginAirportCode;
	}

	public String getFlightDestinationAirportCode() {
		return flightDestinationAirportCode;
	}

	public void setFlightDestinationAirportCode(String flightDestinationAirportCode) {
		this.flightDestinationAirportCode = flightDestinationAirportCode;
	}
	
	public Map<String, LinkedHashMap<String, String>> getSeatConfigurationElements() {
		return seatConfigurationElements;
	}
	

	public void setSeatConfigurationElements(Map<String, LinkedHashMap<String, String>> seatConfigurationElements) {
		this.seatConfigurationElements = seatConfigurationElements;
	}

}
