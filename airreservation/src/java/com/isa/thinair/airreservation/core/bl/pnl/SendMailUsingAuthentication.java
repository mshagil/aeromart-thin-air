package com.isa.thinair.airreservation.core.bl.pnl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;

public class SendMailUsingAuthentication {

	private static Log log = LogFactory.getLog(SendMailUsingAuthentication.class);

	private static AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();

	private static Object lock = new Object();

	public SendMailUsingAuthentication() {
	}

	public void postMail(String recipients[], String subject, String message, boolean alternateSender) throws MessagingException {

		String smtpHost = airReservationConfig.getDefaultMailServer();

		String smtpAuthUser = airReservationConfig.getDefaultMailServerUsername();

		String smtpAuthPassword = airReservationConfig.getDefaultMailServerPassword();

		String emailFromAddress = airReservationConfig.getSitaMessageSender();

		/**
		 * alternateMessageSender configs
		 */
		if (alternateSender) {
			Map<String,String> alternateMailServer = airReservationConfig.getAlternateMessageSender();
			smtpHost = (String) alternateMailServer.get("altMailServer");
			smtpAuthUser = (String) alternateMailServer.get("altMailServerUsername");
			smtpAuthPassword = (String) alternateMailServer.get("altMailServerPassword");
			emailFromAddress = (String) alternateMailServer.get("altSender");
		}

		synchronized (lock) {// Is it required to synchronize with EmailBroadcaster.sendMessages(...) ?
			Session session = null;
			Transport transport = null;
			boolean debug = true;
			String toAddressesDebug = "";
			int maxRetryCount = 3;// FIXME take from configuration file
			boolean throwException = false;

			for (int attempt = 1; attempt <= maxRetryCount; attempt++) {
				try {
					InternetAddress[] addressTo = new InternetAddress[recipients.length];

					toAddressesDebug = "";
					for (int i = 0; i < recipients.length; i++) {
						addressTo[i] = new InternetAddress(recipients[i]);

						if ("".equals(toAddressesDebug)) {
							toAddressesDebug += recipients[i];
						} else {
							toAddressesDebug += ";" + recipients[i];
						}

					}

					log.info("Start sending email in thread-safe way  [toAddresses=" + toAddressesDebug + ",subject=" + subject
							+ ",ATTEMPT=" + attempt + "]");

					// mail properties
					Properties props = new Properties();
					props.put("mail.smtp.host", smtpHost);
					props.put("mail.smtp.auth", "true");
					props.put("mail.transport.protocol", "smtp");

					Authenticator auth = null;

					// always create a new session
					session = Session.getInstance(props, auth);

					// print debug messages to the System.out
					session.setDebug(debug);

					// create a message
					Message msg = new MimeMessage(session);

					// set the from and to address
					InternetAddress addressFrom = new InternetAddress(emailFromAddress);

					msg.setFrom(addressFrom);

					msg.setRecipients(Message.RecipientType.TO, addressTo);

					// Setting the Subject and Content Type
					msg.setSubject(subject);
					msg.setContent(message, "text/plain");
					msg.saveChanges();

					transport = session.getTransport("smtp");
					log.debug("Obtained SMTP Transport");

					transport.connect(smtpHost, smtpAuthUser, smtpAuthPassword);
					log.debug("Successfully connected to the SMTP server [smtpHost=" + smtpHost + ",username=" + smtpAuthUser
							+ "]");

					transport.sendMessage(msg, msg.getAllRecipients());
					log.info("Successfully Sent Email [toAddresses=" + toAddressesDebug + ",subject=" + subject + "]");

					transport.close();
					log.debug("Closed SMTP Transport");

					break;// success

				} catch (Throwable t) {
					log.error("Sending email failed [attempt=" + attempt + ",smtp host=" + smtpHost + ",username=" + smtpAuthUser
							+ ",passwordLength="
							+ ((smtpAuthPassword != null) ? Integer.toString(smtpAuthPassword.length()) : "null")
							+ "\n\r toAdddress(es)=" + toAddressesDebug + "\n\r subject=" + subject + "\n\r message body="
							+ message + "]", t);
					throwException = false;
					if (attempt < maxRetryCount) {// retry
						// FIXME - temporary fix for authentication/delivery failures.
						try {
							log.debug("Going to sleep 0.5 seconds before retry sending ...");
							Thread.sleep(500);// wait 0.5 seconds before retry sending FIXME take from configuration
												// file
						} catch (Exception e) {
							log.error(e);
						}
						log.debug("Going to retry sending [attempt=" + attempt + "] ...");
						continue;// retry
					} else {
						log.error("Failed sending email after " + attempt + " attempts.");
						throwException = true;// failure after retry
					}

					if (throwException) {
						if ((t != null) && (t instanceof MessagingException)) {
							throw (MessagingException) t;
						}
						if ((t != null) && (t instanceof Exception)) {
							throw new MessagingException("Sending email failed ", (Exception) t);
						} else {
							MessagingException e = new MessagingException("Sending email failed. ");
							if (t != null && t.getStackTrace() != null)
								e.setStackTrace(t.getStackTrace());
							throw e;
						}
					}
				} finally {
					for (int closeAttempt = 1; closeAttempt <= 1; ++closeAttempt) {// trying once only...
						if (transport != null && transport.isConnected()) {
							try {
								log.warn("Going to close the connected SMTP transport ...[sendAttemp=" + attempt
										+ ",closeAttempt=" + closeAttempt + "]");
								transport.close();
								log.debug("Closed the connected SMTP Transport. [sendAttemp=" + attempt + ",closeAttempt="
										+ closeAttempt + "]");
								break;
							} catch (Exception e) {
								log.error("Closing the connected SMTP transport failed. [sendAttemp=" + attempt
										+ ",closeAttempt=" + closeAttempt + "]", e);
							}
						} else {
							log.debug("No need of closing the SMTP transport. Already closed. [sendAttemp=" + attempt
									+ ",closeAttempt=" + closeAttempt + "]");
							break;
						}
					}
				}
			}
		}
	}

	// Code to read the ADL file
	public static String readPnlFile(String source) throws Exception {
		StringBuffer buffer = new StringBuffer();
		FileReader f = new FileReader(source);
		BufferedReader buf = new BufferedReader(f);
		String tmp = null;
		while ((tmp = buf.readLine()) != null) {
			buffer.append(tmp.trim() + "\n");
		}
		buf.close();
		f.close();
		return buffer.toString();
	}
}
