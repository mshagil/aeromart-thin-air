package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SegmentDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

/**
 * @author Manoj
 */
public class NoTicketChangeMessageCreator extends TypeBReservationMessageCreator {
	
	public NoTicketChangeMessageCreator() {
		segmentsComposingStrategy = new SegmentsComposerForNoAction();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}
	
	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation)
			throws ModuleException {
		
		Map<Integer, Map<Integer, EticketDTO>> eTicketInfo = null;
		if (typeBRequestDTO.geteTicketInfo() != null && !typeBRequestDTO.geteTicketInfo().isEmpty()) {
			eTicketInfo = typeBRequestDTO.geteTicketInfo();
		} 
		
		if (eTicketInfo != null) {
			for (Entry<Integer, Map<Integer, EticketDTO>> eticketInfoEntry : eTicketInfo.entrySet()) {
				List<NameDTO> nameDTOs = new ArrayList<NameDTO>();
				for(ReservationPax resPax : reservation.getPassengers()) {
					if (resPax.getPaxSequence().equals(eticketInfoEntry.getKey())) {
						NameDTO nameDTO = new NameDTO();
						nameDTO.setFirstName(resPax.getFirstName());
						nameDTO.setLastName(resPax.getLastName());
						nameDTO.setPaxTitle(resPax.getTitle());
						nameDTOs.add(nameDTO);
					}				
				}
	
				for (Entry<Integer, EticketDTO> eticket : eticketInfoEntry.getValue().entrySet()) {
					SSRDetailDTO ssrDetailDTO = new SSRDetailDTO();
					if (typeBRequestDTO.getCsOCCarrierCode() != null) {
						ssrDetailDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
					} else {
						ssrDetailDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					}
					ssrDetailDTO.setCodeSSR(GDSExternalCodes.SSRCodes.ET_NOT_CHANGED.getCode());
					ssrDetailDTO.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.EXCHANGE_RECOMMENDED.getCode());
					ssrDetailDTO.setNoOfPax(1);
					SegmentDTO ssrSegmentDTO = null;
					
					for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
						if (reservationSegmentDTO.getSegmentSeq().equals(eticket.getKey())
								&& reservationSegmentDTO.getCsOcCarrierCode() != null
								&& reservationSegmentDTO.getCsOcCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode())
								&& (!ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED
										.equalsIgnoreCase(reservationSegmentDTO.getSubStatus()))) {

							ssrSegmentDTO = new SegmentDTO();
							ssrSegmentDTO.setDepartureStation(reservationSegmentDTO.getOrigin());
							ssrSegmentDTO.setDestinationStation(reservationSegmentDTO.getDestination());
							
							String bookingClass = null;
							if (reservationSegmentDTO.getFareTO() != null) {
								bookingClass = reservationSegmentDTO.getFareTO().getBookingClassCode();
							} else {
								bookingClass = reservationSegmentDTO.getCabinClassCode();
							}
							String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(typeBRequestDTO.getCsOCCarrierCode(),
									bookingClass);
							ssrSegmentDTO.setBookingCode(TTYMessageUtil.getExternalCsBCForGdsMappedBC(
									typeBRequestDTO.getCsOCCarrierCode(), gdsBookingClass));

							if (reservationSegmentDTO.getFlightNo() != null && reservationSegmentDTO.getFlightNo().length() > 2) {
								ssrSegmentDTO.setFlightNumber(reservationSegmentDTO.getFlightNo().substring(2));
							}
							ssrSegmentDTO.setDepartureDate(reservationSegmentDTO.getDepartureDate());
						}
					}
					if (ssrSegmentDTO != null) {
						ssrDetailDTO.setSegmentDTO(ssrSegmentDTO);	
						ssrDetailDTO.setNameDTOs(nameDTOs);
						ssrDetailDTO.setFreeText(eticket.getValue().getEticketNo());
						ssrDTOs.add(ssrDetailDTO);
					}
				}
			}
		}
		return ssrDTOs;
	}	
}
