package com.isa.thinair.airreservation.core.bl.tax;

import java.util.Date;

class TnxRange {
	private Date minDate = null;
	private Date maxDate = null;

	public void addDate(Date date) {
		if (date != null) {
			if (minDate == null || date.compareTo(minDate) < 0) {
				minDate = date;
			}
			if (maxDate == null || date.compareTo(maxDate) > 0) {
				maxDate = date;
			}
		}
	}

	public boolean isWithinRange(Date date) {
		if (date != null && minDate != null && maxDate != null && date.compareTo(minDate) >= 0 && date.compareTo(maxDate) <= 0) {
			return true;
		}
		return false;
	}
}