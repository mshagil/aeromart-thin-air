package com.isa.thinair.airreservation.core.bl.tax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class OperationChgTnx {

	private Date tnxDate;
	private List<ReservationPaxOndCharge> chargeList = null;
	private Map<String, ReservationPaxOndCharge> codeWiseSummarizedCharges = null;
	private Set<String> chargeCodes = null;
	private Long key;

	public OperationChgTnx(Date tnxDate, Long key) {
		this.tnxDate = tnxDate;
		this.key = key;
		this.chargeList = new ArrayList<ReservationPaxOndCharge>();
		this.chargeCodes = new HashSet<>();
	}

	public void addCharge(ReservationPaxOndCharge charge) {
		chargeList.add(charge);
		if (codeWiseSummarizedCharges != null) {
			codeWiseSummarizedCharges = null;
		}
	}

	public void removeCharge(ReservationPaxOndCharge charge) {
		chargeList.remove(charge);
		if (codeWiseSummarizedCharges != null) {
			codeWiseSummarizedCharges = null;
			chargeCodes = new HashSet<>();
		}
	}

	public void addCharges(Collection<ReservationPaxOndCharge> charges) {
		for (ReservationPaxOndCharge charge : charges) {
			addCharge(charge);
		}
	}

	public List<ReservationPaxOndCharge> getChargeList() {
		return chargeList;
	}

	public Map<String, ReservationPaxOndCharge> getChargeKeyWiseSummarizedCharges() {
		summarizeCharges();
		return codeWiseSummarizedCharges;
	}

	public Collection<String> getChargeCodes() {
		return chargeCodes;
	}

	public void summarizeCharges() {
		if (codeWiseSummarizedCharges == null) {
			codeWiseSummarizedCharges = new HashMap<>();
			if (chargeList != null) {
				for (ReservationPaxOndCharge charge : chargeList) {
					String chargeKey = getChargeKey(charge);
					if (charge.getChargeCode() != null) {
						chargeCodes.add(charge.getChargeCode());
					}
					if (!codeWiseSummarizedCharges.containsKey(chargeKey)) {
						ReservationPaxOndCharge newCharge = new ReservationPaxOndCharge();
						newCharge.setChargeCode(getChargeCode(charge));
						newCharge.setChargeGroupCode(charge.getChargeGroupCode());
						newCharge.setChargeRateId(charge.getChargeRateId());
						newCharge.setTaxAppliedCategory(charge.getTaxAppliedCategory());
						newCharge.setTaxableAmount(AccelAeroCalculator.getNullSafeValue(charge.getTaxableAmount(), BigDecimal.ZERO));
						newCharge.setNonTaxableAmount(AccelAeroCalculator.getNullSafeValue(charge.getNonTaxableAmount(), BigDecimal.ZERO));
						newCharge.setAmount(charge.getAmount());
						newCharge.setDiscount(charge.getDiscount());
						newCharge.setAdjustment(charge.getAdjustment());
						newCharge.setTaxDepositStateCode(charge.getTaxDepositStateCode());
						newCharge.setTaxDepositCountryCode(charge.getTaxDepositCountryCode());
						codeWiseSummarizedCharges.put(chargeKey, newCharge);
					} else {
						ReservationPaxOndCharge existingCharge = codeWiseSummarizedCharges.get(chargeKey);
						existingCharge.setAmount(AccelAeroCalculator.add(existingCharge.getAmount(), charge.getAmount()));
						existingCharge.setDiscount(AccelAeroCalculator.add(existingCharge.getDiscount(), charge.getDiscount()));
						existingCharge
								.setAdjustment(AccelAeroCalculator.add(existingCharge.getAdjustment(), charge.getAdjustment()));
						existingCharge.setTaxableAmount(
								AccelAeroCalculator.add(AccelAeroCalculator.getNullSafeValue(existingCharge.getTaxableAmount(), BigDecimal.ZERO),
										AccelAeroCalculator.getNullSafeValue(charge.getTaxableAmount(), BigDecimal.ZERO)));
						existingCharge.setNonTaxableAmount(
								AccelAeroCalculator.add(AccelAeroCalculator.getNullSafeValue(existingCharge.getNonTaxableAmount(), BigDecimal.ZERO),
										AccelAeroCalculator.getNullSafeValue(charge.getNonTaxableAmount(), BigDecimal.ZERO)));
					}
				}
			}
		}
	}

	private String getChargeCode(ReservationPaxOndCharge charge) {
		String chargeCode = null;
		if (charge.getChargeCode() == null) {
			chargeCode = charge.getChargeGroupCode();
		} else {
			chargeCode = charge.getChargeCode();
		}
		return chargeCode;
	}

	private String getChargeKey(ReservationPaxOndCharge charge) {
		String chargeKey = null;
		if (charge.getChargeCode() == null) {
			chargeKey = charge.getTaxAppliedCategory() + "|" + charge.getChargeGroupCode();
		} else {
			chargeKey = charge.getTaxAppliedCategory() + "|" + charge.getChargeCode();
		}
		return chargeKey;
	}

	public BigDecimal getTotalChargeAmount() {
		codeWiseSummarizedCharges = null;
		Map<String, ReservationPaxOndCharge> chargeMap = getChargeKeyWiseSummarizedCharges();
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPaxOndCharge ondCharge : chargeMap.values()) {
			total = AccelAeroCalculator.add(total, ondCharge.getEffectiveAmount());
		}

		return total;
	}

	public Date getTnxDate() {
		return tnxDate;
	}

	public OperationChgTnx clone() {
		return clone(null, null);
	}

	public OperationChgTnx clone(Date tnxDate, Long key) {
		OperationChgTnx tnx = new OperationChgTnx(tnxDate == null ? this.getTnxDate() : tnxDate,
				key == null ? this.getKey() : key);
		for (ReservationPaxOndCharge charge : chargeList) {
			tnx.addCharge(charge);
		}
		return tnx;
	}

	public OperationChgTnx diffWith(OperationChgTnx snapshotTnx) {
		Map<String, ReservationPaxOndCharge> chgKeyWiseChargeMap = getChargeKeyWiseSummarizedCharges();
		Map<String, ReservationPaxOndCharge> snapShotCodeWiseChargeMap = snapshotTnx.getChargeKeyWiseSummarizedCharges();

		List<ReservationPaxOndCharge> gstTaxCollection = new ArrayList<>();

		OperationChgTnx tnx = new OperationChgTnx(this.getTnxDate(), this.getKey());
		for (String chargeKey : chgKeyWiseChargeMap.keySet()) {
			ReservationPaxOndCharge currentCharge = chgKeyWiseChargeMap.get(chargeKey);
			if (snapShotCodeWiseChargeMap.containsKey(chargeKey)) {
				ReservationPaxOndCharge snapshotCharge = snapShotCodeWiseChargeMap.get(chargeKey);

				if (isChargeIncreased(currentCharge, snapshotCharge)) {
					ReservationPaxOndCharge newCharge = new ReservationPaxOndCharge();
					newCharge.setChargeCode(currentCharge.getChargeCode());
					newCharge.setChargeGroupCode(currentCharge.getChargeGroupCode());
					newCharge.setChargeRateId(currentCharge.getChargeRateId());
					newCharge.setTaxAppliedCategory(currentCharge.getTaxAppliedCategory());
					newCharge.setTaxableAmount(
							AccelAeroCalculator.subtract(AccelAeroCalculator.getNullSafeValue(currentCharge.getTaxableAmount(), BigDecimal.ZERO),
									AccelAeroCalculator.getNullSafeValue(snapshotCharge.getTaxableAmount(), BigDecimal.ZERO)));
					newCharge.setNonTaxableAmount(AccelAeroCalculator.subtract(AccelAeroCalculator.getNullSafeValue(currentCharge.getNonTaxableAmount(), BigDecimal.ZERO),
							AccelAeroCalculator.getNullSafeValue(snapshotCharge.getNonTaxableAmount(), BigDecimal.ZERO)));
					newCharge.setAmount(AccelAeroCalculator.subtract(currentCharge.getAmount(), snapshotCharge.getAmount()));
					newCharge
							.setDiscount(AccelAeroCalculator.subtract(currentCharge.getDiscount(), snapshotCharge.getDiscount()));
					newCharge.setAdjustment(
							AccelAeroCalculator.subtract(currentCharge.getAdjustment(), snapshotCharge.getAdjustment()));
					newCharge.setTaxDepositStateCode(currentCharge.getTaxDepositStateCode());
					newCharge.setTaxDepositCountryCode(currentCharge.getTaxDepositCountryCode());
					tnx.addCharge(newCharge);

					if (isGST(newCharge)) {
						gstTaxCollection.add(newCharge);
					}

				}
			} else {
				ReservationPaxOndCharge newCharge = new ReservationPaxOndCharge();
				newCharge.setChargeCode(currentCharge.getChargeCode());
				newCharge.setChargeGroupCode(currentCharge.getChargeGroupCode());
				newCharge.setChargeRateId(currentCharge.getChargeRateId());
				newCharge.setTaxAppliedCategory(currentCharge.getTaxAppliedCategory());
				newCharge.setTaxableAmount(AccelAeroCalculator.getNullSafeValue(currentCharge.getTaxableAmount(), BigDecimal.ZERO));
				newCharge.setNonTaxableAmount(AccelAeroCalculator.getNullSafeValue(currentCharge.getNonTaxableAmount(), BigDecimal.ZERO));
				newCharge.setAmount(currentCharge.getAmount());
				newCharge.setDiscount(currentCharge.getDiscount());
				newCharge.setAdjustment(currentCharge.getAdjustment());
				newCharge.setTaxDepositStateCode(currentCharge.getTaxDepositStateCode());
				newCharge.setTaxDepositCountryCode(currentCharge.getTaxDepositCountryCode());
				tnx.addCharge(newCharge);

				if (isGST(newCharge)) {
					gstTaxCollection.add(newCharge);
				}
			}
		}

		if (AppSysParamsUtil.isGSTJNMigrationEnabled() && gstTaxCollection.size() > 0) {
			BigDecimal jnTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (ReservationPaxOndCharge ondCharge : snapShotCodeWiseChargeMap.values()) {
				if (isJN(ondCharge)) {
					jnTotal = AccelAeroCalculator.add(jnTotal, ondCharge.getAmount());
				}
			}

			if (jnTotal.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				for (ReservationPaxOndCharge gstCharge : gstTaxCollection) {

					if (gstCharge.getAmount().compareTo(jnTotal) > 0) {
						gstCharge.setAmount(AccelAeroCalculator.subtract(gstCharge.getAmount(), jnTotal));
						jnTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {
						gstCharge.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						jnTotal = AccelAeroCalculator.subtract(jnTotal, gstCharge.getAmount());
						tnx.removeCharge(gstCharge);
					}

					if (jnTotal.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 0) {
						break;
					}
				}

			}
		}

		return tnx;
	}

	private boolean isJN(ReservationPaxOndCharge ondCharge) {
		// FIXME
		return ondCharge.getChargeCode() != null && (ondCharge.getChargeCode().startsWith("JN"));
	}

	private boolean isGST(ReservationPaxOndCharge newCharge) {
		// FIXME
		return newCharge.getChargeCode() != null && newCharge.getChargeCode().indexOf("GST") > -1;
	}

	private boolean isChargeIncreased(ReservationPaxOndCharge currentCharge, ReservationPaxOndCharge snapshotCharge) {
		if (currentCharge.getEffectiveAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0
				&& snapshotCharge.getEffectiveAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0) {
			return currentCharge.getEffectiveAmount().compareTo(snapshotCharge.getEffectiveAmount()) < 0;
		} else {
			return currentCharge.getEffectiveAmount().compareTo(snapshotCharge.getEffectiveAmount()) > 0;
		}
	}

	@Override
	public String toString() {
		return "OperationChgTnx [tnxDate=" + tnxDate + ", codeWiseSummarizedCharges=" + getSummarizedChargeToString() + "]";
	}

	private String getSummarizedChargeToString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (String code : getChargeKeyWiseSummarizedCharges().keySet()) {
			sb.append(code + ":" + getChargeKeyWiseSummarizedCharges().get(code).getEffectiveAmount() + ",");
		}
		sb.append("]");
		return sb.toString();
	}

	public Long getKey() {
		return key;
	}

}
