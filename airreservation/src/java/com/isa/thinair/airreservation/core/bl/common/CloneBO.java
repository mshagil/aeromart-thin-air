/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;

import com.isa.thinair.airreservation.api.model.OtherAirlineSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.pnr.PNRNumberGeneratorProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Clone reservation business methods
 * 
 * Clones following entities:
 * 
 * @see com.isa.thinair.airreservation.api.model.Reservation
 * @see com.isa.thinair.airreservation.api.model.ReservationSegment
 * @see com.isa.thinair.airreservation.api.model.ReservationContactInfo
 * @see com.isa.thinair.airreservation.api.model.ReservationAdminInfo
 * @see com.isa.thinair.airreservation.api.model.ReservationPax
 * @see com.isa.thinair.airreservation.api.model.ReservationPaxFare
 * @see com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment
 * @see com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge
 * @see com.isa.thinair.airreservation.api.model.ReservationPaxSegCharge
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class CloneBO {
	/**
	 * Clone ReservationPaxOndCharge
	 * 
	 * @param reservationPaxFare
	 * @param reservationPaxOndCharge
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationPaxOndCharge cloneReservationPaxOndCharge(ReservationPaxFare reservationPaxFare,
			ReservationPaxOndCharge reservationPaxOndCharge) throws ModuleException {
		try {
			if (reservationPaxOndCharge != null) {
				reservationPaxOndCharge = (ReservationPaxOndCharge) BeanUtils.cloneBean(reservationPaxOndCharge);
				reservationPaxOndCharge.setReservationPaxFare(reservationPaxFare);
				reservationPaxOndCharge.setPnrPaxOndChgId(null);
				reservationPaxOndCharge.setVersion(-1);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return reservationPaxOndCharge;
	}

	/**
	 * Clone fare (ond) charges
	 * 
	 * @param reservationPaxFare
	 * @param ondCharges
	 * @param ondWisePaxDueAdjMap
	 * @param flightSegIds
	 * @param discountChargeTOs
	 * @param discountPercentage
	 * @param discountCode
	 * @throws ModuleException
	 */
	private static void cloneOndCharges(ReservationPaxFare reservationPaxFare, Set<ReservationPaxOndCharge> ondCharges,
			Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap, Collection<Integer> flightSegIds,
			Collection<DiscountChargeTO> discountChargeTOs, double discountPercentage, String discountCode)
			throws ModuleException {
		BigDecimal[] totalCharges = ReservationApiUtils.getChargeTypeAmounts();

		if (ondCharges != null) {
			Iterator<ReservationPaxOndCharge> itReservationPaxOndCharge = ondCharges.iterator();
			ReservationPaxOndCharge reservationPaxOndCharge;

			while (itReservationPaxOndCharge.hasNext()) {
				reservationPaxOndCharge = (ReservationPaxOndCharge) itReservationPaxOndCharge.next();
				reservationPaxOndCharge = cloneReservationPaxOndCharge(reservationPaxFare, reservationPaxOndCharge);

				// update PAX balance due adjustment charge by system
				boolean isAddCharge = applyPaxCancelationBalanceDueAdjustment(reservationPaxFare.getReservationPax()
						.getPnrPaxId(), reservationPaxFare.getFareId(), ondWisePaxDueAdjMap, reservationPaxOndCharge);

				if (isAddCharge) {

					// update PAX calculated discount for this charge
					boolean isDiscountApplied = applyPaxOndChargesDiscountInfo(reservationPaxOndCharge, flightSegIds,
							discountChargeTOs, discountPercentage);
					if (isDiscountApplied) {
						reservationPaxFare.setDiscountCode(discountCode);
					}

					// Capture the ond charges
					ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
							reservationPaxOndCharge.getAmount(), totalCharges, reservationPaxOndCharge.getDiscount(),
							reservationPaxOndCharge.getAdjustment());

					reservationPaxFare.addCharge(reservationPaxOndCharge);
				}

			}
		}

		reservationPaxFare.setTotalChargeAmounts(totalCharges);
	}

	/**
	 * Clones reservation pax fare segment
	 * 
	 * @param reservationPaxFare
	 * @param reservationPaxFareSegment
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationPaxFareSegment cloneReservationPaxFareSegment(ReservationPaxFare reservationPaxFare,
			ReservationPaxFareSegment reservationPaxFareSegment) throws ModuleException {

		try {
			if (reservationPaxFareSegment != null) {
				ReservationSegment oldReservationSegment = reservationPaxFareSegment.getSegment();

				reservationPaxFareSegment = (ReservationPaxFareSegment) BeanUtils.cloneBean(reservationPaxFareSegment);
				reservationPaxFareSegment.setPnrPaxFareSegId(null);
				reservationPaxFareSegment.setVersion(-1);

				reservationPaxFareSegment.setReservationPaxFare(reservationPaxFare);
				reservationPaxFareSegment.setSegment(oldReservationSegment);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		return reservationPaxFareSegment;
	}

	/**
	 * Clone fare segments
	 * 
	 * @param reservationPaxFare
	 * @param oldFareSegments
	 * @throws ModuleException
	 */
	private static void cloneFareSegments(ReservationPaxFare reservationPaxFare, Collection<ReservationPaxFareSegment> oldFareSegments)
			throws ModuleException {

		if (oldFareSegments != null) {
			Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = oldFareSegments.iterator();
			ReservationPaxFareSegment reservationPaxFareSegment;

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();

				reservationPaxFareSegment = cloneReservationPaxFareSegment(reservationPaxFare, reservationPaxFareSegment);

				reservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
			}
		}
	}

	/**
	 * Clone reservation passenger fare
	 * 
	 * @param reservationPax
	 * @param reservationPaxFare
	 * @param ondWisePaxDueAdjMap
	 * @param discountChargeTOs
	 * @param discountCode
	 * @param discountPercentage
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationPaxFare cloneReservationPaxFare(ReservationPax reservationPax,
			ReservationPaxFare reservationPaxFare, Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap,
			Collection<DiscountChargeTO> discountChargeTOs, String discountCode, double discountPercentage)
			throws ModuleException {

		try {
			if (reservationPaxFare != null) {
				Set<ReservationPaxOndCharge> oldCharges = reservationPaxFare.getCharges();
				Set<ReservationPaxFareSegment> oldFareSegments = reservationPaxFare.getPaxFareSegments();
				reservationPaxFare = (ReservationPaxFare) BeanUtils.cloneBean(reservationPaxFare);
				reservationPaxFare.setPnrPaxFareId(null);
				reservationPaxFare.setReservationPax(reservationPax);
				reservationPaxFare.setVersion(-1);

				Collection<Integer> flightSegIds = new ArrayList<Integer>();
				if (oldFareSegments != null && !oldFareSegments.isEmpty()) {
					for (ReservationPaxFareSegment paxFareSegment : oldFareSegments) {
						if (paxFareSegment.getSegment() != null && paxFareSegment.getSegment().getFlightSegId() != null) {
							flightSegIds.add(paxFareSegment.getSegment().getFlightSegId());
						}
					}
				}

				// Get Ond charges
				cloneOndCharges(reservationPaxFare, oldCharges, ondWisePaxDueAdjMap, flightSegIds, discountChargeTOs, discountPercentage, discountCode);

				// Get Fare Segment
				cloneFareSegments(reservationPaxFare, oldFareSegments);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		return reservationPaxFare;
	}

	/**
	 * Clone Fares
	 * 
	 * @param reservationPax
	 * @param oldFares
	 * @param ondWisePaxDueAdjMap
	 * @param discountChargeTOs
	 * @param discountCode
	 * @param discountPercentage
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<ReservationPaxFare> cloneFares(ReservationPax reservationPax,
			Collection<ReservationPaxFare> oldFares, Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap,
			Collection<DiscountChargeTO> discountChargeTOs, String discountCode, double discountPercentage)
			throws ModuleException {
		Collection<ReservationPaxFare> clonedObjects = new ArrayList<ReservationPaxFare>();

		if (oldFares != null) {
			Iterator<ReservationPaxFare> itReservationPaxFare = oldFares.iterator();
			ReservationPaxFare reservationPaxFare;

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				reservationPaxFare = cloneReservationPaxFare(reservationPax, reservationPaxFare, ondWisePaxDueAdjMap,
						discountChargeTOs, discountCode, discountPercentage);

				clonedObjects.add(reservationPaxFare);
			}
		}

		return clonedObjects;
	}

	/**
	 * Apply fare to the passenger
	 * 
	 * @param reservationPax
	 * @param clonedFares
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal linkFaresToThePassenger(ReservationPax reservationPax, Collection<ReservationPaxFare> clonedFares)
			throws ModuleException {
		BigDecimal[] totalCharges = ReservationApiUtils.getChargeTypeAmounts();

		if (clonedFares != null) {
			Iterator<ReservationPaxFare> itReservationPaxFare = clonedFares.iterator();
			ReservationPaxFare reservationPaxFare;

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				ReservationCoreUtils.updateTotals(totalCharges, reservationPaxFare.getTotalChargeAmounts(), true);

				reservationPax.addPnrPaxFare(reservationPaxFare);
			}
		}

		ReservationCoreUtils.updateTotals(reservationPax, totalCharges, true);

		return com.isa.thinair.airreservation.api.utils.ReservationApiUtils.getTotalChargeAmount(totalCharges);
	}

	/**
	 * Clone reservation
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static Reservation clone(Reservation reservation, String newCarrierPNR, String newOriginatorPNR, String extRecLocator)
			throws ModuleException {
		try {
			if (reservation != null) {
				ReservationContactInfo reservationContactInfo = reservation.getContactInfo();
				ReservationAdminInfo reservationAdminInfo = reservation.getAdminInfo();
				Collection<ReservationSegment> segments = reservation.getSegments();
				Collection<OtherAirlineSegment> otherAirlineSegments = reservation.getOtherAirlineSegments();
				Date zuluBookingTimestamp = reservation.getZuluBookingTimestamp();

				reservation = (Reservation) BeanUtils.cloneBean(reservation);

				boolean isGdsOrCsPnr = false;
				Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
				if (ReservationApiUtils.isGdsReservation(reservation)) {
					isGdsOrCsPnr = true;
				} else if (csOCCarrirs != null && !csOCCarrirs.isEmpty()) {
					isGdsOrCsPnr = true;
				}
				reservation.setPnr(PNRNumberGeneratorProxy.getNewPNR(isGdsOrCsPnr, newCarrierPNR));
				reservation.setOriginatorPnr(newOriginatorPNR);
				reservation.setExternalRecordLocator(extRecLocator);
				reservation.setVersion(-1);
				reservation.setZuluBookingTimestamp(zuluBookingTimestamp);
				reservationContactInfo = clone(reservationContactInfo);
				reservationContactInfo.setPnr(reservation.getPnr());
				reservationContactInfo.setVersion(-1);
				reservationAdminInfo = cloneReservationAdminInfo(reservationAdminInfo);
				cloneSegments(reservation, segments);
				reservation.setContactInfo(reservationContactInfo);
				reservation.setAdminInfo(reservationAdminInfo);

				// Resetting the passenger counts and the total amounts to zero
				reservation.setTotalPaxAdultCount(0);
				reservation.setTotalPaxChildCount(0);
				reservation.setTotalPaxInfantCount(0);
				reservation.setTotalChargeAmounts(ReservationApiUtils.getChargeTypeAmounts());
				reservation.setBookingCategory(reservation.getBookingCategory());
				reservation.setOriginCountryOfCall(reservation.getOriginCountryOfCall());
				reservation.setItineraryFareMaskFlag(reservation.getItineraryFareMaskFlag());
				cloneOtherAirlineSegments(reservation, otherAirlineSegments);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return reservation;
	}

	/**
	 * Clone reservation contact information
	 * 
	 * @param reservationContactInfo
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationContactInfo clone(ReservationContactInfo reservationContactInfo) throws ModuleException {
		try {
			if (reservationContactInfo != null) {
				reservationContactInfo = (ReservationContactInfo) BeanUtils.cloneBean(reservationContactInfo);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return reservationContactInfo;
	}

	/**
	 * Clone reservation admin information
	 * 
	 * @param reservationAdminInfo
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationAdminInfo cloneReservationAdminInfo(ReservationAdminInfo reservationAdminInfo)
			throws ModuleException {
		try {
			if (reservationAdminInfo != null) {
				reservationAdminInfo = (ReservationAdminInfo) BeanUtils.cloneBean(reservationAdminInfo);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return reservationAdminInfo;
	}

	/**
	 * Clone reservation segment information
	 * 
	 * @param reservation
	 * @param reservationSegment
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationSegment cloneReservationSegment(Reservation reservation, ReservationSegment reservationSegment)
			throws ModuleException {
		try {
			if (reservationSegment != null) {
				reservationSegment = (ReservationSegment) BeanUtils.cloneBean(reservationSegment);
				reservationSegment.setReservation(reservation);
				reservationSegment.setPnrSegId(null);
				reservationSegment.setVersion(-1);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return reservationSegment;
	}
	
	private static OtherAirlineSegment cloneOtherAirlineSegment(Reservation reservation, OtherAirlineSegment reservationSegment)
			throws ModuleException {
		try {
			if (reservationSegment != null) {
				reservationSegment = (OtherAirlineSegment) BeanUtils.cloneBean(reservationSegment);
				reservationSegment.setReservation(reservation);
				reservationSegment.setPnrOtherAirlineSegId(null);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return reservationSegment;
	}

	/**
	 * Clone reservation segment information
	 * 
	 * @param reservation
	 * @param oldSegments
	 * @throws ModuleException
	 */
	private static void cloneSegments(Reservation reservation, Collection<ReservationSegment> oldSegments) throws ModuleException {
		if (oldSegments != null) {
			Iterator<ReservationSegment> itReservationSegment = oldSegments.iterator();
			ReservationSegment reservationSegment;

			while (itReservationSegment.hasNext()) {
				reservationSegment = (ReservationSegment) itReservationSegment.next();
				reservationSegment = cloneReservationSegment(reservation, reservationSegment);
				reservation.addSegment(reservationSegment);
			}
		}
	}
	
	private static void cloneOtherAirlineSegments(Reservation reservation, Collection<OtherAirlineSegment> oldSegments)
			throws ModuleException {
		reservation.setOtherAirlineSegments(null);
		if (oldSegments != null && !oldSegments.isEmpty()) {
			Iterator<OtherAirlineSegment> itReservationSegment = oldSegments.iterator();
			OtherAirlineSegment reservationSegment;

			while (itReservationSegment.hasNext()) {
				reservationSegment = (OtherAirlineSegment) itReservationSegment.next();
				reservationSegment = cloneOtherAirlineSegment(reservation, reservationSegment);
				reservation.addOtherAirlineSegment(reservationSegment);
			}
		}
	}

	public static BigDecimal getTotalCharges(Collection<ReservationPaxFare> colReservationPaxFare) {

		BigDecimal[] totalCharges = ReservationApiUtils.getChargeTypeAmounts();

		if (colReservationPaxFare != null) {
			Iterator<ReservationPaxFare> itReservationPaxFare = colReservationPaxFare.iterator();
			ReservationPaxFare reservationPaxFare;

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				ReservationCoreUtils.updateTotals(totalCharges, reservationPaxFare.getTotalChargeAmounts(), true);
			}
		}

		return ReservationApiUtils.getTotalChargeAmount(totalCharges);
	}
	
	private static void updatePaxBalanceDueAdjustmentCharges(Integer pnrPaxId, Integer fareId,
			Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap, Set<ReservationPaxOndCharge> oldCharges) {

		if (pnrPaxId != null && fareId != null && ondWisePaxDueAdjMap != null && ondWisePaxDueAdjMap.size() > 0
				&& ondWisePaxDueAdjMap.get(fareId) != null && oldCharges != null && oldCharges.size() > 0) {
			Map<Integer, Double> paxWiseDueAdjMap = ondWisePaxDueAdjMap.get(fareId);
			BigDecimal paxDueAdjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			Iterator<ReservationPaxOndCharge> oldChargesItr = oldCharges.iterator();

			while (oldChargesItr.hasNext()) {
				ReservationPaxOndCharge resPaxOndCharge = oldChargesItr.next();
				String chargeGroup = resPaxOndCharge.getChargeGroupCode();

				if (ChargeGroup.ADJ.equals(chargeGroup) && resPaxOndCharge.getAmount().doubleValue() < 0) {

					if (paxWiseDueAdjMap.get(pnrPaxId) != null) {
						paxDueAdjAmount = AccelAeroCalculator.parseBigDecimal(paxWiseDueAdjMap.get(pnrPaxId));

					}

					resPaxOndCharge.setAmount(paxDueAdjAmount);

					if (paxDueAdjAmount.doubleValue() == 0) {
						oldChargesItr.remove();
					}

				}
			}

		}
	}

	private static boolean applyPaxCancelationBalanceDueAdjustment(Integer pnrPaxId, Integer fareId,
			Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap, ReservationPaxOndCharge resPaxOndCharge) {
		boolean isAddCharge = true;
		if (pnrPaxId != null && fareId != null && resPaxOndCharge != null
				&& ChargeGroup.ADJ.equals(resPaxOndCharge.getChargeGroupCode()) && resPaxOndCharge.getAmount().doubleValue() == 0) {
			BigDecimal paxDueAdjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (ondWisePaxDueAdjMap != null && !ondWisePaxDueAdjMap.isEmpty() && ondWisePaxDueAdjMap.get(fareId) != null) {
				Map<Integer, Double> paxWiseDueAdjMap = ondWisePaxDueAdjMap.get(fareId);
				if (paxWiseDueAdjMap.get(pnrPaxId) != null) {
					paxDueAdjAmount = AccelAeroCalculator.parseBigDecimal(paxWiseDueAdjMap.get(pnrPaxId));
					if (paxDueAdjAmount.doubleValue() != 0) {
						// update the actual adjustment amount for the balance due.
						resPaxOndCharge.setAmount(paxDueAdjAmount);
					}
					paxWiseDueAdjMap.put(pnrPaxId, new Double(0));
				}
			}

			if (paxDueAdjAmount.doubleValue() == 0) {
				isAddCharge = false;
			}

		}
		return isAddCharge;
	}
	
	private static boolean applyPaxOndChargesDiscountInfo(ReservationPaxOndCharge ondCharge, Collection<Integer> flightSegIds,
			Collection<DiscountChargeTO> discountChargeTOs, double discountPercentage) {

		boolean isDiscountApplied = false;
		if (ondCharge != null && discountChargeTOs != null && !discountChargeTOs.isEmpty() && flightSegIds != null
				&& !flightSegIds.isEmpty()) {

			Integer rateId = ondCharge.getChargeRateId();
			if (ReservationInternalConstants.ChargeGroup.FAR.equals(ondCharge.getChargeGroupCode())) {
				rateId = ondCharge.getFareId();
			}

			for (DiscountChargeTO discountChargeTO : discountChargeTOs) {

				if ((CollectionUtils.containsAny(discountChargeTO.getFlightSegmentIds(), flightSegIds))
						&& discountChargeTO.getRateId().equals(rateId)
						// && discountChargeTO.getChargeCode().equals(chargeCode)
						&& discountChargeTO.getDiscountAmount().doubleValue() > 0
						&& !AccelAeroCalculator.isGreaterThan(discountChargeTO.getDiscountAmount(), ondCharge.getAmount())) {

					ondCharge.setDiscount(discountChargeTO.getDiscountAmount().negate());

					if (ReservationInternalConstants.ChargeGroup.FAR.equals(ondCharge.getChargeGroupCode())) {
						ondCharge.setDiscountValuePercentage((int) discountPercentage);
					}

					isDiscountApplied = true;
					break;
				}
			}

		}
		return isDiscountApplied;

	}

	private static ReservationSegment cloneReservationSegment(ReservationSegment reservationSegment) throws ModuleException {
		try {
			if (reservationSegment != null) {
				reservationSegment = (ReservationSegment) BeanUtils.cloneBean(reservationSegment);
				reservationSegment.setPnrSegId(null);
				reservationSegment.setVersion(-1);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return reservationSegment;
	}

	public static Map<Integer, ReservationSegment> cloneSegments(Collection<ReservationSegment> oldSegments)
			throws ModuleException {
		Map<Integer, ReservationSegment> clonedResSegmentByPnrSegId = new HashMap<Integer, ReservationSegment>();
		Collection<ReservationSegment> clonedObjects = new ArrayList<ReservationSegment>();
		if (oldSegments != null) {
			Iterator<ReservationSegment> itReservationSegment = oldSegments.iterator();
			ReservationSegment reservationSegment;

			while (itReservationSegment.hasNext()) {
				reservationSegment = (ReservationSegment) itReservationSegment.next();
				Integer pnrSegId = reservationSegment.getPnrSegId();
				reservationSegment = cloneReservationSegment(reservationSegment);
				clonedResSegmentByPnrSegId.put(pnrSegId, reservationSegment);
				clonedObjects.add(reservationSegment);
			}
		}
		return clonedResSegmentByPnrSegId;
	}
	
}
