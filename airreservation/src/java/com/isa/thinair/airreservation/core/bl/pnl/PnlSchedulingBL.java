/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pnl.PNLTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.PnlAdlTiming;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PnlAdlTimingDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * BL Method to get PNL/ADL Timings
 * 
 * @author Byorn de Silva
 * 
 */
public class PnlSchedulingBL {

	private static Log log = LogFactory.getLog(PnlSchedulingBL.class);
	private int defaultPnldepartureGap;
	private PnlAdlTimingDAO pnlAdlTimingDAO = null;

	/**
	 * Constructor Will Intitialize defaultPnldepartureGap and the FlightDAO
	 */
	public PnlSchedulingBL() {
		String pnlGap = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.PNL_DEPARTURE_GAP);

		this.defaultPnldepartureGap = Integer.parseInt(pnlGap);

		this.pnlAdlTimingDAO = ReservationDAOUtils.DAOInstance.PNL_ADL_TIMINGS_DAO;
	}

	/**
	 * Will Retrieve A List of FlightInfo to be Scheduled for the DefaultPnlDepGap.
	 * 
	 * @returns a Collection of PNLTransMissionDetailsDTO
	 */
	public ArrayList<PNLTransMissionDetailsDTO> getFlightForPnlAdlScheduling() throws ModuleException {

		Date date = new Date();

		ArrayList<PNLTransMissionDetailsDTO> list = pnlAdlTimingDAO.getFlightsForPNLScheduling(defaultPnldepartureGap, date);

		try {
			addAdditionalFlightsFallingOutOfDefaultGapRange(list, date);

			populateConfiguredTimings(list);
		} catch (Exception e) {
			log.error("############# [THROUGHCHECKIN] ERROR OCCURED WHEN MANIPULATING NEW TIMINGS", e);
		}

		return list;
	}
	
	public int getFlightForPnlAdlDepartureGap(PNLTransMissionDetailsDTO pnlTransMissionDetailsDTO) throws ModuleException {		
		
		populateTimingsForFlight(pnlTransMissionDetailsDTO);

		return pnlTransMissionDetailsDTO.getPnlDepartureGap();
	}
	

	/**
	 * 
	 * @param list
	 * @param date
	 */
	private void addAdditionalFlightsFallingOutOfDefaultGapRange(ArrayList<PNLTransMissionDetailsDTO> list, Date date) {

		List<PNLTransMissionDetailsDTO> additionalList = new ArrayList<PNLTransMissionDetailsDTO>();

		if (pnlAdlTimingDAO.hasAirportTiming(date)) {

			Collection<String[]> codeTimeArray = pnlAdlTimingDAO.getAirportTimings(date);
			Iterator<String[]> iterCodeTimeArray = codeTimeArray.iterator();

			while (iterCodeTimeArray.hasNext()) {

				String airportAndGap[] = (String[]) iterCodeTimeArray.next();
				String airportCode = airportAndGap[0];
				int newGap = Integer.valueOf(airportAndGap[1]).intValue();
				if (newGap > defaultPnldepartureGap) {
					List<PNLTransMissionDetailsDTO> airportFlights = pnlAdlTimingDAO.getFlightsForPNLSchedulingForAirport(date, this.defaultPnldepartureGap,
							newGap, airportCode);
					additionalList.addAll(airportFlights);
				}
			}
		}

		if (pnlAdlTimingDAO.hasFlightTimings(date)) {

			Collection<String[]> codeTimeArray = pnlAdlTimingDAO.getFlightTimings(date);
			Iterator<String[]> iterCodeTimeArray = codeTimeArray.iterator();

			while (iterCodeTimeArray.hasNext()) {

				String flightNumAndGap[] = (String[]) iterCodeTimeArray.next();
				String flightNumber = flightNumAndGap[0];
				int newGap = Integer.valueOf(flightNumAndGap[1]).intValue();
				if (newGap > defaultPnldepartureGap) {
					List<PNLTransMissionDetailsDTO> flights = pnlAdlTimingDAO.getFlightsForPNLScheduling(date, this.defaultPnldepartureGap, newGap,
							flightNumber);
					additionalList.addAll(flights);
				}
			}
		}

		list.addAll(additionalList);
	}

	/**
	 * If Some of the Flights Have a special configured Value, take from t_pnl_adl_timings table. The Same List's
	 * configured params will get Populated. See pnlDepartureGap and adlDepartureGap props in PNLTransMissionDetailsDTO
	 * 
	 * @param list
	 * @return
	 */
	private void populateConfiguredTimings(ArrayList<PNLTransMissionDetailsDTO> list) throws ModuleException {

		Iterator<PNLTransMissionDetailsDTO> iterPnlTnsDetail = list.iterator();
		while (iterPnlTnsDetail.hasNext()) {
			PNLTransMissionDetailsDTO transMissionDetailsDTO = (PNLTransMissionDetailsDTO) iterPnlTnsDetail.next();
			populateTimingsForFlight(transMissionDetailsDTO);

		}

	}

	/**
	 * transMissionDetailsDTO pnl gap and adl gap need to be populated If timing for flight exists this will be
	 * taken...timing for airport (global setting) will be overidden.
	 * 
	 * @param transMissionDetailsDTO
	 * @throws ModuleException
	 */
	private void populateTimingsForFlight(PNLTransMissionDetailsDTO transMissionDetailsDTO) throws ModuleException {

		// note : if has a timing for flight exists, then this will be taken first
		if (hasATimingForFlight(transMissionDetailsDTO)) {
			transMissionDetailsDTO = pnlAdlTimingDAO.getPnlAdlTiming(transMissionDetailsDTO);

			return;
		}

		if (hasATimingForAirport(transMissionDetailsDTO)) {
			transMissionDetailsDTO = pnlAdlTimingDAO.getPnlAdlAirportTiming(transMissionDetailsDTO);

		}

	}

	/**
	 * 
	 * @param transMissionDetailsDTO
	 * @return
	 * @throws ModuleException
	 */
	private boolean hasATimingForFlight(PNLTransMissionDetailsDTO transMissionDetailsDTO) throws ModuleException {
		return pnlAdlTimingDAO.hasPNLADLTiming(transMissionDetailsDTO.getFlightNumber(),
				transMissionDetailsDTO.getDepartureStation(), transMissionDetailsDTO.getDepartureTimeZulu());

	}

	/**
	 * 
	 * @param transMissionDetailsDTO
	 * @return
	 * @throws ModuleException
	 */
	private boolean hasATimingForAirport(PNLTransMissionDetailsDTO transMissionDetailsDTO) throws ModuleException {

		return pnlAdlTimingDAO.hasPNLADLTimingForAllFlightsInAirport(transMissionDetailsDTO.getDepartureStation(),
				transMissionDetailsDTO.getDepartureTimeZulu());
	}

	/**
	 * For Manual Screen When Savings A PNLADL Timing
	 * 
	 * @param timing
	 * @throws ModuleException
	 */
	public void savePnlAdlTiming(PnlAdlTiming timing) throws ModuleException {

		if (timing == null) {
			throw new ModuleException("airreservations.auxilliary.timing.null");
		}

		boolean isGlobalSetting = false;
		boolean isEditMode = timing.getVersion() < 0 ? false : true;
		// check if its a global setting. if true then flight number has to be
		// null
		if (timing.getApplyToAllFlights().equals(PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES)) {
			isGlobalSetting = true;
			timing.setFlightNumber(null);
		} else {
			timing.setApplyToAllFlights(PNLConstants.PnlAdlTimings.APPLY_TO_ALL_NO);
		}

		// if its a global setting perform a record check
		if (isGlobalSetting) {

			// check if the global setting is duplicating
			if (isEditMode) {

				if (pnlAdlTimingDAO.hasDuplicateGlobalPNLADLTimingEditMode(timing

				.getAirportCode(), timing.getStartingZuluDate(), timing.getEndingZuluDate(), timing.getId())) {
					throw new ModuleException("airreservations.auxilliary.overlappingairporttiming");

				}

			} else {
				if (pnlAdlTimingDAO.hasDuplicateGlobalPNLADLTiming(timing

				.getAirportCode(), timing.getStartingZuluDate(), timing.getEndingZuluDate())) {
					throw new ModuleException("airreservations.auxilliary.overlappingairporttiming");

				}
			}
		}
		// else perform a check for specific flight
		else {
			// check if for the flight number and airport an overlapping record
			// exists.

			if (isEditMode) {

				if (pnlAdlTimingDAO.isOverlappingPNLADLTimingEditMode(timing.getAirportCode(), timing.getFlightNumber(),
						timing.getStartingZuluDate(), timing.getEndingZuluDate(), timing.getId())) {
					throw new ModuleException("airreservations.auxilliary.overlappingtiming");
				}
			} else {

				if (pnlAdlTimingDAO.isOverlappingPNLADLTiming(timing.getAirportCode(), timing.getFlightNumber(),
						timing.getStartingZuluDate(), timing.getEndingZuluDate())) {
					throw new ModuleException("airreservations.auxilliary.overlappingtiming");
				}

			}
		}

		// if code comes here all constraints are passed. safe to save.
		pnlAdlTimingDAO.saveTiming(timing);

	}
}
