package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.SyncMessageIdentifier;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public abstract class TypeBReservationMessageCreator {
	
	SegmentsComposingStrategy segmentsComposingStrategy;
	
	PassengerNamesComposingStrategy passengerNamesComposingStrategy;
	
	public final TypeBRQ createSyncRequest(Reservation reservation, TypeBRequestDTO typeBRequestDTO) throws ModuleException {
		
		TypeBRQ typeBRQ = new TypeBRQ();
		BookingRequestDTO bookingRequestDTO = new BookingRequestDTO();
		List<BookingSegmentDTO> bookingSegmentDTOs = composeSegments(reservation, typeBRequestDTO);
		bookingRequestDTO.setBookingSegmentDTOs(bookingSegmentDTOs);
		List<SSRDTO> ssrDTOs = composePassengerNames(reservation, bookingRequestDTO, typeBRequestDTO);
		if (!composingInfantsSuccess(ssrDTOs)) {
			typeBRQ.setMsgCreationSuccess(false);			
		}
		int exisitngSSRCount = 0;
		if (ssrDTOs != null) {
			exisitngSSRCount = ssrDTOs.size();
		}
		if(typeBRequestDTO.getSsrAssemblerMap() == null){
			typeBRequestDTO.setSsrAssemblerMap(new HashMap<Integer, SegmentSSRAssembler>());
		}
		bookingRequestDTO.setSsrDTOs(addSsrDetails(ssrDTOs, typeBRequestDTO, reservation));
		if (!composingSSRDetailsSuccess(exisitngSSRCount, bookingRequestDTO.getSsrDTOs())) {
			typeBRQ.setMsgCreationSuccess(false);
		}
		bookingRequestDTO.setOsiDTOs(addOSIDetails(reservation, typeBRequestDTO));
		bookingRequestDTO.setResponseMessageIdentifier(getMessageIdentifier());
		bookingRequestDTO.setResponderRecordLocator(getResponderRecordLocator(reservation, typeBRequestDTO));
		setGDSConfigs(reservation, bookingRequestDTO);
		typeBRQ.setBookingRequestDTO(bookingRequestDTO);
		return typeBRQ;		
	}

	public void setGDSConfigs(Reservation reservation, BookingRequestDTO bookingRequestDTO) {
		bookingRequestDTO.setGds(GDSApiUtils.getGDSCode(reservation.getGdsId()));
		bookingRequestDTO.setMessageIdentifier(bookingRequestDTO.getMessageType().getValue());
		bookingRequestDTO.setOriginatorRecordLocator(TTYMessageCreatorUtil.createOriginatorRecordLocator(reservation));
		bookingRequestDTO.setMessageCategory(GDSInternalCodes.TypeBMessageCategory.SYNC_OUT);		
	}

	public final TypeBRQ createCSRequest(Reservation reservation, TypeBRequestDTO typeBRequestDTO) throws ModuleException {
		
		TypeBRQ typeBRQ = new TypeBRQ();
		BookingRequestDTO bookingRequestDTO = new BookingRequestDTO();
		bookingRequestDTO.setBookingSegmentDTOs(composeCSSegments(reservation, typeBRequestDTO));
		List<SSRDTO> ssrDTOs = composePassengerNames(reservation, bookingRequestDTO, typeBRequestDTO);
		if (!composingInfantsSuccess(ssrDTOs)) {
			typeBRQ.setMsgCreationSuccess(false);			
		}
		int exisitngSSRCount = 0;
		if (ssrDTOs != null) {
			exisitngSSRCount = ssrDTOs.size();
		}
		bookingRequestDTO.setSsrDTOs(addSsrDetails(ssrDTOs, typeBRequestDTO, reservation));
		if (!composingSSRDetailsSuccess(exisitngSSRCount, bookingRequestDTO.getSsrDTOs())) {
			typeBRQ.setMsgCreationSuccess(false);
		}
		List<OSIDTO> osiDTOs = addOSIDetails(reservation, typeBRequestDTO);
		if (!composingOSIDetailsSuccess(osiDTOs)) {
			typeBRQ.setMsgCreationSuccess(false);
		} else {
			bookingRequestDTO.setOsiDTOs(osiDTOs);		
		}
		bookingRequestDTO.setResponseMessageIdentifier(getCSMessageIdentifier());
		bookingRequestDTO.setResponderRecordLocator(getResponderRecordLocator(reservation, typeBRequestDTO));
		setCSConfigs(reservation, bookingRequestDTO, typeBRequestDTO);
		if (bookingRequestDTO.getBookingSegmentDTOs() != null && !bookingRequestDTO.getBookingSegmentDTOs().isEmpty()) {
			typeBRQ.setBookingRequestDTO(bookingRequestDTO);
		} else {
			typeBRQ.setMsgCreationSuccess(false);
		}
		return typeBRQ;
	}
	
	public boolean composingOSIDetailsSuccess(List<OSIDTO> osiDTOs) {
		return true;
	}

	public boolean composingSSRDetailsSuccess(int exisitngSSRCount, List<SSRDTO> ssrDTOs) {
		return true;
	}
	
	public boolean composingInfantsSuccess(List<SSRDTO> ssrDTOs) {
		return true;
	}

	public List<BookingSegmentDTO> composeCSSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {
		return segmentsComposingStrategy.composeCSSegments(reservation, typeBRequestDTO);
	}
	
	public List<SSRDTO> composePassengerNames(Reservation reservation, BookingRequestDTO bookingRequestDTO,
			TypeBRequestDTO typeBRequestDTO) {
		return passengerNamesComposingStrategy.composePassengerNames(reservation, bookingRequestDTO, typeBRequestDTO);
	}
	
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation)
			throws ModuleException {
		return ssrDTOs;
	}

	public List<OSIDTO> addOSIDetails(Reservation reservation, TypeBRequestDTO typeBRequestDTO) {
		List<OSIDTO> osiDTOs = new ArrayList<OSIDTO>();
		return osiDTOs;
	}
	
	public String getCSMessageIdentifier() {
		return null;
	}
		
	public void setCSConfigs(Reservation reservation, BookingRequestDTO bookingRequestDTO, TypeBRequestDTO typeBRequestDTO) {
		bookingRequestDTO.setGds(GDSApiUtils.getGDSCode(typeBRequestDTO.getCsOCCarrierCode()));
		bookingRequestDTO.setMessageIdentifier(bookingRequestDTO.getMessageType().getValue());
		bookingRequestDTO.setOriginatorRecordLocator(TTYMessageCreatorUtil.createOriginatorRecordLocator(reservation));
		bookingRequestDTO.setMessageCategory(GDSInternalCodes.TypeBMessageCategory.CODESHARE_OUT);
	}
	
	public List<BookingSegmentDTO> composeSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {
		return segmentsComposingStrategy.composeSegments(reservation, typeBRequestDTO);
	}

	public String getMessageIdentifier() {
		return SyncMessageIdentifier.SYNCHRONIZATION.getCode();
	}
	
	public RecordLocatorDTO getResponderRecordLocator(Reservation reservation, TypeBRequestDTO typeBRequestDTO){
		return TTYMessageCreatorUtil.createResponderRecordLocator(reservation);
	}
	
}
