/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerElement;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.BaseAdditionalInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerAdditions;

/**
 * @author udithad
 *
 */
public class SsrInformation extends
		PassengerAdditions<List<PassengerInformation>> {
	
	@Override
	public void populatePassengerInformation(
			List<PassengerInformation> detailsDTOs) {
		for (PassengerInformation information : detailsDTOs) {
			List<AncillaryDTO> ssrInformation = getSsrAncillaryInformationListBy(information
					.getPnrPaxId());
			information.setSsrs(ssrInformation);
		}
	}

	private List<AncillaryDTO> getSsrAncillaryInformationListBy(
			Integer passengerId) {
		return ssrAncillaryInformation.get(passengerId);
	}

	@Override
	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformation) {
		ssrAncillaryInformation = PnlAdlUtil
				.getPassengerSSRDetails(passengerInformation, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
	}

}
