package com.isa.thinair.airreservation.core.bl.tty;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class ModifySSRMessageCreator extends TypeBReservationMessageCreator {
	
	public ModifySSRMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}

	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation) throws ModuleException {
		List<SSRDetailDTO> ssrDetailsDTOs = TTYMessageCreatorUtil.composeSsrDetails(typeBRequestDTO.getSsrAssemblerMap(), reservation, typeBRequestDTO);
		if (!ssrDetailsDTOs.isEmpty()) {
			ssrDTOs.addAll(ssrDetailsDTOs);
		}
		return ssrDTOs;
	}
	
	@Override
	public boolean composingSSRDetailsSuccess(int exisitngSSRCount, List<SSRDTO> ssrDTOs) {
		if (exisitngSSRCount == ssrDTOs.size()) {
			return false;
		} else {
			return true;
		}
	}

}
