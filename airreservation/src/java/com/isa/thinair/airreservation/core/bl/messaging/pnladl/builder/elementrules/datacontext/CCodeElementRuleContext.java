/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext;


/**
 * @author udithad
 *
 */
public class CCodeElementRuleContext extends RulesDataContext {

	private boolean isRbdEnabled;

	public boolean isRbdEnabled() {
		return isRbdEnabled;
	}

	public void setRbdEnabled(boolean isRbdEnabled) {
		this.isRbdEnabled = isRbdEnabled;
	}

}
