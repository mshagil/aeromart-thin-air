/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext;

/**
 * @author udithad
 *
 */
public class RulesDataContext {

	private String currentLine;
	private String ammendingLine;
	private int reductionLength;
	
	public String getCurrentLine() {
		return currentLine;
	}
	public void setCurrentLine(String currentLine) {
		this.currentLine = currentLine;
	}
	public String getAmmendingLine() {
		return ammendingLine;
	}
	public void setAmmendingLine(String ammendingLine) {
		this.ammendingLine = ammendingLine;
	}
	public int getReductionLength() {
		return reductionLength;
	}
	public void setReductionLength(int reductionLength) {
		this.reductionLength = reductionLength;
	}
	
}
