/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.ChildElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.InfantElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class InfantElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private List<PassengerInformation> passengerInformations;

	private BaseRuleExecutor<RulesDataContext> infantElemeRuleExecutor = new InfantElementRuleExecutor();

	private InfantEticketElementBuilder eticketElementBuilder;
	private InfantDOCOElementBuilder docoElementBuilder;
	private InfantDOCSElementBuilder docsElementBuilder;

	private UtilizedPassengerContext clonedElimentContext;

	private boolean isStartWithNewLine = true;

	public InfantElementBuilder() {
		eticketElementBuilder = new InfantEticketElementBuilder();
		eticketElementBuilder
				.setConcatenationELementBuilder(new NewLineElementBuilder());
		docoElementBuilder = new InfantDOCOElementBuilder();
		docoElementBuilder
				.setConcatenationELementBuilder(new NewLineElementBuilder());
		docsElementBuilder = new InfantDOCSElementBuilder();
		docsElementBuilder
				.setConcatenationELementBuilder(new NewLineElementBuilder());
	}

	@Override
	public void buildElement(ElementContext context) {

		if (context != null && context.getFeaturePack() != null) {
			initContextData(context);
			infantElementTemplate();
		}
		executeNext();
	}

	private void buildEticketElement(ElementContext context) {
		eticketElementBuilder.buildElement(context);
	}

	private void buildDocoElement(ElementContext context) {
		docoElementBuilder.buildElement(context);
	}

	private void buildDocsElement(ElementContext context) {
		docsElementBuilder.buildElement(context);
	}

	private void infantElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		for (PassengerInformation passengerInformation : passengerInformations) {
			if (passengerInformation != null
					&& passengerInformation.getInfant() != null && isValiedInfantName(passengerInformation.getInfant())) {
				buildInfantElement(elementTemplate, passengerInformation);
				currentElement = elementTemplate.toString();
				if (currentElement != null && !currentElement.isEmpty()) {
					ammendmentPreValidation();
					createContextWith(passengerInformation);
					buildEticketElement(clonedElimentContext);
					buildDocsElement(clonedElimentContext);
					buildDocoElement(clonedElimentContext);
				}

			}
		}
	}
	
	private boolean isValiedInfantName(PassengerInformation pax){
		boolean isValied = false;
		if(!("TBA").equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getFirstName()))) && !("TBA")
				.equals(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(pax.getLastName())))){
					isValied = true;
				}
		
		return isValied;
	}

	private ElementContext createContextWith(
			PassengerInformation passengerInformation) {
		clonedElimentContext
				.setUtilizedPassengers(createPassengerInformations(passengerInformation));
		return clonedElimentContext;
	}

	private ArrayList<PassengerInformation> createPassengerInformations(
			PassengerInformation passengerInformation) {
		ArrayList<PassengerInformation> informations = new ArrayList<PassengerInformation>();
		informations.add(passengerInformation);
		return informations;
	}

	private void buildInfantElement(StringBuilder elementTemplate,
			PassengerInformation passengerInformation) {

		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMMyy");
		if (passengerInformation.getInfant() != null) {
			elementTemplate.setLength(0);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.INFT);
			elementTemplate.append(space());
			elementTemplate.append(MessageComposerConstants.HK + 1);
			elementTemplate.append(space());
			if (passengerInformation.getInfant().getDob() != null) {
				elementTemplate.append(dateFormatter.format(
						passengerInformation.getInfant().getDob())
						.toUpperCase());
				elementTemplate.append(space());
			}
			elementTemplate.append(passengerInformation.getInfant()
					.getLastName());
			elementTemplate.append(forwardSlash());
			elementTemplate.append(passengerInformation.getInfant()
					.getFirstName());
			if (passengerInformation.getInfant().getFirstName() != null) {
				elementTemplate.append(passengerInformation.getInfant()
						.getTitle());
			}
		}

	}

	private void ammendmentPreValidation() {
		RuleResponse response;
		if (isStartWithNewLine && currentElement != null
				&& !currentElement.isEmpty()) {
			executeConcatenationElementBuilder(uPContext);
		}
		response = validateSubElement(currentElement);
		if (response.isProceedNextElement()) {
			ammendMessageDataAccordingTo(currentElement);
		} else {
			if (response.getSuggestedElementText() != null
					&& response.getSuggestedElementText().length > 0) {
				ammendSuggestedElements(response);
			}
		}
	}

	private void ammendSuggestedElements(RuleResponse response) {
		for (int i = 0; i < response.getSuggestedElementText().length; i++) {
			if (i == 0) {
				ammendMessageDataAccordingTo(response.getSuggestedElementText()[i]);
			} else {
				if (isStartWithNewLine) {
					executeConcatenationElementBuilder(uPContext);
				}
				ammendMessageDataAccordingTo(MessageComposerConstants.PNLADLMessageConstants.REMARKS_CONT
						+ response.getSuggestedElementText()[i]);
			}
		}
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformations = getPassengerInUtilizedList();
		clonedElimentContext = new UtilizedPassengerContext();
		clonedElimentContext.setCurrentMessageLine(context
				.getCurrentMessageLine());
		clonedElimentContext.setMessageString(context.getMessageString());
		clonedElimentContext.setFeaturePack(context.getFeaturePack());
	}

	private void ammendMessageDataAccordingTo(String element) {
		if (currentElement != null && !currentElement.isEmpty()) {
			ammendToBaseLine(element, currentLine, messageLine);
		}
	}

	private List<PassengerInformation> getPassengerInUtilizedList() {
		List<PassengerInformation> passengerInformations = null;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformations = uPContext.getUtilizedPassengers();
		}
		return passengerInformations;
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = infantElemeRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
