/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.sql.RowSet;

import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.dto.CashPaymentDTO;
import com.isa.thinair.airreservation.api.dto.UserDetails;
import com.isa.thinair.airreservation.api.dto.ccc.InsuranceSaleDTO;
import com.isa.thinair.airreservation.api.model.CashSales;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceSalesType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;

/**
 * SalesDAO is the business DAO interface for the sales service apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface SalesDAO {
	
	/**
	 * used for manual screen.
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public Object[] getCashSalesHistory(Date fromDate, Date toDate);

	@SuppressWarnings("rawtypes")
	public Collection insertCashSalesToExternal(Collection<CashPaymentDTO> col) throws SQLException, ClassNotFoundException, Exception;

	/**
	 * Will clear the T_CASH_SALES prior to transfereing.
	 * 
	 * @param date
	 */
	public void clearCashSalesTable(Date date);	

	public Collection<CashSales> insertCashSalesToInternal(Collection<CashPaymentDTO> col);

	public ArrayList<UserDetails> getUsersForTravelAgent(String agentName);	

	public Collection<AgentTotalSaledDTO> getAgentTotalSales(Date startDate, final Date endDate);

	public Collection<CashPaymentDTO> getCashPayments(Date date);  

	public void updateInternalCashTable(Collection<CashSales> col);

	public BigDecimal getGeneratedCashSalesTotal(Date fromDate, Date toDate);

	public BigDecimal getTransferedCashSalesTotal(Date fromDate, Date toDate, XDBConnectionUtil util);	

	/**
	 * Retrive Cash Sales by date for a given date range.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 * @throws ModuleException
	 */
	public RowSet getSalesByDate(Collection<Date> dateList, Collection<Integer> nominalCodes);

	/**
	 * Get Insurance Sales Data
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public Collection<InsuranceSaleDTO> getInsuranceSalesData(Date fromDate, Date toDate, InsuranceSalesType salesType);

}
