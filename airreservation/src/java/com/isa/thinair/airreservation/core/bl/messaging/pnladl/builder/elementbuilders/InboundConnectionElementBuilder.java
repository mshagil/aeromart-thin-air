/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;

import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.InboundElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.MarketingFlightElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.PnrElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class InboundConnectionElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private PassengerInformation passengerInformation;

	private BaseRuleExecutor<RulesDataContext> inboundElementRuleExecutor = new InboundElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;

		currentElement = "";
		initContextData(context);
		inboundInformationElementTemplate();
		response = validateSubElement(currentElement);
		ammendMessageDataAccordingTo(response);
	}

	private void inboundInformationElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		if (passengerInformation != null
				&& passengerInformation.getInboundInfo() != null) {
			InboundConnectionDTO inboundInformation = passengerInformation
					.getInboundInfo();
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.INBOUND);
			elementTemplate
					.append((inboundInformation.getFlightNumber() != null) ? inboundInformation
							.getFlightNumber() : "");
			elementTemplate
					.append((inboundInformation.getFareClass() != null) ? inboundInformation
							.getFareClass() : "");
			elementTemplate
					.append((inboundInformation.getDate() != null) ? inboundInformation
							.getDate() : "");
			elementTemplate
					.append((inboundInformation.getDepartureStation() != null) ? inboundInformation
							.getDepartureStation() : "");
			elementTemplate
			.append((inboundInformation.getArrivalTime() != null) ? inboundInformation
					.getArrivalTime() : "");
			if(inboundInformation.getDateDifference() != 0 && inboundInformation.getDateDifference() > 0){
				elementTemplate.append("/");
				elementTemplate.append(inboundInformation.getDateDifference());
			}else if(inboundInformation.getDateDifference() != 0 && inboundInformation.getDateDifference() < 1){
				elementTemplate.append("/M");
				elementTemplate.append(inboundInformation.getDateDifference());
			}
			
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.HK);
			currentElement = elementTemplate.toString();
		}
	}

	private boolean validateSegment(InboundConnectionDTO inboundConnectionDTO) {
		boolean isValied = true;
		String departureAirportCode = uPContext.getDepartureAirportCode();
		String destinationAirportCode = uPContext.getDestinationAirportCode();

		if (departureAirportCode != null && destinationAirportCode != null
				&& !departureAirportCode.isEmpty()
				&& !destinationAirportCode.isEmpty()) {
			String segmentCode = destinationAirportCode + "/"
					+ departureAirportCode;
			if (inboundConnectionDTO.getSegmentCode() != null) {
				if (inboundConnectionDTO.getSegmentCode().equals(
						segmentCode)) {
					isValied = false;
				}
			}
		}

		return isValied;
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformation = getFirstPassengerInUtilizedList();
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (currentElement != null && !currentElement.isEmpty()) {
			if (response.isProceedNextElement()) {
				if (response.isSpaceAvailable()) {
					executeSpaceElementBuilder(uPContext);
				} else {
					executeConcatenationElementBuilder(uPContext);
				}
				ammendToBaseLine(currentElement, currentLine, messageLine);
			}
		}
		executeNext();
	}

	private PassengerInformation getFirstPassengerInUtilizedList() {
		PassengerInformation passengerInformation = null;
		int index = 0;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformation = uPContext.getUtilizedPassengers().get(index);
		}
		return passengerInformation;
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				"0"+currentLine.toString()+" ", elementText, 0);
		ruleResponse = inboundElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		PnrElementRuleContext rulesDataContext = new PnrElementRuleContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		rulesDataContext.setPnrWiseGroupCodes(uPContext.getPnrWiseGroupCodes());
		rulesDataContext.setPnrWisePassengerCount(uPContext
				.getPnrWisePassengerCount()
				.get(uPContext.getOngoingStoreType()));
		rulesDataContext.setPnrWisePaxReductionCout(uPContext
				.getPnrWisePassengerReductionCount().get(
						uPContext.getOngoingStoreType()));
		rulesDataContext.setUtilizedPassengerCount(uPContext
				.getUtilizedPassengers().size());
		rulesDataContext.setPnr(uPContext.getPnr());
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
