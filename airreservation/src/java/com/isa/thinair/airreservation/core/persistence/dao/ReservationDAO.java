/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airreservation.api.dto.CarrierDTO;
import com.isa.thinair.airreservation.api.dto.FlightConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldBookingInfoDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.OnholdReservatoinSearchDTO;
import com.isa.thinair.airreservation.api.dto.ResContactInfoSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationBasicsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupPaymentNotificationDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryStatusDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.NotificationDetailInfoDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.PnrFlightInfoDTO;
import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.airreservation.api.dto.onlinecheckin.PNRInfoDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVResInfoDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakInsuredTraveler;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.LmsBlockedCredit;
import com.isa.thinair.airreservation.api.model.OnholdAlert;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.airreservation.api.model.PromotionAttribute;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotificationEvent;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * ReservationDAO is the business DAO interface for the reservation service apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationDAO {
	/**
	 * Search Reservations (FAST LANE READER)
	 * 
	 * @param reservationSearchDTO
	 * @return
	 */
	public Collection<ReservationDTO> getReservations(ReservationSearchDTO reservationSearchDTO);

	/**
	 * Get customer's PNR List
	 * 
	 * @param customerId
	 * @return
	 */
	public List<String> getPnrList(int customerId);

	/**
	 * Search Passenger Credit (FAST LANE READER)
	 * 
	 * @param reservationSearchDTO
	 * @return
	 */
	public Collection<ReservationPaxDTO> getPassengerCredit(ReservationSearchDTO reservationSearchDTO, boolean withAllPaxCredits);

	/**
	 * Returns a Reservation
	 * 
	 * @param pnr
	 * @param loadFares
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Reservation getReservation(String pnr, boolean loadFares) throws CommonsDataAccessException;

	/**
	 * Returns the reservation by the originator PNR
	 * 
	 * @param originatorPnr
	 * @param loadFares
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Reservation getReservationByOriginatorPnr(String originatorPnr, boolean loadFares) throws CommonsDataAccessException;

	/**
	 * Return the reservation by gds Record Locator
	 * 
	 * @param creationDate
	 * @param loadFares
	 * @param originatorPnr
	 * 
	 * @return
	 */
	public Reservation getReservationByExtRecordLocator(String extRLoc, String creationDate, boolean loadFares)
			throws CommonsDataAccessException;

	public Reservation getCandidateParentReservationByExtRecordLocator(String extRLoc) throws CommonsDataAccessException;

	/**
	 * Returns passengers per pnr
	 * 
	 * @param pnr
	 * @param loadFares
	 * @return
	 */
	public Collection<ReservationPax> getPassengers(String pnr, boolean loadFares);

	/**
	 * Save the reservation
	 * 
	 * @param reservation
	 * @return The saved reservation.
	 */
	public Reservation saveReservation(Reservation reservation);

	/**
	 * Returns pnr segments which belongs for a pnr
	 * 
	 * @param pnr
	 * @return Collection of ReservationSegment
	 */
	public Collection<ReservationSegment> getSegments(String pnr);

	/**
	 * Returns Reservation contact information
	 * 
	 * @param pnr
	 * @return
	 */
	public ReservationContactInfo getContactInfo(String pnr) throws CommonsDataAccessException;

	/**
	 * Return reservation contact information (FAST LANE READER)
	 * 
	 * @param pnr
	 * @return
	 */
	public ReservationContactInfo getReservationContactInfo(String pnr) throws CommonsDataAccessException;

	/**
	 * Return contact infomation of the matching last booking
	 * 
	 * @param resContactInfoSearchDTO
	 *            Search criteria
	 */
	public ReservationContactInfo getLatestReservationContactInfo(ResContactInfoSearchDTO resContactInfoSearchDTO);

	/**
	 * Returns the next pnr sequence number
	 * 
	 * @return
	 */
	public String getNextPnrNumber(boolean isGdsReservation);

	/**
	 * Returns The eTicketSequence Number
	 * 
	 * @return
	 */
	public String getNextETicketSQID(String hasExtSeq);

	public Integer saveReservationInsurance(ReservationInsurance reservationInsurance);
	
	public void saveReservationInsurance(List<ReservationInsurance> listReservationInsurance);

	/**
	 * @param pnrList
	 * @param status
	 */
	public void updateReservationInsurancePublishStatus(Collection<String> pnrList, int status);

	public ReservationInsurance getReservationInsurance(Integer id);

	public List<ReservationInsurance> getOnholdReservationInsurance(String pnr);

	public ReservationInsurance getQuotedReservationInsurance(Integer id);

	public BigDecimal getInsuranceMaxValue(String pnr);

	public Collection<String> getCarrierCodes();

	public Collection<String> getCarrierCodesByAirline(String airlineCode);

	public Collection<String> getDuplicateNames(Collection<Integer> flightSegIds, ArrayList<String> adultNameList,
			ArrayList<String> childNameList, String pnr, Collection<Integer> paxIds);

	public void updatePnrSegmentNotifyStatus(
			Collection<ReservationSegmentNotificationEvent> reservationSegmentNotificationEventCol) throws ModuleException;

	public Collection<PnrFlightInfoDTO> getPNRListFromFlightSegment(Integer flightSegmentId);

	public Collection<CarrierDTO> getCarriers();

	public PnrFarePassenger getPnrFarePassenger(String pnr, int flightSegmentId, int paxSequence);

	public Collection<AncillaryStatusDTO> getPnrAncillaryStatus(String pnr);

	public Collection<AncillaryReminderDetailDTO> getFlightSegmentsToScheduleReminder(Date date, String schedulerType);

	public Collection<PassengerSeating> getReservationPaxSeatsList(String pnr);

	public Collection<PassengerMeal> getReservationPaxMealsList(String pnr);

	public Collection<PassengerBaggage> getReservationPaxBaggagesList(String pnr);

	public NotificationDetailInfoDTO getPnrSegListToSendReminder(List<AncillaryStatusDTO> pnrAnciList) throws ModuleException;

	public String getCurrentETicketSQID(String strSeq);

	public String getLastActualEticketForAgent(String agentCode);

	/***
	 * Retrieves reservation related insurance data based on the given status. For AIG , there is only one insurance
	 * created for a particular pnr, but for RAK there will be multiple insurance created for a single pnr, but there
	 * will always be a single SC (confirmed / success) insurance.
	 * 
	 * @param pnr
	 * @param status
	 * @return
	 */
	public Collection<ReservationInsurance> getReservationInsuranceList(String pnr);

	public Collection<RakInsuredTraveler> getTravellersForRakInsurancePublishing(int flightSegmentId);

	public Collection<RakInsuredTraveler> getTravellersForDailyRakInsurancePublishing(Date bookingDate);

	public void updateTnxGranularityFlag(String pnr, String status, String message, Long version);

	public Collection<String> getPnrsOfAlreadyTnxGranularityTracked();

	public void updatePnrsWithTnxGranularityFlag(Collection<String> pnrs, String status);

	public void updateReleaseTimeForDummyReservation(String pnr, Date releaseTimeStamp);

	public int getOnholdReservationCountByIp(String ipAddress, int salesChannel);

	public int getOnholdReservationCountByFlight(int flightId, int salesChannel);

	public int getOnholdReservationCountByPaxName(ReservationPax reservationPax, int salesChannel);

	public int getOnholdReservationCountByPaxEmail(String email, int salesChannel);

	public Collection<OnHoldReleaseTimeDTO> getOnHoldReleaseTime(OnHoldReleaseTimeDTO onHoldReleaseTimeDTO);

	public boolean isReservationTnxGranularityEnable(String pnr);

	public Collection<Integer> getUnflownReservationSegmentIds(String pnr, Date date);

	public Map<Integer, Integer> getSSRCount(int flightSegId, String cabinClassCode, String logicalCabinClass);

	/**
	 * returns onhold reservaions pnrs accroding to OnholdReservatoinSearchDTO minus the reservations for which email
	 * notifications have already been sent. The reservations for which email notifications have already been sent are
	 * from t_onhold_alert table.
	 * 
	 * @param onHoldReservationSearchDTO
	 * @return
	 */
	public Collection<String> getOnHoldReservationPnrsForEmailNotification(OnholdReservatoinSearchDTO onHoldReservationSearchDTO);

	public void saveOnholdAlert(OnholdAlert onholdAlert);

	public OnholdAlert getOnholdAlert(String pnr);

	public void saveAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo);

	public AutoCancellationInfo getAutoCancellationInfo(Integer autoCancellationId);

	public List<Integer> getExpiredAutoCancellationInfoList(Date currentDateTime);

	/**
	 * Return expired insurance based on auto cancellation flag
	 * 
	 * @param cancellationIds
	 * @param marketingCarrier
	 * @return
	 */
	public List<ReservationLiteDTO> getExpiredInsuranceAvailability(Collection<Integer> cancellationIds, String marketingCarrier);

	/**
	 * Return expired lcc reservations due to insurance expire based on auto cancellation flag
	 * 
	 * @param marketingCarrier
	 * @param cancellationIds
	 * 
	 * @return
	 */
	public Map<String, Integer> getExpiredLccInsuranceInfo(String marketingCarrier, Collection<Integer> cancellationIds);

	public void updateAutoCancelSchedulerStatus(List<Integer> autoCnxIds, String schedulerStatus);

	public void saveOrUpdatePromotionForReservation(Collection<PromotionAttribute> colPromotionAttribute);

	public List<OnlineCheckInReminderDTO> getDateForOnlineCheckInReminder(Date date);

	public void saveWLReservationPriority(Collection<ReservationWLPriority> reservationWLPriority);

	public Integer getWLReservationPriority(Integer flightSegId);

	public Collection<ReservationWLPriority> getWaitListedReservations(Integer flightSegId);

	public List<ReservationWLPriority> getWaitListedPrioritiesForSegment(Integer pnrSegmentId);

	public ReservationWLPriority getWaitListedReservationPriority(String pnr, Integer pnrSegId);

	public Collection<ReservationPaxDTO> searchReservations(ReservationSearchDTO searchDTO);

	// for promo code
	/**
	 * 
	 * @param inputParam
	 * @param promoCriteriaID
	 * @return
	 */
	public Reservation getReservationByPromoCriteria(String inputParam, long promoCriteriaID);

	public List<OnlineCheckInReminderDTO> getFlightsSegsForOnlineCheckInReminder(Date date, String schedulerType);

	public List<PNRInfoDTO> getInfoForOnlineCheckinReminder(Integer flightSegId);

	public List<ReservationBasicsDTO> getFlightSegmentPAXDetails(Integer flightSegId, String logicalCabinClass);

	public List<PNRGOVResInfoDTO> getResInfoListForFlightSegment(Integer flightSegmentID);

	public List<GroupPaymentNotificationDTO> getGrooupBookingForReminders();

	public List<ReservationDTO> getImmediateReservations(int customerId, int count);

	public List<OnHoldBookingInfoDTO> getOnholdPnrsForPaymentReminder(Date toDate);

	public ArrayList<Reservation> loadReservationsHavingAirportTransfers(List<Integer> flightSegIds);
	
	public String isPFSReceivedFromOperatingCarrier(String pnr, String flightNo, Date depDate, String pfsCategory);

	public Collection<Reservation> getReservationsByPnr(Collection<String> pnrList, boolean loadFares)
			throws CommonsDataAccessException;
	
	public Collection<Reservation> getReservationsByFlightSegmentId(Integer flightSegId, boolean loadFares)
			throws ModuleException;

	public List<ReservationPaxDetailsDTO> getPAXDetails(Integer flightID, int page);

	public Integer getPAXDetailsCount(Integer flightID);

	public List<FlightConnectionDTO> getConnectionDetials(String pnr);
	
	public Collection<Map<String, String>> getUnflownPNRsForFFID(String ffid);
	
	public Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId);

	public int removeFFIDFromUnflownPNRs(String ffid, Collection<Integer> colPNRs);
	
	public void saveLmsBlockCreditInfo(LmsBlockedCredit lmsBlockedCredit);	
	
	public boolean hasActionedByIBE(String pnr);
	
	public LmsBlockedCredit getLmsBlockCreditInfoByTmpTnxId(Integer tempTnxId);
	
	public Collection<LmsBlockedCredit> getOldLMSBlockedCreditInfoIds(Date fromDateTime);
	
	public void updateLmsBlockCreditStatus(Integer tempTnxId, String creditUtilizedStatus, String remarks);
	
	public Collection<Integer> getLMSBlockedTnxTempPayIdsByPnr(Collection<String> pnrs);

	public Map<Integer, String> getActiveUserDefinedSSRDetails();
	
	public Collection<LmsBlockedCredit> getOldUnconsumedLMSBlockedCreditInfoIds(Date fromDateTime) throws CommonsDataAccessException;

	public Integer getLmsBlockCreditInfoByRewardId(String[] rewardId);

	public LmsBlockedCredit getLmsBlockCreditInfoById(Integer lmsCreditBlockedInfoId);


	/**
	 * Returns the PNR List
	 *
	 * @param flightNumber
	 * @return
	 */
	public Collection<String> getPnrList(String flightNumber, Date deptDate);

    public void updateReservationContactData(ReservationContactInfo reservationContactInfo);

}
