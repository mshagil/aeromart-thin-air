package com.isa.thinair.airreservation.core.bl.revacc.breakdown.migrator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationCreditTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO.ReservationPaxChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.service.RevenueAccountBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFullFillmentDeriverBO;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityRules;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 * 
 * @since August 25, 2010
 */
public class TnxGranularityMigrator {

	private enum ChargeType {
		FRESH, REFUNDABLES, ADJUSTMENTS
	};

	private final String pnr;

	public TnxGranularityMigrator(String pnr) {
		this.pnr = pnr;
	}

	public void execute(Reservation reservation) throws Exception {

		Map<Integer, Collection<ReservationTnx>> mapReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
				.getAllBalanceTransactions(reservation.getPnrPaxIds());

		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				Collection<ReservationTnx> colAllTransactions = mapReservationTnx.get(reservationPax.getPnrPaxId());

				List<ReservationTnx> lstAllTransactions = new ArrayList<ReservationTnx>();
				lstAllTransactions.addAll(colAllTransactions);

				Collections.sort(lstAllTransactions, new AscSortComparator());
				Collection<Date> dates = new ArrayList<Date>();
				Collection<Integer> processedReservationTnxIds = new ArrayList<Integer>();

				for (ReservationTnx reservationTnx : lstAllTransactions) {

					if (ReservationTnxNominalCode.TICKET_PURCHASE.getCode() == reservationTnx.getNominalCode()
							|| ReservationTnxNominalCode.CREDIT_ACQUIRE.getCode() == reservationTnx.getNominalCode()
							|| (ReservationTnxNominalCode.INFANT_TRANSFER.getCode() == reservationTnx.getNominalCode() && reservationTnx
									.getTnxType().equals("DR"))) { // ALL DR records. --> no need to add the payment
																	// breakdown
						processedReservationTnxIds.add(reservationTnx.getTnxId());
					} else {
						// All payment records.Shoud link with fresh chargers.
						if (ReservationTnxNominalCode.getPaymentTypeNominalCodes().contains(reservationTnx.getNominalCode())) {

							ReservationPaymentMetaTO reservationPaymentMetaTO = getReservationPaymentMetaTO(reservation, null,
									reservationTnx.getDateTime(), ChargeType.FRESH, null);

							saveReservationPaxTnxBreakdownForPayment(reservationTnx, reservationPaymentMetaTO
									.getMapPaxWisePaymentMetaInfo().get(Integer.valueOf(reservationTnx.getPnrPaxId())));
							dates.add(reservationTnx.getDateTime());
							processedReservationTnxIds.add(reservationTnx.getTnxId());

						} else if (// Linking all Refundable chargers with the payments.Must link with a existing
									// payment breakdown
						(ReservationTnxNominalCode.INFANT_TRANSFER.getCode() == reservationTnx.getNominalCode() && reservationTnx
								.getTnxType().equals("CR"))
								|| (ReservationTnxNominalCode.ADDED_CHARGES.getCode() == reservationTnx.getNominalCode() && reservationTnx
										.getTnxType().equals("CR"))
								|| (ReservationTnxNominalCode.PAX_CANCEL.getCode() == reservationTnx.getNominalCode() && reservationTnx
										.getTnxType().equals("CR"))) {
							Date fromDate = getDate(dates, false);
							ReservationPaymentMetaTO reservationPaymentMetaTO = getReservationPaymentMetaTO(reservation,
									fromDate, reservationTnx.getDateTime(), ChargeType.REFUNDABLES, null);

							saveReservationPaxTnxBreakdownForRefundables(
									reservationTnx.getPnrPaxId(),
									reservationTnx,
									reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().get(
											Integer.valueOf(reservationTnx.getPnrPaxId())));
							dates.add(reservationTnx.getDateTime());
							processedReservationTnxIds.add(reservationTnx.getTnxId());

						} else if (ReservationTnxNominalCode.ALTER_CHARGE.getCode() == reservationTnx.getNominalCode()
								|| ReservationTnxNominalCode.CANCEL_CHARGE.getCode() == reservationTnx.getNominalCode()) {
							Date fromDate = getDate(dates, true);
							ReservationPaymentMetaTO reservationPaymentMetaTO = getReservationPaymentMetaTO(reservation,
									fromDate, reservationTnx.getDateTime(), ChargeType.FRESH, null);

							Collection<ReservationCredit> colReservationCredit = getReservationCredits(
									new Integer(reservationTnx.getPnrPaxId()), reservation, lstAllTransactions,
									processedReservationTnxIds);

							saveReservationPaxTnxBrkForAlterCharges(
									reservationTnx.getPnrPaxId(),
									reservationTnx.getNominalCode(),
									reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().get(
											new Integer(reservationTnx.getPnrPaxId())), colReservationCredit);
							dates.add(reservationTnx.getDateTime());
							processedReservationTnxIds.add(reservationTnx.getTnxId());

						} else if ((ReservationTnxNominalCode.ADDED_CHARGES.getCode() == reservationTnx.getNominalCode() && reservationTnx
								.getTnxType().equals("DR"))
								|| (ReservationTnxNominalCode.PAX_CANCEL.getCode() == reservationTnx.getNominalCode() && reservationTnx
										.getTnxType().equals("DR"))) {
							ReservationPaymentMetaTO reservationPaymentMetaTO = getReservationPaymentMetaTO(reservation, null,
									reservationTnx.getDateTime(), ChargeType.FRESH, null);

							Collection<ReservationCredit> colReservationCredit = getReservationCredits(
									new Integer(reservationTnx.getPnrPaxId()), reservation, lstAllTransactions,
									processedReservationTnxIds);

							saveReservationPaxTnxBrkForNewCharges(
									reservationTnx.getPnrPaxId(),
									reservationTnx.getNominalCode(),
									reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().get(
											new Integer(reservationTnx.getPnrPaxId())), colReservationCredit);
							dates.add(reservationTnx.getDateTime());
							processedReservationTnxIds.add(reservationTnx.getTnxId());
						} else if (ReservationTnxNominalCode.CREDIT_ADJUST.getCode() == reservationTnx.getNominalCode()) {
							ReservationPaymentMetaTO reservationPaymentMetaTO = getReservationPaymentMetaTO(reservation, null,
									reservationTnx.getDateTime(), ChargeType.ADJUSTMENTS, reservationTnx.getAmount());

							Collection<ReservationCredit> colReservationCredit = getReservationCredits(
									new Integer(reservationTnx.getPnrPaxId()), reservation, lstAllTransactions,
									processedReservationTnxIds);

							if (reservationTnx.getAmount().doubleValue() < 0) {
								Collection<Integer> currentTnxIds = new ArrayList<Integer>();
								currentTnxIds.add(reservationTnx.getTnxId());
								Collection<ReservationCredit> currentReservationCredit = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO
										.getReservationCredits(reservationTnx.getPnrPaxId(), currentTnxIds);
								ReservationCredit reservationCredit = BeanUtils.getFirstElement(currentReservationCredit);
								reservationCredit.setBalance(reservationTnx.getAmount().abs());

								colReservationCredit.add(reservationCredit);
							}

							saveReservationPaxTnxBreakdownForAdjustments(
									reservationTnx.getPnrPaxId(),
									reservationTnx,
									reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().get(
											new Integer(reservationTnx.getPnrPaxId())), colReservationCredit,
									reservationTnx.getAmount());

							dates.add(reservationTnx.getDateTime());
							processedReservationTnxIds.add(reservationTnx.getTnxId());
						} else if (ReservationTnxNominalCode.CREDIT_CF.getCode() == reservationTnx.getNominalCode()) {
							throw new IllegalArgumentException(pnr + " Invalid NominalCode[" + reservationTnx.getNominalCode()
									+ "]");

							// Date fromDate = getDate(dates);
							//
							// List<ReservationTnx> newAllTnx = new ArrayList<ReservationTnx>();
							// newAllTnx.addAll(lstAllTransactions);
							// Collections.sort(newAllTnx, new DescSortComparator());
							//
							// Collection<ReservationCredit> colReservationCredit = getReservationCredits(new
							// Integer(reservationTnx.getPnrPaxId()),
							// reservation, newAllTnx, fromDate, reservationTnx.getDateTime());
							//
							// saveReservationPaxTnxBreakdownForCarryForwardCredit(reservationTnx.getPnrPaxId(),
							// reservationTnx.getAmount(), reservationTnx.getNominalCode(), colReservationCredit);
							//
							// dates.add(reservationTnx.getDateTime());
						} else if (ReservationTnxNominalCode.getRefundTypeNominalCodes()
								.contains(reservationTnx.getNominalCode())) {
							throw new IllegalArgumentException(pnr + " Invalid NominalCode[" + reservationTnx.getNominalCode()
									+ "]");

							// Date fromDate = getDate(dates);
							//
							// Collection<ReservationCredit> colReservationCredit = getReservationCredits(new
							// Integer(reservationTnx.getPnrPaxId()),
							// reservation, lstAllTransactions, fromDate, reservationTnx.getDateTime());
							//
							// saveReservationPaxTnxBreakdownForRefund(reservationTnx, colReservationCredit);
						} else {
							throw new IllegalArgumentException(pnr + " Invalid NominalCode[" + reservationTnx.getNominalCode()
									+ "]");
						}
					}
				}
			}
		}

		Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> mapPerPaxWiseOndPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getPerPaxWiseOndChargeIds(reservation.getPnrPaxIds());

		checkTotalChargesAreMatching(reservation, mapPerPaxWiseOndPayments);
	}

	/**
	 * @return the reservation
	 * @throws Exception
	 */
	public Reservation getReservation() throws Exception {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

	private static Map<Long, Collection<ReservationPaxOndPayment>> getRemainingPaymentMetaInfo(Collection<Long> colPaymentTnxIds,
			Collection<String> colChargeGroupCodes) throws ModuleException {
		Map<Long, Collection<ReservationPaxOndPayment>> mapPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getReservationPaxOndPayments(colPaymentTnxIds);

		Map<Long, Collection<ReservationPaxOndPayment>> remainingAmounts = new HashMap<Long, Collection<ReservationPaxOndPayment>>();

		for (Entry<Long, Collection<ReservationPaxOndPayment>> entry : mapPayments.entrySet()) {
			Long paymentTnxId = entry.getKey();
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = entry.getValue();

			Collection<ReservationPaxOndPayment> colPayments = TnxGranularityUtils
					.getPaymentMetaInfo(colReservationPaxOndPayment);
			Collection<ReservationPaxOndPayment> colRefunds = TnxGranularityUtils.getRefundMetaInfo(colReservationPaxOndPayment);

			remainingAmounts.put(paymentTnxId, searchRemaininingPayments(colPayments, colRefunds, colChargeGroupCodes));
		}

		return remainingAmounts;
	}

	private static Collection<ReservationPaxOndPayment> searchRemaininingPayments(
			Collection<ReservationPaxOndPayment> colPayments, Collection<ReservationPaxOndPayment> colRefunds,
			Collection<String> colChargeGroupCodes) {

		Collection<ReservationPaxOndPayment> remainingPayments = new ArrayList<ReservationPaxOndPayment>();

		for (String chargeGroupCode : colChargeGroupCodes) {
			Collection<ReservationPaxOndPayment> selectedPayments = TnxGranularityUtils.getApplicableChargeTypes(chargeGroupCode,
					colPayments);

			if (selectedPayments.size() > 0) {
				Collection<ReservationPaxOndPayment> selectedRefunds = TnxGranularityUtils.getApplicableChargeTypes(
						chargeGroupCode, colRefunds);

				if (selectedRefunds.size() == 0) {
					remainingPayments.addAll(selectedPayments);
				} else {
					Map<Long, BigDecimal> mapPayments = TnxGranularityUtils.getOndChargeWiseAmounts(selectedPayments);
					Map<Long, BigDecimal> mapRefunds = TnxGranularityUtils.getOndChargeWiseAmounts(selectedRefunds);

					for (Entry<Long, BigDecimal> entry : mapPayments.entrySet()) {
						BigDecimal paymentAmount = entry.getValue();
						BigDecimal refundAmount = mapRefunds.get(entry.getKey()).abs();

						BigDecimal difference = AccelAeroCalculator.subtract(paymentAmount, refundAmount);
						if (difference.doubleValue() > 0) {
							ReservationPaxOndPayment reservationPaxOndPayment = BeanUtils.getFirstElement(selectedPayments);
							Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
							reservationPaxOndPayment = reservationPaxOndPayment.clone();
							reservationPaxOndPayment.setAmount(difference);
							reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);
							remainingPayments.add(reservationPaxOndPayment);
						}
					}
				}
			}
		}

		return remainingPayments;
	}

	private static void saveReservationPaxTnxBreakdownForCarryForwardCredit(String pnrPaxId, BigDecimal carryForwardAmount,
			int nominalCode, Collection<ReservationCredit> colReservationCredit) throws ModuleException {

		Collection<String> colChargeGroupCodes = TnxGranularityRules.getCreditCarryForwardChargeGroupFulFillmentOrder();
		Collection<Long> colPaymentTnxIds = TnxGranularityUtils.getPaymentIds(colReservationCredit);
		Map<Long, Collection<ReservationPaxOndPayment>> remainingPayments = getRemainingPaymentMetaInfo(colPaymentTnxIds,
				colChargeGroupCodes);
		BigDecimal remainingAmount = carryForwardAmount;

		Collection<ReservationPaxOndPayment> composedReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		for (ReservationCredit reservationCredit : colReservationCredit) {
			if (remainingAmount.doubleValue() > 0) {

				if (reservationCredit.getBalance().doubleValue() > 0) {
					Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = remainingPayments.get(reservationCredit
							.getTnxId());

					if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {

						for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {

							if (remainingAmount.doubleValue() > 0) {

								BigDecimal amount = reservationPaxOndPayment.getAmount();
								Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
								reservationPaxOndPayment = reservationPaxOndPayment.clone();
								reservationPaxOndPayment.setRemarks(null);
								reservationPaxOndPayment.setNominalCode(nominalCode);
								reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);
								if (remainingAmount.doubleValue() > amount.doubleValue()) {
									reservationPaxOndPayment.setAmount(amount.negate());
									remainingAmount = AccelAeroCalculator.subtract(remainingAmount, amount);
								} else {
									reservationPaxOndPayment.setAmount(remainingAmount.negate());
									remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
								}
								composedReservationPaxOndPayment.add(reservationPaxOndPayment);
							} else {
								break;
							}
						}
					}
				}
			} else {
				break;
			}
		}

		if (composedReservationPaxOndPayment != null && composedReservationPaxOndPayment.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.saveReservationPaxOndPayment(composedReservationPaxOndPayment);
		}
	}

	private static void saveReservationPaxTnxBreakdownForAdjustments(String pnrPaxId, ReservationTnx operationTnx,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, Collection<ReservationCredit> colReservationCredit,
			BigDecimal amount) throws ModuleException {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		// Start processing for minus adjustments
		Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO = reservationPaxPaymentMetaTO
				.getColPerPaxWiseOndNewAdjustments(true);

		if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {
			for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : colReservationPaxChargeMetaTO) {
				ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
				reservationPaxOndPayment.setPnrPaxId(pnrPaxId.toString());
				reservationPaxOndPayment.setAmount(reservationPaxChargeMetaTO.getAmount());
				reservationPaxOndPayment.setPaymentTnxId(new Long(operationTnx.getTnxId()));
				reservationPaxOndPayment.setPnrPaxOndChgId(new Long(reservationPaxChargeMetaTO.getPnrPaxOndChgId()));
				reservationPaxOndPayment.setChargeGroupCode(reservationPaxChargeMetaTO.getChargeGroupCode());
				reservationPaxOndPayment.setNominalCode(ReservationTnxNominalCode.CREDIT_ADJUST.getCode());
				reservationPaxOndPayment.setRemarks(reservationPaxChargeMetaTO.getRemarks());

				colReservationPaxOndPayment.add(reservationPaxOndPayment);
			}
		}

		// Start processing for plus adjustments + new charges
		colReservationPaxChargeMetaTO = reservationPaxPaymentMetaTO.getColPerPaxWiseOndNewAdjustments(false);

		colReservationPaxChargeMetaTO.addAll(reservationPaxPaymentMetaTO.getColReservationPaxChargeMetaTOForNewCharges());

		if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {
			// Fill in for the cancellation
			colReservationPaxOndPayment.addAll(fillInNewChargesFromAvailableCredits(pnrPaxId,
					ReservationTnxNominalCode.CREDIT_ADJUST.getCode(), colReservationPaxChargeMetaTO, colReservationCredit));
		}

		if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
		}
	}

	private Collection<ReservationCredit> getReservationCredits(Integer pnrPaxId, Reservation reservation,
			List<ReservationTnx> lstAllTransactions, Collection<Integer> processedReservationTnxIds) throws ModuleException {

		Collection<ReservationTnx> colReservationTnx = getReservationTnx(lstAllTransactions, processedReservationTnxIds);
		Collection<Integer> tnxIds = getTransactionIds(colReservationTnx);
		Collection<ReservationCredit> colReservationCredit = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO
				.getReservationCredits(pnrPaxId.toString(), tnxIds);

		BalanceCreditMigrator balanceCreditMigrator = new BalanceCreditMigrator(colReservationTnx, colReservationCredit);
		balanceCreditMigrator.execute();

		return colReservationCredit;
	}

	private Collection<ReservationTnx> getReservationTnx(List<ReservationTnx> lstAllTransactions,
			Collection<Integer> processedReservationTnxIds) {
		Collection<ReservationTnx> filteredReservationTnx = new ArrayList<ReservationTnx>();

		for (ReservationTnx reservationTnx : lstAllTransactions) {
			if (processedReservationTnxIds.contains(reservationTnx.getTnxId())) {
				filteredReservationTnx.add(reservationTnx);
			}
		}

		return filteredReservationTnx;
	}

	private Collection<Integer> getTransactionIds(Collection<ReservationTnx> colReservationTnx) {
		Collection<Integer> transactionIds = new ArrayList<Integer>();

		for (ReservationTnx reservationTnx : colReservationTnx) {
			transactionIds.add(reservationTnx.getTnxId());
		}

		return transactionIds;
	}

	private Date getDate(Collection<Date> dates, boolean isSecondLast) {
		if (dates.size() > 0) {
			if (isSecondLast && dates.size() > 1) {
				Object[] list = dates.toArray();
				return (Date) list[list.length - 2];
			} else {
				return (Date) BeanUtils.getLastElement(dates);
			}
		} else {
			return null;
		}
	}

	private void checkTotalChargesAreMatching(Reservation reservation,
			Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> mapPerPaxWiseOndPayments) throws ModuleException {

		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				pnrPaxIds.add(reservationPax.getPnrPaxId());
			}
		}

		RevenueAccountBD revenueAccountBD = ReservationModuleUtils.getRevenueAccountBD();
		Map mapPaxBalances = revenueAccountBD.getPNRPaxBalances(pnrPaxIds);

		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				Map<Long, Collection<ReservationPaxOndPayment>> mapOndPayments = mapPerPaxWiseOndPayments.get(reservationPax
						.getPnrPaxId());

				// This means there should be valid payment to check the validy. There could be OHD reservations that
				// could get cancel at the end
				// in that case we will not be tracking them in the TnxGranularity level. So no point of checking the
				// charges and payments matching
				// or not
				if (mapOndPayments != null && mapOndPayments.size() > 0) {
					BigDecimal balance = (BigDecimal) mapPaxBalances.get(reservationPax.getPnrPaxId());

					if (balance == null) {
						throw new IllegalArgumentException("balance can not be empty");
					}

					if (balance.doubleValue() <= 0) {
						BigDecimal totalOndPayment = getTotalOndPayment(mapOndPayments);
						BigDecimal reservationTotalChargeAmount = getPassengerCharges(reservationPax);
						BigDecimal difference = AccelAeroCalculator.subtract(reservationTotalChargeAmount, totalOndPayment).abs();
						if (difference.doubleValue() > 0.01) {
							System.out.println("reservationTotalChargeAmount " + reservationTotalChargeAmount);
							System.out.println("amount " + totalOndPayment);
							throw new IllegalArgumentException("charges does not match[case balance <=0]");
						}
					} else {
						BigDecimal totalOndPayment = AccelAeroCalculator.add(getTotalOndPayment(mapOndPayments), balance);
						BigDecimal reservationTotalChargeAmount = getPassengerCharges(reservationPax);
						BigDecimal difference = AccelAeroCalculator.subtract(reservationTotalChargeAmount, totalOndPayment).abs();
						if (difference.doubleValue() > 0.01) {
							System.out.println("reservationTotalChargeAmount " + reservationTotalChargeAmount);
							System.out.println("amount " + totalOndPayment);
							throw new IllegalArgumentException("charges does not match[case balance >0]");
						}
					}
				}
			}
		}
	}

	private BigDecimal getTotalOndPayment(Map<Long, Collection<ReservationPaxOndPayment>> mapOndPayments) {
		Collection<Collection<ReservationPaxOndPayment>> colcolReservationPaxOndPayment = mapOndPayments.values();

		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (Collection<ReservationPaxOndPayment> collection : colcolReservationPaxOndPayment) {
			for (ReservationPaxOndPayment reservationPaxOndPayment : collection) {
				if (!ReservationTnxNominalCode.getRefundTypeNominalCodes().contains(reservationPaxOndPayment.getNominalCode())) {
					amount = AccelAeroCalculator.add(amount, reservationPaxOndPayment.getAmount());
				}
			}
		}

		return amount;
	}

	private BigDecimal getPassengerCharges(ReservationPax reservationPax) {
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
			amount = AccelAeroCalculator.add(amount, getPerPaxChargeAmount(reservationPax));
			ReservationPax infant = BeanUtils.getFirstElement((Collection<ReservationPax>) reservationPax.getInfants());
			amount = AccelAeroCalculator.add(amount, getPerPaxChargeAmount(infant));
		} else {
			amount = AccelAeroCalculator.add(amount, getPerPaxChargeAmount(reservationPax));
		}

		return amount;
	}

	private BigDecimal getPerPaxChargeAmount(ReservationPax reservationPax) {
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (ReservationPaxFare reservationPaxFare : (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
			for (ReservationPaxOndCharge reservationPaxOndCharge : (Collection<ReservationPaxOndCharge>) reservationPaxFare
					.getCharges()) {
				amount = AccelAeroCalculator.add(amount, reservationPaxOndCharge.getEffectiveAmount());
			}
		}

		return amount;
	}

	private static Collection<ReservationPaxOndCharge> getReservationPaxOndCharges(
			Collection<ReservationPaxOndCharge> allCharges, Date fromDate, Date toDate) {
		Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();

		for (ReservationPaxOndCharge reservationPaxOndCharge : allCharges) {

			if (fromDate == null) {
				if (reservationPaxOndCharge.getZuluChargeDate().compareTo(toDate) <= 0) {
					colReservationPaxOndCharge.add(reservationPaxOndCharge);
				}
			} else {
				if (reservationPaxOndCharge.getZuluChargeDate().compareTo(fromDate) > 0
						&& reservationPaxOndCharge.getZuluChargeDate().compareTo(toDate) <= 0) {
					colReservationPaxOndCharge.add(reservationPaxOndCharge);
				}
			}
		}

		return colReservationPaxOndCharge;
	}

	private class AscSortComparator implements Comparator {

		@Override
		public int compare(Object first, Object second) {

			ReservationTnx credit = (ReservationTnx) first;
			ReservationTnx compareTo = (ReservationTnx) second;

			return credit.getTnxId().compareTo(compareTo.getTnxId());
		}
	}

	private static void saveReservationPaxTnxBreakdownForPayment(ReservationTnx creditTnxBF,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO) throws ModuleException {

		Integer pnrPaxId = Integer.parseInt(creditTnxBF.getPnrPaxId());

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = composeOndPayment(pnrPaxId, creditTnxBF,
				reservationPaxPaymentMetaTO);

		if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
		}
	}

	private static Collection<ReservationPaxOndPayment> composeOndPayment(Integer pnrPaxId, ReservationTnx reservationTnx,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO) throws ModuleException {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();
		Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO = new TnxGranularityFullFillmentDeriverBO(
				reservationPaxPaymentMetaTO.getColReservationPaxChargeMetaTOForNewCharges()).execute(reservationTnx.getAmount()
				.abs(), pnrPaxId);

		if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {

			for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : colReservationPaxChargeMetaTO) {
				ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
				reservationPaxOndPayment.setPnrPaxId(pnrPaxId.toString());
				reservationPaxOndPayment.setAmount(reservationPaxChargeMetaTO.getAmount());
				reservationPaxOndPayment.setPaymentTnxId(new Long(reservationTnx.getTnxId()));
				reservationPaxOndPayment.setPnrPaxOndChgId(new Long(reservationPaxChargeMetaTO.getPnrPaxOndChgId()));
				reservationPaxOndPayment.setChargeGroupCode(reservationPaxChargeMetaTO.getChargeGroupCode());
				reservationPaxOndPayment.setNominalCode(reservationTnx.getNominalCode());
				reservationPaxOndPayment.setRemarks(reservationPaxChargeMetaTO.getRemarks());

				colReservationPaxOndPayment.add(reservationPaxOndPayment);
			}
		}

		return colReservationPaxOndPayment;
	}

	private static ReservationPaymentMetaTO getReservationPaymentMetaTO(Reservation reservation, Date fromDate, Date toDate,
			ChargeType chargeType, BigDecimal amount) throws ModuleException {

		Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> mapPerPaxWiseOndPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getPerPaxWiseOndChargeIds(reservation.getPnrPaxIds());

		ReservationPaymentMetaTO reservationPaymentMetaTO = new ReservationPaymentMetaTO();

		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			Integer pnrPaxId = reservationPax.getPnrPaxId();

			if (ReservationApiUtils.isInfantType(reservationPax)) {
				pnrPaxId = reservationPax.getAccompaniedPaxId();
			}

			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo()
					.get(pnrPaxId);

			if (reservationPaxPaymentMetaTO == null) {
				reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
				reservationPaxPaymentMetaTO.setPnrPaxId(pnrPaxId);

				if (chargeType == ChargeType.FRESH) {
					reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewCharges(getColReservationPaxOndCharge(reservationPax,
							mapPerPaxWiseOndPayments.get(pnrPaxId), fromDate, toDate));
				}

				if (chargeType == ChargeType.REFUNDABLES) {
					reservationPaxPaymentMetaTO.addMapPerPaxWiseOndRefundableCharges(getRefundableReservationPaxOndCharge(
							reservationPax, mapPerPaxWiseOndPayments.get(pnrPaxId), fromDate, toDate));
				}

				if (chargeType == ChargeType.ADJUSTMENTS) {
					reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewAdjustments(getAdjustmentsReservationPaxOndCharge(
							reservationPax, mapPerPaxWiseOndPayments.get(pnrPaxId), fromDate, toDate, amount));
				}

				reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().put(pnrPaxId, reservationPaxPaymentMetaTO);
			} else {
				if (chargeType == ChargeType.FRESH) {
					reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewCharges(getColReservationPaxOndCharge(reservationPax,
							mapPerPaxWiseOndPayments.get(pnrPaxId), fromDate, toDate));
				}

				if (chargeType == ChargeType.REFUNDABLES) {
					reservationPaxPaymentMetaTO.addMapPerPaxWiseOndRefundableCharges(getRefundableReservationPaxOndCharge(
							reservationPax, mapPerPaxWiseOndPayments.get(pnrPaxId), fromDate, toDate));
				}

				if (chargeType == ChargeType.ADJUSTMENTS) {
					reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewAdjustments(getAdjustmentsReservationPaxOndCharge(
							reservationPax, mapPerPaxWiseOndPayments.get(pnrPaxId), fromDate, toDate, amount));
				}
			}
		}

		return reservationPaymentMetaTO;
	}

	private static Collection<ReservationPaxOndCharge> getAdjustmentsReservationPaxOndCharge(ReservationPax reservationPax,
			Map<Long, Collection<ReservationPaxOndPayment>> map, Date fromDate, Date toDate, BigDecimal amount) {
		Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();

		for (ReservationPaxFare reservationPaxFare : (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
			Collection<ReservationPaxOndCharge> filteredCharges = getReservationPaxOndCharges(reservationPaxFare.getCharges(),
					fromDate, toDate);
			filteredCharges = getFindChargeMatched(filteredCharges, amount);

			for (ReservationPaxOndCharge reservationPaxOndCharge : filteredCharges) {
				if (map == null || !map.containsKey(new Long(reservationPaxOndCharge.getPnrPaxOndChgId()))) {
					colReservationPaxOndCharge.add(reservationPaxOndCharge);
				}
			}
		}

		return colReservationPaxOndCharge;
	}

	private static Collection<ReservationPaxOndCharge> getFindChargeMatched(Collection<ReservationPaxOndCharge> filteredCharges,
			BigDecimal amount) {

		if (amount == null || amount.doubleValue() < 0) {
			return filteredCharges;
		} else {
			Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();
			for (ReservationPaxOndCharge reservationPaxOndCharge : filteredCharges) {
				if (reservationPaxOndCharge.getEffectiveAmount().compareTo(amount) == 0) {
					colReservationPaxOndCharge.add(reservationPaxOndCharge);
				}
			}

			return colReservationPaxOndCharge;
		}
	}

	private static Collection<ReservationPaxOndCharge> getColReservationPaxOndCharge(ReservationPax reservationPax,
			Map<Long, Collection<ReservationPaxOndPayment>> ondChargeIdsWiseAmounts, Date fromDate, Date toDate) {

		Collection<Long> ondChargIds = new HashSet<Long>();

		if (ondChargeIdsWiseAmounts != null) {
			ondChargIds.addAll(ondChargeIdsWiseAmounts.keySet());
		}

		Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();

		for (ReservationPaxFare reservationPaxFare : (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
			Collection<ReservationPaxOndCharge> filteredCharges = getReservationPaxOndCharges(reservationPaxFare.getCharges(),
					fromDate, toDate);
			for (ReservationPaxOndCharge reservationPaxOndCharge : filteredCharges) {
				// < 0 will not be taken, since that will be totally taken care from the cancellation flows.
				if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() > 0) {
					if (ondChargIds.size() == 0) {
						colReservationPaxOndCharge.add(reservationPaxOndCharge);
					} else {
						if (!ondChargIds.contains(new Long(reservationPaxOndCharge.getPnrPaxOndChgId()))) {
							colReservationPaxOndCharge.add(reservationPaxOndCharge);
						} else {
							Collection<ReservationPaxOndPayment> colDifferenceReservationPaxOndPayment = ondChargeIdsWiseAmounts
									.get(new Long(reservationPaxOndCharge.getPnrPaxOndChgId()));

							BigDecimal amount = TnxGranularityUtils.getTotalAmount(colDifferenceReservationPaxOndPayment);
							BigDecimal difference = AccelAeroCalculator.subtract(reservationPaxOndCharge.getAmount(), amount);

							if (difference.doubleValue() > 0) {
								ReservationPaxOndCharge clonedReservationPaxOndCharge = reservationPaxOndCharge
										.cloneForShallowCopy();
								clonedReservationPaxOndCharge.setAmount(difference);
								colReservationPaxOndCharge.add(clonedReservationPaxOndCharge);
							}
						}
					}
				}
			}
		}

		return colReservationPaxOndCharge;
	}

	private static Map<Long, ReservationPaxOndCharge> getRefundableReservationPaxOndCharge(ReservationPax reservationPax,
			Map<Long, Collection<ReservationPaxOndPayment>> ondChargeIdsWiseAmounts, Date fromDate, Date toDate) {

		Map<Long, ReservationPaxOndCharge> colReservationPaxOndCharge = new HashMap<Long, ReservationPaxOndCharge>();

		for (ReservationPaxFare reservationPaxFare : (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
			Collection<ReservationPaxOndCharge> filteredCharges = getReservationPaxOndCharges(reservationPaxFare.getCharges(),
					fromDate, toDate);
			Collection<ReservationPaxOndCharge> mainCharges = reservationPaxFare.getCharges();
			for (ReservationPaxOndCharge reservationPaxOndCharge : filteredCharges) {
				// < 0 will not be taken, since that will be totally taken care from the cancellation flows.
				// Commented by Nili. This is because if (-) adjustment applied, the refundable will be (+)
				// So we need to verify by matching a refundable charge with it original charge to id the refundable
				// chargers.
				Integer mainPnrPaxOndChgId = getMainPnrPaxOndChgId(reservationPaxOndCharge, mainCharges);
				if (mainPnrPaxOndChgId != null) {
					colReservationPaxOndCharge.put(Long.valueOf(mainPnrPaxOndChgId), reservationPaxOndCharge);
				}
			}
		}

		return colReservationPaxOndCharge;
	}

	// Id the refundable chargers by matching it with it's original charger.
	private static Integer getMainPnrPaxOndChgId(ReservationPaxOndCharge reservationPaxOndCharge,
			Collection<ReservationPaxOndCharge> mainCharges) {

		for (ReservationPaxOndCharge tmpReservationPaxOndCharge : mainCharges) {
			if (tmpReservationPaxOndCharge.getEffectiveAmount().compareTo(
					AccelAeroCalculator.multiply(reservationPaxOndCharge.getEffectiveAmount(), BigDecimal.valueOf(-1))) == 0) {
				if (BeanUtils.nullHandler(tmpReservationPaxOndCharge.getChargeGroupCode()).equals(
						BeanUtils.nullHandler(reservationPaxOndCharge.getChargeGroupCode()))
						&& isEqual(tmpReservationPaxOndCharge.getChargeRateId(), reservationPaxOndCharge.getChargeRateId())
						&& isEqual(tmpReservationPaxOndCharge.getFareId(), reservationPaxOndCharge.getFareId())) {
					return tmpReservationPaxOndCharge.getPnrPaxOndChgId();
				}
			}
		}

		return null;
	}

	private static boolean isEqual(Integer first, Integer second) {
		if (first == null && second == null) {
			return true;
		} else if (first != null && second != null) {
			if (first.equals(second)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private static void saveReservationPaxTnxBreakdownForRefundables(String pnrPaxId, ReservationTnx refundableReservationTnx,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO) throws ModuleException {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		Map<Long, Collection<ReservationPaxOndPayment>> mapPaxOndPayments = reservationPaxPaymentMetaTO
				.getPaxWiseOndChargePayments();

		for (Entry<Long, ReservationPaxOndCharge> entry : reservationPaxPaymentMetaTO.getMapPerPaxWiseOndRefundableCharges()
				.entrySet()) {
			Long ondChargeId = entry.getKey();
			ReservationPaxOndCharge reservationPaxOndCharge = entry.getValue();
			
			BigDecimal actualAmount = reservationPaxOndCharge.getEffectiveAmount();
			
			Collection<ReservationPaxOndPayment> colNewReservationPaxOndPayment = TnxGranularityUtils
					.getRespectiveReservationPaxOndPayment(mapPaxOndPayments.get(ondChargeId),
							actualAmount);

			for (ReservationPaxOndPayment reservationPaxOndPayment : colNewReservationPaxOndPayment) {
				reservationPaxOndPayment.setPnrPaxId(pnrPaxId);
				reservationPaxOndPayment.setPnrPaxOndChgId(new Long(reservationPaxOndCharge.getPnrPaxOndChgId()));
				reservationPaxOndPayment.setChargeGroupCode(reservationPaxOndCharge.getChargeGroupCode());
				reservationPaxOndPayment.setNominalCode(refundableReservationTnx.getNominalCode());
				reservationPaxOndPayment.setRemarks(null);
			}

			colReservationPaxOndPayment.addAll(colNewReservationPaxOndPayment);
		}

		if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
		}
	}

	private static void saveReservationPaxTnxBrkForNewCharges(String pnrPaxId, int nominalCode,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, Collection<ReservationCredit> colReservationCredit)
			throws ModuleException {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges = reservationPaxPaymentMetaTO
				.getColReservationPaxChargeMetaTOForNewCharges();

		// Fill in for the cancellation
		colReservationPaxOndPayment.addAll(fillInNewChargesFromAvailableCredits(pnrPaxId, nominalCode,
				colReservationPaxChargeMetaTOForNewCharges, colReservationCredit));

		if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
		}
	}

	private static void saveReservationPaxTnxBrkForAlterCharges(String pnrPaxId, int nominalCode,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, Collection<ReservationCredit> colReservationCredit)
			throws ModuleException {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges = reservationPaxPaymentMetaTO
				.getColReservationPaxChargeMetaTOForNewCharges();

		// Fill in for the cancellation
		colReservationPaxOndPayment.addAll(fillInNewChargesFromAvailableCredits(pnrPaxId, nominalCode,
				colReservationPaxChargeMetaTOForNewCharges, colReservationCredit));

		if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
		}
	}

	private static Collection<ReservationPaxOndPayment> fillInNewChargesFromAvailableCredits(String pnrPaxId, int nominalCode,
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges,
			Collection<ReservationCredit> colReservationCredit) {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		if (colReservationPaxChargeMetaTOForNewCharges != null && colReservationPaxChargeMetaTOForNewCharges.size() > 0) {

			Collection<String> colChargeGroupOrder = TnxGranularityRules.getPaymentChargeGroupFulFillmentOrder();
			
			ReservationCreditTO maxCreditAmountTO = TnxGranularityFullFillmentDeriverBO.adjustAvailableReservationCredit(
					colReservationPaxChargeMetaTOForNewCharges, colReservationCredit);
			
			for (String chargeGroupCode : colChargeGroupOrder) {
				Collection<ReservationPaxChargeMetaTO> colSelectedReservationPaxChargeMetaTO = TnxGranularityUtils
						.getChargeMetaTOByChargeGroupCode(chargeGroupCode, colReservationPaxChargeMetaTOForNewCharges);

				if (colSelectedReservationPaxChargeMetaTO != null && colSelectedReservationPaxChargeMetaTO.size() > 0) {
					Collection<ReservationPaxChargeMetaTO> colAdjustedCharges = new ArrayList<ReservationPaxChargeMetaTO>();
					Collection<ReservationPaxChargeMetaTO> colRemovingCharges = new ArrayList<ReservationPaxChargeMetaTO>();

					for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : colSelectedReservationPaxChargeMetaTO) {
						BigDecimal chargeAmount = reservationPaxChargeMetaTO.getAmount();
						
						chargeAmount = TnxGranularityUtils
								.getAvailableMaxCredit(maxCreditAmountTO, chargeGroupCode, chargeAmount);
						BigDecimal availableCreditBalance = TnxGranularityUtils.getMaxChargeGroupCredit(maxCreditAmountTO,
								chargeGroupCode);
						availableCreditBalance = AccelAeroCalculator.add(chargeAmount, availableCreditBalance);
						
						Collection<ReservationCredit> appliedReservationCredit = null;

						appliedReservationCredit = getValidReservationCreditForCharge(chargeAmount, colReservationCredit,
								availableCreditBalance, chargeGroupCode);

						if (appliedReservationCredit != null && appliedReservationCredit.size() > 0) {
							reservationPaxChargeMetaTO.setAmount(reservationPaxChargeMetaTO.getAmount());

							Collection<ReservationPaxChargeMetaTO> adjustmentsForTheCharge = getAdjustedCharges(
									reservationPaxChargeMetaTO, appliedReservationCredit);

							if (adjustmentsForTheCharge.size() > 0) {
								colAdjustedCharges.addAll(adjustmentsForTheCharge);
							}

							colRemovingCharges.add(reservationPaxChargeMetaTO);
							for (ReservationCredit reservationCredit : appliedReservationCredit) {
								ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
								reservationPaxOndPayment.setPnrPaxId(pnrPaxId);
								reservationPaxOndPayment.setAmount(reservationCredit.getBalance());
								reservationPaxOndPayment.setPaymentTnxId(reservationCredit.getTnxId());
								reservationPaxOndPayment.setPnrPaxOndChgId(new Long(reservationPaxChargeMetaTO
										.getPnrPaxOndChgId()));
								reservationPaxOndPayment.setChargeGroupCode(reservationPaxChargeMetaTO.getChargeGroupCode());
								reservationPaxOndPayment.setNominalCode(nominalCode);
								reservationPaxOndPayment.setRemarks(null);

								colReservationPaxOndPayment.add(reservationPaxOndPayment);
							}
						}

					}

					if (colAdjustedCharges.size() > 0) {
						colReservationPaxChargeMetaTOForNewCharges.addAll(colAdjustedCharges);
					}

					if (colRemovingCharges.size() > 0) {
						colReservationPaxChargeMetaTOForNewCharges.removeAll(colRemovingCharges);
					}
				}
			}
		}

		return colReservationPaxOndPayment;
	}

	private static Collection<ReservationCredit> getValidReservationCreditForCharge(BigDecimal amount,
			Collection<ReservationCredit> colReservationCredit, BigDecimal groupBalance, String chargeGroupCode) {

		BigDecimal remainingAmount = AccelAeroCalculator.add(AccelAeroCalculator.getDefaultBigDecimalZero(), amount);

		Collection<ReservationCredit> selectedCredits = new ArrayList<ReservationCredit>();
		Collection<ReservationCredit> removingCredits = new ArrayList<ReservationCredit>();
		BigDecimal remainingCreditToConsume = AccelAeroCalculator.add(AccelAeroCalculator.getDefaultBigDecimalZero(), amount);
		for (ReservationCredit reservationCredit : colReservationCredit) {

			// returns applicable amount from current ReservationCredit record
			remainingAmount = TnxGranularityUtils
					.getEffectiveCredit(remainingCreditToConsume, reservationCredit, chargeGroupCode);

			remainingCreditToConsume = AccelAeroCalculator.subtract(remainingCreditToConsume, remainingAmount);

			if (remainingAmount.doubleValue() > 0 && reservationCredit.getBalance().doubleValue() > 0
					&& groupBalance.doubleValue() > 0) {
				// if (reservationCredit.getBalance().doubleValue() <= remainingAmount.doubleValue()) {

				if (groupBalance.doubleValue() <= remainingAmount.doubleValue()
						&& reservationCredit.getBalance().doubleValue() >= remainingAmount.doubleValue()) {

					ReservationCredit clonedReservationCredit = reservationCredit.clone();
					clonedReservationCredit.setBalance(remainingAmount);
					selectedCredits.add(clonedReservationCredit);

					BigDecimal availableCredit = AccelAeroCalculator.subtract(reservationCredit.getBalance(), remainingAmount);
					reservationCredit.setBalance(availableCredit);

					TnxGranularityFullFillmentDeriverBO.wipeOfCreditPayments(reservationCredit, clonedReservationCredit, remainingAmount,
							chargeGroupCode);

					if (reservationCredit.getBalance().doubleValue() == 0) {
						removingCredits.add(reservationCredit);
					}

					remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				} else if (groupBalance.doubleValue() >= remainingAmount.doubleValue()
						&& reservationCredit.getBalance().doubleValue() <= remainingAmount.doubleValue()) {

					BigDecimal effectiveAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

					remainingAmount = AccelAeroCalculator.subtract(remainingAmount, reservationCredit.getBalance());

					BigDecimal availableCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
					effectiveAmount = reservationCredit.getBalance();

					ReservationCredit clonedReservationCredit = reservationCredit.clone();
					clonedReservationCredit.setBalance(effectiveAmount);
					selectedCredits.add(clonedReservationCredit);

					reservationCredit.setBalance(availableCredit);

					TnxGranularityFullFillmentDeriverBO.wipeOfCreditPayments(reservationCredit, clonedReservationCredit, effectiveAmount,
							chargeGroupCode);

					if (reservationCredit.getBalance().doubleValue() == 0) {
						removingCredits.add(reservationCredit);
					}

				} else {

					BigDecimal remainingBalance = AccelAeroCalculator.subtract(reservationCredit.getBalance(), remainingAmount);
					reservationCredit.setBalance(remainingBalance);
					ReservationCredit clonedReservationCredit = reservationCredit.clone();
					clonedReservationCredit.setBalance(remainingAmount);

					TnxGranularityFullFillmentDeriverBO.wipeOfCreditPayments(reservationCredit, clonedReservationCredit, remainingAmount,
							chargeGroupCode);

					remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					selectedCredits.add(clonedReservationCredit);
					// break;
				}
			}
		}

		if (removingCredits.size() > 0) {
			colReservationCredit.removeAll(removingCredits);
		}

		return selectedCredits;
	}

	private static Collection<ReservationPaxChargeMetaTO> getAdjustedCharges(
			ReservationPaxChargeMetaTO reservationPaxChargeMetaTO, Collection<ReservationCredit> appliedReservationCredit) {

		BigDecimal totalBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (ReservationCredit reservationCredit : appliedReservationCredit) {
			totalBalance = AccelAeroCalculator.add(totalBalance, reservationCredit.getBalance());
		}

		Collection<ReservationPaxChargeMetaTO> colAdjustedCharges = new ArrayList<ReservationPaxChargeMetaTO>();

		if (totalBalance.doubleValue() != reservationPaxChargeMetaTO.getAmount().doubleValue()) {
			BigDecimal difference = AccelAeroCalculator.subtract(reservationPaxChargeMetaTO.getAmount(), totalBalance);
			reservationPaxChargeMetaTO = reservationPaxChargeMetaTO.clone();
			reservationPaxChargeMetaTO.setAmount(difference);
			colAdjustedCharges.add(reservationPaxChargeMetaTO);
		}

		return colAdjustedCharges;
	}

	private static void saveReservationPaxTnxBreakdownForRefund(ReservationTnx creditTnxBF,
			Collection<ReservationCredit> colReservationCredit) throws ModuleException {

		Collection<String> colChargeGroupCodes = TnxGranularityRules.getRefundChargeGroupFulFillmentOrder();
		Collection<Long> colPaymentTnxIds = TnxGranularityUtils.getPaymentIds(colReservationCredit);
		Map<Long, Collection<ReservationPaxOndPayment>> remainingPayments = getRemainingPaymentMetaInfo(colPaymentTnxIds,
				colChargeGroupCodes);
		BigDecimal remainingAmount = creditTnxBF.getAmount();

		Collection<ReservationPaxOndPayment> composedReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		for (Collection<ReservationPaxOndPayment> colReservationPaxOndPayment : remainingPayments.values()) {
			if (remainingAmount.doubleValue() > 0) {
				for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
					if (remainingAmount.doubleValue() > 0) {
						BigDecimal amount = reservationPaxOndPayment.getAmount();
						Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
						reservationPaxOndPayment = reservationPaxOndPayment.clone();
						reservationPaxOndPayment.setRemarks(null);
						reservationPaxOndPayment.setNominalCode(creditTnxBF.getNominalCode());
						reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);
						if (remainingAmount.doubleValue() > amount.doubleValue()) {
							reservationPaxOndPayment.setAmount(amount.negate());
							remainingAmount = AccelAeroCalculator.subtract(remainingAmount, amount);
						} else {
							reservationPaxOndPayment.setAmount(remainingAmount.negate());
							remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
						composedReservationPaxOndPayment.add(reservationPaxOndPayment);
					} else {
						break;
					}
				}
			} else {
				break;
			}
		}

		if (composedReservationPaxOndPayment != null && composedReservationPaxOndPayment.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.saveReservationPaxOndPayment(composedReservationPaxOndPayment);
		}
	}

	private class BalanceCreditMigrator {

		private final Collection<ReservationTnx> colReservationTnx;
		private final Collection<ReservationCredit> colReservationCredit;

		/**
		 * constructor of the BalanceCredit command
		 */
		private BalanceCreditMigrator(Collection<ReservationTnx> colReservationTnx,
				Collection<ReservationCredit> colReservationCredit) {
			this.colReservationTnx = colReservationTnx;
			this.colReservationCredit = colReservationCredit;
		}

		/**
		 * execute method of the BalanceCredit command
		 * 
		 * @throws ModuleException
		 */
		private void execute() throws ModuleException {
			DefaultServiceResponse response = new DefaultServiceResponse(true);

			// BigDecimal.
			BigDecimal pnrPaxBalance = getPNRPaxBalance();
			BigDecimal sumOfCredits = getSumOfReservationCredits();

			List<ReservationCredit> creditList = new ArrayList<ReservationCredit>();
			creditList.addAll(this.colReservationCredit);

			// nominal value of pnr pax balance is compared against the sum or gredits....
			// If the values match do nothing..else regain or utilze credit.
			if (pnrPaxBalance.negate().compareTo(sumOfCredits) != 0) {

				// pnr pax balance is a negative value
				if (pnrPaxBalance.intValue() < 0) {
					// if the nominal pnrPaxBalance is greater to the sum of credit
					if (pnrPaxBalance.negate().compareTo(sumOfCredits) > 0) {
						// to get the plus value difference
						BigDecimal diff = pnrPaxBalance.negate().subtract(sumOfCredits);

						regainCredit(creditList, diff);
					}
					// if the nominal pnrPaxBalance is lesser to the sum of credit
					else {
						// get the difference
						BigDecimal diff = sumOfCredits.subtract(pnrPaxBalance.negate());
						BigDecimal d = utilizeCredit(creditList, diff);
						response.addResponceParam(CommandParamNames.RAC_NEW_CREDIT_BALANCE, d);
					}
				} else {
					if (sumOfCredits.doubleValue() != 0) {
						neutralizeCredit(creditList);
					}
				}
			}
		}

		private BigDecimal getPNRPaxBalance() {
			BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (ReservationTnx reservationTnx : this.colReservationTnx) {
				amount = AccelAeroCalculator.add(amount, reservationTnx.getAmount());
			}
			return amount;
		}

		private BigDecimal getSumOfReservationCredits() {
			BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (ReservationCredit reservationCredit : this.colReservationCredit) {
				amount = AccelAeroCalculator.add(amount, reservationCredit.getBalance());
			}
			return amount;
		}

		private void neutralizeCredit(List<ReservationCredit> creditList) {
			for (ReservationCredit reservationCredit : creditList) {
				if (reservationCredit.getBalance().doubleValue() != 0) {
					reservationCredit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
				}
			}
		}

		private BigDecimal utilizeCredit(List<ReservationCredit> creditList, BigDecimal diff) {
			Collections.sort(creditList, new OldestFirstSortComparator());
			Iterator itCredits = creditList.iterator();
			boolean allUtilized = false;
			while (itCredits.hasNext() && !allUtilized) {
				ReservationCredit credit = (ReservationCredit) itCredits.next();
				BigDecimal creditBalance = credit.getBalance();
				if (diff.compareTo(creditBalance) > 0) {
					diff = diff.subtract(creditBalance);
					credit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());

				} else {
					if (diff.compareTo(creditBalance) == 0) {
						credit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
						allUtilized = true;
					} else {
						credit.setBalance(credit.getBalance().subtract(diff));
						allUtilized = true;
					}
				}
			}
			if (!allUtilized) {
				return diff;
			}

			return AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		private void regainCredit(List<ReservationCredit> creditList, BigDecimal diff) throws ModuleException {

			Collections.sort(creditList, new LatestFirstSortComparator());
			Iterator itCredits = creditList.iterator();

			boolean allConsumed = false;

			while (itCredits.hasNext() && !allConsumed) {

				ReservationCredit credit = (ReservationCredit) itCredits.next();

				BigDecimal tnxAmout = getTransaction(credit.getTnxId()).getAmount();
				// since every payment is negative, we get the nominal amount here
				tnxAmout = tnxAmout.negate();

				BigDecimal creditBalance = credit.getBalance();

				// transaction amount remainder is fully suffucient to rgain the diff amount
				if (tnxAmout.subtract(creditBalance).compareTo(diff) == 0) {
					credit.setBalance(tnxAmout);
					allConsumed = true;
				} else {
					// transaction amount remainder is lesser than diff amount
					if (tnxAmout.subtract(creditBalance).compareTo(diff) < 0) {
						diff = diff.subtract(tnxAmout.subtract(creditBalance));
						credit.setBalance(tnxAmout);
					} else {
						// transaction amount remainder is greater than diff amount
						credit.setBalance(diff.add(creditBalance));
						allConsumed = true;
					}
				}
			}

			if (!allConsumed && diff.doubleValue() > 0.01) {
				throw new ModuleException("revacc.regain.inconsitent.credit");
			}
		}

		private ReservationTnx getTransaction(long tnxId) {

			for (ReservationTnx reservationTnx : this.colReservationTnx) {
				if (reservationTnx.getTnxId().longValue() == tnxId) {
					return reservationTnx;
				}
			}

			return null;
		}

		private class OldestFirstSortComparator implements Comparator {

			@Override
			public int compare(Object first, Object second) {

				ReservationCredit credit = (ReservationCredit) first;
				ReservationCredit compareTo = (ReservationCredit) second;

				return (credit.getExpiryDate() == null || compareTo.getExpiryDate() == null) ? 0 : credit.getExpiryDate()
						.compareTo(compareTo.getExpiryDate());
			}
		}

		private class LatestFirstSortComparator implements Comparator {

			@Override
			public int compare(Object first, Object second) {

				ReservationCredit credit = (ReservationCredit) first;
				ReservationCredit compareTo = (ReservationCredit) second;

				return (credit.getExpiryDate() == null || compareTo.getExpiryDate() == null) ? 0 : credit.getExpiryDate()
						.compareTo(compareTo.getExpiryDate()) * -1;
			}
		}
	}

}