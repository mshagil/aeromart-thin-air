package com.isa.thinair.airreservation.core.util;

import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.tax.GSTTaxInvoiceFactory;

public class TaxInvoiceNumberGenerator {
	public static String getTaxInvoiceNumber(String stateCode, String invoiceType) {
		String taxInvoiceNumber = null;
		String invoiceTypeIndicator = null;

		switch (invoiceType) {
		case GSTTaxInvoiceFactory.INVOICE_TYPE_INVOICE:
			invoiceTypeIndicator = "INV";
			break;
		case GSTTaxInvoiceFactory.INVOICE_TYPE_DEBIT_NOTE:
			invoiceTypeIndicator = "DBN";
			break;
		case GSTTaxInvoiceFactory.INVOICE_TYPE_CREDIT_NOTE:
			invoiceTypeIndicator = "CDN";
			break;

		}

		int invoiceSequence = ReservationDAOUtils.DAOInstance.TAX_INVOICE_DAO.getTaxInvoiceNumber(stateCode, invoiceType);
		taxInvoiceNumber = stateCode + invoiceTypeIndicator  + String.format("%010d", invoiceSequence);

		return taxInvoiceNumber;
	}
}
