package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndChgCommission;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.util.AgentCommissionBinder;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Bussiness Object to recalls the applied agent commissions (basis: per pax, per ond)
 * 
 * Functions:
 * 
 * 1) Recall the commission applied from agents account and adjust the agents credit balance
 * 
 * 2) Removing agent commission are tracked by reservation level (CR records will be written to
 * t_pnr_pax_chg_commission)
 */
public class AgentCommissionRemovalBO {

	private static final Log log = LogFactory.getLog(AgentCommissionRemovalBO.class);

	private static TravelAgentFinanceBD travelAgentFinanceBD = null;

	private ReservationPaxOndCharge reservationPaxOndCharge = null;
	private CredentialsDTO credentialsDTO = null;

	public AgentCommissionRemovalBO(ReservationPaxOndCharge reservationPaxOndCharge, CredentialsDTO credentialsDTO) {
		this.reservationPaxOndCharge = reservationPaxOndCharge;
		this.credentialsDTO = credentialsDTO;
	}

	public void execute() throws ModuleException {

		Set<ReservationPaxOndChgCommission> appliedAgentCommissions = reservationPaxOndCharge.getResPaxOndChgCommission();
		BigDecimal totalEffectiveCommission = AccelAeroCalculator.getDefaultBigDecimalZero();
		ReservationPax resPax = reservationPaxOndCharge.getReservationPaxFare().getReservationPax();

		if (!appliedAgentCommissions.isEmpty()) {
			for (ReservationPaxOndChgCommission appliedAgentCommission : appliedAgentCommissions) {
				String tnxFlag = appliedAgentCommission.getDrOrcr();
				if (appliedAgentCommission.getStatus().equals(AirTravelAgentConstants.AgentCommissionStatus.CONFIRMED)) {
					if (tnxFlag.equals(AirTravelAgentConstants.TnxTypes.CREDIT)) {
						totalEffectiveCommission = totalEffectiveCommission.add(appliedAgentCommission.getAmount());
					} else if (tnxFlag.equals(AirTravelAgentConstants.TnxTypes.DEBIT)) {
						totalEffectiveCommission = totalEffectiveCommission.add(appliedAgentCommission.getAmount());
					} else {
						throw new ModuleException("airreservations.arg.unidentified.transaction.type.detected");
					}
				}
			}

			if (totalEffectiveCommission.compareTo(BigDecimal.ZERO) > 0) {
				// fare is already granted a commission

				BigDecimal commissionAmountToRemove = AccelAeroCalculator.getDefaultBigDecimalZero();
				String pnr = resPax.getReservation().getPnr();
				String agentCode = null;
				String userId = null;
				Integer salesChannelCode = null; // 3, for travel agents only
				String status = AirTravelAgentConstants.AgentCommissionStatus.CONFIRMED;

				for (ReservationPaxOndChgCommission appliedAgentCommission : appliedAgentCommissions) {

					if (appliedAgentCommission.getStatus().equals(AirTravelAgentConstants.AgentCommissionStatus.CONFIRMED)
							&& appliedAgentCommission.getDrOrcr().equals(AirTravelAgentConstants.TnxTypes.DEBIT)) {
						// can assume, portion is only from DR content & just one entry
						agentCode = appliedAgentCommission.getAgentCode();
						userId = appliedAgentCommission.getUserId();
						commissionAmountToRemove = appliedAgentCommission.getAmount();
						AgentCommissionBinder.bindCreditInstance(reservationPaxOndCharge, commissionAmountToRemove, agentCode,
								userId, status);

						if (log.isDebugEnabled()) {
							log.debug("[COMMISSION REMOVED] For Agent: " + agentCode + ", by pnr: " + pnr + ", amount: "
									+ commissionAmountToRemove + ", pax_seq: " + resPax.getPaxSequence() + ", pnr_pax_id "
									+ resPax.getPnrPaxId());
						}

						getTravelAgentFinanceBD().removeAgentCommission(agentCode, userId, salesChannelCode,
								commissionAmountToRemove, getTracker(), pnr, null);

						break;
					}
				}

			}

		}

	}

	private TravelAgentFinanceBD getTravelAgentFinanceBD() {
		if (travelAgentFinanceBD == null) {
			travelAgentFinanceBD = ReservationModuleUtils.getTravelAgentFinanceBD();
		}
		return travelAgentFinanceBD;
	}

	private String getTracker() {
		StringBuilder tracker = new StringBuilder();
		if (this.credentialsDTO != null) {
			tracker.append("commission removed by agent: ").append(this.credentialsDTO.getAgentCode());
			tracker.append(", userId: ").append(this.credentialsDTO.getUserId());
		}
		return tracker.toString();
	}
}
