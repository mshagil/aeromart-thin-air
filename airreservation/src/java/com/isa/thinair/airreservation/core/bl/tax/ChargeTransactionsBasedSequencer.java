package com.isa.thinair.airreservation.core.bl.tax;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

public class ChargeTransactionsBasedSequencer implements Sequencer {

	private static final int SEC_TO_ROUND = 3;
	private Reservation reservation;
	private Map<Long, Long> payMap = new HashMap<>();
	private Map<Long, Long> chgMap = new HashMap<>();

	private long nextKey = 0;
	private Map<Date, Long> dateKeymap = new HashMap<>();

	private Map<Long, TnxRange> tnxSeqRangeMap = new HashMap<>();
	private int minimumSequence = -1;

	public ChargeTransactionsBasedSequencer(Reservation reservation) {
		this.reservation = reservation;
		loadTransactions();
	}

	private void loadTransactions() {

		Collection<ReservationPaxOndCharge> charges = ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO
				.getPnrPaxOndCharges(reservation.getPnr());
		for (ReservationPaxOndCharge ondCharge : charges) {
			chgMap.put(new Long(ondCharge.getPnrPaxOndChgId()), new Long(ondCharge.getTransactionSeq()));
		}

		Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> mapPerPaxWiseOndPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getPerPaxWiseOndChargeIds(reservation.getPnrPaxIds());
		Map<Integer, Long> paxTnxIds = new HashMap<>();
		for (Map<Long, Collection<ReservationPaxOndPayment>> paxOndMap : mapPerPaxWiseOndPayments.values()) {
			for (Collection<ReservationPaxOndPayment> paxChargeWise : paxOndMap.values()) {
				for (ReservationPaxOndPayment ondPaxPayment : paxChargeWise) {
					if (nextKey <= ondPaxPayment.getTransactionSeq()) {
						nextKey = ondPaxPayment.getTransactionSeq() + 1;
					}
					Long tnxSeq = chgMap.get(ondPaxPayment.getPnrPaxOndChgId());
					long minimumSeq = captureMinimumSequence(tnxSeq);
					if ((ondPaxPayment.getOriginalPaxOndPaymentId() == null || ondPaxPayment.getOriginalPaxOndPaymentId() <= 0)
							&& ReservationTnxNominalCode.CREDIT_PAYOFF.getCode() != ondPaxPayment.getNominalCode()
							&& ReservationTnxNominalCode.CREDIT_BF.getCode() != ondPaxPayment.getNominalCode()) {
						if ((payMap.containsKey(ondPaxPayment.getPaymentTnxId()) && payMap.get(ondPaxPayment.getPaymentTnxId()) > minimumSeq)
								|| !payMap.containsKey(ondPaxPayment.getPaymentTnxId())) {
							payMap.put(ondPaxPayment.getPaymentTnxId(), minimumSeq);
						}

					}
					if (ondPaxPayment.getPaxOndRefundId() != null) {
						Long refundId = new Long(ondPaxPayment.getPaxOndRefundId());
						if ((payMap.containsKey(refundId) && payMap.get(refundId) > minimumSeq) || !payMap.containsKey(refundId)) {
							payMap.put(refundId, minimumSeq);
						}
					}

					if (!tnxSeqRangeMap.containsKey(tnxSeq)) {
						tnxSeqRangeMap.put(tnxSeq, new TnxRange());
					}

					paxTnxIds.put(Integer.parseInt(ondPaxPayment.getPaymentTnxId().toString()), tnxSeq);
					ondPaxPayment.getPaymentTnxId();
				}
			}
		}

		List<ReservationTnx> tnxs = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getTransactions(paxTnxIds.keySet());

		for (ReservationTnx tnx : tnxs) {
			Long tnxSeq = paxTnxIds.get(tnx.getTnxId());
			tnxSeqRangeMap.get(tnxSeq).addDate(tnx.getDateTime());
		}
	}

	private long captureMinimumSequence(long value) {
		if (minimumSequence == -1 || value < minimumSequence) {
			minimumSequence = (int) value;
		}
		return value;
	}

	private Long findMatchingSequenceFromDate(Date date) {
		Long retSeq = -1L;
		for (Long seq : tnxSeqRangeMap.keySet()) {
			if (tnxSeqRangeMap.get(seq).isWithinRange(date)) {
				retSeq = seq;
				break;
			}
		}

		return retSeq;
	}

	@Override
	public Long getKey(ReservationPaxOndCharge charge) {
		Long chargeId = new Long(charge.getPnrPaxOndChgId());
		if (!chgMap.containsKey(chargeId)) {
			Long matchingSeq = findMatchingSequenceFromDate(charge.getZuluChargeDate());
			if (matchingSeq != -1) {
				chgMap.put(chargeId, matchingSeq);
			} else {
				Date date = getNormalizedDate(charge.getZuluChargeDate());
				if (!dateKeymap.containsKey(date)) {
					long sequence = nextKey++;
					dateKeymap.put(date, sequence);
				}
				chgMap.put(chargeId, dateKeymap.get(date));
			}
		} else {
			if (!tnxSeqRangeMap.containsKey(chgMap.get(chargeId))) {
				tnxSeqRangeMap.put(chgMap.get(chargeId), new TnxRange());
			}
			tnxSeqRangeMap.get(chgMap.get(chargeId)).addDate(charge.getZuluChargeDate());
		}
		Long key = chgMap.get(chargeId);

		return key;
	}

	@Override
	public Long getKey(ReservationTnx resTnx) {
		Long tnxId = new Long(resTnx.getTnxId());
		if (!payMap.containsKey(tnxId)) {
			Long matchingSeq = findMatchingSequenceFromDate(resTnx.getDateTime());
			if (matchingSeq != -1) {
				payMap.put(tnxId, matchingSeq);
			} else {
				Date date = getNormalizedDate(resTnx.getDateTime());
				if (!dateKeymap.containsKey(date)) {
					long sequence = nextKey++;
					dateKeymap.put(date, sequence);
				}
				payMap.put(tnxId, dateKeymap.get(date));
			}
		} else {
			tnxSeqRangeMap.get(payMap.get(tnxId)).addDate(resTnx.getDateTime());
		}
		Long key = payMap.get(tnxId);
		return key;
	}

	private Date getNormalizedDate(Date inputDate) {
		BigDecimal epochBD = BigDecimal.valueOf(inputDate.getTime());
		BigDecimal boundry = BigDecimal.valueOf(SEC_TO_ROUND * 1000);
		BigDecimal breakpoint = BigDecimal.valueOf(SEC_TO_ROUND * 1000 - 1);
		BigDecimal rounded = AccelAeroRounderPolicy.getRoundedValue(epochBD, boundry, breakpoint);

		Date newd = new Date(Long.parseLong(rounded.toPlainString()));

		return newd;
	}

	@Override
	public int getMinmumSequence() {
		return minimumSequence;
	}

}
