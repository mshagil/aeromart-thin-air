/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for making a final response
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="constructResponse"
 */
public class ConstructResponse extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ConstructResponse.class);

	/**
	 * Execute method of the ConstructResponse command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Map<String, String> paymentAuthIdMap = (Map<String, String>) this.getParameter(CommandParamNames.PAYMENT_AUTHID_MAP);
		List<String> invoiceNumbers = (List<String>) this.getParameter(CommandParamNames.TAX_INVOICE_NUMBERS);
		// Getting command parameters
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (output != null) {
			response = output;
		}

		if (pnr != null) {
			response.addResponceParam(CommandParamNames.ON_HOLD_PNR, pnr);
		}

		if (paymentAuthIdMap != null) {
			response.addResponceParam(CommandParamNames.PAYMENT_AUTHID_MAP, paymentAuthIdMap);
		}
		
		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> updatedFlexibilitiesMap = (Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>>) this
				.getParameter(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP);
		if (updatedFlexibilitiesMap != null && !updatedFlexibilitiesMap.isEmpty()) {
			response.addResponceParam(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP, updatedFlexibilitiesMap);
		}
		if (reservation != null) {
			response.addResponceParam(CommandParamNames.VERSION, reservation.getVersion());
			response.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_IDS,
					this.getParameter(CommandParamNames.FLIGHT_SEGMENT_IDS));
			response.addResponceParam(CommandParamNames.PNR, this.getParameter(CommandParamNames.PNR));
			response.addResponceParam(CommandParamNames.TAX_INVOICE_NUMBERS, invoiceNumbers);
		}

		response.addResponceParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER,
				this.getParameter(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER));
		response.addResponceParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY,
				this.getParameter(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY));


		log.debug("Exit execute");
		return response;
	}
}
