package com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator.base;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;

public abstract class BaseElementBuilderValidator {

	public abstract boolean validate(String messageType);

	public abstract PassengerStoreTypes getOngoingStoreType(String messageType, PassengerStoreTypes paxStoreType);

}
