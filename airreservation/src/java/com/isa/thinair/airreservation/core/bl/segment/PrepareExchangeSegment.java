/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 

 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.CloneBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to exchange existing confirm segments with prior to full fill the external ticket exchange request. following
 * command will prepare required input arguments for the exchangeSegment command.
 * 
 * @author M.Rikaz
 * @since 1.0
 * @isa.module.command name="prepareExchangeSegment"
 */
public class PrepareExchangeSegment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PrepareExchangeSegment.class);

	/** Holds whether Credentials information */
	private CredentialsDTO credentialsDTO;

	/** PrepareExchangeSegment Constructor */
	public PrepareExchangeSegment() {
	}

	/**
	 * Execute method of the PrepareExchangeSegment command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {
		if (log.isDebugEnabled())
			log.debug("Inside prepareExchangeSegment execute");

		String pnr = this.getParameter(CommandParamNames.PNR, String.class);
		this.credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		this.validateParams(pnr);

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);

		Collection<ReservationSegment> excSegments = new ArrayList<ReservationSegment>();

		// Get the reservation
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		this.validateReservation(reservation);

		Collection<Integer> flownPnrSegIds = new ArrayList<Integer>();
		Collection<Integer> colExchangablePnrSegIds = new ArrayList<Integer>();
		Collection<Integer> colExchangableFltSegIds = new ArrayList<Integer>();
		Collection<Integer> cnxPnrSegIds = new ArrayList<Integer>();
		Map<Integer, Collection<ReservationPaxFare>> exchangeSegPaxFareByPaxId = new HashMap<Integer, Collection<ReservationPaxFare>>();
		Map<Integer, Integer> exchangePnrSegIdByFltSegId = new HashMap<Integer, Integer>();
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
		if (reservation != null) {
			FLOWN_FROM flownCalculateMethod = AppSysParamsUtil.getFlownCalculateMethod();
			for (ReservationPax resPax : reservation.getPassengers()) {
				Collection<ReservationPaxFare> oldFares = new ArrayList<ReservationPaxFare>();
				Collection<EticketTO> eTicketTOs = resPax.geteTickets();
				Collection<ReservationPaxFare> colReservationPaxFare = resPax.getPnrPaxFares();

				Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
				ReservationPaxFare reservationPaxFare;
				ReservationPaxFareSegment reservationPaxFareSegment;
				ReservationSegment reservationSegment;

				Iterator<ReservationPaxFare> itColReservationPaxFare = colReservationPaxFare.iterator();
				while (itColReservationPaxFare.hasNext()) {
					reservationPaxFare = (ReservationPaxFare) itColReservationPaxFare.next();

					itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

					while (itReservationPaxFareSegment.hasNext()) {
						reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();
						reservationSegment = reservationPaxFareSegment.getSegment();
						Collection<ReservationSegmentDTO> segmentsView = reservation.getSegmentsView();

						boolean isFlown = false;
						if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
								.equals(reservationSegment.getStatus())) {
							if (FLOWN_FROM.DATE != flownCalculateMethod && eTicketTOs != null && eTicketTOs.size() > 0) {
								for (EticketTO eticketTO : eTicketTOs) {
									if (eticketTO.getPnrPaxFareSegId().equals(reservationPaxFareSegment.getPnrPaxFareSegId())) {
										if (FLOWN_FROM.ETICKET == flownCalculateMethod
												&& eticketTO.getTicketStatus().equals(EticketStatus.FLOWN.code())) {
											flownPnrSegIds.add(reservationSegment.getPnrSegId());
											isFlown = true;
										} else if (FLOWN_FROM.PFS == flownCalculateMethod
												&& eticketTO.getPaxStatus().equals(
														ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)) {
											flownPnrSegIds.add(reservationSegment.getPnrSegId());
											isFlown = true;
										} else if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()
												&&	(EticketStatus.CHECKEDIN.code().equals(eticketTO.getTicketStatus())
														|| EticketStatus.BOARDED.code().equals(eticketTO.getTicketStatus()))){
											flownPnrSegIds.add(reservationSegment.getPnrSegId());
											isFlown = true;
										}
										break;
									}
								}

							} else {
								for (ReservationSegmentDTO resSegmentDTO : segmentsView) {
									if (reservationSegment.getPnrSegId().equals(resSegmentDTO.getPnrSegId())
											&& currentDate.compareTo(resSegmentDTO.getZuluDepartureDate()) > 0) {
										flownPnrSegIds.add(reservationSegment.getPnrSegId());
										isFlown = true;
									}

								}

							}

							if (!isFlown) {
								colExchangableFltSegIds.add(reservationSegment.getFlightSegId());
								colExchangablePnrSegIds.add(reservationSegment.getPnrSegId());
								if (!excSegments.contains(reservationSegment)) {
									excSegments.add(reservationSegment);
									exchangePnrSegIdByFltSegId.put(reservationSegment.getFlightSegId(),
											reservationSegment.getPnrSegId());

								}

								oldFares.add(reservationPaxFare);
							}
						} else {
							cnxPnrSegIds.add(reservationSegment.getPnrSegId());
						}

					}

				}

				if (excSegments == null || excSegments.isEmpty() || oldFares == null || oldFares.isEmpty()) {
					throw new ModuleException("airreservations.ext.coupon.exchange.no.valid.segment");
				}
				double discountPercentage = 0;
				Collection<ReservationPaxFare> exchangePaxFareColl = CloneBO.cloneFares(resPax, oldFares, null, null, null, discountPercentage);
				exchangeSegPaxFareByPaxId.put(resPax.getPnrPaxId(), exchangePaxFareColl);

			}
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		Map<Integer, ReservationSegment> pnrSegmentByExchangeSegId = CloneBO.cloneSegments(excSegments);

		response.addResponceParam(CommandParamNames.FLOWN_PNR_SEG_ID_LIST, flownPnrSegIds);
		response.addResponceParam(CommandParamNames.EXCHANGE_PNR_SEG_ID_LIST, colExchangablePnrSegIds);
		response.addResponceParam(CommandParamNames.EXCHANGE_FLT_SEG_ID_LIST, colExchangableFltSegIds);
		response.addResponceParam(CommandParamNames.CNX_PNR_SEG_ID_LIST, cnxPnrSegIds);
		response.addResponceParam(CommandParamNames.EXCHANGE_PAX_FARE_MAP, exchangeSegPaxFareByPaxId);
		response.addResponceParam(CommandParamNames.EXCHANGE_PNR_SEGMENT_MAP, pnrSegmentByExchangeSegId);
		response.addResponceParam(CommandParamNames.EXCHANGE_PNR_SEG_ID_MAP, exchangePnrSegIdByFltSegId);

		if (log.isDebugEnabled())
			log.debug("Exit prepareExchangeSegment execute");

		return response;
	}

	private void validateParams(String pnr) throws ModuleException {
		if (log.isDebugEnabled())
			log.debug("Inside validateParams");

		if (pnr == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (log.isDebugEnabled())
			log.debug("Exit validateParams");
	}

	private void validateReservation(Reservation reservation) throws ModuleException {
		if (log.isDebugEnabled())
			log.debug("Inside validateReservation");

		if (reservation == null) {
			throw new ModuleException("airreservations.arg.pnrNumberDoesNotExist");
		}

		if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			throw new ModuleException("airreservations.arg.invalidPnrCancel");
		}

		if (!ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			throw new ModuleException("airreservations.ext.coupon.exchange.invalid.pnr.status");
		}

		if (log.isDebugEnabled())
			log.debug("Exit validateReservation");
	}

}