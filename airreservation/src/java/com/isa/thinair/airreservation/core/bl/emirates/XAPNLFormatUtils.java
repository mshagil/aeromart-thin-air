/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.emirates;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.emirates.EMPNLRecordDTO;
import com.isa.thinair.airreservation.api.dto.emirates.EMRecordDTO;
import com.isa.thinair.airreservation.api.dto.emirates.EMRecordRemarkDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * The utility to validate PNL formating and ground keeping work
 * 
 * @author Byorn
 * @since 1.0
 */
public class XAPNLFormatUtils {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(XAPNLFormatUtils.class);

	/** Holds the pnl file name date format */
	private static final String XAPNL_FILE_NAME_DATE_FORMAT = "ddMMyyyyHHmmss";

	/** Holds the pnl file extension */
	private static final String XAPNL_FILE_EXTENSION = ".txt";

	/**
	 * Returns the pnl file name
	 * 
	 * @param currentDate
	 * @param message
	 * @return
	 */
	public static String getPnlFileName(Date currentDate, int message) {
		return "XAPNL-" + BeanUtils.parseDateFormat(currentDate, XAPNL_FILE_NAME_DATE_FORMAT) + "-" + message
				+ XAPNL_FILE_EXTENSION;
	}

	/**
	 * Move and delete pnl documents
	 * 
	 * @param pnlProcessPath
	 * @param pnlParsedPath
	 */
	private static void moveAndDeleteXAPnls(String pnlProcessPath, String pnlParsedPath, String strMsgName) {
		File dirPnlPath = new File(pnlProcessPath);
		File[] files = dirPnlPath.listFiles();

		// Moving the new files
		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			// If file name exist
			if (strMsgName.equals(file.getName())) {
				File dirParsedPnl = new File(pnlParsedPath);

				// If directory does not exist create one
				if (!dirParsedPnl.exists()) {
					dirParsedPnl.mkdirs();
				}

				File copyFile = new File(dirParsedPnl, file.getName());
				copyAndIgnoreErrors(file, copyFile);
				file.delete();
			}
		}
	}

	/**
	 * The below scenario needs to be handled. (1FERNANDO/ESMIMS/DUDEMR .L/J800076 .R/1INF BABY .R/BSCT HK1 PAX NEED
	 * BABY BASSINET/DUDEMR)
	 * 
	 * @param emPnlrecordDTO
	 * @return
	 * @throws ModuleException
	 */
	protected static void prepareRemarks(EMPNLRecordDTO emPnlrecordDTO) throws ModuleException {

		try {
			boolean onlyOnePax = false;

			Collection<EMRecordDTO> records = emPnlrecordDTO.getRecords();
			Collection<EMRecordRemarkDTO> remarks = emPnlrecordDTO.getRemarks();

			if (remarks == null || remarks.size() == 0) {
				return;
			}

			// throw exception if no pax records
			checkParams(records);

			if (records.size() == 1 && remarks.size() > 0) {

				onlyOnePax = true;
			}

			Iterator<EMRecordDTO> iterRecords = records.iterator();

			while (iterRecords.hasNext()) {
				EMRecordDTO recordDTO = (EMRecordDTO) iterRecords.next();
				populateEmRecordDTO(recordDTO, remarks, onlyOnePax);

			}

		} catch (Exception e) {
			emPnlrecordDTO.setErrorDescription("Exception occured inside prepareRemarks");
		}
	}

	/**
	 * 
	 * @param recordDTO
	 * @param remarks
	 */
	private static void populateEmRecordDTO(EMRecordDTO recordDTO, Collection<EMRecordRemarkDTO> remarks, boolean onlyOnePax) {

		try {

			String firstNameWithTitle = recordDTO.getFirstNameWithTitle();
			Iterator<EMRecordRemarkDTO> iterRemarks = remarks.iterator();

			while (iterRemarks.hasNext()) {
				EMRecordRemarkDTO recordRemarkDTO = (EMRecordRemarkDTO) iterRemarks.next();
				String adviceTo = recordRemarkDTO.getAdviceTo();
				boolean isChildRemark = recordRemarkDTO.isChildRelatedDTO();
				if (adviceTo != null)
					adviceTo.trim();

				// if there is a matching advice name
				// or if there is only one pax,
				// or if its a childremark dto
				// then okay to add it to link it to the RecordDTO.
				if (onlyOnePax == true || firstNameWithTitle.equals(adviceTo) || isChildRemark) {

					// if no ssr code set the error
					if (recordRemarkDTO.getSsrCode() == null || recordRemarkDTO.getSsrCode().equals("")) {
						recordDTO.setErrorDescription("Remark didnt have infant or an ssr");
					}

					// if its not a infant remark or a child remark thenn set the ssrcode and text
					if (!recordRemarkDTO.isInfantRelatedDTO() && !recordRemarkDTO.isChildRelatedDTO()) {

						recordDTO.setSsrCode(recordRemarkDTO.getSsrCode());
						recordDTO.setSsrText(recordRemarkDTO.getCleanedUpSSRText());
					}

					// if its an infant remark then get the names
					if (recordRemarkDTO.isInfantRelatedDTO()) {
						recordDTO.setInfantName(recordRemarkDTO.getInfantfirstName());
						recordDTO.setInfantLastName(recordRemarkDTO.getInfantLastName());
					}

					if (recordRemarkDTO.isChildRelatedDTO()) {

					}

				}

				recordDTO.setErrorDescription(recordRemarkDTO.getErrorDescription());
			}
		} catch (Exception e) {
			recordDTO.setErrorDescription("Error Occured in populateEmRecordDTO method");
		}
	}

	/**
	 * 
	 * @param records
	 * @throws ModuleException
	 */
	private static void checkParams(Collection<EMRecordDTO> records) throws ModuleException {
		if (records == null || records.size() == 0) {

			throw new ModuleException("airreservations.xaPnl.dtoNoRec");
		}

	}

	/**
	 * This method only check if the document has the Main PNL Tag. Returns validated XAPnl documents
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	public static void validateMessages(String xaPnlProcessPath, String xaPnlErrorPath) {

	//	Map uniqueKeyAndXAPnlPartsMap = new HashMap();
		Collection<String> errorFileNames = new ArrayList<String>();
	//	Map partIdAndXAPnlDocs;
		String strHolder;
		BufferedReader buffer;
		boolean isXAPnlFormatCompliant;
		boolean isNextValueUniqueKey;
		boolean isMultipleXAPnlExist;
		int partNumber = -1;
		String uniqueKey = null;

		File directory = new File(xaPnlProcessPath);
		File[] files = directory.listFiles();

		// Validating the XAPnl documents
		for (int i = 0; i < files.length; i++) {
			try {
				buffer = new BufferedReader(new FileReader(files[i]));
				isXAPnlFormatCompliant = false;
				isNextValueUniqueKey = false;
				isMultipleXAPnlExist = false;

				while ((strHolder = buffer.readLine()) != null) {

					// Checking for XAPnl text
					if (strHolder.matches("PNL")) {
						isXAPnlFormatCompliant = true;
						isNextValueUniqueKey = true;

						// Checking for flight number, departure date, departure station and part number
					} else if (isNextValueUniqueKey && strHolder.length() > 0) {
						strHolder = BeanUtils.nullHandler(strHolder);
						uniqueKey = strHolder.substring(0, strHolder.indexOf("PART"));
						uniqueKey = BeanUtils.nullHandler(uniqueKey);
						partNumber = Integer.parseInt(strHolder.substring(strHolder.indexOf("PART") + 4, strHolder.length()));
						isNextValueUniqueKey = false;
					}

					// This means that XAPnl is spanning for multiple parts
					/*
					 * if (isMultipleXAPnlExist == false && uniqueKey != null && (strHolder.indexOf("ENDPART") != -1 ||
					 * partNumber > 1)) { isMultipleXAPnlExist = true;
					 * 
					 * // If unique key already exist if (uniqueKeyAndXAPnlPartsMap.containsKey(uniqueKey)) {
					 * partIdAndXAPnlDocs = (Map) uniqueKeyAndXAPnlPartsMap.get(uniqueKey); partIdAndXAPnlDocs.put(new
					 * Integer(partNumber), files[i].getName()); } else { // Initializing a Tree Map inorder to keep the
					 * parts sorted partIdAndXAPnlDocs = new TreeMap(); partIdAndXAPnlDocs.put(new Integer(partNumber),
					 * files[i].getName());
					 * 
					 * uniqueKeyAndXAPnlPartsMap.put(uniqueKey, partIdAndXAPnlDocs); } }
					 */
				}

				// If any non XAPnl format compliant document exist
				if (isXAPnlFormatCompliant == false) {
					errorFileNames.add(files[i].getName());
				}
			} catch (Exception e) {
				// Purposly catching the exception to continue with other documents
				log.error(" ERROR VALIDATING THE XA Pnl NAMED : " + files[i].getName() + " " + e.getMessage());
			}
		}

		// Move the error file to another directory
		moveErrorFiles(errorFileNames, files, xaPnlErrorPath);

		/*
		 * // Handling the multiple XAPnl documents Iterator itUniqueKey =
		 * uniqueKeyAndXAPnlPartsMap.keySet().iterator(); while (itUniqueKey.hasNext()) { uniqueKey = (String)
		 * itUniqueKey.next(); partIdAndXAPnlDocs = (Map) uniqueKeyAndXAPnlPartsMap.get(uniqueKey);
		 * 
		 * // If Mulitple XAPnl document found // Otherwise the part will left over till the complete parts are
		 * available if (partIdAndXAPnlDocs.values().size() > 1) { mergeMultipleXAPnltoOneXAPnl(xaPnlProcessPath,
		 * uniqueKey, partIdAndXAPnlDocs.values()); } }
		 */

	}

	/**
	 * Return message names
	 * 
	 * @param path
	 * @return
	 */
	public static Collection<String> getMessageNames(String path) {
		File directory = new File(path);
		File[] files = directory.listFiles();
		Collection<String> colMsgNames = new ArrayList<String>();

		for (int i = 0; i < files.length; i++) {
			colMsgNames.add(files[i].getName());
		}

		return colMsgNames;
	}

	/**
	 * Move the error file names
	 * 
	 * @param errorFileNames
	 * @param files
	 * @param pnlErrorPath
	 */
	private static void moveErrorFiles(Collection<String> errorFileNames, File[] files, String pnlErrorPath) {
		// Moving the pnl error documents to a error folder
		if (errorFileNames.size() > 0) {
			for (int i = 0; i < files.length; i++) {
				File f = files[i];

				// If file name exist
				if (errorFileNames.contains(f.getName())) {
					File dirErrorPnl = new File(pnlErrorPath);

					// If directory does not exist create one
					if (!dirErrorPnl.exists()) {
						dirErrorPnl.mkdirs();
					}

					File copyFile = new File(dirErrorPnl, "NON-" + f.getName());
					// Copy and ignore errors
					copyAndIgnoreErrors(f, copyFile);

					// Delete the current file
					f.delete();
				}
			}
		}
	}

	/**
	 * Copies src file to dst file. If the dst file does not exist, it is created
	 * 
	 * @param src
	 * @param dst
	 */
	private static void copyAndIgnoreErrors(File src, File dst) {
		try {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dst);

			// Transfer bytes from in to out
			byte[] buf = new byte[1024];
			int len;

			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			in.close();
			out.close();

		} catch (Exception e) {
			// This can not happen practically
			log.info("################## UNEXCEPECTED PNL COPYING ERROR OCCURED ", e);
		}
	}

	/**
	 * @param args
	 * 
	 *            Temporary Main Method
	 */
	public static void main(String[] args) throws Exception {
		// validateMessages("C:/sample/multi", "C:/sample/error");
		moveAndDeleteXAPnls("c:/PnlProcessPath", "c:/PnlParsedPath", "msg-18072006105728-0.txt");
	}
}
