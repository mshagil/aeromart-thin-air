package com.isa.thinair.airreservation.core.bl.emirates;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.emirates.XAPnlLogDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;

/**
 * Class to implement the bussiness logic related to the Mailing of Failed XA Pnls
 * 
 * @author Byorn
 */
abstract class XAMailError {

	private static Log log = LogFactory.getLog(XAMailError.class);

	/**
	 * Notify Error
	 * 
	 * @param logDTO
	 * @throws ModuleException
	 */
	protected static void notifyError(XAPnlLogDTO logDTO) throws ModuleException {

		try {
			sendMailtoRelaventAuthorities(logDTO);
		} catch (Exception e) {
			log.error("############### ERROR IN NOTIFY ERROR METHOD ", e);
			throw new ModuleException(e, "airreservations.xaPnl.sendMail");

		}

	}

	/**
	 * Will take a list of of email addresses from a config file and will send email to them without exception details
	 * 
	 * @param logDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendMailtoRelaventAuthorities(XAPnlLogDTO logDTO) {

		Properties props = ReservationModuleUtils.getAirReservationConfig().getEmailXAPnlErrorTo();
		Enumeration enumeration = props.elements();
		List messageList = new ArrayList();

		while (enumeration.hasMoreElements()) {

			String emailAddress = (String) enumeration.nextElement();
			logDTO.setEmailTo(emailAddress);
			UserMessaging user = new UserMessaging();
			user.setFirstName(emailAddress);
			// user.setLastName(alertDTO.get
			user.setToAddres(emailAddress);
			messageList.add(user);
		}

		sendMail(messageList, logDTO);
	}

	/**
	 * Is called by the above two methods
	 * 
	 * @param messageList
	 * @param logDTO
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendMail(List messageList, XAPnlLogDTO logDTO) {

		List list = new ArrayList();

		Iterator iterator = messageList.iterator();
		while (iterator.hasNext()) {

			MessageProfile profile = new MessageProfile();
			List msgList = new ArrayList();
			msgList.add(iterator.next());
			profile.setUserMessagings(msgList);
			Topic topic = new Topic();
			HashMap map = new HashMap();
			map.put("xaPnlLogDTO", logDTO);

			topic.setTopicParams(map);
			topic.setTopicName("xapnl_error");
			profile.setTopic(topic);
			list.add(profile);

		}
		ReservationModuleUtils.getMessagingServiceBD().sendMessages(list);
	}

}
