/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.bl.adl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.isa.thinair.airreservation.api.dto.adl.ADLMetaDataDTO;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PNLADLElements;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.util.StringBufferedHandler;

/**
 * 
 * This class will encapsulate theADL formatting function
 * 
 * @author Byorn
 */
class ADLFormatter {

	private final ArrayList<String> filenames = new ArrayList<String>();

	public ArrayList<String> getFileNameList() {
		return this.filenames;
	}

	@SuppressWarnings("rawtypes")
	// TODO Yo developers, pls refactor me if possible
	protected ADLFormatter(ArrayList adlList, ADLMetaDataDTO amdto) throws Exception {
		int maxLineLength = PNLConstants.PNLADLElements.MAX_LINE_LENGTH_IATA;
		Properties p = new Properties();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");

		String templatesRootDir = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
				.getTemplatesroot();

		p.setProperty("file.resource.loader.path", templatesRootDir);
		Velocity.init(p);

		VelocityContext context = new VelocityContext();
		context.put("fileelementlist", adlList);// list
		context.put("metadata", amdto);

		/* get the Template */
		Template t = Velocity.getTemplate("adlfiletemplate.vm");

		/* now render the template into a Writer */
		// FileWriter fw=new FileWriter("C:\\adllist.txt");
		StringWriter sw = new StringWriter();
		t.merge(context, sw);

		StringBufferedHandler buf = new StringBufferedHandler();

		StringTokenizer str = new StringTokenizer(sw.toString(), "\n");

		while (str.hasMoreTokens()) {

			String text = str.nextToken().trim();

			if (!text.equals("") && text.charAt(text.length() - 1) == '\r') {
				text = text.substring(0, text.length() - 1);
			}

			int lines = PnlAdlUtil.linesRequired(text);

			if (lines > 1) {

				String s = null;
				char delimiter = ' ';

				int cc = 0;
				boolean hasMoreRecords = true;
				while (hasMoreRecords) {

					if (text.length() > maxLineLength) {
						s = text.substring(0, maxLineLength);

						delimiter = text.substring(maxLineLength).trim().charAt(0);

					} else {
						if (text.trim().indexOf(".") == 0) {
							buf.write(text.trim());
							buf.newLine();
						} else if (text.length() < 61) {
							buf.write(".RN/");
							buf.write(text.trim());
							buf.newLine();
						} else {
							int dotPlace = text.lastIndexOf(".");
							int hkPlace = text.lastIndexOf("HK1");
							int breakIndex = dotPlace;
							if (dotPlace > 0) {
								breakIndex = dotPlace;
							} else if (hkPlace > 0) {
								breakIndex = hkPlace;
							}

							if (breakIndex > -1) {
								// Length >60 and there is a .X some where.
								// Split at the last dot
								buf.write(".RN/");
								buf.write(text.substring(0, breakIndex));
								buf.newLine();
								buf.write(text.substring(breakIndex));
								buf.newLine();
							} else {
								// Lenth >60 and no .X in the middle
								buf.write(".RN/");
								buf.write(text.substring(0, 60));
								buf.newLine();
								if (text.length() > 60) {
									buf.write(".RN/");
									buf.write(text.substring(60));
									buf.newLine();
								}
							}
						}

						break;
					}

					int splitIndex = s.lastIndexOf('.');

					int dotPlace = s.lastIndexOf(".");

					if (dotPlace > 0) {
						splitIndex = dotPlace;
					} else if (delimiter == '.') {
						splitIndex = maxLineLength;
					}

					if (splitIndex < 0) {

						String part1 = ".RN/" + text.substring(0, 60);
						buf.write(part1.trim());
						buf.newLine();
						int len = text.length();
						text = text.substring(60, len);

					} else if (splitIndex == 0) {
						String tmptext = text.substring(0, maxLineLength);
						int tmplast = tmptext.lastIndexOf(".");
						if (tmplast == splitIndex) {
							String part1 = text.substring(0, maxLineLength);
							buf.write(part1.trim());
							buf.newLine();
							int len = text.length();
							text = text.substring(maxLineLength, len);
						}

					}

					else {
						if (!(PnlAdlUtil.isNumber(text.charAt(0)) && (cc == 0)) & splitIndex > 0 & text.charAt(0) != '.') {
							String part1 = ".RN/" + text.substring(0, splitIndex);
							buf.write(part1.trim());
							buf.newLine();
							int len = text.length();
							text = text.substring(splitIndex, len);

						} else if (!PnlAdlUtil.isNumber(text.charAt(0)) & splitIndex < 0 & text.charAt(0) != '.') {
							String part1 = ".RN/" + text.substring(0, maxLineLength);
							buf.write(part1.trim());
							buf.newLine();
							int len = text.length();
							text = text.substring(maxLineLength, len);

						} else {

							String part1 = text.substring(0, splitIndex);
							buf.write(part1.trim());
							buf.newLine();
							int len = text.length();
							text = text.substring(splitIndex, len);

						}

					}

					if(text.length() == 0){
                    	hasMoreRecords = false;
                    }
					cc++;
				}
			} else {

				buf.write(text.trim());
				buf.newLine();

			}
		}

		String flightNo = amdto.getFlight();
		String board = amdto.getBoardingairport();
		String month = amdto.getMonth();
		String day = amdto.getDay();
		if (Integer.parseInt(day) <= 9)
			day = "0" + day;

		StringBufferedHandler bufin = buf;

		String genadlpath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig()).getGenadlpath();

		File dir = new File(genadlpath);
		// If directory does not exist create one
		if (!dir.exists()) {
			dir.mkdirs();
		}

		String path2 = genadlpath + "/" + flightNo + "-" + board + "-" + month + "" + day + "-" + "ADL" + "-"
				+ simpleDateFormat.format(new Date()) + "-" + "PART1" + ".txt";

		FileWriter fw = new FileWriter(path2);
		filenames.add(path2);

		BufferedWriter bufout = new BufferedWriter(fw);
		String str1 = null;
		String line1 = null, line2 = null, line3 = null;

		int lineCount = 0;
		int multiply = 1;
		int count1 = 0;

		String action = null;
		boolean withO = false;
		boolean isEndReached = false;
		while ((str1 = bufin.readLine()) != null) {
			// String text=str1.trim();

			if (!str1.equalsIgnoreCase("")) {

				// Saving the previous action in case multiple adl exists
				if (str1.trim().equals(PNLADLElements.ADL_ADD) || str1.trim().equals(PNLADLElements.ADL_DEL)
						|| str1.trim().equals(PNLADLElements.ADL_CHG)) {
					action = str1.trim();
				}
				boolean hasEnoughLength = true;
				if (PnlAdlUtil.isNumber(str1.charAt(0))) {
					String dupStr = null;
					int nextPaxLineLength = 1;
					StringBufferedHandler dupBufin = (StringBufferedHandler) bufin.clone();
					while ((dupStr = dupBufin.readLine()) != null) {
						if (!PnlAdlUtil.isNumber(dupStr.charAt(0))) {
							nextPaxLineLength++;
						} else {
							if (maxLineLength < lineCount + nextPaxLineLength) {
								bufin.reset();
								hasEnoughLength = false;
							}
							break;
						}
					}
				}
				if (hasEnoughLength) {
					bufout.write(str1.trim());
					bufout.newLine();
					lineCount++;
					if (str1.startsWith(PNLADLElements.END_ADL)) {
						isEndReached = true;
					}
				}

				if (str1.charAt(0) == '-') {
					line3 = str1.trim();
				}

				if (lineCount == 1)
					line1 = str1.trim();
				if (lineCount == 2)
					line2 = str1.trim();
				if ((str1.charAt(0) == '.' | (PnlAdlUtil.isNumber(str1.charAt(0)) & str1.indexOf(".O") > 0)) && hasEnoughLength) {

					bufin.mark(lineCount);
					count1 = lineCount;
				}
				if ((lineCount == maxLineLength || !hasEnoughLength) && !isEndReached) {

					if (PnlAdlUtil.isNumber(str1.charAt(0)) & str1.indexOf(".O") < 0 & withO) {

						bufin.reset();
						lineCount = count1;

					}

					bufout.write("ENDPART" + multiply);
					lineCount = 0;
					bufout.close();
					fw.close();
					multiply++;
					String path1 = genadlpath + "/" + flightNo + "-" + board + "-" + month + "" + day + "-" + "ADL" + "-"
							+ simpleDateFormat.format(new Date()) + "-" + "PART" + multiply + ".txt";

					fw = new FileWriter(path1);
					filenames.add(path1);
					bufout = new BufferedWriter(fw);

					bufout.write(line1);
					bufout.newLine();
					lineCount++;
					String oline2 = line2.substring(0, line2.length() - 1);
					bufout.write(oline2 + multiply);
					lineCount++;
					bufout.newLine();
					bufout.write(line3);
					lineCount++;
					bufout.newLine();
					bufout.write(action);
					lineCount++;
					bufout.newLine();
				}
			}

		}

		bufout.close();
		fw.close();
		bufin.close();

	}

}
