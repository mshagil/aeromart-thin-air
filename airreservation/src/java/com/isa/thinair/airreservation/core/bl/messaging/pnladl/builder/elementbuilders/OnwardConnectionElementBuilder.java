/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.OnwardElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.PnrElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.PNLADLMessageConstants;

/**
 * @author udithad
 *
 */
public class OnwardConnectionElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private PassengerInformation passengerInformation;

	private BaseRuleExecutor<RulesDataContext> onwardElementRuleExecutor = new OnwardElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		currentElement = "";
		initContextData(context);
		onwardConnectionElementTemplate();
		executeNext();
	}

	private void onwardConnectionElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		List<OnWardConnectionDTO> onwardConnections = null;
		if (passengerInformation != null
				&& passengerInformation.getOnwardconnectionlist() != null) {
			onwardConnections = passengerInformation.getOnwardconnectionlist();
			int index = 1;
			for (OnWardConnectionDTO onwardConnection : onwardConnections) {
					clearTemplate(elementTemplate);
					elementTemplate
							.append(MessageComposerConstants.PNLADLMessageConstants.ONWARD);
					if (index != 1) {
						elementTemplate.append(index);
					}
					elementTemplate.append(forwardSlash());
					elementTemplate.append(onwardConnection.getFlight());
					elementTemplate.append(onwardConnection.getFareclass());
					elementTemplate
							.append((onwardConnection.getDay() < 10) ? "0"
									+ onwardConnection.getDay()
									: onwardConnection.getDay());
					elementTemplate.append(onwardConnection.getArrivalpoint());
					elementTemplate.append(onwardConnection.getDeparturetime());
					if(onwardConnection.getDateDifference() != 0 && onwardConnection.getDateDifference() > 0){
						if(onwardConnection.getArrivalTime() != null){
							elementTemplate.append(onwardConnection.getArrivalTime());
						}
						elementTemplate.append("/");
						elementTemplate.append(onwardConnection.getDateDifference());
					}else if(onwardConnection.getDateDifference() != 0 && onwardConnection.getDateDifference() < 1){
						if(onwardConnection.getArrivalTime() != null){
							elementTemplate.append(onwardConnection.getArrivalTime());
						}
						elementTemplate.append("/M");
						elementTemplate.append(onwardConnection.getDateDifference());
					}
					elementTemplate
							.append(MessageComposerConstants.PNLADLMessageConstants.HK);

					index++;
					currentElement = elementTemplate.toString();
					ammendMessageDataAccordingTo(validateSubElement(currentElement));
			}
		}
	}

	private boolean validateSegment(OnWardConnectionDTO inboundConnectionDTO) {
		boolean isValied = true;
		String departureAirportCode = uPContext.getDepartureAirportCode();
		String destinationAirportCode = uPContext.getDestinationAirportCode();
		if (departureAirportCode != null && destinationAirportCode != null
				&& !departureAirportCode.isEmpty()
				&& !destinationAirportCode.isEmpty()) {
			String segmentCode = destinationAirportCode + "/"
					+ departureAirportCode ;
			if (inboundConnectionDTO.getSegmentCode() != null) {
				if (inboundConnectionDTO.getSegmentCode().equals(segmentCode)) {
					isValied = false;
				}
			}
		}

		return isValied;
	}

	private void clearTemplate(StringBuilder elementTemplate) {
		elementTemplate.setLength(0);
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformation = getFirstPassengerInUtilizedList();
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (currentElement != null && !currentElement.isEmpty()) {
			if (response.isProceedNextElement()) {
				if (response.isSpaceAvailable()) {
					executeSpaceElementBuilder(uPContext);
				} else {
					executeConcatenationElementBuilder(uPContext);
				}
				ammendToBaseLine(currentElement, currentLine, messageLine);
			}
		}
	}

	private PassengerInformation getFirstPassengerInUtilizedList() {
		PassengerInformation passengerInformation = null;
		int index = 0;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformation = uPContext.getUtilizedPassengers().get(index);
		}
		return passengerInformation;
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				"0"+currentLine.toString()+" ", elementText, 0);
		ruleResponse = onwardElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		PnrElementRuleContext rulesDataContext = new PnrElementRuleContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		rulesDataContext.setPnrWiseGroupCodes(uPContext.getPnrWiseGroupCodes());
		rulesDataContext.setPnrWisePassengerCount(uPContext
				.getPnrWisePassengerCount()
				.get(uPContext.getOngoingStoreType()));
		rulesDataContext.setPnrWisePaxReductionCout(uPContext
				.getPnrWisePassengerReductionCount().get(
						uPContext.getOngoingStoreType()));
		rulesDataContext.setUtilizedPassengerCount(uPContext
				.getUtilizedPassengers().size());
		rulesDataContext.setPnr(uPContext.getPnr());
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
