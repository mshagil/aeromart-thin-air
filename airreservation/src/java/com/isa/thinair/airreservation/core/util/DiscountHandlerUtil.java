package com.isa.thinair.airreservation.core.util;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.ChargeResponseDTO;
import com.isa.thinair.airreservation.api.dto.revacc.RefundablesMetaInfoTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class DiscountHandlerUtil {
	public static void filterDiscount(ChargeResponseDTO chargeResponseDTO) {
		if (AppSysParamsUtil.splitPromoDiscountIntoRefundableAndNonRefundables()) {
			Collection<ChargeMetaTO> colRefundableChargeMetaTO = (Collection<ChargeMetaTO>) chargeResponseDTO
					.getRefundableChargeMetaTOs();
			BigDecimal[] refundAmounts = (BigDecimal[]) chargeResponseDTO.getRefundAmounts();

			BigDecimal otherRefundableAmounts = AccelAeroCalculator.add(refundAmounts[0], refundAmounts[1], refundAmounts[2],
					refundAmounts[3], refundAmounts[4], refundAmounts[5], refundAmounts[6]);
			BigDecimal totalDiscountAmount = (refundAmounts[7]).abs();

			if (AccelAeroCalculator.isGreaterThan(totalDiscountAmount, otherRefundableAmounts)) {
				BigDecimal nonRefundableDiscountAmount = AccelAeroCalculator
						.subtract(totalDiscountAmount, otherRefundableAmounts);
				refundAmounts[7] = otherRefundableAmounts.negate();

				ChargeMetaTO nonRefundableDiscChargeMetaTO = null;
				for (ChargeMetaTO chargeMetaTO : colRefundableChargeMetaTO) {
					if (ReservationInternalConstants.ChargeGroup.DISCOUNT.equals(chargeMetaTO.getChargeGroupCode())) {
						chargeMetaTO.setChargeAmount(refundAmounts[7]);

						nonRefundableDiscChargeMetaTO = new ChargeMetaTO();
						nonRefundableDiscChargeMetaTO.setPnrPaxId(chargeMetaTO.getPnrPaxId());
						nonRefundableDiscChargeMetaTO.setPaxType(chargeMetaTO.getPaxType());
						nonRefundableDiscChargeMetaTO.setChargeDate(chargeMetaTO.getChargeDate());
						nonRefundableDiscChargeMetaTO.setChargeAmount(nonRefundableDiscountAmount);
						nonRefundableDiscChargeMetaTO.setChargeGroupCode(chargeMetaTO.getChargeGroupCode());
						nonRefundableDiscChargeMetaTO.setOndCode(chargeMetaTO.getOndCode());
						nonRefundableDiscChargeMetaTO.setChargeRateId(chargeMetaTO.getChargeRateId());
						nonRefundableDiscChargeMetaTO.setChargeCode(chargeMetaTO.getChargeCode());
						nonRefundableDiscChargeMetaTO.setChargeDescription(chargeMetaTO.getChargeDescription());
						nonRefundableDiscChargeMetaTO.setAirportTaxCategory(chargeMetaTO.getAirportTaxCategory());
					}
				}

				if (nonRefundableDiscChargeMetaTO != null) {
					Collection<ChargeMetaTO> colNonRefundableChargeMetaTO = (Collection<ChargeMetaTO>) chargeResponseDTO
							.getNonRefundableChargeMetaTOs();
					colNonRefundableChargeMetaTO.add(nonRefundableDiscChargeMetaTO);
				}

			}
		}
	}

	public static boolean filterDiscount(RefundablesMetaInfoTO refundablesMetaInfoTO) {
		boolean isFiltered = false;
		if (AppSysParamsUtil.splitPromoDiscountIntoRefundableAndNonRefundables()) {
			Collection<ReservationPaxOndCharge> colRefundableChargeMetaTO = refundablesMetaInfoTO
					.getMapRefundablePaxOndChargeMetaInfo().values();

			BigDecimal totalDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal otherRefundableAmounts = AccelAeroCalculator.getDefaultBigDecimalZero();

			ReservationPaxOndCharge discountPaxOndCharge = null;
			for (ReservationPaxOndCharge resPaxOndCharge : colRefundableChargeMetaTO) {
				if (ReservationInternalConstants.ChargeGroup.DISCOUNT.equals(resPaxOndCharge.getChargeGroupCode())) {
					totalDiscountAmount = AccelAeroCalculator.add(totalDiscountAmount, resPaxOndCharge.getAmount());
					discountPaxOndCharge = resPaxOndCharge;
				} else {
					otherRefundableAmounts = AccelAeroCalculator.add(otherRefundableAmounts, resPaxOndCharge.getAmount());
				}
			}

			if (AccelAeroCalculator.isGreaterThan(totalDiscountAmount, otherRefundableAmounts.negate())
					&& discountPaxOndCharge != null) {
				discountPaxOndCharge.setAmount(otherRefundableAmounts.negate());
				refundablesMetaInfoTO.setRefundableTotal(AccelAeroCalculator.add(discountPaxOndCharge.getAmount(),
						otherRefundableAmounts));
				isFiltered = true;
			}
		}

		return isFiltered;

	}
}
