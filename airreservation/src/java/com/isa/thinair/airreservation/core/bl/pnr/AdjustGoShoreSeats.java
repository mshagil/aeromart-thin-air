/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for adjusting go shore seats
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="adjustGoShoreSeats"
 */
public class AdjustGoShoreSeats extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(AdjustGoShoreSeats.class);

	/**
	 * Construct AdjustGoShoreSeats
	 */
	public AdjustGoShoreSeats() {

	}

	/**
	 * Execute method of the AdjustSeats command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		Collection<OndFareDTO> colOndFareDTO = (Collection<OndFareDTO>) this.getParameter(CommandParamNames.OND_FARE_DTOS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		// Checking params
		this.validateParams(colOndFareDTO, credentialsDTO);

		Iterator<OndFareDTO> itColOndFareDTO = colOndFareDTO.iterator();
		OndFareDTO ondFareDTO;
		SegmentSeatDistsDTO segmentSeatDistsDTO;
		Iterator<SegmentSeatDistsDTO> itColSegmentSeatDistsDTO;

		while (itColOndFareDTO.hasNext()) {
			ondFareDTO = itColOndFareDTO.next();

			itColSegmentSeatDistsDTO = ondFareDTO.getSegmentSeatDistsDTO().iterator();

			while (itColSegmentSeatDistsDTO.hasNext()) {
				segmentSeatDistsDTO = itColSegmentSeatDistsDTO.next();
				// Update the go shore
				ReservationBO.updateGoShore(segmentSeatDistsDTO);
			}
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param colOndFareDTO
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(Collection<OndFareDTO> colOndFareDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (colOndFareDTO == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
