/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.XAPnl;
import com.isa.thinair.airreservation.api.model.XAPnlPaxEntry;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.XAPnlDAO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for reconciling reservation
 * 
 * Business Rules: (1) Emirates should have a default agent in placed. Please specify this agent in
 * airreservation-config.xml (2) Default Number of seats will be assigned for this agent from Inventory against this
 * agent (3) Only one segment travels are supported
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="reconcileXAPnlReservation"
 */
public class ReconcileXAPnlReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReconcileXAPnlReservation.class);

	/** Holds the xa pnl dao instance */
	private final XAPnlDAO xaPnlDAO;

	/** Holds the reservation segment dao instance */
	private final ReservationSegmentDAO reservationSegmentDAO;

	/** Hold the reservation bd instance **/
	private final ReservationBD reservationBD;

	/** Holds the reservation query bd instance */
	private final ReservationQueryBD reservationQueryBD;

	/** Holds the default agent code */
	private final Agent agent;

	/** Holds the default agent's station information */
	private final Station station;

	/** Holds the flight segment id */
	private Integer flightSegmentId;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the departure date */
	private Date departureDate;

	/** Holds the from airport */
	private String fromAirport;

	/** Holds the to airport */
	private String toAirport;

	/** Holds the carrier code */
	private String carrierCode;

	/** Holds the cabin class code */
	private String cabinClassCode;

	/**
	 * Construct ReconcileReservation
	 * 
	 * @throws ModuleException
	 */
	private ReconcileXAPnlReservation() throws ModuleException {
		xaPnlDAO = ReservationDAOUtils.DAOInstance.XA_PNL_DAO;

		reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		reservationBD = ReservationModuleUtils.getReservationBD();

		reservationQueryBD = ReservationModuleUtils.getReservationQueryBD();

		String agentCode = BeanUtils.nullHandler(ReservationModuleUtils.getAirReservationConfig().getEmiratesDefaultAgentCode());

		// If agent not specified in the configurations
		if (agentCode.equals("")) {
			throw new ModuleException("airreservations.xaPnl.noDefaultAgentDefined");
		}

		agent = ReservationModuleUtils.getTravelAgentBD().getAgent(agentCode);

		// If agent does not exist
		if (agent == null) {
			throw new ModuleException("airreservations.xaPnl.noDefaultAgentDefined");
		}

		// If agent's station code does not exist
		if (BeanUtils.nullHandler(agent.getStationCode()).equals("")) {
			throw new ModuleException("airreservations.xaPnl.noDefaultAgentStationDefined");
		}

		station = ReservationModuleUtils.getLocationBD().getStation(agent.getStationCode());

		// If station does not exist
		if (station == null) {
			throw new ModuleException("airreservations.xaPnl.noDefaultAgentStationDefined");
		}
	}

	/**
	 * Execute method of the ReconcileXAPnlReservation command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		Integer xaPnlId = (Integer) this.getParameter(CommandParamNames.XA_PNL_ID);

		// Checking params
		this.validateParams(xaPnlId);

		// Get the xa pnl entry
		XAPnl xaPnl = xaPnlDAO.getXAPnlEntry(xaPnlId.intValue());

		// Set the Carrier Code
		this.carrierCode = xaPnl.getCarrierCode();

		// Returns XAPNL entries
		Collection<XAPnlPaxEntry> colXAPnlPaxEntry = xaPnlDAO.getXAPnlPaxEntries(xaPnlId.intValue(), null);

		// Will validate records and derive flight segment information
		this.validateRecordsAndDeriveFlightSegmentInfo(colXAPnlPaxEntry);

		Collection<XAPnlPaxEntry> colErrorXAPnlPaxEntry = new ArrayList<XAPnlPaxEntry>();
		Map<String, Collection<XAPnlPaxEntry>> pnrAndPassengersMap = new HashMap<String, Collection<XAPnlPaxEntry>>();
		Iterator<XAPnlPaxEntry> itColXAPnlPaxEntry = colXAPnlPaxEntry.iterator();
		XAPnlPaxEntry xaPnlPaxEntry;

		while (itColXAPnlPaxEntry.hasNext()) {
			xaPnlPaxEntry = (XAPnlPaxEntry) itColXAPnlPaxEntry.next();

			// If not processed record exist
			if (ReservationInternalConstants.XAPnlProcessStatus.NOT_PROCESSED.equals(xaPnlPaxEntry.getProcessedStatus())) {

				// Grouping same Emirates PNR(s) to AccelAero same PNR(s)
				if (pnrAndPassengersMap.containsKey(xaPnlPaxEntry.getXaPnr())) {
					Collection<XAPnlPaxEntry> passengers = (Collection<XAPnlPaxEntry>) pnrAndPassengersMap.get(xaPnlPaxEntry
							.getXaPnr());
					passengers.add(xaPnlPaxEntry);
				} else {
					Collection<XAPnlPaxEntry> passengers = new ArrayList<XAPnlPaxEntry>();
					passengers.add(xaPnlPaxEntry);
					pnrAndPassengersMap.put(xaPnlPaxEntry.getXaPnr(), passengers);
				}
				// If Error occured record exist
			} else if (ReservationInternalConstants.XAPnlProcessStatus.ERROR_OCCURED.equals(xaPnlPaxEntry.getProcessedStatus())) {
				colErrorXAPnlPaxEntry.add(xaPnlPaxEntry);
			}
		}

		// Process by grouping passenger for pnr
		this.processByGroupingPassengersForPNR(pnrAndPassengersMap, colErrorXAPnlPaxEntry);

		// Save xa pnl pax entries
		xaPnlDAO.saveXAPnlPaxEntries(colXAPnlPaxEntry);

		if (colErrorXAPnlPaxEntry.size() == 0) {
			xaPnl.setProcessedStatus(ReservationInternalConstants.XAPnlProcessStatus.PROCESSED);
		} else {
			xaPnl.setProcessedStatus(ReservationInternalConstants.XAPnlProcessStatus.ERROR_OCCURED);
		}

		// Saving the pfs entry
		xaPnlDAO.saveXAPnlEntry(xaPnl);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Process by grouping passengers for pnr
	 * 
	 * @param pnrAndPassengersMap
	 * @param colErrorXAPnlPaxEntry
	 * @throws ModuleException
	 */
	private void processByGroupingPassengersForPNR(Map<String, Collection<XAPnlPaxEntry>> pnrAndPassengersMap,
			Collection<XAPnlPaxEntry> colErrorXAPnlPaxEntry) throws ModuleException {
		Iterator<String> itPnrs = pnrAndPassengersMap.keySet().iterator();
		Collection<XAPnlPaxEntry> colXAPnlPaxEntry;
		String pnr;

		while (itPnrs.hasNext()) {
			pnr = (String) itPnrs.next();
			colXAPnlPaxEntry = (Collection<XAPnlPaxEntry>) pnrAndPassengersMap.get(pnr);

			// Prepare to create the reservation
			this.prepareToCreateReservation(pnr, colXAPnlPaxEntry, colErrorXAPnlPaxEntry);
		}
	}

	/**
	 * Prepare to create reservation
	 * 
	 * @param externalPNR
	 * @param colXAPnlPaxEntry
	 * @param colErrorXAPnlPaxEntry
	 */
	private void prepareToCreateReservation(String externalPNR, Collection<XAPnlPaxEntry> colXAPnlPaxEntry,
			Collection<XAPnlPaxEntry> colErrorXAPnlPaxEntry) {

		try {
			AvailableFlightSearchDTOBuilder availableFlightSearchDTO = new AvailableFlightSearchDTOBuilder();
			int[] paxCounts = this.getPassengerCounts(colXAPnlPaxEntry);
			availableFlightSearchDTO.setAdultCount(paxCounts[0]);
			availableFlightSearchDTO.setChildCount(paxCounts[1]);
			availableFlightSearchDTO.setInfantCount(paxCounts[2]);
			availableFlightSearchDTO.setFromAirport(fromAirport);
			availableFlightSearchDTO.setToAirport(toAirport);
			availableFlightSearchDTO.setDepartureVariance(0);
			availableFlightSearchDTO.setArrivalVariance(0);
			availableFlightSearchDTO.setDepartureDate(departureDate);
			availableFlightSearchDTO.setReturnFlag(false);
			availableFlightSearchDTO.setAgentCode(agent.getAgentCode());
			availableFlightSearchDTO.setOndCode(fromAirport + "/" + toAirport);

			Collection<Integer> colOutBoundSegments = new ArrayList<Integer>();
			colOutBoundSegments.add(flightSegmentId);
			availableFlightSearchDTO.setOutBoundFlights(colOutBoundSegments);

			availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
					.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
			availableFlightSearchDTO.setCabinClassCode(cabinClassCode);

			// Get the selected fare quote
			SelectedFlightDTO selectedFlightDTO = reservationQueryBD.getFareQuote(availableFlightSearchDTO.getSearchDTO(), null);

			BigDecimal totalAdultQuotaAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalChildQuotaAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalInfantQuotaAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			Collection<OndFareDTO> colOndFareDTO = selectedFlightDTO.getOndFareDTOs(false);

			if (selectedFlightDTO != null) {
				// Only taking the fixed fare
				if (selectedFlightDTO.getFareType() != FareTypes.NO_FARE) {
					double[] totalTaxAmounts;
					double[] totalSChgAmounts;
					for (OndFareDTO ondFareDTO : colOndFareDTO) {

						// Checking the Adult Fare
						if (ReservationApiUtils.isFareExist(ondFareDTO.getAdultFare())) {
							totalAdultQuotaAmount = AccelAeroCalculator.add(totalAdultQuotaAmount,
									AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare()));
						}

						// Checking the Child Fare
						if (ReservationApiUtils.isFareExist(ondFareDTO.getChildFare())) {
							totalChildQuotaAmount = AccelAeroCalculator.add(totalChildQuotaAmount,
									AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare()));
						}

						// Checking the Infant Fare
						if (ReservationApiUtils.isFareExist(ondFareDTO.getInfantFare())) {
							totalInfantQuotaAmount = AccelAeroCalculator.add(totalInfantQuotaAmount,
									AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare()));
						}

						totalTaxAmounts = ondFareDTO.getTotalCharges(ChargeGroups.TAX);
						totalAdultQuotaAmount = AccelAeroCalculator.add(totalAdultQuotaAmount,
								AccelAeroCalculator.parseBigDecimal(totalTaxAmounts[0]));
						totalInfantQuotaAmount = AccelAeroCalculator.add(totalInfantQuotaAmount,
								AccelAeroCalculator.parseBigDecimal(totalTaxAmounts[1]));
						totalChildQuotaAmount = AccelAeroCalculator.add(totalChildQuotaAmount,
								AccelAeroCalculator.parseBigDecimal(totalTaxAmounts[2]));

						totalSChgAmounts = ondFareDTO.getTotalCharges(ChargeGroups.SURCHARGE);
						totalAdultQuotaAmount = AccelAeroCalculator.add(totalAdultQuotaAmount,
								AccelAeroCalculator.parseBigDecimal(totalSChgAmounts[0]));
						totalInfantQuotaAmount = AccelAeroCalculator
								.add(totalInfantQuotaAmount, AccelAeroCalculator.parseBigDecimal(totalSChgAmounts[1]),
										AccelAeroCalculator.parseBigDecimal(ondFareDTO
												.getTotalCharges(ChargeGroups.INFANT_SURCHARGE)[1]));
						totalChildQuotaAmount = AccelAeroCalculator.add(totalChildQuotaAmount,
								AccelAeroCalculator.parseBigDecimal(totalSChgAmounts[2]));
					}
				}
			}

			// This means no flight information plus fare information found
			if (colOndFareDTO == null) {
				throw new ModuleException("airreservations.xaPnl.noFlightsFound");
			}

			// Create the iReservation with fare details.
			IReservation iReservation = new ReservationAssembler(colOndFareDTO, null);

			IPayment iPaymentForAdult = new PaymentAssembler();
			iPaymentForAdult.addAgentCreditPayment(agent.getAgentCode(), totalAdultQuotaAmount, null, null, null, null, null, null, null);

			IPayment iPaymentForChild = new PaymentAssembler();
			iPaymentForChild.addAgentCreditPayment(agent.getAgentCode(), totalChildQuotaAmount, null, null, null, null, null, null, null);

			IPayment iPaymentForParent = new PaymentAssembler();
			iPaymentForParent.addAgentCreditPayment(agent.getAgentCode(),
					AccelAeroCalculator.add(totalAdultQuotaAmount, totalInfantQuotaAmount), null, null, null, null, null, null, null);

			// Add Passengers
			this.addPassengers(iReservation, colXAPnlPaxEntry, iPaymentForAdult, iPaymentForParent, iPaymentForChild);

			// Add Segment Information
			iReservation.addOutgoingSegment(1, flightSegmentId.intValue(), new Integer(1), null, null, null, null, null);

			ReservationContactInfo contactInfo = new ReservationContactInfo();
			contactInfo.setTitle(((XAPnlPaxEntry) BeanUtils.getFirstElement(colXAPnlPaxEntry)).getTitle());
			contactInfo.setFirstName(((XAPnlPaxEntry) BeanUtils.getFirstElement(colXAPnlPaxEntry)).getFirstName());
			contactInfo.setLastName(((XAPnlPaxEntry) BeanUtils.getFirstElement(colXAPnlPaxEntry)).getLastName());
			contactInfo.setCountryCode(station.getCountryCode());
			contactInfo.setMobileNo(agent.getTelephone());
			contactInfo.setFax(agent.getFax());

			// Getting the inbound and out bound information
			String inBoundInfo = ((XAPnlPaxEntry) BeanUtils.getFirstElement(colXAPnlPaxEntry)).getInBoundInfo();
			String outBoundInfo = ((XAPnlPaxEntry) BeanUtils.getFirstElement(colXAPnlPaxEntry)).getOutBoundInfo();

			// Getting the created user note
			String userNote = this.createUserNote(externalPNR, inBoundInfo, outBoundInfo);

			iReservation.addContactInfo(userNote, contactInfo, null);

			// Creating a normal XBE reservation
			ServiceResponce serviceResponce = reservationBD.createWebReservation(iReservation, null, null);
			String strPNR = (String) serviceResponce.getResponseParam(CommandParamNames.PNR);

			// Update reservation status
			this.updateReservationStatus(strPNR, agent.getAgentCode());

			// Update XA Pnl Entries
			this.updateXAPnlEntries(strPNR, ReservationInternalConstants.XAPnlProcessStatus.PROCESSED, null, colXAPnlPaxEntry);
		} catch (ModuleException me) {
			// If any error occured for this entry making it has error occured
			// Update XA Pnl Entries
			this.updateXAPnlEntries(null, ReservationInternalConstants.XAPnlProcessStatus.ERROR_OCCURED, me.getMessageString(),
					colXAPnlPaxEntry);
			colErrorXAPnlPaxEntry.addAll(colXAPnlPaxEntry);
		} catch (Exception e) {
			// If any error occured for this entry making it has error occured
			// Update XA Pnl Entries
			this.updateXAPnlEntries(null, ReservationInternalConstants.XAPnlProcessStatus.ERROR_OCCURED,
					ReconcileXAPnlReservation.ErrorDescription.XA_PNL_APP_ERROR, colXAPnlPaxEntry);
			colErrorXAPnlPaxEntry.addAll(colXAPnlPaxEntry);
		}
	}

	/**
	 * Create the user note
	 * 
	 * @param externalPNR
	 * @param inBoundInfo
	 * @param outBoundInfo
	 * @return
	 */
	private String createUserNote(String externalPNR, String inBoundInfo, String outBoundInfo) {
		String userNote = BeanUtils.nullHandler(carrierCode) + " : " + externalPNR;
		inBoundInfo = BeanUtils.nullHandler(inBoundInfo);
		outBoundInfo = BeanUtils.nullHandler(outBoundInfo);

		// If in bound information exist
		if (!inBoundInfo.equals("")) {
			userNote = userNote + " IN BOUND(" + inBoundInfo + ") ";
		}

		// If out bound information exist
		if (!outBoundInfo.equals("")) {
			userNote = userNote + " OUT BOUND(" + outBoundInfo + ") ";
		}

		return userNote;
	}

	/**
	 * Update XA Pnl Entries Information
	 * 
	 * @param strPNR
	 * @param processStatus
	 * @param errorDesc
	 * @param colXAPnlPaxEntry
	 */
	private void updateXAPnlEntries(String strPNR, String processStatus, String errorDesc,
			Collection<XAPnlPaxEntry> colXAPnlPaxEntry) {
		Iterator<XAPnlPaxEntry> itColXAPnlPaxEntry = colXAPnlPaxEntry.iterator();
		XAPnlPaxEntry xaPnlPaxEntry;

		while (itColXAPnlPaxEntry.hasNext()) {
			xaPnlPaxEntry = (XAPnlPaxEntry) itColXAPnlPaxEntry.next();

			xaPnlPaxEntry.setPnr(strPNR);
			xaPnlPaxEntry.setProcessedStatus(processStatus);
			xaPnlPaxEntry.setErrorDescription(errorDesc);
		}
	}

	/**
	 * Add Passengers
	 * 
	 * @param iReservation
	 * @param colXAPnlPaxEntry
	 * @param iPaymentForAdult
	 * @param iPaymentForParent
	 * @param iPaymentForChild
	 * @throws ModuleException
	 */
	private void addPassengers(IReservation iReservation, Collection<XAPnlPaxEntry> colXAPnlPaxEntry, IPayment iPaymentForAdult,
			IPayment iPaymentForParent, IPayment iPaymentForChild) throws ModuleException {
		Iterator<XAPnlPaxEntry> itColXAPnlPaxEntry = colXAPnlPaxEntry.iterator();
		XAPnlPaxEntry xaPnlPaxEntry;
		String passengerType;
		int i = 1;
		PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();

		while (itColXAPnlPaxEntry.hasNext()) {
			xaPnlPaxEntry = (XAPnlPaxEntry) itColXAPnlPaxEntry.next();

			passengerType = getPassengerType(xaPnlPaxEntry);

			SegmentSSRAssembler adultSegmentSSRs = new SegmentSSRAssembler();

			adultSegmentSSRs.addPaxSegmentSSR(null, SSRUtil.getSSRId(xaPnlPaxEntry.getAdultSSRCode()),
					xaPnlPaxEntry.getAdultSSRRemarks());

			// For Parent
			if (passengerType.equals(PassengerTypes.PARENT)) {
				SegmentSSRAssembler infantSegmentSSRs = new SegmentSSRAssembler();

				iReservation.addParent(xaPnlPaxEntry.getFirstName(), xaPnlPaxEntry.getLastName(), xaPnlPaxEntry.getTitle(), null,
						null, i, i + 1, paxAdditionalInfoDTO, null, iPaymentForParent, adultSegmentSSRs, null,
						null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

				infantSegmentSSRs.addPaxSegmentSSR(null, SSRUtil.getSSRId(xaPnlPaxEntry.getInfantSSRCode()),
						xaPnlPaxEntry.getInfantSSRRemarks());

				iReservation.addInfant(xaPnlPaxEntry.getInfantFirstName(), xaPnlPaxEntry.getInfantLastName(),
						xaPnlPaxEntry.getInfantTitle(), null, null, i + 1, i, paxAdditionalInfoDTO, null, null,
						infantSegmentSSRs, null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
				i = i + 2;

				// For Adult
			} else if (passengerType.equals(PassengerTypes.ADULT)) {
				iReservation.addSingle(xaPnlPaxEntry.getFirstName(), xaPnlPaxEntry.getLastName(), xaPnlPaxEntry.getTitle(), null,
						null, i, paxAdditionalInfoDTO, null, iPaymentForAdult, adultSegmentSSRs, null, null, null,
						null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
				i = i + 1;
				// For Child
			} else if (passengerType.equals(PassengerTypes.CHILD)) {
				iReservation.addChild(xaPnlPaxEntry.getFirstName(), xaPnlPaxEntry.getLastName(), xaPnlPaxEntry.getTitle(), null,
						null, i, paxAdditionalInfoDTO, null, iPaymentForChild, adultSegmentSSRs, null, null, null,
						null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
				i = i + 1;
			}
		}
	}

	/**
	 * Return passenger counts
	 * 
	 * @param colXAPnlPaxEntry
	 * @return
	 * @throws ModuleException
	 */
	private int[] getPassengerCounts(Collection<XAPnlPaxEntry> colXAPnlPaxEntry) throws ModuleException {
		Iterator<XAPnlPaxEntry> itColXAPnlPaxEntry = colXAPnlPaxEntry.iterator();
		XAPnlPaxEntry xaPnlPaxEntry;
		int totalNoOfAdults = 0;
		int totalNoOfChildren = 0;
		int totalNoOfInfants = 0;
		String passengerType;

		while (itColXAPnlPaxEntry.hasNext()) {
			xaPnlPaxEntry = (XAPnlPaxEntry) itColXAPnlPaxEntry.next();

			passengerType = getPassengerType(xaPnlPaxEntry);

			// Parent
			if (passengerType.equals(PassengerTypes.PARENT)) {
				totalNoOfAdults = totalNoOfAdults + 1;
				totalNoOfInfants = totalNoOfInfants + 1;
				// Adult
			} else if (passengerType.equals(PassengerTypes.ADULT)) {
				totalNoOfAdults = totalNoOfAdults + 1;
				// Children
			} else if (passengerType.equals(PassengerTypes.CHILD)) {
				totalNoOfChildren = totalNoOfChildren + 1;
				// Infant
			} else if (passengerType.equals(PassengerTypes.INFANT)) {
				totalNoOfInfants = totalNoOfInfants + 1;
			}
		}

		return new int[] { totalNoOfAdults, totalNoOfChildren, totalNoOfInfants };
	}

	/**
	 * Locate the passenger type
	 * 
	 * @param xaPnlPaxEntry
	 * @return
	 * @throws ModuleException
	 */
	private static String getPassengerType(XAPnlPaxEntry xaPnlPaxEntry) throws ModuleException {
		if (ReservationInternalConstants.PassengerType.ADULT.equals(xaPnlPaxEntry.getPaxType())) {
			String infantFName = BeanUtils.nullHandler(xaPnlPaxEntry.getInfantFirstName());
			String infantLName = BeanUtils.nullHandler(xaPnlPaxEntry.getInfantLastName());
			String infantTitle = BeanUtils.nullHandler(xaPnlPaxEntry.getInfantTitle());

			if (infantFName.equals("") && infantLName.equals("") && infantTitle.equals("")) {
				return PassengerTypes.ADULT;
			} else {
				return PassengerTypes.PARENT;
			}
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(xaPnlPaxEntry.getPaxType())) {
			return PassengerTypes.CHILD;
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(xaPnlPaxEntry.getPaxType())) {
			return PassengerTypes.INFANT;
		} else {
			throw new ModuleException("airreservations.xaPnl.invalidPassengerType");
		}
	}

	/** Donotes error descriptions */
	private static interface ErrorDescription {
		public static final String XA_PNL_APP_ERROR = new ModuleException("airreservations.arg.appErrorWhileReconcile")
				.getMessageString();
	}

	/** Holds the external air line specific passenger types */
	private static interface PassengerTypes {
		/** Holds the adult type */
		public static final String ADULT = "A";
		/** Holds the parent type */
		public static final String PARENT = "P";
		/** Holds the child type */
		public static final String CHILD = "C";
		/** Holds the infant type */
		public static final String INFANT = "I";
	}

	/**
	 * Update reservation status
	 * 
	 * @param strPNR
	 * @param agentCode
	 * @throws ModuleException
	 */
	private void updateReservationStatus(String strPNR, String agentCode) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(strPNR);
		pnrModesDTO.setLoadFares(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		ReservationAdminInfo adminInfo = reservation.getAdminInfo();

		adminInfo.setOriginChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
		adminInfo.setOwnerChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
		adminInfo.setOriginUserId(null);
		adminInfo.setOriginAgentCode(agentCode);
		adminInfo.setOwnerAgentCode(agentCode);

		// Save the reservation
		ReservationProxy.saveReservation(reservation);
	}

	/**
	 * Will validate records and derive flight segment information
	 * 
	 * @param colValues
	 * @throws ModuleException
	 */
	private void validateRecordsAndDeriveFlightSegmentInfo(Collection<XAPnlPaxEntry> colValues) throws ModuleException {
		if (colValues == null || colValues.size() == 0) {
			throw new ModuleException("airreservations.arg.emptyXAPNLEntries");
		}

		Collection<String> colFlightNumbers = new HashSet<String>();
		Collection<Date> colFlightDates = new HashSet<Date>();
		Collection<String> colDepatureStations = new HashSet<String>();
		Collection<String> colArrivalStations = new HashSet<String>();
		Collection<String> colCabinClasses = new HashSet<String>();

		Iterator<XAPnlPaxEntry> itColValues = colValues.iterator();
		XAPnlPaxEntry xaPnlPaxEntry;

		while (itColValues.hasNext()) {
			xaPnlPaxEntry = (XAPnlPaxEntry) itColValues.next();

			colFlightNumbers.add(xaPnlPaxEntry.getFlightNumber());
			colFlightDates.add(xaPnlPaxEntry.getFlightDate());
			colDepatureStations.add(xaPnlPaxEntry.getDepartureAirport());
			colArrivalStations.add(xaPnlPaxEntry.getArrivalAirport());
			colCabinClasses.add(xaPnlPaxEntry.getCabinClassCode());
		}

		if (colFlightNumbers.size() > 1 || colFlightDates.size() > 1 || colDepatureStations.size() > 1
				|| colArrivalStations.size() > 1 || colCabinClasses.size() > 1) {
			throw new ModuleException("airreservations.xaPnl.multipleLocationsFoundInXAPNL");
		}

		flightNumber = (String) BeanUtils.getFirstElement(colFlightNumbers);
		departureDate = (Date) BeanUtils.getFirstElement(colFlightDates);
		fromAirport = (String) BeanUtils.getFirstElement(colDepatureStations);
		toAirport = (String) BeanUtils.getFirstElement(colArrivalStations);
		cabinClassCode = (String) BeanUtils.getFirstElement(colCabinClasses);

		flightSegmentId = reservationSegmentDAO.getFlightSegmentId(flightNumber, departureDate, fromAirport, toAirport);
	}

	/**
	 * Validate Parameters
	 * 
	 * @param xaPnlId
	 * @throws ModuleException
	 */
	private void validateParams(Integer xaPnlId) throws ModuleException {
		log.debug("Inside validateParams");

		if (xaPnlId == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
