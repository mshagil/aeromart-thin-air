package com.isa.thinair.airreservation.core.bl.tax;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;

public class TaxInvoiceGeneratorFactory {

	private List<TaxInvoiceFactory> factories = null;

	public TaxInvoiceGeneratorFactory() throws ModuleException {
		this.factories = new ArrayList<>();
		this.factories.add(new GSTTaxInvoiceFactory());
	}

	public List<TaxInvoiceFactory> getGenerators(TransactionSequence lastPaidTnxSequence) {
		List<TaxInvoiceFactory> applicableFactories = new ArrayList<>();
		for (TaxInvoiceFactory taxGenFac : factories) {
			if (taxGenFac.isApplicable(lastPaidTnxSequence.getChargeTnx())) {
				applicableFactories.add(taxGenFac);
			}
		}
		return applicableFactories;
	}

}
