/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 

 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.CloneBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.persistence.dao.BaggageDAO;
import com.isa.thinair.airreservation.core.persistence.dao.MealDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAirportTransferDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.common.Coupon;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * Command will exchange existing valid confirm segments. Note: Segments will be exchanged as it is along with the
 * ancillaries too will be re-protected.
 * <p>
 * 1.Current confirm segments will be exchanged, except flown segments. <br>
 * 2.Reservation status should be CNF. <br>
 * 3.It should be a GDS booking. <br>
 * </p>
 * 
 * 
 * 
 * @author M.Rikaz
 * @since
 * @isa.module.command name="exchangeSegment"
 */
public class ExchangeSegment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ExchangeSegment.class);

	/** Holds whether Credentials information */
	private CredentialsDTO credentialsDTO;

	/** ExchangeSegment Constructor */
	public ExchangeSegment() {
	}

	/**
	 * Execute method of the ExchangeSegment command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.info("Inside exchangeSegment execute");
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		String pnr = this.getParameter(CommandParamNames.PNR, String.class);
		this.credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions = this
				.getParameter(CommandParamNames.EXCHANGE_TIKCET_TRANSITION_MAP, Map.class);

		Collection<Integer> exchangeFltSegIds = this.getParameter(CommandParamNames.EXCHANGE_FLT_SEG_ID_LIST, Collection.class);
		Map<Integer, Collection<ReservationPaxFare>> exchangePaxFaresByPaxId = this
				.getParameter(CommandParamNames.EXCHANGE_PAX_FARE_MAP, Map.class);

		Map<Integer, ReservationSegment> exchangePnrSegmentByPnrSegId = this
				.getParameter(CommandParamNames.EXCHANGE_PNR_SEGMENT_MAP, Map.class);
		Map<Integer, Integer> exchangePnrSegIdByFltSegId = this.getParameter(CommandParamNames.EXCHANGE_PNR_SEG_ID_MAP,
				Map.class);

		this.validateParams(pnr, exchangeFltSegIds, exchangePaxFaresByPaxId, exchangePnrSegmentByPnrSegId,
				exchangePnrSegIdByFltSegId, transitions);

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);

		// Get the reservation
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		ChargeAssitUnit chargeAU = new ChargeAssitUnit(reservation, new ArrayList<Integer>(), exchangeFltSegIds, true, true,
				false, false, reservation.getVersion(), false, null);

		// Create the cancel segment business object
		CancelSegmentBO cancelSegmentBO = new CancelSegmentBO(false, new CustomChargesTO(), false, true, false, false,
				credentialsDTO, exchangeFltSegIds, false, null, reservation.isInfantPaymentRecordedWithInfant());
		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);

		List<ReservationPaxFare> reservationPaxFareExchanged = null;
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			// For Parent or Adult
			if (!ReservationApiUtils.isInfantType(reservationPax)) {

				reservationPaxFareExchanged = chargeAU.getExchangedPaxFare(reservationPax.getPnrPaxFares());

				addPassengerRevenues(reservationPax, cancelSegmentBO, reservationPaxFareExchanged, false, false,
						chargeAU.isSameFlightModification(), chargeAU.isVoidReservation(), null, true, false, false, false,
						chgTnxGen);

			}
		}

		ReservationCoreUtils.setPnrAndPaxTotalAmounts(chargeAU.getReservation());
		this.cancelPnrAndSegments(chargeAU, credentialsDTO);
		ReservationProxy.saveReservation(reservation);

		Map<Integer, RevenueDTO> passengerRevenueMap = cancelSegmentBO.getMapPerPaxWiseRevenue();

		// Add Exchanged New Segments
		if (exchangePnrSegmentByPnrSegId != null && !exchangePnrSegmentByPnrSegId.isEmpty()) {
			Collection<ReservationSegment> exchangeSegments = exchangePnrSegmentByPnrSegId.values();
			if (exchangeSegments != null && !exchangeSegments.isEmpty()) {
				for (ReservationSegment pnrSegment : exchangeSegments) {
					pnrSegment.setReservation(reservation);
					reservation.addSegment(pnrSegment);
				}

			}
		}

		// Exchanging Segment charges to New Segment
		BigDecimal totalChargeAmount;
		BigDecimal[] totalReservationAmounts = ReservationApiUtils.getChargeTypeAmounts();
		Collection<ReservationPaxFare> colReservationPaxFare = null;
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			colReservationPaxFare = new ArrayList<ReservationPaxFare>();
			Collection<ReservationPaxFare> colReservationPaxFareTemp = exchangePaxFaresByPaxId.get(reservationPax.getPnrPaxId());

			if (colReservationPaxFareTemp != null && !colReservationPaxFareTemp.isEmpty()) {
				for (ReservationPaxFare oldPaxFare : colReservationPaxFareTemp) {

					ReservationPaxFare paxFareNew = (ReservationPaxFare) oldPaxFare.clone();
					for (ReservationPaxOndCharge paxOndChargeNew : oldPaxFare.getCharges()) {
						paxFareNew.addCharge(paxOndChargeNew.cloneForNew(credentialsDTO, false));
					}
					reservationPax.addPnrPaxFare(paxFareNew);

					for (ReservationPaxFareSegment oldPaxFareSegment : oldPaxFare.getPaxFareSegments()) {
						ReservationPaxFareSegment paxFareSegment = (ReservationPaxFareSegment) oldPaxFareSegment.clone();
						if (paxFareSegment.getVersion() == -1) {
							Set<ReservationPaxSegmentSSR> reservationPaxSegmentSSRs = oldPaxFareSegment
									.getReservationPaxSegmentSSRs();
							if (oldPaxFareSegment.getReservationPaxSegmentSSRs() != null
									&& !oldPaxFareSegment.getReservationPaxSegmentSSRs().isEmpty()) {
								for (ReservationPaxSegmentSSR paxSegmentSSR : reservationPaxSegmentSSRs) {
									ReservationPaxSegmentSSR paxSegmentSSRCopy = (ReservationPaxSegmentSSR) paxSegmentSSR.clone();
									paxSegmentSSRCopy.setSSRId(paxSegmentSSR.getSSRId());
									paxSegmentSSRCopy.setReservationPaxFareSegment(paxFareSegment);

									paxFareSegment.addPaxSegmentSSR(paxSegmentSSRCopy);

								}

							}

							Integer key = oldPaxFareSegment.getSegment().getPnrSegId();
							ReservationSegment reservationSegmentCloned = exchangePnrSegmentByPnrSegId.get(key);
							if (reservationSegmentCloned != null) {
								paxFareSegment.setSegment(reservationSegmentCloned);
								paxFareNew.addPaxFareSegment(paxFareSegment);
							}

						}

					}

					colReservationPaxFare.add(paxFareNew);
				}
			}

			totalChargeAmount = CloneBO.linkFaresToThePassenger(reservationPax, colReservationPaxFare);

			if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
				Collection<ReservationPaxFare> colInfantReservationPaxFare = exchangePaxFaresByPaxId
						.get(reservationPax.getAccompaniedPaxId());

				BigDecimal totalInfantAmount = CloneBO.getTotalCharges(colInfantReservationPaxFare);
				totalChargeAmount = AccelAeroCalculator.add(totalChargeAmount, totalInfantAmount);
				colReservationPaxFare.addAll(colInfantReservationPaxFare);
			}

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				this.captureRevenueModification(reservationPax.getPnrPaxId(), passengerRevenueMap, totalChargeAmount,
						colReservationPaxFare);
			}
			// Set the total charge
			// this.setTotalCharge(reservationPax.getPnrPaxId(), newCharge, pnrPaxIdAndPayments);

			// Updating the totals
			ReservationCoreUtils.updateTotals(totalReservationAmounts, reservationPax.getTotalChargeAmounts(), true);
		}
		reservation.setTotalChargeAmounts(totalReservationAmounts);

		ReservationProxy.saveReservation(reservation);
		Collection<Integer> newSegmentSeqNos = new ArrayList<Integer>();
		// old pnrSegId and exchange new pnrSegId
		Map<Integer, Integer> exchangePnrSegIdMap = this.getNewPnrSegIdByExchangePnrSegIdMap(reservation,
				exchangePnrSegIdByFltSegId, newSegmentSeqNos);

		this.reProtectExchangedSegmentAncillary(reservation, exchangePnrSegIdMap);

		ETicketBO.exchangeETickets(reservation.getPassengers(), transitions, exchangePnrSegIdMap.keySet());

		// Composing the reservation audit object
		Collection<ReservationAudit> colReservationAudit = this.composeAudit(pnr, exchangePnrSegmentByPnrSegId.values(),
				reservation, null);

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.PASSENGER_REVENUE_MAP, passengerRevenueMap);
		response.addResponceParam(CommandParamNames.RESERVATION_PAYMENT_META_TO,
				TnxGranularityReservationUtils.getReservationPaymentMetaTO(reservation));
		response.addResponceParam(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, reservation.isEnableTransactionGranularity());
		response.addResponceParam(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, getPassengerPaymentsMap(reservation));

		log.info("Exit exchangeSegment execute");
		return response;
	}

	private void cancelPnrAndSegments(ChargeAssitUnit chargeAU, CredentialsDTO credentialsDTO) throws ModuleException {
		Iterator<ReservationSegment> itReservationSegment = chargeAU.getReservation().getSegments().iterator();
		ReservationSegment reservationSegment;

		Collection<Integer> allPnrSegsToBeCnx = chargeAU.getAllPnrSegmentsToBeMarkedCancelled();
		Collection<Integer> exchangedPnrSegIds = chargeAU.getUserExchangedPnrSegIds();

		while (itReservationSegment.hasNext()) {
			reservationSegment = itReservationSegment.next();

			if (allPnrSegsToBeCnx.contains(reservationSegment.getPnrSegId())) {
				reservationSegment.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
				reservationSegment.setBundledFarePeriodId(null);
				if (exchangedPnrSegIds.contains(reservationSegment.getPnrSegId())) {
					reservationSegment.setSubStatus(ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED);
				}
				if (AppSysParamsUtil.isAutoCancellationEnabled() && reservationSegment.getAutoCancellationId() != null) {
					reservationSegment.setAutoCancellationId(null);
				}
				ReservationApiUtils.captureSegmentStatusChgInfo(reservationSegment, credentialsDTO);
			}

		}

		for (ReservationPax reservationPax : chargeAU.getReservation().getPassengers()) {
			ReservationSSRUtil.cancelPaxSSRs(reservationPax, exchangedPnrSegIds);
		}

		if (chargeAU.isReservationCancelled()) {
			if (chargeAU.isVoidReservation()) {
				// only reservations are to be marked as void.
				chargeAU.getReservation().setIsVoidReservation(ReservationInternalConstants.VoidReservation.YES);
			} else {
				chargeAU.getReservation().setIsVoidReservation(ReservationInternalConstants.VoidReservation.NO);
			}

			// Cancells the reservation passengers
			Iterator<ReservationPax> itReservationPax = chargeAU.getReservation().getPassengers().iterator();
			ReservationPax reservationPax;
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CANCEL);
			}

			// Cancells the reservation
			chargeAU.getReservation().setStatus(ReservationInternalConstants.ReservationStatus.CANCEL);
		}
	}

	private void addPassengerRevenues(ReservationPax reservationPax, CancelSegmentBO cancelSegmentBO,
			List<ReservationPaxFare> reservationPaxFares, boolean applyCnxCharge, boolean applyModCharge,
			boolean isSameFlightsModification, boolean isVoidReservation, FlownAssitUnit flownAsstUnit, boolean isExchanged,
			boolean isFareAdjustmentValid, boolean isTargetFare, boolean isApplyCustomCharge, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {

		if (reservationPaxFares != null) {
			int count = 0;
			for (ReservationPaxFare reservationPaxFare : reservationPaxFares) {
				cancelSegmentBO.processPaxCreditForOnd(reservationPax, reservationPaxFare, applyCnxCharge, applyModCharge,
						isSameFlightsModification, isVoidReservation, flownAsstUnit, isExchanged, isFareAdjustmentValid,
						isTargetFare, count, isApplyCustomCharge, chgTnxGen);
				count++;
			}
		}
	}

	private void validateParams(String pnr, Collection<Integer> exchangeFltSegIds,
			Map<Integer, Collection<ReservationPaxFare>> exchangePaxFaresByPaxId,
			Map<Integer, ReservationSegment> exchangePnrSegmentByPnrSegId, Map<Integer, Integer> exchangePnrSegIdByFltSegId,
			Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || credentialsDTO == null || exchangeFltSegIds == null || exchangeFltSegIds.isEmpty()
				|| exchangePaxFaresByPaxId == null || exchangePaxFaresByPaxId.isEmpty() || exchangePnrSegmentByPnrSegId == null
				|| exchangePnrSegmentByPnrSegId.isEmpty() || exchangePnrSegIdByFltSegId == null
				|| exchangePnrSegIdByFltSegId.isEmpty() || exchangePnrSegIdByFltSegId.values() == null
				|| exchangePnrSegIdByFltSegId.values().isEmpty() || transitions == null || transitions.isEmpty()) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	public Map<Integer, IPayment> getPassengerPaymentsMap(Reservation reservation) throws ModuleException {

		Map<Integer, IPayment> pnrPaxIdPaymentMap = new HashMap<Integer, IPayment>();

		for (ReservationPax pax : reservation.getPassengers()) {
			if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				PaymentAssembler paymentAssm = new PaymentAssembler();
				paymentAssm.addCashPayment(BigDecimal.ZERO, null, null, null, null);
				pnrPaxIdPaymentMap.put(pax.getPnrPaxId(), paymentAssm);
			}
		}

		return pnrPaxIdPaymentMap;
	}

	private BigDecimal captureRevenueModification(Integer pnrPaxId, Map<Integer, RevenueDTO> passengerRevenueMap,
			BigDecimal totalChargeAmount, Collection<ReservationPaxFare> colReservationPaxFares) throws ModuleException {
		RevenueDTO revenueDTO = passengerRevenueMap.get(pnrPaxId);
		if (revenueDTO == null) {
			revenueDTO = new RevenueDTO();
			revenueDTO.setPnrPaxId(pnrPaxId);
			passengerRevenueMap.put(pnrPaxId, revenueDTO);
		}

		BigDecimal modificationPenalty = ReservationCoreUtils.getTolalPenaltyCharges(colReservationPaxFares);

		if (modificationPenalty.compareTo(BigDecimal.ZERO) > 0) {
			totalChargeAmount = AccelAeroCalculator.subtract(totalChargeAmount, modificationPenalty);
			revenueDTO.setModificationPenalty(modificationPenalty);
		}

		revenueDTO.setAddedTotal(totalChargeAmount);
		revenueDTO.setActionNC(ReservationTnxNominalCode.PAX_CANCEL.getCode());
		revenueDTO.setChargeNC(ReservationTnxNominalCode.ALTER_CHARGE.getCode());
		revenueDTO.getAddedCharges().addAll(getChargesMetaInfo(colReservationPaxFares));

		return AccelAeroCalculator.subtract((AccelAeroCalculator.add(revenueDTO.getAddedTotal(),
				revenueDTO.getCancelOrModifyTotal(), revenueDTO.getModificationPenalty())), revenueDTO.getCreditTotal());
	}

	private Collection<ReservationPaxOndCharge> getChargesMetaInfo(Collection<ReservationPaxFare> colReservationPaxFare) {
		Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();

		for (ReservationPaxFare reservationPaxFare : colReservationPaxFare) {
			colReservationPaxOndCharge.addAll(reservationPaxFare.getCharges());
		}

		return colReservationPaxOndCharge;
	}

	private void reProtectExchangedSegmentAncillary(Reservation reservation, Map<Integer, Integer> exchangePnrSegIdMap)
			throws ModuleException {

		// exchange booked meals
		Collection<Integer> colExchangablePnrSegIds = exchangePnrSegIdMap.keySet();
		if (reservation.getMeals() != null && !reservation.getMeals().isEmpty()) {
			Collection<Integer> pnrSegMealId = new ArrayList<Integer>();
			for (PaxMealTO paxMealTo : reservation.getMeals()) {
				if (colExchangablePnrSegIds.contains(paxMealTo.getPnrSegId())) {
					pnrSegMealId.add(paxMealTo.getPkey());
				}

			}

			MealDAO mealDAO = ReservationDAOUtils.DAOInstance.MEAL_DAO;
			Collection<PassengerMeal> passengerMeals = mealDAO.getPassengerMeals(pnrSegMealId);

			if (passengerMeals != null && passengerMeals.size() > 0) {
				for (PassengerMeal ps : passengerMeals) {
					Integer newPnrSegId = exchangePnrSegIdMap.get(ps.getPnrSegId());
					if (newPnrSegId != null) {
						ps.setPnrSegId(newPnrSegId);
						ps.setTimestamp(new Date());
					}
				}
				mealDAO.saveOrUpdate(passengerMeals);
			}
		}

		// exchange booked baggages
		if (reservation.getBaggages() != null && !reservation.getBaggages().isEmpty()) {
			Collection<Integer> pnrSegBaggageId = new ArrayList<Integer>();
			for (PaxBaggageTO baggages : reservation.getBaggages()) {
				if (colExchangablePnrSegIds.contains(baggages.getPnrSegId())) {
					pnrSegBaggageId.add(baggages.getPkey());
				}
			}
			BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
			Collection<PassengerBaggage> passengerBaggages = baggageDAO.getPassengerBaggages(pnrSegBaggageId);

			if (passengerBaggages != null && passengerBaggages.size() > 0) {
				for (PassengerBaggage ps : passengerBaggages) {
					Integer newPnrSegId = exchangePnrSegIdMap.get(ps.getPnrSegmentId());
					ps.setPnrSegmentId(newPnrSegId);
					ps.setTimestamp(new Date());
				}
				baggageDAO.saveOrUpdate(passengerBaggages);
			}
		}

		// exchange booked AirportTransfers
		if (reservation.getAirportTransfers() != null && !reservation.getAirportTransfers().isEmpty()) {
			Collection<Integer> pnrSegAptId = new ArrayList<Integer>();
			for (PaxAirportTransferTO paxAirportTransferTO : reservation.getAirportTransfers()) {
				if (colExchangablePnrSegIds.contains(paxAirportTransferTO.getPnrSegId())) {
					pnrSegAptId.add(paxAirportTransferTO.getPnrPaxSegAptId());
				}
			}
			ReservationAirportTransferDAO airportTransferDAO = ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO;

			Collection<ReservationPaxSegAirportTransfer> paxSegAirportTransferColl = airportTransferDAO
					.getReservationAirportTransfers(pnrSegAptId);

			if (paxSegAirportTransferColl != null && paxSegAirportTransferColl.size() > 0) {
				for (ReservationPaxSegAirportTransfer ps : paxSegAirportTransferColl) {
					Integer newPnrSegId = exchangePnrSegIdMap.get(ps.getPnrSegId());
					ps.setPnrSegId(newPnrSegId);
				}
				airportTransferDAO.saveOrUpdate(paxSegAirportTransferColl);
			}
		}

		// exchange booked seat
		if (reservation.getSeats() != null && !reservation.getSeats().isEmpty()) {
			Collection<Integer> flightAMSeatIds = new ArrayList<Integer>();
			for (PaxSeatTO baggages : reservation.getSeats()) {
				if (colExchangablePnrSegIds.contains(baggages.getPnrSegId())) {
					flightAMSeatIds.add(baggages.getPkey());
				}
			}

			SeatMapDAO seatMapDAO = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO;
			Collection<PassengerSeating> passengerSeatings = seatMapDAO.getReservedFlightSeats(flightAMSeatIds);

			if (passengerSeatings != null && passengerSeatings.size() > 0) {
				for (PassengerSeating passengerSeating : passengerSeatings) {
					Integer newPnrSegId = exchangePnrSegIdMap.get(passengerSeating.getPnrSegId());
					passengerSeating.setPnrSegId(newPnrSegId);
					passengerSeating.setTimestamp(new Date());
				}
				seatMapDAO.saveOrUpdate(passengerSeatings);
			}
		}
	}

	/**
	 * populate oldPnrSegId vs new exchanged pnrSegId
	 * 
	 * @param newSegmentSeqNos
	 */
	private Map<Integer, Integer> getNewPnrSegIdByExchangePnrSegIdMap(Reservation reservation,
			Map<Integer, Integer> exchangePnrSegIdByFltSegId, Collection<Integer> newSegmentSeqNos) {

		Map<Integer, Integer> exchangePnrSegIdMap = new HashMap<Integer, Integer>();
		if (reservation.getSegments() != null && !reservation.getSegments().isEmpty()) {
			for (ReservationSegment pnrSegment : reservation.getSegments()) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(pnrSegment.getStatus())) {
					Integer flightSegId = pnrSegment.getFlightSegId();
					if (exchangePnrSegIdByFltSegId.get(flightSegId) != null) {
						exchangePnrSegIdMap.put(exchangePnrSegIdByFltSegId.get(flightSegId), pnrSegment.getPnrSegId());
						newSegmentSeqNos.add(pnrSegment.getSegmentSeq());
					}
				}

			}
		}
		return exchangePnrSegIdMap;
	}

	private Collection<ReservationAudit> composeAudit(String pnr, Collection<ReservationSegment> pnrSegments,
			Reservation reservation, String userNotes) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.EXTERNAL_TICKET_EXCHANGE.getCode());
		reservationAudit.setUserNote(userNotes);

		Iterator<ReservationSegment> itPnrSegments = pnrSegments.iterator();
		ReservationSegment reservationSegment;
		Collection<Integer> colPnrSegmentIds = new ArrayList<Integer>();
		Map<Integer, String> mapPnrSegIdAndCabinClass = new HashMap<Integer, String>();
		Map<Integer, String> mapPnrSegIdAndLogicalCabinClass = new HashMap<Integer, String>();

		while (itPnrSegments.hasNext()) {
			reservationSegment = itPnrSegments.next();
			colPnrSegmentIds.add(reservationSegment.getPnrSegId());
			mapPnrSegIdAndCabinClass.put(reservationSegment.getPnrSegId(), reservationSegment.getCabinClassCode());
			if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
				mapPnrSegIdAndLogicalCabinClass.put(reservationSegment.getPnrSegId(), reservationSegment.getLogicalCCCode());
			}
		}

		// Get main flight information
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Collect the reservation segment information
		Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationSegmentDAO
				.getSegmentInformation(colPnrSegmentIds);
		Iterator<ReservationSegmentDTO> itColReservationSegmentDTO = colReservationSegmentDTO.iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		StringBuffer changes = new StringBuffer();
		while (itColReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itColReservationSegmentDTO.next();

			changes.append(" Flight No : " + reservationSegmentDTO.getFlightNo());
			changes.append(" Segment Code : " + reservationSegmentDTO.getSegmentCode());
			changes.append(" Departure Date : " + reservationSegmentDTO.getDepartureStringDate("E, dd MMM yyyy HH:mm:ss"));
		}

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExchangeSegment.SEGMENT_INFORMATION, changes.toString());

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExchangeSegment.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExchangeSegment.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);

		return colReservationAudit;
	}
}