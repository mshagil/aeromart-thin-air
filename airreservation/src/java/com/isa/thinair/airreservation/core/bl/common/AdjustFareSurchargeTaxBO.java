package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author rumesh
 * 
 */

public class AdjustFareSurchargeTaxBO {

	private static Log log = LogFactory.getLog(AdjustFareSurchargeTaxBO.class);

	private Collection<OndFareDTO> ondFareDTOs;
	private String pnr;
	private Reservation reservation;
	private boolean isUserModification;
	private Collection<Integer> cnxPnrSegmentIds;

	public AdjustFareSurchargeTaxBO(Collection<OndFareDTO> ondFareDTOs, String pnr, Reservation reservation,
			boolean isUserModification, Collection<Integer> cnxPnrSegmentIds) {
		this.ondFareDTOs = ondFareDTOs;
		this.pnr = pnr;
		this.reservation = reservation;
		this.isUserModification = isUserModification;
		this.cnxPnrSegmentIds = cnxPnrSegmentIds;
	}

	public void adjustFareSurchargeTax(String taxChargeCode) {
		QuotedChargeDTO quotedChargeDTO = getQuotedChargeInNewOndFares(taxChargeCode);
		if (quotedChargeDTO != null && !quotedChargeDTO.isRefundable()) {
			Map<String, Double> paxTypeTaxedAmounts = ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO
					.getCurrentTaxCharged(taxChargeCode, pnr);
			Map<String, Double> effectiveTaxAmounts = quotedChargeDTO.getEffectiveChargeAmountMap();
			Map<String, Double> serviceTaxForNonRefundables = calculateServiceTaxForNonRefundables();

			for (String paxType : effectiveTaxAmounts.keySet()) {
				if (paxTypeTaxedAmounts.containsKey(paxType)) {
					Double taxedAmount = paxTypeTaxedAmounts.get(paxType);
					if (serviceTaxForNonRefundables.get(paxType) != null) {
						taxedAmount = taxedAmount - serviceTaxForNonRefundables.get(paxType);
					}
					if (taxedAmount != null && taxedAmount > 0) {
						Double effectiveTaxAmount = effectiveTaxAmounts.get(paxType);
						Double newTaxAmount = 0.0;
						if (taxedAmount > effectiveTaxAmount) {
							newTaxAmount = 0.0;
						} else {
							newTaxAmount = effectiveTaxAmount - taxedAmount;
						}

						effectiveTaxAmounts.put(paxType, newTaxAmount);
					}
				}
			}

		}
	}

	private QuotedChargeDTO getQuotedChargeInNewOndFares(String taxChargeCode) {
		if (ondFareDTOs != null && ondFareDTOs.size() > 0) {
			OndFareDTO ondFareDTO = ((List<OndFareDTO>) ondFareDTOs).get(OndSequence.OUT_BOUND);
			if (ondFareDTO != null && ondFareDTO.getAllCharges() != null) {
				for (QuotedChargeDTO quotedChargeDTO : ondFareDTO.getAllCharges()) {
					if (taxChargeCode.equals(quotedChargeDTO.getChargeCode())) {
						return quotedChargeDTO;
					}
				}
			}
		}
		return null;
	}

	private Map<String, BigDecimal> getPaxWiseNonRefundables() throws ModuleException {

		boolean isOnHoldReservation = ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())
				? true
				: false;
		Map<String, BigDecimal> paxWiseNonRefundables = new HashMap<String, BigDecimal>();

		Set<Integer> extChargeRateIds = getExternalChargeRateIDs();

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				Collection<Integer> fareIds = new ArrayList<Integer>();
				Collection<Integer> chgRateIds = new ArrayList<Integer>();
				boolean isModifyingSegment = false;
				String paxType = reservationPaxFare.getReservationPax().getPaxType();
				if (!paxWiseNonRefundables.containsKey(paxType)) {
					for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
						if (cnxPnrSegmentIds.contains(reservationPaxFareSegment.getPnrSegId())) {
							isModifyingSegment = true;
							break;
						}
					}

					if (isModifyingSegment) {
						fareIds.add(reservationPaxFare.getFareId());
						for (ReservationPaxOndCharge reservationPaxOndCharge : reservationPaxFare.getCharges()) {
							if (reservationPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.SUR)
									&& !extChargeRateIds.contains(reservationPaxOndCharge.getChargeRateId())) {
								chgRateIds.add(reservationPaxOndCharge.getChargeRateId());
							}
						}
					}

					if ((fareIds.size() > 0 || chgRateIds.size() > 0) && isUserModification && !isOnHoldReservation) {
						FaresAndChargesTO faresAndChargesTO = ReservationModuleUtils.getFareBD().getRefundableStatuses(fareIds,
								chgRateIds, null);
						Map<Integer, FareTO> mapFareTO = faresAndChargesTO.getFareTOs();
						Map<Integer, ChargeTO> mapChgTO = faresAndChargesTO.getChargeTOs();
						BigDecimal totalNonRefundables = AccelAeroCalculator.getDefaultBigDecimalZero();

						Iterator<ReservationPaxOndCharge> itCharges = reservationPaxFare.getCharges().iterator();
						ReservationPaxOndCharge reservationPaxOndCharge;
						FareTO fareTO;
						ChargeTO chargeTO;
						while (itCharges.hasNext()) {
							reservationPaxOndCharge = itCharges.next();
							BigDecimal actualOndChargeAmount = reservationPaxOndCharge.getEffectiveAmount();
							if (reservationPaxOndCharge.getChargeRateId() != null
									&& reservationPaxOndCharge.getChargeGroupCode().equals(
											ReservationInternalConstants.ChargeGroup.SUR)) {
								chargeTO = mapChgTO.get(reservationPaxOndCharge.getChargeRateId());

								if (chargeTO != null && !chargeTO.isRefundable() && !chargeTO.isRefundableOnlyForMOD()) {
									totalNonRefundables = AccelAeroCalculator.add(totalNonRefundables, actualOndChargeAmount);
								}
							} else if (reservationPaxOndCharge.getFareId() != null) {
								fareTO = mapFareTO.get(reservationPaxOndCharge.getFareId());
								if (!ReservationApiUtils.isRefundable(fareTO, paxType)) {
									totalNonRefundables = AccelAeroCalculator.add(totalNonRefundables, actualOndChargeAmount);
								}
							}

						}

						paxWiseNonRefundables.put(paxType, totalNonRefundables);

					}
				}
			}
		}

		return paxWiseNonRefundables;
	}

	private Map<String, Double> calculateServiceTaxForNonRefundables() {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_TAX);
		Map<String, Double> paxWiseCalculatedTax = new HashMap<String, Double>();

		try {
		ExternalChgDTO externalChgDTO = BeanUtils.getFirstElement(ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, null).values());
		
		Map<String, BigDecimal> paxWiseNonRefundables = getPaxWiseNonRefundables();

		for (String paxType : paxWiseNonRefundables.keySet()) {
			BigDecimal taxAmount = AccelAeroCalculator.multiplyDefaultScale(paxWiseNonRefundables.get(paxType),
					externalChgDTO.getRatioValue());
				paxWiseCalculatedTax.put(paxType, taxAmount.doubleValue());
			}
		} catch (ModuleException exp) {

		}


		return paxWiseCalculatedTax;
	}

	private Set<Integer> getExternalChargeRateIDs() {
		Set<Integer> extChargeRateIds = new HashSet<Integer>();
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.MEAL);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SEAT_MAP);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AIRPORT_SERVICE);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.BAGGAGE);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AIRPORT_TRANSFER);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);

		try {
			Collection<ExternalChgDTO> externalChgDTOs = ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null)
					.values();
			for (ExternalChgDTO externalChgDTO : externalChgDTOs) {
				extChargeRateIds.add(externalChgDTO.getChgRateId());
			}
		} catch (ModuleException e) {
			log.error("Error Getting charge rate IDs for ext charges", e);
		}

		return extChargeRateIds;
	}
}
