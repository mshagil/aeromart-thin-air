package com.isa.thinair.airreservation.core.util;

import com.isa.thinair.airreservation.api.dto.pfs.PfsEntryDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PfsXmlDTO;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;


/**
 * @author eric
 * 
 */
public class PFSUtil {

	//private static Log log = LogFactory.getLog(PFSUtil.class);

	public static boolean isPaxFound(ReservationPax reservationPax, PfsPaxEntry pfsParsedEntry) {

		String strDbTitle = BeanUtils.nullHandler(reservationPax.getTitle()).toUpperCase();
		String strDbFirstName = BeanUtils.nullHandler(reservationPax.getFirstName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");
		String strDbLastName = BeanUtils.nullHandler(reservationPax.getLastName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");

		String strDbPaxType = BeanUtils.nullHandler(reservationPax.getPaxType());

		String strPfsTitle = BeanUtils.nullHandler(pfsParsedEntry.getTitle()).toUpperCase();
		String strPfsFirstName = BeanUtils.nullHandler(pfsParsedEntry.getFirstName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");
		String strPfsLastName = BeanUtils.nullHandler(pfsParsedEntry.getLastName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");

		String strPfsPaxType = BeanUtils.nullHandler(pfsParsedEntry.getPaxType());

		String strDbFullName = strDbFirstName + strDbLastName;
		String strPfsFullName = strPfsFirstName + strPfsLastName;

		if (strDbPaxType.equals(strPfsPaxType) && strDbFullName.equals(strPfsFullName)) {
			// If it's an adult title should be same
			if (ReservationInternalConstants.PassengerType.ADULT.equals(strDbPaxType)) {
				//Removing title check as requested - 31MAY2012
				//if (strDbTitle.equals(strPfsTitle)) {
				return true;
				//}
				// If it's a child we are not comparing the titles. Cos for PNL/ADL we send the child as CHD. Titles
				// will be ignored.
				// Please improve this with a later implementation
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(strDbPaxType)) {
				return true;
				// If it's an infant
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(strDbPaxType)) {
				return true;
			}
			return false;
		} else {
			return false;
		}

	}
	
	public static PfsPaxEntry getPFSEntry(PfsXmlDTO pfsXmlDTO, Pfs pfs, PfsEntryDTO entryDTO) {
		PfsPaxEntry entry = new PfsPaxEntry();
		entry.setArrivalAirport(entryDTO.getArrivalAirport());
		entry.setCabinClassCode(entryDTO.getCabinClassCode());
		entry.setDepartureAirport(pfsXmlDTO.getFromAirport());
		
		entry.setEntryStatus(entryDTO.getEntryStatus());
		
		entry.seteTicketNo(entryDTO.geteTicketNo());
		entry.setFirstName(entryDTO.getFirstName());
		entry.setFlightDate(pfsXmlDTO.getDepartureDate());
		entry.setFlightNumber(pfsXmlDTO.getFlightNumber());
		
		entry.setLastName(entryDTO.getLastName());
		entry.setPaxType(entryDTO.getPaxType());
		
		entry.setPfsId(pfs.getPpId());
		
		entry.setPnr(entryDTO.getPnr());
		entry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED);
		entry.setReceivedDate(entryDTO.getReceivedDate());
		
		//set titles
		if ( "MR".equals(entryDTO.getTitle()) ) {
			entry.setTitle(ReservationInternalConstants.PassengerTitle.MR);
		} else if ( "MS".equals(entryDTO.getTitle()) ) {
			entry.setTitle(ReservationInternalConstants.PassengerTitle.MS);
		} else if ( "CHD".equals(entryDTO.getTitle()) ) {
			entry.setTitle(ReservationInternalConstants.PassengerTitle.CHILD);
		} else if ( "MRS".equals(entryDTO.getTitle()) ) {
			entry.setTitle(ReservationInternalConstants.PassengerTitle.MRS);
		} else if ( "MISS".equals(entryDTO.getTitle()) ) {
			entry.setTitle(ReservationInternalConstants.PassengerTitle.MISS);
		} else if ( "MSTR".equals(entryDTO.getTitle()) ) {
			entry.setTitle(ReservationInternalConstants.PassengerTitle.MASTER);
		}
		
		if (entryDTO.getInfant() != null) {
			entry.setIsParent("Y");
		} else {
			entry.setIsParent("N");
		}
		return entry;
	}
	
}
