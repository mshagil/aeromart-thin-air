
package com.isa.thinair.airreservation.core.bl.reminder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ccc.InsuranceSaleDTO;
import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakFlightInsuranceInfoDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakInsuredTraveler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceSalesType;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airschedules.api.model.FlightSegNotificationEvent;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;

public class InsuranceDataPublisherBL {
	private static Log log = LogFactory.getLog(InsuranceDataPublisherBL.class);

	private InsurancePublisherDetailDTO insurancePublisherDetailDTO = null;

	private FlightBD flightBD = ReservationModuleUtils.getFlightBD();

	public FlightSegNotificationEvent publishFlightInsuranceInfo(InsurancePublisherDetailDTO insurancePublisherDetailDTO)
			throws ModuleException {
		this.insurancePublisherDetailDTO = insurancePublisherDetailDTO;
		RakFlightInsuranceInfoDTO rakFlightInsuranceInfoDTO = null;

		String status = insurancePublisherDetailDTO.getStatus();

		// Status check to determine whether this is a status check or insurance upload
		if (status != null
				&& (status.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_SENT) || status
						.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_LAST_SENT))) {
			String[] result = ReservationModuleUtils.getWSClientBD().getInsurancePubStatus(
					insurancePublisherDetailDTO.getInsResponse());
			return updateFlightSegmentEvent(result);
		} else {
			Collection<RakInsuredTraveler> travellers = ReservationModuleUtils.getReservationBD()
					.getTravellersForRakInsurancePublishing(insurancePublisherDetailDTO.getFlightSegId());
			rakFlightInsuranceInfoDTO = new RakFlightInsuranceInfoDTO();
			rakFlightInsuranceInfoDTO.setFlightDepartureDate(insurancePublisherDetailDTO.getDeparturetimeLocal());
			rakFlightInsuranceInfoDTO.setFlightNo(insurancePublisherDetailDTO.getFlightNumber());
			rakFlightInsuranceInfoDTO.setUploadResponse(this.insurancePublisherDetailDTO.getInsResponse());
			rakFlightInsuranceInfoDTO.setOriginAirport(insurancePublisherDetailDTO.getFlightOrigin());

			if (insurancePublisherDetailDTO.getNotificationReminderTime() != null) {
				rakFlightInsuranceInfoDTO.setNotificationSendingTime(insurancePublisherDetailDTO.getNotificationReminderTime());
			} else {
				rakFlightInsuranceInfoDTO.setNotificationSendingTime(new java.util.Date());
			}

			// Insured travellers are available
			if (travellers != null && travellers.size() > 0) {

				rakFlightInsuranceInfoDTO.setTravellers(travellers);
				rakFlightInsuranceInfoDTO.setRecordsNo(String.valueOf(travellers.size()));

			} else {

				rakFlightInsuranceInfoDTO.setRecordsNo(String.valueOf("0"));
			}

			String result = ReservationModuleUtils.getWSClientBD().publishRakInsuranceData(rakFlightInsuranceInfoDTO);
			return updateFlightSegmentEvent(result);

		}
	}
	
	public String publishDailyInsuranceInfo(InsurancePublisherDetailDTO insurancePublisherDetailDTO) throws ModuleException {
		this.insurancePublisherDetailDTO = insurancePublisherDetailDTO;
		RakFlightInsuranceInfoDTO rakFlightInsuranceInfoDTO = null;

		String status = insurancePublisherDetailDTO.getStatus();

		// Status check to determine whether this is a status check or insurance upload
		if (status != null
				&& (status.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_SENT) || status
						.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_LAST_SENT))) {
			String[] result = ReservationModuleUtils.getWSClientBD().getInsurancePubStatus(
					insurancePublisherDetailDTO.getInsResponse());
			if (result != null && result.length > 0) {
				return result[0];
			} else {
				return null;
			}
		} else {
			Collection<RakInsuredTraveler> travellers = ReservationModuleUtils.getReservationBD()
					.getTravellersForDailyRakInsurancePublishing(insurancePublisherDetailDTO.getBookingDate());
			rakFlightInsuranceInfoDTO = new RakFlightInsuranceInfoDTO();
			rakFlightInsuranceInfoDTO.setFlightDepartureDate(insurancePublisherDetailDTO.getDeparturetimeLocal());
			rakFlightInsuranceInfoDTO.setFlightNo(insurancePublisherDetailDTO.getFlightNumber());
			rakFlightInsuranceInfoDTO.setUploadResponse(this.insurancePublisherDetailDTO.getInsResponse());
			rakFlightInsuranceInfoDTO.setOriginAirport(insurancePublisherDetailDTO.getFlightOrigin());

			if (insurancePublisherDetailDTO.getNotificationReminderTime() != null) {
				rakFlightInsuranceInfoDTO.setNotificationSendingTime(insurancePublisherDetailDTO.getNotificationReminderTime());
			} else {
				rakFlightInsuranceInfoDTO.setNotificationSendingTime(new java.util.Date());
			}

			// Insured travellers are available
			if (travellers != null && travellers.size() > 0) {

				rakFlightInsuranceInfoDTO.setTravellers(travellers);
				rakFlightInsuranceInfoDTO.setRecordsNo(String.valueOf(travellers.size()));

			} else {

				rakFlightInsuranceInfoDTO.setRecordsNo(String.valueOf("0"));
			}

			String result = ReservationModuleUtils.getWSClientBD().publishRakInsuranceData(rakFlightInsuranceInfoDTO);
			updateDailyRakInsurancePublishingStatus(result);
			return result;

		}
	}

	/**
	 * Inserts or updates the UploadFile method response to the T_FLIGHT_SEG_NOTIFICATION_EVNT table
	 * 
	 * @param result
	 * @return
	 * @throws ModuleException
	 */
	private FlightSegNotificationEvent updateFlightSegmentEvent(String result) throws ModuleException {

		FlightSegNotificationEvent notifyEvent = null;

		if (this.insurancePublisherDetailDTO != null) {

			if (this.insurancePublisherDetailDTO.getPublishType().equals(InsurancePublisherDetailDTO.PUBLISH_FIRST)) {
				if (result != null && !result.equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_INVALID_XML)) {
					notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
							insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
							InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_SENT, "", 0,
							ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, result, null);
				} else {
					notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
							insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
							InsurancePublisherDetailDTO.INS_PUBLISH_STATE_NOT_SENT, "", 0,
							ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, null, null);
				}
			} else {

				if (result != null && !result.equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_INVALID_XML)) {
					notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
							insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
							InsurancePublisherDetailDTO.INS_PUBLISH_STATE_LAST_SENT, "", 0,
							ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, result, null);
				} else {
					notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
							insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
							InsurancePublisherDetailDTO.INS_PUBLISH_STATE_NOT_SENT, "", 0,
							ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, null, null);
				}

			}

		}
		return notifyEvent;
	}

	/**
	 * Updates the UploadFile method response to the T_PNR_INSURANCE table
	 * @param result
	 * @return
	 * @throws ModuleException
	 */
	private void updateDailyRakInsurancePublishingStatus(String result)throws ModuleException{
		int status = ReservationInternalConstants.InsurancePublishStatus.PUBLISH_SUCCESS;
		if(result != null && !result.equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_INVALID_XML)){
			status = ReservationInternalConstants.InsurancePublishStatus.PUBLISH_FAILED;
		}
		if(this.insurancePublisherDetailDTO !=null && insurancePublisherDetailDTO.getPnrList() != null && insurancePublisherDetailDTO.getPnrList().size() > 0){			
			ReservationDAOUtils.DAOInstance.RESERVATION_DAO.updateReservationInsurancePublishStatus(insurancePublisherDetailDTO.getPnrList(),status);	
		}
		
	}
	/**
	 * Updates status of the insurance upload upon the GetStatus method result
	 * 
	 * @param result
	 * @return
	 * @throws ModuleException
	 */
	private FlightSegNotificationEvent updateFlightSegmentEvent(String[] result) throws ModuleException {

		FlightSegNotificationEvent notifyEvent = null;

		if (this.insurancePublisherDetailDTO != null) {

			if (result != null && result.length > 0) {

				if (log.isDebugEnabled()) {
					log.debug("Processed result " + result[0]);

				}

				if (this.insurancePublisherDetailDTO.getPublishType().equals(InsurancePublisherDetailDTO.PUBLISH_FIRST)) {

					if (result[0].equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_SUCCESS)) {
						notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
								insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
								InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_SUCCESS, "", 0,
								ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, "", null);
					} else if (result[0].equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_ERRONEOUS)) {
						notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
								insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
								InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_SUCCESS, "", 0,
								ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, "",
								DataConverterUtil.createBlob(BeanUtils.nullHandler(result[1]).getBytes()));
					} else if (result[0].equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_FAILED)) {
						notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
								insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
								InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_FAILED, "", 0,
								ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, "",
								DataConverterUtil.createBlob(BeanUtils.nullHandler(result[1]).getBytes()));
					}
				} else {

					if (result[0].equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_SUCCESS)) {
						notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
								insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
								InsurancePublisherDetailDTO.INS_PUBLISH_STATE_LAST_SUCCESS, "", 0,
								ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, "", null);
					} else if (result[0].equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_ERRONEOUS)) {
						notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
								insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
								InsurancePublisherDetailDTO.INS_PUBLISH_STATE_LAST_SUCCESS, "", 0,
								ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, "",
								DataConverterUtil.createBlob(BeanUtils.nullHandler(result[1]).getBytes()));
					} else if (result[0].equals(InsurancePublisherDetailDTO.RAK_INS_UPLOAD_FAILED)) {
						notifyEvent = flightBD.updateFlightSegmentNotifyStatus(insurancePublisherDetailDTO.getFlightSegId(),
								insurancePublisherDetailDTO.getFlightSegmentNotificationEventId(),
								InsurancePublisherDetailDTO.INS_PUBLISH_STATE_LAST_FAILED, "", 0,
								ReservationInternalConstants.NotificationType.RAK_INSURANCE_PUBLISH, "",
								DataConverterUtil.createBlob(BeanUtils.nullHandler(result[1]).getBytes()));
					}
				}

			}

		}
		return notifyEvent;
	}

	/**
	 * Publish daily Sales insurance data
	 * 
	 * @throws ModuleException
	 * @throws ParseException
	 */
	public boolean publishDailySaleInsuranceData(Collection<InsuranceSaleDTO> insData, InsuranceSalesType salesType)
			throws ModuleException {
		Collection<InsuranceSaleDTO> listsWithNames = new ArrayList<InsuranceSaleDTO>();
		Map<String,String> config = ReservationModuleUtils.getAirReservationConfig().getCccInsuranceConfigMap();

		InsuranceSaleDTO tempInsWithNames = null;
	//	String pnr = null;
		int paxCount = 0;
		String fileData = null;

		Map<String, InsuranceSaleDTO> insuranceMap = new HashMap<String, InsuranceSaleDTO>();

		if (insData != null) {
			for (InsuranceSaleDTO insuranceData : insData) {
				if (insuranceMap.containsKey(insuranceData.getPnr())) {
					tempInsWithNames = insuranceMap.get(insuranceData.getPnr());
					switch (paxCount) {
					case 2:
						tempInsWithNames.setPersonName2(insuranceData.getContactName());
						paxCount++;
						break;
					case 3:
						tempInsWithNames.setPersonName3(insuranceData.getContactName());
						paxCount++;
						break;
					case 4:
						tempInsWithNames.setPersonName4(insuranceData.getContactName());
						paxCount++;
						break;
					case 5:
						tempInsWithNames.setPersonName5(insuranceData.getContactName());
						paxCount++;
						break;
					case 6:
						tempInsWithNames.setPersonName6(insuranceData.getContactName());
						paxCount++;
						break;
					case 7:
						tempInsWithNames.setPersonName7(insuranceData.getContactName());
						paxCount++;
						break;
					case 8:
						tempInsWithNames.setPersonName8(insuranceData.getContactName());
						paxCount++;
						break;
					case 9:
						tempInsWithNames.setPersonName9(insuranceData.getContactName());
						paxCount++;
						break;
					case 10:
						tempInsWithNames.setPersonName10(insuranceData.getContactName());
						paxCount++;
						break;
					}
					insuranceMap.put(insuranceData.getPnr(), tempInsWithNames);
				} else {
					insuranceData.setPersonName1(insuranceData.getContactName());
					insuranceMap.put(insuranceData.getPnr(), insuranceData);
					paxCount = 2;
				}
			}

			listsWithNames = insuranceMap.values();
			fileData = createCSVFileData(listsWithNames);
			writeTextFile(fileData, config, salesType);
		}
		return ftpFile(fileData, config, salesType);

	}

	/**
	 * Write a file data
	 * 
	 * @param strFileData
	 */
	private void writeTextFile(String strFileData, Map<String,String> config, InsuranceSalesType salesType) {
		String saleTypeName = "dailySales";
		if (InsuranceSalesType.DAILY_FLOWN_SALES == salesType) {
			saleTypeName = "dailyFlownSales";
		}
		String FILE_NAME = PlatformConstants.getConfigRootAbsPath() + File.separatorChar + "attachments" + File.separatorChar
				+ saleTypeName + CalendarUtil.formatDateYYYYMMDD(CalendarUtil.previousDate(new Date())) + "_"
				+ config.get("fileName");
		Writer writer = null;
		try {
			File file = new File(FILE_NAME);
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(strFileData);
		} catch (FileNotFoundException e) {
			System.out.print("Error writing file.");
		} catch (IOException e) {
			System.out.print("Error writing file.");
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				System.out.print("Error writing file.");
			}
		}
	}

	/**
	 * Create CSV file data
	 * 
	 * @param insData
	 * @return
	 * @throws ParseException
	 */
	private static String createCSVFileData(Collection<InsuranceSaleDTO> insData) throws ModuleException {
		Map<String,String> cccInsuranceConfigData = ReservationModuleUtils.getAirReservationConfig().getCccInsuranceConfigMap();
		String csvSeparator = ";";
		StringBuilder csvBuilder = new StringBuilder();
		if (cccInsuranceConfigData != null && insData != null) {
			for (InsuranceSaleDTO insuranceSaleDTO : insData) {
				if (insuranceSaleDTO != null) {
					csvBuilder.append(insuranceSaleDTO.getStatus()).append(csvSeparator);
					csvBuilder.append(cccInsuranceConfigData.get("agencyCode")).append(csvSeparator);
					csvBuilder.append(cccInsuranceConfigData.get("typeOfContract")).append(csvSeparator);
					csvBuilder.append(insuranceSaleDTO.getNatureOfContract()).append(csvSeparator);
					csvBuilder.append(getInsuranceContractID(cccInsuranceConfigData, insuranceSaleDTO.getInsType())).append(
							csvSeparator);
					csvBuilder.append(insuranceSaleDTO.getPolicyCode()).append(csvSeparator);
					csvBuilder.append(insuranceSaleDTO.getVendor()).append(csvSeparator);
					csvBuilder.append(insuranceSaleDTO.getTravelDate()).append(csvSeparator);
					csvBuilder.append(insuranceSaleDTO.getInsBookingDate()).append(csvSeparator);
					csvBuilder.append(insuranceSaleDTO.getDepartureDate()).append(csvSeparator);
					try {
						csvBuilder.append(
								insuranceSaleDTO.isReturn() ? insuranceSaleDTO.getReturnDate() : CalendarUtil
										.getDateInFormattedString("dd/MM/yyyy", CalendarUtil.add(
												CalendarUtil.getParsedTime(insuranceSaleDTO.getDepartureDate(), "dd/MM/yyyy"), 0,
												0, 90, 0, 0, 0))).append(csvSeparator);
					} catch (ParseException e) {
						csvBuilder.append("").append(csvSeparator);
						log.error(e);
					}
					csvBuilder.append(cccInsuranceConfigData.get("tourOperator")).append(csvSeparator);
					csvBuilder.append(insuranceSaleDTO.getCountryCode()).append(csvSeparator);
					csvBuilder.append(StringUtil.getSubString(insuranceSaleDTO.getContactName(), 30)).append(csvSeparator);
					csvBuilder.append("").append(csvSeparator); // Address
					csvBuilder.append("").append(csvSeparator); // ZIP
					csvBuilder.append("").append(csvSeparator); // Town
					csvBuilder.append("").append(csvSeparator); // Country
					csvBuilder.append("").append(csvSeparator); // Phone
					csvBuilder.append("").append(csvSeparator); // Mobile
					csvBuilder.append("").append(csvSeparator); // E-mail
					csvBuilder.append("").append(csvSeparator); // Emergency Contact name
					csvBuilder.append("").append(csvSeparator); // Professional substitute contact name
					csvBuilder.append("").append(csvSeparator); // child custody contact name
					csvBuilder.append("").append(csvSeparator); // Other Case
					csvBuilder.append(cccInsuranceConfigData.get("typeOfTravel")).append(csvSeparator);
					csvBuilder.append(getInsuranceType(insuranceSaleDTO.getInsType())).append(csvSeparator);
					csvBuilder.append(insuranceSaleDTO.getPaxCount()).append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName1()), 25))
							.append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName2()), 25))
							.append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName3()), 25))
							.append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName4()), 25))
							.append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName5()), 25))
							.append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName6()), 25))
							.append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName7()), 25))
							.append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName8()), 25))
							.append(csvSeparator);
					csvBuilder
							.append(StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName9()), 25))
							.append(csvSeparator);
					csvBuilder.append(
							StringUtil.getSubString(StringUtil.getNotNullString(insuranceSaleDTO.getPersonName10()), 25)).append(
							csvSeparator);
					csvBuilder.append(getIntValue(insuranceSaleDTO.getTotalTikectPrice())).append(csvSeparator);
					csvBuilder.append(getIntValue(insuranceSaleDTO.getAmount())).append(csvSeparator);
					csvBuilder.append("").append(csvSeparator); // Observations
					csvBuilder.append("N").append(csvSeparator); // Taxe
					csvBuilder.append(getIntValue(calculatedInsuranceCommisionAmount(new BigDecimal(
							(String) cccInsuranceConfigData.get("commissionAmount")), insuranceSaleDTO.getAmount()))); // Commission
																														// amount
					csvBuilder.append("").append(csvSeparator); // Option1
					csvBuilder.append("").append(csvSeparator); // Option2
					csvBuilder.append("").append(csvSeparator); // Option3
					csvBuilder.append("").append(csvSeparator); // Percent of fee
					csvBuilder.append("\n");
				}
			}
		}
		return csvBuilder.toString();
	}

	private static BigDecimal calculatedInsuranceCommisionAmount(BigDecimal commisionPercentage, BigDecimal insuranceAmount) {
		BigDecimal commissionAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (commisionPercentage != null)
			commissionAmount = AccelAeroCalculator
					.divide(AccelAeroCalculator.multiply(insuranceAmount, commisionPercentage), 100);
		return commissionAmount;
	}

//	private static BigDecimal calculatedTotalInsuranceTicketPrice(BigDecimal insuranceAmount, BigDecimal insurancePercentage) {
//		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
//		if (insuranceAmount != null && insurancePercentage != null)
//			total = AccelAeroCalculator.multiply(AccelAeroCalculator.divide(insuranceAmount, insurancePercentage), 100);
//		return total;
//	}

	private static String getInsuranceContractID(Map<String,String> config, int insCode) {
		String contactID = "";
		if (insCode == 2) {
			contactID = (String) config.get("cancelation_contractID");
		} else if (insCode == 3) {
			contactID = (String) config.get("multiRisk_contractID");
		}
		return contactID;
	}

	/**
	 * Get Insurance type
	 * 
	 * @param typeCode
	 * @return
	 */
	private static String getInsuranceType(int typeCode) {
		String typeCodeS = "NONE";
		switch (typeCode) {
		case 0:
			typeCodeS = "NONE";
			break;
		case 1:
			typeCodeS = "GEN";
			break;
		case 2:
			typeCodeS = "ANNUL";
			break;
		case 3:
			typeCodeS = "MULTI";
			break;
		}
		return typeCodeS;
	}

	/**
	 * Get insurance percentage
	 * 
	 */
//	private static BigDecimal getInsurancePercentage(String insPercentage, int insType) {
//		String insPercentageAmount = null;
//		if (insPercentage != null) {
//			String[] insChargesArr = insPercentage.trim().split(",");
//			int insCode = 0;
//			for (String insCharge : insChargesArr) {
//				insCode = Integer.parseInt(insCharge.trim().split("=")[0]);
//				if (insType == insCode) {
//					insPercentageAmount = insCharge.trim().split("=")[1];
//					break;
//				}
//			}
//		}
//		if (insPercentageAmount == null)
//			return AccelAeroCalculator.getDefaultBigDecimalZero();
//		else
//			return new BigDecimal(insPercentageAmount);
//	}

	/**
	 * Get integer value - remove decimal
	 * 
	 * @param amount
	 * @return
	 */
	private static String getIntValue(BigDecimal amount) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (amount != null)
			value = AccelAeroCalculator.multiply(amount, AccelAeroCalculator.parseBigDecimal(Math.pow(10, 2)));
		return String.valueOf(value.intValue());
	}

	/**
	 * Ftp upload
	 * 
	 * @param csvFileString
	 */
	private boolean ftpFile(String csvFileString, Map<String,String> config, InsuranceSalesType salesType) {
		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();
		String saleTypeName = "dailySales";
		if (InsuranceSalesType.DAILY_FLOWN_SALES == salesType) {
			saleTypeName = "dailyFlownSales";
		}
		String fileName = saleTypeName + CalendarUtil.formatDateYYYYMMDD(CalendarUtil.previousDate(new Date())) + "_"
				+ config.get("fileName");
		byte[] byteStream = csvFileString.getBytes();

		serverProperty.setServerName((String) config.get("ftpServerName"));
		serverProperty.setUserName((String) config.get("ftpServerUserName"));
		serverProperty.setPassword((String) config.get("ftpServerPassword"));
		ftpUtil.setPassiveMode(true);
		return ftpUtil.uploadAttachment(fileName, byteStream, serverProperty);
	}

}
