package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ConfirmOnholdReservationIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	public ConfirmOnholdReservationIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {

		RuleResponseDTO response = getResponseDTO();

		if (isNotNull(dataContext.getExisitingReservation()) && isNotNull(dataContext.getUpdatedReservation())
				&& isOHDReservation(dataContext.getExisitingReservation())
				&& isCNFReservation(dataContext.getUpdatedReservation())) {

			updatePaxWiseAffectedSegmentListToAllGivenPax(response, getUncancelledSegIds(dataContext.getUpdatedReservation()),
					null, getUncancelledPaxIds(dataContext.getUpdatedReservation()));

		}

		return response;
	}

}
