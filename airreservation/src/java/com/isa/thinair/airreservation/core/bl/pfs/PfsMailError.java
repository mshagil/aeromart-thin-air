/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pfs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.PfsLogDTO;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PaxFinalSalesDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;

/**
 * Class to implement the business logic related to the Mailing of Failed XA Pnls
 * 
 * @author Byorn
 */
public abstract class PfsMailError {

	private static Log log = LogFactory.getLog(PfsMailError.class);

	/**
	 * Notify Error
	 * 
	 * @param pfsId
	 * @param e
	 * @param fileName
	 * @throws ModuleException
	 */
	public static void notifyError(Integer pfsId, Throwable e, String fileName) throws ModuleException {

		PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;

		try {

			PfsLogDTO logDTO = new PfsLogDTO();

			// get the pfs content and format it.
			String pfsContent = "";
			boolean isWaitingPFS = false;

			if (pfsId != null) {
				Pfs pfs = paxFinalSalesDAO.getPFS(pfsId.intValue());
				if (pfs != null) {
					pfsContent = pfs.getPfsContent();
					if (pfs.getProcessedStatus().equalsIgnoreCase(ReservationInternalConstants.PfsStatus.WAITING_FOR_ETL_PROCESS)) {
						isWaitingPFS = true;
					}
					if (pfsContent != null) {
						pfsContent.replaceAll("\n", "<br>");
					}
				}
			}
			logDTO.setPfsContent(pfsContent);

			// if exception
			if (e != null) {

				if (e instanceof ModuleException) {
					ModuleException me = (ModuleException) e;
					logDTO.setExceptionDescription(me.getMessageString());
				} else if (e instanceof CommonsDataAccessException) {
					CommonsDataAccessException me = (CommonsDataAccessException) e;
					logDTO.setExceptionDescription(me.getMessageString());
				} else {
					logDTO.setExceptionDescription(e.getMessage());
				}

				logDTO.setStackTraceElements(e.getStackTrace());
			}

			// get the error pax
			Collection<String> statuss = new ArrayList<String>();
			statuss.add(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			statuss.add(ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED);
			Collection<String> errorPax = new ArrayList<String>();
			if (!isWaitingPFS) {
				errorPax = paxFinalSalesDAO.getPaxNamesForMail(pfsId, statuss);
			}

			if (errorPax != null && errorPax.size() > 0) {
				logDTO.setPaxWithErrors(errorPax);
			}
			statuss.clear();

			// set success pax
			statuss.add(ReservationInternalConstants.PfsProcessStatus.PROCESSED);
			Collection<String> reservationPax = paxFinalSalesDAO.getPaxNamesForMail(pfsId, statuss);
			if (reservationPax != null && reservationPax.size() > 0) {
				logDTO.setPaxWithReservations(reservationPax);
			}

			logDTO.setFileName(fileName);

			sendMailtoRelaventAuthorities(logDTO);

		} catch (Exception x) {
			log.error("################ ERROR IN NOTIFY ERROR METHOD PARSING PFS ID) ", x);
			throw new ModuleException(x, "airreservations.xaPnl.sendMail");
		}

	}

	/**
	 * Notify Error
	 * 
	 * @param logDTO
	 * @throws ModuleException
	 */
	public static void notifyError(PfsLogDTO logDTO) throws ModuleException {
		try {
			sendMailtoRelaventAuthorities(logDTO);
		} catch (Exception e) {
			log.error("############### ERROR IN NOTIFY ERROR METHOD ", e);
			throw new ModuleException(e, "airreservations.xaPnl.sendMail");

		}

	}

	/**
	 * Will take a list of of email addresses from a config file and will send email to them without exception details
	 * 
	 * @param logDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendMailtoRelaventAuthorities(PfsLogDTO logDTO) {

		Properties props = ReservationModuleUtils.getAirReservationConfig().getEmailPfsErrorTo();
		Enumeration enumeration = props.elements();
		List messageList = new ArrayList();

		while (enumeration.hasMoreElements()) {

			String emailAddress = (String) enumeration.nextElement();
			logDTO.setEmailTo(emailAddress);
			UserMessaging user = new UserMessaging();
			user.setFirstName(emailAddress);
			user.setToAddres(emailAddress);
			messageList.add(user);
		}
		sendMail(messageList, logDTO);
	}

	/**
	 * Is called by the above two methods
	 * 
	 * @param messageList
	 * @param logDTO
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendMail(List messageList, PfsLogDTO logDTO) {

		// MessageProfileList to be emailed
		List list = new ArrayList();

		Iterator iterator = messageList.iterator();
		while (iterator.hasNext()) {

			MessageProfile profile = new MessageProfile();
			List msgList = new ArrayList();
			msgList.add(iterator.next());
			profile.setUserMessagings(msgList);
			Topic topic = new Topic();
			HashMap map = new HashMap();
			map.put("pfsLogDTO", logDTO);

			topic.setTopicParams(map);
			topic.setTopicName("pfs_error");
			profile.setTopic(topic);
			list.add(profile);
		}

		ReservationModuleUtils.getMessagingServiceBD().sendMessages(list);
	}

}
