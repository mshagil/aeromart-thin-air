/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PFSDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PFSPaxInfo;
import com.isa.thinair.airreservation.api.dto.pfs.PnrPaxFareSegWisePaxTO;
import com.isa.thinair.airreservation.api.model.NSRefundReservation;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.NSRefundReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PaxFinalSalesDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRQ;
import com.isa.thinair.gdsservices.api.model.GdsEvent;
import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for reconciling reservation. TODO This class must enhance with Generics
 * 
 * Business Rules: (1) For no shore and no rec always the pnr number exist (2) Only Go Shore, No Rec, No Shore exist (3)
 * For Go Shore medium will be cash
 * 
 * TODO: This must be implemented as a message driven bean. also this entire class should clean up and refactor.
 * 
 * @author Nilindra Fernando, Byorn:-- "I did the go show part okaay. :)"
 * @since 1.0
 * @isa.module.command name="reconcileReservation"
 */
public class ReconcileReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReconcileReservation.class);

	/** Holds the passenger final sales dao instance */
	private PaxFinalSalesDAO paxFinalSalesDAO;

	/** Holds the reservation segment dao instance */
	private ReservationSegmentDAO reservationSegmentDAO;

	/** Holds the reservation transaction dao instance */
	private ReservationTnxDAO reservationTnxDAO;

	/** Holds the passenger dao instance */
	private PassengerDAO passengerDAO;

	private ETicketDAO eticketDAO;

	/** Hold the reservation bd instance **/
	private ReservationBD reservationBD;

	private GDSNotifyBD gdsNotifyBD;

	// Todo - pfs audits for the scheduled pfs processing are done. but not yet tested. so commented and committed
	// private PFSAuditDTO pfsAuditDTO;
	// private AuditorBD auditorBD;

	// private static final String GO_SHOW = "GO SHOW";

	/** Construct ReconcileReservation */
	private ReconcileReservation() {
		paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;

		reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		reservationTnxDAO = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;

		passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;

		eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;

		reservationBD = ReservationModuleUtils.getReservationBD();

		// Commenting this out for the moment as module dependency is not defined correctly and this variable not using
		// gdsServicesBD = ReservationModuleUtils.getGDSServicesBD();

		gdsNotifyBD = ReservationModuleUtils.getGDSNotifyBD();
	}

	/**
	 * Execute method of the ReconcileReservation command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		Integer pfsId = (Integer) this.getParameter(CommandParamNames.PFS_ID);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		// auditorBD = ReservationModuleUtils.getAuditorBD(); //for pfs audits
		boolean isCheckinProcess = ((Boolean) this.getParameter(CommandParamNames.IS_CHECKIN_PROCESS)).booleanValue();
		boolean isProcessPFS = ((Boolean) this.getParameter(CommandParamNames.IS_PROCESS_PFS)).booleanValue();
		String checkinMethod = (String) this.getParameter(CommandParamNames.PFS_CHECKIN_METHOD);
		boolean isConsiderFutureDateValidation = (Boolean) this
				.getParameter(CommandParamNames.IS_CONSIDER_FUTURE_DATE_VALIDATION);

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		boolean nsAutoRefundHoldEnabled = AppSysParamsUtil.isNSAutoRefundHoldProcessEnabled();
		boolean processStatus = false;
		// Checking params
		this.validateParams(pfsId, credentialsDTO);

		// Get Pfs parsed entries
		Collection<PfsPaxEntry> colPfsParsedEntry = paxFinalSalesDAO.getPfsParseEntries(pfsId.intValue(), null);
		Pfs pfs = paxFinalSalesDAO.getPFS(pfsId);

		Collection<PfsPaxEntry> colErrorPfsParsedEntry = new ArrayList<PfsPaxEntry>();
		Iterator<PfsPaxEntry> itPfsParsedEntry = colPfsParsedEntry.iterator();
		PfsPaxEntry pfsParsedEntry;
		Map<Integer, FlightReconcileDTO> allFlightReconcileDTOs = new HashMap<Integer, FlightReconcileDTO>();

		Collection<String> collAutoRefundPnr = new ArrayList<String>();
		Set<String> pnrsNeedToCreateTaxInvoices = new HashSet<>();

		Date serverDate = new Date();
		AirportDST aptDST = ReservationModuleUtils.getAirportBD().getEffectiveAirportDST(pfs.getFromAirport(),
				pfs.getDepartureDate());
		int gmtOffset = getGMTOffset(pfs.getFromAirport());
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;
		Date zuluDepDate = CalendarUtil.getZuluDate(pfs.getDepartureDate(), gmtOffset, dstOffset);

		if ((zuluDepDate.compareTo(serverDate) < 0)
				|| ((zuluDepDate.compareTo(CalendarUtil.getEndTimeOfDate(serverDate)) <= 0) && isConsiderFutureDateValidation)) {
			processStatus = true;
			while (itPfsParsedEntry.hasNext()) {
				pfsParsedEntry = (PfsPaxEntry) itPfsParsedEntry.next();

				// If any not processed record exist
				if (ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED.equals(pfsParsedEntry.getProcessedStatus())
						|| ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED
								.equals(pfsParsedEntry.getProcessedStatus())) {
					String originPnr = pfsParsedEntry.getPnr();
					// Move pfs pax processing functions to separate transaction and catching module exception if any,
					// just
					// to continue pfs processing job without interruption
					try {
						
						if (ReservationInternalConstants.PfsPaxStatus.NO_REC.equals(pfsParsedEntry.getEntryStatus())
								&& pfsParsedEntry.getExternalRecordLocator() != null && !pfsParsedEntry.getExternalRecordLocator().isEmpty()
									 && pfsParsedEntry.getMarketingFlightElement() != null && pfsParsedEntry.getCodeShareFlightNo() != null) {
							
							pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.PROCESSED);
							pfsParsedEntry.setErrorDescription(new ModuleException("messagepasser.pfs.skip.norec.codeshare.pnr").getMessageString());
							paxFinalSalesDAO.savePfsParseEntry(pfsParsedEntry);
							
						}else if(pfsParsedEntry.getMarketingFlightElement() != null && pfsParsedEntry.getCodeShareFlightNo() == null){
							
							pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
							pfsParsedEntry.setErrorDescription(new ModuleException("messagepasser.pfs.skip.norec.codeshare.pnr.not.codeshare.flight")
													.getMessageString());
							paxFinalSalesDAO.savePfsParseEntry(pfsParsedEntry);
							
						}else{
							Object values[] = reservationBD.reconcilePfsEntry(pfsParsedEntry, colErrorPfsParsedEntry, credentialsDTO,
									checkinMethod, allFlightReconcileDTOs, pfs);
							
							// TODO improve this. for the moment commit this as this is urgent patch
							pfsParsedEntry = (PfsPaxEntry) values[0];

							if (pfsParsedEntry.isCheckCreditAutoRefund() && !collAutoRefundPnr.contains(pfsParsedEntry.getPnr())) {
								collAutoRefundPnr.add(pfsParsedEntry.getPnr());
							}

							allFlightReconcileDTOs = (Map<Integer, FlightReconcileDTO>) values[1];
							colErrorPfsParsedEntry = (Collection<PfsPaxEntry>) values[2];
							String pnrNeedsTaxInvoiceToGenerate = (String) values[3];


							if (!StringUtil.isNullOrEmpty(pnrNeedsTaxInvoiceToGenerate)) {
								pnrsNeedToCreateTaxInvoices.add(pnrNeedsTaxInvoiceToGenerate);
							}
							// auditorBD.preparePFSAudit(pfsAuditDTO,pfsId.longValue(),CommandParamNames.AUDIT_ACTION_PROCESS,CommandParamNames.AUDIT_ACTION_STATUS_SUCCESS,null);
							// //for pfs audits
						}

					} catch (ModuleException ex) {
						pfsParsedEntry.setPnr(originPnr);
						pfsParsedEntry.setErrorDescription(ex.getMessageString());
						pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
						colErrorPfsParsedEntry.add(pfsParsedEntry);
						paxFinalSalesDAO.savePfsParseEntry(pfsParsedEntry);
						log.info("Exception Occurred while parsing PFS Entry " + pfsParsedEntry.getFirstName() + " "
								+ pfsParsedEntry.getLastName());
						ex.printStackTrace();
					} catch (Exception ex) {
						// Saving the error description and status if exception occured
						paxFinalSalesDAO.savePfsParseEntry(pfsParsedEntry);
						log.info("Exception Occurred while parsing PFS Entry " + pfsParsedEntry.getFirstName() + " "
								+ pfsParsedEntry.getLastName());
						// auditorBD.preparePFSAudit(pfsAuditDTO,pfsId.longValue(),CommandParamNames.AUDIT_ACTION_PROCESS,CommandParamNames.AUDIT_ACTION_STATUS_ERROR,ex.getMessage());
						ex.printStackTrace();
					}

				}
				// If any error occured record exist
				else if (ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED.equals(pfsParsedEntry.getProcessedStatus())) {
					colErrorPfsParsedEntry.add(pfsParsedEntry);
				}
			}

			if (colErrorPfsParsedEntry.size() == 0) {
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.RECONCILE_SUCCESS);
			} else {
				pfs.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			}
		} else {
			pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.PARSED);
		}
		// Saving the pfs entry
		paxFinalSalesDAO.savePfsEntry(pfs);

		// send PFS for Code Share flights
		sendPFSForMarketingAirlines(colPfsParsedEntry);	
		
		// send Empty PFS for Code Share flights
		sendEmptyPFSForMarketingAirlines(pfs.getFlightNumber(), pfs.getDepartureDate());

		if (processStatus) {
			// due to no show if any PAX has credit, then that should be refunded to PAX account.
			if (collAutoRefundPnr != null && collAutoRefundPnr.size() > 0 && !nsAutoRefundHoldEnabled) {
				Iterator<String> autoRefundPnrItr = collAutoRefundPnr.iterator();
				while (autoRefundPnrItr.hasNext()) {
					String pnr = autoRefundPnrItr.next();
					ReservationModuleUtils.getPassengerBD().passengerAutoRefund(pnr, false, credentialsDTO.getTrackInfoDTO(),
							false, null);
				}
			} else if (collAutoRefundPnr != null && collAutoRefundPnr.size() > 0 && nsAutoRefundHoldEnabled) {
				// if NOSHOW auto refund hold processed enabled, pax credit refund will do in later
				this.recordNSRefundHoldPNRs(collAutoRefundPnr);
			}

			DefaultServiceResponse response = this.composeResponse(colErrorPfsParsedEntry, pnrsNeedToCreateTaxInvoices);

			// Removed this check as entire flown statuses will not change if there is a single erroneous record in the
			// PFS.
			// if (response.isSuccess() && isProcessPFS) {
			if (isProcessPFS || isCheckinProcess) {
				// Update entries flown
				Collection<FlightReconcileDTO> colFlightReconcileDTO = this.getSegmentsForReconcilationFLOWN(pfs, "");
				this.updatePnrSegmentList(allFlightReconcileDTOs, colFlightReconcileDTO);
				this.updateRemainingEntriesToFlown(allFlightReconcileDTOs.values(), isCheckinProcess, colReservationAudit,
						pfs.getFlightNumber());
			} else {
				if (isCheckinProcess) {
					// Only one record for checkin process. i.e Loop will iterate one time
					for (Iterator<PfsPaxEntry> ppfIterator = colErrorPfsParsedEntry.iterator(); ppfIterator.hasNext();) {
						PfsPaxEntry pfsPaxEntry = ppfIterator.next();
						response.setResponseCode(pfsPaxEntry.getErrorDescription());
					}
				}
			}

			// Record Audit Flown Entries
			this.postPfsAuditRecord(colReservationAudit, credentialsDTO);
			log.debug("Exit execute");
			return response;
		}
		throw new ModuleException("airreservations.pfs.process.form.downloadTS.lessthanFlightTs",
				AirreservationConstants.MODULE_NAME);
	}

	// for pfs audits
	/*
	 * private static PFSAuditDTO createPfsAuditDTO(CredentialsDTO credentialsDTO) { PFSAuditDTO pfsAuditDTO = new
	 * PFSAuditDTO(); pfsAuditDTO.setIpAddress(credentialsDTO.getTrackInfoDTO().getIpAddress());
	 * pfsAuditDTO.setUserName(credentialsDTO.getUserId()); pfsAuditDTO.setSystemNote("test note");
	 * 
	 * return pfsAuditDTO; }
	 */

	@SuppressWarnings("unchecked")
	private void postPfsAuditRecord(Collection<ReservationAudit> colReservationAudit, CredentialsDTO credentialsDTO)
			throws ModuleException {
		// Get the auditor deleagate
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		Iterator<ReservationAudit> itReservationAuditCol = colReservationAudit.iterator();
		while (itReservationAuditCol.hasNext()) {
			ReservationAudit reservationAudit = (ReservationAudit) itReservationAuditCol.next();

			reservationAudit.setCustomerId(credentialsDTO.getCustomerId());
			reservationAudit.setUserId(credentialsDTO.getUserId());
			reservationAudit.setZuluModificationDate(new Date());
			reservationAudit.setSalesChannelCode(credentialsDTO.getSalesChannelCode());

			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.IP_ADDDRESS, credentialsDTO
						.getTrackInfoDTO().getIpAddress());
			}

			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.ORIGIN_CARRIER, credentialsDTO
						.getTrackInfoDTO().getCarrierCode());
			}

			reservationAudit.getContentMap().put(
					AuditTemplateEnum.TemplateParams.PFSEntry.REMOTE_USER,
					credentialsDTO.getDefaultCarrierCode() + "/" + "[" + credentialsDTO.getAgentCode() + "]" + "/" + "["
							+ credentialsDTO.getUserId() + "]");

			reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
			auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
		}
	}

	/**
	 * Return segments for reconcilation
	 * 
	 * @param pfsEntry
	 * @param arrivalAirport
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<FlightReconcileDTO> getSegmentsForReconcilationFLOWN(Pfs pfsEntry, String arrivalAirport)
			throws ModuleException {

		Collection<Integer> flightSegIds = reservationSegmentDAO.getFlightSegmentIds(pfsEntry.getFlightNumber(),
				pfsEntry.getDepartureDate(), pfsEntry.getFromAirport(), arrivalAirport);

		return reservationSegmentDAO.getPnrSegmentsForReconcilation(flightSegIds, null, true);
	}

	private void injectDetailsForMultiLegFlights(Pfs pfsEntry, Map<Integer, FlightReconcileDTO> allFlightReconcileDTOs) {
		Collection<Integer> flightSegIds = reservationSegmentDAO.getFlightSegmentIds(pfsEntry.getFlightNumber(),
				pfsEntry.getDepartureDate(), pfsEntry.getFromAirport(), "");
		if (flightSegIds.size() > 1) {
			Set<Integer> uniqueFlightSegIds = new HashSet<Integer>();
			for (Integer pnrSegId : allFlightReconcileDTOs.keySet()) {
				FlightReconcileDTO flightReconcileDTO = allFlightReconcileDTOs.get(pnrSegId);
				uniqueFlightSegIds.add(flightReconcileDTO.getFlightSegId());
			}

			if (!uniqueFlightSegIds.containsAll(flightSegIds)) {
				flightSegIds.removeAll(uniqueFlightSegIds);
				this.updatePnrSegmentList(allFlightReconcileDTOs,
						reservationSegmentDAO.getPnrSegmentsForReconcilation(flightSegIds, null, false));
			}
		}
	}

	/**
	 * Update remaining entries to flown
	 * 
	 * @param colFlightReconcileDTO
	 * @throws ModuleException
	 */
	private void updateRemainingEntriesToFlown(Collection<FlightReconcileDTO> colFlightReconcileDTO, boolean isCheckinProcess,
			Collection<ReservationAudit> colReservationAudit, String flightNumber) throws ModuleException {
		Collection<Integer> pnrSegmentIds = this.getPnrSegmentIds(colFlightReconcileDTO);
		SimpleDateFormat sdfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		ReservationAudit reservationAudit;
		String pnr = "";
		String paxName = "";
		boolean includeStandbyPaxInPNLAndADL = AppSysParamsUtil.isIncludeStandbyPaxInPNLAndADL();
		String paxFullName = "";
		Map<String, String> pnrPaxDetailMap = new HashMap<String, String>();
		TicketChangeOfStatusRQ ticketChangeOfStatusRQ;
		 Map<Integer, String> couponStatus = new HashMap<Integer, String>();
		boolean pfsProcessWithTBAPAX = ReservationModuleUtils.getAirReservationConfig().isProcessPFSWithTBAPax();

		if (pnrSegmentIds.size() > 0) {
			Collection<PnrPaxFareSegWisePaxTO> colPnrPaxFareSegWisePaxTO = reservationSegmentDAO
					.getReconcilePaxFareSegments(pnrSegmentIds);
			Collection<Integer> colStandbyPpfsIds = new ArrayList<Integer>();
			Map<Integer, Integer> ppfsIdAndPnrPaxIdMap = getNonOpenReturnNonStandardRecords(colPnrPaxFareSegWisePaxTO,
					colStandbyPpfsIds);
			Collection<Integer> colPnrPaxIds = ppfsIdAndPnrPaxIdMap.values();

			if (colPnrPaxIds.size() > 0) {
				Map<Integer, BigDecimal> mapPaxBalances = reservationTnxDAO.getPNRPaxBalances(colPnrPaxIds);

				Integer ppfsId;
				Integer pnrPaxId;
				ReservationPax reservationPax = new ReservationPax();
				Collection<Integer> colPpfsId = new ArrayList<Integer>();
				Iterator<Integer> itPpfsIdAndPnrPaxIdMap = ppfsIdAndPnrPaxIdMap.keySet().iterator();

				while (itPpfsIdAndPnrPaxIdMap.hasNext()) {
					boolean noBalance = false;
					boolean processPAX = true;
					ppfsId = (Integer) itPpfsIdAndPnrPaxIdMap.next();
					pnrPaxId = (Integer) ppfsIdAndPnrPaxIdMap.get(ppfsId);

					reservationPax = passengerDAO.getPassenger(pnrPaxId.intValue(), false, false, false);
					paxName += " " + reservationPax.getFirstName() + " " + reservationPax.getLastName() + ",";
					paxFullName = reservationPax.getFirstName() + " " + reservationPax.getLastName();

					// avoid processing TBA PAX when PFS processing
					if (paxFullName.length() > 0 && paxFullName.contains("T B A") && !pfsProcessWithTBAPAX) {
						processPAX = false;
					}

					// If it's an adult or parent
					if (mapPaxBalances.containsKey(pnrPaxId)) {
						BigDecimal amount = (BigDecimal) mapPaxBalances.get(pnrPaxId);

						if (((amount != null && amount.doubleValue() <= 0) || AppSysParamsUtil.isUpdateToFlownEvenBalPayExists())
								&& !reservationPax.getStatus().equalsIgnoreCase("OHD") && processPAX) {
							colPpfsId.add(ppfsId);
							noBalance = true;
						}
					} else {

						// If it's an infant
						if (ReservationApiUtils.isInfantType(reservationPax)) {
							if (mapPaxBalances.containsKey(reservationPax.getAccompaniedPaxId())) {
								BigDecimal amount = (BigDecimal) mapPaxBalances.get(reservationPax.getAccompaniedPaxId());

								if (!reservationPax.getStatus().equalsIgnoreCase("OHD")
										&& ((amount != null && amount.doubleValue() <= 0) || AppSysParamsUtil
												.isUpdateToFlownEvenBalPayExists()) && processPAX) {
									colPpfsId.add(ppfsId);
									noBalance = true;
								}
							}
						}
					}

					if (noBalance) {
						if (!pnrPaxDetailMap.containsKey(pnr)) {
							pnrPaxDetailMap.put(pnr, paxName);
							paxName = "";
						} else {
							String paxNameDetail = (String) pnrPaxDetailMap.get(pnr);
							paxNameDetail += paxName;
							pnrPaxDetailMap.remove(pnr);
							pnrPaxDetailMap.put(pnr, paxNameDetail);
							paxName = "";
						}
					}

					couponStatus.put(ppfsId, null);

				}

				if (!includeStandbyPaxInPNLAndADL && colStandbyPpfsIds.size() > 0 && !isCheckinProcess) {
					reservationSegmentDAO.updatePaxFareSegments(colStandbyPpfsIds,
							ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE);
					colPpfsId.removeAll(colStandbyPpfsIds);
				}

				if (colPpfsId.size() > 0 && !isCheckinProcess) {
					
					if (AppSysParamsUtil.isAutoUpdateGroundSegStateWhenAirSegFlown()) {
						// Auto update connected ground segment state along when processing air segment to FLOWN
						Collection<Integer> colGroundSegPpfsId = reservationSegmentDAO.getConnectedGroundSegmentPPFSID(colPpfsId);
						if (colGroundSegPpfsId != null && colGroundSegPpfsId.size() > 0) {
							colPpfsId.addAll(colGroundSegPpfsId);
						}
					}

					reservationSegmentDAO
							.updatePaxFareSegments(colPpfsId, ReservationInternalConstants.PaxFareSegmentTypes.FLOWN);
					// This is to update eticket staus
					if (AppSysParamsUtil.isUpdateEticketStatusWithPFS()) {
						eticketDAO.updatePaxFareSegmentsEticketStatus(colPpfsId);
					}
					// TODO - Malaka - uncomment the following line of code to enable Type A Change of Status message to
					// be sent
					// gdsServicesBD.sendFlownSegmentStatues(colPpfsId);


					if (!couponStatus.isEmpty()) {
						ticketChangeOfStatusRQ = new TicketChangeOfStatusRQ();
						ticketChangeOfStatusRQ.setOverrideStatus(false);
						ticketChangeOfStatusRQ.setRequestType(GdsEvent.Request.CHANGE_OF_STATUS_TO_FINAL);
						ticketChangeOfStatusRQ.setCouponStatus(couponStatus);
						gdsNotifyBD.changeCouponStatus(ticketChangeOfStatusRQ);
					}

				}

				// For checkin process
				if (colPpfsId.size() > 0 && isCheckinProcess) {
					reservationSegmentDAO.updatePaxFareSegmentsForCheckin(colPpfsId);
				}

				if (colPpfsId.size() > 0) {
					for (Iterator<String> itPnr = pnrPaxDetailMap.keySet().iterator(); itPnr.hasNext();) {
						String paxPnr = (String) itPnr.next();
						String paxNamesDetails = (String) pnrPaxDetailMap.get(paxPnr);
						String bookingClass;
						paxNamesDetails = paxNamesDetails.substring(0, paxNamesDetails.length() - 1);

						reservationAudit = new ReservationAudit();
						reservationAudit.setPnr(paxPnr);
						reservationAudit.setModificationType(AuditTemplateEnum.PAX_PFS_ENTRY.getCode());
						reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.PAX_STATUS, "FLOWN");

						Iterator<FlightReconcileDTO> itColFlightReconcileDTO = colFlightReconcileDTO.iterator();
						FlightReconcileDTO flightReconcileDTO;

						while (itColFlightReconcileDTO.hasNext()) {
							flightReconcileDTO = (FlightReconcileDTO) itColFlightReconcileDTO.next();
							if (flightReconcileDTO.getPnr().equals(paxPnr)) {
								bookingClass = paxFinalSalesDAO.getReservationBookingclass(paxPnr,
										flightReconcileDTO.getFlightSegId(), false);
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.PAX_NAME,
										paxNamesDetails);
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.SEGMENT,
										flightReconcileDTO.getSegementCode());
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.BOOKING_CLASS,
										bookingClass);
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.TRAVEL_DATE,
										sdfmt.format(flightReconcileDTO.getDepartureDateTimeLocal()));
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.FLIGHT_NO, flightNumber);
							}
						}
						colReservationAudit.add(reservationAudit);
					}
				}
			}
		}
	}

	private Map<Integer, Integer> getNonOpenReturnNonStandardRecords(
			Collection<PnrPaxFareSegWisePaxTO> colPnrPaxFareSegWisePaxTO, Collection<Integer> colStandbyPpfsIds)
			throws ModuleException {
		Collection<String> bookingCodes = new HashSet<String>();

		for (PnrPaxFareSegWisePaxTO pnrPaxFareSegWisePaxTO : colPnrPaxFareSegWisePaxTO) {
			String bookingCode = BeanUtils.nullHandler(pnrPaxFareSegWisePaxTO.getBookingCode());

			if (bookingCode.length() > 0) {
				bookingCodes.add(bookingCode);
			}
		}

		Map<String, BookingClass> mapBookingCodeAndBookingClass = ReservationModuleUtils.getBookingClassBD()
				.getBookingClassesMap(bookingCodes);

		Map<Integer, Integer> ppfsIdAndPnrPaxIdMap = new HashMap<Integer, Integer>();

		for (PnrPaxFareSegWisePaxTO pnrPaxFareSegWisePaxTO : colPnrPaxFareSegWisePaxTO) {
			String bookingCode = BeanUtils.nullHandler(pnrPaxFareSegWisePaxTO.getBookingCode());

			if (bookingCode.length() > 0) {
				String bcType = mapBookingCodeAndBookingClass.get(bookingCode).getBcType();
				if (!AppSysParamsUtil.isIncludeStandbyPaxInPNLAndADL() && BookingClass.BookingClassType.STANDBY.equals(bcType)) {
					colStandbyPpfsIds.add(pnrPaxFareSegWisePaxTO.getPpfId());
				} else if (!BookingClass.BookingClassType.OPEN_RETURN.equals(bcType)) {
					ppfsIdAndPnrPaxIdMap.put(pnrPaxFareSegWisePaxTO.getPpfId(), pnrPaxFareSegWisePaxTO.getPnrPaxId());
				}
			} else {
				ppfsIdAndPnrPaxIdMap.put(pnrPaxFareSegWisePaxTO.getPpfId(), pnrPaxFareSegWisePaxTO.getPnrPaxId());
			}
		}

		return ppfsIdAndPnrPaxIdMap;
	}

	/**
	 * Prepare Pnr Segment ids
	 * 
	 * @param colFlightReconcileDTO
	 * @return
	 */
	private Collection<Integer> getPnrSegmentIds(Collection<FlightReconcileDTO> colFlightReconcileDTO) {
		Iterator<FlightReconcileDTO> itColFlightReconcileDTO = colFlightReconcileDTO.iterator();
		FlightReconcileDTO flightReconcileDTO;
		Collection<Integer> pnrSegIds = new HashSet<Integer>();

		while (itColFlightReconcileDTO.hasNext()) {
			flightReconcileDTO = (FlightReconcileDTO) itColFlightReconcileDTO.next();

			if (flightReconcileDTO.getPnrSegId() != null) {
				pnrSegIds.add(flightReconcileDTO.getPnrSegId());
			}
		}

		return pnrSegIds;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pfsId
	 * @param externalChgDTO
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(Integer pfsId, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pfsId == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	private DefaultServiceResponse composeResponse(Collection<PfsPaxEntry> colErrorPfsParsedEntry, Set<String> pnrList) {
		DefaultServiceResponse response;
		if (colErrorPfsParsedEntry.size() == 0) {
			// Constructing and returning the command response
			response = new DefaultServiceResponse(true);
			response.addResponceParam(CommandParamNames.PNR_LIST, pnrList);
		} else {
			// Constructing and returning the command response
			response = new DefaultServiceResponse(false);
		}
		return response;
	}

	private void updatePnrSegmentList(Map<Integer, FlightReconcileDTO> flightReconcileDTOs,
			Collection<FlightReconcileDTO> flightReconcileList) {

		for (FlightReconcileDTO flightreconcileDto : flightReconcileList) {

			if (!flightReconcileDTOs.containsKey(flightreconcileDto.getPnrSegId())) {
				flightReconcileDTOs.put(flightreconcileDto.getPnrSegId(), flightreconcileDto);
			}
		}
	}

	private void recordNSRefundHoldPNRs(Collection<String> collAutoRefundPnr) {
		NSRefundReservationDAO nsRefundReservationDAO = ReservationDAOUtils.DAOInstance.NSREFUND_RESERVATION_DAO;
		Map<String, NSRefundReservation> nsRefundHoldPNRs = nsRefundReservationDAO.getNSRefundHoldPNRs();
		Iterator<String> autoRefundPnrItr = collAutoRefundPnr.iterator();
		while (autoRefundPnrItr.hasNext()) {
			String pnr = autoRefundPnrItr.next();
			NSRefundReservation nsRefundReservation;
			if (nsRefundHoldPNRs.size() > 0 && nsRefundHoldPNRs.containsKey(pnr)) {
				nsRefundReservation = nsRefundHoldPNRs.get(pnr);
			} else {
				nsRefundReservation = new NSRefundReservation();
				nsRefundReservation.setPnr(pnr);
				nsRefundReservation.setRefundProcessedStatus(ReservationInternalConstants.NSRefundProcessStatus.NOT_PROCESSED);
			}
			nsRefundReservation.setNsProcessedTime(new Date());
			nsRefundReservationDAO.saveNSRefundReservation(nsRefundReservation);
		}
	}

	private static int getGMTOffset(String airport) throws ModuleException {

		Airport apt = ReservationModuleUtils.getAirportBD().getAirport(airport);
		int gmtOffset = 0;
		if (apt != null) {

			gmtOffset = (apt.getGmtOffsetAction().equals("-")) ? (-1 * apt.getGmtOffsetHours()) : apt.getGmtOffsetHours();
		}

		return gmtOffset;
	}
	
	private static void sendPFSForMarketingAirlines(Collection<PfsPaxEntry> colPfsParsedEntry) throws ModuleException {
		if (colPfsParsedEntry != null && !colPfsParsedEntry.isEmpty()) {
			List<PFSDTO> pfsDTOList = generatePFSEntries(colPfsParsedEntry);
			if (pfsDTOList != null && !pfsDTOList.isEmpty()) {
				String airlineCode = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
				ReservationModuleUtils.getPFSETLServiceBD().sendPFSMessages(pfsDTOList, airlineCode, false);
			}
		}
	}
	
	private void sendEmptyPFSForMarketingAirlines(String flightDesignator, Date departureDate) throws ModuleException {

		Collection<PFSDTO> pfsDTOColl = paxFinalSalesDAO.getCodeShareFlightDetailsForPfs(flightDesignator, departureDate);
		if (pfsDTOColl != null && !pfsDTOColl.isEmpty()) {
			String airlineCode = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			ReservationModuleUtils.getPFSETLServiceBD().sendPFSMessages(new ArrayList<PFSDTO>(pfsDTOColl), airlineCode, true);
		}

	}
	
	private static List<PFSDTO> generatePFSEntries(Collection<PfsPaxEntry> colPfsParsedEntry) throws ModuleException {
		Map<String, List<PfsPaxEntry>> flightWisePaxEntry = new HashMap<String, List<PfsPaxEntry>>();
		List<PFSDTO> pfsDTOList = new ArrayList<PFSDTO>();
		if (colPfsParsedEntry != null && !colPfsParsedEntry.isEmpty()) {
			for (PfsPaxEntry pfsPaxEntry : colPfsParsedEntry) {
				if (ReservationInternalConstants.PfsProcessStatus.PROCESSED.equals(pfsPaxEntry.getProcessedStatus())
						&& !StringUtil.isNullOrEmpty(pfsPaxEntry.getMarketingFlightElement())) {

					String codeShareFlight = pfsPaxEntry.getCodeShareFlightNo();
					if (flightWisePaxEntry.get(codeShareFlight) == null) {
						flightWisePaxEntry.put(codeShareFlight, new ArrayList<PfsPaxEntry>());
					}
					flightWisePaxEntry.get(codeShareFlight).add(pfsPaxEntry);

				}
			}

		}

		if (flightWisePaxEntry != null && !flightWisePaxEntry.isEmpty()) {
			for (String flightNo : flightWisePaxEntry.keySet()) {
				List<PfsPaxEntry> pfsEntryList = flightWisePaxEntry.get(flightNo);
				PFSDTO pfsDTO = null;

				for (PfsPaxEntry pfsPaxEntry : pfsEntryList) {
					Integer gdsId = pfsPaxEntry.getGdsId();
					if (gdsId == null) {
						continue;
					}
					if (pfsDTO == null) {
						GDSStatusTO gdsStatusTO = ReservationApiUtils.getGDSShareCarrierTO(pfsPaxEntry.getGdsId());
						Map<String, Integer> cabinCapacityMap = new HashMap<String, Integer>();
						List<AircraftCabinCapacity> cabinCapacityList = ReservationModuleUtils.getAircraftBD().getAircraftCabinCapacity(pfsPaxEntry.getFlightNumber(), pfsPaxEntry.getFlightDate());
						
						for (AircraftCabinCapacity cabinCapacity : cabinCapacityList) {
							cabinCapacity.getCabinClassCode();
							cabinCapacityMap.put(cabinCapacity.getCabinClassCode(), cabinCapacity.getSeatCapacity());
						}
						
						if (gdsStatusTO != null) {
							pfsDTO = new PFSDTO();
							pfsDTO.setArrivalAirport(pfsPaxEntry.getArrivalAirport());
							pfsDTO.setDepartureAirport(pfsPaxEntry.getDepartureAirport());
							pfsDTO.setFlightDate(pfsPaxEntry.getFlightDate());
							pfsDTO.setFlightNumber(flightNo);
							pfsDTO.setGdsCode(gdsStatusTO.getGdsCode());
							pfsDTO.setCarrierCode(gdsStatusTO.getCarrierCode());
						}

					}

					PFSPaxInfo paxInfo = new PFSPaxInfo();
					paxInfo.setPnr(pfsPaxEntry.getExternalRecordLocator());
					paxInfo.setTitle(toUpperCase(pfsPaxEntry.getTitle()));
					paxInfo.setLastName(toUpperCase(pfsPaxEntry.getLastName()));
					paxInfo.setFirstName(toUpperCase(pfsPaxEntry.getFirstName()));
					paxInfo.setEntryStatus(pfsPaxEntry.getEntryStatus());
					paxInfo.setBookingClass(pfsPaxEntry.getCodeShareBc());
					paxInfo.setCabinClass(pfsPaxEntry.getCabinClassCode());
					paxInfo.setArrivalAirport(pfsPaxEntry.getArrivalAirport());
					pfsDTO.addPassengerName(paxInfo);

				}
				pfsDTOList.add(pfsDTO);
			}
		}

		return pfsDTOList;
	}

	private static String toUpperCase(String name) {
		if (!StringUtil.isNullOrEmpty(name)) {
			name = name.trim().toUpperCase();
		}
		return name;
	}

}
