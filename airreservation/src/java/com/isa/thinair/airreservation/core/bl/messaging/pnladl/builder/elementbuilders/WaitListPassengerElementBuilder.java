/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.MarketingFlightElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.WaitListElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class WaitListPassengerElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private PassengerInformation passengerInformation;

	private BaseRuleExecutor<RulesDataContext> waitListElementRuleExecutor = new WaitListElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;

		initContextData(context);
		waitListPassengerElementTemplate();
		response = validateSubElement(currentElement);
		ammendMessageDataAccordingTo(response);
	}

	private void waitListPassengerElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		if (passengerInformation != null
				&& passengerInformation.getWaitListedInfo() != null
				&& passengerInformation.isWaitListedPassenger()) {
			String waitListPassengerDetails = passengerInformation
					.getWaitListedInfo();
			if (waitListPassengerDetails != null) {
				elementTemplate
						.append(MessageComposerConstants.PNLADLMessageConstants.WAITLIST);
				elementTemplate.append(waitListPassengerDetails);
			}
			currentElement = elementTemplate.toString();
		}
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformation = getFirstPassengerInUtilizedList();
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (currentElement != null && !currentElement.isEmpty()) {
			if (response.isProceedNextElement()) {
				executeSpaceElementBuilder(uPContext);
			} else {
				executeConcatenationElementBuilder(uPContext);
			}
			ammendToBaseLine(currentElement, currentLine, messageLine);
		}

		executeNext();
	}

	private PassengerInformation getFirstPassengerInUtilizedList() {
		PassengerInformation passengerInformation = null;
		int index = 0;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformation = uPContext.getUtilizedPassengers().get(index);
		}
		return passengerInformation;
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = waitListElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
