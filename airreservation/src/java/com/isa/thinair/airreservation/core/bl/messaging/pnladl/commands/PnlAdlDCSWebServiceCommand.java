/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.commands;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PnlAdlFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlAdlMessage;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlAdlMessageResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdditionalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory.PnlAdlAbstractDataFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory.PnlAdlDataFactoryProducer;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.messagebuilderfactory.PnlAdlMessageBuilderFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.responses.MessageResponseAdditionals;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice.BaseWebserviceExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice.adl.PnlWebServiceExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice.pnl.AdlWebserviceExecutor;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for creating a modification object for the call center reservation
 * 
 * @author Uditha Dissanayake
 * @since 1.0
 * @isa.module.command name="pnlAdlWebServiceCommand"
 */
public class PnlAdlDCSWebServiceCommand extends DefaultBaseCommand {

	/** Holds the logger instance ye */
	private static Log log = LogFactory
			.getLog(PnlAdlDCSWebServiceCommand.class);

	private PassengerCollection passengerCollection;
	private BaseDataContext baseDataContext;
	private String messageType;
	private DefaultServiceResponse response;
	private BaseWebserviceExecutor baseWebserviceExecutor;

	/**
	 * Execute method of the pnlAdlAdditionalInfoExtractor command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {

		resolveCommandParameters();
		messageType = getMessageType();

		populateDataContextByPassengerCollection();

		if (baseDataContext.getFeaturePack() != null
				&& baseDataContext.getFeaturePack().isDcsConnectivityEnabled()) {
			if (MessageTypes.ADL.toString().equals(messageType)) {
				baseWebserviceExecutor = new AdlWebserviceExecutor();
				baseDataContext.setWebserviceSuccess(baseWebserviceExecutor
						.executewebService(baseDataContext));
			} else {
				baseWebserviceExecutor = new PnlWebServiceExecutor();
				baseDataContext.setWebserviceSuccess(baseWebserviceExecutor
						.executewebService(baseDataContext));
			}
		}

		createResponseObject(response);
		return response;
	}

	private void resolveCommandParameters() {
		baseDataContext = (BaseDataContext) this
				.getParameter(CommandParamNames.PNL_DATA_CONTEXT);
		response = (DefaultServiceResponse) this
				.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		passengerCollection = baseDataContext.getPassengerCollection();
	}

	private String getMessageType() {
		return baseDataContext.getMessageType();
	}

	private void populateDataContextByPassengerCollection() {
		baseDataContext.setPassengerCollection(passengerCollection);
	}

	private PnlAdditionalDataContext createAdditionalDataContext() {
		PnlAdditionalDataContext additionalDataContext = new PnlAdditionalDataContext();
		additionalDataContext.setDepartureAirportCode(baseDataContext
				.getDepartureAirportCode());
		additionalDataContext.setPassengerCollection(passengerCollection);
		return additionalDataContext;
	}

	private void createResponseObject(DefaultServiceResponse response) {
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE,
				response);
		response.addResponceParam(CommandParamNames.PNL_DATA_CONTEXT,
				baseDataContext);
	}

}
