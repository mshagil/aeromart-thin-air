/* ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;
import javax.sql.RowSet;

import oracle.jdbc.rowset.OracleCachedRowSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.AgentDetailsDTO;
import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.dto.CashPaymentDTO;
import com.isa.thinair.airreservation.api.dto.CashSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.UserDetails;
import com.isa.thinair.airreservation.api.dto.ccc.InsuranceSaleDTO;
import com.isa.thinair.airreservation.api.model.CashSales;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceSalesType;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.SalesDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;

/**
 * SalesDAOImpl is the business DAO hibernate implementation
 * 
 * @author Nilindra Fernando, Byorn
 * @since 1.0
 * @isa.module.dao-impl dao-name="SalesDAO"
 */
public class SalesDAOImpl extends PlatformBaseHibernateDaoSupport implements SalesDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SalesDAOImpl.class);

	/*
	 * Returns total of generated Cash sales from internal summary table
	 * 
	 * @Param fromDate
	 * @Param toDate
	 */	
	public BigDecimal getGeneratedCashSalesTotal(Date fromDate, Date toDate) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { fromDate, toDate };

		String sql = "select sum(total_daily_sales) as tot from t_cash_sales where date_of_sale between ?  and ?";

		BigDecimal d = (BigDecimal) templete.query(sql, params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BigDecimal amount = null;
				while (rs.next()) {
					amount = rs.getBigDecimal("tot");
				}
				return amount;

			}
		});

		return d;
	}

	/*
	 * Returns total of transferred Cash sales from external system
	 * 
	 * @Param fromDate
	 * @Param toDate
	 */
	public BigDecimal getTransferedCashSalesTotal(Date fromDate, Date toDate, XDBConnectionUtil template) {
		BigDecimal totAmount = new BigDecimal(-1);
		//DataSource ds = ReservationModuleUtils.getExternalDBConnectionUtil(). getExternalDatasource();
		DataSource ds = template.getExternalDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { fromDate, toDate };
		String sql = "SELECT SUM(TOTAL_DAILY_SALES) as amt FROM INT_T_CASH_SALES WHERE DATE_OF_SALE BETWEEN ?  and ? ";
		try {
			totAmount = (BigDecimal) templete.query(sql, params, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					BigDecimal amount = null;
					while (rs.next()) {
						amount = rs.getBigDecimal("amt");
					}
					return amount;

				}
			});
		} catch (Exception ex) {
			log.error("External DataBase error:" + ex);
			throw new CommonsDataAccessException(ex, "exdb.getconnection.retrieval.failed", AirreservationConstants.MODULE_NAME);
		}
		return totAmount;
	}

	/*
	 * Inserting Cash Sales data to external system
	 * 
	 * @Param Collection<CashPaymentDTO>
	 * @Param XDBConnectionUtil
	 * 
	 * @throws ClassNotFoundException, SQLException, Exception
	 */
	@SuppressWarnings("rawtypes")
	public Collection<?> insertCashSalesToExternal(Collection<CashPaymentDTO> colCashPaymentDTO) throws SQLException,
			ClassNotFoundException, Exception {

		log.info("############## Going to Insert Cash Sales To External DB");
		Collection<?> output = new ArrayList();

		Connection connection = null;

		Iterator<CashPaymentDTO> it = colCashPaymentDTO.iterator();

		CashPaymentDTO dto = null;

		try {

			connection = ReservationModuleUtils.getExternalDBConnectionUtil().getExternalConnection();

			connection.setAutoCommit(false);

			String sql = "insert into  INT_T_CASH_SALES  values (?,?,?,?)";
			PreparedStatement select = connection
					.prepareStatement("select * from INT_T_CASH_SALES where DATE_OF_SALE=? and STAFF_ID=?");

			String sqlq = "delete from  INT_T_CASH_SALES where DATE_OF_SALE=? and STAFF_ID=?";
			PreparedStatement stmtq = connection.prepareStatement(sqlq);

			while (it.hasNext()) {

				dto = it.next();

				select.setDate(1, new java.sql.Date(dto.getDateOfSale().getTime()));
				select.setString(2, String.valueOf(dto.getStaffId()));
				ResultSet res = select.executeQuery();
				while (res.next()) {

					stmtq.setDate(1, res.getDate("DATE_OF_SALE"));
					stmtq.setString(2, res.getString("STAFF_ID"));
					stmtq.executeUpdate();
				}
				PreparedStatement stmt = connection.prepareStatement(sql);
				stmt.setDate(1, new java.sql.Date(dto.getDateOfSale().getTime()));
				stmt.setString(2, dto.getStaffId());
				stmt.setBigDecimal(3, dto.getTotalSales());
				stmt.setInt(4, 0);
				stmt.executeUpdate();

			}
		}

		catch (Exception exception) {
			log.error("########### Error When Inserting Cash Sales to XDB:", exception);
			connection.rollback();
			if (exception instanceof SQLException || exception instanceof ClassNotFoundException) {
				throw exception;
			} else {
				throw new CommonsDataAccessException(exception, "transfer.cashsales.failed");
			}
		}

		try {
			log.debug("############ Committing Cash Sales Data to XDB");
			connection.commit();

		} catch (Exception e) {
			log.error("########### Error When Committing Cash Sales to XDB:", e);
			throw new CommonsDataAccessException(e, "transfer.creditcardsales.failed");
		} finally {
			// connection.setAutoCommit(prevAutoCommitStatus);
			connection.close();
		}
		log.info("############## Finished Inserting Cash Sales To External DB");
		return output;
	}

	/*
	 * Inserting Cash Sales to internal summary table
	 */
	public Collection<CashSales> insertCashSalesToInternal(Collection<CashPaymentDTO> col) {
		log.info("######## Inserting Cash Sales To Internal");
		Date nowTime = new Date();
		Collection<CashSales> cashSalesPOJOs = new ArrayList<CashSales>();
		Iterator<CashPaymentDTO> it = col.iterator();

		if (col != null) {
			while (it.hasNext()) {
				CashPaymentDTO dto = it.next();
				CashSales cashSales = new CashSales();
				cashSales.setDateOfSales(dto.getDateOfSale());
				cashSales.setStaff_Id(dto.getStaffId());
				cashSales.setTotalDailySales(dto.getTotalSales());
				cashSales.setTransferStatus('N');
				cashSales.setTransferTimeStamp(nowTime);

				cashSalesPOJOs.add(cashSales);

			}
		}
		saveOrUpdateAllCashSales(cashSalesPOJOs);
		log.info("###### Finished Inserting Cash Sales To Internal");
		return cashSalesPOJOs;
	}

	/*
	 * Save All CashSales POJO's
	 * 
	 * @param cashSales
	 */
	private void saveOrUpdateAllCashSales(Collection<CashSales> cashSales) {
		hibernateSaveOrUpdateAll(cashSales);
	}

	/*
	 * Get userId and displayName from agent code
	 * 
	 * @Param agentName
	 * @Return ArrayList<UserDetails>
	 */
	public ArrayList<UserDetails> getUsersForTravelAgent(String agentName) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		final ArrayList<UserDetails> list = new ArrayList<UserDetails>();

		String sql = "select USER_ID, DISPLAY_NAME from T_USER where AGENT_CODE=?";

		Object[] params = { agentName };
		templete.query(sql, params,

		new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					UserDetails dt = new UserDetails();
					dt.setDisplayName(rs.getString("DISPLAY_NAME"));
					dt.setUserId(rs.getString("USER_ID"));
					list.add(dt);

				}
				return list;

			}
		});

		return list;

	}

	/*
	 * Returns a collection of agentCode, totalSales (Gets the total sales of each agent for a given period). Get total
	 * AMOUNT for each agentcode from T_PAX_TRANSACTION table where - DATE is within the given range - NOMINALCODE is
	 * fresh payment payment_code = 'OA' Identify the agentcode by the USER_ID
	 * 
	 * @param startDate
	 *            the schedule start date.
	 * @param endDate
	 *            the schedule end date.
	 * @return a collection.
	 * @see com.isa.thinair.airreservation.core.persistence.dao .ReservationTransactionDAO#getAgentTotalSales(Date,
	 *      Date)
	 */
	public Collection<AgentTotalSaledDTO> getAgentTotalSales(Date startDate, final Date endDate) {

		// ArrayList nominalCodes = (ArrayList) getNominalcodeForInvoiceAgent();
		Collection<AgentDetailsDTO> agentCol = getAllAgentCodes();
		log.debug("agent collection size is" + agentCol.size());
		Iterator<AgentDetailsDTO> it = agentCol.iterator();
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		Collection<AgentTotalSaledDTO> newAgents = new ArrayList<AgentTotalSaledDTO>();

		while (it.hasNext()) {
			AgentDetailsDTO adto = it.next();

			/*
			 * Object params[] = { adto.getAgentCode(), new Integer((String) nominalCodes.get(0)), new Integer((String)
			 * nominalCodes.get(1)), //new Integer((String) nominalCodes.get(2)), endDate, startDate };
			 */
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			String endD = sdf.format(endDate);
			endD = "'" + endD + " 23:59:59'" + ", " + "'dd-mon-yyyy HH24:mi:ss'";

			String startD = sdf.format(startDate);
			startD = "'" + startD + " 00:00:00'" + ", " + "'dd-mon-yyyy HH24:mi:ss'";

			String queryString = "select sum(tr.amount) as total, sum(tr.amount_local) as total_local  "
					+ " from  T_AGENT_TRANSACTION  tr where tr.agent_code='" + adto.getAgentCode()
					+ "'  and  tr.NOMINAL_CODE in (1,7)  " + " and tr.TNX_DATE between to_date(" + startD + ") and to_date("
					+ endD + ")";

			final AgentTotalSaledDTO agent = new AgentTotalSaledDTO();
			agent.setAgentCode(adto.getAgentCode());
			agent.setAccountId(adto.getAccountCode());
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					if (rs != null) {
						while (rs.next()) {
							agent.setTotalSales(rs.getBigDecimal("total"));
							agent.setTotalSalesLocal(rs.getBigDecimal("total_local"));
						}
					}
					return agent;
				}
			});
			newAgents.add(agent);
		}

		log.debug("After new agents size is" + newAgents.size());
		return newAgents;
	}

	/*
	 * Get all agent codes with payment code 'OA'
	 */
	@SuppressWarnings("unchecked")
	public Collection<AgentDetailsDTO> getAllAgentCodes() {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String queryString = " select ta.agent_code as acode,ta.ACCOUNT_CODE as acd from t_agent ta,T_AGENT_PAYMENT_METHOD pmeth  where  pmeth.PAYMENT_CODE='OA' and  ta.agent_code=pmeth.AGENT_CODE ";

		Collection<AgentDetailsDTO> agentCodes = (Collection<AgentDetailsDTO>) templete.query(queryString,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<AgentDetailsDTO> agentCodes = new ArrayList<AgentDetailsDTO>();

						while (rs.next()) {

							AgentDetailsDTO dto = new AgentDetailsDTO();
							dto.setAgentCode(rs.getString("acode"));
							dto.setAccountCode(rs.getString("acd"));
							agentCodes.add(dto);

						}

						return agentCodes;
					}
				});

		return agentCodes;
	}

	/*
	 * Get Cash Payments for the DATE
	 */
	@SuppressWarnings("unchecked")
	public Collection<CashPaymentDTO> getCashPayments(Date date) {
		// Holds the CashPaymentDTO Object Collection
		Collection<CashPaymentDTO> paymentDTOCol = new ArrayList<CashPaymentDTO>();
		// For Holding the Nominnal Codes.
		Collection<Integer> nominalCodes = new ArrayList<Integer>();
		// To Hold the User - > Total Amount
		HashMap<String, BigDecimal> userAmountMap = new HashMap<String, BigDecimal>();

		BigDecimal total = null;
		String userId = null;

		// Get All Agent Users with 'CA' ,
		// Note: For reporting purposes we have to insert a zero record for
		// agent users who have
		// no transaction in the pax transaction.
		Collection<String> agentUsers = getUsersForTravelAgentOutlets();

		// populate the nominal codes
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CASH_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_CASH.getCode()));
		String thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();

		if ((nominalCodes.size() != 0)) {
			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate template = new JdbcTemplate(ds);
			Object params[] = { date, date };

			String sql = " SELECT x.user_id as user_id , sum(x.total) as total from" + " (SELECT user_id, amount AS total "
					+ " FROM t_pax_transaction WHERE " + " nominal_code IN (" + Util.buildIntegerInClauseContent(nominalCodes)
					+ ") AND (PAYMENT_CARRIER_CODE = '" + thisAirlineCode + "' OR PAYMENT_CARRIER_CODE IS NULL) " + " AND "
					+ " TRUNC(tnx_date) = ? " + " UNION ALL " + " SELECT user_id, amount AS total "
					+ " FROM t_pax_ext_carrier_transactions WHERE " + " nominal_code IN ("
					+ Util.buildIntegerInClauseContent(nominalCodes) + ") AND " + " TRUNC(txn_timestamp) = ? ) x"
					+ " GROUP BY x.user_id";

			userAmountMap = (HashMap<String, BigDecimal>) template.query(sql, params, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					HashMap<String, BigDecimal> userAmountMap = new HashMap<String, BigDecimal>();

					if (rs != null) {
						String userId = null;
						BigDecimal total = null;
						while (rs.next()) {
							userId = rs.getString("user_id");
							total = rs.getBigDecimal("total");
							if ((userId != null) && (total != null)) {
								userAmountMap.put(userId, total);
							}
						}
					}
					return userAmountMap;
				}
			});
		}

		// popolate the DTOsa and put it to the collection
		if (userAmountMap != null && userAmountMap.size() > 0) {
			Iterator<String> iterator = userAmountMap.keySet().iterator();

			while (iterator.hasNext()) {
				userId = (String) iterator.next();
				total = (BigDecimal) userAmountMap.get(userId);

				CashPaymentDTO cashPaymentDTO = new CashPaymentDTO();
				cashPaymentDTO.setStaffId(userId);
				cashPaymentDTO.setDateOfSale(date);
				cashPaymentDTO.setTotalSales(total.negate());

				paymentDTOCol.add(cashPaymentDTO);
				if (agentUsers.contains(userId)) {
					agentUsers.remove(userId);
				}
			}

		}
		Iterator<String> it = agentUsers.iterator();
		while (it.hasNext()) {
			userId = it.next();
			if (userId != null) {
				CashPaymentDTO cashPaymentDTO = new CashPaymentDTO();
				cashPaymentDTO.setStaffId(userId);
				cashPaymentDTO.setDateOfSale(date);
				cashPaymentDTO.setTotalSales(AccelAeroCalculator.getDefaultBigDecimalZero());
				paymentDTOCol.add(cashPaymentDTO);
			}
		}
		return paymentDTOCol;
	}

	/*
	 * Get All Agent Users with Payment code 'CA'
	 */
	public Collection<String> getUsersForTravelAgentOutlets() {

		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		String sql = "select user_id from t_agent ta,t_user tu,T_AGENT_PAYMENT_METHOD pmeth  where  pmeth.PAYMENT_CODE='CA' and tu.agent_code=ta.agent_code and ta.agent_code=pmeth.AGENT_CODE";
		final Collection<String> users = new ArrayList<String>();
		templete.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs != null) {

					while (rs.next()) {

						String userId = rs.getString("user_id");
						users.add(userId);

					}
				}
				return null;
			}
		});

		return users;
	}

	/*
	 * Updates transferStatus in internal Summary table from 'N' to 'Y'
	 * 
	 * @Param Collection<CashSales>
	 */
	public void updateInternalCashTable(Collection<CashSales> pojos) {

		Iterator<CashSales> iterPojos = pojos.iterator();
		while (iterPojos.hasNext()) {
			CashSales cashSales = iterPojos.next();
			cashSales.setTransferStatus('Y');
		}
		saveOrUpdateAllCashSales(pojos);
	}

	/*
	 * Clear sales in internal summary tables for a date
	 * 
	 * @Param date
	 */
	public void clearCashSalesTable(Date date) {

		String hql = "select cashSales from CashSales cashSales where cashSales.dateOfSales=?";

		Collection<?> pojos = getSession().createQuery(hql).setDate(0, date).list();

		if (pojos != null && pojos.size() > 0) {
			deleteAll(pojos);
		}

	}

	/*
	 * Returns total of Cash Sales on each date
	 * 
	 * @Param fromDate
	 * @Param toDate
	 */
	public Object[] getCashSalesHistory(Date fromDate, Date toDate) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		final ArrayList<CashSalesHistoryDTO> dtoList = new ArrayList<CashSalesHistoryDTO>();
		final ArrayList<java.sql.Date> dateList = new ArrayList<java.sql.Date>();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String from = sdf.format(fromDate);
		String to = sdf.format(toDate);

		String sql = " select t.date_of_sale as dos, sum(t.total_daily_sales) as total, t.transfer_timestamp as transfertime, t.transfer_status as tnsstatus from t_cash_sales t where "
				+ " trunc(t.date_of_sale) between '"
				+ from
				+ "' and '"
				+ to
				+ "' group by t.date_of_sale, t.transfer_timestamp, t.transfer_status order by t.date_of_sale";

		templete.query(sql,

		new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					CashSalesHistoryDTO dto = new CashSalesHistoryDTO();

					dto.setDateOfSale(rs.getDate("dos"));
					dto.setStaffId(" ");
					dto.setDisplayName("Total Generated");
					dto.setTotal(rs.getBigDecimal("total"));
					dto.setTransferStatus(rs.getString("tnsstatus"));
					dto.setTransferTimeStamp(rs.getTimestamp("transfertime"));

					dateList.add(rs.getDate("dos"));
					dtoList.add(dto);
				}
				return dtoList;
			}
		});

		return new Object[] { dtoList, dateList };

	}

	/*
	 * Returns transactions for given dates based on nominalCodes
	 * 
	 * @Param Collection<Date> dateList
	 * @Param Collection<Integer> nominalCodes
	 */
	public RowSet getSalesByDate(Collection<Date> dateList, Collection<Integer> nominalCodes) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		List<String> paramsList = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT to_char(pt.tnx_date,'dd-mm-yyyy') as t_date, pt.amount AS amount ,  ");
		sb.append(" to_char(pt.tnx_date,'yyyymmdd') as date_for_orderBy,a.agent_code as AgentCode, a.account_code as accountCode ");
		sb.append(" ,pnr_p.pnr , pt.receipt_no as INVOICE_NUMBER ");
		sb.append("  ,actual_p.payment_mode ");
		sb.append(" FROM t_pax_transaction pt, T_AGENT a  ");
		sb.append(",T_PNR_PASSENGER pnr_p ");
		sb.append(" ,T_ACTUAL_PAYMENT_METHOD actual_p ");
		sb.append(" WHERE nominal_code IN (");
		sb.append(Util.buildIntegerInClauseContent(nominalCodes));
		sb.append(" ) AND ");
		int count = 0;
		for (Iterator<Date> iterator = dateList.iterator(); iterator.hasNext();) {
			Date date = iterator.next();
			paramsList.add(CalendarUtil.formatDate(date, "dd-MM-yyyy"));
			if (count == 0) {
				sb.append(" ( ");
			}
			sb.append("  to_date(pt.tnx_date,'dd-mm-yyyy') LIKE to_date(?,'dd-mm-yyyy')  ");
			if (iterator.hasNext()) {
				sb.append(" OR ");
			} else {
				sb.append(" ) ");
			}
			count++;
		}
		sb.append(" AND a.agent_code = pt.agent_code ");
		sb.append(" AND pnr_p.pnr_pax_id = pt.pnr_pax_id ");
		sb.append(" AND actual_p.payment_mode_id (+)= pt.payment_mode_id ");
		try {
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();
			template.query(sb.toString(), paramsList.toArray(), new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						rowSet.populate(rs);
					}
					return null;
				}
			});
			return rowSet;
		} catch (SQLException e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", AirreservationConstants.MODULE_NAME);
		}
	}

	/*
	 * @Returns Collection<InsuranceSaleDTO>
	 * 
	 * @Param fromDate
	 * @Param toDate
	 */
	public Collection<InsuranceSaleDTO> getInsuranceSalesData(Date fromDate, Date toDate, InsuranceSalesType salesType) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		final List<InsuranceSaleDTO> insDataList = new ArrayList<InsuranceSaleDTO>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT DECODE(ins.state, 'SC', '1', 'FL', 'Failed', 'RQ', 'Unknown', 'TO','Unknown','CX','3') status ,");
		sql.append(" DECODE(TOT_PAX_COUNT,1,'I',2,'I',3,'I',4,'I',5,'I',6,'I',7,'I',8,'I',9,'I','G') AS NOC , ");
		sql.append(" res.pnr , ");
		sql.append(" policy_code ,");
		sql.append(" DECODE(res.origin_channel_code, 4 ,'Web', 3, 'Agent',15, 'Other', 1, 'Other', 12, 'Other') AS vendor ,");
		sql.append(" TO_CHAR(date_of_travel, 'DD/MM/YYYY') travel_date ,");
		sql.append(" TO_CHAR(ins.sell_timestamp, 'DD/MM/YYYY') AS ins_booking_date ,");
		sql.append(" TO_CHAR(date_of_travel, 'DD/MM/YYYY') departure_date ,");
		sql.append(" TO_CHAR(date_of_return, 'DD/MM/YYYY') return_date ,");
		sql.append(" co.country_code ,");
		sql.append(" INITCAP(pp.title  || ' '  || pp.first_name   || ' '  || pp.last_name) contact_name, ");
		sql.append(" ins.ins_type AS insurance_type, ");
		sql.append(" TOT_PAX_COUNT                                                    AS pax_count     ,");
		sql.append(" amount ,");
		sql.append(" ip.PREMIUM_AMOUNT as ins_percentages, ");
		sql.append(" ins.ROUND_TRIP ,");
		sql.append(" res.status res_status, ");
		sql.append(" ins.INS_TOTAL_TICKET_PRICE ins_total_ticket_price ");
		sql.append(" FROM t_pnr_insurance ins, t_reservation res, t_pnr_passenger pp , t_station s , t_country co, ");
		if (InsuranceSalesType.DAILY_FLOWN_SALES == salesType) {
			sql.append(" t_pnr_segment ps,  t_flight_segment fs, ");
		}
		sql.append(" t_insurance_premium ip ");
		sql.append(" WHERE NOT EXISTS ");
		sql.append(" (SELECT 'X' FROM t_reservation_audit au,  t_reservation res1  WHERE au.pnr =res1.pnr AND au.template_code IN ('NEWRMP') AND res.pnr=res1.pnr) ");
		sql.append(" AND res.pnr = ins.pnr AND res.pnr = pp.pnr ");
		if (InsuranceSalesType.DAILY_FLOWN_SALES == salesType) {
			sql.append(" AND ps.pnr = res.pnr AND ps.flt_seg_id = fs.flt_seg_id ");
			sql.append(" AND fs.est_time_departure_zulu BETWEEN to_date('" + CalendarUtil.formatSQLDate(fromDate)
					+ "  00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date(' " + CalendarUtil.formatSQLDate(toDate)
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		} else {
			sql.append(" AND ins.sell_timestamp BETWEEN to_date('" + CalendarUtil.formatSQLDate(fromDate)
					+ "  00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date(' " + CalendarUtil.formatSQLDate(toDate)
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		}
		sql.append(" AND ins.destination = s.station_code ");
		sql.append(" AND s.country_code   =co.country_code ");
		sql.append(" AND ins.policy_code IS NOT NULL ");
		sql.append(" AND (ins.state = 'SC' OR ins.state = 'CX') ");
		sql.append(" AND ip.INSURANCE_PREMIUM_ID = 1 ");
		sql.append(" ORDER BY res.pnr ");

		if (log.isDebugEnabled()) {
			log.debug("### SQL:" + sql.toString());
		}
		template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				String policyCode = null;
				String cancelCode = "03";

				while (rs.next()) {
					InsuranceSaleDTO insuranceSaleDTO = new InsuranceSaleDTO();
					insuranceSaleDTO.setPnr(rs.getString("pnr"));
					insuranceSaleDTO.setNatureOfContract(rs.getString("NOC"));
					policyCode = rs.getString("policy_code");
					insuranceSaleDTO.setPolicyCode(policyCode);
					insuranceSaleDTO.setVendor(rs.getString("vendor"));
					insuranceSaleDTO.setTravelDate(rs.getString("travel_date"));
					insuranceSaleDTO.setInsBookingDate(rs.getString("ins_booking_date"));
					insuranceSaleDTO.setDepartureDate(rs.getString("departure_date"));
					insuranceSaleDTO.setReturnDate(rs.getString("return_date"));
					insuranceSaleDTO.setCountryCode(rs.getString("country_code"));
					insuranceSaleDTO.setContactName(rs.getString("contact_name"));
					insuranceSaleDTO.setInsType(rs.getInt("insurance_type"));
					insuranceSaleDTO.setPaxCount(rs.getInt("pax_count"));
					insuranceSaleDTO.setTotalTikectPrice(rs.getBigDecimal("ins_total_ticket_price"));
					insuranceSaleDTO.setAmount(rs.getBigDecimal("amount"));
					insuranceSaleDTO.setInsurancePercentage(rs.getString("ins_percentages"));
					insuranceSaleDTO.setReturn("Y".equalsIgnoreCase(rs.getString("ROUND_TRIP")) ? true : false);
					if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equalsIgnoreCase(rs.getString("res_status"))) {
						insuranceSaleDTO.setStatus(InsuranceSaleDTO.CANCEL);
					} else {
						insuranceSaleDTO.setStatus(rs.getString("status"));
					}
					if (InsuranceSaleDTO.CANCEL.equals(insuranceSaleDTO.getStatus())) {
						if (policyCode != null) {
							if (policyCode.split("_").length == 3) {
								insuranceSaleDTO.setPolicyCode(policyCode.substring(0, policyCode.lastIndexOf("_")) + "_"
										+ cancelCode);
							}
						}
					}
					insDataList.add(insuranceSaleDTO);
				}
				return insDataList;
			}
		});
		return insDataList;

	}
}
