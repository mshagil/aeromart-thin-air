/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context;

import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.FeaturePack;

/**
 * @author udithad
 *
 */
public class MessageHeaderElementContext extends ElementContext {

	private String flightNumber;
	private Date departureDate;
	private String origin;
	private String headerElement;
	private String departureAirportCode;
	private Integer partNumber;
	private Map<String,List<String>> classCodeElements;
	private FeaturePack featurePack;
	private Map<String, LinkedHashMap<String,String>> seatConfigurationElements;
	
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getHeaderElement() {
		return headerElement;
	}
	public void setHeaderElement(String headerElement) {
		this.headerElement = headerElement;
	}
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}
	public Integer getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(Integer partNumber) {
		this.partNumber = partNumber;
	}
	public Map<String, List<String>> getClassCodeElements() {
		return classCodeElements;
	}
	public void setClassCodeElements(Map<String, List<String>> classCodeElements) {
		this.classCodeElements = classCodeElements;
	}
	public FeaturePack getFeaturePack() {
		return featurePack;
	}
	public void setFeaturePack(FeaturePack featurePack) {
		this.featurePack = featurePack;
	}
	public Map<String, LinkedHashMap<String, String>> getSeatConfigurationElements() {
		return seatConfigurationElements;
	}
	
	public void setSeatConfigurationElements(Map<String, LinkedHashMap<String, String>> seatConfigurationElements) {
		this.seatConfigurationElements = seatConfigurationElements;
	}
}
