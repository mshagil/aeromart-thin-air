/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public interface MessageDataStructure<K extends BasePassengerCollection,T extends BaseDataContext> {
	public K getDataStructure(T t) throws ModuleException;
}
