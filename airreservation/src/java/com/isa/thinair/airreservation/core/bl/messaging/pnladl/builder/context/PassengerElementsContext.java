/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context;

import java.util.Map;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;

/**
 * @author udithad
 *
 */
/**
 * @author udithad
 *
 */
public class PassengerElementsContext extends ElementContext {

	private DestinationFare destinationFare;
	private PassengerStoreTypes ongoingStoreType;

	public DestinationFare getDestinationFare() {
		return destinationFare;
	}

	public void setDestinationFare(DestinationFare destinationFare) {
		this.destinationFare = destinationFare;
	}

	public PassengerStoreTypes getOngoingStoreType() {
		return ongoingStoreType;
	}

	public void setOngoingStoreType(PassengerStoreTypes ongoingStoreType) {
		this.ongoingStoreType = ongoingStoreType;
	}

	


}
