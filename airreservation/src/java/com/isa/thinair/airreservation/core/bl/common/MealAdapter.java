package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealUtil;
import com.isa.thinair.airreservation.api.dto.meal.PassengerMealAssembler;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.persistence.dao.MealDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class MealAdapter {

	private static Log log = LogFactory.getLog(SeatMapAdapter.class);

	/**
	 * Called when modify only meals
	 * 
	 * @param res
	 * @param paxMealAssembler
	 * @throws ModuleException
	 */
	public static void prepareMealsToModify(Reservation res, PassengerMealAssembler paxMealAssembler, TrackInfoDTO trackInfo)
			throws ModuleException {
		Map<Integer, Integer> flightMealIdpnrSegId = paxMealAssembler.getFlightMealIdPnrSegIdsMap();

		// get the pnr pax ids
		Collection<Integer> pnrPaxIds = res.getPnrPaxIds();
		MealDAO mealDAO = ReservationDAOUtils.DAOInstance.MEAL_DAO;
		// get the existing meal list
		Collection<PassengerMeal> passengerMeals = mealDAO.getFlightMealsForPnrPax(pnrPaxIds);
		Collection<PassengerMeal> statusToCNx = new ArrayList<PassengerMeal>();
		if (passengerMeals != null && passengerMeals.size() > 0 && paxMealAssembler.getpaxMealTORemove().values().size() > 0) {
			Map<String, Collection<PassengerMeal>> mapPaxExistingMeals = new HashMap<String, Collection<PassengerMeal>>();

			// get the passenger meal from front end.
			// Collection<String> keysToRemove = new ArrayList<String>();

			for (PassengerMeal meal : passengerMeals) {
				// TODO parameterize the unique id creation
				String uniqueId = MealUtil.makeUniqueIdForModifications(meal.getPnrPaxId(), meal.getpPFID(), meal.getPnrSegId())
						+ "|" + meal.getFlightMealId();
				if (mapPaxExistingMeals.get(uniqueId) == null) {
					mapPaxExistingMeals.put(uniqueId, new ArrayList<PassengerMeal>());
				}
				mapPaxExistingMeals.get(uniqueId).add(meal);
				// mapPaxExistingMeals.put(uniqueId, meal);

			}

			for (Collection<PaxMealTO> paxMealTOs : paxMealAssembler.getpaxMealTORemove().values()) {
				for (PaxMealTO paxMealTO : paxMealTOs) {
					for (int i = 0; i < paxMealTO.getAllocatedQty(); ++i) {
						// TODO parameterize the unique id creation
						String uniqueId = MealUtil.makeUniqueIdForModifications(paxMealTO.getPnrPaxId(),
								paxMealTO.getPnrPaxFareId(), paxMealTO.getPnrSegId()) + "|" + paxMealTO.getSelectedFlightMealId();
						Collection<PassengerMeal> meals = mapPaxExistingMeals.get(uniqueId);
						if (meals != null) {
							// marked for deletion
							/*
							 * paxMealAssembler.removePassengerFromFlightMeal( meal.getPnrPaxId(),
							 * meal.getpPFID(),meal.getPnrSegId(), meal.getFlightMealId(), meal .getChargeAmount(),
							 * meal.getMealId(),null, trackInfo);
							 */
							EXISTINGMEALCNXLOOP: for (PassengerMeal meal : meals) {
								if (meal.getStatus().equals(AirinventoryCustomConstants.FlightMealStatuses.RESERVED)) {
									meal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.PAX_CNX);
									statusToCNx.add(meal);
									break EXISTINGMEALCNXLOOP;
								}
							}
							// removeMealList.add(meal);
						} else {
							// remove from collection as the info already exists in db
							// keysToRemove.add(uniqueId);
							throw new ModuleException("airreservation.modify.meal.invalidmealmodificationattempted");
						}
					}
				}
			}

			/*
			 * for (String uniqueId : keysToRemove) { mapPaxMeals.remove(uniqueId);
			 * 
			 * }
			 */
			// update the stautus to CNX for ones which should get removed
			mealDAO.saveOrUpdate(statusToCNx);
		}

		// add the rest
		for (String uniqueId : paxMealAssembler.getPaxMealInfo().keySet()) {
			for (PaxMealTO paxMealTO : paxMealAssembler.getPaxMealInfo().get(uniqueId)) {
				Integer pnrSegId = flightMealIdpnrSegId.get(paxMealTO.getSelectedFlightMealId());
				for (int i = 0; i < paxMealTO.getAllocatedQty(); ++i) {
					paxMealAssembler.addPassengertoNewFlightMeal(paxMealTO.getPnrPaxId(), paxMealTO.getPnrPaxFareId(), pnrSegId,
							paxMealTO.getSelectedFlightMealId(), paxMealTO.getChgDTO().getAmount(), paxMealTO.getMealCode(),
							paxMealTO.getMealId(), null, paxMealTO.getAutoCancellationId(), trackInfo);
				}
			}

		}
	}

	public static void modifyMeals(Map<String, Collection<PaxMealTO>> paxMealToAdd,
			Map<String, Collection<PaxMealTO>> paxMealToRemove) throws ModuleException {
		// to add
		Collection<Integer> fltMealIdsToAdd = new ArrayList<Integer>();
		// Collection<PaxMealTO> toAdd = paxMealToAdd.values();
		for (Collection<PaxMealTO> toAdd : paxMealToAdd.values()) {
			for (PaxMealTO paxMealTo : toAdd) {
				fltMealIdsToAdd.add(paxMealTo.getSelectedFlightMealId());
			}
		}

		// to remove
		Collection<Integer> flightMealIdsToCancel = new ArrayList<Integer>();
		if (paxMealToRemove != null) {

			// Collection<PaxMealTO> toRem= paxMealToRemove.values();
			for (Collection<PaxMealTO> toRem : paxMealToRemove.values()) {
				for (PaxMealTO paxMealTo : toRem) {
					for(int i = 0; i < paxMealTo.getAllocatedQty(); i++) {
						flightMealIdsToCancel.add(paxMealTo.getSelectedFlightMealId());
					}
				}
			}
		}
		ReservationModuleUtils.getMealBD().modifyMeal(fltMealIdsToAdd, flightMealIdsToCancel);
	}

	/**
	 * Populate Meal information
	 * 
	 * @param res
	 * @param pnrSegId
	 * @throws ModuleException
	 */
	public static void populateMealInformation(Reservation res, Integer pnrSegId) throws ModuleException {
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		pnrSegIds.add(pnrSegId);

		Collection<Integer> pnrPaxIds = res.getPnrPaxIds();
		Collection<PaxMealTO> paxMealTos = ReservationDAOUtils.DAOInstance.MEAL_DAO.getReservationMeals(pnrPaxIds, pnrSegIds);
		res.setMeals(paxMealTos);
	}

	/**
	 * Populate Meal information
	 * 
	 * @param res
	 * @param pnrSegId
	 * @throws ModuleException
	 */
	public static Integer getReservationMealTemplate(int flightSegId) throws ModuleException {

		Integer templateId = ReservationDAOUtils.DAOInstance.MEAL_DAO.getTemplateForSegmentId(flightSegId);

		return templateId;

	}

	/**
	 * Method will cancell all reserved meals from existing flight segment. if the existing passenger's meal code
	 * available in the transferring segment then same meal will be allocated for the pax, else meal charge and meal
	 * will be removed . : Using the one in the seatmap as it works fine
	 * 
	 * @param res
	 *            existing reservation
	 * @param flightSegId
	 *            target flight segment id.
	 * @param chgTnxGen
	 * 
	 * @return : The canceled PaxMealTos.
	 * 
	 * @throws ModuleException
	 */
	public static Collection<PaxMealTO> cancellUpdateMealsWhenTransferringSegment(AdjustCreditBO adjustCreditBO, Reservation res,
			Integer pnrSegId, CredentialsDTO credentialsDTO, Integer flightSegId, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {

		Map<String, Integer> paxWiseMealCount = new HashMap<String, Integer>();

		// hold the existing res meals
		Collection<PaxMealTO> paxMealDTOs = res.getMeals();
		// hold the seats to cancel
		Collection<PaxMealTO> affectivePaxMealTOs = new ArrayList<PaxMealTO>();

		Collection<PaxMealTO> toReservedPaxMealTOs = new ArrayList<PaxMealTO>();

		Collection<PaxMealTO> toCancelPaxMealTOs = new ArrayList<PaxMealTO>();

		// Collection<Meal> colSeats = new ArrayList<Meal>();

		/* Hold reservation meals */
		Collection<Integer> mealCollection = new ArrayList<Integer>();

		// if no meals do nothing
		if (paxMealDTOs == null || paxMealDTOs.size() == 0) {
			return toCancelPaxMealTOs;
		}
		// populate the meals to cancel
		for (PaxMealTO paxMealTO : paxMealDTOs) {
			if (paxMealTO.getPnrSegId().equals(pnrSegId)) {
				affectivePaxMealTOs.add(paxMealTO);
			}
		}

		// if no effective meals to cancel not found, do nothing
		if (affectivePaxMealTOs == null || affectivePaxMealTOs.size() == 0) {
			return toCancelPaxMealTOs;
		}

		// use the assembler to gather information on meals to remove
		Collection<Integer> flightMealIds = new ArrayList<Integer>();

		for (PaxMealTO paxMealTo : affectivePaxMealTOs) {
			flightMealIds.add(paxMealTo.getSelectedFlightMealId());
			mealCollection.add(paxMealTo.getMealId());
		}

		// if no effective meal code to cancel - since need to reprotect the corresponding meal codes
		if (mealCollection == null || mealCollection.size() == 0) {
			return toCancelPaxMealTOs;
		}

		if (log.isDebugEnabled()) {
			log.debug("EXISTING Meals : " + mealCollection.toString());
		}

		int sourceMealId = 0;
		int targetMealId = -1;
		// PaxMealTO sourcePaxMealTO;
		PaxMealTO tempPaxMealTO;

		MealBD mealBD = ReservationModuleUtils.getMealBD();
		Collection<FlightMealDTO> flightmealDTOs = mealBD.getAvailableMealDtos(flightSegId.intValue(), mealCollection);

		boolean flag = false;

		for (PaxMealTO sourcePaxMealTO : affectivePaxMealTOs) {

			flag = false;
			sourceMealId = sourcePaxMealTO.getMealId().intValue();

			boolean isMealReprotectable = isMealReprotectable(paxWiseMealCount, flightmealDTOs, sourcePaxMealTO);

			if (isMealReprotectable) {
				for (FlightMealDTO mealDTO : flightmealDTOs) {
					targetMealId = -1;
					targetMealId = mealDTO.getMealId().intValue();
					if (targetMealId == sourceMealId) {
						tempPaxMealTO = new PaxMealTO();
						tempPaxMealTO = sourcePaxMealTO;
						tempPaxMealTO.setSelectedFlightMealId(mealDTO.getFlightMealId());
						tempPaxMealTO.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
						toReservedPaxMealTOs.add(tempPaxMealTO);
						break;

					}
				}
			} else {
				toCancelPaxMealTOs.add(sourcePaxMealTO);
			}

		}

		PassengerMealAssembler p = new PassengerMealAssembler(null, false);

		if (toCancelPaxMealTOs != null && toCancelPaxMealTOs.size() > 0) {
			// add the removing meals one by one to the assembler
			for (PaxMealTO paxMealTo : toCancelPaxMealTOs) {
				p.removePassengerFromFlightMeal(paxMealTo.getPnrPaxId(), paxMealTo.getPnrPaxFareId(), paxMealTo.getPnrSegId(),
						paxMealTo.getSelectedFlightMealId(), paxMealTo.getChgDTO().getAmount(), paxMealTo.getMealId(), null,
						credentialsDTO.getTrackInfoDTO(), paxMealTo.getAllocatedQty());
			}
			// refund the charges
			addPassengerMealChargeAndAdjustCredit(adjustCreditBO, res, p.getpaxMealTORemove(), " ", null, credentialsDTO,
					chgTnxGen);
		}

		// Save the reservation
		ReservationProxy.saveReservation(res);

		// release the meals
		ReservationModuleUtils.getMealBD().vacateMeals(flightMealIds);

		// update the passenger meals table
		updateMealsToCancel(affectivePaxMealTOs);

		if (toReservedPaxMealTOs != null && toReservedPaxMealTOs.size() > 0) {
			// Collection<Integer> toResflightSeatMapIds = new ArrayList<Integer>();
			for (Iterator<PaxMealTO> iter = toReservedPaxMealTOs.iterator(); iter.hasNext();) {
				PaxMealTO paxMeal = (PaxMealTO) iter.next();
				p.addPassengertoNewFlightMeal(paxMeal.getPnrPaxId(), paxMeal.getPnrPaxFareId(), pnrSegId,
						paxMeal.getSelectedFlightMealId(), paxMeal.getChgDTO().getAmount(), paxMeal.getMealCode(),
						paxMeal.getMealId(), null, paxMeal.getAutoCancellationId(), credentialsDTO.getTrackInfoDTO());

			}
			// reserve vacant meals in target flight.
			modifyMeals(p.getpaxMealTOAdd(), null);
			// save passenger- meal info
			MealAdapter.savePaxMealInfo(toReservedPaxMealTOs, credentialsDTO);
		}

		return toCancelPaxMealTOs;
	}

	/**
	 * If the existing passenger's meal code available in the transferring segment then same meal will be allocated for
	 * the pax.
	 * 
	 * @param res
	 *            existing reservation
	 * @param flightSegId
	 *            target flight segment id.
	 * 
	 * @throws ModuleException
	 */
	public static void updateMealsWhenTransferringSegment(AdjustCreditBO adjustCreditBO, Reservation res, Integer pnrSegId,
			CredentialsDTO credentialsDTO, Integer flightSegId) throws ModuleException {
		// hold the existing res meals
		Collection<PaxMealTO> paxMealDTOs = res.getMeals();
		// hold the seats to cancel
		Collection<PaxMealTO> affectivePaxMealTOs = new ArrayList<PaxMealTO>();

		Collection<PaxMealTO> toReservedPaxMealTOs = new ArrayList<PaxMealTO>();

		/* Hold reservation meals */
		Collection<Integer> mealCollection = new ArrayList<Integer>();

		// populate the meals to cancel
		for (PaxMealTO paxMealTO : paxMealDTOs) {
			if (paxMealTO.getPnrSegId().equals(pnrSegId)) {
				affectivePaxMealTOs.add(paxMealTO);
			}
		}

		for (Iterator<PaxMealTO> mealCodeIterator = affectivePaxMealTOs.iterator(); mealCodeIterator.hasNext();) {
			PaxMealTO paxMealDTO = (PaxMealTO) mealCodeIterator.next();
			mealCollection.add(paxMealDTO.getMealId());
		}

		if (log.isDebugEnabled()) {
			log.debug("EXISTING Meals : " + mealCollection.toString());
		}

		int sMealId = 0;
		int tMealId = -1;
		PaxMealTO sourcePaxMealTO;
		PaxMealTO tempPaxMealTO;

		MealBD mealBD = ReservationModuleUtils.getMealBD();
		Collection<FlightMealDTO> flightmealDTOs = mealBD.getAvailableMealDtos(flightSegId.intValue(), mealCollection);

		for (Iterator<PaxMealTO> iterator = affectivePaxMealTOs.iterator(); iterator.hasNext();) {

			sourcePaxMealTO = (PaxMealTO) iterator.next();
			sMealId = sourcePaxMealTO.getMealId().intValue();

			for (Iterator<FlightMealDTO> mealIterator = flightmealDTOs.iterator(); mealIterator.hasNext();) {
				tMealId = -1;
				FlightMealDTO mealDTO = (FlightMealDTO) mealIterator.next();
				tMealId = mealDTO.getMealId().intValue();
				if (tMealId == sMealId) {
					tempPaxMealTO = new PaxMealTO();
					tempPaxMealTO = sourcePaxMealTO;
					tempPaxMealTO.setSelectedFlightMealId(mealDTO.getFlightMealId());
					tempPaxMealTO.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
					toReservedPaxMealTOs.add(tempPaxMealTO);
					break;

				}
			}

		}

		PassengerMealAssembler p = new PassengerMealAssembler(null, false);

		// Save the reservation
		ReservationProxy.saveReservation(res);

		if (toReservedPaxMealTOs != null && toReservedPaxMealTOs.size() > 0) {
			// Collection<Integer> toResflightSeatMapIds = new ArrayList<Integer>();
			for (Iterator<PaxMealTO> iter = toReservedPaxMealTOs.iterator(); iter.hasNext();) {
				PaxMealTO paxMeal = (PaxMealTO) iter.next();
				p.addPassengertoNewFlightMeal(paxMeal.getPnrPaxId(), paxMeal.getPnrPaxFareId(), pnrSegId,
						paxMeal.getSelectedFlightMealId(), paxMeal.getChgDTO().getAmount(), paxMeal.getMealCode(),
						paxMeal.getMealId(), null, paxMeal.getAutoCancellationId(), credentialsDTO.getTrackInfoDTO());

			}
			// reserve vacant meals in target flight.
			modifyMeals(p.getpaxMealTOAdd(), null);
			// save passenger- meal info
			MealAdapter.savePaxMealInfo(toReservedPaxMealTOs, credentialsDTO);
		}

	}

	/*
	 * Checks re-protecting pax's requested meal amount is available in the target flight (re-protecting flight), If so
	 * transfer pax with requested meal. If not, transfer pax with assignable meal & refund credit for remaining meal
	 * amount
	 */
	private static boolean isMealReprotectable(Map<String, Integer> paxMealCount, Collection<FlightMealDTO> targetFlightmealDTOs,
			PaxMealTO sourcePaxMealTO) {
		if (targetFlightmealDTOs != null && targetFlightmealDTOs.size() > 0) {
			for (FlightMealDTO targetFlightMeal : targetFlightmealDTOs) {
				if (targetFlightMeal.getMealId().intValue() == sourcePaxMealTO.getMealId().intValue()) {
					String paxMealKey = sourcePaxMealTO.getPnrSegId() + "#" + sourcePaxMealTO.getMealId();

					if (!paxMealCount.containsKey(paxMealKey)) {
						paxMealCount.put(paxMealKey, 0);
					}

					int usedCount = paxMealCount.get(paxMealKey) + sourcePaxMealTO.getAllocatedQty();
					paxMealCount.put(paxMealKey, usedCount);

					if (targetFlightMeal.getAvailableMeals() > 0 && usedCount <= targetFlightMeal.getAvailableMeals()) {
						return true;
					} else {
						return false;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Cancels seats when transfering segments
	 * 
	 * @param res
	 * @param chgTnxGen
	 * 
	 * @return : The canceled PaxMealTos.
	 * 
	 * @throws ModuleException
	 */
	public static Collection<PaxMealTO> cancellMealWhenTransferringSegment(AdjustCreditBO adjustCreditBO, Reservation res,
			Integer pnrSegId, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// hold the existing res meals
		Collection<PaxMealTO> paxMealDTOs = res.getMeals();
		// hold the meals to cancel
		Collection<PaxMealTO> affectivePaxMealTOs = new ArrayList<PaxMealTO>();

		// if no meals do nothing
		if (paxMealDTOs == null || paxMealDTOs.size() == 0) {
			return affectivePaxMealTOs;
		}
		// populate the meals to cancel
		for (PaxMealTO paxMealTo : paxMealDTOs) {
			if (paxMealTo.getPnrSegId().equals(pnrSegId)) {
				affectivePaxMealTOs.add(paxMealTo);
			}
		}

		// if no effective meals to cancel not found, do nothing
		if (affectivePaxMealTOs == null || affectivePaxMealTOs.size() == 0) {
			return affectivePaxMealTOs;
		}

		// use the assembler to gather information on meals to remove
		PassengerMealAssembler p = new PassengerMealAssembler(null, false);

		// add the removing meals one by one to the assembler
		Collection<Integer> flightMealIds = new ArrayList<Integer>();

		for (PaxMealTO paxMealTo : affectivePaxMealTOs) {
			p.removePassengerFromFlightMeal(paxMealTo.getPnrPaxId(), paxMealTo.getPnrPaxFareId(), pnrSegId,
					paxMealTo.getSelectedFlightMealId(), paxMealTo.getChgDTO().getAmount(), paxMealTo.getMealId(), null,
					credentialsDTO.getTrackInfoDTO(), paxMealTo.getAllocatedQty());
			flightMealIds.add(paxMealTo.getSelectedFlightMealId());
		}
		// refund the charges
		addPassengerMealChargeAndAdjustCredit(adjustCreditBO, res, p.getpaxMealTORemove(), " ", null, credentialsDTO, chgTnxGen);

		// Save the reservation
		ReservationProxy.saveReservation(res);

		// release the meals
		ReservationModuleUtils.getMealBD().vacateMeals(flightMealIds);

		// update the passenger meals table
		updateMealsToCancel(affectivePaxMealTOs);

		return affectivePaxMealTOs;
	}

	public static void addPassengerMealChargeAndAdjustCredit(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Map<String, Collection<PaxMealTO>> passengerMeal, String userNotes, TrackInfoDTO trackInfoDTO,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// Adding charge entry
		Map<String, ReservationPaxOndCharge> paxIdChargeMap = addMealCharge(reservation, passengerMeal, credentialsDTO,
				chgTnxGen);

		for (String uniqueId : paxIdChargeMap.keySet()) {
			Integer pnrPaxId = Integer.parseInt(MealUtil.getPnrPaxId(uniqueId));
			adjustCreditBO.addAdjustment(reservation.getPnr(), pnrPaxId, userNotes, paxIdChargeMap.get(uniqueId), credentialsDTO,
					reservation.isEnableTransactionGranularity());
		}

	}

	@SuppressWarnings("unused")
	private static Map<String, ReservationPaxOndCharge> addMealCharge(Reservation reservation,
			Map<String, Collection<PaxMealTO>> passengerMealMap, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		Map<String, ReservationPaxOndCharge> pnrPaxIdCharge = new HashMap<String, ReservationPaxOndCharge>();
		Integer pnrPaxId = null;
		Collection<ReservationPaxFareSegment> paxFareSegments;
		Iterator<ReservationPaxFareSegment> paxFareSegmentsIterater;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
			Collection<Integer> consumedPaxOndChgIds = new ArrayList<Integer>();
			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				paxFareSegments = reservationPaxFare.getPaxFareSegments();
				paxFareSegmentsIterater = paxFareSegments.iterator();
				while (paxFareSegmentsIterater.hasNext()) {
					ReservationPaxFareSegment resPaxFareSegment = (ReservationPaxFareSegment) paxFareSegmentsIterater.next();

					if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL
							.equals(resPaxFareSegment.getSegment().getStatus())) {
						continue;
					}

					String uniqueId = MealUtil.makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
							reservationPaxFare.getPnrPaxFareId(), resPaxFareSegment.getPnrSegId());
					Collection<PaxMealTO> paxMealTos = passengerMealMap.get(uniqueId);
					int i = 0;
					if (paxMealTos != null) {
						for (PaxMealTO paxMealTo : paxMealTos) {
							ExternalChgDTO chgDTO = null;
							if (paxMealTo != null) {
								chgDTO = paxMealTo.getChgDTO();
								paxMealTo.setPnrPaxId(reservationPax.getPnrPaxId());
							}

							if (chgDTO != null) {
								chgDTO.setAmount(AccelAeroCalculator.multiply(chgDTO.getAmount(),
										new BigDecimal(paxMealTo.getAllocatedQty())));
								PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils
										.getAppliedDiscountInfoForRefund(reservationPaxFare, chgDTO, consumedPaxOndChgIds);
								ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
										.captureReservationPaxOndCharge(chgDTO.getAmount(), null, chgDTO.getChgRateId(),
												chgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, paxDiscInfo,
												null, null, chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

								if (ReservationApiUtils.isInfantType(reservationPaxFare.getReservationPax())) {
									pnrPaxId = reservationPaxFare.getReservationPax().getParent().getPnrPaxId();
								} else {
									pnrPaxId = reservationPaxFare.getReservationPax().getPnrPaxId();
								}

								pnrPaxIdCharge.put(uniqueId + "|" + i, reservationPaxOndCharge);
								++i;
							}
						}
					}
				}
			}
		}

		if (pnrPaxIdCharge.size() == 0) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		// Set reservation and passenger total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

		return pnrPaxIdCharge;
	}

	/**
	 * Update Meal to cancel
	 * 
	 * @param flightAMMealIds
	 */
	private static void updateMealsToCancel(Collection<PaxMealTO> affectedMealTOs) throws ModuleException {

		Collection<Integer> pnrSegMealId = new ArrayList<Integer>();
		for (PaxMealTO paxMealTo : affectedMealTOs) {
			pnrSegMealId.add(paxMealTo.getPkey());
		}

		MealDAO mealDAO = ReservationDAOUtils.DAOInstance.MEAL_DAO;
		Collection<PassengerMeal> passengerMeals = mealDAO.getPassengerMeals(pnrSegMealId);

		if (passengerMeals != null && passengerMeals.size() > 0) {
			for (PassengerMeal ps : passengerMeals) {
				ps.setStatus(AirinventoryCustomConstants.FlightMealStatuses.PAX_CNX);
				ps.setTimestamp(new Date());
			}
			mealDAO.saveOrUpdate(passengerMeals);
		} else {
			throw new ModuleException("airreservation.seat.inconsistent");
		}
	}

	/**
	 * called when creting new booking and modifying only
	 * 
	 * @param paxMealTos
	 * @param credentialsDTO
	 */
	private static void savePaxMealInfo(Collection<PaxMealTO> paxMealTos, CredentialsDTO credentialsDTO) {
		Collection<PassengerMeal> passengerMeals = new ArrayList<PassengerMeal>();

		for (PaxMealTO paxMealTo : paxMealTos) {
			PassengerMeal passengerMeal = new PassengerMeal();
			passengerMeal.setPnrPaxId(paxMealTo.getPnrPaxId());
			passengerMeal.setChargeAmount(paxMealTo.getChgDTO().getAmount());
			passengerMeal.setFlightMealId(paxMealTo.getSelectedFlightMealId());
			passengerMeal.setStatus(paxMealTo.getStatus());
			passengerMeal.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
			passengerMeal.setUserId(credentialsDTO.getUserId());
			passengerMeal.setpPFID(paxMealTo.getPnrPaxFareId());
			passengerMeal.setPnrSegId(paxMealTo.getPnrSegId());
			passengerMeal.setTimestamp(new Date());
			passengerMeal.setCustomerId(credentialsDTO.getCustomerId());
			passengerMeal.setMealId(paxMealTo.getMealId());

			passengerMeals.add(passengerMeal);
		}

		if (passengerMeals.size() > 0) {
			ReservationDAOUtils.DAOInstance.MEAL_DAO.saveOrUpdate(passengerMeals);
		}
	}

	/**
	 * called when modifying only TODO : refactor this later into create flow
	 * 
	 * @param paxMealTos
	 * @param credentialsDTO
	 */
	public static void modifyPaxMealInfo(Collection<Collection<PaxMealTO>> paxMealTos, CredentialsDTO credentialsDTO) {
		Collection<PassengerMeal> passengerMeals = new ArrayList<PassengerMeal>();

		for (Collection<PaxMealTO> colPaxMealTOs : paxMealTos) {
			for (PaxMealTO paxMealTo : colPaxMealTOs) {
				PassengerMeal passengerMeal = new PassengerMeal();
				passengerMeal.setPnrPaxId(paxMealTo.getPnrPaxId());
				passengerMeal.setChargeAmount(paxMealTo.getChgDTO().getAmount());
				passengerMeal.setFlightMealId(paxMealTo.getSelectedFlightMealId());
				passengerMeal.setStatus(paxMealTo.getStatus());
				passengerMeal.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
				passengerMeal.setUserId(credentialsDTO.getUserId());
				passengerMeal.setpPFID(paxMealTo.getPnrPaxFareId());
				passengerMeal.setPnrSegId(paxMealTo.getPnrSegId());
				passengerMeal.setTimestamp(new Date());
				passengerMeal.setCustomerId(credentialsDTO.getCustomerId());
				passengerMeal.setMealId(paxMealTo.getMealId());
				passengerMeal.setAutoCancellationId(paxMealTo.getAutoCancellationId());

				passengerMeals.add(passengerMeal);
			}
		}

		if (passengerMeals.size() > 0) {
			ReservationDAOUtils.DAOInstance.MEAL_DAO.saveOrUpdate(passengerMeals);
		}
	}

	/**
	 * Audit Changes to Meals
	 * 
	 * @param pnr
	 * @param mapAdded
	 * @param mapRemoved
	 * @param credentialsDTO
	 * @param userNote
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static void recordPaxWiseAuditHistoryForModifyMeals(String pnr, Map<String, Collection<PaxMealTO>> mapAdded,
			Map<String, Collection<PaxMealTO>> mapRemoved, CredentialsDTO credentialsDTO, String userNote,
			Collection[] colResInfo) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setModificationType(AuditTemplateEnum.MODIFY_MEALS.getCode());
		if (mapAdded != null && !mapAdded.isEmpty()) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Meals.ADDED,
					ripPaxMealModifications(mapAdded, colResInfo));
		}
		if (mapRemoved != null && !mapRemoved.isEmpty()) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Meals.REMOVED,
					ripPaxMealModifications(mapRemoved, colResInfo));
		}

		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Meals.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Meals.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		ReservationBO.recordModification(pnr, reservationAudit, userNote, credentialsDTO);
	}

	/**
	 * Append Meal Info to Reservation Audit
	 * 
	 * @param pnr
	 * @param flightMealTos
	 * @param credentialsDTO
	 * @param userNote
	 * @param resPax
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static String recordPaxWiseAuditHistoryForModifyMeals(String pnr, Collection<PaxMealTO> flightMealTos,
			CredentialsDTO credentialsDTO, String userNote, Collection[] colResPax) throws ModuleException {
		if (flightMealTos != null && !flightMealTos.isEmpty()) {
			Map<String, Collection<PaxMealTO>> mapMealTo = createDummyMealMap(flightMealTos);
			return ripPaxMealModifications(mapMealTo, colResPax);
		}
		return null;
	}

	public static Map<Integer, Integer> getPnrSegMealTemplateIds(Reservation reservation) throws ModuleException {
		Map<Integer, Integer> segMealTemplates = new HashMap<Integer, Integer>();
		Map<Integer, Integer> segBundledFares = new HashMap<Integer, Integer>();
		for (ReservationSegment resSeg : reservation.getSegments()) {
			if (resSeg.getBundledFarePeriodId() != null) {
				segBundledFares.put(resSeg.getPnrSegId(), resSeg.getBundledFarePeriodId());
			}
		}
		if (!segBundledFares.isEmpty()) {
			Set<Integer> bundlesFarePeriodIds = new HashSet<Integer>(segBundledFares.values());
			if (bundlesFarePeriodIds != null && !bundlesFarePeriodIds.isEmpty()) {
				List<BundledFareDTO> bundledFareDTOs = ReservationModuleUtils.getBundledFareBD()
						.getBundledFareDTOsByBundlePeriodIds((bundlesFarePeriodIds));

				Map<Integer, BundledFareDTO> bundledFareMap = new HashMap<Integer, BundledFareDTO>();
				for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
					bundledFareMap.put(bundledFareDTO.getBundledFarePeriodId(), bundledFareDTO);
				}

				for (Entry<Integer, Integer> segEntry : segBundledFares.entrySet()) {
					Integer pnrSegId = segEntry.getKey();
					Integer bundledFarePeriodId = segEntry.getValue();

					if (bundledFareMap.containsKey(bundledFarePeriodId)) {
						BundledFareDTO bundledFareDTO = bundledFareMap.get(bundledFarePeriodId);
						segMealTemplates.put(pnrSegId,
								bundledFareDTO.getApplicableServiceTemplateId(EXTERNAL_CHARGES.MEAL.toString()));
					}

				}
			}
		}

		return segMealTemplates;
	}

	/**
	 * Rips pax meal info from Map
	 * 
	 * @param mapPaxMealInfo
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String ripPaxMealModifications(Map<String, Collection<PaxMealTO>> mealMap, Collection[] colResInfo) {
		Map<Integer, Collection<PaxMealTO>> mapSortedMealInfo = segmentMealInfo(mealMap.values());
		Iterator<Map.Entry<Integer, Collection<PaxMealTO>>> it = mapSortedMealInfo.entrySet().iterator();
		StringBuilder strMealData = new StringBuilder();
		Collection[] arrResInfo = colResInfo;
		Map<Integer, String> resSegmentInfo = compileSegmentInfo(arrResInfo[1], mealMap.values());
		while (it.hasNext()) {
			Map.Entry<Integer, Collection<PaxMealTO>> map = (Map.Entry<Integer, Collection<PaxMealTO>>) it.next();
			Collection<PaxMealTO> colPaxMeal = (Collection<PaxMealTO>) map.getValue();
			String strSegmentCode = "";
			if (resSegmentInfo != null) {
				int intPnrSeg = (Integer) map.getKey();
				strSegmentCode = resSegmentInfo.get(intPnrSeg);
			}
			if (colPaxMeal != null && !colPaxMeal.isEmpty()) {
				Set<ReservationPax> resPax = (Set<ReservationPax>) arrResInfo[0];
				Map<Integer, Collection<PaxMealTO>> paxMealMap = setPaxInfo(colPaxMeal, resPax);
				boolean processedAtleastOnce = false;
				strMealData.append(strSegmentCode + " =");
				for (Integer pnrPaxId : paxMealMap.keySet()) {
					Collection<PaxMealTO> paxMealCol = paxMealMap.get(pnrPaxId);
					if (processedAtleastOnce) {
						strMealData.append(", ");
					}
					String paxName = "";
					boolean moreThanOneMeal = false;
					for (PaxMealTO paxMealTo : paxMealCol) {
						if (moreThanOneMeal) {
							strMealData.append(", ");
						}
						strMealData.append("[" + paxMealTo.getMealId() + "] " + paxMealTo.getMealCode());
						paxName = paxMealTo.getPaxName();
						moreThanOneMeal = true;
					}
					strMealData.append(" - " + paxName);
					processedAtleastOnce = true;
				}
				strMealData.append(".");
			}
		}
		return strMealData.toString();
	}

	private static Map<Integer, Collection<PaxMealTO>> setPaxInfo(Collection<PaxMealTO> colMealMap, Set<ReservationPax> resPaxs) {
		Map<Integer, Collection<PaxMealTO>> paxMealMap = new HashMap<Integer, Collection<PaxMealTO>>();
		Collection<Integer> colPaxMealIds = new ArrayList<Integer>();
		Map<Integer, List<PaxMealTO>> tempAddMealShelf = new HashMap<Integer, List<PaxMealTO>>();

		for (PaxMealTO paxMealTO : colMealMap) {
			colPaxMealIds.add(paxMealTO.getMealId());
			if (!tempAddMealShelf.containsKey(paxMealTO.getPnrPaxId())) {
				tempAddMealShelf.put(paxMealTO.getPnrPaxId(), new ArrayList<PaxMealTO>());
			}
			tempAddMealShelf.get(paxMealTO.getPnrPaxId()).add(paxMealTO);
		}

		for (ReservationPax resPax : resPaxs) {

			List<PaxMealTO> paxMealTOs = tempAddMealShelf.get(resPax.getPnrPaxId());
			if (paxMealTOs != null) {
				for (PaxMealTO paxMealTO : paxMealTOs) {
					if (paxMealTO != null) {
						paxMealTO.setPaxName(resPax.getFirstName() + " " + resPax.getLastName());
						Map<Integer, String> mapMealNames = fillMealNames(colPaxMealIds);
						if (mapMealNames != null) {
							paxMealTO.setMealCode(mapMealNames.get(paxMealTO.getMealId()));
						}
						if (!paxMealMap.containsKey(resPax.getPnrPaxId())) {
							paxMealMap.put(resPax.getPnrPaxId(), new ArrayList<PaxMealTO>());
						}
						paxMealMap.get(resPax.getPnrPaxId()).add(paxMealTO);
					}
				}
			}
		} // end for

		return paxMealMap;
	}

	private static Map<Integer, String> fillMealNames(Collection<Integer> colMealIds) {
		return getMealCodesForAudit(colMealIds);
	}

	private static Map<Integer, String> getMealCodesForAudit(Collection<Integer> flightMealIds) {
		if (flightMealIds != null && flightMealIds.size() > 0) {
			Map<Integer, String> mealCodes = ReservationDAOUtils.DAOInstance.MEAL_DAO.getMealDetailNames(flightMealIds);
			return mealCodes;
		}
		return null;
	}

	private static Map<String, Collection<PaxMealTO>> createDummyMealMap(Collection<PaxMealTO> colMealTos) {
		Map<String, Collection<PaxMealTO>> mapMealMap = new HashMap<String, Collection<PaxMealTO>>();
		for (PaxMealTO paxMealTO : colMealTos) {
			String uuid = paxMealTO.getPnrPaxId().toString() + paxMealTO.getPnrSegId().toString();
			if (mapMealMap.get(uuid) == null) {
				mapMealMap.put(uuid, new ArrayList<PaxMealTO>());
			}
			mapMealMap.get(uuid).add(paxMealTO);
		}
		return mapMealMap;
	}

	private static Map<Integer, Collection<PaxMealTO>> segmentMealInfo(Collection<Collection<PaxMealTO>> colMealInfo) {
		Map<Integer, Collection<PaxMealTO>> segMealInfo = new LinkedHashMap<Integer, Collection<PaxMealTO>>();
		for (Collection<PaxMealTO> colMealTOs : colMealInfo) {
			for (PaxMealTO paxMealTO : colMealTOs) {

				Collection<PaxMealTO> colExtractedMealTos = (Collection<PaxMealTO>) segMealInfo.get(paxMealTO.getPnrSegId());
				if (colExtractedMealTos != null) {
					colExtractedMealTos.add(paxMealTO);
				} else {
					Collection<PaxMealTO> colSegMeal = new ArrayList<PaxMealTO>();
					colSegMeal.add(paxMealTO);
					segMealInfo.put(paxMealTO.getPnrSegId(), colSegMeal);
				}
			}
		}

		return segMealInfo;
	}

	private static Map<Integer, String> compileSegmentInfo(Collection<ReservationSegmentDTO> colResSeg,
			Collection<Collection<PaxMealTO>> mealMap) {
		if (colResSeg != null) {
			Map<Integer, String> mapSegmentInfo = new HashMap<Integer, String>();
			for (ReservationSegmentDTO reservationSegmentDTO : colResSeg) {
				mapSegmentInfo.put(reservationSegmentDTO.getPnrSegId(), reservationSegmentDTO.getSegmentCode());
			}
			return mapSegmentInfo;
		} else {
			Collection<Integer> colPnrSegIds = new ArrayList<Integer>();
			for (Collection<PaxMealTO> colMealTOs : mealMap)
				for (PaxMealTO paxMealTO : colMealTOs) {
					colPnrSegIds.add(paxMealTO.getPnrSegId());
				}
			return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnrSegmentCodes(colPnrSegIds);

		}
	}

}
