/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for capture on hold passengers
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="captureOnholdPassengers"
 */
public class CaptureOnholdPassengers extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CaptureOnholdPassengers.class);

	/**
	 * Execute method of the CaptureOnholdPassengers command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting the pnr number
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);

		// Validating the parameters
		this.validateParams(pnr, reservation);

		// Capture on hold passengers
		Collection<Integer> pnrPaxIds = new HashSet<Integer>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (ReservationInternalConstants.ReservationPaxStatus.ON_HOLD.equals(reservationPax.getStatus())) {
				pnrPaxIds.add(reservationPax.getPnrPaxId());
			}
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		DefaultServiceResponse output = new DefaultServiceResponse(true);

		response.addResponceParam(CommandParamNames.PNR_PAX_IDS, pnrPaxIds);
		output.addResponceParam(CommandParamNames.CONFIRM_PNR, pnr);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		if (this.getParameter(CommandParamNames.SPLITTED_PAX_IDS) != null) {
			response.addResponceParam(CommandParamNames.SPLITTED_PAX_IDS, this.getParameter(CommandParamNames.SPLITTED_PAX_IDS));
		}

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param reservation
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Reservation reservation) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || reservation == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");

	}
}
