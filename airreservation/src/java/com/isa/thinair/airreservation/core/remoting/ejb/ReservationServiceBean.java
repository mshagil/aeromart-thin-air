/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 

 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.remoting.ejb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.ReservationPaxSegmentPaymentTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.dto.UserNoteHistoryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaSearchTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleSearchTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.assembler.PreferenceAnalyzer;
import com.isa.thinair.airreservation.api.dto.AncillaryAssembler;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.ConnectionPaxInfoTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.EffectedFlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.FlightConnectionDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.api.dto.FlightReservationSummaryDTO;
import com.isa.thinair.airreservation.api.dto.GdsReservationAttributesDto;
import com.isa.thinair.airreservation.api.dto.IbeExitUserDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditUsageDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldBookingInfoDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.OnholdReservatoinSearchDTO;
import com.isa.thinair.airreservation.api.dto.OtherAirlineSegmentTO;
import com.isa.thinair.airreservation.api.dto.PFSMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.PassengerCheckinDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PfsLogDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.dto.RecieptLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.ResContactInfoSearchDTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.ResSegmentEticketInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.ReservationAlertDTO;
import com.isa.thinair.airreservation.api.dto.ReservationBasicsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.TemporyPaymentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.WLConfirmationRequestDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingReqRoutesTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestHistoryTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupPaymentNotificationDTO;
import com.isa.thinair.airreservation.api.dto.aig.INSChargeTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferAssembler;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.AutomaticCheckinAssembler;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.IPassengerAutomaticCheckin;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.baggage.IPassengerBaggage;
import com.isa.thinair.airreservation.api.dto.baggage.PassengerBaggageAssembler;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateRequestDTO;
import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateResponseDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.eticket.PaxEticketTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckResponseDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryDTO;
import com.isa.thinair.airreservation.api.dto.meal.IPassengerMeal;
import com.isa.thinair.airreservation.api.dto.meal.PassengerMealAssembler;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.airreservation.api.dto.pfs.CSPaxBookingCount;
import com.isa.thinair.airreservation.api.dto.pfs.PfsEntryDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PfsXmlDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPassengerDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVResInfoDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVSegmentDTO;
import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakInsuredTraveler;
import com.isa.thinair.airreservation.api.dto.revacc.AdjustCreditTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.seatmap.IPassengerSeating;
import com.isa.thinair.airreservation.api.dto.seatmap.PassengerSeatingAssembler;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.dto.ssr.CommonMedicalSsrParamDTO;
import com.isa.thinair.airreservation.api.model.AlertAction;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.model.BlacklistPAXRule;
import com.isa.thinair.airreservation.api.model.BlacklistReservation;
import com.isa.thinair.airreservation.api.model.CodeSharePfs;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.GroupBookingReqRoutes;
import com.isa.thinair.airreservation.api.model.GroupBookingRequest;
import com.isa.thinair.airreservation.api.model.GroupBookingRequestHistory;
import com.isa.thinair.airreservation.api.model.GroupRequestStation;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.IbeUserExitDetails;
import com.isa.thinair.airreservation.api.model.LmsBlockedCredit;
import com.isa.thinair.airreservation.api.model.NSRefundReservation;
import com.isa.thinair.airreservation.api.model.OfficersMobileNumbers;
import com.isa.thinair.airreservation.api.model.OnholdReleaseTime;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.airreservation.api.model.ReprotectedPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotificationEvent;
import com.isa.thinair.airreservation.api.model.ReservationSegmentTransfer;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.model.UserStation;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.AllocateEnum;
import com.isa.thinair.airreservation.api.utils.AutoCancellationUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.BlacklistPAXUtil;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceSalesType;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PfsStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.TempPaymentInconsistentStatusHistoryTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils.TEMPLATE_TYPES;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airreservation.core.activators.Pop3Client;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils.DAOInstance;
import com.isa.thinair.airreservation.core.bl.GroupBooking.GroupBookingRequestBL;
import com.isa.thinair.airreservation.core.bl.airporttransfer.AirportTransferBL;
import com.isa.thinair.airreservation.core.bl.commission.AddSegmentCS;
import com.isa.thinair.airreservation.core.bl.commission.CommissionStrategy;
import com.isa.thinair.airreservation.core.bl.commission.CreateResCS;
import com.isa.thinair.airreservation.core.bl.commission.ModifyOndCS;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.AirportTransferAdaptor;
import com.isa.thinair.airreservation.core.bl.common.AutoCancellationBO;
import com.isa.thinair.airreservation.core.bl.common.AutomaticCheckinAdaptor;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.BlacklistedPAXCheckBL;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.FlightManifestBL;
import com.isa.thinair.airreservation.core.bl.common.HistoryBL;
import com.isa.thinair.airreservation.core.bl.common.InsuranceHelper;
import com.isa.thinair.airreservation.core.bl.common.InterlinedTransferBL;
import com.isa.thinair.airreservation.core.bl.common.ItineraryBL;
import com.isa.thinair.airreservation.core.bl.common.LccBL;
import com.isa.thinair.airreservation.core.bl.common.MealAdapter;
import com.isa.thinair.airreservation.core.bl.common.OfficersMobNumsAdaptor;
import com.isa.thinair.airreservation.core.bl.common.OnholdReleaseTimeAdaptor;
import com.isa.thinair.airreservation.core.bl.common.PNRBalanceDueChargeComposer;
import com.isa.thinair.airreservation.core.bl.common.PNRCurrentChargeMetaComposer;
import com.isa.thinair.airreservation.core.bl.common.PaymentGatewayBO;
import com.isa.thinair.airreservation.core.bl.common.PnrZuluProxy;
import com.isa.thinair.airreservation.core.bl.common.RecieptBL;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.SearchReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.SeatMapAdapter;
import com.isa.thinair.airreservation.core.bl.common.TaxInvoiceBL;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.emirates.PNLDTOParser;
import com.isa.thinair.airreservation.core.bl.emirates.XAPNLFormatUtils;
import com.isa.thinair.airreservation.core.bl.pfs.PFSDTOParser;
import com.isa.thinair.airreservation.core.bl.pfs.PFSFormatUtils;
import com.isa.thinair.airreservation.core.bl.pfs.PfsMailError;
import com.isa.thinair.airreservation.core.bl.pnr.PNRNumberGeneratorProxy;
import com.isa.thinair.airreservation.core.bl.pnr.ReconcileReservationServices;
import com.isa.thinair.airreservation.core.bl.pnr.RecordAuditViaMDB;
import com.isa.thinair.airreservation.core.bl.pnrgov.PNRGOVPublishBL;
import com.isa.thinair.airreservation.core.bl.promotion.PromotionBL;
import com.isa.thinair.airreservation.core.bl.reminder.AncillaryReminderSenderBO;
import com.isa.thinair.airreservation.core.bl.reminder.IBEOnholdEmailNotifierBL;
import com.isa.thinair.airreservation.core.bl.reminder.IBEOnholdPaymentReminderBL;
import com.isa.thinair.airreservation.core.bl.reminder.InsuranceDataPublisherBL;
import com.isa.thinair.airreservation.core.bl.reminder.OnlineCheckInReminderSenderBL;
import com.isa.thinair.airreservation.core.bl.reminder.ReservationModifyEmailNotifierBL;
import com.isa.thinair.airreservation.core.bl.revacc.ClearIssuedUnConsumedLMSCreditPayments;
import com.isa.thinair.airreservation.core.bl.revacc.ClearLMSBlockedCreditPayments;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.migrator.TnxGranularityMigrator;
import com.isa.thinair.airreservation.core.bl.segment.ChargeBO;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.ssr.SSRBL;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.OfficersMobileNumbersDAO;
import com.isa.thinair.airreservation.core.persistence.dao.OnholdReleaseTimeDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PaxFinalSalesDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.service.bd.ReservationServiceBeanLocal;
import com.isa.thinair.airreservation.core.service.bd.ReservationServiceBeanRemote;
import com.isa.thinair.airreservation.core.util.IbeExitUserDetailsUtil;
import com.isa.thinair.airreservation.core.util.PFSUtil;
import com.isa.thinair.airreservation.core.util.SSRCommonUtil;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.FlightSegNotificationEvent;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.auditor.core.bl.AuditorBL;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.dto.ConfigOnholdTimeLimitsSearchDTO;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.OfficerMobNumsConfigsSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.typea.common.Coupon;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.paymentbroker.api.dto.CreditCardPaymentStatusDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.ReprotectedPaxSearchrequest;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

/**
 * Session bean to provide Reservation Service related functionalities changeSegments
 * 
 * @author Nilindra Fernando
 * 
 * @since 1.0
 */
@Stateless
@RemoteBinding(jndiBinding = "ReservationService.remote")
@LocalBinding(jndiBinding = "ReservationService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ReservationServiceBean extends PlatformBaseSessionBean
		implements ReservationServiceBeanRemote, ReservationServiceBeanLocal {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReservationServiceBean.class);

	private static String PNRSEGID_SEPERATOR = "$";

	/**
	 * Returns the reservation
	 * 
	 * @param pnrModesDTO
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			Reservation reservation;

			if (pnrModesDTO.isLoadLocalTimes()) {
				// Get the caller credentials
				reservation = PnrZuluProxy.getReservationWithLocalTimes(pnrModesDTO, credentialsDTO);
			} else {
				reservation = ReservationProxy.getReservation(pnrModesDTO);
			}

			if (pnrModesDTO.isRecordAudit()) {
				ReservationAudit reservationAudit = new ReservationAudit();
				reservationAudit.setModificationType(AuditTemplateEnum.VIEWED_RESERVATION.getCode());

				// Setting the ip address
				if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ViewedReservation.IP_ADDDRESS,
							credentialsDTO.getTrackInfoDTO().getIpAddress());
				}

				// Setting the origin carrier code
				if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ViewedReservation.ORIGIN_CARRIER,
							credentialsDTO.getTrackInfoDTO().getCarrierCode());
				}

				// Record the modification
				final String usetNotes = pnrModesDTO.getUserNote() != null ? pnrModesDTO.getUserNote() : null;
				ReservationBO.recordModification(pnrModesDTO.getPnr(), reservationAudit, usetNotes, credentialsDTO);
			}

			return reservation;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Retrive the Candidate parent for given external locator
	 */
	@Override
	public Reservation getCandidateParentReservationFromExtLocator(String externalLocator) throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getCandidateParentReservationByExtRecordLocator(externalLocator);
	}

	/**
	 * Blocks seats
	 * 
	 * @param flightBookingDTO
	 * @return block seat ids
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<TempSegBcAlloc> blockSeats(Collection<OndFareDTO> colOndFare, Collection<Integer> flightAmSeatIds)
			throws ModuleException {
		try {
			return ReservationBO.blockSeats(colOndFare, flightAmSeatIds);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::blockSeats ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::blockSeats ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Release block seats
	 * 
	 * @param colBlockIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void releaseBlockedSeats(Collection<TempSegBcAlloc> colBlockIds) throws ModuleException {
		try {
			ReservationBO.releaseBlockedSeats(colBlockIds);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::releaseBlockedSeats ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::releaseBlockedSeats ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void releaseBlockedSeats(Collection<TempSegBcAlloc> colBlockIds, boolean isFailSilently) throws ModuleException {
		try {
			ReservationModuleUtils.getFlightInventoryResBD().releaseBlockedSeats(colBlockIds, isFailSilently);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::releaseBlockedSeats ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::releaseBlockedSeats ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return available flights with all fares (For WEB USERS) All flights - fare exist Selected flight - fare exist
	 * 
	 * @param availableFlightSearchDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AvailableFlightDTO getAvailableFlightsWithAllFares(AvailableFlightSearchDTO availableFlightSearchDTO,
			TrackInfoDTO trackInfo) throws ModuleException {
		try {
			return ReservationBO.getAvailableFlightsWithAllFares(availableFlightSearchDTO, getUserPrincipal(), trackInfo);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getAvailableFlightsWithAllFares ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return Quote fare for a selected flight
	 * 
	 * @param availableFlightSearchDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SelectedFlightDTO getFareQuote(AvailableFlightSearchDTO availableFlightSearchDTO, TrackInfoDTO trackInfo)
			throws ModuleException {
		try {
			return ReservationBO.getFareQuote(availableFlightSearchDTO, getUserPrincipal(), trackInfo, false);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFareQuote ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public SelectedFlightDTO getFareQuoteTnx(AvailableFlightSearchDTO availableFlightSearchDTO, TrackInfoDTO trackInfo)
			throws ModuleException {
		try {
			return ReservationBO.getFareQuote(availableFlightSearchDTO, getUserPrincipal(), trackInfo, true);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFareQuote ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<OndFareDTO> recreateFareSegCharges(OndRebuildCriteria criteria) throws ModuleException {
		try {
			return ReservationBO.recreateFareSegCharges(criteria);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::recreateFareSegCharges ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean checkInventoryAvailability(OndRebuildCriteria criteria) throws ModuleException {
		try {
			return ReservationBO.checkInventoryAvailability(criteria);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::recreateFareSegCharges ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return reservation history information (FAST LANE READER)
	 * 
	 * @param pnr
	 * @return Collection of ReservationHistoryDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationModificationDTO> getPnrHistory(String pnr) throws ModuleException {
		try {
			// Get the caller credentials
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			return PnrZuluProxy.getReservationModificationsWithLocalTimes(pnr, false, credentialsDTO, false);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrHistory ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Search Reservations (FAST LANE READER)
	 * 
	 * @param reservationSearchDTO
	 * @return collection of ReservationDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationDTO> getReservations(ReservationSearchDTO reservationSearchDTO) throws ModuleException {
		try {
			return SearchReservationProxy.getReservations(reservationSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservations ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getPnrByExtRecordLocator(String extRLoc, String creationDate, boolean loadFares) throws ModuleException {
		try {
			return SearchReservationProxy.getPnrByExtRecordLocator(extRLoc, creationDate, loadFares);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservations ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return Early reservations (FAST LANE READER)
	 * 
	 * @param customerId
	 * @param date
	 * @param colReservationStates
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationDTO> getEarlyReservations(int customerId, Date date, Collection<String> colReservationStates, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		try {
			return SearchReservationProxy.getEarlyReservations(customerId, date, colReservationStates, trackInfoDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getEarlyReservations ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return After reservations
	 * 
	 * @param customerId
	 * @param date
	 * @param colReservationStates
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationDTO> getAfterReservations(int customerId, Date date, Collection<String> colReservationStates, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		try {
			return SearchReservationProxy.getAfterReservations(customerId, date, colReservationStates, trackInfoDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getAfterReservations ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public List<String> getPnrList(int customerId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getPnrList(customerId);
		} catch (CommonsDataAccessException ex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrList ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		}
	}

	/**
	 * Search Passenger Sum Credit (FAST LANE READER)
	 * 
	 * @param reservationSearchDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationPaxDTO> getPnrPassengerSumCredit(ReservationSearchDTO reservationSearchDTO)
			throws ModuleException {
		try {
			return SearchReservationProxy.getPnrPassengerSumCredit(reservationSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrPassengerSumCredit ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Extend on hold reseration
	 * 
	 * @param pnr
	 * @param minutes
	 * @param version
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce extendOnholdReservation(String pnr, int minutes, long version, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);

			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			// Check reservation version compatibility
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);

			// Check restricted segment constraint
			ValidationUtils.checkRestrictedSegmentConstraints(reservation, null,
					"airreservations.cancellation.restrictedSegPayShouldExist");

			Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
			ReservationPax reservationPax;
			Date releaseTimeStamp = null;

			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();

				releaseTimeStamp = reservationPax.getZuluReleaseTimeStamp();

				// If any worst case the release time stamp is empty
				if (releaseTimeStamp == null) {
					releaseTimeStamp = BeanUtils.extendByMinutes(new Date(), minutes);
				} else {
					releaseTimeStamp = BeanUtils.extendByMinutes(releaseTimeStamp, minutes);
				}

				reservationPax.setZuluReleaseTimeStamp(releaseTimeStamp);
			}

			// Saving the reservation
			ReservationProxy.saveReservation(reservation);

			if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
					&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null
					&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
				// send typeB message
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.EXTEND_ONHOLD.getCode());
				typeBRequestDTO.setReleaseTimeStamp(releaseTimeStamp);
				TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
			}

			Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
			if (csOCCarrirs != null) {
				// send codeshare typeB message
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.EXTEND_ONHOLD.getCode());
				typeBRequestDTO.setReleaseTimeStamp(releaseTimeStamp);
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
			}

			// Composing the audit
			ReservationAudit reservationAudit = new ReservationAudit();
			reservationAudit.setModificationType(AuditTemplateEnum.EXTENDED_ONHOLD.getCode());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExtendedOnhold.NEW_DURATION,
					Integer.toString(minutes) + ReservationTemplateUtils.MINUTES);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExtendedOnhold.NEW_RELEASE_TIME,
					BeanUtils.parseDateFormat(releaseTimeStamp, "E, dd MMM yyyy HH:mm:ss") + ReservationTemplateUtils.ZULU_TIME);

			// Setting the ip address
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExtendedOnhold.IP_ADDDRESS,
						credentialsDTO.getTrackInfoDTO().getIpAddress()); // AARESAA-811
			}

			// Setting the origin carrier code
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExtendedOnhold.ORIGIN_CARRIER,
						credentialsDTO.getTrackInfoDTO().getCarrierCode());
			}

			// Record the modification
			ReservationBO.recordModification(pnr, reservationAudit, null, credentialsDTO);
			DefaultServiceResponse response = new DefaultServiceResponse(true);
			return response;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::extendOnholdReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Splits the reservation Removes the paxids passed from pnr passed and add them to a new reservation while
	 * recording a transaction log
	 * 
	 * @param pnr
	 * @param paxIds
	 * @param version
	 * @param newOriginatorPNR
	 * @param newCarrierPNR
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce splitReservation(String pnr, Collection<Integer> paxIds, long version, String newOriginatorPNR,
			String newCarrierPNR, TrackInfoDTO trackInfoDTO, String userNotes, String extRecordLocator) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.SPLIT_RESERVATION_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.NEW_ORIGINATOR_PNR, newOriginatorPNR);
			command.setParameter(CommandParamNames.NEW_CARRIER_PNR, newCarrierPNR);
			command.setParameter(CommandParamNames.PNR_PAX_IDS, paxIds);
			command.setParameter(CommandParamNames.IS_PAX_SPLIT, new Boolean(true));
			command.setParameter(CommandParamNames.TRIGGER_ISOLATED_INFANT_MOVE, new Boolean(false));
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.USER_NOTES, userNotes);
			command.setParameter(CommandParamNames.EXT_RECORD_LOCATOR, extRecordLocator);
			command.setParameter(CommandParamNames.GDS_NOTIFY_ACTION, GDSInternalCodes.GDSNotifyAction.SPLIT_PAX.getCode());

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::splitReservation ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::splitReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Split reservation for dummy bookings
	 * 
	 * @param oldReservation
	 * @param newPnr
	 * @param paxIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce splitDummyReservation(Reservation oldReservation, String newPnr, Collection<Integer> paxIds,
			boolean isSplitForRemovePax) throws ModuleException {
		try {

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.SPLIT_DUMMY_RESERVATION);

			command.setParameter(CommandParamNames.NEW_ORIGINATOR_PNR, newPnr);
			command.setParameter(CommandParamNames.PNR_PAX_IDS, paxIds);
			command.setParameter(CommandParamNames.RESERVATION, oldReservation);
			command.setParameter(CommandParamNames.IS_PAX_CANCEL, isSplitForRemovePax);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::splitReservation ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::splitReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update the reservation (To update the contact and the passenger information)
	 * 
	 * @param reservation
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updateReservation(Reservation reservation, TrackInfoDTO trackInfoDTO, boolean dupNameCheck,
			boolean applyNameChangeCharge, Collection<String> allowedOperations, String reasonForAllowBlPax)
			throws ModuleException {
		try {
			// Retrieve called credentials information
			if (trackInfoDTO == null) {
				trackInfoDTO = new TrackInfoDTO();
				trackInfoDTO.setIpAddress(getUserPrincipal().getIpAddress());
				trackInfoDTO.setCarrierCode(getUserPrincipal().getDefaultCarrierCode());
			}
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.UPDATE_RESERVATION_MACRO);
			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.CHECK_DUPLICATE_NAMES, dupNameCheck);
			command.setParameter(CommandParamNames.APPLY_NAME_CHANGE_CHARGE, applyNameChangeCharge);
			command.setParameter(CommandParamNames.IS_DUMMY_RESERVATION, Boolean.valueOf(false));
			command.setParameter(CommandParamNames.IS_FROM_NAME_CHANGE, Boolean.valueOf(true));
			command.setParameter(CommandParamNames.USER_NOTES, reservation.getUserNote());
			command.setParameter(CommandParamNames.USER_NOTE_TYPE, reservation.getUserNoteType());
			command.setParameter(CommandParamNames.IS_TBA_NAME_CHANGE, Boolean.valueOf(true));
			command.setParameter(CommandParamNames.ALLOWED_SUB_OPERATIONS, allowedOperations);
			command.setParameter(CommandParamNames.PNR, reservation.getPnr());
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY,
					getReservationBeforeUpdate(reservation.getPnr()));
			command.setParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, reasonForAllowBlPax);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateReservation ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update dummy reservation pax info
	 * 
	 * @param reservation
	 * @param trackInfoDTO
	 * @param dupNameCheck
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updateDummyReservation(Reservation reservation, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			// Retrieve called credentials information
			if (trackInfoDTO == null) {
				trackInfoDTO = new TrackInfoDTO();
				trackInfoDTO.setIpAddress(getUserPrincipal().getIpAddress());
				trackInfoDTO.setCarrierCode(getUserPrincipal().getDefaultCarrierCode());
			}
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.UPDATE_RESERVATION);
			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.CHECK_DUPLICATE_NAMES, Boolean.valueOf(false));
			command.setParameter(CommandParamNames.IS_DUMMY_RESERVATION, Boolean.valueOf(true));
			command.setParameter(CommandParamNames.IS_FROM_NAME_CHANGE, Boolean.valueOf(true));
			command.setParameter(CommandParamNames.IS_TBA_NAME_CHANGE, Boolean.valueOf(false));
			command.setParameter(CommandParamNames.APPLY_NAME_CHANGE_CHARGE, Boolean.valueOf(false));

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateDummyReservation ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateDummyReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Cancells an existing reservation and releases the seat allocated for it. Pax credit is recorded after deducting a
	 * cancellation charge.
	 * 
	 * @param pnr
	 * @param version
	 * @param trackInfoDTO
	 * @param customAdultCancelCharge
	 * @param customChildCancelCharge
	 * @param customInfantCancelCharge
	 * @return
	 * @throws ModuleException
	 */

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce cancelReservation(String pnr, CustomChargesTO customChargesTO, long version, boolean releaseOnHold,
			boolean cnxFlownSegs, boolean utilizeAllRefundables, TrackInfoDTO trackInfoDTO, boolean isVoidReservation,
			String userNotes, int gdsNotifyAction, Map<Integer, List<ExternalChgDTO>> paxCharges) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			// call the cancel reservation macro
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CANCEL_RESERVATION_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.CUSTOM_CHARGES, customChargesTO);
			command.setParameter(CommandParamNames.IS_PAX_CANCEL, Boolean.TRUE);
			command.setParameter(CommandParamNames.IS_CANCEL_CHG_OPERATION, Boolean.TRUE);
			command.setParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION, Boolean.FALSE);
			command.setParameter(CommandParamNames.IS_CNX_FLOWN_SEGS, Boolean.valueOf(cnxFlownSegs));
			command.setParameter(CommandParamNames.UTILIZED_REFUNDABLES, Boolean.valueOf(utilizeAllRefundables));
			command.setParameter(CommandParamNames.VERSION, Long.valueOf(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.IS_VOID_OPERATION, isVoidReservation);
			command.setParameter(CommandParamNames.USER_NOTES, userNotes);
			command.setParameter(CommandParamNames.GDS_NOTIFY_ACTION, Integer.valueOf(gdsNotifyAction));
			command.setParameter(CommandParamNames.IS_OHD_RELEASE, releaseOnHold);
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));
			command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
			command.setParameter(CommandParamNames.PAX_EXTERNAL_CHARGE_MAP, paxCharges);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::cancelReservation ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::cancelReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Remove passengers
	 * 
	 * Business Rules: (1)If the passenger has not paid anything, delete his record (2)If he has paid some amount, split
	 * the reservation and add him to the new reservation (3)Cancel all segments of the new reservation (take
	 * cancellation charge for each segment)
	 * 
	 * @param pnr
	 * @param pnrPaxIds
	 * @param customChargesTO
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce removePassengers(String pnr, String newPnr, Collection<Integer> pnrPaxIds,
			CustomChargesTO customChargesTO, long version, TrackInfoDTO trackInfoDTO, String userNotes,
			Map<Integer, List<ExternalChgDTO>> paxExternalCharges) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.REMOVE_PASSENGER_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.NEW_ORIGINATOR_PNR, newPnr);
			command.setParameter(CommandParamNames.NEW_CARRIER_PNR, newPnr);
			command.setParameter(CommandParamNames.PNR_PAX_IDS, pnrPaxIds);
			command.setParameter(CommandParamNames.IS_PAX_CANCEL, new Boolean(true));
			command.setParameter(CommandParamNames.IS_REMOVE_PAX, new Boolean(true));
			command.setParameter(CommandParamNames.IS_CANCEL_CHG_OPERATION, new Boolean(true));
			command.setParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION, new Boolean(false));
			command.setParameter(CommandParamNames.IS_CNX_FLOWN_SEGS, new Boolean(false));
			command.setParameter(CommandParamNames.CUSTOM_CHARGES, customChargesTO);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.IS_PAX_SPLIT, new Boolean(true));
			command.setParameter(CommandParamNames.TRIGGER_ISOLATED_INFANT_MOVE, new Boolean(true));
			command.setParameter(CommandParamNames.UTILIZED_REFUNDABLES, new Boolean(false));
			command.setParameter(CommandParamNames.IS_VOID_OPERATION, false);
			command.setParameter(CommandParamNames.USER_NOTES, userNotes);
			command.setParameter(CommandParamNames.GDS_NOTIFY_ACTION,
					Integer.valueOf(GDSInternalCodes.GDSNotifyAction.REMOVE_PAX.getCode()));
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));
			command.setParameter(CommandParamNames.PAX_EXTERNAL_CHARGE_MAP, paxExternalCharges);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::removePassengers ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::removePassengers ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Expire On hold reservations
	 * 
	 * @param toDate
	 * @param customAdultCancelCharge
	 * @param customChildCancelCharge
	 * @param customInfantCancelCharge
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void expireOnholdReservations(Date toDate, BigDecimal customAdultCancelCharge, BigDecimal customChildCancelCharge,
			BigDecimal customInfantCancelCharge) throws ModuleException {
		log.info(" +===================================+ ");
		log.info("  Started Expire On hold Reservations ");
		log.info(" +===================================+ ");

		ReservationBO.expireOnholdReservations(toDate, customAdultCancelCharge, customChildCancelCharge,
				customInfantCancelCharge);

		log.info(" +===================================+  ");
		log.info(" Expire On hold Reservations Completed ");
		log.info(" +===================================+  ");
	}

	/**
	 * Cancel Segments
	 * 
	 * @param fltSegments
	 * @param customCharge
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Object[] locateOndsAndCancel(Map fltSegments, BigDecimal customCharge, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Get the confirmed reservation ond(s)
		Map mapPnrOnd = reservationSegmentDAO.getConfirmedPnrOnds(fltSegments.keySet());

		Iterator itPnr = mapPnrOnd.keySet().iterator();
		Iterator itGrpSeg;
		Map mapGrpSeg;
		String pnr;
		Integer grpId;
		EffectedFlightSegmentDTO effectedFlightSegmentDTO;
		Collection colEffectedFlightSegmentDTO = new ArrayList();
		Collection colErrorEffectedFlightSegmentDTO = new ArrayList();

		while (itPnr.hasNext()) {
			pnr = (String) itPnr.next();
			mapGrpSeg = (Map) mapPnrOnd.get(pnr);

			itGrpSeg = mapGrpSeg.keySet().iterator();

			while (itGrpSeg.hasNext()) {
				grpId = (Integer) itGrpSeg.next();
				effectedFlightSegmentDTO = (EffectedFlightSegmentDTO) mapGrpSeg.get(grpId);

				// Capture the segment code from the hash map
				Map segmentDetailsMap = (Map) fltSegments.get(effectedFlightSegmentDTO.getFlightSegmentId());
				effectedFlightSegmentDTO
						.setFlightSegmentCode((String) segmentDetailsMap.get(AlertTemplateEnum.TemplateParams.SEGMENT));

				// Add the effected flight segment
				colEffectedFlightSegmentDTO.add(effectedFlightSegmentDTO);
			}
		}

		return new Object[] { colEffectedFlightSegmentDTO, colErrorEffectedFlightSegmentDTO };
	}

	/**
	 * Cancel Segment
	 * 
	 * @param pnr
	 * @param segmentIds
	 * @param version
	 * @param cnxFlownSegs
	 * @param trackInfoDTO
	 * @param customAdultCancelCharge
	 * @param customChildCancelCharge
	 * @param customInfantCancelCharge
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce cancelSegments(String pnr, Collection<Integer> requestedSegmentIds, CustomChargesTO customChargesTO,
			long version, boolean cnxFlownSegs, TrackInfoDTO trackInfoDTO, boolean hasExternalCarrierPayments, String userNotes,
			boolean applyCancelCharge) throws ModuleException {

		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CANCEL_SEGMENT_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PNR_SEGMENT_IDS, requestedSegmentIds);
			command.setParameter(CommandParamNames.CUSTOM_CHARGES, customChargesTO);
			command.setParameter(CommandParamNames.IS_PAX_CANCEL, new Boolean(true));
			command.setParameter(CommandParamNames.IS_CANCEL_CHG_OPERATION, new Boolean(true));
			command.setParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION, new Boolean(false));
			command.setParameter(CommandParamNames.APPLY_CANCEL_CHARGE, new Boolean(applyCancelCharge));
			command.setParameter(CommandParamNames.APPLY_MODIFY_CHARGE, new Boolean(false));
			command.setParameter(CommandParamNames.HAS_EXT_CARRIER_PAYMENTS, new Boolean(hasExternalCarrierPayments));
			command.setParameter(CommandParamNames.IS_CNX_FLOWN_SEGS, new Boolean(cnxFlownSegs));
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.ORDERED_NEW_FLIGHT_SEGMENT_IDS, null);
			command.setParameter(CommandParamNames.USER_NOTES, userNotes);
			command.setParameter(CommandParamNames.IS_VOID_OPERATION, false);
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));
			command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::cancelSegments ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::cancelSegments ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Cancel Segment
	 * 
	 * @param pnr
	 * @param segmentIds
	 * @param version
	 * @param cnxFlownSegs
	 * @param trackInfoDTO
	 * @param customAdultCancelCharge
	 * @param customChildCancelCharge
	 * @param customInfantCancelCharge
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce cancelSegmentsWithRequiresNew(String pnr, Collection<Integer> requestedSegmentIds,
			CustomChargesTO customChargesTO, long version, boolean cnxFlownSegs, TrackInfoDTO trackInfoDTO,
			boolean hasExternalCarrierPayments, String userNotes, boolean applyCancelCharge) throws ModuleException {

		return cancelSegments(pnr, requestedSegmentIds, customChargesTO, version, cnxFlownSegs, trackInfoDTO,
				hasExternalCarrierPayments, userNotes, applyCancelCharge);
	}

	/**
	 * Cancel segments applying modification charge
	 * 
	 * @param pnr
	 * @param requestedSegmentIds
	 * @param version
	 * @param cnxFlownSegs
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce cancelSegmentsApplyingModCharge(String pnr, Collection<Integer> requestedSegmentIds,
			Collection<Integer> newFltSegIds, CustomChargesTO customChargesTO, long version, boolean cnxFlownSegs,
			TrackInfoDTO trackInfoDTO, String userNotes, boolean hasExternalCarrierPayments) throws ModuleException {

		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			if (customChargesTO == null) {
				customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
			}
			// Creates the custom charges TO

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CANCEL_SEGMENT_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PNR_SEGMENT_IDS, requestedSegmentIds);
			command.setParameter(CommandParamNames.CUSTOM_CHARGES, customChargesTO);
			command.setParameter(CommandParamNames.IS_PAX_CANCEL, new Boolean(true));
			command.setParameter(CommandParamNames.IS_CANCEL_CHG_OPERATION, new Boolean(true));
			command.setParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION, new Boolean(false));
			command.setParameter(CommandParamNames.APPLY_CANCEL_CHARGE, new Boolean(false));
			command.setParameter(CommandParamNames.APPLY_MODIFY_CHARGE, new Boolean(true));
			command.setParameter(CommandParamNames.HAS_EXT_CARRIER_PAYMENTS, new Boolean(hasExternalCarrierPayments));
			command.setParameter(CommandParamNames.IS_CNX_FLOWN_SEGS, new Boolean(cnxFlownSegs));
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.USER_NOTES, userNotes);
			command.setParameter(CommandParamNames.ORDERED_NEW_FLIGHT_SEGMENT_IDS, newFltSegIds);
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::cancelSegments ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::cancelSegments ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Change Segments
	 * 
	 * @param pnr
	 * @param oldSegmentIds
	 * @param iSegment
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @param modifyCharge
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce changeSegments(String pnr, Collection<Integer> oldSegmentIds, ISegment iSegment,
			CustomChargesTO customChargesTO, Collection<TempSegBcAlloc> blockKeyIds, Integer paymentTypes, long version,
			TrackInfoDTO trackInfoDTO, Boolean isFlexiSelected, boolean enableECFraudChecking, boolean useOtherCarrierPaxCredit,
			boolean hasPreviousOtherCarrierPayments, boolean isOpenRetConfSegModification, boolean isActualPayment)
			throws ModuleException {

		String comments = "";

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		SegmentAssembler sAssembler = (SegmentAssembler) iSegment;

		// Injecting the segment credentials information
		sAssembler.injectSegmentCredentials(credentialsDTO);

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(sAssembler, pnr);

		// Load the reservation contact information
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);

		// Get the reservation service local instance

		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(sAssembler.getTnxIds(),
				contactInfo, colPaymentInfo, credentialsDTO, true, false);

		LCCClientPnrModesDTO pnrModesDto = new LCCClientPnrModesDTO();

		pnrModesDto.setPnr(pnr);
		pnrModesDto.setLoadFares(true);
		pnrModesDto.setLoadSegView(true);
		pnrModesDto.setLoadSegViewFareCategoryTypes(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDto);

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CHANGE_SEGMENT_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PNR_SEGMENT_IDS, oldSegmentIds);
			command.setParameter(CommandParamNames.IS_CANCEL_CHG_OPERATION, new Boolean(false));
			command.setParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION, new Boolean(true));
			command.setParameter(CommandParamNames.APPLY_CANCEL_CHARGE, new Boolean(false));
			command.setParameter(CommandParamNames.APPLY_MODIFY_CHARGE, new Boolean(true));
			command.setParameter(CommandParamNames.CUSTOM_CHARGES, customChargesTO);
			command.setParameter(CommandParamNames.IS_PAX_CANCEL, new Boolean(true));
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.OND_FARE_DTOS, sAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_DTO, sAssembler.getFareDiscount());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_INFO, sAssembler.getFareDiscountInfo());
			command.setParameter(CommandParamNames.AGENT_COMMISSION_DTO, sAssembler.getAgentCommissionInfo());
			command.setParameter(CommandParamNames.REMOVE_AGENT_COMMISSION, new Boolean(true));
			command.setParameter(CommandParamNames.USER_NOTES, sAssembler.getUserNote());
			command.setParameter(CommandParamNames.ENDORSEMENT_NOTES, sAssembler.getEndorsementNotes());
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, sAssembler.getSegmentObjectsMap());
			command.setParameter(CommandParamNames.RESERVATION_SEGMENTS, sAssembler.getDummySegments());
			command.setParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS, sAssembler.getExternalSegments());
			command.setParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, sAssembler.getPassengerPaymentsMap());
			command.setParameter(CommandParamNames.IS_CNX_FLOWN_SEGS, new Boolean(true));
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, new Boolean(blockKeyIds == null));
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED,
					ReservationApiUtils.isTriggerForceConfirm(paymentTypes));
			command.setParameter(CommandParamNames.PNR_PAYMENT_TYPES, paymentTypes);
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, sAssembler.getZuluReleaseTimeStamp());
			command.setParameter(CommandParamNames.ORDERED_NEW_FLIGHT_SEGMENT_IDS, sAssembler.getOrderedNewFlightSegmentIds());
			command.setParameter(CommandParamNames.LAST_MODIFICATION_TIMESTAMP, sAssembler.getLastModificationTimeStamp());
			command.setParameter(CommandParamNames.LAST_CURRENCY_CODE, sAssembler.getLastCurrencyCode());
			command.setParameter(CommandParamNames.IS_FLEXI_SELECTED, isFlexiSelected);
			command.setParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED, useOtherCarrierPaxCredit);
			command.setParameter(CommandParamNames.INSURANCE_INFO, sAssembler.getInsuranceRequests());
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, Boolean.TRUE);
			command.setParameter(CommandParamNames.HAS_EXT_CARRIER_PAYMENTS, Boolean.valueOf(hasPreviousOtherCarrierPayments));
			command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, reservation.isEnableTransactionGranularity());
			command.setParameter(CommandParamNames.IS_OPEN_RETURN_CONF_SEG_MODIFICATION,
					new Boolean(isOpenRetConfSegModification));

			if (colPaymentInfo == null || colPaymentInfo.isEmpty()
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(paymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE
							.equals(paymentTypes)) {
				if (!AppSysParamsUtil.isRakEnabled()) {
					command.setParameter(CommandParamNames.INSURANCE_HOLD, Boolean.TRUE);
				}
			}

			command.setParameter(CommandParamNames.CC_FRAUD_ENABLE, Boolean.valueOf(enableECFraudChecking));
			command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.RESERVATION_TYPE, "UPDATE_RESERVATION");
			command.setParameter(CommandParamNames.AUTO_CANCELLATION_INFO, sAssembler.getAutoCancellationInfo());
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, isActualPayment);

			if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				command.setParameter(CommandParamNames.IS_FIRST_PAYMENT, Boolean.TRUE);
			}

			if (customChargesTO.getCustomAdultCCharge() != null || customChargesTO.getCustomChildChargeTO() != null
					|| customChargesTO.getCustomInfantCCharge() != null) {
				command.setParameter(CommandParamNames.IS_CNX_MOD_FEE_CHANGE_AUDIT_NEED, true);
			}
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));

			ServiceResponce serviceResponce = command.execute();
			serviceResponce.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED,
					Boolean.valueOf(useOtherCarrierPaxCredit));

			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);
			ReservationModuleUtils.getReservationBD().sendSms(pnr, contactInfo, credentialsDTO);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::changeSegments " + ReservationApiUtils.getElementsInCollection(colTnxIds), ex);
			comments = "ModuleException::changeSegments";
			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(
					" ((o)) CommonsDataAccessException::changeSegments " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					cdaex);
			comments = "CommonsDataAccessException::changeSegments";
			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Change Segments with REQUIRES_NEW transaction
	 * 
	 * @param pnr
	 * @param oldSegmentIds
	 * @param iSegment
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @param modifyCharge
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce changeSegmentsWithRequiresNew(String pnr, Collection<Integer> oldSegmentIds, ISegment iSegment,
			CustomChargesTO customChargesTO, Collection<TempSegBcAlloc> blockKeyIds, Integer paymentTypes, long version,
			TrackInfoDTO trackInfoDTO, Boolean isFlexiSelected, boolean enableECFraudChecking, boolean useOtherCarrierPaxCredit,
			boolean hasPreviousOtherCarrierPayments, boolean isOpenRetConfSegModification, boolean isActualPayment)
			throws ModuleException {
		return changeSegments(pnr, oldSegmentIds, iSegment, customChargesTO, blockKeyIds, paymentTypes, version, trackInfoDTO,
				isFlexiSelected, enableECFraudChecking, useOtherCarrierPaxCredit, hasPreviousOtherCarrierPayments,
				isOpenRetConfSegModification, isActualPayment);
	}

	@Override
	public void intermediateRefund(String pnr, ISegment iSegment, TrackInfoDTO trackInfoDTO, String exceptionCode)
			throws ModuleException {
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);
		SegmentAssembler sAssembler = (SegmentAssembler) iSegment;
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(sAssembler, pnr);
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(sAssembler.getTnxIds(),
				contactInfo, colPaymentInfo, credentialsDTO, true, false);
		reversePayment(colPaymentInfo, trackInfoDTO, exceptionCode, colTnxIds);
	}

	/**
	 * Add segments
	 * 
	 * @param pnr
	 * @param iSegment
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @param paxSegEticket
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce addSegments(String pnr, ISegment iSegment, Collection<TempSegBcAlloc> blockKeyIds,
			Integer paymentTypes, long version, TrackInfoDTO trackInfoDTO, boolean isFlexiSelected, boolean enableECFraudChecking,
			String selectedPnrSegID, Map<Integer, List<ReservationPaxOndFlexibilityDTO>> ondFlexibilities,
			boolean useOtherCarrierPaxCredit, boolean hasExternalCarrierPayments, boolean isActualPayment)
			throws ModuleException {

		boolean isCapturePayment = true;

		String comments = "";

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		SegmentAssembler sAssembler = (SegmentAssembler) iSegment;

		// Injecting the segment credentials information
		sAssembler.injectSegmentCredentials(credentialsDTO);

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(sAssembler, pnr);

		// Load the reservation contact information
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(sAssembler.getTnxIds(),
				contactInfo, colPaymentInfo, credentialsDTO, true, false);

		LCCClientPnrModesDTO pnrModesDto = new LCCClientPnrModesDTO();
		pnrModesDto.setPnr(pnr);
		pnrModesDto.setLoadFares(true);
		pnrModesDto.setLoadSegView(true);
		pnrModesDto.setLoadSegViewFareCategoryTypes(true);

		Reservation reservation = ReservationProxy.getReservation(pnrModesDto);

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ADD_SEGMENT_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION_SEGMENTS, sAssembler.getDummySegments());
			command.setParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS, sAssembler.getExternalSegments());
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.OND_FARE_DTOS, sAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_DTO, sAssembler.getFareDiscount());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_INFO, sAssembler.getFareDiscountInfo());
			command.setParameter(CommandParamNames.AGENT_COMMISSION_DTO, sAssembler.getAgentCommissionInfo());
			command.setParameter(CommandParamNames.USER_NOTES, sAssembler.getUserNote());
			command.setParameter(CommandParamNames.ENDORSEMENT_NOTES, sAssembler.getEndorsementNotes());
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, sAssembler.getSegmentObjectsMap());
			command.setParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, sAssembler.getPassengerPaymentsMap());
			command.setParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION, new Boolean(false));
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, new Boolean(false));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED,
					ReservationApiUtils.isTriggerForceConfirm(paymentTypes));
			command.setParameter(CommandParamNames.PNR_PAYMENT_TYPES, paymentTypes);
			command.setParameter(CommandParamNames.ONHOLD_OR_FORCE_CONFIRM,
					ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(paymentTypes));
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, sAssembler.getZuluReleaseTimeStamp());
			command.setParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED, Boolean.valueOf(useOtherCarrierPaxCredit));

			command.setParameter(CommandParamNames.INSURANCE_INFO, sAssembler.getInsuranceRequests());
			// Since IS_MODIFY_CHG_APPLY is false MODIFY_AMOUNT and
			// MODIFY_SEG_TOTAL won't have any effect
			command.setParameter(CommandParamNames.PASSENGER_REVENUE_MAP, new HashMap<Integer, RevenueDTO>());
			command.setParameter(CommandParamNames.LAST_MODIFICATION_TIMESTAMP, sAssembler.getLastModificationTimeStamp());
			command.setParameter(CommandParamNames.LAST_CURRENCY_CODE, sAssembler.getLastCurrencyCode());
			command.setParameter(CommandParamNames.IS_FLEXI_SELECTED, isFlexiSelected);
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(true));
			command.setParameter(CommandParamNames.AUTO_CANCELLATION_INFO, sAssembler.getAutoCancellationInfo());

			if (colPaymentInfo == null || colPaymentInfo.isEmpty()
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(paymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE
							.equals(paymentTypes)) {
				command.setParameter(CommandParamNames.INSURANCE_HOLD, new Boolean(isCapturePayment));
			}

			command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
			command.setParameter(CommandParamNames.RESERVATION_TYPE, "UPDATE_RESERVATION");
			command.setParameter(CommandParamNames.CC_FRAUD_ENABLE, new Boolean(enableECFraudChecking));
			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP, ondFlexibilities);
			if (selectedPnrSegID != null) {
				if (selectedPnrSegID.indexOf(PNRSEGID_SEPERATOR) > 0) {
					selectedPnrSegID = selectedPnrSegID.substring(0, selectedPnrSegID.indexOf(PNRSEGID_SEPERATOR));
				}
				command.setParameter(CommandParamNames.CONNECTING_FLT_SEG_ID, Integer.parseInt(selectedPnrSegID));
			}
			command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, reservation.isEnableTransactionGranularity());
			command.setParameter(CommandParamNames.HAS_EXT_CARRIER_PAYMENTS, new Boolean(hasExternalCarrierPayments));
			command.setParameter(CommandParamNames.IS_OPEN_RETURN_CONF_SEG_MODIFICATION, new Boolean(false));
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, isActualPayment);

			ServiceResponce serviceResponce = command.execute();
			serviceResponce.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED,
					Boolean.valueOf(useOtherCarrierPaxCredit));

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);
			ReservationModuleUtils.getReservationBD().sendSms(pnr, contactInfo, credentialsDTO);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::addSegments " + ReservationApiUtils.getElementsInCollection(colTnxIds), ex);
			comments = "ModuleException::addSegments";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::addSegments " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					cdaex);
			comments = "CommonsDataAccessException::addSegments";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Adds an infant
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param paxSegEticket
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce addInfant(String pnr, IPassenger iPassenger, Collection<TempSegBcAlloc> blockKeyIds,
			Integer paymentTypes, long version, Map<Integer, Map<Integer, EticketDTO>> paxSegEticket, TrackInfoDTO trackInfoDTO,
			boolean enableECFraudChecking, boolean goshowInfant, boolean otherCarrierCreditUsed,
			AutoCancellationInfo autoCancellationInfo) throws ModuleException {

		boolean isCapturePayment = true;

		String comments = "";
		PassengerAssembler paxAssembler = (PassengerAssembler) iPassenger;

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(paxAssembler, pnr);

		// Load the reservation contact informationR
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD()
				.makeTemporyPaymentEntry(paxAssembler.getTnxIds(), contactInfo, colPaymentInfo, credentialsDTO, true, false);

		LCCClientPnrModesDTO pnrModesDto = new LCCClientPnrModesDTO();
		pnrModesDto.setPnr(pnr);
		pnrModesDto.setLoadFares(true);
		pnrModesDto.setLoadSegView(true);
		pnrModesDto.setLoadSegViewFareCategoryTypes(true);

		Reservation reservation = ReservationProxy.getReservation(pnrModesDto);

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ADD_INFANT_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);
			command.setParameter(CommandParamNames.OND_FARE_DTOS, paxAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.INFANTS, paxAssembler.getDummyPassengers());
			command.setParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, paxAssembler.getPassengerPaymentsMap());
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, new Boolean(false));
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED,
					ReservationApiUtils.isTriggerForceConfirm(paymentTypes));
			command.setParameter(CommandParamNames.PNR_PAYMENT_TYPES, paymentTypes);
			command.setParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS, paxAssembler.getPaxSegmentSSRDTOMaps());
			command.setParameter(CommandParamNames.E_TICKET_MAP, paxSegEticket);

			command.setParameter(CommandParamNames.RESERVATION_TYPE, "UPDATE_RESERVATION");
			command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
			command.setParameter(CommandParamNames.CC_FRAUD_ENABLE, new Boolean(enableECFraudChecking));
			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, isCapturePayment);
			command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, reservation.isEnableTransactionGranularity());
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, new Boolean(goshowInfant));
			command.setParameter(CommandParamNames.AUTO_CANCELLATION_INFO, autoCancellationInfo);
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, new Boolean(true));
			command.setParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED, Boolean.valueOf(otherCarrierCreditUsed));
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));

			if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				command.setParameter(CommandParamNames.IS_FIRST_PAYMENT, Boolean.TRUE);
			}

			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);
			ReservationModuleUtils.getReservationBD().sendSms(pnr, contactInfo, credentialsDTO);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::addInfant " + ReservationApiUtils.getElementsInCollection(colTnxIds), ex);
			comments = "ModuleException::addInfant";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::addInfant " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					cdaex);
			comments = "CommonsDataAccessException::addInfant";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce addInfantWithRequiresNew(String pnr, IPassenger iPassenger, Collection<TempSegBcAlloc> blockKeyIds,
			Integer paymentTypes, long version, Map<Integer, Map<Integer, EticketDTO>> paxSegEticket, TrackInfoDTO trackInfoDTO,
			boolean enableECFraudChecking, boolean goshowInfant) throws ModuleException {
		return addInfant(pnr, iPassenger, blockKeyIds, paymentTypes, version, paxSegEticket, trackInfoDTO, enableECFraudChecking,
				goshowInfant, false, null);
	}

	/**
	 * adds a infant to dummy reservation
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce addInfantForDummyReservation(Reservation reservation, IPassenger iPassenger) throws ModuleException {

		PassengerAssembler paxAssembler = (PassengerAssembler) iPassenger;
		ServiceResponce serviceResponce;

		try {

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.MAKE_INFANT_FOR_DUMMY_RESERVATION);

			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.INFANTS, paxAssembler.getDummyPassengers());

			serviceResponce = command.execute();
			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::addInfantForDummyReservation " + ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Adjust Credit for manually
	 * 
	 * @param pnr
	 * @param pnrPaxFareId
	 * @param chargeRateID
	 * @param amount
	 * @param userNotes
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce adjustCreditManual(String pnr, int pnrPaxFareId, Integer chargeRateID, BigDecimal amount,
			String userNotes, long version, Map<Integer, BigDecimal> totalPlusAdjByPaxFareId, TrackInfoDTO trackInfoDTO,
			ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS) throws ModuleException {
		return adjustCreditManual(pnr, Arrays.asList(pnrPaxFareId), chargeRateID, amount, userNotes, version,
				totalPlusAdjByPaxFareId, trackInfoDTO, serviceTaxRS, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce adjustCreditManual(String pnr, List<Integer> pnrPaxFareIds, Integer chargeRateID, BigDecimal amount,
			String userNotes, long version, Map<Integer, BigDecimal> totalPlusAdjByPaxFareId, TrackInfoDTO trackInfoDTO,
			ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS, Map<Integer, List<ExternalChgDTO>> handlingFeeByPax)
			throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			// Return the reservation
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			List<Integer> forcedConfirmedPaxSeqsBeforeAdj = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

			// Checking to see if any concurrent update exist
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);

			AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);
			ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
			for (Integer pnrPaxFareId : pnrPaxFareIds) {

				Map<Integer, BigDecimal> adjustedPassengerChargeAmount = ReservationApiUtils
						.getAdjustedAmountForPassenger(reservation, pnrPaxFareId, amount, totalPlusAdjByPaxFareId);

				if (adjustedPassengerChargeAmount != null && adjustedPassengerChargeAmount.size() > 0) {

					BigDecimal adjustedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					for (Integer paxFareId : adjustedPassengerChargeAmount.keySet()) {
						adjustedAmount = adjustedPassengerChargeAmount.get(paxFareId);

						if (adjustedAmount.doubleValue() != 0) {
							// Adjusting the credit manually
							ReservationBO.adjustCreditManual(adjustCreditBO, reservation, paxFareId, chargeRateID, adjustedAmount,
									ReservationInternalConstants.ChargeGroup.ADJ, userNotes, trackInfoDTO, credentialsDTO,
									chgTnxGen);
						}

					}

				} else {
					// Adjusting the credit manually
					ReservationBO.adjustCreditManual(adjustCreditBO, reservation, pnrPaxFareId, chargeRateID, amount,
							ReservationInternalConstants.ChargeGroup.ADJ, userNotes, trackInfoDTO, credentialsDTO, chgTnxGen);
				}

				ReservationBO.addServiceTaxAdjustmentBalance(reservation, credentialsDTO, pnrPaxFareId, userNotes, adjustCreditBO,
						serviceTaxRS, chgTnxGen);

				ReservationBO.addHandlingFeeForAdjustmentBalance(reservation, credentialsDTO, pnrPaxFareId, userNotes,
						adjustCreditBO, chgTnxGen, handlingFeeByPax);

			}
			// Save the reservation
			ReservationProxy.saveReservation(reservation);
			adjustCreditBO.callRevenueAccountingAfterReservationSave();

			reservation = ReservationProxy.getReservation(pnrModesDTO);
			List<Integer> forcedConfirmedPaxSeqsAfterAdj = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
			handleETGeneration(reservation, forcedConfirmedPaxSeqsBeforeAdj, forcedConfirmedPaxSeqsAfterAdj, credentialsDTO);

			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::adjustCreditManual ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::adjustCreditManual ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Expire Passenger credits
	 * 
	 * @param date
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void expirePassengerCredits(Date date) throws ModuleException {
		// Retrieve called credentials information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(null);

		try {
			ReservationTnxDAO reservationTnxDAO = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
			Collection<AdjustCreditTO> colAdjustCreditTO = reservationTnxDAO.getExpiredCredits(date);
			// Contains empty lists to avoid null pointer exceptions
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
			Iterator<AdjustCreditTO> itAdjustCreditTO = colAdjustCreditTO.iterator();
			while (itAdjustCreditTO.hasNext()) {
				AdjustCreditTO adjustCreditTO = itAdjustCreditTO.next();
				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ADJUST_CREDIT_MACRO);
				command.setParameter(CommandParamNames.PNR, adjustCreditTO.getPnr());
				command.setParameter(CommandParamNames.PNR_PAX_ID, Integer.toString(adjustCreditTO.getPnrPaxId()));
				command.setParameter(CommandParamNames.MODIFY_AMOUNT, adjustCreditTO.getAmount());
				command.setParameter(CommandParamNames.USER_NOTES, adjustCreditTO.getUserNotes());
				command.setParameter(CommandParamNames.ACTION_NC, ReservationTnxNominalCode.CREDIT_ACQUIRE);
				command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
				command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, reservationPaxPaymentMetaTO);
				command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY,
						adjustCreditTO.isEnableTransactionGranularity());
				command.setParameter(CommandParamNames.ORIGINAL_PAYMENT_TNX_ID, adjustCreditTO.getOriginalPaymentTnxID());
				command.execute();
			}
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::expirePassengerCredits ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::expirePassengerCredits ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update the reservation for the carried out payment
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param isForceConfirm
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updateReservationForPayment(String pnr, IPassenger iPassenger, boolean isForceConfirm,
			boolean acceptMorePaymentsThanItsCharges, long version, TrackInfoDTO trackInfoDTO, boolean enableECFraudChecking,
			boolean isPaymentForAcquireCredits, boolean isCapturePayment, boolean otherCarrierCreditUsed, boolean actualPayment,
			boolean isFirstPayment, LoyaltyPaymentInfo loyaltyPaymentInfo, boolean isGdsSale, PaxAdjAssembler paxAdjAssmbler)
			throws ModuleException {

		String comments = "";

		PassengerAssembler paxAssembler = (PassengerAssembler) iPassenger;

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(paxAssembler, pnr);

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		// Load the reservation contact information
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = new ArrayList<Integer>();
		if (isCapturePayment) {
			colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(paxAssembler.getTnxIds(), contactInfo,
					colPaymentInfo, credentialsDTO, true, false);
		}

		// To save last saved pay currency
		String lastCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		if (paxAssembler.getPassengerPaymentsMap() != null && paxAssembler.getPassengerPaymentsMap().values().size() > 0) {
			PaymentAssembler payAsm = ((PaymentAssembler) paxAssembler.getPassengerPaymentsMap().values().iterator().next());
			PaymentInfo firstPayment = BeanUtils.getFirstElement(payAsm.getPayments());
			if (firstPayment != null && firstPayment.getPayCurrencyDTO() != null) {
				lastCurrencyCode = firstPayment.getPayCurrencyDTO().getPayCurrencyCode();
			}
		}

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.UPDATE_RESERVATION_PAYMENT_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR, new Boolean(false));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, new Boolean(isForceConfirm));
			command.setParameter(CommandParamNames.ACCEPT_MORE_PAYMENTS_THAN_ITS_CHARGES,
					new Boolean(acceptMorePaymentsThanItsCharges));
			command.setParameter(CommandParamNames.IS_PAYMENT_FOR_ACQUIRE_CREDITS, new Boolean(isPaymentForAcquireCredits));
			command.setParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, paxAssembler.getPassengerPaymentsMap());
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.INSURANCE_CHK_HOLD, Boolean.TRUE);
			command.setParameter(CommandParamNames.LAST_CURRENCY_CODE, lastCurrencyCode);
			command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
			command.setParameter(CommandParamNames.RESERVATION_TYPE, "UPDATE_RESERVATION");
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(isCapturePayment));
			command.setParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED, Boolean.valueOf((otherCarrierCreditUsed)));
			command.setParameter(CommandParamNames.IS_GDS_SALE, new Boolean(isGdsSale));
			command.setParameter(CommandParamNames.IS_FIRST_PAYMENT, new Boolean(isFirstPayment));
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));

			if (paxAssembler.getExternalPaymentTnxInfo() != null) {
				command.setParameter(CommandParamNames.TRIGGER_EXTERNAL_PAY_TX_UPDATE, new Boolean(true));
				command.setParameter(CommandParamNames.EXTERNAL_PAY_TX_INFO, paxAssembler.getExternalPaymentTnxInfo());
			}
			if (colPaymentInfo == null || colPaymentInfo.isEmpty()) {
				command.setParameter(CommandParamNames.INSURANCE_HOLD, new Boolean(true));
			}

			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, new Boolean(false));
			// Fraud check enable for CC payment through the WS
			command.setParameter(CommandParamNames.CC_FRAUD_ENABLE, new Boolean(enableECFraudChecking));
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, actualPayment);
			command.setParameter(CommandParamNames.PAX_ADJ_ASSEMBLER, paxAdjAssmbler);

			if (loyaltyPaymentInfo != null) {
				command.setParameter(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS,
						loyaltyPaymentInfo.getPaxProductPaymentBreakdown());
			}

			ServiceResponce serviceResponce = command.execute();
			serviceResponce.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED,
					Boolean.valueOf((otherCarrierCreditUsed)));

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);
			ReservationModuleUtils.getReservationBD().sendSms(pnr, contactInfo, credentialsDTO);

			if (loyaltyPaymentInfo != null) {
				if (trackInfoDTO != null && trackInfoDTO.getAppIndicator() != null) {
					redeemLoyaltyRewards(loyaltyPaymentInfo.getLoyaltyRewardIds(), trackInfoDTO.getAppIndicator(),
							loyaltyPaymentInfo.getMemberAccountId());
				} else {
					redeemLoyaltyRewards(loyaltyPaymentInfo.getLoyaltyRewardIds(), AppIndicatorEnum.APP_IBE,
							loyaltyPaymentInfo.getMemberAccountId());
				}

			}

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateReservationForPayment "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), ex);
			comments = "ModuleException::updateReservationForPayment";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			if (loyaltyPaymentInfo != null) {
				ReservationModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(pnr,
						loyaltyPaymentInfo.getLoyaltyRewardIds(), loyaltyPaymentInfo.getMemberAccountId());
			}

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateReservationForPayment "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::updateReservationForPayment";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			if (loyaltyPaymentInfo != null) {
				ReservationModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(pnr,
						loyaltyPaymentInfo.getLoyaltyRewardIds(), loyaltyPaymentInfo.getMemberAccountId());
			}

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update reservation payment after adding an additional charge to the reservation.
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param isForceConfirm
	 * @param pnrPaxFareIdMap
	 * @param totalAdjustmentAmount
	 * @param userNotes
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updateResForPaymentWithSeriveCharge(String pnr, IPassenger iPassenger, boolean isForceConfirm,
			boolean acceptMorePaymentsThanItsCharges, Integer pnrPaxFareId, Integer chargeRateID, BigDecimal amount,
			String userNotes, long version, TrackInfoDTO trackInfoDTO) throws ModuleException {

		ServiceResponce serviceResponce = null;
		try {
			serviceResponce = adjustCreditManual(pnr, pnrPaxFareId, chargeRateID, amount, userNotes, version, null, trackInfoDTO,
					null);
			serviceResponce = updateReservationForPayment(pnr, iPassenger, isForceConfirm, acceptMorePaymentsThanItsCharges,
					version + 1, trackInfoDTO, true, false, true, false, false, false, null, false, null);
		} catch (ModuleException me) {
			log.error(" ((o)) ModuleException::updateResForPaymentWithSeriveCharge ", me);
			this.sessionContext.setRollbackOnly();
			throw me;
		}
		return serviceResponce;
	}

	/**
	 * Update reservation payments with the split operation
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updateReservationForSplit(String pnr, IPassenger iPassenger, long version, TrackInfoDTO trackInfoDTO,
			boolean enableECFraudChecking, boolean isPaymentForAcquireCredits, boolean otherCarrierCreditUsed,
			boolean actualPayment, boolean isCapturePayment) throws ModuleException {

		String comments = "";

		PassengerAssembler paxAssembler = (PassengerAssembler) iPassenger;

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(paxAssembler, pnr);

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		// Load the reservation contact information
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD()
				.makeTemporyPaymentEntry(paxAssembler.getTnxIds(), contactInfo, colPaymentInfo, credentialsDTO, true, false);

		// To save last saved pay currency
		String lastCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		if (paxAssembler.getPassengerPaymentsMap() != null && paxAssembler.getPassengerPaymentsMap().values().size() > 0) {
			PaymentAssembler payAsm = ((PaymentAssembler) paxAssembler.getPassengerPaymentsMap().values().iterator().next());
			PaymentInfo firstPayinfo = BeanUtils.getFirstElement(payAsm.getPayments());
			if (firstPayinfo != null && firstPayinfo.getPayCurrencyDTO() != null) {
				lastCurrencyCode = firstPayinfo.getPayCurrencyDTO().getPayCurrencyCode();
			}
		}

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.UPDATE_RESERVATION_SPLIT_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR, new Boolean(false));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, new Boolean(false));
			command.setParameter(CommandParamNames.ACCEPT_MORE_PAYMENTS_THAN_ITS_CHARGES, new Boolean(false));
			command.setParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, paxAssembler.getPassengerPaymentsMap());
			command.setParameter(CommandParamNames.IS_PAX_SPLIT, new Boolean(true));
			command.setParameter(CommandParamNames.TRIGGER_ISOLATED_INFANT_MOVE, new Boolean(false));
			command.setParameter(CommandParamNames.IS_PAYMENT_FOR_ACQUIRE_CREDITS, new Boolean(isPaymentForAcquireCredits));
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, new Boolean(false));
			command.setParameter(CommandParamNames.LAST_CURRENCY_CODE, lastCurrencyCode);

			command.setParameter(CommandParamNames.RESERVATION_TYPE, "UPDATE_RESERVATION");
			command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
			command.setParameter(CommandParamNames.CC_FRAUD_ENABLE, new Boolean(enableECFraudChecking));
			command.setParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED, new Boolean(otherCarrierCreditUsed));
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, actualPayment);
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(isCapturePayment));

			ServiceResponce serviceResponce = command.execute();
			serviceResponce.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED, new Boolean(otherCarrierCreditUsed));

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);
			ReservationModuleUtils.getReservationBD().sendSms(pnr, contactInfo, credentialsDTO);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(
					" ((o)) ModuleException::updateReservationForSplit " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					ex);
			comments = "ModuleException::updateReservationForSplit";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateReservationForSplit "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::updateReservationForSplit";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Create on hold reservation
	 * 
	 * @param iReservation
	 * @param blockKeyIds
	 * @param forceConfirm
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce createOnHoldReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			boolean forceConfirm, TrackInfoDTO trackInfoDTO, boolean isCapturePayment) throws ModuleException {

		String comments = "";

		// Retrieve the called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		ReservationAssembler rAssembler = (ReservationAssembler) iReservation;

		// Injecting the segment credentials information
		rAssembler.injectSegmentCredentials(credentialsDTO);

		// Get the next pnr
		String pnr = ReservationBO.getNextPnrNumber(rAssembler.getTnxIds(), rAssembler);

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(rAssembler, pnr);

		// Set the pnr number
		rAssembler.getDummyReservation().setPnr(pnr);

		// set dummy booking flag to no
		rAssembler.setDummyBooking(ReservationInternalConstants.DummyBooking.NO);

		//
		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(rAssembler.getTnxIds(),
				rAssembler.getDummyReservation().getContactInfo(), colPaymentInfo, credentialsDTO, true, false);

		LoyaltyPaymentInfo loyaltyPaymentInfo = rAssembler.getLoyaltyPaymentInfo();

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CREATE_ONHOLD_RESERVATION_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			ReservationContactInfo reservationContact = rAssembler.getDummyReservation().getContactInfo();
			reservationContact.setPnr(pnr);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, reservationContact);
			command.setParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO, rAssembler.getReservationAdminInfoTO());
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.RESERVATION, rAssembler.getDummyReservation());
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, rAssembler.getPnrZuluReleaseTimeStamp());
			command.setParameter(CommandParamNames.OND_FARE_DTOS, rAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_DTO, rAssembler.getFareDiscount());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_INFO, rAssembler.getFareDiscountInfo());
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, rAssembler.getSegmentObjectsMap());
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, new Boolean(false));
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);
			command.setParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR, new Boolean(false));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, new Boolean(forceConfirm));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.INSURANCE_INFO, rAssembler.getInsuranceRequest());
			command.setParameter(CommandParamNames.INSURANCE_HOLD, Boolean.TRUE);
			command.setParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS, rAssembler.getPaxSegmentSSRDTOMaps());
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, new Boolean(false));
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(isCapturePayment));
			command.setParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET,
					ReservationApiUtils.getFlightSegmentsForOnDFare(rAssembler.getInventoryFares()));
			command.setParameter(CommandParamNames.BOOKING_TYPE, rAssembler.getBookingType());
			command.setParameter(CommandParamNames.ALLOW_CREATE_RESERVATION_AFTER_CUT_OFF_TIME,
					rAssembler.isAllowFlightSearchAfterCutOffTime());
			command.setParameter(CommandParamNames.AUTO_CANCELLATION_INFO, rAssembler.getAutoCancellationInfo());
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, new Boolean(false));
			command.setParameter(CommandParamNames.ONHOLD_OR_FORCE_CONFIRM, new Boolean(true));
			command.setParameter(CommandParamNames.AGENT_COMMISSION_DTO, rAssembler.getAgentCommissionInfo());
			command.setParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, iReservation.getReasonForAllowBlPaxRes());

			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);
			ReservationModuleUtils.getReservationBD().sendSms(pnr, rAssembler.getDummyReservation().getContactInfo(),
					credentialsDTO);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createOnHoldReservation " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					ex);
			comments = "ModuleException::createOnHoldReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createOnHoldReservation "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::createOnHoldReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Create reservation through aaServices
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce createAAServiceReservationMacro(IReservation iReservation, Collection<TempSegBcAlloc> collBlockIDs,
			Collection<OndFareDTO> ondFareDto, boolean forceConfirm, boolean onholdReservation, boolean isCapturePayment,
			String userNotes, TrackInfoDTO trackInfo) throws ModuleException {

		if (!(onholdReservation || forceConfirm)) {
			throw new ModuleException("aaservice.operation.not.supported", "aaservice");
		}
		try {
			if (collBlockIDs == null) {
				collBlockIDs = this.blockSeats(ondFareDto, null);
			}
			
			ServiceResponce serviceResponse = null;
			serviceResponse = this.createOnHoldReservation(iReservation, collBlockIDs, forceConfirm, trackInfo, isCapturePayment);

			return serviceResponse;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createAAServiceReservationMacro ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createAAServiceReservationMacro ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce createPayCarrierfulfillReservation(Reservation reservation, TrackInfoDTO trackInfoDTO,
			ReservationAdminInfoTO adminInfoTO, Map<Integer, Collection<ReservationPaxExtTnx>> paxWiseExtTnx,
			String reasonForAllowBlPax) throws ModuleException {
		// Retrieve the called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CREATE_PAY_CARRIER_FULFILLMENT);

			ReservationContactInfo reservationContact = reservation.getContactInfo();
			reservationContact.setPnr(reservation.getPnr());
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, reservationContact);
			command.setParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO, adminInfoTO);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.EXT_PAX_TNX_MAP, paxWiseExtTnx);
			command.setParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, reasonForAllowBlPax);

			ServiceResponce serviceResponce = command.execute();

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createPayCarrierfulfillReservation fails", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::createPayCarrierfulfillReservation fails", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Create CC Reservation
	 * 
	 * @param iReservation
	 * @param blockKeyIds
	 * @param trackInfoDTO
	 * @param wsSessionId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce createCCReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			TrackInfoDTO trackInfoDTO, boolean enableFraud, boolean actualPayment) throws ModuleException {

		boolean isCapturePayment = true;

		String comments = "";

		// Retrieve the called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		ReservationAssembler rAssembler = (ReservationAssembler) iReservation;

		// Injecting the segment credentials information
		rAssembler.injectSegmentCredentials(credentialsDTO);

		// Get the next pnr
		String pnr = "";
		if (rAssembler.isCreateResNPayInitInDifferentFlows()) {
			pnr = ReservationBO.getNextPnrNumber(null, rAssembler);
		} else {
			pnr = ReservationBO.getNextPnrNumber(rAssembler.getTnxIds(), rAssembler);
		}

		// Get the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(rAssembler, pnr);

		// Set the pnr
		rAssembler.getDummyReservation().setPnr(pnr);

		// set the dummy booking flag
		rAssembler.setDummyBooking(ReservationInternalConstants.DummyBooking.NO);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(rAssembler.getTnxIds(),
				rAssembler.getDummyReservation().getContactInfo(), colPaymentInfo, credentialsDTO, true, false);

		// check whether the reservation has zero payment if true set the force confirm to true
		boolean forceConfirm = true;
		for (ReservationPax pax : rAssembler.getDummyReservation().getPassengers()) {
			PaymentAssembler payment = (PaymentAssembler) pax.getPayment();
			if (payment != null && payment.getPayments() != null) {
				if (payment.getPayments().size() > 0 && payment.getTotalPayAmount().compareTo(BigDecimal.ZERO) > 0) {
					forceConfirm = false;
					break;
				}
			}
		}
		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CREATE_C_C_RESERVATION_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION, rAssembler.getDummyReservation());
			command.setParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO, rAssembler.getReservationAdminInfoTO());
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, rAssembler.getPnrZuluReleaseTimeStamp());
			command.setParameter(CommandParamNames.OND_FARE_DTOS, rAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_DTO, rAssembler.getFareDiscount());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_INFO, rAssembler.getFareDiscountInfo());
			command.setParameter(CommandParamNames.AGENT_COMMISSION_DTO, rAssembler.getAgentCommissionInfo());
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, rAssembler.getSegmentObjectsMap());
			ReservationContactInfo reservationContact = rAssembler.getDummyReservation().getContactInfo();
			reservationContact.setPnr(pnr);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, reservationContact);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, new Boolean(false));
			command.setParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR, new Boolean(true));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, new Boolean(forceConfirm));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.INSURANCE_INFO, rAssembler.getInsuranceRequest());
			command.setParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS, rAssembler.getPaxSegmentSSRDTOMaps());
			command.setParameter(CommandParamNames.CC_FRAUD_ENABLE, new Boolean(enableFraud));
			command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
			command.setParameter(CommandParamNames.RESERVATION_TYPE, "CCRESERVATION");
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, new Boolean(false));
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, isCapturePayment);
			command.setParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET,
					ReservationApiUtils.getFlightSegmentsForOnDFare(rAssembler.getInventoryFares()));
			command.setParameter(CommandParamNames.BOOKING_TYPE, rAssembler.getBookingType());
			command.setParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED, rAssembler.isUsedOtherCarrierCredit());
			command.setParameter(CommandParamNames.ALLOW_CREATE_RESERVATION_AFTER_CUT_OFF_TIME,
					rAssembler.isAllowFlightSearchAfterCutOffTime());
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, actualPayment);
			command.setParameter(CommandParamNames.IS_FIRST_PAYMENT, new Boolean(true));
			command.setParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, rAssembler.getReasonForAllowBlPaxRes());
			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createCCReservation " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					ex);
			comments = "ModuleException::createCCReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createCCReservation "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::createCCReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public ServiceResponce createGDSReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			Map<Integer, Map<Integer, EticketDTO>> eTicketInfo, TrackInfoDTO trackInfoDTO, boolean enableFraud)
			throws ModuleException {

		boolean isCapturePayment = true;

		String comments = "";

		// Retrieve the called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		ReservationAssembler rAssembler = (ReservationAssembler) iReservation;

		// Injecting the segment credentials information
		rAssembler.injectSegmentCredentials(credentialsDTO);

		// Get the next pnr
		String pnr = ReservationBO.getNextPnrNumber(rAssembler.getTnxIds(), rAssembler);

		// Get the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(rAssembler, pnr);

		// Set the pnr
		rAssembler.getDummyReservation().setPnr(pnr);

		// set the dummy booking flag
		rAssembler.setDummyBooking(ReservationInternalConstants.DummyBooking.NO);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(rAssembler.getTnxIds(),
				rAssembler.getDummyReservation().getContactInfo(), colPaymentInfo, credentialsDTO, true, false);

		ReservationContactInfo reservationContact = rAssembler.getDummyReservation().getContactInfo();
		reservationContact.setPnr(pnr);

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CREATE_G_D_S_RESERVATION_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION, rAssembler.getDummyReservation());
			command.setParameter(CommandParamNames.E_TICKET_MAP, eTicketInfo);
			command.setParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO, rAssembler.getReservationAdminInfoTO());
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, rAssembler.getPnrZuluReleaseTimeStamp());
			command.setParameter(CommandParamNames.OND_FARE_DTOS, rAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, rAssembler.getSegmentObjectsMap());
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, reservationContact);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, Boolean.FALSE);
			command.setParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR, Boolean.TRUE);
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, Boolean.FALSE);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.INSURANCE_INFO, rAssembler.getInsuranceRequest());
			command.setParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS, rAssembler.getPaxSegmentSSRDTOMaps());
			command.setParameter(CommandParamNames.CC_FRAUD_ENABLE, new Boolean(enableFraud));
			command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
			command.setParameter(CommandParamNames.RESERVATION_TYPE, "CCRESERVATION");
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, Boolean.FALSE);
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, isCapturePayment);
			command.setParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET,
					ReservationApiUtils.getFlightSegmentsForOnDFare(rAssembler.getInventoryFares()));
			command.setParameter(CommandParamNames.ALLOW_CREATE_RESERVATION_AFTER_CUT_OFF_TIME,
					rAssembler.isAllowFlightSearchAfterCutOffTime());
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, new Boolean(true));

			String reasonForAllowBlPax = "Reserved From GDS";
			command.setParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, reasonForAllowBlPax);

			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createCCReservation " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					ex);
			comments = "ModuleException::createCCReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createCCReservation "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::createCCReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Create Web Reservation
	 * 
	 * @param iReservation
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce createWebReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		boolean isCapturePayment = true;

		String comments = "";

		// Retrieve the called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		ReservationAssembler rAssembler = (ReservationAssembler) iReservation;

		// Injecting the segment credentials information
		rAssembler.injectSegmentCredentials(credentialsDTO);

		// Get the next pnr
		String pnr = ReservationBO.getNextPnrNumber(rAssembler.getTnxIds(), rAssembler);

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(rAssembler, pnr);

		// Set the pnr
		rAssembler.getDummyReservation().setPnr(pnr);

		// set dummy booking flag to no
		rAssembler.setDummyBooking(ReservationInternalConstants.DummyBooking.NO);

		ReservationBO.injectIPToCountry(rAssembler, trackInfoDTO);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(rAssembler.getTnxIds(),
				rAssembler.getDummyReservation().getContactInfo(), colPaymentInfo, credentialsDTO, true, false);

		LoyaltyPaymentInfo loyaltyPaymentInfo = rAssembler.getLoyaltyPaymentInfo();

		Boolean triggerBlockSeats = (blockKeyIds == null);
		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CREATE_WEB_RESERVATION_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION, rAssembler.getDummyReservation());
			command.setParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO, rAssembler.getReservationAdminInfoTO());
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, rAssembler.getPnrZuluReleaseTimeStamp());
			command.setParameter(CommandParamNames.OND_FARE_DTOS, rAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_DTO, rAssembler.getFareDiscount());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_INFO, rAssembler.getFareDiscountInfo());
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, rAssembler.getSegmentObjectsMap());
			ReservationContactInfo reservationContact = rAssembler.getDummyReservation().getContactInfo();
			reservationContact.setPnr(pnr);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, reservationContact);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, triggerBlockSeats);
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);
			command.setParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR, new Boolean(true));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, new Boolean(false));
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.INSURANCE_INFO, rAssembler.getInsuranceRequest());
			command.setParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS, rAssembler.getPaxSegmentSSRDTOMaps());
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, new Boolean(false));
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, isCapturePayment);
			command.setParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET,
					ReservationApiUtils.getFlightSegmentsForOnDFare(rAssembler.getInventoryFares()));
			command.setParameter(CommandParamNames.ALLOW_CREATE_RESERVATION_AFTER_CUT_OFF_TIME,
					rAssembler.isAllowFlightSearchAfterCutOffTime());
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, new Boolean(true));
			if (loyaltyPaymentInfo != null) {
				command.setParameter(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS,
						loyaltyPaymentInfo.getPaxProductPaymentBreakdown());
			}

			String reasonForAllowBlPax = "Reserved From WEB";
			command.setParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, reasonForAllowBlPax);

			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);

			// Call redeem loyalty rewards
			if (loyaltyPaymentInfo != null) {
				redeemLoyaltyRewards(loyaltyPaymentInfo.getLoyaltyRewardIds(), trackInfoDTO.getAppIndicator(),
						loyaltyPaymentInfo.getMemberAccountId());
			}

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createWebReservation " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					ex);
			comments = "ModuleException::createWebReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			if (loyaltyPaymentInfo != null) {
				ReservationModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(pnr,
						loyaltyPaymentInfo.getLoyaltyRewardIds(), loyaltyPaymentInfo.getMemberAccountId());
			}

			log.error(ReservationApiUtils.getBasicReservationInfo(rAssembler));
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createWebReservation "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::createWebReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			if (loyaltyPaymentInfo != null) {
				ReservationModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(pnr,
						loyaltyPaymentInfo.getLoyaltyRewardIds(), loyaltyPaymentInfo.getMemberAccountId());
			}

			log.error(ReservationApiUtils.getBasicReservationInfo(rAssembler));
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void intermediateRefund(IReservation iReservation, TrackInfoDTO trackInfoDTO, String exceptionCode)
			throws ModuleException {
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);
		ReservationAssembler rAssembler = (ReservationAssembler) iReservation;
		String pnr = ReservationBO.getNextPnrNumber(rAssembler.getTnxIds(), rAssembler);
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(rAssembler, pnr);
		BigDecimal temporaryTotalPaymentAmount = ReservationBO.getTotalCardPayCurrencymentAmount(rAssembler);
		BigDecimal reservationTotalPaymentamount = ReservationBO.getReservationTotalPaymentAmount(colPaymentInfo);
		/**
		 * The credit card payment amount and reservation total payment should be eqal if any case , amounts are
		 * mismatch we need to avoid the process Then the scheduler process will reconcile the payments correctly
		 */
		if (temporaryTotalPaymentAmount.equals(reservationTotalPaymentamount)) {
			Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
					rAssembler.getTnxIds(), rAssembler.getDummyReservation().getContactInfo(), colPaymentInfo, credentialsDTO,
					true, false);
			reversePayment(colPaymentInfo, trackInfoDTO, exceptionCode, colTnxIds);
			String comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);
		} else {
			// Scheduler will reconsile the transactions.
			/**
			 * Track the reservation data Need to investigate root cause of failure. It could be some charges missing in
			 * the reservation
			 */
			log.info("### Please investigate the root causes of card payment mismatch :"
					+ rAssembler.getDummyReservation().getPnr());
			log.info("### Mismatch amount :"
					+ AccelAeroCalculator.subtract(temporaryTotalPaymentAmount, reservationTotalPaymentamount));

		}
	}

	/**
	 * Create Go Shore Reservation
	 * 
	 * @param iReservation
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce createGoShoreReservation(IReservation iReservation, TrackInfoDTO trackInfoDTO) throws ModuleException {

		String comments = "";

		// Retrieve the called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		ReservationAssembler rAssembler = (ReservationAssembler) iReservation;

		// Injecting the segment credentials information
		rAssembler.injectSegmentCredentials(credentialsDTO);

		// Get the next pnr
		String pnr = ReservationBO.getNextPnrNumber(rAssembler.getTnxIds(), rAssembler);

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(rAssembler, pnr);

		// Set the pnr
		rAssembler.getDummyReservation().setPnr(pnr);

		// set the dummy booking flag to N
		rAssembler.setDummyBooking(ReservationInternalConstants.DummyBooking.NO);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(rAssembler.getTnxIds(),
				rAssembler.getDummyReservation().getContactInfo(), colPaymentInfo, credentialsDTO, true, false);

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CREATE_GO_SHORE_RESERVATION_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION, rAssembler.getDummyReservation());
			command.setParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO, rAssembler.getReservationAdminInfoTO());
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, rAssembler.getPnrZuluReleaseTimeStamp());
			command.setParameter(CommandParamNames.OND_FARE_DTOS, rAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_DTO, rAssembler.getFareDiscount());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_INFO, rAssembler.getFareDiscountInfo());
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, rAssembler.getSegmentObjectsMap());
			ReservationContactInfo reservationContact = rAssembler.getDummyReservation().getContactInfo();
			reservationContact.setPnr(pnr);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, reservationContact);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR, new Boolean(true));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, new Boolean(false));
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, new Boolean(true));
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(true));
			command.setParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET,
					ReservationApiUtils.getFlightSegmentsForOnDFare(rAssembler.getInventoryFares()));
			command.setParameter(CommandParamNames.ALLOW_CREATE_RESERVATION_AFTER_CUT_OFF_TIME,
					rAssembler.isAllowFlightSearchAfterCutOffTime());
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, new Boolean(true));

			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(
					" ((o)) ModuleException::createGoShoreReservation " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					ex);
			comments = "ModuleException::createGoShoreReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createGoShoreReservation "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::createGoShoreReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Create Split Reservation
	 * 
	 * @param iReservation
	 * @param blockKeyIds
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce createSplitReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		String comments = "";

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		ReservationAssembler rAssembler = (ReservationAssembler) iReservation;

		// Injecting the segment credentials information
		rAssembler.injectSegmentCredentials(credentialsDTO);

		// Get the next pnr
		String pnr = ReservationBO.getNextPnrNumber(rAssembler.getTnxIds(), rAssembler);

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(rAssembler, pnr);

		// Set the pnr
		rAssembler.getDummyReservation().setPnr(pnr);

		rAssembler.setDummyBooking(ReservationInternalConstants.DummyBooking.NO);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(rAssembler.getTnxIds(),
				rAssembler.getDummyReservation().getContactInfo(), colPaymentInfo, credentialsDTO, true, false);

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CREATE_SPLIT_RESERVATION_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION, rAssembler.getDummyReservation());
			command.setParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO, rAssembler.getReservationAdminInfoTO());
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, rAssembler.getPnrZuluReleaseTimeStamp());
			ReservationContactInfo reservationContact = rAssembler.getDummyReservation().getContactInfo();
			reservationContact.setPnr(pnr);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, reservationContact);
			command.setParameter(CommandParamNames.OND_FARE_DTOS, rAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_DTO, rAssembler.getFareDiscount());
			command.setParameter(CommandParamNames.FARE_DISCOUNT_INFO, rAssembler.getFareDiscountInfo());
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, rAssembler.getSegmentObjectsMap());
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, new Boolean(false));
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);
			command.setParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR, new Boolean(false));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, new Boolean(false));
			command.setParameter(CommandParamNames.IS_PAX_SPLIT, new Boolean(true));
			command.setParameter(CommandParamNames.TRIGGER_ISOLATED_INFANT_MOVE, new Boolean(false));
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS, rAssembler.getPaxSegmentSSRDTOMaps());
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, new Boolean(false));
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(true));
			command.setParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET,
					ReservationApiUtils.getFlightSegmentsForOnDFare(rAssembler.getInventoryFares()));
			command.setParameter(CommandParamNames.ALLOW_CREATE_RESERVATION_AFTER_CUT_OFF_TIME,
					rAssembler.isAllowFlightSearchAfterCutOffTime());
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, new Boolean(true));
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));

			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);
			ReservationModuleUtils.getReservationBD().sendSms(pnr, rAssembler.getDummyReservation().getContactInfo(),
					credentialsDTO);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createSplitReservation " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					ex);
			comments = "ModuleException::createSplitReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createSplitReservation "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::createSplitReservation";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * Return reservation passenger
	 * 
	 * @param pnrPaxId
	 * @param loadFares
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationPax getPassenger(int pnrPaxId, boolean loadFares) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getPassenger(pnrPaxId, loadFares, false, false);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassenger ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return reservation passenger Titles
	 * 
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> getPassengerTitleMap() throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getPassengerTitles();
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassengerTitleMap ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return passengers
	 * 
	 * @param pnr
	 * @param loadFares
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationPax> getPassengers(String pnr, boolean loadFares) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getPassengers(pnr, loadFares);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassengers ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return segments
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationSegment> getSegments(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getSegments(pnr);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getSegments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return AirportTransfers
	 * 
	 * @param pnrPaxIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PaxAirportTransferTO> getReservationAirportTransfersByPaxId(Collection<Integer> pnrPaxIds)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO.getReservationAirportTransfersByPaxId(pnrPaxIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationAirportTransfersByPaxId ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationSegmentDTO> getSegmentsView(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getPnrSegmentsView(pnr);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getSegments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return reservation contact information
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationContactInfo getContactInfo(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getContactInfo(pnr);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getContactInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return reservation segments
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationSegment> getPnrSegments(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getPnrSegments(pnr);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrSegments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return reservations for the segment
	 * 
	 * @param flightSegId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Reservation> getPnrsForSegment(int flightSegId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getPnrsForSegment(flightSegId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrsForSegment ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return reservation passenger payments
	 * 
	 * @param colPnrPaxId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, Collection<ReservationTnx>> getPNRPaxPaymentsAndRefunds(Collection<Integer> colPnrPaxId)
			throws ModuleException {
		try {
			return ReservationBO.getPNRPaxPaymentsAndRefunds(colPnrPaxId, false, true);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPNRPaxPayments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, Collection<ReservationTnx>> getPNRPaxPaymentsAndRefundsTnx(Collection<Integer> colPnrPaxId)
			throws ModuleException {
		try {
			return ReservationBO.getPNRPaxPaymentsAndRefunds(colPnrPaxId, false, true);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPNRPaxPayments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return reservation credits (FAST LANE READER)
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PaxCreditDTO> getReservationCredits(int customerId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.getReservationCredits(customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationCredits ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return Pax Credit for List of PNRs
	 * 
	 * @param pnrList
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PaxCreditDTO> getPaxCreditForPnrs(List<String> pnrList) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.getPaxCreditForPnrs(pnrList);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPaxCreditForPnrs ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the reservation segments for given flight segment ids
	 * 
	 * @param flightSegIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationSegment> getPnrSegments(List<Integer> flightSegIds, boolean loadReservation, boolean loadFares)
			throws ModuleException {

		try {

			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getReservationSegments(flightSegIds, null, null,
					loadReservation, loadFares);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrSegments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the no.of confirmed and onhold pnrs for each segment of a flight
	 * 
	 * @param flightIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<FlightReservationSummaryDTO> getFlightReservationsSummary(Collection<Integer> flightIds)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getFlightReservationsSummary(flightIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightReservationsSummary ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ExternalFlightSegment saveExternalFlightSegment(ExternalFlightSegment externalFlightSegment) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.saveExternalFlightSegment(externalFlightSegment);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveExternalFlightSegment ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ExternalFlightSegment getExternalFlightSegment(String carrierCode, Integer extFlightSegRef) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getExternalFlightSegment(carrierCode, extFlightSegRef);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getExternalFlightSegment ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update reservation contact information
	 * 
	 * @param pnr
	 * @param version
	 * @param contactInfo
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updateContactInfo(String pnr, long version, ReservationContactInfo contactInfo,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.UPDATE_R_CONTACT_INFO_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.IS_DUMMY_RESERVATION, Boolean.valueOf(false));
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getFlightReservationsSummary ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightReservationsSummary ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update the dummy reservation contact info
	 * 
	 * @param pnr
	 * @param contactInfo
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updateDummyReservationContactInfo(String pnr, long version, ReservationContactInfo contactInfo,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {

			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.UPDATE_CONTACT_INFO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.IS_DUMMY_RESERVATION, Boolean.valueOf(true));
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getFlightReservationsSummary ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightReservationsSummary ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public DefaultServiceResponse updateReservatonStatus(Reservation reservation, String newStatus) {

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			reservationPax.setStatus(newStatus);
		}

		reservation.setStatus(newStatus);
		ReservationProxy.saveReservation(reservation);

		return new DefaultServiceResponse(true);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public DefaultServiceResponse updateReservationModifiability(Reservation reservation, char modifiableReservation) {

		reservation.setModifiableReservation(modifiableReservation);
		ReservationProxy.saveReservation(reservation);
		return new DefaultServiceResponse(true);
	}

	/**
	 * Return Ond Charges for cancellation
	 * 
	 * @param pnr
	 * @param pnrSegmentIds
	 * @param loadMetaAmounts
	 * @param version
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PnrChargesDTO> getOndChargesForCancellation(String pnr, Collection<Integer> pnrSegmentIds,
			boolean loadMetaAmounts, long version, CustomChargesTO customChargesTO, boolean hasExternalPayments,
			boolean isVoidOperation) throws ModuleException {
		try {
			return new ChargeBO(loadMetaAmounts, getCallerCredentials(null)).getOndChargesForMultipleSegments(pnr, pnrSegmentIds,
					null, false, false, true, false, version, customChargesTO, hasExternalPayments, false, null, false,
					isVoidOperation, null);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getOndChargesForCancellation ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Collection<PnrChargesDTO> getOndChargesForRequote(boolean loadMetaAmounts, String pnr,
			Collection<Integer> pnrSegmentIds, Collection<Integer> newFltSegIds, long version, CustomChargesTO customChargesTO,
			boolean hasExternalPayments, boolean isForModification, boolean applyCnxCharge, boolean applyModCharge,
			Collection<OndFareDTO> ondFares, boolean skipPenaltyCharges, boolean isVoidOperation, boolean isFareAdjustmentValid,
			Map<Integer, String> ondFareTypeByFareIdMap, CredentialsDTO credentialsDTO) throws ModuleException {

		try {
			return new ChargeBO(loadMetaAmounts, credentialsDTO).getOndChargesForMultipleSegments(pnr, pnrSegmentIds,
					newFltSegIds, isForModification, false, applyCnxCharge, applyModCharge, version, customChargesTO,
					hasExternalPayments, true, ondFares, skipPenaltyCharges, isVoidOperation, ondFareTypeByFareIdMap);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getOndChargesForCancellation ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return Ond charges for cancellation applying modification charge
	 * 
	 * @param pnr
	 * @param pnrSegmentIds
	 * @param loadMetaAmounts
	 * @param version
	 * @param customChargesTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PnrChargesDTO> getOndChargesForCancellationApplyingModCharge(String pnr, Collection<Integer> pnrSegmentIds,
			Collection<Integer> newFltSegIds, boolean loadMetaAmounts, long version, CustomChargesTO customChargesTO,
			boolean hasExternalPayments, boolean isVoidOperation) throws ModuleException {
		try {
			return new ChargeBO(loadMetaAmounts, getCallerCredentials(null)).getOndChargesForMultipleSegments(pnr, pnrSegmentIds,
					newFltSegIds, false, false, false, true, version, customChargesTO, hasExternalPayments, false, null, false,
					isVoidOperation, null);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getOndChargesForCancellation ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return Ond Charges for modification
	 * 
	 * @param pnr
	 * @param pnrSegmentIds
	 * @param newFltSegIds
	 * @param loadMetaAmounts
	 * @param version
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PnrChargesDTO> getOndChargesForModification(String pnr, Collection<Integer> pnrSegmentIds,
			Collection<Integer> newFltSegIds, boolean loadMetaAmounts, boolean enableModifyProrating, long version,
			CustomChargesTO customChargesTo, boolean isVoidOperation) throws ModuleException {
		try {
			return new ChargeBO(loadMetaAmounts, getCallerCredentials(null)).getOndChargesForMultipleSegments(pnr, pnrSegmentIds,
					newFltSegIds, true, enableModifyProrating, false, true, version, customChargesTo, false, false, null, false,
					isVoidOperation, null);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getOndChargesForModification ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Collection<ChargeMetaTO>> getPnrPaxIdWiseCurrentCharges(String pnr, long version,
			boolean excludedPnrSegCharges, List<String> excludedPnrSegIds) throws ModuleException {
		try {
			return PNRCurrentChargeMetaComposer.getPnrPaxIdWiseCurrentCharges(pnr, version, excludedPnrSegCharges,
					excludedPnrSegIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrPaxIdWiseCurrentCharges ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Set<ChargesDetailDTO>> getPnrPaxSeqWiseBalanceDueCharges(LCCClientPnrModesDTO pnrModesDTO)
			throws ModuleException {
		try {
			return PNRBalanceDueChargeComposer.getPnrPaxSeqWiseBalanceDueCharges(pnrModesDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrPaxIdWiseBalanceDueCharges ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PnrChargesDTO> getPnrCharges(String pnr, boolean loadMetaAmounts, boolean includeFlown,
			boolean applyCnxCharge, long version) throws ModuleException {
		try {
			return new ChargeBO(loadMetaAmounts, getCallerCredentials(null)).getPnrCharges(pnr, version, null, false,
					includeFlown, applyCnxCharge, false, null);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrCharges ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PnrChargesDTO> getPnrCharges(String pnr, boolean loadMetaAmounts, long version,
			CustomChargesTO customChargesTO, boolean hasExternalPayments, boolean isVoidOperation,
			List<Integer> autoCancelingPnrSegIds) throws ModuleException {
		try {
			return new ChargeBO(loadMetaAmounts, getCallerCredentials(null)).getPnrCharges(pnr, version, customChargesTO,
					hasExternalPayments, false, true, isVoidOperation, autoCancelingPnrSegIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrCharges ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the reservation contact information (FAST LANE READER)
	 * 
	 * @param pnr
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationContactInfo getReservationContactInfo(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getReservationContactInfo(pnr);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationContactInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return contact infomation of the matching last booking
	 * 
	 * @param resContactInfoSearchDTO
	 *            Search criteria
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationContactInfo getLatestReservationContactInfo(ResContactInfoSearchDTO resContactInfoSearchDTO)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getLatestReservationContactInfo(resContactInfoSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getLatestReservationContactInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Passenger Refund
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param userNotes
	 * @param version
	 * @param trackInfoDTO
	 * @param preferredRefundOrder
	 *            Order the refund should deduct charges. (TAX,SUR etc.)
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce passengerRefund(String pnr, IPassenger iPassenger, String userNotes, boolean isCapturePayment,
			long version, TrackInfoDTO trackInfoDTO, boolean isAutoRefund, Collection<String> preferredRefundOrder,
			Collection<Long> pnrPaxOndChgIds, boolean removeAgentCommission) throws ModuleException {

		return groupPassengerRefund(pnr, iPassenger, userNotes, isCapturePayment, version, trackInfoDTO, isAutoRefund,
				preferredRefundOrder, pnrPaxOndChgIds, removeAgentCommission, false);
	}

	/**
	 * Group passenger refund
	 * 
	 * TODO : Deprecate passengerRefund and document this method when QA is done and dry group refund implemented.
	 * 
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce groupPassengerRefund(String pnr, IPassenger iPassenger, String userNotes, boolean isCapturePayment,
			long version, TrackInfoDTO trackInfoDTO, boolean isAutoRefund, Collection<String> preferredRefundOrder,
			Collection<Long> pnrPaxOndChgIds, boolean removeAgentCommission, boolean isManualRefund) throws ModuleException {

		String comments = "";

		PassengerAssembler passengerAssembler = (PassengerAssembler) iPassenger;

		// Load the reservation contact information
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		Collection<PaymentInfo> totalPayments = ReservationBO.getRefundInfo(passengerAssembler, pnr);

		// Make the tempory payment entry
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
				passengerAssembler.getTnxIds(), contactInfo, totalPayments, credentialsDTO, false, false);
		if (colTnxIds.size() == 0) {
			for (PaymentInfo payInfo : totalPayments) {
				if (payInfo instanceof OfflinePaymentInfo) {
					OfflinePaymentInfo offlinePaymentInfo = new OfflinePaymentInfo();
					offlinePaymentInfo.setTotalAmount(payInfo.getTotalAmount());
					offlinePaymentInfo.setPayCurrencyDTO(payInfo.getPayCurrencyDTO());
					Integer temporyPaymentId = ReservationModuleUtils.getReservationBD().recordTemporyPaymentEntryForOffline(
							contactInfo, offlinePaymentInfo, credentialsDTO, false, true, pnr);
					colTnxIds.add(temporyPaymentId);
				}
			}
		}

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.PASSENGER_REFUND_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.USER_NOTES, userNotes);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.PAYMENT_INFO, totalPayments);
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, passengerAssembler.getPassengerPaymentsMap());
			command.setParameter(CommandParamNames.SET_PREVIOUS_CREDIT_CARD_PAYMENT, false);
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, Boolean.valueOf(isCapturePayment));
			command.setParameter(CommandParamNames.PAX_REFUND_TYPE, Boolean.valueOf(isAutoRefund));
			command.setParameter(CommandParamNames.PREFERRED_REFUND_ORDER, preferredRefundOrder);
			command.setParameter(CommandParamNames.PNR_PAX_OND_CHARGE_IDS, pnrPaxOndChgIds);
			command.setParameter(CommandParamNames.REMOVE_AGENT_COMMISSION, new Boolean(removeAgentCommission));
			command.setParameter(CommandParamNames.IS_MANUAL_REFUND, new Boolean(isManualRefund));

			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::passengerRefund " + ReservationApiUtils.getElementsInCollection(colTnxIds), ex);
			comments = "ModuleException::passengerRefund";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(totalPayments, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::passengerRefund "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			comments = "CommonsDataAccessException::passengerRefund";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(totalPayments, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Transfer reservation ownership
	 * 
	 * @param pnr
	 * @param ownerAgentCode
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce transferOwnerShip(String pnr, String ownerAgentCode, long version, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.TRANSFER_OWNER_SHIP_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.OWNER_AGENT_CODE, ownerAgentCode);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.IS_DUMMY_RESERVATION, Boolean.valueOf(false));

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::transferOwnerShip ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::transferOwnerShip ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Transfer ownership for dummy bookings
	 * 
	 * @param pnr
	 * @param ownerAgentCode
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce transferOwnerShipForDummyRes(String pnr, String ownerAgentCode, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.TRANSFER_OWNER_SHIP);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.OWNER_AGENT_CODE, ownerAgentCode);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.IS_DUMMY_RESERVATION, Boolean.valueOf(true));

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::transferOwnerShipForDummyRes ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::transferOwnerShipForDummyRes ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Re Protect Reservations
	 * 
	 * @param transferSeatDTO
	 * @param isAlert
	 * @param overAlloc
	 * @param overAllocSeats
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce transferSeats(Collection<TransferSeatDTO> transferSeatDTOs, boolean isAlert, AllocateEnum overAlloc,
			int overAllocSeats, FlightAlertDTO fltAltDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RE_PROTECT_RESERVATIONS_MACRO);
			command.setParameter(CommandParamNames.TRANSFER_PAX_DTO, transferSeatDTOs);
			command.setParameter(CommandParamNames.IS_ALEART, new Boolean(isAlert));
			command.setParameter(CommandParamNames.OVER_ALLOC_ENUM, overAlloc);
			command.setParameter(CommandParamNames.OVER_ALLOC_SEATS, new Integer(overAllocSeats));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, fltAltDTO);

			ServiceResponce responce = command.execute();
			if (!responce.isSuccess()) {
				this.sessionContext.setRollbackOnly();
				return responce;
			}
			return responce;

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::transferSeats ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::transferSeats ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Roll Forward reservations
	 * 
	 * @param transferSeatDTO
	 * @param isAlert
	 * @param overAlloc
	 * @param overAllocSeats
	 * @param fromDate
	 * @param toDate
	 * @param excludeDays
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce rollForwardReservations(TransferSeatDTO transferSeatDTO, boolean isAlert, AllocateEnum overAlloc,
			int overAllocSeats, Date fromDate, Date toDate, Frequency includeDays, boolean includeCLSFlights,
			FlightAlertDTO flightAlertDTO) throws ModuleException {

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ROLL_FORWARD_RESERVATIONS_MACRO);
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			command.setParameter(CommandParamNames.TRANSFER_PAX_DTO, transferSeatDTO);
			command.setParameter(CommandParamNames.IS_ALEART, new Boolean(isAlert));
			command.setParameter(CommandParamNames.OVER_ALLOC_ENUM, overAlloc);
			command.setParameter(CommandParamNames.OVER_ALLOC_SEATS, new Integer(overAllocSeats));
			command.setParameter(CommandParamNames.ROLLFORWARD_FROM_DATE, fromDate);
			command.setParameter(CommandParamNames.ROLLFORWARD_TO_DATE, toDate);
			command.setParameter(CommandParamNames.ROLLFORWARD_INCLUDE_FREQUENCY, includeDays);
			command.setParameter(CommandParamNames.INCLUDE_CLS_FLIGHTS, Boolean.valueOf(includeCLSFlights));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);

			ServiceResponce responce = command.execute();
			if (!responce.isSuccess()) {
				this.sessionContext.setRollbackOnly();
				return responce;
			}
			return responce;

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::rollForwardReservations ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::rollForwardReservations ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * Search Reservations for credit card information (FAST LANE READER)
	 * 
	 * @param reservationPaymentDTO
	 * @return collection of ReservationDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationPaxDTO> getReservationsForCreditCardInfo(ReservationPaymentDTO reservationPaymentDTO)
			throws ModuleException {

		try {
			return SearchReservationProxy.getReservationsForCreditCardInfo(reservationPaymentDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationsForCreditCardInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns caller credentials information
	 * 
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	private CredentialsDTO getCallerCredentials(TrackInfoDTO trackInfoDTO) throws ModuleException {
		UserPrincipal userPrincipal = this.getUserPrincipal();
		return ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
	}

	/**
	 * Return credit card information
	 * 
	 * @param colTnxIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, CardPaymentInfo> getCreditCardInfo(Collection<Integer> colTnxIds, boolean isOwnTnx)
			throws ModuleException {
		try {
			Map<Integer, CardPaymentInfo> ccPaymentMap = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getCreditCardInfo(colTnxIds, isOwnTnx);

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currencyExchangeRate = null;

			for (Entry<Integer, CardPaymentInfo> entry : ccPaymentMap.entrySet()) {
				String payCurrencyCode = entry.getValue().getPayCurrencyDTO().getPayCurrencyCode();
				currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(payCurrencyCode, ApplicationEngine.IBE);

				entry.getValue().getPayCurrencyDTO().setPayCurrMultiplyingExchangeRate(
						new BigDecimal(currencyExchangeRate.getMultiplyingExchangeRate().toPlainString()));
			}

			return ccPaymentMap;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getCreditCardInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return credit card information from RESPCD
	 * 
	 * @param colTnxIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, CardPaymentInfo> getCreditCardInfoFromRESPCD(Collection<Integer> colTnxIds, boolean isOwnTnx)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getCreditCardInfoFromRESPCD(colTnxIds, isOwnTnx);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getCreditCardInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<AlertAction> getAlertActions() throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_ALERT_ACTION_DAO.getAlertActions();
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getAlertActions ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * Email itinerary
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void emailItinerary(ItineraryLayoutModesDTO itineraryLayoutModesDTO, Collection<Integer> colSelectedPnrPaxIds,
			TrackInfoDTO trackInfoDTO, boolean isCheckIndividual) throws ModuleException {
		try {
			// Get the caller credentials
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			ItineraryBL.emailItinerary(itineraryLayoutModesDTO, colSelectedPnrPaxIds, credentialsDTO, isCheckIndividual);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::emailItinerary ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception exception) {
			log.error("EmailItinerary failed", exception);
		}
	}

	/**
	 * Email Interline Itinerary
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void emailInterlineItinerary(ItineraryDTO itineraryDTO) throws ModuleException {
		try {
			// Get the caller credentials
			CredentialsDTO credentialsDTO = getCallerCredentials(itineraryDTO.getTrackInfoDTO());
			ItineraryBL.emailInterlineItinerary(itineraryDTO, credentialsDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::emailInterlineItinerary ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return user notes
	 * 
	 * @param pnr
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationModificationDTO> getUserNotes(String pnr, boolean isClassifyUN) throws ModuleException {
		try {
			// Get the caller credentials
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			return PnrZuluProxy.getReservationModificationsWithLocalTimes(pnr, true, credentialsDTO, isClassifyUN);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getUserNotes ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Clear segments alerts
	 * 
	 * @param colPnrSegIds
	 *            type of integer
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void clearSegmentAlerts(Map<Integer, String> mapPnrSegActions) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

			// Get confirmed reservation segments
			Collection<ReservationSegment> colReservationSegment = reservationSegmentDAO.getReservationSegments(null,
					mapPnrSegActions.keySet(), null, false, false);

			// Record Modifications
			ReservationBO.recordAlertModifications(colReservationSegment, credentialsDTO, AuditTemplateEnum.CLEARED_ALERT,
					mapPnrSegActions, null);

			// Clear segment alerts
			reservationSegmentDAO.updateReservationAlerts(ReservationInternalConstants.AlertTypes.ALERT_FALSE, null,
					mapPnrSegActions.keySet(), null);
			reservationSegmentDAO.updateReservationSegmentMealReprotectStatus(ReservationInternalConstants.AlertTypes.ALERT_FALSE,
					null, mapPnrSegActions.keySet(), null);
			reservationSegmentDAO.updateReservationSegmentSeatReprotectStatus(ReservationInternalConstants.AlertTypes.ALERT_FALSE,
					null, mapPnrSegActions.keySet(), null);

			AlertingBD alertingBD = ReservationModuleUtils.getAlertingBD();
			// Remove alerts of reservation segments
			alertingBD.removeAlertsOfPnrSegments(new ArrayList<Integer>(mapPnrSegActions.keySet()));
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::clearSegmentAlerts ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::clearSegmentAlerts ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Clear alerts
	 * 
	 * @param colAlertIDs
	 *            type of integer
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void clearAlerts(Collection<Integer> colAlertIDs) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
			AlertingBD alertingBD = ReservationModuleUtils.getAlertingBD();

			// Get collection of PNR Seg IDs, if only used by given alerts
			Collection<Integer> colPnrSegIds = alertingBD.getPNRSegIDsOfAlerts(colAlertIDs);

			// If Any PNR Segment does not exist. This could happen via concurrency
			if (colPnrSegIds == null || colPnrSegIds.size() == 0) {
				throw new ModuleException("airreservations.clearAlert.cannotLocatePNRSegments");
			}

			// Get confirmed reservation segments
			Collection<ReservationSegment> colReservationSegment = reservationSegmentDAO.getReservationSegments(null,
					colPnrSegIds, null, false, false);

			// Record Modifications
			ReservationBO.recordAlertModifications(colReservationSegment, credentialsDTO, AuditTemplateEnum.CLEARED_ALERT, null,
					null);

			// Update the segments as alert false
			reservationSegmentDAO.updateReservationAlerts(ReservationInternalConstants.AlertTypes.ALERT_FALSE, null, colPnrSegIds,
					null);
			reservationSegmentDAO.updateReservationSegmentMealReprotectStatus(ReservationInternalConstants.AlertTypes.ALERT_FALSE,
					null, colPnrSegIds, null);
			reservationSegmentDAO.updateReservationSegmentSeatReprotectStatus(ReservationInternalConstants.AlertTypes.ALERT_FALSE,
					null, colPnrSegIds, null);

			// Remove alerts of reservation segments
			alertingBD.removeAlerts(colAlertIDs);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::clearAlerts ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::clearAlerts ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Make Tempory payment entry
	 * 
	 * @param mapTnxIds
	 * @param reservationContactInfo
	 * @param colPaymentInfo
	 * @param credentialsDTO
	 * @param isCredit
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection<Integer> makeTemporyPaymentEntry(Map<Integer, CardPaymentInfo> mapTnxIds,
			ReservationContactInfo reservationContactInfo, Collection<PaymentInfo> colPaymentInfo, CredentialsDTO credentialsDTO,
			boolean isCredit, boolean isOfflinePayment) throws ModuleException {
		try {
			if (mapTnxIds == null || mapTnxIds.size() == 0) {
				Map<Integer, CardPaymentInfo> mapTmpPaymentIdAndCardInfo = PaymentGatewayBO.recordTemporyPaymentEntry(
						reservationContactInfo, colPaymentInfo, credentialsDTO, isCredit, true, isOfflinePayment);
				return new ArrayList<Integer>(mapTmpPaymentIdAndCardInfo.keySet());
			} else {
				PaymentGatewayBO.updateTemporyPaymentReferences(mapTnxIds, colPaymentInfo);
				return new ArrayList<Integer>(mapTnxIds.keySet());
			}
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::makeTemporyPaymentEntry ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::makeTemporyPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Make Tempory payment entry [THIS IS USED BY RESERVATION MODULE INTERNALLY TO KEEP TRACK OF TEMPORY PAYMENT
	 * INFORMATION]
	 * 
	 * @param mapTnxIds
	 * @param reservationContactInfo
	 * @param colPaymentInfo
	 * @param credentialsDTO
	 * @param isCredit
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TempPaymentTnx recordTemporyPaymentEntry(TemporyPaymentTO temporyPaymentTO, TrackInfoDTO trackInfo)
			throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfo);
			return LccBL.recordTemporyPaymentEntry(temporyPaymentTO, credentialsDTO);
		} catch (ModuleException mx) {
			log.error(" ((o)) ModuleException::recordTemporyPaymentEntry ", mx);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(mx, mx.getExceptionCode(), mx.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::recordTemporyPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Used to update the temp payment with the Fraud CC check result
	 * 
	 * @param tnxId
	 * @param result
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateTempPaymentWithFC(int tnxId, CCFraudCheckResponseDTO result) throws ModuleException {
		try {
			TempPaymentTnx paymentTnx = PaymentGatewayBO.getTempPaymentTnx(tnxId);
			paymentTnx.setFraudStatus(result.getStatus().toString());
			paymentTnx.setFraudMessage(result.getMessage());
			paymentTnx.setFraudRef(result.getRef());
			paymentTnx.setFraudScore(result.getScore());
			paymentTnx.setFraudResultCode(result.getResultCode());
			PaymentGatewayBO.saveTempPaymentTnx(paymentTnx);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::makeTemporyPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr) throws ModuleException {
		return PaymentGatewayBO.getTempPaymentsForReservation(pnr);
	}

	/**
	 * Make Tempory payment entry [THIS IS USED BY OUTSIDE OF RESERVATION MODULE TO KEEP TRACK OF TEMPORY PAYMENT
	 * INFORMATION]
	 * 
	 * @param pnr
	 * @param reservationContactInfo
	 * @param iPayment
	 * @param isCredit
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<Integer, CardPaymentInfo> makeTemporyPaymentEntry(String pnr, ReservationContactInfo reservationContactInfo,
			IPayment iPayment, boolean isCredit, TrackInfoDTO trackInfoDTO, boolean isOfflinePayment,
			boolean isCsOcFlightAvailable) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			PaymentAssembler paymentAssembler = (PaymentAssembler) iPayment;

			if (paymentAssembler.getPayments().size() == 0) {
				throw new ModuleException("airreservations.temporyPayment.missingParameters");
			}

			pnr = BeanUtils.nullHandler(pnr);

			if (pnr.equals("") && (BeanUtils.nullHandler(reservationContactInfo.getFirstName()).equals("")
					|| BeanUtils.nullHandler(reservationContactInfo.getLastName()).equals("")
					|| BeanUtils.nullHandler(reservationContactInfo.getPhoneNo()).equals(""))) {
				throw new ModuleException("airreservations.temporyPayment.missingParameters");
			}

			if (pnr.length() > 0) {
				// Load the reservation contact information
				reservationContactInfo = this.getReservationContactInfo(pnr);
			} else {
				// If pnr number is empty get a new pnr number
				pnr = PNRNumberGeneratorProxy.getNewPNR(isCsOcFlightAvailable, null);
			}

			ReservationBO.getPaymentInfo(paymentAssembler, pnr);
			return PaymentGatewayBO.recordTemporyPaymentEntry(reservationContactInfo, paymentAssembler.getPayments(),
					credentialsDTO, isCredit, false, isOfflinePayment);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::makeTemporyPaymentEntry ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::makeTemporyPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void getPaymentInfo(PaymentAssembler paymentAssembler, String pnr) throws ModuleException {
		try {
			ReservationBO.getPaymentInfo(paymentAssembler, pnr);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getPaymentInfo ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPaymentInfo ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update the tempory payment entry payment status
	 * 
	 * @param colTnxIds
	 * @param status
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateTempPaymentEntry(Collection<Integer> colTnxIds, String status, String comment, String ccLastDigits,
			String pnr, String alias) throws ModuleException {
		try {
			PaymentGatewayBO.updateTempPaymentEntries(colTnxIds, status, comment, ccLastDigits, false, pnr, alias);
		} catch (ModuleException ex) {
			log.info("#############################################");
			log.info(" T_TEMP_PAYMENT_TNX is in inconsistent state ");
			log.info(" Error occured while removing following tempory payment transaction entries, "
					+ " Having the following entries in T_TEMP_PAYMENT_TNX leads to inconsistent data " + colTnxIds);
			log.info("#############################################");
			log.error(" ((o)) ModuleException::updateTempPaymentEntry ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.info("#############################################");
			log.info(" T_TEMP_PAYMENT_TNX is in inconsistent state ");
			log.info(" Error occured while removing following tempory payment transaction entries, "
					+ " Having the following entries in T_TEMP_PAYMENT_TNX leads to inconsistent data " + colTnxIds);
			log.info("#############################################");
			log.error(" ((o)) CommonsDataAccessException::updateTempPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateTempPaymentEntryWithActualAmount(Collection<Integer> colTnxIds, String status, String comment,
			String ccLastDigits, BigDecimal eDirhamFee) throws ModuleException {
		try {
			PaymentGatewayBO.updateTempPaymentEntriesWithActualAmount(colTnxIds, status, comment, ccLastDigits, false,
					eDirhamFee);
		} catch (ModuleException ex) {
			log.info("#############################################");
			log.info(" T_TEMP_PAYMENT_TNX is in inconsistent state ");
			log.info(" Error occured while removing following tempory payment transaction entries, "
					+ " Having the following entries in T_TEMP_PAYMENT_TNX leads to inconsistent data " + colTnxIds);
			log.info("#############################################");
			log.error(" ((o)) ModuleException::updateTempPaymentEntry ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.info("#############################################");
			log.info(" T_TEMP_PAYMENT_TNX is in inconsistent state ");
			log.info(" Error occured while removing following tempory payment transaction entries, "
					+ " Having the following entries in T_TEMP_PAYMENT_TNX leads to inconsistent data " + colTnxIds);
			log.info("#############################################");
			log.error(" ((o)) CommonsDataAccessException::updateTempPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update the tempory payment entries with payment status for bulk records
	 * 
	 * @param colTnxIds
	 * @param status
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateTempPaymentEntriesForBulkRecords(Collection<Integer> colTnxIds, String status) throws ModuleException {
		try {
			PaymentGatewayBO.updateTempPaymentEntriesForBulkRecords(colTnxIds, status);
		} catch (ModuleException ex) {
			log.info("#############################################");
			log.info(" T_TEMP_PAYMENT_TNX is in inconsistent state ");
			log.info(" Error occured while removing following tempory payment transaction entries, "
					+ " Having the following entries in T_TEMP_PAYMENT_TNX leads to inconsistent data " + colTnxIds);
			log.info("#############################################");
			log.error(" ((o)) ModuleException::updateTempPaymentEntriesForBulkRecords ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.info("#############################################");
			log.info(" T_TEMP_PAYMENT_TNX is in inconsistent state ");
			log.info(" Error occured while removing following tempory payment transaction entries, "
					+ " Having the following entries in T_TEMP_PAYMENT_TNX leads to inconsistent data " + colTnxIds);
			log.info("#############################################");
			log.error(" ((o)) CommonsDataAccessException::updateTempPaymentEntriesForBulkRecords ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update the tempory payment entry payment status Payment Successful or Payment Failure can occure
	 * 
	 * @param mapTnxIds
	 * @param isSuccess
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateTempPaymentEntryPaymentStatus(Map<Integer, CommonCreditCardPaymentInfo> mapTnxIds,
			IPGResponseDTO ipgResponseDTO) throws ModuleException {
		Collection<Integer> colPaymentTnxIds = new ArrayList<Integer>(mapTnxIds.keySet());
		try {
			String status;

			if (ipgResponseDTO.isSuccess()) {
				status = ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS;
				for (Integer integer : mapTnxIds.keySet()) {
					CommonCreditCardPaymentInfo cardPaymentInfo = mapTnxIds.get(integer);
					cardPaymentInfo.setPaymentSuccess(true);
				}
			} else {
				status = ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE;
			}

			if (ipgResponseDTO.isSuccess() && ipgResponseDTO.geteDirhamFee() != null
					&& ipgResponseDTO.geteDirhamFee().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				PaymentGatewayBO.updateTempPaymentEntriesWithActualAmount(colPaymentTnxIds, status, null,
						ipgResponseDTO.getCcLast4Digits(), true, ipgResponseDTO.geteDirhamFee());
			} else {

				if (ipgResponseDTO.isSaveCreditCard()) {
					PaymentGatewayBO.updateTempPaymentEntries(colPaymentTnxIds, status, null, ipgResponseDTO.getCcLast4Digits(),
							true, null, ipgResponseDTO.getAlias(), ipgResponseDTO.isSaveCreditCard());
				} else {
					PaymentGatewayBO.updateTempPaymentEntries(colPaymentTnxIds, status, null, ipgResponseDTO.getCcLast4Digits(),
							true, null, null);
				}
			}
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateTempPaymentEntryPaymentStatus ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateTempPaymentEntryPaymentStatus ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update the tempory payment entry payment status Payment Successful or Payment Failure can occure for voucher
	 * purpose
	 * 
	 * @param tempTnxId
	 * @param isSuccess
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateTempPaymentEntryPaymentStatus(int tempTnxId, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		Collection<Integer> colPaymentTnxIds = new ArrayList<Integer>();
		colPaymentTnxIds.add(tempTnxId);
		try {
			String status;

			if (ipgResponseDTO.isSuccess()) {
				status = ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS;
			} else {
				status = ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE;
			}
			PaymentGatewayBO.updateTempPaymentEntries(colPaymentTnxIds, status, null, ipgResponseDTO.getCcLast4Digits(), true,
					null, null);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateTempPaymentEntryPaymentStatus ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateTempPaymentEntryPaymentStatus ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Handle multiple payment responses
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean processMultiplePaymentResponses(int transactionID) throws ModuleException {
		try {
			return PaymentGatewayBO.processMultiplePaymentResponses(transactionID);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::processMultiplePaymentResponses ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::processMultiplePaymentResponses ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Get time stamp of payment request
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Date getPaymentRequestTime(int paymentBrokerRefNo) throws ModuleException {
		return PaymentGatewayBO.getPaymentRequestTime(paymentBrokerRefNo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TempPaymentTnx loadTempPayment(int tnxId) throws ModuleException {
		return PaymentGatewayBO.loadTempPayment(tnxId);
	}

	/**
	 * Get amount of the payment request
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BigDecimal getTempPaymentAmount(int paymentBrokerRefNo) throws ModuleException {
		return PaymentGatewayBO.getTempPaymentAmount(paymentBrokerRefNo);
	}

	/**
	 * Get payment currency amount of the payment request
	 */
	public BigDecimal getTempPaymentCurrencyAmount(int paymentBrokerRefNo) throws ModuleException {
		return PaymentGatewayBO.getTempPaymentCurrencyAmount(paymentBrokerRefNo);
	}

	/**
	 * Performs a reverse payment
	 * 
	 * @param colPaymentInfo
	 * @param trackInfoDTO
	 * @param triggeredExceptionCode
	 * @param colTnxIds
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void reversePayment(Collection<PaymentInfo> colPaymentInfo, TrackInfoDTO trackInfoDTO, String triggeredExceptionCode,
			Collection<Integer> colTnxIds) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			PaymentGatewayBO.performReversePayment(colPaymentInfo, credentialsDTO, triggeredExceptionCode, colTnxIds);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reversePayment ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reversePayment ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Transfer Reservation Segment
	 * 
	 * @param pnr
	 * @param pnrSegIdAndNewFlgSegId
	 * @param isAlert
	 * @param version
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce transferReservationSegment(String pnr, Map<Integer, Integer> pnrSegIdAndNewFlgSegId, boolean isAlert,
			boolean noReprotectNoReturn, long version, boolean isSegmentTransferredByNorecProcess,
			List<Integer> reprotectFltSegIds, String visibleIBEonly, String salesChannelKey) throws ModuleException {

		try {

			CredentialsDTO credentialsDTO = getCallerCredentials(null);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.TRANSFER_RESERVATION_SEGMENT);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PNR_FLIGHT_SEG_MAP, pnrSegIdAndNewFlgSegId);
			command.setParameter(CommandParamNames.IS_ALEART, new Boolean(isAlert));
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.SEAT_NO_REPROTECT_No_RETURN, new Boolean(noReprotectNoReturn));
			command.setParameter(CommandParamNames.IS_SEGMENT_TRANSFERRED_BY_NOREC_PROCESS, isSegmentTransferredByNorecProcess);
			command.setParameter(CommandParamNames.REPROTECT_FLT_SEG_IDS, reprotectFltSegIds);
			command.setParameter(CommandParamNames.VISIBLE_IBE_ONLY, visibleIBEonly);
			command.setParameter(CommandParamNames.SALES_CHANNEL_KEY, salesChannelKey);
			return command.execute();

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::transferReservationSegment ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::transferReservationSegment ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Alert reservations
	 * 
	 * @param reservationAlertDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce alertReservations(ReservationAlertDTO reservationAlertDTO) throws ModuleException {
		try {
			log.debug("Inside alertReservations");

			if (reservationAlertDTO == null || reservationAlertDTO.getColFlightReservationAlertDTO() == null
					|| reservationAlertDTO.getFlightAlertDTO() == null) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}

			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			Iterator<FlightReservationAlertDTO> itColFlightReservationAlertDTO = reservationAlertDTO
					.getColFlightReservationAlertDTO().iterator();
			FlightReservationAlertDTO flightReservationAlertDTO;
			Collection<FlightReservationAlertDTO> cancelFlights = new ArrayList<FlightReservationAlertDTO>();
			Collection<FlightReservationAlertDTO> reScheduleFlights = new ArrayList<FlightReservationAlertDTO>();
			Collection<FlightReservationAlertDTO> seatChnageFlights = new ArrayList<FlightReservationAlertDTO>();
			Collection<FlightReservationAlertDTO> changeFlights = new ArrayList<FlightReservationAlertDTO>();
			// Identifying the different sets
			while (itColFlightReservationAlertDTO.hasNext()) {
				flightReservationAlertDTO = itColFlightReservationAlertDTO.next();

				if (ReservationInternalConstants.ReservationAlertTypes.CANCEL_FLIGHT
						.equals(flightReservationAlertDTO.getReservationAlertType())) {
					cancelFlights.add(flightReservationAlertDTO);
				} else if (ReservationInternalConstants.ReservationAlertTypes.RESCHEDULE_FLIGHT
						.equals(flightReservationAlertDTO.getReservationAlertType())) {
					reScheduleFlights.add(flightReservationAlertDTO);
				}
				if (flightReservationAlertDTO.getFlightSeatChangesInfo() != null) {
					seatChnageFlights.add(flightReservationAlertDTO);
					Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ALERT_RESERVATIONS_MACRO);

					command.setParameter(CommandParamNames.FLIGHT_RESERVATION_ALERT_DTOS, changeFlights);
					command.setParameter(CommandParamNames.FLIGHT_RESERVATION_ALERT_DTOS_SEAT_LOST, seatChnageFlights);
					command.setParameter(CommandParamNames.IS_CANCEL_FLIGHT, new Boolean(false));
					command.setParameter(CommandParamNames.IS_RESCHEDULE_FLIGHT, new Boolean(true));
					command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, reservationAlertDTO.getFlightAlertDTO());
					command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

					command.execute();
				}
			}

			// Holds the error effected flight segment data transfer information
			Collection colErrorEffectedFlightSegmentDTO = new ArrayList();

			// For cancel flights
			if (cancelFlights.size() > 0) {
				Iterator<FlightReservationAlertDTO> itCancelFlights = cancelFlights.iterator();
				while (itCancelFlights.hasNext()) {
					flightReservationAlertDTO = itCancelFlights.next();
					Object[] object = this.locateOndsAndCancel(flightReservationAlertDTO.getFlightSegments(),
							AccelAeroCalculator.getDefaultBigDecimalZero(), null);
					flightReservationAlertDTO.setColEffectedFlightSegmentDTO((Collection<EffectedFlightSegmentDTO>) object[0]);
					colErrorEffectedFlightSegmentDTO.addAll((Collection) object[1]);
				}

				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ALERT_RESERVATIONS_MACRO);

				command.setParameter(CommandParamNames.FLIGHT_RESERVATION_ALERT_DTOS, cancelFlights);
				command.setParameter(CommandParamNames.IS_CANCEL_FLIGHT, new Boolean(true));
				command.setParameter(CommandParamNames.IS_RESCHEDULE_FLIGHT, new Boolean(false));
				command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, reservationAlertDTO.getFlightAlertDTO());
				command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

				command.execute();
			}

			// For re schedule flights
			if (reScheduleFlights.size() > 0) {
				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ALERT_RESERVATIONS_MACRO);

				command.setParameter(CommandParamNames.FLIGHT_RESERVATION_ALERT_DTOS, reScheduleFlights);
				command.setParameter(CommandParamNames.IS_CANCEL_FLIGHT, new Boolean(false));
				command.setParameter(CommandParamNames.IS_RESCHEDULE_FLIGHT, new Boolean(true));
				command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, reservationAlertDTO.getFlightAlertDTO());
				command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

				command.execute();
			}

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(true);
			defaultServiceResponse.addResponceParam(ResponseCodes.EFFECTED_ERROR_FLIGHT_SEGMENT_INFORMATION,
					colErrorEffectedFlightSegmentDTO);

			log.debug("Exit alertReservations");
			return defaultServiceResponse;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::alertReservations ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::alertReservations ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Get flight manifest details
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String viewFlightManifest(FlightManifestOptionsDTO manifestOptions) throws ModuleException {
		try {
			return FlightManifestBL.viewFlightManifest(manifestOptions);
		} catch (Exception ex) {
			log.error(" ((o)) ModuleException::viewFlightManifest ", ex);
			throw new ModuleException(ex.getMessage());
		}
	}

	/**
	 * Send Flight Mainfest as email
	 * 
	 * @param flightId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Boolean sendFlightManifestAsEmail(FlightManifestOptionsDTO manifestOptions) throws ModuleException {
		try {
			return FlightManifestBL.sendFlightManifestAsEmail(manifestOptions);
		} catch (Exception ex) {
			log.error(" ((o)) ModuleException::alertReservations ", ex);
			throw new ModuleException(ex.getMessage());
		}
	}

	/**
	 * Reconcile PFS Reservations
	 * 
	 * @param pfsId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce reconcileReservations(int pfsId, boolean isCheckinProcess, boolean isProcessPFS, String checkInMethod,
			boolean isConsiderFutureDateValidation) throws ModuleException {
		try {
			// Retrieve the called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECONCILE_RESERVATION);
			command.setParameter(CommandParamNames.PFS_ID, new Integer(pfsId));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.IS_CHECKIN_PROCESS, isCheckinProcess);
			command.setParameter(CommandParamNames.IS_PROCESS_PFS, isProcessPFS);
			command.setParameter(CommandParamNames.PFS_CHECKIN_METHOD, checkInMethod);
			command.setParameter(CommandParamNames.IS_CONSIDER_FUTURE_DATE_VALIDATION, isConsiderFutureDateValidation);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reconcileReservations ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reconcileReservations ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Reconcile XA PNL Reservations
	 * 
	 * @param xaPnlId
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce reconcileXAPnlReservations(int xaPnlId) throws ModuleException {
		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECONCILE_X_A_PNL_RESERVATION);
			command.setParameter(CommandParamNames.XA_PNL_ID, new Integer(xaPnlId));

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reconcileXAPnlReservations ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reconcileXAPnlReservations ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns pfs entry
	 * 
	 * @param pfsID
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Pfs getPFS(int pfsID) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.getPFS(pfsID);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPFS ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * Returns pfs entry paged data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getPagedPFSData(List<ModuleCriterion> criteria, int startIndex, int noRecs, List<String> orderByFieldList)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.getPagedPFSData(criteria, startIndex, noRecs,
					orderByFieldList);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPagedPFSData ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Pfs entry
	 * 
	 * @param action
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void savePfsEntry(Pfs pfs) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			int pfsCount = 0;
			if (pfs.getFlightNumber() != null && pfs.getFromAirport() != null && pfs.getDepartureDate() != null) {
				pfsCount = paxFinalSalesDAO.getPfsEntriesCount(pfs.getFlightNumber(), pfs.getFromAirport(),
						pfs.getDepartureDate());
			}

			if (pfsCount == 0) {
				paxFinalSalesDAO.savePfsEntry(pfs);
			} else {
				throw new ModuleException("airreservations.arg.pfsEntry.duplicate");
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::savePfsEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Pfs entry
	 * 
	 * @param action
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void savePfs(Pfs pfs) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			paxFinalSalesDAO.savePfsEntry(pfs);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::savePfsEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Pfs entry and returns the ppId
	 * 
	 * @param pfs
	 * @return ppId
	 * @throws ModuleException
	 */

	@Override
	public int savePfsEntryForAuditing(Pfs pfs) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			int ppId = 0;
			int pfsCount = 0;
			if (pfs.getFlightNumber() != null && pfs.getFromAirport() != null && pfs.getDepartureDate() != null) {
				pfsCount = paxFinalSalesDAO.getPfsEntriesCount(pfs.getFlightNumber(), pfs.getFromAirport(),
						pfs.getDepartureDate());
			}

			if (pfsCount == 0) {
				ppId = paxFinalSalesDAO.savePfsEntryForAudit(pfs);
			} else {
				throw new ModuleException("airreservations.arg.pfsEntry.duplicate");
			}

			return ppId;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::savePfsEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Pfs entry
	 * 
	 * @param action
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int createPfs(Pfs pfs) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			paxFinalSalesDAO.savePfsEntry(pfs);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::savePfsEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return pfs.getPpId();
	}

	/**
	 * Returns pfs parse entries
	 * 
	 * @param pfsId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PfsPaxEntry> getPfsParseEntries(int pfsId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.getPfsParseEntries(pfsId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPfsParseEntries ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save pfs parse entry
	 * 
	 * @param action
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void savePfsParseEntry(Pfs pfs, PfsPaxEntry pfsParsed) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			if ((pfsParsed.getVersion() == -1)
					&& (!ReservationInternalConstants.PfsStatus.PARSED.equals(pfs.getProcessedStatus()))) {
				// Adding PAX to a PFS record that hasn't got "PARSED" status
				// Make the PFS parsed
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.PARSED);
				paxFinalSalesDAO.savePfsEntry(pfs);
			}

			paxFinalSalesDAO.savePfsParseEntry(pfsParsed);
			if (pfsParsed.getParentPpId() != null) {
				paxFinalSalesDAO.saveParent(pfsParsed);
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::savePfsParseEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void savePfsParseEntry(PfsPaxEntry pfsParsed) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			paxFinalSalesDAO.savePfsParseEntry(pfsParsed);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::savePfsParseEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Delete the pfs parse entry
	 * 
	 * @param pfs
	 * @param pfsParsed
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deletePfsParseEntry(Pfs pfs, PfsPaxEntry pfsParsed) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			paxFinalSalesDAO.deletePfsParseEntry(pfsParsed);

			int pfsPaxCount = paxFinalSalesDAO.getPfsParseEntriesCount(pfs.getPpId(), null);

			if (pfsPaxCount == 0) {
				// All the PAX deleted for the PFS
				// Set the PFS status to UNPARSED
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED);
				paxFinalSalesDAO.savePfsEntry(pfs);
			} else if (paxFinalSalesDAO.getPfsParseEntriesCount(pfs.getPpId(),
					ReservationInternalConstants.PfsProcessStatus.PROCESSED) == pfsPaxCount) {
				// All the PAX are processed
				// Set the PFS status to Reconciled
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.RECONCILE_SUCCESS);
				paxFinalSalesDAO.savePfsEntry(pfs);
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::deletePfsParseEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Delete the pfs entry
	 * 
	 * @param pfs
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deletePfsEntry(Pfs pfs) throws ModuleException {
		try {
			AuditorBD auditorBD = MessagingModuleUtils.getAuditorBD();
			auditorBD.deleteAuditsByID(pfs.getPpId());

			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			paxFinalSalesDAO.deletePfsEntry(pfs);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::deletePfsEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return pfs parse entries count
	 * 
	 * @param pfsId
	 * @param processedStatus
	 * 
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int getPfsParseEntryCount(int pfsId, String processedStatus) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			return paxFinalSalesDAO.getPfsParseEntriesCount(pfsId, processedStatus);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPfsParseEntryCount ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns a collection of FlightSegmentDTO
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<FlightReconcileDTO> getPnrSegmentsForReconcilation(String flightNumber, Date departureDate,
			String fromSegmentCode, String toSegmentCode, String pnr) throws ModuleException {
		try {
			ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

			Collection<Integer> flightSegId = reservationSegmentDAO.getFlightSegmentIds(flightNumber, departureDate,
					fromSegmentCode, toSegmentCode);

			return reservationSegmentDAO.getPnrSegmentsForReconcilation(flightSegId, pnr, false);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrSegmentsForReconcilation ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public FlightReconcileDTO getFlightSegmentsForReconcilation(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws ModuleException {

		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		Integer flightSegId = reservationSegmentDAO.getFlightSegmentId(flightNumber, departureDate, fromSegmentCode,
				toSegmentCode);

		FlightSegement fs = ReservationModuleUtils.getFlightBD().getFlightSegment(flightSegId);

		FlightReconcileDTO flightReconcileDTO = new FlightReconcileDTO();
		flightReconcileDTO.setFlightSegId(fs.getFltSegId());
		flightReconcileDTO.setFlightId(fs.getFlightId());
		flightReconcileDTO.setSegementCode(fs.getSegmentCode());
		flightReconcileDTO.setDepartureDateTimeLocal(fs.getEstTimeDepatureLocal());
		flightReconcileDTO.setDepartureDateTimeZulu(fs.getEstTimeDepatureZulu());

		return flightReconcileDTO;
	}

	/**
	 * Returns the correct flight segment id
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Integer getFlightSegmentId(String flightNumber, Date departureDate, String fromSegmentCode, String toSegmentCode)
			throws ModuleException {
		try {
			ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

			return reservationSegmentDAO.getFlightSegmentId(flightNumber, departureDate, fromSegmentCode, toSegmentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightSegmentId ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Download pfs messages from mail server
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void downloadPfsMessagesFromMailServer() throws ModuleException {
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		String processPath = airReservationConfig.getPfsProcessPath();

		String defaultMailServer = airReservationConfig.getPopMailServer();
		String username = airReservationConfig.getPfsMailServerUsername();
		String password = airReservationConfig.getPfsMailServerPassword();

		String ttyMailServer = airReservationConfig.getTtyMailServer();
		String ttyUserName = airReservationConfig.getTtyMailServerUserName();
		String ttyPassword = airReservationConfig.getTtyMailServerPassword();

		boolean isDefinedTTYMailSrever = !BeanUtils.nullHandler(ttyMailServer).isEmpty()
				&& !BeanUtils.nullHandler(ttyUserName).isEmpty() && !BeanUtils.nullHandler(ttyPassword).isEmpty();

		boolean isFailedLoginToMailServer = false;
		boolean fetchStatus = airReservationConfig.getPfsFetchFromMailServer().equalsIgnoreCase("true") ? true : false;
		boolean deleteStatus = airReservationConfig.getPfsDeleteFromMailServer().equalsIgnoreCase("true") ? true : false;

		try {
			// If specified to fetch emails then fetching emails
			if (fetchStatus) {
				try {
					if (!defaultMailServer.isEmpty() && !username.isEmpty() && !password.isEmpty()) {
						Pop3Client.getMessages(processPath, defaultMailServer, username, password,
								ReservationInternalConstants.PopMessageType.PFS_MESSAGE, deleteStatus);
					}
				} catch (ModuleException ex) {
					isFailedLoginToMailServer = true;
					log.error(" ((o)) ModuleException::downloadETLMessagesFromMailServer ", ex);
					if (!isDefinedTTYMailSrever) {
						throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
					}
				} catch (CommonsDataAccessException cdaex) {
					isFailedLoginToMailServer = true;
					log.error(" ((o)) CommonsDataAccessException::downloadETLMessagesFromMailServer ", cdaex);
					if (!isDefinedTTYMailSrever) {
						throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
					}
				}
				try {
					if (isDefinedTTYMailSrever) {
						Pop3Client.getMessages(processPath, ttyMailServer, ttyUserName, ttyPassword,
								ReservationInternalConstants.PopMessageType.PFS_MESSAGE, deleteStatus);
					}
				} catch (ModuleException ex) {
					log.error(" ((o)) ModuleException::downloadETLMessagesFromMailServer ", ex);
					if (isFailedLoginToMailServer) {
						throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
					}
				} catch (CommonsDataAccessException cdaex) {
					log.error(" ((o)) CommonsDataAccessException::downloadETLMessagesFromMailServer ", cdaex);
					if (isFailedLoginToMailServer) {
						throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
					}
				}
			} else {
				log.info(" FetchPfsFromMailServer IS SET TO FALSE THEREFORE NOT FETCHING ANY EMAILS ");
			}
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::downloadPfsMessagesFromMailServer ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::downloadPfsMessagesFromMailServer ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Download xapnl messages from mail server
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void downloadXAPnlMessagesFromMailServer() throws ModuleException {
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		String processPath = airReservationConfig.getXaPnlProcessPath();
		String defaultMailServer = airReservationConfig.getPopMailServer();
		String username = airReservationConfig.getXaPnlMailServerUsername();
		String password = airReservationConfig.getXaPnlMailServerPassword();
		boolean fetchStatus = airReservationConfig.getXaFetchPnlFromMailServer().equalsIgnoreCase("true") ? true : false;
		boolean deleteStatus = airReservationConfig.getXaDeleteFromMailServer().equalsIgnoreCase("true") ? true : false;

		try {
			// If specified to fetch emails then fetching emails
			if (fetchStatus) {
				Pop3Client.getMessages(processPath, defaultMailServer, username, password,
						ReservationInternalConstants.PopMessageType.PNL_MESSAGE, deleteStatus);
			} else {
				log.info(" FetchXAPnlFromMailServer IS SET TO FALSE THEREFORE NOT FETCHING ANY EMAILS ");

			}
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::downloadXAPnlMessagesFromMailServer ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::downloadXAPnlMessagesFromMailServer ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Parse pfs document
	 * 
	 * @param strMsgName
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<Integer, PFSMetaDataDTO> parsePfsDocument(String strMsgName) throws ModuleException {
		Map<Integer, PFSMetaDataDTO> parsedMetaPfs = new HashMap<Integer, PFSMetaDataDTO>();

		try {
			parsedMetaPfs = PFSDTOParser.insertPfsToDatabase(strMsgName);
		} catch (ModuleException me) {
			this.sessionContext.setRollbackOnly();
			// Error pfs documents will there as it is...
			// So later they can correct it and fix it
			log.error("################ Error on PFS document named " + strMsgName + " will remain as it is... ");
			log.error("################ Error on insert PFS to database ", me);
			throw me;
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			// Error pfs documents will there as it is...
			// So later they can correct it and fix it
			log.error("################ Error on PFS document named " + strMsgName + " will remain as it is... ");
			log.error("################ Error on insert PFS to database ", cdaex);
			throw cdaex;
		}

		return parsedMetaPfs;
	}

	/**
	 * Parse pnl document
	 * 
	 * @param strMsgName
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer parseXAPnlDocument(Collection<String> msgNamesThatMayHavePartTwoThree, String strMsgName)
			throws ModuleException {
		Integer pnlId = null;

		try {
			pnlId = new Integer(PNLDTOParser.insertPnlToDatabase(msgNamesThatMayHavePartTwoThree, strMsgName));
		} catch (ModuleException me) {
			this.sessionContext.setRollbackOnly();
			// Error pnl documents will there as it is...
			// So later they can correct it and fix it
			log.error("################ Error on PNL document named " + strMsgName + " will remain as it is... ");
			log.error("################ Error on insert PNL to database ", me);
			throw me;
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			// Error pnl documents will there as it is...
			// So later they can correct it and fix it
			log.error("################ Error on PNL document named " + strMsgName + " will remain as it is... ");
			log.error("################ Error on insert PNL to database ", cdaex);
			throw cdaex;
		}

		return pnlId;
	}

	/**
	 * Returns validated pfs documents
	 * 
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> getValidatedPfsDocuments() {
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		String pfsProcessPath = airReservationConfig.getPfsProcessPath();
		String pfsErrorPath = airReservationConfig.getPfsErrorPath();
		String partialDownloadPath = airReservationConfig.getPfsPartialDownloadPath();

		// Validate the messages
		Map<String, String> pfsContents = PFSFormatUtils.validateMessages(pfsProcessPath, pfsErrorPath, partialDownloadPath);

		// Return the message names
		return PFSFormatUtils.getMessageNames(pfsProcessPath, pfsContents);
	}

	/**
	 * Returns validated xaPnl documents
	 * 
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String> getValidatedXAPnlDocuments() {
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		String xaPnlProcessPath = airReservationConfig.getXaPnlProcessPath();
		String xaPnlErrorPath = airReservationConfig.getXaPnlErrorPath();

		// Validate the messages
		XAPNLFormatUtils.validateMessages(xaPnlProcessPath, xaPnlErrorPath);

		// Return the message names
		return XAPNLFormatUtils.getMessageNames(xaPnlProcessPath);
	}

	/**
	 * Prcoess automatic reconcilation
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce processAutomaticReconcilation() throws ModuleException {

		synchronized (this) {

			final String generalLogFileName = "Pfs-General-Failure " + new Date().toString();
			Throwable exceptionToLog = null;
			HashMap<String, Throwable> fileExceptionMap = new HashMap<String, Throwable>();
			Map<String, String> colMsgNames = null;
			// AuditorBD auditorBD = null;
			log.info("######### AUTOMATIC PFS RECONCILLATION PROCESS STARTED ##########");
			String pfsProcessPath = ReservationModuleUtils.getAirReservationConfig().getPfsProcessPath();
			String pfsParsedPath = ReservationModuleUtils.getAirReservationConfig().getPfsParsedPath();
			String pfsErrorPath = ReservationModuleUtils.getAirReservationConfig().getPfsErrorPath();

			try {
				// Download PFS Messages
				ReservationModuleUtils.getReservationBD().downloadPfsMessagesFromMailServer();
				colMsgNames = ReservationModuleUtils.getReservationBD().getValidatedPfsDocuments();
				Iterator<String> itColMsgNames = colMsgNames.keySet().iterator();
				String strMsgName;
				Integer pfsId;
				PFSMetaDataDTO pfsMetaDataDTO = new PFSMetaDataDTO();

				Map<Integer, PFSMetaDataDTO> parsedMetaPfs = new HashMap<Integer, PFSMetaDataDTO>();

				while (itColMsgNames.hasNext()) {
					strMsgName = itColMsgNames.next();

					try {
						log.info("######### BEFORE PARSING PFS FILE :  ##########  " + strMsgName);
						parsedMetaPfs = ReservationModuleUtils.getReservationBD().parsePfsDocument(strMsgName);
						if (parsedMetaPfs.size() != 0) {
							Map.Entry<Integer, PFSMetaDataDTO> entry = parsedMetaPfs.entrySet().iterator().next();
							pfsId = entry.getKey();
							pfsMetaDataDTO = entry.getValue();
						} else {
							pfsId = null;
						}
					} catch (ModuleException e) {
						log.error("########################## ERROR AFTER PARSING FOR " + strMsgName + e.getLocalizedMessage(),
								e);
						fileExceptionMap.put(strMsgName, exceptionToLog = e);
						continue;
					} catch (Exception e) {
						log.error("########################## ERROR AFTER PARSING FOR " + strMsgName + e.getLocalizedMessage(),
								e);
						fileExceptionMap.put(strMsgName, exceptionToLog = e);
						continue;
					}

					// This means the document is already parsed successfully
					if (pfsId != null) {
						Throwable xception = null;
						try {
							if (pfsMetaDataDTO != null) {
								Airport airport = ReservationModuleUtils.getAirportBD()
										.getAirport(pfsMetaDataDTO.getBoardingAirport());
								if (AppSysParamsUtil.isProcessPFSAfterETLProcessed() && airport.etlProcessEnabled()) {
									String pfsFlightNumber = pfsMetaDataDTO.getActualFlightNumber();
									Date pfsDepDate = pfsMetaDataDTO.getRealDepartureDate();
									Collection<String> processedStatus = new ArrayList<String>();
									processedStatus.add(ParserConstants.ETLProcessStatus.PROCESSED);
									processedStatus.add(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
									boolean hasETL = ReservationModuleUtils.getEtlBD().hasETL(pfsFlightNumber, pfsDepDate,
											airport.getAirportCode(), processedStatus);
									if (hasETL) { // ETL already processed, so continue PFS processing
										log.info("######### BEFORE RECONCILING PFS FILE :  ##########  " + strMsgName
												+ "PFS_ID : " + pfsId);
										ReservationModuleUtils.getReservationBD().reconcileReservations(pfsId.intValue(), false,
												true, null, AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
										log.info("######### AFTER RECONCILING PFS FILE :  ##########  " + strMsgName + "PFS_ID : "
												+ pfsId);
									} else { // Halt PFS processing until process ETL
										log.info("######### PFS FILE :  ##########  " + strMsgName + "PFS_ID : " + pfsId
												+ " IS UPDATED AS WAITING");
										Pfs pfs = ReservationModuleUtils.getReservationBD().getPFS(pfsId.intValue());
										pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.WAITING_FOR_ETL_PROCESS);
										ReservationModuleUtils.getReservationBD().savePfs(pfs);
									}
								} else { // ETL will not process in this airport, so continue PFS processing
									log.info("######### BEFORE RECONCILING PFS FILE :  ##########  " + strMsgName);
									ReservationModuleUtils.getReservationBD().reconcileReservations(pfsId.intValue(), false, true,
											null, AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
									log.info("######### AFTER RECONCILING PFS FILE :  ##########  " + strMsgName);
								}
							} else { // Empty PFS will process here
								log.info("######### BEFORE RECONCILING PFS FILE :  ##########  " + strMsgName);
								ReservationModuleUtils.getReservationBD().reconcileReservations(pfsId.intValue(), false, true,
										null, AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
								log.info("######### AFTER RECONCILING PFS FILE :  ##########  " + strMsgName);
							}

						} catch (ModuleException e) {
							log.error(" ERROR OCCURED WHILE RECONCILING PFS ID : " + pfsId + " DATE :" + new Date(), e);

							xception = e;

						} catch (Exception e) {
							log.error(" ERROR OCCURED WHILE RECONCILING PFS ID : " + pfsId + " DATE :" + new Date(), e);
							xception = e;

						} finally {
							if (xception != null) {
								PFSFormatUtils.moveAndDeletePfs(pfsProcessPath, pfsErrorPath, strMsgName);
							} else {
								PFSFormatUtils.moveAndDeletePfs(pfsProcessPath, pfsParsedPath, strMsgName);
							}
							PfsMailError.notifyError(pfsId, xception, strMsgName);
						}

					}
				}
			} catch (ModuleException ex) {
				fileExceptionMap.put(generalLogFileName, exceptionToLog = ex);
				this.sessionContext.setRollbackOnly();
				log.error(" ((o)) ModuleException::processAutomaticReconcilation ", ex);
				throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
			} catch (CommonsDataAccessException cdaex) {
				fileExceptionMap.put(generalLogFileName, exceptionToLog = cdaex);
				this.sessionContext.setRollbackOnly();
				log.error(" ((o)) CommonsDataAccessException::processAutomaticReconcilation ", cdaex);
				throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
			} finally {
				// email for each file will be created
				if (exceptionToLog != null) {
					log.info("########################## PFS ERRORS EXIST STARTING TO LOG   ##########");
					Iterator<String> iter = fileExceptionMap.keySet().iterator();
					PfsLogDTO logDTO;

					while (iter.hasNext()) {
						String fileName = iter.next();
						PFSFormatUtils.moveAndDeletePfs(pfsProcessPath, pfsErrorPath, fileName);
						Throwable xceptionToLog = fileExceptionMap.get(fileName);

						logDTO = new PfsLogDTO();
						if (colMsgNames != null && colMsgNames.get(fileName) != null) {
							logDTO.setFileName(fileName);
							logDTO.setPfsContent(colMsgNames.get(fileName));
						} else {
							logDTO.setFileName(fileName);
						}
						logDTO.setStackTraceElements(xceptionToLog.getStackTrace());
						logDTO.setExceptionDescription(xceptionToLog.getMessage());

						if (xceptionToLog instanceof ModuleException) {
							ModuleException me = (ModuleException) xceptionToLog;

							// do not send mail for this..as mail is already
							// sent.
							if (me.getExceptionCode().equals("airreservations.pfs.cannotParsePfs")) {
								continue;
							}

							logDTO.setCustomDescription(me.getMessageString());

						}
						PfsMailError.notifyError(logDTO);

					}
					log.info("########################## PFS ERRORS EXIST ... FINISHED  MAILING   ##########");
				}
				// PFSFormatUtils.moveAndDeleteAnyRemainingPfs(pfsProcessPath,pfsParsedPath);
			}
			log.info("######### AUTOMATIC RECONCILLATION PROCESS ENDED ##########");

			return new DefaultServiceResponse(true);
		}
	}

	public PFSAuditDTO createPfsAuditDTO() throws ModuleException {
		PFSAuditDTO pfsAuditDTO = new PFSAuditDTO(null, getCallerCredentials(null).getUserId(), null, null);
		return pfsAuditDTO;
	}

	/**
	 * Prcoess Parsed PFS reconcilation
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce processParsedPFSReconcilation() throws ModuleException {
		String pfsProcessPath = ReservationModuleUtils.getAirReservationConfig().getPfsProcessPath();
		String pfsParsedPath = ReservationModuleUtils.getAirReservationConfig().getPfsParsedPath();
		String pfsErrorPath = ReservationModuleUtils.getAirReservationConfig().getPfsErrorPath();

		Integer maxPassedPeriodDates = 3;

		String strMsgName = "";

		log.info("######### AUTOMATIC RECONCILLATION PARSED PFS PROCESS STARTED ##########");

		Map<Integer, PFSMetaDataDTO> waitingParsedPFS = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO
				.getWaitingPFSList(maxPassedPeriodDates);
		if (waitingParsedPFS.size() > 0) {
			for (Entry<Integer, PFSMetaDataDTO> entry : waitingParsedPFS.entrySet()) {
				Integer pfsId = entry.getKey();
				Throwable tmpException = null;
				try {
					PFSMetaDataDTO pfsMetaDataDTO = entry.getValue();
					Collection<String> processedStatus = new ArrayList<String>();
					processedStatus.add(ParserConstants.ETLProcessStatus.PROCESSED);
					processedStatus.add(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
					boolean hasETL = ReservationModuleUtils.getEtlBD().hasETL(pfsMetaDataDTO.getFlightNumber(),
							pfsMetaDataDTO.getRealDepartureDate(), pfsMetaDataDTO.getBoardingAirport(), processedStatus);
					if (hasETL) {
						strMsgName = PFSFormatUtils.getPfsFileName(pfsMetaDataDTO.getDateDownloaded(), 1);
						log.info("######### BEFORE RECONCILING PARSED PFS FILE :  ##########  " + strMsgName + " PFS_ID : "
								+ pfsId);
						ReservationModuleUtils.getReservationBD().reconcileReservations(pfsId.intValue(), false, true, null,
								AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
						log.info("######### AFTER RECONCILING PARSED PFS FILE :  ##########  " + strMsgName + " PFS_ID : "
								+ pfsId);
					}
				} catch (ModuleException e) {
					log.error(" ERROR OCCURED WHILE RECONCILING PARSED PFS ID : " + pfsId + " DATE :" + new Date(), e);

					tmpException = e;

				} catch (Exception e) {
					log.error(" ERROR OCCURED WHILE RECONCILING PARSED PFS ID : " + pfsId + " DATE :" + new Date(), e);
					tmpException = e;

				} finally {
					if (tmpException != null) {
						PFSFormatUtils.moveAndDeletePfs(pfsProcessPath, pfsParsedPath, strMsgName);
					} else {
						PFSFormatUtils.moveAndDeletePfs(pfsProcessPath, pfsErrorPath, strMsgName);
					}
					PfsMailError.notifyError(pfsId, tmpException, strMsgName);
				}

			}

		}
		log.info("######### AUTOMATIC RECONCILLATION PARSED PFS PROCESS ENDED ##########");

		return new DefaultServiceResponse(true);
	}

	/**
	 * Get Failed XA Pnl Ids. Will update all "E" to "N"
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection<Integer> getFailedXAPnlIds() throws ModuleException {
		return PNLDTOParser.getFailedXAPnls();
	}

	/**
	 * Audit credit card failures
	 * 
	 * @param pnr
	 * @param amount
	 * @param errorFunction
	 * @param errorDescription
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void auditCreditCardFailures(String pnr, BigDecimal amount, String errorFunction, String errorDescription,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);

			ReservationAudit reservationAudit = new ReservationAudit();
			reservationAudit.setModificationType(AuditTemplateEnum.PAYMENT_ERROR.getCode());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentError.AMOUNT, amount.toString());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentError.ERROR_FUNCTION, errorFunction);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentError.ERROR_DESCRIPTION, errorDescription);

			// Setting the ip address
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentError.IP_ADDDRESS,
						credentialsDTO.getTrackInfoDTO().getIpAddress());
			}

			// Setting the origin carrier code
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentError.ORIGIN_CARRIER,
						credentialsDTO.getTrackInfoDTO().getCarrierCode());
			}

			// Record the modification
			ReservationBO.recordModification(pnr, reservationAudit, null, credentialsDTO);
		} catch (ModuleException ex) {
			this.sessionContext.setRollbackOnly();
			log.error(" ((o)) ModuleException::auditCreditCardFailures ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error(" ((o)) CommonsDataAccessException::auditCreditCardFailures ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save/Update External Payment Transaction Information
	 * 
	 * @param externalPaymentTnx
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveOrUpdateExternalPayTxInfo(ExternalPaymentTnx externalPaymentTnx) throws ModuleException {
		try {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveOrUpdateExternalPayTxInfo(externalPaymentTnx);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveOrUpdateExternalPayTxInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns all the external payment transactions for given PNR for front-end display.
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PNRExtTransactionsTO getPNRExtPayTransactions(String pnr) throws ModuleException {
		try {
			ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
			criteriaDTO.setPnr(pnr);

			Map<String, PNRExtTransactionsTO> transactionsMap = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getExtPayTransactions(criteriaDTO);
			PNRExtTransactionsTO pnrExtTransactionsTO = null;
			if (transactionsMap != null) {
				pnrExtTransactionsTO = transactionsMap.get(pnr);
			}
			return pnrExtTransactionsTO;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPNRExtPayTransactions ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return external payment transactions for the specified criteria.
	 * 
	 * @param criteriaDTO
	 * @return Map<PNR, PNRExtTransactionsTO>
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, PNRExtTransactionsTO> getExtPayTransactions(ExtPayTxCriteriaDTO criteriaDTO) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getExtPayTransactions(criteriaDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveOrUpdateExternalPayTxInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Reconciles reservation payments as per the bank payments for a given transaction.
	 * 
	 * @param externalPaymentTnx
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce reconcileExtPayTransaction(ExternalPaymentTnx externalPaymentTnx, String onAccAgentCode,
			boolean isManualRecon) throws ModuleException {
		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECON_EXT_PAY_TRANSACTION);
			command.setParameter(CommandParamNames.EXTERNAL_PAY_TX_INFO, externalPaymentTnx);
			command.setParameter(CommandParamNames.ONACC_AGENTCODE_FOR_EXT_PAY, onAccAgentCode);
			command.setParameter(CommandParamNames.TRIGGER_EXTERNAL_PAY_TX_MANUAL_RECON, new Boolean(isManualRecon));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, getCallerCredentials(null));

			return command.execute();
		} catch (Exception ex) {
			log.error(" ((o)) Exception::reconcileExtPayTransaction ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, "airreservations.reconExtPay.failed");
		}
	}

	/**
	 * Reconciles reservation payments as per the bank payments for given transactions.
	 * 
	 * @param pnrExtTransactionsTOs
	 *            collection of PNRExtTransactionsTO
	 * @param onAccAgentCode
	 *            Agent code on which onaccount transactions are recorded
	 * @param isManualRecon
	 *            Specify whether manual or scheduled reconciliation
	 * @param startTimestamp
	 *            Specifies the start timestamp when called for daily transaction reconciliation
	 * @param endTimestamp
	 *            Specifies the end timestamp when called for daily transaction reconciliation
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce reconcileExtPayTransactions(Collection<PNRExtTransactionsTO> pnrExtTransactionsTOs,
			boolean isManualRecon, Date startTimestamp, Date endTimestamp, String agencyCode) throws ModuleException {
		try {
			log.info("Starting external payment reconciliation at [Time = " + new Date(System.currentTimeMillis())
					+ ",agencyCode=" + agencyCode + "]");
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			if (isManualRecon) {
				log.info("Manual reconciliation [userID=" + credentialsDTO.getUserId() + "]");
			}

			Collection<String> reconciledBalQueryKeys = new ArrayList<String>();
			boolean isExceptionOccured = false;

			if (pnrExtTransactionsTOs != null && pnrExtTransactionsTOs.size() > 0) {
				// Map onAccAgentsMap =
				// ReservationModuleUtils.getAirReservationConfig().getOnAccAgentForBankChannelMap();
				String onAccAgentCode = agencyCode;
				for (PNRExtTransactionsTO pnrExtTransactionsTO2 : pnrExtTransactionsTOs) {
					try {
						PNRExtTransactionsTO pnrExtTransactionsTO = pnrExtTransactionsTO2;
						if (log.isDebugEnabled()) {
							log.debug("Before calliing Recon ::" + pnrExtTransactionsTO != null
									? pnrExtTransactionsTO.getSummary()
									: "null");
						}
						if (pnrExtTransactionsTO.getExtPayTransactions() != null) {
							for (ExternalPaymentTnx externalPaymentTnx2 : pnrExtTransactionsTO.getExtPayTransactions()) {
								ExternalPaymentTnx externalPaymentTnx = externalPaymentTnx2;
								// onAccAgentCode = (String) onAccAgentsMap.get(externalPaymentTnx.getChannel());

								try {
									// if (onAccAgentCode == null) {
									// log.error("External payment tnx reconciliation failed. " +
									// "Invalid sales channel or Agency for sales channel is not configured. " +
									// "[pnr = " + externalPaymentTnx.getPnr() +
									// ",balQKey=" + externalPaymentTnx.getBalanceQueryKey() +
									// ",channel=" + externalPaymentTnx.getChannel() + "]");
									// throw new ModuleException("airreservations.reconExtPay.onAccAgencyNotConfigured",
									// AirreservationConstants.MODULE_NAME);
									// }

									log.info("Starting external payment reconciliation for " + "[pnr = "
											+ externalPaymentTnx.getPnr() + ", balQKey=" + externalPaymentTnx.getBalanceQueryKey()
											+ "]");

									ReservationModuleUtils.getReservationBD().reconcileExtPayTransaction(externalPaymentTnx,
											onAccAgentCode, isManualRecon);

									log.info("Completed external payment reconciliation for " + "[pnr = "
											+ externalPaymentTnx.getPnr() + ", balQKey=" + externalPaymentTnx.getBalanceQueryKey()
											+ "]");
								} catch (Exception ex) {
									log.error("External payment transaction recon failed [" + "pnr=" + externalPaymentTnx.getPnr()
											+ ",balQueryKey=" + externalPaymentTnx.getBalanceQueryKey() + "]", ex);
									try {
										// Update the reconcilation status as failed
										log.info("External payment tnx reconciliation failed for " + "[pnr = "
												+ externalPaymentTnx.getPnr() + ", balQKey="
												+ externalPaymentTnx.getBalanceQueryKey() + "]. Need manual action.");
										externalPaymentTnx.setReconciliationSource(isManualRecon
												? ReservationInternalConstants.ExtPayTxReconSourceType.MANUAL
												: ReservationInternalConstants.ExtPayTxReconSourceType.SCHEDULED_JOB);
										externalPaymentTnx.setReconciledTimestamp(new Date());
										externalPaymentTnx.setReconcilationStatus(
												ReservationInternalConstants.ExtPayTxReconStatus.RECON_FAILED);
										externalPaymentTnx.setRemarks((externalPaymentTnx.getRemarks() == null
												? ""
												: externalPaymentTnx.getRemarks() + "|")
												+ "Reconciliation failed, Need manual action");
										if (isManualRecon) {
											externalPaymentTnx.setManualReconBy(credentialsDTO.getUserId());
										}
										// Save the data starting a new transaction
										ReservationModuleUtils.getReservationBD()
												.saveOrUpdateExternalPayTxInfo(externalPaymentTnx);
									} catch (Exception e) {
										log.error("Status update of recon failed transaction failed [" + "pnr="
												+ externalPaymentTnx.getPnr() + ",balQueryKey="
												+ externalPaymentTnx.getBalanceQueryKey() + "]", ex);
									}
								}
								reconciledBalQueryKeys.add(externalPaymentTnx.getBalanceQueryKey());
							}
						}
					} catch (Exception ex) {
						if (!isManualRecon) {
							isExceptionOccured = true;
							log.error("Scheduled reconcilation partially or fully failed for txns falling between "
									+ "[startTimestamp=" + startTimestamp + ", endTimestamp=" + endTimestamp + "]");
							log.error(" ((o)) Exception::reconcileExtPayTransactions ", ex);
						} else {
							if (ex instanceof ModuleException) {
								throw (ModuleException) ex;
							} else {
								throw new ModuleException(ex, "airreservations.reconExtPay.failed",
										AirreservationConstants.MODULE_NAME);
							}
						}
					}
				}
			}

			// Update the recon status of the transactions whose recon status is not set
			if (!isManualRecon && !isExceptionOccured) {
				ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
				criteriaDTO.setStartTimestamp(startTimestamp);
				criteriaDTO.setEndTimestamp(endTimestamp);
				criteriaDTO.setAgentCode(agencyCode);
				criteriaDTO.addReconStatusToInclude(ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED);

				log.info("Implicit reconciliation of transactions done from " + startTimestamp + " to " + endTimestamp);

				Map<String, PNRExtTransactionsTO> pnrTnxsMap = ReservationModuleUtils.getReservationBD()
						.getExtPayTransactions(criteriaDTO);

				if (pnrTnxsMap != null) {
					for (String string : pnrTnxsMap.keySet()) {
						PNRExtTransactionsTO pnrTnxs = pnrTnxsMap.get(string);
						if (pnrTnxs != null && pnrTnxs.getExtPayTransactions() != null) {
							for (ExternalPaymentTnx externalPaymentTnx : pnrTnxs.getExtPayTransactions()) {
								ExternalPaymentTnx unReconciledTnx = externalPaymentTnx;
								if (!reconciledBalQueryKeys.contains(unReconciledTnx.getBalanceQueryKey())) {
									// Set common attributes
									unReconciledTnx.setReconciliationSource(isManualRecon
											? ReservationInternalConstants.ExtPayTxReconSourceType.MANUAL
											: ReservationInternalConstants.ExtPayTxReconSourceType.SCHEDULED_JOB);
									unReconciledTnx.setReconciledTimestamp(new Date());

									// Transaction success at both ends - Bank and AA
									if (ReservationInternalConstants.ExtPayTxStatus.SUCCESS.equals(unReconciledTnx.getStatus())) {
										log.info("Implicit Recon - Updating external payment transaction success at both ends "
												+ "[pnr = " + unReconciledTnx.getPnr() + ", balQKey="
												+ unReconciledTnx.getBalanceQueryKey() + "]");
										unReconciledTnx.setReconcilationStatus(
												ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED);
										unReconciledTnx.setRemarks(
												(unReconciledTnx.getRemarks() == null ? "" : unReconciledTnx.getRemarks() + "|")
														+ "Successfully reconciled");

										// Only balance querying done
									} else if (ReservationInternalConstants.ExtPayTxStatus.INITIATED
											.equals(unReconciledTnx.getStatus())) {
										log.info(
												"Implicit Recon - Updating status of balance query only done external payment tnx "
														+ "[pnr = " + unReconciledTnx.getPnr() + ", balQKey="
														+ unReconciledTnx.getBalanceQueryKey() + "]");
										unReconciledTnx.setStatus(ReservationInternalConstants.ExtPayTxStatus.ABORTED);
										unReconciledTnx.setReconcilationStatus(
												ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED);
										unReconciledTnx.setRemarks(
												(unReconciledTnx.getRemarks() == null ? "" : unReconciledTnx.getRemarks() + "|")
														+ "Only balance query done");

										// Transaction unsuccessful at AA, no corresponding transaction at Bank -> at
										// present following statuses are not possible
									} else if (ReservationInternalConstants.ExtPayTxStatus.FAILED
											.equals(unReconciledTnx.getStatus())
											|| ReservationInternalConstants.ExtPayTxStatus.UNKNOWN
													.equals(unReconciledTnx.getStatus())) {
										log.info(
												"Implicit Recon - Updating failed external payment transaction at AA and having no record at bank "
														+ "[pnr = " + unReconciledTnx.getPnr() + ", balQKey="
														+ unReconciledTnx.getBalanceQueryKey() + "]");
										unReconciledTnx.setReconcilationStatus(
												ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED);
										unReconciledTnx.setRemarks(
												(unReconciledTnx.getRemarks() == null ? "" : unReconciledTnx.getRemarks() + "|")
														+ "Failed or Status Unknown transaction");

										// Updating Aborted transaction -> Reconciliation Source get updated
									} else if (ReservationInternalConstants.ExtPayTxStatus.ABORTED
											.equals(unReconciledTnx.getStatus())) {
										log.info("Implicit Recon - Updating Manually reconciled external payment txn " + "[pnr = "
												+ unReconciledTnx.getPnr() + ", balQKey=" + unReconciledTnx.getBalanceQueryKey()
												+ "]");
										unReconciledTnx.setReconcilationStatus(
												ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED);
										unReconciledTnx.setRemarks(
												(unReconciledTnx.getRemarks() == null ? "" : unReconciledTnx.getRemarks() + "|")
														+ "Updating aborted transaction");

										// Unexpected transaction
									} else {
										log.error("Implicit Recon - Unexpected transaction found "
												+ unReconciledTnx.getSummary().toString());
									}

									// Save the data starting a new transaction
									ReservationModuleUtils.getReservationBD().saveOrUpdateExternalPayTxInfo(unReconciledTnx);
								} else {
									log.error("Recon failed transaction, need manual action "
											+ unReconciledTnx.getSummary().toString());
								}
							}
						}
					}
				}
			}

			if (!isManualRecon) {
				Map<String, String> recipients = ReservationModuleUtils.getAirReservationConfig()
						.getExtPayReconEmailRecipientsMap();

				if (ReservationModuleUtils.getAirReservationConfig().isSendEmailAfterExtPayRecon() && recipients != null
						&& recipients.size() > 0) {
					Collection<ExternalPaymentTnx> bankExcessTxns = new ArrayList<ExternalPaymentTnx>();
					Collection<ExternalPaymentTnx> aaExcessTxns = new ArrayList<ExternalPaymentTnx>();
					Collection<ExternalPaymentTnx> invalidStatusTxns = new ArrayList<ExternalPaymentTnx>();

					if (pnrExtTransactionsTOs != null && pnrExtTransactionsTOs.size() > 0) {

						for (PNRExtTransactionsTO pnrExtTransactionsTO2 : pnrExtTransactionsTOs) {
							PNRExtTransactionsTO pnrExtTransactionsTO = pnrExtTransactionsTO2;
							if (pnrExtTransactionsTO.getExtPayTransactions() != null) {
								for (ExternalPaymentTnx externalPaymentTnx2 : pnrExtTransactionsTO.getExtPayTransactions()) {
									ExternalPaymentTnx externalPaymentTnx = externalPaymentTnx2;
									if (ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS
											.equals(externalPaymentTnx.getExternalPayStatus())) {
										bankExcessTxns.add(externalPaymentTnx);
									} else if (ReservationInternalConstants.ExtPayTxStatus.AA_EXCESS
											.equals(externalPaymentTnx.getExternalPayStatus())) {
										aaExcessTxns.add(externalPaymentTnx);
									} else {
										invalidStatusTxns.add(externalPaymentTnx);
									}
								}
							}
						}
					}

					boolean allInSync = bankExcessTxns.size() == 0 && aaExcessTxns.size() == 0 && invalidStatusTxns.size() == 0;

					// Prepare for calling messaging
					List messagesList = new ArrayList();

					for (String string : recipients.keySet()) {
						String key = string;
						// User Message
						UserMessaging userMessaging = new UserMessaging();
						userMessaging.setFirstName(key);
						userMessaging.setToAddres(recipients.get(key));

						HashMap emailDataMap = new HashMap();
						emailDataMap.put("allInSync", new Boolean(allInSync));
						emailDataMap.put("isExceptionOccured", new Boolean(isExceptionOccured));
						emailDataMap.put("bankExcessTxns", bankExcessTxns);
						emailDataMap.put("aaExcessTxns", aaExcessTxns);
						emailDataMap.put("invalidStatusTxns", invalidStatusTxns);
						emailDataMap.put("userName", key);
						emailDataMap.put("fromDate",
								startTimestamp != null ? CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(startTimestamp) : "");
						emailDataMap.put("toDate",
								endTimestamp != null ? CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(endTimestamp) : "");

						List messages = new ArrayList();
						messages.add(userMessaging);

						// Topic
						Topic topic = new Topic();
						topic.setTopicParams(emailDataMap);
						topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.EXT_PAY_RECON_STATUS_TEMPLATE);
						// User Profile
						MessageProfile messageProfile = new MessageProfile();
						messageProfile.setUserMessagings(messages);
						messageProfile.setTopic(topic);

						messagesList.add(messageProfile);
					}

					ReservationModuleUtils.getMessagingServiceBD().sendMessages(messagesList);
				}
			}

			log.info("Completed external payment reconciliation at " + new Date(System.currentTimeMillis()));
			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reconcileExtPayTransactions ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reconcileExtPayTransactions ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns Terms & Conditions in the specified locale.
	 * 
	 * @param locale
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getTermsNConditions(Locale locale) throws ModuleException {
		return ReservationTemplateUtils.getTermsNConditions(locale, TEMPLATE_TYPES.WS_TEMPLATE);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getTermsNConditions(String templateName, Locale locale) throws ModuleException {
		return ReservationTemplateUtils.getTermsNConditions(templateName, locale);
	}

	/**
	 * Returns Object[]{pnrPaxId, pnrPaxFareId, paxType} corresponding to first adult or child pax with pnrPaxFareId,
	 * paxType linked to first segment.
	 * 
	 * @param pnr
	 * @return Object[]{pnrPaxId, pnrPaxFareId, paxType}
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Object[] getFirstPaxPnrPaxFareId(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_DAO.getFirstPaxPnrPaxFareId(pnr);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFirstPaxPnrPaxFareId", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Gets itinerary for print
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getItineraryForPrint(ItineraryLayoutModesDTO itineraryLayoutModesDTO, Collection<Integer> colSelectedPnrPaxIds,
			TrackInfoDTO trackInfoDTO, boolean isCheckIndividual) throws ModuleException {
		try {
			// Get the caller credentials
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			return ItineraryBL.getItineraryForPrint(itineraryLayoutModesDTO, colSelectedPnrPaxIds, credentialsDTO,
					isCheckIndividual);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getItineraryForPrint ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Gets itinerary for print
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getTaxInvoiceForPrint(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation)
			throws ModuleException {
		try {
			return TaxInvoiceBL.getTaxInvoiceForPrint(taxInvoicesList, reservation);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getInvoiceForPrint ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getBarcodeImagePathForItinerary(String pnr, List<String> segmentList) throws ModuleException {
		try {
			return ItineraryBL.generateBarcode(pnr, segmentList);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getBarcodeImagePathForItinerary", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Gets itinerary for print
	 * 
	 * @param recieptLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getRecieptForPrint(RecieptLayoutModesDTO recieptLayoutModesDTO, Collection<Integer> colSelectedPnrPaxIds,
			TrackInfoDTO trackInfoDTO, boolean isCheckIndividual) throws ModuleException {
		try {
			// Get the caller credentials
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			return RecieptBL.getRecieptForPrint(recieptLayoutModesDTO, colSelectedPnrPaxIds, credentialsDTO, isCheckIndividual);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getItineraryForPrint ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Gets interline itinerary for print
	 * 
	 * @param itineraryDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getInterlineItineraryForPrint(ItineraryDTO itineraryDTO) throws ModuleException {
		try {
			return ItineraryBL.getInterlineItineraryForPrint(itineraryDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getInterlineItineraryForPrint ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the quoted charges information
	 * 
	 * @param colEXTERNAL_CHARGES
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getQuotedExternalCharges(Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES,
			TrackInfoDTO trackInfoDTO, Integer operationalMode) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			return ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, credentialsDTO.getAgentCode(), operationalMode);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getQuotedExternalCharges ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the initiated tempory payment information
	 * 
	 * @param date
	 * @param colIPGIdentificationDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<IPGQueryDTO> getInitiatedPaymentInfo(Date date,
			Collection<IPGIdentificationParamsDTO> colIPGIdentificationDTO) throws ModuleException {
		try {
			Collection<String> colStates = new ArrayList<String>();
			colStates.add(ReservationInternalConstants.TempPaymentTnxTypes.INITIATED);
			colStates.add(ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS);

			Collection<String> colStateHistory = new ArrayList<String>();
			colStateHistory.add(TempPaymentInconsistentStatusHistoryTypes.I_PS_RF);
			colStateHistory.add(TempPaymentInconsistentStatusHistoryTypes.I_PS_PS_RF);
			colStateHistory.add(TempPaymentInconsistentStatusHistoryTypes.I_RF_US);

			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getTemporyPaymentInfo(date, colStates, colStateHistory,
					colIPGIdentificationDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getInitiatedPaymentInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public IPGQueryDTO getTemporyPaymentInfo(int paymentBrokerRefNo) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getTemporyPaymentInfo(paymentBrokerRefNo);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getInitiatedPaymentInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Process interlined segments transfer
	 * 
	 * @param executionTime
	 * @param interlinedAirLineTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce processInterlinedSegments(Date executionTime, InterlinedAirLineTO interlinedAirLineTO)
			throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			Map<String, Collection<ReservationSegmentDTO>> pnrAndSegments = InterlinedTransferBL
					.getPNRAndSegmentInformation(executionTime, interlinedAirLineTO);
			String pnr;
			Collection<ReservationSegmentDTO> colReservationSegmentDTO;

			if (log.isInfoEnabled()) {
				log.info("====================================================");
				log.info(" Following PNR(s) were detected to transfer \n\r \t [ " + pnrAndSegments.keySet().toString()
						+ " ] \n\r \t ");
				log.info("====================================================");
			}

			for (String string : pnrAndSegments.keySet()) {
				pnr = string;
				colReservationSegmentDTO = pnrAndSegments.get(pnr);
				InterlinedTransferBL.processPNRForSegments(pnr, interlinedAirLineTO, colReservationSegmentDTO, credentialsDTO);
			}
			return new DefaultServiceResponse(true);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::processInterlinedSegments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Update the reservation segment transfer states
	 * 
	 * @param colReservationSegmentDTO
	 * @param transferPNR
	 * @param processDesc
	 * @param status
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateReservationSegmentTransfer(Collection<ReservationSegmentDTO> colReservationSegmentDTO, String transferPNR,
			String processDesc, String status) throws ModuleException {
		try {
			InterlinedTransferBL.updateReservationSegmentTransferStates(colReservationSegmentDTO, transferPNR, processDesc,
					status);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateReservationSegmentTransfer ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the reservation transfer segment information
	 * 
	 * @param colPnrSegIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, ReservationSegmentTransfer> getReservationTransferSegments(Collection<Integer> colPnrSegIds)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getReservationTransferSegments(colPnrSegIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationTransferSegments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Notify reservation segment transfer failures
	 * 
	 * @param executionDate
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void notifySegmentTransferFailures(Date executionDate) throws ModuleException {
		try {
			InterlinedTransferBL.notifySegmentTransferFailures(executionDate);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::notifySegmentTransferFailures ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Get all credits
	 * 
	 * @param pnrpaxID
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Collection<CreditInfoDTO>> getAllCredits(Collection<Integer> pnrPaxIds) throws ModuleException {
		try {
			Map<Integer, Collection<CreditInfoDTO>> colAvailableCredits = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO
					.getAvailableCredit(pnrPaxIds);
			Map<Integer, Collection<CreditInfoDTO>> colAcquiredCredits = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO
					.getAcquiredCredit(pnrPaxIds);
			for (Integer pnrPaxId : colAcquiredCredits.keySet()) {
				if (colAvailableCredits.keySet().contains(pnrPaxId)) {
					colAvailableCredits.get(pnrPaxId).addAll(colAcquiredCredits.get(pnrPaxId));
				} else {
					colAvailableCredits.put(pnrPaxId, colAcquiredCredits.get(pnrPaxId));
				}
			}
			return colAvailableCredits;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::availableCredit ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Extend credits
	 * 
	 * @param creditId
	 * @param expiryDate
	 * @param note
	 * @param pnrPaxId
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce extendCredits(long creditId, Date expiryDate, String note, String pnr, int pnrPaxId,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			return ReservationModuleUtils.getRevenueAccountBD().extendCredits(creditId, expiryDate, note, pnr, pnrPaxId,
					credentialsDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::extendCredits ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Reinstantiate credit
	 * 
	 * @param pnr
	 * @param pnrPaxId
	 * @param paxTxnId
	 * @param amount
	 * @param expDate
	 * @param note
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce reinstateCredit(String pnr, int pnrPaxId, int paxTxnId, BigDecimal amount, Date expDate, String note,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);
			// Contains empty lists to avoid null pointer exceptions
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
			return ReservationModuleUtils.getRevenueAccountBD().reinstateCredit(pnr, pnrPaxId, paxTxnId, amount, expDate,
					reservationPaxPaymentMetaTO, credentialsDTO, note, false);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reinstateCredit ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Seat Map
	 * 
	 * @param pnr
	 * @param pnrPaxFareId
	 * @param chargeRateID
	 * @param amount
	 * @param userNotes
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce modifySeats(IPassengerSeating ipassengerSeating, String pnr, String userNotes, long version,
			boolean updatePayment, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			PassengerSeatingAssembler passengerSeatingAssembler = (PassengerSeatingAssembler) ipassengerSeating;
			// Collection passengerSeating = new ArrayList();
			IPassenger iPassenger = passengerSeatingAssembler.getPassenger();
			boolean isForceConfirmed = passengerSeatingAssembler.isForceConfirmed();

			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);

			// Return the reservation
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
			// Checking to see if any concurrent update exist
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
			List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

			SeatMapAdapter.prepareSeatsToModify(reservation, passengerSeatingAssembler, trackInfoDTO);
			Map<String, PaxSeatTO> paxIdSeatMapToAdd = passengerSeatingAssembler.getPaxSeatToAdd();
			Collection<PaxSeatTO> paxSeatDTOs = new ArrayList<PaxSeatTO>();
			AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation, true);

			if (!paxIdSeatMapToAdd.isEmpty()) {
				paxSeatDTOs.addAll(paxIdSeatMapToAdd.values());
				SeatMapAdapter.addPassengerSeatChargeAndAdjustCredit(adjustCreditBO, reservation, paxIdSeatMapToAdd, userNotes,
						trackInfoDTO, credentialsDTO, chgTnxGen);
			}

			Map<String, PaxSeatTO> paxIdSeatMapToRem = passengerSeatingAssembler.getPaxSeatToRemove();
			if (!paxIdSeatMapToRem.isEmpty()) {
				SeatMapAdapter.addPassengerSeatChargeAndAdjustCredit(adjustCreditBO, reservation, paxIdSeatMapToRem, userNotes,
						trackInfoDTO, credentialsDTO, chgTnxGen);
			}

			ADLNotifyer.updateStatusToCHG(reservation);
			if (!isForceConfirmed) {
				// // Recording adl change operation according to new implemenatation
				ADLNotifyer.recordReservation(reservation, AdlActions.C,
						getAncilliaryAddedPassengers(paxIdSeatMapToAdd.keySet(), paxIdSeatMapToRem.keySet()),
						reservation.getStatus());
			}
			// Save the reservation
			ReservationProxy.saveReservation(reservation);

			adjustCreditBO.callRevenueAccountingAfterReservationSave();

			// modify flight seat statuss
			SeatMapAdapter.modifySeats(paxIdSeatMapToAdd, paxIdSeatMapToRem);

			// save passenger- seat info
			SeatMapAdapter.savePaxSeatingInfo(paxSeatDTOs, credentialsDTO);
			// TODO refactor to do seperately
			// ReservationModuleUtils.getReservationBD().modifyMeals(ipassengerMeal, pnr, userNotes, version+1,
			// trackInfoDTO);
			reservation = ReservationProxy.getReservation(pnrModesDTO);
			List<Integer> forcedConfirmedPaxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
			handleETGeneration(reservation, forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange,
					credentialsDTO);

			if (updatePayment && iPassenger != null) {
				// if code comes here means no errors
				updateReservationForPayment(pnr, iPassenger, isForceConfirmed, false, version + 1, trackInfoDTO, true, false,
						true, false, false, false, null, false, null);
			}

			// record audit history for modify seats
			if (!paxIdSeatMapToRem.isEmpty() || !paxIdSeatMapToAdd.isEmpty()) {
				SeatMapAdapter.recordAuditHistoryForModifySeats(pnr, passengerSeatingAssembler.getStatusFlightSeatIdsMap(),
						credentialsDTO, userNotes);
			}

			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::modifySeats ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::modifySeats ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce modifyMeals(IPassengerMeal ipassengerMeal, String pnr, String userNotes, long version,
			boolean updatePayment, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			PassengerMealAssembler passengerMealAssembler = (PassengerMealAssembler) ipassengerMeal;
			IPassenger iPassenger = passengerMealAssembler.getPassenger();
			boolean isForceConfirmed = passengerMealAssembler.isForceConfirmed();

			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			// Return the reservation
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
			// Checking to see if any concurrent update exist
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
			List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

			MealAdapter.prepareMealsToModify(reservation, passengerMealAssembler, trackInfoDTO);
			Map<String, Collection<PaxMealTO>> paxIdMealToAdd = passengerMealAssembler.getpaxMealTOAdd();
			Collection<Collection<PaxMealTO>> paxMealDTOs = new ArrayList<Collection<PaxMealTO>>();

			AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation, true);

			if (!paxIdMealToAdd.isEmpty()) {
				paxMealDTOs.addAll(paxIdMealToAdd.values());

				MealAdapter.addPassengerMealChargeAndAdjustCredit(adjustCreditBO, reservation, paxIdMealToAdd, userNotes,
						trackInfoDTO, credentialsDTO, chgTnxGen);
			}

			Map<String, Collection<PaxMealTO>> paxIdMealToRem = passengerMealAssembler.getpaxMealTORemove();
			if (!paxIdMealToRem.isEmpty()) {
				MealAdapter.addPassengerMealChargeAndAdjustCredit(adjustCreditBO, reservation, paxIdMealToRem, userNotes,
						trackInfoDTO, credentialsDTO, chgTnxGen);
			}
			// TODO remove this
			ADLNotifyer.updateStatusToCHG(reservation);

			if (!isForceConfirmed) {
				// // Recording adl change operation according to new implemenatation
				ADLNotifyer.recordReservation(reservation, AdlActions.C,
						getAncilliaryAddedPassengers(paxIdMealToAdd.keySet(), paxIdMealToRem.keySet()), reservation.getStatus());
			}
			// Save the reservation
			ReservationProxy.saveReservation(reservation);

			adjustCreditBO.callRevenueAccountingAfterReservationSave();

			// modify flight meal statuss
			MealAdapter.modifyMeals(paxIdMealToAdd, paxIdMealToRem);

			// save passenger- meal info
			// MealAdapter.savePaxMealInfo(paxMealDTOs, credentialsDTO);
			MealAdapter.modifyPaxMealInfo(paxMealDTOs, credentialsDTO);

			reservation = ReservationProxy.getReservation(pnrModesDTO);
			List<Integer> forcedConfirmedPaxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
			handleETGeneration(reservation, forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange,
					credentialsDTO);

			if (updatePayment && iPassenger != null) {
				// if code comes here means no errors
				updateReservationForPayment(pnr, iPassenger, isForceConfirmed, false, version + 1, trackInfoDTO, true, false,
						true, false, false, false, null, false, null);
			}

			// record audit for modify meals
			if (!paxIdMealToAdd.isEmpty() || !paxIdMealToRem.isEmpty()) {
				Collection[] colResInfo = { reservation.getPassengers(), reservation.getSegmentsView() };
				MealAdapter.recordPaxWiseAuditHistoryForModifyMeals(pnr, paxIdMealToAdd, paxIdMealToRem, credentialsDTO,
						userNotes, colResInfo);
			}

			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::modifySeats ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::modifySeats ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Add/Change External Segment Information
	 * 
	 * @param pnr
	 * @param colExternalSegmentTO
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce changeExternalSegments(String pnr, Collection<ExternalSegmentTO> colExternalSegmentTO, long version,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CHANGE_EXTERNAL_SEGMENT_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS, colExternalSegmentTO);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

			ServiceResponce serviceResponce = command.execute();

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::changeExternalSegments ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::changeExternalSegments ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce changeOtherAirlineSegments(String pnr, Collection<OtherAirlineSegmentTO> colExternalSegmentTO,
			long version, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CHANGE_OTHER_AIRLINE_SEGMENT_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.RESERVATION_OTHER_AIRLINE_SEGMENTS, colExternalSegmentTO);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

			ServiceResponce serviceResponce = command.execute();

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::changeExternalSegments ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::changeExternalSegments ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public ServiceResponce changeExternalSegmentsForDummyReservation(Reservation reservation,
			Collection<ExternalSegmentTO> colExternalSegmentTO) throws ModuleException {
		try {

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CHANGE_DUMMY_RES_EXTERNAL_SEGMENT);
			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS, colExternalSegmentTO);

			ServiceResponce serviceResponce = command.execute();

			return serviceResponce;

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::changeExternalSegments ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::changeExternalSegments ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Change Segments
	 * 
	 * @param pnr
	 * @param oldSegmentIds
	 * @param iSegment
	 * @param modifyCharge
	 * @param version
	 * @param trackInfoDTO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce addInsurance(List<IInsuranceRequest> insuranceRequests, String pnr, IPassenger iPassenger,
			boolean forceConfirm, boolean paymentCompleted) throws ModuleException {
		// Retrieve called credential information
		PassengerAssembler paxAssembler = (PassengerAssembler) iPassenger;

		// Return the payment information
		// Collection colPaymentInfo = ReservationBO.getPaymentInfo(paxAssembler, pnr);

		boolean hasPayment = false;

		if (paxAssembler.getPassengerPaymentsMap() != null && paxAssembler.getPassengerPaymentsMap().values() != null
				&& paxAssembler.getPassengerPaymentsMap().values().size() > 0 && paymentCompleted) {
			for (PaymentAssembler payment : (Collection<PaymentAssembler>) paxAssembler.getPassengerPaymentsMap().values()) {
				if (payment.isPaymentExist()) {
					hasPayment = true;
					break;
				}
			}
		}

		CredentialsDTO credentialsDTO = getCallerCredentials(null);

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ADD_INSURANCE_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.INSURANCE_INFO, insuranceRequests);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, forceConfirm);

			if (!hasPayment) {
				command.setParameter(CommandParamNames.INSURANCE_HOLD, new Boolean(true));
			}

			ServiceResponce serviceResponce = command.execute();
			List<InsuranceResponse> insuranceResponses = (List<InsuranceResponse>) serviceResponce
					.getResponseParam(CommandParamNames.INSURANCE_RES);

			// Create Eticket for insurance modification
			if (insuranceResponses != null && !insuranceResponses.isEmpty()) {

				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
				pnrModesDTO.setLoadSegView(true);
				Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

				List<Integer> paxSeqsForInsuranceModification = ReservationApiUtils.getPaxSeqForInsuranceModify(reservation);
				handleETGeneration(reservation, paxSeqsForInsuranceModification, new ArrayList<Integer>(), credentialsDTO);
			}

			ReservationAudit reservationAudit = new ReservationAudit();
			reservationAudit.setModificationType(AuditTemplateEnum.ADD_INSURANCE.getCode());

			String insuranceAudit = InsuranceHelper.getAudit(insuranceResponses);
			if (insuranceAudit != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.INSURANCE, insuranceAudit);
			}

			if (getUserPrincipal() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.IP_ADDDRESS,
						getUserPrincipal().getIpAddress());
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.ORIGIN_CARRIER,
						getUserPrincipal().getDefaultCarrierCode());
			}

			ReservationBO.recordModification(pnr, reservationAudit, null, credentialsDTO);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::addInsurance ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::addInsurance ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Expire Open Return Segments
	 * 
	 * @param toDate
	 * @param customAdultCancelCharge
	 * @param customChildCancelCharge
	 * @param customInfantCancelCharge
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void expireOpenReturnSegments(Date executionDate, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge) throws ModuleException {
		log.info(" +===================================+ ");
		log.info("  Started Expire Open Return Segments ");
		log.info(" +===================================+ ");

		try {
			ReservationBO.expireOpenReturnSegments(executionDate, customAdultCancelCharge, customChildCancelCharge,
					customInfantCancelCharge);
		} catch (Exception e) {
			log.error("EXPIRE_OPEN_RETURN_SEGMENTS ", e);
		}
		log.info(" +===================================+ ");
		log.info("  Expire Open Return Segments Completed ");
		log.info(" +===================================+ ");
	}

	/**
	 * Confirms the Open Return Segments
	 * 
	 * @param pnr
	 * @param pnrSegIds
	 * @param iSegment
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce confirmOpenReturnSegments(String pnr, Collection<Integer> pnrSegIds, ISegment iSegment,
			Collection<TempSegBcAlloc> blockKeyIds, Integer paymentTypes, long version, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		SegmentAssembler sAssembler = (SegmentAssembler) iSegment;

		// Injecting the segment credentials information
		sAssembler.injectSegmentCredentials(credentialsDTO);

		// Return the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(sAssembler, pnr);

		// Load the reservation contact information
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);

		// Get the reservation service local instance
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(sAssembler.getTnxIds(),
				contactInfo, colPaymentInfo, credentialsDTO, true, false);
		boolean triggerBlockSeats = false;
		if (blockKeyIds == null) {
			triggerBlockSeats = true;
		}

		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CONFIRM_OPEN_RETURN_SEGMENTS_MACRO);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PNR_SEGMENT_IDS, pnrSegIds);
			command.setParameter(CommandParamNames.RESERVATION_SEGMENTS, sAssembler.getDummySegments());
			command.setParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS, sAssembler.getExternalSegments());

			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.VERSION, new Long(version));
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.RESERVATION_CONTACT_INFO, contactInfo);
			command.setParameter(CommandParamNames.PAYMENT_INFO, colPaymentInfo);
			command.setParameter(CommandParamNames.OND_FARE_DTOS, sAssembler.getInventoryFares());
			command.setParameter(CommandParamNames.SEG_OBJECTS_MAP, sAssembler.getSegmentObjectsMap());
			command.setParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, sAssembler.getPassengerPaymentsMap());
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, blockKeyIds);

			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, new Boolean(triggerBlockSeats));
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED,
					ReservationApiUtils.isTriggerForceConfirm(paymentTypes));
			command.setParameter(CommandParamNames.PNR_PAYMENT_TYPES, paymentTypes);
			command.setParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, sAssembler.getZuluReleaseTimeStamp());
			command.setParameter(CommandParamNames.INSURANCE_INFO, sAssembler.getInsuranceRequests());
			command.setParameter(CommandParamNames.LAST_MODIFICATION_TIMESTAMP, sAssembler.getLastModificationTimeStamp());
			command.setParameter(CommandParamNames.LAST_CURRENCY_CODE, sAssembler.getLastCurrencyCode());

			command.setParameter(CommandParamNames.ORDERED_NEW_FLIGHT_SEGMENT_IDS, sAssembler.getOrderedNewFlightSegmentIds());
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(true));

			if (colPaymentInfo == null || colPaymentInfo.isEmpty()
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(paymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE
							.equals(paymentTypes)) {
				command.setParameter(CommandParamNames.INSURANCE_HOLD, new Boolean(true));
			}
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));
			ServiceResponce serviceResponce = command.execute();

			// Remove the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, null, null, null, null);

			ReservationModuleUtils.getReservationBD().sendSms(pnr, contactInfo, credentialsDTO);

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(
					" ((o)) ModuleException::confirmOpenReturnSegments " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					ex);
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, null, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, null, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::confirmOpenReturnSegments "
					+ ReservationApiUtils.getElementsInCollection(colTnxIds), cdaex);
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, null, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, null, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * JIRA AARESAA-1953 Check For duplicate Names in single flight segment
	 * 
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String> getDuplicateNameReservations(Collection<Integer> flightSegIds, ArrayList<String> adultNameList,
			ArrayList<String> childNameList, String pnr, Collection<Integer> paxIds) throws ModuleException {
		try {
			ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
			return reservationDAO.getDuplicateNames(flightSegIds, adultNameList, childNameList, pnr, paxIds);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce modifyAncillaries(String pnr, String userNotes, long version, IPassenger iPassenger,
			boolean isForceConfirmed, Collection<AncillaryAssembler> ancillaryAssemblers, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		Map<Integer, SegmentSSRAssembler> ssrAssemblerMap = new HashMap<Integer, SegmentSSRAssembler>();
		PassengerSeatingAssembler seatingAssembler = null;
		IPassengerMeal ipassengerMeal = null;
		IPassengerBaggage ipassengerBaggage = null;
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		version = ReservationModuleUtils.getReservationBD().removeInsurance(pnr, version);

		if (ancillaryAssemblers != null) {
			for (AncillaryAssembler ancillaryAssembler2 : ancillaryAssemblers) {
				AncillaryAssembler ancillaryAssembler = ancillaryAssembler2;

				if (ancillaryAssembler instanceof SegmentSSRAssembler) {
					SegmentSSRAssembler ssrAssembler = (SegmentSSRAssembler) ancillaryAssembler;

					ssrAssemblerMap.put(ssrAssembler.getPaxSeguence(), ssrAssembler);
				} else if (ancillaryAssembler instanceof IPassengerSeating) {
					seatingAssembler = (PassengerSeatingAssembler) ancillaryAssembler;
				} else if (ancillaryAssembler instanceof IPassengerMeal) {
					ipassengerMeal = (IPassengerMeal) ancillaryAssembler;
				} else if (ancillaryAssembler instanceof IPassengerBaggage) {
					ipassengerBaggage = (IPassengerBaggage) ancillaryAssembler;
				}
			}
		}

		if (seatingAssembler != null) {
			ReservationModuleUtils.getReservationBD().modifySeats(seatingAssembler, pnr, userNotes, version, false,
					credentialsDTO.getTrackInfoDTO());
			version++;
		}

		if (ipassengerMeal != null) {
			ReservationModuleUtils.getReservationBD().modifyMeals(ipassengerMeal, pnr, userNotes, version, false,
					credentialsDTO.getTrackInfoDTO());
			version++;
		}

		if (ipassengerBaggage != null) {
			ReservationModuleUtils.getReservationBD().modifyBaggages(ipassengerBaggage, pnr, userNotes, version, false,
					credentialsDTO.getTrackInfoDTO());
			version++;
		}

		if (iPassenger != null) {
			updateReservationForPayment(pnr, iPassenger, isForceConfirmed, false, version, credentialsDTO.getTrackInfoDTO(),
					AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), false, true, false, false, false,
					null, false, null);
		}

		return new DefaultServiceResponse(true);
	}

	/**
	 * Return Mashreq credits
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<LoyaltyCreditDTO> getMashreqCredits(int customerId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.getMashreqCredits(customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getMashreqCredits ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return Mashreq credits usage
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<LoyaltyCreditUsageDTO> getMashreqCreditUsage(int customerId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.getMashreqCreditUsage(customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getMashreqCreditUsage ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Removes insurance from a reservation used only in modify anci services. mod seg and add seg use the command
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public long removeInsurance(String pnr, long version) throws ModuleException {
		CredentialsDTO credentialsDTO = getCallerCredentials(null);

		if (pnr != null) {
			// Retrieves the Reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation, false);
			if (reservation != null && reservation.getReservationInsurance() != null) {
				List<ReservationInsurance> listResIns = reservation.getReservationInsurance();
				for (ReservationInsurance resIns : listResIns) {
					if ("OH".equals(resIns.getState())) {
						Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
						colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);

						Map<EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationBO
								.getQuotedExternalCharges(colEXTERNAL_CHARGES, credentialsDTO.getAgentCode(), null);
						ExternalChgDTO externalChgDTO = (ExternalChgDTO) (mapExternalChgs.get(EXTERNAL_CHARGES.INSURANCE))
								.clone();

						Collection<Integer> colPnrPaxId = reservation.getPnrPaxIds();

						Collection<INSChargeTO> colInsDtos = ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO
								.getInsuranceCharges(colPnrPaxId, externalChgDTO.getChgRateId());

						AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);

						if (colInsDtos != null && !colInsDtos.isEmpty()) {
							for (INSChargeTO insDto : colInsDtos) {
								ReservationBO.adjustCreditManual(adjustCreditBO, reservation, insDto.getPnrPaxFareId(),
										insDto.getChargeRateId(), insDto.getAmount().negate(), insDto.getChargeGroup(), "",
										credentialsDTO.getTrackInfoDTO(), credentialsDTO, chgTnxGen);
							}
						}

						reservation.setInsuranceId(null);
						reservation.setReservationInsurance(null);
						ReservationProxy.saveReservation(reservation);
						adjustCreditBO.callRevenueAccountingAfterReservationSave();

						if (!resIns.getState().equalsIgnoreCase(ReservationInternalConstants.INSURANCE_STATES.CX.toString())) {
							resIns.setState(ReservationInternalConstants.INSURANCE_STATES.CX.toString());
							ReservationProxy.saveOrUpdateReservationInsurance(resIns);
						}

						version = version + 1;
					}
				}
			}
		}
		return version;
	}

	/**
	 * Note that the method will return pax credit payment map. Since we need to trac that only.
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce makePayment(TrackInfoDTO trackInfo, PaymentAssembler paymentAssembler, String pnr,
			ReservationContactInfo contactInfo, Collection<Integer> colTnxIds) throws ModuleException {
		String comments = "";
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.MAKE_PAYMENT);
		try {
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PAYMENT_INFO, paymentAssembler.getPayments());
			// setting up an empty array list to by pass validations
			command.setParameter(CommandParamNames.OND_FARE_DTOS, new ArrayList<Integer>());
			command.setParameter(CommandParamNames.TRIGGER_BLOCK_SEATS, new Boolean(false));
			// setting up an empty array list to by pass validations
			command.setParameter(CommandParamNames.BLOCK_KEY_IDS, new ArrayList<Integer>());
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.CC_FRAUD_ENABLE, new Boolean(false));
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(true));

			ServiceResponce response = command.execute();
			response.addResponceParam(CommandParamNames.PAYMENT_INFO, paymentAssembler.getPayments());
			return response;

		} catch (ModuleException mx) {
			// Possibility of executing follwoing blocks will be very low. Because this will be just make payment and no
			// other
			// heavy processes after that (like create reservation, etc.)
			log.error(" ModuleException : make payment " + ReservationApiUtils.getElementsInCollection(colTnxIds), mx);
			comments = "ModuleException : make payment";
			// Update the tempory payment entries. Note that temp payment entries will be updated as reservation
			// failure.
			// But this is not the common reservation failure scenario
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(paymentAssembler.getPayments(), trackInfo, mx.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(mx, mx.getExceptionCode(), mx.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error("CommonsDataAccessException : make payment " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					cdaex);
			comments = "CommonsDataAccessException : make payment";
			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(paymentAssembler.getPayments(), trackInfo, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce refundPayment(TrackInfoDTO trackInfo, PaymentAssembler paymentAssembler, String pnr,
			ReservationContactInfo contactInfo, Collection<Integer> tempTnxIds) throws ModuleException {
		// Note : For credit cards we have reverse and refund methods. From this command system will call the refund
		// only.
		// This should be the behaviour because this method can also be initiated from LCC. So this should be a refund
		// instead of reversal.
		// Refer LCCCreateReservationBL book method to check the flow.

		String comments = "";
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);

		ReservationBO.getPaymentInfo(paymentAssembler, pnr);

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.REVERSE_PAYMENT);
		try {
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PAYMENT_INFO, paymentAssembler.getPayments());
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, tempTnxIds);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.SET_PREVIOUS_CREDIT_CARD_PAYMENT, true);
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, new Boolean(true));

			ServiceResponce response = command.execute();
			response.addResponceParam(CommandParamNames.PAYMENT_INFO, paymentAssembler.getPayments());

			return response;

		} catch (ModuleException mx) {
			// Possibility of executing follwoing blocks will be very low. Because this will be just refund payment.
			// This is suppose to call for a system failure and not by a user interaction.
			// You can argue we dont need the reversal here because if it is refunded we are done.
			// But hiding the exception is not good.
			log.error("ModuleException : refund payment " + ReservationApiUtils.getElementsInCollection(tempTnxIds), mx);
			comments = "ModuleException : refund payment";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(paymentAssembler.getPayments(), trackInfo, mx.getExceptionCode(), tempTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(mx, mx.getExceptionCode(), mx.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error("CommonsDataAccessException : refund payment " + ReservationApiUtils.getElementsInCollection(tempTnxIds),
					cdaex);
			comments = "CommonsDataAccessException : refund payment";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(paymentAssembler.getPayments(), trackInfo, cdaex.getExceptionCode(), tempTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TempPaymentTnx recordTemporyPaymentEntry(TemporyPaymentTO temporyPaymentTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			return LccBL.recordTemporyPaymentEntry(temporyPaymentTO, credentialsDTO);
		} catch (ModuleException mx) {
			log.error(" ((o)) ModuleException::recordTemporyPaymentEntry ", mx);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(mx, mx.getExceptionCode(), mx.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::recordTemporyPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce isExternalReferenceExist(String externalReference, String status, char productType)
			throws ModuleException {
		DefaultServiceResponse defaultServiceResponse = null;
		try {

			defaultServiceResponse = new DefaultServiceResponse(false);

			Collection<TempPaymentTnx> tnxCollection = PaymentGatewayBO.getTempPaymentsForReservation(externalReference, status,
					productType);
			boolean isPaymentExist = PaymentGatewayBO.isExternalReferenceExist(externalReference);

			Map<String, String> paymentMap = null;
			if (tnxCollection == null || tnxCollection.size() == 0) {
				paymentMap = PaymentGatewayBO.getAgentPayments(externalReference);
			}

			if (tnxCollection != null && tnxCollection.size() > 0) {
				defaultServiceResponse.setSuccess(true);
				for (TempPaymentTnx tempPaymentTnx : tnxCollection) {
					if (tempPaymentTnx != null && tempPaymentTnx.getAmount() != null) {
						defaultServiceResponse.addResponceParam(ReservationInternalConstants.TempPaymentTnxConstants.TOTAL_AMOUNT,
								tempPaymentTnx.getAmount());

						defaultServiceResponse.addResponceParam(
								ReservationInternalConstants.TempPaymentTnxConstants.EXTERNAL_REFERENCE, isPaymentExist);
					}
				}
			} else if (paymentMap != null && !paymentMap.isEmpty()) {
				if (paymentMap.containsKey(externalReference)) {
					defaultServiceResponse.setSuccess(true);
					defaultServiceResponse.addResponceParam(ReservationInternalConstants.TempPaymentTnxConstants.TOTAL_AMOUNT,
							new BigDecimal(paymentMap.get(externalReference)));
					defaultServiceResponse.addResponceParam(
							ReservationInternalConstants.TempPaymentTnxConstants.EXTERNAL_REFERENCE, isPaymentExist);
				}
			}

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::recordTemporyPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return defaultServiceResponse;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce isExternalReferenceExist(String externalReference, char productType) throws ModuleException {
		DefaultServiceResponse defaultServiceResponse = null;
		try {
			defaultServiceResponse = new DefaultServiceResponse(false);
			Collection<TempPaymentTnx> tnxCollection = PaymentGatewayBO.getTempPaymentsForReservation(externalReference,
					productType);

			if (tnxCollection != null && tnxCollection.size() > 0) {
				defaultServiceResponse.setSuccess(true);
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::recordTemporyPaymentEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return defaultServiceResponse;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Collection<ReservationTnx>> getAllBalanceTransactions(Collection<Integer> pnrPaxIds)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getAllBalanceTransactions(pnrPaxIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getAllBalanceTransactions ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Get Checkin passenger details
	 * 
	 * @param flightId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PassengerCheckinDTO> getCheckinPassengers(int flightId, String airportCode, String checkinStatus,
			boolean standBy, boolean goShow) throws ModuleException {
		try {
			ReservationAuxilliaryDAO reservationAuxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			ReservationTnxDAO reservationTnxDAO = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
			Collection<PassengerCheckinDTO> colChecking = reservationAuxilliaryDAO.getCheckinPassengers(flightId, airportCode,
					checkinStatus, standBy, goShow);
			if (colChecking == null || colChecking.isEmpty()) {
				return new ArrayList<PassengerCheckinDTO>();
			}

			return reservationTnxDAO.getPNRPaxPendingPayments(colChecking);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getCheckinPassengers ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Get Class of serveice for flight
	 * 
	 * @param flightId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String> getCOSForFlight(int flightId) throws ModuleException {
		try {
			ReservationAuxilliaryDAO reservationAuxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			return reservationAuxilliaryDAO.getClassOfServicesForFlight(flightId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightManifest ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * This method will update the checkin status of the passenger
	 * 
	 * @param pnrPaxFareSegId
	 * @param checkinStatus
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updatePaxCheckinStatus(ArrayList<Integer> pnrPaxFareSegId, String checkinStatus) throws ModuleException {
		try {
			ReservationAuxilliaryDAO reservationAuxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			reservationAuxilliaryDAO.updatePaxCheckinStatus(pnrPaxFareSegId, checkinStatus);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightManifest ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReservationPaxFare> getReservationPaxFare(Integer pnrPaxId) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_DAO.getReservationPaxFare(pnrPaxId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer saveReservationInsurance(ReservationInsurance reservationInsurance) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.saveReservationInsurance(reservationInsurance);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Reservation getReservation(String pnr, boolean loadFares) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getReservation(pnr, loadFares);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Reservation getReservationNonTnx(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			Reservation reservation;

			if (pnrModesDTO.isLoadLocalTimes()) {
				// Get the caller credentials
				reservation = PnrZuluProxy.getReservationWithLocalTimes(pnrModesDTO, credentialsDTO);
			} else {
				reservation = ReservationProxy.getReservation(pnrModesDTO);
			}

			return reservation;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationInsurance getReservationInsurance(int insuranceRefNumber) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getReservationInsurance(insuranceRefNumber);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String> getCarrierCodesByAirline(String airlineCode) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getCarrierCodesByAirline(airlineCode);
	}

	/**
	 * Returns pfs entry
	 * 
	 * @param fromAirport
	 * @param flightNumber
	 * @param departureDate
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int getPFS(String fromAirport, String flightNumber, Date departureDate) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.getPFS(fromAirport, flightNumber, departureDate);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPFS ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updatePnrSegmentNotifyStatus(
			Collection<ReservationSegmentNotificationEvent> reservationSegmentNotificationEventCol) throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		reservationDAO.updatePnrSegmentNotifyStatus(reservationSegmentNotificationEventCol);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PnrFarePassenger getPnrFarePassenger(String pnr, int flightSegmentId, int paxSequence) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getPnrFarePassenger(pnr, flightSegmentId, paxSequence);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrFarePassenger ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<AncillaryReminderDetailDTO> getFlightSegmentsToScheduleReminder(Date date, String schedulerType)
			throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return (List<AncillaryReminderDetailDTO>) reservationDAO.getFlightSegmentsToScheduleReminder(date, schedulerType);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendAncillaryReminderNotification(AncillaryReminderDetailDTO reminderDetailDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			AncillaryReminderSenderBO ancillaryReminderSenderBO = new AncillaryReminderSenderBO();
			ancillaryReminderSenderBO.sendAncillaryReminderNotification(reminderDetailDTO, credentialsDTO);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	// This will return the first ppf id linked to the pnrSegId. Need to pass
	// the pnrPaxId if need the exact ppf_id
	public Integer getReservationPaxFareId(Integer pnrSegId) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_DAO.getReservationPaxFareId(pnrSegId);
	}

	@Override
	public String getCurrentEticketNo(String strAgentCode) throws ModuleException {
		return ReservationBO.getTheCurrentETicket(strAgentCode);
	}

	@Override
	public String getCurrentTicketNoOnly(String strAgentCode) throws ModuleException {
		return ReservationBO.getCurrentTicketNoOnly(strAgentCode);
	}

	@Override
	public String getLastActualEticket(String strAgentCode) throws ModuleException {
		return ReservationBO.getLastActualEticketForAgent(strAgentCode);
	}

	@Override
	public Map<Long, Collection<ReservationPaxOndPayment>> getReservationPaxOndPaymentByOndChargeId(Integer pnrPaxId,
			Collection<Long> colPnrPaxOndChgId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getReservationPaxOndPaymentByOndChargeId(pnrPaxId,
					colPnrPaxOndChgId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationPaxOndPaymentByOndChargeId ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce modifySSRs(Map<Integer, SegmentSSRAssembler> paxSSRMap, String pnr, String userNotes,
			IPassenger iPassenger, Integer version, boolean updatePayment, EXTERNAL_CHARGES extChargeType, TrackInfoDTO trackInfo,
			boolean cancelOnly) throws ModuleException {
		try {

			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);
			SSRCommonUtil.modifySSRs(pnr, version, paxSSRMap, iPassenger, userNotes, extChargeType, credentialsDTO, cancelOnly);

			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::modifySeats ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::modifySeats ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce modifyAirportTransfers(AirportTransferAssembler paxAirportTransfer, String pnr, String userNotes,
			IPassenger iPassenger, Integer version, boolean updatePayment, EXTERNAL_CHARGES extChargeType, TrackInfoDTO trackInfo,
			boolean cancelOnly) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadAirportTransferInfo(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			// Checking to see if any concurrent update exist
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
			List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

			AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation, true);
			ChargeTnxSeqGenerator chgTnxSeq = new ChargeTnxSeqGenerator(reservation);
			Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferToRemove = paxAirportTransfer
					.getPaxAirportTransferTORemove();
			if (!paxAirportTransferToRemove.isEmpty()) {
				AirportTransferAdaptor.addPassengerAptChargeAndAdjustCredit(adjustCreditBO, reservation,
						paxAirportTransferToRemove, userNotes, trackInfo, credentialsDTO, chgTnxSeq);
			}
			Collection<ReservationPaxSegAirportTransfer> passengerAPTsToCancel = AirportTransferAdaptor
					.cancelPaxApts(paxAirportTransferToRemove, reservation);

			Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferToAdd = paxAirportTransfer
					.getPaxAirportTransferInfo();
			if (!paxAirportTransferToAdd.isEmpty()) {
				AirportTransferAdaptor.addPassengerAptChargeAndAdjustCredit(adjustCreditBO, reservation, paxAirportTransferToAdd,
						userNotes, trackInfo, credentialsDTO, chgTnxSeq);
			}
			Collection<ReservationPaxSegAirportTransfer> passengerAPTsToAdd = AirportTransferAdaptor
					.addModifyPaxApts(paxAirportTransferToAdd, credentialsDTO);

			AirportTransferBL.modifyAirportTransfer(reservation, adjustCreditBO, pnr, passengerAPTsToAdd, passengerAPTsToCancel);

			reservation = ReservationProxy.getReservation(pnrModesDTO);
			List<Integer> forcedConfirmedPaxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
			handleETGeneration(reservation, forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange,
					credentialsDTO);

			// record audit for modify apts
			if (!paxAirportTransferToAdd.isEmpty() || !paxAirportTransferToRemove.isEmpty()) {
				Collection[] colResInfo = { reservation.getPassengers(), reservation.getSegmentsView() };
				AirportTransferAdaptor.recordPaxWiseAuditHistoryForModifyApts(pnr, paxAirportTransferToAdd,
						paxAirportTransferToRemove, credentialsDTO, userNotes, colResInfo);
			}

			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::modifySeats ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::modifySeats ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public ServiceResponce modifyAutoCheckins(IPassengerAutomaticCheckin ipaxAutoCheckin, String pnr, String userNotes,
			Integer version, boolean updatePayment, TrackInfoDTO trackInfo) throws ModuleException {
		try {

			AutomaticCheckinAssembler paxAutoCheckinAssembler = (AutomaticCheckinAssembler) ipaxAutoCheckin;
			IPassenger iPassenger = paxAutoCheckinAssembler.getPassenger();
			boolean isForceConfirmed = paxAutoCheckinAssembler.isForceConfirmed();
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadAutoCheckinInfo(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			// Checking to see if any concurrent update exist
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
			List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

			AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation, true);
			ChargeTnxSeqGenerator chgTnxSeq = new ChargeTnxSeqGenerator(reservation);
			Map<String, Collection<PaxAutomaticCheckinTO>> paxAutomaticChecinToAdd = paxAutoCheckinAssembler.getPaxAutoCheckinInfo();
			if (!paxAutomaticChecinToAdd.isEmpty()) {
				AutomaticCheckinAdaptor.addPassengerAutoCknChargeAndAdjustCredit(adjustCreditBO, reservation,
						paxAutomaticChecinToAdd, userNotes, trackInfo, credentialsDTO, chgTnxSeq);
			}

			Map<String, Collection<PaxAutomaticCheckinTO>> paxAutoCheckinTORemove = paxAutoCheckinAssembler
					.getPaxAutomaticChecinTORemove();
			if (!paxAutoCheckinTORemove.isEmpty()) {
				AutomaticCheckinAdaptor.addPassengerAutoCknChargeAndAdjustCredit(adjustCreditBO, reservation,
						paxAutoCheckinTORemove, userNotes, trackInfo, credentialsDTO, chgTnxSeq);
			}

			ADLNotifyer.updateStatusToCHG(reservation);
			if (!isForceConfirmed) {
				// // Recording adl change operation according to new implemenatation
				ADLNotifyer.recordReservation(reservation, AdlActions.C,
						getAncilliaryAddedPassengers(paxAutomaticChecinToAdd.keySet(), paxAutoCheckinTORemove.keySet()),
						reservation.getStatus());
			}

			// Save the reservation
			ReservationProxy.saveReservation(reservation);

			adjustCreditBO.callRevenueAccountingAfterReservationSave();
			
			// cancel removed auto checkins
			AutomaticCheckinAdaptor.cancelPaxAutoCheckins(paxAutoCheckinTORemove, reservation);

			// save pax autocheckin info
			AutomaticCheckinAdaptor.savePaxAutoCheckinDetails(paxAutomaticChecinToAdd, credentialsDTO);

			reservation = ReservationProxy.getReservation(pnrModesDTO);
			List<Integer> forcedConfirmedPaxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
			handleETGeneration(reservation, forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange, credentialsDTO);

			if (updatePayment && iPassenger != null) {
				// if code comes here means no errors
				updateReservationForPayment(pnr, iPassenger, isForceConfirmed, false, version + 1, trackInfo, true, false,
						true, false, false, false, null, false, null);
			}

			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::modifyAutoCheckins ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::modifyAutoCheckins ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	private static void handleETGeneration(Reservation reservation, List<Integer> forcedConfirmedPaxSeqsBeforeChange,
			List<Integer> forcedConfirmedPaxSeqsAfterChange, CredentialsDTO credentialsDTO) throws ModuleException {

		Collection<ReservationPax> etGenerationEligiblePax = ReservationApiUtils.getConfirmedPaxByOperationUsingForceCNFSeqs(
				forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange, reservation);
		if (reservation.getOriginatorPnr() == null) {
			Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
			if (!etGenerationEligiblePax.isEmpty()) {
				CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
				modifiedPaxfareSegmentEtickets = ETicketBO.updateETickets(
						ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, tktInfoDto), etGenerationEligiblePax,
						reservation, true);

				if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
					Collection<Integer> affectingPnrSegIds = new ArrayList<Integer>();
					for (ReservationPax resPax : reservation.getPassengers()) {
						Iterator<ReservationPaxFare> itReservationPaxFare = resPax.getPnrPaxFares().iterator();
						Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
						ReservationPaxFare reservationPaxFare;
						ReservationPaxFareSegment reservationPaxFareSegment;

						while (itReservationPaxFare.hasNext()) {
							reservationPaxFare = itReservationPaxFare.next();
							itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

							while (itReservationPaxFareSegment.hasNext()) {
								reservationPaxFareSegment = itReservationPaxFareSegment.next();
								for (ReservationPaxFareSegmentETicket resPaxFareSegmentETicket : modifiedPaxfareSegmentEtickets) {
									if (resPaxFareSegmentETicket.getPnrPaxFareSegId()
											.equals(reservationPaxFareSegment.getPnrPaxFareSegId())) {
										affectingPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
									}
								}
							}
						}
					}
					ADLNotifyer.recordReservation(reservation, AdlActions.D, etGenerationEligiblePax, null);
				}
				ADLNotifyer.recordReservation(reservation, AdlActions.A, etGenerationEligiblePax, null);
			}
		} else if (!etGenerationEligiblePax.isEmpty()) {
			ADLNotifyer.recordReservation(reservation, AdlActions.D, etGenerationEligiblePax, null);
			ADLNotifyer.recordReservation(reservation, AdlActions.A, etGenerationEligiblePax, null);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReservationInsurance> getReservationInsuranceByPnr(String pnr) throws ModuleException {

		try {
			List<ReservationInsurance> insuranceList = new ArrayList<ReservationInsurance>(
					ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getReservationInsuranceList(pnr));

			return insuranceList;
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<RakInsuredTraveler> getTravellersForRakInsurancePublishing(int flightSegmentId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getTravellersForRakInsurancePublishing(flightSegmentId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<RakInsuredTraveler> getTravellersForDailyRakInsurancePublishing(Date bookingDate) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getTravellersForDailyRakInsurancePublishing(bookingDate);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public FlightSegNotificationEvent publishRakInuranceData(InsurancePublisherDetailDTO insurancePublisherDetailDTO)
			throws ModuleException {

		try {
			InsuranceDataPublisherBL insuranceDataPublisherBL = new InsuranceDataPublisherBL();
			return insuranceDataPublisherBL.publishFlightInsuranceInfo(insurancePublisherDetailDTO);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String publishRakInuranceDailyData(InsurancePublisherDetailDTO insurancePublisherDetailDTO) throws ModuleException {

		try {
			InsuranceDataPublisherBL insuranceDataPublisherBL = new InsuranceDataPublisherBL();
			return insuranceDataPublisherBL.publishDailyInsuranceInfo(insurancePublisherDetailDTO);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateTnxGranularityFlag(String pnr, String status, String message, Long version) throws ModuleException {
		try {
			message = BeanUtils.nullHandler(message);
			if (message.length() >= 300) {
				message = message.substring(0, 299);
			}

			ReservationDAOUtils.DAOInstance.RESERVATION_DAO.updateTnxGranularityFlag(pnr, status, message, version);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updatePnrsWithTnxGranularityFlag(Collection<String> pnrs, String status) throws ModuleException {
		try {
			if (pnrs.size() > 0) {
				Collection[] collections = BeanUtils.split(pnrs, 100);

				for (Collection collection : collections) {
					if (collection != null && collection.size() > 0) {
						ReservationDAOUtils.DAOInstance.RESERVATION_DAO.updatePnrsWithTnxGranularityFlag(collection, status);
					}
				}
			}
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void migrateTnxForGranularity(String pnr) {
		Reservation reservation = null;

		try {
			TnxGranularityMigrator tnxGranularityMigrator = new TnxGranularityMigrator(pnr);
			reservation = tnxGranularityMigrator.getReservation();

			tnxGranularityMigrator.execute(reservation);
			ReservationModuleUtils.getReservationBD().updateTnxGranularityFlag(reservation.getPnr(), "S", "",
					reservation.getVersion());
		} catch (Throwable cdaex) {
			this.sessionContext.setRollbackOnly();
			try {
				if (reservation != null) {
					ReservationModuleUtils.getReservationBD().updateTnxGranularityFlag(reservation.getPnr(), "F",
							BeanUtils.nullHandler(cdaex.getMessage()), reservation.getVersion());
				} else {
					ReservationModuleUtils.getReservationBD().updateTnxGranularityFlag(pnr, "F",
							BeanUtils.nullHandler(cdaex.getMessage()), null);
				}
			} catch (Throwable e) {
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal getInsuranceMaxValue(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getInsuranceMaxValue(pnr);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendSms(String pnr, ReservationContactInfo reservationContactInfo, CredentialsDTO credentialsDTO)
			throws ModuleException {
		if (pnr == null || reservationContactInfo == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		if (AppSysParamsUtil.sendSMSForOnholdReservations()) {
			ReservationBO.sendSMSForOnHoldReservations(pnr, reservationContactInfo.getMobileNo(), credentialsDTO);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getActualPaymentMethodById(Integer actualPaymentMethodId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getActualPaymentMethodNameById(actualPaymentMethodId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getActualPaymentMethodById ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int getPFSPaxTypeEntryCount(Integer pfsId, String pnr, String paxType) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			return paxFinalSalesDAO.getPaxTypeCount(pfsId, pnr, paxType);
		} catch (CommonsDataAccessException cdaex) {
			log.error("((o)) CommonsDataAccessException::getPFSPaxTypeCount ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getNewPNR(boolean isGdsReservation, String nextPNRSeqNumber) {
		return PNRNumberGeneratorProxy.getNewPNR(isGdsReservation, nextPNRSeqNumber);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean publishInsuranceSalesData(InsuranceSalesType salesType) throws ModuleException {
		try {
			InsuranceDataPublisherBL insuranceDataPublisherBL = new InsuranceDataPublisherBL();
			Date date = CalendarUtil.previousDate(new Date());

			return insuranceDataPublisherBL.publishDailySaleInsuranceData(
					ReservationDAOUtils.DAOInstance.SALES_DAO.getInsuranceSalesData(date, date, salesType), salesType);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * Returns pfs parse entries
	 * 
	 * @param PNR
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getReservationCabinClass(String pnr, Integer pnrSegID) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.getReservationCabinclass(pnr, pnrSegID);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPfsParseEntries ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public long updateOriginatorPNRForReservation(String pnr) throws ModuleException {
		try {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(false);
			pnrModesDTO.setLoadSegView(false);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(false);

			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			if (StringUtils.isEmpty(reservation.getOriginatorPnr())) {
				reservation.setOriginatorPnr(pnr);
				Reservation savedReservation = ReservationProxy.saveReservation(reservation);
				return savedReservation.getVersion();
			}

			return reservation.getVersion();

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateOriginatorPNRForReservation ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return external payment transactions for the specified request UID.
	 * 
	 * @param reqUID
	 * @return Map<PNR, PNRExtTransactionsTO>
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, PNRExtTransactionsTO> getExtPayTransactionsForReqUID(String reqUID) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getExtPayTransactionsForReqUID(reqUID);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getExtPayTransactionsForReqUID ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * @param ipassengerBaggage
	 * @param pnr
	 * @param userNotes
	 * @param version
	 * @param updatePayment
	 * @param trackInfoDTO
	 * 
	 */
	@SuppressWarnings("rawtypes")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce modifyBaggages(IPassengerBaggage ipassengerBaggage, String pnr, String userNotes, long version,
			boolean updatePayment, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			PassengerBaggageAssembler passengerBaggageAssembler = (PassengerBaggageAssembler) ipassengerBaggage;
			IPassenger iPassenger = passengerBaggageAssembler.getPassenger();
			boolean isForceConfirmed = passengerBaggageAssembler.isForceConfirmed();

			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			// Return the reservation
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			// Checking to see if any concurrent update exist
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
			List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

			BaggageAdaptor.prepareBaggagesToModify(reservation, passengerBaggageAssembler, trackInfoDTO);
			// Map<String, PaxBaggageTO> paxIdBaggageToAdd = passengerBaggageAssembler.getPaxBaggageTOAdd();
			Map<String, PaxBaggageTO> paxIdBaggageToAdd = passengerBaggageAssembler.getPaxBaggageInfo();
			Collection<PaxBaggageTO> paxBaggageDTOs = new ArrayList<PaxBaggageTO>();

			AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation, true);
			ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
			if (!paxIdBaggageToAdd.isEmpty()) {
				paxBaggageDTOs.addAll(paxIdBaggageToAdd.values());

				BaggageAdaptor.addPassengerBaggageChargeAndAdjustCredit(adjustCreditBO, reservation, paxIdBaggageToAdd, userNotes,
						trackInfoDTO, credentialsDTO, chgTnxGen);
			}

			Map<String, PaxBaggageTO> paxIdBaggageToRem = passengerBaggageAssembler.getPaxBaggageTORemove();
			if (!paxIdBaggageToRem.isEmpty()) {
				BaggageAdaptor.addPassengerBaggageChargeAndAdjustCredit(adjustCreditBO, reservation, paxIdBaggageToRem, userNotes,
						trackInfoDTO, credentialsDTO, chgTnxGen);
			}

			ADLNotifyer.updateStatusToCHG(reservation);
			if (!isForceConfirmed) {
				// // Recording adl change operation according to new implemenatation
				ADLNotifyer.recordReservation(reservation, AdlActions.C,
						getAncilliaryAddedPassengers(paxIdBaggageToAdd.keySet(), paxIdBaggageToRem.keySet()),
						reservation.getStatus());
			}
			// Save the reservation
			ReservationProxy.saveReservation(reservation);

			adjustCreditBO.callRevenueAccountingAfterReservationSave();

			// modify flight baggage statuss
			BaggageAdaptor.modifyBaggages(paxIdBaggageToAdd, paxIdBaggageToRem);

			// save passenger- baggage info
			BaggageAdaptor.savePaxBaggageInfo(paxBaggageDTOs, credentialsDTO);

			reservation = ReservationProxy.getReservation(pnrModesDTO);
			List<Integer> forcedConfirmedPaxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
			handleETGeneration(reservation, forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange,
					credentialsDTO);

			if (updatePayment && iPassenger != null) {
				// if code comes here means no errors
				// updateReservationForPayment(pnr, iPassenger, isForceConfirmed, false, version+1, trackInfoDTO, true,
				// false);
				updateReservationForPayment(pnr, iPassenger, isForceConfirmed, false, version + 1, trackInfoDTO, true, false,
						true, false, false, false, null, false, null);
			}

			// record audit for modify baggages
			if (!paxIdBaggageToAdd.isEmpty() || !paxIdBaggageToRem.isEmpty()) {
				Collection[] colResInfo = { reservation.getPassengers(), reservation.getSegmentsView() };
				BaggageAdaptor.recordPaxWiseAuditHistoryForModifyBaggages(pnr, paxIdBaggageToAdd, paxIdBaggageToRem,
						credentialsDTO, userNotes, colResInfo);
			}

			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::modifyBaggages ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::modifyBaggages ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public void updateReleaseTimeForDummyReservation(String pnr, Date releaseTimeStamp) throws ModuleException {
		try {
			ReservationDAOUtils.DAOInstance.RESERVATION_DAO.updateReleaseTimeForDummyReservation(pnr, releaseTimeStamp);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	public ReservationTnx getLccTransaction(String pnrPaxId, String lccUniqueId) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getLccTransaction(pnrPaxId, lccUniqueId);
	}

	@Override
	public BigDecimal getPayedAmountFromOndFareDto(Collection<OndFareDTO> colOndFareDTO, String paxType) throws ModuleException {
		return ReservationApiUtils.getPayedAmountFromOndFareDto(colOndFareDTO, paxType);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isReservationOnholdable(ResOnholdValidationDTO onholdValidationDTO) throws ModuleException {
		boolean ohdAllowed;

		// if IBE onhold is disabled and the validation is not IBEPAYMENT validation
		if (!AppSysParamsUtil.isOnHoldEnable(OnHold.IBE) && !AppSysParamsUtil.isOnHoldEnable(OnHold.IBEOFFLINEPAYMENT)
				&& ResOnholdValidationDTO.IBE_PAYMENT_VALIDATION != onholdValidationDTO.getValidationType()) {
			if (log.isInfoEnabled()) {
				log.info("### IBE onhold not allow for unmatched validation type");
			}
			return false;
		}
		// if validation is IBEPAYMENT validation and IBEPAYMENT onhold is disabled
		else if (onholdValidationDTO.getValidationType() == ResOnholdValidationDTO.IBE_PAYMENT_VALIDATION
				&& !AppSysParamsUtil.isOnHoldEnable(OnHold.IBEPAYMENT)) {
			if (log.isInfoEnabled()) {
				log.info("### IBE onhold not allow for IBEPAYMENT app parameter ");
			}
			return false;
		}

		switch (onholdValidationDTO.getValidationType()) {
		case ResOnholdValidationDTO.COMBINE_VALIDATION:
			// Pax + other validations (IP, Flight & depature cutoff constraint check)

			if (ValidationUtils.isOHDAllowForBookingDate(onholdValidationDTO.getDepatureDateList())
					&& ValidationUtils.isOHDAllowForCountry(onholdValidationDTO.getIpAddress())
					&& ValidationUtils.isOHDAllowForIp(onholdValidationDTO.getIpAddress(), onholdValidationDTO.getSalesChannel())
					&& ValidationUtils.isOHDAllowForFlight(onholdValidationDTO.getFlightSegIdList(),
							onholdValidationDTO.getSalesChannel())
					&& ValidationUtils.isOHDAllowForPax(onholdValidationDTO.getPaxList(), onholdValidationDTO.getContactEmail(),
							onholdValidationDTO.getSalesChannel())) {
				ohdAllowed = true;
			} else {
				ohdAllowed = false;
			}

			break;
		case ResOnholdValidationDTO.OTHER_VALIDATION:
			// IP, Flight & depature cutoff constraint check

			if (ValidationUtils.isOHDAllowForBookingDate(onholdValidationDTO.getDepatureDateList())
					&& ValidationUtils.isOHDAllowForCountry(onholdValidationDTO.getIpAddress())
					&& ValidationUtils.isOHDAllowForIp(onholdValidationDTO.getIpAddress(), onholdValidationDTO.getSalesChannel())
					&& ValidationUtils.isOHDAllowForFlight(onholdValidationDTO.getFlightSegIdList(),
							onholdValidationDTO.getSalesChannel())) {
				ohdAllowed = true;
			} else {
				ohdAllowed = false;
			}

			break;

		case ResOnholdValidationDTO.PAX_VALIDATION:
			// Only pax constraint is checked here

			ohdAllowed = ValidationUtils.isOHDAllowForPax(onholdValidationDTO.getPaxList(), onholdValidationDTO.getContactEmail(),
					onholdValidationDTO.getSalesChannel());

			break;
		case ResOnholdValidationDTO.IBE_PAYMENT_VALIDATION:

			if (ValidationUtils.isOHDAllowForIp(onholdValidationDTO.getIpAddress(), onholdValidationDTO.getSalesChannel())
					&& ValidationUtils.isOHDAllowForFlight(onholdValidationDTO.getFlightSegIdList(),
							onholdValidationDTO.getSalesChannel())
					&& ValidationUtils.isOHDAllowForPax(onholdValidationDTO.getPaxList(), onholdValidationDTO.getContactEmail(),
							onholdValidationDTO.getSalesChannel())) {
				ohdAllowed = true;
			} else {
				ohdAllowed = false;
			}

			break;

		case ResOnholdValidationDTO.IBE_OFFLINE_VALIDATION:

			if (ValidationUtils.isOHDAllowForBookingDate(onholdValidationDTO.getDepatureDateList())
					&& ValidationUtils.isOHDAllowForIp(onholdValidationDTO.getIpAddress(), onholdValidationDTO.getSalesChannel())
					&& ValidationUtils.isOHDAllowForFlight(onholdValidationDTO.getFlightSegIdList(),
							onholdValidationDTO.getSalesChannel())) {
				ohdAllowed = true;
			} else {
				ohdAllowed = false;
			}

			break;

		default:
			// Shouldn't come here
			ohdAllowed = false;
			break;

		}

		return ohdAllowed;
	}

	/**
	 * Return Reservation TnxGranularity
	 * 
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isReservationTnxGranularityEnable(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.isReservationTnxGranularityEnable(pnr);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassengerTitleMap ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean hasPayments(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.hasPayments(pnr);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassengerTitleMap ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public PfsPaxEntry getPfsInfantPaxParent(int infantPPID) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.getParent(infantPPID);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPFS ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public PfsPaxEntry getPfsPaxEntry(int ppId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.getPFSPaxEntry(ppId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPFS ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Long, Collection<ReservationPaxOndPayment>> getReservationPaxOndPayments(Collection<Long> colPaymentTnxIds)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getReservationPaxOndPayments(colPaymentTnxIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationPaxOndPayments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> getReservationPaxSegmentPayments(
			Collection<Integer> colPaymentTnxIds, Collection<String> paxSeqLccUniqueTIDs,
			Collection<Integer> requestingNominalCodes) throws ModuleException {

		if (colPaymentTnxIds == null && paxSeqLccUniqueTIDs == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (requestingNominalCodes == null || requestingNominalCodes.isEmpty()) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		try {
			Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> reservationPaxSegmentPaymentsMap = new HashMap<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>();

			if (colPaymentTnxIds != null && colPaymentTnxIds.size() > 0) {
				reservationPaxSegmentPaymentsMap.putAll(ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
						.getReservationPaxSegmentPaymentsForOwnReservation(colPaymentTnxIds, requestingNominalCodes));
			}

			if (AppSysParamsUtil.isLCCConnectivityEnabled() && paxSeqLccUniqueTIDs != null && paxSeqLccUniqueTIDs.size() > 0) {
				reservationPaxSegmentPaymentsMap.putAll(ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
						.getReservationPaxSegmentPaymentsForGroupReservation(paxSeqLccUniqueTIDs, requestingNominalCodes));
			}

			return reservationPaxSegmentPaymentsMap;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationPaxSegmentPayments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Pair<String, Integer>, BigDecimal> getHandlingChargesForPaxTnxs(Collection<Integer> transactionIds,
			Collection<String> lccUniqueTnxIds) throws ModuleException {
		if (transactionIds == null && lccUniqueTnxIds == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		try {
			Map<Pair<String, Integer>, BigDecimal> handlingChargesForPaxTnxs = new HashMap<Pair<String, Integer>, BigDecimal>();

			Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
			String handlingChargeCode = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE.toString()).toString();

			if (StringUtils.isEmpty(handlingChargeCode)) {
				throw new ModuleException("airreservations.arg.invalid.null", "Cannot find Handling Charge Code");
			}

			if (transactionIds != null && transactionIds.size() > 0) {
				handlingChargesForPaxTnxs.putAll(ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
						.getHandlingChargesForOwnResPaxTnxs(transactionIds, handlingChargeCode));
			}

			if (AppSysParamsUtil.isLCCConnectivityEnabled() && lccUniqueTnxIds != null && lccUniqueTnxIds.size() > 0) {
				handlingChargesForPaxTnxs.putAll(ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
						.getHandlingChargesForGroupResPaxTnxs(lccUniqueTnxIds, handlingChargeCode));
			}

			return handlingChargesForPaxTnxs;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getHandlingChargesForPaxTnxs ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean checkSSRChargeAssignedInReservation(int ssrId) throws ModuleException {
		try {
			return SSRBL.checkSSRChargeAssignedInReservation(ssrId);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::checkSSRChargeAssignedInReservation ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::checkSSRChargeAssignedInReservation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendEmailMedicalSsr(CommonMedicalSsrParamDTO medicalSsrDTO) throws ModuleException {
		try {
			SSRBL.sendEmailMedicalSsr(medicalSsrDTO);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::sendEmailMedicalSsr ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::sendEmailMedicalSsr ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long getOnHoldReleaseTime(OnHoldReleaseTimeDTO onHoldReleaseTimeDTO) throws ModuleException {
		try {
			return ReservationBO.getOnHoldReleaseTime(onHoldReleaseTimeDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getReservationPaxOndPayments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<RollForwardFlightAvailRS> getAvaialableFlightForRollForward(RollForwardFlightSearchDTO rollForwardFlightSearch)
			throws ModuleException {
		try {
			return ReservationBO.getAvaialableFlightForRollForward(rollForwardFlightSearch);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getAvaialableFlightForRollForward ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveOrUpdateOnholdReleaseTime(OnholdReleaseTime onholdReleaseTime, Map<String, String[]> contMap)
			throws ModuleException {
		try {
			long versionFlg = onholdReleaseTime.getVersion();
			OnholdReleaseTimeDAO onholdReleaseTimeDAO = ReservationDAOUtils.DAOInstance.ONHOLD_RELEASE_TIME_DAO;
			onholdReleaseTimeDAO.saveOrUpdateOnholdReleaseTimeConfig(onholdReleaseTime);
			OnholdReleaseTimeAdaptor.auditForModifyOnholdReleaseTimeConfigModification(getUserPrincipal(), onholdReleaseTime,
					contMap, versionFlg);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveOrUpdateOnholdReleaseTime ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteOnholdReleaseTimeConfig(int releaseTimeId) throws ModuleException {
		ReservationDAOUtils.DAOInstance.ONHOLD_RELEASE_TIME_DAO.deleteOnholdReleaseTimeConfig(releaseTimeId);
		OnholdReleaseTimeAdaptor.auditForDeleteOnholdReleaseTimeConfigModification(getUserPrincipal(), releaseTimeId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public OnholdReleaseTime getOnholdReleaseTime(int releaseTimeId) throws ModuleException {
		OnholdReleaseTime onholdRelTime = ReservationDAOUtils.DAOInstance.ONHOLD_RELEASE_TIME_DAO
				.getOnholdReleaseTime(releaseTimeId);
		onholdRelTime.setUser(getUserPrincipal().getUserId());
		return onholdRelTime;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page getOnholdReleaseTimes(ConfigOnholdTimeLimitsSearchDTO configOnholdTimeLimitsSearchDTO, int start, int recSize)
			throws ModuleException {
		return ReservationDAOUtils.DAOInstance.ONHOLD_RELEASE_TIME_DAO.getOnholdReleaseTimes(configOnholdTimeLimitsSearchDTO,
				start, recSize);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isOhdRelCfgLegal(OnholdReleaseTime onholdRelTime) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.ONHOLD_RELEASE_TIME_DAO.isOhdRelCfgLegal(onholdRelTime);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce rollForwardOnholdBooking(String sourcePnr, AvailableFlightSearchDTO availableFlightSearchDTO,
			List<ReservationSegmentDTO> reservationSegmentDTOs, Map<String, RollForwardFlightRQ> rollForwardingFlights,
			String userId, TrackInfoDTO trackInfoDTO, boolean isDuplicateNameSkip) throws ModuleException {
		try {

			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ROLL_FORWARD_ONHOLD_BOOKING_MACRO);
			command.setParameter(CommandParamNames.PNR, sourcePnr);
			command.setParameter(CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO, availableFlightSearchDTO);
			command.setParameter(CommandParamNames.RES_FLIGHT_SEGMENT_DTO, reservationSegmentDTOs);
			command.setParameter(CommandParamNames.ROLL_FORWARD_FLIGHT_LIST, rollForwardingFlights);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.USER_ID, userId);
			command.setParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED, false);
			command.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT, true);
			command.setParameter(CommandParamNames.IS_DUPLICATE_NAME_SKIP, isDuplicateNameSkip);
			return command.execute();
		} catch (ModuleException me) {
			log.error(" ((o)) ModuleException::rollForwardOnholdBooking ", me);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::rollForwardOnholdBooking ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the correct flight segment id
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Integer getFlightSegmentIdBySegmentCode(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode, String segmentCode) throws ModuleException {
		try {
			ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

			return reservationSegmentDAO.getFlightSegmentIdBySegmentCode(flightNumber, departureDate, fromSegmentCode,
					toSegmentCode, segmentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightSegmentIdBySegmentCode ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getHistoryForPrint(String pnr, Collection<UserNoteHistoryTO> colUserNoteHistoryTO) throws ModuleException {
		try {
			return HistoryBL.getHistoryForPrint(pnr, colUserNoteHistoryTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getHistoryForPrint ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updatePassengerEtickets(String pnr, Map<Integer, Map<Integer, EticketDTO>> passengerWiseEticketNumbers,
			boolean exchangePreviousEtickets) throws ModuleException {

		try {
			// Retrieves the Reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = ETicketBO.updateETickets(
					passengerWiseEticketNumbers, reservation.getPassengers(), reservation, exchangePreviousEtickets);

			Collection<ReservationPax> etGenerationEligiblePaxForAdl = getUpdatedETHavingPassengers(
					modifiedPaxfareSegmentEtickets);
			ADLNotifyer.recordReservation(reservation, AdlActions.A, etGenerationEligiblePaxForAdl, reservation.getStatus());
			ADLNotifyer.recordExchangedETicketForAdl(modifiedPaxfareSegmentEtickets, reservation, etGenerationEligiblePaxForAdl);

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updatePassengerEtickets", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error(" ((o)) CommonsDataAccessException::updatePassengerEtickets ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ResSegmentEticketInfoDTO> getEticketInformation(List<String> lccUniqueIDs, List<Integer> txnIDs)
			throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getEticketInfo(lccUniqueIDs, txnIDs);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getEticketInformation ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbers(Collection<Integer> passengerSequnce,
			List<Integer> segmentSequnce, CreateTicketInfoDTO ticketInfoDto) throws ModuleException {
		try {
			return ETicketBO.generateIATAETicketNumbersForDryInterLine(passengerSequnce, segmentSequnce, ticketInfoDto);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::generateIATAETicketNumbers ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbers(Reservation reservation,
			CreateTicketInfoDTO ticketingInfoDto) throws ModuleException {
		try {
			return ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, ticketingInfoDto);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::generateIATAETicketNumbers ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Integer> getFlightSegmentIds(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getFlightSegmentIds(flightNumber, departureDate,
					fromSegmentCode, toSegmentCode);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightSegmentIds ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Integer getPassengerIdByETicketNumber(String eticketNumber) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.ETicketDAO.getPassengerIdByETicketNumber(eticketNumber);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassengerIdByETicketNumber ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EticketTO getPassengerETicketDetail(Integer pnrPaxId, Integer flightId) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.ETicketDAO.getPassengerETicketDetail(pnrPaxId, flightId);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassengerETicketDetail ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getPNRByEticketNo(String eticketNumber) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.ETicketDAO.getPNRByEticketNo(eticketNumber);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPNRByEticketNo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Set<String>> getPNRsByEticketNo(List<String> eticketNumbers) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.ETicketDAO.getPNRsByEticketNo(eticketNumbers);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPNRsByEticketNo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ETicketUpdateResponseDTO updateETicketStatus(ETicketUpdateRequestDTO eTicketUpdateRequestDTO) throws ModuleException {
		try {
			return ETicketBO.updateETicketStatus(eTicketUpdateRequestDTO, this.getCallerCredentials(null));
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateETicketStatus", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateETicketStatus ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updateExternalEticketInfo(List<EticketTO> eticketTOs) throws ModuleException {
		try {
			ETicketBO.updateExternalEticketInfo(eticketTOs);
			DefaultServiceResponse response = new DefaultServiceResponse(true);
			return response;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateExternalETicketStatus", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateExternalETicketStatus ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public void exchangePassengerEtickets(String pnr, List<Integer> oldPnrSegmentIds, List<Integer> newSegmentsSequenceIds)
			throws ModuleException {
		try {
			// Retrieves the Reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			ETicketBO.exchangeETickets(reservation, oldPnrSegmentIds, newSegmentsSequenceIds);

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updatePassengerEtickets", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error(" ((o)) CommonsDataAccessException::updatePassengerEtickets ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	public void exchangeEticketsForGivenPassengers(String pnr, List<Integer> oldPnrSegmentIds,
			List<Integer> newSegmentsSequenceIds, List<Integer> paxSeqsToExchangeET) throws ModuleException {
		try {
			// Retrieves the Reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			Collection<ReservationPax> passengersToExchangeET = new HashSet<ReservationPax>();
			for (ReservationPax passenger : reservation.getPassengers()) {
				if (paxSeqsToExchangeET.contains(passenger.getPaxSequence())) {
					passengersToExchangeET.add(passenger);
				}
			}

			ETicketBO.exchangeETicketsForGivenPassengers(reservation, oldPnrSegmentIds, newSegmentsSequenceIds,
					passengersToExchangeET);

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updatePassengerEtickets", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error(" ((o)) CommonsDataAccessException::updatePassengerEtickets ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Object[] reconcilePfsEntry(PfsPaxEntry pfsParsedEntry, Collection<PfsPaxEntry> colErrorPfsParsedEntry,
			CredentialsDTO credentialsDTO, String checkinMethod, Map<Integer, FlightReconcileDTO> allFlightReconcileDTOs, Pfs pfs)
			throws ModuleException {
		try {

			return (new ReconcileReservationServices()).reconcilePfsEntry(pfsParsedEntry, colErrorPfsParsedEntry, credentialsDTO,
					checkinMethod, allFlightReconcileDTOs, pfs);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reconcilePfsEntry", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reconcilePfsEntry ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendIBEOnholdReservationEmail(OnholdReservatoinSearchDTO onHoldReservationSearchDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			IBEOnholdEmailNotifierBL notiferBL = new IBEOnholdEmailNotifierBL();
			notiferBL.sendNotifications(credentialsDTO, onHoldReservationSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::sendIBEOnholdReservationEmail ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateOnHoldAlert(String pnr, char newStatus) throws ModuleException {
		try {
			IBEOnholdEmailNotifierBL notiferBL = new IBEOnholdEmailNotifierBL();
			notiferBL.updateOnHoldAlert(pnr, newStatus);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateOnHoldAlert ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean checkIfPfsAlreadyExists(Date date) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			return paxFinalSalesDAO.hasPFSFile(date);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::checkIfPfsAlreadyExists ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean checkIfPfsAlreadyExists(Date departureDate, String flightNumber, String fromAirport) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			return paxFinalSalesDAO.checkIfPfsAlreadyExists(departureDate, flightNumber, fromAirport);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::checkIfPfsAlreadyExists ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAuditNextSeatPromotionRequest(String pnr, String status, Map<String, String> contentMapForAudit)
			throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			PromotionBL.saveAuditNextSeatPromotionRequest(pnr, status, contentMapForAudit, credentialsDTO);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::saveAuditNextSeatPromotionRequest", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveAuditNextSeatPromotionRequest ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAuditFlexiDatePromotionRequest(String pnr, String status, Map<String, Object> contentMapForAudit)
			throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			PromotionBL.saveAuditFlexiDatePromotionRequest(pnr, status, contentMapForAudit, credentialsDTO);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::saveAuditFlexiDatePromotionRequest", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveAuditFlexiDatePromotionRequest ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveOrUpdate(Collection<ReservationPaxFareSegmentETicket> eTickets) throws ModuleException {
		try {
			ETicketDAO eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
			eticketDAO.saveOrUpdate(eTickets);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::saveOrUpdate", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveOrUpdate ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservationPaxFareSegmentETicket getPaxFareSegmentEticket(String eticketNumber, int coupNumber, String msgType)
			throws ModuleException {
		try {
			ETicketDAO eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
			return eticketDAO.getPaxFareSegmentEticket(eticketNumber, coupNumber, msgType);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getPaxFareSegmentEticket", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPaxFareSegmentEticket ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getBookingClassType(String eticketNumber, int coupNumber) throws ModuleException {
		try {
			ETicketDAO eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
			return eticketDAO.getBookingClassType(eticketNumber, coupNumber);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getBookingClassType", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getBookingClassType ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EticketTO getEticketInfo(String eticketNumber, Integer coupNumber) throws ModuleException {
		try {
			ETicketDAO eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
			return eticketDAO.getEticketInfo(eticketNumber, coupNumber);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getEticketInfo", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getEticketInfo ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public void updatePaxFareSegments(Collection<Integer> colPpfsId, String status) throws ModuleException {
		try {
			ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
			reservationSegmentDAO.updatePaxFareSegments(colPpfsId, status);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updatePaxFareSegments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer updatePfs(PfsXmlDTO pfsXmlDTO) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;

			if (paxFinalSalesDAO.hasPFS(pfsXmlDTO.getFlightNumber(), pfsXmlDTO.getFromAirport(), pfsXmlDTO.getDepartureDate())) {
				throw new ModuleException("airreservations.arg.pfsEntry.duplicate");
			}
			Pfs pfs = new Pfs();
			pfs.setDateDownloaded(new Date());
			pfs.setDepartureDate(pfsXmlDTO.getDepartureDate());
			pfs.setFlightNumber(pfsXmlDTO.getFlightNumber());
			pfs.setFromAddress("DCS_WS");
			pfs.setFromAirport(pfsXmlDTO.getFromAirport());
			pfs.setProcessedStatus(PfsStatus.PARSED);

			// save pfs
			paxFinalSalesDAO.savePfsEntry(pfs);

			// pax entry list
			for (PfsEntryDTO entryDTO : pfsXmlDTO.getEntryDTOs()) {

				PfsPaxEntry entry = PFSUtil.getPFSEntry(pfsXmlDTO, pfs, entryDTO);

				paxFinalSalesDAO.savePfsParseEntry(entry);

				// update infant info
				if (entryDTO.getInfant() != null) {
					PfsEntryDTO infantDTO = entryDTO.getInfant();
					PfsPaxEntry infant = new PfsPaxEntry();
					infant.setTitle(infantDTO.getTitle());
					infant.setFirstName(infantDTO.getFirstName());
					infant.setLastName(infantDTO.getLastName());
					infant.setIsParent("N");
					infant.setParentPpId(entry.getPpId());
					paxFinalSalesDAO.savePfsParseEntry(entry);
				}
			}
			return pfs.getPpId();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updatePfs", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updatePfs ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void changeReservationCabinClass(FlightSegement flightSeg, Collection<String> cabinClassesToBeDeleted,
			Map<String, Integer> availableCabinClasses) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			ReservationBO.changeReservationCabinClass(flightSeg, cabinClassesToBeDeleted, availableCabinClasses, credentialsDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::changeReservationCabinClass ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public void sendBookingChangesEmailToAgent(Collection<String> pnrCollection, String agentCode, String modificationType,
			Map<String, Map<String, Date>> onHoldTimeMap) throws ModuleException {
		if (AppSysParamsUtil.isSendAgentEmailOnReservationModified()) {
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			ReservationModifyEmailNotifierBL notiferBL = new ReservationModifyEmailNotifierBL();

			notiferBL.sendNotificationsToAgent(credentialsDTO, pnrCollection, agentCode, modificationType, onHoldTimeMap);
		}
	}

	@Override
	public ReservationBalanceTO getRequoteBalanceSummary(RequoteBalanceQueryRQ balanceQueryTO, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo) throws ModuleException {
		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.REQUOTE_BALANCE_CALCULATOR);
		command.setParameter(CommandParamNames.BALANCE_QUERY_DTO, balanceQueryTO);
		command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfo);
		command.setParameter(CommandParamNames.IS_VOID_OPERATION, Boolean.FALSE);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		ServiceResponce serviceResponce = command.execute();
		return (ReservationBalanceTO) serviceResponce.getResponseParam(CommandParamNames.BALANCE_SUMMARY_DTO);
	}

	/**
	 * Modify Segments using requote
	 * 
	 * @param pnr
	 * @param oldSegmentIds
	 * @param iSegment
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @param modifyCharge
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce requoteModifySegments(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		String comments = "";
		String pnr = requoteModifyRQ.getPnr();

		// Retrieve called credential information
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		// All the payment information
		Collection<PaymentInfo> colPaymentInfo = ReservationBO.getPaymentInfo(requoteModifyRQ.getAllPaymentAssemblers(), pnr);

		// Load the reservation contact information
		ReservationContactInfo contactInfo = this.getReservationContactInfo(pnr);

		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
				requoteModifyRQ.getTemporaryTnxMap(), contactInfo, colPaymentInfo, credentialsDTO, true, false);

		try {

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.REQUOTE_SEGMENT_MACRO);
			command.setParameter(CommandParamNames.REQUOTE_MODIFY_RQ, requoteModifyRQ);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.TEMPORY_PAYMENT_IDS, colTnxIds);
			command.setParameter(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
			command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));
			command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
			command.setParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, requoteModifyRQ.getReasonForAllowBlPax());
			ServiceResponce serviceResponce = command.execute();
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, comments, null, null, null);
			ReservationModuleUtils.getReservationBD().sendSms(pnr, contactInfo, credentialsDTO);

			if (requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo() != null) {
				LoyaltyPaymentInfo loyaltyPaymentInfo = requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo();
				redeemLoyaltyRewards(loyaltyPaymentInfo.getLoyaltyRewardIds(), trackInfoDTO.getAppIndicator(),
						loyaltyPaymentInfo.getMemberAccountId());
			}

			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::changeSegments " + ReservationApiUtils.getElementsInCollection(colTnxIds), ex);
			comments = "ModuleException::changeSegments";

			if (requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo() != null) {
				AirproxyModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(pnr,
						requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo().getLoyaltyRewardIds(),
						requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo().getMemberAccountId());
			}

			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, ex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(
					" ((o)) CommonsDataAccessException::changeSegments " + ReservationApiUtils.getElementsInCollection(colTnxIds),
					cdaex);
			comments = "CommonsDataAccessException::changeSegments";

			if (requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo() != null) {
				AirproxyModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(pnr,
						requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo().getLoyaltyRewardIds(),
						requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo().getMemberAccountId());
			}

			// Update the tempory payment entries
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, comments, null, null, null);
			// Exception occured so performing a reverse payment and removing
			// the tempory entry
			this.reversePayment(colPaymentInfo, trackInfoDTO, cdaex.getExceptionCode(), colTnxIds);
			comments = ":undo:";
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, comments, null, null, null);

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void auditPaxAutoRefund(String pnr, String paxInfo, BigDecimal amount, String status, String txnDetails,
			CredentialsDTO credentialsDTO, int auditOption, ReservationAdminInfo adminInfo) throws ModuleException {
		try {

			ReservationAudit reservationAudit = new ReservationAudit();

			if (auditOption == 1) {
				reservationAudit.setModificationType(AuditTemplateEnum.PAX_AUTO_REFUND.getCode());
			} else if (auditOption == 2) {
				reservationAudit.setModificationType(AuditTemplateEnum.PAX_AGENT_AUTO_REFUND.getCode());
			}

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.PAX_DETAILS, paxInfo);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.AMOUNT, amount.toString());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.TXN_DETAILS, txnDetails);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.TXN_STATUS, status);

			if (auditOption == 1) {
				// Setting the ip address
				if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.IP_ADDDRESS,
							credentialsDTO.getTrackInfoDTO().getIpAddress());
				}

				// Setting the origin carrier code
				if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.ORIGIN_CARRIER,
							credentialsDTO.getTrackInfoDTO().getCarrierCode());
				}
			} else if (auditOption == 2) {
				// Setting the ip address
				if (adminInfo.getOriginIPAddress() != null) {
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.IP_ADDDRESS,
							adminInfo.getOriginIPAddress());
				}

				// Setting the origin carrier code
				if (adminInfo.getOriginCarrierCode() != null) {
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.ORIGIN_CARRIER,
							adminInfo.getOriginCarrierCode());
				}
			}

			// Record the modification
			ReservationBO.recordModification(pnr, reservationAudit, null, credentialsDTO);
		} catch (ModuleException ex) {
			this.sessionContext.setRollbackOnly();
			log.error(" ((o)) ModuleException::auditIBEAutoRefund ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error(" ((o)) CommonsDataAccessException::auditIBEAutoRefund ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce passengerRefundWithRequiresNew(String pnr, IPassenger iPassenger, String userNotes,
			boolean isCapturePayment, long version, TrackInfoDTO trackInfoDTO, boolean removeAgentCommission)
			throws ModuleException {
		return passengerRefund(pnr, iPassenger, userNotes, isCapturePayment, version, trackInfoDTO, true, null, null,
				removeAgentCommission);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce passengerAutoRefund(String pnr, boolean refundOnlyNSPax, TrackInfoDTO trackInfoDTO,
			boolean isVoidOperation, String ownerChannelId) throws ModuleException {

		try {

			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.AUTOMATIC_CREDIT_REFUND);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.IS_REFUND_ONLY_NOSHOWPAX, refundOnlyNSPax);
			command.setParameter(CommandParamNames.IS_VOID_OPERATION, isVoidOperation);
			command.setParameter(CommandParamNames.OWNER_CHANNEL_ID, ownerChannelId);
			return command.execute();

		} catch (ModuleException me) {
			log.error(" ((o)) ModuleException::passengerAutoRefund ", me);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::passengerAutoRefund ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updatePassengerCoupon(PassengerCouponUpdateRQ paxCouponUpdateRQ, TrackInfoDTO trackInfo, boolean allowExceptions)
			throws ModuleException {
		try {
			// Retrieve the called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);

			ETicketBO.updatePassengerCouponStatus(paxCouponUpdateRQ, credentialsDTO, allowExceptions);

		} catch (ModuleException me) {
			log.error(" ((o)) ModuleException::updatePassengerCoupon ", me);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::updatePassengerCoupon ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateGroupPassengerCoupon(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ, TrackInfoDTO trackInfo,
			boolean allowExceptions) throws ModuleException {
		try {

			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);
			ETicketBO.updateGroupPassengerCouponStatus(groupPaxCouponUpdateRQ, credentialsDTO, allowExceptions);

		} catch (ModuleException me) {
			log.error(" ((o)) ModuleException::updateGroupPassengerCoupon ", me);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::updateGroupPassengerCoupon ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updatePassengerCoupons(Integer flightId, String airport) throws ModuleException {
		List<PaxEticketTO> eticketIds = ReservationModuleUtils.getReservationAuxilliaryBD().getPaxEtickets(flightId, airport);
		for (PaxEticketTO paxEticket : eticketIds) {
			try {
				updatePassengerCouponToFlown(paxEticket, airport);
			} catch (ModuleException me) {
				log.error(" ((o)) ModuleException::failed update ", me);
				// TODO raise support error
			}
		}

	}

	private void updatePassengerCouponToFlown(PaxEticketTO paxEticket, String airport) throws ModuleException {
		PassengerCouponUpdateRQ paxCouponRQ = new PassengerCouponUpdateRQ();
		paxCouponRQ.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		paxCouponRQ.setEticketId(paxEticket.getEticketId());
		if (AppSysParamsUtil.getPaxStatusForAutoFlown().contains("PFS")) {
			paxCouponRQ.setPaxStatus(ReservationInternalConstants.PfsPaxStatus.FLOWN);
		}
		if (AppSysParamsUtil.getPaxStatusForAutoFlown().contains("ETICKET")) {
			paxCouponRQ.setEticketStatus(EticketStatus.FLOWN.code());
		}
		PassengerTicketCouponAuditDTO couponAuditDetail = new PassengerTicketCouponAuditDTO();
		couponAuditDetail.setPassengerName(paxEticket.getName());
		couponAuditDetail.setPnr(paxEticket.getPnr());
		couponAuditDetail.setPnrPaxId(paxEticket.getPnrPaxId() + "");
		couponAuditDetail.setRemark("System flown update for airport " + airport);
		paxCouponRQ.setCouponAudit(couponAuditDetail);

		ReservationModuleUtils.getPassengerBD().updatePassengerCoupon(paxCouponRQ, null, false);
	}

	@Override
	public boolean isAllowVoidReservation(Reservation reservation) throws ModuleException {
		return CancellationUtils.isVoidReservation(reservation);
	}

	public Integer getAppliedFareDiscountPercentage(String pnr) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO.getAppliedFareDiscountPercentage(pnr);
	}

	public Collection<Integer> getFlightSegmentIds(String pnr, boolean isCNFOnly) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getFlightSegmentIds(pnr, isCNFOnly);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightSegmentIds ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AutoCancellationInfo getAutoCancellationInfo(String pnr, Reservation reservation, Date firstDepartureTime,
			Collection<Integer> modifiedSegIds, String cancellationType, boolean hasBufferTimePrivilege,
			boolean getExistingCnxTimeOnly) throws ModuleException {
		return AutoCancellationUtil.getAutoCancellationInfo(pnr, reservation, firstDepartureTime, modifiedSegIds,
				cancellationType, hasBufferTimePrivilege, getExistingCnxTimeOnly);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public AutoCancellationInfo saveAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo, String pnr,
			TrackInfoDTO trackInfo) throws ModuleException {
		try {

			// Retrieve the called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);

			AutoCancellationBO.saveAutoCancellationInfo(autoCancellationInfo, pnr, credentialsDTO);
			return autoCancellationInfo;

		} catch (ModuleException me) {
			log.error(" ((o)) ModuleException::saveAutoCancellationInfo ", me);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::saveAutoCancellationInfo ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void executeAutoCancellation(Date currentDate, BigDecimal customAdultCancelCharge, BigDecimal customChildCancelCharge,
			BigDecimal customInfantCancelCharge) throws ModuleException {
		log.info(" +=============================================+ ");
		log.info("  Started Auto Cancellation on " + currentDate);
		log.info(" +=============================================+ ");
		executeAutoCancellationOwn(currentDate, customAdultCancelCharge, customChildCancelCharge, customInfantCancelCharge);
		executeAutoCancellationInterline();
		log.info(" +=============================================+ ");
		log.info("  Completed Auto Cancellation on " + currentDate);
		log.info(" +=============================================+ ");
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void executeAutoCancellationOwn(Date currentDate, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge) throws ModuleException {
		try {
			ReservationBO.executeAutoCancellation(currentDate, customAdultCancelCharge, customChildCancelCharge,
					customInfantCancelCharge);
		} catch (ModuleException me) {
			log.error(" ((o)) ModuleException::executeAutoCancellation ", me);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::executeAutoCancellation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void executeAutoCancellationInterline() throws ModuleException {
		// If LCC connectivity enabled, handle dry/interline cancellation via LCC
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			ReservationModuleUtils.getLCCReservationBD().executeAutoCancellation();
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce removeAutoCancellation(String pnr, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {

			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.REMOVE_AUTO_CANCELLATION);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			return command.execute();

		} catch (ModuleException me) {
			log.error(" ((o)) ModuleException::resetAutoCancellation ", me);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::resetAutoCancellation ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Integer> getExpiredCancellationIDs(Date currentDateTime) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getExpiredAutoCancellationInfoList(currentDateTime);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::getExpiredCancellationIDs ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Integer> getAutoCancellableInfo(String marketingCarrier, Collection<Integer> autoCnxIds)
			throws ModuleException {
		try {
			return AutoCancellationBO.getMarketingCarrierExpiredInfo(marketingCarrier, autoCnxIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::getAutoCancellablePNRs ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateAutoCancelSchedulerStatus(List<Integer> autoCnxIds, String schedulerStatus) throws ModuleException {
		try {
			ReservationDAOUtils.DAOInstance.RESERVATION_DAO.updateAutoCancelSchedulerStatus(autoCnxIds, schedulerStatus);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::updateAutoCancelSchedulerStatus ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce createAlert(String pnr, Collection<Integer> pnrSegIds, String description,
			LCCClientReservation lccReservation, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			if (lccReservation != null) {
				Set<LCCClientReservationSegment> segments = lccReservation.getSegments();
				List<Integer> pnrSegIdList = new ArrayList<Integer>();
				List<AlertDetailDTO> alertList = new ArrayList<AlertDetailDTO>();
				Map<String, List<LCCClientReservationSegment>> alertMap = new HashMap<String, List<LCCClientReservationSegment>>();

				for (LCCClientReservationSegment resSegment : segments) {
					if (!alertMap.containsKey(resSegment.getCarrierCode())) {
						alertMap.put(resSegment.getCarrierCode(), new ArrayList<LCCClientReservationSegment>());
					}
					alertMap.get(resSegment.getCarrierCode()).add(resSegment);
				}

				for (Entry<String, List<LCCClientReservationSegment>> entry : alertMap.entrySet()) {
					if (AppSysParamsUtil.getDefaultCarrierCode().equals(entry.getKey())) {
						for (LCCClientReservationSegment segment : entry.getValue()) {
							if (pnrSegIds.contains(segment.getPnrSegID())) {
								pnrSegIdList.add(segment.getPnrSegID());
							}
						}
						ReservationBO.createAlert(pnr, pnrSegIdList, description, new Long(2), credentialsDTO);
					} else {
						for (LCCClientReservationSegment segment : entry.getValue()) {
							if (pnrSegIds.contains(segment.getPnrSegID())) {
								AlertDetailDTO alertDTO = new AlertDetailDTO();
								alertDTO.setAlertTypeId(new Long(2));
								alertDTO.setContent(description);
								alertDTO.setPnr(pnr);
								alertDTO.setPriorityCode(new Long(1));
								alertDTO.setDepDate(segment.getDepartureDateZulu());
								alertDTO.setTimeStamp(CalendarUtil.getCurrentZuluDateTime());
								alertDTO.setCarrierCode(entry.getKey());
								alertDTO.setPnrSegId(segment.getPnrSegID());
								alertList.add(alertDTO);
							}
						}
						AirproxyModuleUtils.getLCCAlertingBD().addAlerts(alertList, trackInfoDTO, entry.getKey());
					}
				}
			} else {
				ReservationBO.createAlert(pnr, pnrSegIds, description, new Long(2), credentialsDTO);
			}
			return new DefaultServiceResponse(false);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createAlert", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createAlert ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAuditPromotionRequestSubcriptions(PromotionType promotionType, String pnr, String status,
			Map<String, Object> contentMapForAudit, Map<Integer, List<String>> segCodesDirectionWise) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			PromotionBL.saveAuditPromotionRequestSubcriptions(promotionType, pnr, status, contentMapForAudit, credentialsDTO,
					segCodesDirectionWise);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::saveAuditFlexiDatePromotionRequest", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveAuditFlexiDatePromotionRequest ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Map<String, BigDecimal> getExistingModificationChargesDue(List<Integer> pnrSegIds) throws ModuleException {
		try {
			return AutoCancellationBO.getExistingModificationChargeDue(pnrSegIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::getExistingModificationChargesDue ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public BigDecimal calculateBalanceToPayAfterAutoCancellation(Map<String, Collection<Integer>> groupWiseSegIds,
			List<Integer> autoCnxInfants, Map<String, CustomChargesTO> groupWiseCustomCharges, String pnr, Long version)
			throws ModuleException {
		try {
			return AutoCancellationBO.calculateBalanceToPayAfterAutoCancellation(groupWiseSegIds, autoCnxInfants,
					groupWiseCustomCharges, pnr, version);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::getBalanceToPayAfterAutoCancellation ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<OnlineCheckInReminderDTO> getFlightSegsForOnlineCheckInReminder(Date date, String schedulerType)
			throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getFlightsSegsForOnlineCheckInReminder(date, schedulerType);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<OnlineCheckInReminderDTO> getDateForOnlineCheckInReminder(Date date) throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getDateForOnlineCheckInReminder(date);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendOnlineCheckInReminder(OnlineCheckInReminderDTO onlineCheckInReminderDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			OnlineCheckInReminderSenderBL onlineCheckInReminderSenderBL = new OnlineCheckInReminderSenderBL();
			onlineCheckInReminderSenderBL.sendOnlineCheckInReminderNotification(onlineCheckInReminderDTO, credentialsDTO);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void confirmWaitListedReservations(List<WLConfirmationRequestDTO> wlRequests) throws ModuleException {
		CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
		ReservationBO.confirmWaitListedReservations(wlRequests, credentialsDTO);

	}

	public Collection<ReservationWLPriority> getWaitListedReservations(Integer flightSegId) throws ModuleException {
		try {

			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getWaitListedReservations(flightSegId);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getWaitListedReservations ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReservationWLPriority> getWaitListedPrioritiesForSegment(Integer pnrSegmentId) throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return (List<ReservationWLPriority>) reservationDAO.getWaitListedPrioritiesForSegment(pnrSegmentId);
	}

	@Override
	public void deleteAllErrorEntries(Pfs pfs) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			paxFinalSalesDAO.deleteAllErrorEntries(pfs.getPpId());

			int pfsPaxCount = paxFinalSalesDAO.getPfsParseEntriesCount(pfs.getPpId(), null);

			if (pfsPaxCount == 0) {
				// All the PAX deleted for the PFS
				// Set the PFS status to UNPARSED
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED);
				paxFinalSalesDAO.savePfsEntry(pfs);
			} else if (paxFinalSalesDAO.getPfsParseEntriesCount(pfs.getPpId(),
					ReservationInternalConstants.PfsProcessStatus.PROCESSED) == pfsPaxCount) {
				// All the PAX are processed
				// Set the PFS status to Reconciled
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.RECONCILE_SUCCESS);
				paxFinalSalesDAO.savePfsEntry(pfs);
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::deletePfsParseEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce removePassengersWithRequiresNew(String pnr, String newPnr, Collection<Integer> pnrPaxIds,
			CustomChargesTO customChargesTO, long version, TrackInfoDTO trackInfoDTO, String userNotes,
			Map<Integer, List<ExternalChgDTO>> paxExternalCharges) throws ModuleException {
		return removePassengers(pnr, newPnr, pnrPaxIds, customChargesTO, version, trackInfoDTO, userNotes, paxExternalCharges);
	}

	public boolean isNOSHOPaxEntry(String eTicketNo, String paxStatus, String segmentCode) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.isNOSHOPaxEntry(eTicketNo.trim(), paxStatus, segmentCode);
		} catch (Exception e) {
			log.error(" ((o)) CommonsDataAccessException::getPFS ", e);
			throw new ModuleException(e.getMessage());
		}
	}

	@Override
	public boolean isFlownOrNoShowPaxEntry(String eTicketNo, String segmentCode) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO.isFlownOrNoShowPaxEntry(eTicketNo.trim(), segmentCode);
		} catch (Exception e) {
			log.error(" ((o)) CommonsDataAccessException::accesPaxStatus ", e);
			throw new ModuleException(e.getMessage());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifyWaitListedPriority(String pnr, Integer priority, Integer fltSegId, long version, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);

			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			// Check reservation version compatibility
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);

			// Check restricted segment constraint
			ValidationUtils.checkRestrictedSegmentConstraints(reservation, null,
					"airreservations.cancellation.restrictedSegPayShouldExist");

			ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;

			Collection<ReservationWLPriority> wlReservations = reservationDAO.getWaitListedReservations(fltSegId);
			Collection<ReservationWLPriority> updatedWLlReservations = new ArrayList<ReservationWLPriority>();

			ReservationWLPriority updatedWLReservation = null;
			ReservationWLPriority replacedWLReservation = null;
			Integer oldPriority = null;
			for (ReservationWLPriority wlPriority : wlReservations) {
				if (wlPriority.getPnr().equals(pnr)
						&& wlPriority.getStatus().equals(ReservationInternalConstants.ReservationType.WL.toString())) {
					updatedWLReservation = wlPriority;
					oldPriority = wlPriority.getPriority();
				}
				if (wlPriority.getPriority().intValue() == priority.intValue()) {
					replacedWLReservation = wlPriority;
				}

			}
			if (replacedWLReservation != null) {
				replacedWLReservation.setPriority(updatedWLReservation.getPriority());
				updatedWLlReservations.add(replacedWLReservation);
			}
			updatedWLReservation.setPriority(priority);
			updatedWLlReservations.add(updatedWLReservation);

			reservationDAO.saveWLReservationPriority(updatedWLlReservations);
			List<Integer> pnrSegmentIds = new ArrayList<Integer>();
			for (ReservationSegment resSeg : reservation.getSegments()) {
				if (resSeg.getFlightSegId().intValue() == fltSegId.intValue()) {
					pnrSegmentIds.add(resSeg.getPnrSegId());
					break;
				}
			}

			// Composing the audit
			ReservationAudit reservationAudit = new ReservationAudit();
			reservationAudit.setModificationType(AuditTemplateEnum.CHANGED_WL_PRIORITY.getCode());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedWLPriority.NEW_PRIORITY, priority.toString());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedWLPriority.OLD_PRIORITY,
					oldPriority.toString());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedWLPriority.SEGMENT_INFORMATION,
					ReservationCoreUtils.getSegmentInformation(pnrSegmentIds));
			// Setting the ip address
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedWLPriority.IP_ADDDRESS,
						credentialsDTO.getTrackInfoDTO().getIpAddress()); // AARESAA-811
			}

			// Setting the origin carrier code
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedWLPriority.ORIGIN_CARRIER,
						credentialsDTO.getTrackInfoDTO().getCarrierCode());
			}

			// Record the modification
			ReservationBO.recordModification(pnr, reservationAudit, null, credentialsDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::modifyWaitListedPriority ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, Collection<Integer>> getPaxwiseFlownSegments(Reservation reservation) {
		if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.DATE) {
			Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservation.getSegmentsView().iterator();
			ReservationSegmentDTO reservationSegmentDTO;
			Date currentDate = new Date();
			Collection<Integer> colFlownPnrSegmentIds = new HashSet<Integer>();
			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = itReservationSegmentDTO.next();
				if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					if (currentDate.compareTo(reservationSegmentDTO.getZuluDepartureDate()) > 0) {
						colFlownPnrSegmentIds.add(reservationSegmentDTO.getPnrSegId());
					}
				}
			}
			Map<Integer, Collection<Integer>> map = new HashMap<Integer, Collection<Integer>>();
			for (ReservationPax pax : reservation.getPassengers()) {
				map.put(pax.getPnrPaxId(), colFlownPnrSegmentIds);
			}
			return map;

		} else {
			List<Integer> cnfPnrSegs = new ArrayList<Integer>();

			for (ReservationSegment resSeg : reservation.getSegments()) {
				if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					cnfPnrSegs.add(resSeg.getPnrSegId());
				}
			}
			boolean skipOnHold = false;
			if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET
					&& reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				skipOnHold = true;
			}
			if (!skipOnHold && cnfPnrSegs.size() > 0) {
				Map<Integer, Collection<Integer>> paxFlownPnrSegMap = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
						.getPaxWiseFlownSegments(cnfPnrSegs);
				if (AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
					Map<Integer, Collection<Integer>> paxFlownPnrSegMapET = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
							.getPaxWiseBoardedCheckedInSegments(cnfPnrSegs);
					for (Integer colFlownSeg : paxFlownPnrSegMap.keySet()) {
						if (paxFlownPnrSegMap.get(colFlownSeg).isEmpty() && paxFlownPnrSegMapET.get(colFlownSeg) != null
								&& paxFlownPnrSegMap.get(colFlownSeg) != paxFlownPnrSegMapET.get(colFlownSeg)) {

							paxFlownPnrSegMap.get(colFlownSeg).addAll(paxFlownPnrSegMapET.get(colFlownSeg));
						}
					}
				}
				if (!paxFlownPnrSegMap.isEmpty()) {

					return paxFlownPnrSegMap;
				}
			}
			Map<Integer, Collection<Integer>> map = new HashMap<Integer, Collection<Integer>>();
			for (ReservationPax pax : reservation.getPassengers()) {
				map.put(pax.getPnrPaxId(), new ArrayList<Integer>());
			}
			return map;
		}
	}

	public Collection<ReservationPaxDTO> searchReservations(ReservationSearchDTO searchDTO) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.searchReservations(searchDTO);
	}

	/**
	 * NOSHOW Auto Refund Prcoess
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce processNSAutoRefundPNRs() throws ModuleException {
		CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
		boolean nsAutoRefundEnabled = AppSysParamsUtil.isNSAutoRefundHoldProcessEnabled();
		ServiceResponce response;
		NSRefundReservation nsRefundReservation;
		if (nsAutoRefundEnabled) {
			Map<String, NSRefundReservation> nsRefundHoldPnrs = ReservationDAOUtils.DAOInstance.NSREFUND_RESERVATION_DAO
					.getNSRefundHoldPNRs();
			if (nsRefundHoldPnrs.size() > 0) {
				for (Map.Entry<String, NSRefundReservation> entry : nsRefundHoldPnrs.entrySet()) {
					nsRefundReservation = entry.getValue();
					try {
						response = passengerAutoRefund(entry.getKey(), true, credentialsDTO.getTrackInfoDTO(), false, null);
						boolean isSuccessfullRefund = Boolean
								.valueOf(String.valueOf(response.getResponseParam(PaymentConstants.PARAM_SUCCESSFULL_REFUND)));
						if (isSuccessfullRefund) {
							nsRefundReservation
									.setRefundProcessedStatus(ReservationInternalConstants.NSRefundProcessStatus.REFUND_SUCCESS);
							nsRefundReservation.setRefundProcessedTime(new Date());
						} else {
							nsRefundReservation
									.setRefundProcessedStatus(ReservationInternalConstants.NSRefundProcessStatus.PROCESSED);
							nsRefundReservation.setRefundProcessedTime(new Date());
						}
					} catch (Exception ex) {
						log.error("NS Refund Process failed for PNR : " + entry.getKey(), ex);
						nsRefundReservation
								.setRefundProcessedStatus(ReservationInternalConstants.NSRefundProcessStatus.ERROR_OCCURED);
						nsRefundReservation.setRefundProcessedTime(new Date());
					} finally {
						ReservationDAOUtils.DAOInstance.NSREFUND_RESERVATION_DAO.saveNSRefundReservation(nsRefundReservation);
						log.info("NS Refund Reservation recode updated for PNR : " + entry.getKey());
					}
				}
			} else {
				log.debug("No reservation found for NOSHOW Refund process");
			}
		} else {
			log.debug("NOSHOW Refund process Disabled");
		}

		return new DefaultServiceResponse(true);
	}

	public boolean isReservationAvailable(long promoCriteriaID) {
		Reservation selReservation = ReservationProxy.getReservation(promoCriteriaID);
		if (selReservation == null) {
			return false;
		} else {
			return true;
		}
		// TODO Auto-generated method stub

	}

	public List<ReservationBasicsDTO> getFlightSegmentPAXDetails(Integer flightSegId, String logicalCabinClass)
			throws ModuleException {
		try {

			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getFlightSegmentPAXDetails(flightSegId, logicalCabinClass);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getFlightSegmentPAXDetails ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public List<ReservationPaxDetailsDTO> getPAXDetails(Integer flightId, int page) throws ModuleException {
		try {

			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getPAXDetails(flightId, page);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPAXDetails ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Integer getPAXDetailsCount(Integer flightId) throws ModuleException {
		try {

			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getPAXDetailsCount(flightId);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPAXDetailsCount ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public List<FlightConnectionDTO> getConnectionDetials(String pnr) throws ModuleException {
		try {

			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getConnectionDetials(pnr);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPAXConnectionDetails ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Page<GroupBookingRequestTO> getGroupBookingRequestDetails(GroupBookingRequestTO grpBookingReq, String userCode,
			Integer start, boolean mainReqestOnly) throws ModuleException {

		Collection<GroupBookingRequestTO> transformedDTOs = new HashSet<GroupBookingRequestTO>();
		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();
		GroupBookingRequest getReq = grpBookingReqBL.setSearchGroupBookingReqObject(grpBookingReq);
		if (grpBookingReq.getGroupBookingRoutes() != null) {
			Set<GroupBookingReqRoutes> grpBookingRoutes = grpBookingReqBL
					.setGroupBookingReqRoutesObject(grpBookingReq.getGroupBookingRoutes());
			getReq.setGroupBookingRoutes(grpBookingRoutes);
		}
		Page<GroupBookingRequest> resultPage = ReservationDAOUtils.DAOInstance.GrpBookingDAO.get(getReq, start, 20, userCode,
				mainReqestOnly);
		for (GroupBookingRequest groupRequest : resultPage.getPageData()) {
			transformedDTOs.add(grpBookingReqBL.setGroupBookingReqToObject(groupRequest));
		}
		Page<GroupBookingRequestTO> resultTOPage = new Page<GroupBookingRequestTO>(resultPage.getTotalNoOfRecords(),
				resultPage.getStartPosition(), resultPage.getEndPosition(), transformedDTOs);
		return resultTOPage;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce addGroupBookingRequestDetails(GroupBookingRequestTO grpBookingReq) throws ModuleException {

		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();
		GroupBookingRequest saveReq = grpBookingReqBL.setGroupBookingReqObject(grpBookingReq);
		Set<GroupBookingReqRoutes> grpBookingRoutes = grpBookingReqBL
				.setGroupBookingReqRoutesObject(grpBookingReq.getGroupBookingRoutes());
		saveReq.setGroupBookingRoutes(grpBookingRoutes);

		AuditAirMaster.doAudit(((GroupBookingRequest) saveReq), getUserPrincipal());
		saveReq = ReservationDAOUtils.DAOInstance.GrpBookingDAO.save(saveReq);

		ReservationDAOUtils.DAOInstance.GrpBookingDAO
				.saveGroupBookingHistory(grpBookingReqBL.setGroupBookingHistory(saveReq, grpBookingReq.getUserId(), 0));
		grpBookingReq = grpBookingReqBL.setGroupBookingReqToObject(saveReq);
		DefaultServiceResponse res = new DefaultServiceResponse(true);
		res.addResponceParam(CommandParamNames.RESPONSE_KEY, grpBookingReq);
		return res;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce updateGroupBookingRequest(GroupBookingRequestTO grpBookingReq) throws ModuleException {

		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();
		GroupBookingRequest saveReq = grpBookingReqBL.setGroupBookingReqObject(grpBookingReq);
		Set<GroupBookingReqRoutes> grpBookingRoutes = grpBookingReqBL
				.setGroupBookingReqRoutesObject(grpBookingReq.getGroupBookingRoutes());
		saveReq.setGroupBookingRoutes(grpBookingRoutes);
		ReservationDAOUtils.DAOInstance.GrpBookingDAO.update(saveReq);
		ReservationDAOUtils.DAOInstance.GrpBookingDAO
				.saveGroupBookingHistory(grpBookingReqBL.setGroupBookingHistory(saveReq, saveReq.getCraeteUserCode(), 0));
		AuditAirMaster.doAudit(((GroupBookingRequest) saveReq), getUserPrincipal());

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.RESPONSE_KEY, grpBookingReqBL.setGroupBookingReqToObject(saveReq));
		return response;
	}

	@Override
	public ServiceResponce updateGroupBookingAdminStatus(GroupBookingRequestTO grpBookingReq, Collection<Long> requestIDs)
			throws ModuleException {

		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();
		GroupBookingRequest saveReq = grpBookingReqBL.setGroupBookingReqObject(grpBookingReq);

		for (Long reqID : requestIDs) {
			saveReq.setRequestID(reqID);
			ReservationDAOUtils.DAOInstance.GrpBookingDAO.updateAdminStatus(saveReq);
			ReservationDAOUtils.DAOInstance.GrpBookingDAO.saveGroupBookingHistory(
					grpBookingReqBL.setGroupBookingHistory(saveReq, saveReq.getCraeteUserCode(), saveReq.getStatusID()));
			AuditAirMaster.doAudit(((GroupBookingRequest) saveReq), getUserPrincipal());
		}
		return new DefaultServiceResponse(true);
	}

	@Override
	public ServiceResponce updateGroupBookingAgentStatus(GroupBookingRequestTO grpBookingReq) throws ModuleException {

		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();
		GroupBookingRequest saveReq = grpBookingReqBL.setGroupBookingReqObject(grpBookingReq);
		ReservationDAOUtils.DAOInstance.GrpBookingDAO.updateAgentsStatus(saveReq);
		ReservationDAOUtils.DAOInstance.GrpBookingDAO.saveGroupBookingHistory(
				grpBookingReqBL.setGroupBookingHistory(saveReq, saveReq.getCraeteUserCode(), saveReq.getStatusID()));
		AuditAirMaster.doAudit(((GroupBookingRequest) saveReq), getUserPrincipal());
		return new DefaultServiceResponse(true);
	}

	@Override
	public ServiceResponce updateApproveStatus(GroupBookingRequestTO grpBookingReq, Map<Long, BigDecimal> editedData)
			throws ModuleException {

		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();
		GroupBookingRequest saveReq = grpBookingReqBL.setGroupBookingReqObject(grpBookingReq);

		for (Entry<Long, BigDecimal> entry : editedData.entrySet()) {
			saveReq.setRequestID(entry.getKey());
			saveReq.setAgreedFare(entry.getValue());
			ReservationDAOUtils.DAOInstance.GrpBookingDAO.updateApproveStatus(saveReq);
			ReservationDAOUtils.DAOInstance.GrpBookingDAO.saveGroupBookingHistory(
					grpBookingReqBL.setGroupBookingHistory(saveReq, saveReq.getCraeteUserCode(), saveReq.getStatusID()));
			AuditAirMaster.doAudit(((GroupBookingRequest) saveReq), getUserPrincipal());
		}
		return new DefaultServiceResponse(true);
	}

	@Override
	public ServiceResponce executeRollforward(long requestID, Set<GroupBookingRequestTO> rollForwardedRequests)
			throws ModuleException {

		GroupBookingRequest existingRequest = ReservationDAOUtils.DAOInstance.GrpBookingDAO.find(requestID);

		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();

		existingRequest.getSubRequests().clear();

		List<GroupBookingRequestTO> sortedList = new ArrayList<GroupBookingRequestTO>(rollForwardedRequests);
		Collections.sort(sortedList, new Comparator<GroupBookingRequestTO>() {

			@Override
			public int compare(GroupBookingRequestTO o1, GroupBookingRequestTO o2) {
				return o1.getGroupBookingRoutes().iterator().next().getDepartureDate()
						.compareTo(o2.getGroupBookingRoutes().iterator().next().getDepartureDate());
			}

		});

		for (GroupBookingRequestTO rollForwardReq : sortedList) {
			GroupBookingRequest saveReq = grpBookingReqBL.createNewGroupBookingReqObject(existingRequest);

			saveReq.setAdultAmount(rollForwardReq.getAdultAmount());
			saveReq.setChildAmount(rollForwardReq.getChildAmount());
			saveReq.setInfantAmount(rollForwardReq.getInfantAmount());
			saveReq.setOalFare(rollForwardReq.getOalFare());
			saveReq.setAgentComments(rollForwardReq.getAgentComments());

			Set<GroupBookingReqRoutes> reqRoutes = new HashSet<GroupBookingReqRoutes>();
			for (GroupBookingReqRoutes route : existingRequest.getGroupBookingRoutes()) {

				GroupBookingReqRoutes newRoute = new GroupBookingReqRoutes();
				GroupBookingReqRoutesTO routeTO = grpBookingReqBL.findMatchingRoute(rollForwardReq.getGroupBookingRoutes(),
						route);

				newRoute.setArrival(routeTO.getArrival());
				newRoute.setReturningDate(routeTO.getReturningDate());
				newRoute.setDeparture(routeTO.getDeparture());
				newRoute.setDepartureDate(routeTO.getDepartureDate());
				newRoute.setFlightNumber(routeTO.getFlightNumber());
				newRoute.setVia(routeTO.getVia());

				reqRoutes.add(newRoute);
			}

			saveReq.setGroupBookingRoutes(reqRoutes);
			saveReq = ReservationDAOUtils.DAOInstance.GrpBookingDAO.save(saveReq);
			existingRequest.getSubRequests().add(saveReq);

			ReservationDAOUtils.DAOInstance.GrpBookingDAO
					.saveGroupBookingHistory(grpBookingReqBL.setGroupBookingHistory(saveReq, saveReq.getCraeteUserCode(), 0));
			AuditAirMaster.doAudit(((GroupBookingRequest) saveReq), getUserPrincipal());
		}

		ReservationDAOUtils.DAOInstance.GrpBookingDAO.update(existingRequest);

		return new DefaultServiceResponse(true);
	}

	@Override
	public List<PNRGOVResInfoDTO> getResInfoListForFlightSegment(Integer flightSegmentID) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getResInfoListForFlightSegment(flightSegmentID);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void publishPNRGOV(int flightSegmentID, String contryCode, String airportCode, int timePeriod, String inboundOutbound)
			throws ModuleException {
		PNRGOVPublishBL pnrgov = new PNRGOVPublishBL();
		pnrgov.publishPassengerDataForFlight(flightSegmentID, contryCode, airportCode, timePeriod, inboundOutbound,
				this.getUserPrincipal());

	}

	public List<PNRGOVSegmentDTO> getSegmentsForPNR(String pnr, String carrierCode) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getReservationSegmentsForPNR(pnr, carrierCode);
	}

	public List<PNRGOVPassengerDTO> getPassengersForPNR(String pnr) {
		return ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getPassengersForPNR(pnr);
	}

	@Override
	public ServiceResponce updateBookingStatus(long requestID, BigDecimal payment, String passengerList,
			ArrayList<String> outFlightRPHList, ArrayList<String> retFlightRPHList) throws ModuleException {

		ServiceResponce returnResponce = null;
		GroupBookingRequest grpRequest = ReservationDAOUtils.DAOInstance.GrpBookingDAO.getValidGroupBookingRequest(requestID,
				payment);
		if (isValidGrpBookingRequestRoute(grpRequest, passengerList, outFlightRPHList, retFlightRPHList)) {
			returnResponce = ReservationDAOUtils.DAOInstance.GrpBookingDAO.updateBookingStatus(requestID, payment);
		}
		return returnResponce;
	}

	@Override
	public GroupBookingRequestTO validateGroupBookingRequest(long requestID, BigDecimal totalAmount, String passengerList,
			ArrayList<String> outFlightRPHList, ArrayList<String> retFlightRPHList, String agentCode) throws ModuleException {
		GroupBookingRequestTO returnRequest = null;
		GroupBookingRequest grpRequest = ReservationDAOUtils.DAOInstance.GrpBookingDAO.getValidGroupBookingRequest(requestID,
				totalAmount);

		if (grpRequest != null && grpRequest.getAgreedFare() != null && grpRequest.getAgreedFare().equals(totalAmount)
				&& grpRequest.getRequestedAgentCode().equals(agentCode)) {
			if (isValidGrpBookingRequestRoute(grpRequest, passengerList, outFlightRPHList, retFlightRPHList)) {
				returnRequest = new GroupBookingRequestBL().setGroupBookingReqToObject(grpRequest);
			}
		}

		return returnRequest;
	}

	private boolean isValidGrpBookingRequestRoute(GroupBookingRequest grpRequest, String passengerList,
			ArrayList<String> outFlightRPHList, ArrayList<String> retFlightRPHList) {
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyyMMdd");
		String grpPsgList = grpRequest.getAdultAmount() + "|" + grpRequest.getChildAmount() + "|" + grpRequest.getInfantAmount();

		if (!grpPsgList.equals(passengerList)) {
			return false;
		} else {
			for (GroupBookingReqRoutes reqRoute : grpRequest.getGroupBookingRoutes()) {

				String ondStr;
				if (reqRoute.getVia() != null) {
					ondStr = reqRoute.getDeparture() + "/" + reqRoute.getVia() + "/" + reqRoute.getArrival();
				} else {
					ondStr = reqRoute.getDeparture() + "/" + reqRoute.getArrival();
				}

				String obSegments = getONDCode(outFlightRPHList);
				Date departureDate = new Date();
				try {
					departureDate = DATE_FORMAT.parse(outFlightRPHList.get(0).toString().split("\\$")[3]);
				} catch (ParseException e) {
					log.error(e);
				}
				if (!(obSegments.equals(ondStr) && sdf.format(departureDate).equals(sdf.format(reqRoute.getDepartureDate())))) {
					return false;
				}

				if (retFlightRPHList != null) {
					String retOndCode = getInverseSegmentCode(ondStr);

					String ibSegments = getONDCode(retFlightRPHList);
					Date returnDate = new Date();
					try {
						returnDate = DATE_FORMAT.parse(retFlightRPHList.get(0).toString().split("\\$")[3]);
					} catch (ParseException e) {
						log.error(e);
					}
					if (!(ibSegments.equals(retOndCode)
							&& sdf.format(returnDate).equals(sdf.format(reqRoute.getReturningDate())))) {
						return false;
					}

				}
			}
			return true;
		}

	}

	private String getONDCode(Collection<String> rphList) {
		String ondCode = null;
		for (String rphVal : rphList) {
			String ond = rphVal.split("\\$")[1];
			if (ondCode == null) {
				ondCode = ond;
			} else {
				String[] airports = ond.split(Pattern.quote("/"));
				for (int i = 0; i < airports.length; i++) {
					if (i == 0) {
						continue;
					}
					ondCode += "/" + airports[i];
				}
			}
		}
		return ondCode;
	}

	private static String getInverseSegmentCode(String ond) {
		String inverseOND = "";
		String airports[] = ond.split("/");
		for (int i = airports.length - 1; i >= 0; i--) {
			inverseOND += i == airports.length - 1 ? airports[i] : "/" + airports[i];
		}
		return inverseOND;
	}

	@Override
	public Collection<GroupPaymentNotificationDTO> getGrooupBookingForReminders() {
		Collection<GroupPaymentNotificationDTO> grpBookingNotifications = ReservationDAOUtils.DAOInstance.GrpBookingDAO
				.getGrooupBookingForReminders();
		return grpBookingNotifications;
	}

	@Override
	public void sendTotalPaymentDueReminder() {
		// TODO Auto-generated method stub
		CredentialsDTO credentialsDTO;
		try {
			credentialsDTO = getCallerCredentials(null);

			GroupBookingRequestBL groupBookingReq = new GroupBookingRequestBL();
			groupBookingReq.sendDuePaymentReminder(credentialsDTO);
		} catch (ModuleException ex) {
			// TODO Auto-generated catch block
			log.error(" ((o)) ModuleException::sendTotalPaymentDueReminder ", ex);

		}
	}

	@Override
	public ServiceResponce addUserStations(String user, Set<String> stations) throws ModuleException {

		// Collection<UserStation> userStation = ReservationDAOUtils.DAOInstance.GrpBookingDAO.getUserStation(user);

		for (String strStaion : stations) {
			UserStation usrSta = new UserStation();
			usrSta.setUserId(user);
			usrSta.setStattionCode(strStaion);
			ReservationDAOUtils.DAOInstance.GrpBookingDAO.saveUserStation(usrSta);

		}
		return new DefaultServiceResponse(true);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public IbeExitUserDetailsDTO saveIbeExitDetais(IbeExitUserDetailsDTO ibeExitDetails) {
		IbeUserExitDetails exitDetails = IbeExitUserDetailsUtil.convertTo(ibeExitDetails);
		ReservationDAOUtils.DAOInstance.IBE_EXIT_DETAILS_DAO.saveIbeExitDetails(exitDetails);
		return IbeExitUserDetailsUtil.convertTo(exitDetails);
	}

	@Override
	public IbeExitUserDetailsDTO getIbeExitDetais(Integer exitDetailsId) {
		IbeUserExitDetails ibeUserExitDetails = ReservationDAOUtils.DAOInstance.IBE_EXIT_DETAILS_DAO
				.getIbeUserExitDetails(exitDetailsId);
		return IbeExitUserDetailsUtil.convertTo(ibeUserExitDetails);

	}

	@Override
	public String getUserStation(String userId) {
		StringBuilder returnStr = new StringBuilder();
		Collection<UserStation> stationExist = ReservationDAOUtils.DAOInstance.GrpBookingDAO.getUserStation(userId);
		UserStation[] stations = stationExist.toArray(new UserStation[stationExist.size()]);

		for (int i = 0; i < stations.length; i++) {
			if (i == stations.length - 1) {
				returnStr.append(stations[i].getStattionCode());
			} else {
				returnStr.append(stations[i].getStattionCode() + ",");
			}
		}
		return returnStr.toString();
	}

	@Override
	public ServiceResponce addGroupBookingStations(long groupReqID, Set<String> stations) throws ModuleException {

		for (String strStaion : stations) {
			GroupRequestStation grpSta = new GroupRequestStation();
			grpSta.setGroupBookingRequestID(groupReqID);
			grpSta.setStationCode(strStaion);
			ReservationDAOUtils.DAOInstance.GrpBookingDAO.saveGroupBookingStation(grpSta);
		}
		return new DefaultServiceResponse(true);
	}

	@Override
	public String getGroupBookingStation(long groupReqID) throws ModuleException {
		StringBuilder returnStr = new StringBuilder();
		Collection<UserStation> stationExist = ReservationDAOUtils.DAOInstance.GrpBookingDAO.getGroupBookingStation(groupReqID);
		GroupRequestStation[] stations = stationExist.toArray(new GroupRequestStation[stationExist.size()]);

		for (int i = 0; i < stations.length; i++) {
			if (i == stations.length - 1) {
				returnStr.append(stations[i].getStationCode());
			} else {
				returnStr.append(stations[i].getStationCode() + ",");
			}
		}
		return returnStr.toString();
	}

	@Override
	public ServiceResponce updateGroupBookingRequote(GroupBookingRequestTO grpBookingReq) throws ModuleException {

		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();
		GroupBookingRequest saveReq = grpBookingReqBL.setGroupBookingReqObject(grpBookingReq);
		ReservationDAOUtils.DAOInstance.GrpBookingDAO.updateAgentRequote(saveReq);
		ReservationDAOUtils.DAOInstance.GrpBookingDAO.saveGroupBookingHistory(
				grpBookingReqBL.setGroupBookingHistory(saveReq, saveReq.getCraeteUserCode(), saveReq.getStatusID()));
		AuditAirMaster.doAudit(((GroupBookingRequest) saveReq), getUserPrincipal());
		return new DefaultServiceResponse(true);
	}

	@Override
	public Page<GroupBookingRequestTO> getGroupBookingRequestForStations(GroupBookingRequestTO grpBookingReq, String stationCode,
			String stations, Integer start, boolean mainReqestOnly) throws ModuleException {

		Collection<GroupBookingRequestTO> transformedDTOs = new HashSet<GroupBookingRequestTO>();
		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();
		GroupBookingRequest getReq = grpBookingReqBL.setSearchGroupBookingReqObject(grpBookingReq);
		if (grpBookingReq.getGroupBookingRoutes() != null) {
			Set<GroupBookingReqRoutes> grpBookingRoutes = grpBookingReqBL
					.setGroupBookingReqRoutesObject(grpBookingReq.getGroupBookingRoutes());
			getReq.setGroupBookingRoutes(grpBookingRoutes);
		}
		Page<GroupBookingRequest> resultPage = ReservationDAOUtils.DAOInstance.GrpBookingDAO.getSharedRequest(getReq, start, 20,
				stationCode, stations, mainReqestOnly);
		for (GroupBookingRequest groupRequest : resultPage.getPageData()) {
			transformedDTOs.add(grpBookingReqBL.setGroupBookingReqToObject(groupRequest));
		}
		Page<GroupBookingRequestTO> resultTOPage = new Page<GroupBookingRequestTO>(resultPage.getTotalNoOfRecords(),
				resultPage.getStartPosition(), resultPage.getEndPosition(), transformedDTOs);
		return resultTOPage;
	}

	@Override
	public GroupBookingRequestTO validateModifyResGroupBookingRequest(long requestID, BigDecimal totalAmount,
			String passengerList, ArrayList<String> flightlist, String agentCode) throws ModuleException {
		GroupBookingRequestTO returnRequest = null;
		GroupBookingRequest grpRequest = ReservationDAOUtils.DAOInstance.GrpBookingDAO.getValidGroupBookingRequest(requestID,
				totalAmount);

		if (grpRequest != null && grpRequest.getAgreedFare() != null && grpRequest.getAgreedFare().compareTo(totalAmount) == 0
				&& grpRequest.getRequestedAgentCode().equals(agentCode)) {
			if (isValidGrpBookingRequestRouteForOnhold(grpRequest, passengerList, flightlist)) {
				returnRequest = new GroupBookingRequestBL().setGroupBookingReqToObject(grpRequest);
			}
		}

		return returnRequest;
	}

	private boolean isValidGrpBookingRequestRouteForOnhold(GroupBookingRequest grpRequest, String passengerList,
			ArrayList<String> flightList) {
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyyMMdd");
		String grpPsgList = grpRequest.getAdultAmount() + "|" + grpRequest.getChildAmount() + "|" + grpRequest.getInfantAmount();

		if (!grpPsgList.equals(passengerList)) {
			return false;
		} else {
			int count = 0;
			for (GroupBookingReqRoutes reqRoute : grpRequest.getGroupBookingRoutes()) {
				String ondStr = reqRoute.getDeparture() + "/" + reqRoute.getArrival();
				for (int i = 0; i < flightList.size(); i++) {
					String obSegments = flightList.get(i).toString().split("\\$")[1];

					Date departureDate = new Date();
					try {
						departureDate = DATE_FORMAT.parse(flightList.get(i).toString().split("\\$")[3]);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (obSegments.equals(ondStr) && sdf.format(departureDate).equals(sdf.format(reqRoute.getDepartureDate()))) {
						count++;
					}
				}

			}
			if (count == grpRequest.getGroupBookingRoutes().size()) {
				return true;
			} else {
				return false;
			}
		}

	}

	@Override
	public Page<GroupBookingRequestHistoryTO> getGroupBookingRequestHistory(long requestID) throws ModuleException {
		Collection<GroupBookingRequestHistoryTO> transformedDTOs = new HashSet<GroupBookingRequestHistoryTO>();
		GroupBookingRequestBL grpBookingReqBL = new GroupBookingRequestBL();

		Page<GroupBookingRequestHistory> resultPage = ReservationDAOUtils.DAOInstance.GrpBookingDAO
				.getGroupBookingHistory(requestID, 0, 20);
		for (GroupBookingRequestHistory groupRequest : resultPage.getPageData()) {
			transformedDTOs.add(grpBookingReqBL.setGroupBookingReqHistoryObject(groupRequest));
		}
		Page<GroupBookingRequestHistoryTO> resultTOPage = new Page<GroupBookingRequestHistoryTO>(resultPage.getTotalNoOfRecords(),
				resultPage.getStartPosition(), resultPage.getEndPosition(), transformedDTOs);
		return resultTOPage;
	}

	@Override
	public ServiceResponce updateGroupBookingStatus(long requestID, BigDecimal totalPayment, String pnr) throws ModuleException {
		ServiceResponce serviceRes = ReservationDAOUtils.DAOInstance.GrpBookingDAO.reservationBookingStatus(requestID, pnr);
		if (serviceRes.isSuccess()) {
			ReservationDAOUtils.DAOInstance.GrpBookingDAO.updateBookingPaymentStatus(requestID, totalPayment);
		}

		return new DefaultServiceResponse(true);
	}

	public List<ReservationDTO> getImmediateReservations(int customerId, int count) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getImmediateReservations(customerId, count);
	}

	public boolean applyAgentCommissionForOnd(PreferenceAnalyzer preferenceAnalyzer, String ondCode, List<String> journeyOnds,
			List<List<String>> journeyOndSummary) {

		CommissionStrategy commissionStrategy = null;

		boolean applyCommission = false;
		if (preferenceAnalyzer != null) {

			PreferenceAnalyzer.MODE mode = preferenceAnalyzer.getOperation();

			if (mode != null) {
				switch (mode) {

				case CREATE_FRESH:
					commissionStrategy = new CreateResCS();
					break;

				case ADD_OND:
					commissionStrategy = new AddSegmentCS();
					break;

				case MODIFY_OND:
					commissionStrategy = new ModifyOndCS();
					break;

				default:
					break;
				}
			}

		}

		if (commissionStrategy != null) {
			applyCommission = commissionStrategy.execute(preferenceAnalyzer, ondCode, journeyOnds, journeyOndSummary);
		}

		return applyCommission;
	}

	@Override
	public Collection<Reservation> getReservation(long groupBookingReqId) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.GrpBookingDAO.getReservation(groupBookingReqId);
	}

	@Override
	public String getActualPaymentByPnr(String pnr) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.GrpBookingDAO.getActualPaymentByPnr(pnr);
	}

	public void getAffectedPnrSegments(Collection<Integer> flightSegmentIDs) throws ModuleException {
		CredentialsDTO credentialsDTO = getCallerCredentials(null);

		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		List<Integer> colPnrSegmentIds = reservationSegmentDAO.getAffectedPnrs(flightSegmentIDs,
				ReservationInternalConstants.ReservationStatus.CONFIRMED);
		if (colPnrSegmentIds != null && colPnrSegmentIds.size() > 0) {
			Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationSegmentDAO
					.getSegmentInformation(colPnrSegmentIds);
			for (ReservationSegmentDTO seg : colReservationSegmentDTO) {
				ReservationAudit reservationAudit = new ReservationAudit();
				reservationAudit.setPnr(seg.getPnr());
				reservationAudit.setModificationType(AuditTemplateEnum.CHANGE_FLIGHT_TIME.getCode());
				if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ViewedReservation.ORIGIN_CARRIER,
							credentialsDTO.getTrackInfoDTO().getCarrierCode());
				}
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.FLIGHT_NO, seg.getFlightNo());
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.DEPARTURE_DATE,
						seg.getDepartureDate().toString());
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.ARRIVAL_DATE,
						seg.getArrivalDate().toString());
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.SEGMENT_CODE,
						seg.getSegmentCode());

				ReservationBO.recordModification(seg.getPnr(), reservationAudit, null, credentialsDTO);
			}
		}
	}

	public void auditFlightTimeChanges(Collection<Integer> flightSegmentIDs, Collection<FlightSegement> updatedSegList,
			String flightNo, String userId) throws ModuleException {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		List<Integer> pnrList = reservationSegmentDAO.getAffectedPnrs(flightSegmentIDs,
				ReservationInternalConstants.ReservationStatus.CONFIRMED);
		ArrayList<ReservationAudit> reservationAuditList = new ArrayList<ReservationAudit>();
		if (!pnrList.isEmpty()) {
			for (Iterator<Integer> it = pnrList.iterator(); it.hasNext();) {
				Object pnr = it.next();
				for (FlightSegement updatedSeg : updatedSegList) {
					ReservationAudit reservationAudit = new ReservationAudit();
					reservationAudit.setPnr(pnr.toString());
					reservationAudit.setZuluModificationDate(new Date());
					reservationAudit.setModificationType(AuditTemplateEnum.CHANGE_FLIGHT_TIME.getCode());
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.FLIGHT_NO, flightNo);
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.DEPARTURE_DATE,
							updatedSeg.getEstTimeDepatureLocal().toString());
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.ARRIVAL_DATE,
							updatedSeg.getEstTimeArrivalLocal().toString());
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.SEGMENT_CODE,
							updatedSeg.getSegmentCode());
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdateFlightTime.USER_ID, userId);
					reservationAudit.setUserId(userId);
					reservationAuditList.add(reservationAudit);
				}
			}
			RecordAuditViaMDB auditViaMDB = new RecordAuditViaMDB();
			auditViaMDB.sendMessageViaMDB(reservationAuditList);
		}
	}

	@Override
	public Map<String, Integer> getSegmentBundledFarePeriodIds(String pnr) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getSegmentBundledFarePeriodIds(pnr);
		} catch (CommonsDataAccessException ex) {
			log.error(" ((o)) ModuleException::getSegmentBundledFarePeriodIds ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		}
	}

	@Override
	public ServiceTaxContainer getApplicableServiceTax(AvailableIBOBFlightSegment availableIBOBFlightSegment,
			EXTERNAL_CHARGES taxExternalChargeCode) throws ModuleException {
		try {
			return ReservationBO.getApplicableServiceTax(taxExternalChargeCode, availableIBOBFlightSegment);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airreservation.desc");
		}
	}

	@Override
	public ServiceTaxContainer getApplServiceTaxContainer(ReservationSegmentDTO reservationSegmentDTO,
			EXTERNAL_CHARGES taxExternalChargeCode) throws ModuleException {
		try {
			return ReservationBO.getApplicableServiceTax(taxExternalChargeCode, reservationSegmentDTO);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airreservation.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void mergeGdsReservationAttributes(GdsReservationAttributesDto gdsReservationAttributesDto) throws ModuleException {

		try {
			Reservation reservation = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
					.getReservation(gdsReservationAttributesDto.getPnr(), false);

			reservation.setExternalRecordLocator(gdsReservationAttributesDto.getExternalRecLocator());
			reservation.setExternalPos(gdsReservationAttributesDto.getExternalPos());

			ReservationDAOUtils.DAOInstance.RESERVATION_DAO.saveReservation(reservation);
		} catch (Exception e) {
			throw new ModuleException("GdsReservationAttributes FAIL", e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendIBEOnholdPaymentReminderEmail(String pnr, boolean isGroupPnr, boolean isCreateBooking)
			throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(null);
			/**
			 * AARESAA-18772 For interline or Dry, payment is not allowed via APIs(because no ETKT implementation),So I
			 * am also not sending the email for the same.But I have made proficience in the code to support this when
			 * ever this is allowed Then we just have to remove !isGroupPnr
			 **/

			if (!isGroupPnr) {
				// Load Reservation
				LCCClientReservation lccClientReservation = AirproxyModuleUtils.getAirproxyReservationBD()
						.searchReservationByPNR(getPnrModesDTO(pnr, isGroupPnr), null, credentialsDTO.getTrackInfoDTO());

				if (lccClientReservation != null
						&& ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(lccClientReservation.getStatus())) {
					// Send Email reminder
					IBEOnholdPaymentReminderBL paymentReminderBL = new IBEOnholdPaymentReminderBL();
					paymentReminderBL.sendPaymentReminder(lccClientReservation, credentialsDTO, isCreateBooking);
				}
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::sendIBEOnholdPaymentReminderEmail ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	private static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
		} else {
			pnrModesDTO.setPnr(pnr);
		}

		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLocalTimes(false);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);

		return pnrModesDTO;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<OnHoldBookingInfoDTO> getPnrInformationToSendPaymentReminders(Date toDate) throws ModuleException {
		try {

			if (toDate == null) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}
			List<OnHoldBookingInfoDTO> bookingInfoDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
					.getOnholdPnrsForPaymentReminder(toDate);
			return bookingInfoDTOs;

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrInformationToSendPaymentReminders ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public void getPfsFilesViaSitaTex(List<String> messages) throws ModuleException {
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		String processPath = airReservationConfig.getPfsProcessPath();
		if (!messages.isEmpty()) {
			File messagesDir = new File(processPath);
			if (!messagesDir.exists()) {
				boolean status = messagesDir.mkdirs();
				if (!status) {
					throw new ModuleException("airreservations.sitatex.filesCreationError");
				}
			}
			Date currentDate = new Date();
			FileWriter fileWriter = null;
			BufferedWriter bufferWriter = null;
			File file = null;
			int i = 0;

			for (String message : messages) {
				try {
					file = new File(messagesDir, PFSFormatUtils.getPfsFileName(currentDate, i));
					fileWriter = new FileWriter(file);
					bufferWriter = new BufferedWriter(fileWriter);
					bufferWriter.write(message);
					log.info(" ###SITATEXT####### File Created " + file.getName());
					bufferWriter.close();
					fileWriter.close();
				} catch (Exception e) {
					log.error(" ###SITATEXT######## File Creation Error File Name is " + file.getName(), e);
				} finally {
					if (bufferWriter != null) {
						try {
							bufferWriter.close();
						} catch (IOException e) {
						}
					}
				}
				i++;
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public EticketTO getEticketDetailsByExtETicketNumber(String extEticketNumber, Integer extCoupNumber) throws ModuleException {
		try {
			ETicketDAO eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
			return eticketDAO.getEticketDetailsByExtETicketNumber(extEticketNumber, extCoupNumber);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getEticketDetailsByExtETicketNumber", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getEticketDetailsByExtETicketNumber ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isCodeSharePNR(String extEticketNumber) throws ModuleException {

		try {
			return ReservationDAOUtils.DAOInstance.ETicketDAO.isCodeSharePNR(extEticketNumber);
		} catch (Exception e) {
			log.error(" ((o)) CommonsDataAccessException::isCodeSharePNR ", e);
			throw new ModuleException(e.getMessage());
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cancelAirportTransfers(Collection<Integer> pnrSegIds) throws ModuleException {
		ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO.cancelAirportTransfer(pnrSegIds);
	}

	@Override
	public void cancelAirportTransferRequests(Reservation reservation, Collection<Integer> pnrSegmentIds,
			Map<Integer, Collection<PaxAirportTransferTO>> cancelledSSRMap, CredentialsDTO credentialsDTO) {
		try {
			TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
			trackInfoDTO.setIpAddress(getUserPrincipal().getIpAddress());
			trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation.getPnr(), false);
			ReservationApiUtils.cancelSegmentApts(reservation, pnrSegmentIds, getCallerCredentials(trackInfoDTO), chgTnxGen);

			AirportTransferBL.sendAirportTransferCancelNotifications(reservation, pnrSegmentIds, cancelledSSRMap,
					getCallerCredentials(trackInfoDTO), null);
		} catch (ModuleException e) {
			log.error(" ((o)) CommonsDataAccessException::sendAirportTransferCancelNotifications ", e);
			e.printStackTrace();
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ArrayList<Reservation> loadReservationsHavingAirportTransfers(List<Integer> flightSegIds) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.loadReservationsHavingAirportTransfers(flightSegIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPnrSegments ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	private void redeemLoyaltyRewards(String[] rewardIds, AppIndicatorEnum appIndicator, String loyaltyMemberId)
			throws ModuleException {
		if (rewardIds != null && rewardIds.length > 0) {
			ServiceResponce response = ReservationModuleUtils.getLoyaltyManagementBD().redeemIssuedRewards(rewardIds,
					appIndicator, loyaltyMemberId);

			if (!response.isSuccess()) {
				throw new ModuleException("airreservations.loyalty.redeem.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce updatePaxNames(Map<Integer, NameDTO> changedPaxNamesMap, String pnr, long version, String appIndicator,
			boolean skipDuplicateNameCheck, TrackInfoDTO trackInfoDTO, String reasonForAllowBlPax) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);

		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Check reservation version compatibility
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);

		for (Integer pnrPaxId : changedPaxNamesMap.keySet()) {
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {
					NameDTO nameDTO = changedPaxNamesMap.get(pnrPaxId);
					reservationPax.setFirstName(nameDTO.getFirstname());
					reservationPax.setLastName(nameDTO.getLastName());
					reservationPax.getPaxAdditionalInfo().setFfid(nameDTO.getFFID());
					reservationPax.setTitle(nameDTO.getTitle());
					break;
				}
			}
		}

		if (trackInfoDTO == null) {
			trackInfoDTO = new TrackInfoDTO();
			trackInfoDTO.setIpAddress(getUserPrincipal().getIpAddress());
			trackInfoDTO.setCarrierCode(getUserPrincipal().getDefaultCarrierCode());
		}
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

		boolean dupNameCheck = AppSysParamsUtil.isDuplicateNameCheckEnabled() && !skipDuplicateNameCheck;

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.UPDATE_RESERVATION_MACRO);
		command.setParameter(CommandParamNames.RESERVATION, reservation);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.CHECK_DUPLICATE_NAMES, dupNameCheck);
		command.setParameter(CommandParamNames.CHECK_DUPLICATE_NAMES, Boolean.valueOf(false));
		command.setParameter(CommandParamNames.IS_DUMMY_RESERVATION, Boolean.valueOf(false));
		command.setParameter(CommandParamNames.APPLY_NAME_CHANGE_CHARGE, Boolean.valueOf(false));
		command.setParameter(CommandParamNames.IS_FROM_NAME_CHANGE, Boolean.valueOf(true));
		command.setParameter(CommandParamNames.IS_TBA_NAME_CHANGE, Boolean.valueOf(true));
		command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, getReservationBeforeUpdate(pnr));
		command.setParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, reasonForAllowBlPax);

		return command.execute();
	}

	@SuppressWarnings("rawtypes")
	public void sendGroupBookingNotificationMessages(LCCClientReservation ownReservation, String action) throws ModuleException {
		GroupBookingRequestBL groupBookingReq = new GroupBookingRequestBL();
		List messagesList = groupBookingReq.sendGroupBookingNotificationMessages(ownReservation, action);

		if (messagesList.size() > 0) {
			ReservationModuleUtils.getMessagingServiceBD().sendMessages(messagesList);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page getOfficersMobileNumbers(OfficerMobNumsConfigsSearchDTO officerMobNumsConfigsSearchDTO, int startIndex,
			int pageLength) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.OFFICERS_MOBILE_NUMBERS_DAO
				.getOfficersMobileNumbers(officerMobNumsConfigsSearchDTO, startIndex, pageLength);
	}

	@Override
	public Collection<String> getOfficersEmailList() throws ModuleException {
		return ReservationDAOUtils.DAOInstance.OFFICERS_MOBILE_NUMBERS_DAO.getOfficersEmailList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteOfficersMobileNumbersConfig(int OfficerId) throws ModuleException {
		ReservationDAOUtils.DAOInstance.OFFICERS_MOBILE_NUMBERS_DAO.deleteOfficersMobileNumbersConfig(OfficerId);
		OfficersMobNumsAdaptor.auditForDeleteOfficersMobNumsConfigModification(getUserPrincipal(), OfficerId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public OfficersMobileNumbers getOfficersMobNums(int releaseTimeId) throws ModuleException {
		OfficersMobileNumbers OfficersMobNums = ReservationDAOUtils.DAOInstance.OFFICERS_MOBILE_NUMBERS_DAO
				.getOfficersMobNums(releaseTimeId);
		return OfficersMobNums;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveOrUpdateOfficersMobileNumbersConfig(OfficersMobileNumbers OfficersMobNums, Map<String, String[]> contMap)
			throws ModuleException {
		try {
			long versionFlg = OfficersMobNums.getVersion();
			OfficersMobileNumbersDAO OfficersMobNumsDAO = ReservationDAOUtils.DAOInstance.OFFICERS_MOBILE_NUMBERS_DAO;
			OfficersMobNumsDAO.saveOrUpdateOfficersMobileNumbersConfig(OfficersMobNums);
			OfficersMobNumsAdaptor.auditForOfficersMobNumsConfigModification(getUserPrincipal(), OfficersMobNums, contMap,
					versionFlg);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveOrUpdateOfficersMobileNumbers ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<String> getOfficersMobileNumbersList() throws ModuleException {
		return ReservationDAOUtils.DAOInstance.OFFICERS_MOBILE_NUMBERS_DAO.getOfficersMobileNumbersList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce syncAATotalGroupChargesWithCarrier(GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ, TrackInfoDTO trackInfo)
			throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.SYNC_CARRIER_TOTAL_GROUP_CHARGES);

			command.setParameter(CommandParamNames.PNR, gdsChargeAdjustmentRQ.getPnr());
			command.setParameter(CommandParamNames.GDS_CHARGE_BY_PAX, gdsChargeAdjustmentRQ.getGdsChargesByPax());
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::syncAATotalGroupChargesWithCarrier ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::syncAATotalGroupChargesWithCarrier ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Integer> getPaxFareSegmentETicketIDs(Collection<Integer> getPaxFareSegmentETicketID)
			throws ModuleException {
		try {
			ETicketDAO eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
			return eticketDAO.getPaxFareSegmentETicketIDs(getPaxFareSegmentETicketID);

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getPaxFareSegmentETicketID", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPaxFareSegmentETicketID ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce exchangeReservationSegment(String pnr, Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.EXCHANGE_SEGMENT_MACRO);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.EXCHANGE_TIKCET_TRANSITION_MAP, transitions);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::exchangeReservationSegment ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::exchangeReservationSegment ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersAsSubstitution(Reservation reservation,
			CreateTicketInfoDTO ticketingInfoDto) throws ModuleException {
		return ETicketBO.generateIATAETicketNumbersAsSubstitution(reservation, ticketingInfoDto);
	}

	public Map<String, Collection<String>> getPassengerTitleTypes(Collection<String> excludingPaxTypes) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getPassengerTitleTypes(excludingPaxTypes);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassengerTitleMap ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public ReservationDiscountDTO calculateDiscount(DiscountRQ promotionRQ, TrackInfoDTO trackInfoDTO) throws ModuleException {
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.DISCOUNT_CALCULATOR);
		command.setParameter(CommandParamNames.PROMO_CALCULATOR_RQ, promotionRQ);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		ServiceResponce serviceResponce = command.execute();
		return (ReservationDiscountDTO) serviceResponce.getResponseParam(CommandParamNames.RESERVATION_DISCOUNT_DTO);

	}

	public Collection<Reservation> getReservationsByPnr(Collection<String> pnrList, boolean loadFares) throws ModuleException {
		return ReservationProxy.getReservationsByPnr(pnrList, loadFares);
	}

	public Collection<Reservation> getReservationsByFlightSegmentId(Integer flightSegId, boolean loadFares)
			throws ModuleException {
		return ReservationProxy.getReservationsByFlightSegmentId(flightSegId, loadFares);
	}

	private Collection<ReservationPax> getAncilliaryAddedPassengers(Collection<String> addedPnrPaxIds,
			Collection<String> updatedPnrPaxIds) {
		ReservationPax pax;
		Collection<ReservationPax> anciUpdatedPasPaxs = new ArrayList<ReservationPax>();
		Collection<String> pnrPaxIds = new ArrayList<String>();
		pnrPaxIds.addAll(addedPnrPaxIds);
		pnrPaxIds.addAll(updatedPnrPaxIds);

		for (String paxId : pnrPaxIds) {
			pax = new ReservationPax();
			pax.setPnrPaxId(new Integer(StringUtil.getSubString(paxId, paxId.indexOf("|"))));
			anciUpdatedPasPaxs.add(pax);
		}
		return anciUpdatedPasPaxs;
	}

	private Collection<ReservationPax>
			getUpdatedETHavingPassengers(Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets) {
		Collection<ReservationPax> etGenerationEligiblePaxForAdl = new ArrayList<ReservationPax>();
		for (ReservationPaxFareSegmentETicket segETicket : modifiedPaxfareSegmentEtickets) {
			ReservationPax pax = new ReservationPax();
			pax.setPnrPaxId(segETicket.getReservationPaxETicket().getPnrPaxId());
			etGenerationEligiblePaxForAdl.add(pax);
		}
		return etGenerationEligiblePaxForAdl;
	}

	@Override
	public Collection<CSPaxBookingCount> getCodeSharePaxCountByBookingClass(String csFlightNumber, Date departureDate,
			String carrierCode) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			return paxFinalSalesDAO.getCodeSharePaxCountByBookingClass(csFlightNumber, departureDate, carrierCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getCodeSharePaxCountByBookingClass ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveCodeSharePfsEntry(CodeSharePfs pfs) throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			paxFinalSalesDAO.saveCodeSharePfsEntry(pfs);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::saveCodeSharePfsEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	public boolean checkCodeSharePfsAlreadySent(Date departureDate, String flightNumber, String fromAirport, String carrierCode)
			throws ModuleException {
		try {
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			return paxFinalSalesDAO.checkCodeSharePfsAlreadySent(departureDate, flightNumber, fromAirport, carrierCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::checkCodeSharePfsAlreadySent ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce adjustGDSChargesBulkManual(String pnr,
			Map<Integer, Map<Integer, Map<String, BigDecimal>>> paxWiseAjustments, long version, String userNote,
			Map<Integer, BigDecimal> totalPlusAdjByPaxFareId, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			// Retrieve called credential information
			CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			// Return the reservation
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			List<Integer> forcedConfirmedPaxSeqsBeforeAdj = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

			// Checking to see if any concurrent update exist
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);

			AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);
			ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation, false);
			ChargeCache cc = new ChargeCache();
			String chargeCode = null;
			if (paxWiseAjustments != null && !paxWiseAjustments.isEmpty()) {
				for (Integer pnrPaxId : paxWiseAjustments.keySet()) {
					Map<Integer, Map<String, BigDecimal>> paxChargeSet = paxWiseAjustments.get(pnrPaxId);
					if (paxChargeSet != null && !paxChargeSet.isEmpty()) {
						for (Integer pnrPaxFareId : paxChargeSet.keySet()) {
							Map<String, BigDecimal> groupWise = paxChargeSet.get(pnrPaxFareId);
							groupWise = sortByValuesDesc(groupWise);
							for (String chargeGroup : groupWise.keySet()) {
								if (ChargeGroup.FAR.equals(chargeGroup)) {
									chargeCode = AirinventoryCustomConstants.ChargeCodes.GDS_FARE_GROUP_CHARGE_ADJ;
								} else if (ChargeGroup.TAX.equals(chargeGroup)) {
									chargeCode = AirinventoryCustomConstants.ChargeCodes.GDS_TAX_GROUP_CHARGE_ADJ;
								} else if (ChargeGroup.SUR.equals(chargeGroup)) {
									chargeCode = AirinventoryCustomConstants.ChargeCodes.GDS_SUR_GROUP_CHARGE_ADJ;
								} else {
									chargeCode = AirinventoryCustomConstants.ChargeCodes.GDS_OTHER_GROUP_CHARGE_ADJ;
								}
								Integer chargeRateID = cc.getChargeRateId(chargeCode);

								BigDecimal amount = groupWise.get(chargeGroup);

								Map<Integer, BigDecimal> adjustedPassengerChargeAmount = ReservationApiUtils
										.getAdjustedAmountForPassenger(reservation, pnrPaxFareId, amount,
												totalPlusAdjByPaxFareId);

								if (adjustedPassengerChargeAmount != null && adjustedPassengerChargeAmount.size() > 0) {

									BigDecimal adjustedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
									for (Integer paxFareId : adjustedPassengerChargeAmount.keySet()) {
										adjustedAmount = adjustedPassengerChargeAmount.get(paxFareId);

										if (adjustedAmount.doubleValue() != 0) {
											// Adjusting the credit manually
											ReservationBO.adjustCreditManual(adjustCreditBO, reservation, paxFareId, chargeRateID,
													adjustedAmount, ReservationInternalConstants.ChargeGroup.ADJ, userNote,
													trackInfoDTO, credentialsDTO, chgTnxGen);
										}

									}

								} else {
									// Adjusting the credit manually
									ReservationBO.adjustCreditManual(adjustCreditBO, reservation, pnrPaxFareId, chargeRateID,
											amount, ReservationInternalConstants.ChargeGroup.ADJ, userNote, trackInfoDTO,
											credentialsDTO, chgTnxGen);
								}
							}
						}

					}

				}
				// Save the reservation
				ReservationProxy.saveReservation(reservation);
				adjustCreditBO.callRevenueAccountingAfterReservationSave();

				reservation = ReservationProxy.getReservation(pnrModesDTO);
				List<Integer> forcedConfirmedPaxSeqsAfterAdj = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
				handleETGeneration(reservation, forcedConfirmedPaxSeqsBeforeAdj, forcedConfirmedPaxSeqsAfterAdj, credentialsDTO);
			}

			return new DefaultServiceResponse(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::adjustGDSChargesBulkManual ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::adjustGDSChargesBulkManual ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	private Map<String, BigDecimal> sortByValuesDesc(Map<String, BigDecimal> unsortMap) {

		List<Map.Entry<String, BigDecimal>> list = new LinkedList<Map.Entry<String, BigDecimal>>(unsortMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, BigDecimal>>() {
			public int compare(Map.Entry<String, BigDecimal> o1, Map.Entry<String, BigDecimal> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<String, BigDecimal> sortedMap = new LinkedHashMap<String, BigDecimal>();
		for (Iterator<Map.Entry<String, BigDecimal>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, BigDecimal> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	private Reservation getReservationBeforeUpdate(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDto = new LCCClientPnrModesDTO();
		pnrModesDto.setPnr(pnr);
		pnrModesDto.setLoadFares(true);
		pnrModesDto.setLoadPaxAvaBalance(true);
		pnrModesDto.setLoadSegView(true);
		pnrModesDto.setLoadSSRInfo(true);

		return ReservationProxy.getReservation(pnrModesDto);

	}

	@Override
	public ServiceResponce detectReservationModification(Reservation reservationBeforeModify) throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RES_MODIFY_DETECTOR);

		command.setParameter(CommandParamNames.PNR, reservationBeforeModify.getPnr());
		command.setParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, reservationBeforeModify);
		return command.execute();
	}

	/**
	 * Add User Note
	 * 
	 * @param userNoteTO
	 */
	public void addUserNote(UserNoteTO userNoteTO, TrackInfoDTO trackInfoDTO) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);
			AuditorBL auditorBL = new AuditorBL();
			auditorBL.doAuditUN(userNoteTO, credentialsDTO);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::addUserNote", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveBlacklistPAX(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO) throws ModuleException {
		BlacklistPAX blacklistPAXToSave = BlacklistPAXUtil.blacklistPAXTOToBlacklistPAX(blacklistPAXCriteriaTO);
		blacklistPAXToSave = (BlacklistPAX) setUserDetails(blacklistPAXToSave);
		ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.saveBlacklistPAX(blacklistPAXToSave);

	}

	@Override
	public Page<BlacklistPAXCriteriaTO> searchBlacklistPAXCriteria(BlacklistPAXCriteriaSearchTO blacklistPAXCriteriaSearchTO,
			Integer start, Integer size) throws ModuleException {
		try {
			Page<BlacklistPAX> resultPage = ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO
					.searchBlacklistPAX(blacklistPAXCriteriaSearchTO, start, size);

			Collection<BlacklistPAXCriteriaTO> transformedDTOs = new HashSet<BlacklistPAXCriteriaTO>();
			for (BlacklistPAX criteria : resultPage.getPageData()) {
				transformedDTOs.add(BlacklistPAXUtil.blacklistPAXToBlacklistPAXTO(criteria));
			}

			Page<BlacklistPAXCriteriaTO> resultTOPage = new Page<BlacklistPAXCriteriaTO>(resultPage.getTotalNoOfRecords(),
					resultPage.getStartPosition(), resultPage.getEndPosition(), transformedDTOs);

			return resultTOPage;
		} catch (Exception x) {
			throw handleException(x, true, log);
		}

	}

	@Override
	public boolean isBlacklistPAXAvailable(long blacklistPAXCriteriaID) {
		BlacklistPAX blacklistPAX = ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO
				.searchBlacklistPAXByBlacklistPAXId(blacklistPAXCriteriaID);
		if (blacklistPAX != null) {
			return true;
		}

		return false;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public long updateBlacklistPAX(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO) throws ModuleException {
		BlacklistPAX blacklistPAXToSave = BlacklistPAXUtil.blacklistPAXTOToBlacklistPAX(blacklistPAXCriteriaTO);
		blacklistPAXToSave = (BlacklistPAX) setUserDetails(blacklistPAXToSave);
		return ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.updateBlacklistPAX(blacklistPAXToSave);

	}

	@Override
	public Collection<String> removeFFIDFromUnflownPNRs(String ffid, TrackInfoDTO trackInfoDTO) throws ModuleException {

		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;

		Collection<Map<String, String>> pnrPaxList = reservationDAO.getUnflownPNRsForFFID(ffid);

		Collection<String> pnrList = new ArrayList<String>();
		Collection<Integer> pnrPaxIDList = new ArrayList<Integer>();

		if (!pnrPaxList.isEmpty()) {

			for (Map<String, String> paxMap : pnrPaxList) {
				pnrPaxIDList.add(Integer.parseInt(paxMap.get("pnr_pax_id")));
			}

			reservationDAO.removeFFIDFromUnflownPNRs(ffid, pnrPaxIDList);
			CredentialsDTO credentialsDTO = getCallerCredentials(trackInfoDTO);

			for (Map<String, String> paxMap : pnrPaxList) {
				ReservationAudit reservationAudit = new ReservationAudit();
				reservationAudit.setPnr(paxMap.get("pnr"));
				pnrList.add(paxMap.get("pnr"));
				reservationAudit.setZuluModificationDate(new Date());
				reservationAudit.setModificationType(AuditTemplateEnum.FFID_REMOVE_CUSTOMER_NAME_UPDATE.getCode());
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.RemoveFFIDFromUnflownPNRs.FFID, ffid);
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.RemoveFFIDFromUnflownPNRs.PAX_NAME,
						paxMap.get("title") + " " + paxMap.get("first_name") + " " + paxMap.get("last_name"));
				ReservationBO.recordModification(paxMap.get("pnr"), reservationAudit, null, credentialsDTO);
			}
		}
		return pnrList;
	}

	public Reservation loadReservationByPnrPaxId(int pnrPaxId) throws ModuleException {
		try {
			Reservation reservation = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.loadReservationByPnrPaxId(pnrPaxId);
			Collection<ReservationSegmentDTO> segmentsView = ReservationProxy.getPnrSegmentsView(reservation.getPnr(), true);
			reservation.setSegmentsView(segmentsView);
			return reservation;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassenger ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public ServiceResponce reverseRefundableCharges(String pnr, Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			boolean isNoShoTaxReversal, TrackInfoDTO trackInfoDTO) throws ModuleException {
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfoDTO);
		try {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CHARGE_REVERSER);

			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.PNR_PAX_IDS, pnrPaxIds);
			command.setParameter(CommandParamNames.PNR_SEGMENT_IDS, pnrSegIds);
			command.setParameter(CommandParamNames.IS_NO_SHOW_TAX_REVERSAL, isNoShoTaxReversal);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

			ServiceResponce serviceResponce = command.execute();
			return serviceResponce;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reverseRefundableCharges ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reverseRefundableCharges ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void markPaymentTransactionNotFirst(Integer pnrPaxID, Integer tnxID, String lccUniqueTnxID) throws ModuleException {

		ReservationPaxExtTnx extPaxTnx = null;
		if (lccUniqueTnxID != null && !"".contentEquals(lccUniqueTnxID.trim())) {
			Collection<ReservationPaxExtTnx> paxExtTranxs = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getExternalPaxPayments(pnrPaxID);
			for (ReservationPaxExtTnx extTrnx : paxExtTranxs) {
				if (lccUniqueTnxID.equals(extTrnx.getLccUniqueId())) {
					extPaxTnx = extTrnx;
				}
			}
		}
		ReservationTnx paxTransaction = null;
		if (tnxID != null && tnxID != 0) {
			ReservationTnx tnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getTransaction(tnxID);
			if (lccUniqueTnxID != null && !"".contentEquals(lccUniqueTnxID.trim())) {
				if (lccUniqueTnxID.equals(tnx.getLccUniqueId())) {
					paxTransaction = tnx;
				}
			} else {
				paxTransaction = tnx;
			}

			if (paxTransaction == null) {
				throw new ModuleException("invalid pax transaction");
			}
		}

		if (paxTransaction == null && extPaxTnx == null) {
			throw new ModuleException("invalid pax transaction");
		}

		if (extPaxTnx != null && CommonsConstants.YES.equals(extPaxTnx.getFirstPayment())) {
			extPaxTnx.setFirstPayment(CommonsConstants.NO);
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxExtTnx(extPaxTnx);
		}

		if (paxTransaction != null && CommonsConstants.YES.equals(paxTransaction.getFirstPayment())) {
			paxTransaction.setFirstPayment(CommonsConstants.NO);
			ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.saveTransaction(paxTransaction);
		}

	}

	@Override
	public List<ConnectionPaxInfoTO> getConnectionPaxInfo(Date fromDate, Date toDate, int connectionTime, List<String> segments)
			throws ModuleException {

		try {

			if (segments == null || segments.isEmpty()) {
				return Collections.emptyList();
			}

			return ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getConnectionPax(fromDate, toDate, connectionTime, segments);

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPassengerTitleMap ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public List<TaxInvoice> getTaxInvoiceFor(List<String> invoiceNumbers) throws ModuleException {
		return DAOInstance.TAX_INVOICE_DAO.getTaxInvoiceFor(invoiceNumbers);
	}

	@Override
	public List<TaxInvoice> getTaxInvoiceFor(String pnr) throws ModuleException {
		return DAOInstance.TAX_INVOICE_DAO.getTaxInvoiceFor(pnr);
	}

	@Override
	public List<TaxInvoice> getTaxInvoiceForPrintInvoice(String pnr) throws ModuleException {
		return DAOInstance.TAX_INVOICE_DAO.getTaxInvoiceForPrintInvoice(pnr);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer recordTemporyPaymentEntryForOffline(ReservationContactInfo reservationContactInfo,
			OfflinePaymentInfo offlinePaymentInfo, CredentialsDTO credentialsDTO, boolean isCredit, boolean isOfflinePayment,
			String pnr) throws ModuleException {
		return PaymentGatewayBO.recordTemporyPaymentEntryForOffline(reservationContactInfo, offlinePaymentInfo, credentialsDTO,
				isCredit, isOfflinePayment, pnr);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void handleBalanceAmountRequestForOfflinePGW(ReservationContactInfo reservationContactInfo,
			OfflinePaymentInfo offlinePaymentInfo, CredentialsDTO credentialsDTO, boolean isCredit, boolean isOfflinePayment,
			CreditCardPaymentStatusDTO creditCardPaymentStatusDTO, IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		Integer temporaryPaymentID = PaymentGatewayBO.recordTemporyPaymentEntryForOffline(reservationContactInfo,
				offlinePaymentInfo, credentialsDTO, isCredit, isOfflinePayment, creditCardPaymentStatusDTO.getPnr());
		creditCardPaymentStatusDTO.setTemporaryPaymentId(temporaryPaymentID);
		creditCardPaymentStatusDTO.setMerchantTnxId(AppIndicatorEnum.APP_IBE.toString() + temporaryPaymentID.toString());
		ReservationModuleUtils.getPaymentBrokerBD().auditCCTransaction(creditCardPaymentStatusDTO, ipgIdentificationParamsDTO);

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection<Integer> makeTemporyPaymentEntry(Map<Integer, CardPaymentInfo> mapTnxIds,
			ReservationContactInfo reservationContactInfo, Collection<PaymentInfo> colPaymentInfo, TrackInfoDTO trackInfo,
			boolean isCredit, boolean isOfflinePayment) throws ModuleException {
		CredentialsDTO credentialsDTO = this.getCallerCredentials(trackInfo);
		Collection<Integer> colTnxIds = ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(mapTnxIds,
				reservationContactInfo, colPaymentInfo, credentialsDTO, true, false);
		return colTnxIds;
	}

	public void invokeTestInvoiceCall(String pnr) throws ModuleException {
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.TAX_INVOICE_GENERATION);
		command.setParameter(CommandParamNames.PNR, pnr);
		command.execute();
	}

	public void saveLmsBlockCreditInfo(LmsBlockedCredit lmsBlockedCredit) throws ModuleException {
		ReservationDAOUtils.DAOInstance.RESERVATION_DAO.saveLmsBlockCreditInfo(lmsBlockedCredit);
	}

	@Override
	public ExternalChgDTO getHandlingFeeForModification(Integer operationalMode) throws ModuleException {
		CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
		return ReservationBO.getQuotedChargeForModificationHandlingFee(credentialsDTO.getAgentCode(), operationalMode);
	}

	public LmsBlockedCredit getLmsBlockCreditInfoByTmpTnxId(Integer tempTnxId) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getLmsBlockCreditInfoByTmpTnxId(tempTnxId);
	}
	
	public void updateLMSCreditUtilizationSuccess(Integer tempTnxId) throws ModuleException {
		ReservationDAOUtils.DAOInstance.RESERVATION_DAO.updateLmsBlockCreditStatus(tempTnxId, LmsBlockedCredit.SUCCESS, null);
	}
	
	public void updateLMSCreditUtilizationFailed(Integer tempTnxId) throws ModuleException {
		ReservationDAOUtils.DAOInstance.RESERVATION_DAO.updateLmsBlockCreditStatus(tempTnxId, LmsBlockedCredit.FAIL, null);
	}

	public void cancelFailedLMSBlockedCreditPayments() throws ModuleException {

		try {
			ClearLMSBlockedCreditPayments clearLMSBlockedCreditPayments = new ClearLMSBlockedCreditPayments();
			clearLMSBlockedCreditPayments.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::cancelFailedLMSBlockedCreditPayments ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::cancelFailedLMSBlockedCreditPayments ", cdaex);

			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public void cancelFailedLMSBlockedCreditPaymentsByTempIds(Collection<Integer> tempTnxIds) throws ModuleException {

		try {
			ClearLMSBlockedCreditPayments clearLMSBlockedCreditPayments = new ClearLMSBlockedCreditPayments();
			clearLMSBlockedCreditPayments.cancelBlockedLMSCredits(tempTnxIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::cancelFailedLMSBlockedCreditPaymentsByTempIds ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Collection<Integer> getLMSBlockedTnxTempPayIdsByPnr(Collection<String> pnrs) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getLMSBlockedTnxTempPayIdsByPnr(pnrs);
	}
	@Override
	public Page<BlacklisPaxReservationTO> searchBlacklistPaxResTO(BlacklisPaxReservationTO blPaxResTOSearch, Integer start,
			Integer size) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.searchBlacklistPaxResTO(blPaxResTOSearch, start, size);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode());
		}
	}

	@Override
	public void updateBlacklistReservation(BlacklisPaxReservationTO blacklistResTOSave) throws ModuleException {
		try {
			BlacklistReservation blacklistResSave = BlacklistPAXUtil.blacklistPaxResTOToBlacklistReservation(blacklistResTOSave);
			blacklistResSave = (BlacklistReservation) setUserDetails(blacklistResSave);
			ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.updateBlacklistReservation(blacklistResSave);

		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Page<BlacklistPAXRuleTO> getBlacklistPAXRules(Integer start, Integer size,
			BlacklistPAXRuleSearchTO blacklistPAXRuleSearchTO) throws ModuleException {
		try {
			Page<BlacklistPAXRule> resultPage = ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO
					.searchBlacklistPAXRules(blacklistPAXRuleSearchTO, start, size);
			Collection<BlacklistPAXRuleTO> transformedDTOs = new HashSet<BlacklistPAXRuleTO>();
			for (BlacklistPAXRule criteria : resultPage.getPageData()) {
				transformedDTOs.add(BlacklistPAXUtil.blacklistPAXRuleToBlacklistPAXRuleTO(criteria));
			}

			Page<BlacklistPAXRuleTO> resultTOPage = new Page<BlacklistPAXRuleTO>(resultPage.getTotalNoOfRecords(),
					resultPage.getStartPosition(), resultPage.getEndPosition(), transformedDTOs);

			return resultTOPage;
		} catch (Exception x) {
			throw handleException(x, true, log);
		}

	}

	@Override
	public void saveBlacklistRule(BlacklistPAXRuleTO blacklistPAXRuleTO) throws ModuleException {
		BlacklistPAXRule blacklistPAXRuleToSave = BlacklistPAXUtil.blacklistPAXRuleTOToBlacklistPAXRule(blacklistPAXRuleTO);
		blacklistPAXRuleToSave = (BlacklistPAXRule) setUserDetails(blacklistPAXRuleToSave);
		ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.saveBlacklistPAXRule(blacklistPAXRuleToSave);

	}

	@Override
	public boolean checkRuleAvailability(String strDataEle, Long ruleId) throws ModuleException {
		List<BlacklistPAXRule> blacklistPAXRules = ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.getBlacklistPAXRules();
		String[] dataEle = strDataEle.split(",");
		List<String> dataEleLIst = Arrays.asList(dataEle);
		int noOfEle = dataEleLIst.size();

		for (BlacklistPAXRule blacklistPAXRule : blacklistPAXRules) {
			List<String> saveddataEleLIst = BlacklistPAXUtil.getDataElementsList(blacklistPAXRule);
			if (Arrays.equals(dataEle, saveddataEleLIst.toArray())) {
				if (dataEleLIst.size() == noOfEle) {
					if (ruleId == null) {
						return true;
					} else if (!blacklistPAXRule.getRuleId().equals(ruleId.longValue())) {
						return true;
					}
				}
				return false;
			}
		}

		return false;
	}

	@Override
	public List<BlacklistPAX> getBalcklistedPaxReservation(List<BlacklisPaxReservationTO> paxList, boolean getOnlyFirstElement)
			throws ModuleException {

		return BlacklistedPAXCheckBL.checkForBlacklistedPAX(paxList, getOnlyFirstElement);
	}

	@Override
	public void saveBlacklistedPnrPassengers(List<BlacklistReservation> blackListPnrPassengerList) throws ModuleException {
		try {
			for (BlacklistReservation blPnrPax : blackListPnrPassengerList) {
				blPnrPax = (BlacklistReservation) setUserDetails(blPnrPax);
				if (StringUtil.isNullOrEmpty(blPnrPax.getCreatedBy())) {
					blPnrPax.setCreatedBy("web");
				}
			}
			ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.saveBlacklistedPnrPassengerList(blackListPnrPassengerList);
		} catch (Exception e) {
			throw new ModuleException(e, e.getMessage());
		}
	}

	@Override
	public Collection<Integer> getBLackListPaxIdVsPnrPaxId(String pnr) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.getBLackListPaxIdVsPnrPaxId(pnr);

	}

	/**
	 * Return the relevant reprotect flights for the given alertId
	 * 
	 * @param alertId
	 * @return reprotect flights for the given alertId
	 * @throws ModuleException
	 */
	public Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId) throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getSelfReprotectFlights(alertId);
	}

	/**
	 * This will check if segment transfer is done using IBE
	 * 
	 * @param pnr
	 * @return true if self reprotection is done
	 * @throws ModuleException
	 */
	public boolean hasActionedByIBE(String pnr) throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.hasActionedByIBE(pnr);
	}

	@Override
	public void deleteBlacklistPAX(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO) throws ModuleException {
		BlacklistPAX blacklistPAX = BlacklistPAXUtil.blacklistPAXTOToBlacklistPAX(blacklistPAXCriteriaTO);
		blacklistPAX = (BlacklistPAX) setUserDetails(blacklistPAX);
		ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.deleteBlacklistPAX(blacklistPAX);
	}

	@Override
	public boolean isBlackListPaxAlreadyInReservations(Integer blacklistedPaxId) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.isBlackListPaxAlreadyInReservations(blacklistedPaxId);
	}
	
	@Override
	public void saveAllReprotectedPassengers(Collection<ReprotectedPassenger> reprotectedPassengers) {
		ReservationDAOUtils.DAOInstance.REPROTECTED_PAX_DAO.saveAllReprotectedPassengers(reprotectedPassengers);
	}

	@Override
	public List<ReprotectedPassenger> getAllEntriesOfPaxList(List<Integer> pnrPaxIds) {
		return ReservationDAOUtils.DAOInstance.REPROTECTED_PAX_DAO.getAllEntriesOfPaxList(pnrPaxIds);
	}

	@Override
	public Page<ReprotectedPaxDTO> searchReprotectedPax(int start, int pageSize,
			ReprotectedPaxSearchrequest reprotectedPaxSearchrequest) {
		return ReservationDAOUtils.DAOInstance.REPROTECTED_PAX_DAO.searchReprotectedPax(start, pageSize,
				reprotectedPaxSearchrequest);
	}

	@Override
	public int getReprotectedPaxCount(ReprotectedPaxSearchrequest reprotectedPaxSearchrequest) {
		return ReservationDAOUtils.DAOInstance.REPROTECTED_PAX_DAO.getReprotectedPaxCount(reprotectedPaxSearchrequest);
	}

	@Override
	public void clearIssuedUnConsumedLMSCreditPayments() throws ModuleException {
		try {

			ClearIssuedUnConsumedLMSCreditPayments clearLMSBlockedCreditPayments = new ClearIssuedUnConsumedLMSCreditPayments();
			clearLMSBlockedCreditPayments.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::clearIssuedUnConsumedLMSCreditPayments ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::clearIssuedUnConsumedLMSCreditPayments ", cdaex);

			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	public void changeLMSBlockCreditStatusToCancel(String[] rewardIDs, String pnr, String memberId) throws ModuleException {

		if (rewardIDs != null) {
			Integer lmsCreditBlockedInfoId = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
					.getLmsBlockCreditInfoByRewardId(rewardIDs);
			if (lmsCreditBlockedInfoId != null) {
				LmsBlockedCredit lmsBlockedCredit = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
						.getLmsBlockCreditInfoById(lmsCreditBlockedInfoId);
				if (lmsBlockedCredit != null) {
					lmsBlockedCredit.setCreditUtilizedStatus(LmsBlockedCredit.CANCEL);
					saveLmsBlockCreditInfo(lmsBlockedCredit);
				}
			}
		}

	}

	@Override
	public void updateLMSCreditUtilizationSuccess(String[] rewardIDs) throws ModuleException {

		if (rewardIDs != null) {
			Integer lmsCreditBlockedInfoId = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
					.getLmsBlockCreditInfoByRewardId(rewardIDs);
			if (lmsCreditBlockedInfoId != null) {
				LmsBlockedCredit lmsBlockedCredit = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
						.getLmsBlockCreditInfoById(lmsCreditBlockedInfoId);
				if (lmsBlockedCredit != null) {
					lmsBlockedCredit.setCreditUtilizedStatus(LmsBlockedCredit.SUCCESS);
					saveLmsBlockCreditInfo(lmsBlockedCredit);
				}
			}
		}
	}

	@Override
	public LmsBlockedCredit getLmsBlockCreditInfoByRewardId(String[] rewardIDs) throws ModuleException {
		try {

			Integer lmsCreditBlockedInfoId = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
					.getLmsBlockCreditInfoByRewardId(rewardIDs);
			LmsBlockedCredit lmsBlockedCredit = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
					.getLmsBlockCreditInfoById(lmsCreditBlockedInfoId);
			return lmsBlockedCredit;
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) ModuleException::unabletofetchRecordByrewardid ", cdaex);

			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

    @Override
    public void updateReservationContactData(ReservationContactInfo reservationContactInfo) {
        ReservationDAOUtils.DAOInstance.RESERVATION_DAO.updateReservationContactData(reservationContactInfo);
    }

}

class ChargeCache {

	private Map<String, Integer> cache = new HashMap<String, Integer>();

	public Integer getChargeRateId(String chargeCode) throws ModuleException {
		if (!cache.containsKey(chargeCode)) {
			QuotedChargeDTO quotedChargeDTO = ReservationModuleUtils.getChargeBD().getCharge(chargeCode, null, null, null, false);
			if (chargeCode != null && quotedChargeDTO != null) {
				cache.put(chargeCode, quotedChargeDTO.getChargeRateId());
			}
		}
		return cache.get(chargeCode);
	}
}
