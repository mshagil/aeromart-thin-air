package com.isa.thinair.airreservation.core.bl.tty;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class AddSegmentMessageCreator extends TypeBReservationMessageCreator {
	
	public AddSegmentMessageCreator() {
		segmentsComposingStrategy = new SegmentsComposerForAddSegment();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}
	
	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation) throws ModuleException {
		List<SSRDetailDTO> ssrDetailsDTOs = TTYMessageCreatorUtil.composeSsrDetails(typeBRequestDTO.getSsrAssemblerMap(), reservation, typeBRequestDTO);
		if (!ssrDetailsDTOs.isEmpty()) {
			ssrDTOs.addAll(ssrDetailsDTOs);
		}
		return ssrDTOs;
	};
	
}
