/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for adjusting seats
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="adjustBlockSeats"
 */
public class AdjustBlockSeats extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(AdjustBlockSeats.class);

	/**
	 * Construct AdjustSeat
	 */
	public AdjustBlockSeats() {

	}

	/**
	 * Execute method of the AdjustSeats command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		Collection<TempSegBcAlloc> blockIds = (Collection<TempSegBcAlloc>) this.getParameter(CommandParamNames.BLOCK_KEY_IDS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Boolean triggerPnrForceConfirm = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		Integer numberOfConfirmAdultSeats = (Integer) this.getParameter(CommandParamNames.NO_OF_CONFIRM_ADULT_SEATS);
		Integer numberOfConfirmInfantSeats = (Integer) this.getParameter(CommandParamNames.NO_OF_CONFIRM_INFANT_SEATS);
		Map flightSeatIdsAndPaxTypes = (Map) this.getParameter(CommandParamNames.FLIGHT_SEAT_IDS_AND_PAX_TYPES);
		Collection[] flightMealInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_MEAL_IDS);

		Collection[] flightBaggageInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_BAGGAGE_IDS);

		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		String pnr = (String) this.getParameter(CommandParamNames.PNR);

		// Checking params
		this.validateParams(blockIds, numberOfConfirmAdultSeats, numberOfConfirmInfantSeats, triggerPnrForceConfirm,
				credentialsDTO);

		if (triggerPnrForceConfirm.booleanValue()) {
			// Move the seats to the sold state
			ReservationBO.moveBlockedSeatsToSold(blockIds, flightSeatIdsAndPaxTypes, flightMealInfo[1], flightBaggageInfo[1]);
		} else {
			// Move the seats to the sold state
			ReservationBO.moveBlockSeats(blockIds, numberOfConfirmAdultSeats, numberOfConfirmInfantSeats,
					flightSeatIdsAndPaxTypes, flightMealInfo[1], flightBaggageInfo[1]);
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param blockIds
	 * @param numberOfConfirmAdultSeats
	 * @param numberOfConfirmInfantSeats
	 * @param triggerPnrForceConfirm
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(Collection<TempSegBcAlloc> blockIds, Integer numberOfConfirmAdultSeats,
			Integer numberOfConfirmInfantSeats, Boolean triggerPnrForceConfirm, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (blockIds == null || numberOfConfirmAdultSeats == null || numberOfConfirmInfantSeats == null
				|| triggerPnrForceConfirm == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
