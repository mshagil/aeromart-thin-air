/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReinstatedCredit;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.service.RevenueAccountBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReinstatedCreditDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author indika
 * @isa.module.command name="recordReinstatedCredit"
 */
public class RecordReinstatedCredit extends DefaultBaseCommand {

	private ReinstatedCreditDAO reinstatedCreditDAO;

	PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;

	public RecordReinstatedCredit() {
		reinstatedCreditDAO = ReservationDAOUtils.DAOInstance.REINSTATED_CREDIT_DAO;
	}

	@Override
	public ServiceResponce execute() throws ModuleException {
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal adjustedAmount = (BigDecimal) this.getParameter(CommandParamNames.MODIFY_AMOUNT);
		Integer paxTxnID = (Integer) this.getParameter(CommandParamNames.PAX_TXN_ID);
		Date expiryDate = (Date) this.getParameter(CommandParamNames.EXPIRY_DATE);
		String note = (String) this.getParameter(CommandParamNames.NOTE);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Calendar oCal = Calendar.getInstance();

		// checkparams
		if (pnrPaxId == null || adjustedAmount == null || paxTxnID == null || expiryDate == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		ReinstatedCredit reinstatedCredit = new ReinstatedCredit();
		reinstatedCredit.setPaxTxnID(paxTxnID);
		reinstatedCredit.setAmount(AccelAeroCalculator.multiply(adjustedAmount, -1));
		reinstatedCredit.setPnrpaxId(pnrPaxId);
		reinstatedCredit.setDatetime(oCal.getTime());
		reinstatedCredit.setNote(note);

		reinstatedCreditDAO.saveReinstateCredit(reinstatedCredit);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		ReservationAudit reservationAudit = this.composeAudit(pnr, pnrPaxId, adjustedAmount, note, expiryDate, credentialsDTO);
		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		return response;
	}

	private ReservationAudit composeAudit(String pnr, String pnrPaxId, BigDecimal amount, String userNotes, Date expDate,
			CredentialsDTO credentialsDTO) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CREDIT_REINSTATE.getCode());
		reservationAudit.setUserNote(userNotes);

		ReservationPax reservationPax = passengerDAO.getPassenger(Integer.parseInt(pnrPaxId), false, false, false);

		RevenueAccountBD revenueAccountBD = ReservationModuleUtils.getRevenueAccountBD();
		BigDecimal balance = revenueAccountBD.getPNRPaxBalance(pnrPaxId);

		// Setting the pax name
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ReinstateCredit.PAX_NAME, reservationPax.getFirstName());

		// Setting the amount
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ReinstateCredit.PAX_REINSTATED_AMUONT, amount.abs()
				.toString());
		// Setting the passenger balance
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ReinstateCredit.PAX_NEW_BALANCE, balance == null ? "0"
				: balance.toString());
		// Setting New Expiry date
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ReinstateCredit.PAX_NEW_EXP_DATE,
				CalendarUtil.getDateInFormattedString("dd/MM/yyyy", expDate));

		return reservationAudit;
	}

}
