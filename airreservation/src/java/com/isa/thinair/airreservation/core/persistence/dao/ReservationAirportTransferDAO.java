package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;

/**
 * @author Manoj Dhanushka
 */
public interface ReservationAirportTransferDAO {

	public void saveOrUpdate(Collection<ReservationPaxSegAirportTransfer> airportTransfers);
	
	public Collection<PaxAirportTransferTO> getReservationAirportTransfersByPaxId(Collection<Integer> pnrPaxIds);
	
	public Collection<AirportTransferNotificationDetailsDTO> getAirportTransfesForNotification(String pnr, Collection<Integer> pnrSegIds, Collection<Integer> paxSegAptIds);
	
	public void cancelAirportTransfer(Collection<Integer> pnrPaxSegAirportTransferIds);
	
	public Collection<ReservationPaxSegAirportTransfer> getReservationAirportTransfers(Collection<Integer> pnrPaxSegAirportTransferIds);	
}
