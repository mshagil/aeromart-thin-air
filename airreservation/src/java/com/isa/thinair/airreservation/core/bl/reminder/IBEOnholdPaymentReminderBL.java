/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.reminder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.DateUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.OnholdAlert;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.util.I18NMessagingUtil;
import com.isa.thinair.airreservation.core.util.ReservationReminderUtil;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

/**
 * @author nafly
 *
 */
public class IBEOnholdPaymentReminderBL {

	private final Log log = LogFactory.getLog(IBEOnholdPaymentReminderBL.class);
	private final MessagingServiceBD messagingServiceBD;
	private final ReservationDAO reservationDAO;

	public IBEOnholdPaymentReminderBL() {
		this.messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
		this.reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
	}


	/**
	 * 
	 * @param lccClientReservation
	 * @param credentialsDTO
	 * @param isCreateBooking TODO
	 * @throws ModuleException
	 */
	public void sendPaymentReminder(LCCClientReservation lccClientReservation, CredentialsDTO credentialsDTO, boolean isCreateBooking)
			throws ModuleException {

		if (lccClientReservation != null && credentialsDTO != null) {
			CommonReservationContactInfo contactInfo = lccClientReservation.getContactInfo();

			if (contactInfo != null && contactInfo.getEmail() != null) {

				UserMessaging userMessaging = new UserMessaging();
				userMessaging.setFirstName(contactInfo.getFirstName());
				userMessaging.setLastName(contactInfo.getLastName());
				userMessaging.setToAddres(contactInfo.getEmail());

				List<UserMessaging> messages = new ArrayList<UserMessaging>();
				messages.add(userMessaging);

				HashMap<String, Object> onHoldEmailDataMap = getOnHoldEmailDataMap(lccClientReservation);

				String preferredLanguage = lccClientReservation.getContactInfo().getPreferredLanguage();

				String direction = "ltr";
				if (preferredLanguage.equals("ar")) {
					direction = "rtl";
				}
				onHoldEmailDataMap.put("direction", direction);

				Locale locale = new Locale(ReservationReminderUtil.getPrefLanguage(lccClientReservation));
				Map<String, String> labels = I18NMessagingUtil.getIBEOnHoldPaymentReminderEmailMessages(locale);
				onHoldEmailDataMap.put("label", labels);

				Topic topic = new Topic();
				topic.setTopicParams(onHoldEmailDataMap);
				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.IBE_ONHOLD_PAYMENT_REMINDER);
				topic.setAttachMessageBody(false);

				List<ReservationAudit> audits = new ArrayList<ReservationAudit>();

				ReservationAudit reservationAudit = new ReservationAudit();
				reservationAudit.setModificationType(AuditTemplateEnum.OHD_PAY_REMINDER.getCode());
				ReservationAudit.createReservationAudit(reservationAudit, lccClientReservation.getPNR(), null, credentialsDTO);
				reservationAudit
						.addContentMap(AuditTemplateEnum.TemplateParams.OnholdAlert.EMAIL_ADDRESS, contactInfo.getEmail());

				audits.add(reservationAudit);
				topic.setAuditInfo(audits);

				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				// Record Alert only for reminders,not for create bookings
				if (!isCreateBooking) {
					OnholdAlert onholdAlert = null;
					onholdAlert = reservationDAO.getOnholdAlert(lccClientReservation.getPNR());
					if (onholdAlert == null) {
						onholdAlert = new OnholdAlert();
						onholdAlert.setPnr(lccClientReservation.getPNR());
						if (lccClientReservation.getAdminInfo() != null
								&& lccClientReservation.getAdminInfo().getOwnerChannelId() != null
								&& !lccClientReservation.getAdminInfo().getOwnerChannelId().equals("")) {
							onholdAlert.setChannelCode(new Integer(ReservationReminderUtil.getBookingChannel(lccClientReservation
									.getAdminInfo().getOwnerChannelId())));
						}
						onholdAlert.setAlertSent(ReservationInternalConstants.OnholdAlertStatus.ATTEMPTED);
						onholdAlert.setAlertType(ReservationInternalConstants.OHDAlertTypes.PAYMENT_REMINDER.code());
						onholdAlert.setSentTimeStamp(new Date());
					} else {
						onholdAlert.setSentTimeStamp(new Date());
					}
					reservationDAO.saveOnholdAlert(onholdAlert);
				}
				messagingServiceBD.sendMessage(messageProfile);
			}
		}
	}

	private HashMap<String, Object> getOnHoldEmailDataMap(LCCClientReservation reservation) throws ModuleException {

		String ibeURL = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL);
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pnr", reservation.getPNR());

		String station = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		Date releaseTimeLocal = DateUtil.getLocalDateTime(reservation.getZuluReleaseTimeStamp(), station);
		dataMap.put("releaseTime", CalendarUtil.formatDate(releaseTimeLocal, "dd-MMM-yyyy HH:mm"));
		// dataMap.put("destination", ReservationReminderUtil.getFinalDestination(reservation.getSegments()));

		CommonReservationContactInfo contactInfo = reservation.getContactInfo();
		dataMap.put("customerName",
				(new StringBuilder()).append(contactInfo.getTitle()).append(" ").append(contactInfo.getFirstName()).append(" ")
						.append(contactInfo.getLastName()).toString());

		dataMap.put("ibeURL", ibeURL);
		dataMap.put("logoImageName", ReservationReminderUtil.getLogoImageName(AppSysParamsUtil.getCarrierCode()));

		String strCarrier = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CARRIER_NAME);
		dataMap.put("airlineName", strCarrier);

		String currency = AppSysParamsUtil.getBaseCurrency();

		String totalBalance = AccelAeroCalculator.formatAsDecimal(reservation.getTotalChargeAmount());
		dataMap.put("totalBalance", currency + " " + totalBalance);

		String totalFare = AccelAeroCalculator.formatAsDecimal(reservation.getTotalTicketFare());
		dataMap.put("totalFare", currency + " " + totalFare);

		String totalCharges = AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(reservation.getTotalTicketTaxCharge(),
				reservation.getTotalTicketSurCharge(), reservation.getTotalTicketCancelCharge(),
				reservation.getTotalTicketModificationCharge(), reservation.getTotalTicketAdjustmentCharge()));

		dataMap.put("totalCharges", currency + " " + totalCharges);

		String callCenterNo = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO);
		dataMap.put("callCenterNumber", callCenterNo);

		String companyEmail = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.COMP_EMAIL);
		dataMap.put("companyEmail", companyEmail);

		dataMap.put("partner_url", AppSysParamsUtil.getPaymentPartnerURL());

		return dataMap;
	}

}
