package com.isa.thinair.airreservation.core.bl.common;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.FlightSegReminderNotificationDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.NotificationDetailInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.dto.AncillaryReminderAuditDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.constants.PlatformConstants;

public class AncillaryReminderBL {
	
	private static String ANCI_REMINDER_VM_TEMPLATE ="/templates/email/ancillary_reminder_email_";
	private static String ANCI_REMINDER_VM_TEMPLATE_FILE_TYPE = ".xml.vm";
	private static HashMap<String, Boolean> AnciReminderLangTemplateAvailability =  new HashMap<String, Boolean>();
	private static final Locale defaultLocale = new Locale("en");

	public static void
			emailAncillaryReminder(List<NotificationDetailInfoDTO> pnrListToSendReminder, CredentialsDTO credentialsDTO)
					throws ModuleException {

		List<MessageProfile> messageProfileList = new ArrayList<MessageProfile>();
		for (NotificationDetailInfoDTO notificationDetailInfoDTO : pnrListToSendReminder) {

			String emailAddress = BeanUtils.nullHandler(notificationDetailInfoDTO.getEmail());

			if (emailAddress.length() > 0) {
				UserMessaging userMessaging = new UserMessaging();
				userMessaging.setFirstName(notificationDetailInfoDTO.getName());
				userMessaging.setLastName("");
				userMessaging.setToAddres(emailAddress);

				Locale locale = null;
				if (notificationDetailInfoDTO.getIteneraryLanguage() != null &&
						checkAnciReminderVMTemplateExsistance(new Locale(notificationDetailInfoDTO.getIteneraryLanguage().trim()))) {
					locale = new Locale(notificationDetailInfoDTO.getIteneraryLanguage().trim());
				} else {
					locale = defaultLocale;
				}

				List<UserMessaging> messages = new ArrayList<UserMessaging>();
				messages.add(userMessaging);
				HashMap<String, NotificationDetailInfoDTO> ancillaryReminderDataMap = (HashMap<String, NotificationDetailInfoDTO>) getAncillaryReminderDataMap(notificationDetailInfoDTO);

				// Topic
				Topic topic = new Topic();
				topic.setTopicParams(ancillaryReminderDataMap);
				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.ANCILLARY_REMINDER_EMAIL);
				topic.setLocale(locale);
				topic.setAttachMessageBody(true);
				topic.setAuditInfo(composeAudit(notificationDetailInfoDTO, credentialsDTO));

				// User Profile
				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				messageProfileList.add(messageProfile);
			}
		}

		// Send the list of msaages
		if (messageProfileList != null && messageProfileList.size() > 0) {
			ReservationModuleUtils.getMessagingServiceBD().sendMessages(messageProfileList);
		}

	}

	private static Map<String, NotificationDetailInfoDTO> getAncillaryReminderDataMap(NotificationDetailInfoDTO detailInfoDTO) {
		Map<String, NotificationDetailInfoDTO> ancillaryDataMap = new HashMap<String, NotificationDetailInfoDTO>();

		ancillaryDataMap.put("notificationDetailDto", detailInfoDTO);

		return ancillaryDataMap;
	}

	private static Collection<AncillaryReminderAuditDTO> composeAudit(NotificationDetailInfoDTO notificationDetailInfoDTO,
			CredentialsDTO credentialsDTO) throws ModuleException {
		AncillaryReminderAuditDTO ancillaryReminderAuditDTO = new AncillaryReminderAuditDTO();
		ancillaryReminderAuditDTO.setPnr(notificationDetailInfoDTO.getPnr());
		ancillaryReminderAuditDTO.setFlightSegmentId(notificationDetailInfoDTO.getFlightSegmentId());
		ancillaryReminderAuditDTO.setFlightSegNotificationId(notificationDetailInfoDTO.getFlightSegNotificationEventId());
		ancillaryReminderAuditDTO.setQrtzJobName(notificationDetailInfoDTO.getQrtzJobName());
		ancillaryReminderAuditDTO.setQrtzJobGroupName(notificationDetailInfoDTO.getQrtzJobGroupName());
		ancillaryReminderAuditDTO.setUserId(credentialsDTO.getUserId());
		ancillaryReminderAuditDTO.getFlightSegNotifyDTOList().addAll(notificationDetailInfoDTO.getFlightSegmentsList());

		Collection<AncillaryReminderAuditDTO> colAncillaryReminderAudit = new ArrayList<AncillaryReminderAuditDTO>();
		colAncillaryReminderAudit.add(ancillaryReminderAuditDTO);

		return colAncillaryReminderAudit;
	}

	public static boolean checkInsuranceStatus(NotificationDetailInfoDTO notificationDetailInfoDTO) throws ModuleException {
		CommonMasterBD commonMasterBD = ReservationModuleUtils.getCommonMasterBD();
		return AppSysParamsUtil.isShowTravelInsurance()
				&& commonMasterBD.isAIGCountryCodeActive(getFirstDepartureAirportCode(notificationDetailInfoDTO
						.getFlightSegmentsList()));
	}

	private static String getFirstDepartureAirportCode(List<FlightSegReminderNotificationDTO> pnrSegmentsList) {
		return pnrSegmentsList.get(0).getSegmentCode().substring(0, 3);
	}

	public static void getSegmentsDetails(List<FlightSegReminderNotificationDTO> flightSegmentsList) throws ModuleException {
		Collection<String> airportsCodes = new ArrayList<String>();
		AirportBD airportBD = ReservationModuleUtils.getAirportBD();
		Map<String, CachedAirportDTO> mapAirportCodes = null;

		for (FlightSegReminderNotificationDTO flightSegNotifyDTO : flightSegmentsList) {
			airportsCodes.addAll(Arrays.asList(flightSegNotifyDTO.getSegmentCode().split("/")));
		}

		mapAirportCodes = airportBD.getCachedOwnAirportMap(airportsCodes);
		for (FlightSegReminderNotificationDTO flightSegNotify : flightSegmentsList) {
			String[] arrSegmets = flightSegNotify.getSegmentCode().split("/");
			String composedSegment = "";

			for (int index = 0; index < arrSegmets.length; index++) {
				CachedAirportDTO airport = mapAirportCodes.get(arrSegmets[index]);

				if (index != 0) {
					composedSegment += " / ";
				}
				composedSegment += airport.getAirportName();
			}
			flightSegNotify.setSegmentDetails(composedSegment);
		}
	}

	/**
	 * Fill the check-in times for the FlightSegReminderNotificationDTO
	 * 
	 * @param flightSegmentsList
	 *            : The flight segment notification list.
	 * @throws ModuleException
	 */
	public static void getCheckInTimes(List<FlightSegReminderNotificationDTO> flightSegmentsList) throws ModuleException {

		for (FlightSegReminderNotificationDTO flightSegmentNotification : flightSegmentsList) {
			String[] segmentAirports = flightSegmentNotification.getSegmentCode().split("/");
			String departureAirportCode = null;
			if (segmentAirports.length > 0) {
				departureAirportCode = segmentAirports[0];
			}

			flightSegmentNotification.setCheckInDate(ReservationApiUtils.getCheckInDate(
					flightSegmentNotification.getDepartureDateTime(), departureAirportCode,
					flightSegmentNotification.getFlightNumber()));
		}
	}

	public static long[] deprtureRemainingDays(Date dateFrom, Date dateTo) {

		long[] dayHourArr = new long[2];

		long difInMil = dateFrom.getTime() - dateTo.getTime();
		long milPerDay = 1000 * 60 * 60 * 24;
		long milPerHour = 1000 * 60 * 60;
		// long milPerMin = 1000 * 60;
		// long milPerSec = 1000;

		long days = difInMil / milPerDay;
		long hour = (difInMil - days * milPerDay) / milPerHour;
		// long min = (difInMil - days * milPerDay - hour * milPerHour) / milPerMin;
		// long sec = (difInMil - days * milPerDay - hour * milPerHour - min * milPerMin) / milPerSec;
		// long mil = (difInMil - days * milPerDay - hour * milPerHour - min * milPerMin - sec * milPerSec);

		dayHourArr[0] = days;
		dayHourArr[1] = hour;

		return dayHourArr;
	}

	private static Map<String, String> getAncillaryAppoxPriceMap() {
		return ReservationModuleUtils.getAirReservationConfig().getAncillaryAppoxPrice();
	}

	public static void setSearAndInsuranceAmountToDisplay(NotificationDetailInfoDTO detailInfoDTO) {
		String seatAppoxPrice = (String) getAncillaryAppoxPriceMap().get("seat");
		String insuranceAppoxPrice = (String) getAncillaryAppoxPriceMap().get("insurance");
		String baseCurrency = AppSysParamsUtil.getBaseCurrency();

		String seatReminderQuote = baseCurrency.toUpperCase() + " " + seatAppoxPrice;
		String insuranceReminderQuote = baseCurrency.toUpperCase() + " " + insuranceAppoxPrice;

		detailInfoDTO.setSeatApproxAmount(seatReminderQuote);
		detailInfoDTO.setInsuranceApproxAmount(insuranceReminderQuote);
	}

	public static String getOndCode(Reservation reservation) throws ModuleException {
		Collection<ReservationSegmentDTO> segmentDTOs = reservation.getSegmentsView();
		String ondCode = null;

		for (Iterator<ReservationSegmentDTO> itSegmentDTOs = segmentDTOs.iterator(); itSegmentDTOs.hasNext();) {
			ReservationSegmentDTO segmentDTO = (ReservationSegmentDTO) itSegmentDTOs.next();
			String segCode = segmentDTO.getSegmentCode();

			if (itSegmentDTOs.hasNext()) {
				ReservationSegmentDTO nextSegmentDTO = (ReservationSegmentDTO) itSegmentDTOs.next();
				String nextSegCode = nextSegmentDTO.getSegmentCode();

				if (ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE.equals(nextSegmentDTO.getReturnFlag())
						&& segCode.substring(4).equals(nextSegCode.subSequence(0, 3))) {
					ondCode = segCode.substring(0, 3) + "/" + nextSegCode.substring(4);
				} else {
					ondCode = segCode;
				}
			} else {
				ondCode = segCode;
			}

			break;
		}

		return ondCode;
	}

	private static boolean checkAnciReminderVMTemplateExsistance(Locale locale){
		if(!AnciReminderLangTemplateAvailability.containsKey(locale.getLanguage().toUpperCase())){
			String configAbsPath =  PlatformConstants.getConfigRootAbsPath();
			String filePath =  configAbsPath + ANCI_REMINDER_VM_TEMPLATE + locale.getLanguage().toUpperCase() + ANCI_REMINDER_VM_TEMPLATE_FILE_TYPE;
			File fileAnciRemVM =  new File(filePath);
			AnciReminderLangTemplateAvailability.put(locale.getLanguage().toUpperCase(), fileAnciRemVM.exists());
		}
		return AnciReminderLangTemplateAvailability.get(locale.getLanguage().toUpperCase());
	}
}
