/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.core.persistence.dao.LccDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * @author Nilindra Fernando
 * @isa.module.dao-impl dao-name="LccDAO"
 */
public class LccDAOImpl extends HibernateDaoSupport implements LccDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(LccDAOImpl.class);

	private static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	public Long getNextLccReservationPaymentTnxId() {
		log.debug("Inside getNextPnrNumber");
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		// Final SQL
		String sql = " SELECT LCC_S_TEMP_PAYMENT_TNX.NEXTVAL TNX_ID FROM DUAL ";

		String transactionId = (String) jdbcTemplate.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				String transactionId = "";
				if (resultSet != null) {
					if (resultSet.next()) {
						transactionId = BeanUtils.nullHandler(resultSet.getString("TNX_ID"));
					}
				}
				return transactionId;
			}
		});

		log.debug("Exit getNextPnrNumber");
		return new Long(transactionId);
	}

}
