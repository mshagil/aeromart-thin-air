package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Map;

import com.isa.thinair.airreservation.api.model.NSRefundReservation;

/**
 * NSRefundReservationDAO is the business DAO interface for the NSRefundReservation service apis
 * 
 * @author Jagath
 * @since 1.0
 */

public interface NSRefundReservationDAO {

	/**
	 * @return List of PNR
	 */
	public Map<String, NSRefundReservation> getNSRefundHoldPNRs();

	/**
	 * @param nsRefundReservation
	 */
	public void saveNSRefundReservation(NSRefundReservation nsRefundReservation);

	/**
	 * @param pnr
	 * @return
	 */
	public boolean hasUnprocessedNSReservation(String pnr);
}