package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ModificationTypeWiseSegsResponse {
	
	public ModificationTypeWiseSegsResponse(List<Integer> addSegIds, List<Integer> delSegIds  ,List<Integer> chgSegIds){
		this.addSegIds = addSegIds;
		this.delSegIds = delSegIds;
		this.chgSegIds = chgSegIds;
	}
	

	List<Integer> addSegIds;

	List<Integer> delSegIds;

	List<Integer> chgSegIds;

	public List<Integer> getAddSegIds() {
		if (addSegIds == null) {
			addSegIds = new ArrayList<Integer>();
		}
		return addSegIds;
	}

	public void addToAddSegIds(Integer SegId) {
		getAddSegIds().add(SegId);
	}

	public void addToAddSegIds(Collection SegIds) {
		getAddSegIds().addAll(SegIds);
	}

	public List<Integer> getDelSegIds() {
		if (delSegIds == null) {
			delSegIds = new ArrayList<Integer>();
		}
		return delSegIds;
	}

	public void addToDelSegIds(Integer SegId) {
		getDelSegIds().add(SegId);
	}

	public void addToDelSegIds(Collection SegIds) {
		getDelSegIds().addAll(SegIds);
	}

	public List<Integer> getChgSegIds() {
		if (chgSegIds == null) {
			chgSegIds = new ArrayList<Integer>();
		}
		return chgSegIds;
	}

	public void addToChgSegIds(Integer SegId) {
		getChgSegIds().add(SegId);
	}

	public void addToChgSegIds(Collection SegIds) {
		getChgSegIds().addAll(SegIds);
	}

}
