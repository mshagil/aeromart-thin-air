package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.PerPaxPriceBreakdown;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationCreditTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO.ReservationPaxChargeMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationPaxPaymentCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author Nilindra Fernando
 * 
 * @since August 13, 2010
 */
public class TnxGranularityUtils {

	public static Collection<ReservationPaxChargeMetaTO> getChargeMetaTOByChargeGroupCode(String chargeGroupCode,
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO) {
		Collection<ReservationPaxChargeMetaTO> selectedReservationPaxOndMetaTOs = new ArrayList<ReservationPaxChargeMetaTO>();

		if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {
			for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : colReservationPaxChargeMetaTO) {
				if (chargeGroupCode.equals(BeanUtils.nullHandler(reservationPaxChargeMetaTO.getChargeGroupCode()))) {
					selectedReservationPaxOndMetaTOs.add(reservationPaxChargeMetaTO);
				}
			}
		}

		return selectedReservationPaxOndMetaTOs;
	}

	private static Map<String, Collection<ReservationPaxOndPayment>> getPaxOndPaymentByChargeGroup(
			Collection<ReservationPaxOndPayment> colReservationPaxChargeMetaTO) {
		Map<String, Collection<ReservationPaxOndPayment>> chargeGroupWiseOndPayments = new HashMap<String, Collection<ReservationPaxOndPayment>>();
		if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {
			for (ReservationPaxOndPayment paxOndPayment : colReservationPaxChargeMetaTO) {
				String chargeGroupCode = BeanUtils.nullHandler(paxOndPayment.getChargeGroupCode());
				if (!chargeGroupWiseOndPayments.containsKey(chargeGroupCode)) {
					chargeGroupWiseOndPayments.put(chargeGroupCode, new ArrayList<ReservationPaxOndPayment>());
				}
				chargeGroupWiseOndPayments.get(chargeGroupCode).add(paxOndPayment);
			}
		}

		return chargeGroupWiseOndPayments;
	}

	protected static PerPaxPriceBreakdown composePerPaxPriceBreakdown(
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {

		BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal taxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal surAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal otherAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
			Collection<String> orderedChargeGroups = TnxGranularityRules.getPaymentChargeGroupFulFillmentOrder();
			Map<String, Collection<ReservationPaxOndPayment>> chargeGroupWiseOndPayments = getPaxOndPaymentByChargeGroup(colReservationPaxOndPayment);

			for (String chargeGroupCode : orderedChargeGroups) {
				if (chargeGroupWiseOndPayments.containsKey(chargeGroupCode)) {
					for (ReservationPaxOndPayment reservationPaxOndPayment : chargeGroupWiseOndPayments.get(chargeGroupCode)) {

						if (chargeGroupCode.equals(ReservationInternalConstants.ChargeGroup.FAR)) {
							fareAmount = AccelAeroCalculator.add(fareAmount, reservationPaxOndPayment.getAmount());
						} else if (chargeGroupCode.equals(ReservationInternalConstants.ChargeGroup.TAX)) {
							taxAmount = AccelAeroCalculator.add(taxAmount, reservationPaxOndPayment.getAmount());
						} else if (chargeGroupCode.equals(ReservationInternalConstants.ChargeGroup.SUR)
								|| chargeGroupCode.equals(ReservationInternalConstants.ChargeGroup.INF)) {
							surAmount = AccelAeroCalculator.add(surAmount, reservationPaxOndPayment.getAmount());
						} else {
							otherAmount = AccelAeroCalculator.add(otherAmount, reservationPaxOndPayment.getAmount());
						}
					}
				}
			}

		}

		return new PerPaxPriceBreakdown(fareAmount, taxAmount, surAmount, otherAmount);
	}

	public static Collection<ReservationPaxOndPayment> getApplicableChargeTypes(String chargeGroupCode,
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		Collection<ReservationPaxOndPayment> colApplicableReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
			if (BeanUtils.nullHandler(reservationPaxOndPayment.getChargeGroupCode()).equals(chargeGroupCode)) {
				colApplicableReservationPaxOndPayment.add(reservationPaxOndPayment);
			}
		}

		return colApplicableReservationPaxOndPayment;
	}

	public static Collection<ReservationPaxOndPayment> getPaymentMetaInfo(
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		List<Integer> nominalCodes = ReservationTnxNominalCode.getPaymentTypeNominalCodes();
		for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
			if (reservationPaxOndPayment.getNominalCode().equals(ReservationTnxNominalCode.CREDIT_ADJUST.getCode())) {
				nominalCodes.addAll(ReservationTnxNominalCode.getAdjModCnxNominalCodes());
				break;
			}
		}
		return getFilteredMetaInfo(colReservationPaxOndPayment, nominalCodes);
	}

	public static Collection<ReservationPaxOndPayment> getRefundMetaInfo(
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		return getFilteredMetaInfo(colReservationPaxOndPayment, ReservationTnxNominalCode.getRefundTypeNominalCodes());
	}

	protected static Collection<ReservationPaxOndPayment> getFilteredMetaInfo(
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment, List<Integer> nominalCodes) {
		Collection<ReservationPaxOndPayment> selectedReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
			if (nominalCodes.contains(reservationPaxOndPayment.getNominalCode())) {
				selectedReservationPaxOndPayment.add(reservationPaxOndPayment);
			}
		}

		return selectedReservationPaxOndPayment;
	}

	public static Map<Long, BigDecimal> getOndChargeWiseAmounts(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		Map<Long, BigDecimal> ondChargeWiseAmounts = new HashMap<Long, BigDecimal>();

		for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
			BigDecimal amount = ondChargeWiseAmounts.get(reservationPaxOndPayment.getPnrPaxOndChgId());

			if (amount == null) {
				amount = AccelAeroCalculator.getDefaultBigDecimalZero();
			}

			ondChargeWiseAmounts.put(reservationPaxOndPayment.getPnrPaxOndChgId(),
					AccelAeroCalculator.add(amount, reservationPaxOndPayment.getAmount()));
		}

		return ondChargeWiseAmounts;
	}

	protected static Collection<ReservationPaxOndPayment> getOndPaymentsForCharge(
			Collection<ReservationPaxOndPayment> colPayments, Long pnrPaxOndChgId) {

		Collection<ReservationPaxOndPayment> colPaymentsForCharge = new ArrayList<ReservationPaxOndPayment>();
		for (ReservationPaxOndPayment reservationPaxOndPayment : colPayments) {
			if (reservationPaxOndPayment.getPnrPaxOndChgId().equals(pnrPaxOndChgId)) {
				colPaymentsForCharge.add(reservationPaxOndPayment);

			}
		}
		return colPaymentsForCharge;
	}

	public static Collection<Long> getPaymentIds(Collection<ReservationCredit> colReservationCredit) {
		Collection<Long> colPaymentTnxIds = new ArrayList<Long>();

		for (ReservationCredit reservationCredit : colReservationCredit) {
			colPaymentTnxIds.add(reservationCredit.getTnxId());
		}

		return colPaymentTnxIds;
	}

	public static BigDecimal getTotalAmount(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
			if (!ReservationTnxNominalCode.getRefundTypeNominalCodes().contains(reservationPaxOndPayment.getNominalCode())) {
				totalAmount = AccelAeroCalculator.add(totalAmount, reservationPaxOndPayment.getAmount());
			}

		}

		return totalAmount;
	}

	protected static Collection<ReservationPaxOndPayment> getReservationPaxOndPayments(
			Map<Long, Collection<ReservationPaxOndPayment>> mapPaxOndPayments) {

		Collection<ReservationPaxOndPayment> composedPayments = new ArrayList<ReservationPaxOndPayment>();

		for (Collection<ReservationPaxOndPayment> colReservationPaxOndPayment : mapPaxOndPayments.values()) {
			composedPayments.addAll(colReservationPaxOndPayment);
		}

		return composedPayments;
	}

	/**
	 * Get the payment breakdown for new charge. For instance if a particular charge is split among two payments, and
	 * later that charge is refunded , then the records which will be recored to that refund charge should also split
	 * among the payments. This method does that job of prorating the charge among the payments.
	 * 
	 * @param colReservationPaxOndPayment
	 * @param amount
	 * @return
	 */
	public static Collection<ReservationPaxOndPayment> getRespectiveReservationPaxOndPayment(
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment, BigDecimal amount) {

		Collection<ReservationPaxOndPayment> finalPayments = new ArrayList<ReservationPaxOndPayment>();

		if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
			BigDecimal totalAmount = getTotalAmount(colReservationPaxOndPayment);

			// If valid payments exists only we need track the refundables
			if (totalAmount.doubleValue() > 0) {
				BigDecimal calculatingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				int size = colReservationPaxOndPayment.size();

				int i = 1;

				for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
					Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
					reservationPaxOndPayment = reservationPaxOndPayment.clone();
					reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);

					if (i == size) {
						reservationPaxOndPayment.setAmount(AccelAeroCalculator.subtract(amount, calculatingAmount));
					} else {
						reservationPaxOndPayment.setAmount(AccelAeroCalculator.divide(
								AccelAeroCalculator.multiply(reservationPaxOndPayment.getAmount(), amount), totalAmount));

						calculatingAmount = AccelAeroCalculator.add(calculatingAmount, reservationPaxOndPayment.getAmount());
					}

					finalPayments.add(reservationPaxOndPayment);
					i++;
				}
			}
		}

		return finalPayments;
	}

	protected static PerPaxPriceBreakdown composePerPaxPriceBreakdown(ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO) {

		BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal taxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal surAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal otherAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (ReservationPaxOndCharge reservationPaxOndCharge : reservationPaxPaymentMetaTO.getColPerPaxWiseOndNewCharges()) {
			String chargeGroupCode = BeanUtils.nullHandler(reservationPaxOndCharge.getChargeGroupCode());

			if (chargeGroupCode.equals(ReservationInternalConstants.ChargeGroup.FAR)) {
				fareAmount = AccelAeroCalculator.add(fareAmount, reservationPaxOndCharge.getEffectiveAmount());
			} else if (chargeGroupCode.equals(ReservationInternalConstants.ChargeGroup.TAX)) {
				taxAmount = AccelAeroCalculator.add(taxAmount, reservationPaxOndCharge.getEffectiveAmount());
			} else if (chargeGroupCode.equals(ReservationInternalConstants.ChargeGroup.SUR)
					|| chargeGroupCode.equals(ReservationInternalConstants.ChargeGroup.INF)) {
				surAmount = AccelAeroCalculator.add(surAmount, reservationPaxOndCharge.getEffectiveAmount());
			} else {
				otherAmount = AccelAeroCalculator.add(otherAmount, reservationPaxOndCharge.getEffectiveAmount());
			}
		}

		if (reservationPaxPaymentMetaTO.getColPerPaxWiseOndNewCharges().size() > 0) {
			return new PerPaxPriceBreakdown(fareAmount, taxAmount, surAmount, otherAmount);
		} else {
			return null;
		}
	}
		
	private static BigDecimal lendCreditFromOtherGroups(ReservationCreditTO balanceAmountTO,
			ReservationCreditTO maxCreditAmountTO, BigDecimal needed, String chargeGroup) {

		BigDecimal consumedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalConsumedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (balanceAmountTO.getTotalCredit().doubleValue() > 0 && needed.doubleValue() > 0) {

			BigDecimal availableFare = balanceAmountTO.getFareAmount();
			BigDecimal availableTax = balanceAmountTO.getTaxAmount();
			BigDecimal availableSur = balanceAmountTO.getSurchargeAmount();
			BigDecimal availableOther = balanceAmountTO.getOtherAmount();

			if (availableFare.doubleValue() > 0) {

				consumedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (availableFare.doubleValue() >= needed.doubleValue()) {
					availableFare = AccelAeroCalculator.subtract(availableFare, needed);
					consumedAmount = needed;

					needed = AccelAeroCalculator.getDefaultBigDecimalZero();
				} else if (availableFare.doubleValue() < needed.doubleValue()) {
					needed = AccelAeroCalculator.subtract(needed, availableFare);
					consumedAmount = availableFare;

					availableFare = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				maxCreditAmountTO.setFareAmount(AccelAeroCalculator.subtract(maxCreditAmountTO.getFareAmount(), consumedAmount));

				totalConsumedAmount = AccelAeroCalculator.add(totalConsumedAmount, consumedAmount);
				balanceAmountTO.setFareAmount(availableFare);
				maxCreditAmountTO.addCreditConsumedGroupNew(chargeGroup, ChargeGroup.FAR, consumedAmount);

			}

			if (availableTax.doubleValue() > 0 && needed.doubleValue() > 0) {

				consumedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (availableTax.doubleValue() >= needed.doubleValue()) {
					availableTax = AccelAeroCalculator.subtract(availableTax, needed);
					consumedAmount = needed;

					needed = AccelAeroCalculator.getDefaultBigDecimalZero();
				} else if (availableTax.doubleValue() < needed.doubleValue()) {
					needed = AccelAeroCalculator.subtract(needed, availableTax);
					consumedAmount = availableTax;

					availableTax = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				maxCreditAmountTO.setTaxAmount(AccelAeroCalculator.subtract(maxCreditAmountTO.getTaxAmount(), consumedAmount));

				totalConsumedAmount = AccelAeroCalculator.add(totalConsumedAmount, consumedAmount);
				balanceAmountTO.setTaxAmount(availableTax);
				maxCreditAmountTO.addCreditConsumedGroupNew(chargeGroup, ChargeGroup.TAX, consumedAmount);

			}

			if (availableSur.doubleValue() > 0 && needed.doubleValue() > 0) {
				consumedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (availableSur.doubleValue() >= needed.doubleValue()) {
					availableSur = AccelAeroCalculator.subtract(availableSur, needed);
					consumedAmount = needed;

					needed = AccelAeroCalculator.getDefaultBigDecimalZero();
				} else if (availableSur.doubleValue() < needed.doubleValue()) {
					needed = AccelAeroCalculator.subtract(needed, availableSur);
					consumedAmount = availableSur;

					availableSur = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				maxCreditAmountTO.setSurchargeAmount(AccelAeroCalculator.subtract(maxCreditAmountTO.getSurchargeAmount(),
						consumedAmount));

				totalConsumedAmount = AccelAeroCalculator.add(totalConsumedAmount, consumedAmount);
				balanceAmountTO.setSurchargeAmount(availableSur);
				maxCreditAmountTO.addCreditConsumedGroupNew(chargeGroup, ChargeGroup.SUR, consumedAmount);

			}

			if (availableOther.doubleValue() > 0 && needed.doubleValue() > 0) {
				consumedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (availableOther.doubleValue() >= needed.doubleValue()) {
					availableOther = AccelAeroCalculator.subtract(availableOther, needed);
					consumedAmount = needed;

					needed = AccelAeroCalculator.getDefaultBigDecimalZero();
				} else if (availableOther.doubleValue() < needed.doubleValue()) {
					needed = AccelAeroCalculator.subtract(needed, availableOther);
					consumedAmount = availableOther;

					availableOther = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				maxCreditAmountTO
						.setOtherAmount(AccelAeroCalculator.subtract(maxCreditAmountTO.getOtherAmount(), consumedAmount));

				totalConsumedAmount = AccelAeroCalculator.add(totalConsumedAmount, consumedAmount);
				balanceAmountTO.setOtherAmount(availableOther);
				maxCreditAmountTO.addCreditConsumedGroupNew(chargeGroup, ChargeGroup.ANY, consumedAmount);
			}

		}

		return totalConsumedAmount;
	}

	/**
	 * function to identify optimized value for each main charge groups[FAR,TAX,SUR] with against available credit.
	 * */
	public static ReservationCreditTO optimizedCreditToUtilizeFromAvailableCredit(ReservationCreditTO availableAmountTO,
			ReservationCreditTO requiredAmountTO) {

		BigDecimal availableFare = availableAmountTO.getFareAmount();
		BigDecimal availableTax = availableAmountTO.getTaxAmount();
		BigDecimal availableSur = availableAmountTO.getSurchargeAmount();
		BigDecimal availableOther = availableAmountTO.getOtherAmount();

		BigDecimal balanceFare = AccelAeroCalculator.subtract(availableFare, requiredAmountTO.getFareAmount());
		BigDecimal balanceTax = AccelAeroCalculator.subtract(availableTax, requiredAmountTO.getTaxAmount());
		BigDecimal balanceSur = AccelAeroCalculator.subtract(availableSur, requiredAmountTO.getSurchargeAmount());
		BigDecimal balanceOther = AccelAeroCalculator.subtract(availableOther, requiredAmountTO.getOtherAmount());

		ReservationCreditTO balanceAmountTO = new ReservationCreditTO();

		if (balanceFare.doubleValue() > 0) {
			balanceAmountTO.setFareAmount(balanceFare);
		}

		if (balanceTax.doubleValue() > 0) {
			balanceAmountTO.setTaxAmount(balanceTax);
		}

		if (balanceSur.doubleValue() > 0) {
			balanceAmountTO.setSurchargeAmount(balanceSur);
		}

		if (balanceOther.doubleValue() > 0) {
			balanceAmountTO.setOtherAmount(balanceOther);
		}

		ReservationCreditTO maxCreditAmountTO = new ReservationCreditTO();
		maxCreditAmountTO.setFareAmount(availableFare);
		maxCreditAmountTO.setTaxAmount(availableTax);
		maxCreditAmountTO.setSurchargeAmount(availableSur);
		maxCreditAmountTO.setOtherAmount(availableOther);

		maxCreditAmountTO.addCreditConsumedGroupNew(ChargeGroup.FAR, ChargeGroup.FAR, availableFare);
		maxCreditAmountTO.addCreditConsumedGroupNew(ChargeGroup.TAX, ChargeGroup.TAX, availableTax);
		maxCreditAmountTO.addCreditConsumedGroupNew(ChargeGroup.SUR, ChargeGroup.SUR, availableSur);
		maxCreditAmountTO.addCreditConsumedGroupNew(ChargeGroup.ANY, ChargeGroup.ANY, availableOther);

		if (balanceFare.doubleValue() < 0) {
			BigDecimal consumedAmount = lendCreditFromOtherGroups(balanceAmountTO, maxCreditAmountTO, balanceFare.negate(),
					ChargeGroup.FAR);

			maxCreditAmountTO.setFareAmount(AccelAeroCalculator.add(maxCreditAmountTO.getFareAmount(), consumedAmount));
		}

		if (balanceSur.doubleValue() < 0) {
			BigDecimal consumedAmount = lendCreditFromOtherGroups(balanceAmountTO, maxCreditAmountTO, balanceSur.negate(),
					ChargeGroup.SUR);
			maxCreditAmountTO.setSurchargeAmount(AccelAeroCalculator.add(maxCreditAmountTO.getSurchargeAmount(), consumedAmount));

		}

		if (balanceTax.doubleValue() < 0) {

			BigDecimal consumedAmount = lendCreditFromOtherGroups(balanceAmountTO, maxCreditAmountTO, balanceTax.negate(),
					ChargeGroup.TAX);

			maxCreditAmountTO.setTaxAmount(AccelAeroCalculator.add(maxCreditAmountTO.getTaxAmount(), consumedAmount));

		}

		if (balanceOther.doubleValue() < 0) {
			BigDecimal consumedAmount = lendCreditFromOtherGroups(balanceAmountTO, maxCreditAmountTO, balanceOther.negate(),
					ChargeGroup.ANY);
			maxCreditAmountTO.setOtherAmount(AccelAeroCalculator.add(maxCreditAmountTO.getOtherAmount(), consumedAmount));

		}

		return maxCreditAmountTO;

	}

	public static BigDecimal getMaxChargeGroupCredit(ReservationCreditTO maxCreditAmountTO, String chargeGroup) {
		BigDecimal credit = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (maxCreditAmountTO != null && maxCreditAmountTO.getTotalCredit().doubleValue() > 0 && chargeGroup != null) {

			if (ChargeGroup.FAR.equals(chargeGroup)) {
				credit = maxCreditAmountTO.getFareAmount();
			} else if (ChargeGroup.TAX.equals(chargeGroup)) {
				credit = maxCreditAmountTO.getTaxAmount();
			} else if (ChargeGroup.SUR.equals(chargeGroup)) {
				credit = maxCreditAmountTO.getSurchargeAmount();
			} else {
				credit = maxCreditAmountTO.getOtherAmount();
			}

		}

		return credit;
	}

	public static BigDecimal getAvailableMaxCredit(ReservationCreditTO maxCreditAmountTO, String chargeGroup,
			BigDecimal chargeAmount) {
		BigDecimal credit = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (maxCreditAmountTO != null && maxCreditAmountTO.getTotalCredit().doubleValue() > 0 && chargeGroup != null) {

			credit = getMaxChargeGroupCredit(maxCreditAmountTO, chargeGroup);

			if (credit.doubleValue() > 0) {
				if (credit.doubleValue() >= chargeAmount.doubleValue()) {
					credit = AccelAeroCalculator.subtract(credit, chargeAmount);

				} else {
					chargeAmount = credit;
					credit = AccelAeroCalculator.getDefaultBigDecimalZero();
				}
			} else {
				chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			}

			if (ChargeGroup.FAR.equals(chargeGroup)) {
				maxCreditAmountTO.setFareAmount(credit);
			} else if (ChargeGroup.TAX.equals(chargeGroup)) {
				maxCreditAmountTO.setTaxAmount(credit);
			} else if (ChargeGroup.SUR.equals(chargeGroup)) {
				maxCreditAmountTO.setSurchargeAmount(credit);
			} else {
				maxCreditAmountTO.setOtherAmount(credit);
			}

		}

		return chargeAmount;
	}


	/**
	 * returns whether charge-group is a valid category for ANY category.
	 * */
	public static boolean isValidOtherChargeGroup(String chargeGroupCode) {

		List<String> otherChargeGroupList = new ArrayList<String>();
		otherChargeGroupList.add(ChargeGroup.CNX);
		otherChargeGroupList.add(ChargeGroup.MOD);
		otherChargeGroupList.add(ChargeGroup.ADJ);
		otherChargeGroupList.add(ChargeGroup.PEN);
		otherChargeGroupList.add(ChargeGroup.INF);
		otherChargeGroupList.add(ChargeGroup.DISCOUNT);

		List<String> validChargeGroupList = new ArrayList<String>();
		validChargeGroupList.add(ChargeGroup.FAR);
		validChargeGroupList.add(ChargeGroup.SUR);
		validChargeGroupList.add(ChargeGroup.TAX);

		if (!validChargeGroupList.contains(chargeGroupCode) && otherChargeGroupList.contains(chargeGroupCode)) {
			return true;
		}

		return false;

	}

	/**
	 * returns applicable credit amount with respect to each charge groups[FAR/TAX/SUR/OTHER] vs ReservationCredit.
	 * */
	public static BigDecimal getEffectiveCredit(BigDecimal remainingCreditToConsume, ReservationCredit reservationCredit,
			String chargeGroupCode) {
		BigDecimal effectiveAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		Set<ReservationPaxPaymentCredit> payments = reservationCredit.getPayments();
		if (reservationCredit != null && payments != null && payments.size() > 0) {
			Iterator<ReservationPaxPaymentCredit> paymentCReditItr = payments.iterator();
			while (paymentCReditItr.hasNext()) {

				ReservationPaxPaymentCredit paymentCredit = paymentCReditItr.next();
				BigDecimal paymentCreditAmount = paymentCredit.getAmount();

				if (chargeGroupCode.equals(paymentCredit.getChargeGroupCode())
						|| (isValidOtherChargeGroup(paymentCredit.getChargeGroupCode()) && isValidOtherChargeGroup(chargeGroupCode))
						&& paymentCreditAmount.doubleValue() > 0) {
					if (paymentCreditAmount.doubleValue() >= remainingCreditToConsume.doubleValue()) {
						effectiveAmount = AccelAeroCalculator.add(effectiveAmount, remainingCreditToConsume);
						remainingCreditToConsume = AccelAeroCalculator.getDefaultBigDecimalZero();
						break;
					} else {
						remainingCreditToConsume = AccelAeroCalculator.subtract(remainingCreditToConsume, paymentCreditAmount);
						effectiveAmount = AccelAeroCalculator.add(effectiveAmount, paymentCreditAmount);
					}
				}

			}

		}

		return effectiveAmount;
	}
	
	
	/**
	 * following function will adjust the payment charges collection[ADD/UPDATE] with respect to the mismatch with
	 * credit amount. always we assume credit amount is the correct value.
	 * 
	 * @param colChargeGroupCodes
	 * */
	public static void validateAndAdjustWithReservationCredit(ReservationCredit reservationCredit,
			Collection<String> colChargeGroupCodes) {
		if (reservationCredit != null && colChargeGroupCodes != null) {

			if (reservationCredit.getBalance().doubleValue() != reservationCredit.getPaymentCreditTotal().doubleValue()) {
				if (reservationCredit.getBalance().doubleValue() > reservationCredit.getPaymentCreditTotal().doubleValue()) {
					BigDecimal difference = AccelAeroCalculator.subtract(reservationCredit.getBalance(),
							reservationCredit.getPaymentCreditTotal());
					ReservationPaxPaymentCredit paxPaymentCredit = new ReservationPaxPaymentCredit();
					paxPaymentCredit.setPaxOndPaymentId(null);
					paxPaymentCredit.setAmount(difference);
					paxPaymentCredit.setRemarks("Balance Out Amount");
					paxPaymentCredit.setChargeGroupCode(ChargeGroup.ADJ);

					reservationCredit.addPayments(paxPaymentCredit);

				} else {
					BigDecimal eliminateCredit = AccelAeroCalculator.subtract(reservationCredit.getPaymentCreditTotal(),
							reservationCredit.getBalance());

					if (colChargeGroupCodes != null && colChargeGroupCodes.size() > 0) {
						for (String chargeGroupCode : colChargeGroupCodes) {

							Collection<ReservationPaxPaymentCredit> selectedPaymentCredits = TnxGranularityUtils
									.getApplicablePaymentCreditByChargeGroup(chargeGroupCode, reservationCredit.getPayments(), false);

							if (selectedPaymentCredits != null && selectedPaymentCredits.size() > 0) {
								for (ReservationPaxPaymentCredit paxPaymentCredit : selectedPaymentCredits) {
									BigDecimal amount = paxPaymentCredit.getAmount();

									if (paxPaymentCredit != null && eliminateCredit.doubleValue() > 0 && amount.doubleValue() > 0) {

										if (amount.doubleValue() >= eliminateCredit.doubleValue()) {
											paxPaymentCredit.setAmount(AccelAeroCalculator.subtract(amount, eliminateCredit));
											eliminateCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
										} else {
											paxPaymentCredit.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
											eliminateCredit = AccelAeroCalculator.subtract(eliminateCredit, amount);

										}

									}

								}

							}
						}
					}
				}
			}

		}

	}
	
	public static Collection<ReservationPaxPaymentCredit> getApplicablePaymentCreditByChargeGroup(String chargeGroupCode,
			Collection<ReservationPaxPaymentCredit> colPaxPaymentCredit, boolean isGroupOnly) {
		Collection<ReservationPaxPaymentCredit> colApplicablePaxPaymentCredit = new ArrayList<ReservationPaxPaymentCredit>();

		for (ReservationPaxPaymentCredit paxPaymentCredit : colPaxPaymentCredit) {
			if (BeanUtils.nullHandler(paxPaymentCredit.getChargeGroupCode()).equals(chargeGroupCode)
					|| (!isGroupOnly && ChargeGroup.ADJ.equals(paxPaymentCredit.getChargeGroupCode()) && "Balance Out Amount"
							.equalsIgnoreCase(paxPaymentCredit.getRemarks()))) {
				colApplicablePaxPaymentCredit.add(paxPaymentCredit);
			}

		}

		return colApplicablePaxPaymentCredit;
	}
	
	public static BigDecimal getTotalAvailableCredit(Map<Long, Collection<ReservationPaxOndPayment>> remainingPayments) {

		BigDecimal availableTotalCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (Collection<ReservationPaxOndPayment> colReservationPaxOndPayment : remainingPayments.values()) {

			for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {

				availableTotalCredit = AccelAeroCalculator.add(availableTotalCredit, reservationPaxOndPayment.getAmount());

			}
		}

		return availableTotalCredit;

	}
	
	public static void extractPaymentCreditConsumedSummaryMap(Map<Long, Map<String, BigDecimal>> creditUtilizedMapByTnx,
			Collection<ReservationPaxOndPayment> composedReservationPaxOndPayment) {

		if (creditUtilizedMapByTnx == null) {
			creditUtilizedMapByTnx = new HashMap<Long, Map<String, BigDecimal>>();
		}

		if (composedReservationPaxOndPayment != null && composedReservationPaxOndPayment.size() > 0) {
			Iterator<ReservationPaxOndPayment> reservationPaxOndPaymentItr = composedReservationPaxOndPayment.iterator();
			while (reservationPaxOndPaymentItr.hasNext()) {
				ReservationPaxOndPayment resPaxOndPayment = reservationPaxOndPaymentItr.next();
				Long paymentTnxId = resPaxOndPayment.getPaymentTnxId();
				String chargeGroup = resPaxOndPayment.getChargeGroupCode();

				Map<String, BigDecimal> paymentCreditUtilizedMap = creditUtilizedMapByTnx.get(paymentTnxId);

				if (paymentCreditUtilizedMap == null) {
					paymentCreditUtilizedMap = new HashMap<String, BigDecimal>();
				}

				BigDecimal chargeAmount = paymentCreditUtilizedMap.get(chargeGroup);

				if (chargeAmount == null) {
					chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				chargeAmount = AccelAeroCalculator.add(chargeAmount, resPaxOndPayment.getAmount().negate());
				paymentCreditUtilizedMap.put(chargeGroup, chargeAmount);

				creditUtilizedMapByTnx.put(paymentTnxId, paymentCreditUtilizedMap);
			}
		}

	}
	
	public static BigDecimal getTotalConsumedCreditAmount(Map<String, BigDecimal> paymentCreditUtilizedMap) {
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paymentCreditUtilizedMap != null && paymentCreditUtilizedMap.size() > 0) {

			for (String chargeGroup : paymentCreditUtilizedMap.keySet()) {
				BigDecimal amount = paymentCreditUtilizedMap.get(chargeGroup);

				if (amount != null) {
					totalAmount = AccelAeroCalculator.add(totalAmount, amount);
				}
			}

		}

		return totalAmount;
	}
	
	private static BigDecimal adjustPaymentCreditAmount(Collection<ReservationPaxPaymentCredit> paymentCredits,
			String chargeGroupCode, BigDecimal consumedCreditAmount, boolean isGroupOnly) {
		Collection<ReservationPaxPaymentCredit> selectedPaymentCredits = TnxGranularityUtils
				.getApplicablePaymentCreditByChargeGroup(chargeGroupCode, paymentCredits, isGroupOnly);

		if (selectedPaymentCredits != null && selectedPaymentCredits.size() > 0) {
			for (ReservationPaxPaymentCredit paxPaymentCredit : selectedPaymentCredits) {
				BigDecimal amount = paxPaymentCredit.getAmount();

				if (paxPaymentCredit != null && consumedCreditAmount.doubleValue() > 0 && amount.doubleValue() > 0) {

					if (amount.doubleValue() >= consumedCreditAmount.doubleValue()) {
						paxPaymentCredit.setAmount(AccelAeroCalculator.subtract(amount, consumedCreditAmount));
						consumedCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {
						paxPaymentCredit.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						consumedCreditAmount = AccelAeroCalculator.subtract(consumedCreditAmount, amount);

					}

				}

			}

		}

		return consumedCreditAmount;
	}

	public static void
			utilizeReservationPaymentCredit(ReservationCredit credit, Map<String, BigDecimal> paymentCreditUtilizedMap) {
		if (credit != null && credit.getPayments() != null && credit.getPayments().size() > 0 && paymentCreditUtilizedMap != null
				&& paymentCreditUtilizedMap.size() > 0)
			for (String chargeGroupCode : paymentCreditUtilizedMap.keySet()) {

				BigDecimal groupChargeAmount = paymentCreditUtilizedMap.get(chargeGroupCode);

				if (groupChargeAmount != null && groupChargeAmount.doubleValue() > 0) {
					groupChargeAmount = adjustPaymentCreditAmount(credit.getPayments(), chargeGroupCode, groupChargeAmount, true);

					if (groupChargeAmount.doubleValue() > 0) {
						// if consumed credit is more than the existing available group credit, then credit would has
						// been consumed from other groups
						groupChargeAmount = adjustPaymentCreditAmount(credit.getPayments(), chargeGroupCode, groupChargeAmount,
								false);
					}
				}

			}
	}

	/**
	 * some flows payment currency amount is missing even-though its in base currency.following function will update the
	 * payment currency amount only when BASE.
	 * */
	public static void updatePayCurrencyDTOPayCurrencyAmount(ReservationTnx creditTnxBF, PayCurrencyDTO payCurrencyDTO) {
		if (payCurrencyDTO != null
				&& AppSysParamsUtil.getBaseCurrency().equals(payCurrencyDTO.getPayCurrencyCode())
				&& ((payCurrencyDTO.getTotalPayCurrencyAmount() != null
						&& creditTnxBF.getAmount().doubleValue() != payCurrencyDTO.getTotalPayCurrencyAmount().doubleValue() && payCurrencyDTO
						.getTotalPayCurrencyAmount().doubleValue() == 0) || (payCurrencyDTO.getTotalPayCurrencyAmount() == null))) {
			payCurrencyDTO.setTotalPayCurrencyAmount(creditTnxBF.getAmount());
		}
	}
	
	public static void normalizePaymentCurrencyAmounts(PayCurrencyDTO payCurrencyDTO,
			ReservationPaxTnxBreakdown reservationPaxTnxBreakdown, PerPaxPriceBreakdown perPaxPriceBreakdown) {

		if (!AppSysParamsUtil.getBaseCurrency().equals(payCurrencyDTO.getPayCurrencyCode())) {
			BigDecimal payCurrencyTotal = reservationPaxTnxBreakdown.getTotalPriceInPayCurrency();

			if (reservationPaxTnxBreakdown.getFareAmount().doubleValue() == 0) {
				reservationPaxTnxBreakdown.setFareAmountInPayCurrency(reservationPaxTnxBreakdown.getFareAmount());
			} else {
				BigDecimal remainTotal = AccelAeroCalculator.add(perPaxPriceBreakdown.getTaxAmount(),
						perPaxPriceBreakdown.getOtherAmount(), perPaxPriceBreakdown.getSurAmount());
				if (remainTotal.doubleValue() == 0) {
					reservationPaxTnxBreakdown.setFareAmountInPayCurrency(payCurrencyTotal);
				} else {
					payCurrencyTotal = AccelAeroCalculator.subtract(payCurrencyTotal,
							reservationPaxTnxBreakdown.getFareAmountInPayCurrency());
				}
			}

			if (reservationPaxTnxBreakdown.getTaxAmount().doubleValue() == 0) {
				reservationPaxTnxBreakdown.setTaxAmountInPayCurrency(reservationPaxTnxBreakdown.getTaxAmount());
			} else {
				BigDecimal remainTotal = AccelAeroCalculator.add(perPaxPriceBreakdown.getOtherAmount(),
						perPaxPriceBreakdown.getSurAmount());
				if (remainTotal.doubleValue() == 0) {
					reservationPaxTnxBreakdown.setTaxAmountInPayCurrency(payCurrencyTotal);
				} else {
					payCurrencyTotal = AccelAeroCalculator.subtract(payCurrencyTotal,
							reservationPaxTnxBreakdown.getTaxAmountInPayCurrency());
				}
			}

			if (reservationPaxTnxBreakdown.getModAmount().doubleValue() == 0) {
				reservationPaxTnxBreakdown.setModAmountInPayCurrency(reservationPaxTnxBreakdown.getModAmount());
			} else {
				BigDecimal remainTotal = perPaxPriceBreakdown.getSurAmount();
				if (remainTotal.doubleValue() == 0) {
					reservationPaxTnxBreakdown.setModAmountInPayCurrency(payCurrencyTotal);
				} else {
					payCurrencyTotal = AccelAeroCalculator.subtract(payCurrencyTotal,
							reservationPaxTnxBreakdown.getModAmountInPayCurrency());
				}
			}

			if (reservationPaxTnxBreakdown.getSurchargeAmount().doubleValue() == 0) {
				reservationPaxTnxBreakdown.setSurchargeAmountInPayCurrency(reservationPaxTnxBreakdown.getSurchargeAmount());
			} else {
				payCurrencyTotal = AccelAeroCalculator.subtract(payCurrencyTotal,
						reservationPaxTnxBreakdown.getSurchargeAmountInPayCurrency());

			}
		}
	}
	
}
