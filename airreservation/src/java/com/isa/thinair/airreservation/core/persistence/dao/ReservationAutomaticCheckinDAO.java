package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckin;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Mohamed Rizwan
 */
public interface ReservationAutomaticCheckinDAO {

	public void saveOrUpdate(Collection<ReservationPaxSegAutoCheckin> autoCheckins);
	
	public void cancelAutomaticCheckin(Collection<Integer> pnrPaxSegAutoCheckinIds);

	public Collection<PaxAutomaticCheckinTO> getReservationAutomaticCheckinsByPaxId(Collection<Integer> pnrPaxIds);

	public Collection<ReservationPaxSegAutoCheckin> getReservationAutomaticCheckins(Collection<Integer> pnrPaxSegAutoCheckinIds);
	
	public Collection<ReservationPaxSegAutoCheckin> getReservationPaxAutoCheckinList(String pnr);

	public int updateReservationPaxSegAutoCheckin(Collection<Integer> pnrpaxIds) throws ModuleException;
	
	public String getSeatPreferenceByFlightAmSeatId(Integer flightAmSeatId);
}
