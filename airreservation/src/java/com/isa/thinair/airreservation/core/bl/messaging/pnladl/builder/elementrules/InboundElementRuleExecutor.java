/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules;

import java.util.ArrayList;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.AvailableSpaceRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.PnrAmmendValidityRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class InboundElementRuleExecutor extends
		BaseRuleExecutor<RulesDataContext> {

	public InboundElementRuleExecutor() {
		if(rulesList == null){
			rulesList = new ArrayList<BaseRule>();
		}
		rulesList.add(new AvailableSpaceRule());
	}
	
	@Override
	public RuleResponse validateElementRules(RulesDataContext context) {
		boolean isValied = true;
		response = new RuleResponse();
		
		for(BaseRule<RulesDataContext> rule:rulesList){
			isValied = rule.validateRule(context);
			if(!isValied){
				break;
			}
		}
		
		BaseRule ammendValidityRule = new PnrAmmendValidityRule();
		response.setProceedNextElement(ammendValidityRule.validateRule(context));
		
		if(isValied){
			response.setSpaceAvailable(isValied);
		}
		return response;
	}

}
