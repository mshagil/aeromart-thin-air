package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAirportTransferDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Manoj Dhanushka
 * 
 * @isa.module.dao-impl dao-name="ReservationAirportTransferDAO"
 */
public class ReservationAirportTransferDAOImpl extends PlatformBaseHibernateDaoSupport implements ReservationAirportTransferDAO {
	
	public void saveOrUpdate(Collection<ReservationPaxSegAirportTransfer> airportTransfers) {
		super.hibernateSaveOrUpdateAll(airportTransfers);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<PaxAirportTransferTO> getReservationAirportTransfersByPaxId(Collection<Integer> pnrPaxIds) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select ppat.ppsat_id, ppat.REQUEST_TIMESTAMP, ppat.CONTACT_NO, ppat.ADDRESS, ppat.pickup_type, ppat.PNR_PAX_ID, ppat.PNR_SEG_ID, ppat.PPF_ID , ppat.AMOUNT, ppat.SALES_CHANNEL_CODE, ppat.USER_ID, ppat.APT_ID, ppat.airport_code, apt.*  ");
		sb.append(" from t_pnr_pax_seg_airport_transfer ppat, t_airport_transfer apt ");
		sb.append(" where ppat.status = 'RES' AND apt.airport_tranfer_id = ppat.apt_id ");
		if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
			sb.append(" AND ppat.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + ")");
		}
		sb.append(" Order by ppat.pnr_seg_id");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<PaxAirportTransferTO> colAirportTransfers = (Collection<PaxAirportTransferTO>) template.query(sb.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<PaxAirportTransferTO> passengerApts = new ArrayList<PaxAirportTransferTO>();
				while (rs.next()) {
					PaxAirportTransferTO paxAptTO = new PaxAirportTransferTO();
					paxAptTO.setPnrPaxSegAptId(new Integer(rs.getInt("ppsat_id")));
					paxAptTO.setRequestTimeStamp(CalendarUtil.formatDate(rs.getTimestamp("REQUEST_TIMESTAMP"), CalendarUtil.PATTERN11));
					paxAptTO.setPnrPaxId(new Integer(rs.getInt("pnr_pax_id")));
					paxAptTO.setPnrSegId(new Integer(rs.getInt("pnr_seg_id")));
					paxAptTO.setContactNo(rs.getString("CONTACT_NO"));
					paxAptTO.setAddress(rs.getString("ADDRESS"));
					paxAptTO.setPpfId(new Integer(rs.getInt("PPF_ID")));
					paxAptTO.setChargeAmount(rs.getBigDecimal("AMOUNT"));
					paxAptTO.setAirportTransferId(new Integer(rs.getInt("APT_ID")));
					paxAptTO.setAirportCode(rs.getString("airport_code"));
					paxAptTO.setUserId(rs.getString("USER_ID"));
					paxAptTO.setSalesChannelCode(new Integer(rs.getInt("SALES_CHANNEL_CODE")));
					paxAptTO.setPickupType(rs.getString("pickup_type"));
					AirportTransferExternalChgDTO externalChgDTO = new AirportTransferExternalChgDTO();
					externalChgDTO.setAmount(rs.getBigDecimal("AMOUNT"));
					paxAptTO.setChgDTO(externalChgDTO);
					AirportTransferTO aptDTO = new AirportTransferTO();
					aptDTO.setProviderAddress(rs.getString("SERVICE_PROVIDER_ADDRESS"));
					aptDTO.setProviderEmail(rs.getString("SERVICE_PROVIDER_EMAIL"));
					aptDTO.setProviderName(rs.getString("SERVICE_PROVIDER_NAME"));
					aptDTO.setProviderNumber(rs.getString("SERVICE_PROVIDER_NUMBER"));
					aptDTO.setAirportTransferId(new Integer(rs.getInt("AIRPORT_TRANFER_ID")));
					paxAptTO.setProvider(aptDTO);
					passengerApts.add(paxAptTO);
				}
				return passengerApts;
			}
		});

		return colAirportTransfers;

	}
	
	@Override
	public Collection<AirportTransferNotificationDetailsDTO> getAirportTransfesForNotification(String pnr, Collection<Integer> pnrSegIds, Collection<Integer> paxSegAptIds) {

		StringBuilder sb = new StringBuilder();
		sb.append("    SELECT pp.title           ,");
		sb.append("    pp.first_name             ,");
		sb.append("    pp.last_name              ,");
		sb.append("    pp.pnr                    ,");
		sb.append("    pe.e_ticket_number        ,");
		sb.append("    f.flight_number           ,");
		sb.append("    fs.segment_code           ,");
		sb.append("    b.logical_cabin_class_code,");
		sb.append("    ppat.request_timestamp    ,");
		sb.append("    ppat.address              ,");
		sb.append("    ppat.contact_no           ,");
		sb.append("    ppat.pickup_type          ,");
		sb.append("    rc.c_mobile_no            ,");
		sb.append("    rc.c_phone_no             ,");
		sb.append("    rc.c_email				 ,");
		sb.append("    at.airport_code			 ,");
		sb.append("    at.service_provider_name  ,");
		sb.append("    at.service_provider_email  ");
		sb.append("     FROM t_pnr_pax_seg_airport_transfer ppat,");
		sb.append("    t_pnr_passenger pp                       ,");
		sb.append("    t_pnr_segment ps                         ,");
		sb.append("    t_flight_segment fs                      ,");
		sb.append("    t_flight f                               ,");
		sb.append("    t_pnr_pax_fare ppf                       ,");
		sb.append("    t_pnr_pax_fare_segment ppfs              ,");
		sb.append("    t_booking_class b                        ,");
		sb.append("    t_pnr_pax_fare_seg_e_ticket ppfse        ,");
		sb.append("    t_pax_e_ticket pe                        ,");
		sb.append("    t_reservation_contact rc					,");
		sb.append("    t_airport_transfer at					 ");
		sb.append("      WHERE ppat.pnr_pax_id   = pp.pnr_pax_id");
		sb.append("    AND ppat.pnr_seg_id       = ps.pnr_seg_id");
		sb.append("    AND ps.flt_seg_id         = fs.flt_seg_id");
		sb.append("    AND fs.flight_id          = f.flight_id");
		sb.append("    AND ppat.ppf_id           = ppf.ppf_id");
		sb.append("    AND ppfs.ppf_id           = ppat.ppf_id");
		sb.append("    AND ppfs.pnr_seg_id       = ppat.pnr_seg_id");
		sb.append("    AND b.booking_code        = ppfs.booking_code");
		sb.append("    AND ppfse.ppfs_id         = ppfs.ppfs_id");
		sb.append("    AND ppfse.pax_e_ticket_id = pe.pax_e_ticket_id");
		sb.append("    AND rc.pnr                = pp.pnr");
		sb.append("    AND at.airport_tranfer_id = ppat.apt_id");
		if (paxSegAptIds != null && paxSegAptIds.size() > 0) {
			sb.append("    AND ppat.ppsat_id      in (");
			sb.append(Util.constructINStringForInts(paxSegAptIds));
			sb.append(" ) ");		
		} else if (pnrSegIds != null && pnrSegIds.size() > 0) {
			sb.append("    AND ppat.pnr_seg_id      in (");
			sb.append(Util.constructINStringForInts(pnrSegIds));
			sb.append(" ) ");
		}
		if (!pnr.isEmpty()) {
			sb.append("  AND pp.pnr = '");
			sb.append(pnr);
			sb.append("' ");
		}
		sb.append("    ORDER BY ppat.pnr_seg_id");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		@SuppressWarnings("unchecked")
		Collection<AirportTransferNotificationDetailsDTO> transferRequests = (Collection<AirportTransferNotificationDetailsDTO>) template.query(
				sb.toString(), new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Collection<AirportTransferNotificationDetailsDTO> colAptResultDTOs = new ArrayList<AirportTransferNotificationDetailsDTO>();

						if (rs != null) {
							while (rs.next()) {
								AirportTransferNotificationDetailsDTO tranferRequest = new AirportTransferNotificationDetailsDTO();
								tranferRequest.setTitle(rs.getString("title"));
								tranferRequest.setFirstName(rs.getString("first_name"));
								tranferRequest.setLastName(rs.getString("last_name"));
								tranferRequest.setPnr(rs.getString("pnr"));
								tranferRequest.setEticketNumber(rs.getString("e_ticket_number"));
								tranferRequest.setFlightNo(rs.getString("flight_number"));
								tranferRequest.setSegmentCode(rs.getString("segment_code"));
								tranferRequest.setCabinClass(rs.getString("logical_cabin_class_code"));
								tranferRequest.setPickupDate(CalendarUtil.formatDate(rs.getTimestamp("request_timestamp"), CalendarUtil.PATTERN9));
								tranferRequest.setPickupTime(CalendarUtil.formatDate(rs.getTimestamp("request_timestamp"), CalendarUtil.PATTERN12));
								tranferRequest.setPickupAddress(rs.getString("address"));
								tranferRequest.setPickupContactNo(rs.getString("contact_no"));
								tranferRequest.setPickupType(rs.getString("pickup_type"));
								tranferRequest.setPaxMobileNo(rs.getString("c_mobile_no"));
								tranferRequest.setPaxPhoneNo(rs.getString("c_phone_no"));
								tranferRequest.setPaxEmail(rs.getString("c_email") == null ? "" : rs.getString("c_email"));
								tranferRequest.setAirportCode(rs.getString("airport_code"));
								tranferRequest.setProviderName(rs.getString("service_provider_name"));
								tranferRequest.setProviderEmail(rs.getString("service_provider_email"));
								colAptResultDTOs.add(tranferRequest);
							}
						}
						return colAptResultDTOs;
					}
				});

		return transferRequests;
	}
	
	@Override
	public void cancelAirportTransfer(Collection<Integer> pnrPaxSegAirportTransferIds) {

		StringBuilder hql = new StringBuilder("UPDATE ReservationPaxSegAirportTransfer SET status=:status ");
		hql.append("WHERE pnrPaxSegAirportTransferId in (");
		hql.append(Util.constructINStringForInts(pnrPaxSegAirportTransferIds));
		hql.append(" ) ");

		Query qry = getSession().createQuery(hql.toString()).setParameter("status", "CNX");
		qry.executeUpdate();
	}
	
	@Override
	public Collection<ReservationPaxSegAirportTransfer> getReservationAirportTransfers(Collection<Integer> pnrPaxSegAirportTransferIds) {

		String inStr = PlatformUtiltiies.constructINStringForCharactors(pnrPaxSegAirportTransferIds);

		Collection<ReservationPaxSegAirportTransfer> paxSegAirportTransfers = find(
				"from ReservationPaxSegAirportTransfer r " + "where r.pnrPaxSegAirportTransferId in (" + inStr + ")", ReservationPaxSegAirportTransfer.class);

		return paxSegAirportTransfers;
	}
}
